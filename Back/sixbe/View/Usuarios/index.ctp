<?php

     App::import('Lib', 'FormBuilder');
     
     $formBuilder = new FormBuilder();
     
     $formBuilder->setDataListado($this, 'Listado de Usuarios', 'Datos de los Usuarios', 'Usuario', 'Usuarios', $usuarios);

     //Headers
     $formBuilder->addHeader('Id', 'Usuario.id', "10%");
     $formBuilder->addHeader('Código', 'Usuario.username', "40%");
     $formBuilder->addHeader('Apellido y Nombre', 'Usuario.apellido_nombres', "40%");

     //Fields
     $formBuilder->addField('Usuario', 'id');
     $formBuilder->addField('Usuario', 'username');
     $formBuilder->addField('Usuario', 'apellido_nombres');
     
     $formBuilder->getListado();
     
?>

                    
