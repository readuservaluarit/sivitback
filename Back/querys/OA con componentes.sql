
SELECT producto.`codigo` codigo_valvula,comprobante.`nro_comprobante` AS NRO_OA,comprobante.`fecha_generacion` AS FECHA_EMISION_OA ,
comprobante.fecha_entrega AS FECHA_ENTREGA_COMPONENTES, comprobante_item.`cantidad` AS CANT_OA,
componente.codigo AS CODIGO_COMPONENTE,componente.d_producto AS DESC_COMPONENTE,producto_relacion.`cantidad_hijo` CANTIDAD_COMPONENTE_NECESARIO_VALVULA,
stock_producto.`stock` AS cantidad_componente_almacen, comprometido.stock AS cantidad_componente_comprometido
FROM comprobante_item
JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`
JOIN producto_relacion ON producto_relacion.`id_producto_padre` = comprobante_item.`id_producto`
JOIN producto ON producto.id = comprobante_item.`id_producto`
JOIN producto AS componente ON componente.id = producto_relacion.`id_producto_hijo`
JOIN stock_producto ON stock_producto.id = componente.id
JOIN stock_producto AS comprometido ON comprometido.id = componente.id
WHERE id_tipo_comprobante=104 AND id_estado_comprobante IN (1,5)
AND stock_producto.`id_deposito` = 4 AND comprometido.id_deposito=6

LIMIT 1000000000