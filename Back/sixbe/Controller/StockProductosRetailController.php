<?php


App::uses('StockProductosController', 'Controller');

class StockProductosRetailController extends StockProductosController {
	
	
    public $name = 'StockProductos';
    public $model = 'StockProducto';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $tipo_producto = EnumProductoTipo::MRETAIL;
    public $padre = "Producto";
    public $d_padre = "d_producto";
    public $deposito_fijo_principal = 4;
    public $deposito_fijo_complementario = 12;


   
    
    
     /**
    * @secured(CONSULTA_STOCK)
    */
    public function index(){
        
        parent::index();
    }
    
    /**
    * @secured(CONSULTA_STOCK)
    */
    public function getModel($vista='default'){
        
        parent::getModel($vista);
    }
}


?>