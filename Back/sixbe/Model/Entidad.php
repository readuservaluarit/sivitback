<?php
App::uses('AppModel', 'Model');

/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class Entidad extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'entidad';

    public $actsAs = array('Containable');
    
    

/**
 * Validation rules
 *
 * @var array
 */
	/*
    public $validate = array(
		'cheque_desde' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar el comienzo de la cartera',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);
    */
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 
 
    public function getCountStaticIndex(){
    	
    	
    	
    	$cant = $this->find('count',
    			array('conditions' => array(
    					'Entidad.estatica_sistema'=>1
    			)));
    			
    	return $cant;		
    	
    	
    			
    }

}
