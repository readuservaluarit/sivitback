/* Stock total valmec componente STVC = Stock tabla + Stock comprometido
   Stock fisico almacen = STVC - Stock OA Completa

*/









SELECT  producto.`codigo`, 
stock_producto.`stock` stock_columna_stock,
sp2.`stock` columna_comprometido,

(stock_producto.`stock` + sp2.`stock`) suma_stock_sistema_mas_comprometido

,producto.id ,producto.`d_producto`,
IFNULL(tabla.cantidad,0) cantidad_oa_completa,
(stock_producto.`stock` + sp2.`stock`) - IFNULL(tabla.cantidad,0) stock_teorico_almacen_fisico



 FROM 
stock_producto

JOIN producto ON producto.`id` = stock_producto.`id`

LEFT JOIN stock_producto sp2 ON sp2.id = stock_producto.`id`

LEFT JOIN 
/*Este Join une con una subtabla de componentes en estado completo o sea aquellos usados en  ordenes de armado que estan completas*/

(SELECT producto_relacion.`id_producto_hijo`,SUM(producto_relacion.`cantidad_hijo`*comprobante_item.`cantidad`) AS cantidad FROM comprobante_item
JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`
JOIN producto_relacion ON producto_relacion.`id_producto_padre` = comprobante_item.`id_producto`

WHERE id_tipo_comprobante = 104

AND comprobante_item.`activo` = 1
AND id_estado_comprobante = 5

GROUP BY producto_relacion.`id_producto_hijo`
LIMIT 1000000000
) tabla ON producto.id = tabla.id_producto_hijo



WHERE stock_producto.id_deposito = 4

AND sp2.`id_deposito` = 6

AND producto.`id_producto_tipo` = 2









LIMIT 10000000000



