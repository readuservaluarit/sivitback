<?php
        include(APP.'View'.DS.'ComprobanteItemLotes'.DS.'json'.DS.'comprobante_item_lote.ctp');

		/*SOLO se se usa desde IRM*/
		
		/*$model["id"]["show_grid"] = 1;
		$model["id"]["type"] = "string";
        $model["id"]["header_display"] = "ID";
        $model["id"]["order"] = "1";
        $model["id"]["width"] = "2";
        $model["id"]["text_colour"] = "#660000";
        */
		
		/*
		$model["id_lote"]["show_grid"] = 0;
		$model["id_lote"]["type"] = "string";
        $model["id_lote"]["header_display"] = "ID Lote";
        $model["id_lote"]["order"] = "1";
        $model["id_lote"]["width"] = "2";
        $model["id_lote"]["text_colour"] = "#660000";
		*/
		
		$model["d_tipo_lote"]["show_grid"] = 1;
        $model["d_tipo_lote"]["header_display"] = "Tipo Lote";
        $model["d_tipo_lote"]["order"] = "2";
        $model["d_tipo_lote"]["width"] = "13";
        $model["d_tipo_lote"]["text_colour"] = "#660000";
		
		/*Se usa desde IRM*/
        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "C&oacute;digo";
        $model["codigo"]["order"] = "3";
        $model["codigo"]["width"] = "18";
        $model["codigo"]["text_colour"] = "#660000";  
		$model["codigo"]["picker"] = EnumMenu::mnuLotePicker;
		
		$model["d_tiene_certificado"]["show_grid"] = 1;
        $model["d_tiene_certificado"]["header_display"] = "Cert.";
        $model["d_tiene_certificado"]["order"] = "4";
        $model["d_tiene_certificado"]["width"] = "7";
        $model["d_tiene_certificado"]["text_colour"] = "#660000";
       	$model["d_tiene_certificado"]["read_only"] = "0";
       	
       	$model["d_validado"]["show_grid"] = 1;
        $model["d_validado"]["header_display"] = "Val.";
        $model["d_validado"]["order"] = "5";
        $model["d_validado"]["width"] = "7";
        $model["d_validado"]["text_colour"] = "#660000";
       	$model["d_validado"]["read_only"] = "0";
       	
        $output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "ComprobanteItemLote",
				"content" => $model
			);
        
          echo json_encode($output);
?>