SELECT comprobante.id 
FROM comprobante_item 

JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`
JOIN comprobante_item factura_item ON factura_item.`id_comprobante_item_origen` = comprobante_item.`id`
JOIN comprobante factura ON factura.id = factura_item.`id_comprobante`




WHERE comprobante.id_tipo_comprobante=202 AND comprobante.id_estado_comprobante=1 AND factura.`id_estado_comprobante` IN (115,116)

AND factura.`id_tipo_comprobante` IN (801,806) AND comprobante_item.`activo` = 1

AND factura_item.`activo` = 1


GROUP BY comprobante_item.`id_comprobante`

HAVING SUM(factura_item.cantidad)=SUM(comprobante_item.`cantidad`)
