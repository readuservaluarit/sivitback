<?php
App::uses('AppModel', 'Model');

/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class Controlador extends AppModel {

    public $useTable = 'controlador';
    public $actsAs = array('Containable');

    
}


