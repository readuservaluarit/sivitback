SELECT 
                           
                            persona.id,
                            persona.razon_social,
                            persona.tel,
                            persona.codigo,
                
                              SUM( (comprobante.total_comprobante/comprobante.valor_moneda2)*tipo_comprobante.signo_comercial) AS total_comprobante,
                              
                              
                            
                        
                             
                          
                              SUM(
                              
                              ( 
                              ROUND(comprobante.total_comprobante/comprobante.valor_moneda2,4) - 
                              /*Le RESTO LOS RECIBOS O LAS ORDENES DE PAGO*/
                              IFNULL(
                                (SELECT 
                                  SUM(precio_unitario/c.valor_moneda2) 
                                FROM
                                  comprobante_item 
                                  JOIN comprobante c 
                                    ON c.id = comprobante_item.`id_comprobante` 
                                WHERE id_comprobante_origen = comprobante.id 
                                  AND c.id_tipo_comprobante IN (208,207) 
                                  AND c.id_estado_comprobante  IN (1,115,116) 
                             
                                 ),
                                0
                              )
                              
                              )*tipo_comprobante.signo_comercial
                              
                              
                              ) AS saldo 
                              
                            FROM
                              comprobante 
                              LEFT JOIN punto_venta 
                                ON comprobante.id_punto_venta = punto_venta.id 
                              LEFT JOIN tipo_comprobante 
                                ON tipo_comprobante.id = comprobante.id_tipo_comprobante 
                          
                              LEFT JOIN persona
                                ON persona.id = comprobante.id_persona
                            WHERE id_tipo_comprobante IN (
                                801,806,811,817,833,804,809,815,836,803,808,813,818,835,802,807,812,819,834,200,209,210,0
                              ) 
                              AND id_estado_comprobante IN (115, 116) 
            
                              AND comprobante.definitivo = 1  
                            
                            
                            AND comprobante.id_persona NOT IN (1,2)
                          
                              GROUP BY comprobante.id_persona 
                              
                              HAVING 
                              
                              SUM(
                              
                              ( 
                              ROUND(comprobante.total_comprobante/comprobante.valor_moneda2,4) - 
                              /*Le RESTO LOS RECIBOS O LAS ORDENES DE PAGO*/
                              IFNULL(
                                (SELECT 
                                  SUM(precio_unitario/c.valor_moneda2) 
                                FROM
                                  comprobante_item 
                                  JOIN comprobante c 
                                    ON c.id = comprobante_item.`id_comprobante` 
                                WHERE id_comprobante_origen = comprobante.id 
                                  AND c.id_tipo_comprobante IN (208,207) 
                                  AND c.id_estado_comprobante  IN (1,115,116) 
                             
                                 ),
                                0
                              )
                              
                              )*tipo_comprobante.signo_comercial
                              
                              
                              ) <> 0