<?php


class MisDatosController extends AppController {
    public $name = 'MisDatos';
    public $model = 'Usuario';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
  
  
  
    public function index() {
        
 
        
            $this->loadModel($this->model);
            $this->loadModel("EstadoTipoComprobante");
            $this->loadModel("TipoComprobantePersona");
            $this->loadModel("SistemaMonedaEnlazada");
            $this->loadModel("Entidad");
			// $this->loadModel("Moneda");//por el momento voy a usar un Cbo aux en memoria como cache
            $id_segmento_temporal=0;
            $id=$this->Session->read("Auth.User.id"); 
           
            $this->Usuario->id = $id;
            
            $this->Usuario->contain(array('Rol','PuntoVenta'));
            $this->request->data = $this->Usuario->read();
            
         if($this->RequestHandler->ext != 'json') {
        
            $rol = $this->request->data['Rol']['id'];  
            
            App::import('Lib', 'FormBuilder');
            $formBuilder = new FormBuilder();

            //Configuracion del Formulario
            $formBuilder->setController($this);
            $formBuilder->setModelName($this->model);
            $formBuilder->setControllerName($this->name);
            $formBuilder->setTitulo('Usuarios');
            $formBuilder->setTituloForm('Mis datos de Usuario');
            //Form
            $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal  well span6'));

            //obtengo el rol del usuario logueado
            $codigo_rol=$this->Session->read("Auth.User.Rol.codigo");
       
            $this->request->data['Usuario']['password'] = '';
            $this->request->data['Usuario']['password_new'] = '';
            $this->request->data['Usuario']['password_new_confirm'] = '';
         
            $formBuilder->addFormBeginRow();
            
                                        
            $formBuilder->addFormHtml("<div></div>");
                                                                 
            $formBuilder->addFormInput('username', 'Usuario', array('class'=>'control-group span10'), array('disabled' => true,'class' => 'required', 'label' => false));
            
            $formBuilder->addFormInput('nombre', 'Apellido y Nombres', array('class'=>'control-group span10'), array('class' => 'required', 'label' => false));
            $formBuilder->addFormInput('email', 'E-Mail', array('class'=>'control-group span10'), array('class' => 'required', 'label' => false));
            $formBuilder->addFormInput('password', 'Contrase&ntilde;a Actual', array('class'=>'control-group span10'), array('class' => 'required', 'label' => false));
            $formBuilder->addFormInput('password_new', 'Contrase&ntilde;a Nueva', array('class'=>'control-group span10'), array('class' => 'required', 'label' => false, 'type'=>'password'));
            $formBuilder->addFormInput('password_new_confirm', 'Confirmar Contrase&ntilde;a Nueva', array('class'=>'control-group span10'), array('class' => 'required', 'label' => false, 'type'=>'password'));
            
            //Fortaleza
            $formBuilder->addFormHtml("<div id='fortaleza' class='control-group span10'>
                                         <label class='control-label' for='progressBar'>Fortaleza</label>
                                            <div class='controls' style = 'border:1px solid rgb(204, 204, 204);height:20px;width:220px;border-radius:5px;'>
                                                <div class='progress progress-striped'>
                                                    <div id='progressBar' class='progress-bar progress-bar-success' role='progressbar' aria-valuenow='40' aria-valuemin='0' aria-valuemax='100' style='width: 0%; max-width:220px;'>
                                                        <span id='progressBarMsg' class='sr-only'>40% Complete (success)</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>");
            $formBuilder->addFormHtml("<a  style='margin-bottom:20px;float:left;cursor:pointer;' id='CambiarPassword'  >Cambiar Constrase&ntilde;a</a>");
            $formBuilder->addFormHtml("<div></div>");
            $script = "
                    
                    $( '#CambiarPassword' ).click(function() {
                    
                         jQuery(this).hide();
                         jQuery('#UsuarioPassword').parent().parent().show();
                         jQuery('#UsuarioPasswordNew').parent().parent().show();
                         jQuery('#UsuarioPasswordNewConfirm').parent().parent().show();    
                         jQuery('#fortaleza').show();   
                        });
                        
                        $( document ).ready(function () {
                         jQuery('#UsuarioPassword').parent().parent().hide();
                         jQuery('#UsuarioPasswordNew').parent().parent().hide();
                         jQuery('#UsuarioPasswordNewConfirm').parent().parent().hide(); 
                         jQuery('#fortaleza').hide();
                         jQuery('#btnLimpiar').hide();
                        
                        });
            
                        $( '#UsuarioPasswordNew' ).keyup(function() {
                            
                            var desc = new Array();
                            desc[0] = ' Muy D&eacute;bil ';
                            desc[1] = ' D&eacute;bil ';
                            desc[2] = ' Aceptable ';
                            desc[3] = ' Buena ';
                            desc[4] = ' Fuerte ';
                            desc[5] = ' Muy Fuerte ';
                            var classCss = 'sucess';
                            var width = '100%';
                            
                             var pass_strong = passwordStrength(jQuery(this).val());
                             switch(pass_strong) {
                                case 0:  classCss = 'danger';
                                         width = '30%';
                                    
                                    break;
                                case 1: classCss = 'danger';
                                        width = '30%';
                                    break;
                                case 2: classCss = 'warning';
                                        width = '40%';
                                    break;
                                case 3: classCss = 'warning';
                                         width = '60%';
                                    break;
                                case 4: classCss = 'sucess';
                                         width = '80%';
                                    break;
                                case 5: classCss = 'sucess';
                                        width = '100%';
                                    break;            
                             }                                          
                             jQuery('#progressBar').css('width',width);
                             jQuery('#progressBarMsg').html(desc[pass_strong] + '  ' + width );
                             var className = jQuery('#progressBar').attr('class');
                             jQuery( '#progressBar' ).removeClass( className );
                             className = 'progress-bar progress-bar-' + classCss;
                             jQuery( '#progressBar' ).addClass( className );
                             
                        });
                        
                        function passwordStrength(password)
                        {
                           

                            var score   = 0;

                            //if password bigger than 6 give 1 point
                            if (password.length > 6) score++;

                            //if password has both lower and uppercase characters give 1 point    
                            if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) score++;

                            //if password has at least one number give 1 point
                            if (password.match(/\d+/)) score++;

                            //if password has at least one special caracther give 1 point
                            if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) )    score++;

                            //if password bigger than 12 give another 1 point
                            if (password.length > 12) score++;

                             return score;
                             
                             
                        }";  
            
           
           $formBuilder->addCommonScript($script);
          
          $script="
          
                    
                    
                    function validateForm() {
                    
                        
                        
                        Validator.clearValidationMsgs('validationMsg_');
                        var form = 'UsuarioIndexForm';
                        var validator = new Validator(form);
                        
                        
                        
                        
                        validator.validateRequired('UsuarioNombre', 'Debe ingresar un Apellido y Nombre');
                        validator.validateRequired('UsuarioEmail', 'Debe ingresar un Email');
                        validator.validateEmail('UsuarioEmail', 'El e-mail es invalido');
                        
                      
                      
                      if (! jQuery('#UsuarioPassword').parent().parent().is(':hidden') )  {
                       
                             validator.validateRequired('UsuarioPasswordNew', 'La Constrase&ntilde;a Nueva es requerida');
                             validator.validateRequired('UsuarioPasswordNewConfirm','La confirmaci&oacute;n de la Constrase&ntilde;a Nueva es requerida');   
                              
                            if( $('#UsuarioPasswordNew').val() != $('#UsuarioPasswordNewConfirm').val()  ){
                                    validator.addErrorMessage('UsuarioPasswordNewConfirm','Las Constrase&ntilde;as ingresadas no coinciden, por favor verif&iacute;quelo.');
                                    validator.showValidations('', 'validationMsg_');
                                    return false;
                            }else{
                                //Valido si el password actual es valido
                               $.ajax({
                                'url': './".$this->name."/valida_password',
                                'data': $('#UsuarioIndexForm').serialize(),
                                'async': false,
                                'success': function(data) {
                                    console.log(data);  
                                    if(data !=1){
                                      
                                       validator.addErrorMessage('UsuarioPassword','La Constrase&ntilde;a actual ingresada no es v&aacute;lida , verif&iacute;quela.');
                                       validator.showValidations('', 'validationMsg_');
                                       
                                       return false; 
                                    }
                                    
                                   
                                   
                                    }
                                });
                           }      
                      }
                          
                    if(!validator.isAllValid()){
                            validator.showValidations('', 'validationMsg_');
                            return false;   
                    }
                        return true;     
                     }";
                     
            $formBuilder->addCommonScript($script);   
            $formBuilder->setFormCancelButtonParameters(array('controller' =>'Usuarios', 'action' => 'home'));
            

            $formBuilder->setFormValidationFunction('validateForm()');
            $formBuilder->addFormEndRow();
            
            
             /////Fin TAB General
            $this->set('abm',$formBuilder);
            $this->set('mode', "M");
            $this->set('id', $id);
            $this->render('/FormBuilder/abm');
            
         }else{ // por json
         
         	
         	
         	
         	$this->loadModel(EnumModel::Usuario);
         	
         	$data = $this->Usuario->MisDatos($this->request->data,$this);
         	
         	
           
            
            
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>0
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
             
             
         }  
       
        
    }
    
   
    function edit($id) {
     
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->Usuario->id = $id;
            $datos = 0;
            unset( $this->request->data['Usuario']['username']);
            unset( $this->request->data['Usuario']['id_usuario']); /*Seguridad Usuario B2B*/
            unset( $this->request->data['Usuario']['id_rol']); /*Seguridad Usuario B2B*/
            unset( $this->request->data['Usuario']['activo']); /*Seguridad Usuario B2B*/
            unset( $this->request->data['Usuario']['intentos_fallidos']); /*Seguridad Usuario B2B*/
            unset( $this->request->data['Usuario']['cantidad_maxima_sesiones_multiples']); /*Seguridad Usuario B2B*/
            
            
            if( strlen($this->request->data['Usuario']['password']) > 0  )
                $datos = json_decode($this->valida_password(1)); 
            if( isset( $this->request->data['Usuario']['password']) && mb_strlen($this->request->data['Usuario']['password']) > 0 && $datos !=0  )
                $this->request->data['Usuario']['password'] = AuthComponent::password($this->request->data['Usuario']['password_new']); 
            else
                unset( $this->request->data['Usuario']['password']);
                
                
            $this->request->data['Usuario']['fecha_modificacion'] = date('Y-m-d H:i:s');    
           
            if ($this->Usuario->save($this->request->data["Usuario"])){
                $this->Session->setFlash('El usuario ha sido modificado exitosamente.', 'success');
                $this->redirect(array('controller' =>EnumController::MisDatos, 'action' => 'MisDatosPanel'));
            }else{
                $this->Session->setFlash('El usuario No ha sido modificado correctamente.', 'error');
                $this->redirect(array('controller' =>EnumController::MisDatos, 'action' => 'MisDatosPanel'));
                
            }
        } 
        $this->redirect(array('controller' =>EnumController::MisDatos, 'action' => 'MisDatosPanel'));
    }
    
    function back(){
        
            App::import('Lib', 'FormBuilder');
            $formBuilder = new FormBuilder();

            //Configuracion del Formulario
            $formBuilder->setController($this);
            $formBuilder->setModelName($this->model);
            $formBuilder->setControllerName($this->name);
            //Form
            $formBuilder->setForm2($this->model,  array('style' => 'display:none'));
        
            $script = "
                    
                        $( document ).ready(function () {
                             alert(1);
                        
                        });
                        
                       ; "; 
            
           
           $formBuilder->addCommonScript($script);
           
            $this->set('abm',$formBuilder);
            $this->set('mode', "M");
            $this->render('/FormBuilder/abm');  
        
    }
    
    
    function valida_password($llamada_interna=0) {
        
        //valido el password actual cuando lo cambia
        $this->loadModel("Usuario");
        if($llamada_interna == 0){
        	$password = AuthComponent::password($this->request->data['Usuario']['password']);
        	$data = $this->Usuario->find('count', array('conditions'=>array('Usuario.id'=>AuthComponent::user('id'),'Usuario.password'=>$password),'contain' => false));
            echo json_encode($data);
        
        }else{
             $password = AuthComponent::password($this->request->data['Usuario']['password']);
             $data = $this->Usuario->find('count', array('conditions'=>array('Usuario.id'=>$this->request['data']['Usuario']['id'],'Usuario.password'=>$password),'contain' => false));
             return $data;
        }
        
        die();
    }
    
    
     
    
    
     
     
     
     public function getServerTime(){
         
           $data['Server']['fecha_hoy'] = (string) date("Y-m-d H:i:s");      //TODO: IMPORTANTE que el front LEA ESTO es para que el front no tome la hora local de la maquina sino la del servidor
           $data['Server']['fecha_formato'] = (string) 'YYYY-MM-DD HH:MM:SS';      //TODO: 
            
           $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>0
            );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
     } 
     
     
     
     
     
     
     
     
     
     
     
     public function getCacheModel(){
     	
		$this->loadModel(EnumModel::DatoEmpresa);
		$output = $this->DatoEmpresa->getCacheModel();
		echo json_encode($output);
		die();
     	
     
     }
     
     
     
     
     
     public function getCacheIndexEstaticaSistema(){
     	
     	
     	//$getModelCache = unserialize(Cache::read(EnumCacheName::getModel));
     	
     	$getModelCache = unserialize(file_get_contents(APP."webroot/media_file/cache_get_index_estatica_sistema.txt"));
     	

     	if ($getModelCache == false) { //esto no va
     		
     		
     		$error = EnumError::ERROR;
     		$message = "La cache no existe en el servidor";
     		
     		
     	}else {
     		
     		$error = EnumError::SUCCESS;
     		
     	}
     	
     	
     	// echo 'true';
     	// die();
     	
     	$output = array(
     			"status" => $error,
     			"message" => "list",
     			"content" =>$getModelCache,
     			"page_count" =>0
     	);
     	echo json_encode($output);
     	die();
     	
     	
     }
     
     
     public function getCacheIndexTablasEstaticaCliente(){
     	
     	
     	//$getModelCache = unserialize(Cache::read(EnumCacheName::getModel));
     	
     	$getModelCache = unserialize(file_get_contents(EnumCacheName::getCacheIndexTablasEstaticaSistema));
     	
     	
     	if ($getModelCache == false) { //si por algo no esta la regenero
     		
     		
	     	$error = EnumError::ERROR;
	     	$message = "La cache no existe en el servidor";
     		
     		
     	}
     	
     	
     	// echo 'true';
     	// die();
     	
     	$output = array(
     			"status" =>EnumError::SUCCESS,
     			"message" => "list",
     			"content" =>$getModelCache,
     			"page_count" =>0
     	);
     	echo json_encode($output);
     	die();
     	
     	
     }
     
     
     public function MisDatosPanel(){
     	

     		
     		
     		
     		$this->loadModel($this->model);
     		$this->loadModel("EstadoTipoComprobante");
     		$this->loadModel("TipoComprobantePersona");
     		$this->loadModel("SistemaMonedaEnlazada");
     		$this->loadModel("Entidad");
     		// $this->loadModel("Moneda");//por el momento voy a usar un Cbo aux en memoria como cache
     		$id_segmento_temporal=0;
     		$id=$this->Session->read("Auth.User.id");
     		
     		$this->Usuario->id = $id;
     		
     		$this->Usuario->contain(array('Rol','PuntoVenta'));
     		$this->request->data = $this->Usuario->read();
     		
     		if($this->RequestHandler->ext != 'json') {
     			
     			$rol = $this->request->data['Rol']['id'];
     			
     			App::import('Lib', 'FormBuilderNew');
     			$formBuilder = new FormBuilderNew();
     			
     			

     			//Configuracion del Formulario
     			$formBuilder->setController($this);
     			$formBuilder->setModelName($this->model);
     			$formBuilder->setControllerName($this->name);
     			$formBuilder->setTitulo('Usuarios');
     			$formBuilder->setTituloForm('Mis datos de Usuario');
     			
     			$formBuilder->showBackButton(true);
     			//Form
     			$formBuilder->setForm2($this->model,  array('class' => ''));
     			
     			//obtengo el rol del usuario logueado
     			$codigo_rol=$this->Session->read("Auth.User.Rol.codigo");
     			
     			$this->request->data['Usuario']['password'] = '';
     			$this->request->data['Usuario']['password_new'] = '';
     			$this->request->data['Usuario']['password_new_confirm'] = '';
     			$this->request->data['Usuario']['email_password'] = '';
     			
     			$formBuilder->setBackButtonParameters(array('controller' => $this->controllerName, 'action' => 'MisDatosPanel'));
     			
     			$formBuilder->addFormBeginRow();
     			
     			
     			
     			//$formBuilder->addFormHtml("<div></div>");
     			
     			$formBuilder->addFormInput('username', 'Usuario', array('class'=>'form-group row'), array('disabled' => true,'class' => 'form-control form-control-sm required', 'label' => false));
     			
     			$formBuilder->addFormInput('nombre', 'Apellido y Nombres', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm required', 'label' => false));
     			$formBuilder->addFormInput('email', 'E-Mail', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm required', 'label' => false));
     			$formBuilder->addFormInput('email_password', 'Contrase&ntilde;a Email (para envio de comprobantes)', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm  required', 'label' => false,'type'=>'password'));
     			$formBuilder->addFormInput('password', 'Contrase&ntilde;a Actual', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm  required', 'label' => false));
     			
     			
     			$formBuilder->addFormInput('password_new', 'Contrase&ntilde;a Nueva', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm  required', 'label' => false, 'type'=>'password'));
     			$formBuilder->addFormInput('password_new_confirm', 'Confirmar Contrase&ntilde;a Nueva', array('class'=>'form-group row'), array('class' => 'form-control  form-control-sm required', 'label' => false, 'type'=>'password'));
     			
     			
     			
     			$formBuilder->addFormHtml("<a class='btn btn-primary' style='margin-bottom:20px;float:left;cursor:pointer;color:white;' id='enviaMailPrueba'  > <i class='fa fa-envelope'> Enviar E-mail prueba</i></a>");
     			
     			
     			//Fortaleza
     			$formBuilder->addFormHtml("<div id='fortaleza' >
                                         <label for='progressBar'>Fortaleza</label>
                                            <div class='controls' style = 'border:1px solid rgb(204, 204, 204);height:20px;width:220px;border-radius:5px;'>
                                                <div class='progress progress-striped'>
                                                    <div id='progressBar' class='alert alert-primary' role='progressbar' aria-valuenow='40' aria-valuemin='0' aria-valuemax='100' style='width: 0%; max-width:220px;'>
                                                        <span id='progressBarMsg' ></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>");
     			$formBuilder->addFormHtml("<a class='btn btn-primary' style='margin-left:20px;margin-bottom:20px;float:left;cursor:pointer;color:white;' id='CambiarPassword'  > <i class='fa fa-key'> Cambiar Constrase&ntilde;a</i></a>");

     			$script = "


				$('#btnCancelar').hide();//lo oculto pero no sirve en este form, tener en cuenta que gracias a este boton se serializa el form

  				$( '#enviaMailPrueba' ).click(function() {


						

  						Validator.clearValidationMsgs('validationMsg_');
                        var form = 'UsuarioMisDatosPanelForm';
                        var validator = new Validator(form);
     					
     					
     					
     					
                  
                        validator.validateRequired('UsuarioEmail', 'Debe ingresar un Email');
                        validator.validateEmail('UsuarioEmail', 'El e-mail es invalido');

     					if(!validator.isAllValid()){
                            validator.showValidations('', 'validationMsg_');
                            return false;
                    		}

   						$.ajax({
                                'url': '".Router::url(array(
                                		'controller' => EnumController::MailSender,
                                		'action' => 'envioPrueba',
                                		
                                ))."',
                             	'data': { 'data[Usuario][email_test_destino]': $('#UsuarioEmail').val()},
                                'async': false,
								'type': 'POST',
								'dataType': 'json',
                                'success': function(data) {

					

                                 
                                 


                         
                                    if(data.status == 'error'){
			
                                		
                                       validator.addErrorMessage('UsuarioEmail',data.message);
                                       validator.showValidations('', 'validationMsg_');
                                		
                                       return false;
                                    }else{

										Validator.clearValidationMsgs('validationMsg_');//limpio validaciones
										alert(data.message);

									}
                                }
					});



				
					    });

				  
     					
                    $( '#CambiarPassword' ).click(function() {
     					

            			 $('#enviaMailPrueba').hide();
                         $(this).hide();
                         $('#UsuarioPassword').parent().parent().show();
                         $('#UsuarioPasswordNew').parent().parent().show();
                         $('#UsuarioPasswordNewConfirm').parent().parent().show();
                         $('#fortaleza').show();
                        });
     					
                        $( document ).ready(function () {
                       	 $('#UsuarioPassword').parent().parent().hide();
                         $('#UsuarioPasswordNew').parent().parent().hide();
                         $('#UsuarioPasswordNewConfirm').parent().parent().hide();
                         $('#fortaleza').hide();
                         $('#btnLimpiar').hide();
     					
                        });
     					
                        $( '#UsuarioPasswordNew' ).keyup(function() {
     					
                            var desc = new Array();
                            desc[0] = ' Muy D&eacute;bil ';
                            desc[1] = ' D&eacute;bil ';
                            desc[2] = ' Aceptable ';
                            desc[3] = ' Buena ';
                            desc[4] = ' Fuerte ';
                            desc[5] = ' Muy Fuerte ';
                            var classCss = 'sucess';
                            var width = '100%';
     					
                             var pass_strong = passwordStrength(jQuery(this).val());

                             switch(pass_strong) {
                                case 0:  classCss = 'alert alert-danger';
                                         width = '30%';
     					
                                    break;
                                case 1: classCss = 'alert alert-danger';
                                        width = '30%';
                                    break;
                                case 2: classCss = 'alert alert-warning';
                                        width = '40%';
                                    break;
                                case 3: classCss = 'alert alert-warning';
                                         width = '60%';
                                    break;
                                case 4: classCss = 'alert alert-success';
                                         width = '80%';
                                    break;
                                case 5: classCss = 'alert alert-success';
                                        width = '100%';
                                    break;
                             }
                             jQuery('#progressBar').css('width',width);
                             jQuery('#progressBarMsg').html(desc[pass_strong] + '  ' + width );
                             var className = jQuery('#progressBar').attr('class');
                             jQuery( '#progressBar' ).removeClass( className );
                             className = 'progress-bar progress-bar-' + classCss;
                             jQuery( '#progressBar' ).addClass( className );
     					
                        });
     					
                        function passwordStrength(password)
                        {
     					
     					
                            var score   = 0;
     					
                            //if password bigger than 6 give 1 point
                            if (password.length > 6) score++;
     					
                            //if password has both lower and uppercase characters give 1 point
                            if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) score++;
     					
                            //if password has at least one number give 1 point
                            if (password.match(/\d+/)) score++;
     					
                            //if password has at least one special caracther give 1 point
                            if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) )    score++;
     					
                            //if password bigger than 12 give another 1 point
                            if (password.length > 12) score++;
     					
                             return score;
     					
     					
                        }";
     			
     			
     			$formBuilder->addCommonScript($script);
     			
     			$script="
     					
     					
     					
                    function validateForm() {
     					
     					
     					
                        Validator.clearValidationMsgs('validationMsg_');
                        var form = 'UsuarioMisDatosPanelForm';
                        var validator = new Validator(form);
     					
     					
     					
     					
                        validator.validateRequired('UsuarioNombre', 'Debe ingresar un Apellido y Nombre');
                        validator.validateRequired('UsuarioEmail', 'Debe ingresar un Email');
                        validator.validateEmail('UsuarioEmail', 'El e-mail es invalido');
     					
     					
     					
                      if (! jQuery('#UsuarioPassword').parent().parent().is(':hidden') )  {
     					
                             validator.validateRequired('UsuarioPasswordNew', 'La Constrase&ntilde;a Nueva es requerida');
                             validator.validateRequired('UsuarioPasswordNewConfirm','La confirmaci&oacute;n de la Constrase&ntilde;a Nueva es requerida');
     					
                            if( $('#UsuarioPasswordNew').val() != $('#UsuarioPasswordNewConfirm').val()  ){
                                    validator.addErrorMessage('UsuarioPasswordNewConfirm','Las Constrase&ntilde;as ingresadas no coinciden, por favor verif&iacute;quelo.');
                                    validator.showValidations('', 'validationMsg_');
                                    return false;
                            }else{
                                //Valido si el password actual es valido
                               $.ajax({
                                'url': '".Router::url(array(
                                		'controller' => EnumController::MisDatos,
                                		'action' => 'valida_password',
                                		
                                ))."',
                             	'data': { 'data[Usuario][password]': $('#UsuarioPassword').val()},
                                'async': false,
								'type': 'POST',
                                'success': function(data) {
                                    console.log(data);
                                    if(data !=1){
                                		
                                       validator.addErrorMessage('UsuarioPassword','La Constrase&ntilde;a actual ingresada no es v&aacute;lida , verif&iacute;quela.');
                                       validator.showValidations('', 'validationMsg_');
                                		
                                       return false;
                                    }
                                		
                                		
                                		
                                    }
                                });
                           }
                      }
                                		
                    if(!validator.isAllValid()){
                            validator.showValidations('', 'validationMsg_');
                            return false;
                    }
                        return true;
                     }";
     			
     			$formBuilder->addCommonScript($script);
     			$formBuilder->setFormCancelButtonParameters(array('controller' =>'Usuarios', 'action' => 'home'));
     			
     			
     			$formBuilder->setFormValidationFunction('validateForm()');
     			$formBuilder->addFormEndRow();
     			
     			
     			
     			$this->layout = "desktop_panel";
     			/////Fin TAB General
     			$this->set('abm',$formBuilder);
     			$this->set('mode', "M");
     			$this->set('id', $id);
     			$this->render('/FormBuilder/abm');
     			
     }
     }
     
     
     public function getVersion(){
     	
     	
     	$this->loadModel(EnumModel::DatoEmpresa);
     	
     	$datoEmpresa = $this->DatoEmpresa->find('first', array('conditions' => array('DatoEmpresa.id' => 1), 'contain' => false));
     	
     	
     	$output = array(
     			"status" =>EnumError::SUCCESS,
     			"message" => "",
     			"content" => "",
     			"product_version" => $datoEmpresa["DatoEmpresa"]["product_version"],
     	);
     	
     	
     	
     	echo json_encode($output);
     	die();
     }
     
     
     public function beforeFilter(){
     	
     	App::uses('ConnectionManager', 'Model');
     	
     	$this->Auth->allow('getVersion');
     	
     	parent::beforeFilter();
     }
    
    
 
    
 
    
  
}
?>