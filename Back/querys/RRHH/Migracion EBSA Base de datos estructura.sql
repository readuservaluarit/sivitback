
/* Alter table in target */
ALTER TABLE `comprobante` 
	ADD COLUMN `chk_envia_mail` TINYINT(4)   NULL AFTER `cantidad_impresion_pdf` , 
	CHANGE `id_tipo_documento` `id_tipo_documento` INT(11)   NULL AFTER `chk_envia_mail` , 
	DROP COLUMN `envia_mail` ;

/* Alter table in target */
ALTER TABLE `comprobante_item` 
	DROP KEY `comprobante_item_ibfk_10` , 
	DROP KEY `comprobante_item_ibfk_9` , 
	ADD KEY `id_detalle_tipo_comprobante`(`id_detalle_tipo_comprobante`) , 
	ADD KEY `id_persona`(`id_persona`) ;
	
	
	
	
/* Create table in target */
CREATE TABLE `estado_rrhh_liquidacion`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_estado_rrhh_liquidacion` VARCHAR(20) COLLATE utf8mb4_general_ci NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `operador_funcion_matematica`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`codigo` VARCHAR(5) COLLATE latin1_swedish_ci NOT NULL  , 
	`d_operador_funcion_matematica` VARCHAR(10) COLLATE latin1_swedish_ci NULL  , 
	`operador_funcion_matematica_extensa` VARCHAR(50) COLLATE latin1_swedish_ci NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';



/*****************/


/* Create table in target */
CREATE TABLE `persona_estado_civil`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_persona_estado_civil` VARCHAR(20) COLLATE utf8mb4_general_ci NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `persona_parentezco`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_persona_parentezco` VARCHAR(20) COLLATE utf8mb4_general_ci NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `persona_sexo`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_persona_sexo` VARCHAR(10) COLLATE utf8mb4_general_ci NOT NULL  , 
	`codigo` VARCHAR(1) COLLATE utf8mb4_general_ci NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Alter table in target */
ALTER TABLE `reporte_tablero_usuario` 
	ADD KEY `id_reporte_tablero`(`id_reporte_tablero`) , 
	ADD KEY `id_usuario`(`id_usuario`) ;
ALTER TABLE `reporte_tablero_usuario`
	ADD CONSTRAINT `reporte_tablero_usuario_ibfk_1` 
	FOREIGN KEY (`id_reporte_tablero`) REFERENCES `reporte_tablero` (`id`) , 
	ADD CONSTRAINT `reporte_tablero_usuario_ibfk_2` 
	FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ;


/* Create table in target */
CREATE TABLE `request_log`(
	`controller` VARCHAR(50) COLLATE latin1_swedish_ci NULL  , 
	`id_usuario` INT(11) NULL  , 
	`action` VARCHAR(50) COLLATE latin1_swedish_ci NULL  , 
	`date` DATETIME NULL  
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_actividad`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`codigo` INT(11) NULL  , 
	`d_rrhh_actividad` VARCHAR(150) COLLATE utf8mb4_general_ci NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Alter table in target */
ALTER TABLE `rrhh_categoria` 
	ADD COLUMN `id_rrhh_tipo_liquidacion` INT(11)   NULL AFTER `importe` , 
	ADD COLUMN `codigo` VARCHAR(20)  COLLATE latin1_swedish_ci NULL AFTER `id_rrhh_tipo_liquidacion` , 
	ADD KEY `id_rrhh_convenio`(`id_rrhh_convenio`) , 
	ADD KEY `id_rrhh_tipo_liquidacion`(`id_rrhh_tipo_liquidacion`) ;



/* Create table in target */
CREATE TABLE `rrhh_clase_legajo`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_tipo_liquidacion` VARCHAR(50) COLLATE latin1_swedish_ci NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_clase_liquidacion`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_clase_liquidacion` VARCHAR(50) COLLATE utf8mb4_general_ci NOT NULL  , 
	`codigo` VARCHAR(4) COLLATE utf8mb4_general_ci NOT NULL  , 
	`activo` TINYINT(4) NOT NULL  , 
	`fecha_modificacion` DATETIME NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Alter table in target */
ALTER TABLE `rrhh_concepto` 
	ADD COLUMN `formula_codigo` VARCHAR(3000)  COLLATE latin1_swedish_ci NULL AFTER `id_rrhh_tipo_concepto` , 
	ADD COLUMN `formula` VARCHAR(3000)  COLLATE latin1_swedish_ci NULL AFTER `formula_codigo` , 
	ADD COLUMN `chk_asumir_cero_si_resultado_negativo` TINYINT(4)   NULL AFTER `formula` , 
	ADD COLUMN `orden_liquidacion` INT(11)   NULL AFTER `chk_asumir_cero_si_resultado_negativo` , 
	ADD COLUMN `chk_es_cantidad` TINYINT(4)   NULL AFTER `orden_liquidacion` , 
	ADD COLUMN `chk_es_valor_unitario` TINYINT(4)   NULL AFTER `chk_es_cantidad` , 
	ADD COLUMN `chk_es_importe` TINYINT(4)   NULL AFTER `chk_es_valor_unitario` , 
	ADD COLUMN `valor_generico` DECIMAL(20,2)   NULL AFTER `chk_es_importe` , 
	ADD COLUMN `orden_calculo` INT(11)   NULL AFTER `valor_generico` , 
	ADD COLUMN `tipo_vigencia` INT(4)   NULL AFTER `orden_calculo` , 
	ADD COLUMN `chk_tasa_liquidacion` TINYINT(4)   NULL AFTER `tipo_vigencia` , 
	ADD COLUMN `fecha_modificacion` DATETIME   NULL AFTER `chk_tasa_liquidacion` , 
	ADD COLUMN `chk_periodo` TINYINT(4)   NULL AFTER `fecha_modificacion` , 
	ADD COLUMN `chk_filtro_sexo` TINYINT(4)   NULL AFTER `chk_periodo` , 
	ADD COLUMN `chk_filtro_clase_legajo` TINYINT(4)   NULL AFTER `chk_filtro_sexo` , 
	ADD COLUMN `chk_filtro_categoria` TINYINT(4)   NULL AFTER `chk_filtro_clase_legajo` , 
	ADD COLUMN `chk_filtro_sindicato` TINYINT(4)   NULL AFTER `chk_filtro_categoria` , 
	ADD COLUMN `chk_filtro_estado_civil` TINYINT(4)   NULL AFTER `chk_filtro_sindicato` , 
	ADD COLUMN `chk_filtro_area` TINYINT(4)   NULL AFTER `chk_filtro_estado_civil` , 
	ADD COLUMN `chk_filtro_obra_social` TINYINT(4)   NULL AFTER `chk_filtro_area` , 
	ADD COLUMN `chk_fecha_historico` TINYINT(4)   NULL AFTER `chk_filtro_obra_social` , 
	ADD COLUMN `chk_fecha_ganancia` TINYINT(4)   NULL AFTER `chk_fecha_historico` , 
	ADD COLUMN `observacion` VARCHAR(300)  COLLATE latin1_swedish_ci NULL AFTER `chk_fecha_ganancia` , 
	ADD UNIQUE KEY `codigo`(`codigo`) , 
	ADD KEY `id_cuenta_contable`(`id_cuenta_contable`) ;



/* Create table in target */
CREATE TABLE `rrhh_concepto_area`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_concepto` INT(11) NOT NULL  , 
	`id_area` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `rrhh_concepto_categoria`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_concepto` INT(11) NOT NULL  , 
	`id_rrhh_categoria` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `rrhh_concepto_obra_social`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_concepto` INT(11) NULL  , 
	`id_rrhh_obra_social` INT(11) NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `rrhh_concepto_persona_estado_civil`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_concepto` INT(11) NOT NULL  , 
	`id_persona_estado_civil` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `rrhh_concepto_persona_sexo`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_concepto` INT(11) NOT NULL  , 
	`id_persona_sexo` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `rrhh_concepto_sindicato`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_concepto` INT(11) NOT NULL  , 
	`id_rrh_sindicato` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `rrhh_concepto_tipo_legajo`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_concepto` INT(11) NOT NULL  , 
	`id_rrhh_clase_legajo` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `rrhh_condicion_sicoss`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_situacion_sicoss` VARCHAR(100) COLLATE utf8mb4_general_ci NULL  , 
	`codigo` INT(11) NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Alter table in target */
ALTER TABLE `rrhh_convenio` 
	ADD COLUMN `duracion_mes` TINYINT(4)   NULL AFTER `activo` ;


/* Create table in target */
CREATE TABLE `rrhh_tipo_liquidacion`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`codigo` VARCHAR(4) COLLATE utf8mb4_general_ci NOT NULL  , 
	`d_rrrhh_tipo_liquidacion` VARCHAR(50) COLLATE utf8mb4_general_ci NOT NULL  , 
	`fecha_modificacion` DATETIME NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';

/* Create table in target */
CREATE TABLE `rrhh_liquidacion`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_liquidacion` VARCHAR(50) COLLATE utf8mb4_general_ci NOT NULL  , 
	`fecha_generacion` DATE NULL  , 
	`fecha_periodo` DATE NULL  , 
	`fecha_ganancia` DATE NULL  , 
	`id_tipo_liquidacion` INT(11) NOT NULL  , 
	`id_estado_rrhh_liquidacion` INT(11) NOT NULL  , 
	`chk_habilita_ingreso_novedades` TINYINT(4) NOT NULL  , 
	`id_clase_legajo` INT(11) NOT NULL  , 
	`chk_aplica_todo_empleado` TINYINT(4) NULL  , 
	`id_clase_liquidacion` TINYINT(4) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `id_tipo_liquidacion`(`id_tipo_liquidacion`) , 
	KEY `id_estado_rrhh_liquidacion`(`id_estado_rrhh_liquidacion`) , 
	CONSTRAINT `rrhh_liquidacion_ibfk_1` 
	FOREIGN KEY (`id_tipo_liquidacion`) REFERENCES `rrrhh_tipo_liquidacion` (`id`) , 
	CONSTRAINT `rrhh_liquidacion_ibfk_2` 
	FOREIGN KEY (`id_estado_rrhh_liquidacion`) REFERENCES `estado_rrhh_liquidacion` (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `rrhh_liquidacion_concepto`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_liquidacion` INT(11) NOT NULL  , 
	`id_rrhh_concepto` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `id_rrhh_liquidacion`(`id_rrhh_liquidacion`) , 
	KEY `id_rrhh_concepto`(`id_rrhh_concepto`) , 
	CONSTRAINT `rrhh_liquidacion_concepto_ibfk_1` 
	FOREIGN KEY (`id_rrhh_liquidacion`) REFERENCES `rrhh_liquidacion` (`id`) , 
	CONSTRAINT `rrhh_liquidacion_concepto_ibfk_2` 
	FOREIGN KEY (`id_rrhh_concepto`) REFERENCES `rrhh_concepto` (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `rrhh_liquidacion_persona`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_liquidacion` INT(11) NOT NULL  , 
	`id_persona` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `rrhh_lugar_pago`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_lugar_pago` VARCHAR(20) COLLATE utf8mb4_general_ci NOT NULL  , 
	`id_provincia` INT(11) NULL  , 
	`calle` VARCHAR(20) COLLATE utf8mb4_general_ci NULL  , 
	`numero_calle` INT(11) NULL  , 
	`codigo_postal` VARCHAR(10) COLLATE utf8mb4_general_ci NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Alter table in target */
ALTER TABLE `rrhh_modelo_liquidacion` 
	ADD COLUMN `id_rrhh_convenio` INT(11)   NULL AFTER `d_rrhh_modelo_liquidacion` , 
	ADD KEY `id_rrhh_convenio`(`id_rrhh_convenio`) ;
ALTER TABLE `rrhh_modelo_liquidacion`
	ADD CONSTRAINT `rrhh_modelo_liquidacion_ibfk_1` 
	FOREIGN KEY (`id_rrhh_convenio`) REFERENCES `rrhh_convenio` (`id`) ;


/* Create table in target */
CREATE TABLE `rrhh_novedad_persona`(
	`id` BIGINT(20) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_liquidacion` INT(11) NOT NULL  , 
	`id_persona` INT(11) NOT NULL  , 
	`id_rrhh_concepto` INT(11) NOT NULL  , 
	`cantidad` DECIMAL(20,2) NULL  , 
	`valor_unitario` DECIMAL(20,2) NULL  , 
	`importe` DECIMAL(20,2) NULL  , 
	`importe_calculado` DECIMAL(20,2) NOT NULL  , 
	`formula` VARCHAR(1500) COLLATE utf8mb4_general_ci NULL  , 
	`periodo` DATE NULL  , 
	`indice` INT(11) NULL  , 
	`fecha_modificacion` DATE NULL  , 
	`chk_usa_fecha_ganancia` TINYINT(4) NULL  , 
	`fecha_ganancia` DATE NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `id_rrhh_liquidacion`(`id_rrhh_liquidacion`) , 
	KEY `id_persona`(`id_persona`) , 
	KEY `id_rrhh_concepto`(`id_rrhh_concepto`) , 
	CONSTRAINT `rrhh_novedad_persona_ibfk_1` 
	FOREIGN KEY (`id_rrhh_liquidacion`) REFERENCES `rrhh_liquidacion` (`id`) , 
	CONSTRAINT `rrhh_novedad_persona_ibfk_2` 
	FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id`) , 
	CONSTRAINT `rrhh_novedad_persona_ibfk_3` 
	FOREIGN KEY (`id_rrhh_concepto`) REFERENCES `rrhh_concepto` (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `rrhh_obra_social`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`codigo` VARCHAR(10) COLLATE utf8mb4_general_ci NULL  , 
	`d_rrhh_obra_social` VARCHAR(50) COLLATE utf8mb4_general_ci NULL  , 
	`activa` TINYINT(4) NULL  , 
	`fecha_modificacion` DATETIME NULL  , 
	`codigo_extra` INT(11) NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Alter table in target */
ALTER TABLE `rrhh_persona_concepto` 
	ADD COLUMN `orden` INT(11)   NOT NULL AFTER `id_rrhh_concepto` , 
	ADD COLUMN `cantidad` DECIMAL(20,2)   NULL AFTER `orden` , 
	ADD COLUMN `valor_unitario` DECIMAL(20,2)   NULL AFTER `cantidad` , 
	ADD COLUMN `importe` DECIMAL(20,2)   NULL AFTER `valor_unitario` , 
	ADD COLUMN `id_rrhh_convenio` INT(11)   NULL AFTER `importe` , 
	DROP COLUMN `valor` , 
	ADD KEY `id_persona`(`id_persona`) , 
	ADD KEY `id_rrhh_concepto`(`id_rrhh_concepto`) , 
	ADD KEY `id_rrhh_convenio`(`id_rrhh_convenio`) ;
ALTER TABLE `rrhh_persona_concepto`
	ADD CONSTRAINT `rrhh_persona_concepto_ibfk_1` 
	FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id`) , 
	ADD CONSTRAINT `rrhh_persona_concepto_ibfk_2` 
	FOREIGN KEY (`id_rrhh_concepto`) REFERENCES `rrhh_concepto` (`id`) , 
	ADD CONSTRAINT `rrhh_persona_concepto_ibfk_3` 
	FOREIGN KEY (`id_rrhh_convenio`) REFERENCES `rrhh_convenio` (`id`) ;


/* Create table in target */
CREATE TABLE `rrhh_sindicato`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_sindicato` VARCHAR(20) COLLATE utf8mb4_general_ci NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `rrhh_situacion_sicoss`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`codigo` INT(11) NOT NULL  , 
	`d_rrhh_situacion_sicoss` VARCHAR(100) COLLATE utf8mb4_general_ci NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Alter table in target */
ALTER TABLE `rrhh_tipo_concepto` 
	ADD COLUMN `signo_contable` INT(11)   NULL AFTER `d_rrhh_tipo_concepto` , 
	ADD COLUMN `codigo` INT(11)   NULL AFTER `signo_contable` ;

/* Create table in target */
CREATE TABLE `rrhh_tipo_liquidacion_liquidacion`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_liquidacion` INT(11) NOT NULL  , 
	`id_rrhh_tipo_liquidacion` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `id_rrhh_liquidacion`(`id_rrhh_liquidacion`) , 
	KEY `id_rrhh_tipo_liquidacion`(`id_rrhh_tipo_liquidacion`) , 
	CONSTRAINT `rrhh_tipo_liquidacion_liquidacion_ibfk_1` 
	FOREIGN KEY (`id_rrhh_liquidacion`) REFERENCES `rrhh_liquidacion` (`id`) , 
	CONSTRAINT `rrhh_tipo_liquidacion_liquidacion_ibfk_2` 
	FOREIGN KEY (`id_rrhh_tipo_liquidacion`) REFERENCES `rrhh_clase_legajo` (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `rrhh_variable`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`codigo` VARCHAR(100) COLLATE latin1_swedish_ci NOT NULL  , 
	`calculo_sql` LONGTEXT COLLATE latin1_swedish_ci NULL  , 
	`codigo_no_visible` VARCHAR(10) COLLATE latin1_swedish_ci NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';





/* Create table in target */
CREATE TABLE `tabla_auxiliar`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`codigo` VARCHAR(10) COLLATE utf8mb4_general_ci NULL  , 
	`d_tabla_auxiliar` VARCHAR(50) COLLATE utf8mb4_general_ci NOT NULL  , 
	`id_modulo` INT(11) NULL  , 
	`tabla_padre` VARCHAR(50) COLLATE utf8mb4_general_ci NULL  , 
	`tabla_vinculada` VARCHAR(50) COLLATE utf8mb4_general_ci NOT NULL  , 
	`tabla_vinculada_campo_relacional` VARCHAR(20) COLLATE utf8mb4_general_ci NOT NULL  , 
	`header1` VARCHAR(20) COLLATE utf8mb4_general_ci NULL  , 
	`columna_tabla_vinculada1` INT(11) NULL  , 
	`header2` VARCHAR(20) COLLATE utf8mb4_general_ci NULL  , 
	`columna_tabla_vinculada2` INT(11) NULL  , 
	`header3` VARCHAR(20) COLLATE utf8mb4_general_ci NULL  , 
	`columna_tabla_vinculada3` INT(11) NULL  , 
	`header4` VARCHAR(20) COLLATE utf8mb4_general_ci NULL  , 
	`columna_tabla_vinculada4` INT(11) NULL  , 
	`header5` VARCHAR(20) COLLATE utf8mb4_general_ci NULL  , 
	`columna_tabla_vinculada5` INT(11) NULL  , 
	`header6` VARCHAR(20) COLLATE utf8mb4_general_ci NULL  , 
	`columna_tabla_vinculada6` INT(11) NULL  , 
	`header7` VARCHAR(20) COLLATE utf8mb4_general_ci NULL  , 
	`columna_tabla_vinculada7` INT(11) NULL  , 
	`fecha_modificacion` DATETIME NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `id_modulo`(`id_modulo`) , 
	CONSTRAINT `tabla_auxiliar_ibfk_1` 
	FOREIGN KEY (`id_modulo`) REFERENCES `modulo` (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Create table in target */
CREATE TABLE `tabla_auxiliar_item`(
	`id_tabla_auxiliar` INT(11) NOT NULL  , 
	`id_registro_tabla_auxiliar` INT(11) NULL  , 
	`valor1` DECIMAL(20,2) NULL  DEFAULT 0.00 , 
	`valor2` DECIMAL(20,2) NULL  DEFAULT 0.00 , 
	`valor3` DECIMAL(20,2) NULL  DEFAULT 0.00 , 
	`valor4` DECIMAL(20,2) NULL  DEFAULT 0.00 , 
	`valor5` DECIMAL(20,2) NULL  DEFAULT 0.00 , 
	`valor6` DECIMAL(20,2) NULL  DEFAULT 0.00 , 
	`valor7` DECIMAL(20,2) NULL  DEFAULT 0.00 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';

/************************/


/* Alter table in target */
ALTER TABLE `persona` 
	CHANGE `dpto` `dpto` VARCHAR(3)  COLLATE latin1_swedish_ci NOT NULL AFTER `piso` , 
	ADD COLUMN `id_rrhh_condicion_sicoss` INT(11)   NULL AFTER `id_rrhh_convenio` , 
	ADD COLUMN `id_rrhh_situacion_sicoss` INT(11)   NULL AFTER `id_rrhh_condicion_sicoss` , 
	ADD COLUMN `id_rrhh_actividad` INT(11)   NULL AFTER `id_rrhh_situacion_sicoss` , 
	ADD COLUMN `rrhh_basico` DECIMAL(20,2)   NULL AFTER `id_rrhh_actividad` , 
	ADD COLUMN `id_rrhh_obra_social` INT(11)   NULL AFTER `rrhh_basico` , 
	ADD COLUMN `id_rrhh_lugar_pago` INT(11)   NULL AFTER `id_rrhh_obra_social` , 
	ADD COLUMN `id_persona_parentezco` INT(11)   NULL AFTER `id_rrhh_lugar_pago` , 
	ADD COLUMN `id_persona_estado_civil` INT(11)   NULL AFTER `id_persona_parentezco` , 
	ADD COLUMN `id_rrhh_clase_legajo` INT(11)   NULL AFTER `id_persona_estado_civil` ,
	ADD KEY `id_rrhh_clase_legajo`(`id_rrhh_clase_legajo`) , 
	ADD KEY `id_rrhh_lugar_pago`(`id_rrhh_lugar_pago`) , 
	DROP KEY `PRIMARY`, ADD PRIMARY KEY(`id`,`dpto`) ;
	
	
	/*
	OJO paso esta
	ALTER TABLE `persona` 

	ADD COLUMN `id_rrhh_condicion_sicoss` INT(11)   NULL AFTER `id_rrhh_convenio` , 
	ADD COLUMN `id_rrhh_situacion_sicoss` INT(11)   NULL AFTER `id_rrhh_condicion_sicoss` , 
	ADD COLUMN `id_rrhh_actividad` INT(11)   NULL AFTER `id_rrhh_situacion_sicoss` , 
	ADD COLUMN `rrhh_basico` DECIMAL(20,2)   NULL AFTER `id_rrhh_actividad` , 
	ADD COLUMN `id_rrhh_obra_social` INT(11)   NULL AFTER `rrhh_basico` , 
	ADD COLUMN `id_rrhh_lugar_pago` INT(11)   NULL AFTER `id_rrhh_obra_social` , 
	ADD COLUMN `id_persona_parentezco` INT(11)   NULL AFTER `id_rrhh_lugar_pago` , 
	ADD COLUMN `id_persona_estado_civil` INT(11)   NULL AFTER `id_persona_parentezco` , 
	ADD COLUMN `id_rrhh_clase_legajo` INT(11)   NULL AFTER `id_persona_estado_civil` 
	 ;
	
	*/
	
	
	
	
	
	
	ALTER TABLE `persona`
	ADD CONSTRAINT `persona_ibfk_18` 
	FOREIGN KEY (`id_condicion_pago`) REFERENCES `condicion_pago` (`id`) , 
	ADD CONSTRAINT `persona_ibfk_19` 
	FOREIGN KEY (`id_persona_origen`) REFERENCES `persona_origen` (`id`) , 
	ADD CONSTRAINT `persona_ibfk_20` 
	FOREIGN KEY (`id_rrhh_lugar_pago`) REFERENCES `rrhh_lugar_pago` (`id`) , 
	ADD CONSTRAINT `persona_ibfk_21` 
	FOREIGN KEY (`id_rrhh_clase_legajo`) REFERENCES `rrhh_clase_legajo` (`id`) , 
	ADD CONSTRAINT `persona_ibfk_22` 
	FOREIGN KEY (`id_rrhh_clase_legajo`) REFERENCES `rrhh_clase_legajo` (`id`) ;
ALTER TABLE `rrhh_categoria`
	ADD CONSTRAINT `rrhh_categoria_ibfk_1` 
	FOREIGN KEY (`id_rrhh_tipo_liquidacion`) REFERENCES `rrhh_clase_legajo` (`id`) , 
	ADD CONSTRAINT `rrhh_categoria_ibfk_2` 
	FOREIGN KEY (`id_rrhh_convenio`) REFERENCES `rrhh_convenio` (`id`) ;	
	
	ALTER TABLE `rrhh_concepto`
	ADD CONSTRAINT `rrhh_concepto_ibfk_1` 
	FOREIGN KEY (`id_cuenta_contable`) REFERENCES `cuenta_contable` (`id`) ;
	
	
	
	
	
	
	
	
	
	
/*  Alter View in target  */
DELIMITER $$
ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `comprobante_item_view` AS (SELECT `comprobante_item`.`id` AS `id`,`comprobante_item`.`id_comprobante` AS `id_comprobante`,`comprobante_item`.`cantidad` AS `cantidad`,`comprobante_item`.`descuento_unitario` AS `descuento_unitario`,`comprobante_item`.`precio_unitario_bruto` AS `precio_unitario_bruto`,`comprobante_item`.`precio_unitario` AS `precio_unitario`,`comprobante_item`.`orden` AS `orden`,`comprobante_item`.`n_item` AS `n_item`,`comprobante_item`.`id_iva` AS `id_iva`,`comprobante_item`.`id_producto` AS `id_producto`,`comprobante_item`.`codigo_observacion` AS `codigo_observacion`,`comprobante_item`.`item_observacion` AS `item_observacion`,`comprobante_item`.`dias` AS `dias`,`comprobante_item`.`id_comprobante_item_origen` AS `id_comprobante_item_origen`,`comprobante_item`.`activo` AS `activo`,`comprobante_item`.`cantidad_cierre` AS `cantidad_cierre`,`comprobante_item`.`id_unidad` AS `id_unidad`,`comprobante_item`.`id_comprobante_origen` AS `id_comprobante_origen`,`comprobante_item`.`id_migracion` AS `id_migracion`,`comprobante_item`.`importe_descuento_unitario` AS `importe_descuento_unitario`,`comprobante_item`.`precio_unitario2` AS `precio_unitario2`,`comprobante_item`.`precio_unitario3` AS `precio_unitario3`,`comprobante_item`.`id_lista_precio` AS `id_lista_precio`,`comprobante_item`.`precio_minimo_producto` AS `precio_minimo_producto`,`comprobante_item`.`id_tipo_lote` AS `id_tipo_lote`,`comprobante_item`.`requiere_conformidad` AS `requiere_conformidad`,`tipo_comprobante`.`cantidad_ceros_decimal` AS `cantidad_ceros_decimal`,`comprobante_item`.`ajusta_diferencia_cambio` AS `ajusta_diferencia_cambio`,`comprobante_item`.`monto_diferencia_cambio` AS `monto_diferencia_cambio`,`comprobante_item`.`producto_codigo` AS `producto_codigo`,CAST((`comprobante`.`fecha_generacion` + INTERVAL IF(ISNULL(`comprobante_item`.`dias`),0,`comprobante_item`.`dias`) DAY) AS DATE) AS `fecha_entrega`,`comprobante_item`.`id_persona` AS `id_persona`,`comprobante_item`.`id_detalle_tipo_comprobante` AS `id_detalle_tipo_comprobante`,`comprobante_item`.`fecha_generacion` AS `fecha_generacion`,`comprobante_item`.`fecha_cierre` AS `fecha_cierre`,`comprobante_item`.`fecha_vencimiento` AS `fecha_vencimiento`,`comprobante_item`.`cantidad2` AS `cantidad2`,`comprobante_item`.`cantidad3` AS `cantidad3`,(`comprobante_item`.`precio_unitario` * `comprobante_item`.`cantidad`) AS `total`,(((`comprobante_item`.`precio_unitario` * `comprobante_item`.`cantidad`) * (1 + (`iva`.`d_iva` / 100))) - (`comprobante_item`.`precio_unitario` * `comprobante_item`.`cantidad`)) AS `total_iva`,`comprobante`.`lugar_entrega` AS `lugar_entrega`,`comprobante`.`entrega_domicilio` AS `entrega_domicilio`,`comprobante`.`id_transportista` AS `id_transportista` FROM (((`comprobante_item` JOIN `comprobante` ON((`comprobante`.`id` = `comprobante_item`.`id_comprobante`))) JOIN `tipo_comprobante` ON((`comprobante`.`id_tipo_comprobante` = `tipo_comprobante`.`id`))) LEFT JOIN `iva` ON((`comprobante_item`.`id_iva` = `iva`.`id`))))$$
DELIMITER ;


/*  Alter View in target  */
DELIMITER $$
ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lista_precio_producto_view` AS (SELECT IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),`lpp_padre`.`id`,`lista_precio_producto`.`id`) AS `id`,`producto`.`id_producto_tipo` AS `id_producto_tipo`,`producto`.`id_producto_clasificacion` AS `id_producto_clasificacion`,`producto`.`id_origen` AS `id_origen`,`lista_precio`.`id` AS `id_lista_precio`,IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),`lpp_padre`.`id_producto`,`lista_precio_producto`.`id_producto`) AS `id_producto`,IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),`lpp_padre`.`id_moneda`,`lista_precio`.`id_moneda`) AS `id_moneda`,IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),(`lpp_padre`.`precio` * (1 + (`lista_precio`.`porcentaje_variacion` / 100))),`lista_precio_producto`.`precio`) AS `precio`,`lista_precio`.`id_tipo_lista_precio` AS `id_tipo_lista_precio`,IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),`lpp_padre`.`precio_minimo`,`lista_precio_producto`.`precio_minimo`) AS `precio_minimo`,IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),`lpp_padre`.`porcentaje_utilidad_minima`,`lista_precio_producto`.`porcentaje_utilidad_minima`) AS `porcentaje_utilidad_minima`,IF((`lista_precio`.`id_tipo_lista_precio` = 1),0,IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),(`producto`.`costo` * ((100 + `lpp_padre`.`porcentaje_utilidad_minima`) / 100)),(`producto`.`costo` * ((100 + `lista_precio_producto`.`porcentaje_utilidad_minima`) / 100)))) AS `precio_minimo_por_utilidad`,`producto`.`codigo` AS `codigo`,`producto`.`costo` AS `costo`,`producto`.`d_producto` AS `d_producto`,`iva`.`d_iva` AS `d_iva`,`moneda`.`simbolo` AS `moneda_simbolo`,`producto_tipo`.`id_destino_producto` AS `id_destino_producto`,ROUND((IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),(`lpp_padre`.`precio` * (1 + (`lista_precio`.`porcentaje_variacion` / 100))),`lista_precio_producto`.`precio`) * (1 + (`iva`.`d_iva` / 100))),4) AS `precio_final`,IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),`lpp_padre`.`precio`,0) AS `precio_base`,`lista_precio`.`d_lista_precio` AS `d_lista_precio` FROM ((((((`lista_precio` LEFT JOIN `lista_precio_producto` ON((`lista_precio`.`id` = `lista_precio_producto`.`id_lista_precio`))) LEFT JOIN `lista_precio_producto` `lpp_padre` ON((`lista_precio`.`id_lista_precio_padre` = `lpp_padre`.`id_lista_precio`))) JOIN `producto` ON((`producto`.`id` = IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),`lpp_padre`.`id_producto`,`lista_precio_producto`.`id_producto`)))) JOIN `iva` ON((`iva`.`id` = `producto`.`id_iva`))) JOIN `moneda` ON((`moneda`.`id` = `lista_precio`.`id_moneda`))) JOIN `producto_tipo` ON((`producto_tipo`.`id` = `producto`.`id_producto_tipo`))) WHERE (`producto`.`activo` = 1))$$
DELIMITER ;


/*  Alter View in target  */
DELIMITER $$
ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `modulo_funcion_entidad_view` AS SELECT NULL AS `parent_id`,`modulo`.`id` AS `id`,NULL AS `lft`,NULL AS `rght`,`modulo`.`d_modulo` AS `d_arbol_permisos`,0 AS `es_permiso` FROM `modulo` UNION SELECT `tipo_funcion`.`id_modulo` AS `parent_id`,(`tipo_funcion`.`id` * 100000) AS `tipo_funcion.id*100000`,NULL AS `lft`,NULL AS `rght`,REPLACE(`entidad`.`controller`,'Controller','') AS `d_arbol_permisos`,0 AS `es_permiso` FROM (`tipo_funcion` JOIN `entidad` ON((`entidad`.`id` = `tipo_funcion`.`id_entidad`))) UNION SELECT (`funcion`.`id_tipo_funcion` * 100000) AS `parent_id`,`funcion`.`id` AS `id`,NULL AS `lft`,NULL AS `rght`,`funcion`.`c_funcion` AS `d_arbol_permisos`,1 AS `es_permiso` FROM `funcion`$$
DELIMITER ;


/*  Alter View in target  */
DELIMITER $$
ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pedidos_con_orden_ensamble_view` AS (SELECT `pedido_interno`.`nro_comprobante` AS `nro_comprobante`,DATE_FORMAT(`pedido_interno`.`fecha_generacion`,'%d-%m-%Y') AS `fecha_generacion_pedido`,DATE_FORMAT(`pedido_interno`.`fecha_entrega`,'%d-%m-%Y') AS `fecha_entrega_global_pedido`,DATE_FORMAT(CAST((`pedido_interno`.`fecha_generacion` + INTERVAL IF(ISNULL(`pedido_interno_item`.`dias`),0,`pedido_interno_item`.`dias`) DAY) AS DATE),'%d-%m-%Y') AS `fecha_entrega_item`,`pedido_interno_item`.`dias` AS `dias`,`pedido_interno`.`plazo_entrega_dias` AS `dias_pi`,`pedido_interno_item`.`cantidad` AS `cantidad`,`producto`.`codigo` AS `codigo`,`producto`.`d_producto` AS `d_producto`,`usuario`.`username` AS `username`,`producto`.`id` AS `id_producto`,`pedido_interno`.`id_persona` AS `id_persona`,`pedido_interno_item`.`item_observacion` AS `observacion_pedido_interno`,IF((`pedido_interno`.`inspeccion` = 1),'SI','NO') AS `inspeccion_pedido_interno`,IF((`pedido_interno`.`certificados` = 1),'SI','NO') AS `certificados_pedido_interno`,`estado_comprobante`.`d_estado_comprobante` AS `estado_pedido`,`orden_armado`.`nro_comprobante` AS `nro_orden_armado`,(SELECT `auditoria_comprobante`.`fecha` FROM `auditoria_comprobante` WHERE ((`auditoria_comprobante`.`id_comprobante` = `orden_armado`.`id`) AND (`auditoria_comprobante`.`id_estado_comprobante_nuevo` = 5)) ORDER BY `auditoria_comprobante`.`id` LIMIT 1) AS `fecha_completo`,(SELECT `auditoria_comprobante`.`fecha` FROM `auditoria_comprobante` WHERE ((`auditoria_comprobante`.`id_comprobante` = `orden_armado`.`id`) AND (`auditoria_comprobante`.`id_estado_comprobante_nuevo` = 9)) ORDER BY `auditoria_comprobante`.`id` LIMIT 1) AS `fecha_cumplido`,DATE_FORMAT(`orden_armado`.`fecha_generacion`,'%d-%m-%Y') AS `fecha_generacion_oa`,`estado_orden_armado`.`d_estado_comprobante` AS `estado_orden_armado`,`persona`.`razon_social` AS `razon_social`,`persona`.`codigo` AS `persona_codigo`,DATE_FORMAT(`orden_armado`.`fecha_entrega`,'%d-%m-%Y') AS `fecha_entrega_orden_armado`,(CASE WHEN (`orden_armado`.`id` > 0) THEN `orden_armado_item`.`cantidad` ELSE 0 END) AS `cantidad_orden_armado`,`orden_armado`.`observacion` AS `observacion_oa`,`pedido_interno_item`.`n_item` AS `nro_item`,`orden_armado`.`id` AS `id`,`orden_armado`.`direccion` AS `ubicacion_oa` FROM ((((((((`comprobante_item` `orden_armado_item` JOIN `comprobante` `orden_armado` ON((`orden_armado`.`id` = `orden_armado_item`.`id_comprobante`))) LEFT JOIN `comprobante_item` `pedido_interno_item` ON((`pedido_interno_item`.`id` = `orden_armado_item`.`id_comprobante_item_origen`))) LEFT JOIN `comprobante` `pedido_interno` ON((`pedido_interno`.`id` = `pedido_interno_item`.`id_comprobante`))) LEFT JOIN `estado_comprobante` `estado_orden_armado` ON((`estado_orden_armado`.`id` = `orden_armado`.`id_estado_comprobante`))) LEFT JOIN `producto` ON((`producto`.`id` = `orden_armado_item`.`id_producto`))) LEFT JOIN `usuario` ON((`usuario`.`id` = `pedido_interno`.`id_usuario`))) LEFT JOIN `estado_comprobante` ON((`estado_comprobante`.`id` = `pedido_interno`.`id_estado_comprobante`))) LEFT JOIN `persona` ON((`persona`.`id` = `pedido_interno`.`id_persona`))) WHERE ((`orden_armado`.`id_tipo_comprobante` = 104) AND (`orden_armado`.`id_estado_comprobante` <> 2) AND (`orden_armado`.`id_estado_comprobante` NOT IN (116,2))) LIMIT 100000000000)$$
DELIMITER ;


/*  Alter View in target  */
DELIMITER $$
ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `persona_view` AS SELECT `persona`.`id` AS `id`,`persona`.`razon_social` AS `razon_social`,`persona`.`email` AS `email`,`persona`.`id_pais` AS `id_pais`,`persona`.`id_provincia` AS `id_provincia`,`persona`.`localidad` AS `localidad`,`persona`.`ciudad` AS `ciudad`,`persona`.`calle` AS `calle`,`persona`.`numero_calle` AS `numero_calle`,`persona`.`piso` AS `piso`,`persona`.`dpto` AS `dpto`,`persona`.`tel` AS `tel`,`persona`.`interno1` AS `interno1`,`persona`.`fax` AS `fax`,`persona`.`posicion` AS `posicion`,`persona`.`cuit` AS `cuit`,`persona`.`ib` AS `ib`,`persona`.`id_cuenta` AS `id_cuenta`,`persona`.`cod_postal` AS `cod_postal`,`persona`.`id_tipo_iva` AS `id_tipo_iva`,`persona`.`id_persona_figura` AS `id_persona_figura`,`persona`.`activo` AS `activo`,`persona`.`website` AS `website`,`persona`.`id_tipo_documento` AS `id_tipo_documento`,`persona`.`observaciones` AS `observaciones`,`persona`.`cuenta_corriente` AS `cuenta_corriente`,`persona`.`sis_sistema` AS `sis_sistema`,`persona`.`sis_acceso` AS `sis_acceso`,`persona`.`sis_usuario_web` AS `sis_usuario_web`,`persona`.`sis_password` AS `sis_password`,`persona`.`observacion_extra` AS `observacion_extra`,`persona`.`id_persona_categoria` AS `id_persona_categoria`,`persona`.`id_cuenta_contable` AS `id_cuenta_contable`,`persona`.`id_moneda` AS `id_moneda`,`persona`.`requiere_orden_compra` AS `requiere_orden_compra`,`persona`.`id_cuenta_contable_cuenta_corriente` AS `id_cuenta_contable_cuenta_corriente`,`persona`.`monto_autorizacion_orden_compra` AS `monto_autorizacion_orden_compra`,`persona`.`id_transportista` AS `id_transportista`,`persona`.`flete_paga_proveedor` AS `flete_paga_proveedor`,`persona`.`entrega_en_domicilio` AS `entrega_en_domicilio`,`persona`.`nombre_fantasia` AS `nombre_fantasia`,`persona`.`descuento` AS `descuento`,`persona`.`id_forma_pago` AS `id_forma_pago`,`persona`.`id_condicion_pago` AS `id_condicion_pago`,`persona`.`id_media_file` AS `id_media_file`,`persona`.`valoracion` AS `valoracion`,`persona`.`fecha_creacion` AS `fecha_creacion`,`persona`.`id_iva` AS `id_iva`,`persona`.`fecha_nacimiento` AS `fecha_nacimiento`,`persona`.`pasaporte` AS `pasaporte`,`persona`.`pasaporte_fecha_vto` AS `pasaporte_fecha_vto`,`persona`.`persona_extranjero` AS `persona_extranjero`,`persona`.`id_tipo_actividad` AS `id_tipo_actividad`,`persona`.`limite_credito` AS `limite_credito`,`persona`.`limiite_credito_documento` AS `limiite_credito_documento`,`persona`.`cuenta_corriente_deshabilitada` AS `cuenta_corriente_deshabilitada`,`persona`.`dias_limite_cuenta_corriente` AS `dias_limite_cuenta_corriente`,`persona`.`id_tipo_persona` AS `id_tipo_persona`,`persona`.`pasaporte_fecha_creacion` AS `pasaporte_fecha_creacion`,`persona`.`observacion` AS `observacion`,`persona`.`id_migracion` AS `id_migracion`,`persona`.`fecha_borrado` AS `fecha_borrado`,`persona`.`email2` AS `email2`,`persona`.`email3` AS `email3`,`persona`.`email4` AS `email4`,`persona`.`tel2` AS `tel2`,`persona`.`interno2` AS `interno2`,`persona`.`horario_contacto` AS `horario_contacto`,`persona`.`pasaporte_nacionalidad` AS `pasaporte_nacionalidad`,`persona`.`nombre` AS `nombre`,`persona`.`apellido` AS `apellido`,`persona`.`id_area` AS `id_area`,`persona`.`fecha_ingreso` AS `fecha_ingreso`,`persona`.`fecha_egreso` AS `fecha_egreso`,`persona`.`sexo` AS `sexo`,`persona`.`celular` AS `celular`,`persona`.`interno` AS `interno`,`persona`.`direccion_latitud` AS `direccion_latitud`,`persona`.`direccion_longitud` AS `direccion_longitud`,`persona`.`id_tipo_contrato` AS `id_tipo_contrato`,`persona`.`id_persona_padre` AS `id_persona_padre`,`persona`.`d_persona` AS `d_persona`,`persona`.`id_estado_persona` AS `id_estado_persona`,`persona`.`id_situacion_ib` AS `id_situacion_ib`,`persona`.`codigo` AS `codigo`,`persona`.`id_otro_sistema` AS `id_otro_sistema`,`persona`.`nro_pasaporte` AS `nro_pasaporte`,`persona`.`id_tipo_comercializacion_por_defecto` AS `id_tipo_comercializacion_por_defecto`,`pais`.`d_pais` AS `d_pais`,`provincia`.`d_provincia` AS `d_provincia`,`tipo_iva`.`d_tipo_iva` AS `d_tipo_iva`,`tipo_documento`.`d_tipo_documento` AS `d_tipo_documento`,`estado_persona`.`d_estado_persona` AS `d_estado_persona`,`cuenta_contable`.`d_cuenta_contable` AS `d_cuenta_contable`,`cc2`.`d_cuenta_contable` AS `d_cuenta_contable_cuenta_corriente`,`persona_categoria`.`d_persona_categoria` AS `d_persona_categoria`,IF(ISNULL(`persona`.`id_lista_precio`),(SELECT `lista_precio`.`id` FROM `lista_precio` WHERE (`lista_precio`.`por_defecto` = 1) LIMIT 1),`persona`.`id_lista_precio`) AS `id_lista_precio`,`persona`.`meli_nickname` AS `meli_nickname`,`persona`.`meli_id_usuario` AS `meli_id_usuario`,`persona`.`lugar_entrega` AS `lugar_entrega`,`usuario`.`id` AS `id_usuario`,`usuario`.`username` AS `sivit_usuario`,IFNULL(`usuario`.`activo`,1) AS `sivit_usuario_activo`,`persona`.`id_persona_origen` AS `id_persona_origen` FROM (((((((((`persona` LEFT JOIN `pais` ON((`pais`.`id` = `persona`.`id_pais`))) LEFT JOIN `provincia` ON((`provincia`.`id` = `persona`.`id_provincia`))) LEFT JOIN `tipo_iva` ON((`tipo_iva`.`id` = `persona`.`id_tipo_iva`))) LEFT JOIN `tipo_documento` ON((`tipo_documento`.`id` = `persona`.`id_tipo_documento`))) LEFT JOIN `estado_persona` ON((`estado_persona`.`id` = `persona`.`id_estado_persona`))) LEFT JOIN `cuenta_contable` ON((`cuenta_contable`.`id` = `persona`.`id_cuenta_contable`))) LEFT JOIN `cuenta_contable` `cc2` ON((`cc2`.`id` = `persona`.`id_cuenta_contable_cuenta_corriente`))) LEFT JOIN `persona_categoria` ON((`persona_categoria`.`id` = `persona`.`id_persona_categoria`))) LEFT JOIN `usuario` ON((`usuario`.`id_persona` = `persona`.`id`)))$$
DELIMITER ;	
	
	
	
	ALTER TABLE `persona` 
  ADD FOREIGN KEY (`id_rrhh_convenio`) REFERENCES `rrhh_convenio` (`id`),
  ADD FOREIGN KEY (`id_rrhh_condicion_sicoss`) REFERENCES `rrhh_condicion_sicoss` (`id`),
  ADD FOREIGN KEY (`id_rrhh_situacion_sicoss`) REFERENCES `rrhh_situacion_sicoss` (`id`),
  ADD FOREIGN KEY (`id_rrhh_actividad`) REFERENCES `rrhh_actividad` (`id`),
  ADD FOREIGN KEY (`id_rrhh_obra_social`) REFERENCES `rrhh_obra_social` (`id`),
  ADD FOREIGN KEY (`id_rrhh_lugar_pago`) REFERENCES `rrhh_lugar_pago` (`id`),
  ADD FOREIGN KEY (`id_persona_parentezco`) REFERENCES `persona_parentezco` (`id`),
  ADD FOREIGN KEY (`id_persona_estado_civil`) REFERENCES `persona_estado_civil` (`id`),
  ADD FOREIGN KEY (`id_rrhh_clase_legajo`) REFERENCES `rrhh_clase_legajo` (`id`) ;
	
	
	

/* Alter table in target */
ALTER TABLE `persona` 
	CHANGE `celular` `celular` VARCHAR(20)  COLLATE latin1_swedish_ci NULL AFTER `fecha_egreso` , 
	ADD COLUMN `id_persona_sexo` INT(11)   NULL AFTER `id_rrhh_clase_legajo` , 
	DROP COLUMN `sexo` , 
	ADD KEY `id_persona_sexo`(`id_persona_sexo`) ;
ALTER TABLE `persona`
	ADD CONSTRAINT `persona_ibfk_32` 
	FOREIGN KEY (`id_persona_sexo`) REFERENCES `persona_sexo` (`id`) ;


/* Alter table in target */
ALTER TABLE `reporte_tablero_usuario` 
	DROP KEY `id_reporte_tablero` , 
	ADD UNIQUE KEY `reporte_tablero_usuario_unique`(`id_reporte_tablero`,`id_usuario`) ;

/* Alter table in target */
ALTER TABLE `rrhh_categoria` 
	CHANGE `importe` `importe` DECIMAL(20,2)   NULL AFTER `id_rrhh_convenio` , 
	ADD COLUMN `activo` TINYINT(4)   NULL DEFAULT 1 AFTER `codigo` , 
	DROP COLUMN `abreviado` ;

/* Alter table in target */
ALTER TABLE `rrhh_concepto_categoria` 
	ADD KEY `id_rrhh_categoria`(`id_rrhh_categoria`) , 
	ADD KEY `id_rrhh_concepto`(`id_rrhh_concepto`) ;
ALTER TABLE `rrhh_concepto_categoria`
	ADD CONSTRAINT `rrhh_concepto_categoria_ibfk_1` 
	FOREIGN KEY (`id_rrhh_concepto`) REFERENCES `rrhh_concepto` (`id`) , 
	ADD CONSTRAINT `rrhh_concepto_categoria_ibfk_2` 
	FOREIGN KEY (`id_rrhh_categoria`) REFERENCES `rrhh_categoria` (`id`) ;


/* Create table in target */
CREATE TABLE `rrhh_concepto_clase_legajo`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_concepto` INT(11) NOT NULL  , 
	`id_rrhh_clase_legajo` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `id_rrhh_concepto`(`id_rrhh_concepto`) , 
	KEY `id_rrhh_clase_legajo`(`id_rrhh_clase_legajo`) , 
	CONSTRAINT `rrhh_concepto_clase_legajo_ibfk_1` 
	FOREIGN KEY (`id_rrhh_concepto`) REFERENCES `rrhh_concepto` (`id`) , 
	CONSTRAINT `rrhh_concepto_clase_legajo_ibfk_2` 
	FOREIGN KEY (`id_rrhh_clase_legajo`) REFERENCES `rrhh_clase_legajo` (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Alter table in target */
ALTER TABLE `rrhh_concepto_persona_sexo` 
	ADD KEY `id_persona_sexo`(`id_persona_sexo`) , 
	ADD KEY `id_rrhh_concepto`(`id_rrhh_concepto`) ;
ALTER TABLE `rrhh_concepto_persona_sexo`
	ADD CONSTRAINT `rrhh_concepto_persona_sexo_ibfk_1` 
	FOREIGN KEY (`id_rrhh_concepto`) REFERENCES `rrhh_concepto` (`id`) , 
	ADD CONSTRAINT `rrhh_concepto_persona_sexo_ibfk_2` 
	FOREIGN KEY (`id_persona_sexo`) REFERENCES `persona_sexo` (`id`) ;


/* Alter table in target */
ALTER TABLE `rrhh_concepto_sindicato` 
	ADD KEY `id_rrh_sindicato`(`id_rrh_sindicato`) , 
	ADD KEY `id_rrhh_concepto`(`id_rrhh_concepto`) ;
ALTER TABLE `rrhh_concepto_sindicato`
	ADD CONSTRAINT `rrhh_concepto_sindicato_ibfk_1` 
	FOREIGN KEY (`id_rrhh_concepto`) REFERENCES `rrhh_concepto` (`id`) , 
	ADD CONSTRAINT `rrhh_concepto_sindicato_ibfk_2` 
	FOREIGN KEY (`id_rrh_sindicato`) REFERENCES `rrhh_sindicato` (`id`) ;


/* Create table in target */
CREATE TABLE `rrhh_concepto_tipo_liquidacion`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_concepto` INT(11) NOT NULL  , 
	`id_rrhh_tipo_liquidacion` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `id_rrhh_concepto`(`id_rrhh_concepto`) , 
	KEY `id_rrhh_tipo_liquidacion`(`id_rrhh_tipo_liquidacion`) , 
	CONSTRAINT `rrhh_concepto_tipo_liquidacion_ibfk_1` 
	FOREIGN KEY (`id_rrhh_concepto`) REFERENCES `rrhh_concepto` (`id`) , 
	CONSTRAINT `rrhh_concepto_tipo_liquidacion_ibfk_2` 
	FOREIGN KEY (`id_rrhh_tipo_liquidacion`) REFERENCES `rrhh_tipo_liquidacion` (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Alter table in target */
ALTER TABLE `rrhh_liquidacion` 
	DROP FOREIGN KEY `rrhh_liquidacion_ibfk_1`  ;
ALTER TABLE `rrhh_liquidacion`
	ADD CONSTRAINT `rrhh_liquidacion_ibfk_1` 
	FOREIGN KEY (`id_tipo_liquidacion`) REFERENCES `rrhh_tipo_liquidacion` (`id`) ;


/* Alter table in target */
ALTER TABLE `rrhh_sindicato` 
	ADD COLUMN `codigo` VARCHAR(10)  COLLATE utf8mb4_general_ci NULL AFTER `d_rrhh_sindicato` ;

/* Alter table in target */
ALTER TABLE `rrhh_tipo_liquidacion` 
	ADD COLUMN `codigo` VARCHAR(4)  COLLATE utf8mb4_general_ci NOT NULL AFTER `id` , 
	ADD COLUMN `d_rrrhh_tipo_liquidacion` VARCHAR(50)  COLLATE utf8mb4_general_ci NOT NULL AFTER `codigo` , 
	ADD COLUMN `fecha_modificacion` DATETIME   NULL AFTER `d_rrrhh_tipo_liquidacion` , 
	ADD COLUMN `activo` TINYINT(4)   NOT NULL DEFAULT 1 AFTER `fecha_modificacion` , 
	DROP COLUMN `d_rrhh_tipo_liquidacion` , DEFAULT CHARSET='utf8mb4', COLLATE ='utf8mb4_general_ci' ;

/* Create table in target */
CREATE TABLE `rrhh_tipo_liquidacion_clase_liquidacion`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_tipo_liquidacion` INT(11) NOT NULL  , 
	`id_rrhh_clase_liquidacion` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `id_rrhh_tipo_liquidacion`(`id_rrhh_tipo_liquidacion`) , 
	KEY `id_rrhh_clase_liquidacion`(`id_rrhh_clase_liquidacion`) , 
	CONSTRAINT `rrhh_tipo_liquidacion_clase_liquidacion_ibfk_1` 
	FOREIGN KEY (`id_rrhh_tipo_liquidacion`) REFERENCES `rrhh_tipo_liquidacion` (`id`) , 
	CONSTRAINT `rrhh_tipo_liquidacion_clase_liquidacion_ibfk_2` 
	FOREIGN KEY (`id_rrhh_clase_liquidacion`) REFERENCES `rrhh_clase_liquidacion` (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_general_ci';


/* Alter table in target */
ALTER TABLE `tipo_comprobante` 
	ADD KEY `id_cuenta_bancaria`(`id_cuenta_bancaria`) ;
ALTER TABLE `tipo_comprobante`
	ADD CONSTRAINT `tipo_comprobante_ibfk_6` 
	FOREIGN KEY (`id_cuenta_bancaria`) REFERENCES `cuenta_bancaria` (`id`) ;


/* Alter table in target */
ALTER TABLE `tipo_iva` 
	ADD COLUMN `asociado_comprobante_exportacion` TINYINT(4)   NULL AFTER `requiere_forma_pago` ;
	
	
	
	ALTER TABLE `persona` ADD COLUMN `id_rrhh_categoria` INT NULL AFTER `id_persona_sexo`; 
	ALTER TABLE `persona` ADD FOREIGN KEY (`id_rrhh_categoria`) REFERENCES `rrhh_categoria`(`id`); 
	
	
	ALTER TABLE `rrhh_clase_legajo` CHANGE `d_rrhh_tipo_liquidacion` `d_rrhh_clase_legajo` VARCHAR(50) CHARSET latin1 COLLATE latin1_swedish_ci NOT NULL; 
	
	ALTER TABLE `rrhh_condicion_sicoss` CHANGE `d_rrhh_situacion_sicoss` `d_rrhh_condicion_sicoss` VARCHAR(100) CHARSET utf8mb4 COLLATE utf8mb4_general_ci NULL; 
	
	
	ALTER TABLE `rrhh_liquidacion` CHANGE `id_clase_liquidacion` `id_clase_liquidacion` INT(4) NOT NULL; 
	ALTER TABLE `rrhh_liquidacion` ADD FOREIGN KEY (`id_clase_liquidacion`) REFERENCES `rrhh_clase_liquidacion`(`id`); 
	ALTER TABLE `rrhh_liquidacion` ADD FOREIGN KEY (`id_clase_legajo`) REFERENCES `rrhh_clase_legajo`(`id`); 
	
	
	ALTER TABLE `rrhh_liquidacion_persona` ADD FOREIGN KEY (`id_rrhh_liquidacion`) REFERENCES `rrhh_liquidacion`(`id`), ADD FOREIGN KEY (`id_persona`) REFERENCES `persona`(`id`); 
	
	ALTER TABLE `rrhh_concepto` ADD COLUMN `chk_rrhh_tipo_liquidacion` TINYINT NULL AFTER `chk_fecha_ganancia`;
	ALTER TABLE `rrhh_concepto` CHANGE `chk_rrhh_tipo_liquidacion` `chk_rrhh_tipo_liquidacion` TINYINT(4) DEFAULT 1 NULL; 
	UPDATE rrhh_concepto SET chk_rrhh_tipo_liquidacion = 1;
	
	UPDATE rrhh_concepto 
		JOIN rrhh_concepto_tipo_liquidacion ON rrhh_concepto_tipo_liquidacion.`id_rrhh_concepto` = rrhh_concepto.`id`

		SET chk_rrhh_tipo_liquidacion = 0;

ALTER TABLE `persona` CHANGE `rrhh_basico` `rrhh_basico` DECIMAL(20,2) DEFAULT 0 NULL; 

UPDATE persona SET rrhh_basico=0;

ALTER TABLE `persona` CHANGE `fecha_antiguedad` `fecha_antiguedad` DATE NULL; 

ALTER TABLE `rrhh_concepto` ADD FOREIGN KEY (`id_rrhh_tipo_concepto`) REFERENCES `rrhh_tipo_concepto`(`id`); 

ALTER TABLE `tabla_auxiliar` ADD COLUMN `tabla_padre_campo_relacional` VARCHAR(20) NULL AFTER `tabla_padre`;


UPDATE `tabla_auxiliar` SET `tabla_padre_campo_relacional` = 'id_rrhh_obra_social' WHERE `id` = '1'; 
UPDATE `tabla_auxiliar` SET `tabla_padre_campo_relacional` = 'id_rrhh_lugar_pago' WHERE `id` = '2'; 


ALTER TABLE `rrhh_liquidacion` CHANGE `id_clase_liquidacion` `id_rrhh_clase_liquidacion` INT(4) NOT NULL; 
ALTER TABLE `rrhh_concepto_sindicato` CHANGE `id_rrh_sindicato` `id_rrhh_sindicato` INT(11) NOT NULL; 
INSERT INTO `rrhh_variable` (`id`, `codigo`) VALUES ('0', 'CODIGO DE CONCEPTO'); 
UPDATE `rrhh_variable` SET `id` = '0' WHERE `id` = '10'; 

UPDATE `rrhh_variable` SET `codigo_no_visible` = 'CCO' WHERE `id` = '0'; 
UPDATE `rrhh_variable` SET `codigo_no_visible` = 'SJO' WHERE `id` = '1'; 
UPDATE `rrhh_variable` SET `codigo_no_visible` = 'TSR' WHERE `id` = '4'; 
UPDATE `rrhh_variable` SET `codigo_no_visible` = 'THE' WHERE `id` = '5'; 
UPDATE `rrhh_variable` SET `codigo_no_visible` = 'ANT AEA' WHERE `id` = '6'; 
UPDATE `rrhh_variable` SET `codigo_no_visible` = 'TRE' WHERE `id` = '7'; 
UPDATE `rrhh_variable` SET `codigo_no_visible` = 'TSF' WHERE `id` = '8'; 
UPDATE `rrhh_variable` SET `codigo_no_visible` = 'TAU' WHERE `id` = '9'; 
UPDATE `rrhh_variable` SET `codigo_no_visible` = 'BC' WHERE `id` = '2'; 
UPDATE `rrhh_variable` SET `codigo_no_visible` = 'PR' WHERE `id` = '3'; 
INSERT INTO `rrhh_variable` (id,codigo, codigo_no_visible) VALUES (13,'UNIDAD/IMPORTE/CANTIDAD', 'UIC'); 
INSERT INTO `rrhh_variable` (id,codigo, codigo_no_visible) VALUES (14,'Dias trabajados en el mes', 'DTM'); 
INSERT INTO `rrhh_variable` (id,codigo, codigo_no_visible) VALUES (15,'Dias trabajados en el semestre', 'DTS'); 

INSERT INTO `rrhh_variable` (id,codigo, codigo_no_visible) VALUES (18,'Cantidad dias en el semestre', 'CDS'); 

INSERT INTO `rrhh_variable` (id,codigo, codigo_no_visible) VALUES (17,'Cantidad dias en el semestre', 'CDM'); 
UPDATE `rrhh_variable` SET `codigo` = 'DIAS TRABAJADOS EN EL MES' WHERE `id` = '14'; 
UPDATE `rrhh_variable` SET `codigo` = 'DIAS TRABAJADOS EN EL SEMESTRE' WHERE `id` = '15'; 
UPDATE `rrhh_variable` SET `codigo` = 'CANTIDAD DE DIAS EN EL MES' WHERE `id` = '17'; 
UPDATE `rrhh_variable` SET `codigo` = 'CANTIDAD DE DIAS EN EL SEMESTRE' WHERE `id` = '18'; 


INSERT INTO `rrhh_variable` (id,`codigo`, `codigo_no_visible`) VALUES (19,'ABRE PARENTESIS', '('); 
 INSERT INTO `rrhh_variable` (id`codigo`, `codigo_no_visible`) VALUES (20,'CIERRA PARENTESIS', ')'); 
 ALTER TABLE `persona` ADD COLUMN `numero_documento` INT NULL AFTER `id_rrhh_categoria`; 
 ALTER TABLE `persona` CHANGE `dpto` `dpto` VARCHAR(3) CHARSET latin1 COLLATE latin1_swedish_ci NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (`id`); 
 ALTER TABLE `persona` CHANGE `dpto` `dpto` VARCHAR(5) CHARSET latin1 COLLATE latin1_swedish_ci NULL;
 
 
insert  into `persona_parentezco`(`id`,`d_persona_parentezco`) values (1,'HIJO'),(2,'HIJA'),(3,'ESPOSO'),(4,'ESPOSA');
ALTER TABLE `persona` ADD COLUMN `id_rrhh_sindicato` INT NULL AFTER `numero_documento`; 
ALTER TABLE `persona` ADD FOREIGN KEY (`id_rrhh_sindicato`) REFERENCES `rrhh_sindicato`(`id`); 
ALTER TABLE `cuenta_bancaria` ADD COLUMN `id_banco` INT NULL AFTER `alias_cbu`, ADD FOREIGN KEY (`id_banco`) REFERENCES `banco`(`id`); 
ALTER TABLE `rrhh_persona_concepto` CHANGE `orden` `orden` INT(11) NULL;
ALTER TABLE `rrhh_liquidacion` CHANGE `id_clase_legajo` `id_rrhh_clase_legajo` INT(11) NOT NULL; 
ALTER TABLE `rrhh_liquidacion` CHANGE `id_tipo_liquidacion` `id_rrhh_tipo_liquidacion` INT(11) NOT NULL; 

CREATE TABLE `rrhh_liquidacion_categoria`( `id` INT NOT NULL AUTO_INCREMENT, `id_rrhh_liquidacion` INT NOT NULL, `id_rrhh_categoria` INT NOT NULL, PRIMARY KEY (`id`) ); 
CREATE TABLE `rrhh_liquidacion_lugar_pago` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_rrhh_liquidacion` INT NOT NULL,
  `id_rrhh_lugar_pago` INT NOT NULL,
  PRIMARY KEY (`id`)
) ;

CREATE TABLE `rrhh_liquidacion_area` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_rrhh_liquidacion` INT NOT NULL,
  `id_area` INT NOT NULL,
  PRIMARY KEY (`id`)
) ;

ALTER TABLE `rrhh_liquidacion` 
  ADD COLUMN `chk_legajos` TINYINT NULL AFTER `id_rrhh_clase_liquidacion`,
  ADD COLUMN `rango_lista` INT NULL AFTER `chk_legajos` ;
  
  ALTER TABLE `rrhh_liquidacion` 
  ADD COLUMN `legajo_desde` INT NULL AFTER `rango_lista`,
  ADD COLUMN `legajo_hasta` INT NULL AFTER `legajo_desde` ;

ALTER TABLE `rrhh_liquidacion` ADD COLUMN `fecha_inicio_vacaciones` DATE NULL AFTER `id_rrhh_clase_liquidacion`, ADD COLUMN `fecha_fin_vacaciones` DATE NULL AFTER `fecha_inicio_vacaciones`; 

ALTER TABLE `rrhh_novedad_persona` CHANGE `importe_calculado` `importe_calculado` DECIMAL(20,2) NULL; 
ALTER TABLE `rrhh_novedad_persona` DROP COLUMN `id`, DROP PRIMARY KEY; 
/*Esto se hace para migrar las areas en donde trabajan los legajos*/
UPDATE persona SET id_area = NULL;

UPDATE centro_costo SET id_area = NULL;

DELETE FROM AREA;
insert  into `area`(`id`,`d_area`,`abreviado_d_area`) values (1,'ADMINISTRACION',NULL),(2,'FABRICA',NULL),(3,'REBABA',NULL),(4,'MOLDEO PESADO',NULL),(5,'MOLDEO LIVIANO',NULL),(6,'HORNO',NULL),(7,'INSPECCION FINAL',NULL),(8,'PORTERIA',NULL),(9,'RECUPERADOR DE ARENA',NULL),(10,'NOYERIA',NULL),(11,'PRENSA Y ROMPE CAJA',NULL),(12,'GERENCIA',NULL),(13,'CALIDAD',NULL),(14,'RECURSOS HUMANOS',NULL),(15,'COMPRAS',NULL),(16,'PRODUCCION',NULL),(17,'INGENIERIA',NULL),(18,'COMEDOR',NULL),(19,'VENTAS',NULL),(20,'PAÑOL',NULL),(21,'PAÑOL DE MODELOS',NULL),(22,'MECANIZADO',NULL),(23,'MANTENIMIENTO',NULL),(24,'LIMPIEZA',NULL),(25,'FORESTAL',NULL),(26,'SERENO',NULL);
/***************************/


/************SINDICATOS*****************/

UPDATE persona SET id_rrhh_sindicato = NULL;
delete from rrhh_sindicato`;
insert  into `rrhh_sindicato`(`id`,`d_rrhh_sindicato`,`codigo`) values (1,'ASIMRA 3%',NULL),(2,'UOM 2%',NULL),(3,'UOM 2.5%',NULL),(4,'EXCLUIDO DE CONVENIO',NULL),(5,'ASIMRA 2,5 %',NULL),(6,'UATRE 2%',NULL);

/*************************************/


/*Obras Sociales*************************/
DELETE FROM rrhh_concepto_obra_social
DELETE FROM rrhh_obra_social
INSERT  INTO `rrhh_obra_social`(`id`,`codigo`,`d_rrhh_obra_social`,`activa`,`fecha_modificacion`,`codigo_extra`) VALUES (12,'0','NINGUNA',NULL,NULL,NULL),(13,'109','O.S. DEL PERSONAL JERARQUICO DE LA INDUSTRIA GRAFI',NULL,NULL,NULL),(14,'208','O.S. DEL PERSONAL DE LA INDUSTRIA GRAFICA DE LA PR',NULL,NULL,NULL),(15,'307','O.S. PORTUARIOS ARGENTINOS DE MAR DEL PLATA',NULL,NULL,NULL),(16,'406','O.S. DEL PERSONAL DEL ORGANISMO DE CONTROL EXTERNO',NULL,NULL,NULL),(17,'505','O.S . DE CAPITANES, PILOTOS Y PATRONES DE PESCA',NULL,NULL,NULL),(18,'604','O.S . DE AGENTES DE LOTERIAS Y AFINES DE LA REPUBL',NULL,NULL,NULL),(19,'703','MUTUAL DEL PERSONAL DEL AGUA Y LA ENERGIA ELECTRIC',NULL,NULL,NULL),(20,'802','O.S. DE Y.P.F.',NULL,NULL,NULL),(21,'901','O.S. DE LA ACTIVIDAD DE SEGUROS, REASEGUROS, CAPIT',NULL,NULL,NULL),(22,'1003','O.S. DEL PERSONAL DE OBRAS Y SERVICIOS SANITARIOS',NULL,NULL,NULL),(23,'1102','O.S. PARA LA ACTIVIDAD DOCENTE',NULL,NULL,NULL),(24,'1201','O.S. PARA EL PERSONAL DEL MINISTERIO DE ECONOMIA Y',NULL,NULL,NULL),(25,'1300','O.S. FERROVIARIA',NULL,NULL,NULL),(26,'1508','O.S . DE LA ASOCIACION CIVIL PRO SINDICATO AMAS DE',NULL,NULL,NULL),(27,'1607','O.S. DEL SINDICATO UNIDO DE TRABAJADORES DE LA IND',NULL,NULL,NULL),(28,'1706','O.S . DEL PERSONAL JERARQUICO DE LA REPUBLICA ARGE',NULL,NULL,NULL),(29,'1805','O.S . DE LOS LEGISLADORES DE LA REPUBLICA ARGENTIN',NULL,NULL,NULL),(30,'1904','O.S DE LA FEDERACION DE CAMARAS Y CENTROS COMERCIA',NULL,NULL,NULL),(31,'2006','O.S. de VIALIDAD NACIONAL',NULL,NULL,NULL),(32,'2105','O.S. PROFESIONALES DEL TURF DE LA RA (OSPROTURA)',NULL,NULL,NULL),(33,'2204','O.S. DE EMPL.Y PERS. JERARQ.DE LA ACT.DEL NEUM.ARG',NULL,NULL,NULL),(34,'2303','O.S. PARA EL PERSONAL DE EMPRESAS DE LIMPIEZA, SER',NULL,NULL,NULL),(35,'2402','OBRA SOCIAL DEL PERSONAL JERARQUICO DEL TRANSPORTE',NULL,NULL,NULL),(36,'2501','O.S. DE MINISTROS, SECRETARIOS Y SUBSECRETARIOS',NULL,NULL,NULL),(37,'2600','O.S. DE LOS TRABAJADORES DE LA CARNE Y AFINES DE L',NULL,NULL,NULL),(38,'2709','O.S. DE LOS TRABAJADORES ASOCIADOS A LA ASOCIACION',NULL,NULL,NULL),(39,'2808','O.S. DE LA CAMARA DE EMPRESARIOS DE AGENCIAS DE RE',NULL,NULL,NULL),(40,'2907','O.S. ASOCIACION MUTUAL METALURGICA VILLA CONSTITUC',NULL,NULL,NULL),(41,'3009','O.S. PERSONAL ASOCIADO A ASOC.MUTUAL SANCOR (O.S.P',NULL,NULL,NULL),(42,'100106','O.S. PARA EL PERSONAL DE LA INDUSTRIA ACEITERA DES',NULL,NULL,NULL),(43,'100205','O.S. DE ACTORES',NULL,NULL,NULL),(44,'100304','O.S. DE TECNICOS DE VUELO DE LINEAS AEREAS',NULL,NULL,NULL),(45,'100403','O.S. DEL PERSONAL SUPERIOR Y PROFESIONAL DE EMPRES',NULL,NULL,NULL),(46,'100502','O.S. DEL PERSONAL AERONAUTICO',NULL,NULL,NULL),(47,'100601','O.S. DEL PERSONAL DE AERONAVEGACION DE ENTES PRIVA',NULL,NULL,NULL),(48,'100700','O.S. DEL PERSONAL TECNICO AERONAUTICO',NULL,NULL,NULL),(49,'100809','O.S. DE AERONAVEGANTES',NULL,NULL,NULL),(50,'100908','O.S. DE EMPLEADOS DE AGENCIAS DE INFORMES (O.S.E.D',NULL,NULL,NULL),(51,'101000','O.S. DEL PERSONAL DE AGUAS GASEOSAS Y AFINES',NULL,NULL,NULL),(52,'101109','O.S. DE ALFAJOREROS, REPOSTEROS, PIZZEROS Y HELADE',NULL,NULL,NULL),(53,'101208','O.S. DEL PERSONAL DE LA INDUSTRIA DE LA ALIMENTACI',NULL,NULL,NULL),(54,'101307','O.S. DEL PERSONAL DE ARTES GRAFICAS DEL CHACO',NULL,NULL,NULL),(55,'101406','O.S. ARTES GRAFICAS DE SANTA FE',NULL,NULL,NULL),(56,'101505','O.S. DE ARTISTAS DE VARIEDADES',NULL,NULL,NULL),(57,'101604','O.S. DEL PERSONAL DEL AUTOMOVIL CLUB ARGENTINO',NULL,NULL,NULL),(58,'101703','O.S. DEL PERSONAL DEL AZUCAR DE CALILEGUA',NULL,NULL,NULL),(59,'101802','O.S. DEL PERSONAL DEL AZUCAR DEL INGENIO LA ESPERA',NULL,NULL,NULL),(60,'101901','O.S. DEL PERSONAL DEL AZUCAR DEL INGENIO LEDESMA',NULL,NULL,NULL),(61,'102003','O.S. DEL PERSONAL DEL AZUCAR DEL INGENIO LAS TOSCA',NULL,NULL,NULL),(62,'102102','O.S. DEL PERSONAL DEL AZUCAR DEL INGENIO RIO GRAND',NULL,NULL,NULL),(63,'102201','O.S. DEL PERSONAL DEL AZUCAR DEL INGENIO SAN ISIDR',NULL,NULL,NULL),(64,'102300','O.S. DEL PERSONAL DEL AZUCAR DEL INGENIO SAN MARTI',NULL,NULL,NULL),(65,'102409','O.S. DEL PERSONAL DEL AZUCAR DE VILLA OCAMPO',NULL,NULL,NULL),(66,'102607','O.S. DEL PERSONAL DE LA ACTIVIDAD AZUCARERA TUCUMA',NULL,NULL,NULL),(67,'102706','O.S. DEL PERSONAL DE LA INDUSTRIA AZUCARERA',NULL,NULL,NULL),(68,'102805','O.S. DE BANCARIOS',NULL,NULL,NULL),(69,'102904','O.S. DEL PERSONAL DE BARRACAS DE LANAS, CUEROS Y A',NULL,NULL,NULL),(70,'103006','O.S. DEL PERSONAL DE LA INDUSTRIA BOTONERA',NULL,NULL,NULL),(71,'103105','O.S. DEL PERSONAL DE LA INDUSTRIA DEL CALZADO',NULL,NULL,NULL),(72,'103204','O.S. DE CONDUCTORES CAMIONEROS Y PERSONAL DEL TRAN',NULL,NULL,NULL),(73,'103303','O.S. DEL PERSONAL DE LA JUNTA NACIONAL DE CARNES',NULL,NULL,NULL),(74,'103402','O.S. DEL PERSONAL DE CARGA Y DESCARGA',NULL,NULL,NULL),(75,'103600','O.S. DEL PERSONAL AUXILIAR DE CASAS PARTICULARES',NULL,NULL,NULL),(76,'103709','O.S. DEL PERSONAL DE LA INDUSTRIA DEL CAUCHO',NULL,NULL,NULL),(77,'103808','O.S. DEL PERSONAL DEL CAUCHO',NULL,NULL,NULL),(78,'103907','O.S. DEL PERSONAL DE LA INDUSTRIA DEL CAUCHO DE SA',NULL,NULL,NULL),(79,'104009','O.S. DEL PERSONAL DE CEMENTERIOS DE LA REP. ARGENT',NULL,NULL,NULL),(80,'104108','O.S. DE CERAMISTAS',NULL,NULL,NULL),(81,'104207','O.S. DEL PERSONAL DE LA CERAMICA, SANITARIOS, PORC',NULL,NULL,NULL),(82,'104306','O.S. DEL PERSONAL DE LA ACTIVIDAD CERVECERA Y AFIN',NULL,NULL,NULL),(83,'104405','O.S. DEL PERSONAL CINEMATOGRAFICO DE MAR DEL PLATA',NULL,NULL,NULL),(84,'104504','O.S. DEL PERSONAL DE LA INDUSTRIA CINEMATOGRAFICA',NULL,NULL,NULL),(85,'104603','O.S. DE OPERADORES CINEMATOGRAFICOS',NULL,NULL,NULL),(86,'104702','O.S. DE COLCHONEROS',NULL,NULL,NULL),(87,'104801','O.S. DE COLOCADORES DE AZULEJOS, MOSAICOS, GRANITE',NULL,NULL,NULL),(88,'104900','O.S. DE EMPLEADOS DE COMERCIO',NULL,NULL,NULL),(89,'105002','O.S. DE CONDUCTORES NAVALES',NULL,NULL,NULL),(90,'105101','O.S. DE CONSIGNATARIOS DEL MERCADO GENERAL DE HACI',NULL,NULL,NULL),(91,'105200','INSTITUTO DE SERVICIOS SOCIALES PARA EL PERSONAL D',NULL,NULL,NULL),(92,'105309','O.S. DEL PERSONAL ADMINISTRATIVO Y TECNICO DE LA C',NULL,NULL,NULL),(93,'105408','O.S. DEL PERSONAL DE LA CONSTRUCCION',NULL,NULL,NULL),(94,'105507','O.S. DE LOS CORTADORES DE LA INDUMENTARIA',NULL,NULL,NULL),(95,'105606','O.S. DEL PERSONAL DE LA INDUSTRIA DEL CUERO Y AFIN',NULL,NULL,NULL),(96,'105705','O.S. DEL PERSONAL DE LA INDUSTRIA DEL CHACINADO Y ',NULL,NULL,NULL),(97,'105804','O.S. DE CHOFERES DE CAMIONES',NULL,NULL,NULL),(98,'105903','O.S. CHOFERES PARTICULARES',NULL,NULL,NULL),(99,'106005','O.S. DEL PERSONAL DE ENTIDADES DEPORTIVAS Y CIVILE',NULL,NULL,NULL),(100,'106104','O.S. DE EMPLEADOS DE DESPACHANTES DE ADUANA',NULL,NULL,NULL),(101,'106203','O.S. DEL PERS. DE DISTRIBUIDORES CINEMATOGRAF. DE ',NULL,NULL,NULL),(102,'106302','O.S. DE DOCENTES PARTICULARES',NULL,NULL,NULL),(103,'106401','O.S. DEL PERSONAL DE EDIFCIOS DE RENTA Y HORIZONTA',NULL,NULL,NULL),(104,'106500','O.S. DEL PERSONAL DE EDIFICIOS DE RENTAS Y HORIZON',NULL,NULL,NULL),(105,'106609','O.S. ELECTRICISTAS NAVALES',NULL,NULL,NULL),(106,'106708','O.S. DE OBREROS EMPACADORES DE FRUTA DE RIO NEGRO ',NULL,NULL,NULL),(107,'106807','O.S. DEL PERSONAL DE LA ENSEÑANZA PRIVADA',NULL,NULL,NULL),(108,'106906','O.S. DEL PERSONAL DE ESCRIBANIAS DE LA PROVINCIA D',NULL,NULL,NULL),(109,'107008','O.S. DEL PERSONAL DE ESCRIBANOS',NULL,NULL,NULL),(110,'107107','O.S. DEL PERSONAL DEL ESPECTACULO PUBLICO',NULL,NULL,NULL),(111,'107206','O.S. DEL PERSONAL DE ESTACIONES DE SERVICIO, GARAG',NULL,NULL,NULL),(112,'107404','O.S. DEL PERSONAL DE FARMACIA',NULL,NULL,NULL),(113,'107503','O.S. DEL PERSONAL DE LA FERIA INFANTIL',NULL,NULL,NULL),(114,'107602','O.S. DEL PERSONAL DE FERMOLAC',NULL,NULL,NULL),(115,'107701','O.S. DE FERROVIARIOS',NULL,NULL,NULL),(116,'107800','O.S. DEL PERSONAL DE LA INDUSTRIA DEL FIBROCEMENTO',NULL,NULL,NULL),(117,'107909','O.S. DEL PERSONAL DE LA INDUSTRIA FIDEERA',NULL,NULL,NULL),(118,'108001','O.S. DEL PERSONAL DE LA INDUSTRIA FORESTAL DE SANT',NULL,NULL,NULL),(119,'108100','O.S. DEL PERSONAL DE LA INDUSTRIA DEL FOSFORO',NULL,NULL,NULL),(120,'108209','O.S. DE FOTOGRAFOS',NULL,NULL,NULL),(121,'108407','O.S. DEL PERSONAL DE LA ACTIVIDAD FRUCTICOLA',NULL,NULL,NULL),(122,'108506','O.S. DEL PERSONAL DE MANIPULEO, EMPAQUE Y EXPEDICI',NULL,NULL,NULL),(123,'108605','O.S. DE FUTBOLISTAS',NULL,NULL,NULL),(124,'108704','O.S. DE TECNICOS DE FUTBOL',NULL,NULL,NULL),(125,'108803','O.S. DEL PERSONAL DEL TURISMO, HOTELERO Y GASTRONO',NULL,NULL,NULL),(126,'108902','O.S. DEL PERSONAL SUPERIOR DE GOOD YEAR ARGENTINA',NULL,NULL,NULL),(127,'109004','O.S. DEL PERSONAL GRAFICO',NULL,NULL,NULL),(128,'109103','O.S. DEL PERSONAL GRAFICO DE CORRIENTES',NULL,NULL,NULL),(129,'109202','O.S. DE GUINCHEROS Y MAQUINISTAS DE GRUAS MOVILES',NULL,NULL,NULL),(130,'109301','O.S. DEL PERSONAL DE CONSIGNATARIOS DEL MERCADO NA',NULL,NULL,NULL),(131,'109400','O.S. DEL PERSONAL DE LA INDUSTRIA DEL HIELO Y MERC',NULL,NULL,NULL),(132,'109509','O.S. DEL PERSONAL DE LOS HIPODROMOS DE BUENOS AIRE',NULL,NULL,NULL),(133,'109608','O.S. DEL PERSONAL MENSUALIZADO DEL JOCKEY CLUB DE ',NULL,NULL,NULL),(134,'109707','O.S. DEL PERSONAL DE IMPRENTA, DIARIOS Y AFINES',NULL,NULL,NULL),(135,'109905','O.S. DEL PERSONAL DEL INGENIO SAN PABLO',NULL,NULL,NULL),(136,'110008','O.S. DEL PERSONAL DE JABONEROS',NULL,NULL,NULL),(137,'110107','O.S. DE JARDINEROS,PARQUISTAS,VIVERISTAS Y FLORIC.',NULL,NULL,NULL),(138,'110206','O.S. DEL PERSONAL DE JOCKEY CLUB DE ROSARIO',NULL,NULL,NULL),(139,'110305','O.S. DEL PERSONAL LADRILLERO',NULL,NULL,NULL),(140,'110404','O.S. DEL PERSONAL DE LA INDUSTRIA LADRILLERA A MAQ',NULL,NULL,NULL),(141,'110503','O.S. DEL PERSONAL DE LA INDUSTRIA LECHERA',NULL,NULL,NULL),(142,'110602','O.S. DE LOCUTORES',NULL,NULL,NULL),(143,'110701','O.S. DE LA FEDERACION ARGENTINA DE TRABAJADORES DE',NULL,NULL,NULL),(144,'110800','O.S. DE LOS TRABAJADORES DE LAS EMPRESAS DE ELECTR',NULL,NULL,NULL),(145,'110909','O.S. DEL PERSONAL DE LUZ Y FUERZA DE CORDOBA',NULL,NULL,NULL),(146,'111001','O.S. DEL PERSONAL DE LA INDUSTRIA MADERERA',NULL,NULL,NULL),(147,'111209','O.S. DEL PERSONAL DE MAESTRANZA',NULL,NULL,NULL),(148,'111308','O.S. DE MAQUINISTAS DE TEATRO Y TELEVISION',NULL,NULL,NULL),(149,'111407','O.S. DE CAPITANES DE ULTRAMAR Y OFICIALES DE LA MA',NULL,NULL,NULL),(150,'111506','O.S. CAPITANES BAQUEANOS FLUVIALES DE LA MARINA ME',NULL,NULL,NULL),(151,'111605','O.S. DE EMPLEADOS DE LA MARINA MERCANTE',NULL,NULL,NULL),(152,'111704','O.S. DE ENCARGADOS APUNTADORES MARITIMOS',NULL,NULL,NULL),(153,'111803','O.S. DEL PERSONAL MARITIMO',NULL,NULL,NULL),(154,'111902','O.S. DEL SINDICATO DE MECANICOS Y AFINES DEL TRANS',NULL,NULL,NULL),(155,'112004','O.S. DEL PERSONAL SUPERIOR DE MERCEDES BENZ ARGENT',NULL,NULL,NULL),(156,'112103','O.S. DE LA UNION OBRERA METALURGICO DE LA REPUBLIC',NULL,NULL,NULL),(157,'112202','O.S. DE LOS SUPERVISORES DE LA INDUSTRIA METALMECA',NULL,NULL,NULL),(158,'112301','O.S. DEL PERSONAL DE MICROS Y OMNIBUS DE MENDOZA',NULL,NULL,NULL),(159,'112400','O.S. DE LA ACTIVIDAD MINERA',NULL,NULL,NULL),(160,'112509','O.S. MODELOS ARGENTINOS',NULL,NULL,NULL),(161,'112608','O.S. DEL PERSONAL DE LA INDUSTRIA MOLINERA',NULL,NULL,NULL),(162,'112707','O.S. DEL PERSONAL MOSAISTA',NULL,NULL,NULL),(163,'112806','O.S. DE MUSICOS',NULL,NULL,NULL),(164,'112905','O.S. DE MUSICOS DE CUYO',NULL,NULL,NULL),(165,'113007','O.S. DE MUSICOS DE MAR DEL PLATA',NULL,NULL,NULL),(166,'113205','O.S. DE JEFES Y OFICIALES NAVALES DE RADIOCOMUNICA',NULL,NULL,NULL),(167,'113304','O.S. DE JEFES Y OFICIALES MAQUINISTAS NAVALES',NULL,NULL,NULL),(168,'113403','O.S. DEL PERSONAL NAVAL',NULL,NULL,NULL),(169,'113601','O.S. DEL PERSONAL DE LA INDUSTRIA DEL NEUMATICO',NULL,NULL,NULL),(170,'113700','O.S. DEL PERSONAL DE LA INDUSTRIA NAVAL',NULL,NULL,NULL),(171,'113809','O.S. DE COMISARIOS NAVALES',NULL,NULL,NULL),(172,'113908','O.S. DEL PERSONAL DE PANADERIAS',NULL,NULL,NULL),(173,'114000','O.S. DE PANADEROS, PASTELEROS Y FACTUREROS DE ENTR',NULL,NULL,NULL),(174,'114109','O.S. DEL PERSONAL DEL PAPEL, CARTON Y QUIMICOS',NULL,NULL,NULL),(175,'114208','O.S. DE LA INDUSTRIA DE PASTAS ALIMENTICIAS',NULL,NULL,NULL),(176,'114307','O.S. TRABAJ. PAST, CONFIT, PIZZEROS, HELAD. Y ALFA',NULL,NULL,NULL),(177,'114505','O.S. DE PATRONES DE CABOTAJE DE RIOS Y PUERTOS',NULL,NULL,NULL),(178,'114604','O.S. DE PELETEROS',NULL,NULL,NULL),(179,'114703','O.S. DEL PERSONAL DE PELUQUERIAS, ESTETICA Y AFINE',NULL,NULL,NULL),(180,'114802','O.S. DE OFICIALES PELUQUEROS Y PEINADORES',NULL,NULL,NULL),(181,'114901','O.S.OFICIALES PELUQUEROS Y PEINADORES DE ROSARIO',NULL,NULL,NULL),(182,'115003','O.S. DEL PERSONAL DE LA ACTIVIDAD PERFUMISTA',NULL,NULL,NULL),(183,'115102','O.S. DE TRABAJADORES DE PRENSA DE BUENOS AIRES',NULL,NULL,NULL),(184,'115201','O.S. DEL PERSONAL DE LA INDUSTRIA DEL PESCADO DE M',NULL,NULL,NULL),(185,'115300','O.S. DE PETROLEROS',NULL,NULL,NULL),(186,'115409','O.S. DEL PETROLEO Y GAS PRIVADO',NULL,NULL,NULL),(187,'115508','O.S. DE PETROLEROS DE CORDOBA',NULL,NULL,NULL),(188,'115607','O.S. DEL PERSONAL DE LA INDUSTRIA PETROQUIMICA',NULL,NULL,NULL),(189,'115706','O.S. DE PILOTOS DE LINEAS AEREAS COMERCIALES Y REG',NULL,NULL,NULL),(190,'115805','O.S. DEL PERSONAL DE FABRICAS DE PINTURA',NULL,NULL,NULL),(191,'115904','O.S. DEL PERSONAL DE ACADEMIAS PITMAN',NULL,NULL,NULL),(192,'116006','O.S. DEL PERSONAL DE LA INDUSTRIA DEL PLASTICO',NULL,NULL,NULL),(193,'116105','O.S. DE CAPATACES ESTIBADORES PORTUARIOS',NULL,NULL,NULL),(194,'116204','O.S. DE PORTUARIOS ARGENTINOS',NULL,NULL,NULL),(195,'116303','O.S. PORTUARIOS DE BAHIA BLANCA',NULL,NULL,NULL),(196,'116402','O.S. PORTUARIOS DE NECOCHEA Y QUEQUEN',NULL,NULL,NULL),(197,'116501','O.S. PORTUARIOS DE ROSARIO',NULL,NULL,NULL),(198,'116600','O.S. PORTUARIOS DE SAN LORENZO',NULL,NULL,NULL),(199,'116709','O.S. PORTUARIOS DE PUERTO SAN MARTIN Y BELLA VISTA',NULL,NULL,NULL),(200,'116808','O.S. PORTUARIOS PUERTO SAN NICOLAS',NULL,NULL,NULL),(201,'116907','O.S. PORTUARIOS DE SAN PEDRO',NULL,NULL,NULL),(202,'117009','O.S. PORTUARIOS DE SANTA FE',NULL,NULL,NULL),(203,'117108','O.S. PORTUARIOS DE VILLA CONSTITUCIÓN',NULL,NULL,NULL),(204,'117207','O.S. DEL PERSONAL DE PRENSA DE LA REPUBLICA ARGENT',NULL,NULL,NULL),(205,'117405','O.S. DEL PERSONAL DE PRENSA DE BAHIA BLANCA',NULL,NULL,NULL),(206,'117603','O.S. DEL PERSONAL DE PRENSA DE LA PROVINCIA DEL CH',NULL,NULL,NULL),(207,'117702','O.S. DEL PERSONAL DE PRENSA DE MAR DEL PLATA',NULL,NULL,NULL),(208,'117801','O.S. DEL PERSONAL DE PRENSA DE MENDOZA',NULL,NULL,NULL),(209,'118002','O.S EMPLEADOS DE PRENSA DE CORDOBA',NULL,NULL,NULL),(210,'118101','O.S. DE PROFESIONALES DE LA OS DEL PERSONAL DEL PA',NULL,NULL,NULL),(211,'118200','O.S. DE AGENTES DE PROPAGANDA MEDICA DE LA REPUBLI',NULL,NULL,NULL),(212,'118309','O.S. DE AGENTES DE PROPAGANDA MEDICA DE CORDOBA',NULL,NULL,NULL),(213,'118408','O.S. DE AGENTES DE PROPAGANDA MEDICA DE ENTRE RIOS',NULL,NULL,NULL),(214,'118507','O.S. DE AGENTES DE PROPAGANDA MEDICA DE ROSARIO',NULL,NULL,NULL),(215,'118606','O.S. DEL PERSONAL DE LA PUBLICIDAD',NULL,NULL,NULL),(216,'118705','O.S. DEL PERSONAL DE INDUSTRIAS QUIMICAS Y PETROQU',NULL,NULL,NULL),(217,'118804','O.S. DE RECIBIDORES DE GRANOS Y ANEXOS',NULL,NULL,NULL),(218,'118903','O.S. DEL PERSONAL DE RECOLECCION Y BARRIDO DE ROSA',NULL,NULL,NULL),(219,'119005','O.S. DEL PERSONAL DE REFINERIAS DE MAIZ',NULL,NULL,NULL),(220,'119104','O.S. DE LA INDUSTRIA DE MATERIALES REFRACTARIOS Y ',NULL,NULL,NULL),(221,'119203','O.S. DE RELOJEROS Y JOYEROS',NULL,NULL,NULL),(222,'119302','O.S. DEL PERSONAL RURAL Y ESTIBADORES DE LA REPUBL',NULL,NULL,NULL),(223,'119401','O.S. DEL PERSONAL DE LA INDUSTRIA SALINERA',NULL,NULL,NULL),(224,'119500','O.S. DEL PERSONAL DE LA SANIDAD ARGENTINA',NULL,NULL,NULL),(225,'119609','O.S. DEL PERSONAL DE INSTALACIONES SANITARIAS',NULL,NULL,NULL),(226,'119708','O.S. DEL PERSONAL DE SEGURIDAD COMERCIAL, INDUSTRI',NULL,NULL,NULL),(227,'119807','O.S. DEL PERSONAL DEL SEGURO',NULL,NULL,NULL),(228,'119906','O.S. DE SERENOS DE BUQUES',NULL,NULL,NULL),(229,'120108','O.S. DE TECNICOS, PROFESIONALES, EMPLEADOS Y SUPER',NULL,NULL,NULL),(230,'120207','O.S. DEL PERSONAL DE STANDARD ELECTRIC',NULL,NULL,NULL),(231,'120306','O.S. DEL PERSONAL DE SUPERVISION DE LA EMPRESA SUB',NULL,NULL,NULL),(232,'120405','O.S. DEL PERSONAL DE DIRECCION DE LA EMPRESA SUBTE',NULL,NULL,NULL),(233,'120504','O.S. DEL PERSONAL DE LA INDUSTRIA DEL TABACO',NULL,NULL,NULL),(234,'120603','O.S. DE EMPLEADOS DEL TABACO',NULL,NULL,NULL),(235,'120702','O.S. DEL PERSONAL DE LAS TELECOMUNICACIONES DE LA ',NULL,NULL,NULL),(236,'120801','O.S. DE TRABAJADORES DE LAS COMUNICACIONES (OSTRAC',NULL,NULL,NULL),(237,'120900','O.S. DEL PERSONAL DE TELEVISION',NULL,NULL,NULL),(238,'121002','O.S. DEL PERSONAL DE LA INDUSTRIA TEXTIL',NULL,NULL,NULL),(239,'121101','O.S. DE EMPLEADOS TEXTILES Y AFINES',NULL,NULL,NULL),(240,'121200','O.S. DE TINTOREROS, SOMBREREROS Y LAVADEROS',NULL,NULL,NULL),(241,'121309','O.S. DEL PERSONAL DE LA INDUSTRIA DEL TRACTOR',NULL,NULL,NULL),(242,'121408','O.S. DEL TRANSPORTE AUTOMOTOR DE ROSARIO',NULL,NULL,NULL),(243,'121507','O.S. DE LA INDUSTRIA DEL TRANSPORTE AUTOMOTOR DE C',NULL,NULL,NULL),(244,'121606','O.S. DE CONDUCTORES DE TRANSPORTE COLECTIVO DE PAS',NULL,NULL,NULL),(245,'121705','O.S. DEL PERSONAL DE LA ACTIVIDAD DEL TURF',NULL,NULL,NULL),(246,'121804','O.S. CONDUCTORES DE TAXIS DE CORDOBA',NULL,NULL,NULL),(247,'121903','O.S. DE VAREADORES',NULL,NULL,NULL),(248,'122005','O.S. DEL PERSONAL DE LA INDUSTRIA DEL VESTIDO Y AF',NULL,NULL,NULL),(249,'122104','O.S. DE VIAJANTES VENDEDORES DE LA REPUBLICA ARGEN',NULL,NULL,NULL),(250,'122203','O.S. DE VIAJANTES DE COMERCIO',NULL,NULL,NULL),(251,'122302','O.S. DEL PERSONAL DE LA ACTIVIDAD VIAL',NULL,NULL,NULL),(252,'122401','O.S. DE EMPLEADOS DE LA INDUSTRIA DEL VIDRIO',NULL,NULL,NULL),(253,'122500','O.S. DEL PERSONAL DE LA INDUSTRIA DEL VIDRIO',NULL,NULL,NULL),(254,'122609','O.S. DEL PERSONAL DE LA ACTIVIDAD VITIVINICOLA',NULL,NULL,NULL),(255,'122708','O.S. DEL PERSONAL DE VIALIDAD NACIONAL',NULL,NULL,NULL),(256,'122807','O.S. DEL PERSONAL DE VIGILANCIA Y SEGURIDAD COMERC',NULL,NULL,NULL),(257,'122906','O.S.DEL PERSONAL. ESTACIONES DE SERVICIO, GARAGES,',NULL,NULL,NULL),(258,'123008','O.S. PARA EL PERSONAL DE ESTACIONES DE SERVICIO, G',NULL,NULL,NULL),(259,'123107','O.S. DE TALLERISTAS A DOMICILIO',NULL,NULL,NULL),(260,'123206','O.S. DE BAÑEROS Y AFINES DEL PARTIDO DE GENERAL PU',NULL,NULL,NULL),(261,'123305','O.S. DEL PERSONAL DE SOCIEDADES DE AUTORES Y AFINE',NULL,NULL,NULL),(262,'123404','O.S. DEL PERSONAL DE PRENSA DE ROSARIO',NULL,NULL,NULL),(263,'123503','O.S. DEL PERSONAL DE PRENSA DE TUCUMAN',NULL,NULL,NULL),(264,'123602','O.S. DE TRABAJADORES DE PERKINS ARGENTINA S.A.I.C.',NULL,NULL,NULL),(265,'123701','O.S. DE PEONES DE TAXIS DE LA CAPITAL FEDERAL',NULL,NULL,NULL),(266,'123800','O.S. DEL PERSONAL GUARDAVIDAS Y AFINES DE LA REPUB',NULL,NULL,NULL),(267,'123909','O.S. DE VENDEDORES AMBULANTES DE LA REPUBLICA ARGE',NULL,NULL,NULL),(268,'124001','O.S. DE BOXEADORES AGREMIADOS DE LA REPUBLICA ARGE',NULL,NULL,NULL),(269,'124209','O.S. DE EMBALADORES, DESCARTADORES Y ALAMBRADORES ',NULL,NULL,NULL),(270,'124308','O.S. DE EMPLEADOS Y OBREROS GASTRONOMICOS DE TUCUM',NULL,NULL,NULL),(271,'124407','O.S. PARA EL PERSONAL DE EMPRESA DE LIMPIEZA, SERV',NULL,NULL,NULL),(272,'124506','O.S. DE LOS TRABAJADORES DE LA INDUSTRIA DEL GAS',NULL,NULL,NULL),(273,'124704','O.S. DE TRABAJADORES DE LA INDUSTRIA AVICOLA Y AFI',NULL,NULL,NULL),(274,'124803','O.S. DE ARBITROS DE LA ASOCIACION DEL FUTBOL ARGEN',NULL,NULL,NULL),(275,'124902','O.S. DE LA FEDERACION NACIONAL DE SINDICATOS DE CO',NULL,NULL,NULL),(276,'125004','O.S. YACIMIENTOS CARBONIFEROS',NULL,NULL,NULL),(277,'125103','O.S. DE LOS PROFESIONALES UNIVERSITARIOS DEL AGUA ',NULL,NULL,NULL),(278,'125202','O.S. DE CAPITANES, PILOTOS Y PATRONES DE PESCA ( V',NULL,NULL,NULL),(279,'125301','O.S. FEDERAL DE LA FEDERACION DE TRABAJADORES OBRA',NULL,NULL,NULL),(280,'125400','O.S.PARA EL PERSONAL DE OBRAS Y SERVICIOS SANITARI',NULL,NULL,NULL),(281,'125509','O.S. DE LA FEDERACION ARGENTINA DEL TRABAJADOR DE ',NULL,NULL,NULL),(282,'125608','O.S. DEL PERSONAL SUPERIOR DE LA INDUSTRIA DEL GAS',NULL,NULL,NULL),(283,'125707','O.S.UNION PERSONAL DE LA UNION DE TRABAJADORES CIV',NULL,NULL,NULL),(284,'125806','O.S. DEL PERSONAL JERARQUICO DEL AGUA Y LA ENERGIA',NULL,NULL,NULL),(285,'125905','O.S. ARBITROS DEPORTIVOS DE LA REPUBLICA ARGENTINA',NULL,NULL,NULL),(286,'126007','O.S. DE LA FEDERACION GREMIAL DE LA INDUSTRIA DE L',NULL,NULL,NULL),(287,'126106','O.S. DE LOS TRABAJADORES DE LA EDUCACION PRIVADA',NULL,NULL,NULL),(288,'126205','O.S. DE LOS EMPLEADOS DE COMERCIO Y ACTIVIDADES CI',NULL,NULL,NULL),(289,'126304','O.S. BANCARIA ARGENTINA',NULL,NULL,NULL),(290,'126403','O.S. MEDICA AVELLANEDA',NULL,NULL,NULL),(291,'126502','O.S. DE LA CONFEDERACION DE OBREROS Y EMPLEADOS MU',NULL,NULL,NULL),(292,'126601','O.S. DEL PERSONAL DE INDUSTRIAS QUIMICAS Y PETROQU',NULL,NULL,NULL),(293,'126700','O.S. DEL PERSONAL DE LA ACTIVIDAD AZUCARERA TUCUMA',NULL,NULL,NULL),(294,'126809','O.S. DE CONDUCT.DE REMISES Y AUTOS AL INSTANTE Y A',NULL,NULL,NULL),(295,'126908','OBRA SOCIAL DE LOS MEDICOS DE LA CIUDAD DE BUENOS ',NULL,NULL,NULL),(296,'127000','OBRA SOCIAL DE TRABAJADORES DE ESTACIONES DE SERVI',NULL,NULL,NULL),(297,'127109','OBRA SOCIAL DEL PERSONAL DE TELECOMUNICACIONES SIN',NULL,NULL,NULL),(298,'127208','O.S. DE MANDOS MEDIOS DE TELECOMUNICACIONES EN LA ',NULL,NULL,NULL),(299,'127307','O.S. DE TRABAJADORES VIALES Y AFINES DE LA REPUBLI',NULL,NULL,NULL),(300,'127406','O.S. DE OBREROS Y EMPLEADOS TINTOREROS, SOMBRERERO',NULL,NULL,NULL),(301,'127505','O.S. DE EMPLEADOS DE FARMACIA',NULL,NULL,NULL),(302,'200103','O.S. DE LA SECRETARIA DE AGRICULTURA, GANADERIA Y ',NULL,NULL,NULL),(303,'200202','O.S. CENTRO REGIONAL DE AGUAS SUBTERRANEAS',NULL,NULL,NULL),(304,'200301','OBRA SOCIAL DE LAS SECRETARIAS DE INDUSTRIA Y COME',NULL,NULL,NULL),(305,'200400','O.S.DEL PERSONAL DE LA EMPRESA NACIONAL DE CORREOS',NULL,NULL,NULL),(306,'200509','O.S. DEL MINISTERIO DE DEFENSA',NULL,NULL,NULL),(307,'200608','O.S. FLOTA FLUVIAL DEL ESTADO ARGENTINO',NULL,NULL,NULL),(308,'200707','O.S. GAS DEL ESTADO',NULL,NULL,NULL),(309,'200806','DIRECCION GENERAL DE O.S. DEL MINISTERIO DEL INTER',NULL,NULL,NULL),(310,'200905','O.S. DEL MINISTERIO DE JUSTICIA',NULL,NULL,NULL),(311,'201007','OS DE EMPRESA LINEAS MARITIMAS ARGENTINAS',NULL,NULL,NULL),(312,'201106','DIRECCION GENERAL DE O.S. DEL MINISTERIO DE RELACI',NULL,NULL,NULL),(313,'201205','O.S. DEL MINISTERIO DE OBRAS Y SERVICIOS PUBLICOS',NULL,NULL,NULL),(314,'201304','DIRECCION DE O.S. DE LA EMPRESA NACIONAL DE TELECO',NULL,NULL,NULL),(315,'201403','O.S. DE LA UNIVERSIDAD DE BUENOS ARIES',NULL,NULL,NULL),(316,'201502','O.S. DIRECCION NACIONAL DE VIALIDAD',NULL,NULL,NULL),(317,'201700','O.S. YACIMIENTOS CARBONIFEROS FISCALES',NULL,NULL,NULL),(318,'201809','O.S. YACIMIENTOS PETROLIFEROS FISCALES',NULL,NULL,NULL),(319,'201908','INSTITUTO NACIONAL DE OBRAS SOCIALES PARA EL PERSO',NULL,NULL,NULL),(320,'300100','O.S. ANILSUD',NULL,NULL,NULL),(321,'300308','O.S. BOROQUIMICA S.A.M.I.C.A.F.',NULL,NULL,NULL),(322,'300407','O.S. DE LA EMPRESA PRIVADA CELULOSA ARGENTINA S.A.',NULL,NULL,NULL),(323,'300506','O.S. CORPORACION CEMENTERA ARGENTINA',NULL,NULL,NULL),(324,'300605','O.S. DEL PERSONAL DE CERAMICA SAN LORENZO',NULL,NULL,NULL),(325,'300704','O.S. CERAS JOHNSON',NULL,NULL,NULL),(326,'300803','O.S. CALILEGUA S.A.A.I.C.',NULL,NULL,NULL),(327,'301004','O.S. DESTILERIAS SAN IGNACIO S.A.I.C.',NULL,NULL,NULL),(328,'301103','O.S. DUNLOP ARGENTINA LIMITADA',NULL,NULL,NULL),(329,'301202','O.S. DUPERIAL ORBEA',NULL,NULL,NULL),(330,'301301','O.S. COMPAÑÍA EMBOTELLADORA ARGENTINA',NULL,NULL,NULL),(331,'301400','O.S. ELECTROCLOR S.C.A.',NULL,NULL,NULL),(332,'301509','O.S. MUTUALIDAD EMPLEADOS FIRESTONE',NULL,NULL,NULL),(333,'301608','O.S. AUTOLATINA ARGENTINA S.A.',NULL,NULL,NULL),(334,'301707','O.S. DE ALLIED DOMECD ARGENTINA. S.A (ANTERIOR DEN',NULL,NULL,NULL),(335,'301806','O.S. SOCIEDAD MINERA HIERRO PATAGONICOS DE SIERRA ',NULL,NULL,NULL),(336,'301905','O.S. DE SERVICIOS ASISTENCIALES DE LA COMPAÑÍA ITA',NULL,NULL,NULL),(337,'302007','O.S. INGENIO RIO GRANDE S.A.',NULL,NULL,NULL),(338,'302106','O.S. PARA DIRECTIVOS, TECNICOS Y EMPLEADOS DE JOHN',NULL,NULL,NULL),(339,'302205','O.S. LEDESMA S.A.A.I.',NULL,NULL,NULL),(340,'302304','O.S. COMPAÑÍA MINERA AGUILAR S.A.',NULL,NULL,NULL),(341,'302403','O.S. MOLINOS RIO DE LA PLATA',NULL,NULL,NULL),(342,'302502','O.S. PASA PETROQUIMICA ARGENTINA S.A.',NULL,NULL,NULL),(343,'302601','O.S. COOPERATIVA DE ASISTENCIA MUTUA Y TURISMO DEL',NULL,NULL,NULL),(344,'302700','O.S. COMPAÑÍA QUIMICA S.A.',NULL,NULL,NULL),(345,'302809','O.S. REFINERIAS DE MAIZ S.A.I.C.F.',NULL,NULL,NULL),(346,'302908','DIRECCION GENERAL DE SERVICIOS ASISTENCIALES DE SE',NULL,NULL,NULL),(347,'303000','O.S. SULFACID S.A.I.F.Y C.',NULL,NULL,NULL),(348,'303109','O.S. SUPERCO',NULL,NULL,NULL),(349,'303208','O.S. EMPRESA PRIVADA WITCEL S.A.',NULL,NULL,NULL),(350,'303307','O.S. CABOT ARGENTINA',NULL,NULL,NULL),(351,'303406','O.S. DEL PERSONAL DE SHELL - CAPSA',NULL,NULL,NULL),(352,'303505','O.S. DEL PERSONAL DE LA COMPANIA GENERAL DE COMBUS',NULL,NULL,NULL),(353,'303604','O.S. DE IPAKO S.A.',NULL,NULL,NULL),(354,'303703','O.S. DEL PERSONAL DE PBBPOLISUR DE BAHIA BLANCA',NULL,NULL,NULL),(355,'303802','O.S. DE ARBITRO DEL FUTBOL ARGENTINO',NULL,NULL,NULL),(356,'303901','O.S. DE FORD ARGENTINA S.A.',NULL,NULL,NULL),(357,'304003','O.S. DE VOLKSWAGEN ARGENTINA S.A.',NULL,NULL,NULL),(358,'400107','O.S. DEL PERSONAL DE DIRECCION DE LAS EMPRESAS DE ',NULL,NULL,NULL),(359,'400206','O.S. DEL PERSONAL DE DIRECCION DE LA INDUSTRIA AUT',NULL,NULL,NULL),(360,'400305','O.S. COOPERATIVA LIMITADA DE ASISTENCIA MEDICA, FA',NULL,NULL,NULL),(361,'400404','O.S. DEL PERSONAL DE DIRECCION DE LA INDUSTRIA CER',NULL,NULL,NULL),(362,'400503','O.S. ASOCIACION DEL PERSONAL DE DIRECCION Y JERERQ',NULL,NULL,NULL),(363,'400602','O.S. DEL PERSONAL DIRECTIVO DE LA INDUSTRIA DE LA ',NULL,NULL,NULL),(364,'400701','O.S. CAMARA DE LA INDUSTRIA CURTIDORA ARGENTINA',NULL,NULL,NULL),(365,'400800','O.S. DE EJECUTIVOS Y DEL PERSONAL DE DIRECCION DE ',NULL,NULL,NULL),(366,'400909','O.S. ACCION SOCIAL DE EMPRESARIOS',NULL,NULL,NULL),(367,'401001','O.S. DEL PERSONAL DE DIRECCION DE LAS EMPRESAS QUE',NULL,NULL,NULL),(368,'401100','O.S. DEL PERSONAL DE DIRECCION ALFREDO FORTABAT',NULL,NULL,NULL),(369,'401209','O.S. DEL PERSONAL DE DIRECCION DE LA INDUSTRIA MET',NULL,NULL,NULL),(370,'401308','O.S. PARA EL PERSONAL DE DIRECCION DE LA INDUSTRIA',NULL,NULL,NULL),(371,'401407','O.S. PARA EL PERSONAL DE DIRECCION DE LA ACTIVIDAD',NULL,NULL,NULL),(372,'401506','ASOCIACION DE PRESTACIONES SOCIALES PARA EMPRESARI',NULL,NULL,NULL),(373,'401605','O.S. DEL PERSONAL DE DIRECCION DE LA INDUSTRIA PRI',NULL,NULL,NULL),(374,'401704','O.S. DE DIRECTIVOS Y EMPRESARIOS PEQUEÑOS Y MEDIAN',NULL,NULL,NULL),(375,'401803','O.S. ASOCIACION MUTUAL DEL PERSONAL DE PHILIPS ARG',NULL,NULL,NULL),(376,'401902','O.S. DEL PERSONAL DE DIRECCION DE PERFUMERIAS E. W',NULL,NULL,NULL),(377,'402004','O.S. DEL PERSONAL DE DIRECCION DE LA SANIDAD LUIS ',NULL,NULL,NULL),(378,'402103','O.S. ASOCIACION DEL PERSONAL DE DIRECCION DE LA IN',NULL,NULL,NULL),(379,'402202','O.S. MUTUALIDAD INDUSTRIAL TEXTIL ARGENTINA',NULL,NULL,NULL),(380,'402301','O.S. ASOCIACION DEL PERSONAL SUPERIOR DE LA ORGANI',NULL,NULL,NULL),(381,'402400','O.S. PARA EL PERSONAL DE DIRECCION DE LA INDUTRIA ',NULL,NULL,NULL),(382,'402509','O.S. YPF',NULL,NULL,NULL),(383,'402608','O.S. ASOCIACION DE SERVICIOS SOCIALES PARA EMPRESA',NULL,NULL,NULL),(384,'402707','O.S. DE DIRECCION OSDO',NULL,NULL,NULL),(385,'402806','O.S. DE DIRECCION DE LA ACTIVIDAD AEROCOMERCIAL PR',NULL,NULL,NULL),(386,'402905','O.S. DE DIRECCION WITCEL',NULL,NULL,NULL),(387,'500104','INSTITUTO DE SERVICIOS SOCIALES BANCARIOS',NULL,NULL,NULL),(388,'500203','INSTITUTO DE OBRA SOCIAL',NULL,NULL,NULL),(389,'500302','INSTITUTO DE SERVICIOS SOCIALES PARA EL PERSONAL D',NULL,NULL,NULL),(390,'500401','O.S. PARA EMPLEADOS DE COMERCIO Y ACTIVIDADES CIVI',NULL,NULL,NULL),(391,'500500','O.S. PARA LA ACTIVIDAD DOCENTE',NULL,NULL,NULL),(392,'500609','INSTITUTO DE SERVICIOS SOCIALES PARA EL PERSONAL F',NULL,NULL,NULL),(393,'500708','INSTITUTO DE OS PARA EL PERSONAL DEL MINISTERIO DE',NULL,NULL,NULL),(394,'500807','INSTITUTO NACIONAL DE SERVICIOS SOCIALES PARA JUBI',NULL,NULL,NULL),(395,'500906','O.S. PARA EL PERSONAL DE OBRAS SANITARIAS DE LA NA',NULL,NULL,NULL),(396,'501107','INSTITUTO DE SERVICIOS SOCIALES PARA LAS ACTIVIDAD',NULL,NULL,NULL),(397,'501206','INSTITUTO DE SERVICIOS SOCIALES PARA EL PERSONAL D',NULL,NULL,NULL),(398,'501404','INSTITUTO DE SERVICIOS SOCIALES PARA EL TERRITORIO',NULL,NULL,NULL),(399,'501503','INSTITUTO DE SERVICIOS SOCIALES PARA EL PERSONAL D',NULL,NULL,NULL),(400,'600200','ASOCIACIÓN REGIONAL BARILOCHE DE OBRAS SOCIALES',NULL,NULL,NULL),(401,'600309','ASOCIACIÓN DE OBRAS SOCIALES DE BELLA VISTA',NULL,NULL,NULL),(402,'600408','ASOCIACIÓN DE OBRAS SOCIALES DE COMODORO RIVADAVIA',NULL,NULL,NULL),(403,'602305','ASOCIACIÓN DE OBRAS SOCIALES DE ROSARIO',NULL,NULL,NULL),(404,'602701','ASOCIACIÓN DE OBRAS SOCIALES DE SAN JUAN',NULL,NULL,NULL),(405,'603100','ASOCIACIÓN DE OBRAS SOCIALES DE TRELEW',NULL,NULL,NULL),(406,'700108','O.S. DEL PERSONAL MUNICIPAL DE AVELLANEDA',NULL,NULL,NULL),(407,'700801','O.S. DEL PERSONAL MUNICIPAL DE LA MATANZA',NULL,NULL,NULL),(408,'701002','O.S. DEL PERSONAL MUNICIPAL DE TRES DE FEBRERO',NULL,NULL,NULL),(409,'701101','O.S. DEL PERSONAL MUNICIPAL DE SANTIAGO DEL ESTERO',NULL,NULL,NULL),(410,'701200','O.S. TRABAJADORES MUNICIPALES DE GRAL. PUEYRREDON',NULL,NULL,NULL),(411,'800105','O.S. ATANOR S.A. MIXTA',NULL,NULL,NULL),(412,'800204','O.S. FORJA ARGENTINA S.A.I.C.',NULL,NULL,NULL),(413,'800303','O.S. PAPEL MISIONERO S.A.I.F. Y C.',NULL,NULL,NULL),(414,'800402','O.S. PETROQUIMICA GENERAL MOSCÓN',NULL,NULL,NULL),(415,'800501','O.S. DE ACEROS PARANA',NULL,NULL,NULL),(416,'900102','O.S. FIAT',NULL,NULL,NULL);
/*Parametrizar rrhh_concepto_obra_social */
/************************/


UPDATE persona SET fecha_antiguedad = fecha_ingreso WHERE id_tipo_persona = 8;
ALTER TABLE `persona` CHANGE `fecha_antiguedad` `fecha_antiguedad` DATE NULL; 

INSERT INTO `rrhh_lugar_pago` (`id`, `d_rrhh_lugar_pago`, `calle`, `numero_calle`, `codigo_postal`) VALUES ('2', 'EBSA', '123', '123', '1111'); 

update persona set id_rrhh_lugar_pago = 2 where id_tipo_persona = 8;
ALTER TABLE `tabla_auxiliar_item` ADD COLUMN `id` BIGINT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`); 

UPDATE `tipo_comprobante` SET `impresion_footer_left` = 'Recibí de conformidad de ENERBOM S.A el importe de esta liquidacion en pago de mi remuneracion correspondiente al periodo indicado y la copia de este recibo' WHERE `id` = '701'; 

ALTER TABLE `tipo_comprobante` ADD COLUMN `imagen1` VARCHAR(50) NULL AFTER `id_cuenta_bancaria`; 

CREATE TABLE `rrhh_forma_pago`( `id` INT NOT NULL AUTO_INCREMENT, `d_rrhh_forma_pago` VARCHAR(50) NOT NULL, PRIMARY KEY (`id`) ); 

INSERT INTO `rrhh_forma_pago` (`d_rrhh_forma_pago`) VALUES ('CAJA DE AHORRO'); 
INSERT INTO `rrhh_forma_pago` (`d_rrhh_forma_pago`) VALUES ('CHEQUE'); 
INSERT INTO `rrhh_forma_pago` (`d_rrhh_forma_pago`) VALUES ('EFECTIVO'); 
UPDATE `rrhh_forma_pago` SET `d_rrhh_forma_pago` = 'DEPOSITO BANCARIO' WHERE `id` = '1'; 

ALTER TABLE `persona` ADD COLUMN `id_rrhh_forma_pago` INT NULL AFTER `id_rrhh_sindicato`; 
CREATE TABLE `rrhh_modalidad_contratacion`( `id` INT NOT NULL AUTO_INCREMENT, `codigo` INT NOT NULL, `d_rrhh_modalidad_contratacion` VARCHAR(100) NOT NULL, `tasa` DECIMAL DEFAULT 0, `activo` TINYINT DEFAULT 1, PRIMARY KEY (`id`) ); 
insert  into `rrhh_modalidad_contratacion`(`id`,`codigo`,`d_rrhh_modalidad_contratacion`,`tasa`,`activo`) values (1,0,'Contrato Modalidad Promovida. Reducción 0%','0',1),(2,1,'A tiempo parcial: Indeterminado/Permanente','0',1),(3,2,'Becarios - Residencias médicas L.22127','0',1),(4,3,'De aprendizaje L.25013','0',1),(5,4,'Especial de fomento del empleo L.24465','0',1),(6,5,'Fomento del empleo. L.24013 y L.24465','0',1),(7,6,'Lanzamiento nueva actividad. L.24013 y L.24465','0',1),(8,7,'Período de prueba. L.24465 y L.25013','0',1),(9,8,'A tiempo completo:Indeterminado/Trabajo Permanente','0',1),(10,9,'Práctica laboral para jóvenes','0',1),(11,10,'Pasantías - Sin Obra Obra Social','0',1),(12,11,'Trabajo de temporada','0',1),(13,12,'Trabajo eventual','0',1),(14,13,'Trabajo formación','0',1),(15,14,'Nuevo período de prueba','0',1),(16,15,'Pues Nue V y M de 25 a 44 L.25250','0',1),(17,16,'Nvo Periodo Prueba Trab Discapacit Art.34 L.24147','0',1),(18,17,'Pues Nue men 25, VyM 45 o más y M Jefe de Fam','0',1),(19,18,'Trab Discapacit Art.34 L.24147','0',1),(20,19,'Pto Nvo V y M 25 a 44 Art.34 L.24147 L.25250','0',1),(21,20,'Pto Nvo men 25, VyM 45 o más y M J/F Art34 L24147','0',1),(22,21,'A tiempo parcial determinado','0',1),(23,22,'A tiempo completo determinado','0',1),(24,23,'Psonal no permanente L.22248','0',1),(25,24,'Psonal de la Construccion L.22250','0',1),(26,25,'Empleo Público provincial','0',1),(27,26,'Benefic de Prog de Empleo, Capacita y Recup Produ','0',1),(28,27,'Pasantías L.26427 -con obra social-','0',1),(29,28,'Programas Jefes y Jefas de hogar','0',1),(30,29,'Decreto Nro.1212/03 - Aportante Autónomo','0',1),(31,30,'Nvo Per. Prueba. Trab. Discapacit. Art.87 L.24013','0',1),(32,31,'Trabajador Discapacitado Art.87 L.24013','0',1),(33,32,'Per. Prueba 6 L.25877','0',1),(34,33,'Per. Prueba 6 L.25877 Benefic. Planes Jefes/as','0',1),(35,34,'Per. Prueba 6 L.25877 Art.34 Ley 24147','0',1),(36,35,'Per. Prueba 6 L.25877 A.34 L.24147 Benefic Planes','0',1),(37,36,'Per. Pru 6 L.25877 Trab.Disc. A.87 L24013','0',1),(38,37,'Per. Pru 6 L.25877 Trab.Disc. A.87 L24013 Ben.Plan','0',1),(39,38,'Pto Nvo 6 L.25877','0',1),(40,39,'Pto Nvo 6 L.25877 Benefic. Planes Jefes/as','0',1),(41,40,'Pto Nvo 6 L.25877 Art.34 Ley 24147','0',1),(42,41,'Pto Nvo 6 L.25877 A.34 L.24147 Benef.Planes','0',1),(43,42,'Pto Nvo 6 L.25877 Trab.Disc. A87 L24013','0',1),(44,43,'Pto Nvo 6 L.25877 Trab.Disc. A87 L24013 Benef.Plan','0',1),(45,44,'Changa Solidaria CCT 62/75','0',1),(46,45,'Psonal no Perm. Hoteles CCT 362/03 Art.68 Inc.B','0',1),(47,46,'Planta transit. Adm Publica Nac., Prov. y/o Munic.','0',1),(48,47,'Representación gramial','0',1),(49,48,'Art.4 L.24241. Traslado temp Ext o Conv Bilat.','0',1),(50,49,'Directores - Empleados SA con OS y LTR','0',1),(51,50,'Contrato modalidad promovida. Reducción 50%','50',1),(52,51,'Pasantías L.26427 - c/OS - Benef. Pensión Disca.','0',1),(53,95,'Planes Ministerio de Trabajo','0',1),(54,96,'Plan Trabajo por San Luis L.5411/03','0',1),(55,97,'Programa Trabajo para Jovenes tucumanos','0',1),(56,98,'BONUS 2da Oportunidad','0',1),(57,99,'LRT (Directores SA, municipios, org, cent y ...','0',1),(58,100,'Contrato modalidad promovida. Reducción 100%','100',1),(59,102,'Personal permanente discontínuo con ART (para uso de la EU) Decreto Nº 762/14.','0',1),(60,110,'Trabajo permanente prestación continua Ley 26727','0',1),(61,111,'Trabajo temporario Ley 26727','0',1),(62,112,'Trabajo permanente discontinuo ley 26727','0',1),(63,113,'Trabajo por equipo o cuadrilla fafmilar Ley 26727','0',1),(64,114,'Trabajo temporario con Reducción Ley 26727','0',1),(65,115,'Trabajo permanente discontinuo con Reducción ley 26727','0',1),(66,201,'Beneficio primeros 12 meses.','0',1),(67,202,'Trabajador discapacitado art.34 L.24147 beneficio primeros 12 meses.','0',1),(68,203,'Trabajador discapacitado art.87 L.24013 beneficio primeros 12 meses.','0',1),(69,204,'P.Nuevo art.16 Ley 26476 2º año','25',1),(70,205,'P.Nuevo art.16 Ley 26476 Trab.disc art.34 Ley 24147 2º año','25',1),(71,206,'P.Nuevo art.16 Ley 26476 Trab.disc art.87 Ley 24013 2º año','25',1),(72,301,'Tiempo indeterminado','0',1),(73,302,'Trabajador discapacitado art. 34 L 24147. Tiempo indeterminado','0',1),(74,303,'Trabajador discapacitado art. 87 L 24013. Tiempo indeterminado','0',1),(75,304,'Tiempo parcial. Art. 92 ter LCT','0',1),(76,305,'Trabajador discapacitado art. 34 L 24147. Tiempo parcial. Art. 92 ter LCT','0',1),(77,306,'Trabajador discapacitado art. 87 L 24013. Tiempo parcial. Art. 92 ter LCT','0',1),(78,307,'Hasta 15 empleados. Primeros 12 meses. Tiempo indeterminado','0',1),(79,308,'Hasta 15 empleados. Primeros 12 meses. Tiempo indeterminado/Trabajador discapacitado art. 34 Ley 241','0',1),(80,309,'Hasta 15 empleados. Primeros 12 meses. Tiempo indeterminado/Trabajador discapacitado art. 87 Ley 240','0',1),(81,310,'Hasta 15 empleados. Segundos 12 meses. Tiempo indeterminado','0',1),(82,311,'Hasta 15 empleados. Segundos 12 meses. Tiempo indeterminado. Trabajador discapacitado art. 34 Ley 24','0',1),(83,312,'Hasta 15 empleados. Segundos 12 meses. Tiempo indeterminado. Trabajador discapacitado art. 87 Ley 24','0',1),(84,313,'16 a 80 empleados. Tiempo indeterminado','0',1),(85,314,'16 a 80 empleados. Tiempo indeterminado. Trabajador discapacitado art. 34 Ley 24147','0',1),(86,315,'16 a 80 empleados. Tiempo indeterminado. Trabajador discapacitado art. 87 Ley 24013','0',1),(87,985,'CCG Vitivinícola de Neuquén','0',1),(88,987,'CCG Vitivinícola de La Rioja','0',1),(89,989,'CCG Tabaco Salta','0',1),(90,990,'CCG Tabaco Jujuy','0',1),(91,991,'CCG vitivinícola de Río Negro','0',1),(92,992,'CCG multiproducto del Chaco','0',1),(93,993,'CCG frutihortícola de San Juan','0',1),(94,994,'CCG Yerba Mate Misiones y Corrientes','0',1),(95,995,'CCG vitivinícola de San Juan','0',1),(96,996,'CCG vitivinícola de Mendoza','0',1),(97,997,'CCG forestal del Chaco','0',1),(98,998,'CCG tabaco Virginia del Chaco','0',1),(99,999,'CCG Art 11 Ley 26476 Trabajador registrado Rectificacion real fecha de inicio','0',1);

ALTER TABLE `tipo_comprobante` CHANGE `imagen1` `imagen1` VARCHAR(500) CHARSET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE `tipo_comprobante` ADD COLUMN `imagen2` VARCHAR(500) NULL AFTER `imagen1`; 
no debe ser vacio chk_aplica_todo_empleado
el combo search debe aceptar filtros, por ejemplo activo en conceptos.
el multiple de obra social debe tener el codigo o debe permitir buscar




CREATE TABLE `rrhh_F931_concepto_afip`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_concepto` VARCHAR(200) COLLATE latin1_swedish_ci NOT NULL  , 
	`codigo` INT(11) NOT NULL  , 
	`f931_tipo_concepto` INT(11) NOT NULL  , 
	`ful_codigo` INT(11) NULL  , 
	PRIMARY KEY (`id`) , 
	UNIQUE KEY `codigo`(`codigo`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';
CREATE TABLE `rrhh_F931_tipo_concepto`( `id` INT NOT NULL AUTO_INCREMENT, `d_rrhh_F931_tipo_concepto` VARCHAR(30), PRIMARY KEY (`id`) ); 

INSERT INTO `rrhh_F931_tipo_concepto` (`id`, `d_rrhh_F931_tipo_concepto`) VALUES ('1', 'REMUNERATIVO'); 
INSERT INTO `rrhh_F931_tipo_concepto` (`id`, `d_rrhh_F931_tipo_concepto`) VALUES ('2', 'NO REMUNERATIVO'); 
INSERT INTO `rrhh_F931_tipo_concepto` (`id`, `d_rrhh_F931_tipo_concepto`) VALUES ('3', 'DESCUENTO'); 


CREATE TABLE `rrhh_F931_bases_calculo`( `id` INT NOT NULL AUTO_INCREMENT, `id_F931_tipo_concepto` INT NOT NULL, `d_rrhh_F931_bases_calculo` VARCHAR(200) NOT NULL, PRIMARY KEY (`id`) );
CREATE TABLE `rrhh_F931_tipo_liquidacion`( `id` INT NOT NULL AUTO_INCREMENT, `codigo` VARCHAR(10) NOT NULL, `d_rrhh_F931_tipo_liquidacion` VARCHAR(100) NOT NULL, PRIMARY KEY (`id`) ); 


INSERT INTO `rrhh_F931_tipo_liquidacion` (`codigo`, `d_rrhh_F931_tipo_liquidacion`) VALUES ('M', 'Mensual'); 
 INSERT INTO`rrhh_F931_tipo_liquidacion` (`codigo`, `d_rrhh_F931_tipo_liquidacion`) VALUES ('Q', 'Quincenal'); 
INSERT INTO `rrhh_F931_tipo_liquidacion` (`codigo`, `d_rrhh_F931_tipo_liquidacion`) VALUES ('S', 'Semanal'); 


CREATE TABLE `rrhh_F931_concepto_afip_uso_libre`( `id` INT NOT NULL AUTO_INCREMENT, `f931_tipo_concepto` INT NOT NULL, `codigo_desde` INT NOT NULL, `codigo_hasta` INT NOT NULL, PRIMARY KEY (`id`) ); 
ALTER TABLE `rrhh_F931_concepto_afip_uso_libre` ADD COLUMN `codigo` INT NULL AFTER `id`; 
ALTER TABLE `rrhh_F931_concepto_afip_uso_libre` CHANGE `codigo` `codigo` INT(11) NOT NULL; 
ALTER TABLE `rrhh_F931_concepto_afip_uso_libre` CHANGE `f931_tipo_concepto` `id_f931_tipo_concepto` INT(11) NOT NULL; 
ALTER TABLE `rrhh_F931_concepto_afip_uso_libre` ADD COLUMN `d_rrhh_F931_concepto_afip_uso_libre` VARCHAR(100) NOT NULL AFTER `id`; 
RENAME TABLE `rrhh_F931_bases_calculo` TO `rrhh_F931_bases_calculo_afip`; 


CREATE TABLE `rrhh_F931_bases_calculo_afip_default`( `id` INT NOT NULL AUTO_INCREMENT, `codigo_desde` INT NOT NULL, `codigo_hasta` INT NOT NULL, `id_rrhh_F931_bases_calculo_afip` INT NOT NULL, `estado` INT NOT NULL, `modificado` INT NOT NULL DEFAULT 0, PRIMARY KEY (`id`) ); 
ALTER TABLE `rrhh_F931_bases_calculo_afip_default` ADD COLUMN `codigo` INT NOT NULL AFTER `id`;
CREATE TABLE `rrhh_concepto_F931_concepto_afip`( `id` INT NOT NULL AUTO_INCREMENT, `id_rrhh_concepto` INT NOT NULL, `id_rrhh_F931_concepto_afip` INT NOT NULL, PRIMARY KEY (`id`) );


insert  into `rrhh_F931_bases_calculo_afip`(`id`,`id_F931_tipo_concepto`,`d_rrhh_F931_bases_calculo`) values (1,1,'Sistema Previsional Argentino - SIPA - Aporte'),(2,1,'Sistema Previsional Argentino - SIPA - Contribuciones'),(3,1,'INSSJyP - Aporte'),(4,1,'INSSJyP- Contribuciones'),(5,1,'Obra Social - Aporte'),(6,1,'Obra Social - Contribuciones'),(7,1,'Fondo Solidario de Redistribución (ex ANSSAL) - Aporte'),(8,1,'Fondo Solidario de Redistribución (ex ANSSAL) - Contribuciones'),(9,1,'RENATEA (ex RENATRE) - Aporte'),(10,1,'RENATEA (ex RENATRE) - Contribuciones'),(12,1,'Asignaciones Familiares - Contribuciones'),(14,1,'Fondo Nacional de Empleo - Contribuciones'),(16,1,'Ley de Riesgos del Trabajo - Contribuciones'),(17,1,'Seguro Colectivo de Vida Obligatorio - Aporte'),(18,1,'Seguro Colectivo de Vida Obligatorio - Contribuciones'),(19,1,'Regímenes Diferenciales - Aporte'),(21,1,'Regímenes Especiales - Aporte');

/*Data for the table `rrhh_F931_bases_calculo_afip_default` */

insert  into `rrhh_F931_bases_calculo_afip_default`(`id`,`codigo`,`codigo_desde`,`codigo_hasta`,`id_rrhh_F931_bases_calculo_afip`,`estado`,`modificado`) values (158,0,110000,499999,1,1,0),(159,0,500000,519999,1,0,1),(160,0,520000,529000,1,0,0),(161,0,530000,539000,1,0,0),(162,0,540000,549000,1,0,0),(163,0,550000,559999,1,0,1),(164,0,799999,820000,1,0,0),(165,0,820001,821000,1,0,0),(166,0,110000,499999,2,1,0),(167,0,500000,519999,2,0,1),(168,0,520000,529000,2,0,0),(169,0,530000,539000,2,0,0),(170,0,540000,549000,2,0,0),(171,0,550000,559999,2,0,1),(172,0,799999,820000,2,0,0),(173,0,820001,821000,2,0,0),(174,0,110000,499999,3,1,0),(175,0,500000,519999,3,0,1),(176,0,520000,529000,3,0,0),(177,0,530000,539000,3,0,0),(178,0,540000,549000,3,0,0),(179,0,550000,559999,3,0,1),(180,0,799999,820000,3,0,0),(181,0,820001,821000,3,0,0),(182,0,110000,499999,4,1,0),(183,0,500000,519999,4,0,1),(184,0,520000,529000,4,0,0),(185,0,530000,539000,4,0,0),(186,0,540000,549000,4,0,0),(187,0,550000,559999,4,0,1),(188,0,799999,820000,4,0,0),(189,0,820001,821000,4,0,0),(190,0,110000,499999,5,1,0),(191,0,500000,519999,5,0,1),(192,0,520000,529000,5,0,0),(193,0,530000,539000,5,1,0),(194,0,540000,549000,5,1,0),(195,0,550000,559999,5,0,1),(196,0,799999,820000,5,0,0),(197,0,820001,821000,5,0,0),(198,0,110000,499999,6,1,0),(199,0,500000,519999,6,0,1),(200,0,520000,529000,6,0,0),(201,0,530000,539000,6,0,0),(202,0,540000,549000,6,1,0),(203,0,550000,559999,6,0,1),(204,0,799999,820000,6,0,0),(205,0,820001,821000,6,0,0),(206,0,110000,499999,7,1,0),(207,0,500000,519999,7,0,1),(208,0,520000,529000,7,0,0),(209,0,530000,539000,7,1,0),(210,0,540000,549000,7,1,0),(211,0,550000,559999,7,0,1),(212,0,799999,820000,7,0,0),(213,0,820001,821000,7,0,0),(214,0,110000,499999,8,1,0),(215,0,500000,519999,8,0,1),(216,0,520000,529000,8,0,0),(217,0,530000,539000,8,0,0),(218,0,540000,549000,8,1,0),(219,0,550000,559999,8,0,1),(220,0,799999,820000,8,0,0),(221,0,820001,821000,8,0,0),(222,0,110000,499999,9,1,0),(223,0,500000,519999,9,0,1),(224,0,520000,529000,9,0,0),(225,0,530000,539000,9,0,0),(226,0,540000,549000,9,0,0),(227,0,550000,559999,9,0,1),(228,0,799999,820000,9,0,0),(229,0,820001,821000,9,0,0),(230,0,110000,499999,10,1,0),(231,0,500000,519999,10,0,1),(232,0,520000,529000,10,0,0),(233,0,530000,539000,10,0,0),(234,0,540000,549000,10,0,0),(235,0,550000,559999,10,0,1),(236,0,799999,820000,10,0,0),(237,0,820001,821000,10,0,0),(238,0,110000,499999,12,1,0),(239,0,500000,519999,12,0,1),(240,0,520000,529000,12,0,0),(241,0,530000,539000,12,0,0),(242,0,540000,549000,12,0,0),(243,0,550000,559999,12,0,1),(244,0,799999,820000,12,0,0),(245,0,820001,821000,12,0,0),(246,0,110000,499999,14,1,0),(247,0,500000,519999,14,0,1),(248,0,520000,529000,14,0,0),(249,0,530000,539000,14,0,0),(250,0,540000,549000,14,0,0),(251,0,550000,559999,14,0,1),(252,0,799999,820000,14,0,0),(253,0,820001,821000,14,0,0),(254,0,110000,499999,16,1,0),(255,0,500000,519999,16,0,1),(256,0,520000,529000,16,0,0),(257,0,530000,539000,16,0,0),(258,0,540000,549000,16,0,0),(259,0,550000,559999,16,0,1),(260,0,799999,820000,16,0,0),(261,0,820001,821000,16,0,0),(262,0,110000,499999,17,0,0),(263,0,500000,519999,17,0,1),(264,0,520000,529000,17,0,0),(265,0,530000,539000,17,0,0),(266,0,540000,549000,17,0,0),(267,0,550000,559999,17,0,1),(268,0,799999,820000,17,0,0),(269,0,820001,821000,17,0,0),(270,0,110000,499999,18,0,0),(271,0,500000,519999,18,0,1),(272,0,520000,529000,18,0,0),(273,0,530000,539000,18,0,0),(274,0,540000,549000,18,0,0),(275,0,550000,559999,18,0,1),(276,0,799999,820000,18,0,0),(277,0,820001,821000,18,0,0),(278,0,110000,499999,19,0,0),(279,0,500000,519999,19,0,1),(280,0,520000,529000,19,0,0),(281,0,530000,539000,19,0,0),(282,0,540000,549000,19,0,0),(283,0,550000,559999,19,0,1),(284,0,799999,820000,19,0,0),(285,0,820001,821000,19,0,0),(286,0,110000,499999,21,0,0),(287,0,500000,519999,21,0,1),(288,0,520000,529000,21,0,0),(289,0,530000,539000,21,0,0),(290,0,540000,549000,21,0,0),(291,0,550000,559999,21,0,1),(292,0,799999,820000,21,0,0),(293,0,820001,821000,21,0,0);

/*Data for the table `rrhh_F931_concepto_afip` */

insert  into `rrhh_F931_concepto_afip`(`id`,`d_rrhh_concepto`,`codigo`,`f931_tipo_concepto`,`ful_codigo`) values (1,'Sueldo',110000,1,0),(2,'Preaviso',110001,1,0),(3,'Remuneraciones en especie',110002,1,0),(4,'Comida',110003,1,0),(5,'Habitación',110004,1,0),(6,'Licencias por estudio',110005,1,0),(7,'Donación de sangre',110006,1,0),(8,'Feriado',110007,1,0),(9,'Prest. Dineraria Ley 24577 (primeros 10d)',110008,1,0),(10,'Prest. Dineraria Ley 24577 (a cargo de ART)',110009,1,0),(11,'Incremento Solidario. Dec. 14/2020',110011,1,0),(12,'Sueldo - De uso libre',111000,1,111),(13,'Sueldo - De uso libre',111001,1,111),(14,'Sueldo anual complementario',120000,1,0),(15,'SAC 1er semestre',120001,1,0),(16,'SAC 2do semestre',120002,1,0),(17,'SAC proporcional',120003,1,0),(18,'SAC - De uso libre',121000,1,121),(19,'Horas extras',130000,1,0),(20,'Horas extras al 50 %',130001,1,0),(21,'Horas extras al 100 %',130002,1,0),(22,'Horas extras al 200 %',130003,1,0),(23,'Zona desfavorable',140000,1,0),(24,'Adelanto vacacional',150000,1,0),(25,'Adicionales',160000,1,0),(26,'Adicional por antigüedad',160001,1,0),(27,'Adicional por título',160002,1,0),(28,'Adicional por tarea',160003,1,0),(29,'Adicional por desarraigo',160004,1,0),(30,'Gratificaciones y/o Premios',170000,1,0),(31,'Premio por presentismo',170001,1,0),(32,'Premio por producción',170002,1,0),(33,'Comisiones',170003,1,0),(34,'Accesorios',170004,1,0),(35,'Viáticos sin comprobante',170005,1,0),(36,'Propinas habituales no prohibidas',170006,1,0),(37,'Asignaciones Familiares',510000,2,0),(38,'Ayuda escolar',510001,2,0),(39,'Asignación por hijo/hijo con discapacidad',510002,2,0),(40,'Asignación por maternidad',510003,2,0),(41,'Asignación por maternidad down',510004,2,0),(42,'Asignación por matrimonio',510005,2,0),(43,'Asignación por nacimiento / adopción',510006,2,0),(44,'Asignación por prenatal',510007,2,0),(45,'Beneficios sociales',520000,2,0),(46,'Servicio de comedor',520001,2,0),(47,'Gastos médicos',520002,2,0),(48,'Provisión de ropa de trabajo',520003,2,0),(49,'Guardería',520004,2,0),(50,'Provisión de útiles escolares',520005,2,0),(51,'Gastos de sepelio',520006,2,0),(52,'Cursos de capacitación',520007,2,0),(53,'Becas',520008,2,0),(54,'Desempleo',520009,2,0),(55,'Gratificación por cese laboral',520010,2,0),(56,'Indemnización por extinción del contrato de trabajo',520011,2,0),(57,'Vacaciones no gozadas',520012,2,0),(58,'Incapacidad permanente',520013,2,0),(59,'Indemnización por Despido',520014,2,0),(60,'Indemnización Sustitutiva de Preaviso',520015,2,0),(61,'Integración del Mes de Despido',520016,2,0),(62,'SAC sobre integración o preaviso',520017,2,0),(63,'SAC sobre vacaciones no gozadas',520018,2,0),(64,'Incrementos no remunerativos (con aportes OS)',530000,2,0),(65,'Incrementos no remunerativos (con aportes y contribuciones OS)',540000,2,0),(66,'Importes no remunerativos especiales',550000,2,0),(67,'Redondeo (No Remunerativo)',799999,2,0),(68,'Sistema previsional',810000,3,0),(69,'INSSJyP',810001,3,0),(70,'Obra Social',810002,3,0),(71,'Fondo Solidario de Redistribución (ex ANSSAL)',810003,3,0),(72,'Cuota Sindical',810004,3,0),(73,'Seguro de Vida',810005,3,0),(74,'RENATEA (ex RENATRE)',810006,3,0),(75,'Préstamos',810007,3,0),(76,'Impuesto a las Ganancias',810008,3,0),(77,'Obra Social - Adherentes',810009,3,0),(78,'Fondo Solidario de Redistribución (Ex ANSSAL) - Adherentes',810010,3,0),(79,'Ajuste aporte Dec. 561/2019',810011,2,0),(80,'Otros descuentos',820000,3,0);

/*Data for the table `rrhh_F931_concepto_afip_uso_libre` */

insert  into `rrhh_F931_concepto_afip_uso_libre`(`id`,`d_rrhh_F931_concepto_afip_uso_libre`,`codigo`,`id_f931_tipo_concepto`,`codigo_desde`,`codigo_hasta`) values (1,'Sueldo - De uso libre',111,1,111000,119999),(2,'SAC - De uso libre',121,1,121000,129999),(3,'Horas extras - De uso libre',131,1,131000,139999),(4,'Zona desfavorable - De uso libre',141,1,141000,149999),(5,'Adelanto Vacacional - De uso libre',151,1,151000,159999),(6,'Adicionales - De uso libre',161,1,161000,169999),(7,'Gratificaciones y/o Premios - De Uso Libre',171,1,171000,179999),(8,'Asignaciones familiares - De uso libre',511,2,511000,519999),(9,'Beneficios Sociales - De uso libre',521,2,521000,529999),(10,'Incrementos no remunerativos (con aportes OS) - De uso libre',531,2,531000,539999),(11,'Incrementos no remunerativos (con aportes y contribuciones OS) - De uso libre',541,2,541000,549999),(12,'Importes no remunerativos especiales - De uso libre',551,2,551000,559000),(13,'Otros descuentos - De uso libre',821,3,821000,829999);


CREATE TABLE `rrhh_concepto_F931_base_calculo_afip`( `id` INT NOT NULL AUTO_INCREMENT, `id_rrhh_concepto` INT NOT NULL, `id_rrhh_F931_bases_calculo_afip` INT NOT NULL, PRIMARY KEY (`id`) ); 


ALTER TABLE `persona` ADD COLUMN `fecha_inicio_vacaciones` DATE NULL AFTER `id_rrhh_forma_pago`, ADD COLUMN `fecha_fin_vacaciones` DATE NULL AFTER `fecha_inicio_vacaciones`; 

ALTER TABLE `rrhh_variable` DROP COLUMN `calculo_sql`;

INSERT INTO `rrhh_variable` (`id`, `codigo`, `codigo_no_visible`) VALUES ('22', 'MES LIQUIDACION', 'MLI');
UPDATE `rrhh_variable` SET `codigo_no_visible` = 'MDL' WHERE `id` = '22'; 
UPDATE `rrhh_variable` SET `codigo` = 'MES DE LIQUIDACION' WHERE `id` = '22'; 
 INSERT INTO `rrhh_variable` (`id`, `codigo`, `codigo_no_visible`) VALUES ('23', 'MES DE GANANCIA', 'MDG'); 
INSERT INTO `rrhh_variable` (`id`, `codigo`, `codigo_no_visible`) VALUES ('24', 'DIAS DE LICENCIA', 'DLI'); 
INSERT INTO `rrhh_variable` (`id`, `codigo`, `codigo_no_visible`) VALUES ('25', 'DIAS DE LICENCIA EN EL PERIODO', 'DLP'); 
CREATE TABLE `rrhh_F931_configuracion`( `id` INT NOT NULL AUTO_INCREMENT, `diasbase` BIT, `parhijo` VARCHAR(3), `parcony` VARCHAR(3), `paradhe` VARCHAR(3), `tipoemp` VARCHAR(50), `cantdias` VARCHAR(4), `canthoras` VARCHAR(4), `apos` VARCHAR(4), `coos` VARCHAR(4), `baapos` VARCHAR(4), `bacoos` VARCHAR(4), `baLRT` VARCHAR(4), `remmat` VARCHAR(4), `paposs` VARCHAR(4), `pcontss` VARCHAR(4), `rembruta` VARCHAR(4), `BI1` VARCHAR(4), `BI2` VARCHAR(4), `BI3` VARCHAR(4), `BI4` VARCHAR(4), `BI5` VARCHAR(4), `BI6` VARCHAR(4), `BI7` VARCHAR(4), `BI8` VARCHAR(4), `BI9` VARCHAR(4), `BaseCalcDifASS` VARCHAR(4), `BaseCalcDifCSS` VARCHAR(4), `bi10` VARCHAR(4), `Detracciones` VARCHAR(4), `ProBaseImp` VARCHAR(4), PRIMARY KEY (`id`) ); 
ALTER TABLE `tipo_empleador` DROP PRIMARY KEY; 

ALTER TABLE `tipo_empleador` DROP PRIMARY KEY; 
insert  into `tipo_empleador`(`id`,`d_tipo_empleador`) values (0,'Administración Pública'),(1,'Decreto Nº 814/01 art 2 inc. b)'),(2,'Empresas Servicios eventuales. Decreto 814/01 art '),(3,'Provincias Y Otros'),(4,'Decreto 814/01 art 2 inc. a)'),(5,'Empresas Servicios eventuales. Decreto 814/01 art '),(7,'Enseñanza privada'),(8,'Decreto Nº 1212/03, exclusivamente AFA Y CLUBES'),(9,'Contribuyente NO SIPA');
ALTER TABLE  `rrhh_liquidacion` ADD COLUMN `numero_F931` INT NULL AFTER `legajo_hasta`; 
ALTER TABLE `rrhh_tipo_liquidacion` ADD COLUMN `id_rrhh_F931_tipo_liquidacion` INT NULL AFTER `activo`; 

CREATE TABLE rrhh_conjunto_concepto( `id` INT NOT NULL AUTO_INCREMENT, `codigo` VARCHAR(4) NOT NULL, `d_rrhh_conjunto_concepto` VARCHAR(50) NOT NULL, `activo` TINYINT NOT NULL, PRIMARY KEY (`id`) ); 

CREATE TABLE rrhh_conjunto_concepto_item( `id_rrhh_conjunto_concepto_item` INT NOT NULL, `id_rrhh_concepto` INT NOT NULL, FOREIGN KEY (`id_rrhh_conjunto_concepto_item`) REFERENCES rrhh_conjunto_concepto(`id`), FOREIGN KEY (`id_rrhh_concepto`) REFERENCES rrhh_concepto(`id`) ); 

ALTER TABLE `rrhh_conjunto_concepto_item` CHANGE `id_rrhh_conjunto_concepto_item` `id_rrhh_conjunto_concepto` INT NOT NULL; 
ALTER TABLE `rrhh_conjunto_concepto_item` ADD COLUMN `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`); 

ALTER TABLE `persona` ADD COLUMN `trabajador_convencionado` TINYINT NULL AFTER `fecha_fin_vacaciones`, ADD COLUMN `seguro_colectivo_vida` TINYINT NULL AFTER `trabajador_convencionado`; 
ALTER TABLE `persona` ADD COLUMN `id_rrhh_situacion_revista` INT NULL AFTER `seguro_colectivo_vida`, ADD COLUMN `rrhh_situacion_revista_dia_inicio` INT NULL AFTER `id_rrhh_situacion_revista`, ADD COLUMN `id_rrhh_situacion_revista2` INT NULL AFTER `rrhh_situacion_revista_dia_inicio`, ADD COLUMN `rrhh_situacion_revista_dia_inicio2` INT NULL AFTER `id_rrhh_situacion_revista2`, ADD COLUMN `id_rrhh_situacion_revista3` INT NULL AFTER `rrhh_situacion_revista_dia_inicio2`, ADD COLUMN `rrhh_situacion_revista_dia_inicio3` INT NULL AFTER `id_rrhh_situacion_revista3`; 


/* Alter table in target */
ALTER TABLE `agrupacion_asiento` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `habilitado` `habilitado` TINYINT(4)   NULL AFTER `observacion` , 
	CHANGE `id_estado_asiento_default` `id_estado_asiento_default` INT(11)   NULL AFTER `habilitado` ;

/* Alter table in target */
ALTER TABLE `alarma` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `activa` `activa` TINYINT(4)   NULL AFTER `d_alarma` , 
	CHANGE `muestra_inicio` `muestra_inicio` TINYINT(4)   NULL AFTER `id_menu` , 
	CHANGE `muestra_fin` `muestra_fin` TINYINT(4)   NULL AFTER `muestra_inicio` ;

/* Alter table in target */
ALTER TABLE `alarma_usuario` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_alarma` `id_alarma` INT(11)   NULL AFTER `id` , 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL AFTER `id_alarma` ;

/* Alter table in target */
ALTER TABLE `area` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `asiento` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `imputable` `imputable` TINYINT(4)   NULL AFTER `fecha` , 
	CHANGE `mayor` `mayor` TINYINT(4)   NULL AFTER `imputable` , 
	CHANGE `id_clase_asiento` `id_clase_asiento` INT(11)   NULL AFTER `mayor` , 
	CHANGE `id_estado_asiento` `id_estado_asiento` INT(11)   NULL AFTER `id_clase_asiento` , 
	CHANGE `id_ejercicio_contable` `id_ejercicio_contable` INT(11)   NULL AFTER `id_estado_asiento` , 
	CHANGE `id_tipo_asiento` `id_tipo_asiento` INT(11)   NULL AFTER `id_ejercicio_contable` , 
	CHANGE `id_asiento_efecto` `id_asiento_efecto` INT(11)   NULL AFTER `id_tipo_asiento` , 
	CHANGE `id_modelo` `id_modelo` INT(11)   NULL AFTER `id_asiento_efecto` , 
	CHANGE `id_moneda` `id_moneda` INT(11)   NOT NULL DEFAULT 1 AFTER `id_modelo` , 
	CHANGE `id_comprobante` `id_comprobante` BIGINT(20) UNSIGNED   NULL AFTER `valor_moneda2` , 
	CHANGE `id_asiento_revertido` `id_asiento_revertido` BIGINT(20)   NULL AFTER `id_comprobante` , 
	CHANGE `id_periodo_contable` `id_periodo_contable` BIGINT(20)   NULL AFTER `fecha_conciliacion` , 
	CHANGE `id_agrupacion_asiento` `id_agrupacion_asiento` INT(11)   NULL AFTER `id_periodo_contable` , 
	CHANGE `manual` `manual` TINYINT(4)   NULL AFTER `id_agrupacion_asiento` , 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL AFTER `codigo` ;

/* Alter table in target */
ALTER TABLE `asiento_cuenta_contable` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_asiento` `id_asiento` BIGINT(20)   NOT NULL AFTER `id` , 
	CHANGE `id_cuenta_contable` `id_cuenta_contable` INT(11)   NOT NULL AFTER `id_asiento` , 
	CHANGE `id_centro_costo` `id_centro_costo` INT(11)   NULL AFTER `id_cuenta_contable` , 
	CHANGE `es_debe` `es_debe` TINYINT(4)   NOT NULL AFTER `monto` , 
	CHANGE `conciliado` `conciliado` TINYINT(4)   NULL DEFAULT 0 AFTER `es_debe` ;

/* Alter table in target */
ALTER TABLE `asiento_cuenta_contable_centro_costo_item` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_asiento_cuenta_contable` `id_asiento_cuenta_contable` BIGINT(20)   NOT NULL AFTER `id` , 
	CHANGE `id_centro_costo` `id_centro_costo` INT(11)   NOT NULL AFTER `id_asiento_cuenta_contable` ;

/* Alter table in target */
ALTER TABLE `asiento_efecto` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `habilitado` `habilitado` TINYINT(4)   NULL AFTER `d_asiento_efecto` ;

/* Alter table in target */
ALTER TABLE `asiento_modelo` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_clase_asiento` `id_clase_asiento` INT(11)   NULL AFTER `d_asiento_modelo` , 
	CHANGE `id_tipo_asiento` `id_tipo_asiento` INT(11)   NULL AFTER `id_clase_asiento` , 
	CHANGE `id_moneda` `id_moneda` INT(11)   NULL AFTER `id_tipo_asiento` ;

/* Alter table in target */
ALTER TABLE `asiento_modelo_cuenta_contable` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_asiento_modelo` `id_asiento_modelo` BIGINT(20)   NOT NULL AFTER `id` , 
	CHANGE `id_cuenta_contable` `id_cuenta_contable` INT(11)   NOT NULL AFTER `id_asiento_modelo` , 
	CHANGE `es_debe` `es_debe` TINYINT(4)   NOT NULL AFTER `id_cuenta_contable` ;

/* Alter table in target */
ALTER TABLE `audi_atributo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_entidad` `id_entidad` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_tipo_formulario` `id_tipo_formulario` INT(11)   NULL AFTER `d_atributo` , 
	CHANGE `ver_auditoria` `ver_auditoria` TINYINT(4)   NOT NULL DEFAULT 1 AFTER `id_tipo_formulario` ;

/* Alter table in target */
ALTER TABLE `audi_entidad` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `ver_auditoria` `ver_auditoria` TINYINT(4)   NOT NULL DEFAULT 1 AFTER `d_entidad` ;

/* Alter table in target */
ALTER TABLE `auditoria_comprobante` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_comprobante` `id_comprobante` BIGINT(20)   NULL AFTER `id` , 
	CHANGE `id_estado_comprobrante_anterior` `id_estado_comprobrante_anterior` INT(11)   NULL AFTER `id_comprobante` , 
	CHANGE `id_estado_nuevo` `id_estado_nuevo` INT(11)   NULL AFTER `fecha` , 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL AFTER `id_estado_nuevo` , 
	CHANGE `id_estado_comprobante_nuevo` `id_estado_comprobante_nuevo` INT(11)   NULL AFTER `id_usuario` , 
	CHANGE `impresion_pdf` `impresion_pdf` TINYINT(4)   NULL AFTER `d_punto_venta` , 
	CHANGE `nro_comprobante` `nro_comprobante` BIGINT(20)   NULL AFTER `impresion_pdf` , 
	CHANGE `id_punto_venta` `id_punto_venta` INT(11)   NULL AFTER `nro_comprobante` ;

/* Alter table in target */
ALTER TABLE `auditoria_producto` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_producto` `id_producto` BIGINT(20)   NULL AFTER `id` , 
	CHANGE `id_comprobante` `id_comprobante` BIGINT(20)   NULL AFTER `id_producto` , 
	CHANGE `id_deposito` `id_deposito` INT(11)   NULL AFTER `id_comprobante` , 
	CHANGE `id_orden_armado` `id_orden_armado` INT(11)   NULL AFTER `id_deposito` , 
	CHANGE `positivo` `positivo` TINYINT(4)   NULL AFTER `id_orden_armado` ;

/* Alter table in target */
ALTER TABLE `banco` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `codigo` `codigo` INT(11)   NULL AFTER `cuit` , 
	CHANGE `id_tipo_documento` `id_tipo_documento` INT(11)   NULL DEFAULT 3 AFTER `codigo` ;

/* Alter table in target */
ALTER TABLE `banco_sucursal` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_banco` `id_banco` INT(11)   NULL COMMENT 'no_visible' AFTER `id` , 
	CHANGE `id_provincia` `id_provincia` INT(11)   NULL COMMENT 'no_visible' AFTER `codigo_postal` , 
	CHANGE `id_pais` `id_pais` INT(10)   NULL DEFAULT 1 COMMENT 'no_visible' AFTER `id_provincia` ;

/* Alter table in target */
ALTER TABLE `cake_sessions` 
	CHANGE `expires` `expires` INT(11)   NULL AFTER `data` , 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL AFTER `expires` , 
	CHANGE `principal` `principal` INT(11)   NULL AFTER `id_usuario` ;

/* Alter table in target */
ALTER TABLE `campo_extra` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `campo_extra_registro` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_campo_extra` `id_campo_extra` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_tabla_apuntada` `id_tabla_apuntada` INT(11)   NOT NULL AFTER `dato` ;

/* Alter table in target */
ALTER TABLE `cartera_rendicion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_tipo_cartera_rendicion` `id_tipo_cartera_rendicion` INT(11)   NOT NULL AFTER `d_cartera_rendicion` , 
	CHANGE `id_cuenta_contable` `id_cuenta_contable` INT(11)   NULL AFTER `id_tipo_cartera_rendicion` , 
	CHANGE `id_dato_empresa` `id_dato_empresa` INT(11)   NULL DEFAULT 1 AFTER `id_cuenta_contable` , 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL AFTER `id_dato_empresa` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL DEFAULT 1 AFTER `id_usuario` ;

/* Alter table in target */
ALTER TABLE `categoria` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `imprimible_oa` `imprimible_oa` TINYINT(4)   NULL DEFAULT 1 AFTER `d_categoria` , 
	CHANGE `importable_desde_certificado_calidad` `importable_desde_certificado_calidad` TINYINT(4)   NULL AFTER `imprimible_oa` ;

/* Alter table in target */
ALTER TABLE `centro_costo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_dato_empresa` `id_dato_empresa` INT(11)   NOT NULL DEFAULT 1 AFTER `id` , 
	CHANGE `id_area` `id_area` INT(11)   NULL AFTER `d_centro_costo` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL DEFAULT 1 AFTER `id_area` ;

/* Alter table in target */
ALTER TABLE `cheque` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_banco` `id_banco` INT(11)   NULL AFTER `nro_cheque` , 
	CHANGE `id_persona_ingreso` `id_persona_ingreso` INT(11)   NULL AFTER `id_banco` , 
	CHANGE `id_persona_egreso` `id_persona_egreso` INT(11)   NULL AFTER `id_persona_ingreso` , 
	CHANGE `titular_cuit` `titular_cuit` BIGINT(20)   NULL AFTER `id_persona_egreso` , 
	CHANGE `cruzado` `cruzado` TINYINT(4)   NULL DEFAULT 0 AFTER `monto` , 
	CHANGE `id_estado_cheque` `id_estado_cheque` INT(11)   NOT NULL AFTER `titular_razon_social` , 
	CHANGE `id_tipo_cheque` `id_tipo_cheque` INT(11)   NOT NULL AFTER `id_estado_cheque` , 
	CHANGE `id_moneda` `id_moneda` INT(11)   NULL AFTER `id_tipo_cheque` , 
	CHANGE `id_chequera` `id_chequera` INT(11)   NULL AFTER `valor_moneda` , 
	CHANGE `dias` `dias` INT(11)   NULL AFTER `id_chequera` , 
	CHANGE `id_banco_sucursal` `id_banco_sucursal` INT(11)   NULL AFTER `observacion` , 
	CHANGE `clearing` `clearing` INT(11)   NULL AFTER `id_banco_sucursal` , 
	CHANGE `id_comprobante_entrada` `id_comprobante_entrada` BIGINT(20) UNSIGNED   NULL AFTER `clearing` , 
	CHANGE `id_comprobante_salida` `id_comprobante_salida` BIGINT(20) UNSIGNED   NULL AFTER `id_comprobante_entrada` , 
	CHANGE `id_comprobante_rechazo` `id_comprobante_rechazo` BIGINT(20) UNSIGNED   NULL AFTER `id_comprobante_salida` , 
	CHANGE `no_a_la_orden` `no_a_la_orden` TINYINT(4)   NULL DEFAULT 0 AFTER `id_comprobante_rechazo` , 
	CHANGE `comun` `comun` TINYINT(4)   NULL AFTER `no_a_la_orden` , 
	CHANGE `diferido` `diferido` TINYINT(4)   NULL AFTER `comun` , 
	CHANGE `conciliado` `conciliado` TINYINT(4)   NULL AFTER `diferido` ;

/* Alter table in target */
ALTER TABLE `chequera` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_cuenta_bancaria` `id_cuenta_bancaria` INT(11)   NOT NULL DEFAULT 0 AFTER `id` , 
	CHANGE `cheque_desde` `cheque_desde` BIGINT(11)   NOT NULL DEFAULT 0 AFTER `id_cuenta_bancaria` , 
	CHANGE `cheque_hasta` `cheque_hasta` BIGINT(11)   NULL AFTER `cheque_desde` , 
	CHANGE `numero_chequera` `numero_chequera` INT(11)   NOT NULL DEFAULT 0 AFTER `cheque_hasta` , 
	CHANGE `cantidad_cheques` `cantidad_cheques` INT(6)   NOT NULL DEFAULT 0 AFTER `fecha_entrega` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL AFTER `observaciones` , 
	CHANGE `numero_actual` `numero_actual` BIGINT(20)   NULL DEFAULT 0 AFTER `activo` , 
	CHANGE `id_estado_chequera` `id_estado_chequera` INT(11)   NULL AFTER `numero_actual` , 
	CHANGE `id_cuenta_contable` `id_cuenta_contable` INT(11)   NULL AFTER `d_chequera` ;

/* Alter table in target */
ALTER TABLE `clase_asiento` 
	CHANGE `id` `id` INT(4)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `clase_cuenta_contable` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `saldo_habitual_deudor` `saldo_habitual_deudor` TINYINT(4)   NULL AFTER `abreviado` ;

/* Alter table in target */
ALTER TABLE `clearing` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `horas` `horas` INT(11)   NOT NULL DEFAULT 0 AFTER `abreviado` ;

/* Alter table in target */
ALTER TABLE `color` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `comprobante` 
	CHANGE `id` `id` BIGINT(20) UNSIGNED   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_tipo_comprobante` `id_tipo_comprobante` INT(11)   NOT NULL COMMENT 'no_visible' AFTER `id` , 
	CHANGE `nro_comprobante` `nro_comprobante` BIGINT(20)   NULL AFTER `id_tipo_comprobante` , 
	CHANGE `id_persona` `id_persona` INT(11)   NULL COMMENT 'no_visible' AFTER `nro_comprobante` , 
	CHANGE `id_transportista` `id_transportista` INT(11)   NULL COMMENT 'no_visible' AFTER `fecha_aprobacion` , 
	CHANGE `id_moneda` `id_moneda` INT(11)   NULL DEFAULT 2 COMMENT 'no_visible' AFTER `id_transportista` , 
	CHANGE `id_dato_empresa` `id_dato_empresa` INT(11)   NULL COMMENT 'no_visible' AFTER `valor_moneda` , 
	CHANGE `id_sucursal` `id_sucursal` INT(11)   NULL COMMENT 'no_visible' AFTER `id_dato_empresa` , 
	CHANGE `id_punto_venta` `id_punto_venta` INT(11)   NULL COMMENT 'no_visible' AFTER `importe_declarado` , 
	CHANGE `id_forma_pago` `id_forma_pago` INT(11)   NULL COMMENT 'no_visible' AFTER `id_punto_venta` , 
	CHANGE `cae` `cae` BIGINT(20)   NULL AFTER `observacion` , 
	CHANGE `nro_comprobante_afip` `nro_comprobante_afip` BIGINT(20)   NULL AFTER `cae` , 
	CHANGE `id_deposito_origen` `id_deposito_origen` INT(11)   NULL COMMENT 'no_visible' AFTER `vencimiento_cae` , 
	CHANGE `genera_remito` `genera_remito` TINYINT(4)   NULL AFTER `id_deposito_origen` , 
	CHANGE `valida_cuenta_corriente` `valida_cuenta_corriente` TINYINT(4)   NULL AFTER `genera_remito` , 
	CHANGE `id_estado_comprobante` `id_estado_comprobante` INT(11)   NULL DEFAULT 1 COMMENT 'no_visible' AFTER `valida_cuenta_corriente` , 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL COMMENT 'no_visible' AFTER `id_estado_comprobante` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL DEFAULT 1 COMMENT 'no_visible' AFTER `id_usuario` , 
	CHANGE `validez_dias` `validez_dias` INT(11)   NULL AFTER `plazo_entrega` , 
	CHANGE `inspeccion` `inspeccion` TINYINT(4)   NULL AFTER `lugar_entrega` , 
	CHANGE `certificados` `certificados` TINYINT(4)   NULL AFTER `inspeccion` , 
	CHANGE `plazo_entrega_dias` `plazo_entrega_dias` INT(11)   NULL AFTER `orden_compra_externa` , 
	CHANGE `id_deposito_destino` `id_deposito_destino` INT(11)   NULL AFTER `plazo_entrega_dias` , 
	CHANGE `aprobado` `aprobado` TINYINT(4)   NULL AFTER `id_deposito_destino` , 
	CHANGE `definitivo` `definitivo` TINYINT(4)   NULL AFTER `aprobado` , 
	CHANGE `genera_movimiento_stock` `genera_movimiento_stock` TINYINT(4)   NULL AFTER `definitivo` , 
	CHANGE `id_detalle_tipo_comprobante` `id_detalle_tipo_comprobante` INT(11)   NULL AFTER `genera_movimiento_stock` , 
	CHANGE `tiene_impuesto` `tiene_impuesto` TINYINT(4)   NULL DEFAULT 0 AFTER `id_detalle_tipo_comprobante` , 
	CHANGE `tiene_iva` `tiene_iva` TINYINT(4)   NULL DEFAULT 0 AFTER `tiene_impuesto` , 
	CHANGE `id_pedido_interno` `id_pedido_interno` BIGINT(20) UNSIGNED   NULL AFTER `tiene_iva` , 
	CHANGE `chk_pedido_interno` `chk_pedido_interno` TINYINT(4)   NULL AFTER `id_pedido_interno_fox` , 
	CHANGE `id_unidad_valor` `id_unidad_valor` INT(11)   NULL AFTER `fecha_cierre` , 
	CHANGE `comprobante_electronico` `comprobante_electronico` TINYINT(4)   NULL DEFAULT 0 AFTER `total_comprobante` , 
	CHANGE `comprobante_genera_asiento` `comprobante_genera_asiento` TINYINT(4)   NULL DEFAULT 0 AFTER `d_tipo_comprobante_letra` , 
	CHANGE `id_cartera_rendicion` `id_cartera_rendicion` INT(11)   NULL AFTER `comprobante_genera_asiento` , 
	CHANGE `id_periodo_impositivo` `id_periodo_impositivo` INT(11)   NULL AFTER `valor_moneda3` , 
	CHANGE `id_comprobante_lote_ejecucion` `id_comprobante_lote_ejecucion` BIGINT(20)   NULL AFTER `fecha_ejecucion_lote` , 
	CHANGE `id_comprobante_lote` `id_comprobante_lote` INT(11)   NULL AFTER `id_comprobante_lote_ejecucion` , 
	CHANGE `id_otro_sistema` `id_otro_sistema` BIGINT(20)   NULL AFTER `id_comprobante_lote` , 
	CHANGE `id_tipo_comercializacion` `id_tipo_comercializacion` INT(11)   NULL AFTER `referencia_comprobante_externo` , 
	CHANGE `permite_ajuste_diferencia_cambio` `permite_ajuste_diferencia_cambio` TINYINT(4)   NULL AFTER `id_tipo_comercializacion` , 
	CHANGE `enlazar_con_moneda` `enlazar_con_moneda` TINYINT(4)   NULL AFTER `permite_ajuste_diferencia_cambio` , 
	CHANGE `id_moneda_enlazada` `id_moneda_enlazada` INT(4)   NULL AFTER `enlazar_con_moneda` , 
	CHANGE `cantidad_impresion_pdf` `cantidad_impresion_pdf` TINYINT(4)   NULL DEFAULT 0 AFTER `id_moneda_enlazada` , 
	CHANGE `chk_envia_mail` `chk_envia_mail` TINYINT(4)   NULL AFTER `cantidad_impresion_pdf` , 
	CHANGE `id_tipo_documento` `id_tipo_documento` INT(11)   NULL AFTER `chk_envia_mail` , 
	CHANGE `id_condicion_pago` `id_condicion_pago` INT(11)   NULL AFTER `id_tipo_documento` , 
	CHANGE `mail_enviado` `mail_enviado` TINYINT(4)   NULL AFTER `id_condicion_pago` , 
	CHANGE `meli_order_id` `meli_order_id` BIGINT(20)   NULL AFTER `mail_enviado` , 
	CHANGE `entrega_domicilio` `entrega_domicilio` INT(4)   NULL AFTER `meli_order_id` , 
	CHANGE `id_persona_secundaria` `id_persona_secundaria` INT(11)   NULL AFTER `entrega_domicilio` , 
	CHANGE `meli_shippment_id` `meli_shippment_id` INT(11)   NULL AFTER `id_persona_secundaria` , 
	CHANGE `imprimir_obs_cuerpo` `imprimir_obs_cuerpo` TINYINT(4)   NULL AFTER `meli_shippment_cost` , 
	ADD COLUMN `chk_asociado_rechazado` TINYINT(4)   NULL AFTER `imprimir_obs_cuerpo` ;

/* Alter table in target */
ALTER TABLE `comprobante_cartera_rendicion` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_comprobante` `id_comprobante` BIGINT(20) UNSIGNED   NOT NULL AFTER `id` , 
	CHANGE `id_cartera_rendicion` `id_cartera_rendicion` INT(11)   NOT NULL AFTER `id_comprobante` ;

/* Alter table in target */
ALTER TABLE `comprobante_digital` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_media_file` `id_media_file` BIGINT(20)   NULL AFTER `id` , 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL AFTER `d_comprobante_digital` ;

/* Alter table in target */
ALTER TABLE `comprobante_impuesto` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_comprobante` `id_comprobante` BIGINT(20) UNSIGNED   NOT NULL AFTER `id` , 
	CHANGE `id_impuesto` `id_impuesto` INT(11)   NOT NULL DEFAULT 0 AFTER `id_comprobante` , 
	CHANGE `id_categoria_impuesto` `id_categoria_impuesto` INT(11)   NULL DEFAULT 0 AFTER `id_impuesto` , 
	CHANGE `id_item_asiento_contable` `id_item_asiento_contable` INT(11)   NULL DEFAULT 0 AFTER `id_categoria_impuesto` , 
	CHANGE `id_provincia` `id_provincia` INT(11)   NULL AFTER `porcentaje_reduccion` , 
	CHANGE `tipo_alicuota_afip` `tipo_alicuota_afip` INT(11)   NULL AFTER `id_provincia` , 
	CHANGE `id_comprobante_referencia` `id_comprobante_referencia` BIGINT(20) UNSIGNED   NULL AFTER `d_comprobante_impuesto` ;

/* Alter table in target */
ALTER TABLE `comprobante_item` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_comprobante` `id_comprobante` BIGINT(20) UNSIGNED   NOT NULL AFTER `id` , 
	CHANGE `orden` `orden` INT(11)   NULL AFTER `precio_unitario` , 
	CHANGE `n_item` `n_item` INT(11)   NULL AFTER `orden` , 
	CHANGE `id_iva` `id_iva` INT(11)   NULL DEFAULT 1 AFTER `n_item` , 
	CHANGE `id_producto` `id_producto` INT(11)   NULL AFTER `id_iva` , 
	CHANGE `dias` `dias` INT(11)   NULL AFTER `item_observacion` , 
	CHANGE `id_comprobante_item_origen` `id_comprobante_item_origen` BIGINT(20)   NULL AFTER `dias` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL DEFAULT 1 AFTER `id_comprobante_item_origen` , 
	CHANGE `id_unidad` `id_unidad` INT(11)   NULL AFTER `cantidad_cierre` , 
	CHANGE `id_comprobante_origen` `id_comprobante_origen` BIGINT(20) UNSIGNED   NULL AFTER `id_unidad` , 
	CHANGE `id_migracion` `id_migracion` BIGINT(20)   NULL AFTER `id_comprobante_origen` , 
	CHANGE `id_lista_precio` `id_lista_precio` INT(11)   NULL AFTER `precio_unitario3` , 
	CHANGE `id_tipo_lote` `id_tipo_lote` INT(11)   NULL AFTER `precio_minimo_producto` , 
	CHANGE `requiere_conformidad` `requiere_conformidad` TINYINT(4)   NULL DEFAULT 0 AFTER `id_tipo_lote` , 
	CHANGE `ajusta_diferencia_cambio` `ajusta_diferencia_cambio` TINYINT(4)   NULL DEFAULT 0 AFTER `requiere_conformidad` , 
	CHANGE `id_otro_sistema` `id_otro_sistema` BIGINT(20)   NULL AFTER `monto_diferencia_cambio` , 
	CHANGE `id_persona` `id_persona` INT(11)   NULL AFTER `producto_codigo` , 
	CHANGE `id_detalle_tipo_comprobante` `id_detalle_tipo_comprobante` INT(11)   NULL AFTER `id_persona` ;

/* Alter table in target */
ALTER TABLE `comprobante_item_lote` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_comprobante_item` `id_comprobante_item` BIGINT(20)   NOT NULL AFTER `id` , 
	CHANGE `id_lote` `id_lote` BIGINT(20)   NOT NULL AFTER `id_comprobante_item` , 
	CHANGE `orden` `orden` INT(11)   NULL AFTER `id_lote` ;

/* Alter table in target */
ALTER TABLE `comprobante_lote` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `dia_proceso` `dia_proceso` INT(11)   NOT NULL AFTER `d_comprobante_lote` , 
	CHANGE `hora_proceso` `hora_proceso` INT(11)   NULL AFTER `fecha_ultimo_proceso` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL AFTER `observacion` , 
	CHANGE `manual` `manual` TINYINT(4)   NULL AFTER `activo` , 
	CHANGE `electronico` `electronico` TINYINT(4)   NULL AFTER `manual` ;

/* Alter table in target */
ALTER TABLE `comprobante_lote_ejecucion` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_comprobante_lote` `id_comprobante_lote` INT(11)   NOT NULL AFTER `id` ;

/* Alter table in target */
ALTER TABLE `comprobante_lote_item` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL FIRST , 
	CHANGE `id_comprobante` `id_comprobante` BIGINT(20)   NOT NULL AFTER `id` , 
	CHANGE `id_comprobante_lote` `id_comprobante_lote` INT(11)   NOT NULL AFTER `id_comprobante` , 
	CHANGE `id_comprobante_lote_ejecucion` `id_comprobante_lote_ejecucion` BIGINT(20)   NULL AFTER `id_comprobante_lote` ;

/* Alter table in target */
ALTER TABLE `comprobante_punto_venta_numero` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_tipo_comprobante` `id_tipo_comprobante` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_punto_venta` `id_punto_venta` INT(11)   NULL AFTER `id_tipo_comprobante` , 
	CHANGE `numero_actual` `numero_actual` BIGINT(11)   NOT NULL DEFAULT 0 COMMENT 'BORRAR_ USAR ID_CONTADOR' AFTER `id_punto_venta` , 
	CHANGE `id_contador` `id_contador` INT(11)   NULL AFTER `observacion` , 
	CHANGE `por_defecto` `por_defecto` TINYINT(4)   NULL AFTER `id_contador` , 
	CHANGE `permite_modificar_nro_comprobante` `permite_modificar_nro_comprobante` TINYINT(4)   NULL AFTER `por_defecto` , 
	CHANGE `id_tipo_comprobante_punto_venta_numero` `id_tipo_comprobante_punto_venta_numero` TINYINT(4)   NULL COMMENT 'es un ENUM' AFTER `fecha_vencimiento_codigo_legal_talonario` , 
	CHANGE `id_impuesto` `id_impuesto` INT(11)   NULL AFTER `id_tipo_comprobante_punto_venta_numero` , 
	CHANGE `id_estado_comprobante_defecto_sin_uso` `id_estado_comprobante_defecto_sin_uso` INT(11)   NULL AFTER `id_impuesto` ;

/* Alter table in target */
ALTER TABLE `comprobante_recepcion_item` 
	CHANGE `id` `id` BIGINT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_comprobante_item` `id_comprobante_item` BIGINT(11)   NOT NULL AFTER `id` ;

/* Alter table in target */
ALTER TABLE `comprobante_valor` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_valor` `id_valor` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_comprobante` `id_comprobante` BIGINT(20) UNSIGNED   NOT NULL AFTER `id_valor` , 
	CHANGE `id_moneda` `id_moneda` INT(11)   NULL AFTER `monto` , 
	CHANGE `id_cheque` `id_cheque` BIGINT(20)   NULL AFTER `d_comprobante_valor` , 
	CHANGE `id_cuenta_bancaria` `id_cuenta_bancaria` INT(11)   NULL AFTER `id_cheque` , 
	CHANGE `orden` `orden` INT(11)   NULL AFTER `referencia` , 
	CHANGE `id_comprobante_referencia` `id_comprobante_referencia` BIGINT(20) UNSIGNED   NULL AFTER `orden` , 
	CHANGE `origen` `origen` TINYINT(4)   NULL AFTER `id_comprobante_referencia` , 
	CHANGE `id_tarjeta_credito` `id_tarjeta_credito` INT(11)   NULL AFTER `origen` , 
	CHANGE `id_tipo_tarjeta_credito` `id_tipo_tarjeta_credito` INT(11)   NULL AFTER `id_tarjeta_credito` , 
	CHANGE `conciliado` `conciliado` TINYINT(4)   NULL AFTER `nro_tarjeta` ;



/* Alter table in target */
ALTER TABLE `configuracion_aplicacion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `test` `test` TINYINT(4)   NULL AFTER `url_informe` , 
	CHANGE `id_pais` `id_pais` INT(11)   NULL AFTER `url_key` , 
	CHANGE `id_provincia` `id_provincia` INT(11)   NULL AFTER `id_pais` , 
	CHANGE `max_notas_usuario` `max_notas_usuario` INT(11)   NULL AFTER `urlNegocioProduccionAfip` ;

/* Alter table in target */
ALTER TABLE `contador` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `numero_actual` `numero_actual` BIGINT(11)   NULL DEFAULT 0 AFTER `d_contador` ;

/* Alter table in target */
ALTER TABLE `controlador` 
	CHANGE `id` `id` INT(11)   NULL FIRST ;

/* Alter table in target */
ALTER TABLE `csv_import_conciliacion` 
	CHANGE `es_debe` `es_debe` TINYINT(4)   NULL AFTER `columna4` , 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT AFTER `es_debe` ;

/* Alter table in target */
ALTER TABLE `cuenta_bancaria` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_tipo_cuenta_bancaria` `id_tipo_cuenta_bancaria` INT(11)   NULL DEFAULT 2 COMMENT 'no_visible' AFTER `nro_iban` , 
	CHANGE `id_banco_sucursal` `id_banco_sucursal` INT(11)   NULL COMMENT 'no_visible' AFTER `id_tipo_cuenta_bancaria` , 
	CHANGE `propia` `propia` TINYINT(4)   NULL DEFAULT 1 COMMENT 'no_visible' AFTER `id_banco_sucursal` , 
	CHANGE `id_persona` `id_persona` INT(11)   NULL COMMENT 'no_visible' AFTER `propia` , 
	CHANGE `id_moneda` `id_moneda` INT(11)   NULL COMMENT 'no_visible' AFTER `id_persona` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL COMMENT 'no_visible' AFTER `id_moneda` , 
	CHANGE `id_cuenta_contable` `id_cuenta_contable` INT(11)   NULL COMMENT 'no_visible' AFTER `activo` , 
	CHANGE `id_cuenta_contable_puente` `id_cuenta_contable_puente` INT(11)   NULL COMMENT 'no_visible' AFTER `id_cuenta_contable` , 
	CHANGE `id_banco` `id_banco` INT(11)   NULL AFTER `alias_cbu` ;



/* Alter table in target */
ALTER TABLE `cuenta_contable_centro_costo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_cuenta_contable` `id_cuenta_contable` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_centro_costo` `id_centro_costo` INT(11)   NOT NULL AFTER `id_cuenta_contable` ;

/* Alter table in target */
ALTER TABLE `dato_empresa` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_provincia` `id_provincia` INT(11)   NULL COMMENT 'no_visible' AFTER `localidad` , 
	CHANGE `id_pais` `id_pais` INT(11)   NULL COMMENT 'no_visible' AFTER `id_provincia` , 
	CHANGE `id_tipo_iva` `id_tipo_iva` INT(11)   NULL COMMENT 'no_visible' AFTER `email` , 
	CHANGE `id_iva` `id_iva` INT(11)   NULL COMMENT 'no_visible' AFTER `id_tipo_iva` , 
	CHANGE `cuit` `cuit` BIGINT(20)   NOT NULL AFTER `id_iva` , 
	CHANGE `id_actividad_no_computable` `id_actividad_no_computable` INT(11)   NULL COMMENT 'no_visible' AFTER `nro_agente_retencion` , 
	CHANGE `autoimpresor` `autoimpresor` TINYINT(4)   NULL AFTER `id_actividad_no_computable` , 
	CHANGE `id_tipo_documento` `id_tipo_documento` INT(11)   NULL DEFAULT 3 COMMENT 'no_visible' AFTER `concepto_afip` , 
	CHANGE `id_tipo_empresa` `id_tipo_empresa` INT(11)   NULL DEFAULT 1 COMMENT 'no_visible' AFTER `id_tipo_documento` , 
	CHANGE `agente_percepcion_iibb` `agente_percepcion_iibb` TINYINT(4)   NULL AFTER `domicilio` , 
	CHANGE `id_moneda` `id_moneda` INT(11)   NULL DEFAULT 1 COMMENT 'moneda base primaria' AFTER `agente_percepcion_iibb` , 
	CHANGE `permite_stock_negativo` `permite_stock_negativo` TINYINT(4)   NULL AFTER `id_moneda` , 
	CHANGE `actualiza_costo_en_cadena` `actualiza_costo_en_cadena` TINYINT(4)   NULL DEFAULT 1 AFTER `permite_stock_negativo` , 
	CHANGE `id_lista_precio` `id_lista_precio` INT(11)   NULL DEFAULT 2 AFTER `actualiza_costo_en_cadena` , 
	CHANGE `es_sucursal` `es_sucursal` INT(11)   NULL DEFAULT 0 AFTER `id_lista_precio` , 
	CHANGE `afectar_stock_con_orden_armado` `afectar_stock_con_orden_armado` TINYINT(4)   NULL AFTER `es_sucursal` , 
	CHANGE `mostrar_leyenda_dolar_factura` `mostrar_leyenda_dolar_factura` TINYINT(4)   NULL DEFAULT 0 AFTER `afectar_stock_con_orden_armado` , 
	CHANGE `codigo_producto_longitud_maxima` `codigo_producto_longitud_maxima` INT(11)   NULL DEFAULT 100 AFTER `mostrar_leyenda_dolar_factura` , 
	CHANGE `cerrar_pedido_venta_con_facturacion` `cerrar_pedido_venta_con_facturacion` TINYINT(4)   NULL AFTER `codigo_producto_longitud_maxima` , 
	CHANGE `proceso_armado_dias_estimado` `proceso_armado_dias_estimado` INT(11)   NULL DEFAULT 15 AFTER `cerrar_pedido_venta_con_facturacion` , 
	CHANGE `check_precio_coef_venta_min` `check_precio_coef_venta_min` TINYINT(4)   NULL DEFAULT 1 AFTER `app_path` , 
	CHANGE `checkeo_pedido_interno_cliente_no_repite_oc` `checkeo_pedido_interno_cliente_no_repite_oc` TINYINT(4)   NULL AFTER `check_precio_coef_venta_min` , 
	CHANGE `cierre_periodo` `cierre_periodo` BINARY(1)   NULL DEFAULT '0' COMMENT 'Para verse a si mismo para generar pdfs' AFTER `ventas_permite_cancelar_asiento_ingreso_comprobante` , 
	CHANGE `chequea_remito_imprimir_solo_con_remitido` `chequea_remito_imprimir_solo_con_remitido` TINYINT(4)   NULL DEFAULT 1 AFTER `cierre_periodo` , 
	CHANGE `id_moneda2` `id_moneda2` INT(11)   NULL COMMENT '//Moneda base secundaria' AFTER `chequea_remito_imprimir_solo_con_remitido` , 
	CHANGE `dias_clearing` `dias_clearing` INT(11)   NULL DEFAULT 2 AFTER `id_moneda2` , 
	CHANGE `id_cuenta_contable_caja` `id_cuenta_contable_caja` INT(11)   NULL AFTER `dias_clearing` , 
	CHANGE `id_cuenta_contable_cheque_cartera` `id_cuenta_contable_cheque_cartera` INT(11)   NULL AFTER `id_cuenta_contable_caja` , 
	CHANGE `solo_moneda_corriente` `solo_moneda_corriente` TINYINT(4)   NULL DEFAULT 1 AFTER `id_cuenta_contable_cheque_cartera` , 
	CHANGE `mostrar_error_parametrizacion_contable` `mostrar_error_parametrizacion_contable` TINYINT(4)   NULL DEFAULT 1 AFTER `solo_moneda_corriente` , 
	CHANGE `id_cuenta_contable_cheque_rechazado` `id_cuenta_contable_cheque_rechazado` INT(11)   NULL AFTER `product_version` , 
	CHANGE `id_moneda3` `id_moneda3` INT(11)   NULL AFTER `id_cuenta_contable_cheque_rechazado` , 
	CHANGE `id_cuenta_contable_resultado_ejercicio` `id_cuenta_contable_resultado_ejercicio` INT(11)   NULL AFTER `direccion_longitud` , 
	CHANGE `id_cuenta_resultado_no_asignado` `id_cuenta_resultado_no_asignado` INT(11)   NULL AFTER `id_cuenta_contable_resultado_ejercicio` , 
	CHANGE `email_smtp_port` `email_smtp_port` INT(11)   NULL AFTER `email_smtp_host` , 
	CHANGE `email_smtp_auth` `email_smtp_auth` TINYINT(4)   NULL AFTER `email_set_from_name` , 
	CHANGE `usa_smtp` `usa_smtp` TINYINT(4)   NULL AFTER `email_smtp_auth` , 
	CHANGE `id_situacion_ib` `id_situacion_ib` INT(11)   NULL AFTER `usa_smtp` , 
	CHANGE `email_intentos_fallidos` `email_intentos_fallidos` INT(11)   NULL AFTER `local_path` , 
	CHANGE `mercadolibre_user_id` `mercadolibre_user_id` BIGINT(20)   NULL AFTER `mercadolibre_access_token` , 
	CHANGE `mercadolibre_access_token_expire_seconds` `mercadolibre_access_token_expire_seconds` BIGINT(20)   NULL AFTER `mercadolibre_user_id` , 
	CHANGE `mercadolibre_id_punto_venta_defecto_pedido` `mercadolibre_id_punto_venta_defecto_pedido` INT(11)   NULL AFTER `mercadolibre_refresh_token` , 
	CHANGE `mercadolibre_id_lista_precio` `mercadolibre_id_lista_precio` INT(11)   NULL AFTER `mercadolibre_id_punto_venta_defecto_pedido` , 
	CHANGE `mercadolibre_conexion_activa` `mercadolibre_conexion_activa` TINYINT(4)   NULL AFTER `mercadolibre_id_lista_precio` , 
	CHANGE `mercadolibre_enviar_mensaje_post_venta` `mercadolibre_enviar_mensaje_post_venta` TINYINT(4)   NULL AFTER `mercadolibre_mensaje_post_venta` , 
	CHANGE `sivit_clientes_conexion_activa` `sivit_clientes_conexion_activa` TINYINT(4)   NULL AFTER `url_sivit` , 
	CHANGE `sivit_proveedores_conexion_activa` `sivit_proveedores_conexion_activa` TINYINT(4)   NULL AFTER `sivit_clientes_conexion_activa` ;

/* Alter table in target */
ALTER TABLE `dato_empresa_impuesto` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_dato_empresa` `id_dato_empresa` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_impuesto` `id_impuesto` INT(11)   NOT NULL AFTER `id_dato_empresa` , 
	CHANGE `sin_vencimiento` `sin_vencimiento` TINYINT(4)   NULL AFTER `fecha_vigencia_hasta` , 
	CHANGE `check_mostrar_impuesto_libro_iva` `check_mostrar_impuesto_libro_iva` TINYINT(4)   NULL DEFAULT 0 AFTER `sin_vencimiento` ;

/* Alter table in target */
ALTER TABLE `deposito` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_sucursal` `id_sucursal` INT(11)   NOT NULL DEFAULT 0 AFTER `abreviado` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL AFTER `ubicacion` , 
	CHANGE `id_persona` `id_persona` INT(11)   NULL AFTER `activo` , 
	CHANGE `parent_id` `parent_id` INT(11)   NULL AFTER `id_persona` , 
	CHANGE `id_deposito_categoria` `id_deposito_categoria` INT(11)   NULL AFTER `parent_id` , 
	CHANGE `lft` `lft` INT(11)   NULL AFTER `id_deposito_categoria` , 
	CHANGE `rght` `rght` INT(11)   NULL AFTER `lft` , 
	CHANGE `id_dato_empresa` `id_dato_empresa` INT(11)   NULL DEFAULT 1 AFTER `rght` ;

/* Alter table in target */
ALTER TABLE `deposito_categoria` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `deposito_movimiento_tipo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_movimiento_tipo` `id_movimiento_tipo` INT(11)   NULL AFTER `id` , 
	CHANGE `id_deposito` `id_deposito` INT(11)   NULL AFTER `id_movimiento_tipo` , 
	CHANGE `es_origen` `es_origen` TINYINT(4)   NULL AFTER `id_deposito` , 
	CHANGE `por_defecto` `por_defecto` TINYINT(4)   NULL COMMENT '0' AFTER `es_origen` , 
	CHANGE `id_punto_venta` `id_punto_venta` INT(11)   NULL AFTER `por_defecto` , 
	CHANGE `id_tipo_comprobante` `id_tipo_comprobante` INT(11)   NULL AFTER `id_punto_venta` ;

/* Alter table in target */
ALTER TABLE `descuento_familia_persona` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_persona` `id_persona` INT(11)   NOT NULL COMMENT 'no_visible' AFTER `id` , 
	CHANGE `id_familia_producto` `id_familia_producto` INT(11)   NOT NULL COMMENT 'no_visible' AFTER `id_persona` ;

/* Alter table in target */
ALTER TABLE `destino_producto` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `detalle_tipo_comprobante` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_tipo_comprobante` `id_tipo_comprobante` INT(11)   NULL AFTER `d_detalle_tipo_comprobante` , 
	CHANGE `default` `default` TINYINT(4)   NULL AFTER `descripcion_accion` , 
	CHANGE `afecta_stock_origen` `afecta_stock_origen` TINYINT(4)   NULL DEFAULT 0 AFTER `default` , 
	CHANGE `tiene_iva` `tiene_iva` TINYINT(4)   NULL DEFAULT 0 AFTER `afecta_stock_origen` , 
	CHANGE `tiene_impuestos` `tiene_impuestos` TINYINT(4)   NULL DEFAULT 0 AFTER `tiene_iva` , 
	CHANGE `puede_editar_precio_unitario` `puede_editar_precio_unitario` TINYINT(4)   NULL DEFAULT 0 AFTER `tiene_impuestos` , 
	CHANGE `id_impuesto` `id_impuesto` INT(4)   NULL AFTER `puede_editar_precio_unitario` , 
	CHANGE `id_cuenta_contable` `id_cuenta_contable` INT(11)   NULL AFTER `id_impuesto` , 
	CHANGE `genera_nd` `genera_nd` INT(11)   NULL DEFAULT 0 AFTER `id_cuenta_contable` , 
	CHANGE `dias` `dias` INT(11)   NULL DEFAULT 0 AFTER `genera_nd` , 
	CHANGE `dias2` `dias2` INT(11)   NULL DEFAULT 0 AFTER `dias` ;



/* Alter table in target */
ALTER TABLE `empleado` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_area` `id_area` INT(11)   NULL COMMENT 'no_visible' AFTER `apellido` , 
	CHANGE `foto` `foto` INT(11)   NULL COMMENT 'no_visible' AFTER `sexo` , 
	CHANGE `tipo_documento` `tipo_documento` INT(11)   NULL AFTER `foto` , 
	CHANGE `nro_documento` `nro_documento` INT(11)   NULL AFTER `tipo_documento` , 
	CHANGE `id_media_file` `id_media_file` BIGINT(20)   NULL COMMENT 'no_visible' AFTER `interno` , 
	CHANGE `id_tipo_contrato` `id_tipo_contrato` INT(11)   NULL AFTER `direccion_longitud` ;

/* Alter table in target */
ALTER TABLE `entidad` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `auditada` `auditada` TINYINT(4)   NULL AFTER `d_entidad` , 
	CHANGE `permite_categoria_generica` `permite_categoria_generica` TINYINT(4)   NULL DEFAULT 0 AFTER `auditada` , 
	CHANGE `estatica_sistema` `estatica_sistema` TINYINT(4)   NULL AFTER `permite_categoria_generica` , 
	CHANGE `tiene_cache_cliente` `tiene_cache_cliente` TINYINT(4)   NULL AFTER `estatica_sistema` , 
	CHANGE `dinamica` `dinamica` TINYINT(4)   NULL AFTER `tiene_cache_cliente` ;

/* Alter table in target */
ALTER TABLE `entidad_categoria` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_entidad` `id_entidad` INT(11)   NULL AFTER `id` ;

/* Alter table in target */
ALTER TABLE `entidad_categoria_valor_entidad` 
	CHANGE `id_entidad_categoria_valor_posible` `id_entidad_categoria_valor_posible` INT(11)   NULL FIRST , 
	CHANGE `id_entidad_origen` `id_entidad_origen` BIGINT(20)   NULL AFTER `id_entidad_categoria_valor_posible` ;

/* Alter table in target */
ALTER TABLE `entidad_categoria_valor_posible` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_entidad_categoria` `id_entidad_categoria` INT(11)   NULL AFTER `id` ;

/* Alter table in target */
ALTER TABLE `estado` 
	CHANGE `id` `id` INT(11)   NOT NULL FIRST , 
	CHANGE `activo` `activo` TINYINT(4)   NULL AFTER `d_estado` ;

/* Alter table in target */
ALTER TABLE `estado_asiento` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `activo` `activo` TINYINT(4)   NULL DEFAULT 1 AFTER `d_estado_asiento` ;

/* Alter table in target */
ALTER TABLE `estado_cheque` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `estado_chequera` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `estado_comprobante` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `activo` `activo` TINYINT(6)   NOT NULL DEFAULT 0 AFTER `d_estado_comprobante` ;

/* Alter table in target */
ALTER TABLE `estado_persona` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_tipo_persona` `id_tipo_persona` INT(11)   NULL AFTER `d_estado_persona` , 
	CHANGE `por_defecto` `por_defecto` TINYINT(4)   NULL AFTER `id_tipo_persona` ;

/* Alter table in target */
ALTER TABLE `estado_rrhh_liquidacion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `estado_tipo_cheque` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_estado_cheque` `id_estado_cheque` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_tipo_cheque` `id_tipo_cheque` INT(11)   NOT NULL AFTER `id_estado_cheque` ;

/* Alter table in target */
ALTER TABLE `estado_tipo_comprobante` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_estado_comprobante` `id_estado_comprobante` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_tipo_comprobante` `id_tipo_comprobante` INT(11)   NOT NULL AFTER `id_estado_comprobante` , 
	CHANGE `parent_id` `parent_id` INT(11)   NULL AFTER `id_tipo_comprobante` ;

/* Alter table in target */
ALTER TABLE `familia_producto` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_otro_sistema` `id_otro_sistema` INT(11)   NULL AFTER `codigo` ;

/* Alter table in target */
ALTER TABLE `filtro` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `forma_pago` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `vencimiento` `vencimiento` INT(11)   NULL AFTER `d_forma_pago` , 
	CHANGE `id_valor` `id_valor` INT(11)   NULL AFTER `interes` ;

/* Alter table in target */
ALTER TABLE `funcion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_tipo_funcion` `id_tipo_funcion` INT(11)   NULL AFTER `d_funcion` ;

/* Alter table in target */
ALTER TABLE `funcion_rol` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rol` `id_rol` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_funcion` `id_funcion` INT(11)   NOT NULL AFTER `id_rol` ;

/* Alter table in target */
ALTER TABLE `gen_audi` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `historico_login` 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL FIRST , 
	CHANGE `id_rol` `id_rol` INT(11)   NULL AFTER `nombre` ;

/* Alter table in target */
ALTER TABLE `impuesto` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_tipo_impuesto` `id_tipo_impuesto` INT(11)   NOT NULL DEFAULT 0 COMMENT 'no_visible' AFTER `codigo_impuesto` , 
	CHANGE `id_sistema` `id_sistema` INT(11)   NOT NULL DEFAULT 0 COMMENT 'no_visible' AFTER `id_tipo_impuesto` , 
	CHANGE `id_cuenta_contable` `id_cuenta_contable` INT(11)   NULL COMMENT 'no_visible' AFTER `id_sistema` , 
	CHANGE `id_estado` `id_estado` INT(11)   NOT NULL DEFAULT 0 COMMENT 'no_visible' AFTER `id_cuenta_contable` , 
	CHANGE `id_impuesto_geografia` `id_impuesto_geografia` INT(11)   NULL AFTER `id_estado` , 
	CHANGE `Fl_item` `Fl_item` SMALLINT(6)   NULL DEFAULT 0 COMMENT 'no_visible' AFTER `id_impuesto_geografia` , 
	CHANGE `Fl_comprobante` `Fl_comprobante` SMALLINT(6)   NULL DEFAULT 0 COMMENT 'no_visible' AFTER `Fl_item` , 
	CHANGE `Fl_acumulado` `Fl_acumulado` SMALLINT(6)   NULL DEFAULT 0 COMMENT 'no_visible' AFTER `Fl_comprobante` , 
	CHANGE `Fl_importe_total` `Fl_importe_total` SMALLINT(4)   NULL DEFAULT 0 COMMENT 'no_visible' AFTER `Fl_acumulado` , 
	CHANGE `Fl_proporcional` `Fl_proporcional` SMALLINT(6)   NULL DEFAULT 0 COMMENT 'no_visible' AFTER `Fl_importe_total` , 
	CHANGE `Fl_sifere` `Fl_sifere` SMALLINT(6)   NULL DEFAULT 0 COMMENT 'no_visible' AFTER `Fl_proporcional` , 
	CHANGE `Fl_sicore` `Fl_sicore` SMALLINT(6)   NULL DEFAULT 0 COMMENT 'no_visible' AFTER `Fl_sifere` , 
	CHANGE `Fl_arib` `Fl_arib` SMALLINT(6)   NULL DEFAULT 0 COMMENT 'no_visible' AFTER `Fl_sicore` , 
	CHANGE `Fl_pago` `Fl_pago` SMALLINT(6)   NULL DEFAULT 0 COMMENT 'no_visible' AFTER `Fl_arib` , 
	CHANGE `visible` `visible` TINYINT(4)   NULL AFTER `Fl_pago` , 
	CHANGE `importa_archivo` `importa_archivo` TINYINT(4)   NULL AFTER `visible` , 
	CHANGE `autogenerado` `autogenerado` TINYINT(4)   NULL DEFAULT 1 AFTER `base_imponible_desde` , 
	CHANGE `id_sub_tipo_impuesto` `id_sub_tipo_impuesto` INT(11)   NULL AFTER `codigo_regimen` , 
	CHANGE `global_cliente` `global_cliente` TINYINT(4)   NULL AFTER `id_sub_tipo_impuesto` , 
	CHANGE `global_proveedor` `global_proveedor` TINYINT(4)   NULL AFTER `global_cliente` , 
	CHANGE `id_tipo_comercializacion` `id_tipo_comercializacion` INT(11)   NULL AFTER `global_proveedor` , 
	CHANGE `id_provincia` `id_provincia` INT(11)   NULL AFTER `id_tipo_comercializacion` , 
	CHANGE `pertenece_escala` `pertenece_escala` TINYINT(4)   NULL DEFAULT 0 AFTER `alicuota` ;

/* Alter table in target */
ALTER TABLE `impuesto_geografia` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `codigo_afip` `codigo_afip` INT(11)   NULL AFTER `d_impuesto_geografia` ;

/* Alter table in target */
ALTER TABLE `iva` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `tipo_alicuota_afip` `tipo_alicuota_afip` INT(11)   NULL DEFAULT 5 COMMENT '5 es el 21%' AFTER `d_iva` ;

/* Alter table in target */
ALTER TABLE `licencia` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `tiempo_duracion_dias` `tiempo_duracion_dias` INT(11)   NOT NULL AFTER `descripcion` , 
	CHANGE `infinita` `infinita` TINYINT(4)   NULL AFTER `tiempo_duracion_dias` ;

/* Alter table in target */
ALTER TABLE `lista_precio` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `activo` `activo` TINYINT(11)   NOT NULL DEFAULT 0 AFTER `fecha_vencimiento` , 
	CHANGE `id_sistema` `id_sistema` INT(4)   NULL DEFAULT 1 AFTER `activo` , 
	CHANGE `id_lista_precio_padre` `id_lista_precio_padre` INT(11)   NULL AFTER `id_sistema` , 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL AFTER `fecha_actualizacion` , 
	CHANGE `id_tipo_lista_precio` `id_tipo_lista_precio` INT(1)   NULL AFTER `id_usuario` , 
	CHANGE `por_defecto` `por_defecto` TINYINT(4)   NULL AFTER `id_tipo_lista_precio` , 
	CHANGE `id_persona` `id_persona` INT(11)   NULL AFTER `por_defecto` , 
	CHANGE `permite_bonificacion_volumen` `permite_bonificacion_volumen` TINYINT(4)   NULL AFTER `id_persona` , 
	CHANGE `id_moneda` `id_moneda` INT(11)   NULL AFTER `permite_bonificacion_volumen` , 
	CHANGE `cantidad_decimales` `cantidad_decimales` TINYINT(4)   NULL DEFAULT 2 AFTER `id_moneda` ;

/* Alter table in target */
ALTER TABLE `lista_precio_producto` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_lista_precio` `id_lista_precio` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_producto` `id_producto` INT(11)   NOT NULL AFTER `id_lista_precio` , 
	CHANGE `id_moneda` `id_moneda` INT(11)   NOT NULL DEFAULT 2 AFTER `precio_minimo` , 
	CHANGE `porcentaje_bonificacion_1_hasta` `porcentaje_bonificacion_1_hasta` INT(11)   NULL AFTER `cantidad_bonificacion_1_hasta` , 
	CHANGE `porcentaje_bonificacion_2_hasta` `porcentaje_bonificacion_2_hasta` INT(11)   NULL AFTER `cantidad_bonificacion_2_hasta` , 
	CHANGE `porcentaje_bonificacion_3_hasta` `porcentaje_bonificacion_3_hasta` INT(11)   NULL AFTER `cantidad_bonificacion_3_hasta` , 
	CHANGE `porcentaje_bonificacion_4_hasta` `porcentaje_bonificacion_4_hasta` INT(11)   NULL AFTER `cantidad_bonificacion_4_hasta` , 
	CHANGE `porcentaje_bonificacion_5_hasta` `porcentaje_bonificacion_5_hasta` INT(11)   NULL AFTER `cantidad_bonificacion_5_hasta` , 
	CHANGE `porcentaje_bonificacion_6_hasta` `porcentaje_bonificacion_6_hasta` INT(11)   NULL AFTER `cantidad_bonificacion_6_hasta` ;

/* Alter table in target */
ALTER TABLE `localidad` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_zona` `id_zona` INT(11)   NULL DEFAULT 0 AFTER `d_localidad` , 
	CHANGE `id_provincia` `id_provincia` INT(11)   NOT NULL DEFAULT 0 AFTER `id_zona` , 
	CHANGE `codigo_postal` `codigo_postal` INT(5)   NULL DEFAULT 0 AFTER `id_provincia` ;

/* Alter table in target */
ALTER TABLE `lote` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `utilizado` `utilizado` TINYINT(4)   NULL AFTER `fecha_emision` , 
	CHANGE `id_tipo_lote` `id_tipo_lote` INT(11)   NULL COMMENT 'no_visible' AFTER `d_lote` , 
	CHANGE `tiene_certificado` `tiene_certificado` TINYINT(4)   NULL AFTER `codigo` , 
	CHANGE `validado` `validado` TINYINT(4)   NULL AFTER `tiene_certificado` ;

/* Alter table in target */
ALTER TABLE `mail_to_send` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_comprobante` `id_comprobante` BIGINT(20)   NULL AFTER `id` , 
	CHANGE `enviado` `enviado` INT(11)   NULL AFTER `id_comprobante` , 
	CHANGE `intento_fallidos` `intento_fallidos` INT(11)   NULL AFTER `enviado` ;

/* Alter table in target */
ALTER TABLE `marca` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `media_file` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_media_file_tipo` `id_media_file_tipo` INT(11)   NULL AFTER `tamanio` , 
	CHANGE `id_estado` `id_estado` INT(11)   NULL AFTER `fecha_creacion` , 
	CHANGE `permisos_admin` `permisos_admin` TINYINT(4)   NULL AFTER `extension` ;

/* Alter table in target */
ALTER TABLE `media_file_tipo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `mime_type` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `modulo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `habilitado` `habilitado` TINYINT(4)   NULL AFTER `d_modulo` , 
	CHANGE `cantidad_decimales_formateo` `cantidad_decimales_formateo` INT(11)   NULL DEFAULT 2 AFTER `habilitado` ;

/* Alter table in target */
ALTER TABLE `moneda` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `activa_asiento` `activa_asiento` TINYINT(4)   NULL AFTER `simbolo_internacional` , 
	CHANGE `permite_factura_electronica` `permite_factura_electronica` TINYINT(4)   NULL DEFAULT 0 AFTER `activa_asiento` ;

/* Alter table in target */
ALTER TABLE `movimiento` 
	CHANGE `id` `id` BIGINT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_movimiento_tipo` `id_movimiento_tipo` INT(11)   NULL AFTER `lotes` , 
	CHANGE `id_producto` `id_producto` INT(11)   NULL AFTER `id_movimiento_tipo` , 
	CHANGE `id_componente` `id_componente` INT(11)   NULL AFTER `id_producto` , 
	CHANGE `id_materia_prima` `id_materia_prima` INT(12)   NULL AFTER `id_componente` , 
	CHANGE `id_orden_armado` `id_orden_armado` INT(11)   NULL AFTER `fecha` , 
	CHANGE `id_orden_produccion` `id_orden_produccion` INT(11)   NULL AFTER `id_orden_armado` , 
	CHANGE `id_factura` `id_factura` BIGINT(11)   NULL AFTER `id_orden_produccion` , 
	CHANGE `id_lote` `id_lote` BIGINT(20)   NULL AFTER `observacion` , 
	CHANGE `id_orden_compra` `id_orden_compra` BIGINT(20)   NULL AFTER `id_lote` , 
	CHANGE `id_comprobante` `id_comprobante` BIGINT(20)   NULL AFTER `id_orden_compra` , 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL AFTER `id_comprobante` , 
	CHANGE `id_movimiento_padre` `id_movimiento_padre` BIGINT(20)   NULL AFTER `nro_informe_recepcion` , 
	CHANGE `id_deposito_origen` `id_deposito_origen` INT(11)   NULL AFTER `id_movimiento_padre` , 
	CHANGE `id_deposito_destino` `id_deposito_destino` INT(11)   NULL AFTER `id_deposito_origen` , 
	CHANGE `id_comprobante_item` `id_comprobante_item` BIGINT(20)   NULL AFTER `id_deposito_destino` ;

/* Alter table in target */
ALTER TABLE `movimiento_item` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_movimiento` `id_movimiento` BIGINT(20)   NOT NULL AFTER `id` , 
	CHANGE `id_deposito` `id_deposito` INT(11)   NOT NULL AFTER `id_movimiento` , 
	CHANGE `suma` `suma` TINYINT(4)   NOT NULL AFTER `id_deposito` ;

/* Alter table in target */
ALTER TABLE `movimiento_lote` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_movimiento` `id_movimiento` BIGINT(20)   NULL AFTER `id` , 
	CHANGE `id_lote` `id_lote` BIGINT(20)   NULL AFTER `id_movimiento` ;

/* Alter table in target */
ALTER TABLE `movimiento_tipo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `visible` `visible` TINYINT(4)   NULL DEFAULT 1 COMMENT 'no_visible' AFTER `d_movimiento_tipo` , 
	CHANGE `id_movimiento_tipo_revertir` `id_movimiento_tipo_revertir` INT(11)   NULL COMMENT 'no_visible' AFTER `detalle` , 
	CHANGE `automatico` `automatico` TINYINT(4)   NULL COMMENT 'si este campo esta en true. no se puede generar desde frmMovimientos' AFTER `id_movimiento_tipo_revertir` ;

/* Alter table in target */
ALTER TABLE `nivel_autorizado_informacion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `nota_usuario` 
	CHANGE `id` `id` BIGINT(20)   NULL FIRST , 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL AFTER `id` , 
	CHANGE `id_estado` `id_estado` INT(11)   NULL AFTER `fecha_creacion` ;

/* Alter table in target */
ALTER TABLE `operador_funcion_matematica` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `orden_armado` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_producto` `id_producto` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `cantidad_armar_planificado` `cantidad_armar_planificado` INT(11)   NULL AFTER `fecha_emision` , 
	CHANGE `cantidad` `cantidad` INT(11)   NULL AFTER `cantidad_armar_planificado` , 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL AFTER `cantidad` , 
	CHANGE `id_estado` `id_estado` INT(11)   NULL AFTER `id_usuario` , 
	CHANGE `id_pedido_interno_fox` `id_pedido_interno_fox` BIGINT(20) UNSIGNED   NULL AFTER `fecha_entrega` , 
	CHANGE `chk_pedido_interno` `chk_pedido_interno` TINYINT(4)   NULL AFTER `observacion` , 
	CHANGE `id_pedido_interno` `id_pedido_interno` BIGINT(20) UNSIGNED   NULL AFTER `chk_pedido_interno` , 
	CHANGE `id_tipo_orden_armado` `id_tipo_orden_armado` TINYINT(4)   NULL AFTER `id_pedido_interno` , 
	CHANGE `genera_movimiento_stock` `genera_movimiento_stock` TINYINT(4)   NULL AFTER `presion_ensayo` ;

/* Alter table in target */
ALTER TABLE `orden_armado_item` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_orden_armado` `id_orden_armado` INT(11)   NULL AFTER `id` , 
	CHANGE `id_componente` `id_componente` INT(11)   NULL AFTER `id_orden_armado` ;

/* Alter table in target */
ALTER TABLE `orden_produccion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_componente` `id_componente` INT(11)   NOT NULL COMMENT 'no_visible' AFTER `id` , 
	CHANGE `cantidad` `cantidad` INT(11)   NULL AFTER `id_componente` , 
	CHANGE `id_estado` `id_estado` INT(11)   NOT NULL COMMENT 'no_visible' AFTER `observacion` ;

/* Alter table in target */
ALTER TABLE `origen` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `interno` `interno` TINYINT(4)   NULL AFTER `d_origen` ;

/* Alter table in target */
ALTER TABLE `pais` 
	CHANGE `id` `id` INT(10)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `activo` `activo` TINYINT(4)   NULL DEFAULT 0 AFTER `codigo2` , 
	CHANGE `cuit_persona_juridica` `cuit_persona_juridica` INT(11)   NULL AFTER `activo` , 
	CHANGE `cuit_persona_fisica` `cuit_persona_fisica` INT(11)   NULL AFTER `cuit_persona_juridica` , 
	CHANGE `codigo_afip` `codigo_afip` INT(11)   NULL AFTER `cuit_persona_fisica` ;

/* Alter table in target */
ALTER TABLE `parametro_contabilidad` 
	CHANGE `id` `id` INT(11)   NULL FIRST , 
	CHANGE `id_modulo` `id_modulo` INT(11)   NULL AFTER `id` ;

/* Alter table in target */
ALTER TABLE `periodo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `periodo_contable` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_ejercicio_contable` `id_ejercicio_contable` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `nro_periodo` `nro_periodo` INT(11)   NOT NULL AFTER `id_ejercicio_contable` , 
	CHANGE `id_asiento_ultimo` `id_asiento_ultimo` BIGINT(20)   NULL AFTER `fecha_cierre` , 
	CHANGE `cerrado` `cerrado` TINYINT(4)   NOT NULL AFTER `id_asiento_ultimo` , 
	CHANGE `disponible` `disponible` TINYINT(4)   NOT NULL AFTER `cerrado` , 
	CHANGE `check_comprobantes_al_cierre` `check_comprobantes_al_cierre` TINYINT(4)   NULL DEFAULT 1 AFTER `fecha_disponible_cierre` ;


/* Alter table in target */
ALTER TABLE `persona` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_pais` `id_pais` INT(11)   NULL AFTER `email` , 
	CHANGE `id_provincia` `id_provincia` INT(11)   NULL AFTER `id_pais` , 
	CHANGE `cuit` `cuit` BIGINT(20)   NULL AFTER `posicion` , 
	CHANGE `id_cuenta` `id_cuenta` INT(3)   NULL AFTER `ib` , 
	CHANGE `id_tipo_iva` `id_tipo_iva` INT(4)   NULL DEFAULT 0 AFTER `cod_postal` , 
	CHANGE `id_persona_figura` `id_persona_figura` INT(11)   NOT NULL DEFAULT 2 AFTER `id_tipo_iva` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL DEFAULT 1 AFTER `id_persona_figura` , 
	CHANGE `id_tipo_documento` `id_tipo_documento` INT(11)   NULL DEFAULT 3 AFTER `website` , 
	CHANGE `cuenta_corriente` `cuenta_corriente` TINYINT(4)   NULL DEFAULT 1 AFTER `observaciones` , 
	CHANGE `id_persona_categoria` `id_persona_categoria` INT(11)   NULL AFTER `observacion_extra` , 
	CHANGE `id_cuenta_contable` `id_cuenta_contable` INT(11)   NULL AFTER `id_persona_categoria` , 
	CHANGE `id_moneda` `id_moneda` INT(11)   NULL DEFAULT 2 AFTER `id_cuenta_contable` , 
	CHANGE `requiere_orden_compra` `requiere_orden_compra` TINYINT(4)   NULL AFTER `id_moneda` , 
	CHANGE `id_cuenta_contable_cuenta_corriente` `id_cuenta_contable_cuenta_corriente` INT(11)   NULL AFTER `requiere_orden_compra` , 
	CHANGE `id_transportista` `id_transportista` INT(11)   NULL AFTER `monto_autorizacion_orden_compra` , 
	CHANGE `flete_paga_proveedor` `flete_paga_proveedor` TINYINT(4)   NULL AFTER `id_transportista` , 
	CHANGE `entrega_en_domicilio` `entrega_en_domicilio` TINYINT(4)   NULL AFTER `flete_paga_proveedor` , 
	CHANGE `id_lista_precio` `id_lista_precio` INT(11)   NULL DEFAULT 2 AFTER `entrega_en_domicilio` , 
	CHANGE `id_forma_pago` `id_forma_pago` INT(11)   NULL AFTER `descuento` , 
	CHANGE `id_media_file` `id_media_file` INT(11)   NULL AFTER `id_forma_pago` , 
	CHANGE `valoracion` `valoracion` INT(11)   NULL AFTER `id_media_file` , 
	CHANGE `id_iva` `id_iva` INT(11)   NULL AFTER `fecha_creacion` , 
	CHANGE `persona_extranjero` `persona_extranjero` TINYINT(4)   NULL AFTER `pasaporte_fecha_vto` , 
	CHANGE `id_tipo_actividad` `id_tipo_actividad` INT(11)   NULL AFTER `persona_extranjero` , 
	CHANGE `cuenta_corriente_deshabilitada` `cuenta_corriente_deshabilitada` TINYINT(4)   NULL AFTER `limiite_credito_documento` , 
	CHANGE `dias_limite_cuenta_corriente` `dias_limite_cuenta_corriente` INT(11)   NULL AFTER `cuenta_corriente_deshabilitada` , 
	CHANGE `id_tipo_persona` `id_tipo_persona` INT(11)   NOT NULL AFTER `dias_limite_cuenta_corriente` , 
	CHANGE `id_migracion` `id_migracion` INT(11)   NULL AFTER `observacion` , 
	CHANGE `id_area` `id_area` INT(11)   NULL AFTER `apellido` , 
	CHANGE `id_tipo_contrato` `id_tipo_contrato` INT(11)   NULL AFTER `direccion_longitud` , 
	CHANGE `id_persona_padre` `id_persona_padre` INT(11)   NULL AFTER `id_tipo_contrato` , 
	CHANGE `id_estado_persona` `id_estado_persona` INT(11)   NULL AFTER `d_persona` , 
	CHANGE `id_situacion_ib` `id_situacion_ib` INT(11)   NULL AFTER `id_estado_persona` , 
	CHANGE `id_otro_sistema` `id_otro_sistema` INT(11)   NULL AFTER `codigo` , 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL AFTER `nro_pasaporte` , 
	CHANGE `id_tipo_comercializacion_por_defecto` `id_tipo_comercializacion_por_defecto` INT(11)   NULL AFTER `id_usuario` , 
	CHANGE `id_condicion_pago` `id_condicion_pago` INT(11)   NULL AFTER `id_tipo_comercializacion_por_defecto` , 
	CHANGE `meli_id_usuario` `meli_id_usuario` BIGINT(20)   NULL AFTER `meli_nickname` , 
	CHANGE `id_persona_origen` `id_persona_origen` INT(11)   NULL AFTER `lugar_entrega` , 
	CHANGE `id_rrhh_convenio` `id_rrhh_convenio` INT(11)   NULL AFTER `fecha_antiguedad` , 
	CHANGE `id_rrhh_condicion_sicoss` `id_rrhh_condicion_sicoss` INT(11)   NULL AFTER `id_rrhh_convenio` , 
	CHANGE `id_rrhh_situacion_sicoss` `id_rrhh_situacion_sicoss` INT(11)   NULL AFTER `id_rrhh_condicion_sicoss` , 
	CHANGE `id_rrhh_actividad` `id_rrhh_actividad` INT(11)   NULL AFTER `id_rrhh_situacion_sicoss` , 
	CHANGE `id_rrhh_obra_social` `id_rrhh_obra_social` INT(11)   NULL AFTER `rrhh_basico` , 
	CHANGE `id_rrhh_lugar_pago` `id_rrhh_lugar_pago` INT(11)   NULL AFTER `id_rrhh_obra_social` , 
	CHANGE `id_persona_parentezco` `id_persona_parentezco` INT(11)   NULL AFTER `id_rrhh_lugar_pago` , 
	CHANGE `id_persona_estado_civil` `id_persona_estado_civil` INT(11)   NULL AFTER `id_persona_parentezco` , 
	CHANGE `id_rrhh_clase_legajo` `id_rrhh_clase_legajo` INT(11)   NULL AFTER `id_persona_estado_civil` , 
	CHANGE `id_persona_sexo` `id_persona_sexo` INT(11)   NULL AFTER `id_rrhh_clase_legajo` , 
	CHANGE `id_rrhh_categoria` `id_rrhh_categoria` INT(11)   NULL AFTER `id_persona_sexo` , 
	CHANGE `numero_documento` `numero_documento` INT(11)   NULL AFTER `id_rrhh_categoria` , 
	CHANGE `id_rrhh_sindicato` `id_rrhh_sindicato` INT(11)   NULL AFTER `numero_documento` , 
	CHANGE `id_rrhh_forma_pago` `id_rrhh_forma_pago` INT(11)   NULL AFTER `id_rrhh_sindicato` , 
	CHANGE `trabajador_convencionado` `trabajador_convencionado` TINYINT(4)   NULL AFTER `fecha_fin_vacaciones` , 
	CHANGE `seguro_colectivo_vida` `seguro_colectivo_vida` TINYINT(4)   NULL AFTER `trabajador_convencionado` , 
	CHANGE `id_rrhh_situacion_revista` `id_rrhh_situacion_revista` INT(11)   NULL AFTER `seguro_colectivo_vida` , 
	CHANGE `rrhh_situacion_revista_dia_inicio` `rrhh_situacion_revista_dia_inicio` INT(11)   NULL AFTER `id_rrhh_situacion_revista` , 
	CHANGE `id_rrhh_situacion_revista2` `id_rrhh_situacion_revista2` INT(11)   NULL AFTER `rrhh_situacion_revista_dia_inicio` , 
	CHANGE `rrhh_situacion_revista_dia_inicio2` `rrhh_situacion_revista_dia_inicio2` INT(11)   NULL AFTER `id_rrhh_situacion_revista2` , 
	CHANGE `id_rrhh_situacion_revista3` `id_rrhh_situacion_revista3` INT(11)   NULL AFTER `rrhh_situacion_revista_dia_inicio2` , 
	CHANGE `rrhh_situacion_revista_dia_inicio3` `rrhh_situacion_revista_dia_inicio3` INT(11)   NULL AFTER `id_rrhh_situacion_revista3`;

/* Alter table in target */
ALTER TABLE `persona_categoria` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_tipo_persona` `id_tipo_persona` INT(11)   NULL AFTER `d_persona_categoria` ;

/* Alter table in target */
ALTER TABLE `persona_estado_civil` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `persona_figura` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `persona_impuesto_alicuota` 
	CHANGE `id` `id` BIGINT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_impuesto` `id_impuesto` INT(11)   NOT NULL DEFAULT 0 AFTER `porcentaje_alicuota` , 
	CHANGE `id_persona` `id_persona` INT(11)   NOT NULL DEFAULT 0 AFTER `id_impuesto` , 
	CHANGE `sin_vencimiento` `sin_vencimiento` TINYINT(4)   NULL AFTER `id_persona` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL DEFAULT 1 AFTER `sin_vencimiento` ;

/* Alter table in target */
ALTER TABLE `persona_impuesto_alicuota_temporal` 
	CHANGE `cuit` `cuit` BIGINT(20)   NULL AFTER `fecha_vigencia_hasta` , 
	CHANGE `id_persona` `id_persona` INT(11)   NULL AFTER `final` ;

/* Alter table in target */
ALTER TABLE `persona_impuesto_alicuota_temporal_caba` 
	CHANGE `cuit` `cuit` BIGINT(20)   NULL AFTER `fecha_vigencia_hasta` , 
	CHANGE `id_persona` `id_persona` INT(11)   NULL AFTER `razon_social` ;

/* Alter table in target */
ALTER TABLE `persona_origen` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_tipo_persona` `id_tipo_persona` INT(11)   NULL AFTER `d_persona_origen` ;

/* Alter table in target */
ALTER TABLE `persona_parentezco` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `persona_sexo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `persona_valor_tipo_comprobante` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_persona` `id_persona` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_valor` `id_valor` INT(11)   NOT NULL AFTER `id_persona` , 
	CHANGE `id_tipo_comprobante` `id_tipo_comprobante` INT(11)   NOT NULL AFTER `id_valor` ;

/* Alter table in target */
ALTER TABLE `producto` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_unidad` `id_unidad` INT(11)   NULL DEFAULT 1 COMMENT 'no_visible' AFTER `peso` , 
	CHANGE `id_iva` `id_iva` INT(11)   NULL DEFAULT 1 COMMENT 'no_visible' AFTER `id_unidad` , 
	CHANGE `activo` `activo` TINYINT(4)   NOT NULL DEFAULT 1 COMMENT 'no_visible' AFTER `id_iva` , 
	CHANGE `id_familia_producto` `id_familia_producto` INT(11)   NULL COMMENT 'no_visible' AFTER `d_producto_extensa` , 
	CHANGE `id_categoria` `id_categoria` INT(11)   NULL COMMENT 'no_visible' AFTER `stock_minimo` , 
	CHANGE `id_origen` `id_origen` INT(11)   NULL DEFAULT 1 COMMENT 'no_visible' AFTER `id_categoria` , 
	CHANGE `id_media_file` `id_media_file` BIGINT(11)   NULL COMMENT 'no_visible' AFTER `codigo_barra` , 
	CHANGE `publicar_internet` `publicar_internet` TINYINT(4)   NULL COMMENT 'no_visible' AFTER `codigo_barra5` , 
	CHANGE `permite_stock_negativo` `permite_stock_negativo` TINYINT(4)   NULL COMMENT 'no_visible' AFTER `publicar_internet` , 
	CHANGE `requiere_despacho` `requiere_despacho` TINYINT(4)   NULL COMMENT 'no_visible' AFTER `permite_stock_negativo` , 
	CHANGE `puntos_premio` `puntos_premio` INT(11)   NULL COMMENT 'no_visible' AFTER `stock_maximo` , 
	CHANGE `puntos_premio_dinero` `puntos_premio_dinero` INT(11)   NULL COMMENT 'no_visible' AFTER `puntos_premio` , 
	CHANGE `id_marca` `id_marca` INT(11)   NULL COMMENT 'no_visible' AFTER `puntos_premio_dinero` , 
	CHANGE `compuesto` `compuesto` TINYINT(4)   NULL COMMENT 'no_visible' AFTER `id_marca` , 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL AFTER `fecha_ultima_modificacion` , 
	CHANGE `id_moneda_costo` `id_moneda_costo` INT(11)   NULL AFTER `impuesto_interno_porcentual` , 
	CHANGE `id_componente_migracion` `id_componente_migracion` INT(11)   NULL AFTER `id_moneda_costo` , 
	CHANGE `id_producto_tipo` `id_producto_tipo` INT(11)   NOT NULL DEFAULT 1 AFTER `d_categoria_extra` , 
	CHANGE `id_materia_prima_migracion` `id_materia_prima_migracion` INT(11)   NULL AFTER `id_producto_tipo` , 
	CHANGE `revision` `revision` TINYINT(4)   NULL AFTER `id_materia_prima_migracion` , 
	CHANGE `id_cuenta_contable_compra` `id_cuenta_contable_compra` INT(11)   NULL AFTER `fecha_revision` , 
	CHANGE `id_cuenta_contable_venta` `id_cuenta_contable_venta` INT(11)   NULL AFTER `id_cuenta_contable_compra` , 
	CHANGE `id_sub_familia_producto` `id_sub_familia_producto` INT(11)   NULL AFTER `id_cuenta_contable_venta` , 
	CHANGE `id_tipo_comercializacion` `id_tipo_comercializacion` INT(11)   NULL DEFAULT 1 AFTER `id_sub_familia_producto` , 
	CHANGE `es_contable` `es_contable` TINYINT(4)   NULL DEFAULT 1 AFTER `id_tipo_comercializacion` , 
	CHANGE `es_imprimible` `es_imprimible` TINYINT(4)   NULL DEFAULT 1 AFTER `es_contable` , 
	CHANGE `id_producto_clasificacion` `id_producto_clasificacion` INT(11)   NULL AFTER `es_imprimible` , 
	CHANGE `id_otro_sistema` `id_otro_sistema` INT(11)   NULL AFTER `id_producto_clasificacion` , 
	CHANGE `id_talle` `id_talle` INT(11)   NULL AFTER `id_otro_sistema` , 
	CHANGE `id_color` `id_color` INT(11)   NULL AFTER `id_talle` , 
	CHANGE `id_area` `id_area` INT(11)   NULL AFTER `id_color` , 
	CHANGE `id_periodo` `id_periodo` INT(11)   NULL AFTER `meli_url_producto` ;

/* Alter table in target */
ALTER TABLE `producto_clasificacion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `tiene_stock` `tiene_stock` TINYINT(4)   NULL DEFAULT 1 AFTER `codigo` , 
	CHANGE `tiene_relacion` `tiene_relacion` TINYINT(4)   NULL AFTER `tiene_stock` ;

/* Alter table in target */
ALTER TABLE `producto_codigo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_producto` `id_producto` INT(11)   NULL AFTER `id` ;

/* Alter table in target */
ALTER TABLE `producto_relacion` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_producto_padre` `id_producto_padre` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_producto_hijo` `id_producto_hijo` INT(11)   NOT NULL AFTER `id_producto_padre` ;

/* Alter table in target */
ALTER TABLE `producto_tipo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `tiene_stock` `tiene_stock` TINYINT(4)   NULL AFTER `codigo` , 
	CHANGE `id_destino_producto` `id_destino_producto` INT(11)   NULL AFTER `tiene_stock` , 
	CHANGE `interno` `interno` TINYINT(4)   NULL AFTER `id_destino_producto` , 
	CHANGE `remitible` `remitible` TINYINT(4)   NULL AFTER `interno` , 
	CHANGE `id_tipo_comercializacion` `id_tipo_comercializacion` INT(11)   NULL AFTER `controllerStock` , 
	CHANGE `requiere_conformidad` `requiere_conformidad` TINYINT(4)   NULL AFTER `id_tipo_comercializacion` , 
	CHANGE `id_deposito_fijo_principal` `id_deposito_fijo_principal` INT(11)   NULL AFTER `requiere_conformidad` , 
	CHANGE `id_deposito_fijo_complementario` `id_deposito_fijo_complementario` INT(11)   NULL AFTER `id_deposito_fijo_principal` ;

/* Alter table in target */
ALTER TABLE `provincia` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `extranjero` `extranjero` TINYINT(4)   NULL AFTER `d_provincia` , 
	CHANGE `id_pais` `id_pais` INT(11)   NULL AFTER `extranjero` , 
	CHANGE `codigo_afip` `codigo_afip` INT(11)   NULL AFTER `abreviatura` , 
	CHANGE `sifere_jurisdiccion` `sifere_jurisdiccion` INT(11)   NULL AFTER `minimo_imponible_retencion` ;

/* Alter table in target */
ALTER TABLE `punto_venta` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_dato_empresa` `id_dato_empresa` INT(11)   NULL DEFAULT 1 AFTER `id` , 
	CHANGE `numero` `numero` INT(11)   NULL AFTER `id_dato_empresa` , 
	CHANGE `id_pais` `id_pais` INT(11)   NULL DEFAULT 9 AFTER `numero` , 
	CHANGE `id_provincia` `id_provincia` INT(11)   NULL DEFAULT 1 AFTER `id_pais` , 
	CHANGE `cai` `cai` BIGINT(20)   NULL AFTER `direccion` , 
	CHANGE `factura_electronica` `factura_electronica` TINYINT(4)   NULL DEFAULT 0 AFTER `fecha_vencimiento` , 
	CHANGE `id_moneda` `id_moneda` INT(11)   NULL AFTER `factura_electronica` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL AFTER `id_moneda` ;

/* Alter table in target */
ALTER TABLE `qa_accion_correctiva` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_qa_origen_falla` `id_qa_origen_falla` INT(11)   NULL COMMENT 'no_visible' AFTER `fecha` , 
	CHANGE `id_area` `id_area` INT(11)   NULL COMMENT 'no_visible' AFTER `accion_correctiva_adoptada` , 
	CHANGE `id_reclamo` `id_reclamo` INT(11)   NULL COMMENT 'no_visible' AFTER `id_area` , 
	CHANGE `id_responsable_cumplimiento` `id_responsable_cumplimiento` INT(11)   NULL COMMENT 'no_visible' AFTER `id_reclamo` , 
	CHANGE `id_efectividad_responsable` `id_efectividad_responsable` INT(11)   NULL COMMENT 'no_visible' AFTER `id_responsable_cumplimiento` , 
	CHANGE `id_estado` `id_estado` INT(11)   NULL COMMENT 'no_visible' AFTER `fecha_efectividad` ;

/* Alter table in target */
ALTER TABLE `qa_certificado_calidad` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `cantidad` `cantidad` INT(11)   NULL AFTER `fecha` , 
	CHANGE `id_pedido_interno` `id_pedido_interno` BIGINT(20) UNSIGNED   NULL AFTER `observaciones` , 
	CHANGE `id_estado` `id_estado` INT(11)   NULL AFTER `id_pedido_interno` , 
	CHANGE `id_producto` `id_producto` INT(11)   NULL AFTER `id_estado` , 
	CHANGE `id_persona` `id_persona` INT(11)   NULL AFTER `f5` , 
	CHANGE `id_orden_armado` `id_orden_armado` BIGINT(11)   NULL AFTER `id_persona` , 
	CHANGE `id_remito` `id_remito` BIGINT(20) UNSIGNED   NULL AFTER `id_orden_armado` , 
	CHANGE `id_comprobante_aux` `id_comprobante_aux` BIGINT(20)   NULL AFTER `id_remito` , 
	DROP FOREIGN KEY `fkestad`  , 
	DROP FOREIGN KEY `fkidprod`  ;
ALTER TABLE `qa_certificado_calidad`
	ADD CONSTRAINT `fkestad` 
	FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION , 
	ADD CONSTRAINT `fkidprod` 
	FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;


/* Alter table in target */
ALTER TABLE `qa_certificado_calidad_item` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_qa_certificado_calidad` `id_qa_certificado_calidad` BIGINT(20)   NULL COMMENT 'no_visible' AFTER `id` , 
	CHANGE `id_producto_hijo` `id_producto_hijo` INT(11)   NOT NULL COMMENT 'no_visible' AFTER `lote` , 
	CHANGE `n_item` `n_item` INT(11)   NULL AFTER `id_producto_hijo` , 
	CHANGE `orden` `orden` INT(11)   NULL COMMENT 'no_visible' AFTER `n_item` ;

/* Alter table in target */
ALTER TABLE `qa_conformidad` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_producto` `id_producto` INT(11)   NULL AFTER `id` , 
	CHANGE `id_proveedor` `id_proveedor` INT(11)   NULL AFTER `fecha` , 
	CHANGE `cantidad` `cantidad` INT(11)   NULL AFTER `id_proveedor` , 
	CHANGE `id_area` `id_area` INT(11)   NULL AFTER `cantidad` , 
	CHANGE `id_qa_origen_falla` `id_qa_origen_falla` INT(11)   NULL AFTER `id_area` , 
	CHANGE `id_estado` `id_estado` INT(11)   NULL AFTER `observacion` , 
	CHANGE `id_comprobante` `id_comprobante` BIGINT(20)   NULL AFTER `id_estado` ;

/* Alter table in target */
ALTER TABLE `qa_origen_falla` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `reclamo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_persona` `id_persona` INT(11)   NULL AFTER `id` , 
	CHANGE `id_tipo_reclamo` `id_tipo_reclamo` INT(11)   NULL AFTER `fecha` , 
	CHANGE `id_comprobante` `id_comprobante` BIGINT(11)   NULL AFTER `id_tipo_reclamo` , 
	CHANGE `anulado` `anulado` TINYINT(4)   NULL AFTER `detalle` , 
	CHANGE `nro_serv_tecnico` `nro_serv_tecnico` INT(11)   NULL AFTER `anulado` , 
	CHANGE `avisado` `avisado` TINYINT(4)   NULL AFTER `nro_serv_tecnico` ;

/* Alter table in target */
ALTER TABLE `reporte` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_mime_type` `id_mime_type` INT(11)   NULL AFTER `method` , 
	CHANGE `id_sub_tipo_impuesto` `id_sub_tipo_impuesto` INT(11)   NULL AFTER `id_mime_type` , 
	CHANGE `id_tipo_impuesto` `id_tipo_impuesto` INT(11)   NULL AFTER `id_sub_tipo_impuesto` ;

/* Alter table in target */
ALTER TABLE `reporte_tablero` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `activa` `activa` TINYINT(4)   NULL AFTER `d_reporte_tablero` , 
	CHANGE `muestra_inicio` `muestra_inicio` TINYINT(4)   NULL AFTER `id_menu` , 
	CHANGE `muestra_fin` `muestra_fin` TINYINT(4)   NULL AFTER `muestra_inicio` , 
	CHANGE `tipo_reporte_tablero` `tipo_reporte_tablero` INT(11)   NULL AFTER `muestra_fin` , 
	CHANGE `orden` `orden` INT(11)   NULL AFTER `js_widget` ;

/* Alter table in target */
ALTER TABLE `reporte_tablero_usuario` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_reporte_tablero` `id_reporte_tablero` INT(11)   NULL AFTER `id` , 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL AFTER `id_reporte_tablero` , 
	CHANGE `orden` `orden` INT(11)   NULL AFTER `id_usuario` ;

/* Alter table in target */
ALTER TABLE `request_log` 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL AFTER `controller` ;

/* Alter table in target */
ALTER TABLE `rol` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_nivel_autorizado_informacion` `id_nivel_autorizado_informacion` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `asignacion_geografica` `asignacion_geografica` INT(11)   NOT NULL AFTER `descripcion` , 
	CHANGE `solo_lectura` `solo_lectura` TINYINT(4)   NULL AFTER `codigo` ;

/* Alter table in target */
ALTER TABLE `rrhh_actividad` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `codigo` `codigo` INT(11)   NULL AFTER `id` ;

/* Alter table in target */
ALTER TABLE `rrhh_categoria` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_convenio` `id_rrhh_convenio` INT(11)   NULL AFTER `d_rrhh_categoria` , 
	CHANGE `id_rrhh_tipo_liquidacion` `id_rrhh_tipo_liquidacion` INT(11)   NULL AFTER `importe` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL DEFAULT 1 AFTER `codigo` ;

/* Alter table in target */
ALTER TABLE `rrhh_clase_legajo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `rrhh_clase_liquidacion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `activo` `activo` TINYINT(4)   NOT NULL AFTER `codigo` ;

/* Alter table in target */
ALTER TABLE `rrhh_concepto` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `codigo` `codigo` VARCHAR(4)  COLLATE latin1_swedish_ci NOT NULL AFTER `d_rrhh_concepto` , 
	CHANGE `activo` `activo` TINYINT(4)   NOT NULL DEFAULT 1 AFTER `codigo` , 
	CHANGE `id_cuenta_contable` `id_cuenta_contable` INT(11)   NULL AFTER `activo` , 
	CHANGE `orden_impresion` `orden_impresion` INT(11)   NULL DEFAULT 1 AFTER `id_cuenta_contable` , 
	CHANGE `id_rrhh_tipo_concepto` `id_rrhh_tipo_concepto` INT(11)   NULL AFTER `texto_mostrar` , 
	CHANGE `chk_asumir_cero_si_resultado_negativo` `chk_asumir_cero_si_resultado_negativo` TINYINT(4)   NULL AFTER `formula` , 
	CHANGE `orden_liquidacion` `orden_liquidacion` INT(11)   NULL AFTER `chk_asumir_cero_si_resultado_negativo` , 
	CHANGE `chk_es_cantidad` `chk_es_cantidad` TINYINT(4)   NULL AFTER `orden_liquidacion` , 
	CHANGE `chk_es_valor_unitario` `chk_es_valor_unitario` TINYINT(4)   NULL AFTER `chk_es_cantidad` , 
	CHANGE `chk_es_importe` `chk_es_importe` TINYINT(4)   NULL AFTER `chk_es_valor_unitario` , 
	CHANGE `orden_calculo` `orden_calculo` INT(11)   NULL AFTER `valor_generico` , 
	CHANGE `tipo_vigencia` `tipo_vigencia` INT(4)   NULL AFTER `orden_calculo` , 
	CHANGE `chk_tasa_liquidacion` `chk_tasa_liquidacion` TINYINT(4)   NULL AFTER `tipo_vigencia` , 
	CHANGE `chk_periodo` `chk_periodo` TINYINT(4)   NULL AFTER `fecha_modificacion` , 
	CHANGE `chk_filtro_sexo` `chk_filtro_sexo` TINYINT(4)   NULL AFTER `chk_periodo` , 
	CHANGE `chk_filtro_clase_legajo` `chk_filtro_clase_legajo` TINYINT(4)   NULL AFTER `chk_filtro_sexo` , 
	CHANGE `chk_filtro_categoria` `chk_filtro_categoria` TINYINT(4)   NULL AFTER `chk_filtro_clase_legajo` , 
	CHANGE `chk_filtro_sindicato` `chk_filtro_sindicato` TINYINT(4)   NULL AFTER `chk_filtro_categoria` , 
	CHANGE `chk_filtro_estado_civil` `chk_filtro_estado_civil` TINYINT(4)   NULL AFTER `chk_filtro_sindicato` , 
	CHANGE `chk_filtro_area` `chk_filtro_area` TINYINT(4)   NULL AFTER `chk_filtro_estado_civil` , 
	CHANGE `chk_filtro_obra_social` `chk_filtro_obra_social` TINYINT(4)   NULL AFTER `chk_filtro_area` , 
	CHANGE `chk_fecha_historico` `chk_fecha_historico` TINYINT(4)   NULL AFTER `chk_filtro_obra_social` , 
	CHANGE `chk_fecha_ganancia` `chk_fecha_ganancia` TINYINT(4)   NULL AFTER `chk_fecha_historico` , 
	CHANGE `chk_rrhh_tipo_liquidacion` `chk_rrhh_tipo_liquidacion` TINYINT(4)   NULL DEFAULT 1 AFTER `chk_fecha_ganancia` ;

/* Alter table in target */
ALTER TABLE `rrhh_concepto_area` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_concepto` `id_rrhh_concepto` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_area` `id_area` INT(11)   NOT NULL AFTER `id_rrhh_concepto` ;

/* Alter table in target */
ALTER TABLE `rrhh_concepto_categoria` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_concepto` `id_rrhh_concepto` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_rrhh_categoria` `id_rrhh_categoria` INT(11)   NOT NULL AFTER `id_rrhh_concepto` ;

/* Alter table in target */
ALTER TABLE `rrhh_concepto_clase_legajo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_concepto` `id_rrhh_concepto` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_rrhh_clase_legajo` `id_rrhh_clase_legajo` INT(11)   NOT NULL AFTER `id_rrhh_concepto` ;

/* Alter table in target */
ALTER TABLE `rrhh_concepto_F931_base_calculo_afip` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_concepto` `id_rrhh_concepto` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_rrhh_F931_bases_calculo_afip` `id_rrhh_F931_bases_calculo_afip` INT(11)   NOT NULL AFTER `id_rrhh_concepto` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_concepto_F931_concepto_afip` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_concepto` `id_rrhh_concepto` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_rrhh_F931_concepto_afip` `id_rrhh_F931_concepto_afip` INT(11)   NOT NULL AFTER `id_rrhh_concepto` , 
	ADD KEY `id_rrhh_concepto`(`id_rrhh_concepto`) , 
	ADD KEY `id_rrhh_F931_concepto_afip`(`id_rrhh_F931_concepto_afip`) , COLLATE ='utf8mb4_general_ci' ;
ALTER TABLE `rrhh_concepto_F931_concepto_afip`
	ADD CONSTRAINT `rrhh_concepto_F931_concepto_afip_ibfk_1` 
	FOREIGN KEY (`id_rrhh_concepto`) REFERENCES `rrhh_concepto` (`id`) , 
	ADD CONSTRAINT `rrhh_concepto_F931_concepto_afip_ibfk_2` 
	FOREIGN KEY (`id_rrhh_F931_concepto_afip`) REFERENCES `rrhh_F931_concepto_afip` (`id`) ;


/* Alter table in target */
ALTER TABLE `rrhh_concepto_obra_social` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_concepto` `id_rrhh_concepto` INT(11)   NULL AFTER `id` , 
	CHANGE `id_rrhh_obra_social` `id_rrhh_obra_social` INT(11)   NULL AFTER `id_rrhh_concepto` ;

/* Alter table in target */
ALTER TABLE `rrhh_concepto_persona_estado_civil` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_concepto` `id_rrhh_concepto` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_persona_estado_civil` `id_persona_estado_civil` INT(11)   NOT NULL AFTER `id_rrhh_concepto` ;

/* Alter table in target */
ALTER TABLE `rrhh_concepto_persona_sexo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_concepto` `id_rrhh_concepto` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_persona_sexo` `id_persona_sexo` INT(11)   NOT NULL AFTER `id_rrhh_concepto` ;

/* Alter table in target */
ALTER TABLE `rrhh_concepto_sindicato` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_concepto` `id_rrhh_concepto` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_rrhh_sindicato` `id_rrhh_sindicato` INT(11)   NOT NULL AFTER `id_rrhh_concepto` ;

/* Alter table in target */
ALTER TABLE `rrhh_concepto_tipo_liquidacion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_concepto` `id_rrhh_concepto` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_rrhh_tipo_liquidacion` `id_rrhh_tipo_liquidacion` INT(11)   NOT NULL AFTER `id_rrhh_concepto` ;

/* Alter table in target */
ALTER TABLE `rrhh_condicion_sicoss` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `codigo` `codigo` INT(11)   NULL AFTER `d_rrhh_condicion_sicoss` ;

/* Alter table in target */
ALTER TABLE `rrhh_conjunto_concepto` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `codigo` `codigo` VARCHAR(4)  COLLATE utf8mb4_general_ci NOT NULL AFTER `id` , 
	CHANGE `d_rrhh_conjunto_concepto` `d_rrhh_conjunto_concepto` VARCHAR(50)  COLLATE utf8mb4_general_ci NOT NULL AFTER `codigo` , 
	CHANGE `activo` `activo` TINYINT(4)   NOT NULL AFTER `d_rrhh_conjunto_concepto` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_conjunto_concepto_item` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_conjunto_concepto` `id_rrhh_conjunto_concepto` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_rrhh_concepto` `id_rrhh_concepto` INT(11)   NOT NULL AFTER `id_rrhh_conjunto_concepto` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_convenio` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `activo` `activo` TINYINT(4)   NULL AFTER `d_rrhh_convenio` , 
	CHANGE `duracion_mes` `duracion_mes` TINYINT(4)   NULL AFTER `activo` ;

/* Alter table in target */
ALTER TABLE `rrhh_F931_bases_calculo_afip` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_F931_tipo_concepto` `id_F931_tipo_concepto` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `d_rrhh_F931_bases_calculo` `d_rrhh_F931_bases_calculo` VARCHAR(200)  COLLATE utf8mb4_general_ci NOT NULL AFTER `id_F931_tipo_concepto` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_F931_bases_calculo_afip_default` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `codigo` `codigo` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `codigo_desde` `codigo_desde` INT(11)   NOT NULL AFTER `codigo` , 
	CHANGE `codigo_hasta` `codigo_hasta` INT(11)   NOT NULL AFTER `codigo_desde` , 
	CHANGE `id_rrhh_F931_bases_calculo_afip` `id_rrhh_F931_bases_calculo_afip` INT(11)   NOT NULL AFTER `codigo_hasta` , 
	CHANGE `estado` `estado` INT(11)   NOT NULL AFTER `id_rrhh_F931_bases_calculo_afip` , 
	CHANGE `modificado` `modificado` INT(11)   NOT NULL DEFAULT 0 AFTER `estado` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_F931_concepto_afip` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `codigo` `codigo` INT(11)   NOT NULL AFTER `d_rrhh_concepto` , 
	CHANGE `f931_tipo_concepto` `f931_tipo_concepto` INT(11)   NOT NULL AFTER `codigo` , 
	CHANGE `ful_codigo` `ful_codigo` INT(11)   NULL AFTER `f931_tipo_concepto` ;

/* Alter table in target */
ALTER TABLE `rrhh_F931_concepto_afip_uso_libre` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `d_rrhh_F931_concepto_afip_uso_libre` `d_rrhh_F931_concepto_afip_uso_libre` VARCHAR(100)  COLLATE utf8mb4_general_ci NOT NULL AFTER `id` , 
	CHANGE `codigo` `codigo` INT(11)   NOT NULL AFTER `d_rrhh_F931_concepto_afip_uso_libre` , 
	CHANGE `id_f931_tipo_concepto` `id_f931_tipo_concepto` INT(11)   NOT NULL AFTER `codigo` , 
	CHANGE `codigo_desde` `codigo_desde` INT(11)   NOT NULL AFTER `id_f931_tipo_concepto` , 
	CHANGE `codigo_hasta` `codigo_hasta` INT(11)   NOT NULL AFTER `codigo_desde` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_F931_configuracion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `parhijo` `parhijo` VARCHAR(3)  COLLATE utf8mb4_general_ci NULL AFTER `diasbase` , 
	CHANGE `parcony` `parcony` VARCHAR(3)  COLLATE utf8mb4_general_ci NULL AFTER `parhijo` , 
	CHANGE `paradhe` `paradhe` VARCHAR(3)  COLLATE utf8mb4_general_ci NULL AFTER `parcony` , 
	CHANGE `tipoemp` `tipoemp` VARCHAR(50)  COLLATE utf8mb4_general_ci NULL AFTER `paradhe` , 
	CHANGE `cantdias` `cantdias` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `tipoemp` , 
	CHANGE `canthoras` `canthoras` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `cantdias` , 
	CHANGE `apos` `apos` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `canthoras` , 
	CHANGE `coos` `coos` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `apos` , 
	CHANGE `baapos` `baapos` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `coos` , 
	CHANGE `bacoos` `bacoos` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `baapos` , 
	CHANGE `baLRT` `baLRT` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `bacoos` , 
	CHANGE `remmat` `remmat` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `baLRT` , 
	CHANGE `paposs` `paposs` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `remmat` , 
	CHANGE `pcontss` `pcontss` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `paposs` , 
	CHANGE `rembruta` `rembruta` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `pcontss` , 
	CHANGE `BI1` `BI1` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `rembruta` , 
	CHANGE `BI2` `BI2` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `BI1` , 
	CHANGE `BI3` `BI3` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `BI2` , 
	CHANGE `BI4` `BI4` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `BI3` , 
	CHANGE `BI5` `BI5` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `BI4` , 
	CHANGE `BI6` `BI6` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `BI5` , 
	CHANGE `BI7` `BI7` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `BI6` , 
	CHANGE `BI8` `BI8` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `BI7` , 
	CHANGE `BI9` `BI9` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `BI8` , 
	CHANGE `BaseCalcDifASS` `BaseCalcDifASS` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `BI9` , 
	CHANGE `BaseCalcDifCSS` `BaseCalcDifCSS` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `BaseCalcDifASS` , 
	CHANGE `bi10` `bi10` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `BaseCalcDifCSS` , 
	CHANGE `Detracciones` `Detracciones` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `bi10` , 
	CHANGE `ProBaseImp` `ProBaseImp` VARCHAR(4)  COLLATE utf8mb4_general_ci NULL AFTER `Detracciones` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_F931_tipo_concepto` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `d_rrhh_F931_tipo_concepto` `d_rrhh_F931_tipo_concepto` VARCHAR(30)  COLLATE utf8mb4_general_ci NULL AFTER `id` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_F931_tipo_liquidacion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `codigo` `codigo` VARCHAR(10)  COLLATE utf8mb4_general_ci NOT NULL AFTER `id` , 
	CHANGE `d_rrhh_F931_tipo_liquidacion` `d_rrhh_F931_tipo_liquidacion` VARCHAR(100)  COLLATE utf8mb4_general_ci NOT NULL AFTER `codigo` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_forma_pago` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `d_rrhh_forma_pago` `d_rrhh_forma_pago` VARCHAR(50)  COLLATE utf8mb4_general_ci NOT NULL AFTER `id` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_liquidacion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_tipo_liquidacion` `id_rrhh_tipo_liquidacion` INT(11)   NOT NULL AFTER `fecha_ganancia` , 
	CHANGE `id_estado_rrhh_liquidacion` `id_estado_rrhh_liquidacion` INT(11)   NOT NULL AFTER `id_rrhh_tipo_liquidacion` , 
	CHANGE `chk_habilita_ingreso_novedades` `chk_habilita_ingreso_novedades` TINYINT(4)   NOT NULL AFTER `id_estado_rrhh_liquidacion` , 
	CHANGE `id_rrhh_clase_legajo` `id_rrhh_clase_legajo` INT(11)   NOT NULL AFTER `chk_habilita_ingreso_novedades` , 
	CHANGE `chk_aplica_todo_empleado` `chk_aplica_todo_empleado` TINYINT(4)   NULL AFTER `id_rrhh_clase_legajo` , 
	CHANGE `id_rrhh_clase_liquidacion` `id_rrhh_clase_liquidacion` INT(4)   NOT NULL AFTER `chk_aplica_todo_empleado` , 
	CHANGE `chk_legajos` `chk_legajos` TINYINT(4)   NULL AFTER `fecha_fin_vacaciones` , 
	CHANGE `rango_lista` `rango_lista` INT(11)   NULL AFTER `chk_legajos` , 
	CHANGE `legajo_desde` `legajo_desde` INT(11)   NULL AFTER `rango_lista` , 
	CHANGE `legajo_hasta` `legajo_hasta` INT(11)   NULL AFTER `legajo_desde` , 
	CHANGE `numero_F931` `numero_F931` INT(11)   NULL AFTER `legajo_hasta` ;

/* Alter table in target */
ALTER TABLE `rrhh_liquidacion_area` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_liquidacion` `id_rrhh_liquidacion` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_area` `id_area` INT(11)   NOT NULL AFTER `id_rrhh_liquidacion` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_liquidacion_categoria` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_liquidacion` `id_rrhh_liquidacion` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_rrhh_categoria` `id_rrhh_categoria` INT(11)   NOT NULL AFTER `id_rrhh_liquidacion` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_liquidacion_concepto` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_liquidacion` `id_rrhh_liquidacion` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_rrhh_concepto` `id_rrhh_concepto` INT(11)   NOT NULL AFTER `id_rrhh_liquidacion` ;

/* Alter table in target */
ALTER TABLE `rrhh_liquidacion_lugar_pago` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_liquidacion` `id_rrhh_liquidacion` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_rrhh_lugar_pago` `id_rrhh_lugar_pago` INT(11)   NOT NULL AFTER `id_rrhh_liquidacion` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_liquidacion_persona` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_liquidacion` `id_rrhh_liquidacion` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_persona` `id_persona` INT(11)   NOT NULL AFTER `id_rrhh_liquidacion` ;

/* Alter table in target */
ALTER TABLE `rrhh_lugar_pago` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_provincia` `id_provincia` INT(11)   NULL AFTER `d_rrhh_lugar_pago` , 
	CHANGE `numero_calle` `numero_calle` INT(11)   NULL AFTER `calle` ;

/* Alter table in target */
ALTER TABLE `rrhh_modalidad_contratacion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `codigo` `codigo` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `d_rrhh_modalidad_contratacion` `d_rrhh_modalidad_contratacion` VARCHAR(100)  COLLATE utf8mb4_general_ci NOT NULL AFTER `codigo` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL DEFAULT 1 AFTER `tasa` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_modelo_liquidacion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_convenio` `id_rrhh_convenio` INT(11)   NULL AFTER `d_rrhh_modelo_liquidacion` ;

/* Alter table in target */
ALTER TABLE `rrhh_modelo_liquidacion_concepto_item` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_modelo_liquidacion` `id_rrhh_modelo_liquidacion` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_rrhh_concepto` `id_rrhh_concepto` INT(11)   NOT NULL AFTER `id_rrhh_modelo_liquidacion` ;

/* Alter table in target */
ALTER TABLE `rrhh_novedad_persona` 
	CHANGE `id_rrhh_liquidacion` `id_rrhh_liquidacion` INT(11)   NOT NULL FIRST , 
	CHANGE `id_persona` `id_persona` INT(11)   NOT NULL AFTER `id_rrhh_liquidacion` , 
	CHANGE `id_rrhh_concepto` `id_rrhh_concepto` INT(11)   NOT NULL AFTER `id_persona` , 
	CHANGE `indice` `indice` INT(11)   NULL AFTER `periodo` , 
	CHANGE `chk_usa_fecha_ganancia` `chk_usa_fecha_ganancia` TINYINT(4)   NULL AFTER `fecha_modificacion` ;

/* Alter table in target */
ALTER TABLE `rrhh_obra_social` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `activa` `activa` TINYINT(4)   NULL AFTER `d_rrhh_obra_social` , 
	CHANGE `codigo_extra` `codigo_extra` INT(11)   NULL AFTER `fecha_modificacion` ;

/* Alter table in target */
ALTER TABLE `rrhh_persona_concepto` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_persona` `id_persona` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_rrhh_concepto` `id_rrhh_concepto` INT(11)   NOT NULL AFTER `id_persona` , 
	CHANGE `orden` `orden` INT(11)   NULL AFTER `id_rrhh_concepto` , 
	CHANGE `id_rrhh_convenio` `id_rrhh_convenio` INT(11)   NULL AFTER `importe` ;

/* Alter table in target */
ALTER TABLE `rrhh_sindicato` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `rrhh_situacion_sicoss` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `codigo` `codigo` INT(11)   NOT NULL AFTER `id` ;

/* Alter table in target */
ALTER TABLE `rrhh_subsistema_seguridad_social_afip` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `d_rrhh_subsistema_seguridad_social_afip` `d_rrhh_subsistema_seguridad_social_afip` VARCHAR(20)  COLLATE utf8mb4_general_ci NOT NULL AFTER `id` , 
	CHANGE `codigo` `codigo` VARCHAR(10)  COLLATE utf8mb4_general_ci NULL AFTER `d_rrhh_subsistema_seguridad_social_afip` , 
	CHANGE `aporte` `aporte` TINYINT(4)   NULL AFTER `codigo` , 
	CHANGE `contribucion` `contribucion` TINYINT(4)   NULL AFTER `aporte` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_subsistema_seguridad_social_concepto_afip` 
	CHANGE `id` `id` INT(11)   NOT NULL FIRST , 
	CHANGE `id_rrhh_subsistema_seguridad_social_concepto` `id_rrhh_subsistema_seguridad_social_concepto` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_rrhh_concepto_afip` `id_rrhh_concepto_afip` INT(11)   NOT NULL AFTER `id_rrhh_subsistema_seguridad_social_concepto` , 
	CHANGE `aporte` `aporte` TINYINT(4)   NOT NULL DEFAULT 0 AFTER `id_rrhh_concepto_afip` , 
	CHANGE `contribucion` `contribucion` TINYINT(4)   NOT NULL DEFAULT 0 AFTER `aporte` , COLLATE ='utf8mb4_general_ci' ;

/* Alter table in target */
ALTER TABLE `rrhh_tipo_concepto` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `signo_contable` `signo_contable` INT(11)   NULL AFTER `d_rrhh_tipo_concepto` , 
	CHANGE `codigo` `codigo` INT(11)   NULL AFTER `signo_contable` ;

/* Alter table in target */
ALTER TABLE `rrhh_tipo_liquidacion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	ADD COLUMN `d_rrhh_tipo_liquidacion` VARCHAR(50)  COLLATE utf8mb4_general_ci NOT NULL AFTER `codigo` , 
	CHANGE `fecha_modificacion` `fecha_modificacion` DATETIME   NULL AFTER `d_rrhh_tipo_liquidacion` , 
	CHANGE `activo` `activo` TINYINT(4)   NOT NULL DEFAULT 1 AFTER `fecha_modificacion` , 
	CHANGE `id_rrhh_F931_tipo_liquidacion` `id_rrhh_F931_tipo_liquidacion` INT(11)   NULL AFTER `activo` , 
	DROP COLUMN `d_rrrhh_tipo_liquidacion` ;

/* Alter table in target */
ALTER TABLE `rrhh_tipo_liquidacion_clase_liquidacion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_tipo_liquidacion` `id_rrhh_tipo_liquidacion` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_rrhh_clase_liquidacion` `id_rrhh_clase_liquidacion` INT(11)   NOT NULL AFTER `id_rrhh_tipo_liquidacion` ;

/* Alter table in target */
ALTER TABLE `rrhh_tipo_liquidacion_liquidacion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rrhh_liquidacion` `id_rrhh_liquidacion` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_rrhh_tipo_liquidacion` `id_rrhh_tipo_liquidacion` INT(11)   NOT NULL AFTER `id_rrhh_liquidacion` ;

/* Alter table in target */
ALTER TABLE `rrhh_variable` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `rubros` 
	CHANGE `CODIGO` `CODIGO` INT(11)   NULL FIRST ;

/* Alter table in target */
ALTER TABLE `sistema` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `genera_asiento` `genera_asiento` INT(1)   NULL DEFAULT 0 COMMENT 'no_visible' AFTER `d_sistema` , 
	CHANGE `visible` `visible` TINYINT(4)   NULL DEFAULT 0 AFTER `genera_asiento` ;

/* Alter table in target */
ALTER TABLE `sistema_moneda_enlazada` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_sistema` `id_sistema` INT(11)   NULL AFTER `id` , 
	CHANGE `id_moneda_origen` `id_moneda_origen` INT(11)   NULL AFTER `id_sistema` , 
	CHANGE `id_moneda_enlazada` `id_moneda_enlazada` INT(11)   NULL AFTER `id_moneda_origen` ;

/* Alter table in target */
ALTER TABLE `sistema_parametro` 
	CHANGE `id_sistema` `id_sistema` INT(11)   NOT NULL FIRST , 
	CHANGE `id_cuenta_contable_cuenta_corriente` `id_cuenta_contable_cuenta_corriente` INT(11)   NOT NULL AFTER `id_sistema` , 
	CHANGE `id_cuenta_contable_iva` `id_cuenta_contable_iva` INT(11)   NOT NULL AFTER `id_cuenta_contable_cuenta_corriente` , 
	CHANGE `id_cuenta_contable` `id_cuenta_contable` INT(11)   NOT NULL AFTER `id_cuenta_contable_iva` , 
	CHANGE `id_cuenta_contable_cuenta_contado` `id_cuenta_contable_cuenta_contado` INT(11)   NOT NULL AFTER `id_cuenta_contable` , 
	CHANGE `id_cuenta_contable_descuento_condicionado` `id_cuenta_contable_descuento_condicionado` INT(11)   NULL AFTER `id_cuenta_contable_cuenta_contado` , 
	CHANGE `id_tipo_comercializacion` `id_tipo_comercializacion` INT(11)   NOT NULL DEFAULT 1 AFTER `id_cuenta_contable_descuento_condicionado` , 
	CHANGE `id_cuenta_contable_compensacion` `id_cuenta_contable_compensacion` INT(11)   NULL AFTER `id_tipo_comercializacion` ;

/* Alter table in target */
ALTER TABLE `situacion_ib` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `codigo` `codigo` INT(11)   NULL AFTER `d_situacion_ib` ;

/* Alter table in target */
ALTER TABLE `stock_producto` 
	CHANGE `id` `id` INT(11)   NOT NULL FIRST , 
	CHANGE `id_deposito` `id_deposito` INT(11)   NOT NULL DEFAULT 1 AFTER `stock` ;

/* Alter table in target */
ALTER TABLE `sub_familia_producto` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_familia_producto` `id_familia_producto` INT(11)   NULL AFTER `d_sub_familia_producto` , 
	CHANGE `id_otro_sistema` `id_otro_sistema` INT(11)   NULL AFTER `id_familia_producto` ;

/* Alter table in target */
ALTER TABLE `sub_tipo_impuesto` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_tipo_impuesto` `id_tipo_impuesto` INT(11)   NULL AFTER `id` ;

/* Alter table in target */
ALTER TABLE `tabla_auxiliar` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_modulo` `id_modulo` INT(11)   NULL AFTER `d_tabla_auxiliar` , 
	CHANGE `columna_tabla_vinculada1` `columna_tabla_vinculada1` INT(11)   NULL AFTER `header1` , 
	CHANGE `columna_tabla_vinculada2` `columna_tabla_vinculada2` INT(11)   NULL AFTER `header2` , 
	CHANGE `columna_tabla_vinculada3` `columna_tabla_vinculada3` INT(11)   NULL AFTER `header3` , 
	CHANGE `columna_tabla_vinculada4` `columna_tabla_vinculada4` INT(11)   NULL AFTER `header4` , 
	CHANGE `columna_tabla_vinculada5` `columna_tabla_vinculada5` INT(11)   NULL AFTER `header5` , 
	CHANGE `columna_tabla_vinculada6` `columna_tabla_vinculada6` INT(11)   NULL AFTER `header6` , 
	CHANGE `columna_tabla_vinculada7` `columna_tabla_vinculada7` INT(11)   NULL AFTER `header7` ;

/* Alter table in target */
ALTER TABLE `tabla_auxiliar_item` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_tabla_auxiliar` `id_tabla_auxiliar` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_registro_tabla_auxiliar` `id_registro_tabla_auxiliar` INT(11)   NULL AFTER `id_tabla_auxiliar` ;

/* Alter table in target */
ALTER TABLE `talle` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `tarjeta_credito` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_tipo_tarjeta_credito` `id_tipo_tarjeta_credito` INT(11)   NULL DEFAULT 2 COMMENT 'no_visible' AFTER `nro_tarjeta` , 
	CHANGE `id_cuenta_bancaria` `id_cuenta_bancaria` INT(11)   NULL COMMENT 'no_visible' AFTER `id_tipo_tarjeta_credito` , 
	CHANGE `propia` `propia` TINYINT(4)   NULL DEFAULT 1 COMMENT 'no_visible' AFTER `id_cuenta_bancaria` , 
	CHANGE `id_persona` `id_persona` INT(11)   NULL COMMENT 'no_visible' AFTER `propia` , 
	CHANGE `id_moneda` `id_moneda` INT(11)   NULL COMMENT 'no_visible' AFTER `id_persona` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL COMMENT 'no_visible' AFTER `id_moneda` , 
	CHANGE `id_cuenta_contable` `id_cuenta_contable` INT(11)   NULL COMMENT 'no_visible' AFTER `activo` , 
	CHANGE `id_cuenta_contable_puente` `id_cuenta_contable_puente` INT(11)   NULL COMMENT 'no_visible' AFTER `id_cuenta_contable` ;

/* Alter table in target */
ALTER TABLE `tesoreria_clase_transaccion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `tipo_actividad` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `codigo` `codigo` INT(11)   NULL AFTER `d_tipo_actividad` ;

/* Alter table in target */
ALTER TABLE `tipo_asiento` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_estado_inicial_asiento` `id_estado_inicial_asiento` INT(11)   NOT NULL AFTER `abreviado` , 
	CHANGE `edita_estado_asiento` `edita_estado_asiento` TINYINT(4)   NOT NULL AFTER `id_estado_inicial_asiento` , 
	CHANGE `genera_asiento_resumen` `genera_asiento_resumen` TINYINT(4)   NOT NULL AFTER `edita_estado_asiento` , 
	CHANGE `habilitado` `habilitado` TINYINT(4)   NOT NULL AFTER `observacion` , 
	CHANGE `id_agrupacion_asiento` `id_agrupacion_asiento` INT(11)   NOT NULL AFTER `habilitado` ;

/* Alter table in target */
ALTER TABLE `tipo_asiento_leyenda` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_tipo_asiento` `id_tipo_asiento` INT(11)   NOT NULL AFTER `d_tipo_asiento_leyenda` , 
	CHANGE `por_defecto` `por_defecto` TINYINT(4)   NULL AFTER `id_tipo_asiento` ;

/* Alter table in target */
ALTER TABLE `tipo_cartera_rendicion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `tipo_cheque` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `tipo_comercializacion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `tipo_comprobante` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_sistema` `id_sistema` INT(11)   NULL DEFAULT 0 AFTER `id` , 
	CHANGE `signo_comercial` `signo_comercial` SMALLINT(6)   NULL DEFAULT 0 AFTER `abreviado_tipo_comprobante` , 
	CHANGE `signo_contable` `signo_contable` SMALLINT(6)   NULL DEFAULT 0 AFTER `signo_comercial` , 
	CHANGE `Fl_Importe` `Fl_Importe` SMALLINT(6)   NULL DEFAULT 0 AFTER `signo_contable` , 
	CHANGE `Fl_Valor` `Fl_Valor` SMALLINT(6)   NULL DEFAULT 0 AFTER `Fl_Importe` , 
	CHANGE `Fl_Minuta` `Fl_Minuta` SMALLINT(6)   NULL DEFAULT 0 AFTER `Fl_Valor` , 
	CHANGE `Fl_Envio` `Fl_Envio` SMALLINT(6)   NULL DEFAULT 0 AFTER `Fl_Minuta` , 
	CHANGE `requiere_iva` `requiere_iva` TINYINT(6)   NULL DEFAULT 0 AFTER `Fl_Envio` , 
	CHANGE `afecta_stock` `afecta_stock` TINYINT(4)   NULL DEFAULT 0 AFTER `d2_tipo_comprobante` , 
	CHANGE `tipo_comportamiento_stock` `tipo_comportamiento_stock` TINYINT(4)   NULL DEFAULT 0 AFTER `afecta_stock` , 
	CHANGE `codigo_afip` `codigo_afip` INT(11)   NULL AFTER `tipo_comportamiento_stock` , 
	CHANGE `id_deposito_origen` `id_deposito_origen` INT(11)   NULL COMMENT 'no se usa mas' AFTER `codigo_afip` , 
	CHANGE `id_deposito_destino` `id_deposito_destino` INT(11)   NULL COMMENT 'no se usan mas. usar id_tipo_movmiento' AFTER `id_deposito_origen` , 
	CHANGE `id_tipo_movimiento` `id_tipo_movimiento` INT(11)   NULL AFTER `letra` , 
	CHANGE `flag_cargar_codigo_en_item_descripcion` `flag_cargar_codigo_en_item_descripcion` TINYINT(4)   NULL DEFAULT 1 AFTER `id_tipo_movimiento` , 
	CHANGE `genera_asiento` `genera_asiento` TINYINT(4)   NULL AFTER `controller` , 
	CHANGE `id_asiento_modelo` `id_asiento_modelo` INT(11)   NULL AFTER `genera_asiento` , 
	CHANGE `cambia_modelo_asiento_al_ingreso` `cambia_modelo_asiento_al_ingreso` TINYINT(4)   NULL AFTER `id_asiento_modelo` , 
	CHANGE `edita_modelo_asiento_al_ingreso` `edita_modelo_asiento_al_ingreso` TINYINT(4)   NULL AFTER `cambia_modelo_asiento_al_ingreso` , 
	CHANGE `muestra_asiento_al_ingreso` `muestra_asiento_al_ingreso` TINYINT(4)   NULL AFTER `edita_modelo_asiento_al_ingreso` , 
	CHANGE `incluye_items_en_asiento` `incluye_items_en_asiento` TINYINT(4)   NULL AFTER `muestra_asiento_al_ingreso` , 
	CHANGE `id_producto_default` `id_producto_default` INT(11)   NULL AFTER `incluye_items_en_asiento` , 
	CHANGE `requiere_contador` `requiere_contador` TINYINT(4)   NULL DEFAULT 1 AFTER `id_producto_default` , 
	CHANGE `id_impuesto` `id_impuesto` INT(11)   NULL AFTER `requiere_contador` , 
	CHANGE `permite_item_sin_id_producto` `permite_item_sin_id_producto` TINYINT(4)   NULL AFTER `id_impuesto` , 
	CHANGE `id_estado_asiento_default` `id_estado_asiento_default` INT(11)   NULL AFTER `permite_item_sin_id_producto` , 
	CHANGE `permite_multimoneda` `permite_multimoneda` TINYINT(4)   NULL AFTER `impresion_footer_right` , 
	CHANGE `id_generar_comprobante_simultaneo` `id_generar_comprobante_simultaneo` INT(11)   NULL AFTER `back_color` , 
	CHANGE `permite_total_cero` `permite_total_cero` TINYINT(4)   NULL DEFAULT 0 AFTER `id_generar_comprobante_simultaneo` , 
	CHANGE `permite_agregar_item_libre_importacion` `permite_agregar_item_libre_importacion` TINYINT(4)   NULL AFTER `permite_total_cero` , 
	CHANGE `codigo_agip` `codigo_agip` TINYINT(4)   NULL AFTER `permite_agregar_item_libre_importacion` , 
	CHANGE `requiere_definicion_menu` `requiere_definicion_menu` TINYINT(4)   NULL DEFAULT 0 AFTER `letra_arba` , 
	CHANGE `cantidad_ceros_decimal` `cantidad_ceros_decimal` INT(11)   NULL DEFAULT 2 AFTER `id_menu` , 
	CHANGE `plazo_entrega_dias` `plazo_entrega_dias` INT(11)   NULL DEFAULT 30 AFTER `cantidad_ceros_decimal` , 
	CHANGE `permite_modificar_nro_comprobante` `permite_modificar_nro_comprobante` TINYINT(4)   NULL DEFAULT 0 AFTER `plazo_entrega_dias` , 
	CHANGE `permite_ingresar_d_punto_venta` `permite_ingresar_d_punto_venta` TINYINT(4)   NULL AFTER `permite_modificar_nro_comprobante` , 
	CHANGE `id_persona_default` `id_persona_default` INT(4)   NULL AFTER `permite_ingresar_d_punto_venta` , 
	CHANGE `permite_ajuste_diferencia_cambio` `permite_ajuste_diferencia_cambio` TINYINT(4)   NULL AFTER `id_persona_default` , 
	CHANGE `contiene_comprobante_valor` `contiene_comprobante_valor` TINYINT(4)   NULL AFTER `permite_ajuste_diferencia_cambio` , 
	CHANGE `envia_mail_al_conformar` `envia_mail_al_conformar` TINYINT(4)   NULL DEFAULT 0 AFTER `contiene_comprobante_valor` , 
	CHANGE `adjunta_pdf_mail` `adjunta_pdf_mail` TINYINT(4)   NULL DEFAULT 0 AFTER `envia_mail_al_conformar` , 
	CHANGE `email` `email` TINYINT(4)   NULL AFTER `adjunta_pdf_mail` , 
	CHANGE `usa_mail_usuario_envio` `usa_mail_usuario_envio` TINYINT(4)   NULL AFTER `html_mail` , 
	CHANGE `id_cuenta_bancaria` `id_cuenta_bancaria` INT(11)   NULL AFTER `usa_mail_usuario_envio` ;

/* Alter table in target */
ALTER TABLE `tipo_comprobante_impuesto` 
	CHANGE `id_impuesto` `id_impuesto` INT(11)   NOT NULL DEFAULT 0 FIRST , 
	CHANGE `id_tipo_comprobante` `id_tipo_comprobante` INT(11)   NOT NULL DEFAULT 0 AFTER `id_impuesto` ;

/* Alter table in target */
ALTER TABLE `tipo_comprobante_persona` 
	CHANGE `id_persona` `id_persona` INT(11)   NULL FIRST , 
	CHANGE `id_tipo_comprobante` `id_tipo_comprobante` INT(11)   NULL AFTER `id_persona` ;

/* Alter table in target */
ALTER TABLE `tipo_comprobante_punto_venta_numero` 
	CHANGE `id` `id` TINYINT(11)   NOT NULL FIRST , 
	CHANGE `id_estado_comprobante_defecto` `id_estado_comprobante_defecto` INT(11)   NULL AFTER `d_tipo_comprobante_punto_venta_numero` ;

/* Alter table in target */
ALTER TABLE `tipo_contrato` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `tipo_cuenta_bancaria` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `tipo_cuenta_contable` 
	CHANGE `id` `id` INT(11)   NOT NULL FIRST ;

/* Alter table in target */
ALTER TABLE `tipo_documento` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `tipo_empleador` 
	CHANGE `id` `id` INT(11)   NOT NULL FIRST ;

/* Alter table in target */
ALTER TABLE `tipo_empresa` 
	CHANGE `id` `id` INT(11)   NOT NULL FIRST ;

/* Alter table in target */
ALTER TABLE `tipo_funcion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_modulo` `id_modulo` INT(11)   NULL AFTER `id` , 
	CHANGE `id_entidad` `id_entidad` INT(11)   NULL AFTER `id_modulo` , 
	ADD PRIMARY KEY(`id`) ;

/* Alter table in target */
ALTER TABLE `tipo_impuesto` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `tipo_iva` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `codigo_afip` `codigo_afip` INT(11)   NULL AFTER `d_tipo_iva` , 
	CHANGE `requiere_cuit` `requiere_cuit` TINYINT(4)   NULL AFTER `id_tipo_comprobante_factura` , 
	CHANGE `discrimina` `discrimina` TINYINT(4)   NULL AFTER `requiere_cuit` , 
	CHANGE `requiere_forma_pago` `requiere_forma_pago` TINYINT(4)   NULL DEFAULT 1 AFTER `discrimina` , 
	CHANGE `asociado_comprobante_exportacion` `asociado_comprobante_exportacion` TINYINT(4)   NULL AFTER `requiere_forma_pago` ;

/* Alter table in target */
ALTER TABLE `tipo_lista_precio` 
	CHANGE `id` `id` INT(11)   NOT NULL FIRST ;

/* Alter table in target */
ALTER TABLE `tipo_lote` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `tipo_movimiento_cuenta_corriente` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `tipo_persona` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_contador` `id_contador` INT(11)   NULL AFTER `d_tipo_persona` ;

/* Alter table in target */
ALTER TABLE `tipo_reclamo` 
	CHANGE `id` `id` INT(11)   NOT NULL FIRST ;

/* Alter table in target */
ALTER TABLE `tipo_tarjeta_credito` 
	CHANGE `id` `id` INT(11)   NOT NULL FIRST ;

/* Alter table in target */
ALTER TABLE `tur_cliente` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `numero` `numero` INT(20)   NULL AFTER `email` , 
	CHANGE `id_forma_pago` `id_forma_pago` INT(11)   NULL COMMENT 'no_visible' AFTER `porc_percepcion_ib` , 
	CHANGE `id_pais` `id_pais` INT(11)   NULL COMMENT 'no_visible' AFTER `localidad` , 
	CHANGE `id_provincia` `id_provincia` INT(11)   NULL COMMENT 'no_visible' AFTER `id_pais` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL DEFAULT 1 COMMENT 'no_visible' AFTER `id_provincia` , 
	CHANGE `forma_pago` `forma_pago` INT(11)   NULL COMMENT 'no_visible' AFTER `percepcion` , 
	CHANGE `id_tipo_documento` `id_tipo_documento` INT(11)   NULL DEFAULT 3 COMMENT 'no_visible' AFTER `forma_pago` , 
	CHANGE `id_media_file` `id_media_file` BIGINT(20)   NULL COMMENT 'no_visible' AFTER `tiene_cuenta_corriente` , 
	CHANGE `valoracion` `valoracion` INT(11)   NULL AFTER `pasaporte_fecha_vto` , 
	CHANGE `id_cliente_categoria` `id_cliente_categoria` INT(11)   NULL COMMENT 'no_visible' AFTER `fecha_creacion` ;

/* Alter table in target */
ALTER TABLE `tur_cotizacion` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_moneda` `id_moneda` INT(11)   NULL COMMENT 'no_visible' AFTER `observacion` , 
	CHANGE `id_persona` `id_persona` INT(11)   NULL COMMENT 'no_visible' AFTER `valor_moneda` , 
	CHANGE `id_usuario` `id_usuario` INT(11)   NULL DEFAULT 1 COMMENT 'no_visible' AFTER `id_persona` , 
	CHANGE `id_estado` `id_estado` INT(11)   NULL COMMENT 'no_visible' AFTER `cuit` , 
	CHANGE `id_forma_pago` `id_forma_pago` INT(11)   NULL COMMENT 'no_visible' AFTER `id_estado` ;

/* Alter table in target */
ALTER TABLE `tur_cotizacion_cliente_asociado` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_cliente` `id_cliente` INT(11)   NOT NULL AFTER `id` , 
	CHANGE `id_cotizacion` `id_cotizacion` INT(11)   NOT NULL AFTER `id_cliente` ;

/* Alter table in target */
ALTER TABLE `tur_cotizacion_concepto` 
	CHANGE `id` `id` INT(11)   NOT NULL FIRST ;

/* Alter table in target */
ALTER TABLE `tur_cotizacion_item` 
	CHANGE `id` `id` BIGINT(20)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_producto` `id_producto` INT(11)   NULL COMMENT 'no_visible' AFTER `id` , 
	CHANGE `id_cotizacion` `id_cotizacion` INT(11)   NOT NULL COMMENT 'no_visible' AFTER `id_producto` , 
	CHANGE `id_cotizacion_concepto` `id_cotizacion_concepto` INT(11)   NULL AFTER `id_cotizacion` , 
	CHANGE `cant_adulto` `cant_adulto` INT(11)   NULL DEFAULT 0 AFTER `detalle` , 
	CHANGE `cant_infantil` `cant_infantil` INT(11)   NULL AFTER `cant_adulto` , 
	CHANGE `cant_menor` `cant_menor` INT(11)   NULL AFTER `cant_infantil` , 
	CHANGE `dias` `dias` INT(11)   NULL AFTER `cant_menor` , 
	CHANGE `id_moneda_item` `id_moneda_item` INT(11)   NULL AFTER `valor_moneda_item` , 
	CHANGE `orden` `orden` INT(11)   NULL COMMENT 'no_visible' AFTER `id_moneda_item` , 
	CHANGE `n_item` `n_item` INT(11)   NULL AFTER `orden` ;

/* Alter table in target */
ALTER TABLE `unidad` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_unidad_afip` `id_unidad_afip` INT(11)   NULL AFTER `x_unidad` , 
	CHANGE `id_unidad_calculo` `id_unidad_calculo` INT(11)   NULL AFTER `id_unidad_afip` , 
	CHANGE `activo` `activo` TINYINT(4)   NULL AFTER `fraccionada` ;

/* Alter table in target */
ALTER TABLE `unidad_afip` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `unidad_calculo` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST ;

/* Alter table in target */
ALTER TABLE `usuario` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `id_rol` `id_rol` INT(11)   NULL AFTER `id` , 
	CHANGE `activo` `activo` TINYINT(4)   NOT NULL DEFAULT 0 AFTER `email` , 
	CHANGE `id_licencia` `id_licencia` INT(11)   NULL AFTER `activo` , 
	CHANGE `id_punto_venta_fiscal` `id_punto_venta_fiscal` INT(11)   NULL AFTER `id_licencia` , 
	CHANGE `editar_validar_precio_minimo` `editar_validar_precio_minimo` TINYINT(4)   NULL AFTER `id_punto_venta_fiscal` , 
	CHANGE `cantidad_maxima_sesiones_multiples` `cantidad_maxima_sesiones_multiples` INT(11)   NULL DEFAULT 1 AFTER `editar_validar_precio_minimo` , 
	CHANGE `intentos_fallidos` `intentos_fallidos` INT(11)   NULL DEFAULT 0 AFTER `email_password` , 
	CHANGE `id_persona` `id_persona` INT(11)   NULL AFTER `url_avatar` ;

/* Alter table in target */
ALTER TABLE `valor` 
	CHANGE `id` `id` INT(11)   NOT NULL AUTO_INCREMENT FIRST , 
	CHANGE `visible` `visible` TINYINT(4)   NULL AFTER `codigo_valor` ; 
	
	
ALTER TABLE `rrhh_liquidacion` CHANGE `id_rrhh_clase_liquidacion` `id_rrhh_clase_liquidacion` INT(4) NULL; 

ALTER TABLE `rrhh_concepto` DROP FOREIGN KEY `rrhh_concepto_ibfk_2`; 
ALTER TABLE `rrhh_tipo_concepto` CHANGE `id` `id` INT(11) NOT NULL; 

DELIMITER $$

ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `legajo_view` AS 
SELECT
  `persona`.`id`                                    AS `id`,
  `persona`.`email`                                 AS `email`,
  `persona`.`id_pais`                               AS `id_pais`,
  `persona`.`id_provincia`                          AS `id_provincia`,
  `persona`.`localidad`                             AS `localidad`,
  `persona`.`ciudad`                                AS `ciudad`,
  `persona`.`calle`                                 AS `calle`,
  `persona`.`numero_calle`                          AS `numero_calle`,
  `persona`.`piso`                                  AS `piso`,
  `persona`.`dpto`                                  AS `dpto`,
  `persona`.`tel`                                   AS `tel`,
  `persona`.`interno1`                              AS `interno1`,
  `persona`.`fax`                                   AS `fax`,
  `persona`.`posicion`                              AS `posicion`,
  `persona`.`cuit`                                  AS `cuit`,
  `persona`.`ib`                                    AS `ib`,
  `persona`.`id_cuenta`                             AS `id_cuenta`,
  `persona`.`cod_postal`                            AS `cod_postal`,
  `persona`.`id_tipo_iva`                           AS `id_tipo_iva`,
  `persona`.`id_persona_figura`                     AS `id_persona_figura`,
  `persona`.`activo`                                AS `activo`,
  `persona`.`id_tipo_documento`                     AS `id_tipo_documento`,
  `persona`.`observaciones`                         AS `observaciones`,
  `persona`.`observacion_extra`                     AS `observacion_extra`,
  `persona`.`id_persona_categoria`                  AS `id_persona_categoria`,
  `persona`.`id_cuenta_contable`                    AS `id_cuenta_contable`,
  `persona`.`id_moneda`                             AS `id_moneda`,
  `persona`.`requiere_orden_compra`                 AS `requiere_orden_compra`,
  `persona`.`id_forma_pago`                         AS `id_forma_pago`,
  `persona`.`id_condicion_pago`                     AS `id_condicion_pago`,
  `persona`.`id_media_file`                         AS `id_media_file`,
  `persona`.`valoracion`                            AS `valoracion`,
  `persona`.`fecha_creacion`                        AS `fecha_creacion`,
  `persona`.`fecha_nacimiento`                      AS `fecha_nacimiento`,
  `persona`.`pasaporte`                             AS `pasaporte`,
  `persona`.`pasaporte_fecha_vto`                   AS `pasaporte_fecha_vto`,
  `persona`.`persona_extranjero`                    AS `persona_extranjero`,
  `persona`.`id_tipo_actividad`                     AS `id_tipo_actividad`,
  `persona`.`id_tipo_persona`                       AS `id_tipo_persona`,
  `persona`.`pasaporte_fecha_creacion`              AS `pasaporte_fecha_creacion`,
  `persona`.`observacion`                           AS `observacion`,
  `persona`.`id_migracion`                          AS `id_migracion`,
  `persona`.`fecha_borrado`                         AS `fecha_borrado`,
  `persona`.`email2`                                AS `email2`,
  `persona`.`email3`                                AS `email3`,
  `persona`.`email4`                                AS `email4`,
  `persona`.`tel2`                                  AS `tel2`,
  `persona`.`interno2`                              AS `interno2`,
  `persona`.`horario_contacto`                      AS `horario_contacto`,
  `persona`.`pasaporte_nacionalidad`                AS `pasaporte_nacionalidad`,
  `persona`.`nombre`                                AS `nombre`,
  `persona`.`apellido`                              AS `apellido`,
  `persona`.`id_area`                               AS `id_area`,
  `persona`.`fecha_ingreso`                         AS `fecha_ingreso`,
  `persona`.`fecha_egreso`                          AS `fecha_egreso`,
  `persona`.`id_persona_sexo`                       AS `id_persona_sexo`,
  `persona`.`celular`                               AS `celular`,
  `persona`.`interno`                               AS `interno`,
  `persona`.`direccion_latitud`                     AS `direccion_latitud`,
  `persona`.`direccion_longitud`                    AS `direccion_longitud`,
  `persona`.`id_tipo_contrato`                      AS `id_tipo_contrato`,
  `persona`.`id_persona_padre`                      AS `id_persona_padre`,
  `persona`.`d_persona`                             AS `d_persona`,
  `persona`.`id_estado_persona`                     AS `id_estado_persona`,
  `persona`.`codigo`                                AS `codigo`,
  `persona`.`id_otro_sistema`                       AS `id_otro_sistema`,
  `persona`.`nro_pasaporte`                         AS `nro_pasaporte`,
  `persona`.`id_tipo_comercializacion_por_defecto`  AS `id_tipo_comercializacion_por_defecto`,
  `pais`.`d_pais`                                   AS `d_pais`,
  `provincia`.`d_provincia`                         AS `d_provincia`,
  `tipo_documento`.`d_tipo_documento`               AS `d_tipo_documento`,
  `estado_persona`.`d_estado_persona`               AS `d_estado_persona`,
  `cuenta_contable`.`d_cuenta_contable`             AS `d_cuenta_contable`,
  `persona_categoria`.`d_persona_categoria`         AS `d_persona_categoria`,
  `usuario`.`id`                                    AS `id_usuario`,
  `usuario`.`username`                              AS `sivit_usuario`,
  IFNULL(`usuario`.`activo`,1)                      AS `sivit_usuario_activo`,
  `persona`.`id_persona_origen`                     AS `id_persona_origen`,
  `rrhh_convenio`.`id`                              AS `id_rrhh_convenio`,
  `rrhh_convenio`.`d_rrhh_convenio`                 AS `d_rrhh_convenio`,
  `rrhh_condicion_sicoss`.`id`                      AS `id_rrhh_condicion_sicoss`,
  `rrhh_condicion_sicoss`.`d_rrhh_condicion_sicoss` AS `d_rrhh_condicion_sicoss`,
  `rrhh_situacion_sicoss`.`id`                      AS `id_rrhh_situacion_sicoss`,
  `rrhh_situacion_sicoss`.`d_rrhh_situacion_sicoss` AS `d_rrhh_situacion_sicoss`,
  `rrhh_actividad`.`d_rrhh_actividad`               AS `d_rrhh_actividad`,
  `persona`.`rrhh_basico`                           AS `rrhh_basico`,
  `rrhh_obra_social`.`id`                           AS `id_rrhh_obra_social`,
  `rrhh_obra_social`.`d_rrhh_obra_social`           AS `d_rrhh_obra_social`,
  `rrhh_lugar_pago`.`id`                            AS `id_rrhh_lugar_pago`,
  `rrhh_lugar_pago`.`d_rrhh_lugar_pago`             AS `d_rrhh_lugar_pago`,
  `rrhh_clase_legajo`.`id`                          AS `id_rrhh_clase_legajo`,
  `rrhh_clase_legajo`.`d_rrhh_clase_legajo`         AS `d_rrhh_clase_legajo`,
  `rrhh_categoria`.`id`                             AS `id_rrhh_categoria`,
  `rrhh_categoria`.`d_rrhh_categoria`               AS `d_rrhh_categoria`,
  `persona`.`numero_documento`                      AS `numero_documento`,
  `persona`.`id_persona_estado_civil`               AS `id_persona_estado_civil`,
  `persona`.`fecha_antiguedad`                      AS `fecha_antiguedad`,
  `persona`.`id_rrhh_sindicato`                     AS `id_rrhh_sindicato`,
  `persona`.`trabajador_convencionado`              AS `trabajador_convencionado`,
  `persona`.`seguro_colectivo_vida`                 AS `seguro_colectivo_vida`,
  `persona`.`id_rrhh_situacion_revista`             AS `id_rrhh_situacion_revista`,
  `persona`.`rrhh_situacion_revista_dia_inicio`     AS `rrhh_situacion_revista_dia_inicio`,
  `persona`.`id_rrhh_situacion_revista2`            AS `id_rrhh_situacion_revista2`,
  `persona`.`rrhh_situacion_revista_dia_inicio2`    AS `rrhh_situacion_revista_dia_inicio2`,
  `persona`.`id_rrhh_situacion_revista3`            AS `id_rrhh_situacion_revista3`,
  `persona`.`rrhh_situacion_revista_dia_inicio3`    AS `rrhh_situacion_revista_dia_inicio3`
FROM ((((((((((((((((`persona`
                  LEFT JOIN `pais`
                    ON ((`pais`.`id` = `persona`.`id_pais`)))
                 LEFT JOIN `provincia`
                   ON ((`provincia`.`id` = `persona`.`id_provincia`)))
                LEFT JOIN `tipo_documento`
                  ON ((`tipo_documento`.`id` = `persona`.`id_tipo_documento`)))
               LEFT JOIN `estado_persona`
                 ON ((`estado_persona`.`id` = `persona`.`id_estado_persona`)))
              LEFT JOIN `cuenta_contable`
                ON ((`cuenta_contable`.`id` = `persona`.`id_cuenta_contable`)))
             LEFT JOIN `cuenta_contable` `cc2`
               ON ((`cc2`.`id` = `persona`.`id_cuenta_contable_cuenta_corriente`)))
            LEFT JOIN `persona_categoria`
              ON ((`persona_categoria`.`id` = `persona`.`id_persona_categoria`)))
           LEFT JOIN `usuario`
             ON ((`usuario`.`id_persona` = `persona`.`id`)))
          LEFT JOIN `rrhh_convenio`
            ON ((`rrhh_convenio`.`id` = `persona`.`id_rrhh_convenio`)))
         LEFT JOIN `rrhh_condicion_sicoss`
           ON ((`rrhh_condicion_sicoss`.`id` = `persona`.`id_rrhh_condicion_sicoss`)))
        LEFT JOIN `rrhh_situacion_sicoss`
          ON ((`rrhh_situacion_sicoss`.`id` = `persona`.`id_rrhh_situacion_sicoss`)))
       LEFT JOIN `rrhh_actividad`
         ON ((`rrhh_actividad`.`id` = `persona`.`id_rrhh_actividad`)))
      LEFT JOIN `rrhh_obra_social`
        ON ((`rrhh_obra_social`.`id` = `persona`.`id_rrhh_obra_social`)))
     LEFT JOIN `rrhh_lugar_pago`
       ON ((`rrhh_lugar_pago`.`id` = `persona`.`id_rrhh_lugar_pago`)))
    LEFT JOIN `rrhh_clase_legajo`
      ON ((`rrhh_clase_legajo`.`id` = `persona`.`id_rrhh_clase_legajo`)))
   LEFT JOIN `rrhh_categoria`
     ON ((`rrhh_categoria`.`id` = `persona`.`id_rrhh_categoria`)))
WHERE (`persona`.`id_tipo_persona` = 8)$$

DELIMITER ;

SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));

ALTER TABLE `rrhh_concepto` ADD COLUMN `chk_de_sistema` TINYINT DEFAULT 0 NULL AFTER `chk_rrhh_tipo_liquidacion`; 
ALTER TABLE `rrhh_F931_configuracion` CHANGE `tipoemp` `id_tipo_empleador` INT NULL; 

CREATE TABLE `rrhh_F931_configuracion_concepto`( `id_rrhh_F931_configuracion` INT, `id_rrhh_concepto` INT, `apos` TINYINT, `coos` TINYINT, `conOS` TINYINT, `BI4` TINYINT ); 


ALTER TABLE `rrhh_F931_configuracion` CHANGE `diasbase` `diasbase` TINYINT(1) NULL; 

ALTER TABLE `rrhh_F931_configuracion` DROP COLUMN `BI4`; 
ALTER TABLE `rrhh_F931_configuracion` DROP COLUMN `apos`, DROP COLUMN `coos`; 
ALTER TABLE `rrhh_F931_configuracion` CHANGE `diasbase` `diasbase` INT(1) NULL; 
ALTER TABLE `rrhh_F931_configuracion_concepto` CHANGE `apos` `apos` INT(4) NULL, CHANGE `coos` `coos` INT(4) NULL, CHANGE `conOS` `conOS` INT(4) NULL, CHANGE `BI4` `BI4` INT(4) NULL; 
ALTER TABLE `rrhh_novedad_persona` CHANGE `cantidad` `cantidad` DECIMAL(20,2) DEFAULT 0 NULL, CHANGE `valor_unitario` `valor_unitario` DECIMAL(20,2) DEFAULT 0 NULL, CHANGE `importe` `importe` DECIMAL(20,2) DEFAULT 0 NULL, CHANGE `importe_calculado` `importe_calculado` DECIMAL(20,2) DEFAULT 0 NULL; 
ALTER TABLE `rrhh_concepto` CHANGE `valor_generico` `valor_generico` DECIMAL(20,2) DEFAULT 0 NULL; 
UPDATE rrhh_concepto SET valor_generico = 0 WHERE valor_generico IS NULL;
ALTER TABLE `rrhh_liquidacion` CHANGE `id_rrhh_clase_legajo` `id_rrhh_clase_legajo` INT(11) NULL; 


ALTER TABLE `rrhh_persona_concepto` ADD COLUMN `vigencia_desde` DATE NULL AFTER `id_rrhh_convenio`, ADD COLUMN `vigencia_hasta` DATE NULL AFTER `vigencia_desde`; 
ALTER TABLE `rrhh_novedad_persona` ADD COLUMN `vigencia_desde` DATE NULL AFTER `fecha_ganancia`, ADD COLUMN `vigencia_hasta` DATE NULL AFTER `vigencia_desde`; 





 

	
	
	