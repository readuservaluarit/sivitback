<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class CuentaContableAdd extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'cuenta_contable';

   
     public $actsAs = array('Containable');
    public $virtualFields = array(
    'd_codigo_cuenta_contable' => 'CONCAT(CuentaContable.d_cuenta_contable, " ", CuentaContable.codigo)'
);

/**
 * Validation rules
 *
 * @var array
 */
	

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 public $belongsTo = array(
        // 'Estado' => array(
            // 'className' => 'Estado',
            // 'foreignKey' => 'id_estado',
            // 'dependent' => false,
            // 'conditions' => '',
            // 'fields' => '',
  
        // ),
        'TipoCuentaContable' => array(
            'className' => 'TipoCuentaContable',
            'foreignKey' => 'id_tipo_cuenta_contable',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
 		
 		/*
        'CuentaContableParent' => array(
            'className' => 'CuentaContable',
            'foreignKey' => 'parent_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        )
        */
        
);

public $hasMany = array(
        'CuentaContableCentroCosto' => array(
            'className' => 'CuentaContableCentroCosto',
            'foreignKey' => 'id_cuenta_contable',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        );


}
