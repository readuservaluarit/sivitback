<?php 
		include(APP.'View'.DS.'ComprobanteItemComprobantes'.DS.'json'.DS.'default.ctp');
/* VER DOCUMENTACION  https://docs.google.com/document/d/1oXAtK0GqJnHuNhWrWEJ8nt4XgBXAGAlxXP2tkDWiVwM/edit?pli=1
0] De Cuerpo
1] De Cierre H
2] De Cierre N
3] De Cuerpo
4] Doble bloqueo y venteo*/
		$model["n_item"]["show_grid"] = 1;
        $model["n_item"]["header_display"] = "Nro. Item";
        $model["n_item"]["order"] = "1";
        $model["n_item"]["width"] = "7";
		
		$model["item_observacion"]["show_grid"] = 1;
        $model["item_observacion"]["header_display"] = "Fluido";/*AGUA AIRE*/
        $model["item_observacion"]["order"] = "1";
        $model["item_observacion"]["width"] = "7";
		
		$model["precio_unitario"]["show_grid"] = 1;
		$model["precio_unitario"]["read_only"] = 0;
        $model["precio_unitario"]["header_display"] = "Presion"; /*SOLO  LLEVA LA DESCRIPCION DE QUE ES CUERPO, VASTAGO ETC*/
		
		$model["cantidad"]["show_grid"] = 1;
		$model["cantidad"]["read_only"] = 0;
        $model["cantidad"]["header_display"] = "T.min"; /*SOLO  LLEVA LA DESCRIPCION DE QUE ES CUERPO, VASTAGO ETC*/
        
        $model["codigo_hijo"]["show_grid"] = 1;
		$model["codigo_hijo"]["read_only"] = 0;
		$model["codigo_hijo"]["type"] = EnumTipoDato::cadena;
        $model["codigo_hijo"]["header_display"] = "C&oacute;digo";
        $model["codigo_hijo"]["order"] = "2";
        $model["codigo_hijo"]["width"] = "20";
		
        $model["d_producto_hijo"]["show_grid"] = 1;
		$model["d_producto_hijo"]["read_only"] = 0;
        $model["d_producto_hijo"]["type"] = EnumTipoDato::cadena;
        $model["d_producto_hijo"]["header_display"] = "Art&iacute;culo Parte";
        $model["d_producto_hijo"]["order"] = "3";
        $model["d_producto_hijo"]["width"] = "35";
        
		/*TOdo esto va en la otra table*/
        // $model["lote"]["show_grid"] = 1;
        // $model["lote"]["header_display"] = "Lote/s";
        // $model["lote"]["order"] = "4";
        // $model["lote"]["width"] = "75";
		// $model["lote"]["text_colour"] = "#0000FF";
		
		// $model["componente_codigo"]["show_grid"] = 1;
        // $model["componente_codigo"]["header_display"] = "OT/s";
        // $model["componente_codigo"]["order"] = "2";
        // $model["componente_codigo"]["width"] = "20";
        

 $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItem",
                    "content" => $model
                );
        echo json_encode($output);
?>