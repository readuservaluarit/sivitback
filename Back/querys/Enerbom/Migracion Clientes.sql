SELECT 
  persona_id AS id,
  cliente.clemp AS razon_social,
  REPLACE(clcuit, '-', '') AS cuit,
    cliente.`CLCOD` AS cli_cod,
  sucdir AS calle,
  0 AS numero_calle,
  '' AS piso,
  '' AS depto,
  succp AS cod_postal,
  sucloc AS localidad,
  sucpro AS id_provincia,
  succo2 AS id_pais,
  clflg AS id_tipo_iva,
  suctel AS tel,
  '' AS email,
  '' AS observacion, 
  1 AS id_estado_persona

FROM
  cliente,
  sucursal,
  matpers_id 
WHERE matpers_id.persona_tipo = 'C' 
  AND matpers_id.persona_cod = cliente.clcod 
  AND matpers_id.persona_cod = sucursal.clcod 
  AND matpers_id.persona_suc = sucursal.sucnum 
  AND clcuit>0