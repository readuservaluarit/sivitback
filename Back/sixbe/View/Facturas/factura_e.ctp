<?php   

function digitoVerificadorCaeAfip($txt){
  $i = $pares = $impares = 0;

  for( $i = 0; $i < strlen($txt); $i++ ) {
    // OJO: Los caracteres impares esta en la posiciones pares del array y viceversa, porque el array arranca desde la posicion 0
    if( $i % 2 ) {
      $pares += $txt[$i];
    } else {
      $impares += $txt[$i];
    }
  }
  $impares = $impares * 3;
  
  $total = $pares + $impares;
  
  $dv = $total % 10;

  return $dv;
}

/**
 * PDF exportación de Orden Compra
 */

App::import('Vendor','tcpdf/tcpdf'); 
App::import('Vendor','tcpdf/tcpdf_barcodes_1d'); 
$logo = '/img/'.$datos_empresa["DatoEmpresa"]["id"].'.png';


$comprobante = new Comprobante();

if($comprobante->esImportado($datos_pdf['ComprobanteItem']) == 1)
    $datos_pdf['ComprobanteItem']['orden_compra_externa'] = implode(",",$comprobante->getOrdenCompraExterna($datos_pdf['ComprobanteItem']));
// create new PDF document
// create new PDF document
$fecha_entrega = new DateTime($datos_pdf['Comprobante']['fecha_contable']);
$fecha_inicio_actividad = new DateTime($datos_empresa["DatoEmpresa"]["fecha_inicio_actividad"]);


$proforma = "";
if(!$datos_pdf["Comprobante"]["cae"] >0){
    $proforma = " PROFORMA ";
    $letra = "X";
    $cod = "";
}else{

	
	$letra = strtoupper($datos_pdf["TipoComprobante"]["letra"]);
	$cod = "cod 01";

}



$aviso_anulado = '';
    if($datos_pdf['Comprobante']['id_estado_comprobante'] == EnumEstadoComprobante::Anulado)
        $aviso_anulado = ' ANULADO';

$html = '<table width="100%" border="0" cellspacing="0" style="font-size:10px;border:2px solid #000000; font-family:\'Courier New\', Courier, monospace">
          <tr>
            <td style="padding: 5px 5px 5px 5px;" width="37%" rowspan="4" align="center" valign="center"  ><img src="'.$datos_empresa["DatoEmpresa"]["app_path"].$logo.'" width="420" height="121" />  </td>
            <td width="8%" height="3%" rowspan="2"  style="border:2px solid #000000">
                <div align="center" style="font-size:30px">'.$letra.'</div>
                 <span align="center" style="font-size:10px;">'.$cod.'</span>
            </td>
            <td width="55%" style="padding-left:5px; font-size:17px;font-weight:bold;"> FACTURA '.$proforma.' '.$aviso_anulado.': </td>
            
          </tr>
          <tr>
            <td style="padding-left:5px; font-size:15px;"> Nro '.str_pad($datos_pdf["PuntoVenta"]["numero"],4,"0",STR_PAD_LEFT).'-'.str_pad($datos_pdf["Comprobante"]["nro_comprobante"],8,"0",STR_PAD_LEFT).'</td>
          </tr> 
          
          <tr>
            <td width="8%" height="2%" align="center" valign="middle" >&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr >
            <td  width="8%" height="5%" align="center" valign="middle" >&nbsp;</td>
            <td style="padding-left:5px;"> Original Blanco / Copia color </td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["calle"].' '.$datos_empresa["DatoEmpresa"]["numero"].' '.$datos_empresa["DatoEmpresa"]["codigo_postal"].' '.$datos_empresa["Provincia"]["d_provincia"].'</td>
            <td rowspan="5">&nbsp;</td>
            <td style="padding-left:5px;"> Fecha: '.$fecha_entrega->format('d-m-Y').'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> E-mail: '.$datos_empresa["DatoEmpresa"]["email"].'</td>
            <td style="padding-left:5px;"> CUIT: '.$datos_empresa["DatoEmpresa"]["cuit"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> Tel: '.$datos_empresa["DatoEmpresa"]["tel"].'</td>
            <td style="padding-left:5px;"> Ingresos Brutos: '.$datos_empresa["DatoEmpresa"]["iibb"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["website"].'</td>
            <td style="padding-left:5px;"> Inicio de actividades: '.$fecha_inicio_actividad->format('d-m-Y').'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.strtoupper($datos_empresa["TipoIva"]["d_tipo_iva"]).'</td>
            <td style="padding-left:5px;">&nbsp;</td>
          </tr>
        </table>';

$html;



class MyTCPDF extends TCPDF{

    public $datos_header;
    public $datos_footer;
    public $bar_code;
    public $observaciones;
    
    public function set_datos_header($datos_empresa){
        $this->datos_header = $datos_empresa; 

    }
    public function set_datos_footer($datos_empresa_footer){
        $this->datos_footer = $datos_empresa_footer; 

    }
     public function set_barcode($bar_code){
        $this->bar_code = $bar_code; 

    }
    
    public function set_observaciones($observaciones){
        $this->observaciones = $observaciones; 

    }
  public function Header(){
     
        
        
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_header, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
  }
  
  public function Footer(){
     
        
        
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_footer, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->observaciones, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
     
     $style['text'] = true;
     $style['border'] = true;
     $style['align'] = 'C';
     $this->write1DBarcode($this->bar_code, 'I25', '40', '', '', 9, 0.4, $style, 'N');
      
      //$this->writeHTMLCell($w = 0, $h = 0, $x = '200', $y = '', $this->bar_code, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
      //$this->Cell(0, 20, $this->bar_code , 0, false, 'C', 0, '', 0, false, 'T', 'M');
      $this->Cell(0, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
      
  }
  
}

$pdf = new MyTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
     
$pdf->set_datos_header($html);

 
$descuento = (($datos_pdf["Comprobante"]["total_comprobante"]*$datos_pdf["Comprobante"]["descuento"])/100) ;



//genero el codigo de barras para ver las especificaciones http://www.formularioscontinuos.com/nota1.html
if( $datos_pdf["Comprobante"]["cae"] != '' ){

    //genero el string para el codigo de barras
    $cuit =  str_replace("-","",$datos_empresa["DatoEmpresa"]["cuit"]);
    $tipo_doc_afip = str_pad($datos_pdf["TipoComprobante"]["codigo_afip"],2,"0",STR_PAD_LEFT);
    $cae = $datos_pdf["Comprobante"]["cae"];
    $fecha_vencimiento = str_replace("-","",$datos_pdf["Comprobante"]["vencimiento_cae"]);
    
    $codigo_barras = $cuit.$tipo_doc_afip.$cae.$fecha_vencimiento;
    $codigo_barras = $codigo_barras.digitoVerificadorCaeAfip($codigo_barras);
    $pdf->set_barcode($codigo_barras);  
    
   
}  


$impuestos = '';
if(isset($datos_pdf["ComprobanteImpuesto"])){

        foreach($datos_pdf["ComprobanteImpuesto"] as $impuesto){

            $impuestos .= ' <tr>
                 <td style="width:30%;">'.$impuesto["Impuesto"]["abreviado_impuesto"].'</td>
                <td style="width:40%;">'.money_format("%!n",$impuesto["tasa_impuesto"]).'% (B.Imp. '.money_format("%!n",$impuesto["base_imponible"]).')</td>
                 <td style="width:30%;">'.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format("%!n",$impuesto["importe_impuesto"]).'</td>
               </tr>';
               
        }
} 


App::import('Vendor', 'NumberToLetterConverter', array('file' => 'Classes/NumberToLetterConverter.class.php'));   
$numero_conversion = new NumberToLetterConverter();
$letras_total =  "Importe en letras: ".$numero_conversion->to_word(str_replace(".",",",$datos_pdf["Comprobante"]["total_comprobante"]),$datos_pdf["Moneda"]["simbolo_internacional"]);
$footer='
  <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>                     
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody> 
</table>


<table width="100%" border="1" style="width:100%;">
   <tr>';
   
  
  if($datos_pdf["Comprobante"]["imprimir_obs_cuerpo"] != 1)
  	$observacion =  nl2br($datos_pdf["Comprobante"]["observacion"]);
  else
  	$observacion = "";
        
  $footer.='<td width="50%">Observaciones: '.$observacion;
 	
 	
    
     if($datos_pdf["Comprobante"]["id_moneda"] == EnumMoneda::Peso && $datos_pdf["Comprobante"]["valor_moneda"]!= $datos_pdf["Comprobante"]["valor_moneda2"])
        $footer.=' <br/>  Esta factura por un monto de '.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n',$datos_pdf["Comprobante"]["total_comprobante"]).' equivale a USD '.money_format('%!n',round($datos_pdf["Comprobante"]["total_comprobante"]/$datos_pdf["Comprobante"]["valor_moneda"],4)).'  Ref 1 USD= '.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!.5n',bcdiv($datos_pdf["Comprobante"]["valor_moneda"],1,5)).'. Ref Interna Factura nro ='.$datos_pdf["Comprobante"]["id"];
     elseif($datos_pdf["Comprobante"]["id_moneda"] == EnumMoneda::Dolar  && $datos_pdf["Comprobante"]["valor_moneda"]!= $datos_pdf["Comprobante"]["valor_moneda2"])
        $footer.=' <br/>  Esta factura por un monto de '.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n',$datos_pdf["Comprobante"]["total_comprobante"]).' equivale a PES '.money_format('%!n',round($datos_pdf["Comprobante"]["total_comprobante"]/$datos_pdf["Comprobante"]["valor_moneda2"],4)).'  Ref 1 USD= PES '.money_format('%!.5n',bcdiv($datos_pdf["Comprobante"]["valor_moneda"]/$datos_pdf["Comprobante"]["valor_moneda2"],1,5)).'. Ref Interna Factura nro ='.$datos_pdf["Comprobante"]["id"];
     elseif($datos_pdf["Comprobante"]["id_moneda"] == EnumMoneda::Euro  && $datos_pdf["Comprobante"]["valor_moneda"]!= $datos_pdf["Comprobante"]["valor_moneda3"])
        $footer.=' <br/>  Esta factura por un monto de '.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n',$datos_pdf["Comprobante"]["total_comprobante"]).' equivale a PES '.money_format('%!n',round($datos_pdf["Comprobante"]["total_comprobante"]/$datos_pdf["Comprobante"]["valor_moneda3"],4)).'  Ref 1 USD=  EU'.money_format('%!.5n',bcdiv($datos_pdf["Comprobante"]["valor_moneda"]/$datos_pdf["Comprobante"]["valor_moneda3"],1,5)).'. Ref Interna Factura nro ='.$datos_pdf["Comprobante"]["id"];
       
           
    $footer.='<br>'.$letras_total.'</td>'; 
     
     $footer.='<td width="50%">
     <table width="100%" border="0">
     
      <tr>
         <td style="width:30%;">DESC. GLOBAL</td>
         <td style="width:40%;">&nbsp;</td>
         <td style="width:30%;">'.money_format('%!n',round($datos_pdf["Comprobante"]["descuento"],2)).'%</td>
       </tr>
       
       <tr>
         <td style="width:30%;">SUBTOTAL</td>
         <td style="width:40%;">&nbsp;</td>
         <td style="width:30%;">'.$datos_pdf["Moneda"]["simbolo_internacional"].' '.$datos_pdf["Comprobante"]["subtotal_neto"].'</td>
       </tr>
       '.$impuestos.'
       <tr>
         <td><strong>TOTAL</strong></td>
         <td>&nbsp;</td>
         <td><strong>'.$datos_pdf["Moneda"]["simbolo_internacional"].' '. money_format("%!n", $datos_pdf["Comprobante"]["total_comprobante"]).'</strong></td>
       </tr>
     </table></td>
   </tr>
   <tr>
     <td > CAE: '.$datos_pdf["Comprobante"]["cae"].'</td>
     <td > Fecha Vto.: '.$datos_pdf["Comprobante"]["vencimiento_cae"].'</td>
   </tr>
   <tr>
     <td  colspan="2" style="font-size:6px;">'.$datos_pdf["TipoComprobante"]["impresion_observacion_extra"].'</td>
   
   </tr>
</table>
';


   
   $html .= "<HR>";
   
   
   if( $datos_pdf["Comprobante"]["cae"] != '' ){
   
      $html.=$pdf->SetBarcode($codigo_barras,I25);
     
   
   }
   
   
   $html .= '
   
   <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody>
</table>
   
  ';
  
  $pdf->set_datos_footer($footer);
  
  
ob_clean();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sivit by Valuarit');
$pdf->SetTitle('Factura');
$pdf->SetSubject('Factura');
//$pdf->SetKeywords('');

                              

// set default header data

$title = "";
$subtitle = ""; 
//$pdf->SetHeaderData($logo, 190, $title, $subtitle, array(0,0,0), array(0,104,128));
$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 50, 20);
$pdf->SetHeaderMargin(10);
$pdf->SetFooterMargin(65);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
/*$lg = Array();
$lg = Array();
$lg['a_meta_charset'] = 'ISO-8859-1';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';
*/
//$pdf->setLanguageArray($lg);
    
// ---------------------------------------------------------

// set default font subsetting mode
//$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
/*
$html = <<<EOD
<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
EOD;
*/

$tbody = "";

$renglones = $datos_pdf["ComprobanteItem"];

$data = array();
$comprobante_item = new ComprobanteItem();

foreach($renglones as &$renglon){

   $descuento = ($renglon['descuento_unitario']);
     $aux["ComprobanteItem"]["precio_unitario"] = $renglon['precio_unitario'];
    $aux["ComprobanteItem"]["cantidad"] = $renglon['cantidad'];
    $precio_subtotal = $comprobante_item->getTotalItem($aux);//hago esto del $aux porque la funcion toma con ComprobanteItem
    
    
    if(isset($renglon["ComprobanteItemOrigen"]) && $renglon["ComprobanteItemOrigen"]>0){//ES IMPORTADA DE UN COMPROBANTE

        $renglon["orden_compra_externa"]  = $renglon["ComprobanteItemOrigen"]["Comprobante"]["orden_compra_externa"];

    }else{

        $renglon["orden_compra_externa"] = '';
    }
                  
    
   
  $tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
   $tbody .= "<td>" .$renglon['n_item'] . "</td>";
   $tbody .= "<td>" .$renglon['orden_compra_externa'] . "</td>";
   $tbody .= "<td>" .$renglon['Producto']['Unidad']['d_unidad'] . "</td>";
   $tbody .= "<td>" .$renglon['cantidad']. "</td>";
   $tbody .= "<td>" .$comprobante_item->getCodigoFromProducto($renglon) . "</td>";
   $tbody .= "<td>" .$renglon['item_observacion'] . "</td>";       
   $tbody .= "<td>" .money_format('%!n', $renglon['precio_unitario_bruto']). "</td>";
   $tbody .= "<td>" .$renglon['Iva']['d_iva_detalle'] . "</td>"; 
   $tbody .= "<td>" .$descuento. "</td>";
   $tbody .= "<td>" .money_format('%!n', $precio_subtotal)."</td>";
   $tbody .= "</tr>";
}
 
//agrego las observaciones

$time = strtotime($datos_pdf["Comprobante"]["fecha_vencimiento"]);
$month_venc = date("m",$time);
$year_venc =  date("y",$time);
$day_venc =   date("d",$time);






$html='';


$html .=' 
 <table width="100%"   style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
  <tr >
                    <td colspan="4"></td>
                  </tr>
  <tr>  
  
 <tr >
                    <td colspan="4"></td>
                  </tr>
  <tr>  
  <tr >
                    <td colspan="4"></td>
                  </tr>
  <tr>  

 </table>


                  
                    <table width="100%" border="1"  style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
                  <tr  style="background-color:rgb(224, 225, 229);">
                    <td colspan="4"><div align="center"><strong>Cliente</strong></div></td>
                  </tr>
                  <tr>                                     
                    <td width="10%"><strong>Codigo:</strong> '.$datos_pdf["Persona"]["codigo"].'</td>
                    <td width="42%"><strong>R. Social:</strong> '.$datos_pdf["Persona"]["razon_social"].'</td>
                    <td width="26%"><strong>'.$datos_pdf["Persona"]["TipoDocumento"]["d_tipo_documento"].':</strong> '.$datos_pdf["Persona"]["cuit"].' </td>
                    
              
                        <td width="22%"><strong>O/C:</strong> '.$datos_pdf["Comprobante"]["orden_compra_externa"].'</td>
                    
                    

                   </tr>
                         <tr>
                                <td colspan="3" align="left" ><strong>Direccion:</strong> '.$datos_pdf["Persona"]["calle"].' - '.$datos_pdf["Persona"]["numero_calle"].' - '.$datos_pdf["Persona"]["localidad"].' - '.$datos_pdf["Persona"]["ciudad"].' - '.$datos_pdf["Persona"]["Provincia"]["d_provincia"].' - '.$datos_pdf["Persona"]["Pais"]["d_pais"].'</td>
                                <td rowspan="2"><strong>Teléfono:</strong> '.$datos_pdf["Persona"]["tel"].'</td>
                              </tr>
                              
                              <tr>
                                
                                <td colspan="3" align="left"><strong>Condicion de Iva:</strong> '.$datos_pdf["Persona"]["TipoIva"]["d_tipo_iva"].'</td>
                              </tr>
                    
                    <tr  style="background-color:rgb(224, 225, 229);">
                                    <td colspan="4"><div align="center"><strong>Condiciones de Venta</strong></div></td>
                    </tr>
                    <tr>
                    <td colspan="2"><div align="center"><strong>'.$datos_pdf["CondicionPago"]["d_condicion_pago"].'</strong></div></td>
                    <td colspan="2"><div align="center"><strong> Vencimiento: '.$day_venc.'-'.$month_venc.'-'.$year_venc.'</strong></div></td>
                    
                    </tr>
                     <tr  style="background-color:rgb(224, 225, 229);">
                                    <td colspan="4"><div align="center"><strong>Referencias</strong></div></td>
                    </tr>
                    <tr>
                    <td colspan="2"><div align="center"><strong>Remitos: </strong>'.implode(",", $remitos_relacionados).'</div></td>
                    <td colspan="2"><div align="center"><strong>Pedidos de Venta: </strong>'.implode(",", $pedidos_internos_relacionados).'</div></td>
                    
                    
                    </tr>
                    
                    
                                 </table>


                  
              

<table width="100%" border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;font-family:\'Courier New\', Courier, monospace">

   
     <tbody>
     
        <tr >
        <td colspan="2"> </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
 
        <tr >
        <td  colspan="6" ><HR></td>
      
         </tr>
     </tbody>
</table>


<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">

   <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:3%;"><strong>Item</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:7%;"><strong>O/C</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>Un.</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>Cant</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:13%;"><strong>C&oacute;digo</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:24%;"><strong>Descripci&oacute;n</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>P.Un.</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:9%;"><strong>Iva(%)</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>Dto.Un(%)</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:15%;"><strong>Subtotal</strong></th>
    </tr>

  <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
   <tr style=\"font-size:10px;font-family:\'Courier New\', Courier, monospace\">
     <td colspan="10">
    </td>
  </tr>
    '.$tbody.'
  </tbody>  
</table>';
  
  
  $obs_en_cuerpo = ''; 

if($datos_pdf["Comprobante"]["imprimir_obs_cuerpo"] == 1){


$obs_en_cuerpo = '

<table width="100%" border="0"  align="left" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">


<td>


Observaciones:'.nl2br($datos_pdf["Comprobante"]["observacion"]).'
</td>
</table>';


$html.= $obs_en_cuerpo;



}
  
   
   
   
$pdf->writeHTML($html, true, false, true, false, true);


// QRCODE,L : QR-CODE Low error correction
// set style for barcode
$style = array(
    'border' => 2,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

//$pdf->Text(20, 25, 'QRCODE L');

// ---------------------------------------------------------




$pdf_name = $comprobante->getNamePDF($datos_pdf);
$root_pdf = $comprobante->getRootPDF();



if($datos_pdf["TipoComprobante"]["adjunta_pdf_mail"] == 1) {

	$path_para_pdf = $datos_empresa["DatoEmpresa"]["local_path"].$root_pdf;
          	
	$pdf->Output($path_para_pdf.$pdf_name.'.pdf', 'F');//Esta linea guarda un archivo en path_para_pdf
}


// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->SetBarcode(date("Y-m-d H:i:s", time()));
ob_clean();
$pdf->Output(str_replace(".","",$datos_pdf["TipoComprobante"]["abreviado_tipo_comprobante"].'_'.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'-'.$datos_pdf["Persona"]["razon_social"]).'.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
