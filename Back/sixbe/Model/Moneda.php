<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class Moneda extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'moneda';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'd_moneda' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Debe ingresar un Nombre.',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'unico' => array(
            'rule' => array('isUnique'),
            'message' => 'La moneda ya existe debe escribir una que no exista'
        )
        ),
        'cotizacion' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Debe ingresar una cotizaci&oacute;n.',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
    /*public $hasMany = array(
        'Region' => array(
            'className' => 'Region',
            'foreignKey' => 'id_jurisdiccion',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'id_jurisdiccion',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public $childRecordMessage = "Existe por lo menos una regi&oacute;n, predio, establecimiento o usuario relacionado/a con la jurisdicci&oacute;n.";
    
    */
    
    
    
    function getCotizacion($id_moneda){
        
         $moneda = $this->find('first', array(
                                    'conditions' => array('Moneda.id' => $id_moneda),
                                    'contain' =>false
                                    ));
         
         if($moneda)
            return $moneda["Moneda"]["cotizacion"];         
         else
            return 0;
            
            
    }
    
    
     function getFieldByTipoMoneda($id_tipo_moneda){
        
      switch($id_tipo_moneda){
          
          case EnumTipoMoneda::Base:
            return "valor_moneda";
          break;
          
          case EnumTipoMoneda::Corriente:
            return "valor_moneda2";
          break;
          
          case EnumTipoMoneda::CorrienteSecundaria:
            return "valor_moneda3";
          break;
          
      }  
            
    }
    
    
   
    
    function getTipoMonedaById($id_moneda){
        
        App::uses('CakeSession', 'Model/Datasource');
        $datoEmpresa = CakeSession::read('Empresa');
        
        switch($id_moneda){
            
            
            case $datoEmpresa["DatoEmpresa"]["id_moneda"]:
                return EnumTipoMoneda::Base;
                break;
            
            case $datoEmpresa["DatoEmpresa"]["id_moneda2"]:
                return EnumTipoMoneda::Corriente;
                break;
            case $datoEmpresa["DatoEmpresa"]["id_moneda3"]:
                return EnumTipoMoneda::CorrienteSecundaria;
                break;    
        }
        
        
        
    }
    
    
    function getSimboloInternacional($id_moneda){
        
        $moneda = $this->find('first', array(
                                                'conditions' => array('Moneda.id' => $id_moneda),
                                                'contain' =>false
                                                ));
                                                
       return $moneda["Moneda"]["simbolo_internacional"];
 
        
    }
    

    
   /* 
    public function beforeSave($options = array())
    {
       $monedas = $this->find('count',array('conditions'=>array("Moneda.cotizacion"=>1,
        
                                                                             "NOT" => array( "Moneda.id" => array($this->data["Moneda"]["id"]))
                                                                            
                                                                            )));
                                                                            
       if($monedas>0)
        return false;
       else
        return true;
                                                                            
                                                                            
    }
    */
    
    public function getMonedaBase(){
      
      
       $moneda = $this->find('first',array('conditions'=>array("Moneda.cotizacion"=>1)));
       
       return $moneda; 
        
        
    }

}
