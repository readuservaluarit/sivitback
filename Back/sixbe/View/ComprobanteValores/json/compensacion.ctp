<?php
		include(APP.'View'.DS.'ComprobanteValores'.DS.'json'.DS.'comprobante_valor.ctp');  
        

		
		$model["d_comprobante_valor"]["show_grid"] = 1;
        $model["d_comprobante_valor"]["header_display"] = "Descripci&oacute;n";
        $model["d_comprobante_valor"]["order"] = "2";
        $model["d_comprobante_valor"]["width"] = "20";
		$model["d_comprobante_valor"]["read_only"] = "0";
		$model["d_comprobante_valor"]["text_colour"] = "#000000";
		
		$model["referencia"]["show_grid"] = 1;
        $model["referencia"]["header_display"] = "Ref.";
        $model["referencia"]["order"] = "3";
        $model["referencia"]["width"] = "20";
		$model["referencia"]["read_only"] = "0";		
		$model["referencia"]["text_colour"] = "#000000";
		
		$model["monto"]["show_grid"] = 1;
        $model["monto"]["header_display"] = "Monto";
        $model["monto"]["order"] = "4";
        $model["monto"]["width"] = "20";
		$model["monto"]["read_only"] = "0";		
		$model["monto"]["text_colour"] = "#006600";
		
		$model["d_moneda"]["show_grid"] = 1;
        $model["d_moneda"]["header_display"] = "Moneda";
        $model["d_moneda"]["order"] = "5";
        $model["d_moneda"]["width"] = "15";
		$model["d_moneda"]["read_only"] = "1";		
		$model["d_moneda"]["text_colour"] = "#004400";
		
		$model["valor_moneda"]["show_grid"] = 1;
        $model["valor_moneda"]["header_display"] = "Cotiz";
        $model["valor_moneda"]["order"] = "6";
        $model["valor_moneda"]["width"] = "15";
		$model["valor_moneda"]["read_only"] = "1";		
		$model["valor_moneda"]["text_colour"] = "#006633";
        	   
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteValor",
                    "content" => $model
                );
				
        echo json_encode($output);
?>