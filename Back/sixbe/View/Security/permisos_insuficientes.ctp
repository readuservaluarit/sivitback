<style>
  .center {text-align: center; margin-left: auto; margin-right: auto; margin-bottom: auto; margin-top: auto;}
</style>
<title>Permisos Insuficientes</title>
<body>
  <div class="hero-unit center">
    <h2>Acción Denegada</h2>
    <small><font face="Tahoma">Usted no posee los permisos suficientes para ejecutar esta acción.</font></small>
    <br /><br /><br />
    <?php 
        echo $this->Html->link("<i class='icon-home icon-white'></i> Ir al Inicio" , array('controller' => 'Usuarios', 'action' => 'home'), array('class' => 'btn btn-large btn-primary', 'escape' => false)); 
    ?>
  </div>
  <br />