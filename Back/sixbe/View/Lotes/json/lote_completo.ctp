<?php
        // CONFIG  CON MODE DISPLAYED CELLS
        
        include(APP.'View'.DS.'Lotes'.DS.'json'.DS.'lote.ctp');
      
		$model["id"]["show_grid"] = 0; //MP: TENES Q SETEAR ACA en CERO PARA Q SEA INVISIBLE
        $model["id"]["header_display"] = "Nro Lote";
        $model["id"]["order"] = "0";
		$model["id"]["width"] = "6";
		
		$model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "Codigo";
        $model["codigo"]["order"] = "1";
		$model["codigo"]["width"] = "10";
		$model["codigo"]["read_only"] = "1";
		$model["codigo"]["text_colour"] = "#000099";
		// $model["codigo"]["picker"] = EnumMenu::mnuArticulosGeneral;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
		
		$model["d_tipo_lote"]["show_grid"] = 1;
        $model["d_tipo_lote"]["header_display"] = "Tipo";
        $model["d_tipo_lote"]["order"] = "2";
		$model["d_tipo_lote"]["width"] = "6";
		$model["d_tipo_lote"]["read_only"] = "0";
		
		$model["d_lote"]["show_grid"] = 1;
        $model["d_lote"]["header_display"] = "Descripcion";
        $model["d_lote"]["order"] = "3";
		$model["d_lote"]["width"] = "13";
		$model["d_lote"]["read_only"] = "0";
		
		$model["fecha_emision"]["show_grid"] = 1;
        $model["fecha_emision"]["header_display"] = "Fcha. Emision";
        $model["fecha_emision"]["order"] = "4";
        $model["fecha_emision"]["width"] = "6";
		$model["fecha_emision"]["read_only"] = "0";
        $model["fecha_emision"]["text_colour"] = "#000077";
		
		
		$model["utilizado"]["show_grid"] = 1;
        $model["utilizado"]["header_display"] = "Utilizado";
        $model["utilizado"]["order"] = "5";
        $model["utilizado"]["width"] = "6";
		$model["utilizado"]["read_only"] = "0";
        $model["utilizado"]["text_colour"] = "#880000";

		// $model["fecha_vencimiento"]["show_grid"] = 1; //INVISIBLE
        // $model["fecha_vencimiento"]["header_display"] = "Fecha Vto.";
        // $model["fecha_vencimiento"]["order"] = "6";
		// $model["fecha_vencimiento"]["width"] = "10";
        
         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Lote",
                    "content" => $model
                );
        
          echo json_encode($output);
?>