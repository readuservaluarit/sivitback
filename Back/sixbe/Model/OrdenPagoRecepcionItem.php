<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class OrdenPagoRecepcionItem extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'orden_pago_recepcion_item';

    public $actsAs = array('Containable');
   

// id  (id_item) 
// +*id_orden_pago
// +id_condicion_pago (Efectivo, cheque, Ctacte, Transferencia, ) 
// +orden
// +monto
// +nro_factura
// *valor_moneda_item
// *id_moneda_item
// *observacion  (Efectivo, cheque, Ctacte, Banco, ) 
// *id_banco(Efectivo, cheque, Ctacte, Banco, ) 
   
   
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

public $belongsTo = array(
        'OrdenPago' => array(
            'className' => 'OrdenPago',
            'foreignKey' => 'id_orden_pago',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'CondicionPago' => array(
             'className' => 'CondicionPago',
             'foreignKey' => 'id_condicion_pago_item',
             'conditions' => '',
             'fields' => '',
             'order' => ''
         ),
		
		'Moneda' => array(
            'className' => 'Moneda',
            'foreignKey' => 'id_moneda_item',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
       
        'Banco' => array(
            'className' => 'Banco',
            'foreignKey' => 'id_banco',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
 );
 

   /*
 public $hasMany = array(
        'ArticuloRelacion' => array(
            'className' => 'ArticuloRelacion',
            'foreignKey' => 'id_producto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        
);
	  */

}
