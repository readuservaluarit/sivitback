<?php

class CondicionPagoPicker extends EntityPicker{
    
    public $model = "CondicionPago";
    
    public $pickerTitle = "Selección de Forma de Pago";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Nombre", "field" => "d_condicion_pago", "width" => "80%", "keyOrder" => "d_condicion_pago"),
                                  array("title" => "Vencimiento", "field" => "vencimiento", "width" => "80%", "keyOrder" => "vencimiento"),
                                 
                                );

    public function getPickerConditions($busqueda){
        
        $pickerConditions = array('CondicionPago.d_condicion_pago LIKE' => '%' . $busqueda['bus_unico'] . '%');
        
        return $pickerConditions;
        
        
    }
    
    public function getPickerInitialFilter(){
        
        //Filtra siempre por la Jurisdiccion del usuario si corresponde
        return DataFilter::getJurisdiccionPickerFilter($this->model);
        
    }

}