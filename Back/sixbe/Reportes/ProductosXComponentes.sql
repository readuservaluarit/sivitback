/*Este reporte muestra la cantidad de productos en los que estan presentes cada componente*/
SELECT cm.`codigo` AS codigo_componente,COUNT(*) AS cant_productos_presentes FROM componente_producto cp
LEFT JOIN componente cm ON cm.`id` = cp.`id_componente`
WHERE cm.`codigo` LIKE 'VNR%' OR cm.`codigo` LIKE 'VCR%' OR cm.`codigo` LIKE 'H00%' OR cm.`codigo` LIKE 'EMC%' 
OR cm.`codigo` LIKE 'EHU%' OR cm.`codigo` LIKE 'EGV%' OR cm.`codigo` LIKE 'ABF%' OR cm.`codigo` LIKE 'ANF%' 
OR cm.`codigo` LIKE 'ANR%' OR cm.`codigo` LIKE 'H00%' OR cm.`codigo` LIKE 'ASM%'
AND cm.`activo`=1

GROUP BY cp.id_componente

ORDER BY cm.`codigo` ASC
