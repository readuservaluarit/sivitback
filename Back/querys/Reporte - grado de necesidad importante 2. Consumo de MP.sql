/*Cantidad de kilos mecanizados por Valmec en lo que va de 2016 desglosado por Orden de Fabricación. Esta información la necesitamos mes a mes y el global de los 7 meses transcurridos. Si ponen todo en una misma solapa después lo sumo en el Excel.*/


SELECT orden_produccion.id AS of,producto.id id_componente,producto.`codigo`,producto.`d_producto` AS d_componente,unidad.d_unidad,producto.`peso`,cantidad AS unidades_producidad
,producto_relacion.`cantidad_hijo` AS cantidad_mp_insumida_individual,unidad2.d_unidad AS unidad_mp,producto_relacion.`cantidad_hijo` * orden_produccion.`cantidad` AS cant_mp_mecanizada



FROM orden_produccion 
JOIN producto ON producto.id = orden_produccion.`id_componente`
JOIN unidad ON unidad.`id`=producto.`id_unidad`
JOIN producto_relacion ON producto_relacion.`id_producto_padre` = producto.id
JOIN producto AS materia_prima ON materia_prima.id = producto_relacion.`id_producto_hijo`
JOIN unidad AS unidad2 ON unidad2.id = materia_prima.id_unidad

WHERE YEAR(fecha_emision)=2016 AND id_estado IN(1,9)