<?php
App::uses('AppController', 'Controller');

class ComprobanteItemLotesController extends AppController {
    public $name = 'ComprobanteItemLotes';
    public $model = 'ComprobanteItemLote';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    /**
     * @secured(CONSULTA_COMPROBANTE_ITEM_LOTE)
     */
	public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
        
       
            
        $this->loadModel($this->model);
        $this->loadModel(EnumModel::Comprobante);
        $this->PaginatorModificado->settings = array('limit' => 100000, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $conditions = array();
        
        $codigo_lote = $this->getFromRequestOrSession($this->model.'.codigo_lote');
        $tiene_certificado = $this->getFromRequestOrSession($this->model.'.tiene_certificado');
        $validado = $this->getFromRequestOrSession($this->model.'.validado');
        
        
        if($codigo_lote!=""){
        	array_push($this->array_filter_names,array("Cod. Lote.:"=>$codigo_lote));
        	array_push($conditions, array('Lote.codigo LIKE ' => '%'. $codigo_lote . '%'));
        }
        
        if($tiene_certificado!=""){
        	array_push($this->array_filter_names,array("Tiene certificado.:"=>$tiene_certificado));
        	array_push($conditions, array('Lote.tiene_certificado' => $tiene_certificado));
        }
        
        if($validado!=""){
        	array_push($this->array_filter_names,array("Lote Validado.:"=>$validado));
        	array_push($conditions, array('Lote.validado' => $validado));
        }
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain'=>array('ComprobanteItem'=>array("Comprobante"=>array("PuntoVenta","TipoComprobante","Persona")),'Lote'=>array('TipoLote')),
            'conditions' => $conditions,
        	'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
        	
        	$valor["ComprobanteItemLote"]["pto_nro_comprobante"] = (string) $this->Comprobante->GetNumberComprobante($valor["ComprobanteItem"]["Comprobante"]['PuntoVenta']['numero'],$valor["ComprobanteItem"]["Comprobante"]['nro_comprobante']);
        	$valor["ComprobanteItemLote"]["codigo"] = $valor["Lote"]["codigo"];
        	//$valor["ComprobanteItemLote"]["tiene_certificado"] = $valor["Lote"]["tiene_certificado"];
        	$valor["ComprobanteItemLote"]["razon_social"] = $valor["ComprobanteItem"]["Comprobante"]["Persona"]["razon_social"];
			
        	if($valor["Lote"]["tiene_certificado"] == "1"){
        		$valor["ComprobanteItemLote"]["d_tiene_certificado"] = "Si";
        	}else{
        		$valor["ComprobanteItemLote"]["d_tiene_certificado"] = "No";
        	}
			
			if($valor["Lote"]["validado"] == "1"){
        		$valor["ComprobanteItemLote"]["d_validado"] = "Si";
        	}else{
        		$valor["ComprobanteItemLote"]["d_validado"] = "No";
        	}
        	
        	
        	
        	
        	$valor["ComprobanteItemLote"]["codigo_tipo_comprobante"] =  $valor["ComprobanteItem"]["Comprobante"]['TipoComprobante']['codigo_tipo_comprobante'];
        	$valor["ComprobanteItemLote"]["d_lote"] = 	  				$valor["Lote"]["d_lote"];
        	$valor["ComprobanteItemLote"]["fecha_lote"] = 				$valor["Lote"]["fecha_emision"];
        	$valor["ComprobanteItemLote"]["d_tipo_lote"] = 				$valor["Lote"]["TipoLote"]["d_tipo_lote"];
        	
        	unset($valor["Lote"]);
        	unset($valor["ComprobanteItem"]);
        
        	$this->{EnumModel::Comprobante}->formatearFechas($valor);
        }
        
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
    }
    
    
    /**
     * @secured(BAJA_COMPROBANTE_ITEM_LOTE)
     */
    
    public function delete($id) {
    	$this->loadModel($this->model);
    	$mensaje = "";
    	$status = "";
    	$this->{$this->model}->id = $id;
		
    		try{
    			if ($this->{$this->model}->delete()) {
    				
    				$status = EnumError::SUCCESS;
    				$mensaje = "Ha sido eliminado/a exitosamente.";
    				$output = array(
    						"status" => $status,
    						"message" => $mensaje,
    						"content" => ""
    				);
    				
    			}else{
    				$status = EnumError::ERROR;
    				$mensaje = "NO ha podido ser eliminado/a.";
    			}
    			
    		}catch(Exception $ex){
    			$status = EnumError::ERROR;
    			$mensaje = $ex->getMessage();
    		}
    	$output = array(
    			"status" => $status,
    			"message" => $mensaje,
    			"content" => ""
    	);
    	
    	$this->output = $output;

        $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
    }
    
    /**
     * @secured(CONSULTA_COMPROBANTE_ITEM_LOTE)
     */
    public function getModel($vista='default'){
    	
    	$model = parent::getModelCamposDefault();//esta en APPCONTROLLER
    	$model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
    	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    	
    	$this->set('model',$model);
    	Configure::write('debug',0);
    	$this->render($vista);
    }
    
    /**
     * @secured(CONSULTA_COMPROBANTE_ITEM_LOTE)
     */ 
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Lotes en Items de Comprobantes");
    	
    }
    
    
    
    
    
    
   
    
    

    
    
    
   
    
}
?>