<?php
App::uses('ComprobantesController', 'Controller');


  /**
    * @secured(CONSULTA_ORDEN_PAGO)
  */
class OrdenesPagoController extends ComprobantesController {
    
    public $name = EnumController::OrdenesPago;
    public $model = 'Comprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $requiere_impuestos = 1; //si el comprobante acepta impuesto, esto llama a las funciones de IVA E IMPUESTOS
    public $id_sistema_comprobante = EnumSistema::COMPRAS;
    
   
    /**
    * @secured(CONSULTA_ORDEN_PAGO)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros($this->model); 
            
        
        
        $array_conditions = array('ComprobanteItem','Persona','EstadoComprobante',
								  'Moneda','CondicionPago','TipoComprobante',
								  'ComprobanteImpuesto' => array('Impuesto'=> array('Sistema','ImpuestoGeografia')),
								  'PuntoVenta','Sucursal' => array('Provincia','Pais'),'Transportista',
								  'CarteraRendicion' =>  array('TipoCarteraRendicion'),'Usuario'); //contacto es la sucursal
        
        
            
        
        
        
        //Si pasa el ID traigo diferentes models relacionados,sino lo basico
        /*
        if(in_array('Comprobante.id',$conditions))//elijo los models a traer dependiendo si es consulta o index
            $array_conditions = array('Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante','ComprobanteImpuesto'=>array('Impuesto'=>array('Sistema','ImpuestoGeografia')),'PuntoVenta');
        else
             $array_conditions = array('Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante','ComprobanteImpuesto','PuntoVenta');
        
        */ 
                       
        $this->paginate = array('paginado'=>$this->paginado,
        	'maxLimit'=> $this->maxLimitRows,
             'contain' =>$array_conditions,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.id desc'
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
    
    
        
        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'cotizacion.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'cotizacion.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
             
        	if(isset($valor['TipoComprobante']) && $valor['TipoComprobante']['id']>0){
                 $valor[$this->model]['d_tipo_comprobante'] = 		$valor['TipoComprobante']['d_tipo_comprobante'];
                 $valor[$this->model]['codigo_tipo_comprobante'] =  $valor['TipoComprobante']['codigo_tipo_comprobante'];
                 
             }
             unset($valor['TipoComprobante']);
             
             
             if(isset($valor['CarteraRendicion']) && $valor['CarteraRendicion']["id"]>0){
             	$valor[$this->model]['id_tipo_cartera_rendicion'] = $valor['CarteraRendicion']['id_tipo_cartera_rendicion'];
             	$valor[$this->model]['d_tipo_cartera_rendicion'] =  $valor['CarteraRendicion']['TipoCarteraRendicion']['d_tipo_cartera_rendicion'];
             	$valor[$this->model]['d_cartera_rendicion'] =  $valor['CarteraRendicion']['d_cartera_rendicion'];
             	
             }
             unset($valor['CarteraRendicion']);
             /*
             foreach($valor[$this->model]['ComprobanteItem'] as &$producto ){
                 
                 $producto["d_producto"] = $producto["Producto"]["d_producto"];
                 $producto["codigo_producto"] = $this->getProductoCodigo($producto);
                 unset($producto["Producto"]);
             }
             */
             if(isset($valor['Moneda'])){
                 $valor[$this->model]['d_moneda'] = $valor['Moneda']['d_moneda'];
                 $valor[$this->model]['d_moneda_simbolo'] = $valor['Moneda']['simbolo'];
             
             }
             unset($valor['Moneda']);
             
             if(isset($valor['ComprobanteImpuesto'])){
                  $valor[$this->model]['ComprobanteImpuestoDefault'] =  $this->getImpuestos($valor['ComprobanteImpuesto'],$valor["Comprobante"]["id"],array(EnumSistema::COMPRAS),array(),EnumTipoImpuesto::RETENCIONES);
                  $valor[$this->model]['ComprobanteImpuestoTotal'] = (string) $this->TotalImpuestos($valor[$this->model]['ComprobanteImpuestoDefault']);
             }
             
             if(isset($valor['EstadoComprobante'])){
                $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
               
             }
             unset($valor['EstadoComprobante']);
			 
             if(isset($valor['CondicionPago']) && $valor['CondicionPago']['id']>0){
                $valor[$this->model]['d_condicion_pago'] = $valor['CondicionPago']['d_condicion_pago'];
               
             }
             unset($valor['CondicionPago']);
             
             if(isset($valor['Transportista']) && $valor['Transportista']['id']>0){
                $valor[$this->model]['t_razon_social'] = $valor['Transportista']['razon_social'];
                $valor[$this->model]['t_direccion'] = $valor['Transportista']['calle'].'-'.$valor['Transportista']['numero_calle'].'-'.$valor['Transportista']['localidad'].$valor['Transportista']['ciudad'];
                unset($valor['Transportista']);
             }
             
             if(isset($valor['Persona'])){
                 if($valor['Persona']['id']== null)
                 {    
                    $valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social']; 
                 }
                 else{
                     $valor[$this->model]['razon_social'] =   $valor['Persona']['razon_social'];
                     $valor[$this->model]['id_tipo_iva'] =    $valor['Persona']['id_tipo_iva'];
                     $valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
                 }
                 
                 unset($valor['Persona']);
             }
             
            
          
               if(isset($valor['TipoComprobante'])){
                  $valor['Comprobante']['d_tipo_comprobante'] =$valor['TipoComprobante']['d_tipo_comprobante'];
              }

              $valor["Comprobante"]["es_importado"] = (string) $this->Comprobante->esImportado($valor['ComprobanteItem']);
              
              
              if(isset($valor['PuntoVenta'])){
                $valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
                unset($valor['PuntoVenta']);
             }
              
              unset($valor['ComprobanteItem']);
              
              
              if(isset($valor['Usuario'])){
              	
              	$valor['Comprobante']['d_usuario'] = $valor['Usuario']['nombre'];
              }
              
           /* if(isset())
              $comprobantes_relacionados = $this->getComprobantesRelacionados($id);
           */
            
            unset($valor['Sucursal']);
            unset($valor['ComprobanteImpuesto']);
           $this->{$this->model}->formatearFechas($valor); 
        }
        
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    //fin vista json  
    }

    /**
    * @secured(ADD_ORDEN_PAGO)
    */
    public function add(){
        
    $this->loadModel("Cheque");
    $cheques_error = '';
    
     $this->ProcesaDescuento();
     $this->ConvertirPreciosUnitariosAPositivo($this->request->data);
    
 
    
    
   
     
    if($this->Cheque->validarEgresoCheque($this->request->data,$cheques_error) == 1)    
            parent::add();    
     else{
              $mensaje = $cheques_error;
              $tipo = EnumError::ERROR;  
              
              $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                );      
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content"));         
     }
            
              
         
         
     }
    
    
    
    
    /**
    * @secured(MODIFICACION_ORDEN_PAGO)
    */
    public function edit($id){
  
    $this->loadModel("Cheque");
    $this->loadModel("Comprobante");
    $cheques_error = '';
    $hay_error = 0;
    $mensaje = "";
    
    
    $this->ConvertirPreciosUnitariosAPositivo($this->request->data);
    if($this->Comprobante->getDefinitivoGrabado($id) != 1 ){
    //si envia decuento entonces calculo el neto
    $this->ProcesaDescuento();
    
     
    if($this->Cheque->validarEgresoCheque($this->request->data,$cheques_error) == 1){    
              //si es definitivo le seteo el id_comprobante a los cheques
             
              
              $orden_pago = array();
              $valida_fecha_op = $this->validarFechaOrdenPago($orden_pago);
              
              
              
              if(!$valida_fecha_op){

              	$fch_contable = date("d-m-Y", strtotime($orden_pago["Comprobante"]["fecha_contable"]));
              	$tipo = EnumError::ERROR;
              	$mensaje = "Existe una Orden de Pago con fecha contable superior a la actual. Fecha de la Orden de Pago: ".$fch_contable.". La OP actual debe tener la misma fecha contable o mayor.";
              	throw new Exception($mensaje);
              }
              	
              
              
              if($this->Comprobante->getDefinitivoGrabado($id) != 1 && $this->Comprobante->getDefinitivo($this->request->data) == 1  && $this->Cheque->tieneCheques($this->request->data)== 1 && $hay_error == 0 ){
              	
              	
              	$id_comprobante_destino = $this->request->data["Comprobante"]["id"];
              	$array_cheques = $this->Cheque->getArrayCheques($this->request->data);
              	$ds = $this->Cheque->getdatasource();
              	
              	
              	try{
              		$ds->begin();
              		$this->Cheque->actualizaComprobanteCheques($id_comprobante_destino,$array_cheques,"id_comprobante_salida");
              		
              		
              		$array_cheque_terceros = array();
              		$array_cheque_propios = array();
              		
              		$this->Cheque->SeparaCheques($array_cheques,$array_cheque_terceros,$array_cheque_propios);//esta funcion le paso un array con cheques y me devuelve 2 arrays uno con los cheques de terceros y otro con los cheques propios
              		$this->Cheque->actualizaEstadoCheques(EnumEstadoCheque::Emitido,$array_cheque_propios);//actualizo el estado de los cheques
              		$this->Cheque->actualizaEstadoCheques(EnumEstadoCheque::Aplicado,$array_cheque_terceros);//actualizo el estado de los cheques
              		
              		
              		
              		
              	
              	}catch(Exception $e){
              		
              		$tipo = EnumError::ERROR;
              		$mensaje = 'Hubo una falla cuando se actulizaron los comprobantes de Salida en los cheques'.$e->getMessage();
              		$ds->rollback();
              		$hay_error = 1;
              	}
              	
              	
              	
              }
              
              
        
              if($hay_error == 0){ /*Sino hubo falla al actualizar los cheques entonces si edito*/
              
              
              	
                $diferencia = $this->request->data["Comprobante"]["diferencia_valores"]; //si es (-) debe generar CI si es (+) genera DI
                 
                
                if($diferencia < 0)
                	$id_tipo_comprobante_a_generar = EnumTipoComprobante::CreditoInternoCompra;
                else if($diferencia >0)
                	$id_tipo_comprobante_a_generar = EnumTipoComprobante::DebitoInternoCompra;
                	
                 //el total del comprobante es la sumatoria de los valores ingresados
                 $this->request->data["Comprobante"]["total_comprobante"] = $this->request->data["Comprobante"]["total_comprobante"] + $diferencia*-1;
                         
              
                
             // $this->request->data["Comprobante"]["genera_ci "] =1;
                if($this->request->data["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion &&
                		($this->request->data["Comprobante"]["chkGeneraCreditoInterno"] == 1 || $this->request->data["Comprobante"]["chkGeneraDebitoInterno"] == 1) && $diferencia!=0){
                     
                     
                     
                     //debo generar credito interno ya que me esta pagando de mas. El balance del recibo entre recibido y imputados va a ser 0 ya que el CI compensa. Lo hago transaccional asi es mas seguro.
                	$contador = false;
                 	if($diferencia < 0)
                 		$contador = $this->chequeoContador(EnumTipoComprobante::CreditoInternoCompra,$this->request->data["Comprobante"]["id_punto_venta"]);
                    elseif($diferencia >0)
                    	$contador = $this->chequeoContador(EnumTipoComprobante::DebitoInternoCompra,$this->request->data["Comprobante"]["id_punto_venta"]);
                    
                     if($contador){
                         
                         
                         $dsc = $this->Comprobante->getdatasource();
                         
                        
                         
                         try{
                         
                             $dsc->begin();
                             $this->Comprobante->CalcularMonedaBaseSecundaria($this->request->data,$this->model);//esta linea la pongo aca asi puedo acceder al valor_moneda, valor_moneda2 antes de llamar al edit
                             
                             $nro_comprobante_interno = $this->obetener_ultimo_numero_comprobante($id_tipo_comprobante_a_generar,$this->request->data["Comprobante"]["id_punto_venta"],5,"+");
                             
                             
                             if($diferencia < 0)
                                $observacion = "Credito Interno por Orden de Pago Nro:".$this->request->data["Comprobante"]["nro_comprobante"];
                             else if($diferencia > 0)
                             	$observacion = "Debito Interno por Orden de Pago Nro:".$this->request->data["Comprobante"]["nro_comprobante"];
                             
                             $cabecera_comprobante = $this->Comprobante->getCabeceraComprobante(abs($diferencia),abs($diferencia),abs($diferencia),$id_tipo_comprobante_a_generar,date('Y-m-d'),EnumEstadoComprobante::CerradoReimpresion,"",$this->request->data["Comprobante"]["id_moneda"],$this->request->data["Comprobante"]["valor_moneda"],$this->request->data["Comprobante"]["valor_moneda2"],$this->request->data["Comprobante"]["valor_moneda3"],$observacion,$nro_comprobante_interno,$this->request->data["Comprobante"]["id_persona"],$this->Auth->user('id'),1,0,1,$this->request->data["Comprobante"]["id_punto_venta"]);
                             
                             
                             $items_comprobante = $this->Comprobante->getComprobanteItem(1,abs($diferencia),abs($diferencia),1,1,EnumIva::exento,$observacion,1,$id,0);//el credito interno tiene como origen el recibo
                             $id_comprobante_interno = $this->Comprobante->InsertaComprobante($cabecera_comprobante,$items_comprobante);
                             
                             //Se inserto entonces actualizo los datos del recibo.
                             //$this->request->data["Comprobante"]["diferencia"] = 0;
                             
                             
                             //////////Agrego el item Credito interno de ventas al Recibo
                             
                             /*
                             $item_comprobante_credito_interno = $this->Comprobante->getComprobanteItem("",$diferencia,$diferencia,count($this->request->data["ComprobanteItemComprobante"])+1,count($this->request->data["ComprobanteItemComprobante"])+1,EnumIva::exento,"",1,$id_credito_interno,0);
                             
                             array_push($this->request->data["ComprobanteItemComprobante"],$item_comprobante_credito_interno["ComprobanteItem"]);
                             */
                             
                             parent::edit($id);
                             $dsc->commit();
                             
                             if($this->Cheque->tieneCheques($this->request->data)== 1 && isset($ds))
                             	$ds->commit();
                             
                             $errror_retenciones = "";
                             $mensaje_retenciones = "";
                             $this->generarRetencionesPorOrdenPago($id, $errror_retenciones,$mensaje_retenciones,$this->error);//genera las retenciones
                             $mensaje_error_mail = "";
                             $this->avisoEmailOrdenPago($id,$mensaje_error_mail);//es diferente a comprobantes ya que genera retenciones PDF y el metodo aviso Email generaPDF en el editar asique debe ser llamado luego de generar los PDF
                             if($errror_retenciones == "success")
                            	 return 0; 
                            else{
                            	 	
                            	 	$tipo = $errror_retenciones;
                            	 	$mensaje.= $mensaje." ".$mensaje_retenciones;
                             }
                             }catch(Exception $e){
                             
                             $tipo = EnumError::ERROR;
                             $mensaje = 'Hubo una falla cuando se intento crear el comprobante interno. Intente nuevamente'.$e->getMessage();
                             $dsc->rollback();
                             
                             if($this->Cheque->tieneCheques($this->request->data)== 1 && $this->error == 0 && $hay_error ==0 && isset($ds) )
                             	$ds->rollback();
                             
                             
                             
                             
                         }    
                     }else{
                          $tipo = EnumError::ERROR;  
                          $mensaje = "Se debe definir el numero inicial del contador para este comprobante (Credito/Debito Interno de Compras).La Orden de Pago no pudo editarse. Inicie el contador y luego edite el recibo"; 
                         
                     }
                    
                }else{//solo edito no necesito mas nada
                    
                     parent::edit($id);
                     $errror_retenciones = "";
                     $mensaje_retenciones = "";
                     
                     if($this->Cheque->tieneCheques($this->request->data)== 1 && $this->error == 0 && $hay_error ==0 && isset($ds) )
                     	$ds->commit();
                     	
                     
                     $this->generarRetencionesPorOrdenPago($id, $errror_retenciones,$mensaje_retenciones,$this->error);//genera las retenciones
                     $$mensaje_error_mail = "";
                     $this->avisoEmailOrdenPago($id,$mensaje_error_mail);//es diferente a comprobantes ya que genera retenciones PDF y el metodo aviso Email generaPDF en el editar asique debe ser llamado luego de generar los PDF
                     if($errror_retenciones == "success")
                    	 return 0; 
                     else{
                     	
                     	$tipo = $errror_retenciones;
                     	$mensaje.= $mensaje." ".$mensaje_retenciones;
                     }
                    
                     //return 0; 
                }
              
              
               
              
               }   
    }else{
              $mensaje = $cheques_error;
              $tipo = EnumError::ERROR;  
              
             
     }
      /*Aca solo entra si hubo error si entra por el edit sale por flujo de mensaje*/
     
     
    }else{
    	
    	$mensaje = 'El comprobante en esta instacia no puede ser editado';
    	$tipo = EnumError::ERROR;
    	$this->error = 1;
    }
    
    
    
      $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                );      
            $this->set($output);
      
            $this->set("_serialize", array("status", "message", "content"));
            return;         
                
            
        
    }
    
    

    
   /**
    * @secured(BAJA_ORDEN_PAGO)
    */
   public function deleteItem($id_item,$externo=1){
        
        $this->loadModel($this->model);
        
        
        
    
        
        
   
        $this->{$this->model}->ComprobanteItem->id = $id_item;
       try{
           
           //aca se debe chequear si el item es borrable. O sea sino se factura etc.
           
            if ($this->{$this->model}->ComprobanteItem->delete() ) {
                
                   
        
                $status = EnumError::SUCCESS;
                $mensaje = "El Item ha sido eliminado exitosamente.";
              
            }else{
              
                
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
                
                
            }
        }
        catch(Exception $ex){ 
              $mensaje = "Ha ocurrido un error, el item no ha podido ser borrado";
               $status = EnumError::ERROR; 
        }
        
        
        
        
        if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
        }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
        }
        
        
        
        
    }
     
    
      /**
    * @secured(CONSULTA_ORDEN_PAGO)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

    }
    
  
    /**
    * @secured(REPORTE_ORDEN_PAGO)
    */
    public function pdfExport($id,$vista=''){
        
    
    $this->loadModel("Comprobante");
    $orden_pago = $this->Comprobante->find('first', array(
                                                        'conditions' => array('Comprobante.id' => $id),
                                                        'contain' =>array('TipoComprobante')
                                                        ));
                                                        
     
    $comprobantes_relacionados = $this->getComprobantesRelacionados($id,0,1,0);  
                                       
      if($orden_pago){
        
        
      	$credito_internos_relacionados = array();
      	$debito_internos_relacionados = array();
      	foreach($comprobantes_relacionados as $comprobante){
      		
      		
      		if( isset($comprobante["Comprobante"]) && $comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::CreditoInternoCompra)
      			array_push($credito_internos_relacionados,$comprobante);
      			
      		if( isset($comprobante["Comprobante"]) && $comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::DebitoInternoCompra)
      			array_push($debito_internos_relacionados,$comprobante);
      			
      			
      	}
      	
      	$this->set('credito_interno_relacionados',$credito_internos_relacionados);
      	$this->set('debito_interno_relacionados',$debito_internos_relacionados);
      	
        
       
        
         $letra = $orden_pago["TipoComprobante"]["letra"];
         $vista = "ordenPago".$letra;  
         parent::pdfExport($id,$vista);    
        
  
      }
     
        
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
        
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);
    }

    
    
    
    
    
 
    
     private function ProcesaDescuento(){
     	
     	
     	if($this->request->data[$this->model]["id_tipo_comprobante"] == EnumTipoComprobante::OrdenPagoAutomatica || $this->request->data[$this->model]["id_tipo_comprobante"] == EnumTipoComprobante::OrdenPagoManual){
        
         if(isset($this->request->data[$this->model]["descuento"]) && $this->request->data[$this->model]["descuento"]>0){
        
            $this->request->data[$this->model]["subtotal_bruto"] = $this->request->data[$this->model]["total_comprobante"]*(1/$this->request->data[$this->model]["descuento"]);
            $this->request->data[$this->model]["subtotal_neto"]  = $this->request->data[$this->model]["subtotal_bruto"];
    
        }else{
        
            $this->request->data[$this->model]["subtotal_bruto"] = $this->request->data[$this->model]["total_comprobante"];
            $this->request->data[$this->model]["subtotal_neto"]  = $this->request->data[$this->model]["total_comprobante"];
        }
        return;
        
     	} 
        
        
    }
    
    
    
    public function Anular($id_orden_pago){
        
        $this->loadModel("Comprobante");
        $this->loadModel("Asiento");
        $this->loadModel("Cheque");
        
        
          $recibo = $this->Comprobante->find('first', array(
                    'conditions' => array('Comprobante.id'=>$id_orden_pago,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                    'contain' => array('ComprobanteValor','Asiento','ChequeSalida')
                ));
                
                 $ds = $this->Comprobante->getdatasource(); 
                 // $ds->rollback();
          $aborto = 0;
          
          $listado_cheques = array();
          $listado_cheques_propios = array();
          
                   
            if( $recibo && $recibo["Comprobante"]["id_estado_comprobante"]!= EnumEstadoComprobante::Anulado ){
                
                try{
                	
                	
                	if($this->PeriodoValidoParaAnular($recibo) == 0)
                		throw new Exception("El Comprobante no es posible anularlo. Ya que el Peri&oacute;do contable en el cual se encuenta no esta disponible.");
                	
                    $ds->begin();
                    
                    
                    
                    
                  
                            $aborto =0; 
                       
                       
                     if($aborto!=1){
                               foreach($recibo["ChequeSalida"] as $cheque){
                                   
                                if(($cheque["id_estado_cheque"]!= EnumEstadoCheque::Rechazado)){//quiere decir que NO salio el cheque y lo puedo usar
                                
                                     
                                     if($cheque["id_tipo_cheque"] == EnumTipoCheque::Terceros){
                                     $this->Cheque->updateAll(
                                                            array('Cheque.id_comprobante_salida' => NULL,'Cheque.id_estado_cheque'=>EnumEstadoCheque::Cartera ),
                                                            array('Cheque.id' => $cheque["id"]) );   
                                
                                     array_push($listado_cheques,$cheque["nro_cheque"]);
                                    }else{//el propio lo envio nuevamente a la cartera que el usuario elija
                                        
                                           $this->Cheque->updateAll(
                                                            array('Cheque.id_comprobante_salida' => NULL,'Cheque.id_estado_cheque'=>EnumEstadoCheque::Cartera ),
                                                            array('Cheque.id' => $cheque["id"]) );   
                                      array_push($listado_cheques_propios,$cheque["nro_cheque"]);   
                                    }
                                }
                            }        
                        
                      
                            if(isset($recibo["ComprobanteValor"])){
                            	foreach($recibo["ComprobanteValor"] as $comprobante_valor){
                            		if($comprobante_valor["monto"] == 0){//quiere decir que NO salio el cheque y lo puedo usar
                            			
                            			$this->Comprobante->ComprobanteValor->delete($comprobante_valor["id"]);
                            		}
                            	}
                            }
                        
                        $output_asiento_revertir = array("id_asiento"=>0);
                        
                        if($recibo["Comprobante"]["comprobante_genera_asiento"] == 1)
                            $output_asiento_revertir = $this->Asiento->revertir($recibo["Asiento"]["id"]);  /*Se debe revertir el asiento del comprobante*/  
                        
                        /* Si genero RETENCIONES se deben anular */
                        
                        
                        
                        $this->Comprobante->updateAll(
                                                        array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
                                                        array('Comprobante.id' => $id_orden_pago) );
                                                        
                        $this->Comprobante->anularRetencionPropia($id_orden_pago);           
                        
                        
                        
                        //busco los creditos internos hechos
                        $credito_interno = $this->Comprobante->ComprobanteItem->find('first',array(
                        		
                        		'conditions'=>array("ComprobanteItem.id_comprobante_origen"=>$id_orden_pago,"Comprobante.id_tipo_comprobante"=>EnumTipoComprobante::CreditoInternoCompra),
                        		'contain'=>array("Comprobante")
                        		
                        		
                        ) );
                        
                        //anulo los CI hechos
                        if($credito_interno)
                        	$this->Comprobante->updateAll(
                        			array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
                        			array('Comprobante.id' => $credito_interno["Comprobante"]["id"]) );
                        	
                        	
                        	//busco los debitos internos hechos
                        $debito_interno = $this->Comprobante->ComprobanteItem->find('first',array(
                        			
                        			'conditions'=>array("ComprobanteItem.id_comprobante_origen"=>$id_orden_pago,"Comprobante.id_tipo_comprobante"=>EnumTipoComprobante::DebitoInternoCompra),
                        			'contain'=>array("Comprobante")
                        			
                        			
                        	) );
                        
                        
                       
                       
                       if($debito_interno)
                        	$this->Comprobante->updateAll(
                        			array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
                        			array('Comprobante.id' => $debito_interno["Comprobante"]["id"]) );
                        	
                                                        
                        $ds->commit(); 
                        $tipo = EnumError::SUCCESS;
                        
                        
                        $op = $this->Comprobante->find('first', array(
                        		'conditions' => array('Comprobante.id'=>$id_orden_pago,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                        		'contain' => false
                        ));
                        
                        $this->CerrarComprobantesAsociados($op);/*leo Nuevamente el Recibo*/
                        
                      
                       $mensaje = '';
                        if(count($listado_cheques)>0)
                            $mensaje .= ". Se liberaron en la cartera los siguientes cheques: Cheque Nro:".implode(" Cheque Nro: ",$listado_cheques);
                            
                        if(count($listado_cheques_propios)>0)
                        	$mensaje .= " Se Anularon los siguientes cheques propios y no pueden volver a ser utilizados: Cheque Nro:".implode(" Cheque Nro: ",$listado_cheques_propios);    
                        
                        $mensaje = "El Comprobante se anulo correctamente ".$mensaje;       
                        
                        
                        $op_anulada = $this->Comprobante->find('first', array(
                        		'conditions' => array('Comprobante.id'=>$id_orden_pago,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                        		'contain' => false
                        ));
                        
                        $this->Auditar($id_orden_pago,$op_anulada);
                        
                                                
                    }else{
                        
                        $tipo = EnumError::ERROR;
                        $mensaje = "El Comprobante NO puede anularse ya existe un cheque que se ingreso fue utilizado para otra operatoria";
                    }
              
                
                
                   
                    
                    
                  
                  
                    
                     
               }catch(Exception $e){ 
                
                $ds->rollback();
                $tipo = EnumError::ERROR;
                $mensaje = "No es posible anular el Comprobante ERROR:".$e->getMessage();
                
              }  
                            
            }else{
                
               $tipo = EnumError::ERROR;
               $mensaje = "No es posible anular el Comprobante ya se encuenta ANULADO";
            } 
            
            
            if(isset( $output_asiento_revertir["id_asiento"]))
            	$id_asiento_revertir =  $output_asiento_revertir["id_asiento"];
            else
            	$id_asiento_revertir = 0;
            
        $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
        		"id_asiento" => $id_asiento_revertir,
                );      
        echo json_encode($output);
        die();
        
         
        
    }
    
    
    public function CalcularImpuestos($id_comprobante,$id_tipo_impuesto='',$error=0,$message=''){
        
    	if($this->request->data[$this->model]["id_tipo_comprobante"] == EnumTipoComprobante::OrdenPagoAutomatica || $this->request->data[$this->model]["id_tipo_comprobante"] == EnumTipoComprobante::OrdenPagoManual  || $this->request->data[$this->model]["id_tipo_comprobante"] == EnumTipoComprobante::OrdenPagoManual){
       $total = 0;
       
       if($this->request->data[$this->model]["aprobado"] != 1 ){
       	$total = parent::CalcularImpuestos($id_comprobante,EnumTipoImpuesto::RETENCIONES);
       $total += $this->totalRetencionesManual($error,$message);  
       $total += $this->totalRetencionesAutomatico($error,$message);  
       }
       
       return $total;
    	}else{
    		return 0;
    	}
        
        
    }
    
    
    public function getQueryImpuestos($id_comprobante,$id_sistema,$id_tipo_impuesto,$array_impuestos_not_in=array() ){
        
        $this->loadModel("PersonaImpuestoAlicuota");
        $this->loadModel("Impuesto");
        $this->loadModel("ComprobanteImpuesto");
        
   $query_tipo_impuesto = '';
         
    if($id_tipo_impuesto>0)
        $query_tipo_impuesto = " AND i.id_tipo_impuesto=".$id_tipo_impuesto;
        
    
        
    $impuestos_not_in = ''; 
    
    $array_impuestos_not_in = array();
    
    $array_impuestos_ganancias = $this->Impuesto->getImpuestosArray(EnumTipoImpuesto::RETENCIONES,EnumSubTipoImpuesto::RETENCION_GANANCIA);
    
    $array_not_in = array_merge($array_impuestos_not_in,$array_impuestos_ganancias);
    
    $array_impuestos_retenciones_iva = $this->Impuesto->getImpuestosArray(EnumTipoImpuesto::RETENCIONES,EnumSubTipoImpuesto::RETENCION_IVA);
    
    $array_not_in = array_merge($array_not_in,$array_impuestos_retenciones_iva);
    
    
 
   
     
    
     
    if(count($array_not_in)>0)
        $impuestos_not_in = " AND i.id NOT IN (".implode(",",$array_not_in).")";
     
     $items_comprobantes = $this->Comprobante->ComprobanteItem->find('all',array( 'conditions' => array('ComprobanteItem.id_comprobante' => $id_comprobante,"NOT"=>array('ComprobanteOrigen.id_estado_comprobante'=>array(EnumEstadoComprobante::Anulado,EnumEstadoComprobante::Cancelado))),'contain'=>array('ComprobanteOrigen')));
    
     $array_id_comprobantes = array(0);
     
     foreach($items_comprobantes as $comprobante){
        /*ACA EXCLUYO CIERTOS COMPROBANTES EN LO QUE NO TIENEN QUE CALCULARSE LAS RETENCIONES*/ 
     	if(!in_array($comprobante["ComprobanteOrigen"]["id_tipo_comprobante"],array(EnumTipoComprobante::SaldoInicioProveedor,EnumTipoComprobante::CreditoInternoCompra,EnumTipoComprobante::DebitoInternoCompra,EnumTipoComprobante::AnticipoCompra)))
            array_push($array_id_comprobantes,$comprobante["ComprobanteOrigen"]["id"]);   
     }
     
     //       $neto_de_iva_parcial =  $total_parcial - round(($total_parcial*$total_iva_comprobante)/$total_comprobante,2);
     
     //SUM(ROUND(ComprobanteItem.precio_unitario / (ComprobanteOrigen.total_comprobante/ComprobanteOrigen.subtotal_neto),4)
    
    $query = "	



					SELECT 
                                                ".$id_comprobante." AS id_comprobante,
                                                comprobante.id_moneda AS id_moneda,
                                                pia.id_impuesto,
												  ROUND(
												  SUM(
												
												      (
												        c.precio_unitario  
												        /*Esto se lo resto porque los exentos y no gravados no influyen en el calculo de la retencion*/
												      ) / (
												        (
												          comprobante.total_comprobante / comprobante.subtotal_neto
												        ) * tipo_comprobante.signo_comercial
												      )
												    )- 

													SUM(IFNULL(
												          (SELECT 
												            SUM(precio_unitario * cantidad* tipo_comprobante.signo_comercial) 
												          FROM
												            comprobante_item 
														JOIN comprobante co ON co.id = comprobante_item.id_comprobante
														JOIN tipo_comprobante ON tipo_comprobante.id = co.id_tipo_comprobante
												          WHERE id_comprobante = c.id_comprobante_origen 
												            AND comprobante_item.id_iva IN (4, 8, 9)
															AND tipo_comprobante.requiere_iva = 1 /*Esto lo pongo para que no le reste las facturas C*/
															

															),
												          0
												        ))
												  ,4) AS base_imponible,
												  (
												     ROUND(
												  SUM(
												
												      (
												        c.precio_unitario  
												        /*Esto se lo resto porque los exentos y no gravados no influyen en el calculo de la retencion*/
												      ) / (
												        (
												          comprobante.total_comprobante / comprobante.subtotal_neto
												        ) * tipo_comprobante.signo_comercial
												      )
												    )-


												 SUM(IFNULL(
												          (
															SELECT 
												            SUM(precio_unitario * cantidad* tipo_comprobante.signo_comercial) 
												          FROM
												            comprobante_item 
														JOIN comprobante co ON co.id = comprobante_item.id_comprobante
														JOIN tipo_comprobante ON tipo_comprobante.id = co.id_tipo_comprobante
												          WHERE id_comprobante = c.id_comprobante_origen 
												            AND comprobante_item.id_iva IN (4, 8, 9)
															AND tipo_comprobante.requiere_iva = 1

														),
												          0
												        ))
												  ,4)
												    
												    
												    
												    
												    * (pia.porcentaje_alicuota / 100)
												  ) 
												  
												  
												  
												  AS importe_impuesto,
											
                                                pia.porcentaje_alicuota AS tasa_impuesto,
                                                ig.codigo_afip,
                                                i.`base_imponible_desde`,
                                                i.`base_imponible_hasta`,
												pia.fecha_vigencia_desde,
												pia.fecha_vigencia_hasta,
												pia.sin_vencimiento
                                              FROM
                                                persona_impuesto_alicuota pia 
                                                JOIN impuesto i 
                                                  ON i.id = pia.id_impuesto 
                                                JOIN comprobante_item c 
                                                  ON c.id_comprobante_origen IN(".implode(",",$array_id_comprobantes).")
                                                 JOIN impuesto_geografia ig
                                                  ON ig.id = i.id_impuesto_geografia
                                                 JOIN comprobante on comprobante.id = c.id_comprobante_origen 
												JOIN tipo_comprobante ON tipo_comprobante.id = comprobante.id_tipo_comprobante
											
                                              WHERE i.id_sistema = ".$id_sistema." 
                                                AND pia.id_persona = comprobante.id_persona
                                                ".$query_tipo_impuesto."  
                                                ".$impuestos_not_in."
                                                
                                                AND c.id_comprobante=".$id_comprobante."

                                               
                                                AND c.activo = 1
												AND pia.porcentaje_alicuota>0

												AND pia.activo = 1


						
										
												GROUP BY id_impuesto

                                              ";
                                               
    
   
    
     $data = $this->PersonaImpuestoAlicuota->query($query);//ejecuto la query
     
     $data = $this->PersonaImpuestoAlicuota->filtrarBaseImponible($data);//filtra por base imponible desde y hasta
                                              
                   
  
     $id_persona = $this->Comprobante->getPersona($id_comprobante);      
     
     
     if(!$data)//si esta vacio entonces lo redefino
        $data = array();                                   
     
        $retenciones_iva_compra = $this->PersonaImpuestoAlicuota->find('all', array(
        		'conditions' => array('PersonaImpuestoAlicuota.id_persona' => $id_persona,'Impuesto.id_tipo_impuesto'=>EnumTipoImpuesto::RETENCIONES,"Impuesto.id_sistema"=>EnumSistema::COMPRAS,"Impuesto.id_sub_tipo_impuesto"=>EnumSubTipoImpuesto::RETENCION_IVA,'PersonaImpuestoAlicuota.activo'=>1),
        		'contain' =>array('Impuesto')
        ));
        
        
        //$this->PersonaImpuestoAlicuota->tieneImpuesto($id_persona,EnumImpuesto::RETENCIONIVACOMPRAS,1)
        foreach($retenciones_iva_compra as $impuesto_retencion_iva){
         
     	
     	
        	$base_imponible_desde =  $this->Impuesto->getMinimoAplicacion($impuesto_retencion_iva["Impuesto"]["id"]);
        	$base_imponible_hasta =  $this->Impuesto->getMaximoAplicacion($impuesto_retencion_iva["Impuesto"]["id"]);
         
         
         
            $impuestos_iva = $this->ComprobanteImpuesto->find('all', array(
            		'conditions' => array('ComprobanteImpuesto.id_comprobante' => $array_id_comprobantes,'ComprobanteImpuesto.id_impuesto' => EnumImpuesto::IVACOMPRAS,'Comprobante.id_tipo_comercializacion'=>$impuesto_retencion_iva["Impuesto"]["id_tipo_comercializacion"]),
                                                'contain' => array('Comprobante')
                                                ));
            
            if($impuestos_iva){
                
            	$alicuota_retencion = $this->PersonaImpuestoAlicuota->getAlicuotaRetencion($id_persona,$impuesto_retencion_iva["Impuesto"]["id"]);
                
                
                $persona_impuesto_alicuota_record = $this->PersonaImpuestoAlicuota->find('first', array(
                		'conditions' => array('PersonaImpuestoAlicuota.id_persona' => $id_persona,'PersonaImpuestoAlicuota.id_impuesto' => $impuesto_retencion_iva["Impuesto"]["id"]),
                		'contain' => false
                ));
                
                
                $total_iva = 0;
                
                foreach($impuestos_iva as $impuesto){//estos son los ivas de los comprobantes
                   
                   
                    $comprobante_item = $this->Comprobante->ComprobanteItem->find('first', array(
                                                'conditions' => array('ComprobanteItem.id_comprobante' => $id_comprobante,'ComprobanteItem.id_comprobante_origen' => $impuesto["Comprobante"]["id"]),
                                                'contain' => array('Comprobante','ComprobanteOrigen')
                                                ));
                                                
                                                 
                   
                   
                    
                    $total_iva_comprobante = $impuesto["ComprobanteImpuesto"]["importe_impuesto"]/$impuesto["Comprobante"]["valor_moneda2"];
                    $total_parcial = $comprobante_item["ComprobanteItem"]["precio_unitario"];
                    $total_comprobante = $impuesto["Comprobante"]["total_comprobante"]/$impuesto["Comprobante"]["valor_moneda2"];
                    
                    $total_iva_a_imputar_parcial =  round(($total_parcial*$total_iva_comprobante)/$total_comprobante,2);
                    
                    
                   $total_iva +=  $total_iva_a_imputar_parcial;
                    
                   
                    
                }
                
                
                   $total_iva = round($total_iva,2);
                
                   
                   
                   $array_aux = array() ;
                   $array_aux["pia"]["id_impuesto"] = $impuesto_retencion_iva["Impuesto"]["id"];
                   $array_aux["pia"]["sin_vencimiento"] = $persona_impuesto_alicuota_record["PersonaImpuestoAlicuota"]["sin_vencimiento"];
                   $array_aux["pia"]["fecha_vigencia_hasta"] = $persona_impuesto_alicuota_record["PersonaImpuestoAlicuota"]["fecha_vigencia_hasta"];
                   $array_aux["pia"]["fecha_vigencia_desde"] = $persona_impuesto_alicuota_record["PersonaImpuestoAlicuota"]["fecha_vigencia_desde"];
                   $array_aux["c"]["base_imponible"] = $total_iva ;
                   $array_aux["ig"]["codigo_afip"] = '';
                   $array_aux[0]["importe_impuesto"] = ($total_iva*$alicuota_retencion)/100;
                   $array_aux["pia"]["tasa_impuesto"] = $alicuota_retencion;
                   
                   if($array_aux[0]["importe_impuesto"]>=$base_imponible_desde  && $array_aux[0]["importe_impuesto"]<= $base_imponible_hasta  && $array_aux[0]["importe_impuesto"]>0){ //chequeo si el minimo es potable y lo puedo agregar
                    	array_push($data,$array_aux);
                
                    }    
         
         
         
                
                
            } 
         
         
     }                                      
    
      return $data;
    }
    
    
    protected function borraImpuestos($id_comprobante){
    	
    	$this->loadModel(EnumModel::Comprobante);
    	
    	$op = $this->{EnumModel::Comprobante}->find('first', array(
    			'conditions' => array('Comprobante.id' => $id_comprobante),
    			'contain' =>false
    	));
    	
    	
    	if($this->request->data[$this->model]["id_tipo_comprobante"] == EnumTipoComprobante::OrdenPagoAutomatica || $this->request->data[$this->model]["id_tipo_comprobante"] == EnumTipoComprobante::OrdenPagoManual){
    	
    		if($this->request->data[$this->model]["aprobado"] != 1 || $op[EnumModel::Comprobante]["aprobado"] != 1)//si se aprobo no borro nada
        	$this->Comprobante->ComprobanteImpuesto->deleteAll(array('ComprobanteImpuesto.id_comprobante' => $id_comprobante,'Impuesto.autogenerado'=>1));
    	}
        
        
    }
    
    private function totalRetencionesManual(&$error=0,&$message=''){
         
        $this->loadModel("ComprobanteImpuesto"); 
        $this->loadModel(EnumModel::Impuesto); 
         //$this->request->data[$this->model]
        
        //$comprobanteImpuesto = new ComprobanteImpuesto();
        $impuesto_obj = new Impuesto();
        //$retencion = 0;
        
       /* if(isset($this->request->data["ComprobanteImpuesto"]))
            $retencion = $comprobanteImpuesto->getImpuesto($this->request->data["ComprobanteImpuesto"],EnumImpuesto::RETENCIONDEGANANCIACOMPRA);
        */
       
         
         
         if(isset($this->request->data["ComprobanteImpuesto"]) ){ 
          
        $total_impuestos_manuales =0;
        $comprobantes_impuestos = array();    
        foreach($this->request->data["ComprobanteImpuesto"] as $impuesto){  
         
         
        	if($impuesto_obj->esAutogenerado($impuesto["id_impuesto"])== 0 && $impuesto["importe_impuesto"]>0){  //si el impuesto es manual entonces me quedo con los datos  
                    
                 $comprobante_impuesto = array();
                 $comprobante_impuesto["id_comprobante"] = $impuesto["id_comprobante"];
                 $comprobante_impuesto["id_impuesto"] = $impuesto["id_impuesto"] ;
                 $comprobante_impuesto["importe_impuesto"] = (string) round($impuesto["importe_impuesto"],2);  
                 $total_impuestos_manuales +=$comprobante_impuesto["importe_impuesto"];
                 
                 array_push($comprobantes_impuestos,$comprobante_impuesto);
         }
         }
         
         
         if(count($comprobantes_impuestos)>0){
         	$this->ComprobanteImpuesto->saveAll($comprobantes_impuestos); 

            $errores = $this->ComprobanteImpuesto->validationErrors;
                
            $errores_string = "";
            //var_dump($errores);
            foreach ($errores as $error){ //recorro los errores y armo el mensaje
                if(!is_array($error))
                $errores_string.= "&bull; ".$error."\n";
                else
                $errores_string.= $this->getModelErrorFromArray($error)."\n"; 
            }

            $message = $errores_string  ;
            if(strlen($message)>0)
            $error = 1;


            return $total_impuestos_manuales;
         }else{
             
             return 0;
         }
         
         
         }else{
             
             return 0;
         }
        
    }
    
    
     private function totalRetencionesAutomatico(&$error=0,&$message=''){
         
     	/*Calculo de Retenciones de Ganancia
     	 * http://estudiogiagante.com.ar/calculo-retencion-impuesto-ganancias.html
     	 * 
     	 * */
     	  
     	if(isset($this->request->data[$this->model]["tiene_impuesto"]) && $this->request->data[$this->model]["tiene_impuesto"] == 1 
			  && $this->request->data[$this->model]["aprobado"] != 1 ){ /*si la OP se aprobo no recalculo retenciones*/
     	
          $this->loadModel("PersonaImpuestoAlicuota");
          $this->loadModel("Impuesto");
          $this->loadModel("ComprobanteImpuesto");
          
          
          
          $mes = date('m');
          $comprobante_impuesto_obj = new ComprobanteImpuesto(); 

          
          $id_persona = $this->request->data[$this->model]["id_persona"];//obtengo de la orden de pago el id_persona
          $id_comprobante = $this->id;//obtengo de la orden de pago el id_persona
          $id_tipo_comprobante =  $this->request->data[$this->model]["id_tipo_comprobante"];//en este caso es OP
          
          
          //busco si la persona tiene alguna "Retencion" de compra asignada
          $retenciones_compra = $this->PersonaImpuestoAlicuota->find('all', array(
                                                        'conditions' => array('PersonaImpuestoAlicuota.id_persona' => $id_persona,
														'Impuesto.id_tipo_impuesto'=>EnumTipoImpuesto::RETENCIONES,
														"Impuesto.id_sistema"=>EnumSistema::COMPRAS,
														"Impuesto.id_sub_tipo_impuesto"=>EnumSubTipoImpuesto::RETENCION_GANANCIA,
														'PersonaImpuestoAlicuota.activo'=>1,"Impuesto.autogenerado"=>1),
                                                        'contain' =>array('Impuesto')
                                                        ));
          if($retenciones_compra){
              
              $comprobantes_impuestos = array(); 
			  
              foreach($retenciones_compra as $retencion){
                  
                  
              	
                     $id_tipo_comercializacion = $retencion["Impuesto"]["id_tipo_comercializacion"];
                     
                     $neto_gravado_acumulado_mensual = $this->getTotalNetoAcumuladoXTipoComprobante($id_persona,$mes,$id_tipo_comprobante,0,$id_comprobante,array(EnumEstadoComprobante::CerradoReimpresion),$id_tipo_comercializacion);
                     
                     $importe_pagar_neto_iva_local = $this->getTotalNetoAcumuladoXTipoComprobante($id_persona,$mes,$id_tipo_comprobante,$id_comprobante,0,array(EnumEstadoComprobante::Abierto),$id_tipo_comercializacion);//sumatoria de items netos de IVA y agrupados por id_tipo_comercializacion
                  
                     $total_retencion_mes = $comprobante_impuesto_obj->getAcumulado($mes,EnumTipoImpuesto::RETENCIONES,EnumSubTipoImpuesto::RETENCION_GANANCIA,$id_persona,$id_tipo_comercializacion);//sumo las retenciones para un mismo tipo de comercializacion
                     
                     $minimo_no_sujeto_retencion = $retencion["Impuesto"]["minimo_no_sujeto_retencion"];
                     
                     $tasa = $retencion["Impuesto"]["alicuota"];//se toma de la tabla impuesto para el caso de retenciones de ganancias
                     
                     if($retencion["Impuesto"]["pertenece_escala"] !="1"){
                     
	                     if($total_retencion_mes == 0){ //sino retuve retenciones anteriormente
		                     
		                    ///ej: 100.000                         //0 del mens    + sumatoria de FC de la OP sin IVA
	                     	if( $minimo_no_sujeto_retencion > ($neto_gravado_acumulado_mensual + $importe_pagar_neto_iva_local) )
								
		                        $neto_sujeto_retencion = 0;
		                     else
		                     	$neto_sujeto_retencion =   ($neto_gravado_acumulado_mensual + $importe_pagar_neto_iva_local)  - $minimo_no_sujeto_retencion;
							
								$retencion_acumulada_determinada = 0;
								
								if($neto_sujeto_retencion >0)
									$retencion_acumulada_determinada =  round(($neto_sujeto_retencion/100)*$tasa,2);
							}else{ 
	                     		//si ya hice retenciones en ese id_tipo_comercializacion entonces la base_imponible es el neto sin iva
		                     	$neto_sujeto_retencion = $importe_pagar_neto_iva_local;
		                     	$retencion_acumulada_determinada = round(($neto_sujeto_retencion/100)*$tasa,2);
							}
                     
                     }else{//si pertenece a escala el impuesto
                     	
                     	$neto_sujeto_retencion = 0;
                     	$neto_sujeto_retencion =   ($neto_gravado_acumulado_mensual + $importe_pagar_neto_iva_local)  - $minimo_no_sujeto_retencion;
                     	$retencion_acumulada_determinada = 0;
                     	
                     	if($neto_sujeto_retencion >0)/*Si es por escala le sumo el importe fijo_retencion - le resto la base imponible desde*/
                     		$retencion_acumulada_determinada =  $retencion["Impuesto"]["importe_fijo_retencion"] + round((($neto_sujeto_retencion - $retencion["Impuesto"]["base_imponible_desde"]) /100)*$tasa,2) - $total_retencion_mes;
                     	
                     }
					 
                     if( ( $total_retencion_mes >0 || 

                           $importe_pagar_neto_iva_local >= $retencion["Impuesto"]["base_imponible_desde"]   
						   && $importe_pagar_neto_iva_local < $retencion["Impuesto"]["base_imponible_hasta"]
                     	)
                     		&& $retencion["Impuesto"]["pertenece_escala"] !="1"
                     		&&  $retencion_acumulada_determinada > $retencion["Impuesto"]["importe_minimo_retencion"]
                      ){  //agrego la retencion
                         
                          $comprobante_impuesto = array();
                          $comprobante_impuesto["id_comprobante"] = $id_comprobante;
                          $comprobante_impuesto["id_impuesto"] = $retencion["Impuesto"]["id"] ; 
                          $comprobante_impuesto["importe_impuesto"] = (string) $retencion_acumulada_determinada; 
                          $comprobante_impuesto["tasa_impuesto"] = (string) $tasa; 
                          $comprobante_impuesto["base_imponible"] = (string) $neto_sujeto_retencion; 
                          array_push($comprobantes_impuestos,$comprobante_impuesto); 
                         
                         
                     }elseif($retencion["Impuesto"]["pertenece_escala"] =="1" && $retencion_acumulada_determinada>0 &&  $neto_sujeto_retencion >= $retencion["Impuesto"]["base_imponible_desde"] 
                     		&& $neto_sujeto_retencion<=$retencion["Impuesto"]["base_imponible_hasta"] 
                     		
                     		){//esta condicion es para las escalas que son en base al neto sujeto a retencion
                     	
                     			$comprobante_impuesto = array();
                     			$comprobante_impuesto["id_comprobante"] = $id_comprobante;
                     			$comprobante_impuesto["id_impuesto"] = $retencion["Impuesto"]["id"] ;
                     			$comprobante_impuesto["importe_impuesto"] = (string) $retencion_acumulada_determinada;
                     			$comprobante_impuesto["tasa_impuesto"] = (string) $tasa;
                     			$comprobante_impuesto["base_imponible"] = (string) $neto_sujeto_retencion;
                     			array_push($comprobantes_impuestos,$comprobante_impuesto);
                     }
              }

              if(count($comprobantes_impuestos)>0)
              	$this->ComprobanteImpuesto->saveAll($comprobantes_impuestos);
                
               $errores = $this->ComprobanteImpuesto->validationErrors;
                    
                $errores_string = "";
                //var_dump($errores);
                foreach ($errores as $error){ //recorro los errores y armo el mensaje
                    if(!is_array($error))
                        $errores_string.= "&bull; ".$error."\n";
                    else
                       $errores_string.= $this->getModelErrorFromArray($error)."\n"; 
                }
				
                if(strlen($errores_string)>0)
                    $error = 1;
                else
                    return $retencion_acumulada_determinada; //no hubo errores
          }else
            return 0;    
                  
     	}else{
     		return 0;
     	}

     }
    
    
    protected function actualizoMontos($id_c,$nro_c,$total_iva,$total_impuestos){
     return;   
    }
    
    
    /**
    * @secured(CONSULTA_ORDEN_PAGO)
    */
    public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
    {
        parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
    }
    
    
    /**
     * @secured(BTN_EXCEL_ORDENESPAGO)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Ordenes de Pago");
    	
    	
    	
    }
    
    
    public function CerrarComprobantesAsociados($recibo_objeto){
    	
    	/*traigo los items del recibo que son comprobantes*/
    	
    	$this->loadModel("ComprobanteItem");
    	$this->loadModel("Comprobante");
    	
    	
    	if( isset($recibo_objeto["Comprobante"]["id"]) && $recibo_objeto && $recibo_objeto["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion || $recibo_objeto["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::Anulado){
    		
    		
    		$recibo = $this->ComprobanteItem->find('all', array(
    				'conditions' => array('ComprobanteItem.id_comprobante' => $recibo_objeto["Comprobante"]["id"]),
    				'contain' =>array('Comprobante','ComprobanteOrigen')
    		));
    		
    		
    		
    		
    		
    		$id_moneda = $recibo_objeto["Comprobante"]["id_moneda"];
    		
    		$comprobante_obj = new Comprobante();
    		
    		foreach($recibo as $comprobante_item){
    			
    			$comprobante_origen = $comprobante_item["ComprobanteOrigen"];
    			
    			$id_comprobante = $comprobante_origen["id"];
    			
    			
    			$saldo = 0;
    			if($recibo_objeto["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion )/*solo pregunto el saldo si es cerrado*/
    				$saldo = abs($comprobante_obj->getComprobantesImpagos($comprobante_origen["id_persona"],1,$id_comprobante,1,EnumSistema::COMPRAS,0,$id_moneda));
    				
    				
    				
    				if($saldo <= 0.01 && $recibo_objeto["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion ){
    					
    					
    					$this->Comprobante->updateAll(
    							array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Cumplida ),
    							array('Comprobante.id' => $id_comprobante) );
    					
    					
    				}else{
    					
    					
    						
    						$this->Comprobante->updateAll(
    								array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::CerradoReimpresion ),
    								array('Comprobante.id' => $id_comprobante) );
    				
    					
    					
    				}
    				
    		}
    		
    	}
    	
    	
    }
    
    
    /**
     * @secured(ADD_RETENCION_PROPIA)
     */
    public function generarRetencionesPorOrdenPago($id_comprobante,&$error,&$mensaje,$error_en_edit_op){
    	
    	$this->loadModel("ComprobanteImpuesto");
    	$this->loadModel($this->model);
    	$this->RequestHandler->ext = 'json';
    	
    	
    	$error = EnumError::SUCCESS;
    	if($error_en_edit_op == 0 && $this->request->data["Comprobante"]["definitivo"] == 1 && $this->request->data["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion){
	    	$cimpuestos = $this->ComprobanteImpuesto->find('all', array(
	    			'conditions' => array('ComprobanteImpuesto.id_comprobante' => $id_comprobante,'Impuesto.id_tipo_impuesto'=>EnumTipoImpuesto::RETENCIONES),
	    			'contain' =>array("Comprobante",'Impuesto'=>array("TipoComprobante"))
	    	));
	    	
	    	
	    	if(count($cimpuestos)>0){//si tengo retenciones las tratato de generar
	    		foreach($cimpuestos as $key =>$retencion){
	    			
	    			if($retencion["ComprobanteImpuesto"]["importe_impuesto"]>0){
	    			
			    			$id_comprobante_impuesto = $retencion["ComprobanteImpuesto"]["id"];
			    			$this->id_impuesto = $retencion["ComprobanteImpuesto"]["id_impuesto"];//es es para el Obetener numero de comprobante
			    			$id_tipo_comprobante = $this->getTipoRetencion($retencion["ComprobanteImpuesto"]["id_impuesto"]);
			    			
			    			
			    			$contador = $this->chequeoContador($id_tipo_comprobante,$this->request->data["Comprobante"]["id_punto_venta"],$retencion["ComprobanteImpuesto"]["id_impuesto"]);
			    			
			    			$comprobante["Comprobante"]["id_tipo_comprobante"] = $id_tipo_comprobante;
			    			$comprobante["Comprobante"]["id_punto_venta"] = $retencion["Comprobante"]["id_punto_venta"];
			    			$comprobante["Comprobante"]["total_comprobante"] = $retencion["ComprobanteImpuesto"]["importe_impuesto"];
			    			$comprobante["Comprobante"]["id_moneda"] = $retencion["Comprobante"]["id_moneda"];
			    			$comprobante["Comprobante"]["valor_moneda"] = $retencion["Comprobante"]["valor_moneda"];
			    			$comprobante["Comprobante"]["id_estado_comprobante"] = EnumEstadoComprobante::CerradoReimpresion;
			    			$comprobante["Comprobante"]["definitivo"] = 1;
			    			$comprobante["Comprobante"]["fecha_add"] = date("Y-m-d H:i:s");
			    			$comprobante["Comprobante"]["fecha_generacion"] = date("Y-m-d H:i:s");
			    			$comprobante["Comprobante"]["fecha_contable"] = $retencion["Comprobante"]["fecha_contable"];//la retencion tiene la fecha_contable del comprobante
			    			$comprobante["Comprobante"]["id_persona"] = $retencion["Comprobante"]['id_persona'];
			
			    			$condicion_impuesto = " and id_impuesto=".$this->id_impuesto;
			    			$condition = "WHERE id_tipo_comprobante=".$id_tipo_comprobante." and id_punto_venta=".$retencion["Comprobante"]["id_punto_venta"].$condicion_impuesto.";";
			    			$this->{$this->model}->query("UPDATE comprobante_punto_venta_numero SET numero_actual = LAST_INSERT_ID(numero_actual + 1)".$condition);
			    			$result = $this->{$this->model}->query("SELECT numero_actual from comprobante_punto_venta_numero ".$condition);
			    			
			    			try{
			    				if($contador){
			    					$comprobante["Comprobante"]["nro_comprobante"] = $result[0]["comprobante_punto_venta_numero"]["numero_actual"];
			    				
				    				$id_comprobante_retencion = $this->Comprobante->InsertaComprobante($comprobante,array());
				    				
				    				if($id_comprobante_retencion>0){
				    					$id_comprobante_generado = $id_comprobante_retencion;
				    					
				    					/*actualizo el comprobante impuesto con el id de la retencion*/
				    					$this->ComprobanteImpuesto->updateAll(
				    							array('ComprobanteImpuesto.id_comprobante_referencia' => $id_comprobante_generado ),
				    							array('ComprobanteImpuesto.id' => $id_comprobante_impuesto) );

				    					$error = EnumError::SUCCESS;
				    					//return 0;
				    					//$this->pdfExport($id_comprobante_impuesto);
				    				}else{
				    					$mensaje = "Ha ocurrido un error y no puede crearse la Retenci&oacute;n".$e->getMessage();
				    					$error = EnumError::ERROR;	
				    				}
			    				}else{
			    					$mensaje .= "Ha ocurrido un error.Debe parametrizar desde parametricas el numerador para algunas de las Retenciones presentes, recuerde que el impuesto tambien debe asignarse en la parametrizaci&oacute;n. Luego de esto genere la Retenci&oacute;n desde el bot&oacute;n Generar Retenci&oacute;n de la Orden de Pago";
			    					$error = EnumError::ERROR;	
			    				}
			    			}catch(Exception $e){
			    				$mensaje = "Ha ocurrido un error y no puede crearse la Retenci&oacute;n".$e->getMessage();
			    				$error = EnumError::ERROR;
			    			}
					}	
	    		}
	    	}
    	}
    }
    
    
    protected  function validarFechaOrdenPago(&$orden_pago){
    	
    	$fecha_contable = $this->request->data["Comprobante"]["fecha_contable"];
    	
    	$orden_pago = $this->Comprobante->find('first', array(
    			'conditions' => array('Comprobante.fecha_contable >' => $fecha_contable,
									  'Comprobante.id_tipo_comprobante ' => array(EnumTipoComprobante::OrdenPagoManual), //MP - modificaion por problema de Carga de factura de compras con fecha contable de hoy teniendo OTRO COMPROBANTE con fecha contable mayor()
									  'Comprobante.id_estado_comprobante'=>array(EnumEstadoComprobante::CerradoReimpresion) ),
    			'contain' =>false,
    			'order' =>'Comprobante.fecha_contable desc'
    	));
    	
    	
    	if($orden_pago && count($orden_pago)>0)
    		return false;
    	else 
    		return true;	
    }
    
    
    public  function avisoEmail($id_comprobante=0,&$mensaje_error = ""){ //la sobreescribo porque en OP cambia el flujo y se debe llamar en otro lado
    	
    	parent::avisoEmail($id_comprobante,$mensaje_error);
    	
    	
    }
    
    protected  function avisoEmailOrdenPago($id_comprobante=0,$mensaje_error = ""){ //la sobreescribo porque en OP cambia el flujo y se debe llamar en otro lado
    	
       		parent::avisoEmail($id_comprobante,$mensaje_error);
    }
}
?>