<?php

App::uses('NotasController', 'Controller');




class NotasDebitoMiPymeVentaController extends NotasController

{
	
	
	public $name = EnumController::NotasDebitoMiPymeVenta;
	
	
	
	/**
	 * @secured(CONSULTA_NOTA)
	 */
	public
	
	function getModel($vista = 'default')
	{
		
		
		$model = parent::getModelCamposDefault(); //esta en APPCONTROLLER
		$model = $this->editforView($model, $vista); //esta funcion edita y agrega campos para la vista, debe estar LOCAL
	}
	
	private
	function editforView($model, $vista)
	{ //esta funcion recibe el model y pone los campos que se van a ver en la grilla
		$model = parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
		
		// $model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
		$this->autoRender = false;
		$this->layout = false;
		$this->viewPath = "/NotasMiPyme/";
		$this->set('model', $model);
		Configure::write('debug', 0);
		$this->render($vista);
	}
	
	
	protected function arrayCabeceraAfip($CbteTipo,$PtoVta,$Concepto,$DocTipo,$DocNro,$CbteDesde,$CbteHasta,$CbteFch,$FchServDesde,$FchServHasta,$FchVtoPago,$ImpTotal,$ImpTotConc,$ImpNeto,$ImpOpEx,$ImpIVA,$ImpTrib,$MonId,$MonCotiz){
		
		
		$FeCAEReq = array (
				'FeCAEReq' => array (
						'FeCabReq' => array (
								'CantReg' => 1,
								'CbteTipo' => $CbteTipo,
								'PtoVta' => $PtoVta
						),
						'FeDetReq' => array (
								'FECAEDetRequest' => array(
										'Concepto' => $Concepto,
										'DocTipo' => $DocTipo,
										'DocNro' => $DocNro,
										'CbteDesde' => $CbteDesde,
										'CbteHasta' => $CbteHasta,
										'CbteFch' => $CbteFch,
										'FchServDesde' => $FchServDesde,
										'FchServHasta' => $FchServHasta,
										'ImpTotal' => number_format(abs($ImpTotal),2,'.',''),
										'ImpTotConc' => number_format(abs($ImpTotConc),2,'.',''),
										'ImpNeto' => number_format(abs($ImpNeto),2,'.',''),
										'ImpOpEx' => number_format(abs($ImpOpEx),2,'.',''),
										'ImpIVA' => number_format(abs($ImpIVA),2,'.',''),
										'ImpTrib' => number_format(abs($ImpTrib),2,'.',''),
										'MonId' => $MonId,
										'MonCotiz' => $MonCotiz
								)
						)
				),
		);
		
		return $FeCAEReq;
		
		
		
	}
	
	protected function ArmarOpcionalesAfip(&$Opcional_array){
	
		$this->loadModel(EnumModel::Comprobante);
		$this->loadModel(EnumModel::TipoComprobante);
		
		$opcionales = array();		
		$Opcional_array = array( 'Opcional' => array ());
		
		/*Asignacion de VALORES opcionales*/
		// $opcional["id"] = 2101;//CBU del EMISOR
		// $opcional["valor"] = $this->TipoComprobante->getCuentaBancaria(
					// $this->request->data['Comprobante']['id_tipo_comprobante'])["nro_cbu"];
		// array_push($opcionales, $opcional);
		
		$opcional["id"] = 22;//referencia NOTA DE CREDITO 
		/*'N' el comprado no rechazo; 'S' el comprador YA LO Rechazo*/
		if( $this->request->data['Comprobante']['chk_asociado_rechazado'] == '1' ) {
			$opcional["valor"] = 'S'; 
		}
		else {
			$opcional["valor"] = 'N'; 
		}
		array_push($opcionales, $opcional);
		
		
		$opcional["id"] = 23;//referencia comercial
		$opcional["valor"] = $this->request->data['Comprobante']['orden_compra_externa'];
		
		array_push($opcionales, $opcional);
		/*Procesar los campos */
		foreach($opcionales as $opcional){
			
			$opcion = array();
			$opcion["Id"] =   $opcional["id"];
			$opcion["Valor"] =  $opcional["valor"];
				
			array_push($Opcional_array["Opcional"],$opcion);
		}
		/*<ar:Opcionales>
			<ar:Opcional>
				<ar:Id>22</ar:Id>
				<ar:Valor>N</ar:Valor>
			</ar:Opcional>
		</ar:Opcionales>*/
	}
	
	
	
	protected function ArmarComprobantesAsociadosAfip(&$Asociados_array){
		
		$this->loadModel(EnumModel::Comprobante);
		$this->loadModel(EnumModel::TipoComprobante);
		
		//$this->request->data['ComprobanteItem']['ComprobanteOrigen']
		
		$this->Comprobante->contain(array('TipoComprobante','PuntoVenta','ComprobanteItem' => array('Producto'=>array('Iva'),'ComprobanteItemOrigen'=>array('Comprobante'=>array('TipoComprobante','PuntoVenta','Persona'))),'Moneda','Persona'=> array('Pais','Provincia','TipoDocumento','TipoIva'),'CondicionPago','ComprobanteImpuesto'=>array('Impuesto')));
		$this->Comprobante->id = $this->request->data["Comprobante"]["id"];
		$comprobante_guardado = $this->Comprobante->read();
		
		$opcionales = array();
		
			$Asociados_array = array( 'CbteAsoc' => array ());

			$this->loadModel(EnumModel::TipoComprobante);
			
			$auxiliar = array();
		
			foreach($comprobante_guardado['ComprobanteItem'] as $comp_asoc){
				
				$datoEmpresa = $this->Session->read('Empresa');
				
				$empresaCuit  = str_replace("-","",$datoEmpresa["DatoEmpresa"]["cuit"]);
				
				$fecha_comprobante = strtotime(  $comp_asoc["ComprobanteItemOrigen"]['Comprobante']['fecha_contable']);
				$CbteFch= date("Ymd", $fecha_comprobante);
				/*$opcional["Cuit"] = (string) $comp_asoc["ComprobanteItemOrigen"]["Comprobante"]["Persona"]["cuit"];*/
				 $opcional["Cuit"] = (string) $empresaCuit;
			
				$opcional["Tipo"] = $comp_asoc["ComprobanteItemOrigen"]["Comprobante"]["TipoComprobante"]["codigo_afip"];
				$opcional["PtoVta"] = $comp_asoc["ComprobanteItemOrigen"]["Comprobante"]["PuntoVenta"]["numero"];
				$opcional["Nro"] = $comp_asoc["ComprobanteItemOrigen"]['Comprobante']["nro_comprobante"];
				$opcional["CbteFch"] = (string) $CbteFch;
	
				if(!in_array($opcional["Tipo_cbte"]."-".$opcional["Cbte_nro"], $auxiliar)){
				
					array_push($auxiliar, $opcional["Tipo_cbte"]."-".$opcional["Cbte_nro"]);
					array_push($Asociados_array["CbteAsoc"],$opcional);
				}
			}
			/*
			 * <CbteAsoc>
					<Tipo_cbte>short</Tipo_cbte>
					<Punto_vta>int</Punto_vta>
					<Cbte_nro>long</Cbte_nro>
					<Cuit>string</Cuit>
					<Fecha_cbte>string</Fecha_cbte>
				</CbteAsoc>
			 */
	}	
}

?>