<?php 
        include(APP.'View'.DS.'TarjetasCredito'.DS.'json'.DS.'tarjeta_credito.ctp');
        
        
        
        App::import('Model','Comprobante');
		$comprobante = new Comprobante();
		$contabilidad = $comprobante->getActivoModulo(EnumModulo::CONTABILIDAD);
        
		$model["d_banco_sucursal"]["show_grid"] = 1;
        $model["d_banco_sucursal"]["header_display"] = "Sucursal Bancaria";
        $model["d_banco_sucursal"]["order"] = "1";
         $model["d_banco_sucursal"]["width"] = "20";
        $model["d_banco_sucursal"]["text_colour"] = "#000000";
        
        
        $model["d_tipo_tarjeta_credito"]["show_grid"] = 1;
        $model["d_tipo_tarjeta_credito"]["header_display"] = "Tipo Tarjeta";
        $model["d_tipo_tarjeta_credito"]["order"] = "2";
        $model["d_tipo_tarjeta_credito"]["width"] = "20";
        $model["d_tipo_tarjeta_credito"]["text_colour"] = "#0000FF";



        $model["nro_tarjeta"]["show_grid"] = 1;
        $model["nro_tarjeta"]["header_display"] = "Nro. Tarjeta";
        $model["nro_tarjeta"]["order"] = "3";
        $model["nro_tarjeta"]["width"] = "20";
        $model["nro_tarjeta"]["text_colour"] = "#0000FF";
        
        
        
        
        $model["d_tipo_cuenta_bancaria"]["show_grid"] = 1;
        $model["d_tipo_cuenta_bancaria"]["header_display"] = "Tipo Cuenta";
        $model["d_tipo_cuenta_bancaria"]["order"] = "4";
        $model["d_tipo_cuenta_bancaria"]["width"] = "20";
        $model["d_tipo_cuenta_bancaria"]["text_colour"] = "#0000FF";
        
        
        
        $model["d_moneda"]["show_grid"] = 1;
        $model["d_moneda"]["header_display"] = "Moneda";
        $model["d_moneda"]["order"] = "5";
        $model["d_moneda"]["width"] = "10";
        $model["d_moneda"]["text_colour"] = "#0000FF";
        
        
        
        $model["d_propia"]["show_grid"] = 1;
        $model["d_propia"]["header_display"] = "Propia?";
        $model["d_propia"]["order"] = "6";
        $model["d_propia"]["width"] = "10";
        $model["d_propia"]["text_colour"] = "#0000FF";
        
        
        if($contabilidad == 1){
	        $model["codigo_cuenta_contable"]["show_grid"] = 1;
	        $model["codigo_cuenta_contable"]["header_display"] = "C&oacute;digo Cuenta Cont.";
	        $model["codigo_cuenta_contable"]["order"] = "7";
	        $model["codigo_cuenta_contable"]["width"] = "10";
	        $model["codigo_cuenta_contable"]["text_colour"] = "#0000FF";
	        
	        $model["d_cuenta_contable"]["show_grid"] = 1;
	        $model["d_cuenta_contable"]["header_display"] = "Cuenta Contable";
	        $model["d_cuenta_contable"]["order"] = "8";
	        $model["d_cuenta_contable"]["width"] = "10";
	        $model["d_cuenta_contable"]["text_colour"] = "#0000FF";
		 }

 $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "TarjetaCredito",
                    "content" => $model
                );
        echo json_encode($output);
         
        

?>