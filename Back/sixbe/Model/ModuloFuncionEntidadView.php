<?php
App::uses('AppModel', 'Model');



class ModuloFuncionEntidadView extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'modulo_funcion_entidad_view';
    
    
    

    public $actsAs = array('Containable');
    
    public $source = "modulo_funcion_entidad_view";
    public $view = "modulo_funcion_entidad_view";
    
    
  
   
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'El id del item no puede ser vacio',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);
    
    
    
    
    
    /*
    public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     $this->virtual_fields_view = array(
		 "d_producto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
		 "d_unidad"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
		 "codigo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "item_codigo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_facturado"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_remitido"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_notas_credito_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_pendiente_a_facturar_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_pendiente_a_remitir_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_pendiente_notas_credito_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_remitido_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_facturado_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_nota_credito_posibles"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_facturado"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_nota_credito"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_pendiente_nota_credito"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_pendiente_a_remitir"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_pendiente_a_facturar"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "d_moneda"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "d_moneda_simbolo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "dias_atraso_item"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "fecha_generacion"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
		 "dias_atraso_global"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "moneda_simbolo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "persona_tel"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "razon_social"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "d_estado_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "orden_compra_externa"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "id_moneda"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "nro_comprobante"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "id_iva"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "valor_moneda"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "valor_moneda2"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "valor_moneda3"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "fecha_entrega"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
		 "id_persona"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "id_deposito_origen"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "d_deposito_origen"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "id_deposito_destino"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "t_razon_social"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "t_direccion"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_recibida_irm"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_pendiente_irm"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "pto_nro_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "d_tipo_comprobante_origen"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "pto_nro_comprobante_origen"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "nro_comprobante_origen"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "fecha_generacion_origen"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
		 "fecha_vencimiento_origen"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_comprobante_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_orden_compra_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_pendiente_a_gastar"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_remito_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_pendiente_irm"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_pedido"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_pendiente_a_pedido"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_aprobada_a_facturar_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_aprobada_a_facturar"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_recibida_irm_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_pendiente_irm_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "referencia_comprobante_externo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     	 "enlazar_con_moneda"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     	 "d_iva"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     	 "cantidad_pendiente_a_armar"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     	 "total_armado"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     	 "persona_codigo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     	 "persona_id"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     	 "d_orden_armado"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_desvio"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "fecha_limite_armado"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
		 "fecha_entrega_origen"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
		 "pto_nro_pedido_interno"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		);
	}
	
	*/

	//The Associations below have been created with all possible keys, those that are not needed can be removed
/*
public $belongsTo = array(
        'Comprobante' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Producto' => array(
            'className' => 'Producto',
            'foreignKey' => 'id_producto',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ComprobanteOrigen' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante_origen',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        
        'ComprobanteItemOrigen' => array(
            'className' => 'ComprobanteItemOrigen',
            'foreignKey' => 'id_comprobante_item_origen',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'Iva' => array(
            'className' => 'Iva',
            'foreignKey' => 'id_iva',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Unidad' => array(
            'className' => 'Unidad',
            'foreignKey' => 'id_unidad',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
       'ListaPrecio' => array( //hace referencia a la sucursal del comprobante
            'className' => 'ListaPrecio',
            'foreignKey' => 'id_lista_precio',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'TipoLote' => array( //hace referencia a la sucursal del comprobante
            'className' => 'TipoLote',
            'foreignKey' => 'id_tipo_lote',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
        
       
      
 );
 
*/
    
   


}
