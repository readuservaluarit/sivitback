<?php


class ArticulosRelacionController extends AppController {
	
	
	
	public $name = 'ArticulosRelacion';
	public $model = 'ArticuloRelacion';
	public $helpers = array ('Session', 'Paginator', 'Js');
	public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
	public $padre = "ArticuloPadre";
	public $hijo = "ArticuloHijo";
	
	
	
	
	public function index($id_producto = null) {
		
		
		
		$this->loadModel($this->model);
		
		if($this->request->is('ajax'))
			$this->layout = 'ajax';
			

			$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
			
			//Recuperacion de Filtros
			$id_producto = $this->getFromRequestOrSession($this->model.'.id_producto');
			
			$chk_articulo_superiores = $this->getFromRequestOrSession($this->model.'.chk_articulos_superiores');//enviado desde pantalla Articulo Relacion.
			
			//TODO:Agregar estos filtros
			$codigo_hijo = $this->getFromRequestOrSession($this->model.'.codigo_hijo');
			$descripcion_hijo = $this->getFromRequestOrSession($this->model.'.descripcion_hijo');
			$id_deposito = $this->getFromRequestOrSession($this->model.'.id_deposito');
			
			if($chk_articulo_superiores == "1"){
				
				$id_producto_hijo = $id_producto;
				
			}
			
			
			
			
			$conditions = array();
			
			
			
			
			
			
			if( $chk_articulo_superiores == "1" )  //Restrictivo para el filtro de
			{
				/* Esto lo hice para que se busque a la inversa o sea pasando el id_componente te devuelve los productos*/
				
				array_push($conditions, array($this->model.'.id_producto_hijo' => $id_producto_hijo));
				array_push($conditions, array($this->hijo.'.activo' => 1));
				
				
				$data = $this->{$this->model}->find('all', array(
						'conditions' =>   array($conditions),
						'contain' =>array($this->hijo,$this->padre=>array('StockProducto'=>array('conditions'=>array('StockProducto.id_deposito'=>$id_deposito))) )
				));
				
				$page_count = 0;
				
				foreach($data as &$valor)
				{
					$valor[$this->model]["id_referencia"] = $valor["ArticuloRelacion"]["id"];
					$valor[$this->model]["id_producto"] = $valor["ArticuloPadre"]["id"];
					$valor[$this->model]["codigo_producto"] = $valor["ArticuloPadre"]["codigo"];
					$valor[$this->model]["d_producto"] = $valor["ArticuloPadre"]["d_producto"];
					
					$valor[$this->model]["ArticuloPadre"] = $valor["ArticuloPadre"];  //MP: Desde el listado Producto Componente se consume las propiedades del productos para entrar a Consultar / Editar del frmProducto
					
					if(isset($valor['ArticuloPadre']['StockProducto']) && $valor['ArticuloPadre']['StockProducto'][0]['id']!= null ){
						$valor[$this->model]['stock'] = $valor['ArticuloPadre']['StockProducto'][0]['stock'];
						
					}
				}
			}
			else
			{
				
				if($id_producto!='')
					array_push($conditions, array($this->model.'.id_producto_padre' => $id_producto));
					
					
					array_push($conditions, array($this->hijo.'.activo' => 1));
					
					
					$this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
							'contain' => array('ArticuloHijo'=>array('StockProducto'=>array('conditions'=>array('StockProducto.id_deposito'=>$id_deposito)))),
							'conditions' => $conditions,
							'limit' => 1000, //estoo es solo para traer todos los componentes
							'page' => $this->getPageNum()
					);
					
					// vista json
					$this->PaginatorModificado->settings = $this->paginate;
					$data = $this->PaginatorModificado->paginate($this->model);
					$page_count = $this->params['paging'][$this->model]['pageCount'];
					
					foreach($data as &$prodcom){
						
						
						$prodcom[$this->model]["d_producto"] = $prodcom['ArticuloHijo']["d_producto"];
						$prodcom[$this->model]['codigo_producto'] = $prodcom['ArticuloHijo']['codigo'];
						
						
						
						$prodcom[$this->model]["d_producto_hijo"] = $prodcom['ArticuloHijo']["d_producto"];
						$prodcom[$this->model]['codigo_hijo'] = $prodcom['ArticuloHijo']['codigo'];
						$prodcom[$this->model]['costo_hijo'] = (string) ($prodcom['ArticuloHijo']['costo']*$prodcom[$this->model]['cantidad_hijo']);
						$prodcom[$this->model]['costo_unitario_hijo'] =  (string)  $prodcom['ArticuloHijo']['costo'];
						
						if(isset($prodcom['ArticuloHijo']['StockProducto']) && $prodcom['ArticuloHijo']['StockProducto'][0]['id']!= null ){
							$prodcom[$this->model]['stock'] = $prodcom['ArticuloHijo']['StockProducto'][0]['stock'];
							
						}
						
						
						unset($prodcom['ArticuloHijo']);
						
					}
					
			}
			
			
			
			$output = array(
					"status" =>EnumError::SUCCESS,
					"message" => "list",
					"content" => $data,
					"page_count" =>$page_count
			);
			$this->set($output);
			$this->set("_serialize", array("status", "message","page_count", "content"));
			
			
			
			
			
			
			//fin vista json
			
	}
	
	/**
	 * @secured(ABM_USUARIOS)
	 */
	public function view($id) {
		$this->redirect(array('action' => 'abm', 'C', $id));
	}
	
	
	public function add() {
		if ($this->request->is('post')){
			$this->loadModel($this->model);
			
			try{
				if ($this->ArticuloRelacion->saveAll($this->request->data, array('deep' => true))){
					$mensaje = "El Item ha sido creado exitosamente";
					$tipo = EnumError::SUCCESS;
					
				}
			}catch(Exception $e){
				
				$mensaje = "Ha ocurrido un error,el item no ha podido ser asociado.".$e->getMessage();
				$tipo = EnumError::ERROR;
			}
			$output = array(
					"status" => $tipo,
					"message" => $mensaje,
					"content" => ""
			);
			//si es json muestro esto
			if($this->RequestHandler->ext == 'json'){
				$this->set($output);
				$this->set("_serialize", array("status", "message", "content"));
			}else{
				
				$this->Session->setFlash($mensaje, $tipo);
				$this->redirect(array('action' => 'index'));
			}
			
		}
		
		//si no es un post y no es json
		if($this->RequestHandler->ext != 'json')
			$this->redirect(array('action' => 'abm', 'A'));
			
			
	}
	
	
	
	public function edit($id) {
		
		
		if (!$this->request->is('get')){
			
			$this->loadModel($this->model);
			$this->ArticuloRelacion->id = $id;
			
			try{
				if ($this->ArticuloRelacion->saveAll($this->request->data)){
					if($this->RequestHandler->ext == 'json'){
						$output = array(
								"status" =>EnumError::SUCCESS,
								"message" => "La asociacion de item ha sido modificado exitosamente",
								"content" => ""
						);
						
						$this->set($output);
						$this->set("_serialize", array("status", "message", "content"));
					}else{
						$this->Session->setFlash('La asociacion item ha sido modificado exitosamente.', 'success');
						$this->redirect(array('controller' => 'Componentes', 'action' => 'index'));
						
					}
				}else{
					
					if($this->RequestHandler->ext == 'json'){
						$output = array(
								"status" =>EnumError::ERROR,
								"message" => "Ha ocurrido un error, el item no ha podido ser asociado.",
								"content" => ""
						);
						
						$this->set($output);
						$this->set("_serialize", array("status", "message", "content"));
					}else{
						$this->Session->setFlash('Ha ocurrido un error, el Componente no ha podido modificarse.', 'error');
						$this->redirect(array('controller' => 'Componentes', 'action' => 'index'));
						
					}
					
					
					
					
				}
			}catch(Exception $e){
				$this->Session->setFlash('Ha ocurrido un error, el item no ha podido modificarse.', 'error');
				if($this->RequestHandler->ext == 'json'){
					$output = array(
							"status" =>EnumError::ERROR,
							"message" => "Ha ocurrido un error, el item no ha podido ser asociado.".$e->getMessage(),
							"content" => ""
					);
					
					$this->set($output);
					$this->set("_serialize", array("status", "message", "content"));
				}else{
					$this->Session->setFlash('Ha ocurrido un error, el item no ha podido ser asociado.', 'error');
					$this->redirect(array('controller' => 'Componentes', 'action' => 'index'));
					
				}
				
			}
			$this->redirect(array('action' => 'index'));
		}
		
		if($this->RequestHandler->ext != 'json')
			$this->redirect(array('action' => 'abm', 'M', $id));
	}
	
	
	/**
	 * @secured(BORRAR_COMPONENTE_PRODUCTO)
	 */
	function delete($id) {
		$this->loadModel($this->model);
		$mensaje = "";
		$status = "";
		try{
			if ($this->ArticuloRelacion->delete($id)) {
				
				//$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
				
				$status = EnumError::SUCCESS;
				$mensaje = "La asociacion del item ha sido eliminada exitosamente.";
				$output = array(
						"status" => $status,
						"message" => $mensaje,
						"content" => ""
				);
				
			}
			
			else
				throw new Exception();
				
		}catch(Exception $ex){
			//$this->Session->setFlash($ex->getMessage(), 'error');
			
			$status = EnumError::ERROR;
			$mensaje = $ex->getMessage();
			$output = array(
					"status" => $status,
					"message" => $mensaje,
					"content" => ""
			);
		}
		
		if($this->RequestHandler->ext == 'json'){
			$this->set($output);
			$this->set("_serialize", array("status", "message", "content"));
			
		}else{
			$this->Session->setFlash($mensaje, $status);
			$this->redirect(array('controller' => $this->name, 'action' => 'index'));
			
		}
		
		
		
		
		
	}
	
	
	
	
	
	
	public function home(){
		if($this->request->is('ajax'))
			$this->layout = 'ajax';
			else
				$this->layout = 'default';
	}
	
	public function existe_codigo(){
		
		$codigo = $this->request->query['codigo'];
		$id_componente = $this->request->query['id'];
		$this->loadModel("Producto");
		
		$existe = $this->Producto->find('count',array('conditions' => array(
				'codigo' => $codigo,
				'id !=' =>$id_componente
		)
		));
		
		if($existe>0)
			$error = EnumError::ERROR;
			else
				$error = EnumError::SUCCESS;
				
				
				
				$output = array(
						"status" => $error,
						"message" => "",
						"content" => $data
				);
				$this->set($output);
				$this->set("_serialize", array("status", "message", "content"));
				
				
				
	}
	
	
	
	public function getModel($vista='default')
	{
		$model = parent::getModelCamposDefault();//esta en APPCONTROLLER
		$model =  parent::setDefaultFieldsForView($model,$vista); // esta en APPCONTROLLER
		$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
		
	}
	
	
	
	
	
	public function editforView($model,$vista)
	{
		
		$model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
		//$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
		
		$this->set('model',$model);
		$this->set('model_name',$this->model);
		$this->set('d_hijo',$this->d_hijo);
		Configure::write('debug',0);
		$this->render($vista);
		
	}
	
	
	
}
?>