<?php

class PersonasFigurasController extends AppController {
    public $name = 'PersonasFiguras';
    public $model = 'PersonaFigura';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $tiene_permisos = 0;
    
    

    
   public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

     
		
        
        $conditions = array(); 
    
		
        

        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  
             
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }

    
    
   
    public function getModel($vista='default'){
    	
    	$model = parent::getModelCamposDefault();
    	$model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
    	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
    	
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    	
    	
    	
    	$this->set('model',$model);
    	Configure::write('debug',0);
    	$this->render($vista);
    	
    	
    	
    	
    	
    	
    }


}
?>