<?php
App::uses('AppModel', 'Model');
App::uses('ComprobanteItem', 'Model');
App::uses('ComprobanteImpuesto', 'Model');
App::uses('Movimiento', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class Comprobante extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'comprobante';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        
        
    );

    
    
    /*
    public $virtualFields = array(
    		'd_tipo_comprobante' => 'TipoComprobante.d_tipo_comprobante',
    		'codigo_tipo_comprobante' => 'TipoComprobante.codigo',
    		'd_usuario' => 'Usuario.username'
    );
    */
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    public $hasOne = array(
        'Asiento' => array(
            'className' => 'Asiento',
            'foreignKey' => 'id_comprobante',
        ),
         'ComprobanteReferenciaImpuesto' => array(
            'className' => 'ComprobanteImpuesto',
            'foreignKey' => 'id_comprobante_referencia',
        )
    ); 
    
    
    public $belongsTo = array(
        'TipoComprobante' => array(
            'className' => 'TipoComprobante',
            'foreignKey' => 'id_tipo_comprobante',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Persona' => array(
            'className' => 'Persona',
            'foreignKey' => 'id_persona',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    		'PersonaSecundaria' => array(
    				'className' => 'Persona',
    				'foreignKey' => 'id_persona_secundaria',
    				'conditions' => '',
    				'fields' => '',
    				'order' => ''
    		),
        
        'EstadoComprobante' => array(
            'className' => 'EstadoComprobante',
            'foreignKey' => 'id_estado_comprobante',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Transportista' => array(
            'className' => 'Persona',
            'foreignKey' => 'id_transportista',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Moneda' => array(
            'className' => 'Moneda',
            'foreignKey' => 'id_moneda',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'DatoEmpresa' => array(
            'className' => 'DatoEmpresa',
            'foreignKey' => 'id_dato_empresa',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        /*'Sucursal' => array(
            'className' => 'Sucursal',
            'foreignKey' => 'id_sucursal',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),*/
        /*
        'Comprobante2' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante_origen',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),*/
        'PuntoVenta' => array(
            'className' => 'PuntoVenta',
            'foreignKey' => 'id_punto_venta',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CondicionPago' => array(
            'className' => 'CondicionPago',
            'foreignKey' => 'id_condicion_pago',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    'FormaPago' => array(
    				'className' => 'FormaPago',
    				'foreignKey' => 'id_forma_pago',
    				'conditions' => '',
    				'fields' => '',
    				'order' => ''
    		),
        'DepositoOrigen' => array(
            'className' => 'DepositoOrigen',
            'foreignKey' => 'id_deposito_origen',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'DepositoDestino' => array(
            'className' => 'DepositoDestino',
            'foreignKey' => 'id_deposito_destino',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'id_usuario',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Sucursal' => array( //hace referencia a la sucursal del comprobante
            'className' => 'Persona',
            'foreignKey' => 'id_sucursal',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'DetalleTipoComprobante' => array( //hace referencia a la sucursal del comprobante
            'className' => 'DetalleTipoComprobante',
            'foreignKey' => 'id_detalle_tipo_comprobante',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'CarteraRendicion' => array( //hace referencia a la sucursal del comprobante
            'className' => 'CarteraRendicion',
            'foreignKey' => 'id_cartera_rendicion',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
      'PeriodoImpositivo' => array( //hace referencia a la sucursal del comprobante
            'className' => 'PeriodoImpositivo',
            'foreignKey' => 'id_periodo_impositivo',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        
         'ComprobanteLote' => array( //hace referencia a la sucursal del comprobante
            'className' => 'ComprobanteLote',
            'foreignKey' => 'id_comprobante_lote',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        
        'ComprobanteLoteEjecucion' => array( //hace referencia a la sucursal del comprobante
            'className' => 'ComprobanteLoteEjecucion',
            'foreignKey' => 'id_comprobante_lote_ejecucion',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    	
    	'TipoComercializacion' => array( //hace referencia a la sucursal del comprobante
    				'className' => 'TipoComercializacion',
    				'foreignKey' => 'id_tipo_comercializacion',
    				'conditions' => '',
    				'fields' => '',
    				'order' => ''
    		),
         
       'MonedaEnlazada' => array( //hace referencia a la sucursal del comprobante
    				'className' => 'Moneda',
    				'foreignKey' => 'id_moneda_enlazada',
    				'conditions' => '',
    				'fields' => '',
    				'order' => ''
    		),
      'TipoDocumento' => array( //hace referencia a la sucursal del comprobante
    				'className' => 'TipoDocumento',
    				'foreignKey' => 'id_tipo_documento',
    				'conditions' => '',
    				'fields' => '',
    				'order' => ''
    		)
      
 );
    
/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'ComprobanteItem' => array(
            'className' => 'ComprobanteItem',
            'foreignKey' => 'id_comprobante',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'ComprobanteImpuesto' => array(
            'className' => 'ComprobanteImpuesto',
            'foreignKey' => 'id_comprobante',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'ComprobanteItemComprobante' => array(
            'className' => 'ComprobanteItemComprobante',
            'foreignKey' => 'id_comprobante',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'ComprobanteValor' => array(
            'className' => 'ComprobanteValor',
            'foreignKey' => 'id_comprobante',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'ChequeEntrada' => array(
            'className' => 'Cheque',
            'foreignKey' => 'id_comprobante_entrada',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
         'ChequeSalida' => array(
            'className' => 'Cheque',
            'foreignKey' => 'id_comprobante_salida',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'ChequeRechazo' => array(
            'className' => 'Cheque',
            'foreignKey' => 'id_comprobante_rechazo',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
    		'Movimiento' => array(
    				'className' => 'Movimiento',
    				'foreignKey' => 'id_comprobante',
    				'dependent' => false,
    				'conditions' => '',
    				'fields' => '',
    				'order' => '',
    				'limit' => '',
    				'offset' => '',
    				'exclusive' => '',
    				'finderQuery' => '',
    				'counterQuery' => ''
    		),
        
      
        
        
        /*
         'ComprobanteCarteraRendicion' => array(       /*Para manejar los fondos fijos y rendir gastos
            'className' => 'ComprobanteCarteraRendicion',
            'foreignKey' => 'id_cartera_rendicion',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
         */
         
         
    );
    
    
    public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
        
         
         
         $this->virtual_fields_view = array(
         
         "d_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "pto_nro_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "codigo_persona"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "razon_social"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_moneda_simbolo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_estado_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_condicion_pago"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "t_razon_social"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "t_direccion"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_sucursal"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "ComprobanteIvaTotal"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_usuario"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_moneda"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "codigo_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "ComprobanteImpuestoDefault"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null),
         "ComprobanteImpuestoIva"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null),
         "ComprobanteImpuestoTotal"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "tasa_impuesto"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "importe_impuesto"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "base_imponible"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_impuesto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "saldo"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_detalle_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "es_importado"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
         "id_tipo_iva"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_deposito_origen"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_deposito_destino"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_cartera_rendicion"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "id_comprobante_impuesto"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
         "numero"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
         "signo_comercial"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),

         "forecolor"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "backcolor"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "id_tipo_cartera_rendicion"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_tipo_cartera_rendicion"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "id_lista_precio_defecto_persona"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
         "es_para_stock"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
         "pto_nro_comprobante_origen"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		  "fecha_entrega_maxima"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
		  "fecha_entrega_minima"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
		  "cotizacion"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "fecha_limite_armado"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
         "fecha_entrega_origen"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
         "codigo_producto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_provincia"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "id_producto"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
         "Producto"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null),
         "cantidad"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "cantidad_cierre"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "nro_comprobante_origen"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
         "total_por_comprobante"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "c_retirado"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "c_terminado"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "mp_cantidad"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "mp_retirado"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "mp_terminado"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_tipo_documento"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_forma_pago"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "descripcion"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         		
       
         );
         
         }     
    
    public function getSucursalEnvio($comprobante){
        
        
        if(isset($comprobante['Sucursal']) && isset($comprobante['Sucursal']['id']) ){  //si esta seteado la Sucursal quiere decir que la direccion de facturacion es diferente a la de origen
        
        
            
           $comprobante["Persona"]["calle"] =  'Sucursal: '.$comprobante["Sucursal"]["nombre"].' - '.$comprobante["Sucursal"]["calle"];
           $comprobante["Persona"]["numero_calle"] =  $comprobante["Sucursal"]["numero_calle"];
           $comprobante["Persona"]["localidad"] =  $comprobante["Sucursal"]["localidad"];
           $comprobante["Persona"]["ciudad"] =  $comprobante["Sucursal"]["ciudad"];
           $comprobante["Persona"]["Provincia"]["d_provincia"] = $comprobante["Sucursal"]["Provincia"]["d_provincia"];
           $comprobante["Persona"]["Provincia"]["d_provincia"] = $comprobante["Sucursal"]["Pais"]["d_pais"];
           $comprobante["Persona"]["tel"] = $comprobante["Sucursal"]["tel"]; 
           
        }
        
        return $comprobante; 
        
        
    }
    
    
    public function getSumaDescuentosIndividuales($array_items,$id=0){
        
        $descuento = 0;
        foreach($array_items as $renglon){
        
            $descuento += ($renglon['precio_unitario_bruto']-$renglon['precio_unitario'])* $renglon['cantidad'];
            
        
        
        }
        
        
        return $descuento;
        
    }
    
    
    
public function cerrar_comprobantes_asociados($id,$array_tipo_comprobante_origen){ //$id_comprobante en este caso es el de PI


           /*
           
            $array_tipo_comprobante_origen es igual a $this->id_tipo_comprobante definido en las propiedades del controller
            
            En el caso de Factura de Venta $array_tipo_comprobante_origen tiene las facturas     array(EnumTipoComprobante::FacturaA,EnumTipoComprobante::FacturaB,EnumTipoComprobante::FacturaC)
            En el caso de 
           
           */
        
        
          $items_comprobante = $this->ComprobanteItem->find('all', array(
                                                'conditions' => array('ComprobanteItem.id_comprobante' => $id,'ComprobanteItem.activo'=>1),
                                                'contain' => false
                                                ));
        
        
        
        if($items_comprobante){   /*Si tiene items el comprobane Origen entonces*/
            
            $estan_todos_facturados = 1;
              
              
            foreach($items_comprobante as $item){
                
              
                
                
            $comprobante_item = $item["ComprobanteItem"];
            $procesados = $this->ComprobanteItem->GetItemprocesadosEnImportacion($comprobante_item,$array_tipo_comprobante_origen);     
            $total_procesados = (string) $this->ComprobanteItem->GetCantidadItemProcesados($procesados);
            $total_item = $item["ComprobanteItem"]["cantidad"];
            
            
            
            if($total_procesados < $total_item){
                $estan_todos_facturados = 0;
                
                break;
            }
            
            
        }
        
        
        if($estan_todos_facturados == 1){//cierro el Comprobante Asociado porque estan todos Procesados los items   ya sea por que se facturaron todos PI o se cumplio con la OC a travez de la factura de compra
        
        //actualizo la fecha de cierre fecha_cierre
        // actualizo el estado a cerrado id_estado_comprobante CerradoReimpresion- 116
        
        $hoy = date("Y-m-d");
         $this->updateAll(
                                  array('Comprobante.fecha_cierre' => "'".$hoy."'" ,"Comprobante.id_estado_comprobante"=> EnumEstadoComprobante::CerradoReimpresion,"Comprobante.definitivo"=>1 ),
                                  array('Comprobante.id' => $id) );
        
        
            
            
        }
        
        
      }else{ //No tiene ITEMS
          
          return 0;
          
      }
        
    }
    
    
    public function abrir_comprobantes_asociados($id,$array_tipo_comprobante_origen){ //$id_comprobante en este caso es el de PI
    	
    	
    	/*
    	
    	$array_tipo_comprobante_origen es igual a $this->id_tipo_comprobante definido en las propiedades del controller
    	
    	En el caso de Factura de Venta $array_tipo_comprobante_origen tiene las facturas     array(EnumTipoComprobante::FacturaA,EnumTipoComprobante::FacturaB,EnumTipoComprobante::FacturaC)
    	En el caso de
    	
    	*/
    	
    	
    	$items_comprobante = $this->ComprobanteItem->find('all', array(
    			'conditions' => array('ComprobanteItem.id_comprobante' => $id,'ComprobanteItem.activo'=>1),
    			'contain' => false
    	));
    	
    	
    	
    	if($items_comprobante){   /*Si tiene items el comprobane Origen entonces*/
    		
    		$estan_todos_facturados = 1;
    		
    		
    		foreach($items_comprobante as $item){
    			
    			
    			
    			
    			$comprobante_item = $item["ComprobanteItem"];
    			$procesados = $this->ComprobanteItem->GetItemprocesadosEnImportacion($comprobante_item,$array_tipo_comprobante_origen);
    			$total_procesados = (string) $this->ComprobanteItem->GetCantidadItemProcesados($procesados);
    			$total_item = $item["ComprobanteItem"]["cantidad"];
    			
    			
    			
    			if($total_procesados < $total_item){
    				$estan_todos_facturados = 0;
    				
    				break;
    			}
    			
    			
    		}
    		
    		
    		if($estan_todos_facturados != 1){//cierro el Comprobante Asociado porque estan todos Procesados los items   ya sea por que se facturaron todos PI o se cumplio con la OC a travez de la factura de compra
    			
    			//actualizo la fecha de cierre fecha_cierre
    			// actualizo el estado a cerrado id_estado_comprobante CerradoReimpresion- 116
    			
    	
    			$this->updateAll(
    					array("Comprobante.id_estado_comprobante"=> EnumEstadoComprobante::Abierto,"Comprobante.definitivo"=>0 ),
    					array('Comprobante.id' => $id) );
    			
    			
    			
    			
    		}
    		
    		
    	}else{ //No tiene ITEMS
    		
    		return 0;
    		
    	}
    	
    }
    
    
  public function esImportado($items){ //Comprueba si un comprobante tiene ITEMS importados. X lo que se concidera un COmprobante IMportado. Ejemplo: Factura hecha a partir de un PI
        
        
        $tiene = 0;
        if(count($items) > 0 ){
            foreach($items as $item){
              
                
              if($item["id_comprobante_item_origen"] > 0 ){
                  
                  ++$tiene;
              }
              
              if($tiene > 0)
                return 1;
              else
                return 0;
              
                
                
            }
        }else
            return 0;
        
    }
    
    public function GetNumberComprobante($pto_venta,$nro_comprobante){
        
        
        
        if( $pto_venta != 0 )
            return str_pad($pto_venta, 4, "0", STR_PAD_LEFT).' - '.str_pad($nro_comprobante, 8, "0", STR_PAD_LEFT);
        else
            return str_pad($nro_comprobante, 8, "0", STR_PAD_LEFT);
}  


public function CalcularIva($id_comprobante,$comprobante){//$is_edit viene en 1 si se lo llama desde un metodo de Edit
    
    
    
    $comprobante_impuesto_class = ClassRegistry::init('ComprobanteImpuesto');
    $comprobante_item_class = ClassRegistry::init('ComprobanteItem');
    
    $id_tipo_comprobante = $comprobante->request->data[$comprobante->model]['id_tipo_comprobante'];
    
    
     // DONE: Consultar tipo comprobante campo nuevo requiere_iva
     
     App::import('Model', EnumModel::TipoComprobante);
     App::import('Model', EnumModel::Iva);
     
     $tipo_comprobante = New TipoComprobante();
     $iva_obj = New Iva();
     
     $tiene_iva =1;
     if(isset($comprobante->request->data[$comprobante->model]["tiene_iva"]))//flag que envia el front para calcular o no el IVA. En el caso de compras en el ADD me lo envia, pero el usuario puede destildarlo en el edit.
        $tiene_iva = $comprobante->request->data[$comprobante->model]["tiene_iva"];
    
    if(isset($comprobante->requiere_impuestos) && $comprobante->requiere_impuestos == 1 && $tiene_iva == 1 && $tipo_comprobante->tiene_iva($id_tipo_comprobante
    )  /*&&  $tipo_comprobante->getSistema($id_tipo_comprobante)== EnumSistema::VENTAS  */ ){ //si maneja impuestos propiedad de clase, $tiene_iva es un flag enviado
    
        //$this->loadModel("ComprobanteImpuesto");
    
    
            $valores_iva = array();
            
            //$this->loadModel("ComprobanteItem");
            $data = $comprobante_item_class->find('all', array(
            'joins' => array(
            array(
                'table' => 'producto',
                'alias' => 'Producto2',
                'type' => 'LEFT',
                'conditions' => array(
                    'ComprobanteItem.id_producto = Producto2.id'
                )
            ),
            array(
                'table' => 'iva',
                'alias' => 'iva2',
                'type' => 'LEFT',
                'conditions' => array(
                    'iva2.id = ComprobanteItem.id_iva'
                )
            )
        ),
            'conditions' => array(
                'ComprobanteItem.id_comprobante' => $id_comprobante,
                'ComprobanteItem.activo' => 1,
                'ComprobanteItem.precio_unitario >' => 0
                
            ),
            'fields' => array(
            
            
            
            
            '(

                     SUM(ComprobanteItem.precio_unitario*ComprobanteItem.`cantidad`) - 
                     
                     (SUM(ComprobanteItem.precio_unitario*ComprobanteItem.`cantidad`)*Comprobante.descuento)/100
                     
                     
                     ) as total_sum' , '((

                     SUM(ComprobanteItem.precio_unitario*ComprobanteItem.`cantidad`)  -
                     
                     (SUM(ComprobanteItem.precio_unitario*ComprobanteItem.`cantidad`)*Comprobante.descuento)/100
                     
                     
                     )*iva2.`d_iva`)/100  AS monto','ComprobanteItem.id_iva','iva2.d_iva','iva2.tipo_alicuota_afip','iva2.d_iva_detalle'),
            'group' => array('ComprobanteItem.id_iva')
        ));
                
        
        $comprobantes_impuestos = array();
        $total = 0;
        
        
        
        foreach($data as $valor){
         
         
         $comprobante_impuesto = array();
         $comprobante_impuesto["id_comprobante"] = $id_comprobante;  
         $id_iva = EnumImpuesto::IVANORMAL; 
         $id_sistema_tipo_comprobante = $tipo_comprobante->getSistema($id_tipo_comprobante);
         
         if( $id_sistema_tipo_comprobante == EnumSistema::VENTAS)
            $id_iva = EnumImpuesto::IVANORMAL;
            
         if( $id_sistema_tipo_comprobante == EnumSistema::COMPRAS)
            $id_iva = EnumImpuesto::IVACOMPRAS; 
         
         if($id_sistema_tipo_comprobante == EnumSistema::CAJA)
            	$id_iva = EnumImpuesto::IVACOMPRAS; 
            
         $comprobante_impuesto["id_impuesto"] = $id_iva;   
         $comprobante_impuesto["base_imponible"] = (string) round(trim($valor[0]["total_sum"]),2);  
         $comprobante_impuesto["tipo_alicuota_afip"] = (string) trim($valor["iva2"]["tipo_alicuota_afip"]);  
         $comprobante_impuesto["importe_impuesto"] = (string) round($valor[0]["monto"],2);  
         
         
         
         $comprobante_impuesto["tasa_impuesto"] = $iva_obj->getTasaIvaPorAlicuotaAfip($valor["iva2"]["tipo_alicuota_afip"]);
         
         
         $comprobante_impuesto["d_iva_detalle"] = $valor["iva2"]["d_iva_detalle"];
         
         $total = $total + $comprobante_impuesto["importe_impuesto"]; 
            
         
         /*$iva_item["iva"] = $valor["iva"]["d_iva"];   
         $iva_item["monto_iva"] = round($valor[0]["monto"],2);
         $iva_item["tipo_alicuota_afip"] = trim($valor["iva"]["tipo_alicuota_afip"]);
         $iva_item["base_imponible"] = round(trim($valor[0]["total_sum"]),2);//sumatoria de los items con un mismo tipo de iva sin ese iva. BASE IMPONIBLE
         
         */
         array_push($comprobantes_impuestos,$comprobante_impuesto);
            
        }
        
        
        if(count($comprobantes_impuestos)>0){
            //borro impuestos
            $comprobante_impuesto_class->deleteAll(array("ComprobanteImpuesto.id_comprobante"=>$id_comprobante,"ComprobanteImpuesto.id_impuesto"=>array(EnumImpuesto::IVANORMAL,EnumImpuesto::IVACOMPRAS) ),false);     
            $comprobante_impuesto_class->saveAll($comprobantes_impuestos);     
        }    
            
       return round($total,2);  
    }else{
    	
    	$id_sistema_tipo_comprobante =  $tipo_comprobante->getSistema($id_tipo_comprobante);
    	
    	if($tiene_iva == 0 && $tipo_comprobante->tiene_iva($id_tipo_comprobante)==1  &&  ($id_sistema_tipo_comprobante == EnumSistema::COMPRAS || $id_sistema_tipo_comprobante== EnumSistema::CAJA) ){
    	
    		//en el caso de COMPRAS O CAJA si no envia el flag de tiene_iva y el comprobante acepta iva, se asume que lo envio al iva. Entonces se busca en la BD ya que previamente lo guardo la clase
    		//comprobante y luego se envia el total
    	
    	
    		$comprobantes_impuestos = array();
    		
    		$total = 0;
    		
    		$data = $comprobante_impuesto_class->find('all', array(
    				
    				'conditions' => array(
    						'ComprobanteImpuesto.id_comprobante' => $id_comprobante,
    						'ComprobanteImpuesto.id_impuesto' => array(EnumImpuesto::IVACOMPRAS)
    				
    				),
    				'fields' => array(
    						
    						
    						
    						
    						'
    						
                     IFNULL(SUM(ComprobanteImpuesto.importe_impuesto),0) as total_sum')
    		));
    		
    		
    		return round(trim($data[0][0]["total_sum"]),2); 
    	}else 
    	return 0;
    	
    }
     
    
    
    
    
    
    
}


/* ESTA FUNCION determina el tipo de comprobante del cual fue importado los items*/
public function origenComprobante($items){
    
    
       $item_comprobante = $this->ComprobanteItem->find('first', array(
                                                'conditions' => array('ComprobanteItem.id' => $items[0]["id_comprobante_item_origen"],'ComprobanteItem.activo'=>1),
                                                'contain' =>array('Comprobante')
                                                ));
                                                
       return $item_comprobante["Comprobante"]["id_tipo_comprobante"];
    
    
    
}




function getComprobantesImpagos($id_persona,$considerar_notas=0,$id_comprobante='',$llamada_interna=0,
	$id_sistema = EnumSistema::VENTAS,$id_cartera_rendicion='',$id_moneda,$saldo_desde=0,$saldo_hasta=0,
	$fecha_entrega_desde=0,$fecha_entrega_hasta=0,$nros_comprobante=0,&$data=0,$fecha_vencimiento_desde="",
	$fecha_vencimiento_hasta="",$array_tipo_comprobante=array()){
                              
            /*  
            * $considerar_notas  ESTE PARAMETRO DEFINE SI SE RESTAN LAS NC O SE SUMAN LAS ND AUTOMATICAMENTE
            * 
            * $array_tipo_comprobante se pasa en frmComprobante y no es obligatorio, no lo uso en la query sino el foreach para filtrar
            */
            
            $id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda);
            $campo_valor = $this->getFieldByTipoMoneda($id_tipo_moneda);//obtengo el campo en la cual expresar el reporte
            
            
            $nros_comprobante_array = explode(',', $nros_comprobante);
            
            
            
            if($id_sistema == EnumSistema::VENTAS)
            	$id_estado_comprobante_filtrar = EnumEstadoComprobante::CerradoConCae;
            else 
            	$id_estado_comprobante_filtrar = EnumEstadoComprobante::CerradoReimpresion;
            
            	
            	
            
            $condition = '';
            
            if(count($nros_comprobante_array)>0 && $nros_comprobante!="" && $nros_comprobante!=0)
            	$condition = " AND comprobante.nro_comprobante IN (".$nros_comprobante.")";
            	
            		
            $array_recibos = array();
            $array_notas_credito = array();
            $array_notas_debito = array();
            $array_facturas = array();
            $saldo_inicial = array();
            $credito_interno = array();
            $debito_interno = array();
            $anticipo_compra = array();
            
            
            $this->getArrayComprobantes($id_sistema,$array_recibos,$array_notas_credito,$array_notas_debito,$array_facturas,$saldo_inicial,$credito_interno,$debito_interno,$anticipo_compra);
            
            $array_irm = implode(",",array(EnumTipoComprobante::InformeRecepcionMateriales));
            $array_orden_compra = implode(",",array(EnumTipoComprobante::OrdenCompra));
            

          if($id_comprobante!='')
                $condition = " AND comprobante.id IN(".$id_comprobante.") ";
                
          if($id_cartera_rendicion >0)//esto es porque el parametro es obligatorio y puede no estar cargado desde el front ver linea 175 frmComprobante.cs
                $condition = " AND comprobante.id_cartera_rendicion=".$id_cartera_rendicion;    
             
          
          $condition_persona = '';//no siempre envio la persona para saber impagos, por ejemplo en orden pago de gasto solo envio cartera
          
          
          if($id_persona !='' && $id_persona >0)
          	$condition_persona = " AND comprobante.id_persona=".$id_persona;
          
          if(!$id_cartera_rendicion>0)
           			//$condition.= " AND comprobante.id_persona NOT IN (".EnumPersonaBase::ClienteNoRegistrado.",".EnumPersonaBase::ProveedorNoRegistrado.") ";
          	
           
           
           if($fecha_vencimiento_desde !="")
           		$condition.=" AND comprobante.fecha_vencimiento >='".$fecha_vencimiento_desde."'"; 
           
           
           if($fecha_vencimiento_hasta !="")
           			$condition.=" AND comprobante.fecha_vencimiento <='".$fecha_vencimiento_hasta."'"; 
           	
             $saldo = 0; 
             
             
             
             
             $query_saldo = "(round(comprobante.total_comprobante/comprobante.".$campo_valor.",4)) - 
                              /*Le RESTO LOS RECIBOS O LAS ORDENES DE PAGO*/
                              IFNULL(
                                (SELECT 
                                  SUM(precio_unitario/c.".$campo_valor.")
                                FROM
                                  comprobante_item 
                                  JOIN comprobante c 
                                    ON c.id = comprobante_item.`id_comprobante` 
                                WHERE id_comprobante_origen = comprobante.id 
                                  AND c.id_tipo_comprobante IN (".$array_recibos.") 
                                  AND c.id_estado_comprobante  IN (1,115,116) 
                                 ),
                                0
                              )";  
                              

             $query_saldo_hasta = "";                 
             if($saldo_hasta !=0){
                 
                 $query_saldo_hasta = " AND ".$query_saldo ."<".$saldo_hasta;
             }
                              
             
            
            
            $query = "SELECT 
                              comprobante.id,
                              comprobante.nro_comprobante,
                              (comprobante.total_comprobante/comprobante.".$campo_valor.")*tipo_comprobante.signo_comercial as total_comprobante,
                              comprobante.fecha_vencimiento,
                              comprobante.fecha_generacion,
                              punto_venta.numero,
                              comprobante.d_punto_venta,
                              tipo_comprobante.letra,
							  comprobante.valor_moneda/comprobante.".$campo_valor." as valor_moneda,
							  comprobante.valor_moneda2/comprobante.".$campo_valor." as valor_moneda2,
							  comprobante.valor_moneda3/comprobante.".$campo_valor." as valor_moneda3,
							  comprobante.id_moneda,
							  comprobante.total_comprobante as total_comprobante_origen,
							  comprobante.enlazar_con_moneda,
							  comprobante.id_moneda_enlazada,
							  persona.razon_social,
							  comprobante.id_tipo_comprobante,
                       
                              tipo_comprobante.codigo_tipo_comprobante,
                              tipo_comprobante.signo_comercial,
                              ".$query_saldo." AS saldo,
                              IFNULL(
                                    (SELECT 
                                      SUM(comp_item_irm.cantidad)
                                      FROM
                                      comprobante_item 
                                      JOIN comprobante_item c_item2 
                                   
                                        ON c_item2.id = comprobante_item.id_comprobante_item_origen 
                                      JOIN comprobante c 

                                        ON c_item2.id_comprobante = c.id 
                                        
                                       JOIN comprobante_item comp_item_irm ON comp_item_irm.id_comprobante_item_origen = c_item2.id/*este es el id del item de la OC*/
                                       JOIN comprobante comp_irm ON comp_irm.id = comp_item_irm.id_comprobante 

                                    WHERE comprobante_item.id_comprobante = comprobante.id 
                                   
                                      AND c.id_tipo_comprobante IN (".$array_orden_compra.") 
                                      AND c.id_estado_comprobante IN (1, 115, 116)
                                      AND comp_irm.id_tipo_comprobante IN (".$array_irm.")
                                      AND comp_irm.id_estado_comprobante IN (1, 115, 116)
                                      
                                      )
                                      ,
                                    0
                                  ) AS cantidad_en_irm
                            FROM
                              comprobante 
                              LEFT JOIN punto_venta 
                                ON comprobante.id_punto_venta = punto_venta.id 
                              LEFT JOIN tipo_comprobante 
                                ON tipo_comprobante.id = comprobante.id_tipo_comprobante 
  							 LEFT JOIN persona 
								ON persona.id = comprobante.id_persona
						
                            WHERE 
                              comprobante.definitivo = 1 
                              AND id_estado_comprobante NOT IN (9) 
                              AND ( id_tipo_comprobante IN (
									".$array_facturas.",".$array_notas_credito.",".$array_notas_debito."
                            	  ) AND id_estado_comprobante IN (".$id_estado_comprobante_filtrar.")  
								  OR 
								 ( id_tipo_comprobante IN (".$credito_interno.",".$debito_interno.",".$saldo_inicial.",".$anticipo_compra.") AND id_estado_comprobante IN (116) ) 
								  )
							  AND ".$query_saldo." > ".$saldo_desde." ".$query_saldo_hasta." ".$condition." ".$condition_persona."

							ORDER BY persona.razon_social ASC
            ";
           //var_dump($id_sistema);
            
           //$this->log($query);
            $factura = $this->query($query);
            
            
            App::uses('CakeSession', 'Model/Datasource');
            $datoEmpresa = CakeSession::read('Empresa');
            
            $id_moneda_base  = $datoEmpresa["DatoEmpresa"]["id_moneda"];
            $id_moneda_corriente  = $datoEmpresa["DatoEmpresa"]["id_moneda2"];
            $id_moneda_secundaria  = $datoEmpresa["DatoEmpresa"]["id_moneda3"];
            
            if(count($array_tipo_comprobante)>0)
            	$considero_filtro_tipo_comprobante = 1;
            else
            	$considero_filtro_tipo_comprobante = 0;
			
			foreach($factura as $key=>&$item){
				
				
				
				
				
				
                $saldo = round($item[0]["saldo"]*$factura[$key]['tipo_comprobante']['signo_comercial'],2);
                $factura[$key]["Comprobante"] = $item["comprobante"];
                $factura[$key]["Comprobante"]["cantidad_irm"] = $item[0]["cantidad_en_irm"];
                
                $factura[$key]["Comprobante"]["saldo"] = (string) $saldo;
                $factura[$key]["Comprobante"]["total_comprobante"] = (string) $item[0]["total_comprobante"];
				$factura[$key]["Comprobante"]["total_comprobante_origen"] = (string)  $factura[$key]['comprobante']['total_comprobante_origen'];
				//$factura[$key]["Comprobante"]["total_comprobante_origen"] = (string)  $factura[$key]['comprobante']['total_comprobante_origen'];
                $factura[$key]["Comprobante"]["total_a_imputar"] = (string) $saldo;//TODO: hacer que grilla sea editable
                $factura[$key]["Comprobante"]["signo_comercial"] = (string) $factura[$key]['tipo_comprobante']['signo_comercial'];
                
         
                
                
                if($factura[$key]["Comprobante"]["enlazar_con_moneda"] == 1){
                	
                	$id_moneda_enlazada = $factura[$key]["Comprobante"]["id_moneda_enlazada"]; 
                	
              
                
                	if($factura[$key]["Comprobante"]["id_moneda"] == $id_moneda_corriente){
	                	switch ( $id_moneda_enlazada ){
		                	
		                	
		                		
		                		
		                	case $id_moneda_base:
		                		$factura[$key]["Comprobante"]["valor_moneda"] =  round($item[0]['valor_moneda'],4);
		                	break;
		                	case $id_moneda_corriente:
		                		$factura[$key]["Comprobante"]["valor_moneda"] =  round($item[0]['valor_moneda2'],4);
		                	break;
		                	case $id_moneda_secundaria:
		                		$factura[$key]["Comprobante"]["valor_moneda"] =  round($item[0]['valor_moneda3'],4);
		                	break;
		                		
		                		
		                  	
		                }
                
                }else{
                	
                	switch ( $id_moneda_enlazada ){
                		
                		
                		
                		
                	
                		case $id_moneda_corriente:
                			$factura[$key]["Comprobante"]["valor_moneda"] =  round($item[0]['valor_moneda'],4);
                			break;
                		case $id_moneda_secundaria:
                			$factura[$key]["Comprobante"]["valor_moneda"] =  round($item[0]['valor_moneda3'],4);
                			break;
                			
                			
                			
                	}
                	
                	
                	
                }
                
                }else{
                
                $factura[$key]["Comprobante"]["valor_moneda"]  =  round($item[0]['valor_moneda'],4);
                
               
                $factura[$key]["Comprobante"]["valor_moneda2"] =  round($item[0]['valor_moneda2'],4);
                
                $factura[$key]["Comprobante"]["valor_moneda3"] =  round($item[0]['valor_moneda3'],4);
                }
                
                $factura[$key]["Comprobante"]["id_moneda"] = (string) $id_moneda;//se fuerza el id_moneda del comprobante al valor que se convirtio
                
                $factura[$key]["Comprobante"]["razon_social"] =  $factura[$key]['persona']['razon_social'];
                
                
                
                $factura[$key]["Comprobante"]["simbolo_signo_comercial"] =  "+";
                
                if($factura[$key]["Comprobante"]["signo_comercial"] ==  -1)
                	$factura[$key]["Comprobante"]["simbolo_signo_comercial"] = "-";
                
                
                
                
                
                if($factura[$key]['punto_venta']['numero']>0)
                    $factura[$key]["Comprobante"]["pto_nro_comprobante"] = (string) $this->GetNumberComprobante($factura[$key]['punto_venta']['numero'],$factura[$key]['Comprobante']['nro_comprobante']);
                else
                   $factura[$key]["Comprobante"]["pto_nro_comprobante"] = (string) $this->GetNumberComprobante($factura[$key]['comprobante']['d_punto_venta'],$factura[$key]['Comprobante']['nro_comprobante']); 
                
                if(isset($factura[$key]['tipo_comprobante']))
                    $factura[$key]["Comprobante"]["d_tipo_comprobante"] = $factura[$key]['tipo_comprobante']['codigo_tipo_comprobante'].$factura[$key]['tipo_comprobante']['letra'];
                else
                    $factura[$key]["Comprobante"]["d_tipo_comprobante"] = $factura[$key]['tipo_comprobante']['codigo_tipo_comprobante'];

                $factura[$key]["Comprobante"]["fecha_vencimiento"] = $this->formatDate($factura[$key]["Comprobante"]["fecha_vencimiento"]);
               
                $factura[$key]["Comprobante"]["fecha_generacion"] = $this->formatDate($factura[$key]["Comprobante"]["fecha_generacion"]); 
                
              
       
                
                    
                unset($item["comprobante"]);
                unset($item["punto_venta"]);
                unset($item["tipo_comprobante"]);
                unset($item[0]);
                //si mandan el filtro de tipo_comprobante y el tipo_comprobante NO esta en el array lo borro
                if($considero_filtro_tipo_comprobante == 1 && !in_array($factura[$key]["Comprobante"]["id_tipo_comprobante"],$array_tipo_comprobante))
                	unset($factura[$key]);
            }
            
            
            
            $array_comprobantes = array();
            
            
            foreach($factura as $key=>&$comprobante){
            	array_push($array_comprobantes,$comprobante);
            }
            
            
            $factura = $array_comprobantes;

            $output = array(
                "status" =>EnumError::SUCCESS,
                "message" => "Comprobante",
                "content" => $factura
            );
            
           if($llamada_interna == 0){ 
            echo json_encode($output);

            die();     
          }else{
          	$data = $factura;
              return $saldo; /*Esto se hace porque se llama como metodo desde adentro en el controller de Recibos*/
          }
        }
        
  function getComprobantesImpagosAgrupadoPorPersona($id_persona=0,$considerar_notas=0,$id_comprobante='',$llamada_interna=0,$id_sistema = EnumSistema::VENTAS,$fecha_vencimiento_desde=0,$fecha_vencimiento_hasta=0,$id_cartera_rendicion='',$id_moneda=EnumMoneda::Peso){
      
            $array_recibos = array();
            $array_notas_credito = array();
            $array_notas_debito = array();
            $array_facturas = array();
            $saldo_inicial = array();
            $credito_interno = array();
            $anticipo_compra = array();
            
             $id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda);
            
            $campo_valor = $this->getFieldByTipoMoneda($id_tipo_moneda);//obtengo el campo en la cual expresar el reporte
            $condicion_persona ='';
            if($id_persona !=0){
                $condicion_persona = ' AND comprobante.id_persona = '.$id_persona.'';
                
            }
            
            
            $condicion = '';
            if($fecha_vencimiento_desde !=0) 
                $condicion= " AND comprobante.fecha_vencimiento >='".$fecha_vencimiento_desde."'";
            
            if($fecha_vencimiento_hasta !=0) 
                $condicion.= " AND comprobante.fecha_vencimiento <='".$fecha_vencimiento_hasta."'";
            
                
                $condicion.= "AND comprobante.id_persona NOT IN (".EnumPersonaBase::ClienteNoRegistrado.",".EnumPersonaBase::ProveedorNoRegistrado.")";
                
                
                
                
                
           	if($id_sistema == EnumSistema::VENTAS)
               $id_estado_comprobante_filtrar = EnumEstadoComprobante::CerradoConCae;
            else
               $id_estado_comprobante_filtrar = EnumEstadoComprobante::CerradoReimpresion;
                		
                		
                
                
                
            
      $this->getArrayComprobantes($id_sistema,$array_recibos,$array_notas_credito,$array_notas_debito,$array_facturas,$saldo_inicial,$credito_interno,$debito_interno,$anticipo_compra);
      
      $query = "SELECT 
                           
                            persona.id,
                            persona.razon_social,
                            persona.tel,
                            persona.codigo,
                
                              SUM( (comprobante.total_comprobante/comprobante.".$campo_valor.")*tipo_comprobante.signo_comercial) AS total_comprobante,
                              
                              
                            
                        
                             
                          
                              SUM(
                              
                              ( 
                              ROUND(comprobante.total_comprobante/comprobante.".$campo_valor.",4) - 
                              /*Le RESTO LOS RECIBOS O LAS ORDENES DE PAGO*/
                              IFNULL(
                                (SELECT 
                                  SUM(precio_unitario/c.".$campo_valor.") 
                                FROM
                                  comprobante_item 
                                  JOIN comprobante c 
                                    ON c.id = comprobante_item.`id_comprobante` 
                                WHERE id_comprobante_origen = comprobante.id 
                                  AND c.id_tipo_comprobante IN (".$array_recibos.") 
                                  AND c.id_estado_comprobante  IN (1,115,116) 
                             
                                 ),
                                0
                              )
                              
                              )*tipo_comprobante.signo_comercial
                              
                              
                              ) AS saldo 
                              
                            FROM
                              comprobante 
                              LEFT JOIN punto_venta 
                                ON comprobante.id_punto_venta = punto_venta.id 
                              LEFT JOIN tipo_comprobante 
                                ON tipo_comprobante.id = comprobante.id_tipo_comprobante 
                          
                              LEFT JOIN persona
                                ON persona.id = comprobante.id_persona
                            WHERE 




 								comprobante.definitivo = 1 
                              AND id_estado_comprobante NOT IN (9) 
                              AND ( id_tipo_comprobante IN (
									".$array_facturas.",".$array_notas_credito.",".$array_notas_debito."
                            	  ) AND id_estado_comprobante IN (".$id_estado_comprobante_filtrar.")  
								  OR 
								 ( id_tipo_comprobante IN (".$credito_interno.",".$debito_interno.",".$saldo_inicial.",".$anticipo_compra.") AND id_estado_comprobante IN (116) ) 
								  )




                            
                            ".$condicion_persona."
                            ".$condicion."
                          
                              group by comprobante.id_persona 
                              
                              HAVING 
                              
                              SUM(
                              ( 
                              ROUND(comprobante.total_comprobante/comprobante.".$campo_valor.",4) - 
                              /*Le RESTO LOS RECIBOS O LAS ORDENES DE PAGO*/
                              IFNULL(
                                (SELECT 
                                  SUM(precio_unitario/c.".$campo_valor.") 
                                FROM
                                  comprobante_item 
                                  JOIN comprobante c 
                                    ON c.id = comprobante_item.`id_comprobante` 
                                WHERE id_comprobante_origen = comprobante.id 
                                  AND c.id_tipo_comprobante IN (".$array_recibos.") 
                                  AND c.id_estado_comprobante  IN (1,115,116) 
                             
                                 ),
                                0
                              )
                              
                              )*tipo_comprobante.signo_comercial
                              
                              
                              ) <> 0 
                              
                             
                              
                              ";
                              
                        
           //var_dump($id_sistema);
           
      		//$this->log($query);
            $factura = $this->query($query);
            
         
            
            foreach($factura as $key=>&$item){
                
            
     
                $factura[$key]["Comprobante"]["saldo"] = (string) round($item[0]["saldo"],2);
                $factura[$key]["Comprobante"]["total_comprobante"] = (string) round($item[0]["total_comprobante"],2);
                $factura[$key]["Comprobante"]["razon_social"] = (string)  $factura[$key]['persona']['razon_social'];
                $factura[$key]["Comprobante"]["id_persona"] = (string)  $factura[$key]['persona']['id'];
                $factura[$key]["Comprobante"]["tel"] = (string)  $factura[$key]['persona']['tel'];
                $factura[$key]["Comprobante"]["codigo_persona"] = (string)  $factura[$key]['persona']['codigo'];
            }
            
             $output = array(
                "status" =>EnumError::SUCCESS,
                "message" => "Comprobante",
                "content" => $factura
            );
            
           if($llamada_interna == 0){ 
            echo json_encode($output);

            die(); 
           }else{
           	
           	return $factura;
           	
           }
  
  }      
        

  public function ExpresarEnTipoMoneda($id_tipo_moneda,$valor,$comprobante,$id_moneda_base){
      
      if($id_tipo_moneda == EnumTipoMoneda::Base){//Si me pide en MONEDA BASE
       if($comprobante["id_moneda"] == $id_moneda_base ){
           return round($valor,2);
          
       }else{
           
            return round($valor/$comprobante["valor_moneda"],2);
       }
       }else{
           
          if($comprobante["id_moneda"] == $id_moneda_base ){
              
           return round($valor*$comprobante["valor_moneda2"],2);
          
       }else{
           return round($valor,2);
            
       }  
           
           
           
           
       }
      
      
  }
  
  
  
  public function ExpresarEnTipoMonedaMejorado($valor_base,$key_valor_moneda,$comprobante,$es_valor_con_calculo = 0,$base_imponible=0,$tasa=0,$redondeo_decimal=0){//si el valor es un valor por conversion en base a una tasa entonces debo recalcular el valor
      
  	
  	if($redondeo_decimal == 0)
  		$redondeo_decimal = 2;
  	
  	
  		if(isset($comprobante[$key_valor_moneda]))	{
			  	if($es_valor_con_calculo == 0)
			  		return round($valor_base/$comprobante[$key_valor_moneda],$redondeo_decimal);
			  	else{//cuando es un valor con base imponible y tasa al pasar de Dolares a pesos hay que realizar el calculo nuevamente ya que al tener 2 decimales en la BD y luego convertirlo ese valor es incorrecto
			  		
			  		$valor = ($base_imponible*$tasa)/100;
			  		return round($valor/$comprobante[$key_valor_moneda],$redondeo_decimal);
			  	}
			      
  	
  		}else{
  			
  			$a;
  		}
  	
      
  }
  
  public function getDefinitivo($data=''){
    
      
      
      
    /*Por default un comprobante es definitivo*/
    $definitivo = 1;
    
    
    if(!isset($data["Comprobante"]["definitivo"]))
        $definitivo = 1;
    elseif(isset($data["Comprobante"]["definitivo"]))
        $definitivo = $data["Comprobante"]["definitivo"];
        
    
   return $definitivo; 
}

public function getDefinitivoGrabado($id){
    
    $comprobante = $this->find('first', array(
                                                'conditions' => array('Comprobante.id' => $id),
                                                'contain' =>false
                                                )); 
        
    
   return $comprobante["Comprobante"]["definitivo"]; 
}


public function esRecibo($comprobante){
    
    
    if(in_array($comprobante["Comprobante"]["id_tipo_comprobante"],$this->getArrayRecibos()) )
        return 1;
    else
        return 0;
    
    
    
}

public function getTipoComprobante($id_comprobante){
    
    $comprobante = $this->find('first', array(
                                                'conditions' => array('Comprobante.id' => $id_comprobante),
                                                'contain' =>false
                                                )); 
    return $comprobante["Comprobante"]["id_tipo_comprobante"]; 
    
}


public function getArrayRecibos(){
    
    return array(EnumTipoComprobante::ReciboA,EnumTipoComprobante::ReciboB,EnumTipoComprobante::ReciboC,EnumTipoComprobante::ReciboManual,EnumTipoComprobante::ReciboAutomatico);
}

public function getArrayOrdenesPago(){
    
    return array(EnumTipoComprobante::OrdenPagoAutomatica,EnumTipoComprobante::OrdenPagoManual);
}

public function getArrayOrdenesPagoRendicionGasto(){
    
    return array(EnumTipoComprobante::OrdenPagoRenGasto);
}



public function esOrdenPago($comprobante){
    
    
    if(in_array($comprobante["Comprobante"]["id_tipo_comprobante"],array(EnumTipoComprobante::OrdenPagoManual,EnumTipoComprobante::OrdenPagoAutomatica)) )
        return 1;
    else
        return 0;
    
    
    
}


public function esOrdenPagoGasto($comprobante){
	
	
	if(in_array($comprobante["Comprobante"]["id_tipo_comprobante"],array(EnumTipoComprobante::OrdenPagoRenGasto)) )
		return 1;
		else
			return 0;
			
			
			
}

public function getEstado($id){
    
    $comprobante = $this->find('first', array(
                                                'conditions' => array('Comprobante.id' => $id),
                                                'contain' =>false
                                                )); 
        
    
   if($comprobante)
    return $comprobante["Comprobante"]["id_estado_comprobante"]; 
   else
    return 0;
}

public function getPersona($id){
    
    $comprobante = $this->find('first', array(
                                                'conditions' => array('Comprobante.id' => $id),
                                                'contain' =>false
                                                )); 
        
    
   if($comprobante)
    return $comprobante["Comprobante"]["id_persona"]; 
   else
    return 0;
}



public function getEstadoGrabado($id){
    
    $comprobante = $this->find('first', array(
                                                'conditions' => array('Comprobante.id' => $id),
                                                'contain' =>false
                                                )); 
        
    
   if($comprobante)
    return $comprobante["Comprobante"]["id_estado_comprobante"]; 
   else
    return 0;
}


public function  InsertaComprobante($cabecera,$items){ /*Cabecera es un array con ASIENTO y $detalle es un array con el tipo AsientoCuentaContable*/
    
    
    
          
  $ds = $this->getdatasource();
  try{
      
      
    $ds->begin();
    
    if($this->saveAll($cabecera)){
        $id_comprobante = $this->id;
        
        $comprobante_item = new ComprobanteItem();      //creo un objeto para poder insertar los hijos ya que necesito el ID del padre
        
        if(count($items)>0){
	        foreach($items as &$item){
	            
	            $item["id_comprobante"] = $id_comprobante;
	            
	            if(isset($item["ComprobanteItem"]))//esto es si son muchos items entonces viene un array
	            	$item["ComprobanteItem"]["id_comprobante"] = $id_comprobante;
	            	
	        }
	        $comprobante_item->saveAll($items);
	        }
        $ds->commit();
        return $id_comprobante;
         
    }else{
        
        return 0;
    }
   
  
  }catch(Exception $e){
    $ds->rollback();
    return 0;
   } 
    
    
}


  
  
  public function formatearFechas(&$comprobante){
      
      
        if($comprobante["Comprobante"]["fecha_generacion"]!=null)
            $comprobante["Comprobante"]["fecha_generacion"] = date("d-m-Y", strtotime($comprobante["Comprobante"]["fecha_generacion"]));
        
        
        if($comprobante["Comprobante"]["fecha_vencimiento"]!=null)
            $comprobante["Comprobante"]["fecha_vencimiento"] = date("d-m-Y", strtotime($comprobante["Comprobante"]["fecha_vencimiento"]));
        
        if($comprobante["Comprobante"]["fecha_contable"]!=null)
            $comprobante["Comprobante"]["fecha_contable"] = date("d-m-Y", strtotime($comprobante["Comprobante"]["fecha_contable"]));
        
        
        
        if($comprobante["Comprobante"]["fecha_anulacion"]!=null)
            $comprobante["Comprobante"]["fecha_anulacion"] = date("d-m-Y", strtotime($comprobante["Comprobante"]["fecha_anulacion"]));
        
        if($comprobante["Comprobante"]["fecha_entrega"]!=null)
            $comprobante["Comprobante"]["fecha_entrega"] = date("d-m-Y", strtotime($comprobante["Comprobante"]["fecha_entrega"]));
        
        if($comprobante["Comprobante"]["fecha_cierre"]!=null)
            $comprobante["Comprobante"]["fecha_cierre"] = date("d-m-Y", strtotime($comprobante["Comprobante"]["fecha_cierre"]));
            
        if($comprobante["Comprobante"]["fecha_contable"]!=null)
            $comprobante["Comprobante"]["fecha_contable"] = date("d-m-Y", strtotime($comprobante["Comprobante"]["fecha_contable"]));    
              
       if($comprobante["Comprobante"]["fecha_aprobacion"]!=null)
            $comprobante["Comprobante"]["fecha_aprobacion"] = date("d-m-Y", strtotime($comprobante["Comprobante"]["fecha_aprobacion"]));    
       
            
        if(isset($comprobante["ComprobanteItemLote"]["fecha_lote"]) && $comprobante["ComprobanteItemLote"]["fecha_lote"]!=null)
           $comprobante["ComprobanteItemLote"]["fecha_lote"] = date("d-m-Y", strtotime($comprobante["ComprobanteItemLote"]["fecha_lote"]));
            	
       
     
              
  }
  
  public function anularRetencionPropia($id_orden_pago){
      
      /*Anula las retenciones generadas a traves de una Orden de Pago*/
      
      $impuesto_comprobante = $this->ComprobanteImpuesto->find('all', array(
                                                'conditions' => array('ComprobanteImpuesto.id_comprobante' => $id_orden_pago),
                                                'contain' => array('Impuesto')
                                                ));
                                                
      if($impuesto_comprobante){
          
          foreach($impuesto_comprobante as $impuesto){
              
              if($impuesto["Impuesto"]["id_tipo_impuesto"] == EnumTipoImpuesto::RETENCIONES /*&& $impuesto["Impuesto"]["id_sub_tipo_impuesto"] == EnumSubTipoImpuesto::RETENCION_GANANCIA*/ ){//debo anular la retencion asociada
               
               $id_retencion_asociada = $impuesto["ComprobanteImpuesto"]["id_comprobante_referencia"];
               
               $this->updateAll(
                                          array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado ),
                                          array('Comprobante.id' => $id_retencion_asociada) );
                  
              }
              
              
          }
          
      }
        
      
      
      
      
     
      
  }
  
  
  
  
  function getArrayComprobantes($id_sistema,&$array_recibos,&$array_notas_credito,&$array_notas_debito,&$array_facturas,&$saldo_inicial,&$credito_interno,&$debito_interno,&$anticipo_compra){
      
      
     switch($id_sistema){
                
                case   EnumSistema::VENTAS:
                    
                    $array_recibos =  implode(",",$this->getTiposComprobanteController(EnumController::Recibos));
                    $array_notas_credito = implode(",",array(EnumTipoComprobante::NotaCreditoA,EnumTipoComprobante::NotaCreditoB,EnumTipoComprobante::NotaCreditoC,EnumTipoComprobante::NotaCreditoE,EnumTipoComprobante::NotaCreditoM));
                    $array_notas_debito = implode(",",array(EnumTipoComprobante::NotaDebitoA,EnumTipoComprobante::NotaDebitoB,EnumTipoComprobante::NotaDebitoC,EnumTipoComprobante::NotaDebitoE,EnumTipoComprobante::NotaDebitoM));
                    $array_facturas = implode(",",   $this->getTiposComprobanteController(EnumController::Facturas) );
                    $saldo_inicial =  EnumTipoComprobante::SaldoInicioCliente;
                    $credito_interno =  EnumTipoComprobante::CreditoInternoVenta;
                    $debito_interno =  EnumTipoComprobante::DebitoInternoVenta;
                    $anticipo_compra =  0;
                
                
                break;
                
                case EnumSistema::COMPRAS:
                    
                    $array_recibos =  implode(",",  $this->getTiposComprobanteController(EnumController::OrdenesPago)  );
                    $array_notas_credito = implode(",",array(EnumTipoComprobante::NOTASDECREDITOACOMPRA,EnumTipoComprobante::NOTASDECREDITOBCOMPRA,EnumTipoComprobante::NOTASDECREDITOCCOMPRA,EnumTipoComprobante::NOTASDECREDITOMCOMPRA));
                    $array_notas_debito = implode(",",array(EnumTipoComprobante::NOTASDEDEBITOACOMPRA,EnumTipoComprobante::NOTASDEDEBITOBCOMPRA,EnumTipoComprobante::NOTASDEDEBITOCCOMPRA,EnumTipoComprobante::NOTASDEDEBITOMCOMPRA));
                    $array_facturas = implode(",", $this->getTiposComprobanteController(EnumController::FacturasCompra) );
                    $saldo_inicial =  EnumTipoComprobante::SaldoInicioProveedor;
                    $credito_interno =  EnumTipoComprobante::CreditoInternoCompra;
                    $debito_interno =  EnumTipoComprobante::DebitoInternoCompra;
                    $anticipo_compra =  EnumTipoComprobante::AnticipoCompra;
                break;
                
                case EnumSistema::CAJA:
                
                    $array_recibos =  implode(",",array(EnumTipoComprobante::OrdenPagoRenGasto));
                    $array_notas_credito = 0;
                    $array_notas_debito  =0;
                    $array_facturas = implode(",", $this->getTiposComprobanteController(EnumController::Gastos) );
                    $saldo_inicial = 0;
                    $credito_interno = 0;
                    $debito_interno = 0;
                    $anticipo_compra = 0;
                
            } 
      
      
      
  }
  
  
  
  public function HizoMovimientoStock($id_comprobante,$id_tipo_movimiento){
      
      $movimiento_class = ClassRegistry::init('Movimiento');
      
      
      $movimiento = $movimiento_class->find('first', array(
                                                'conditions' => array('Movimiento.id_comprobante' => $id_comprobante,'Movimiento.id_movimiento_tipo' => $id_tipo_movimiento),
                                                'contain' => false
                                                ));
                                                
      if($movimiento && isset($movimiento["Movimiento"]["id"]) && $movimiento["Movimiento"]["id"]>0)
        return $movimiento["Movimiento"]["id"];
      else
        return 0;
      
      
      
      
      
  }
  
  
  public function TieneIva($id_comprobante){
    
    $comprobante = $this->find('first', array(
                                                'conditions' => array('Comprobante.id' => $id_comprobante),
                                                'contain' =>false
                                                )); 
        
    
   if($comprobante)
    return $comprobante["Comprobante"]["tiene_iva"]; 
   else
    return 0;
}

	
public function getTotalPorImpuestoGeografia($id_comprobante,$id_impuesto_geografia=0,$id_impuesto=0,$id_tipo_impuesto=0,$id_sub_tipo_impuesto=0){
   
    
    
    $conditions = array();
    
    
    
    array_push($conditions, array('ComprobanteImpuesto.id_comprobante' => $id_comprobante));
    
    
    if($id_impuesto !=0)
         array_push($conditions, array('ComprobanteImpuesto.id_impuesto' => $id_impuesto));
      
         
         
    if($id_tipo_impuesto !=0)
         array_push($conditions, array('Impuesto.id_tipo_impuesto' => $id_tipo_impuesto)); 
         
             
             
    if($id_sub_tipo_impuesto !=0)
         array_push($conditions, array('Impuesto.id_sub_tipo_impuesto' => $id_sub_tipo_impuesto));   
         
         
     if($id_impuesto_geografia !=0)
         array_push($conditions, array('Impuesto.id_impuesto_geografia' => $id_impuesto_geografia));     
         
           
    

        $comprobante = $this->ComprobanteImpuesto->find('first', array(
                                                'fields'=>array('SUM(ComprobanteImpuesto.importe_impuesto) as total'),
                                                'conditions' => $conditions,
                                                'contain' =>array('Impuesto')
                                                )); 
        
  
    
   if($comprobante && $comprobante[0]["total"]>0)
    return $comprobante[0]["total"]; 
   else
    return 0;
}



public function getImportePorImpuesto($id_comprobante,$id_sistema,$tasa,$id_impuesto=0){
    
    
    if($id_sistema == EnumSistema::VENTAS)
        $id_impuesto = EnumImpuesto::IVANORMAL;
    elseif($id_sistema == EnumSistema::COMPRAS)
        $id_impuesto = EnumImpuesto::IVACOMPRAS;
    
    $comprobante = $this->ComprobanteImpuesto->find('first', array(
                                                'fields'=>array('SUM(ComprobanteImpuesto.base_imponible) as total'),
                                                'conditions' => array('ComprobanteImpuesto.id_comprobante' => $id_comprobante,'ComprobanteImpuesto.id_impuesto'=>$id_impuesto,'ComprobanteImpuesto.tasa_impuesto'=>$tasa),
                                                'contain' =>array('Impuesto')
                                                )); 
        
    
   if($comprobante && $comprobante["total"][0]>0)
    return $comprobante[0]["total"]; 
   else
    return 0;
}


public function getCodigoOperacionAfip($id_comprobante,$total_no_gravado=0,$total_exento=0){
    
    
    
   $comprobante = $this->find('first', array(
                                                'conditions' => array('Comprobante.id' => $id_comprobante),
                                                'contain' =>array("Persona","TipoComprobante")
                                                ));
                                                
                                                 
   if($comprobante){
       

       
       
       
    App::uses('CakeSession', 'Model/Datasource');
    $datoEmpresa = CakeSession::read('Empresa');
    $id_pais_empresa  = $datoEmpresa["DatoEmpresa"]["id_pais"];

     
      
    if($comprobante["TipoComprobante"]["letra"] == "E"){//FACTURA E   
        
        if($comprobante["Persona"]["id_pais"] == $id_pais_empresa )
            $letra =  "Z";//Z- Exportaciones a la zona franca o imporacion.
        else
        	$letra = "X"; //Exportacion al exterior o importacion
        

    
    }elseif($comprobante["Comprobante"]["subtotal_neto"] == $total_exento)
    		$letra = "E"; //EXENTO
    elseif($comprobante["Comprobante"]["subtotal_neto"] == $total_no_gravado)  /*si solo tiene 1 iva a tasa 0%*/ 
    		$letra = "N"; //No gravado
    else
    		$letra = "0";//NO corresponde
   }
   
   return $letra;
    
    
    
    
}


public function  getCantidadIvas($id_comprobante,$id_sistema,$tasa=0,$condisiderar_tasa=0,$conciderar_importe_mayor_a=0,$importe_impuesto_mayor_a =0,$excluir_tipo_iva_por_tipo_alicuota_afip = 0,$id_tipo_alicuota_afip_excluir = array()){
    
       if($id_sistema == EnumSistema::VENTAS)
        $id_impuesto = EnumImpuesto::IVANORMAL;
       elseif($id_sistema == EnumSistema::COMPRAS)
        $id_impuesto = EnumImpuesto::IVACOMPRAS;
      
      
        
      $conditions = array();
      
      array_push($conditions,array('ComprobanteImpuesto.id_comprobante' => $id_comprobante));
      array_push($conditions,array('ComprobanteImpuesto.id_impuesto' => $id_impuesto));

      
      if($condisiderar_tasa == 1)
       array_push($conditions,array('ComprobanteImpuesto.tasa_impuesto'=>$tasa)); 
      
      
       if($excluir_tipo_iva_por_tipo_alicuota_afip == 1 && count($id_tipo_alicuota_afip_excluir)>0)
       	array_push($conditions,array("NOT"=>array('ComprobanteImpuesto.tipo_alicuota_afip'=>$id_tipo_alicuota_afip_excluir))); 
       
      
      if($conciderar_importe_mayor_a == 1)
        array_push($conditions,array('ComprobanteImpuesto.importe_impuesto >'=>$importe_impuesto_mayor_a));  
        
      $comprobante_impuesto_cant = $this->ComprobanteImpuesto->find('count', array(
                                      
                                                'conditions' => $conditions,
                                                'contain' =>array('Impuesto'),
                                                'group'=>array("ComprobanteImpuesto.tasa_impuesto")
                                                )); 
      
      if($comprobante_impuesto_cant)
      	return $comprobante_impuesto_cant;   
      else
      	return 0;
    
    
}


public function  getCantidadIvasTasaMayorACero($id_comprobante,$id_sistema){
	
	if($id_sistema == EnumSistema::VENTAS)
		$id_impuesto = EnumImpuesto::IVANORMAL;
		elseif($id_sistema == EnumSistema::COMPRAS)
		$id_impuesto = EnumImpuesto::IVACOMPRAS;
		
		
		
		$conditions = array();
		
		array_push($conditions,array('ComprobanteImpuesto.id_comprobante' => $id_comprobante));
		array_push($conditions,array('ComprobanteImpuesto.id_impuesto' => $id_impuesto));
		
		
	
		array_push($conditions,array('ComprobanteImpuesto.tasa_impuesto >'=>0));
		
		$comprobante_impuesto_cant = $this->ComprobanteImpuesto->find('count', array(
							
							'conditions' => $conditions,
							'contain' =>array('Impuesto'),
							'group'=>array("ComprobanteImpuesto.tasa_impuesto")
					));
					
					if($comprobante_impuesto_cant)
						return $comprobante_impuesto_cant;
						else
							return 0;
							
							
}




public function SacarComaDecimalaNumeroYFormatearConCeros($numero,$cantidad_enteros,$cantidad_decimal,$caracter_relleno){
    
        
        $numero_vector = explode(".",$numero);//le saco el decimal
        $importe_entero = str_pad($numero_vector[0],$cantidad_enteros,$caracter_relleno,STR_PAD_LEFT);

        if(	isset($numero_vector[1]) && strlen($numero_vector[1])>$cantidad_enteros)//si tiene mas de un decimal
          $importe_decimal = substr($numero_vector[1],0,$cantidad_decimal);
        else
          $importe_decimal = str_pad($numero_vector[1],$cantidad_decimal,$caracter_relleno,STR_PAD_RIGHT);
          
          
        return $importe_entero.$importe_decimal;   
    
    
}

public function getNoGravado($id_comprobante,$redondeo=1){
    
  
    $comprobante_item = $this->ComprobanteItem->find('first', array(
                                                'fields'=>array('SUM(ComprobanteItem.precio_unitario*ComprobanteItem.cantidad) as total'),
                                                'conditions' => array('ComprobanteItem.id_comprobante' => $id_comprobante,'ComprobanteItem.id_iva'=>array(EnumIva::nogravado,EnumIva::cero)),
                 
    		
    		
    		'contain' =>false
                                                )); 
    
    
    
   
    
			  if($comprobante_item[0]["total"]==0){
			    return "0.00";
			  }else{
			  	
			  	if($redondeo == 1){
			    	return round($comprobante_item[0]["total"],2);
			  	}else{
			  		
			  		return $comprobante_item[0]["total"];
			  		
			  	}
			  }
    
    
    
    
    
}







public function getGravado($id_comprobante,$redondeo=1){
    
    $no_gravado = $this->getNoGravado($id_comprobante,1);
    $exento = $this->getExento($id_comprobante,1);
    
    $comprobante = $this->find('first', array(
                                                'conditions' => array('Comprobante.id' => $id_comprobante),
                                                'contain' =>false
                                                ));
                                                
    $subtotal_neto = $comprobante["Comprobante"]["subtotal_neto"];
    
    if($redondeo == 1)
    	return round(($subtotal_neto - $no_gravado - $exento),2);
    else
    	return ($subtotal_neto - $no_gravado - $exento);
}


public function getExento($id_comprobante,$redondeo = 1){
    
   $comprobante_item = $this->ComprobanteItem->find('first', array(
                                                'fields'=>array('SUM(ComprobanteItem.precio_unitario*ComprobanteItem.cantidad) as total'),
                                                'conditions' => array('ComprobanteItem.id_comprobante' => $id_comprobante,'ComprobanteItem.id_iva'=>array(EnumIva::exento)),
                                                'contain' =>false
                                                )); 
  if($comprobante_item[0]["total"]==0)
    return "0.00";
  else{
  	
  	if($redondeo == 1){
    	return round($comprobante_item[0]["total"],2);
  	}else{
  		
  		return $comprobante_item[0]["total"];
  	}
    
  }
    
}


public function CalcularMonedaBaseSecundaria(&$data,$model,$es_asiento = 0){
    
    /*Siempre el front me envia valor_moneda = (MONEDA DEL COMPROBANTE / COTIZACION MONEDA BASE) 
    valor_moneda2= 
    valor_moneda3= 
    */
    
	
	if(isset($data[$model]["id_moneda"]) && $data[$model]["id_moneda"]>0){
	    App::uses('CakeSession', 'Model/Datasource');
	    App::uses('Moneda', 'Model');
	    $datoEmpresa = CakeSession::read('Empresa');
	    
	    $moneda_obj = New Moneda();
	    
	    $cotizacion_moneda_elegida = $moneda_obj->getCotizacion($data[$model]["id_moneda"]);
	   
	    $cotizacion_moneda_base = $moneda_obj->getCotizacion($datoEmpresa["DatoEmpresa"]["id_moneda"]);
	    
	    $cotizacion_moneda_corriente = $moneda_obj->getCotizacion($datoEmpresa["DatoEmpresa"]["id_moneda2"]);
	    
	    $cotizacion_moneda_corriente_secundaria = $moneda_obj->getCotizacion($datoEmpresa["DatoEmpresa"]["id_moneda3"]);
	    
	    if(!isset($data[$model]["valor_moneda2"]))
	    	$data[$model]["valor_moneda2"] = $cotizacion_moneda_elegida /  $cotizacion_moneda_corriente;
	    
	    if(!isset($data[$model]["valor_moneda3"]))
	    	$data[$model]["valor_moneda3"] =  $cotizacion_moneda_elegida / $cotizacion_moneda_corriente_secundaria;
	    
    	
	}
    	
    
                 
}


function getTiposComprobanteController($enum_controller){
    
    
    switch($enum_controller){
        
        case EnumController::Facturas:
        case EnumController::FacturaItems:
        
            return array(EnumTipoComprobante::FacturaA,EnumTipoComprobante::FacturaB,EnumTipoComprobante::FacturaC,EnumTipoComprobante::FacturaE,EnumTipoComprobante::FacturaM,EnumTipoComprobante::ReciboA,EnumTipoComprobante::ReciboB,EnumTipoComprobante::ReciboC,EnumTipoComprobante::ReciboM,
            EnumTipoComprobante::FacturaCreditoA,EnumTipoComprobante::FacturaCreditoB,EnumTipoComprobante::FacturaCreditoC
            			);
        break;
        
        
        case EnumController::FacturasCredito:
        case EnumController::FacturaCreditoItems:
        	
        	return array(EnumTipoComprobante::FacturaCreditoA,EnumTipoComprobante::FacturaCreditoB,EnumTipoComprobante::FacturaCreditoC);
        	
        	
        	break;
        
        
        
        case EnumController::Notas:
        case EnumController::NotaItems:
        
        	return array(EnumTipoComprobante::NotaCreditoA,EnumTipoComprobante::NotaCreditoB,EnumTipoComprobante::NotaCreditoC,EnumTipoComprobante::NotaCreditoM,EnumTipoComprobante::NotaCreditoE,EnumTipoComprobante::NotaDebitoA,EnumTipoComprobante::NotaDebitoB,EnumTipoComprobante::NotaDebitoC,
            EnumTipoComprobante::NotaDebitoM,EnumTipoComprobante::NotaDebitoE,EnumTipoComprobante::NotaCreditoAFacturaCredito,EnumTipoComprobante::NotaCreditoBFacturaCredito,EnumTipoComprobante::NotaCreditoCFacturaCredito,EnumTipoComprobante::NotaDebitoAFacturaCredito,EnumTipoComprobante::NotaDebitoBFacturaCredito,EnumTipoComprobante::NotaDebitoCFacturaCredito);
        break;
        
        
        case EnumController::FacturasCompra:
        case EnumController::FacturaCompraItems:
        
            return array(EnumTipoComprobante::FACTURASACOMPRA,EnumTipoComprobante::FACTURASBCOMPRA,EnumTipoComprobante::FACTURASCCOMPRA,EnumTipoComprobante::FACTURASMCOMPRA,EnumTipoComprobante::ReciboACompra,EnumTipoComprobante::ReciboBCompra,EnumTipoComprobante::ReciboCCompra,EnumTipoComprobante::ReciboMCompra);
        break;
        
        
        case EnumController::AnticiposCompra:
        case EnumController::AnticipoCompraItems:
        	
        	return array(EnumTipoComprobante::AnticipoCompra);
        	break;
        
        
        case EnumController::NotasCompra:
        case EnumController::NotaCompraItems:
        
            return array(EnumTipoComprobante::NOTASDECREDITOACOMPRA,EnumTipoComprobante::NOTASDECREDITOBCOMPRA,EnumTipoComprobante::NOTASDECREDITOCCOMPRA,EnumTipoComprobante::NOTASDECREDITOMCOMPRA,/*EnumTipoComprobante::NotaCreditoE,*/EnumTipoComprobante::NOTASDEDEBITOACOMPRA,EnumTipoComprobante::NOTASDECREDITOBCOMPRA,EnumTipoComprobante::NOTASDEDEBITOCCOMPRA,EnumTipoComprobante::NOTASDEDEBITOMCOMPRA/*,EnumTipoComprobante::NotaDebitoE*/);
        break;
        
        case EnumController::Remitos:
        case EnumController::RemitoItems:
        
            return array(EnumTipoComprobante::Remito);
        break;
        
        case EnumController::RemitosCompra:
        case EnumController::RemitoCompraItems:
        
            return array(EnumTipoComprobante::RemitoCompra);
        break;
        
         case EnumController::Cotizaciones:
         case EnumController::CotizacionItems:
        
            return array(EnumTipoComprobante::Cotizacion);
        break;
        
        case EnumController::PedidosInternos:
         case EnumController::PedidoInternoItems:
        
            return array(EnumTipoComprobante::PedidoInterno);
        break;
        
         case EnumController::SaldosInicialesCliente:
         case EnumController::SaldosInicialClienteItems:
        
            return array(EnumTipoComprobante::SaldoInicioCliente);
        break;
        
         case EnumController::Recibos:
         case EnumController::ReciboItems:
        
            return array(EnumTipoComprobante::ReciboManual,EnumTipoComprobante::ReciboAutomatico);
        break;
        
        case EnumController::OrdenCompra:
        case EnumController::OrdenCompraItems:
        
            return array(EnumTipoComprobante::OrdenCompra);
        break;
        
         case EnumController::SaldosInicialesProveedor:
         case EnumController::SaldosInicialProveedorItems:
        
            return array(EnumTipoComprobante::SaldoInicioProveedor);
        break;
        
        case EnumController::OrdenesPago:
        case EnumController::OrdenPagoItems:
        
            return array(EnumTipoComprobante::OrdenPagoManual,EnumTipoComprobante::OrdenPagoAutomatica);
        break;
        
        
        case EnumController::OrdenesPagoGasto:
        case EnumController::OrdenPagoGastoItems:
        
            return array(EnumTipoComprobante::OrdenPagoRenGasto);
        break;
        
        case EnumController::MovimientosBancarios:
        
            return array(EnumTipoComprobante::MovimientoBancario);
        break;

        case EnumController::MovimientosCaja:
        
            return array(EnumTipoComprobante::IngresoCaja,EnumTipoComprobante::EgresoCaja);
        break;
        
        
           case EnumController::Gastos:
           case EnumController::GastoItems:
           
        
            return array(EnumTipoComprobante::GASTOA,EnumTipoComprobante::GASTOB,EnumTipoComprobante::GASTOC,EnumTipoComprobante::Tique,EnumTipoComprobante::TiqueFacturaA,EnumTipoComprobante::TiqueFacturaB);
        break;
        
        
        case EnumController::CreditosInternoCliente:
           
        
            return array(EnumTipoComprobante::CreditoInternoVenta);
        break;
        
        
          case EnumController::CreditosInternoProveedor:
           
        
            return array(EnumTipoComprobante::CreditoInternoCompra);
        break;
        
          case EnumController::InformeRecepcionMateriales:
          case EnumController::InformeRecepcionMaterialesItems:
           
        
            return array(EnumTipoComprobante::InformeRecepcionMateriales);
        break;
        
          case EnumController::NotasCreditoCompra:
          	
          	
          	return array(EnumTipoComprobante::NOTASDECREDITOACOMPRA,EnumTipoComprobante::NOTASDECREDITOBCOMPRA,EnumTipoComprobante::NOTASDECREDITOCCOMPRA,EnumTipoComprobante::NOTASDECREDITOMCOMPRA);
          	break;
          	
          case EnumController::NotasDebitoCompra:
          	
          	
          	return array(EnumTipoComprobante::NOTASDEDEBITOACOMPRA,EnumTipoComprobante::NOTASDECREDITOBCOMPRA,EnumTipoComprobante::NOTASDEDEBITOCCOMPRA,EnumTipoComprobante::NOTASDEDEBITOMCOMPRA);
          	break;
          	
          case EnumController::NotasCreditoVenta:
          	
          	
          	return array(EnumTipoComprobante::NotaCreditoA,EnumTipoComprobante::NotaCreditoB,EnumTipoComprobante::NotaCreditoC,EnumTipoComprobante::NotaCreditoM,EnumTipoComprobante::NotaCreditoE);
          	break;
          	
          	
          case EnumController::NotasCreditoMiPymeVenta:
          	
          	
          	return array(EnumTipoComprobante::NotaCreditoAFacturaCredito,EnumTipoComprobante::NotaCreditoBFacturaCredito,EnumTipoComprobante::NotaCreditoCFacturaCredito);
          	break;
          	
          	
          	
          case EnumController::NotasDebitoVenta:
          	
          	
          	return array(EnumTipoComprobante::NotaDebitoA,EnumTipoComprobante::NotaDebitoB,EnumTipoComprobante::NotaDebitoC,EnumTipoComprobante::NotaDebitoM,EnumTipoComprobante::NotaDebitoE);
          	break;
          	
          
          case EnumController::NotasDebitoMiPymeVenta:
          	
          	
          	return array(EnumTipoComprobante::NotaDebitoAFacturaCredito,
					     EnumTipoComprobante::NotaDebitoBFacturaCredito,
						 EnumTipoComprobante::NotaDebitoCFacturaCredito);
          	break;
          	
          	
          	
          case EnumController::RechazoCheques:
          	
          	
          	return array(EnumTipoComprobante::ChequeRechazado);
          	break;
          	
          	
          case EnumController::CotizacionesCompra:
          	
          	
          	return array(EnumTipoComprobante::PresupuestoDeCompras);
          	break;
          	
          case EnumController::DebitosInternoProveedor:
          	
          	
          	return array(EnumTipoComprobante::DebitoInternoCompra);
          	break;
          	
          	
          	
          case EnumController::DebitosInternoCliente:
          	return array(EnumTipoComprobante::DebitoInternoVenta);
          	break;
          	
          	
          	return array(EnumTipoComprobante::DebitoInternoCompra);
          	break;
          	
          	
          case EnumController::Retenciones:
          	
          	
          	return array(EnumTipoComprobante::RetencionGANANCIA,EnumTipoComprobante::RetencionIIBB,EnumTipoComprobante::RetencionIVA,EnumTipoComprobante::RetencionSUSS);
          	break;
          	
          	
          case EnumController::OrdenesPagoAnticipo:
          case EnumController::OrdenPagoAnticipoItems:
          	
          	return array(EnumTipoComprobante::OrdenPagoAnticipo);
          	break;
          	
          	
          case EnumController::OrdenArmado:
          case EnumController::OrdenArmadoItems:
          	
          	return array(EnumTipoComprobante::OrdenArmado);
          	break;
          	
          	
          case EnumController::Mantenimientos:
          case EnumController::MantenimientoItems:
          	
          	return array(EnumTipoComprobante::Mantenimiento);
          	break;
          	
          	
          case EnumController::OrdenesTrabajo:
          case EnumController::OrdenTrabajoItems:
          	
          	return array(EnumTipoComprobante::OrdenTrabajo);
          	break;
          	
          	
          	
          	
          case EnumController::ReclamosVenta:
          case EnumController::ReclamosVentaItems:
          	
          	return array(EnumTipoComprobante::ReclamoVenta);
          	break;
          	
          	
          case EnumController::RemitosInterno:
          case EnumController::RemitoInternoItems:
          	
          	return array(EnumTipoComprobante::RemitoInterno);
          	break;
          	
          	
          case EnumController::QaCertificadosCalidad:
          case EnumController::QaCertificadoCalidadItems:
          	
          	return array(EnumTipoComprobante::QaCertificadoCalidad);
          	break;
          	
          	
          	
          	
        
     
    }
    
    
    
}



public function getItems($id,$activo = 1){
    
    
     $comprobante_items = $this->ComprobanteItem->find('all', array(
                                                'conditions' => array('ComprobanteItem.id_comprobante' => $id,'ComprobanteItem.activo' => $activo),
                                                'contain' =>false
                                                )); 
        
    
   if($comprobante_items)
    return $comprobante_items; 
   else
    return 0;
    
    
}


public function getCabeceraComprobante($total_comprobante,$subtotal_neto,$subtotal_bruto,$id_tipo_comprobante,$fecha_generacion,$id_estado_comprobante,$id_detalle_tipo_comprobante,$id_moneda,$valor_moneda,$valor_moneda2,$valor_moneda3,$observacion,$nro_comprobante,$id_persona,$id_usuario,$activo,$comprobante_genera_asiento,$definitivo,$id_punto_venta,$enlazar_con_moneda=-1){
    
            $cabecera_comprobante["Comprobante"]["total_comprobante"] = $total_comprobante;
            $cabecera_comprobante["Comprobante"]["subtotal_neto"] = $subtotal_neto;
            $cabecera_comprobante["Comprobante"]["subtotal_bruto"] = $subtotal_bruto;
            $cabecera_comprobante["Comprobante"]["id_tipo_comprobante"] = $id_tipo_comprobante;//ver que tipo necesita
            $cabecera_comprobante["Comprobante"]["fecha_generacion"] = $fecha_generacion;
            $cabecera_comprobante["Comprobante"]["id_estado_comprobante"] = $id_estado_comprobante;
            
            if($id_detalle_tipo_comprobante!="")
                $cabecera_comprobante["Comprobante"]["id_detalle_tipo_comprobante"] = $id_detalle_tipo_comprobante;
            
            $cabecera_comprobante["Comprobante"]["id_moneda"] = $id_moneda;
            $cabecera_comprobante["Comprobante"]["valor_moneda"] = $valor_moneda;
            $cabecera_comprobante["Comprobante"]["valor_moneda2"] = $valor_moneda2;
            $cabecera_comprobante["Comprobante"]["valor_moneda3"] = $valor_moneda3;
            $cabecera_comprobante["Comprobante"]["observacion"] = $observacion;
            $cabecera_comprobante["Comprobante"]["nro_comprobante"] = $nro_comprobante;
            $cabecera_comprobante["Comprobante"]["id_persona"] = $id_persona;
            $cabecera_comprobante["Comprobante"]["id_usuario"] = $id_usuario;
            $cabecera_comprobante["Comprobante"]["activo"] = $activo;
            $cabecera_comprobante["Comprobante"]["comprobante_genera_asiento"] = $comprobante_genera_asiento;//esta ND no debe generar asiento
            $cabecera_comprobante["Comprobante"]["definitivo"] = $definitivo;//esta ND no debe generar asiento
            $cabecera_comprobante["Comprobante"]["id_punto_venta"] = $id_punto_venta;//esta ND no debe generar asiento
            
            
            if($enlazar_con_moneda!=-1)
            	$cabecera_comprobante["Comprobante"]["enlazar_con_moneda"] = $enlazar_con_moneda;
            
            return $cabecera_comprobante;
            
    
    
    
    
}


public function getComprobanteItem($cantidad,$precio_unitario_bruto,$precio_unitario,$orden,$n_item,$id_iva,$item_observacion,$activo,$id_comprobante_origen="",$descuento_unitario){
    
        if($cantidad !="")
            $items_comprobante["ComprobanteItem"]["cantidad"] = $cantidad;
        
        if($precio_unitario_bruto !="")
        $items_comprobante["ComprobanteItem"]["precio_unitario_bruto"] = $precio_unitario_bruto;
        
        if($precio_unitario !="")
        $items_comprobante["ComprobanteItem"]["precio_unitario"] = $precio_unitario;
        
        if($orden !="")
            $items_comprobante["ComprobanteItem"]["orden"] = $orden;
        
        if($n_item !="")
            $items_comprobante["ComprobanteItem"]["n_item"] = $n_item;
        
        if($id_iva !="")
            $items_comprobante["ComprobanteItem"]["id_iva"] = $id_iva;
            
        if($descuento_unitario !="")
            $items_comprobante["ComprobanteItem"]["descuento_unitario"] = $descuento_unitario;
        
        $items_comprobante["ComprobanteItem"]["item_observacion"] = $item_observacion;
        
        $items_comprobante["ComprobanteItem"]["activo"] = $activo;
        
        if($id_comprobante_origen !="")
        $items_comprobante["ComprobanteItem"]["id_comprobante_origen"] = $id_comprobante_origen;//la Nota de debito va a tener como id_comprobante_origen al RechazoCheque
        
        return $items_comprobante;
}





 public function getOrdenCompraExterna($items){
      $array_orden_compra_externa  = array();
      
     if($items){ 
          foreach($items as $item){
              
              if(isset($item["ComprobanteItemOrigen"]["Comprobante"])){
                  
                if(!in_array($item["ComprobanteItemOrigen"]["Comprobante"]["orden_compra_externa"],$array_orden_compra_externa))  
                    array_push($array_orden_compra_externa,$item["ComprobanteItemOrigen"]["Comprobante"]["orden_compra_externa"]);
              }
              
          }
      }
      
      return $array_orden_compra_externa;
      
      
  }   
  
  
  public function getComprobantesRelacionados($id_comprobante,$id_tipo_comprobante=0,$generados=1,$incluir_comprobante_item_comprobante=1,$model,$estados_comprobante_exlcuir=array()){ //esta funcion trae los comprobantes relacionados de un comprobante. Estos se relacionan a travez de sus items en el campo id_comprobante_item_origen
  	//si generados =1 entonces devuelvo los comprobantes que se generaron a partir del mio
  	
  

  	
  	App::uses('CakeSession', 'Model/Datasource');
  	App::uses('ComprobanteItem', 'Model');
  	
  	$ci_obj = New ComprobanteItem();
  	
  	
  	$array_id_comprobante = array();
  	
  	$array_comprobantes_sub_relacionados = array();
  	$conditions = array();
  	
  	$filter_id_persona = "";
  	
  	/*Seguridad Usuario B2B*/
  	if(CakeSession::read("Auth.User.id_persona")>0){
  		
  		$id_persona = CakeSession::read("Auth.User.id_persona");
  		
  		array_push($conditions, array('Comprobante.id_persona'=>$id_persona));
  		$filter_id_persona = " AND comprobante.id_persona =".$id_persona;
  		
  	}
  	
  	array_push($conditions, array("ComprobanteItem.id_comprobante"=>$id_comprobante));
  	array_push($conditions, array('ComprobanteItem.activo'=>1));
  	
  	
  	
  	
  	/*Levanto los items del comprobante para buscar los relacionados*/
  	$comprobantes_relacionados = $ci_obj->find('all', array(
  			'conditions' => array($conditions),
  			'contain' =>array("Comprobante")
  	));
  	
  	
  	
  	
  	foreach($comprobantes_relacionados as $item){
  		
  		
  		$comprobantes = $ci_obj->query("SELECT t.id_comprobante , @pv:=t.id_comprobante_item_origen AS parent
                                        FROM (SELECT * FROM comprobante_item ORDER BY id DESC) t
                                        JOIN
                                        (SELECT @pv:=".$item["ComprobanteItem"]["id"].")tmp
                                        WHERE t.id=@pv;"
  				);
  		foreach($comprobantes as $comprobante){
  			
  			if($comprobante["t"]["id_comprobante"]!=$id_comprobante)
  				array_push($array_id_comprobante,$comprobante["t"]["id_comprobante"]);
  		}
  	}
  	
  	
  	
  	if($generados == 1){ //agrego los comprobantes que se generaron a partir del mio
  		
  		
  		$comprobantes = $ci_obj->query("SELECT DISTINCT id_comprobante
  				
                                                    FROM comprobante_item WHERE id_comprobante_item_origen IN
  				
  				
                                                    (SELECT comprobante_item.id FROM comprobante_item 
														JOIN comprobante ON comprobante.id = comprobante_item.id_comprobante				

													WHERE id_comprobante=".$id_comprobante.$filter_id_persona.")"
  				);
  		
  		
  		foreach($comprobantes as $comprobante){
  			
  			if($comprobante["comprobante_item"]["id_comprobante"]!=$id_comprobante)
  				array_push($array_id_comprobante,$comprobante["comprobante_item"]["id_comprobante"]);
  			
  				//$array_comprobantes_sub_relacionados = $this->getComprobantesRelacionados($comprobante["comprobante_item"]["id_comprobante"],0,1);
  				
  				
  				
  				$comprobantes_gen_seg_nivel = $ci_obj->query("SELECT DISTINCT id_comprobante
  						
                                                    FROM comprobante_item WHERE id_comprobante_item_origen IN
  						
  						
                                                    (SELECT id FROM comprobante_item WHERE id_comprobante=".$comprobante["comprobante_item"]["id_comprobante"].")"
  						);
  				
  				
  				foreach($comprobantes_gen_seg_nivel as $comprobanteseglev){
  					
  					if($comprobanteseglev["comprobante_item"]["id_comprobante"]!=$comprobante["comprobante_item"]["id_comprobante"])
  						array_push($array_id_comprobante,$comprobanteseglev["comprobante_item"]["id_comprobante"]);
  					
  					
  					
  				}
  				
  				
  				
  				
  				
  		}
  		
  		
  		
  		///////////IMPOSITIVOS/////////////////////////////////////////////
  		$comprobantes = $ci_obj->query("SELECT DISTINCT id_comprobante_referencia
  						
                                                    FROM comprobante_impuesto
													JOIN comprobante ON comprobante.id = comprobante_impuesto.id_comprobante
													 WHERE id_comprobante =".$id_comprobante.$filter_id_persona
  				);
  		
  		
  		foreach($comprobantes as $comprobante){
  			
  			if($comprobante["comprobante_impuesto"]["id_comprobante_referencia"]!=$id_comprobante)
  				array_push($array_id_comprobante,$comprobante["comprobante_impuesto"]["id_comprobante_referencia"]);
  		}
  		
  		///////////IMPOSITIVOS/////////////////////////////////////////////
  	}
  	
  	
  	
  	
  	
  	
  	/*selecciono aquellos comprobantes que tienen como hijos al comprobante que les paso. Ejemplo le pase FACTURA y me devuelve RECIBO*/
  	$comprobantes = $ci_obj->query("SELECT DISTINCT comprobante_item.id_comprobante
  											
                                                    FROM comprobante_item 
													JOIN comprobante ON comprobante.id = comprobante_item.id_comprobante

											WHERE id_comprobante_origen=".$id_comprobante.$filter_id_persona);
  	
  	foreach($comprobantes as $comprobante){
  		
  		
  		array_push($array_id_comprobante,$comprobante["comprobante_item"]["id_comprobante"]);
  	}
  	
  	
  	
  	if($incluir_comprobante_item_comprobante == 1){
  		//////////SELECCIONO LOS QUE SON COMPROBANTE ITEM COMPROBANTE O SEA SON COMPROBANTES DENTRO DE OTROS/////////////////////////////
  		$comprobantes = $ci_obj->query("SELECT DISTINCT id_comprobante_origen
  				
                                                    FROM comprobante_item 
										JOIN comprobante ON comprobante.id = comprobante_item.id_comprobante
												WHERE id_comprobante=".$id_comprobante.$filter_id_persona);
  		
  		foreach($comprobantes as $comprobante){
  			
  			
  			array_push($array_id_comprobante,$comprobante["comprobante_item"]["id_comprobante_origen"]);
  		}
  		
  		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  	}
  	
  	
  	
  	
  	
  	
  	////////////////////////////////////COn los id de los comprobantes los busco en la tabla para obtener mas info///////////////////////////////////
  	
  	$conditions = array();
  	
  	if(count($estados_comprobante_exlcuir)> 0)
  		array_push($conditions,array("NOT" =>array("Comprobante.id_estado_comprobante"=>$estados_comprobante_exlcuir)) );
  	
  		array_push($conditions,array("Comprobante.id"=>$array_id_comprobante) );
  	
  		
  		/*Seguridad Usuario B2B*/
  		if(CakeSession::read("Auth.User.id_persona")>0){
  			
  			$id_persona = $id_persona = CakeSession::read("Auth.User.id_persona");
  			
  			array_push($conditions, array('Comprobante.id_persona'=>$id_persona));

  			
  		}
  		
  	
  	if(count($array_id_comprobante)>0){
  		
  		$comprobantes_relacionados = $this->find('all', array(
  				'conditions' => $conditions,
  				'contain' =>array('PuntoVenta','TipoComprobante','EstadoComprobante','Transportista','Persona')
  		));
  		
  		
  		foreach($comprobantes_relacionados as &$valor){
  			
  			$valor["Comprobante"]["d_tipo_comprobante"] = $valor["TipoComprobante"]["codigo_tipo_comprobante"];
  			$valor["Comprobante"]["codigo_tipo_comprobante"] = $valor["TipoComprobante"]["codigo_tipo_comprobante"];
  			$valor["Comprobante"]["signo_comercial"] = $valor["TipoComprobante"]["signo_comercial"];
  			$valor["Comprobante"]["razon_social"] = $valor["Persona"]["razon_social"];
  			unset($valor['TipoComprobante']);
  			
  			if(isset($valor['PuntoVenta'])){
  				$valor[$model]['pto_nro_comprobante'] = (string) $this->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$model]['nro_comprobante']);
  				//unset($valor['PuntoVenta']);
  			}
  			if(isset($valor['EstadoComprobante'])){
  				$valor[$model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
  				unset($valor['EstadoComprobante']);
  			}
  			if(isset($valor['Transportista'])){
  				$valor[$model]['t_razon_social'] = $valor['Transportista']['razon_social'];
  				unset($valor['Transportista']);
  			}
  			
  		}
  		
  		
  		
  	}
  	
  	
  	//SELECT DISTINCT id_comprobante FROM comprobante_item WHERE id_comprobante_item_origen IN (SELECT id FROM comprobante_item WHERE id_comprobante=217)
  	
  	
  	
  	/*
  	 if($id_tipo_comprobante == 0 ){
  	 
  	 $filtros = array('OR'=>array(array('ci2.id_comprobante' => $id_comprobante),array('ComprobanteItem.id_comprobante' => $id_comprobante)));
  	 
  	 
  	 }else{
  	 
  	 
  	 
  	 $filtros = array(array('OR'=>array(array('ci2.id_comprobante' => $id_comprobante),array('ComprobanteItem.id_comprobante' => $id_comprobante))),'Comprobante.id_tipo_comprobante' => $id_tipo_comprobante);
  	 
  	 
  	 
  	 
  	 }
  	 
  	 
  	 
  	 //busco el item antes de que sea modificado para ver estado anterior
  	 $comprobantes_relacionados = $this->ComprobanteItem->find('all', array(
  	 'joins' => array(
  	 array(
  	 'table' => 'comprobante_item',
  	 'alias' => 'ci2',
  	 'type' => 'LEFT',
  	 'conditions' => array(
  	 'ci2.id = ComprobanteItem.id_comprobante_item_origen'
  	 )
  	 ),
  	 array(
  	 'table' => 'comprobante',
  	 'alias' => 'c',
  	 'type' => 'LEFT',
  	 'conditions' => array(
  	 'c.id = ComprobanteItem.id_comprobante'
  	 )
  	 
  	 )
  	 
  	 
  	 
  	 ),
  	 'fields'=>array('DISTINCT Comprobante.*'),
  	 'conditions' => $filtros,
  	 'contain' =>array('Comprobante'=>array('PuntoVenta'))
  	 )
  	 
  	 );
  	 */
  	return array_merge($comprobantes_relacionados,$array_comprobantes_sub_relacionados);
  	
  	
  	
  	
  }
  
  
  
  
  public function getComprobantesPorTipoYFechaYEstado($signo_comercial,$array_estado_comprobante,$fecha_desde,$fecha_hasta,$campo_busqueda = "fecha_generacion"){
  	
  	$conditions = array();
  	
  	
  		    array_push($conditions, array("TipoComprobante.signo_comercial"=>1));
  		
  		if(count($array_estado_comprobante)>0)
  			array_push($conditions, array("Comprobante.id_estado_comprobante"=>$array_estado_comprobante));
  			
  			
  			array_push($conditions, array("Comprobante.".$campo_busqueda." >=" =>$fecha_desde));
  			array_push($conditions, array("Comprobante.".$campo_busqueda." <="=>$fecha_hasta));
  			
  			
  			$comprobantes = $this->find('all', array(
  					'conditions' => $conditions ,
  					'contain' => array("PuntoVenta","TipoComprobante")
  			));
  			
  			
  			if(count($comprobantes)>0)
  				return $comprobantes;
  				else
  					return array();
  					
  					
  					
  }
  
  
  public function getNumerosComprobante($comprobantes){
  	

  	
  	$nro_comprobate = array();
  	if(count($comprobantes)>0){
  		
  		foreach($comprobantes as $comprobante){
  			
  			$d_tipo_comprobante = $comprobante["TipoComprobante"]["codigo_tipo_comprobante"];
  			
  			if(isset($comprobante["PuntoVenta"]["numero"]))
  				array_push($nro_comprobate, $d_tipo_comprobante. " " .$this->GetNumberComprobante($comprobante["PuntoVenta"]["numero"],$comprobante["Comprobante"]["nro_comprobante"]));
  				else
  					array_push($nro_comprobate, $d_tipo_comprobante. " " .$this->GetNumberComprobante($comprobante["Comprobante"]["d_punto_venta"],$comprobante["Comprobante"]["nro_comprobante"]));
  		}
  		
  	}
  	
  	return $nro_comprobate;
  	
  	
  }
  
  
  
  public function totalOrdenPago($orden_pago){
  	
  	$suma_comprobantes = 0;
  	$suma_diferencias_cambio = 0;
  	$suma_impuestos= 0;
  	
  	if(isset($orden_pago["Comprobante"]["ComprobanteItemComprobante"])){
  		
  		foreach($orden_pago["Comprobante"]["ComprobanteItemComprobante"] as $item){
  			
  			if($item["precio_unitario"]>0 && !is_null($item["precio_unitario"]) )
  				$suma_comprobantes = $suma_comprobantes + $item["precio_unitario"];
  			
  				if($item["monto_diferencia_cambio"]>0 && !is_null($item["monto_diferencia_cambio"])  &&  $item["ajusta_diferencia_cambio"]== 1)
  				$suma_diferencias_cambio =  $suma_diferencias_cambio + $item["monto_diferencia_cambio"];
  		}
  		
  	}
  	
  	
  	
  	if(isset($orden_pago["ComprobanteImpuesto"])){
  		
  		foreach($orden_pago["ComprobanteImpuesto"] as $item){
  			
  			$suma_impuestos= $suma_impuestos+ $item["importe_impuesto"];
  			
  		}
  		
  	}
  	
  	
  	
  	return ($suma_comprobantes + $suma_diferencia_cambio - $suma_impuestos - $orden_pago["Comprobante"]["importe_descuento"]);
  }
  
  
  
  public function getSumaImpuestos($comprobante){
  	
  	$suma_impuestos = 0;
  	
  	if($comprobante){
	  	
	  	foreach($comprobante["ComprobanteImpuesto"] as $item){
	  		
	  		$suma_impuestos= $suma_impuestos + ($item["base_imponible"]*$item["tasa_impuesto"])/100;
	  		
	  	}
  	}
  	
  	
  	return $suma_impuestos;
  	
  }
  
  
  public function getHeaderPDF($datos_pdf,$datos_empresa,$nombre_comprobante_pdf){
  	
  	
  	$fecha_inicio_actividad = new DateTime($datos_empresa["DatoEmpresa"]["fecha_inicio_actividad"]);
  	$fecha_entrega = new DateTime($datos_pdf['Comprobante']['fecha_contable']);
  	
  	$logo = '/img/'.$datos_empresa["DatoEmpresa"]["id"].'.png';
  	
  	
  	$nombre_comprobante_pdf = $datos_pdf["TipoComprobante"]["d_tipo_comprobante"];
  	
  	
  	$aviso_anulado = '';
  	if($datos_pdf['Comprobante']['id_estado_comprobante'] == EnumEstadoComprobante::Anulado)
  		$aviso_anulado = ' ANULADO';
  	
  		
  		
  		
  	$proforma = "";
  	
  	
  	$facturas_nota_venta  = array_merge($this->getTiposComprobanteController(EnumController::Facturas),$this->getTiposComprobanteController(EnumController::Notas));
  	
  	
  
  	if(in_array($datos_pdf['Comprobante']['id_tipo_comprobante'], $facturas_nota_venta )){//si el comprobante es alguno que necesite CAE
  	
	  	if(!$datos_pdf["Comprobante"]["cae"] >0){
	  			$proforma = " PROFORMA ";
	  			$letra = "X";
	  			$cod = "";
	  	}else{
	  			
	  			
	  			$letra = strtoupper($datos_pdf["TipoComprobante"]["letra"]);
	  			$cod = "cod ".str_pad($datos_pdf["TipoComprobante"]["codigo_afip"], 2, "0", STR_PAD_LEFT);
	  	}
  		
  	
  	}else{
  		
  		$letra = strtoupper($datos_pdf["TipoComprobante"]["letra"]);
  		$cod = "";
  	}
  	
  	
  	
  	
  	$html = '<table width="100%" border="0" cellspacing="0" style="font-size:10px;border:2px solid #000000; font-family:\'Courier New\', Courier, monospace">
          <tr>
            <td style="padding: 5px 5px 5px 5px;" width="37%" rowspan="4"  align="left" valign="center"  ><img src="'.$datos_empresa["DatoEmpresa"]["app_path"].$logo.'" width="420" height="121" />&nbsp;<strong>Razon Social: </strong>'.$datos_empresa["DatoEmpresa"]["razon_social"].' </td>
            <td width="8%" height="3%" rowspan="2"  style="border:2px solid #000000">
                <div align="center" style="font-size:30px">'.$letra.'</div>
                 <span align="center" style="font-size:10px;">'.$cod.'</span>
            </td>
            <td width="55%" style="padding-left:5px; font-size:17px;font-weight:bold;"> '.$nombre_comprobante_pdf.'  '.$proforma.' '.$aviso_anulado.': </td>
            		
          </tr>
          <tr>
            <td style="padding-left:5px; font-size:15px;"> Nro '.str_pad($datos_pdf["PuntoVenta"]["numero"],4,"0",STR_PAD_LEFT).'-'.str_pad($datos_pdf["Comprobante"]["nro_comprobante"],8,"0",STR_PAD_LEFT).'</td>
          </tr>
            		
          <tr>
            <td width="8%" height="2%" align="center" valign="middle" >&nbsp;</td>
            <td></td>
          </tr>
          <tr >
            <td  width="8%" height="5%" align="center" valign="middle" >&nbsp;</td>
            <td style="padding-left:5px;"> Original Blanco / Copia color </td>
          </tr>
          <tr>
           <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["calle"].' '.$datos_empresa["DatoEmpresa"]["numero"].' CP: '.$datos_empresa["DatoEmpresa"]["codigo_postal"].' <br>'.' '.$datos_empresa["DatoEmpresa"]["localidad"].' '.$datos_empresa["Provincia"]["d_provincia"].'</td>
            <td rowspan="5">&nbsp;</td>
            <td style="padding-left:5px;"><strong> Fecha:</strong>'.$fecha_entrega->format('d-m-Y').'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"><strong> E-mail: </strong>'.$datos_empresa["DatoEmpresa"]["email"].'</td>
            <td style="padding-left:5px;"><strong> CUIT: </strong>'.$datos_empresa["DatoEmpresa"]["cuit"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> <strong>Tel:</strong> '.$datos_empresa["DatoEmpresa"]["tel"].'</td>
            <td style="padding-left:5px;"><strong> Ingresos Brutos:</strong> '.$datos_empresa["DatoEmpresa"]["iibb"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["website"].'</td>
            <td style="padding-left:5px;"> <strong>Inicio de actividades:</strong> '.$fecha_inicio_actividad->format('d-m-Y').'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"><strong> '.strtoupper($datos_empresa["TipoIva"]["d_tipo_iva"]).'</strong></td>
            <td style="padding-left:5px;">&nbsp;</td>
          </tr>
        </table>';
  	
  	return $html;
  }
  
  
  
  public function getNamePDF($datos_pdf){
  	
  	
  	
  	$razon_social = $this->sanear_string(trim($datos_pdf["Persona"]["razon_social"]));
  	return str_replace(".","",$datos_pdf["TipoComprobante"]["codigo_tipo_comprobante"].'_'.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'-'.$razon_social);
  }
  
  
  public function getNamePDFRetencion($datos_pdf){
  	
  	
  	$razon_social = $this->sanear_string(trim($datos_pdf["Persona"]["razon_social"]));
  	return str_replace(".","",$datos_pdf["Comprobante"]["TipoComprobante"]["codigo_tipo_comprobante"].'_'.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'-'.$datos_pdf["ComprobanteReferencia"]["TipoComprobante"]["abreviado_tipo_comprobante"].'_'.$datos_pdf['Impuesto']['d_impuesto'].'_'.$datos_pdf["ComprobanteReferencia"]["nro_comprobante_completo"].'-'.$razon_social);
  	
  }
  
  
  public function getFooterPDF($datos_pdf,$datos_empresa){
  
  }
  
  
  public function getTipoMovimientoDefecto($id_comprobante){
  	
  	$comprobante = $this->find('first', array(
  			'conditions' => array('Comprobante.id' => $id_comprobante),
  			'contain' =>array('TipoComprobante')
  	));
  	return $comprobante["TipoComprobante"]["id_tipo_movimiento"];
  	
  }
  
  
  public function getRootPDF(){//define donde se guardan en el servidor los PDF generados
  	
  	return '/webroot/files/comprobantes/';
  }
  
  public function actualizaCountViewPdf($id_comprobante){
  	
  	
  	
  	$this->query("UPDATE comprobante SET cantidad_impresion_pdf = IF(cantidad_impresion_pdf IS NULL, 1, cantidad_impresion_pdf + 1)  WHERE id=".$id_comprobante.";");
  	return;
  	
  }
  
  
  public function pdfExport($id,$vista){
  	
  	try{
  		
  		
  		$this->actualizaCountViewPdf($id);
  		
  		//$this->Comprobante->id = $id;
  		/*$this->Comprobante->contain(array('ComprobanteItem' => array('Producto'=>array('Iva'),'Iva','Unidad'),'EstadoComprobante','Moneda',
  		 'Persona'=> array('Pais','Provincia'),'CondicionPago','PuntoVenta','TipoComprobante','CondicionPago',
  		 'ComprobanteImpuesto'=>array('Impuesto'),'Transportista','Sucursal'=>array('Provincia','Pais')));
  		 */
  		
  		$data = $this->find('first',array('contain'=>array('ComprobanteItem' => array( 'conditions'=>array('ComprobanteItem.activo'=>1),
  			'order' => $this->ComprobanteItem->getOrderItems("ComprobanteItem",$id),
  				'Producto'=>array('Iva','Unidad'),'Iva','Unidad','ComprobanteItemOrigen'=>array("Comprobante")
  		),
  				'EstadoComprobante','Moneda','CarteraRendicion','Persona'=> array('Pais','Provincia','TipoDocumento','TipoIva'),
  				'CondicionPago','PuntoVenta','TipoComprobante'=>array(),'CondicionPago','Usuario',
  				'ComprobanteImpuesto'=>array('Impuesto','ComprobanteReferencia'),'Transportista'=>array('Provincia','Pais'),
  				'Sucursal'=>array('Provincia','Pais'),'ComprobanteItemComprobante'=>array('ComprobanteOrigen'=>array('TipoComprobante','PuntoVenta','MonedaEnlazada','Moneda')),
  				'ComprobanteValor'=>array('Valor','CuentaBancaria'=>array('BancoSucursal'=>array('Banco') ),'Cheque'=>array('Banco','BancoSucursal','Chequera'=>array('CuentaBancaria'=>array('BancoSucursal'=>array('Banco')) ))),'ChequeEntrada'=>array('Banco'),'ChequeSalida'=>array('Banco','Chequera'=>array('CuentaBancaria'=>array('BancoSucursal'=>array('Banco')) ) ),'DetalleTipoComprobante','ComprobanteReferenciaImpuesto'=>array('Comprobante'=>array("ComprobanteItem"=>array("ComprobanteOrigen"=>array("TipoComprobante"))),'Impuesto')),
  				'conditions' =>array('Comprobante.id'=>$id),
  		));
  		//$data = $this->Comprobante->read();
  		$data= $this->getSucursalEnvio($data);

  		
  		
  		if( $data["Persona"]["id"] == null )
  		{
  			$data["Persona"]["razon_social"] = $data["Comprobante"]["razon_social"];
  			$data["Persona"]["cuit"] =           $data["Comprobante"]["cuit"];
  		}
  		
  		$data["Comprobante"]["nro_comprobante_completo"] =
  		$this->GetNumberComprobante($data["PuntoVenta"]["numero"],$data["Comprobante"]["nro_comprobante"]);
  		
  		$this->set('datos_pdf',$data);
  		//$this->set('datos_empresa',$this->Session->read('Empresa'));
  		//$this->set('array_filters_name',$this->array_filter_names);
  		
  		$impuestos = '';
  		$this->set('id',$id);
  		Configure::write('debug',0);
  		$this->layout = 'pdf'; //esto usara el layout pdf.ctp
  		//$this->response->type('pdf');
  		
  		
  		if($vista=='')
  		{
  			$this->render();
  		}else
  		{
  			
  			// $this->autoRender = false;
  			// $this->layout= false;
  			// $this->viewPath = 'Recibos/';
  			
  			
  			/*
  			 App::import('Model','Recibo');
  			 
  			 $recibo = new Recibo();
  			 
  			 //$recibo->getPDF($data,$this->Session->read('Empresa'),0);
  			 
  			 */
  			
  			//$this->render($vista);
  			
  			
  			
  			
  			$view = new View($this,false);
  			$view->layout= 'pdf'; // if you want to disable layout
  			
  			
  			$view->set('datos_pdf',$data);
  			//$view->set('datos_empresa',$this->Session->read('Empresa'));
  			//$view->set('array_filters_name',$this->array_filter_names);
  			
  			$impuestos = '';
  			$view->set('id',$id);
  			
  			$view->render($vista);
  		
  			
  		}
  		
  	}catch(Exception $e){
  		$output = array(
  				"status" =>EnumError::ERROR,
  				"message" => $e->getMessage(),
  				"content" => "",
  				
  		);
  		
  	}
  }
  
  




}
