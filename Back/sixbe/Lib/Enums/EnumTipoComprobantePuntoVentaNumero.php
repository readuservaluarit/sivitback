<?php
//https://docs.google.com/spreadsheets/d/1AH9R_spFKqupmHD-Dsbq33T72RRUcjdx1J7Xf98CBpg/edit#gid=1494275133
class EnumTipoComprobantePuntoVentaNumero
{
	const     Ajeno    = -1;// es solo para la parametrizacion de comprobante_punto_venta_numero en este caso en esa tabla el id_punto_venta es NULL
	const     PropioOfflinePreimpreso    = 0;
	const     PropioNumeradorSistema   = 1; // PROFORMA
	const     PropioElectronicoOnline   = 2;
	const     PropioElectronicoOfflineExportacion   = 20;
	const     PropioNumeradorSistemaEDITABLE = 10;  // Se puede anular y volver asignar un numero q ya paso. CASO OA (acuņamiento de cuerpos) por defecto la numeracion del sistema ej : 11-000001 ; 11-000002 ; 11-000003 etc
}
?>