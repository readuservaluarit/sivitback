<?php

App::uses('NotasCompraController', 'Controller');




class NotasDebitoCompraController extends NotasCompraController

    {
    	public $name = EnumController::NotasDebitoCompra;
    
    
    	
    	/**
    	 * @secured(CONSULTA_NOTA_COMPRA)
    	 */
    	public
    	
    	function getModel($vista = 'default')
    	{
    		
    		
    		$model = parent::getModelCamposDefault(); //esta en APPCONTROLLER
    		$model = $this->editforView($model, $vista); //esta funcion edita y agrega campos para la vista, debe estar LOCAL
    	}
    	
    	
    	private
    	function editforView($model, $vista)
    	{ //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    		$model = parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
    		
    		// $model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
    		$this->autoRender = false;
    		$this->layout = false;
    		$this->viewPath = "/NotasCompra/";
    		$this->set('model', $model);
    		Configure::write('debug', 0);
    		$this->render($vista);
    	}
    
     }
     

?>