


SET @id_impuesto := 29;
SET @direccion_archivo := '/var/www/PadronRGSPer082015.txt';
SET @separador_linea := ';';
SET @salto_linea := '\n';
SET @tabla_temporal := 'persona_impuesto_alicuota_temporal';



/*importo el archivo sobre la tabla*/
TRUNCATE persona_impuesto_alicuota_temporal;
LOAD DATA INFILE '/var/www/PadronRGSPer082015.txt' INTO TABLE persona_impuesto_alicuota_temporal
FIELDS TERMINATED BY ';'
LINES TERMINATED BY '\n';


INSERT INTO persona_impuesto_alicuota 
           (fecha_vigencia_desde,fecha_vigencia_hasta,porcentaje_alicuota,id_persona,id_impuesto)
 SELECT DISTINCT piat.fecha_vigencia_desde,piat.`fecha_vigencia_hasta`,piat.`porcentaje_alicuota`,p.id,29

                                                        FROM persona_impuesto_alicuota_temporal piat 

                                                        JOIN persona p ON p.`cuit`=piat.`cuit`
                                                        
                                                         LEFT JOIN persona_impuesto_alicuota pia ON p.id = pia.id_persona
                                                         
                                                         WHERE pia.`id_persona` IS NULL



/*actualizo la tabla temporal con el id_persona*/

UPDATE  persona_impuesto_alicuota_temporal piat
JOIN persona p 


ON p.cuit= piat.cuit

SET 

piat.id_persona = p.id;



UPDATE persona_impuesto_alicuota pia
JOIN persona_impuesto_alicuota_temporal piat
ON piat.id_persona = pia.id_persona
SET pia.fecha_vigencia_desde = piat.fecha_vigencia_desde,
    pia.fecha_vigencia_hasta = piat.fecha_vigencia_hasta,
    pia.porcentaje_alicuota = piat.porcentaje_alicuota
WHERE id_impuesto=29;



INSERT INTO persona_impuesto_alicuota (fecha_vigencia_desde,fecha_vigencia_hasta,porcentaje_alicuota,id_persona,id_impuesto)
SELECT piat.fecha_vigencia_desde,piat.`fecha_vigencia_hasta`,piat.`porcentaje_alicuota`,p.`id`,29 

FROM persona_impuesto_alicuota_temporal piat 

JOIN persona p ON p.`cuit`=piat.`cuit`


WHERE piat.`id_persona`=0



DELETE FROM persona_impuesto_alicuota pia WHERE



SELECT * FROM persona_impuesto_alicuota







