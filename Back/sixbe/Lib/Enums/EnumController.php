<?php

class EnumController
{	
	 const  AlarmasUsuario = "AlarmasUsuario";
	 const  RightSidebar = "RightSidebar";
	 const  Entidades = "Entidades";
	 const  Stock = "Stock";
	 const  StockComponentes = "StockComponentes";
	 const  StockMateriaPrimas = "StockMateriaPrimas";
     const  StockProductos = "StockProductos";
     const  StockProductosRetail = "StockProductosRetail";
     const  StockBienesUso = "StockBienesUso";
     const  StockInsumosInterno = "StockInsumosInterno";
	 const  StockPackaging = "StockPackaging";
	 const  StockTracer = "StockTracer";
	 const  Sucursales = "Sucursales";
	 const  TiposAsiento = "TiposAsiento";
	 const  TiposAsientoLeyenda = "TiposAsientoLeyenda";
	 const  TiposCarteraRendicion = "TiposCarteraRendicion";
	 const  TiposCheque = "TiposCheque";
	 const  TiposComercializacion = "TiposComercializacion";
	 const  TiposComprobante = "TiposComprobante";
	 const  TiposCuentaBancaria = "TiposCuentaBancaria";
	 const  TiposCuentaContable = "TiposCuentaContable";
	 const  TiposDocumento = "TiposDocumento";
	 const  TiposEmpresa = "TiposEmpresa";
	 const  TiposImpuesto = "TiposImpuesto";
	 const  Modulos = "Modulos";
	 const  TiposIva = "TiposIva";
	 const  TiposLote = "TiposLote";
	 const  TiposPersona = "TiposPersona";
	 const  TransportistasCategorias = "TransportistasCategorias";
	 const  Transportistas = "Transportistas";
	 const  TurCotizacionConceptos = "TurCotizacionConceptos";
	 const  TurCotizaciones = "TurCotizaciones";
	 const  UnidadesAfip = "UnidadesAfip";
	 const  UnidadesCalculo = "UnidadesCalculo";
	 const  Unidades = "Unidades";
	 const  Usuarios = "Usuarios";
	 const  Valores = "Valores";
	 const  AgrupacionesAsiento = "AgrupacionesAsiento";
	 const  App = "App";
	 const  Areas = "Areas";
	 const  Articulo = "Articulo";
	 const  Asientos = "Asientos";
	 const  AsientosCuentaContable = "AsientosCuentaContable";
	 const  AsientosEfecto = "AsientosEfecto";
	 const  AsientosModelo = "AsientosModelo";
	 const  AsientosModeloCuentaContable = "AsientosModeloCuentaContable";
	 const  Auditorias = "Auditorias";
	 const  Bancos = "Bancos";
	 const  BancosSucursal = "BancosSucursal";
	 const  CarterasRendicion = "CarterasRendicion";
	 const  Categorias = "Categorias";
	 const  CentrosCosto = "CentrosCosto";
	 const  Chequeras = "Chequeras";
	 const  Cheques = "Cheques";
	 const  ChequesPropio = "ChequesPropio";
	 const  ClasesAsiento = "ClasesAsiento";
	 const  ClasesCuentaContable = "ClasesCuentaContable";
	 const  ClientesCategorias = "ClientesCategorias";
	 const  Clientes = "Clientes";
	 const  Componentes = "Componentes";
	 const  ComponentesProducto = "ComponentesProducto";
	 const  ComprobanteImpuestos = "ComprobanteImpuestos";
	 const  ComprobanteItemComprobantes = "ComprobanteItemComprobantes";
	 const  ComprobanteItemLotes = "ComprobanteItemLotes";
	 const  ComprobanteItems = "ComprobanteItems";
	 const  Comprobantes = "Comprobantes";
	 const  ComprobantesPuntoVentaNumero = "ComprobantesPuntoVentaNumero";
     const  ComprobanteValores = "ComprobanteValores";
     const  ComprobanteLotes = "ComprobanteLotes";
	 const  ComprobanteLoteEjecuciones = "ComprobanteLoteEjecuciones";
	 const  ConceptosCaja = "ConceptosCaja";
	 const  ConfiguracionAplicaciones = "ConfiguracionAplicaciones";
	 const  Contactos = "Contactos";
	 const  Costos = "Costos";
	 const  Cotizaciones = "Cotizaciones";
	 const  CotizacionesCompra = "CotizacionesCompra";
     const  CotizacionItems = "CotizacionItems";
     const  CotizacionCompraItems = "CotizacionCompraItems";
     const  Contadores = "Contadores";
     const  CreditosInternoCliente = "CreditosInternoCliente";
	 const  CreditosInternoProveedor = "CreditosInternoProveedor";
	 const  DebitosInternoProveedor = "DebitosInternoProveedor";
	 const  CuentasBancaria = "CuentasBancaria";
	 const  CuentasContable = "CuentasContable";
	 const  CuentasCorrienteCliente = "CuentasCorrienteCliente";
	 const  CuentasCorriente = "CuentasCorriente";
	 const  CuentasCorrientesProveedor = "CuentasCorrienteProveedor";
	 const  DatoEmpresaImpuestos = "DatoEmpresaImpuestos";
	 const  DatosEmpresa = "DatosEmpresa";
	 const  DepositoCategorias = "DepositoCategorias";
	 const  DepositoMovimientoTipos = "DepositoMovimientoTipos";
	 const  Depositos = "Depositos";
	 const  DescuentoFamiliaPersonas = "DescuentoFamiliaPersonas";
	 const  DestinosArticulo = "DestinosArticulo";
	 const  DetallesTipoComprobante = "DetallesTipoComprobante";
	 const  EjerciciosContable = "EjerciciosContable";
	 const  Empleados = "Empleados";
	 const  EntityPicker = "EntityPicker";
	 const  Equipos = "Equipos";
	 const  EstadosAsientos = "EstadosAsientos";
	 const  EstadosCheque = "EstadosCheque";
	 const  EstadosChequera = "EstadosChequera";
	 const  EstadosComprobante = "EstadosComprobante";
	 const  Estados = "Estados";
	 const  EstadosTipoCheque = "EstadosTipoCheque";
	 const  FacturaCompraItems = "FacturaCompraItems";
	 
	 const  FacturaItems = "FacturaItems";
	 const  FacturaCreditoItems = "FacturaCreditoItems";
	 const  FacturasCompra = "FacturasCompra";
	 const  AnticiposCompra = "AnticiposCompra";
	 const  AnticipoCompraItems= "AnticipoCompraItems";
	 const  Facturas = "Facturas";
	 const  FacturasCredito = "FacturasCredito";
	 const  FamiliasProducto = "FamiliasProducto";
	 const  Files = "Files";
	 const  FormasPago = "FormasPago";
	 const  Funciones = "Funciones";
     const  Gastos = "Gastos";
	 const  GastoItems = "GastoItems";
	 const  Georeferencia = "Georeferencia";
	 const  IdDetalleAbm = "IdDetalleAbm";
	 const  IdDetalleOrdenAbm = "IdDetalleOrdenAbm";
     const  Impuestos = "Impuestos";
     const  InformeRecepcionMateriales = "InformeRecepcionMateriales";
	 const  InformeRecepcionMaterialesItems = "InformeRecepcionMaterialesItems";
	 const  Ivas = "Ivas";
	 const  ListaPrecioProductos = "ListaPrecioProductos";
	 const  Lotes = "Lotes";
	 const  Marcas = "Marcas";
	 const  MateriasPrimaComponente = "MateriasPrimaComponente";
	 const  MateriasPrimaDepositos = "MateriasPrimaDepositos";
	 const  MateriasPrimas = "MateriasPrimas";
	 const  MediaFiles = "MediaFiles";
	 const  MisDatos = "MisDatos";
	 const  Monedas = "Monedas";
	 const  MovimientosBancarios = "MovimientosBancarios";
	 const  MovimientosCaja = "MovimientosCaja";
	 const  MovimientosCarteraRendicion = "MovimientosCarteraRendicion";
	 const  Movimientos = "Movimientos";
	 const  MovimientosTipo = "MovimientosTipo";
	 const  NotaCompraItems = "NotaCompraItems";
	 const  NotaItems = "NotaItems";
	 const  NotasCompra = "NotasCompra";
	 const  NotasCreditoCompra = "NotasCreditoCompra";
	 const  NotasDebitoCompra = "NotasDebitoCompra";
	 const  NotasCreditoVenta = "NotasCreditoVenta";
	 const  NotasDebitoVenta = "NotasDebitoVenta";
	 const  Notas = "Notas";
	 const  NotasUsuario = "NotasUsuario";
	 const  OrdenArmado = "OrdenArmado";
	 const  OrdenArmadoItems = "OrdenArmadoItems";
	 const  OrdenCompra = "OrdenCompra";
	 const  OrdenCompraItems = "OrdenCompraItems";
	 
	 const  OrdenesTrabajo= "OrdenesTrabajo";
	 const  OrdenTrabajoItems = "OrdenTrabajoItems";
	 
	 const  OrdenesPago = "OrdenesPago";
	 const  OrdenesPagoGasto = "OrdenesPagoGasto";
	 const  OrdenesPagoAnticipo = "OrdenesPagoAnticipo";
	 const  OrdenPagoAnticipoItems= "OrdenPagoAnticipoItems";
	 const  OrdenesProduccion = "OrdenesProduccion";
	 const  OrdenPagoConcepto = "OrdenPagoConcepto";
	 const  OrdenPagoGastoItems = "OrdenPagoGastoItems";
	 const  OrdenPagoItems = "OrdenPagoItems";
	 const  Origenes = "Origenes";
	 const  Pages = "Pages";
	 const  Pais = "Pais";
	 const  Paises = "Paises";
	 const  PedidoB2BItems= "PedidoB2BItems";
	 const  PedidoInternoItems= "PedidoInternoItems";
	 const  PedidosInternos = "PedidosInternos";
	 const  PedidosWeb= "PedidosWeb";	
	 const  PedidosB2B = "PedidosB2B";
	 const  PedidosB2C = "PedidosB2C";
	 const  PeriodosContable = "PeriodosContable";
	 const  PersonaImpuestosAlicuotas = "PersonaImpuestosAlicuotas";
	 const  PersonasCategorias = "PersonasCategorias";
	 const  Personas = "Personas";
	 const  Planos = "Planos";
	 const  Productos = "Productos";
	 const  ProductosRelacion = "ProductosRelacion";
	 const  ProveedoresCategorias = "ProveedoresCategorias";
	 const  Proveedores = "Proveedores";
	 const  Provincias = "Provincias";
	 const  PuntosVenta = "PuntosVenta";
	 const  QaAccionesCorrectivas = "QaAccionesCorrectivas";
	 const  QaCertificadoCalidadItems = "QaCertificadoCalidadItems";
	 const  QaCertificadosCalidad = "QaCertificadosCalidad";
	 const  QaConformidades = "QaConformidades";
	 const  QaInformeItems = "QaInformeItems";
	 const  QaInformes = "QaInformes";
	 const  RecepcionMateriales = "RecepcionMateriales";
	 const  RecepcionMaterialesItems = "RecepcionMaterialesItems";
	 const  ReciboItems = "ReciboItems";
	 const  ReciboRecepcionItems = "ReciboRecepcionItems";
	 const  Recibos = "Recibos";
	 const  RechazoCheques = "RechazoCheques";
	 const  RemitoCompraItems = "RemitoCompraItems";
	 const  RemitoItems = "RemitoItems";
	 const  RemitosCompra = "RemitosCompra";
	 const  Remitos = "Remitos";
	 const  RemitosInterno = "RemitosInterno";
	 const  RemitoInternoItems = "RemitoInternoItems";
	 const  Reportes = "Reportes";
	 const  RetencionesAjenas = "RetencionesAjenas";
	 const  Retenciones = "Retenciones";
	 const  Roles = "Roles";
	 const  RequerimientoInternoCompra = "RequerimientoInternoCompra";
	 const  RequerimientoInternoCompraItems= "RequerimientoInternoCompraItems";
	
	 const  SaldosInicialClienteItems = "SaldosInicialClienteItems";
	 const  SaldosInicialProveedorItems = "SaldosInicialProveedorItems";
	 const  SaldosInicialesCliente = "SaldosInicialesCliente";
	 const  SaldosIniciales = "SaldosIniciales";
	 const  SaldosInicialesProveedor = "SaldosInicialesProveedor";
	 const  Security = "Security";
     const  Sistemas = "Sistemas";
     const  Scrap = "Scrap";
     const  StockScrap = "StockScrap";
     const  Packaging = "Packaging";
     const  BieneUso = "BienesUso";
     const  InsumosInterno = "InsumosInterno";
     const  Servicios = "Servicios";
	 const  GastosArticulo = "GastosArticulo";
	 const  DebitosInternoCliente = "DebitosInternoCliente";
	 const  Mantenimientos = "Mantenimientos";
	 const  MantenimientoItems= "MantenimientoItems";
	 const  ReportesTableroUsuario= "ReportesTableroUsuario";
	 const  MercadoLibre= "MercadoLibre";
	 const  ClientesOrigen= "ClientesOrigen";
	 const  ProveedoresOrigen= "ProveedoresOrigen";
	 const  PersonasOrigen= "PersonasOrigen";
	 const  ReclamosVenta= "ReclamosVenta";
	 const  ReclamosVentaItems= "ReclamosVentaItems";
	 const  MailSender = "MailSender";
	 const  NotasCreditoMiPymeVenta = "NotasCreditoMiPymeVenta";
	 const  NotasDebitoMiPymeVenta = "NotasDebitoMiPymeVenta";

	 
}

  
?>