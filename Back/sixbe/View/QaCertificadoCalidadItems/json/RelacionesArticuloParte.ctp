<?php 
		include(APP.'View'.DS.'ComprobanteItemComprobantes'.DS.'json'.DS.'default.ctp');


		$model["n_item"]["show_grid"] = 1;
        $model["n_item"]["header_display"] = "Nro. Item";
        $model["n_item"]["order"] = "1";
        $model["n_item"]["width"] = "7";

   		$model["d_categoria"]["show_grid"] = 1;
		$model["d_categoria"]["read_only"] = 0;
		$model["d_categoria"]["type"] = EnumTipoDato::cadena;
        $model["d_categoria"]["header_display"] = "C&adegoria"; /*SOLO  LLEVA LA DESCRIPCION DE QUE ES CUERPO, VASTAGO ETC*/
		
		$model["d_producto"]["show_grid"] = 1;
		$model["d_producto"]["read_only"] = 0;
        $model["d_producto"]["type"] = EnumTipoDato::cadena;
        $model["d_producto"]["header_display"] = "Material";
        $model["d_producto"]["order"] = "3";
        $model["d_producto"]["width"] = "35";
        
        $model["codigo_hijo"]["show_grid"] = 1;
		$model["codigo_hijo"]["read_only"] = 0;
		$model["codigo_hijo"]["type"] = EnumTipoDato::cadena;
        $model["codigo_hijo"]["header_display"] = "C&oacute;digo";
        $model["codigo_hijo"]["order"] = "2";
        $model["codigo_hijo"]["width"] = "20";
		
        
        
		/*TOdo esto va en la otra table*/
        // $model["lote"]["show_grid"] = 1;
        // $model["lote"]["header_display"] = "Lote/s";
        // $model["lote"]["order"] = "4";
        // $model["lote"]["width"] = "75";
		// $model["lote"]["text_colour"] = "#0000FF";
		
		// $model["componente_codigo"]["show_grid"] = 1;
        // $model["componente_codigo"]["header_display"] = "OT/s";
        // $model["componente_codigo"]["order"] = "2";
        // $model["componente_codigo"]["width"] = "20";
        

 $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItem",
                    "content" => $model
                );
        echo json_encode($output);
?>