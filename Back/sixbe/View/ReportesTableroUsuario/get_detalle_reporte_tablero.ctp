<?php
   $abm->setController($this);
 //$abm->getListado();
?>



<div class="container" style="margin-top:50px;">
    <div class="row">
    	<div class="col-sm-12">
    	<?php
   $abm->getListado();
   
   ?>
   
    	
    	      </div>
	</div>
</div>


<script>

$(document).ready( function () {

$(document).attr('title','<?php echo $abm->getTitulo() . " " . date('d-m-Y');?>');

 var table =   $('#myTable').DataTable( {
   "dom": 'Bfrtip',
   "colReorder": true,
     "buttons": [
         
        {
            text: '&nbsp;Volver',
            action: function ( e, dt, node, config ) {
               window.history.back();
              
        },
         attr:  {
            
                id: 'volverid'
            }
     
        
        }
            ,
        {
            extend: 'copy',
            text: '&nbsp;Copiar'
        }
        , {
            extend: 'excel',
            text: '&nbsp;Excel'
        },
        {
            extend: 'pdf',
            text: '&nbsp;PDF'
        },

        
        
    ],
     "language": {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "Ningun resultado - sorry",
            "info": "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
            "infoEmpty": "Sin registros disponibles",
            "infoFiltered": "(filtered from _MAX_ total records)",
             "loadingRecords": "Cargando...",
             "processing":     "Procesando...",
            "search":         "Busqueda:",
             "paginate": {
					        "first":      "Primero",
					        "last":       "Ultimo",
					        "next":       "Siguiente",
					        "previous":   "Previo"
					    }
        }
} );




    
} );

$( document ).ready(function() {
$(".buttons-copy").removeClass("dt-button");
$(".buttons-copy").toggleClass("fa fa-copy");
$(".buttons-copy").removeClass("dt-button");
$(".buttons-copy").addClass("btn btn-primary");


$(".buttons-excel").removeClass("dt-button");
$(".buttons-excel").toggleClass("fa fa-file");
$(".buttons-excel").removeClass("dt-button");
$(".buttons-excel").addClass("btn btn-primary");


$(".buttons-pdf").removeClass("dt-button");
$(".buttons-pdf").toggleClass("fa fa-file");
$(".buttons-pdf").removeClass("dt-button");
$(".buttons-pdf").addClass("btn btn-primary");



$("#volverid").removeClass("dt-button");
$("#volverid").toggleClass("fa fa-arrow-left");
$("#volverid").removeClass("dt-button");
$("#volverid").addClass("btn btn-primary");



});


</script>