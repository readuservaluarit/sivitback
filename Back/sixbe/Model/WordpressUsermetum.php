<?php

App::uses('WordpressAppModel', 'Wordpress.Model');

class WordpressUsermetum extends WordpressAppModel {

    public $useDbConfig = 'wordpress_database';
    public $name = 'WordpressUsermetum';
    public $primaryKey = 'umeta_id';
    public $useTable = 'six_wp_usermeta';

    //The Associations below have been created with all possible keys, those that are not needed can be removed
    public $belongsTo = array(
        'User' => array(
            'className' => 'WordpressUser',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
?>