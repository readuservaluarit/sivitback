<?php

    if($nombre_archivo  == "")
        $nombre_archivo = "ArbaRetencion";
        
    header("Content-Disposition: attachment; filename=".$nombre_archivo.".txt");
    
    
    /*
     
		RETENCIONES
		La fecha de corte es de 1 a 15 y de 16 a 30 o 31 (fin de mes)
    
    */
    
    
    
    header("Pragma: no-cache"); 
    header("Expires: 0"); 
    
    $comprobante_obj  = new Comprobante();
    $persona_obj  = new Persona();
    $id_moneda_base  = $dato_empresa["DatoEmpresa"]["id_moneda"];
    
    foreach($datos as $comprobante){
        
      $nro_doc_final_01 =  str_pad($comprobante["Comprobante"]["Persona"]["cuit"], 11, '0', STR_PAD_LEFT);
      $nro_doc_final_01 = $persona_obj->getCuitConGuiones($nro_doc_final_01);
      
      $fecha = new DateTime($comprobante["Comprobante"]['fecha_contable']);
      $fecha_02 =$fecha->format('d/m/Y');
      
      
      
     
     

      $punto_venta_03 = str_pad($comprobante["ComprobanteReferencia"]["PuntoVenta"]["numero"], 4, '0', STR_PAD_LEFT);
      $nro_comprobante_04 = str_pad($comprobante["ComprobanteReferencia"]["nro_comprobante"], 8, '0', STR_PAD_LEFT);//el numero de la retencion generada por el siv
      
      $importe_impuesto = $comprobante_obj->ExpresarEnTipoMonedaMejorado($comprobante["ComprobanteImpuesto"]["importe_impuesto"],"valor_moneda2", $comprobante["Comprobante"],1,$comprobante["ComprobanteImpuesto"]["base_imponible"],$comprobante["ComprobanteImpuesto"]["tasa_impuesto"]);
       
       
       $importe_impuesto = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($importe_impuesto,8,2,"0");;
       $parte_entera = substr($importe_impuesto,0,8);
       $parte_decimal = substr($importe_impuesto,8,2);
       $importe_impuesto_05 = $parte_entera.','.$parte_decimal;
       $tipo_operacion_a_06 = 'A';
     
       
       
     
       
       
       
       $linea_texto = $nro_doc_final_01.$fecha_02.$punto_venta_03.$nro_comprobante_04.$importe_impuesto_05.$tipo_operacion_a_06; 
       
       
       
       echo $linea_texto.  "\r\n";       
    }
        


?>