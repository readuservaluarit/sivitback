<?php
/**
* @secured(CONSULTA_EJERCICIO_CONTABLE)
*/
class EjerciciosContableController extends AppController {
    public $name = 'EjerciciosContable';
    public $model = 'EjercicioContable';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
/**
* @secured(CONSULTA_EJERCICIO_CONTABLE)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->loadModel("Contador");
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
        $nombre = strtolower($this->getFromRequestOrSession('EjercicioContable.d_ejercio_contable'));
        $id_estado = strtolower($this->getFromRequestOrSession('EjercicioContable.id_estado'));
        $fecha_apertura_desde = strtolower($this->getFromRequestOrSession('EjercicioContable.fecha_apertura_desde'));
        $id_excluir = strtolower($this->getFromRequestOrSession('EjercicioContable.id_excluir'));
        
        $conditions = array(); 
        if ($nombre != "") {
            array_push($conditions, array('LOWER(EjercicioContable.d_EJERCICIO_CONTABLE) LIKE' => '%' . $nombre . '%'));
        }
        
        if ($id_estado != "") {
            array_push($conditions, array('EjercicioContable.id_estado' =>  $id_estado));
        }
        
        if ($fecha_apertura_desde != "") {
            array_push($conditions, array('EjercicioContable.fecha_apertura >=' =>  $fecha_apertura_desde));
        }
        
        
        if ($fecha_apertura_desde != "") {
            array_push($conditions,  array(  'NOT' => array(
            'EjercicioContable.id' => array($id_excluir)
        )));
        }
      
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' => array('Contador','Estado'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de EjerciciosContable', 'Datos de las EjerciciosContable', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_EJERCICIO_CONTABLE', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'EjercicioContable.id', "10%");
                $formBuilder->addHeader('Nombre', 'EjercicioContable.d_EJERCICIO_CONTABLE', "50%");
                $formBuilder->addHeader('Nombre', 'EjercicioContable.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_EJERCICIO_CONTABLE');
                $formBuilder->addField($this->model, 'cotizacion');
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        foreach($data as &$dato) {
               $dato["EjercicioContable"]["d_contador"] = $dato["Contador"]["d_contador"];
               $dato["EjercicioContable"]["ultimo_asiento"] = (string) $this->Contador->getNumeroActual($dato["Contador"]["id"]);
               unset($dato["Contador"]);
               $dato["EjercicioContable"]["d_estado"] = $dato["Estado"]["d_estado"];
               unset($dato["Estado"]);
               
               if($dato["EjercicioContable"]["activo"] == 1)
                $dato["EjercicioContable"]["d_activo"] = "Si";
               else
                $dato["EjercicioContable"]["d_activo"] = "No";
            
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    


    /**
    * @secured(ADD_EJERCICIO_CONTABLE)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_ejercicio_contable = '';
            
            
            $ds = $this->EjercicioContable->getdatasource();
            try{
                $ds->begin();
                if ($this->EjercicioContable->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "El EjercicioContable ha sido creada exitosamente";
                    $tipo = EnumError::SUCCESS;
                    $id_ejercicio_contable = $this->EjercicioContable->id;
                    
                    
                    if($this->request->data["EjercicioContable"]["crear_periodo"] == 1)
                        $this->crearPeriodos($id_ejercicio_contable);
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
                
             $ds->commit();
            
            }catch(Exception $e){
                
             $ds->rollback();
                
                $mensaje = "Ha ocurrido un error,el Ejercicio Contable no ha podido ser creada.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" =>$id_ejercicio_contable
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content" ,"id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }

    /**
    * @secured(MODIFICACION_EJERCICIO_CONTABLE)
    */
     
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            $this->loadModel("PeriodoContable");
            
            try{ 
                
                $id_estado_grabado = $this->EjercicioContable->getEstado($id);
                $id_estado_nuevo = $this->request->data["EjercicioContable"]["id_estado"];
                $id_asiento = 0;
                
                
                if($id_estado_grabado != EnumEstado::Cerrada){
                    
                    $ds = $this->{$this->model}->getdatasource();
                    $ds->begin(); 
                    
                    
                    
                    if ($this->{$this->model}->saveAll($this->request->data)){
                        
                         if($id_estado_nuevo == EnumEstado::Cerrada){
                             
                         	
                            $fecha = data('Y-m-d');
                         	
                         	
                            $this->PeriodoContable->updateAll(array('PeriodoContable.disponible' =>  0,'PeriodoContable.cerrado' =>  1,'PeriodoContable.fecha_cierre' => $fecha),
                         			array( 'PeriodoContable.id_ejercicio_contable'=>$id));
                         	
                            
                            $this->EjercicioContable->updateAll(array('EjercicioContable.activo' =>  0),
                            		array( 'EjercicioContable.id'=>$id));//se actulizan los demas con 0 y queda activo el seleccionado
                         	/*
                             $id_asiento = $this->cerraryAbrirEjercicio($id);
                             
                             if($id_asiento != 0){
                                 
                                 
                                 $status = EnumError::SUCCESS;
                                 $mensaje =  "El Ejercicio Contable se cerro correctamente y se genero el Asiento de Cierre y Apertura. Debe Activar el nuevo Ejercicio editando el nuevo Ejercicio y presionando Activar.";
                                 $ds->commit();
                             }else{
                                 
                                 $status =    "error";
                                 $mensaje =  "El Ejercicio Contable no ha podido cerrarse, intente nuevamente";
                                 $ds->rollback();
                                 
                             }
                             */
                         	
                         	$status = EnumError::SUCCESS;
                         	$mensaje =  "El Ejercicio Contable se cerro correctamente. Debe Activar el nuevo Ejercicio editando el nuevo Ejercicio y presionando Activar.";
                         	$ds->commit();
                         	
                         }else{
                             
                             
                             
                             
                             $ds->commit();
                             
                             $mensaje = "";
                             
                            
                            /*
                             if($this->request->data["EjercicioContable"]["activo"] == 1)
                                $this->EjercicioContable->updateAll(array('EjercicioContable.activo' =>  0),
                             array('NOT' => array('EjercicioContable.id'=>$id)));//se actulizan los demas con 0 y queda activo el seleccionado
                              */
                             
                           
                             $periodo_actual =  $this->PeriodoContable->getPeriodoActual();//busco el periodo actual
                             
                             if($periodo_actual == 0){
                                 $mensaje = ". Se debe definir al menos un peri&oacute;do actual disponible y abierto. Recuerde que el peri&oacute;do actual debe estar dentro del rango de la fecha corriente.";
                             }
                             
                             
                              $mensaje =  "El EjercicioContable ha sido modificada exitosamente".$mensaje;
                              $status = EnumError::SUCCESS;
                         
                             
                         }
                        
                        
                    }else{   //si hubo error recupero los errores de los models
                        
                        $errores = $this->{$this->model}->validationErrors;
                        $errores_string = "";
                        foreach ($errores as $error){ //recorro los errores y armo el mensaje
                            $errores_string.= "&bull; ".$error[0]."\n";
                            
                        }
                        $mensaje = $errores_string;
                        $status = EnumError::ERROR; 
                   }
                }else{
                    
                        $mensaje = "El Ejercicio Contable no se puede modificar ya que se encuentra cerrado";
                        $status = EnumError::ERROR;
                    
                }   
               
               if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => "",
                            "id_asiento"=>$id_asiento
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content","id_asiento"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, el EjercicioContable no ha podido modificarse.', 'error');
                
            }
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            //$this->{$this->model}->contain('Provincia','Pais');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }

    /**
    * @secured(BAJA_EJERCICIO_CONTABLE,READONLY_PROTECTED)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->EjercicioContable->id = $id;
     
       try{
            if ($this->EjercicioContable->delete() ) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "El EjercicioContable ha sido eliminada exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }
    
    
    
    
	
	public function getModel($vista='default')
	{    
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    }
    
    
    
    private function editforView($model,$vista)
	{  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);    
        
    }
    
    
    private function crearPeriodos($id_ejercicio_contable){
        
      $this->loadModel("PeriodoContable");
      $this->loadModel("EjercicioContable");
      $meses = array(1,2,3,4,5,6,7,8,9,10,11,12);
      $periodos_a_grabar = array();
      
      
      $ejercicio_contable = $this->EjercicioContable->find('first', array(
                    'conditions' =>array("EjercicioContable.id" =>$id_ejercicio_contable)
                )
                );
                
       $mes_fecha_apertura = substr($ejercicio_contable["EjercicioContable"]["fecha_apertura"],5,2);//obtengo el mes de la fecha
                
       foreach($meses as $mes){
           
      
           $periodo = array();
           $year = $this->getYearEjercicioPorMesYEjercicio($ejercicio_contable,$mes);
           $periodo["fecha_hasta"] = $this->_data_last_month_day($mes,$year);
           $periodo["fecha_desde"] = $this->_data_first_month_day($mes,$year);
           $periodo["id_ejercicio_contable"] = $id_ejercicio_contable;
           $periodo["cerrado"] = 0;
           $periodo["disponible"] = 0;
           $periodo["nro_periodo"] = 12 - $this->getDiffInMonths($year.'-'.$mes.'-01',$ejercicio_contable["EjercicioContable"]["fecha_cierre"]);
           $periodo["desc_periodo"] = $this->nombremes($mes).' - '.$this->getYearEjercicioPorMesYEjercicio($ejercicio_contable,$mes);
           $periodo["d_periodo_contable"] = "Periodo Nro ".$periodo["nro_periodo"]." ".$ejercicio_contable["EjercicioContable"]["d_ejercicio"];
           array_push($periodos_a_grabar,$periodo);
           
           
       }
       
       $this->PeriodoContable->saveAll($periodos_a_grabar);
        
    } 
    
    
    
    private function getYearEjercicioPorMesYEjercicio($ejercicio_contable,$mes){  //pasandole un ejercicio contable y un mes me retorna el año correspondiente a ese mes
        
       $anio_inicio = substr($ejercicio_contable["EjercicioContable"]["fecha_apertura"],0,4);  //anio inicio
       $anio_fin = substr($ejercicio_contable["EjercicioContable"]["fecha_cierre"],0,4);  //anio fin; 
       
       if ($anio_inicio == $anio_fin)
        $respuesta = $anio_inicio;
       else{
           $mes_anio_inicio = substr($ejercicio_contable["EjercicioContable"]["fecha_apertura"],5,2);
           $mes_anio_fin = substr($ejercicio_contable["EjercicioContable"]["fecha_cierre"],5,2);
           
           if($mes >= $mes_anio_inicio){    
            $respuesta = $anio_inicio;
          } else {
            $respuesta = $anio_fin;
           
         }  
           
       }
       
       return $respuesta;
       
    }
    
    
    protected function cerraryAbrirEjercicio($id_ejercicio){//id_ejercicio a cerrar
        
        
        $this->loadModel("Moneda");
        $this->loadModel("DatoEmpresa");
        
        
        $id_moneda = $this->DatoEmpresa->getIdMonedaCorriente();
        
        $this->BalanceSumasYSaldos(1,0,$id_ejercicio,EnumEstadoAsiento::EnRevision,$id_moneda);
        $sumas_y_saldos = $this->data;
        $id_ejercicio_nuevo = $this->request->data["EjercicioContable"]["id_ejercicio_contable_nuevo"];//le envio el id del ejercicio a abril y en el cual se le hace el asiento de apertura
        
        $this->CrearAsientoAperturaOCierre($id_ejercicio_nuevo,$sumas_y_saldos,$id_moneda,0);//creo el cierre del periodo
        $id_asiento = $this->CrearAsientoAperturaOCierre($id_ejercicio_nuevo,$sumas_y_saldos,$id_moneda,1);//creo la apertura y el resultado del ejercicio
        return $id_asiento;
        
    }
    
    
    public function CrearAsientoAperturaOCierre($id_ejercicio_nuevo,$sumas_y_saldos,$id_moneda,$apertura=1){//se hace en moneda Corriente
        
        $this->loadModel("EjercicioContable");
        $this->loadModel("Moneda");
        $this->loadModel("Asiento");
        $this->loadModel("Contador");
        $this->loadModel("Comprobante");
        $this->loadModel("PeriodoContable");
        
        $cabecera_asiento["Asiento"]["manual"] = 0;
        
        
        $id_contador_ejercicio_contable = $this->EjercicioContable->getContador($id_ejercicio_nuevo);
        $cabecera_asiento["Asiento"]["codigo"] = $this->Contador->getProximoNumero($id_contador_ejercicio_contable);
        
        if($apertura == 1)
            $cabecera_asiento["Asiento"]["d_asiento"] = "Asiento de Apertura para el ejercicio ". $this->EjercicioContable->getDescripcion($id_ejercicio_nuevo);
        else
        $cabecera_asiento["Asiento"]["d_asiento"] = "Asiento de Cierre para el ejercicio ". $this->EjercicioContable->getDescripcion($this->request->data["EjercicioContable"]["id"]);
        
        $cabecera_asiento["Asiento"]["id_estado_asiento"] = EnumEstadoAsiento::EnRevision;
        $cabecera_asiento["Asiento"]["id_clase_asiento"] = EnumClaseAsiento::Apertura;
        $cabecera_asiento["Asiento"]["id_moneda"] = $id_moneda;
        $cabecera_asiento["Asiento"]["fecha"] = date("Y-m-d");
        
        if($apertura == 1){
            
            $cabecera_asiento["Asiento"]["id_agrupacion_asiento"] = EnumAgrupacionAsiento::asientodeapertura;
            $primer_periodo =  $this->EjercicioContable->getPrimerPeriodo($id_ejercicio_nuevo);
            $cabecera_asiento["Asiento"]["id_periodo_contable"] = $primer_periodo;
            $cabecera_asiento["Asiento"]["id_ejercicio_contable"] = $id_ejercicio_nuevo;
        
        }else{
            
            $cabecera_asiento["Asiento"]["id_agrupacion_asiento"] = EnumAgrupacionAsiento::asientodecierre;
            
            $periodo_actual = $this->PeriodoContable->getPeriodoActual();
            $cabecera_asiento["Asiento"]["id_periodo_contable"] = $periodo_actual["id"];
            $cabecera_asiento["Asiento"]["id_ejercicio_contable"] = $this->request->data["EjercicioContable"]["id"];
            
            
        }
        
        
        $cabecera_asiento["Asiento"]["valor_moneda"] = $this->Moneda->getCotizacion($id_moneda);
        
        
        $this->Comprobante->CalcularMonedaBaseSecundaria($cabecera_asiento,"Asiento");
        
        
        
        /*Transformo el sumas y saldos en registros para asiento_cuenta_contable*/
        $cuentas_detalle = array();
        
        $resultado_ejercicio = 0;
        foreach($sumas_y_saldos as $valor){
            
            $valor = $valor["CuentaContable"];//lo bajo un nivel
            
            $asiento_cuenta_contable = array();
            
            if($apertura == 1){
                
                
                
                if($valor["saldo"]<0){
                   
                    $asiento_cuenta_contable["AsientoCuentaContable"]["es_debe"] = 0;
                    $monto = $valor["saldo"]*-1;
                }else{
                    $asiento_cuenta_contable["AsientoCuentaContable"]["es_debe"] = 1;
                    $monto = $valor["saldo"];
                }
            
            }else{
                
                if($valor["saldo"]<0){
                   
                    $asiento_cuenta_contable["AsientoCuentaContable"]["es_debe"] = 1;
                    $monto = $valor["saldo"]*-1;
                }else{
                    $asiento_cuenta_contable["AsientoCuentaContable"]["es_debe"] = 0;
                    $monto = $valor["saldo"];
                }
                
                
            }
            $resultado_ejercicio +=  $valor["saldo"];// el resultado puede ser negativo o positivo
            
                
                
            $asiento_cuenta_contable["AsientoCuentaContable"]["id_cuenta_contable"] = $valor["id"];
            $asiento_cuenta_contable["AsientoCuentaContable"]["monto"] = $monto;
            
            if($valor["id"]>0 && $monto!=0)//solo lo agrego si existe el id_cuenta_contable
                array_push($cuentas_detalle,$asiento_cuenta_contable);
        }
        
        $id_asiento = $this->Asiento->InsertaAsiento($cabecera_asiento,$cuentas_detalle);
        
        if($apertura == 1)//cuando creo el asiento de apertura ahi creo el resultado del ejercicio del ejercicio por cerrar y pongo en 0 los del ejercicio nuevo
            $this->crearAsientoResultadoEjercicio($this->request->data["EjercicioContable"]["id"],$id_moneda,$resultado_ejercicio,$id_ejercicio_nuevo);
        
        return $id_asiento;
             
        

          
    }
    
    
    public function crearAsientoResultadoEjercicio($id_ejercicio_por_cerrar,$id_moneda,$resultado_ejercicio,$id_ejercicio_nuevo){
        
        $this->loadModel("EjercicioContable");
        $this->loadModel("Moneda");
        $this->loadModel("Asiento");
        $this->loadModel("Contador");
        $this->loadModel("Comprobante");
        
        App::uses('CakeSession', 'Model/Datasource');
        $datoEmpresa = CakeSession::read('Empresa');
        $id_cuenta_contable_resultado_ejercicio = $datoEmpresa["DatoEmpresa"]["id_cuenta_contable_resultado_ejercicio"];
        $id_cuenta_contable_resultado_no_asignado = $datoEmpresa["DatoEmpresa"]["id_cuenta_resultado_no_asignado"];
        
        
        
        ////////////////////////////Asiento resultado ejercicio para Ejercicio CONTABLE por cerrar///////////////////////////////////////////////////
        $cabecera_asiento["Asiento"]["manual"] = 0;
        $id_contador_ejercicio_contable = $this->EjercicioContable->getContador($id_ejercicio_por_cerrar);
        $cabecera_asiento["Asiento"]["codigo"] = $this->Contador->getProximoNumero($id_contador_ejercicio_contable);
        $cabecera_asiento["Asiento"]["d_asiento"] = "Asiento de Resultado para el ejercicio ". $this->EjercicioContable->getDescripcion($id_ejercicio_por_cerrar);
        $cabecera_asiento["Asiento"]["id_estado_asiento"] = EnumEstadoAsiento::EnRevision;
        $cabecera_asiento["Asiento"]["id_clase_asiento"] = EnumClaseAsiento::RdosAcumulados;
        $cabecera_asiento["Asiento"]["id_moneda"] = $id_moneda;
        $cabecera_asiento["Asiento"]["fecha"] = date("Y-m-d");
        $cabecera_asiento["Asiento"]["id_agrupacion_asiento"] = EnumAgrupacionAsiento::resultados;
        $primer_periodo =  $this->EjercicioContable->getPrimerPeriodo($id_ejercicio_por_cerrar);
        $cabecera_asiento["Asiento"]["id_periodo_contable"] = $primer_periodo;
        $cabecera_asiento["Asiento"]["id_ejercicio_contable"] = $id_ejercicio_por_cerrar;
        $cabecera_asiento["Asiento"]["valor_moneda"] = $this->Moneda->getCotizacion($id_moneda);
        
         $this->Comprobante->CalcularMonedaBaseSecundaria($cabecera_asiento,"Asiento");
         
         

         
         $cuentas_detalles = array();
         
         if($resultado_ejercicio>0){
            $cuentas_detalle["AsientoCuentaContable"]["es_debe"] = 1;
            $cuentas_detalle["AsientoCuentaContable"]["id_cuenta_contable"] = $id_cuenta_contable_resultado_ejercicio;
            $cuentas_detalle["AsientoCuentaContable"]["monto"] = $resultado_ejercicio;
            
            
            array_push($cuentas_detalles,$cuentas_detalle);
            
            $cuentas_detalle["AsientoCuentaContable"]["es_debe"] = 0;
            $cuentas_detalle["AsientoCuentaContable"]["id_cuenta_contable"] = $id_cuenta_contable_resultado_no_asignado;
            $cuentas_detalle["AsientoCuentaContable"]["monto"] = $resultado_ejercicio;
            
            array_push($cuentas_detalles,$cuentas_detalle);
          
            
         }else{
            
            $cuentas_detalle["AsientoCuentaContable"]["es_debe"] = 0;
            $cuentas_detalle["AsientoCuentaContable"]["id_cuenta_contable"] = $id_cuenta_contable_resultado_ejercicio;
            $cuentas_detalle["AsientoCuentaContable"]["monto"] = $resultado_ejercicio;
            
            
            array_push($cuentas_detalles,$cuentas_detalle);
            
            $cuentas_detalle["AsientoCuentaContable"]["es_debe"] = 1;
            $cuentas_detalle["AsientoCuentaContable"]["id_cuenta_contable"] = $id_cuenta_contable_resultado_no_asignado;
            $cuentas_detalle["AsientoCuentaContable"]["monto"] = $resultado_ejercicio;
            
            array_push($cuentas_detalles,$cuentas_detalle);
         }
         
         
         
         
          $id_asiento = $this->Asiento->InsertaAsiento($cabecera_asiento,$cuentas_detalles);    
        
            
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        $cabecera_asiento["Asiento"]["manual"] = 0;
        $id_contador_ejercicio_contable = $this->EjercicioContable->getContador($id_ejercicio_nuevo);
        $cabecera_asiento["Asiento"]["codigo"] = $this->Contador->getProximoNumero($id_contador_ejercicio_contable);
        $cabecera_asiento["Asiento"]["d_asiento"] = "Asiento de Resultado para el ejercicio ". $this->EjercicioContable->getDescripcion($id_ejercicio_nuevo);
        $cabecera_asiento["Asiento"]["id_estado_asiento"] = EnumEstadoAsiento::EnRevision;
        $cabecera_asiento["Asiento"]["id_clase_asiento"] = EnumClaseAsiento::RdosAcumulados;
        $cabecera_asiento["Asiento"]["id_moneda"] = $id_moneda;
        $cabecera_asiento["Asiento"]["fecha"] = date("Y-m-d");
        $cabecera_asiento["Asiento"]["id_agrupacion_asiento"] = EnumAgrupacionAsiento::resultados;
        $primer_periodo =  $this->EjercicioContable->getPrimerPeriodo($id_ejercicio_nuevo);
        $cabecera_asiento["Asiento"]["id_periodo_contable"] = $primer_periodo;
        $cabecera_asiento["Asiento"]["id_ejercicio_contable"] = $id_ejercicio_nuevo;
        $cabecera_asiento["Asiento"]["valor_moneda"] = $this->Moneda->getCotizacion($id_moneda);
        $this->Comprobante->CalcularMonedaBaseSecundaria($cabecera_asiento,"Asiento");
        

        
        $cuentas_detalle["AsientoCuentaContable"]["es_debe"] = 0;
        $cuentas_detalle["AsientoCuentaContable"]["id_cuenta_contable"] = $id_cuenta_contable_resultado_ejercicio;
        $cuentas_detalle["AsientoCuentaContable"]["monto"] = 0;
        
        
        array_push($cuentas_detalles,$cuentas_detalle);
        
        $cuentas_detalle["AsientoCuentaContable"]["es_debe"] = 1;
        $cuentas_detalle["AsientoCuentaContable"]["id_cuenta_contable"] = $id_cuenta_contable_resultado_no_asignado;
        $cuentas_detalle["AsientoCuentaContable"]["monto"] = 0;
        
        array_push($cuentas_detalles,$cuentas_detalle);
        
        $id_asiento = $this->Asiento->InsertaAsiento($cabecera_asiento,$cuentas_detalles);  
        
        ////////////////////////////Asiento resultado ejercicio para Ejercicio CONTABLE NUEVO va en 0//////////////////////////////////////////////////
        
        return $id_asiento;
        
    }
    
  
    
     

}
?>