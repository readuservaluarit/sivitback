<?php

class SegmentoPicker extends EntityPicker{
    
    public $model = "Segmento";
    
    public $pickerTitle = "Selección de Segmento";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Nombre", "field" => "nombre", "width" => "80%", "keyOrder" => "nombre")
                                );

    public function getPickerConditions($busqueda){
        
        return array('Segmento.nombre LIKE' => '%' . $busqueda['bus_unico'] . '%');
    }
    
    public function getPickerJoins() {
        
        $joins = array();
        
        $join = DataFilter::getSegmentoPickerJoinFilter();
        
        if($join != null)
            array_push($joins, $join);
            
        return $joins;
        
    }

}