SELECT comprobante.`fecha_generacion`, nro_comprobante,cantidad cantidad_pedido,producto.`codigo` codigo_producto,producto.`d_producto` desc_produc

,componenente.codigo componente, componenente.`d_producto` AS desc_comp,producto_relacion.`cantidad_hijo` AS cantidad_unitaria_componente


FROM comprobante_item
JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`
JOIN producto ON producto.id = comprobante_item.`id_producto`
JOIN producto_relacion ON producto.id = producto_relacion.`id_producto_padre`

JOIN producto AS componenente ON componenente.id = producto_relacion.`id_producto_hijo`

WHERE comprobante.`id_tipo_comprobante` =202

AND YEAR(comprobante.`fecha_generacion`)>=2016 AND id_estado_comprobante <>9 LIMIT 100000000000

