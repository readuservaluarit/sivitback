<?php
	// CONFIG  CON MODE DISPLAYED CELLS
	include(APP.'View'.DS.'TiposComprobante'.DS.'json'.DS.'tipo_comprobante.ctp');
	$model["id"]["show_grid"] = 1;
	$model["id"]["header_display"] = "Id";
	$model["id"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["id"]["order"] = "0";
	$model["id"]["text_colour"] = "#000000";
	
	$model["codigo_tipo_comprobante"]["show_grid"] = 1;
	$model["codigo_tipo_comprobante"]["header_display"] = "C&oacute;digo";
	$model["codigo_tipo_comprobante"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["codigo_tipo_comprobante"]["order"] = "1";
	$model["codigo_tipo_comprobante"]["text_colour"] = "#6600AA";
	
	$model["d_tipo_comprobante"]["show_grid"] = 1;
	$model["d_tipo_comprobante"]["header_display"] = "Descripci&oacute;n";
	$model["d_tipo_comprobante"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["d_tipo_comprobante"]["order"] = "2";
	$model["d_tipo_comprobante"]["text_colour"] = "#0000EE";
	
	$model["abreviado_tipo_comprobante"]["show_grid"] = 1;
	$model["abreviado_tipo_comprobante"]["header_display"] = "Abrev.";
	$model["abreviado_tipo_comprobante"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["abreviado_tipo_comprobante"]["order"] = "3";
	$model["abreviado_tipo_comprobante"]["text_colour"] = "#005500";
	
	$model["signo_comercial"]["show_grid"] = 1;
	$model["signo_comercial"]["header_display"] = "Sign Comercial.";
	$model["signo_comercial"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["signo_comercial"]["order"] = "4";
	$model["signo_comercial"]["text_colour"] = "#005500";
	
	$model["signo_contable"]["show_grid"] = 1;
	$model["signo_contable"]["header_display"] = "Sign Contable.";
	$model["signo_contable"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["signo_contable"]["order"] = "5";
	$model["signo_contable"]["text_colour"] = "#005500";
	
	$model["letra"]["show_grid"] = 1;
	$model["letra"]["header_display"] = "Letra";
	$model["letra"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["letra"]["order"] = "6";
	$model["letra"]["text_colour"] = "#005500";
	
	$model["impresion_observacion_extra"]["show_grid"] = 1;
	$model["impresion_observacion_extra"]["header_display"] = "Leyenda Imp.";
	$model["impresion_observacion_extra"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["impresion_observacion_extra"]["order"] = "7";
	$model["impresion_observacion_extra"]["text_colour"] = "#005500";
	
	$model["flag_cargar_codigo_en_item_descripcion"]["show_grid"] = 1;
	$model["flag_cargar_codigo_en_item_descripcion"]["header_display"] = "Flag cod-desc";
	$model["flag_cargar_codigo_en_item_descripcion"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["flag_cargar_codigo_en_item_descripcion"]["order"] = "8";
	$model["flag_cargar_codigo_en_item_descripcion"]["text_colour"] = "#005500";
	
	$model["genera_asiento"]["show_grid"] = 1;
	$model["genera_asiento"]["header_display"] = "Flag Genera Asiento?";
	$model["genera_asiento"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["genera_asiento"]["order"] = "9";
	$model["genera_asiento"]["text_colour"] = "#005500";
	
	$model["id_asiento_modelo"]["show_grid"] = 1;
	$model["id_asiento_modelo"]["header_display"] = "Nro. Asiento Modelo";
	$model["id_asiento_modelo"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["id_asiento_modelo"]["order"] = "10";
	$model["id_asiento_modelo"]["text_colour"] = "#005500";
	
	
	$model["cambia_modelo_asiento_al_ingreso"]["show_grid"] = 1;
	$model["cambia_modelo_asiento_al_ingreso"]["header_display"] = "Flag Cambia Modelo Ingreso";
	$model["cambia_modelo_asiento_al_ingreso"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["cambia_modelo_asiento_al_ingreso"]["order"] = "21";
	$model["cambia_modelo_asiento_al_ingreso"]["text_colour"] = "#005500";
	
	$model["edita_modelo_asiento_al_ingreso"]["show_grid"] = 1;
	$model["edita_modelo_asiento_al_ingreso"]["header_display"] = "Flag Edita Modelo Ingreso";
	$model["edita_modelo_asiento_al_ingreso"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["edita_modelo_asiento_al_ingreso"]["order"] = "22";
	$model["edita_modelo_asiento_al_ingreso"]["text_colour"] = "#005500";
	
	$model["muestra_asiento_al_ingreso"]["show_grid"] = 1;
	$model["muestra_asiento_al_ingreso"]["header_display"] = "Flag Muestra Asiento Ingreso";
	$model["muestra_asiento_al_ingreso"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["muestra_asiento_al_ingreso"]["order"] = "23";
	$model["muestra_asiento_al_ingreso"]["text_colour"] = "#005500";
	
	$model["d_sistema"]["show_grid"] = 1;
	$model["d_sistema"]["type"] = EnumTipoDato::cadena;
	$model["d_sistema"]["header_display"] = "Desc. Sistema";
	$model["d_sistema"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["d_sistema"]["order"] = "24";
	$model["d_sistema"]["text_colour"] = "#005500";
	
	$model["id_tipo_movimiento"]["show_grid"] = 1;
	$model["id_tipo_movimiento"]["type"] = EnumTipoDato::cadena;
	$model["id_tipo_movimiento"]["header_display"] = "Cod. Mov";
	$model["id_tipo_movimiento"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["id_tipo_movimiento"]["order"] = "25";
	$model["id_tipo_movimiento"]["text_colour"] = "#CC0000";
	
	$model["d_movimiento_tipo"]["show_grid"] = 1;
	$model["d_movimiento_tipo"]["type"] = EnumTipoDato::cadena;
	$model["d_movimiento_tipo"]["header_display"] = "Desc Mov.";
	$model["d_movimiento_tipo"]["minimun_width"] = "20"; //REstriccion mas de esto no se puede achicar
	$model["d_movimiento_tipo"]["order"] = "26";
	$model["d_movimiento_tipo"]["text_colour"] = "#BB0033";
	
	 $output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "TipoComprobante",
				"content" => $model
			);
	
	  echo json_encode($output);
?>