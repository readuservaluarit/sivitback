<?php
	class EnumMenu
    {
      const mnuArticulosGeneral =  "mnuArticulosGeneral"; 
	  const mnuUsuarios =  "mnuUsuarios"; 
	  const mnuRoles =  "mnuRoles"; 
	  const mnuProducto =  "mnuProducto"; 
	  const mnuProductoA =  "mnuProductoA"; 
	  const mnuProductoRetail =  "mnuProductoRetail"; 
	  const mnuProductoRetailA =  "mnuProductoRetailA"; 
	  const mnuComponente =  "mnuComponente"; 
	  const mnuComponenteA =  "mnuComponenteA"; 
	  const mnuMateriaPrima =  "mnuMateriaPrima"; 
	  const mnuMateriaPrimaA =  "mnuMateriaPrimaA"; 
	  const mnuPackaging =  "mnuPackaging"; 
	  const mnuPackagingA =  "mnuPackagingA"; 
	  const mnuBienUso =  "mnuBienUso"; 
	  const mnuBienUsoA =  "mnuBienUsoA"; 
	  const mnuInsumoInterno =  "mnuInsumoInterno"; 
	  const mnuInsumoInternoA =  "mnuInsumoInternoA"; 
	  const mnuServicios =  "mnuServicios"; 
	  const mnuServicioA =  "mnuServicioA"; 
	  const mnuCategoria =  "mnuCategoria"; 
	  const mnuFamiliaProductos =  "mnuFamiliaProductos"; 
	  const mnuUnidades =  "mnuUnidades"; 
	  const mnuMarca =  "mnuMarca"; 
	  const mnuQaAccionCorrectiva =  "mnuQaAccionCorrectiva"; 
	  const mnuQaConformidad =  "mnuQaConformidad"; 
	  const mnuQaCertificadoCalidad =  "mnuQaCertificadoCalidad"; 
	  const mnuReclamo =  "mnuReclamo"; 
	  const mnuDescuentoPorFamilia =  "mnuDescuentoPorFamilia"; 
	  const mnuClienteCategoria =  "mnuClienteCategoria"; 
	  const mnuCliente =  "mnuCliente"; 
	  const mnuContacto =  "mnuContacto"; 
	  const mnuTurCliente2 =  "mnuTurCliente2"; 
	  

	  
	  const mnuListaPrecioPicker =  "mnuListaPrecioPicker";//Invisible
	  
	  const mnuCobro =  "mnuCobro"; 
	  const mnuCuentaCorrienteCliente =  "mnuCuentaCorrienteCliente"; 
	  const mnuComprobantesImpagosAgrupado =  "mnuComprobantesImpagosAgrupado"; 
	  const mnuRecibo =  "mnuRecibo"; 
	  const mnuReciboMD =  "mnuReciboMD"; 

	  /*flujos de Factura*/
      const mnuFactura = "mnuFactura"; //Listado de Factura  // form_namespace: CarpeDiem.App.frmFactura 		 controller: Facturas
	  
	  const mnuFacturaMDLibreNoRegistradoConsumidorFinal = "mnuFacturaMDLibreNoRegistradoConsumidorFinal";
            
      const mnuFacturaMDLibre = "mnuFacturaMDLibre";  // form_namespace: CarpeDiem.App.frmFacturaMD 		 controller: Facturas
      const mnuFacturaMD_PI = "mnuFacturaMD_PI";  // form_namespace: CarpeDiem.App.frmFacturaMD 		 controller: Facturas
            
      const mnuFacturaMDProformaLibre = "mnuFacturaMDProformaLibre"; //libre
      const mnuFacturaMDProforma_PI = "mnuFacturaMDProforma_PI";
            
      const mnuFacturaMDOfflineLibre = "mnuFacturaMDOfflineLibre"; //libre
      const mnuFacturaMDOffline_PI = "mnuFacturaMDOffline_PI";
            
      const mnuFacturaMDManualExportacionLibre = "mnuFacturaMDManualExportacionLibre"; //libre
      const mnuFacturaMDManualExportacion_PI = "mnuFacturaMDManualExportacion_PI";
            
            
      const mnuFacturaRemitoMD_PI = "mnuFacturaRemitoMD_PI";
	  /**/
	  
	  const mnuRemito =  "mnuRemito"; 
	  const mnuRemitoMD =  "mnuRemitoMD"; 
	  const mnuRemitoMD_PI =  "mnuRemitoMD_PI"; 
	  
	  const mnuMantenimientoMD =  "mnuMantenimientoMD"; 
	  const mnuMantenimiento =  "mnuMantenimiento"; 
	  const mnuMantenimientoR =  "mnuMantenimientoR"; 
	  
	  const mnuNota =  "mnuNota"; 
	  const mnuNotaMDC =  "mnuNotaMDC"; 
	  const mnuNotaMDC_FC =  "mnuNotaMDC_FC"; 
	  const mnuNotaMDD =  "mnuNotaMDD"; 
	  const mnuPedidoInterno =  "mnuPedidoInterno"; 
	  const mnuPedidoInternoMD =  "mnuPedidoInternoMD"; 
	  const mnuPedidoInternoItem =  "mnuPedidoInternoItem"; 
	  const mnuReportePedidoInterno =  "mnuReportePedidoInterno"; 
	  const mnuCotizacionListado =  "mnuCotizacionListado"; 
	  const mnuCotizacionMD =  "mnuCotizacionMD"; 
	  const mnuSaldosInicioListado =  "mnuSaldosInicioListado"; 
	  const mnuSaldosInicialesClienteMD =  "mnuSaldosInicialesClienteMD"; 
	  const mnuPedidoInternoCalendario =  "mnuPedidoInternoCalendario"; 
	  const mnuTransportista =  "mnuTransportista"; 
	  const mnuLibroIvaVenta =  "mnuLibroIvaVenta"; 
	  const mnuLibroVentaPorJurisdiccion =  "mnuLibroVentaPorJurisdiccion"; 
	  const mnuImpuestosComprobante =  "mnuImpuestosComprobante"; 
	  const mnuPadronArba =  "mnuPadronArba"; 
	  const mnuPadronAgip =  "mnuPadronAgip"; 
	  const mnuTipoImpuestoVentas =  "mnuTipoImpuestoVentas"; 
	  const mnuSistema =  "mnuSistema"; 
	  const mnuImpuestoVentas =  "mnuImpuestoVentas"; 
	  const mnuDepositoListado =  "mnuDepositoListado"; 
	  const mnuDepositoMD =  "mnuDepositoMD"; 
	  const mnuDepositoCategorias =  "mnuDepositoCategorias"; 
	  const mnuDepositoCategoriasListado =  "mnuDepositoCategoriasListado"; 
	  const mnuMovimiento =  "mnuMovimiento"; 
	  const mnuStockProductos =  "mnuStockProductos"; 
	  const mnuStockProductosRetail =  "mnuStockProductosRetail"; 
	  const mnuStockMateriaPrimas =  "mnuStockMateriaPrimas"; 
	  const mnuStockComponentes =  "mnuStockComponentes"; 
	  const mnuStockPackaging =  "mnuStockPackaging"; 
	  const mnuStockBienesUso =  "mnuStockBienesUso"; 
	  const mnuStockInsumosInternos =  "mnuStockInsumosInternos"; 
	  const mnuProveedores =  "mnuProveedores"; 
	  const mnuProveedorCategoria =  "mnuProveedorCategoria"; 
	  const mnuProveedorA =  "mnuProveedorA"; 
	  const mnuCuentaCorrienteProveedor =  "mnuCuentaCorrienteProveedor"; 
	  const mnuDeudaTotal =  "mnuDeudaTotal"; 
	  const mnuOrdenPago =  "mnuOrdenPago"; 
	  const mnuOrdenPagoMD =  "mnuOrdenPagoMD"; 
	  const mnuRemitoCompra =  "mnuRemitoCompra"; 
	  const mnuRemitoCompraMDOC =  "mnuRemitoCompraMDOC"; 
	  const mnuRemitoCompraMD =  "mnuRemitoCompraMD"; 
	  const mnuFacturaCompraL =  "mnuFacturaCompraL"; 
	  const mnuFacturaCompraMD =  "mnuFacturaCompraMD"; 
	  const mnuFacturaCompraMD_OC =  "mnuFacturaCompraMD_OC"; 
	  const mnuNotaCompraListado =  "mnuNotaCompraListado"; 
	  const mnuNotaCompra_NCM =  "mnuNotaCompra_NCM"; 
	  const mnuNotaCompra_NC_FC =  "mnuNotaCompra_NC_FC"; 
	  const mnuNotaCompra_ND =  "mnuNotaCompra_ND"; 
	  const mnuSaldosInicioListadoProveedor =  "mnuSaldosInicioListadoProveedor"; 
	  const mnuSaldosInicialesProveedorMD =  "mnuSaldosInicialesProveedorMD"; 
	  const mnuOrdenDeCompra =  "mnuOrdenDeCompra"; 
	  const mnuOrdenDeCompraMD =  "mnuOrdenDeCompraMD"; 
	  const mnuOrdenDeCompraItem =  "mnuOrdenDeCompraItem"; 
	  const mnuRetencion =  "mnuRetencion"; 
	  const mnuLibroIvaCompra =  "mnuLibroIvaCompra"; 
	  const mnuComprobanteImpuesto =  "mnuComprobanteImpuesto"; 
	  const mnuTipoImpuestoCompras =  "mnuTipoImpuestoCompras"; 
	  const mnuImpuestoCompras =  "mnuImpuestoCompras"; 
	  const mnuOrdenArmado =  "mnuOrdenArmado"; 
	  const mnuOrdenProduccion =  "mnuOrdenProduccion"; 
	  const mnuOrdenTrabajo =  "mnuOrdenTrabajo"; 
	  
	  const mnuLote =  "mnuLote"; 
	  const mnuLotePicker =  "mnuLotePicker"; 
	  const mnuMateriaPrimaBase =  "mnuMateriaPrimaBase"; 
	  const mnuEquipo =  "mnuEquipo"; 
	  const mnuNecesidadMecanizado =  "mnuNecesidadMecanizado"; 
	  const mnuProductoPorComponente =  "mnuProductoPorComponente"; 
	  const mnuEmpleado =  "mnuEmpleado"; 
	  const mnuEmpleadoGeneral =  "mnuEmpleadoGeneral"; 
	  const mnuTiposComprobantes =  "mnuTiposComprobantes"; 
	  const mnuEstado =  "mnuEstado"; 
	  const mnuIva =  "mnuIva"; 
	  const mnuTipoIva =  "mnuTipoIva"; 
	  const mnuArea =  "mnuArea"; 
	  const mnuTransporteCategoria =  "mnuTransporteCategoria"; 
	  const mnuFormasPago =  "mnuFormasPago"; 
	  const mnuTiposIva =  "mnuTiposIva"; 
	  const mnuDatosEmpresa =  "mnuDatosEmpresa"; 
	  const mnuComprobantePuntoVentaNumero =  "mnuComprobantePuntoVentaNumero"; 
	  const mnuPuntoDeVenta =  "mnuPuntoDeVenta"; 
	  const mnuAdministracion =  "mnuAdministracion"; 
	  const mnuPeriodoImpositivo =  "mnuPeriodoImpositivo"; 
	  const mnuPais =  "mnuPais"; 
	  const mnuProvincia =  "mnuProvincia"; 
	  const mnuAsientoMD =  "mnuAsientoMD"; 
	  const mnuLibroDiario =  "mnuLibroDiario"; 
	  const mnuMayorCuenta =  "mnuMayorCuenta"; 
	  const mnuBalanceGeneral =  "mnuBalanceGeneral"; 
	  const mnuBalanceSumaYSaldo =  "mnuBalanceSumaYSaldo"; 
	  const mnuAsientoComprobante =  "mnuAsientoComprobante"; 
	  const mnuAsientoModelo =  "mnuAsientoModelo"; 
	  const mnuTipoAsiento =  "mnuTipoAsiento"; 
	  const mnuLeyendaTipoAsientoLeyenda =  "mnuLeyendaTipoAsientoLeyenda"; 
	  const mnuGrupoAsiento =  "mnuGrupoAsiento"; 
	  const mnuAsientoEfecto =  "mnuAsientoEfecto"; 
	  const mnuCuentaContable =  "mnuCuentaContable"; 
	  const mnuCuentaContablePicker =  "mnuCuentaContablePicker"; 
	  const mnuCentroCosto =  "mnuCentroCosto"; 
	  const mnuEjercicio =  "mnuEjercicio"; 
	  const mnuPeriodoContable =  "mnuPeriodoContable"; 
	  const mnuMovimientosbancarios =  "mnuMovimientosbancarios"; 
	  const mnuMovimientosbancarios_CB =  "mnuMovimientosbancarios_CB"; 
	  const mnuMovimientosbancarios_BC =  "mnuMovimientosbancarios_BC"; 
	  const mnuMovimientosbancarios_BB =  "mnuMovimientosbancarios_BB"; 
	  const mnuMovimientosbancarios_CC =  "mnuMovimientosbancarios_CC"; 
	  const mnuMovimientoCajaListado =  "mnuMovimientoCajaListado"; 
	  const mnuMovimientoCajaMD =  "mnuMovimientoCajaMD"; 
	  const mnuMovimientoCaja_Egreso =  "mnuMovimientoCaja_Egreso"; 
	  const mnuMovimientoCajaListadoConceptoIngreso =  "mnuMovimientoCajaListadoConceptoIngreso"; 
	  const mnuMovimientoCajaListadoConceptoEgreso =  "mnuMovimientoCajaListadoConceptoEgreso"; 
	  const mnuMoneda =  "mnuMoneda"; 
	  const mnuBanco =  "mnuBanco"; 
	  const mnuCuentaBancaria =  "mnuCuentaBancaria"; 
	  const mnuChequera =  "mnuChequera"; 
	  const mnuCheques =  "mnuCheques"; 
	  const mnuChequesPropios =  "mnuChequesPropios"; 
	  const mnuChequesTerceros =  "mnuChequesTerceros"; 
	  const mnuChequesAlta =  "mnuChequesAlta"; 
	  const mnuChequesRechazados =  "mnuChequesRechazados"; 
	  const mnuMovimientoCartera =  "mnuMovimientoCartera"; 
	  const mnuCarteraRendicion =  "mnuCarteraRendicion"; 
	  const mnuGastosListado =  "mnuGastosListado"; 
	  const mnuGastosMD =  "mnuGastosMD"; 
	  const mnuOrdenPagoRendicionGasto =  "mnuOrdenPagoRendicionGasto"; 
	  const mnuOrdenPagoRendicionGastoMD =  "mnuOrdenPagoRendicionGastoMD"; 
	  const mnuGastoImpagoTotalizado =  "mnuGastoImpagoTotalizado"; 
	  const mnuListaPrecioVenta = "mnuListaPrecioVenta";
    }  
?>