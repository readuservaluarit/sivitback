<?php

class UsuariosController extends AppController {
    
    
    
    
    public $name = 'Usuarios';
    public $model = 'Usuario';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler','Paginator');
  
    /**
    * @secured(ABM_USUARIOS)
    */
    public function index() {
        
       
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $username = $this->getFromRequestOrSession('Usuario.username');
		$nombre = $this->getFromRequestOrSession('Usuario.nombre');
		
		$activo = $this->getFromRequestOrSession('Usuario.activo');
		
		$id_rol = $this->getFromRequestOrSession('Usuario.id_rol');
		
		$not_id_rol = $this->getFromRequestOrSession('Usuario.not_id_rol');
        
        $conditions = array(); 

        if($username!="")
            array_push($conditions, array('Usuario.username LIKE' => '%' . $username  . '%')); 
             
        if($nombre!="")
            array_push($conditions, array('Usuario.nombre LIKE' => '%' . $nombre  . '%'));
		
		if($activo!="")
            array_push($conditions, array('Usuario.activo =' =>  $activo )); 
		
		if($id_rol!="")
            array_push($conditions, array('Usuario.id_rol =' =>  $id_rol )); 
		
		if($not_id_rol!="")
            array_push($conditions, array('Usuario.id_rol !=' =>  $not_id_rol )); 
		
        
        $this->paginate = array(
            'paginado'=>0,
            'conditions' => $conditions,
            'limit' => 10,
            'page' => $this->getPageNum()
        );
      
        if($this->RequestHandler->ext != 'json'){

            App::import('Lib', 'FormBuilder');
            $formBuilder = new FormBuilder();
            $formBuilder->setDataListado($this, 'Listado de Usuarios', 'Datos de los Usuarios', $this->model, $this->name, $this->Paginator->paginate($this->model));

            //Filters
            $formBuilder->addFilterBeginRow();
            //$formBuilder->addFilterInput('codigo', 'C&oacute;digo', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $codigo));
            $formBuilder->addFilterInput('nombre', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $nombre));
            $formBuilder->addFilterEndRow();
            
            //Headers
            $formBuilder->addHeader('Id', 'usuario.id', 10);
            $formBuilder->addHeader('Codigo', 'usuario.username', "20%");
            $formBuilder->addHeader('Nombre', 'nombre', "40%");
            $formBuilder->addHeader('Rol', 'Rol.codigo', "30%");

            //Fields
            $formBuilder->addField($this->model, 'id');
            $formBuilder->addField($this->model, 'username');
            $formBuilder->addField($this->model, 'nombre');
            $formBuilder->addField('Rol', 'codigo');
            
            $formBuilder->setFiltroUnicoLabel('CÃ³digo / Nombre');
         
            $this->set('abm',$formBuilder);
            $this->render('/FormBuilder/index');
            
        }else{
           $this->PaginatorModificado->settings = $this->paginate; 
           
           
           $data = $this->PaginatorModificado->paginate($this->model);
           
           foreach($data as &$dato){
               
               $dato["Usuario"]["d_rol"] = $dato["Rol"]["codigo"];
               unset($dato["Usuario"]["password"]);
               unset($dato["Rol"]);
               unset($dato["PuntoVenta"]);
               
           }
           
           $page_count = $this->params['paging'][$this->model]['pageCount'];   
            $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
            
        }   
    }
    
    /**
    * @secured(ABM_USUARIOS)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        $id_segmento_temporal=0;
        if ($mode != "A"){
            $this->Usuario->id = $id;
            
            $this->Usuario->contain(array('Rol'));
            
            
            $this->request->data = $this->Usuario->read();
            
            $rol = $this->request->data['Rol']['id'];
           
            
        } else {
            $rol = '';
            $jurisdiccion = '';
            $d_jurisdiccion = '';
            $region = '';
            $d_region = '';
            $lote = '';
            $d_lote = '';
            $segmento = '';
            $d_segmento = '';
            $segmento_id_temp=0;
        }
        
         App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Usuarios');
       
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Usuario');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Usuario');
        else
            $formBuilder->setTituloForm('Consulta de Usuario');
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal multicolumn well span9'));
      
        //Agrego Tabs
        $formBuilder->addFormTab('tab-general', 'General');
         //obtengo el rol del usuario logueado
        $codigo_rol=$this->Session->read("Auth.User.Rol.codigo");
        
        if($codigo_rol=="COORDINADOR JURISDICCIONAL" || $codigo_rol=="SUPERVISOR" ) { }
        //TAB General  
        $formBuilder->addFormBeginTab('tab-general');
        $formBuilder->addFormBeginFieldset('fs-general', 'General');
        $formBuilder->addFormBeginRow();
         
        //Fields
        $formBuilder->addFormHtml("<div></div>");
        $formBuilder->addFormInput('username', 'Usuario', array('class'=>'control-group span5'), array('class' => '', 'label' => false));
        if ($mode == "A")
            $formBuilder->addFormInput('password', 'Contrase&ntilde;a', array('class'=>'control-group span5'), array('class' => 'required', 'label' => false));
        else
            $formBuilder->addFormHidden('password');
        
        $formBuilder->addFormInput('nombre', 'Apellido y Nombres', array('class'=>'control-group span5'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('email', 'E-Mail', array('class'=>'control-group span5'), array('class' => 'required', 'label' => false));
        
        $roles = $this->Usuario->Rol->find('list', array('fields' => array('Rol.id', 'Rol.descripcion')));
        $formBuilder->addFormInput('id_rol', 'Rol', array('class'=>'control-group span5'), array('class' => 'required', 'label' => false, 'options' => $roles, 'empty' => true, 'default' => $rol));
        $formBuilder->addFormInput('activo', 'Activo', array('class'=>'control-group span5'), array('class' => 'control-group', 'label' => false, 'type' => 'checkbox'));  
       
       
        
            
        //Hidden que guarda el codigo de rol (para filtrar los usuarios asociados)
        $formBuilder->addFormHidden('hid_codigo_rol');
        $formBuilder->addFormHidden('hid_nombre_rol');
        $formBuilder->addFormHidden('hid_codigo_segmento');
         
        
             
        $script = " var enum_censista=".EnumRol::Censista.";
                    var enum_coord_jur=".EnumRol::CoordinadorJurisdiccional.";
                    var enum_supervisor=".EnumRol::Supervisor.";
        
                
                
                function ocultoTab(){
                
                     //habilito o deshabilito Tab asociaciones
                    var codigo_rol=$('#UsuarioIdRol option:selected').html();
                    var codigo_rol_numerico=$('#UsuarioIdRol').val();
                    hideTab('tab-asignacionsegmento-link');
                    hideTab('tab-asignacionpredio-link');
                  
                  
                   if(codigo_rol_numerico==enum_censista) {
                     showTab('tab-asignacionsegmento-link');
                     showTab('tab-asignacionpredio-link');
                    
                     if($('#UsuarioIdLote').val()==''){
                        
                        $('#fs-asignacionsegmento-legend').html('No hay Lote Asignado');
                        $('#aseg_listado').hide();
                     
                     }else {
                        $('#fs-asignacionsegmento-legend').html('Asignacion Segmentos');
                        $('#aseg_listado').show();
                     
                     
                     }
                   }
                  
                    
                    if(codigo_rol_numerico==enum_coord_jur) {
                    
                        $('#UsuarioHidNombreRol').val('SUPERVISOR');
                        $('#UsuarioHidCodigoRol').val(enum_supervisor);
                        
                        showTab('tab-asociaciones-link');
                        hideTab('tab-asignacionlote-link');
                       
                        
                    }else if (codigo_rol_numerico==enum_supervisor ) {
                        
                        $('#UsuarioHidNombreRol').val('CENSISTA');
                        $('#UsuarioHidCodigoRol').val(enum_censista);
                       
                        showTab('tab-asociaciones-link');
                        
                        if( $('#UsuarioIdJurisdiccion').val()!=''){
                            showTab('tab-asignacionlote-link');
                         }
                    
                    }
                    else{
                      
                         hideTab('tab-asociaciones-link');
                         hideTab('tab-asignacionlote-link');
                    }
                        
        
        }
            
            $(function() {
                $('#UsuarioIdRol').change(function() {
                    // Deshabilito los botones del form
                    $('form button').attr('disabled',true);
                    ocultoTab();
                    
                    
                    //Oculto todos los campos para que no haga pantalleo
                    $('#UsuarioDJurisdiccion').parent().parent().parent().hide();
                    $('#UsuarioDRegion').parent().parent().parent().hide();
                    $('#UsuarioDLote').parent().parent().parent().hide();
                    $('#UsuarioDSegmento').parent().parent().parent().hide();
                    
                    $.ajax({
                        'url': './".$this->name."/ajax_rol_info',
                        'data': $(this).serialize(),
                        'dataType': 'json',
                        'success': function(data) {
                            var asigGeo = data ? data.Rol.asignacion_geografica : 0;
                            
                            if (asigGeo >= 2) {
                                $('#UsuarioDJurisdiccion').parent().parent().parent().show()
                            } else {
                                $('#UsuarioDJurisdiccion').parent().parent().parent().hide()
                                $('#UsuarioIdJurisdiccion').val('');
                                $('#UsuarioDJurisdiccion').val('');
                            }
                            if (asigGeo >= 3) {
                                $('#UsuarioDRegion').parent().parent().parent().show()
                            } else {
                                $('#UsuarioDRegion').parent().parent().parent().hide()
                                $('#UsuarioIdRegion').val('');
                                $('#UsuarioDRegion').val('');
                            }
                            if (asigGeo >= 4) {
                                $('#UsuarioDLote').parent().parent().parent().show()
                            } else {
                                $('#UsuarioDLote').parent().parent().parent().hide()
                                $('#UsuarioIdLote').val('');
                                $('#UsuarioDLote').val('');
                            }
                            if (asigGeo >= 5) {
                                $('#UsuarioDSegmento').parent().parent().parent().show()
                            } else {
                                $('#UsuarioDSegmento').parent().parent().parent().hide()
                                $('#UsuarioIdSegmento').val('');
                                $('#UsuarioDSegmento').val('');
                            }
                            
                            // Habilito los botones del form
                            $('form button').attr('disabled',false);
                        }
                    });
                });
                
                $('#UsuarioIdRol').trigger('change');
            });
            
            ";
        $formBuilder->addFormCustomScript($script);

        
        /*
       /* //Segmento                          
        $selectionList = array($this->model."IdSegmento" => "id", $this->model."DSegmento" => "nombre");
        $formBuilder->addFormPicker('SegmentoPicker', $selectionList, 'id_segmento', 
                                   array('value' => $segmento), 'd_segmento', 'Segmento', 
                                   array('class'=>'control-group span5'), array('class' => '', 'label' => false, 'value' => $d_segmento),
                                   array(),
                                   array('id_lote' => 'UsuarioIdLote')
                                   );
        */
        
       
        if ( $mode == "M" ) {
       
        $formBuilder->addFormInput('password_new', 'Contrase&ntilde;a Nueva', array('class'=>'control-group span10'), array('class' => 'required', 'label' => false, 'type'=>'password'));
        $formBuilder->addFormInput('password_new_confirm', 'Confirmar Contrase&ntilde;a Nueva', array('class'=>'control-group span10'), array('class' => 'required', 'label' => false, 'type'=>'password'));
            
            
        //Fortaleza
        $formBuilder->addFormHtml("<div id='fortaleza' class='control-group span10'>
                                     <label class='control-label' for='progressBar'>Fortaleza</label>
                                        <div class='controls' style = 'border:1px solid rgb(204, 204, 204);height:20px;width:220px;border-radius:5px;'>
                                            <div class='progress progress-striped'>
                                                <div id='progressBar' class='progress-bar progress-bar-success' role='progressbar' aria-valuenow='40' aria-valuemin='0' aria-valuemax='100' style='width: 0%; max-width:220px;'>
                                                    <span id='progressBarMsg' class='sr-only'>40% Complete (success)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>");
        $formBuilder->addFormHtml("<a  style='margin-bottom:20px;float:left;cursor:pointer;' id='CambiarPassword'  >Cambiar Constrase&ntilde;a</a>");
        $formBuilder->addFormHtml("<div></div>"); 
        
        $script = "
                    
                    $( '#CambiarPassword' ).click(function() {
                    
                         jQuery(this).hide();
                         //jQuery('#UsuarioPassword').parent().parent().show();
                         jQuery('#UsuarioPasswordNew').parent().parent().show();
                         jQuery('#UsuarioPasswordNewConfirm').parent().parent().show();    
                         jQuery('#fortaleza').show();   
                        });
                        
                        $( document ).ready(function () {
                         //jQuery('#UsuarioPassword').parent().parent().hide();
                         jQuery('#UsuarioPasswordNew').parent().parent().hide();
                         jQuery('#UsuarioPasswordNewConfirm').parent().parent().hide(); 
                         jQuery('#fortaleza').hide();
                         jQuery('#btnLimpiar').hide();
                        
                        });
            
                        $( '#UsuarioPasswordNew' ).keyup(function() {
                            
                            var desc = new Array();
                            desc[0] = ' Muy D&eacute;bil ';
                            desc[1] = ' D&eacute;bil ';
                            desc[2] = ' Aceptable ';
                            desc[3] = ' Buena ';
                            desc[4] = ' Fuerte ';
                            desc[5] = ' Muy Fuerte ';
                            var classCss = 'sucess';
                            var width = '100%';
                            
                             var pass_strong = passwordStrength(jQuery(this).val());
                             switch(pass_strong) {
                                case 0:  classCss = 'danger';
                                         width = '30%';
                                    
                                    break;
                                case 1: classCss = 'danger';
                                        width = '30%';
                                    break;
                                case 2: classCss = 'warning';
                                        width = '40%';
                                    break;
                                case 3: classCss = 'warning';
                                         width = '60%';
                                    break;
                                case 4: classCss = 'sucess';
                                         width = '80%';
                                    break;
                                case 5: classCss = 'sucess';
                                        width = '100%';
                                    break;            
                             }                                          
                             jQuery('#progressBar').css('width',width);
                             jQuery('#progressBarMsg').html(desc[pass_strong] + '  ' + width );
                             var className = jQuery('#progressBar').attr('class');
                             jQuery( '#progressBar' ).removeClass( className );
                             className = 'progress-bar progress-bar-' + classCss;
                             jQuery( '#progressBar' ).addClass( className );
                             
                        });
                        
                        function passwordStrength(password)
                        {
                           

                            var score   = 0;

                            //if password bigger than 6 give 1 point
                            if (password.length > 6) score++;

                            //if password has both lower and uppercase characters give 1 point    
                            if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) score++;

                            //if password has at least one number give 1 point
                            if (password.match(/\d+/)) score++;

                            //if password has at least one special caracther give 1 point
                            if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) )    score++;

                            //if password bigger than 12 give another 1 point
                            if (password.length > 12) score++;

                             return score;
                             
                             
                        }";  
            
           
           $formBuilder->addCommonScript($script);
         }
        
        $script = "
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
                
                var form = 'UsuarioAbmForm';
                var validator = new Validator(form);
                
                
                validator.validateRequired('UsuarioUsername', 'Debe ingresar un usuario');
                
                if ($('#UsuarioPassword:visible').length)
                    validator.validateRequired('UsuarioPassword', 'Debe ingresar una contrase&ntilde;a');
                
                if ($('#UsuarioPasswordNew:visible').length) {
                    validator.validateRequired('UsuarioPasswordNew', 'La Constrase&ntilde;a Nueva es requerida');
                    validator.validateRequired('UsuarioPasswordNewConfirm','La confirmaci&oacute;n de la Constrase&ntilde;a Nueva es requerida');   
                      
                    if( $('#UsuarioPasswordNew').val() != $('#UsuarioPasswordNewConfirm').val()  ){
                        validator.addErrorMessage('UsuarioPasswordNewConfirm','Las Constrase&ntilde;as ingresadas no coinciden, por favor verif&iacute;quelo.');
                        validator.showValidations('', 'validationMsg_');
                    } 
                }
                
                validator.validateRequired('UsuarioNombre', 'Debe ingresar un nombre');
                validator.validateRequired('UsuarioEmail', 'Debe ingresar un e-mail', false);
                validator.validateEmail('UsuarioEmail', 'El e-mail es invalido');
                validator.validateRequired('UsuarioIdRol', 'Debe seleccionar un rol');
                
                if ($('#UsuarioDJurisdiccion:visible').length)
                    validator.validateRequired('UsuarioDJurisdiccion', 'Debe seleccionar una jurisdiccion');
                if ($('#UsuarioDRegion:visible').length)
                    validator.validateRequired('UsuarioDRegion', 'Debe seleccionar una region');
                if ($('#UsuarioDLote:visible').length)
                    validator.validateRequired('UsuarioDLote', 'Debe seleccionar un lote');
                if ($('#UsuarioDSegmento:visible').length)
                    validator.validateRequired('UsuarioDSegmento', 'Debe seleccionar un segmento');
                
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }

                return true;
            } 
            function clearForm() {
                $('#UsuarioUsername').val('');
                if ($('#UsuarioPassword:visible').length)
                    $('#UsuarioPassword').val('');
                $('#UsuarioNombre').val('');
                $('#UsuarioEmail').val('');
                $('#UsuarioIdRol').val('');
                $('#UsuarioIdRol').trigger('change');
            }
            ";

        //Para que el cancelar recarge nuevamente la pagina
        $formBuilder->setFormCancelButtonRefreshPage(true);    
        
        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        $formBuilder->addFormEndRow();
        $formBuilder->addFormEndFieldset();
        $formBuilder->addFormEndTab();
        
        
       

       
        
      
         
        //obtengo el rol del usuario logueado
        $codigo_rol=$this->Session->read("Auth.User.Rol.codigo");
        
       
        
        $this->request->data['Usuario']['password_new'] = '';
        $this->request->data['Usuario']['password_new_confirm'] = '';
      
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');
       
        
        
        
       
    }

    /**
    * @secured(ABM_USUARIOS)
    */
    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ABM_USUARIOS, READONLY_PROTECTED)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_add = "";
            
            
            try{
            	
           		 	if($this->chequeoMailRepetido($this->request->data["Usuario"]["email"]) > 0)
            			throw new Exception("El E-Mail que eligi&oacute; ya existe en la base de datos");
 
            
	      
	            	
	            	if($this->chequeoUserNameRepetido($this->request->data["Usuario"]["username"]) > 0)
	            		throw new Exception("El Usuario que eligi&oacute; ya existe en la base de datos");
	            	
		            if ($this->Usuario->saveAll($this->request->data, array('deep' => true))){
		            	
		            	$id_add = $this->Usuario->id;
		            
		            	$error = EnumError::SUCCESS;
		            	$message = "El usuario ha sido creado exitosamente";
		            }else{
		            	
		            	$error = EnumError::ERROR;
		            	$message = "El usuario NO ha podido sido ser creado";
		            	
		            }
	            }catch(Exception $e){
	            	
	            	$error = EnumError::ERROR;
	            	$message = "El usuario NO ha podido sido ser creado"." ".$e->getMessage();
	            }
            
            
		    
            
            
		    if($this->RequestHandler->ext == 'json'){ 
		            	
		    	$output = array(
		    			"status" => $error,
		    			"message" => $message,
		    			"content" => "",
		    			"page_count" =>0,
		    			"id_add"=>$id_add
		    	);
		    	$this->set($output);
		    	$this->set("_serialize", array("status", "message","page_count", "content","id_add"));
		    	
		    }else{
		    	
		    	$this->Session->setFlash($message, $error);
		    	$this->redirect(array('action' => 'index'));
		    	
			            
		   }
        
        }
                     
    }
    
    
    
    
    /**
     * @secured(ADD_SIVIT_CLIENTE)
     */
    public function addSivitClienteUser(){
    	
    	
    	$this->request->data["Usuario"]["id_rol"] = EnumRol::SivitCliente;
    	$this->request->data["Usuario"]["email"] = $this->request->data["Usuario"]["username"];
    	$this->request->data["Usuario"]["activo"] = $this->request->data["Usuario"]["sivit_usuario_activo"];
    	$this->request->data["Usuario"]["nombre"] = $this->request->data["Usuario"]["username"];
    	$this->add();
    	return;
    	
    }
    
    
    /**
     * @secured(ADD_SIVIT_CLIENTE_B2B)
     */
    public function addSivitClienteB2BUser(){
    	
    	
    	$this->request->data["Usuario"]["id_rol"] = EnumRol::SivitB2B;
    	$this->request->data["Usuario"]["email"] = $this->request->data["Usuario"]["username"];
    	$this->request->data["Usuario"]["activo"] = $this->request->data["Usuario"]["sivit_b2b_usuario_activo"];
    	$this->request->data["Usuario"]["nombre"] = $this->request->data["Usuario"]["username"];
    	$this->add();
    	return;
    	
    }
    
    
    
    
    /**
     * @secured(MODIFICACION_SIVIT_CLIENTE)
     */
    public function editSivitClienteUser(){
    	
    	$this->loadModel(EnumModel::Persona);
    	
    	$this->request->data["Usuario"]["id_rol"] = EnumRol::SivitCliente;
    	$this->request->data["Usuario"]["email"] = $this->request->data["Usuario"]["username"];
    	$id_usuario = $this->request->data["Usuario"]["id_usuario"];
    	$id_persona = $this->request->data["Usuario"]["id_persona"];
    	$this->request->data["Usuario"]["id"] = $id_usuario;//le seteo el id asi no hace ADD esto es asi porque el edit viene desde el form de clientes o proveedores
    	$this->request->data["Usuario"]["activo"] = $this->request->data["Usuario"]["sivit_usuario_activo"];
    	
    	
    	$this->request->data['Usuario']['password_new'] = $this->request->data["Usuario"]["password"];
    	
    	
    	
    	$persona = $this->Persona->find('first', array(
    			'conditions' => array('Persona.id' => $id_persona),
    			'contain' =>array('Usuario')
    	));
    	
    	
    	if($persona["Usuario"]["id"] == $id_usuario)
    		$this->edit($id_usuario);
    	else 
    	{
    		
    		$output = array(
    				"status" => EnumError::ERROR,
    				"message" => "No puede actualizar un usuario que no pertenece a la persona",
    				"content" => "",
    				"page_count" =>0
    	
    		);
    		$this->set($output);
    		$this->set("_serialize", array("status", "message","page_count", "content"));
    		
    		
    	}

    	
    }
    
    
    
    /**
     * @secured(MODIFICACION_SIVIT_CLIENTE_B2B)
     */
    public function editSivitClienteB2BUser(){
    	
    	$this->loadModel(EnumModel::Persona);
    	
    	$this->request->data["Usuario"]["id_rol"] = EnumRol::SivitB2B;
    	$this->request->data["Usuario"]["email"] = $this->request->data["Usuario"]["username"];
    	$id_usuario = $this->request->data["Usuario"]["id_usuario"];
    	$id_persona = $this->request->data["Usuario"]["id_persona"];
    	$this->request->data["Usuario"]["id"] = $id_usuario;//le seteo el id asi no hace ADD esto es asi porque el edit viene desde el form de clientes o proveedores
    	$this->request->data["Usuario"]["activo"] = $this->request->data["Usuario"]["sivit_b2b_usuario_activo"];
    	$this->request->data['Usuario']['password_new'] = $this->request->data["Usuario"]["password"];
    	
    	
    	
    	$persona = $this->Persona->find('first', array(
    			'conditions' => array('Persona.id' => $id_persona),
    			'contain' =>array('Usuario')
    	));
    	
    	
    	if($persona["Usuario"]["id"] == $id_usuario)
    		$this->edit($id_usuario);
    		else
    		{
    			
    			$output = array(
    					"status" => EnumError::ERROR,
    					"message" => "No puede actualizar un usuario que no pertenece a la persona",
    					"content" => "",
    					"page_count" =>0
    					
    			);
    			$this->set($output);
    			$this->set("_serialize", array("status", "message","page_count", "content"));
    			
    			
    		}
    		
    		
    }
    
    private function chequeoMailRepetido($mail,$id=0){
    	
    	$this->loadModel(EnumModel::Usuario);
    	
    	$cantidad = $this->Usuario->find('count', array(
    			'conditions' => array('Usuario.email' => $mail,'Usuario.id !=' => $id),
    			'contain' =>array()
    	));
    	
    	return $cantidad;
    	
    	
    	
    }
    
    
    private function chequeoUserNameRepetido($username,$id=0){
    	
    	$this->loadModel(EnumModel::Usuario);
    	
    	$cantidad = $this->Usuario->find('count', array(
    			'conditions' => array('Usuario.username' => $username,'Usuario.id !=' => $id),
    			'contain' =>array()
    	));
    	
    	return $cantidad;
    	
    	
    	
    }
    
    
   
   public function edit($id) {
   	
   	
   			/*Seguridad B2C*/
		   	if($this->Auth->user('id_persona')>0){
		   		
		   		
		   	    $this->request->data['Usuario']['id_persona'] = $id;
		   		$this->request->data['Usuario']['id'] = $this->Auth->user('id');
		   		
		   	}
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->Usuario->id = $id;
            
            unset($this->request->data['Usuario']['cantidad_maxima_sesiones_multiples']);//evito el hack de tener mas sessiones
            unset($this->request->data['Usuario']['hash_sesion']);//evito el hack de tener mas sessiones
            
            
            if( isset($this->request->data['Usuario']['password_new'])  && mb_strlen($this->request->data['Usuario']['password_new'])>0){
               $this->request->data['Usuario']['password'] = AuthComponent::password($this->request->data['Usuario']['password_new']);  
                
            }
            
           
            
             
            try {
            	
            	if($this->chequeoMailRepetido($this->request->data["Usuario"]["email"],$id) > 0)
            		throw new Exception("El E-Mail que eligi&oacute; ya existe en la base de datos");
            		

            	if($this->chequeoUserNameRepetido($this->request->data["Usuario"]["username"],$id) > 0)
            		throw new Exception("El Usuario que eligi&oacute; ya existe en la base de datos");
            	
            		
            
                
            	if ($this->Usuario->saveAll($this->request->data, array('deep' => true))){
            		$error = EnumError::SUCCESS;
            		$message = "El usuario ha sido creado exitosamente";
            
            	}else{
            		
            		$error = EnumError::ERROR;
            		$message = "El usuario NO ha podido sido ser modificado";
            	}
                
            } catch(Exception $e) {
            	
               
                $error = EnumError::ERROR;
                $message = "El usuario NO ha podido sido ser modificado"." ".$e->getMessage();
               
            }
                
           
            
        }
        
        if($this->RequestHandler->ext == 'json'){
        	
        	$output = array(
        			"status" => $error,
        			"message" => $message,
        			"content" => "",
        			"page_count" =>0,
        			"id_add"=>$id_add
        	);
        	$this->set($output);
        	$this->set("_serialize", array("status", "message","page_count", "content","id_add"));
        	
        }else{
        	
        	$this->Session->setFlash($message, $error);
        	$this->redirect(array('action' => 'index'));
        	
        	
        }
        
       
    }
    
  
    /**
    * @secured(ABM_USUARIOS, READONLY_PROTECTED)
    */
    function delete($id) {
        $this->loadModel($this->model);
       try{
            if ($this->Usuario->delete($id)) 
                $this->Session->setFlash('El usuario ha sido eliminado exitosamente.', 'success');
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            $this->Session->setFlash($ex->getMessage(), 'error');
        }
       $this->redirect(array('action' => 'index'));
        
        
    }
    
    /**
    * @secured(ABM_USUARIOS)
    */
    
   
    
    function ajax_rol_info() {
        $this->loadModel("Rol");
        $data = $this->Rol->find('first', array('conditions'=>array('Rol.id'=>$this->params['url']['data']['Usuario']['id_rol']),'contain' => false));
        
        echo json_encode($data);
        die();
    }
    
    
    //se usa por llamada ajax para ver si un usuario secundario ya esta asignado
    function ajax_usuario_secundario() {       
        $this->loadModel("AsociacionUsuario");
        
        //$data = $this->AsociacionUsuario->find('first', array('conditions'=>array('Rol.id'=>$this->params['url']['data']['Usuario']['id_rol']),'contain' => false));
        $count = $this->AsociacionUsuario->find("count", array('conditions' => array('AsociacionUsuario.id_usuario_secundario' =>$this->params->query['data']['AsociacionUsuario']['UsuarioSecundario']['id']) ));
        
        echo $count;
        die();
    }

    
    
    public function login() {

        $this->log("111");      
        
        $this->log($this->request->data);      
       
        $this->layout = 'ajax';
        $this->loadModel('Usuario');
        $this->loadModel('DatoEmpresa');
        $this->loadModel('Modulo');
        $this->loadModel('cake_sessions');
        $this->loadModel(EnumModel::Entidad);
        $this->loadModel(EnumModel::EstadoTipoComprobante);
        $this->loadModel(EnumModel::DepositoMovimientoTipo);
        $data = "";
        $first = 0;
        
        if ($this->request->is('post')) {
            
            
          
            //Traigo el campo activo
            
            if(isset($this->request->data['Usuario']['username']))
                $usuario = $this->Usuario->find('first', array('conditions' => array('Usuario.username' => $this->request->data['Usuario']['username']), 'contain' => array('Rol','PuntoVenta','Persona')));
           elseif(isset($this->request->data['Usuario']['email']))
                $usuario = $this->Usuario->find('first', array('conditions' => array('Usuario.email' => $this->request->data['Usuario']['email']), 'contain' => array('Rol','PuntoVenta','Persona')));
        
               
            
                
           if(isset($usuario['Usuario']['id'])){
                    $activo = isset($usuario['Usuario']['activo'])?$usuario['Usuario']['activo']:0;
                    $id = isset($usuario['Usuario']['id'])?$usuario['Usuario']['id']:0;
                    $cantidad_maxima_sesiones_multiples = isset($usuario['Usuario']['cantidad_maxima_sesiones_multiples'])?$usuario['Usuario']['cantidad_maxima_sesiones_multiples']:0;
                    $id_rol_usuario = isset($usuario['Usuario']['id_rol'])?$usuario['Usuario']['id_rol']:0;
         
                
                
            $cake_session = new cake_sessions(); 
            $datoEmpresa = $this->DatoEmpresa->find('first', array('conditions' => array('DatoEmpresa.id' => 1), 'contain' => array('TipoIva','Provincia','Pais')));
           }
            
            if(isset($usuario['Usuario']['id_persona']) && $usuario['Usuario']['id_persona']>0){//debo chequear si tiene habilitado el portal
            	
            	if($usuario['Persona']['id_tipo_persona'] == EnumTipoPersona::Cliente){
            		
            		if($datoEmpresa['DatoEmpresa']['sivit_clientes_conexion_activa']!= 1)
            			$activo = 0;
            	}elseif($usuario['Persona']['id_tipo_persona'] == EnumTipoPersona::Proveedor){
            		
            		if($datoEmpresa['DatoEmpresa']['sivit_proveedores_conexion_activa']!= 1)
            			$activo = 0;
            	}
            }
            
            
            if( isset($usuario["Usuario"]["id"]) && $usuario["Usuario"]["id"]>0){
            	
            	
                
                
                
                
                
				if($usuario["Usuario"]["intentos_fallidos"] < 3){
							if(isset($this->request->data['Usuario']['hash_sesion']))
								$hash_session = $this->request->data['Usuario']['hash_sesion'];
							else
								$hash_session = "";
							
								if($cake_session->IsUserActive($id,$cantidad_maxima_sesiones_multiples,$id_rol_usuario,$usuario["Usuario"]["username"],$hash_session) == 1 ){ 
					   
									if ($activo == 1 && $this->Auth->login()) {
										$hash = "";
										if(isset($this->request->data['Usuario']['get_hash_sesion'])){
											/*Genero HASH*/
											$hash = session_id();//id unico de session, si o si pertenece a la session de escritorio o a la session web pero no la cromium
											
											$this->Usuario->updateAll(
													array('Usuario.hash_sesion'=>"'".$hash."'"),
													array('Usuario.id' => $usuario["Usuario"]["id"]) );
										}
										
										$this->set('Usuario', $this->Usuario->data);
										
										//busco el datoEMPRESA
										if(!isset($this->request->data['Usuario']['id_dato_empresa']))
											$this->request->data['Usuario']['id_dato_empresa'] = 1; //por default usa la sucursal 1
										
									
										App::import('Model', 'Modulo');
										$modulo = new Modulo();
										
										
										$this->Session->write("Modulo", $modulo->getModulos());
										$this->Session->write("Empresa", $datoEmpresa);
									  
									  //$this->Session->write("Ejercicio", $datoEmpresa);
									  //$navegador = $this->request->data['Usuario']['navegador'];
									  
										$this->saveLogin('');
										$data = array();
										//$this->redirect(array('controller' => 'Usuarios', 'action' => 'home'));
										//json_decode("ok");
										
										
										/*Actualizo los intentos fallidos del usuario*/
										$this->Usuario->updateAll(
												array('Usuario.intentos_fallidos'=> 0),
												array('Usuario.id' => $usuario["Usuario"]["id"]) );
										
										//$this->getCacheEntidades($usuario["Usuario"]["id"]);
										
										
										/*0*/
										if(isset($this->request->data['TieneCache']['MisDatos']['index']) 
											  && $this->request->data['TieneCache']['MisDatos']['index'] == "0") {  //0 = Significa q la terminal (app.net) no tiene cache validad
											
											$mis_datos_index = $this->Usuario->MisDatos($usuario,$this);
											
											
											
											$output = array(
													"status" =>EnumError::SUCCESS,
													"message" => "",
													"content" => $mis_datos_index
									
													
											);
											
											$data[EnumController::MisDatos] = $output;
											
										}
										
									   /*1*/
									   if(isset($this->request->data['TieneCache']['DatoEmpresa']['index']) 
											  && $this->request->data['TieneCache']['DatoEmpresa']['index'] == "0") {  //0 = Significa q la terminal (app.net) no tiene cache validad
									   	
											 $dato_empresa = $this->DatoEmpresa->find('all', array(
																'conditions' => array('DatoEmpresa.id' => 1),
													 'contain'=>array('PuntoVenta'=>array('ComprobantePuntoVentaNumero'),'Pais','Provincia','TipoIva','TipoDocumento',
																'Moneda','ListaPrecio','TipoEmpresa','DatoEmpresaImpuesto','Moneda2','Moneda3')
														));
											 
											 
												
											 
											 /*Esto se hace para que el index de DatoEmpresa con el filtro de id_dato_empresa devuelva exactamente lo mismo que este index consumido desde el login*/
											$index_dato_empresa = $this->DatoEmpresa->index($dato_empresa[0]);
											
											
											
											$output = array(
													"status" =>EnumError::SUCCESS,
													"message" => "list",
													"content" => array($index_dato_empresa),
													"page_count"=>1
													
													
											);
											
											
											
											$data[EnumController::DatosEmpresa] = $output;
											
											
											
										}
									 

									   
									   /*2*/
										if(isset($this->request->data['TieneCache']['GetCacheModel']) 
											  && $this->request->data['TieneCache']['GetCacheModel'] == "0") { //0 = Significa q la terminal (app.net) no tiene cache validad
											
											$mis_datos_cache = $this->Usuario->getCacheModel();//esta en APPModel
											
											$output = array(
													"status" =>EnumError::SUCCESS,
													"message" => "",
													"content" => $mis_datos_cache["content"],
													"cantidad" =>count($mis_datos_cache["content"])
											
											);
											
											
											$data["GetCacheModel"] = $output;
									
										}
										
										/*3*/
										if(isset($this->request->data['TieneCache']['GetCacheStaticIndex'])
												&& $this->request->data['TieneCache']['GetCacheStaticIndex'] == "0") { //0 = Significa q la terminal (app.net) no tiene cache validad
													
													$index_cache = $this->Usuario->getCacheStaticIndex();//estos metodos de getCache estan definidos en el AppModel
													
													
													$output = array(
															"status" =>EnumError::SUCCESS,
															"message" => "",
															"content" => $index_cache,
															"cantidad" =>$this->Entidad->getCountStaticIndex()
															
													);
													
													$data["GetCacheStaticIndex"] = $output;
												
													
										}
										
										/*4*/
										if(isset($this->request->data['TieneCache']['Usuario']['GetCacheMenu'])
												&& $this->request->data['TieneCache']['Usuario']['GetCacheMenu'] == "0") { //0 = Significa q la terminal (app.net) no tiene cache validad
													
													$menu_cache = $this->Usuario->getMenu($this);//estos metodos de getCache estan definidos en el AppModel
													
													
													
													$output = array(
															"status" =>EnumError::SUCCESS,
															"message" => "",
															"content" => $menu_cache
										
															
													);
													
													$data["GetCacheMenu"] = $output;
													
													
										}
										
											
										/*5*/
										if(isset($this->request->data['TieneCache']['Usuario']['GetCachePermisos'])
												&& $this->request->data['TieneCache']['Usuario']['GetCachePermisos'] == "0") { //0 = Significa q la terminal (app.net) no tiene cache validad
													
													$menu_permisos = $this->Usuario->allpermision();//estos metodos de getCache estan definidos en el AppModel
													
													$output = array(
															"status" =>EnumError::SUCCESS,
															"message" => "GetCachePermisos",
															"content" => $menu_permisos,
															"id_rol"=>$usuario['Usuario']['id_rol'],
															"username"=>$usuario['Usuario']['username']
															
															
															
													);
													
													
													$data["GetCachePermisos"] = $output;
										}
										
										
											
													
								
										
										/*6*/
										if(isset($this->request->data['TieneCache']['GetCacheEstadoTipoComprobante'])
												&& $this->request->data['TieneCache']['GetCacheEstadoTipoComprobante'] == "0") { //0 = Significa q la terminal (app.net) no tiene cache validad
													
													$index_etc_cache = $this->EstadoTipoComprobante->getindex(array(),EnumModel::EstadoTipoComprobante);
													
													
													$output = array(
															"status" =>EnumError::SUCCESS,
															"message" => "",
															"content" => $index_etc_cache
															
															
													);
													
													$data["GetCacheEstadoTipoComprobante"] = $output;
								
													
													
										}
										
										
										
										/*7*/
										if(isset($this->request->data['TieneCache']['GetCacheDepositoMovimientoTipo'])
												&& $this->request->data['TieneCache']['GetCacheDepositoMovimientoTipo'] == "0") { //0 = Significa q la terminal (app.net) no tiene cache validad
													
												    
												    $conditions = array();
												    
												    array_push($conditions ,array("Deposito.id_dato_empresa"=>$usuario['Usuario']['id_dato_empresa']));
												    
												    $index_dtc_cache = $this->{EnumModel::DepositoMovimientoTipo}->index($conditions);
													
													
													$output = array(
															"status" =>EnumError::SUCCESS,
															"message" => "",
															"content" => $index_dtc_cache
															
															
													);
													
													$data["GetCacheDepositoMovimientoTipo"] = $output;
													
													
										}
										
										
									 $output = array(
											"status" =>EnumError::SUCCESS,
											"message" => "Logueado correctamente",
											"content" => $data,
											"product_version"=>$datoEmpresa["DatoEmpresa"]["product_version"],
											"hash"=>$hash
										);
										
									} else {	
											if(isset($usuario) && $usuario["Usuario"]["id"]>0){
												
												$this->Usuario->updateAll(
													array('Usuario.intentos_fallidos'=> 'Usuario.intentos_fallidos +1'),
														array('Usuario.id' => $usuario["Usuario"]["id"]) );
											}
											$output = array(
												"status" =>EnumError::ERROR,
												"message" => "Los datos ingresados son inv&aacute;lidos, por favor int&eacute;ntelo nuevamente.",
												"content" => $data,
												"product_version"=>"",
													"hash"=>""
											);
									}
					} else {
						$output = array(
							"status" =>EnumError::ERROR,
							"message" => "Tiene sesiones iniciadas en otros dispositivos o la &uacute;ltima vez que utiliz&oacute; el SIV no cerr&oacute; correctamente la sesion. La cantidad m&aacute;xima de sesiones abiertas disponibles para su usuario es de (".$cantidad_maxima_sesiones_multiples."). Por favor cierre las sesiones abiertas para poder iniciar sesion. Presione 'Cerrar sesi&oacute;nes abiertas'",
							"content" => $data,
							"product_version"=>"",
							"hash"=>""
						);
					}
            	}else{
            		$output = array(
            				"status" =>EnumError::ERROR,
            				"message" => "Llego al m&aacute;ximo de 3 intentos fallidos comuniquese con personal de Valuarit para que blanquee su usuario.",
            				"content" => $data,
            				"product_version"=>"",
            				"hash"=>""
            		);
            	}
            }else{
            	$output = array(
            			"status" =>EnumError::ERROR,
            			"message" => "Verifique los datos.El usuario o la clave son incorrectos",
            			"content" => $data,
            			"product_version"=>"",
            			"hash"=>""
            	);	
            }
        }else{
            //Si es un get y ya esta logueado
            if ($this->Session->check("Auth.User.id")){
                //$this->redirect(array('controller' => 'Usuarios', 'action' => 'home'));
                $data = "";
                $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Ya se encuentra logueado",
                    "content" => $data,
                	"product_version"=>"",
                		"hash"=>""
                );
            }else{ //si ejecuto un GET y no esta logueado
               $first = 1;
               $output = array(
                    "status" =>EnumError::ERROR,
                    "message" => "No se encuentra logueado",
                    "content" => $data,
               		"product_version"=>"",
               		"hash"=>""
                ); 
            }
        }
        

 
        if($this->RequestHandler->ext == 'json'){   
        	if(!isset($this->request->data["Usuario"]["desktop_login"])){
        		$this->set($output);
	            $this->set("_serialize", array("status", "message", "content","product_version","hash","id_rol","username","cantidad"));
        	}else{
        		echo "";//login chromium
        		die();
        	}
        }else{
            if($output["status"]== "error" ){
                if($first !=1)
                	$this->Session->setFlash($output["message"], 'login-error');
                
                //$this->redirect(array('controller' => 'Usuarios', 'action' => 'login'));
                
            }else{
                  $this->redirect(array('controller' => 'Usuarios', 'action' => 'home'));
                
            }
        }
    }
    
    public function remind() {
        //Si ya esta logueado
        if ($this->Session->check("Auth.User.id")){
            $this->redirect(array('controller' => 'Usuarios', 'action' => 'home')); 
        }
            
        $this->layout = 'ajax';
        $this->loadModel('Usuario');
        if ($this->request->is('post')) {
            $usuario = $this->Usuario->find('first', array('conditions' => array('Usuario.username' => $this->request->data['Usuario']['usernamer']), 'contain' => array()));
            
            if (!$usuario['Usuario']['email']) {
                $this->Session->setFlash('Su correo electr&oacute;nico no se encuentra actualizado, por favor cont&aacute;ctese con el administrador del sistema.', 'login-error');
                $this->redirect(array('controller' => 'Usuarios', 'action' => 'login')); 
            }
            if($usuario['Usuario']['activo'] == 0){
                $this->Session->setFlash('Su usuario no se encuentra activo, por favor cont&aacute;ctese con el administrador del sistema.', 'login-error');
                $this->redirect(array('controller' => 'Usuarios', 'action' => 'login')); 
            }
            
            // Genero password
            $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            $pass = "";
            for($i=0;$i<10;$i++) {
                $pass .= substr($str,rand(0,62),1);
            }
            $passEnc = AuthComponent::password($pass);
            
            $this->Usuario->id = $usuario['Usuario']['id'];
            $this->Usuario->saveField('password', $passEnc);
            
            App::uses('CakeEmail', 'Network/Email');
            $Email = new CakeEmail();
            $Email->config('smtp');
            $Email->emailFormat('html');
            $Email->from(array('info@valmec.com.ar' => 'Valmec'));
            $Email->to($usuario['Usuario']['email']);
            $Email->subject('Recupero de Contraseña');
            $Email->send('<img src="'.Router::url('/img/header_mail.jpg', true ).'" /><br />Se ha solicitado el recupero de contraseña para su usuario <b>'.$usuario['Usuario']['username'].'</b>, en el portal de Valuarit Cloud.<br /><br />Su nueva contraseña es: <b>'.$pass . '</b><br /><br /> Recuerde cambiar su contraseña luego del primer ingreso, seleccionando la opci&oacute;n <b>Mis Datos</b>.<br/><br/>Saludos cordiales.');
            
            $this->Session->setFlash('Su nueva contrase&ntilde;a ha sido enviada a su correo electr&oacute;nico.', 'login-success');
            $this->redirect(array('controller' => 'Usuarios', 'action' => 'login')); 
        }
    }
    
    private function saveLogin($navegador){
        
        
        $data = array();
        
        $data['HistoricoLogin']['id_usuario'] = $this->Session->read("Auth.User.id");
        $data['HistoricoLogin']['username'] = $this->Session->read("Auth.User.username");
        $data['HistoricoLogin']['nombre'] = $this->Session->read("Auth.User.nombre");
        $data['HistoricoLogin']['id_rol'] = $this->Session->read("Auth.User.Rol.id");
        $data['HistoricoLogin']['codigo_rol'] = $this->Session->read("Auth.User.Rol.codigo");
        $data['HistoricoLogin']['navegador'] = $navegador; 

        
        $ip = "";
        if( isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != '' )
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else
            $ip = $_SERVER['REMOTE_ADDR'];
        
        $data['HistoricoLogin']['ip'] =  $ip;
        
        $this->loadModel('HistoricoLogin');
        $this->HistoricoLogin->save($data);
        
        
       
        
    }

    public function logout() {
    
    	/*$this->loadModel(EnumModel::Usuario);
    	
    	$this->{EnumModel::Usuario}->updateAll(
    			array('Usuario.hash_sesion'=>""),
    			array('Usuario.id' => $usuario["Usuario"]["id"]) );
    	
    	*/
    	
        $this->redirect($this->Auth->logout());  
    }
    
    
    public function logoutAllSessions(){
    	
    	$this->loadModel("Usuario");
    	$this->loadModel("cake_sessions");
    	
    	//Traigo el campo activo
    	
    	
    	
    	
    	if(isset($this->request->data['Usuario']['username']) || isset($this->request->data['Usuario']['email'])){
    	    
    	    
    	    
    	    if(isset($this->request->data['Usuario']['username']))
    	       $usuario = $this->Usuario->find('first', array('conditions' => array('Usuario.username' => $this->request->data['Usuario']['username']), 'contain' => array()));
    	    else
    	        $usuario = $this->Usuario->find('first', array('conditions' => array('Usuario.email' => $this->request->data['Usuario']['email']), 'contain' => array()));
    	    
    	        
    	        
        	$password = $this->request->data['Usuario']['password'];
        	
        	$id_usuario = $this->Auth->user('id');
        	
        	if($id_usuario>0)
        		$username = $this->Auth->user('username');
        	else 
        		$username= $this->request->data['Usuario']['username'];
        	
    	}
    	
    	
    		if( isset($usuario) && $usuario && AuthComponent::password($password) == $usuario['Usuario']['password'] || ($id_usuario>0) )
		{//si existe el usuario y ademas el password que envia es valido entonces cierro todas las sessiones
	
			ob_start(); 
			
    		//cierro todas las sessiones de los dispositivos
    		$this->cake_sessions->deleteSessionByUserName($username);
    		
    		$content = ob_get_clean(); 
    		
    		$this->Session->destroy();
    		
    		
			$output = array(
					"status" =>EnumError::SUCCESS,
					"message" => "Se ha cerrado sesion de todos los dispositivos, ingrese nuevamente.",
					"content" => ""
			);
    	}else{
    		
    		$content = ob_get_clean(); 
    		
    		$output = array(
    				"status" =>EnumError::ERROR,
    				"message" => "Para borrar las sesiones debe ingresar usuario y password correctos.Los datos ingresados son inv&aacute;lidos, por favor int&eacute;ntelo nuevamente.",
    				"content" => ""
    		);
    	}
    	
    	
    		$this->set($output);
    		$this->set("_serialize", array("status", "message", "content"));
    	}
    
    public function expired(){
        
        //$this->Session->setFlash("Su sesi&oacute;n ha expirado debido al tiempo de inactividad, por favor ingrese nuevamente.", 'login-error');
         $output = array(
                    "status" =>EnumError::ERROR,
                    "message" => "Su sesi&oacute;n ha expirado debido al tiempo de inactividad, por favor ingrese nuevamente",
                    "content" => $data
                ); 
                
        $msg = "";
        if (isset($this->request->query["msg"]))
            $msg = $this->request->query["msg"];
        
        $this->log("Expired: " . $msg);
        $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
        //$this->redirect("login");
        
    }
    
    public function beforeFilter(){
        
		 App::uses('ConnectionManager', 'Model');
		 
		 $this->Auth->allow('login', 'expired', 'logout', 'remind','logoutAllSessions','prueba');
			
		 parent::beforeFilter();
}
    
public function prueba(){
	
	$salida2 = array();
	
	
	

	
	preg_match_all("^\[(.*?)\]^","Es [CCO 0206 C] * [UIC P 30.4] / [UIC P 30] / [UIC P 365] * [CCO 9900 T 1 0 200508 200607 VAR 0]",$salida2);
	var_dump($salida2);


	
	die();
	
}


    public function home(){
        
       
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
    
    public function manual() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->set('link', Router::url('/', true)."files/manuales/Manual_Cenie.pdf");

    }
    
    public function getMenu(){
        
        
            
            if ($this->Session->read('Auth.User')) {
                 App::import('Lib', 'Menu');
                 $menu = new Menu($this);
                 $menu_aux = $menu->getMenujson();
                 $menu_envio = array();
                 
                 foreach ($menu_aux as &$item){
                     $menu_children = array();
                     
                     if(count($item["childrens"]) > 0 ){
                         foreach ($item["childrens"] as $children){
                             $menu_subchildren = array();
                             
                             if(isset($children["childrens"])){
                                 if(count($children["childrens"]) > 0 ){
                                    foreach ($children["childrens"] as $subchildren){
                                       array_push($menu_subchildren,$subchildren); 
                                        
                                    }
                                    $children["childrens"] =  $menu_subchildren;
                                 }
                            } 
                             
                             array_push($menu_children,$children);
                         }
                     $item["childrens"] =  $menu_children;
                     }
                    array_push($menu_envio,$item);
                 }
                 //$newMenu = array_values( (array)$menu->getMenujson() );
                 
                 
                 //$menu_envio = json_encode($menu_envio), FALSE);
                 
                return $menu_envio;
                 //$this->layout = 'ajax';
                 //die();
                
                
            }
                
        
    } 
    
    
      function permision($permiso,$llamada_interna=0){
         
         $sm = new SecurityManager(); 
        // $this->loadModel("Usuario");
         if ($sm->permiso_particular($permiso,$this->Auth->user( 'username' ))>0){
              
           $permiso = 1;   
              
          }else{
              
               $permiso = 0;   
           
              
              
              
          }
         
          
        if($llamada_interna == 0){  
	            $output = array(
	            "status" =>EnumError::SUCCESS,
	            "message" => $permiso ,
	            "content" => "",
	        );
	        $this->set($output);
	        $this->set("_serialize", array("status", "message", "content")); 
        }else{
        	
        	return $permiso;
        }
         
     }
     
     
     
     /**
      * @secured(CONSULTA_USUARIO)
      */
     public function getModel($vista='default'){
     	
     	$model = parent::getModelCamposDefault();
     	$model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
     	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
     	
     }
     
     
     private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
     	
     	
     	
     	$this->set('model',$model);
     	Configure::write('debug',0);
     	$this->render($vista);
     	
     	
     	
     	
     	
     	
     }
     
     
    
    
    private function getCacheEntidades($id_usuario){
    	
    	/*
    	$this->loadModel("Entidad");
    	
		$entidades = $this->Entidad->query("SELECT DISTINCT entidad.`controller`,entidad.id,entidad.`d_entidad`,entidad.`model`,entidad.`estatica_sistema`,entidad.`tiene_cache_cliente` FROM funcion_rol
		JOIN funcion ON funcion.`id` = funcion_rol.`id_funcion`
		JOIN tipo_funcion ON tipo_funcion.`id` = funcion.`id_tipo_funcion`
		JOIN usuario ON usuario.`id_rol` = funcion_rol.`id_rol`
		JOIN entidad ON entidad.`id` = tipo_funcion.`id_entidad`
		WHERE usuario.id = ".$id_usuario."");
		
		
		
		foreach($entidades as &$entidad){
			
			
			if($entidad[0]["entidad"]["estatica_sistema"] == 1){//consulto el index
				
				
			}
		}
		
		*/
		
    	
    	
    }
    
    
   
  
    
    
    
}
?>