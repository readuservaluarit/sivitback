<?php
        
        include(APP.'View'.DS.'AsientosCuentaContable'.DS.'json'.DS.'asiento_cuenta_contable.ctp');
        
    
		
		//esto se presetea agrega en codigo como un CBO
		$model["es_debe"]["show_grid"] = 1;
		$model["es_debe"]["header_display"] = "Debe / Haber";
		$model["es_debe"]["minimun_width"] = "65"; //REstriccion mas de esto no se puede achicar
        $model["es_debe"]["order"] = "1";
		$model["es_debe"]["validate"] = "0";
		$model["es_debe"]["read_only"] = "0";
		$model["es_debe"]["text_colour"] = "#000000";
		// $model["es_debe"]["type"] = "string";
		
        
		$model["id_cuenta_contable"]["show_grid"] = 0;//INVISIBLE
        $model["id_cuenta_contable"]["header_display"] = "id_cuenta_contable";
        //$model["id_cuenta_contable"]["minimun_width"] = "20"; //Restriccion mas de esto no se puede achicar
        $model["id_cuenta_contable"]["order"] = "2";
        $model["id_cuenta_contable"]["text_colour"] = "#0000EE";
		
		$model["codigo_cuenta_contable"]["show_grid"] = 1;//VISIBLE
        $model["codigo_cuenta_contable"]["header_display"] = "[Código]";
		$model["codigo_cuenta_contable"]["width"] = "90"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["codigo_cuenta_contable"]["minimun_width"] = "60"; //Restriccion mas de esto no se puede achicar
        $model["codigo_cuenta_contable"]["order"] = "3";
        $model["codigo_cuenta_contable"]["text_colour"] = "#660044";
		$model["codigo_cuenta_contable"]["read_only"] = "1";
		$model["codigo_cuenta_contable"]["picker"] = EnumMenu::mnuCuentaContablePicker;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
        
        $model["d_cuenta_contable"]["show_grid"] = 1;//VISIBLE
        $model["d_cuenta_contable"]["header_display"] = "[Desc. Cuenta Contable]";
		$model["d_cuenta_contable"]["width"] = "200"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["d_cuenta_contable"]["minimun_width"] = "80"; //Restriccion mas de esto no se puede achicar
        $model["d_cuenta_contable"]["order"] = "4";
        $model["d_cuenta_contable"]["text_colour"] = "#550055";
		$model["d_cuenta_contable"]["read_only"] = "1";
		$model["d_cuenta_contable"]["picker"] = EnumMenu::mnuCuentaContablePicker;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
		
		
		
		//En medio de estos campos se agrega un BotonPicker en grilla
		$model["monto"]["show_grid"] = 0;//INVISIBLE
        $model["monto"]["header_display"] = "Monto";
        $model["monto"]["minimun_width"] = "80"; //REstriccion mas de esto no se puede achicar
        $model["monto"]["order"] = "5";
        $model["monto"]["text_colour"] = "#000000";
		
        
		$model["debe_monto"]["show_grid"] = 1;
		$model["debe_monto"]["validate"] = 1;
        $model["debe_monto"]["header_display"] = "D. Monto";
		$model["debe_monto"]["width"] = "70"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["debe_monto"]["minimun_width"] = "40"; //Restriccion mas de esto no se puede achicar
        $model["debe_monto"]["order"] = "6";
        $model["debe_monto"]["text_colour"] = "#0000FF";
		
        
		$model["haber_monto"]["show_grid"] = 1;
		$model["haber_monto"]["validate"] = 1;
        $model["haber_monto"]["header_display"] = "H. Monto";
		$model["haber_monto"]["width"] = "70";
        $model["haber_monto"]["minimun_width"] = "40"; //Restriccion mas de esto no se puede achicar
        $model["haber_monto"]["order"] = "7";
        $model["haber_monto"]["text_colour"] = "#008800"; //dark green 
		
        
		//SE USA COMO VARIABLE PARA GUARDAR Las modificaiones 
		$model["AsientoCuentaContableCentroCostoItem"]["show_grid"] = 0;
        $model["AsientoCuentaContableCentroCostoItem"]["header_display"] = "ITEMS CENTRO COSTO";
        $model["AsientoCuentaContableCentroCostoItem"]["minimun_width"] = "90"; //Restriccion mas de esto no se puede achicar
        $model["AsientoCuentaContableCentroCostoItem"]["order"] = "8";
        $model["AsientoCuentaContableCentroCostoItem"]["text_colour"] = "#440000";
		

        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "AsientoCuentaContable",
                    "content" => $model
                );
        echo json_encode($output);
        
        
?>