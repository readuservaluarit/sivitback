<?php

class IvaPicker extends EntityPicker{
    
    public $model = "Iva";
    
    public $pickerTitle = "Selección de Iva";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Iva", "field" => "d_iva", "width" => "80%", "keyOrder" => "d_iva"),
                                 
                                );

    public function getPickerConditions($busqueda){
        
        $pickerConditions = array('Iva.d_iva LIKE' => '%' . $busqueda['bus_unico'] . '%');
        
        return $pickerConditions;
        
        
    }
    
    public function getPickerInitialFilter(){
        
        //Filtra siempre por la Jurisdiccion del usuario si corresponde
        return DataFilter::getJurisdiccionPickerFilter($this->model);
        
    }

}