<?php

/**
* @secured(CONSULTA_CUENTA_BANCARIA)
*/
class CuentasBancariaController extends AppController {
    public $name = 'CuentasBancaria';
    public $model = 'CuentaBancaria';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
/**
* @secured(CONSULTA_CUENTA_BANCARIA)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
      
        $id_persona = strtolower($this->getFromRequestOrSession('CuentaBancaria.id_persona'));
        $propia = strtolower($this->getFromRequestOrSession('CuentaBancaria.propia'));//si es propia de la empresa es un 1
        $activo = $this->getFromRequestOrSession('CuentaBancaria.activo');
        
        $conditions = array(); 
        if ($id_persona != "") {
            array_push($conditions,array('CuentaBancaria.id_persona =' =>  $id_persona ));
        }
        
        
        if ($activo!= "") {
        	array_push($conditions,array('CuentaBancaria.activo =' =>  $activo));
        }
        
   
        
        if ($propia != "") { 
            array_push($conditions, array('CuentaBancaria.propia =' =>  $propia ));
        }

        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de CuentasBancaria', 'Datos de las CuentasBancaria', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_CUENTA_BANCARIA', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'CuentaBancaria.id', "10%");
                $formBuilder->addHeader('Nombre', 'CuentaBancaria.d_CUENTA_BANCARIA', "50%");
                $formBuilder->addHeader('Nombre', 'CuentaBancaria.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_CUENTA_BANCARIA');
                $formBuilder->addField($this->model, 'cotizacion');
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }
		else // vista json
		{ 
			$this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
			$page_count = $this->params['paging'][$this->model]['pageCount'];
			
			
			foreach($data as &$dato){
				
				if(isset($dato['Pais'])){
					$dato['CuentaBancaria']['d_pais']=$dato['Pais']['d_pais'];
				}else{
					$dato['CuentaBancaria']['d_pais'] = "";
				}
					
				if(isset($dato['Provincia'])){
					$dato['CuentaBancaria']['d_provincia']=$dato['Provincia']['d_provincia'];
				}else{
					$dato['CuentaBancaria']['d_provincia'] = "";
				}
				 
				if(isset($dato['BancoSucursal']) && isset($dato['BancoSucursal']['d_banco_sucursal'])){
					$dato['CuentaBancaria']['d_banco_sucursal'] = $dato['BancoSucursal']['d_banco_sucursal'];
		
				}else{
					$dato['CuentaBancaria']['d_banco_sucursal'] = "";
				}
				
				if(isset($dato['TipoCuentaBancaria']) && isset($dato['TipoCuentaBancaria']['d_tipo_cuenta_bancaria'])){
					$dato['CuentaBancaria']['d_tipo_cuenta_bancaria']=$dato['TipoCuentaBancaria']['d_tipo_cuenta_bancaria'];       
					$dato['CuentaBancaria']['d_cuenta_bancaria']=$dato['CuentaBancaria']["nro_cuenta"];       
				}else{
					
					$dato['CuentaBancaria']['d_tipo_cuenta_bancaria'] = "";
					$dato['CuentaBancaria']['d_cuenta_bancaria'] = "";
				}
				
				if(isset($dato['Moneda']))
				{
					//MP: Trae todo de la tabla Moneda (Cotizacion del Dia)
					$dato['CuentaBancaria']['d_moneda']     		  =$dato['Moneda']['d_moneda'];       
					$dato['CuentaBancaria']['valor_moneda']  		  =$dato['Moneda']['cotizacion'];     
					$dato['CuentaBancaria']['d_simbolo']     		  =$dato['Moneda']['simbolo'];       
					$dato['CuentaBancaria']['d_abreviacion_afip']     =$dato['Moneda']['abreviacion_afip'];     
					$dato['CuentaBancaria']['d_simbolo_internacional']=$dato['Moneda']['simbolo_internacional'];   
					
				}else{
					
					$dato['CuentaBancaria']['d_moneda']     		   = "";
					$dato['CuentaBancaria']['valor_moneda']  		   = "";
					$dato['CuentaBancaria']['d_simbolo']     		   = "";
					$dato['CuentaBancaria']['d_abreviacion_afip']      = "";
					$dato['CuentaBancaria']['d_simbolo_internacional'] = "";       
					
				}
				
				
				
				if(isset($dato['CuentaContable'])){
                    $dato['CuentaBancaria']['d_cuenta_contable'] = $dato['CuentaContable']['codigo'].' '.$dato['CuentaContable']['d_cuenta_contable'];    
					$dato['CuentaBancaria']['codigo_cuenta_contable'] = $dato['CuentaContable']['codigo'];    
				}else{
                    
                    $dato['CuentaBancaria']['d_cuenta_contable'] = '';
                    $dato['CuentaBancaria']['codigo_cuenta_contable'] = '';
                }	
                
                
                
                
                if($dato["CuentaBancaria"]["propia"] == 1)
                    $dato["CuentaBancaria"]["d_propia"] = "Si";
                else    
                    $dato["CuentaBancaria"]["d_propia"] = "No";
                    
				unset($dato['Pais']);
				unset($dato['Provincia']);
				unset($dato['BancoSucursal']);
				unset($dato['TipoCuentaBancaria']);
				unset($dato['Persona']);
				//unset($dato['Moneda']);
				unset($dato['CuentaContable']);
	   
			}
			
			$output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "list",
				"content" => $data,
				"page_count" =>$page_count
			);
			$this->set($output);
			$this->set("_serialize", array("status", "message","page_count", "content"));
			
		}
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->CuentaBancaria->id = $id;
            $this->request->data = $this->CuentaBancaria->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('CuentaBancaria');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de CuentaBancaria');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de CuentaBancaria');
        else
            $formBuilder->setTituloForm('Consulta de CuentaBancaria');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_CUENTA_BANCARIA', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'BancoAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('BancoDBanco', 'Debe ingresar un nombre');
    


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_CUENTA_BANCARIA)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_add = '';
            
            try{
                if ($this->CuentaBancaria->saveAll($this->request->data, array('deep' => true))){
                    $id_add = $this->CuentaBancaria->id;
                    $mensaje = "La Cuenta Bancaria ha sido creada exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,la Cuenta Bancaria no ha podido ser creada.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" =>$id_add
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }

    /**
    * @secured(MODIFICACION_CUENTA_BANCARIA)
    */
     
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "La Cuenta Bancaria ha sido modificada exitosamente";
                     $status = EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
               }
               
               if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, la Cuenta Bancaria no ha podido modificarse.', 'error');
                
            }
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            //$this->{$this->model}->contain('Provincia','Pais');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
    
        /**
    * @secured(CONSULTA_CUENTA_BANCARIA)
    */
   public function getModel($vista='default')
    {    
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    }
    
    
    
    private function editforView($model,$vista)
    {  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);    
        
    }
    
    /**
    * @secured(BAJA_CUENTA_BANCARIA,READONLY_PROTECTED)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->CuentaBancaria->id = $id;
     
       try{
            if ($this->CuentaBancaria->delete() ) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "La Cuenta Bancaria ha sido eliminada exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }
    
    
    
    
    
    
    public function ImportarResumenBancario($id_cuenta_bancaria){
        
    
         $error = '';
         $message = '';
         $this->ImportarArchivoCsv($model_destino="CsvImportConciliacion",$error,$message);
         if($error == "success"){
          //aca debo procesar la tabla   
         }else{
             
             //informar error
         }
         
         
    }
    
    
    



}
?>