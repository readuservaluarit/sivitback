<?php
class PredioPicker extends EntityPicker{
    
    public $model = "Predio";
    
    public $pickerTitle = "Selección de CUI";
    
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "CUI", "field" => "cui", "width" => "30%", "keyOrder" => "cui"),
                                  array("title" => "Localidad", "field" => "localidad", "width" => "30%", "keyOrder" => "cui"),
                                  array("title" => "Direcci&oacute;n", "field" => "direccion", "width" => "30%", "keyOrder" => "direccion")
                                );

    public function getPickerConditions($busqueda){
        
        return array('Predio.cui LIKE' => '%' . $busqueda['bus_unico'] . '%');
    }
    
    public function getPickerJoins() {
        
        return DataFilter::getPredioPickerJoinFilter();
        
    }

}