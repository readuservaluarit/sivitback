<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class QaConformidad extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'qa_conformidad';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id_qa_origen_falla' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar una falla de origen',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
        
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 public $belongsTo = array(
        'Area' => array(
            'className' => 'Area',
            'foreignKey' => 'id_area',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Estado' => array(
            'className' => 'Estado',
            'foreignKey' => 'id_estado',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Producto' => array(
            'className' => 'Producto',
            'foreignKey' => 'id_producto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Componente' => array(
            'className' => 'Componente',
            'foreignKey' => 'id_componente',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'MateriaPrima' => array(
            'className' => 'MateriaPrima',
            'foreignKey' => 'id_materia_prima',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        
        'Proveedor' => array(
            'className' => 'Proveedor',
            'foreignKey' => 'id_proveedor',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        
         'Comprobante' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'QaOrigenFalla' => array(
            'className' => 'QaOrigenFalla',
            'foreignKey' => 'id_qa_origen_falla',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        
);
	

}
