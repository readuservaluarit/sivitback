<?php


class ChequesController extends AppController {
    public $name = 'Cheques';
    public $model = 'Cheque';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
  

  
  
    /**
    * @secured(CONSULTA_CHEQUE)
    */
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros($this->model);
	    
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
        		'contain' => array('Banco','BancoSucursal'=>array("Banco"),'EstadoCheque','ComprobanteEntrada'=>array('TipoComprobante','Persona','CarteraRendicion','DetalleTipoComprobante'),'ComprobanteSalida'=>array('TipoComprobante','Persona','CarteraRendicion','DetalleTipoComprobante'),'ComprobanteRechazo'=>array('TipoComprobante'),'Chequera'=>array('CuentaBancaria'=>array('BancoSucursal'=>array('Banco'))),'TipoCheque','Moneda'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order'=>'Cheque.id desc'
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
        
        //vista formBuilder
    }
      
    else{ // vista json
      
		$this->PaginatorModificado->settings = $this->paginate; 
		$data = $this->PaginatorModificado->paginate($this->model);
		$page_count = $this->params['paging'][$this->model]['pageCount'];

		$comprobante = New Comprobante();
        foreach($data as $key=> &$valor)
        {    
            $this->CleanForOutput($valor,$comprobante,$data,$key);
        }
        
        $aux = array();
        
        $this->ordenarArray($aux,$data);//si se le borraron posiciones lo reordeno
        $this->data = $aux; 

        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $this->data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
     } 
    //fin vista json
    }
    
    

    /**
    * @secured(ADD_CHEQUE)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
             $this->request->data["Cheque"]["fecha_ingreso"] = date("Y-m-d H:i:s"); 
             
             $id_add = '';
             $nro_cheque = 0;
             
             if($this->request->data["Cheque"]["id_tipo_cheque"] == EnumTipoCheque::Propio){ //Si el cheque es propio entonces le calculo el numero
             
             	
             	if($this->existe_cheque_propio() == 1)
             		throw new Exception('El cheque indicado ya se encuentra ingresado o no esta dentro de los n&uacute;meros aceptables de la cartera');
              
                $id_chequera = $this->request->data["Cheque"]["id_chequera"];
                
                if(strlen(trim($this->request->data["Cheque"]["nro_cheque"])) == 0){
               	    $nro_cheque = $this->Cheque->ultimoNumeroChequePropio($id_chequera);
                	$this->request->data["Cheque"]["nro_cheque"] = $nro_cheque;
                }else{
                	
                	$nro_cheque = $this->request->data["Cheque"]["nro_cheque"];
                }
             }
             
             
             if(!$this->request->data["Cheque"]["id_banco_sucursal"]>0)//esto lo pongo porque al agregar el banco se agrega el id_banco_sucursal como vacio
             	unset($this->request->data["Cheque"]["id_banco_sucursal"]);
             	
             
             
             if($nro_cheque < 0 ){
              $tipo = EnumError::ERROR;  
              $mensaje = "La chequera que eligi&oacute; no tiene cheques mas disponibles, debe dar de alta una nueva o usted ingres&oacute; alg&uacute;n cheque propio con el n&uacute;mero manualmente. En este caso debe indicar el n&uacute;mero de cheque en el campo Nro Cheque"; 
             }else{  
                try{
                if ($this->Cheque->saveAll($this->request->data, array('deep' => true))){
                  
                    $id_add = $this->Cheque->id;
                   
                    $mensaje = "El Cheque ha sido agregado exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el Cheque no ha podido ser ingresado.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             }
            
            
			 $output = array(
						"status" => $tipo,
						"message" => $mensaje,
						"content" => "",
						"id_add" =>$id_add
						);
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
	/**
    * @secured(MODIFICACION_CHEQUE, READONLY_PROTECTED)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            
            
            if(!$this->request->data["Cheque"]["id_banco_sucursal"]>0)//esto lo pongo porque al agregar el banco se agrega el id_banco_sucursal como vacio
            	unset($this->request->data["Cheque"]["id_banco_sucursal"]);
            
            try{
            	
            	
            	$existe = 0;
            	
            	if($this->request->data["Cheque"]["id_tipo_cheque"] == EnumTipoCheque::Propio){
            		
            		$existe = $this->existe_cheque_propio();
            	}
            	
            	
            	
            	if($existe == 0){
		                if ($this->{$this->model}->saveAll($this->request->data)){
		                     $mensaje =  "El Cheque ha sido modificada exitosamente";
		                     $tipo = EnumError::SUCCESS;
		                    
		                    
		                }else{   //si hubo error recupero los errores de los models
		                    
		                    $errores = $this->{$this->model}->validationErrors;
		                    $errores_string = "";
		                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
		                        $errores_string.= "&bull; ".$error[0]."\n";
		                        
		                    }
		                    $mensaje = $errores_string;
		                    $tipo = EnumError::ERROR; 
		               }
            	}else{
            		
            		$tipo = EnumError::ERROR;
            		$mensaje = "El cheque Propio que esta editando ya existe y no es posible asignar ese n&uacute;mero de Cheque.";
            	}
               
               
               
               
               
           
            }catch(Exception $e){
                
                $tipo = EnumError::ERROR; 
                $mensaje = $e->getMessage();
                
            }
            
            
            if($this->RequestHandler->ext == 'json'){
            	$output = array(
            			"status" => $tipo,
            			"message" => $mensaje,
            			"content" => ""
            	);
            	
            	$this->set($output);
            	$this->set("_serialize", array("status", "message", "content"));
            }else{
            	$this->Session->setFlash($mensaje, $status);
            	$this->redirect(array('controller' => $this->name, 'action' => 'index'));
            	
            } 
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            $this->{$this->model}->contain('Provincia','Pais');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
  
  
    



   /**
    * @secured(CONSULTA_CHEQUE)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
        
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        
            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);
        
               
        
        
      
    } 

    public function home(){
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
    
	
	//CUIDADO: borrar este metodo 
    public function transferenciaChequeABanco($array_id_cheques){
        
        
        
        
    }
    
	
// data[Cheque][fecha_cheque_desde]=2018-01-10
// data[Cheque][fecha_cheque_hasta]=2018-01-12
	
    public  function RecuperoFiltros($model){
        
        //Recuperacion de Filtros
        $id_cartera_cheque = $this->getFromRequestOrSession('Cheque.id_cartera_cheque');
        $id_tipo_cheque = $this->getFromRequestOrSession('Cheque.id_tipo_cheque');
        $id_cheque = $this->getFromRequestOrSession('Cheque.id');
        $nro_cheque = $this->getFromRequestOrSession('Cheque.nro_cheque');
        $id_banco = $this->getFromRequestOrSession('Cheque.id_banco');
        $id_estado_cheque = $this->getFromRequestOrSession('Cheque.id_estado_cheque');
        $id_persona = $this->getFromRequestOrSession('Cheque.id_persona');
        $excluir_anulado = $this->getFromRequestOrSession('Cheque.excluir_anulado');
        
        
        $id_chequera = $this->getFromRequestOrSession('Cheque.id_chequera');
        
        $fecha_cheque_desde = $this->getFromRequestOrSession('Cheque.fecha_cheque_desde');
        $fecha_cheque_hasta = $this->getFromRequestOrSession('Cheque.fecha_cheque_hasta');
        $id_estado_cheque_excluir = $this->getFromRequestOrSession('Cheque.id_estado_cheque_excluir');
        
        
        $fecha_emision_cheque_desde = $this->getFromRequestOrSession('Cheque.fecha_emision_desde');
        $fecha_emision_cheque_hasta = $this->getFromRequestOrSession('Cheque.fecha_emision_hasta');

		
		
        $id_comprobante_entrada = $this->getFromRequestOrSession('Cheque.id_comprobante_entrada');
		$nro_comprobante_entrada = $this->getFromRequestOrSession('Cheque.nro_comprobante_entrada');
		$solo_con_comprobante_entrada = $this->getFromRequestOrSession('Cheque.solo_con_comprobante_entrada');
		
        $id_comprobante_salida = $this->getFromRequestOrSession('Cheque.id_comprobante_salida');
		$nro_comprobante_salida = $this->getFromRequestOrSession('Cheque.nro_comprobante_salida');
		$solo_con_comprobante_salida = $this->getFromRequestOrSession('Cheque.solo_con_comprobante_salida');
        
        
        
        $id_comprobante_rechazo = $this->getFromRequestOrSession('Cheque.id_comprobante_rechazo');
        $nro_comprobante_rechazo = $this->getFromRequestOrSession('Cheque.nro_comprobante_rechazo');
        $solo_con_comprobante_rechazo = $this->getFromRequestOrSession('Cheque.solo_con_comprobante_rechazo');
        
		
		$cruzado = $this->getFromRequestOrSession('Cheque.cruzado');
		$no_a_la_orden = $this->getFromRequestOrSession('Cheque.no_a_la_orden');
		$conciliado = $this->getFromRequestOrSession('Cheque.conciliado');
        $id_detalle_tipo_comprobante = $this->getFromRequestOrSession('Cheque.id_detalle_tipo_comprobante');
        $comun = $this->getFromRequestOrSession('Cheque.comun');
        $diferido= $this->getFromRequestOrSession('Cheque.diferido');
      
       $conditions = array(); 
       
       
       if($id_persona !=''){
		   array_push($conditions,    
				array( 'OR' => array(
									'ComprobanteEntrada.id_persona'=>$id_persona,
									'ComprobanteSalida.id_persona '=>$id_persona,
									'ComprobanteRechazo.id_persona'=>$id_persona,
									  )
					 )
				);
       }  
       
		if($cruzado!=""){
            array_push($conditions, array('Cheque.cruzado' =>  $cruzado ));
            array_push($this->array_filter_names,array("Cruzado:"=>$cruzado));
		}
		
		if($conciliado!=""){
            array_push($conditions, array('Cheque.conciliado' =>  $conciliado )); 
            array_push($this->array_filter_names,array("Conciliado:"=>$conciliado));
		}
		
		
		
		if($id_chequera!=""){
			
			array_push($conditions, array('Cheque.id_chequera' =>  $id_chequera));
			array_push($this->array_filter_names,array("Chequera:"=>$id_chequera));
		
		}
		
		
		if($id_cheque !=""){//puede ser un string separado por comas
			
			$ids_cheque = explode(",", $id_cheque);
			array_push($conditions, array('Cheque.id' =>  $ids_cheque));
			array_push($this->array_filter_names,array("Id Cheques:"=>$ids_cheque));
		}
		
        if($id_estado_cheque_excluir!=""){
        	$id_estado_cheque_excluir= explode(',', $id_estado_cheque_excluir);
        	array_push($conditions, array('Cheque.no_a_la_orden' =>  $id_estado_cheque_excluir));
        }
		
		if($no_a_la_orden!=""){
            array_push($conditions, array('Cheque.no_a_la_orden' =>  $no_a_la_orden ));
            array_push($this->array_filter_names,array("No a la orden:"=>$no_a_la_orden));
		}
		
        if($comun!=""){
        	array_push($conditions, array('Cheque.comun' =>  $comun));
        	array_push($this->array_filter_names,array("Comun:"=>$comun));
        }
	   
        
        if($diferido !=""){
        	array_push($conditions, array('Cheque.diferido' =>  $diferido));
        	array_push($this->array_filter_names,array("Diferido:"=>$comun));
        }
        	
        		
        if($id_cartera_cheque!="")
            array_push($conditions, array('Cheque.id_cartera_cheque =' => $id_cartera_cheque)); 
            
               
        if($id_tipo_cheque!="")
            array_push($conditions, array('Cheque.id_tipo_cheque =' =>  $id_tipo_cheque )); 
             
        if($nro_cheque!=""){
            array_push($conditions, array('Cheque.nro_cheque LIKE' =>'%'.$nro_cheque .'%')); 
            array_push($this->array_filter_names,array("Nro Cheque:"=>$nro_cheque));
        }
            
        if($id_banco!=""){
            array_push($conditions, array('Cheque.id_banco' =>  $id_banco ));
            array_push($conditions, array('BancoSucursal.id_banco' =>  $id_banco ));
        }
        if($id_estado_cheque!="")
            array_push($conditions, array('Cheque.id_estado_cheque' =>  $id_estado_cheque )); 
            

        if($solo_con_comprobante_entrada == '0')
            array_push($conditions, array('Cheque.id_comprobante_entrada' =>  null ));
        else if($solo_con_comprobante_entrada == '1') 
        {
			 array_push($conditions, array('Cheque.id_comprobante_entrada >' =>  0 ));
            if($id_comprobante_entrada!="")
                array_push($conditions, array('Cheque.id_comprobante_entrada' =>  $id_comprobante_entrada ));     
			  if($nro_comprobante_entrada!="")
                array_push($conditions, array('ComprobanteEntrada.nro_comprobante LIKE' => '%'. $nro_comprobante_entrada.'%' ));     
        }
		
		
		if($solo_con_comprobante_salida == '0')
            array_push($conditions, array('Cheque.id_comprobante_salida' =>  null ));
         else if($solo_con_comprobante_salida == '1')
         {
		    array_push($conditions, array('Cheque.id_comprobante_salida >' =>  0 ));
			if($id_comprobante_salida!="")
                array_push($conditions, array('Cheque.id_comprobante_salida' =>  $id_comprobante_salida ));
			 if($nro_comprobante_salida!="")
                 array_push($conditions, array('ComprobanteSalida.nro_comprobante LIKE' => '%'. $nro_comprobante_salida.'%' ));     
         }
         
         
         if($solo_con_comprobante_rechazo == '0')
            array_push($conditions, array('Cheque.id_comprobante_rechazo' =>  null ));
         else if($solo_con_comprobante_rechazo == '1')
         {
            array_push($conditions, array('Cheque.id_comprobante_rechazo >' =>  0 ));
            if($id_comprobante_rechazo!="")
                array_push($conditions, array('Cheque.id_comprobante_rechazo' =>  $id_comprobante_salida ));
             if($nro_comprobante_rechazo!="")
                 array_push($conditions, array('ComprobanteRechazo.nro_comprobante LIKE' => '%'. $nro_comprobante_rechazo.'%' ));     
         }
         
         
       if($id_detalle_tipo_comprobante !=''){
           
           switch($id_detalle_tipo_comprobante){
               
               case EnumDetalleTipoComprobante::ChequePropioRechazado:
                    array_push($conditions, array('ComprobanteSalida.id_tipo_comprobante' => array (EnumTipoComprobante::OrdenPagoManual,EnumTipoComprobante::OrdenPagoAutomatica) ));
                    array_push($conditions, array('Cheque.id_estado_cheque <>' => EnumEstadoCheque::Rechazado ));
               break;
               
               case EnumDetalleTipoComprobante::DetercerosRechazadoporBanco:
                    array_push($conditions, array('ComprobanteSalida.id_tipo_comprobante' => array (EnumTipoComprobante::MovimientoBancario) ));
                    array_push($conditions, array('Cheque.id_estado_cheque <>' => EnumEstadoCheque::Rechazado ));
               break;
               
                case EnumDetalleTipoComprobante::Tercerossalidaporegreso:
                case EnumDetalleTipoComprobante::Propiossalidaporegreso:
                    array_push($conditions, array('ComprobanteSalida.id_tipo_comprobante' => array (EnumTipoComprobante::EgresoCaja) ));
                    array_push($conditions, array('Cheque.id_estado_cheque <>' => EnumEstadoCheque::Rechazado ));
               break;
               
                 case EnumDetalleTipoComprobante::TercerosEntregadoaProveedorconND:
                    array_push($conditions, array('ComprobanteSalida.id_tipo_comprobante' => array (EnumTipoComprobante::OrdenPagoManual,EnumTipoComprobante::OrdenPagoAutomatica,EnumTipoComprobante::OrdenPagoRenGasto) ));
                    array_push($conditions, array('Cheque.id_estado_cheque <>' => EnumEstadoCheque::Rechazado ));
               break;
           }
           
       }  
       
       if($excluir_anulado!="" && $excluir_anulado == 1){
       		array_push($conditions, array('Cheque.id_estado_cheque <>' => EnumEstadoCheque::Anulado ));
       		array_push($this->array_filter_names,array("Traer Anulados?:"=>"No"));
       }
       
       
       if($fecha_cheque_desde!=""){
       	array_push($conditions, array($model.'.fecha_cheque >= ' =>  $fecha_cheque_desde));
       	array_push($this->array_filter_names,array("Fecha Cheque Dsd:"=>$this->{$this->model}->formatDate($fecha_cheque_desde)));
       }
       
       if($fecha_cheque_hasta!=""){
       	array_push($conditions, array($model.'.fecha_cheque <= ' => $fecha_cheque_hasta));
       	array_push($this->array_filter_names,array("Fecha Cheque Hta.:"=>$this->{$this->model}->formatDate($fecha_cheque_hasta)));
       }
       
       
       
       if($fecha_emision_cheque_desde!=""){
       	array_push($conditions, array($model.'.fecha_emision >= ' =>  $fecha_emision_cheque_desde));
       	array_push($this->array_filter_names,array("Fecha Emisi&oacute;n Dsd:"=>$this->{$this->model}->formatDate($fecha_emision_cheque_desde)));
       }
       
       if($fecha_emision_cheque_hasta!=""){
       	array_push($conditions, array($model.'.fecha_emision <= ' => $fecha_emision_cheque_hasta));
       	array_push($this->array_filter_names,array("Fecha Emisi&oacute;n Hta.:"=>$this->{$this->model}->formatDate($fecha_emision_cheque_hasta)));
       }
       
        
       // if($solo_con_comprobante_entrada == '1')
            // array_push($conditions, array('Cheque.id_comprobante_entrada >' =>  0 ));
            
      return $conditions;      
    }
    
    
    /**
    * @secured(CONSULTA_CHEQUE)
    */
    public function reporte_cheques(){
        
        $this->paginado =0;
        
         $this->ExportarExcel("default","index","Listado de Cheques");
                 
        
        
    }
    
    protected function CleanForOutput(&$valor,$comprobante,&$data,$key){
        
        
         if($valor['Cheque']['id_tipo_cheque'] == EnumTipoCheque::Propio)
             {            
                 //EnumTipoCheque::Propio
                    if(isset($valor['Chequera']['CuentaBancaria']['BancoSucursal']['Banco']['d_banco'])){
                     $valor['Cheque']['d_banco'] = $valor['Chequera']['CuentaBancaria']['BancoSucursal']['Banco']['d_banco'];
                     $valor['Cheque']['id_banco'] = $valor['Chequera']['CuentaBancaria']['BancoSucursal']['Banco']['id'];
                    }
                    
                    if(isset($valor['Chequera']['CuentaBancaria']['BancoSucursal']['d_banco_sucursal'])){
                        $valor['Cheque']['d_banco_sucursal'] = $valor['Chequera']['CuentaBancaria']['BancoSucursal']['d_banco_sucursal']; 
                        $valor['Cheque']['id_banco_sucursal'] = $valor['Chequera']['CuentaBancaria']['BancoSucursal']['id']; 
                        }             
             }
             else{
                //EnumTipoCheque::Terceros      
                 if(isset($valor['Banco']['d_banco'])){
                     $valor['Cheque']['d_banco'] = $valor['Banco']['d_banco'];
                     $valor['Cheque']['id_banco'] = $valor['Banco']['id'];
                  }   
                 if(isset($valor['BancoSucursal']['d_banco_sucursal'])){
                        $valor['Cheque']['d_banco_sucursal'] = $valor['BancoSucursal']['d_banco_sucursal'];
                        $valor['Cheque']['id_banco_sucursal'] = $valor['BancoSucursal']['id'];
                 } 
             } 
             //COMUN A AMBOS TIPOS DE CHEQUES
             if(isset($valor['Moneda']))
                $valor['Cheque']['d_moneda'] = $valor['Moneda']['d_moneda']; 
             
            if(isset($valor['EstadoCheque']))
                $valor['Cheque']['d_estado_cheque'] = $valor['EstadoCheque']['d_estado_cheque'];
                
            if(isset($valor['TipoCheque']))
                $valor['Cheque']['d_tipo_cheque'] = $valor['TipoCheque']['d_tipo_cheque'];    
                
                if(isset($valor['ComprobanteEntrada']) && $valor['ComprobanteEntrada']["id"]>0){
                 if(isset($valor['ComprobanteEntrada']['TipoComprobante']))
                    $codigo_tipo_comprobante_entrada = $valor['ComprobanteEntrada']['TipoComprobante']['codigo_tipo_comprobante'];
                else
                    $codigo_tipo_comprobante_entrada = '';
                
                    
             if(isset($valor['ComprobanteEntrada']['Persona']['id']) && $valor['ComprobanteEntrada']['Persona']['id']>0)
                   $valor['Cheque']['razon_social_comprobante_entrada'] = $valor['ComprobanteEntrada']['Persona']['razon_social'];
             
             if(isset($valor['ComprobanteEntrada']['CarteraRendicion']['id']) && $valor['ComprobanteEntrada']['CarteraRendicion']['id']>0)
                  $valor['Cheque']['razon_social_comprobante_entrada'] = $valor['ComprobanteEntrada']['CarteraRendicion']['d_cartera_rendicion'];
                   	
                   	
              if(isset($valor['ComprobanteEntrada']['DetalleTipoComprobante']['id']) && $valor['ComprobanteEntrada']['DetalleTipoComprobante']['id']>0)
                   $valor['Cheque']['razon_social_comprobante_entrada'] = $valor['ComprobanteEntrada']['DetalleTipoComprobante']['d_detalle_tipo_comprobante'];
             
            $valor['Cheque']['d_comprobante_entrada'] = $codigo_tipo_comprobante_entrada.'-'.$comprobante->GetNumberComprobante(0,$valor['ComprobanteEntrada']['nro_comprobante']);
            }
                
                    
                    
            
                
                
            if(isset($valor['ComprobanteSalida']) && $valor['ComprobanteSalida']["id"]>0){
            	
                if(isset($valor['ComprobanteSalida']['TipoComprobante']))
                    $codigo_tipo_comprobante_salida = $valor['ComprobanteSalida']['TipoComprobante']['codigo_tipo_comprobante'];
                else
                	$codigo_tipo_comprobante_salida = '';
                
                    if(isset($valor['ComprobanteSalida']['Persona']['id']) && $valor['ComprobanteSalida']['Persona']['id']>0)
                    	$valor['Cheque']['razon_social_comprobante_salida'] = $valor['ComprobanteSalida']['Persona']['razon_social'];
                    
                    
                    if(isset($valor['ComprobanteSalida']['CarteraRendicion']['id']) && $valor['ComprobanteSalida']['CarteraRendicion']['id']>0)
                    	$valor['Cheque']['razon_social_comprobante_salida'] = $valor['ComprobanteSalida']['CarteraRendicion']['d_cartera_rendicion'];
                    
                    
                    if(isset($valor['ComprobanteSalida']['DetalleTipoComprobante']['id']) && $valor['ComprobanteSalida']['DetalleTipoComprobante']['id']>0)
                    		$valor['Cheque']['razon_social_comprobante_salida'] = $valor['ComprobanteSalida']['DetalleTipoComprobante']['d_detalle_tipo_comprobante'];
                    		
                    		
             $valor['Cheque']['d_comprobante_salida'] = $codigo_tipo_comprobante_salida.'-'.$comprobante->GetNumberComprobante(0,$valor['ComprobanteSalida']['nro_comprobante']);
             
             
            }
                    
                   
            
            
            
            if(isset($valor['ComprobanteRechazo']) && $valor['ComprobanteRechazo']["id"]>0){
                if(isset($valor['ComprobanteRechazo']['TipoComprobante']))
                    $codigo_tipo_comprobante_rechazo = $valor['ComprobanteRechazo']['TipoComprobante']['codigo_tipo_comprobante'];
                else
                    $codigo_tipo_comprobante_rechazo = '';
                    
                $valor['Cheque']['d_comprobante_rechazo'] = $codigo_tipo_comprobante_rechazo.'-'.$comprobante->GetNumberComprobante(0,$valor['ComprobanteRechazo']['nro_comprobante']);    
            } 
            
              //$valor['Cheque']["conciliado"] = $this->Cheque->conciliado($valor['Cheque']['id']);
            
            $this->Cheque->formatearFechas($valor);
            
            
       
            
            
         
             
                   
             unset($valor['Banco']);
             unset($valor['BancoSucursal']);
             unset($valor['EstadoCheque']);
             unset($valor['ComprobanteEntrada']);
             unset($valor['ComprobanteSalida']);
             unset($valor['ComprobanteRechazo']);
             unset($valor['Chequera']);
             unset($valor['TipoCheque']);
             unset($valor['Moneda']);
           
    }
    
    
    /**
     * @secured(CONSULTA_CHEQUE)
     */
    public function Anular($id_cheque){
    	
    	$this->loadModel($this->model);
    	
    	$cheque = $this->Cheque->find('first',array('conditions'=>array('Cheque.id'=>$id_cheque)));
    	
    	
    	
    	$anular = 0; 
    	
    	if($cheque){
    		
    		$this->loadModel("ComprobanteValor");
    		
    		$comprobante_valor = $this->ComprobanteValor->find('all',array(
    				
    				
    				
    																		'conditions'=>array('ComprobanteValor.id_cheque'=>$id_cheque,'NOT'=>array('Comprobante.id_estado_comprobante'=>array(EnumEstadoComprobante::Anulado))),
    																		'contain'=>array('Comprobante'=>array('PuntoVenta','TipoComprobante'))
    																				
    																				
    																		)
    																				
    																				
    																				
    																		);
    				
    				
    				
    				
    	
    				
    		
    		
    		
    		if($cheque["Cheque"]["id_comprobante_entrada"]==null && $cheque["Cheque"]["id_comprobante_salida"]==null && $cheque["Cheque"]["id_comprobante_rechazo"]==null
    		
    				&& 		count($comprobante_valor)==0 && $cheque["Cheque"]["id_estado_cheque"] !=EnumEstadoCheque::Anulado
    				
    				){
    			
    		
    			
    			$anular = 1;
    			$message = "El Cheque se anul&oacute; correctamente";
    			$error = EnumError::SUCCESS;
    		}elseif(count($comprobante_valor)>0){
    			
    			$anular = 0;
    			
    			$message = '';
    			$this->loadModel("ComprobanteItem");
    			
    			$message = '';
    			foreach ($comprobante_valor as $comprobante){
    				
    				if(isset($comprobante["Comprobante"]["PuntoVenta"]["id"]) && $comprobante["Comprobante"]["PuntoVenta"]["id"]>0)
    					$message .= "&bull; ".$comprobante["Comprobante"]["TipoComprobante"]["codigo_tipo_comprobante"]." ".$this->ComprobanteItem->GetNumberComprobante($comprobante["Comprobante"]["PuntoVenta"]["numero"],$comprobante["Comprobante"]["nro_comprobante"])."\n";
    					else
    						$message .= "&bull; ".$comprobante["Comprobante"]["TipoComprobante"]["codigo_tipo_comprobante"]." ".$this->ComprobanteItem->GetNumberComprobante($comprobante["Comprobante"]["d_punto_venta"],$comprobante["Comprobante"]["nro_comprobante"])."\n";
    			}
    			
    		}
    		
    	}else{
    		
    		
    		$anular = 0;
    		
    		$message = "El cheque que elijio para eliminar no existe";
    		
    		
    	}
    	
    	if($anular == 1){
    		
    		$this->Cheque->updateAll(
    				array('Cheque.id_estado_cheque' => EnumEstadoCheque::Anulado),
    				array('Cheque.id' => $id_cheque)
    				);
    	}else{
    		
    		$message = "El cheque no puede ser anulado porque esta presente en los siguientes comprobantes:".$message;
    		$error = EnumError::ERROR;
    	}
    	
    	$output = array(
    			"status" => $error,
    			"message" => $message,
    			"content" => ""
    	);
    	echo json_encode($output);
    	die();
    			
    	
    }
    
    
    
    /**
     * @secured(CONSULTA_CHEQUE)
     */
    public function Conciliar($id_cheque){
    	
    	$this->loadModel($this->model);
    	
    	$cheque = $this->Cheque->find('first',array('conditions'=>array('Cheque.id'=>$id_cheque)));
    	
    	
    	
    	$error = EnumError::SUCCESS;
    	$message = "";
    	
    	if($cheque && $cheque["Cheque"]["conciliado"] == 0  ){
    		
    		
    		if($cheque["Cheque"]["fecha_cheque"] > date("Y-m-d") ){
    	
    			
    			$error = EnumError::ERROR;
    			$message = "No es posible conciliar un cheque con fecha menor a la actual";
    		}
    		
    		
    		if($cheque["Cheque"]["id_tipo_cheque"] == EnumTipoCheque::Propio && ($cheque["Cheque"]["id_comprobante_salida"] == null || is_null($cheque["Cheque"]["id_comprobante_salida"]) ) ){
    			
    			$error = EnumError::ERROR;
    			$message = "No es posible conciliar un cheque propio sin comprobante de salida";
    		}
    		
    		
    		
    		if($cheque["Cheque"]["id_tipo_cheque"] == EnumTipoCheque::Terceros  && ($cheque["Cheque"]["id_comprobante_salida"] == null || is_null($cheque["Cheque"]["id_comprobante_salida"])
    				
    				|| $cheque["Cheque"]["id_comprobante_entrada"] == null || is_null($cheque["Cheque"]["id_comprobante_entrada"])
    				
    				) ){
    			
    			$error = EnumError::ERROR;
    			$message = "No es posible conciliar un cheque de terceros sin comprobante de entrada o de salida";
    		}
    		
    		
    		
    		
    		
    		
    		
    		
    	}else{
    		
    		$error = EnumError::ERROR;
    		$message = "El cheque ya se encuentra conciliado";
    	}
    	
    	
    	if($error == EnumError::SUCCESS){/*actualizo el cheque ya que se puede conciliar*/
    		
    		$this->Cheque->updateAll(
    				array('Cheque.conciliado' => 1),
    				array('Cheque.id' => $id_cheque)
    				) ;
    		
    		$error = EnumError::SUCCESS;
    		$message = "El cheque se concili&oacute; correctamente";
    	}
    	
    	
    	$output = array(
    			"status" => $error,
    			"message" => $message,
    			"content" => "",
    			"page_count"=>0
    	);
    	
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message","page_count", "content"));
    	
    	
    	
    }
    
    
    private function existe_cheque_propio(){
    	
    	$this->loadModel($this->model);
    	$this->loadModel("Chequera");
    	/*Si el cheque es propio y lo envia entonces se debe chequear que no este dado de alta*/

    	if(strlen(trim($this->request->data["Cheque"]["nro_cheque"]))>0 && $this->request->data["Cheque"]["id_tipo_cheque"] == EnumTipoCheque::Propio){
    		
    		
    		$conditions = array();
    		array_push($conditions, array('Cheque.nro_cheque' =>  trim($this->request->data["Cheque"]["nro_cheque"])));
    		array_push($conditions, array('Cheque.id_chequera'=>$this->request->data["Cheque"]["id_chequera"]));
    		array_push($conditions, array('NOT'=>array('Cheque.id_estado_cheque'=>EnumEstadoCheque::Anulado)));
    		array_push($conditions, array('Cheque.id_tipo_cheque'=>EnumTipoCheque::Propio));
    		
    		
    		if(isset($this->request->data["Cheque"]["id"]) && $this->request->data["Cheque"]["id"]>0){
    			
    			array_push($conditions, array('NOT'=>array('Cheque.id'=>$this->request->data["Cheque"]["id"])));
    		}
    		
    		
    		$cheque = $this->Cheque->find('count',
    				array('conditions'=>$conditions,
    										'contain'=>array('Chequera')));
    		
    		if($cheque > 0)
    			return 1;
    		else{//chequeo que el cheque sea un cheque valido de la cartera
    			
    			
    			$chequera = $this->Chequera->find('first',
    					array('conditions'=>array('Chequera.id'=>trim($this->request->data["Cheque"]["id_chequera"]))
    							
    							
    					)
    					
    					);
    			
    			
    			if($this->request->data["Cheque"]["nro_cheque"]>=$chequera["Chequera"]["cheque_desde"] && $this->request->data["Cheque"]["nro_cheque"]<=$chequera["Chequera"]["cheque_hasta"])
    				return 0;//el numero de cheque que ingreso es valido y esta dentro de los limites de la cartera
    			else 
    				return 1;
    		}
    		
    	}else{
    		
    		return 0;//en este caso no me envia el nro_cheque entonces debo asignarle uno valido
    	}
    	
    	
    }
    
   
   
    
    
       
}
?>