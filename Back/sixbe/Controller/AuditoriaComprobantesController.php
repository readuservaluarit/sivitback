<?php


class AuditoriaComprobantesController extends AppController {
    public $name = 'AuditoriaComprobantes';
    public $model = 'AuditoriaComprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
/**
* @secured(CONSULTA_AUDITORIA_COMPROBANTE)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
       
       
        $nro_comprobante = $this->getFromRequestOrSession('AuditoriaComprobante.nro_comprobante');
        $id_comprobante = $this->getFromRequestOrSession('AuditoriaComprobante.id_comprobante');
        $id_punto_venta = $this->getFromRequestOrSession('AuditoriaComprobante.id_punto_venta');
        $id_persona = $this->getFromRequestOrSession('AuditoriaComprobante.id_persona');
        $id_estado_comprobante = $this->getFromRequestOrSession('AuditoriaComprobante.id_estado_comprobante');
        $razon_social = $this->getFromRequestOrSession('AuditoriaComprobante.razon_social');
        $id_tipo_comprobante= $this->getFromRequestOrSession('AuditoriaComprobante.id_tipo_comprobante');
        $fecha_desde = $this->getFromRequestOrSession('AuditoriaComprobante.fecha_desde');
        $hora_desde = $this->getFromRequestOrSession('AuditoriaComprobante.hora_desde');
        $fecha_hasta = $this->getFromRequestOrSession('AuditoriaComprobante.fecha_hasta');
        $hora_hasta = $this->getFromRequestOrSession('AuditoriaComprobante.hora_hasta');
        $id_usuario= $this->getFromRequestOrSession('AuditoriaComprobante.id_usuario');
        
        $conditions = array(); 
        
        
        
        if($id_comprobante!='')
        	array_push($conditions,array('AuditoriaComprobante.id_comprobante' =>$id_comprobante));
        
        if($nro_comprobante!='')
        	array_push($conditions,array('AuditoriaComprobante.nro_comprobante' =>$nro_comprobante));
        
        if($id_punto_venta!='')
        	array_push($conditions,array('AuditoriaComprobante.id_punto_venta' =>$id_punto_venta));
        
        	
        if($id_persona!='')
        	array_push($conditions,array('Comprobante.id_persona' =>$id_persona));
        
        if($id_tipo_comprobante!='')
        	array_push($conditions,array('Comprobante.id_tipo_comprobante' =>$id_tipo_comprobante));
        
        
        if($id_estado_comprobante!='')
        	array_push($conditions,array('Comprobante.id_estado_comprobante' =>$id_estado_comprobante));
        
        
        if($razon_social!="")
        	array_push($conditions, array('Comprobante.razon_social LIKE ' => '%' .$razon_social.'%'));   
        
        	
        	
        if($fecha_desde!="")
        	array_push($conditions, array('AuditoriaComprobante.fecha >= ' =>  $fecha_desde.' '.$hora_desde));
        		
        if($fecha_hasta!="")
        	array_push($conditions, array('AuditoriaComprobante.fecha <= ' =>  $fecha_hasta.' '.$hora_hasta)); 
        
        if($id_usuario!="")
        	array_push($conditions, array('AuditoriaComprobante.id_usuario' =>  $id_usuario)); 
        
/*
        	
        if($hora_desde!="")
        	array_push($conditions, array('DATE_FORMAT(AuditoriaComprobante.fecha, "%H:%I" ) >=' => $hora_desde));
        		
        		
        if($hora_hasta!="")
        	array_push($conditions, array('DATE_FORMAT(AuditoriaComprobante.fecha, "%H:%I" )<=' => $hora_hasta));
        
        */	
        			
        	
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
        	'order'=>'AuditoriaComprobante.id desc',
        	'contain'=>array('Comprobante'=>array('Persona','TipoComprobante'),'EstadoComprobanteNuevo','Usuario','PuntoVenta')
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de AuditoriaComprobantes', 'Datos de las AuditoriaComprobantes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_AuditoriaComprobante', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $d_AuditoriaComprobante));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'AuditoriaComprobante.id', "10%");
                $formBuilder->addHeader('Nombre', 'AuditoriaComprobante.d_AuditoriaComprobante', "50%");
                $formBuilder->addHeader('Nombre', 'AuditoriaComprobante.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_AuditoriaComprobante');
                $formBuilder->addField($this->model, 'cotizacion');
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $this->loadModel("Comprobante");
        
        $data = $this->PaginatorModificado->paginate($this->model);
        
        foreach($data as &$valor){
        	
        	
        	if(isset($valor["PuntoVenta"]["d_punto_venta"]) && $valor["PuntoVenta"]["id"]>0)
        		$valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor["Comprobante"]['nro_comprobante']);
        	else
        		$valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['AuditoriaComprobante']['d_punto_venta'],$valor["Comprobante"]['nro_comprobante']);
        	
        
        	
        	
        	$valor["AuditoriaComprobante"]["d_estado"] = $valor["EstadoComprobanteNuevo"]["d_estado_comprobante"];
        	
        	
        	if(isset($valor["Comprobante"]["TipoComprobante"]["id"]) && $valor["Comprobante"]["TipoComprobante"]["id"]>0)
        		$valor["AuditoriaComprobante"]["d_tipo_comprobante"] = $valor["Comprobante"]["TipoComprobante"]["codigo_tipo_comprobante"];
        	
        	
        	$valor["AuditoriaComprobante"]["d_usuario"] = $valor["Usuario"]["username"];
        	
        	if(isset($valor["PuntoVenta"]["d_punto_venta"]) && $valor["PuntoVenta"]["id"]>0)
        		$valor["AuditoriaComprobante"]["d_punto_venta"] = $valor["PuntoVenta"]["d_punto_venta"];
        	
           
        		$valor["AuditoriaComprobante"]["fecha"] = date("d-m-Y H:i:s", strtotime($valor["AuditoriaComprobante"]["fecha"]));
        	
        	unset($valor["Comprobante"]);
        	unset($valor["EstadoComprobanteNuevo"]);
        	unset($valor["Usuario"]);
        	unset($valor["PuntoVenta"]);


        	
        	
        }
        
        
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->AuditoriaComprobante->id = $id;
            $this->request->data = $this->AuditoriaComprobante->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('AuditoriaComprobante');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de AuditoriaComprobante');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de AuditoriaComprobante');
        else
            $formBuilder->setTituloForm('Consulta de AuditoriaComprobante');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_AuditoriaComprobante', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('cotizacion', 'Cotizaci&oacute;n', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));    
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'AuditoriaComprobanteAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('AuditoriaComprobanteDAuditoriaComprobante', 'Debe ingresar un nombre');
        validator.validateRequired('AuditoriaComprobanteCotizacion', 'Debe ingresar una cotizaci&oacute;n');


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_AuditoriaComprobante)
    */
    public function add() {
    	if ($this->request->is('post')){
    		$this->loadModel($this->model);
    		$id_cat = '';
    		
    		try{
    			if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
    				$mensaje = "El AuditoriaComprobante ha sido creado exitosamente";
    				$tipo = EnumError::SUCCESS;
    				$id_cat = $this->{$this->model}->id;
    				
    			}else{
    				$errores = $this->{$this->model}->validationErrors;
    				$errores_string = "";
    				foreach ($errores as $error){
    					$errores_string.= "&bull; ".$error[0]."\n";
    					
    				}
    				$mensaje = $errores_string;
    				$tipo = EnumError::ERROR;
    				
    			}
    		}catch(Exception $e){
    			
    			$mensaje = "Ha ocurrido un error,el AuditoriaComprobante no ha podido ser creado.".$e->getMessage();
    			$tipo = EnumError::ERROR;
    		}
    		$output = array(
    				"status" => $tipo,
    				"message" => $mensaje,
    				"content" => "",
    				"id_add" =>$id_cat
    		);
    		//si es json muestro esto
    		if($this->RequestHandler->ext == 'json'){
    			$this->set($output);
    			$this->set("_serialize", array("status", "message", "content" ,"id_add"));
    		}else{
    			
    			$this->Session->setFlash($mensaje, $tipo);
    			$this->redirect(array('action' => 'index'));
    		}
    		
    	}
    	
    	//si no es un post y no es json
    	if($this->RequestHandler->ext != 'json')
    		$this->redirect(array('action' => 'abm', 'A'));
    		
    		
    }

    /**
    * @secured(MODIFICACION_AuditoriaComprobante)
    */
     
    public function edit($id) {
    	
    	
    	
    	
    	$this->loadModel($this->model);
    	$this->{$this->model}->id = $id;
    	
    	try{
    		if ($this->{$this->model}->saveAll($this->request->data)){
    			$mensaje =  "El Talle ha sido modificada exitosamente";
    			$status = EnumError::SUCCESS;
    			
    			
    		}else{   //si hubo error recupero los errores de los models
    			
    			$errores = $this->{$this->model}->validationErrors;
    			$errores_string = "";
    			foreach ($errores as $error){ //recorro los errores y armo el mensaje
    				$errores_string.= "&bull; ".$error[0]."\n";
    				
    			}
    			$mensaje = $errores_string;
    			$status = EnumError::ERROR;
    		}
    		
    		
    	}catch(Exception $e){
    		$status = EnumError::ERROR;
    		$mensaje = $e->getMessage();
    		
    	}
    	
    	$output = array(
    			"status" => $status,
    			"message" => $mensaje,
    			"content" => ""
    	);
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message", "content"));
    	
    	
    }
    /**
    * @secured(BAJA_AuditoriaComprobante)
    */
    function delete($id) {
    	$this->loadModel($this->model);
    	$mensaje = "";
    	$status = "";
    	$this->{$this->model}->id = $id;
    	
    	try{
    		if ($this->{$this->model}->delete() ) {
    			
    			//$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
    			
    			$status = EnumError::SUCCESS;
    			$mensaje = "El AuditoriaComprobante ha sido eliminado exitosamente.";
    			$output = array(
    					"status" => $status,
    					"message" => $mensaje,
    					"content" => ""
    			);
    			
    		}
    		
    		else
    			throw new Exception();
    			
    	}catch(Exception $ex){
    		//$this->Session->setFlash($ex->getMessage(), 'error');
    		
    		$status = EnumError::ERROR;
    		$mensaje = $ex->getMessage();
    		$output = array(
    				"status" => $status,
    				"message" => $mensaje,
    				"content" => ""
    		);
    	}
    	
    	if($this->RequestHandler->ext == 'json'){
    		$this->set($output);
    		$this->set("_serialize", array("status", "message", "content"));
    		
    	}else{
    		$this->Session->setFlash($mensaje, $status);
    		$this->redirect(array('controller' => $this->name, 'action' => 'index'));
    		
    	}
    	
    	
    	
    	
    	
    }
    
     /**
    * @secured(CONSULTA_AUDITORIA_COMPROBANTE)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
        
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        
            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);
        
               
        
        
      
    }
    
    /**
     * @secured(BTN_EXCEL_AUDITORIACOMPROBANTES)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Auditoria");
    	
    	
    	
    }



}
?>