<?php

             include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');
             
             App::import('Model','Comprobante');
			$comprobante = new Comprobante();
			$produccion = $comprobante->getActivoModulo(EnumModulo::PRODUCCION);
        
			$model["pto_nro_comprobante"]["show_grid"] = 1;
            $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
            $model["pto_nro_comprobante"]["order"] = "0";
            $model["pto_nro_comprobante"]["width"] = "9";
			
		
              
            $model["n_item"]["show_grid"] = 1;
            $model["n_item"]["header_display"] = "Nº Item";
            $model["n_item"]["order"] = "1";
            $model["n_item"]["width"] = "2";
            
            $model["codigo"]["show_grid"] = 1;
            $model["codigo"]["header_display"] = "C&oacute;digo";
            $model["codigo"]["order"] = "2";
            $model["codigo"]["width"] = "9";
			$model["codigo"]["text_colour"] = "#000099";//BLUE
			
			
			$model["pto_nro_pedido_interno"]["show_grid"] = 1;
            $model["pto_nro_pedido_interno"]["header_display"] = "Ped. Int";
            $model["pto_nro_pedido_interno"]["order"] = "3";
            $model["pto_nro_pedido_interno"]["width"] = "9";
			$model["pto_nro_pedido_interno"]["text_colour"] = "#000099";//BLUE

            
            $model["fecha_generacion"]["show_grid"] = 1;
            $model["fecha_generacion"]["header_display"] = "Fch Generaci&oacute;n";
            $model["fecha_generacion"]["order"] = "4";
            $model["fecha_generacion"]["width"] = "8";
            
			$model["fecha_entrega"]["show_grid"] = 1;
            $model["fecha_entrega"]["header_display"] = "Fch Entrega a Armado";
            $model["fecha_entrega"]["order"] = "5";
            $model["fecha_entrega"]["width"] = "7";
			$model["fecha_entrega"]["text_colour"] = "#AA1100";//RED GAO
			
			
			$model["fecha_limite_armado"]["show_grid"] = 1;
            $model["fecha_limite_armado"]["header_display"] = "Fch. Limite Armado";
            $model["fecha_limite_armado"]["order"] = "6";
            $model["fecha_limite_armado"]["width"] = "7";
			$model["fecha_limite_armado"]["text_colour"] = "#AA1100";//RED GAO
			
			
			$model["fecha_entrega_origen"]["show_grid"] = 1;
            $model["fecha_entrega_origen"]["header_display"] = "Fch. Entrega Item Pedido";
            $model["fecha_entrega_origen"]["order"] = "7";
            $model["fecha_entrega_origen"]["width"] = "7";
			$model["fecha_entrega_origen"]["text_colour"] = "#AA1100";//RED GAO
			
			
			
         

            
            $model["cantidad"]["show_grid"] = 1;
            $model["cantidad"]["header_display"] = "Cantidad Planif.";
            $model["cantidad"]["order"] = "10";            
            $model["cantidad"]["width"] = "5";
			$model["cantidad"]["text_colour"] = "#0000FF";//BLUE
			
			
			$model["cantidad_cierre"]["show_grid"] = 1;
            $model["cantidad_cierre"]["header_display"] = "Cantidad.";
            $model["cantidad_cierre"]["order"] = "11";            
            $model["cantidad_cierre"]["width"] = "5";
			$model["cantidad_cierre"]["text_colour"] = "#0000FF";//BLUE
			
			
		

			
            $model["d_estado_comprobante"]["show_grid"] = 1;
            $model["d_estado_comprobante"]["header_display"] = "Estado";
            $model["d_estado_comprobante"]["order"] = "13";            
            $model["d_estado_comprobante"]["width"] = "9";
            $model["d_estado_comprobante"]["text_colour"] = "#FF0000";//RED

 $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItem",
                    "content" => $model
                );
        
          echo json_encode($output);
?>