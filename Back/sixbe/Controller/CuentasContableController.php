<?php


    class CuentasContableController extends AppController {
        public $name = 'CuentasContable';
        public $model = 'CuentaContable';
        public $helpers = array ('Session', 'Paginator', 'Js');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

        /**
        * @secured(CONSULTA_CUENTA_CONTABLE)
        */
        public function index($id_producto = null) {



            $this->loadModel($this->model);

            if($this->request->is('ajax'))
                $this->layout = 'ajax';


            $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);

            //Recuperacion de Filtros
            $id = $this->getFromRequestOrSession('CuentaContable.id');

            $codigo =  $this->getFromRequestOrSession('CuentaContable.codigo');
            $d_cuenta_contable =  $this->getFromRequestOrSession('CuentaContable.d_cuenta_contable');
            
            
            $habilitada =  $this->getFromRequestOrSession('CuentaContable.habilitada');
            /*if($habilitada=='')
                $habilitada =  '1'; // por defecto buscar habilitadas para el combobox*/
                
            $id_clase_cuenta_contable =  $this->getFromRequestOrSession('CuentaContable.id_clase_cuenta_contable');
            $id_tipo_cuenta_contable =  $this->getFromRequestOrSession('CuentaContable.id_tipo_cuenta_contable');
            $get_tree =  $this->getFromRequestOrSession('CuentaContable.get_tree');  //define el filtro si devuelvo  arbol o no  
     
            //$get_tree = 1;//COMENTAR SOLO TESTING

            if($get_tree == ''){
                $get_tree = 0;
            }
            


            $conditions = array(); 

            if($id!='')
                array_push($conditions, array('CuentaContable.id' => $id)); 

            if($codigo!=''){
                array_push($conditions, array('CuentaContable.codigo' => $codigo)); 

            }

            if($d_cuenta_contable!='' && $id=="")
                array_push($conditions, array('CuentaContable.d_cuenta_contable' => $d_cuenta_contable)); 

            if($habilitada!='')
            //array_push($conditions, array('CuentaContable.habilitada ' => '1')); 
               array_push($conditions, array('CuentaContable.habilitada ' => $habilitada)); 

            if($id_clase_cuenta_contable!='')
                array_push($conditions, array('CuentaContable.id_clase_cuenta_contable' => $id_clase_cuenta_contable));

            if($id_tipo_cuenta_contable!='')
                array_push($conditions, array('CuentaContable.id_tipo_cuenta_contable' => $id_tipo_cuenta_contable));
          
            /*
            $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
                'contain' => false,
                'conditions' => $conditions,
                'order'=>'codigo ASC',
                'limit' => 1000, //estoo es solo para traer todos las materias primas
                'page' => $this->getPageNum()
            );

            */

            // vista json
            //$this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
            $page_count = $this->params['paging'][$this->model]['pageCount'];
            //$this->loadModel("Deposito");
        
           
            //$data = $this->CuentaContable->generateTreeList();
            if($get_tree == 1) {
                $data = $this->CuentaContable->find('threaded', array(
                        'conditions'=>$conditions,   
                        'contain' => array(),                        // 'fields' => array('id', 'CuentaContable.parent_id'),
                        'order' => array('CuentaContable.codigo ASC'), // or array('id ASC')
                         // or array('id ASC')
                    ));
                    
                $this->CuentaContable->BorroAsociados($data);    
                //$this->CalculaSaldo($data);    

            }else{

                $data = $this->CuentaContable->find('all', array(
                        // 'fields' => array('id', 'CuentaContable.parent_id'),
                         'contain' => array(),
                        'conditions'=>$conditions,
                        'order' => array('CuentaContable.codigo ASC'), // or array('id ASC')
                        // or array('id ASC')
                    )); 


                  /*
                foreach($data as &$record)
                {        
                    //Toda la logica de mapeo se basan en el model. por eso se subu un nivel a [AsientoCuentaContable ]
                    $record["CuentaContable"]["d_codigo_cuenta_contable_2"] =     "MAURO";// + (string)($record["CuentaContable"]["codigo"]   + $record["CuentaContable"]["d_cuenta_contable"]); 

                }        */
            }
            
            
            
            


            /*
            foreach($data as &$prodcom){


            $get_tree =0;       
            if($get_tree ==1){       

            $data_arbol = $this->CuentaContable->find('all', array(
            'conditions' => array('CuentaContable.parent_id' => $prodcom["CuentaContable"]["id"]),
            'contain' =>false
            ));

            $prodcom["CuentaContable"]["children"] = $data_arbol;


            }

            // $prodcom["CuentaContable"]["d_codigo_cuenta_contable"] =    $prodcom["CuentaContable"]["codigo"].'-'.$prodcom["CuentaContable"]["d_cuenta_contable"];



            }*/

            $output = array(
                "status" =>EnumError::SUCCESS,
                "message" => "list",
                "content" => $data,
                "page_count" =>$page_count
            );
            $this->set($output);
            $this->set("_serialize", array("status", "message","page_count", "content"));
            //fin vista json

        }

        /**
        * @secured(ABM_USUARIOS)
        */
        public function abm($mode, $id = null) {
            if ($mode != "A" && $mode != "M" && $mode != "C")
                throw new MethodNotAllowedException();

            $this->layout = 'ajax';
            $this->loadModel($this->model);
            //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);

            if ($mode != "A"){
                $this->Componente->id = $id;
                $this->Componente->contain();
                $this->request->data = $this->Componente->read();

            } 


            App::import('Lib', 'FormBuilder');
            $formBuilder = new FormBuilder();
            //Configuracion del Formulario

            $formBuilder->setController($this);
            $formBuilder->setModelName($this->model);
            $formBuilder->setControllerName($this->name);
            $formBuilder->setTitulo('Componente');

            if ($mode == "A")
                $formBuilder->setTituloForm('Alta de Componentes');
            elseif ($mode == "M")
                $formBuilder->setTituloForm('Modificaci&oacute;n de Componentes');
            else
                $formBuilder->setTituloForm('Consulta de Componente');



            //Form
            $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

            $formBuilder->addFormBeginRow();
            //Fields
            $formBuilder->addFormInput('codigo', 'C&oacute;digo', array('class'=>'control-group'), array('class' => '', 'label' => false));
            $formBuilder->addFormInput('descripcion', 'Descripci&oacute;n', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
            $formBuilder->addFormInput('costo', 'Costo', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
            $formBuilder->addFormInput('stock', 'Stock', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
            $formBuilder->addFormInput('stock_minimo', 'Stock M&iacute;nimo', array('class'=>'control-group'), array('class' => 'required', 'label' => false));




            $formBuilder->addFormEndRow();   

            $script = "

            function validateForm(){
            Validator.clearValidationMsgs('validationMsg_');

            var form = 'ComponenteAbmForm';
            var validator = new Validator(form);

            validator.validateRequired('ComponenteCodigo', 'Debe ingresar un C&oacute;digo');
            validator.validateRequired('ComponenteDescripcion', 'Debe ingresar una descripcion');
            validator.validateNumeric('ComponenteStock', 'El stock ingresado debe ser num&eacute;rico');
            validator.validateNumeric('ComponenteStockMinimo', 'El stock ingresado debe ser num&eacute;rico');
            validator.validateRequired('ComponenteCosto', 'El costo ingresado debe ser num&eacute;rico');
            //Valido si el Cuit ya existe
            $.ajax({
            'url': './".$this->name."/existe_codigo',
            'data': 'codigo=' + $('#ComponenteCodigo').val()+'&id='+$('#ComponenteId').val(),
            'async': false,
            'success': function(data) {

            if(data !=0){

            validator.addErrorMessage('ComponenteCodigo','El C&oacute;digo que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
            validator.showValidations('', 'validationMsg_');
            return false; 
            }


            }
            }); 


            if(!validator.isAllValid()){
            validator.showValidations('', 'validationMsg_');
            return false;   
            }
            return true;

            } 


            ";

            $formBuilder->addFormCustomScript($script);

            $formBuilder->setFormValidationFunction('validateForm()');




            $this->set('abm',$formBuilder);
            $this->set('mode', $mode);
            $this->set('id', $id);
            $this->render('/FormBuilder/abm');   

        }

        /**
        * @secured(ABM_USUARIOS)
        */
        public function view($id) {        
            $this->redirect(array('action' => 'abm', 'C', $id)); 
        }

        /**
        * @secured(ADD_CUENTA_CONTABLE)
        */
        public function add() {
            if ($this->request->is('post')){
                $this->loadModel("CuentaContableAdd"); //Se usa este model xq el Tree generaba problemas


                $data = array();
                $data["CuentaContableAdd"]['parent_id'] = $this->request->data["CuentaContable"]['parent_id'];
                $data["CuentaContableAdd"]['codigo'] = $this->request->data["CuentaContable"]['codigo'];
                $data["CuentaContableAdd"]['d_cuenta_contable'] = $this->request->data["CuentaContable"]['d_cuenta_contable'];
                $data["CuentaContableAdd"]['id_moneda'] = $this->request->data["CuentaContable"]['id_moneda'];
                $data["CuentaContableAdd"]['saldo_habitual_deudor'] = $this->request->data["CuentaContable"]['saldo_habitual_deudor'];
                $data["CuentaContableAdd"]['habilitada'] = $this->request->data["CuentaContable"]['habilitada'];
                $data["CuentaContableAdd"]['id_clase_cuenta_contable'] = $this->request->data["CuentaContable"]['id_clase_cuenta_contable'];
                $data["CuentaContableAdd"]['id_modulo'] = $this->request->data["CuentaContable"]['id_modulo'];
                $data["CuentaContableAdd"]['observacion'] = $this->request->data["CuentaContable"]['observacion'];
                $data["CuentaContableAdd"]['utiliza_centro_costos'] = $this->request->data["CuentaContable"]['utiliza_centro_costos'];
                
                if(isset($this->request->data["CuentaContableCentroCosto"]))
                    $data["CuentaContableCentroCosto"] = $this->request->data["CuentaContableCentroCosto"];



                $id_add = ''; 
                try{
                    if ($this->CuentaContableAdd->saveAll($data,array("deep"=>true))){
                        $mensaje = "La Cuenta Contable ha sido creada exitosamente";
                        $tipo = EnumError::SUCCESS;
                        $id_add = $this->CuentaContableAdd->id;

                    }else{
                        $mensaje = "Ha ocurrido un error,La Cuenta Contable no ha podido ser creada.";

                        $errores = $this->CuentaContableAdd->validationErrors;
                        $errores_string = "";
                        foreach ($errores as $error){
                            $errores_string.= "&bull; ".$error[0]."\n";

                        }
                        $mensaje = $errores_string;
                        $tipo = EnumError::ERROR; 

                    }
                }catch(Exception $e){

                    $mensaje = "Ha ocurrido un error,La Cuenta Contable no ha podido ser asiganda.".$e->getMessage();
                    $tipo = EnumError::ERROR;
                }
                $output = array(
                    "status" => $tipo,
                    "message" => $mensaje,
                    "content" => "",
                    "id_add" => $id_add
                );
                //si es json muestro esto
                if($this->RequestHandler->ext == 'json'){ 
                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content","id_add"));
                }else{

                    $this->Session->setFlash($mensaje, $tipo);
                    $this->redirect(array('action' => 'index'));
                }     

            }

            //si no es un post y no es json
            if($this->RequestHandler->ext != 'json')
                $this->redirect(array('action' => 'abm', 'A'));   
        }


        /**
        * @secured(MODIFICACION_CUENTA_CONTABLE)
        */
        function edit($id) {

            if (!$this->request->is('get')){

                $this->loadModel("CuentaContableAdd");
                $this->CuentaContableAdd->id = $id;
                
                $data = array();

                $data["CuentaContableAdd"]['parent_id'] = $this->request->data["CuentaContable"]['parent_id'];
                $data["CuentaContableAdd"]['id'] = $id;
                $data["CuentaContableAdd"]['codigo_cuenta_contable'] = $this->request->data["CuentaContable"]['codigo'];
                $data["CuentaContableAdd"]['codigo'] = $this->request->data["CuentaContable"]['codigo'];
                $data["CuentaContableAdd"]['d_cuenta_contable'] = $this->request->data["CuentaContable"]['d_cuenta_contable'];
                $data["CuentaContableAdd"]['id_moneda'] = $this->request->data["CuentaContable"]['id_moneda'];
                $data["CuentaContableAdd"]['saldo_habitual_deudor'] = $this->request->data["CuentaContable"]['saldo_habitual_deudor'];
                $data["CuentaContableAdd"]['habilitada'] = $this->request->data["CuentaContable"]['habilitada'];
                $data["CuentaContableAdd"]['id_clase_cuenta_contable'] = $this->request->data["CuentaContable"]['id_clase_cuenta_contable'];
                $data["CuentaContableAdd"]['id_modulo'] = $this->request->data["CuentaContable"]['id_modulo'];
                $data["CuentaContableAdd"]['observacion'] = $this->request->data["CuentaContable"]['observacion'];
                $data["CuentaContableAdd"]['utiliza_centro_costos'] = $this->request->data["CuentaContable"]['utiliza_centro_costos'];
                
                if(isset($this->request->data["CuentaContableCentroCosto"]))
                    $data["CuentaContableCentroCosto"] = $this->request->data["CuentaContableCentroCosto"];
               
                try{ 
                    if ($this->CuentaContableAdd->saveAll($data,array("deep"=>true))){
                        $mensaje =  "La Cuenta Contable ha sido modificada exitosamente";
                        $status = EnumError::SUCCESS;


                    }else{   //si hubo error recupero los errores de los models

                        $errores = $this->CuentaContableAdd->validationErrors;
                        $errores_string = "";
                        foreach ($errores as $error){ //recorro los errores y armo el mensaje
                            $errores_string.= "&bull; ".$error[0]."\n";

                        }
                        $mensaje = $errores_string;
                        $status = EnumError::ERROR; 
                    }


                }catch(Exception $e){
                    $mensaje = 'Ha ocurrido un error, El Tipo de Movimiento en el deposito no ha podido modificarse.'.$e->getMessage();
                    $status = "error" ;
                    $this->Session->setFlash($mensaje, 'error');

                }


                if($this->RequestHandler->ext == 'json'){  
                    $output = array(
                        "status" => $status,
                        "message" => $mensaje,
                        "content" => ""
                    ); 

                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content"));
                }else{
                    $this->Session->setFlash($mensaje, $status);
                    $this->redirect(array('controller' => $this->name, 'action' => 'index'));

                } 

            }else{ //si me pide algun dato me debe mandar el id
                if($this->RequestHandler->ext == 'json'){ 

                    $this->{$this->model}->id = $id;
                    //$this->{$this->model}->contain('Unidad');
                    $this->request->data = $this->{$this->model}->read();          

                    $output = array(
                        "status" => $status,
                        "message" => $mensaje,
                        "content" => ""
                    ); 

                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content"));
                }else{

                    $this->redirect(array('action' => 'abm', 'M', $id));

                }


            } 

            if($this->RequestHandler->ext != 'json')
                $this->redirect(array('action' => 'abm', 'M', $id));
        }

        /**
        * @secured(BAJA_CUENTA_CONTABLE)
        */
        function delete($id) {

            $this->loadModel("CuentaContableAdd");
            $mensaje = "";
            $status = "";
            $this->CuentaContableAdd->id = $id;
            try{
                if ($this->CuentaContableAdd->delete()) {

                    //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');

                    $status = EnumError::SUCCESS;
                    $mensaje = "La Cuenta Contable ha sido eliminada exitosamente.";
                    $output = array(
                        "status" => $status,
                        "message" => $mensaje,
                        "content" => ""
                    ); 

                }

                else
                    throw new Exception();

            }catch(Exception $ex){ 
                //$this->Session->setFlash($ex->getMessage(), 'error');

                $status = EnumError::ERROR;
                $mensaje = $ex->getMessage();
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
            }

            if($this->RequestHandler->ext == 'json'){
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));

            }else{
                $this->Session->setFlash($mensaje, $status);
                $this->redirect(array('controller' => $this->name, 'action' => 'index'));

            }





        }



        /**
        * @secured(CONSULTA_CUENTA_CONTABLE)
        */

        public function existe_codigo(){


            
            
        	$id_cuenta_contable =  $this->getFromRequestOrSession($this->model.'.id');
            $codigo = $this->getFromRequestOrSession($this->model.'.codigo_cuenta_contable');

            $this->loadModel("CuentaContable");

            $existe = $this->CuentaContable->find('count',array('conditions' => array(
                        'CuentaContable.codigo' => $codigo,
                        'CuentaContable.id !=' =>$id_cuenta_contable
                    )
                ));
            
            $message = "";
            if($existe >0){
            	$error = EnumError::ERROR;
            	$message = "El c&oacute;digo de la cuenta contable que ingreso ya existe, es inv&aacute;lido";
            }else{
            	$error = EnumError::SUCCESS;
            }
            		
            $output = array(
            		"status" => $error,
            		"message" => $message,
            		"content" => "",
            		"message_type"=>EnumMessageType::Modal
            );
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content"));
        
        
        }

        /**
        * @secured(CONSULTA_CUENTA_CONTABLE)
        */
        /**
         * @secured(CONSULTA_STOCK)
         */
        public function getModel($vista='default'){
        	
        	$model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        	$model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
        	
        }
        
        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
        	
        	
        	$this->set('model',$model);
        	Configure::write('debug',0);
        	$this->render($vista);

        	
        }
        
        
     /**
        * @secured(BAJA_CUENTA_CONTABLE)
        */    
    public function deleteItem($id_item){
        
        $this->loadModel($this->model);
        
        
        
    
        
        
        
        $this->{$this->model}->CuentaContableCentroCosto->id = $id_item;
       try{
           
           //aca se debe chequear si el item es borrable. O sea sino se factura etc.
           
            if ($this->{$this->model}->CuentaContableCentroCosto->delete()) {
                
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Item ha sido eliminado exitosamente.";
              
            }else{
              
                
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
                
                
            }
        }
        catch(Exception $ex){ 
              $mensaje = "Ha ocurrido un error, el item no ha podido ser borrado";
               $status = EnumError::ERROR; 
        }
        
        
        
        
        if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        $this->output = $output;
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
        }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
        }
        
        
        
        
    } 
        
     
         
         
         
         
         
        

    }
?>