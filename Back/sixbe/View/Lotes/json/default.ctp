<?php
        // CONFIG  CON MODE DISPLAYED CELLS
        
        include(APP.'View'.DS.'Lotes'.DS.'json'.DS.'lote.ctp');
      
		$model["id"]["show_grid"] = 0; //MP: TENES Q SETEAR ACA en CERO PARA Q SEA INVISIBLE
        $model["id"]["header_display"] = "Nro Lote";
        $model["id"]["order"] = "0";
		$model["id"]["width"] = "6";
		
		$model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "Codigo";
        $model["codigo"]["order"] = "1";
		$model["codigo"]["width"] = "9";
		$model["codigo"]["read_only"] = "1";
		$model["codigo"]["text_colour"] = "#000099";
		$model["codigo"]["picker"] = EnumMenu::mnuLotePicker;
		//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
		
		$model["d_tipo_lote"]["show_grid"] = 1;
        $model["d_tipo_lote"]["header_display"] = "Tipo";
        $model["d_tipo_lote"]["order"] = "2";
		$model["d_tipo_lote"]["width"] = "4";
		$model["d_tipo_lote"]["read_only"] = "0";
		
		$model["d_lote"]["show_grid"] = 1;
        $model["d_lote"]["header_display"] = "Descripcion";
        $model["d_lote"]["order"] = "3";
		$model["d_lote"]["width"] = "13";
		$model["d_lote"]["read_only"] = "0";
		
		$model["d_tiene_certificado"]["show_grid"] = 1;
        $model["d_tiene_certificado"]["header_display"] = "Cert.";
        $model["d_tiene_certificado"]["order"] = "4";
		$model["d_tiene_certificado"]["width"] = "3";
		$model["d_tiene_certificado"]["read_only"] = "0";
		
		$model["d_validado"]["show_grid"] = 1;
        $model["d_validado"]["header_display"] = "Validado";
        $model["d_validado"]["order"] = "5";
		$model["d_validado"]["width"] = "4";
		
		$output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Lote",
                    "content" => $model
                );
        
          echo json_encode($output);
?>