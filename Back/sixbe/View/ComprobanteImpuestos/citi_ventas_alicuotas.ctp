<?php


    App::import('Model','Comprobante'); 
    App::import('Model','DatoEmpresa'); 
    
    if($nombre_archivo  == "")
        $nombre_archivo = "CitiVentasAlicuotas";
        
    header("Content-Disposition: attachment; filename=".$nombre_archivo.".txt");
    header("Pragma: no-cache"); 
    header("Expires: 0");
    
    $comprobante_obj  = new Comprobante();
    $dato_empresa_obj  = new DatoEmpresa();
    
    $id_tipo_moneda = $dato_empresa_obj->getNombreCampoMonedaPorIdMoneda(EnumMoneda::Peso);
    $field_valor_moneda = $comprobante_obj->getFieldByTipoMoneda($id_tipo_moneda);
    
    
   
    
    
    
    
    foreach($datos as $comprobante_impuesto){
        
  
     
       $alicuota_iva_afip_05 =  str_pad($comprobante_impuesto["ComprobanteImpuesto"]["tipo_alicuota_afip"], 4, '0', STR_PAD_LEFT);
       $tipo_comprobante_afip_01 = str_pad($comprobante_impuesto["Comprobante"]["TipoComprobante"]["codigo_afip"], 3, '0', STR_PAD_LEFT);
       $punto_venta_02 = str_pad($comprobante_impuesto["Comprobante"]["PuntoVenta"]["numero"], 5, '0', STR_PAD_LEFT);
       $nro_comprobante_03 = str_pad($comprobante_impuesto["Comprobante"]["nro_comprobante"], 20, '0', STR_PAD_LEFT);
       
       
       /*
       if($comprobante_impuesto["ComprobanteImpuesto"]["tasa_impuesto"] == 0){
         
         $alicuota_iva_afip_05 = str_pad(3, 4, '0', STR_PAD_LEFT);//fuerzo a que sea la correspondiente a 0%  
         $total_gravado = 0.00;
         $total_gravado_04 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_gravado,13,2,"0");
         $impuesto_liquidado_06 = 0.00;
         $impuesto_liquidado_06 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($impuesto_liquidado_06,13,2,"0");
         
       }else{
          
         $total_comprobante_impuesto = $comprobante_obj->ExpresarEnTipoMonedaMejorado($comprobante_impuesto["ComprobanteImpuesto"]["base_imponible"],$field_valor_moneda, $comprobante_impuesto["Comprobante"]);
         
         $total_gravado_04 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_comprobante_impuesto,13,2,"0");  
        
         $impuesto_liquidado_06 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($comprobante_impuesto["ComprobanteImpuesto"]["importe_impuesto"],$field_valor_moneda, $comprobante_impuesto["Comprobante"]);
         $impuesto_liquidado_06 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($impuesto_liquidado_06,13,2,"0"); 
         
       }
       */

       
       
          if($comprobante_impuesto["ComprobanteImpuesto"]["tasa_impuesto"] > 0){
          
	          $total_comprobante_impuesto = $comprobante_obj->ExpresarEnTipoMonedaMejorado($comprobante_impuesto["ComprobanteImpuesto"]["base_imponible"],$field_valor_moneda, $comprobante_impuesto["Comprobante"]);
	         
	         $total_gravado_04 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_comprobante_impuesto,13,2,"0");  
	        
	         $impuesto_liquidado_06 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($comprobante_impuesto["ComprobanteImpuesto"]["importe_impuesto"],$field_valor_moneda, $comprobante_impuesto["Comprobante"]);
	         $impuesto_liquidado_06 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($impuesto_liquidado_06,13,2,"0"); 
			 $linea_texto = $tipo_comprobante_afip_01.$punto_venta_02.$nro_comprobante_03.$total_gravado_04.$alicuota_iva_afip_05.$impuesto_liquidado_06;
			 $comprobante_imprimio[$comprobante_impuesto["Comprobante"]["id"]] = 1;
			 echo $linea_texto.  "\r\n";  
        
        
      }else{
      
 		if(!isset($comprobante_imprimio[$comprobante_impuesto["Comprobante"]["id"]]))  {   
      
      		$linea_texto = $tipo_comprobante_afip_01.$punto_venta_02.$nro_comprobante_03.$comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros(0,13,2,"0").str_pad(3, 4, '0', STR_PAD_LEFT).$comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros(0,13,2,"0");
      		$comprobante_imprimio[$comprobante_impuesto["Comprobante"]["id"]] = 1;
      		 echo $linea_texto.  "\r\n";  
      		}
      
      
      
      }
      
      
  
       

         
    }
        


?>