<?php

App::uses('ComprobanteItemComprobantesController', 'Controller');


class OrdenPagoItemsController extends ComprobanteItemComprobantesController {
    
    public $name = EnumController::OrdenPagoItems;
    public $model = 'ComprobanteItemComprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
	/**
    * @secured(CONSULTA_ORDEN_PAGO)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->loadModel("Moneda");
                
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model); 
        
        $saldo = $this->getFromRequestOrSession('ComprobanteItemComprobante.saldos');
        $id_moneda = $this->getFromRequestOrSession('Comprobante.id_moneda');
         
         $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain'=>array('ComprobanteOrigen'=>array('PuntoVenta','TipoComprobante'),'Comprobante'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order'=>$this->{$this->model}->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_comprobante'))
        );
        
      if($this->RequestHandler->ext != 'json'){  
    
    
        //vista formBuilder
    } else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
                $comprobante = New Comprobante();
                
                foreach($data as &$producto ){ 
                
                         $id_moneda_comprobante_origen = $producto["ComprobanteOrigen"]["id_moneda"];  
                         $id_moneda_comprobante_cabecera = $producto["Comprobante"]["id_moneda"]; 
                         $id_tipo_moneda = $this->Moneda->getTipoMonedaById($id_moneda_comprobante_cabecera); 
                         $campo_moneda = $this->Moneda->getFieldByTipoMoneda($id_tipo_moneda); 
                         
                         //producto["ComprobanteItemComprobante"]["d_producto"] = $producto["Producto"]["d_producto"];
                         $producto["ComprobanteItemComprobante"]["precio_unitario"] = $producto["ComprobanteItemComprobante"]["precio_unitario"]*$producto["ComprobanteOrigen"]["TipoComprobante"]["signo_comercial"];
						 // $producto["ComprobanteItemComprobante"]["nro_comprobante_origen"] = $producto["ComprobanteOrigen"]["nro_comprobante"];
                         $producto["ComprobanteItemComprobante"]["nro_comprobante_origen"] = $producto["ComprobanteOrigen"]["nro_comprobante"];
                         
                         if(isset($producto["ComprobanteOrigen"]["TipoComprobante"])){
                            $producto["ComprobanteItemComprobante"]["d_tipo_comprobante_origen"] = $producto["ComprobanteOrigen"]["TipoComprobante"]["codigo_tipo_comprobante"];
                             $producto["ComprobanteItemComprobante"]["signo_comercial"] = (string) $producto["ComprobanteOrigen"]["TipoComprobante"]["signo_comercial"];
                             
                             $producto["ComprobanteItemComprobante"]["simbolo_signo_comercial"] =  "+";
                             if($producto["ComprobanteOrigen"]["TipoComprobante"]["signo_comercial"] ==  -1)
                                 $producto["ComprobanteItemComprobante"]["simbolo_signo_comercial"] = "-";
                         }
						 $producto["ComprobanteItemComprobante"]["id_moneda_origen"] =    	 $id_moneda_comprobante_origen;
						 $producto["ComprobanteItemComprobante"]["valor_moneda_origen"] =     $producto["ComprobanteOrigen"]["valor_moneda"];
						 
						 /*EL comprobante esta expresado en el id_moneda */
					     // $producto["ComprobanteItemComprobante"]["id_moneda"] =    	 $id_moneda_comprobante_origen;
						  $producto["ComprobanteItemComprobante"]["valor_moneda"] =     (string)($producto["ComprobanteOrigen"]["valor_moneda"]/ 
																								 $producto["ComprobanteOrigen"]["valor_moneda2"]);
						 //$producto["ComprobanteItemComprobante"]["valor_moneda2"] = NO haria falta calcularlos por q se compara el % de dif de cambio contra valor moneda 1 
						 //$producto["ComprobanteItemComprobante"]["valor_moneda3"] =
						 
                         $producto["ComprobanteItemComprobante"]["fecha_generacion_origen"] = $producto["ComprobanteOrigen"]["fecha_generacion"];
                         $producto["ComprobanteItemComprobante"]["fecha_vencimiento_origen"] = $producto["ComprobanteOrigen"]["fecha_vencimiento"];
                        
                         
                         $producto["ComprobanteItemComprobante"]["enlazar_con_moneda"] = $producto["ComprobanteOrigen"]["enlazar_con_moneda"];
						 
						 //$producto["ComprobanteItemComprobante"]["ajusta_diferencia_cambio"] = "1"; //TODO: Traer siemrpe de $producto["ComprobanteOrigen"]["ajusta_diferencia_cambio"];
						 // if(isset($producto["ComprobanteOrigen"]["ajusta_diferencia_cambio"]))
						 // {
							// $producto["ComprobanteOrigen"]["ajusta_diferencia_cambio"] = $producto["ComprobanteOrigen"]["ajusta_diferencia_cambio"];
						 // }
                         
                         $producto["ComprobanteItemComprobante"]["total_comprobante_origen"] = (string) $comprobante->ExpresarEnTipoMonedaMejorado($producto["ComprobanteOrigen"]["total_comprobante"],
                         		$campo_moneda,$producto["ComprobanteOrigen"])*$producto["ComprobanteOrigen"]["TipoComprobante"]["signo_comercial"];
                         
                         //$producto["ComprobanteItemComprobante"]["total_comprobante_origen"] = $producto["ComprobanteOrigen"]["total_comprobante"];
                         
                         
                         if($saldo!='')                     
                            $producto["ComprobanteItemComprobante"]["saldo"] = $comprobante->getComprobantesImpagos($producto["ComprobanteOrigen"]["id_persona"],1,
																													$producto["ComprobanteOrigen"]["id"],1,EnumSistema::COMPRAS,0,$id_moneda);
                         else
                            $producto["ComprobanteItemComprobante"]["saldo"] = '';   
                         //$producto["ComprobanteItemComprobante"]["subtotal_comprobante_origen"] = $producto["ComprobanteOrigen"]["subtotal_comprobante"];
                         
                         if(isset($producto["ComprobanteOrigen"]["d_punto_venta"]))
                            $id_punto_venta = $producto["ComprobanteOrigen"]["d_punto_venta"];
                         else
                            $id_punto_venta = 0;
                         
                         $producto["ComprobanteItemComprobante"]["pto_nro_comprobante_origen"] = $this->ComprobanteItemComprobante->GetNumberComprobante($id_punto_venta,$producto["ComprobanteOrigen"]["nro_comprobante"]);

                         unset($producto["ComprobanteOrigen"]);
                         unset($producto["Comprobante"]);
            }
        }
        
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    
    
     /**
    * @secured(CONSULTA_ORDEN_PAGO)
    */
    public function getModel($vista='default')
    {    
        $model = parent::getModelCamposDefault();
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
    }
    
    
    private function editforView($model,$vista)
    {  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
      $this->set('model',$model);
      Configure::write('debug',0);
      $this->render($vista);
    }
}
?>