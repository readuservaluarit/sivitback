<?php
       
         include(APP.'View'.DS.'ComprobanteImpuestos'.DS.'json'.DS.'comprobante_impuesto.ctp');  
        
        $model["abreviado_impuesto"]["show_grid"] = 1;
        $model["abreviado_impuesto"]["header_display"] = "Impuesto";
        $model["abreviado_impuesto"]["order"] = "1";
        $model["abreviado_impuesto"]["width"] = "15";
        
        $model["tasa_impuesto"]["show_grid"] = 1;
        $model["tasa_impuesto"]["header_display"] = "Alic(%)";
        $model["tasa_impuesto"]["order"] = "2";
        $model["tasa_impuesto"]["width"] = "10";
        
        $model["importe_impuesto"]["show_grid"] = 1;
        $model["importe_impuesto"]["header_display"] = "Importe";
        $model["importe_impuesto"]["order"] = "3";
        $model["importe_impuesto"]["width"] = "20";
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteImpuesto",
                    "content" => $model
                );
        echo json_encode($output);
?>