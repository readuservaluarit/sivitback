<?php
         // CONFIG  CON MODE DISPLAYED CELLS
         include(APP.'View'.DS.'Comprobantes'.DS.'json'.DS.'default.ctp');
       
        
        $model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["type"] = EnumTipoDato::cadena; 
        $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
        $model["pto_nro_comprobante"]["width"] = "15"; //REstriccion mas de esto no se puede achicar
        $model["pto_nro_comprobante"]["order"] = "1";
        $model["pto_nro_comprobante"]["text_colour"] = "#000000";

        $model["fecha_generacion"]["show_grid"] = 1;
        $model["fecha_generacion"]["header_display"] = "Fecha Creaci&oacute;n";
        $model["fecha_generacion"]["width"] = "10";
        $model["fecha_generacion"]["order"] = "2";
        $model["fecha_generacion"]["text_colour"] = "#0000FF";
        
        $model["codigo_persona"]["show_grid"] = 1;
         $model["codigo_persona"]["type"] = EnumTipoDato::cadena; 
        $model["codigo_persona"]["header_display"] = "Cod. Proveedor";
        $model["codigo_persona"]["width"] = "7";
        $model["codigo_persona"]["order"] = "3";
        $model["codigo_persona"]["text_colour"] = "#000000";
        
        
        $model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["type"] = EnumTipoDato::cadena; 
        $model["razon_social"]["header_display"] = "Raz&oacute;n social";
        $model["razon_social"]["width"] = "20";
        $model["razon_social"]["order"] = "4";
        $model["razon_social"]["text_colour"] = "#000000";
        
        
        $model["importe_declarado"]["show_grid"] = 1;
        $model["importe_declarado"]["header_display"] = "Importe Declarado";
        $model["importe_declarado"]["width"] = "7";
        $model["importe_declarado"]["order"] = "5";
        $model["importe_declarado"]["text_colour"] = "#F0000F";
        
        
        $model["d_estado_comprobante"]["show_grid"] = 1;
        $model["d_estado_comprobante"]["type"] = EnumTipoDato::cadena; 
        $model["d_estado_comprobante"]["header_display"] = "Estado";
        $model["d_estado_comprobante"]["width"] = "10";
        $model["d_estado_comprobante"]["order"] = "6";
        $model["d_estado_comprobante"]["text_colour"] = "#004400";
        
        $model["d_usuario"]["show_grid"] = 1;
        $model["d_usuario"]["header_display"] = "Usuario";
        $model["d_usuario"]["order"] = "7";
        $model["d_usuario"]["text_colour"] = "#660000";
        $model["d_usuario"]["width"] = "5";
        
         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Comprobante",
                    "content" => $model
                );
        
          echo json_encode($output);
?>