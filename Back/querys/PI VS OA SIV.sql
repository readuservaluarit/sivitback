
SELECT 
  comprobante.`nro_comprobante` AS nro_comprobante,
  comprobante.`fecha_generacion` AS fecha_generacion_pedido,
  comprobante.`fecha_entrega` AS fecha_entrega_global_pedido,
  comprobante_item.dias AS dias,
  comprobante.plazo_entrega_dias AS dias_pi,
  comprobante_item.cantidad AS cantidad,
  producto.`codigo` AS codigo,
  producto.`d_producto` AS d_producto,
  usuario.`username` AS username,
  producto.id AS id_producto,
  comprobante.`id_persona` AS id_persona,
  comprobante_item.`item_observacion` AS observacion_pedido_interno,
  IF(comprobante.`inspeccion` =1,"SI","NO") AS inspeccion_pedido_interno,
  IF(comprobante.`certificados` =1,"SI","NO") AS certificados_pedido_interno,
  estado_comprobante.`d_estado_comprobante` AS estado_pedido,
  orden_armado.`nro_comprobante` AS nro_orden_armado,
  (SELECT 
    fecha 
  FROM
    auditoria_comprobante 
  WHERE id_comprobante = orden_armado.`id` 
    AND id_estado_comprobante_nuevo = 5 
  ORDER BY id ASC 
  LIMIT 1) AS fecha_completo,
  (SELECT 
    fecha 
  FROM
    auditoria_comprobante 
  WHERE id_comprobante = orden_armado.`id` 
    AND id_estado_comprobante_nuevo = 9 
  ORDER BY id ASC 
  LIMIT 1) AS fecha_cumplido,
  orden_armado.`fecha_generacion` AS fecha_generacion_oa,
  estado_orden_armado.`d_estado_comprobante` AS estado_orden_armado,
  persona.`razon_social` AS razon_social,
  persona.`codigo` AS persona_codigo,
 orden_armado.fecha_entrega AS fecha_entrega_orden_armado,
 orden_armado.`observacion` AS observacion_oa

  
FROM
  comprobante_item 
  JOIN comprobante 
    ON comprobante.id = comprobante_item.`id_comprobante` 
  JOIN estado_comprobante 
    ON estado_comprobante.id = comprobante.`id_estado_comprobante` 
  JOIN producto 
    ON producto.id = comprobante_item.`id_producto` 
    
  LEFT JOIN comprobante_item item_orden_armado 
    ON item_orden_armado.id_comprobante_item_origen = comprobante_item.`id` 
    
  LEFT JOIN comprobante orden_armado 
    ON orden_armado.id = item_orden_armado.`id_comprobante` 
    AND orden_armado.`id_tipo_comprobante` = 104 
    
  LEFT JOIN estado_comprobante estado_orden_armado 
    ON estado_orden_armado.id = orden_armado.`id_estado_comprobante` 
    
    
    LEFT JOIN persona
    ON persona.id = comprobante.`id_persona` 
    
      LEFT JOIN usuario
    ON usuario.id = comprobante.`id_usuario` 
    
WHERE comprobante.id_tipo_comprobante = 202 


  AND comprobante.`id_estado_comprobante` = 1 
LIMIT 100000000000 