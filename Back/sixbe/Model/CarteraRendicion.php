<?php
App::uses('AppModel', 'Model');

/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class CarteraRendicion extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'cartera_rendicion';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		/*'id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar un id v&aacute;lido',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
		),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	
    
    );

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 


public $belongsTo = array(
        'TipoCarteraRendicion' => array(
            'className' => 'TipoCarteraRendicion',
            'foreignKey' => 'id_tipo_cartera_rendicion',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'CuentaContable' => array(
            'className' => 'CuentaContable',
            'foreignKey' => 'id_cuenta_contable',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'Moneda' => array(
            'className' => 'Moneda',
            'foreignKey' => 'id_moneda',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        )
        
        
        
  );
  
  
 public function getCuentaContable($id_cartera_rendicion){
     
     
      
     $cartera_rendicion = $this->find('first', array(
                                                'conditions' => array('CarteraRendicion.id' => $id_cartera_rendicion),
                                                'contain' =>false
                                                ));
                                                
       return $cartera_rendicion["CarteraRendicion"]["id_cuenta_contable"];
 }
 
 
 public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     "d_tipo_cartera_rendicion"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_cuenta_contable"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_cartera_rendicion_extensa"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null)
     );
     
     }
  
   
  
 public function getDescripcion($id_cartera_rendicion){
     	
     	
     	$cartera_rendicion = $this->find('first', array(
     			'conditions' => array('CarteraRendicion.id' => $id_cartera_rendicion),
     			'contain' =>false
     	));
     	
     	return $cartera_rendicion["CarteraRendicion"]["d_cartera_rendicion"];
     }
  
	

}
