<?php

App::uses('WordpressAppModel', 'Wordpress.Model');

class WordpressUser extends WordpressAppModel {

    public $useDbConfig = 'wordpress_database';
    public $name = 'WordpressUser';
    public $primaryKey = 'ID';
    public $useTable = 'six_wp_users';

    //The Associations below have been created with all possible keys, those that are not needed can be removed
    public $hasMany = array(
        'Comment' => array(
            'className' => 'WordpressComment',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Post' => array(
            'className' => 'WordpressPost',
            'foreignKey' => 'post_author',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Usermetum' => array(
            'className' => 'WordpressUsermetum',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
?>
