<?php

class EnumTipoComprobante
{
	
	
    const InformeRecepcionMateriales = 101;  	    	    
    const OrdenTrabajo = 103;  	    
    const RemitoInterno = 105;  	    
    const OrdenArmado = 104;  
    
    
    const SaldoInicioCliente = 200;       //Listo
    const Cotizacion = 201;       //Listo
    const PedidoInterno = 202;    //Listo
    const Remito = 203;           //Listo
    const TurCotizacion = 267;           //Listo
    const CreditoInternoVenta = 209;           //Listo
    const DebitoInternoVenta = 210;           //Listo
    const ReclamoVenta = 268;           

    
    
    
    
    const SaldoInicioProveedor = 300;   //Listo
    const PresupuestoDeCompras = 301;   //Listo
    const OrdenCompra = 302;   //Listo
    const RemitoCompra = 303;  //Listo
    const AnticipoCompra = 304;  //"Credito interno" -> esto es la contra cara de306
    const OrdenPagoAnticipo= 306;  //Listo Documento de valores  igual q orden de pago
    const FacturaCompraImportacion = 350;
    const OrdenPagoAutomatica = 307;   //Al cargar la facturaCompra generar automaticamente la orden de pago
    const OrdenPagoManual = 308; //No es 100% necesaria se puede usar 307 es necesaria cuando se hace un parcial.
    const CreditoInternoCompra = 309; //No es 100% necesaria se puede usar 307 es necesaria cuando se hace un parcial.
    const DebitoInternoCompra = 310; //No es 100% necesaria se puede usar 307 es necesaria cuando se hace un parcial.
    const RetencionIIBB = 311; //No es 100% necesaria se puede usar 307 es necesaria cuando se hace un parcial.
    const RetencionSUSS = 313; //No es 100% necesaria se puede usar 307 es necesaria cuando se hace un parcial.
    const RetencionIVA = 312; //No es 100% necesaria se puede usar 307 es necesaria cuando se hace un parcial.
    const RetencionGANANCIA = 314; //No es 100% necesaria se puede usar 307 es necesaria cuando se hace un parcial.
    const RequerimientoInternoCompra = 315;
	const FACTURASACOMPRA    = 320;
    const NOTASDEDEBITOACOMPRA =     321;
    const NOTASDECREDITOACOMPRA =     322;
    const FACTURASBCOMPRA    = 325;
    const NOTASDEDEBITOBCOMPRA =     326;
    const NOTASDECREDITOBCOMPRA =     327;
    const FACTURASCCOMPRA =     330;
    const NOTASDEDEBITOCCOMPRA =     331;
    const NOTASDECREDITOCCOMPRA =     332;
    const FACTURASMCOMPRA =     333;
    const NOTASDEDEBITOMCOMPRA =     334;
    const NOTASDECREDITOMCOMPRA =     335;
    
    
    const ReciboACompra = 356;
    const ReciboBCompra = 357;
    const ReciboCCompra = 358;
    const ReciboMCompra = 359;
    
    const IngresoCaja = 501; 
    const EgresoCaja = 502;   
    
    const RendicionDeGasto = 503;   //para los que rinden gastos
    const OrdenPagoRenGasto = 504;   //En si no es necesaria puede ser directamente con la rendicion, una vez que se rinde pagarla.
    const OrdenBalanceoRendicion = 505; //nose ni que significa ni que hace
    //503 504 505 se hacen sin Id_Persona. Esto lo copiamos de FLEXXUS   FONDOS->CAJA->EGRESOS E INGRESOS
    const ComprobanteDeGasto = 512; /*Es una factura de proveedor pero sin cuenta corriente http://www.erpmanager.com.ar/020f-erp-tesoreria.html*/
    
    const GASTOA = 513;   //Listo
    const GASTOB= 514;   //Listo
    const GASTOC = 515;  //Listo
    
    const TiqueFacturaA = 516; //nose agregaron todavia
    const TiqueFacturaB = 517; //nose agregaron todavia
    const Tique = 518;    //nose agregaron todavia
    
    
 
 
    const IngresoBanco = 601; //reemplazado por 604
    const EgresoBanco = 602;//reemplazado por 604
    const DepositoCheque = 603;//reemplazado por 604
    const MovimientoBancario = 604; //LISTO- utiliza la tabla comprobante y comprobante_valor si es destino es debe sino es haber relacion 1 a 1.
    const ChequeRechazado = 605; //comprobante que contiene varios ComprobanteValor (cheque adentro). dependiendo del detalletipocomprobante te deja seleccionar unos u otros. 
    
    
    
    
    
    const  ImpuestodeVenta= 702;
    const  ImpuestodeCompra= 703;
    
    const FacturaA = 801;   //Listo
    const FacturaB= 806;   //Listo
    const FacturaC = 811;  //Listo
    const FacturaCreditoA = 890;
    const FacturaCreditoB = 893;
    const FacturaCreditoC = 896;

    
    const FacturaE = 817; //Factura de exportacion
    const FacturaM = 833;
    
    const ReciboAutomatico = 207;
    const ReciboManual = 208;
    
    const ReciboA = 804;
    const ReciboB = 809;
    const ReciboC = 815;
    const ReciboM = 836;
    
    
    const NotaCreditoA = 803;    //Listo
    const NotaCreditoB = 808;  //Listo
    const NotaCreditoC = 813;  //Listo
    const NotaCreditoM = 835;
    const NotaCreditoE = 819;  //Listo
    const NotaCreditoAFacturaCredito = 891;
    const NotaCreditoBFacturaCredito = 894;
    const NotaCreditoCFacturaCredito = 897;
    
    
    
    const NotaDebitoA = 802;  //Listo
    const NotaDebitoB = 807;  //Listo
    const NotaDebitoC = 812;  //Listo
    const NotaDebitoM = 834;  //Listo
    const NotaDebitoE = 818;  //Listo
    const NotaDebitoAFacturaCredito = 892;  //Listo
    const NotaDebitoBFacturaCredito = 895;  //Listo
    const NotaDebitoCFacturaCredito = 898;  //Listo
    
    
    
    const Mantenimiento = 1000;  //Listo
    const QaCertificadoCalidad = 1801;  //Listo

    
    
    
    /*Pantalla movimiento de Valores que agrupe ingresos y egresos*/
  

    
    
    
    
    
}

  
?>