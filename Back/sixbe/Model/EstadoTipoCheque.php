<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class EstadoTipoCheque extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'estado_tipo_cheque';

    public $actsAs = array('Containable');
    
   
/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        /*
        'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

public $belongsTo = array(
        'EstadoCheque' => array(
            'className' => 'EstadoCheque',
            'foreignKey' => 'id_estado_cheque',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'TipoCheque' => array(
            'className' => 'TipoCheque',
            'foreignKey' => 'id_tipo_cheque',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
        
       
      
 );
 
 
 
}
