<?php

/**
* @secured(CONSULTA_LOTE)
*/
class TiposLoteController extends AppController {
	
 //MP: estos permisos estan bien son del padre LOTE -> permisos pegados -> en la fcoFactura en el index se usa un modificar de carga asi:
 /*
           case "id_tipo_lote":
                    cbo.SetView("TipoLotes", "TipoLote", "d_tipo_lote", 0)
                        .SetPadre("Lote") //Hereda los permisos de aca - PEGAR PERMISOS PARA EVITAR q crezcan los numeros de permisos y tener cierto control PEGADO
                         .getListView(param_filter,method);
                    break;
 */
    public $name = 'TiposLote';
    public $model = 'TipoLote';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);

        $conditions = array(); 
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => 10,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de TipoLoteCausas', 'Datos de los Tipo de Impuesto', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_TipoLote', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'TipoLote.id', "10%");
                $formBuilder->addHeader('Nombre', 'TipoLote.d_TipoLote_causa', "50%");
                $formBuilder->addHeader('Nombre', 'TipoLote.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_TipoLoteCausa');
                $formBuilder->addField($this->model, 'cotizacion');
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }

     
       // try{
            // if ($this->TipoLote->delete() ) {
                
                // //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                // $status = EnumError::SUCCESS;
                // $mensaje = "El Tipo de Impuesto ha sido eliminado exitosamente.";
                // $output = array(
                    // "status" => $status,
                    // "message" => $mensaje,
                    // "content" => ""
                // ); 
                
            // }
                
            // else
                 // throw new Exception();
                 
          // }catch(Exception $ex){ 
            // //$this->Session->setFlash($ex->getMessage(), 'error');
            
            // $status = EnumError::ERROR;
            // $mensaje = $ex->getMessage();
            // $output = array(
                    // "status" => $status,
                    // "message" => $mensaje,
                    // "content" => ""
                // ); 
        // }
        
        // if($this->RequestHandler->ext == 'json'){
            // $this->set($output);
        // $this->set("_serialize", array("status", "message", "content"));
            
        // }else{
            // $this->Session->setFlash($mensaje, $status);
            // $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        // }
	// }
    
    
	  /**
        * @secured(CONSULTA_LOTE)
        */
        public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }
        
        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
        	
        	$model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        	
        	
        	$this->set('model',$model);
        	Configure::write('debug',0);
        	$this->render($vista);
        }

    
   



}
?>