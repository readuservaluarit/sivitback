<?php 
		include(APP.'View'.DS.'EjerciciosContable'.DS.'json'.DS.'ejercicio_contable.ctp');
        
		$model["id"]["show_grid"] = 1;
        $model["id"]["header_display"] = "Nro.";
        $model["id"]["order"] = "0";
        $model["id"]["width"] = "5";
        
        $model["d_ejercicio"]["show_grid"] = 1;
        $model["d_ejercicio"]["header_display"] = "Desc.";
        $model["d_ejercicio"]["order"] = "1";
        $model["d_ejercicio"]["width"] = "15";

        $model["fecha_apertura"]["show_grid"] = 1;
        $model["fecha_apertura"]["header_display"] = "Fecha Apertura";
        $model["fecha_apertura"]["order"] = "2";
        $model["fecha_apertura"]["width"] = "10";
		$model["fecha_apertura"]["text_colour"] = "#000077";
        
        $model["fecha_cierre"]["show_grid"] = 1;
        $model["fecha_cierre"]["header_display"] = "Fecha Cierre";
        $model["fecha_cierre"]["order"] = "3";
        $model["fecha_cierre"]["width"] = "10";
		$model["fecha_cierre"]["text_colour"] = "#880044";
        
        $model["ultimo_asiento"]["show_grid"] = 1;
        $model["ultimo_asiento"]["header_display"] = "&Uacute;ltimo Asiento";
        $model["ultimo_asiento"]["order"] = "4";
        $model["ultimo_asiento"]["width"] = "5";
        
        $model["d_estado"]["show_grid"] = 1;
        $model["d_estado"]["header_display"] = "Estado";
        $model["d_estado"]["order"] = "5";
        $model["d_estado"]["width"] = "12";
		$model["d_activo"]["text_colour"] = "#FF0000";
                
        $model["d_activo"]["show_grid"] = 1;
        $model["d_activo"]["header_display"] = "Activo";
        $model["d_activo"]["order"] = "5";
        $model["d_activo"]["width"] = "5";
		$model["d_activo"]["text_colour"] = "#EE0044";

 $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "EjercicioContable",
                    "content" => $model
                );
        echo json_encode($output);
         
        

?>