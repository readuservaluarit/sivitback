<?php
App::uses('PersonasCategoriasController', 'Controller');


  /**
    * @secured(CONSULTA_TRANSPORTISTA_CATEGORIA)
  */
class TransportistasCategoriasController extends PersonasCategoriasController {
    
    public $name = 'TransportistasCategorias';
    public $model = 'PersonaCategoria';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $id_tipo_persona = EnumTipoPersona::Transportista;
    
    
    /**
    * @secured(CONSULTA_TRANSPORTISTA_CATEGORIA)
    */
    public function index() {
        
         parent::index();    
        
    }
    
    /**
    * @secured(ABM_USUARIOS)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->Cotizacion->id = $id;
            $this->Cotizacion->contain();
            $this->request->data = $this->Cotizacion->read();
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Cotizacion');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Cotizaciones');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Cotizaciones');
        else
            $formBuilder->setTituloForm('Consulta de Cotizaciones');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('cuit', 'Cuit', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('calle', 'Calle', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  
        $formBuilder->addFormInput('tel', 'Tel&eacute;fono', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('ciudad', 'Ciudad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descuento', 'Descuento', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descripcion', 'Descripcion', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('localidad', 'Localidad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
         
        
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'ClienteAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('ClienteRazonSocial', 'Debe ingresar una Raz&oacute;n Social');
                validator.validateRequired('ClienteCuit', 'Debe ingresar un CUIT');
                validator.validateNumeric('ClienteDescuento', 'El Descuento ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_cuit',
                        'data': 'cuit=' + $('#ClienteCuit').val()+'&id='+$('#ClienteId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('ClienteCuit','El Cuit que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

    /**
    * @secured(ADD_TRANSPORTISTA_CATEGORIA)
    */
    public function add(){
        
     parent::add();    
        
    }
    
    
    /**
    * @secured(MODIFICAR_TRANSPORTISTA_CATEGORIA)
    */
    public function edit($id){
        
     parent::edit($id);    
        
    }
    
    
     /**
    * @secured(BORRAR_TRANSPORTISTA_CATEGORIA)
    */
    public function delete($id){
        
     parent::delete($id);    
        
    }
    
    
    
    
      /**
    * @secured(CONSULTA_TRANSPORTISTA_CATEGORIA)
    */
	public function getModel($vista='default')
	{
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
      
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
        
        
         $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);          
    }

    // public function getModel($vista="default"){
        
        // $model = parent::getModelCamposDefault();
        // //$model = $this->editforView($model);//esta funcion edita y agrega campos para la vista
        // $output = array(
                    // "status" =>EnumError::SUCCESS,
                    // "message" => $this->model,
                    // "content" => $model
                // );
        
          // echo json_encode($output);
          // die();
    // }
    
    
    // private function editforView($model){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
    
       // // $model =  parent::setDefaultFieldsForView($model);
        
        
        // return $model;
    // }
}
?>