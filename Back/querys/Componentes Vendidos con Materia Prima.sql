SELECT componente.`codigo` componente,componente.`d_producto` desc_componente, ROUND(SUM(comprobante_item.`cantidad`*producto_relacion.`cantidad_hijo`),0) AS cantidad_vendida,

origen.`d_origen` origen_componente,componente.`costo`,

materia_prima.`codigo` materia_prima_codigo,mpr.`cantidad_hijo` cantidad_utilizada,materia_prima.`d_producto` materia_prima_descripcion


 FROM comprobante_item 

 JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`

 JOIN producto ON producto.id = comprobante_item.`id_producto`

 JOIN producto_relacion ON producto_relacion.`id_producto_padre` = producto.`id`

 JOIN producto componente ON componente.id = producto_relacion.`id_producto_hijo`
 
 LEFT JOIN producto_relacion mpr ON mpr.`id_producto_padre` = componente.`id`
 
 LEFT JOIN producto materia_prima ON materia_prima.id = mpr.id_producto_hijo
 
 LEFT JOIN origen ON origen.id = componente.`id_origen`
 

WHERE  

componente.`id_producto_tipo` = 2 AND

comprobante.`id_tipo_comprobante` = 202 AND comprobante_item.`activo`=1

AND YEAR(comprobante.fecha_generacion)= 2018
AND comprobante.`id_estado_comprobante` <>2


GROUP BY componente.id 

LIMIT 1000000


