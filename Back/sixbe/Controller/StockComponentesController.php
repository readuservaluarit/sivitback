<?php


App::uses('StockProductosController', 'Controller');

  /**
    * @secured(CONSULTA_STOCK)
  */
class StockComponentesController extends StockProductosController {
	
	
    public $name = 'StockComponentes';
    public $model = 'StockComponente';
    public $tipo_producto = EnumProductoTipo::Componente;
    public $padre = "Componente";
    public $d_padre = "d_componente";
    public $deposito_fijo_principal = 4;
    public $deposito_fijo_complementario = 6;
    public $traer_cantidad_orden_produccion = 1;  //Este flag es para desde una pantalla de stock traer la cantidad de componentes que estan en una orden de produccion
   
     /**
    * @secured(CONSULTA_STOCK)
    */
    public function index(){
        
        parent::index();
    }
    
    /**
    * @secured(CONSULTA_STOCK)
    */
    public function getModel($vista='default'){
        
        parent::getModel($vista);
    }
}
?>