<?php

App::uses('ComprobanteItemsController', 'Controller');

class SaldosInicialProveedorItemsController extends ComprobanteItemsController {
    
    public $name = EnumController::SaldosInicialProveedorItems;
    public $model = 'ComprobanteItem';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    

     /**
    * @secured(CONSULTA_SALDO_INICIAL_PROVEEDOR)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model); 
        
     

        
        
     
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
         'joins' => array(
            array(
                'table' => 'producto_tipo',
                'alias' => 'ProductoTipo',
                'type' => 'LEFT',
                'conditions' => array(
                    'ProductoTipo.id = Producto.id_producto_tipo'
                )
                
            )),
            'contain' =>array('Comprobante'=>array('PuntoVenta','EstadoComprobante','Moneda'),'Producto'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));

        //Headers
        $formBuilder->addHeader('id', 'Factura.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'Factura.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$producto ){
                 
            
            
            
       
                 
                 
                 $producto["ComprobanteItem"]["total"] = (string) ($producto["ComprobanteItem"]["precio_unitario"]);

                 if(isset($producto["Comprobante"]["PuntoVenta"]["numero"]))
                    $producto["ComprobanteItem"]["nro_comprobante"] = $this->ComprobanteItem->GetNumberComprobante($producto["Comprobante"]["PuntoVenta"]["numero"],$producto["Comprobante"]["nro_comprobante"]);
                 else
                    $producto["ComprobanteItem"]["nro_comprobante"] = $this->ComprobanteItem->GetNumberComprobante($producto["Comprobante"]["d_punto_venta"],$producto["Comprobante"]["nro_comprobante"]); 
                    
                 $producto["ComprobanteItem"]["d_estado_comprobante"] = $producto["Comprobante"]["EstadoComprobante"]["d_estado_comprobante"];
                 $producto["ComprobanteItem"]["moneda_simbolo"] = $producto["Comprobante"]["Moneda"]["simbolo"];
                 $producto["ComprobanteItem"]["id_moneda"] = $producto["Comprobante"]["Moneda"]["id"];
                 
            
                 unset($producto["Producto"]);
                 
                 unset($producto["Comprobante"]);
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    
     /**
    * @secured(CONSULTA_SALDO_INICIAL_PROVEEDOR)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
      
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        
     
      $this->set('model',$model);
      Configure::write('debug',0);
      $this->render($vista);
        
       
   
        

        
    }
    

    
   
    
}
?>