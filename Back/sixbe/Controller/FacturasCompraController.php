    <?php
App::uses('ComprobantesController', 'Controller');

class FacturasCompraController extends ComprobantesController{
    public $name = EnumController::FacturasCompra;
    public $model = 'Comprobante';
    public $helpers = array('Session','Paginator','Js');
    public $components = array(
        'Session',
        'PaginatorModificado',
        'RequestHandler'
    );
    public $requiere_impuestos = 1;

 // si el comprobante acepta impuesto, esto llama a las funciones de IVA E IMPUESTOS
    public $id_sistema_comprobante = EnumSistema::COMPRAS;

    /**
     * @secured(CONSULTA_FACTURA_COMPRA)
     */
    public function index()
	{
        if ($this->request->is('ajax')) $this->layout = 'ajax';
        
        $this->loadModel($this->model);
        
        $conditions = $this->RecuperoFiltros($this->model);
        $array_conditions = array(
            'Persona',
            'EstadoComprobante',
            'Moneda',
            'CondicionPago',
            'TipoComprobante',
            'ComprobanteImpuesto' => array(
                'Impuesto' => array(
                    'Sistema',
                    'ImpuestoGeografia'
                )
            ) ,
            'Sucursal' => array(
                'Provincia',
                'Pais'
            ),
        'Usuario'
        ); //contacto es la sucursal

        // Si pasa el ID traigo diferentes models relacionados,sino lo basico

        /*
        if(in_array('Comprobante.id',$conditions))//elijo los models a traer dependiendo si es consulta o index
        $array_conditions = array('Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante','ComprobanteImpuesto'=>array('Impuesto'=>array('Sistema','ImpuestoGeografia')),'PuntoVenta');
          else
        $array_conditions = array('Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante','ComprobanteImpuesto','PuntoVenta');
        */
        $this->paginate = array(
        	'paginado'=>$this->paginado,
            'maxLimit'=> $this->maxLimitRows,
            'contain' => $array_conditions,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum() ,
            'order' => $this->model . '.id desc'
        );
        if ($this->RequestHandler->ext != 'json')
            {
            App::import('Lib', 'FormBuilder');
            $formBuilder = new FormBuilder();
            $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));

            // Headers

            $formBuilder->addHeader('id', 'cotizacion.id', "10%");
            $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
            $formBuilder->addHeader('CUIT', 'cotizacion.cuit', "20%");
            $formBuilder->addHeader('Email', 'cliente.email', "30%");
            $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

            // Fields

            $formBuilder->addField($this->model, 'id');
            $formBuilder->addField($this->model, 'razon_social');
            $formBuilder->addField($this->model, 'cuit');
            $formBuilder->addField($this->model, 'email');
            $formBuilder->addField($this->model, 'tel');
            $this->set('abm', $formBuilder);
            $this->render('/FormBuilder/index');

            // vista formBuilder

            }
          else
            { // vista json
            $this->PaginatorModificado->settings = $this->paginate; 
            $data = $this->PaginatorModificado->paginate($this->model);
            $page_count = $this->params['paging'][$this->model]['pageCount'];
            foreach($data as & $valor)
                {

                // $valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
                // unset($valor['ComprobanteItem']);

                if (isset($valor['TipoComprobante']))
                    {
                    $valor[$this->model]['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                    $valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
                    unset($valor['TipoComprobante']);
                    }

                $valor[$this->model]['pto_nro_comprobante'] = (string)$this->Comprobante->GetNumberComprobante($valor[$this->model]['d_punto_venta'], $valor[$this->model]['nro_comprobante']);
                /*
                foreach($valor[$this->model]['ComprobanteItem'] as &$producto ){
                $producto["d_producto"] = $producto["Producto"]["d_producto"];
                $producto["codigo_producto"] = $this->getProductoCodigo($producto);
      
                unset($producto["Producto"]);
                }

                */
                
                $valor[$this->model]['cotizacion'] =  	$this->getCotizacionConMonedaCorriente($valor);
                
                
                if (isset($valor['Moneda']))
                    {
                    $valor[$this->model]['d_moneda_simbolo'] = $valor['Moneda']['simbolo'];
                    unset($valor['Moneda']);
                    }

                if (isset($valor['ComprobanteImpuesto']))
                    {
                    $valor[$this->model]['ComprobanteImpuestoDefault'] = $this->getImpuestos($valor['ComprobanteImpuesto'], $valor["Comprobante"]["id"], array(
                        EnumSistema::COMPRAS
                    ) , array() , EnumTipoImpuesto::PERCEPCIONES);
                    $valor[$this->model]['ComprobanteImpuestoTotal'] = (string)$this->TotalImpuestos($valor[$this->model]['ComprobanteImpuestoDefault']);
                    $valor[$this->model]['ComprobanteImpuestoIva'] = $this->getImpuestos($valor['ComprobanteImpuesto'], $valor["Comprobante"]["id"], array(
                        EnumSistema::COMPRAS
                    ) , array(
                        EnumImpuesto::IVACOMPRAS
                    ) , EnumTipoImpuesto::PERCEPCIONES);
                    $valor[$this->model]['ComprobanteIvaTotal'] = (string)$this->TotalImpuestos($valor[$this->model]['ComprobanteImpuestoIva']);
                    }

                if (isset($valor['EstadoComprobante']))
                    {
                    $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
                    unset($valor['EstadoComprobante']);
                    }

                if (isset($valor['CondicionPago']))
                    {
                    $valor[$this->model]['d_condicion_pago'] = $valor['CondicionPago']['d_condicion_pago'];
                    unset($valor['CondicionPago']);
                    }

                if (isset($valor['Persona']))
                    {
                    if ($valor['Persona']['id'] == null)
                        {
                        $valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social'];
                        }
                      else
                        {
                        $valor[$this->model]['razon_social'] = $valor['Persona']['razon_social'];
                        $valor[$this->model]['id_tipo_iva'] = $valor['Persona']['id_tipo_iva'];
                        $valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
                        }

                    unset($valor['Persona']);
                    }

                if (isset($valor['DepositoOrigen']))
                    {
                    $valor['Comprobante']['d_deposito_origen'] = $valor['DepositoOrigen']['d_deposito_origen'];
                    }

                if (isset($valor['DepositoDestino']))
                    {
                    $valor['Comprobante']['d_deposito_destino'] = $valor['DepositoDestino']['d_deposito_destino'];
                    }

                if (isset($valor['TipoComprobante']))
                    {
                    $valor['Comprobante']['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                    }
                    
                    
                    
                if(isset($valor['Usuario'])){
                    	$valor['Comprobante']['d_usuario'] =$valor['Usuario']['nombre'];
                    }

                /* if(isset())
                $comprobantes_relacionados = $this->getComprobantesRelacionados($id);
                */
                $this->
                    {
                    $this->model}->formatearFechas($valor);
                    }

                    
                $this->data = $data;
                
                $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "list",
                    "content" => $data,
                    "page_count" => $page_count
                );
                $this->set($output);
                
                
                $this->set("_serialize", array(
                    "status",
                    "message",
                    "page_count",
                    "content"
                ));
                }

            // fin vista json

            }

        /**
         * @secured(ADD_FACTURA_COMPRA)
         */
        public function add()
		{
            $this->CalcularBaseImponibleImpuestoOnTheFly($this->request->data["Comprobante"]["subtotal_neto"]);    
            parent::add();
		}

        /**
         * @secured(MODIFICACION_FACTURA_COMPRA)
         */
        public function edit($id)
		{
			
			
			
								           
            $this->borraImpuestosCompra($id); //esto lo pongo para q en el edit no duplique
            $this->CalcularBaseImponibleImpuestoOnTheFly($this->request->data["Comprobante"]["subtotal_neto"]);
            parent::edit($id);
            return;
		}

        /**
         * @secured(BAJA_FACTURA_COMPRA)
         */
        public function deleteItem($id,$externo=1)
		{
            parent::deleteItem($id,$externo);
		}

        /**
         * @secured(CONSULTA_FACTURA_COMPRA)
         */
        public function getModel($vista = 'default')
		{
            $model = parent::getModelCamposDefault(); //esta en APPCONTROLLER
            $model = $this->editforView($model, $vista); //esta funcion edita y agrega campos para la vista, debe estar LOCAL
		}

        private function editforView($model, $vista)
		{ //esta funcion recibe el model y pone los campos que se van a ver en la grilla
            $model = parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER

            // $model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);

            $this->set('model', $model);
            Configure::write('debug', 0);
            $this->render($vista);
		}

        public function CalcularImpuestos($id_comprobante, $id_tipo_impuesto = '', $error = 0, $message = '')
		{
		
			return $this->borrraComprobanteImpuestoConBaseCero($error,$message);
		}

        public function CalcularIva($id_c, $objeto)
		{
			return;
		}

        public function actualizoMontos($id_c, $nro_comprobante, $total_iva, $total_impuestos)
		{
			return;
		}

        public function borraImpuestos($id)
		{
			return;
		}

        protected function borraImpuestosCompra($id_comprobante)
		{
            $this->loadModel("Comprobante");
            if ($this->Comprobante->getDefinitivoGrabado($id_comprobante) == 0)
			{
				$this->Comprobante->ComprobanteImpuesto->deleteAll(array(
					'ComprobanteImpuesto.id_comprobante' => $id_comprobante
				) , false);
			}
		}

        /**
         * @secured(CONSULTA_FACTURA_COMPRA)
         */
        public function existe_comprobante()
		{
			parent::existe_comprobante();
		}

            
            
        /**
         * @secured(MODIFICACION_FACTURA_COMPRA)
         */
        public function Anular($id_comprobante)
		{
            $this->loadModel("Comprobante");
            $this->loadModel("Asiento");
            $this->loadModel("Modulo");
            
            $output_asiento_revertir = array("id_asiento"=>0);
            $factura = $this->Comprobante->find('first', array(
                'conditions' => array(
                    'Comprobante.id' => $id_comprobante,
                    'Comprobante.id_tipo_comprobante' => $this->id_tipo_comprobante
                ) ,
                'contain' => array(
                    'ComprobanteValor',
                    'Asiento',
                    'ChequeEntrada'
                )
            ));
            $ds = $this->Comprobante->getdatasource();
            if ($this->Comprobante->getEstadoGrabado($id_comprobante) != EnumEstadoComprobante::Anulado && count($this->getOrdenesPagoRelacionados($id_comprobante)) == 0 && $factura)
                { //s
                try
                    {
             
                    
                    	if($this->PeriodoValidoParaAnular($factura) == 0)
                    		throw new Exception("El Comprobante no es posible anularlo. Ya que el Peri&oacute;do contable en el cual se encuenta no esta disponible.");
              
                    
                    
                    
		                    $ds->begin();
		                    $output_asiento_revertir = array("id_asiento"=>0);
		                    if ($factura["Comprobante"]["comprobante_genera_asiento"] == 1) 
		                            $output_asiento_revertir = $this->Asiento->revertir($factura["Asiento"]["id"]);
		
		                    // le clavo el estado ANULADO
		
		                    $this->Comprobante->updateAll(array(
		                        'Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,
		                        'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'" ,
		                        'Comprobante.definitivo' => 1
		                    ) , array(
		                        'Comprobante.id' => $id_comprobante
		                    ));
		                    
		                    
		                    
		                    
		                    $factura_compra = $this->Comprobante->find('first', array(
		                    		'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
		                    		'contain' => false
		                    ));
		                    
		                    $this->Auditar($id_comprobante,$factura_compra);
		                    
		                    
		                    
		                    
		                    $ds->commit();
		                    $tipo = EnumError::SUCCESS;
		                    $mensaje = "El Comprobante se anulo correctamente ";
                    
                   
                    
                    
                    
                    }

                catch(Exception $e)
                    {
                    $ds->rollback();
                    $tipo = EnumError::ERROR;
                    $mensaje = "No es posible anular el Comprobante ERROR:" . $e->getMessage();
                    }
                }
              else
                {
                $tipo = EnumError::ERROR;
                $mensaje = "No es posible anular el Comprobante ya que  ha sido ANULADO o esta presente en alguna Orden de Pago" . "\n";
                $op_relacionados = $this->getOrdenesPagoRelacionados($id_comprobante);
                if (count($op_relacionados) > 0)
                    {
                    foreach($op_relacionados as $recibo)
                        {
                        $mensaje.= "&bull; Orden de Pago Nro: " . $recibo["Comprobante"]["nro_comprobante"] . "\n";
                        }
                    }
                }

            $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                "id_asiento" => $output_asiento_revertir["id_asiento"],
            );
            echo json_encode($output);
            die();
		}

        /**
         * @secured(CONSULTA_FACTURA_COMPRA)
         */
        public

        function getComprobantesRelacionadosExterno($id_comprobante, $id_tipo_comprobante = 0, $generados = 1)
            {
            parent::getComprobantesRelacionadosExterno($id_comprobante, $id_tipo_comprobante, $generados);
            }
            
            
        
        
            /**
             * @secured(BTN_EXCEL_FACTURASCOMPRA)
             */
            public function excelExport($vista="default",$metodo="index",$titulo=""){
            	
            	parent::excelExport($vista,$metodo,"Listado de Facturas de Compra");
            	
            	
            	
            }
            
            
            
            
            
        
            
        }

?>