<?php
App::uses('ProductosController', 'Controller');
/**
* @secured(CONSULTA_COMPONENTE)
*/
class ComponentesController extends ProductosController {
    public $name = 'Componentes';
    public $model = 'Componente';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $tipo_producto = EnumProductoTipo::Componente;
    public $model_hijos_relacionados = "ArticuloRelacion";
    public $model_hijo = "MateriaPrima";
    public $id_padre = "id_producto_padre";
    public $d_padre = "d_componente";
    public $id_hijo = "id_producto_hijo";
    public $nombre_producto = "El Componente";
    public $model_stock = "StockComponente";
    
  
    
    /**
    * @secured(CONSULTA_COMPONENTE)
    */
    public function index() {
        
        parent::index();
    }
     
    /**
    * @secured(ADD_COMPONENTE)
    */
    public function add() {
        
        parent::add();
      
    }
    
    
    /**
    * @secured(MODIFICACION_COMPONENTE)
    */
    function edit($id) {
        
        parent::edit($id);
    }
    
  
    /**
    * @secured(BAJA_COMPONENTE)
    */
    function delete($id) {
        
        parent::delete($id);
        
    }
    
  
    /**
    * @secured(CONSULTA_COMPONENTE)
    */
    public function existe_codigo(){
        
        parent::existe_codigo();
    }
    
    
    
    
   
    
    /**
    * @secured(CONSULTA_COMPONENTE)
    */
  public function getModel($vista='default'){
       parent::getModel($vista);
  }
    
    
  /**
    * @secured(CONSULTA_COMPONENTE)
    */
  public function getCosto($id_articulo){
    parent::getCosto($id_articulo);
  }  
    
  
  
  /**
   * @secured(CONSULTA_COMPONENTE)
   */
  public function existe_codigo_barra(){
  	
  	
  	parent::existe_codigo_barra();
  	return;
  }
  
}
?>