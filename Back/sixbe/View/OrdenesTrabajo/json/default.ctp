<?php   // CONFIG  CON MODE DISPLAYED CELLS
        include(APP.'View'.DS.'Comprobantes'.DS.'json'.DS.'default.ctp');
      
        $model["codigo_tipo_comprobante"]["show_grid"] = 1;
        $model["codigo_tipo_comprobante"]["header_display"] = "Tipo";
        $model["codigo_tipo_comprobante"]["order"] = "0";
        $model["codigo_tipo_comprobante"]["width"] = "10";
        $model["codigo_tipo_comprobante"]["text_colour"] = "#000000";
        
        $model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
        $model["pto_nro_comprobante"]["order"] = "2";
        $model["pto_nro_comprobante"]["width"] = "20";
        $model["pto_nro_comprobante"]["text_colour"] = "#000000";
        
		$model["codigo_producto"]["show_grid"] = 1;
        $model["codigo_producto"]["header_display"] = "C&oacute;digo";
        $model["codigo_producto"]["order"] = "3";
        $model["codigo_producto"]["width"] = "12";
        $model["codigo_producto"]["text_colour"] = "#000000";
        
        $model["cantidad"]["show_grid"] = 1;
        $model["cantidad"]["header_display"] = "Cant Plan";
        $model["cantidad"]["order"] = "5";
        $model["cantidad"]["width"] = "5";
        $model["cantidad"]["text_colour"] = "#0000DD";
        
        $model["cantidad_cierre"]["show_grid"] = 1;
        $model["cantidad_cierre"]["header_display"] = "Cant Cierre";
        $model["cantidad_cierre"]["order"] = "7";
        $model["cantidad_cierre"]["width"] = "5";
        $model["cantidad_cierre"]["text_colour"] = "#009900";
        
        $model["fecha_generacion"]["show_grid"] = 1;
        $model["fecha_generacion"]["header_display"] = "Fecha Creaci&oacute;n";
        $model["fecha_generacion"]["order"] = "8";
        $model["fecha_generacion"]["width"] = "10";
        $model["fecha_generacion"]["text_colour"] = "#0000FF";
        
		$model["d_estado_comprobante"]["show_grid"] = 1;
        $model["d_estado_comprobante"]["header_display"] = "Estado";
        $model["d_estado_comprobante"]["order"] = "12";
        $model["d_estado_comprobante"]["width"] = "10";
        $model["d_estado_comprobante"]["text_colour"] = "#AA0000";
		
		/*INI - NUEVOS*/		
		// $model["c_retirado"]["show_grid"] = 1;
        // $model["c_retirado"]["header_display"] = "Comp. Retirado";
        // $model["c_retirado"]["order"] = "12";
        // $model["c_retirado"]["width"] = "10";
        // $model["c_retirado"]["text_colour"] = "#AA0000";
		
		$model["c_terminado"]["show_grid"] = 1;
        $model["c_terminado"]["header_display"] = "C Terminado";
        $model["c_terminado"]["order"] = "12";
        $model["c_terminado"]["width"] = "10";
        $model["c_terminado"]["text_colour"] = "#009900";
		
		// $model["mp_cantidad"]["show_grid"] = 1;
        // $model["mp_cantidad"]["header_display"] = "Estado";
        // $model["mp_cantidad"]["order"] = "12";
        // $model["mp_cantidad"]["width"] = "10";
        // $model["mp_cantidad"]["text_colour"] = "#AA0000";
		
		$model["mp_retirado"]["show_grid"] = 1;
        $model["mp_retirado"]["header_display"] = "MP Retirada";
        $model["mp_retirado"]["order"] = "12";
        $model["mp_retirado"]["width"] = "10";
        $model["mp_retirado"]["text_colour"] = "#EE0000";
		
		// $model["mp_terminado"]["show_grid"] = 1;
        // $model["mp_terminado"]["header_display"] = "Estado";
        // $model["mp_terminado"]["order"] = "12";
        // $model["mp_terminado"]["width"] = "10";
        // $model["mp_terminado"]["text_colour"] = "#AA0000";
		/*FIN - NUEVOS*/		
        
        $model["d_usuario"]["show_grid"] = 1;
        $model["d_usuario"]["header_display"] = "Operador";
        $model["d_usuario"]["width"] = "10";
        $model["d_usuario"]["order"] = "14";
        $model["d_usuario"]["text_colour"] = "#770077";
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Comprobante",
                    "content" => $model
                );
        echo json_encode($output);
?>