<?php

App::uses('ComprobanteItemsController', 'Controller');

class NotaItemsController extends ComprobanteItemsController {
    
    public $name = EnumController::NotaItems;
    public $model = 'ComprobanteItem';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
     /**
    * @secured(CONSULTA_NOTA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model);
        $id_comprobante_filtro = $this->getFromRequestOrSession($this->model.'.id_comprobante');
        
     
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
         'joins' => array(
            array(
                'table' => 'producto_tipo',
                'alias' => 'ProductoTipo',
                'type' => 'LEFT',
                'conditions' => array(
                    'ProductoTipo.id = Producto.id_producto_tipo'
                )
                
            )),
			'contain' =>array('Unidad','Comprobante'=>array('PuntoVenta','EstadoComprobante','Moneda'),'Producto'=>array('Unidad'),'ComprobanteItemOrigen'=>array('Unidad')),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order'=>$this->ComprobanteItem->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_comprobante'))
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
    
    
        
        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'Factura.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'Factura.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$producto ){
				
				$producto["ComprobanteItem"]["total_pendiente_notas_credito_origen"] = "0";
			    $producto["ComprobanteItem"]["moneda_simbolo"] = $producto["Comprobante"]["Moneda"]["simbolo"];
                $producto["ComprobanteItem"]["id_moneda"] = $producto["Comprobante"]["Moneda"]["id"];
                
                
                
             
                
                
                if(isset($producto["ComprobanteItem"]["id_unidad"]) && $producto["ComprobanteItem"]["id_unidad"]>0){
                
                	
                	$producto["ComprobanteItem"]["d_unidad"] = $producto["Unidad"]["d_unidad"];
                	$producto["ComprobanteItem"]["id_unidad"] = $producto["Unidad"]["id"];
                }else{
                	
                	$producto["ComprobanteItem"]["d_unidad"] = $producto["ComprobanteItemOrigen"]["Unidad"]["d_unidad"];
                	$producto["ComprobanteItem"]["id_unidad"] = $producto["ComprobanteItemOrigen"]["id_unidad"];
                }
                
                
                if(is_null($producto["ComprobanteItem"]["id_unidad"]) && !$producto["ComprobanteItem"]["id_unidad"]>0 ){
                	
                	$producto["ComprobanteItem"]["d_unidad"] = $producto["Producto"]['Unidad']["d_unidad"];
                	$producto["ComprobanteItem"]["id_unidad"] = $producto["Producto"]['Unidad']["id"];
                }
                
            
                 if($id_comprobante_filtro !="" && $producto["ComprobanteItemOrigen"]["id"]>0){//si se manda el filtro de COmprobante y ademas tiene un origen calculo cuanto le falta
                     
                     $total_item_comprobante_origen = $producto["ComprobanteItemOrigen"]["cantidad"];
                     $total_notas_credito_origen = $this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItemOrigen"],array(EnumTipoComprobante::NotaCreditoA,EnumTipoComprobante::NotaCreditoB,EnumTipoComprobante::NotaCreditoC,EnumTipoComprobante::NotaCreditoM,EnumTipoComprobante::NotaCreditoE) );
                     
                     
                     /*ej: PI 10 -> sin id FC 2  total_pendiente_a_facturar_origen = 10
                           PI 10 -> con id FC 2(ABIERTO) total_pendiente_a_facturar_origen = 8
                           PI 10 -> con id FC 2(CERRADO) total_pendiente_a_facturar_origen = 8 */
                     $producto["ComprobanteItem"]["total_pendiente_notas_credito_origen"] =  
                                    (string)  number_format((float) ($total_item_comprobante_origen 
                                    - $this->ComprobanteItem->GetCantidadItemProcesados($total_notas_credito_origen)), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');;
                  
                 }
                 
                 $producto["ComprobanteItem"]["d_producto"] = $producto["ComprobanteItem"]["item_observacion"];
                 $producto["ComprobanteItem"]["codigo"] = $this->ComprobanteItem->getCodigoFromProducto($producto);
             
                // $producto["ComprobanteItem"]["total"] = (string) $this->{$this->model}->getTotalItem($producto);
                 
                 unset($producto["Producto"]);
                 unset($producto["Comprobante"]);
                 unset($producto["ComprobanteOrigen"]);
                 unset($producto["ComprobanteItemOrigen"]);
                 unset($producto["ComprobanteRecepcionItem"]);
                 unset($producto["Iva"]);
                 unset($producto["Unidad"]);
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    
     /**
    * @secured(CONSULTA_NOTA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
      
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        
     
      $this->set('model',$model);
      Configure::write('debug',0);
      $this->render($vista);
        
       
   
        

        
    }
    

    
   
    
}
?>