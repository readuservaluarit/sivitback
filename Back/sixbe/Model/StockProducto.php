<?php
App::uses('AppModel', 'Model');
App::uses('StockArticulo', 'Model');

/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class StockProducto extends StockArticulo {


    
    public $belongsTo = array(
        'Producto' => array(
            'className' => 'Producto',
            'foreignKey' => 'id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Deposito' => array(
            'className' => 'Deposito',
            'foreignKey' => 'id_deposito',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        
  );
    
    
    
}




