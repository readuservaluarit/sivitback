<?php

    /*SE INFORMAN JUNTAS RETENCION Y PERCEPCION*/


   if($nombre_archivo  == "")
       $nombre_archivo = "CabaPercepcionesRetenciones";
        
    header("Content-Disposition: attachment; filename=".$nombre_archivo.".txt");
    
    header("Pragma: no-cache"); 
    header("Expires: 0"); 
    
    $comprobante_obj  = new Comprobante();
    $id_moneda_base  = $dato_empresa["DatoEmpresa"]["id_moneda"];
    
    foreach($datos as $comprobante){
        
		$linea_log='';
        
        $tipo_operacion_01 = $tipo_reporte;//1 retencion  2 percepcion 
       
		if($comprobante["ComprobanteImpuesto"]["id_impuesto"] == EnumImpuesto::PERCEPIIBBCABA)
			$tipo_operacion_01 = '2';
		else
			$tipo_operacion_01 = '1';
         
       $codigo_norma_02 = "029";//29 http://www.agip.gob.ar/agentes/agentes-de-recaudacion/ib-agentes-recaudacion/aplicativo-arciba/ag-rec-arciba-codigo-de-normas
	   
	  

       $tipo_comprobante_agip_04 = str_pad($comprobante["Comprobante"]["TipoComprobante"]["codigo_agip"], 2, '0', STR_PAD_LEFT);
       
       
       $codigo_agip_original = trim($comprobante["Comprobante"]["TipoComprobante"]["codigo_agip"]);
       
	   $letra_tipo_comprobante_05 = trim($comprobante["Comprobante"]["TipoComprobante"]["letra"]);
	  
     
       $fecha = new DateTime($comprobante["Comprobante"]['fecha_contable']);
       $fecha_03 =$fecha->format('d/m/Y'); 
       
       $punto_venta = str_pad($comprobante["Comprobante"]["PuntoVenta"]["numero"], 4, '0', STR_PAD_LEFT);
	   
	   $linea_log=$linea_log.'pto_vta:'.$punto_venta;
	   
       $nro_comprobante_06 = str_pad($comprobante["Comprobante"]["nro_comprobante"], 12, '0', STR_PAD_LEFT);
      

       $fecha = new DateTime($comprobante["Comprobante"]['fecha_contable']);
       $fecha_comprobante_07 =$fecha->format('d/m/Y'); 
       

       if($comprobante["ComprobanteImpuesto"]["id_impuesto"] == EnumImpuesto::PERCEPIIBBCABA)
			$nro_certificado_propio_09 = '0000000000000000';
       else{
			$nro_certificado_propio_09 = str_pad($comprobante["ComprobanteReferencia"]["nro_comprobante"], 16, '0', STR_PAD_LEFT);
 
       }
	   $comp_ref = $comprobante_obj->getComprobantesRelacionados($comprobante["Comprobante"]["id"],801,0);
	   
	   
	   // $valor[$this->model]['nro_comprobante_origen'] = (string) $valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["Comprobante"]["nro_comprobante"];
	   

	   // $linea_log=$linea_log.'id:'.$comprobante["Comprobante"]["id"];
	   // $linea_log=$linea_log.'com_item_id:'.$comprobante["Comprobante"]['ComprobanteItem'][0]["id"];
		$linea_log=$linea_log.'nro_comprobante_anulacion:'.$comprobante["Comprobante"]['ComprobanteItem'][0]["ComprobanteItemOrigen"]["Comprobante"]["nro_comprobante"];
		
       if(isset($comprobante["ComprobanteReferencia"]["nro_comprobante"]) && $comprobante["ComprobanteReferencia"]["nro_comprobante"]>0)
       		$nro_comprobante_origen = str_pad($comprobante["ComprobanteReferencia"]["nro_comprobante"], 16, '0', STR_PAD_LEFT);
       else
       		$nro_comprobante_origen = '0000000000000000';
       
       if($comprobante["Comprobante"]["Persona"]["id_tipo_documento"] == EnumTipoDocumento::CUIT){
			$tipo_documento_retenido_10 = 3;
			$situacion_ib_12 = str_pad($comprobante["Comprobante"]["Persona"]["SituacionIb"]["codigo"],1,'0',STR_PAD_LEFT);
        }
       elseif($comprobante["Comprobante"]["Persona"]["id_tipo_documento"] == EnumTipoDocumento::CUIL){
			$situacion_ib_12 = str_pad(4,1,'0',STR_PAD_LEFT);
			$tipo_documento_retenido_10 = 2;
        }
       elseif($comprobante["Comprobante"]["Persona"]["id_tipo_documento"] == EnumTipoDocumento::CDI){
			$situacion_ib_12 = str_pad(4,1,'0',STR_PAD_LEFT);
			 $tipo_documento_retenido_10 = 1;
        }
         
       $nro_doc_final_11 =  str_pad($comprobante["Comprobante"]["Persona"]["cuit"], 11, '0', STR_PAD_LEFT);

       /*if($comprobante["Comprobante"]["Persona"]["SituacionIb"]["codigo"] == 4)
			$nro_iibb_13 = '00000000000';
       else
			$nro_iibb_13 = str_pad($comprobante["Comprobante"]["Persona"]["ib"], 11, '0', STR_PAD_LEFT);
        */
        
       //este es un FIX que hay que sacar para que carguen bien los IIBB
       $situacion_ib_12 = 4;
       $nro_iibb_13 = '00000000000';
       
       
       if($comprobante["Comprobante"]["Persona"]["id_tipo_iva"] == EnumTipoIva::IvaResponsableInscripto)
			$situacion_iva_retenido_14 = 1;
       elseif( $comprobante["Comprobante"]["Persona"]["id_tipo_iva"] == EnumTipoIva::ResponsableMonotributo)
			$situacion_iva_retenido_14 = 4;
       elseif( $comprobante["Comprobante"]["Persona"]["id_tipo_iva"] == EnumTipoIva::MonotribSocial)
			$situacion_iva_retenido_14 = 4;
       elseif( $comprobante["Comprobante"]["Persona"]["id_tipo_iva"] == EnumTipoIva::IVASujetoExento)
			$situacion_iva_retenido_14 = 3;
       
        $razon_social = $comprobante_obj->sanear_string(trim($comprobante["Comprobante"]["Persona"]["razon_social"]));
       
       if(strlen($razon_social)>30)
        $razon_social = substr(trim($razon_social),0,30);
       
       $razon_social_15 =  str_pad(trim($razon_social), 30, ' ', STR_PAD_RIGHT);
       
		
		
       $importe_iva_17 = $comprobante_obj->getTotalPorImpuestoGeografia($comprobante["Comprobante"]["id"],EnumImpuestoGeografia::ImpuestoNacional,EnumImpuesto::IVANORMAL);

       $importe_iva_original = $importe_iva_17;//necesito el original para hacer calculos
       
       

       $total_comprobante = $comprobante["Comprobante"]["total_comprobante"];
       
       /*
       $suma_percepciones = $comprobante_obj->getTotalPorImpuestoGeografia($comprobante["Comprobante"]["id"],0,0, EnumTipoImpuesto::PERCEPCIONES,0);
       $importe_otros_conceptos = $suma_percepciones;
      */
      
       $importe_otros_conceptos = 0.00;
         
       $importe_otros_conceptos = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($importe_otros_conceptos,13,2,"0");;
       $parte_entera = substr($importe_otros_conceptos,0,13);
       $parte_decimal = substr($importe_otros_conceptos,13,2);
       $importe_otros_conceptos_16 = $parte_entera.','.$parte_decimal;
        
         
       if($letra_tipo_comprobante_05 == "A" || $letra_tipo_comprobante_05 == "M"){ 
        
        $importe_iva_17 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($importe_iva_17,"valor_moneda2", $comprobante["Comprobante"]);
        $importe_iva_17   = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($importe_iva_17,13,2,"0");
        $parte_entera = substr($importe_iva_17,0,13);
        $parte_decimal = substr($importe_iva_17,13,2);
        $importe_iva_17 = $parte_entera.','.$parte_decimal;
        
       }else{
       
       	 $importe_iva_17 = '0000000000000,00';
       	 $importe_otros_conceptos_16 = '0000000000000,00';
       }
       
      
       $base_imponible = $comprobante_obj->ExpresarEnTipoMonedaMejorado($comprobante["ComprobanteImpuesto"]["base_imponible"],"valor_moneda2", $comprobante["Comprobante"]);
       
       $base_imponible_original = $base_imponible; 
       
       $base_imponible = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($base_imponible,13,2,"0");
       $parte_entera = substr($base_imponible,0,13);
       $parte_decimal = substr($base_imponible,13,2);
       $base_imponible_18 = $parte_entera.','.$parte_decimal;
       

       
     if($comprobante["ComprobanteImpuesto"]["id_impuesto"] == EnumImpuesto::PERCEPIIBBCABA){
       
       		$total_comprobante =  $comprobante["ComprobanteImpuesto"]["base_imponible"]; 
       		$total_comprobante = $comprobante_obj->ExpresarEnTipoMonedaMejorado($total_comprobante,"valor_moneda2", $comprobante["Comprobante"]);

       }else{//el iva no lo sumo en las Retenciones en la OP ya que no importa
       
 			$total_comprobante  = $comprobante["ComprobanteImpuesto"]["base_imponible"];
 			$total_comprobante = $comprobante_obj->ExpresarEnTipoMonedaMejorado($total_comprobante,"valor_moneda2", $comprobante["Comprobante"]);
       }
       
      
       $total_comprobante = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_comprobante,13,2,"0");
       $parte_entera = substr($total_comprobante,0,13);
       $parte_decimal = substr($total_comprobante,13,2);
       $total_comprobante_08 = $parte_entera.','.$parte_decimal;
       
       
       
       $tasa_impuesto = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($comprobante["ComprobanteImpuesto"]["tasa_impuesto"],2,2,"0");
       $parte_entera = substr($tasa_impuesto,0,2);
       $parte_decimal = substr($tasa_impuesto,2,2);
       $tasa_impuesto_19 = $parte_entera.','.$parte_decimal;
       
       
       $importe_impuesto_calculado = round(($base_imponible_original*$comprobante["ComprobanteImpuesto"]["tasa_impuesto"])/100,2);
        
       
      //$importe_impuesto = $comprobante_obj->ExpresarEnTipoMonedaMejorado($comprobante["ComprobanteImpuesto"]["importe_impuesto"],"valor_moneda2", $comprobante["Comprobante"],1,$comprobante["ComprobanteImpuesto"]["base_imponible"],$comprobante["ComprobanteImpuesto"]["tasa_impuesto"]);
       
       $importe_impuesto = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($importe_impuesto_calculado,13,2,"0");
       $parte_entera = substr($importe_impuesto,0,13);
       $parte_decimal = substr($importe_impuesto,13,2);
       $importe_impuesto_20 = $parte_entera.','.$parte_decimal;
       
       $monto_total_retenido_21 = $importe_impuesto_20;
       
       
		if($linea_log!='')
			$linea_log= $linea_log.  "&&\r\n";  
       
       $linea_texto = $linea_log.//va en el renglon de arriba para diferenciar
			$tipo_operacion_01.
			$nro_comprobante_06.$fecha_comprobante_07.
			$total_comprobante_08.$nro_certificado_propio_09.'01'.
			$letra_tipo_comprobante_05.$nro_comprobante_origen.
			$nro_doc_final_11.$codigo_norma_02.$fecha_comprobante_07.
			$monto_total_retenido_21.$tasa_impuesto_19; 
       
       
       if($comprobante["ComprobanteImpuesto"]["importe_impuesto"]>0)
       		echo $linea_texto.  "\r\n";       
       
       
       
    }
    
        


?>