<?php

App::uses('PersonasController', 'Controller');


class ProveedoresController extends PersonasController {
    public $name = 'Proveedores';
    public $model = 'Persona';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $id_tipo_persona = EnumTipoPersona::Proveedor;
    
  
    
    /**
    * @secured(CONSULTA_PROVEEDOR)
    */
    public function index() {
        
        
        $this->request->data['Persona']['id_tipo_persona'] = $this->id_tipo_persona;
        parent::index();
        return;
        
    }
    
    /**
    * @secured(MODIFICACION_PROVEEDOR)
    */
    public function abm($mode, $id = null) {
        
        parent::abm($mode,$id);
        return;
        
    }

    /**
    * @secured(ABM_PROVEEDOR)
    */
    public function view($id) {        
        parent::view($id);
        return;
    }

    /**
    * @secured(ADD_PROVEEDOR)
    */
    public function add() {
    
        $this->request->data["Persona"]["id_tipo_persona"] = $this->id_tipo_persona;
        parent::add(); 
        return;
        
      
    }
   
   
   /**
    * @secured(MODIFICACION_PROVEEDOR)
    */ 
   public function edit($id) {
       
       $this->request->data['Persona']['id_tipo_persona'] = $this->id_tipo_persona;
        
        parent::edit($id);   
        return;   
  }
    
  
    /**
    * @secured(BAJA_PROVEEDOR)
    */
    function delete($id) {
        
        parent::delete($id);
        return;
        
    }
    


   
    
    
    
    
    public function home(){
        parent::home();
        return;
    }
    
  
    /**
    * @secured(CONSULTA_PROVEEDOR)
    */
    public function existe_cuit(){
        
       parent::existe_cuit();
       return;
    }
    
    
    /**
    * @secured(CONSULTA_PROVEEDOR)
    */
     public function existe_codigo($codigo='',$id_persona='',$llamada_interna=0){
        
       $dato = parent::existe_codigo($codigo,$id_persona,$llamada_interna);
       if($llamada_interna == 1)
        return $dato;
       else
        return;
    }
    
    
    
    
    
    
     /**
        * @secured(CONSULTA_PROVEEDOR)
        */
        public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
            //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);

            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);
        }
        
        
        /**
         * @secured(BTN_EXCEL_PROVEEDORES)
         */
        public function excelExport($vista="default",$metodo="index",$titulo=""){
        	
        	parent::excelExport($vista,$metodo,"Listado de Proveedores");
        	
        	
        	
        }
        
        /**
         * @secured(CONSULTA_PROVEEDOR)
         */
        public function getDataCuitFromAfip(){
        	
        	
        	parent::getDataCuitFromAfip();
        	return ;
        	
        }
}
?>