<?php   // CONFIG  CON MODE DISPLAYED CELLS
        include(APP.'View'.DS.'ListaPrecios'.DS.'json'.DS.'lista_precio.ctp');
            
  
        
        $model["d_lista_precio"]["show_grid"] = 1;
        $model["d_lista_precio"]["header_display"] = "Descripci&oacute;n";
        $model["d_lista_precio"]["order"] = "1";
        $model["d_lista_precio"]["width"] = "15";
        $model["d_lista_precio"]["text_colour"] = "#AA0033";
		
		
		
		$model["d_por_defecto"]["show_grid"] = 1;
        $model["d_por_defecto"]["header_display"] = "Por Defecto";
        $model["d_por_defecto"]["order"] = "2";
        $model["d_por_defecto"]["width"] = "4";
        $model["d_por_defecto"]["text_colour"] = "#22AA00";
        
        
        
        
        $model["d_tipo_lista_precio"]["show_grid"] = 1;
        $model["d_tipo_lista_precio"]["header_display"] = "Tipo";
        $model["d_tipo_lista_precio"]["order"] = "3";
        $model["d_tipo_lista_precio"]["width"] = "6";
        $model["d_tipo_lista_precio"]["text_colour"] = "#991122";
        
        
        $model["fecha_vencimiento"]["show_grid"] = 1;
        $model["fecha_vencimiento"]["header_display"] = "Fcha Vto.";
        $model["fecha_vencimiento"]["order"] = "4";
        $model["fecha_vencimiento"]["width"] = "4";
        $model["fecha_vencimiento"]["text_colour"] = "#22AA00";
        
        
        
        $model["porcentaje_variacion"]["show_grid"] = 1;
        $model["porcentaje_variacion"]["header_display"] = "% Variaci&oacute;n";
        $model["porcentaje_variacion"]["order"] = "5";
        $model["porcentaje_variacion"]["width"] = "5";
        $model["porcentaje_variacion"]["text_colour"] = "#000000";
        
        $model["d_lista_padre"]["show_grid"] = 1;
        $model["d_lista_padre"]["header_display"] = "Lista Padre";
        $model["d_lista_padre"]["order"] = "6";
        $model["d_lista_padre"]["width"] = "15";
        $model["d_lista_padre"]["text_colour"] = "#DD0022";
		
		$model["d_tipo_lista_precio_padre"]["show_grid"] = 1;
        $model["d_tipo_lista_precio_padre"]["header_display"] = "Tipo Lista Padre";
        $model["d_tipo_lista_precio_padre"]["order"] = "7";
        $model["d_tipo_lista_precio_padre"]["width"] = "6";
        $model["d_tipo_lista_precio_padre"]["text_colour"] = "#EE0022";
        
        
        
        $model["moneda_simbolo"]["show_grid"] = 1;
        $model["moneda_simbolo"]["header_display"] = "Moneda";
        $model["moneda_simbolo"]["order"] = "8";
        $model["moneda_simbolo"]["width"] = "6";
        $model["moneda_simbolo"]["text_colour"] = "#EE0022";
        
         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ListaPrecio",
                    "content" => $model
                );
        
          echo json_encode($output);
?>