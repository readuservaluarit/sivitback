<?php 



        include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');
        
		$model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["header_display"] = "Razon Social";
        $model["razon_social"]["order"] = "0";
        $model["razon_social"]["width"] = "25";
        
		$model["persona_tel"]["show_grid"] = 1;
        $model["persona_tel"]["header_display"] = "Tel&eacute;fono";
        $model["persona_tel"]["order"] = "1";
        $model["persona_tel"]["width"] = "15";

		$model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Nro. Comp.";
        $model["pto_nro_comprobante"]["order"] = "2";
        $model["pto_nro_comprobante"]["width"] = "15";
		
		$model["orden"]["show_grid"] = 1;
        $model["orden"]["header_display"] = "Nro. Ord";
        $model["orden"]["order"] = "3";
        $model["orden"]["width"] = "3";
        
        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "C&oacute;digo";
        $model["codigo"]["order"] = "4";
        $model["codigo"]["width"] = "15";
        
        
        $model["fecha_generacion"]["show_grid"] = 1;
		$model["fecha_generacion"]["header_display"] = "Fch Generaci&oacute;n";
		$model["fecha_generacion"]["order"] = "5";
		$model["fecha_generacion"]["width"] = "8";
		
		$model["fecha_entrega"]["show_grid"] = 1;
		$model["fecha_entrega"]["header_display"] = "Fch Entrega";
		$model["fecha_entrega"]["order"] = "6";
		$model["fecha_entrega"]["width"] = "7";
		$model["fecha_entrega"]["text_colour"] = "#AA1100";//RED GAO
		
		$model["dias"]["show_grid"] = 1;
		$model["dias"]["header_display"] = "D&iacute;as Prometidos";
		$model["dias"]["order"] = "7";
		$model["dias"]["width"] = "5";
		
		
		$model["dias_atraso_item"]["show_grid"] = 1;
		$model["dias_atraso_item"]["header_display"] = "D&iacute;as Atraso";
		$model["dias_atraso_item"]["order"] = "8";
		$model["dias_atraso_item"]["width"] = "5";
		
		
		$model["dias_atraso_global"]["show_grid"] = 1;
		$model["dias_atraso_global"]["header_display"] = "D&iacute;as Atraso Global";
		$model["dias_atraso_global"]["order"] = "9";
		$model["dias_atraso_global"]["width"] = "5";
        
        $model["item_observacion"]["show_grid"] = 1;
        $model["item_observacion"]["header_display"] = "Observaci&oacute;n";
        $model["item_observacion"]["order"] = "10";
        $model["item_observacion"]["width"] = "20";
		
		$model["d_unidad"]["show_grid"] = 1;
        $model["d_unidad"]["header_display"] = "U";
        $model["d_unidad"]["order"] = "11";
        $model["d_unidad"]["width"] = "10";
        
        $model["cantidad"]["show_grid"] = 1;
        $model["cantidad"]["header_display"] = "Cantidad";
        $model["cantidad"]["order"] = "12";
        $model["cantidad"]["width"] = "9";
		$model["cantidad"]["text_colour"] = "#0000FF";
        
        $model["fecha_entrega"]["show_grid"] = 1;
        $model["fecha_entrega"]["header_display"] = "Fecha Entrega";
        $model["fecha_entrega"]["order"] = "13";
        $model["fecha_entrega"]["width"] = "15";
        $model["fecha_entrega"]["text_colour"] = "#0000FF";
		
		
		
		$model["cantidad_pendiente_a_remitir"]["show_grid"] = 1;
        $model["cantidad_pendiente_a_remitir"]["header_display"] = "Pendiente Rmto.";
        $model["cantidad_pendiente_a_remitir"]["order"] = "11";
        $model["cantidad_pendiente_a_remitir"]["width"] = "4";
		$model["cantidad_pendiente_a_remitir"]["text_colour"] = "#004400";
		$model["cantidad_pendiente_a_remitir"]["read_only"] = "1";
		
		
		
		$model["cantidad_recibida_irm"]["show_grid"] = 1;
        $model["cantidad_recibida_irm"]["header_display"] = "Recibido IRM";
        $model["cantidad_recibida_irm"]["order"] = "14";
        $model["cantidad_recibida_irm"]["width"] = "9";
		$model["cantidad_recibida_irm"]["text_colour"] = "#004400";
		
		$model["cantidad_pendiente_irm"]["show_grid"] = 1;
        $model["cantidad_pendiente_irm"]["header_display"] = "Pendiente IRM";
        $model["cantidad_pendiente_irm"]["order"] = "15";
        $model["cantidad_pendiente_irm"]["width"] = "9";
		$model["cantidad_pendiente_irm"]["text_colour"] = "#F0000F";
        
        
        $model["cantidad_pendiente_a_facturar"]["show_grid"] = 1;
        $model["cantidad_pendiente_a_facturar"]["header_display"] = "Cant. Pend FC.";
        $model["cantidad_pendiente_a_facturar"]["order"] = "16";            
        $model["cantidad_pendiente_a_facturar"]["width"] = "10";
        $model["cantidad_pendiente_a_facturar"]["text_colour"] = "#0000FF";//BLUE
		
		$model["precio_unitario"]["show_grid"] = 1;
        $model["precio_unitario"]["header_display"] = "Precio Unitario";
        $model["precio_unitario"]["order"] = "17";
        $model["precio_unitario"]["width"] = "10";
        
        $model["descuento_unitario"]["show_grid"] = 1;
        $model["descuento_unitario"]["header_display"] = "Desc. Unitario";
        $model["descuento_unitario"]["order"] = "18";
        $model["descuento_unitario"]["width"] = "5";
        
        $model["d_estado_comprobante"]["show_grid"] = 1;
        $model["d_estado_comprobante"]["header_display"] = "Estado";
        $model["d_estado_comprobante"]["order"] = "19";
        $model["d_estado_comprobante"]["width"] = "11";
		
 $output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "ComprobanteItem",
				"content" => $model
                );
        echo json_encode($output);
?>