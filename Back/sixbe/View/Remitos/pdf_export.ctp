<?php   

function digitoVerificadorCaeAfip($txt){
  $i = $pares = $impares = 0;

  for( $i = 0; $i < strlen($txt); $i++ ) {
    // OJO: Los caracteres impares esta en la posiciones pares del array y viceversa, porque el array arranca desde la posicion 0
    if( $i % 2 ) {
      $pares += $txt[$i];
    } else {
      $impares += $txt[$i];
    }
  }
  $impares = $impares * 3;
  
  $total = $pares + $impares;
  
  $dv = $total % 10;

  return $dv;
}

/**
 * PDF exportaciÃ³n de Orden Compra
 */

App::import('Vendor','tcpdf/tcpdf'); 
App::import('Vendor','tcpdf/tcpdf_barcodes_1d'); 
App::import('Model','Comprobante');



	


$comprobante = new Comprobante();


// create new PDF document

  $ConfigEmpresa = $this->Session->read('Empresa'); //Leo los datos de datoEmpresa en session cargados en Usuarios

  
$html = '<table width="100%" border="0" cellspacing="0" style="font-size:10px;border:0px solid #000000; font-family:\'Courier New\', Courier, monospace">
          <tr>
		  
            <td style="padding: 5px 5px 5px 5px;" width="46%" rowspan="4" align="center" valign="center"  >
				
			</td>
            <td width="8%" height="3%" rowspan="2"  style="border:0px solid #000000">
                <div align="center" style="font-size:40px;color:white;">R</div>
            </td>
            <td width="46%" style="padding-left:5px; font-size:17px;font-weight:bold;color:white;"> REMITO: </td>
            
          </tr>
          <tr>
            <td style="padding-left:5px; font-size:15px;color:white;"> Nro '.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'</td>
          </tr> 
          
          <tr>
            <td width="8%" height="2%" align="center" valign="middle" style="color:white;" >Cod 91.</td>
            <td></td>
          </tr>
          <tr >
            <td  width="8%" height="5%" align="center" valign="middle" >&nbsp;</td>
            <td style="padding-left:5px;"> <span style="font-size:5px;color:white;"> <strong>Documento No vÃ¡lido como factura</strong></span></td>
          </tr>
          <tr>
            <td style="padding-left:5px;color:white;padding-top:50px;"> '.$ConfigEmpresa["DatoEmpresa"]["calle"].' '.$ConfigEmpresa["DatoEmpresa"]["numero"].' '.$ConfigEmpresa["DatoEmpresa"]["codigo_postal"].' '.$ConfigEmpresa["Provincia"]["d_provincia"].'</td>
            <td rowspan="5">&nbsp;</td>
            <td style="padding-left:5px;padding-top: 500px;"><span><strong style="color:white;">Fecha:</strong></span>'.$datos_pdf["Comprobante"]["fecha_generacion"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;padding-top:20px;"><span style="font-size:5px;color:white;"><span style="color:white;" ><strong>E-mail:</strong></span> '.$ConfigEmpresa["DatoEmpresa"]["email"].'</td>
            <td style="padding-left:5px;padding-top:20px;"><span style="font-size:5px;color:white;"><span style="color:white;"> <strong>CUIT:</strong></span> '.$ConfigEmpresa["DatoEmpresa"]["cuit"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;padding-top:20px;"> <span style="font-size:5px;color:white;"><span style="color:white;" ><strong>Tel:</strong></span> '.$ConfigEmpresa["DatoEmpresa"]["tel"].'</td>
            <td style="padding-left:5px;padding-top:20px;"><span style="font-size:5px;color:white;"><span style="color:white;" > <strong>Ingresos Brutos:</strong></span> '.$ConfigEmpresa["DatoEmpresa"]["iibb"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$ConfigEmpresa["DatoEmpresa"]["website"].'</td>
            <td style="padding-left:5px;"><span style="font-size:5px;color:white;"><span style="color:white;" > <strong>Inicio de actividades:</strong></span> '.$ConfigEmpresa["DatoEmpresa"]["fecha_inicio_actividad"].'</td>
          </tr>
          <tr>
            
            <td style="padding-left:5px;color:white;"> '.strtoupper($ConfigEmpresa["TipoIva"]["d_tipo_iva"]).'</td>
            <td style="padding-left:5px;"></td>
          </tr>
          <tr>
            
            <td style="padding-left:5px;color:white;"> '.strtoupper($ConfigEmpresa["TipoIva"]["d_tipo_iva"]).'</td>
            <td style="padding-left:5px;"></td>
          </tr>
          <tr>
            
            <td style="padding-left:5px;color:white;"> '.strtoupper($ConfigEmpresa["TipoIva"]["d_tipo_iva"]).'</td>
            <td style="padding-left:5px;"></td>
          </tr>
            
        </table>
        
       
        
        ';



class MyTCPDF extends TCPDF{

    public $datos_header;
    public $datos_footer;
    public $bar_code;
    public $observaciones;
    
    public function set_datos_header($datos_empresa){
        $this->datos_header = $datos_empresa; 

    }
    public function set_datos_footer($datos_empresa_footer){
        $this->datos_footer = $datos_empresa_footer; 

    }
     public function set_barcode($bar_code){
        $this->bar_code = $bar_code; 

    }
    
    public function set_observaciones($observaciones){
        $this->observaciones = $observaciones; 

    }
  public function Header(){
     
        
        
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_header, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
  }
  
  public function Footer(){
     
        
        
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_footer, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->observaciones, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
     
     $style['text'] = true;
     $style['border'] = true;
     $style['align'] = 'C';
     //$this->write1DBarcode($this->bar_code, 'I25', '40', '', '', 18, 0.4, $style, 'N');
      
      //$this->writeHTMLCell($w = 0, $h = 0, $x = '200', $y = '', $this->bar_code, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
      //$this->Cell(0, 20, $this->bar_code , 0, false, 'C', 0, '', 0, false, 'T', 'M');
      //$this->Cell(0, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
      
  }
  
}

$pdf = new MyTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->set_datos_header($html);





//genero el codigo de barras para ver las especificaciones http://www.formularioscontinuos.com/nota1.html
if( $datos_pdf["Comprobante"]["cae"] != '' ){

    //genero el string para el codigo de barras
    $cuit =  str_replace("-","",$datos_empresa["DatoEmpresa"]["cuit"]);
    $tipo_doc_afip = str_pad($datos_pdf["TipoComprobante"]["tipo_documento_afip"],2,"0",STR_PAD_LEFT);
    $cae = $datos_pdf["Factura"]["cae"];
    $fecha_vencimiento = str_replace("-","",$datos_pdf["Factura"]["vencimiento_cae"]);
    
    $codigo_barras = $cuit.$tipo_doc_afip.$cae.$fecha_vencimiento;
    $codigo_barras = $codigo_barras.digitoVerificadorCaeAfip($codigo_barras);
    $pdf->set_barcode($codigo_barras);  
    
   
}   

 $persona = new Persona();
 $direccion_transportista = $persona->getDireccion($datos_pdf["Transportista"]);


    $observaciones = $datos_pdf["Comprobante"]["observacion"];


//$pdf->set_observaciones($observaciones);



   
  if($datos_pdf["Comprobante"]["imprimir_obs_cuerpo"] != 1)
  	$observacion =  nl2br($datos_pdf["Comprobante"]["observacion"]);
  else
  	$observacion = "";
   
      


$footer='
   <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:9px;font-family:\'Courier New\', Courier, monospace;">
 
   <tr >
   
    <th width="100%"  colspan="2" scope="col" style="font-size:12px;"><span style="color:white;">Destino:</span>  </th>
  </tr>
  
  </table>
 <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:9px;font-family:\'Courier New\', Courier, monospace;">
 
   <tr >
   
    <th width="100%"  colspan="2" scope="col" style="font-size:12px;"><span style="color:white;">Destino:</span> '.$datos_pdf["Comprobante"]["lugar_entrega"].'  </th>
  </tr>
  
  
  
 <tr >
    <th width="100%" scope="col"><span style="color:white;">Lugar Entrega / Transportista:</span>  '.$direccion_transportista.'</th>
   
 </tr>
 
 
  
  <tr >
   
    <th width="100%"  colspan="2" scope="col"><span style="color:white;">Observaciones:</span> '.$observacion.'  </th>
  </tr>
  <tr >
  
 
  
 

  
  <tr>
   <td width="50%" scope="col"><span style="color:white;">Importe declarado:</span> '.$datos_pdf["Comprobante"]["Moneda"]["d_moneda"].' '.money_format('%!n',$datos_pdf["Comprobante"]["importe_declarado"]).' </td>
    <td width="50%" scope="col"><span style="color:white;">Cantidad de Bultos:</span>  '.$datos_pdf["Comprobante"]["cantidad_bultos"].'</td>
  </tr>
  
<tr>
   
    <td style="font-size:8px;" width="50%" scope="col"></td>
    <td style="font-size:8px;" width="50%" scope="col"></td>
   
  </tr >
  <tr>
   
    <td style="font-size:8px;" width="50%" scope="col"></td>
    <td style="font-size:8px;" width="50%" scope="col"></td>
   
  </tr >

  
  <tr>
   
    <td style="font-size:8px;color:white;" width="50%" scope="col">CAI:  </td>
    <td style="font-size:8px;color:white;" width="50%" scope="col">VENCIMIENTO:</td>
   
  </tr >
   <tr style="font-size:8px;color:white;">
   <td width="100%" scope="col">TALLERES GRAFICOS POL de E.Pradella y N.CendÃ³n- CUIT NÂ° 30-56957961-5- Fecha de impresiÃ³n: 03/06/2015</td>
    
   </tr>
   
   <tr style="font-size:8px;color:white;">
   
    <td width="100%" scope="col">Del NÂ°: 0008-00005001 al NÂ° 0008-00007000 HabilitaciÃ³n: NÂ°: 1942-P-1953</td>
   </tr>
   
   <tr>
   <td >
    <td style="font-size:8px;color:white;" width="100%" scope="col">ORIGINAL: BLANCO / DUPLICADO: ROSA / TRIPLICADO: CELESTE</td>
   </td>
   </tr>

</table>';
   
   $html .= "<HR>";
   
   
   if( $datos_pdf["Remito"]["cae"] != '' ){
   
      $html.=$pdf->SetBarcode($codigo_barras,I25);
     
   
   }
   
   
   $html .= '
   
   <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody>
</table>
   
  ';
  
  $pdf->set_datos_footer($footer);
  
  
ob_clean();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Valuarit');
$pdf->SetTitle('Factura');
$pdf->SetSubject('Factura');
//$pdf->SetKeywords('');



// set default header data
$logo = '/img/header_factura.jpg';
$title = "";
$subtitle = ""; 
//$pdf->SetHeaderData($logo, 190, $title, $subtitle, array(0,0,0), array(0,104,128));
//$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(4, 50, 4);
//$pdf->SetMargins(PDF_MARGIN_LEFT, 50, 20);
$pdf->SetHeaderMargin(10);
$pdf->SetFooterMargin(60);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
/*$lg = Array();
$lg['a_meta_charset'] = 'ISO-8859-1';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';
*/
$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set default font subsetting mode
//$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 12, '', true);

//MP: CHANGUÃ� PARA LA PRE-IMPRESION
//MP importante dejar margenes amplios para espaciar las lineas pre impresas del texto q se va a printear encima
//http://stackoverflow.com/questions/9682280/linespacing-in-tcpdf-multicell
$pdf->setCellHeightRatio(2.0);
// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
/*
$html = <<<EOD
<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
EOD;
*/

$tbody = "";

$renglones = $datos_pdf["ComprobanteItem"];
$comprobante_item  =  new ComprobanteItem();

$data = array();

foreach($renglones as $renglon){

  
   
   $tbody .= "<tr style=\"font-size:9px;font-family:'Courier New', Courier, monospace\">";
   $tbody .= "<td>" .$renglon['n_item'] . "</td>";
   $tbody .= "<td>" .$renglon['orden_compra_externa'] . "</td>";
   $tbody .= "<td>" .$renglon['cantidad']. "</td>";
   $tbody .= "<td>" .$comprobante_item->getCodigoFromProducto($renglon). "</td>";
   $tbody .= "<td>" .$renglon['item_observacion'] . "</td>";       
   $tbody .= "</tr>";
}
 
//agrego las observaciones

   

$html='';


$html .='
 <table width="100%"   style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
  <tr >
		<td colspan="4"></td>
		</tr>
  <tr>  
  
 
 </table>
  <table width="100%"   style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
  <tr >
        <td colspan="4"></td>
        </tr>
  <tr>  
  
 
 </table>
 <table width="100%" border="0"  style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
	  <tr  >
		<td colspan="4"><div align="center"><strong style="color:white;">Cliente</strong></div></td>
	  </tr>
	  <tr >                                     
		<td  width="12%" style="text-align:left;height:30px;"><span style="color:white;"><strong >Cod.:</strong></span> '.$datos_pdf["Persona"]["codigo"].'</td>
		<td width="56%" style="text-align:left;height:30px;"><span style="color:white;"><strong >R.Social:</strong></span> '.$datos_pdf["Persona"]["razon_social"].'</td>
		<td width="20%" style="text-align:left;height:30px;"><span style="color:white;"><strong >CUIT:</strong></span> '.$datos_pdf["Persona"]["cuit"].' </td>
		<td width="12%" style="text-align:left;height:30px;"><span style="color:white;"><strong >O/C:</strong></span> '.$datos_pdf["Comprobante"]["orden_compra_externa"].'</td>
	  </tr>
	  <tr>

		<td colspan="2" style="text-align:left;height:30px;"><span style="color:white;"><strong>Direccion:</strong></span>  '.$datos_pdf["Persona"]["calle"].' - '.$datos_pdf["Persona"]["numero_calle"].' - '.$datos_pdf["Persona"]["localidad"].' - '.$datos_pdf["Factura"]["Persona"]["ciudad"].' - '.$datos_pdf["Persona"]["Provincia"]["d_provincia"].' - '.$datos_pdf["Persona"]["Pais"]["d_pais"].'  </td>
		<td colspan="2" style="text-align:left;height:30px;"><span style="color:white;"><strong>TelÃ©fono:</strong></span>  '.$datos_pdf["Persona"]["tel"].'</td>
	  </tr>
</table>

<table width="100%" border="0" style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace">
	  <tr style="text-align:center;">
		<td colspan="2"><strong style="color:white;">Referencias</strong></td>
	  </tr>
	  <tr>
		<td width="50%" style="text-align:left;height:20px"><strong style="color:white;">Pedido/s Interno/s: </strong>'.implode(",", $pedidos_internos_relacionados).'</td>
		<td width="50%" style="text-align:left;height:20px"><strong style="color:white;">Factura/s:</strong>'.implode(",", $facturas_relacionados).' </td>
	  </tr>
</table>
                

<table width="100%" border="0px" cellspacing="0.5" cellpadding="0.5" style="width:100%;font-size:6px;font-family:\'Courier New\', Courier, monospace">
     <tbody>
        <tr >
			<td colspan="2"> </td>
			 <td colspan="2">                           </td>
			 <td colspan="2">                           </td>
        </tr>
        <!-- <tr >
			<td  colspan="6" ><HR></td>
         </tr> -->
     </tbody>
</table>


<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">

    <tr style="text-align:center;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:15%;color:white;"><strong>Item</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;;color:white;"><strong>O/C</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:15%;color:white;"><strong>Cant</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;color:white;"><strong>C&oacute;digo</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:50%;color:white;"><strong>Descripci&oacute;n</strong></th>
    </tr>

  <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
    '.$tbody.'
  </tbody>  
</table>';




$obs_en_cuerpo = ''; 

if($datos_pdf["Comprobante"]["imprimir_obs_cuerpo"] == 1){


$obs_en_cuerpo = '

<table width="100%" border="0"  align="left" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">


<td>


Observaciones:'.nl2br($datos_pdf["Comprobante"]["observacion"]).'
</td>
</table>';


$html.= $obs_en_cuerpo;



}
  
  
  
   
   
   
$pdf->writeHTML($html, true, false, true, false, true);


// QRCODE,L : QR-CODE Low error correction
// set style for barcode
$style = array(
    'border' => 2,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

//$pdf->Text(20, 25, 'QRCODE L');

// ---------------------------------------------------------




$pdf_name = $comprobante->getNamePDF($datos_pdf);
$root_pdf = $comprobante->getRootPDF();


if($datos_pdf["TipoComprobante"]["adjunta_pdf_mail"] == 1) {

	$path_para_pdf = $datos_empresa["DatoEmpresa"]["local_path"].$root_pdf;
          	
	$pdf->Output($path_para_pdf.$pdf_name.'.pdf', 'F');//Esta linea guarda un archivo en path_para_pdf
}


// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->SetBarcode(date("Y-m-d H:i:s", time()));
ob_clean();
$pdf->Output(str_replace(".","",$datos_pdf["TipoComprobante"]["abreviado_tipo_comprobante"].'_'.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'-'.$datos_pdf["Persona"]["razon_social"]).'.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
