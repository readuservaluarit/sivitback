<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class DatoEmpresaImpuesto extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'dato_empresa_impuesto';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
       
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
        'DatoEmpresa' => array(
            'className' => 'DatoEmpresa',
            'foreignKey' => 'id_dato_empresa',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Impuesto' => array(
            'className' => 'Impuesto',
            'foreignKey' => 'id_impuesto',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
   );  
   
   
   
   public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     "id_sistema"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_sistema"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_impuesto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_sin_vencimiento"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_mostrar_libro_iva"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     
     );
     
     }
     
     public function add($dato_empresa_impuesto){
     	
     	
     	
     	$this->saveAll($dato_empresa_impuesto);
     	return;
     	
     }
   
   
   

}
