<?php
App::uses('ComprobantesController', 'Controller');


  /**
    * @secured(CONSULTA_RECIBO)
  */
class RecibosController extends ComprobantesController {
    
    public $name = EnumController::Recibos;
    public $model = 'Comprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $requiere_impuestos = 0;//Flag para impuestos calculados por el sistema
    public $id_sistema_comprobante = EnumSistema::VENTAS;

    
    /**
    * @secured(CONSULTA_RECIBO)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);

        
        $conditions = $this->RecuperoFiltros($this->model);
            
            
                       
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>array('Persona','EstadoComprobante','Moneda','TipoComprobante','ComprobanteItem'=>'ComprobanteOrigen','ComprobanteValor','ComprobanteImpuesto', 'PuntoVenta','Usuario'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.id desc'
        ); //el contacto es la sucursal
        
 // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
            
             //$valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
             //unset($valor['ComprobanteItem']);
             
             if(isset($valor['TipoComprobante'])){
                 $valor[$this->model]['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                 $valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
                 unset($valor['TipoComprobante']);
             }
             
			 $valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
             $valor[$this->model]['nro_comprobante'] = (string) str_pad($valor[$this->model]['nro_comprobante'], 8, "0", STR_PAD_LEFT);
             
           
             if(isset($valor['Moneda'])){
                 $valor[$this->model]['d_moneda_simbolo'] = $valor['Moneda']['simbolo'];
                 unset($valor['Moneda']);
             }
             
             if(isset($valor['EstadoComprobante'])){
                $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
                unset($valor['EstadoComprobante']);
             }
             if(isset($valor['CondicionPago'])){
                $valor[$this->model]['d_condicion_pago'] = $valor['CondicionPago']['d_condicion_pago'];
                unset($valor['CondicionPago']);
             }
          
          
             if(isset($valor['Persona'])){
            
            
                 if($valor['Persona']['id']== null){
                     
                    $valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social']; 
                 }else{
                     
                     $valor[$this->model]['razon_social'] = $valor['Persona']['razon_social'];
                     $valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
                 }
                 
                 unset($valor['Persona']);
                 unset($valor['PuntoVenta']);
                 unset($valor['ComprobanteItem']);
             }
             
             
             if(isset($valor['Usuario'])){
             	
             	$valor['Comprobante']['d_usuario'] = $valor['Usuario']['nombre'];
             }
             
             $this->{$this->model}->formatearFechas($valor);
             
            
        }
        
       
 
 
     
     $this->data = $data;
     
     $this->viewPath = "Layouts";
     $this->set("data",$data);
     
     
     $this->set("page_count",$page_count);
     
     
     $seteo_form_builder = array("showActionColumn" =>true,"showNewButton" =>false,"showDeleteButton"=>false,"showEditButton"=>false,"showPrintButton"=>true,"showFooter"=>true,"showHeaderListado"=>true,"showXlsButton"=>false);
     
     $this->title_form = "Listado de Recibos";
     
     $this->preparaHtmLFormBuilder($this,$seteo_form_builder,"");
     
    //fin vista json
    }
	
				/**
	 * @secured(CONSULTA_RECIBO)
	 */
	public function existe_comprobante()
		{
		parent::existe_comprobante();
		}

    
    
    /**
    * @secured(ADD_RECIBO)
    */
    public function add(){
        
    $this->loadModel("Cheque");
    $cheques_error = '';

     $this->ProcesaDescuento();
     $this->ConvertirPreciosUnitariosAPositivo($this->request->data);//como el front me manda negativos  y el back no guarda negativos entonces convierto a positivo los comprobantes que son negativos
     $this->calcularTasaImpuestoOnTheFly();//le calcula la tasa a las retenciones recibidas
     
    if($this->Cheque->validarIngresoCheque($this->request->data,$cheques_error) == 1)    
            parent::add();    
     else{
              $mensaje = $cheques_error;
              $tipo = EnumError::ERROR;  
              
              $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                );      
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content"));         
     }
            
              
         
         
     }
           
   
    
    
    /**
    * @secured(MODIFICACION_RECIBO)
    */
    public function edit($id){
  
    $this->loadModel("Cheque");
    $this->loadModel("Comprobante");
    $cheques_error = '';
    $hay_error = 0;
    
    
    $this->ConvertirPreciosUnitariosAPositivo($this->request->data);
    
    if($this->Comprobante->getDefinitivoGrabado($id) != 1 ){
	    //si envia decuento entonces calculo el neto
	    $this->ProcesaDescuento();
	    $this->calcularTasaImpuestoOnTheFly();//le calcula la tasa a las retenciones recibidas
	    
	     
	    if($this->Cheque->validarIngresoCheque($this->request->data,$cheques_error) == 1){    
	              //si es definitivo le seteo el id_comprobante a los cheques
	              if($this->Comprobante->getDefinitivoGrabado($id) != 1 && $this->Comprobante->getDefinitivo($this->request->data) == 1  && $this->Cheque->tieneCheques($this->request->data)== 1 ){
	                  
	                  
	                   $id_comprobante_origen = $this->request->data["Comprobante"]["id"];
	                   $array_cheques = $this->Cheque->getArrayCheques($this->request->data);
	                   $ds = $this->Cheque->getdatasource();
	                    try{ 
	                        $ds->begin();
	                        $this->Cheque->actualizaComprobanteCheques($id_comprobante_origen,$array_cheques,"id_comprobante_entrada"); 
	                       
	                   }catch(Exception $e){
	                     
	                     $tipo = EnumError::ERROR;
	                     $mensaje = 'Hubo una falla cuando se actulizaron los comprobantes de entrada en los cheques'.$e->getMessage();
	                     $ds->rollback();
	                     $hay_error = 1;
	                   }
	              }
	              
	        
	              if($hay_error == 0){ /*Sino hubo falla al actualizar los cheques entonces si edito*/
	              
	              	
	              
	                      
	              	$diferencia = $this->request->data["Comprobante"]["diferencia_valores"]; //si es (-) debe generar CI si es (+) genera DI
	                 
	                 //el total del comprobante es la sumatoria de los valores ingresados
	                 $this->request->data["Comprobante"]["total_comprobante"] = $this->request->data["Comprobante"]["total_comprobante"] + $diferencia*-1;
	                         
	                // $this->request->data["Comprobante"]["genera_ci "] =1;
	                if($this->request->data["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion &&
	                		$this->request->data["Comprobante"]["chkGeneraCreditoInterno"] == 1 || $this->request->data["Comprobante"]["chkGeneraDebitoInterno"] == 1){
	                     
	                     
	                			
	                		
	                			
	                			
	                			
					                     //debo generar credito interno ya que me esta pagando de mas. El balance del recibo entre recibido y imputados va a ser 0 ya que el CI compensa. Lo hago transaccional asi es mas seguro.
					                     
	                     
					             
                     if($diferencia < 0){
                     	$id_tipo_comprobante_a_generar = EnumTipoComprobante::CreditoInternoVenta;
                     	$leyenda = "Credito Interno por Recibo Nro: ";
                     }else if($diferencia >0){
                     	$id_tipo_comprobante_a_generar = EnumTipoComprobante::DebitoInternoVenta;
                     	$leyenda = "Debito Interno por Recibo Nro: ";
                     }
                     	$contador = $this->chequeoContador($id_tipo_comprobante_a_generar,$this->request->data["Comprobante"]["id_punto_venta"]);
					                     
	                     if($contador){
	                         
	                         
	                         $dsc = $this->Comprobante->getdatasource();
	                   
	                         try{
	                         
	                             $dsc->begin();
	                             $this->Comprobante->CalcularMonedaBaseSecundaria($this->request->data,$this->model);//esta linea la pongo aca asi puedo acceder al valor_moneda, valor_moneda2 antes de llamar al edit
	                             
	                             
	                             
	                             $nro_comprobante_a_generar = $this->obetener_ultimo_numero_comprobante($id_tipo_comprobante_a_generar,$this->request->data["Comprobante"]["id_punto_venta"],5,"+");
	                             $observacion = $leyenda.$this->request->data["Comprobante"]["nro_comprobante"];
	                             
	                             $diferencia = abs($diferencia);//paso a + la diferencia
	                             
	                             $cabecera_comprobante = $this->Comprobante->getCabeceraComprobante($diferencia,$diferencia,$diferencia,$id_tipo_comprobante_a_generar,date('Y-m-d'),EnumEstadoComprobante::CerradoReimpresion,"",$this->request->data["Comprobante"]["id_moneda"],$this->request->data["Comprobante"]["valor_moneda"],$this->request->data["Comprobante"]["valor_moneda2"],$this->request->data["Comprobante"]["valor_moneda3"],$observacion,$nro_comprobante_a_generar,$this->request->data["Comprobante"]["id_persona"],$this->Auth->user('id'),1,0,1,$this->request->data["Comprobante"]["id_punto_venta"]);
	                             
	                             
	                             $items_comprobante = $this->Comprobante->getComprobanteItem(1,$diferencia,$diferencia,1,1,EnumIva::exento,$observacion,1,$id,0);//el credito interno tiene como origen el recibo
	                             $id_comprobante_generado = $this->Comprobante->InsertaComprobante($cabecera_comprobante,$items_comprobante);
	                             
	                             
	                             
	                             
	                             //$this->generaNotaPorDiferenciaCambio();
	                             //Se inserto entonces actualizo los datos del recibo.
	                             //$this->request->data["Comprobante"]["diferencia"] = 0;
	                             
	                             
	                             //////////Agrego el item Credito interno de ventas al Recibo
	                             
	                             /*
	                             $item_comprobante_credito_interno = $this->Comprobante->getComprobanteItem("",$diferencia,$diferencia,count($this->request->data["ComprobanteItemComprobante"])+1,count($this->request->data["ComprobanteItemComprobante"])+1,EnumIva::exento,"",1,$id_credito_interno,0);
	                             
	                             array_push($this->request->data["ComprobanteItemComprobante"],$item_comprobante_credito_interno["ComprobanteItem"]);
	                             */
	                             
	                             parent::edit($id);
	                             
	                             if($this->error !=1){
	                             	$dsc->commit();
	                             	
	                             if($this->Cheque->tieneCheques($this->request->data)== 1)
	                             	$ds->commit();
	                    
	                             	
	                             	
	                             	if($this->Comprobante->TipoComprobante->getAdjuntaPdfMail($this->request->data["Comprobante"]["id_tipo_comprobante"]) == 1){
	                             		$this->output_pdf_file = 0;//solo PDF
	                             		$this->pdfExport($id);//TODO: debo llamar a la funcion para que genere el PDF y no haga OUTPUT
	                             	}
	                             	
	                             }else
	                             	$dsc->rollback();
	                             
	                             	if(isset($ds))
	                             		$ds->rollback();
	                             
	                             return 0; 
	                             }catch(Exception $e){
	                             
	                             $tipo = EnumError::ERROR;
	                             $mensaje = 'Hubo una falla cuando se intento crear el credito/Debito interno. Intente nuevamente'.$e->getMessage();
	                             $dsc->rollback();
	                             $ds->rollback();
	                             
	                             if(isset($array_cheques) && count($array_cheques)>0)
	                             	$this->Cheque->actualizaComprobanteCheques(null,$array_cheques,"id_comprobante_entrada"); 
	                             
	                             
	                             
	                             
	                         }    
	                     }else{
	                     	
	                          $tipo = EnumError::ERROR;  
	                          $mensaje = "Se debe definir el numero inicial del contador para este comprobante (Credito Interno de Ventas/Debito Interno Ventas).El Recibo no pudo editarse. Inicie el contador y luego edite el recibo"; 
	                         
	                     }
	                    
	                }else{//solo edito no necesito mas nada
	                    
	                     parent::edit($id);
	                     
	                     if($this->Cheque->tieneCheques($this->request->data)== 1 && $this->error == 0 && $hay_error ==0 && isset($ds))
	                     	$ds->commit();
	                     
	                     
	                     if(   $this->error == 1 && isset($array_cheques) && count($array_cheques)>0)
	                     	$this->Cheque->actualizaComprobanteCheques(null,$array_cheques,"id_comprobante_entrada");//algo paso en el edit si marque los cheques los dejo sin marcar 
	                     
	                    
	                   
	                     	if($this->Comprobante->TipoComprobante->getAdjuntaPdfMail($this->request->data["Comprobante"]["id_tipo_comprobante"]) == 1){
	                     		$this->output_pdf_file = 0;//solo PDF
	                     		$this->pdfExport($id);//TODO: debo llamar a la funcion para que genere el PDF y no haga OUTPUT
	                    
	                     	}
	                     	
	                    return 0;
	                }
	              
	              
	               }   
	    }else{
	              $mensaje = $cheques_error;
	              $tipo = EnumError::ERROR;  
	              
	             
	     }
    }else{
    	
    	$mensaje = 'El comprobante en esta instacia no puede ser editado';
    	$tipo = EnumError::ERROR;
    	$this->error = 1;
    }
      /*Aca solo entra si hubo error si entra por el edit sale por flujo de mensaje*/
      $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                );      
            $this->set($output);
      
            $this->set("_serialize", array("status", "message", "content"));
            return;         
                
        
    }
    
    
    /**
    * @secured(BAJA_RECIBO)
    */
    public function delete($id){
        
     parent::delete($id);    
        
    }
    
   /**
    * @secured(BAJA_RECIBO)
    */
    public function deleteItem($id_item,$externo=1){
        
        $this->loadModel($this->model,$externo);
        
        
        
    
        
        
        //TODO: BAJA LOGICA
        $this->{$this->model}->ComprobanteItem->id = $id_item;
       try{
           
           //aca se debe chequear si el item es borrable. O sea sino se factura etc.
           
            if ($this->{$this->model}->ComprobanteItem->delete() ) {
                
                   
        
                $status = EnumError::SUCCESS;
                $mensaje = "El Item ha sido eliminado exitosamente.";
              
            }else{
              
                
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
                
                
            }
        }
        catch(Exception $ex){ 
              $mensaje = "Ha ocurrido un error, el item no ha podido ser borrado";
               $status = EnumError::ERROR; 
        }
        
        
        
        
        if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
        }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
        }
        
        
        
        
    }
    
    
    /**
    * @secured(CONSULTA_RECIBO)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
  
    /**
    * @secured(REPORTE_RECIBO)
    */
    public function pdfExport($id,$vista=''){
        
        
     $this->loadModel("Comprobante");
     $recibo = $this->Comprobante->find('first', array(
                                                        'conditions' => array('Comprobante.id' => $id),
                                                        'contain' =>array('TipoComprobante')
                                                        ));
        
     $comprobantes_relacionados = $this->getComprobantesRelacionados($id,0,1,0);  
     
        
        if($recibo){
            
             $credito_internos_relacionados = array();
             foreach($comprobantes_relacionados as $comprobante){


                    if( isset($comprobante["Comprobante"]) && $comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::CreditoInternoVenta)
                        array_push($credito_internos_relacionados,$comprobante);

                


                }
           
            $this->set('credito_interno_relacionados',$credito_internos_relacionados);
            $this->set('tipo_output',$this->output_pdf_file);
            

            $letra = $recibo["TipoComprobante"]["letra"];
            $vista = "recibo".$letra;  
            parent::pdfExport($id,$vista);  
         }
            
     
     
        
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
    }
    
     /**
    * @secured(MODIFICACION_RECIBO)
    */
    public function Anular($id_recibo){
        
        $this->loadModel("Comprobante");
        $this->loadModel("Asiento");
        $this->loadModel("Cheque");
        
        
          $recibo = $this->Comprobante->find('first', array(
                    'conditions' => array('Comprobante.id'=>$id_recibo,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                    'contain' => array('ComprobanteValor','Asiento','ChequeEntrada')
                ));
                
                 $ds = $this->Comprobante->getdatasource(); 
                 // $ds->rollback();
          $aborto = 0;
          
          $listado_cheques = array();
          
                   
            if( $recibo && $recibo["Comprobante"]["id_estado_comprobante"]!= EnumEstadoComprobante::Anulado ){
                
                try{
                	
                	if($this->PeriodoValidoParaAnular($recibo) == 0)
                		throw new Exception("El Comprobante no es posible anularlo.
					Ya que el Peri&oacute;do contable en el cual se encuentra no esta disponible.");
                	
                    $ds->begin();
                    
                     /*Se deben hacer acciones con los cheques*/
                     foreach($recibo["ChequeEntrada"] as $cheque){ //me fijo si los cheques que ingrese aca no fueron utilizados
                       if($cheque["id_comprobante_salida"] >0)
                            $aborto =1; //el cheque fue usado no lo puedo revertir al recibo
                            //TODO: Si el cheque fue usado modificar el revertiAsiento. Lo que debe hacer es si el Cheque fue usado para un pago 
							//o un deposito entonces debe pasar el monto del cheque a la cuenta de efectivo y borrar del asiento lo que tenga que ver con cheques
                            //https://docs.google.com/document/d/1UCAB2BGTTor549GLaDFrjDcpc78bKL0_MQwO_ftNxoM/edit 
                     
                     }
                       
                       
                     if($aborto!=1){
                               foreach($recibo["ChequeEntrada"] as $cheque){
                                if(!($cheque["id_comprobante_salida"]>0)){//quiere decir que NO salio el cheque y lo puedo usar
                                
                                     $this->Cheque->updateAll(
                                                            array('Cheque.id_comprobante_entrada' => NULL,'Cheque.id_estado_cheque'=>EnumEstadoCheque::Cartera ),
                                                            array('Cheque.id' => $cheque["id"]) );   
                                
                                   array_push($listado_cheques,$cheque["nro_cheque"]);
                            
                                }
                            }       
                            
                            
                            
                            
                            if(isset($recibo["ComprobanteValor"])){
	                            foreach($recibo["ComprobanteValor"] as $comprobante_valor){
	                            	if($comprobante_valor["monto"] == 0){//quiere decir que NO salio el cheque y lo puedo usar
	                            		
	                            		$this->Comprobante->ComprobanteValor->delete($comprobante_valor["id"]);
	                            	}
	                            }      
                            }
                        
                              
                        
                            $output_asiento_revertir = array("id_asiento"=>0);
                            if($recibo["Comprobante"]["comprobante_genera_asiento"] == 1)
                                $output_asiento_revertir = $this->Asiento->revertir($recibo["Asiento"]["id"]); 
                                
                                
                            
                            //busco los creditos internos hechos
                            $credito_interno = $this->Comprobante->ComprobanteItem->find('first',array(
                            
                            'conditions'=>array("ComprobanteItem.id_comprobante_origen"=>$id_recibo,"Comprobante.id_tipo_comprobante"=>EnumTipoComprobante::CreditoInternoVenta),
                            'contain'=>array("Comprobante")
                            
                            
                            ) );  
                            
                            //anulo los CI hechos
                            if($credito_interno)
                                 $this->Comprobante->updateAll(
                                                        array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,
															  'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
                                                        array('Comprobante.id' => $credito_interno["Comprobante"]["id"]) );    
                            
                                 
                            
                                 
                           //busco los creditos internos hechos
                           $debito_interno = $this->Comprobante->ComprobanteItem->find('first',array(
                                 		
                           		'conditions'=>array("ComprobanteItem.id_comprobante_origen"=>$id_recibo,"Comprobante.id_tipo_comprobante"=>EnumTipoComprobante::DebitoInternoVenta),
                                 		'contain'=>array("Comprobante")
                                 		
                                 		
                                 ) );
                                 
                                 //anulo los CI hechos
                           if($debito_interno)
                                $this->Comprobante->updateAll(
                                 			array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,
												  'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
                                		array('Comprobante.id' => $debito_interno["Comprobante"]["id"]) );
                                 	
                            
                            
                            /*
                            $this->Comprobante->updateAll(
                                                        array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => date("Y-m-d"),'Comprobante.definitivo' =>1 ),
                                                        array('Comprobante.id' => $id_recibo) ); 
                                                        
                                                        */
                            ///////////////////////////////////////////    
                        
                        
                         
                            $this->Comprobante->updateAll(
                                                        array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
                                                        array('Comprobante.id' => $id_recibo) ); 
                            
                            
                            
                     
                        $ds->commit(); 
                        
                        
                        
                        $recibo = $this->Comprobante->find('first', array(
                        		'conditions' => array('Comprobante.id'=>$id_recibo,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                        		'contain' => false
                        ));
                        
                        $this->CerrarComprobantesAsociados($recibo);/*leo Nuevamente el Recibo*/
                        
                        
                        $tipo = EnumError::SUCCESS;
                      
                       $mensaje = '';
                        if(count($listado_cheques)>0)
                            $mensaje = ". Se liberaron en la cartera los siguientes cheques: Cheque Nro:".implode(" Cheque Nro: ",$listado_cheques);
                        
                        $mensaje = "El Comprobante se anulo correctamente ".$mensaje;       
                      
                        
                        $this->Auditar($id_recibo,$recibo);
                        
                                                
                    }else{
                        
                        $tipo = EnumError::ERROR;
                        $mensaje = "El Comprobante NO puede anularse ya que existe un cheque que se ingreso y que fue utilizado para otra operatoria.";
                    }
              
                     
               }catch(Exception $e){ 
                
                $ds->rollback();
                $tipo = EnumError::ERROR;
                $mensaje = "No es posible anular el Comprobante ERROR:".$e->getMessage();
                
              }  
                            
            }else{
                
               $tipo = EnumError::ERROR;
               $mensaje = "No es posible anular el Comprobante ya se encuenta ANULADO";
            } 
            
        
            if(isset( $output_asiento_revertir["id_asiento"]))
            	$id_asiento_revertir =  $output_asiento_revertir["id_asiento"];
            	else
            		$id_asiento_revertir = 0;
            
        $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
        		"id_asiento" => $id_asiento_revertir,
                );      
        echo json_encode($output);
        die();
        
         
        
    }
    
    public function CalcularImpuestos($id_comprobante,$id_tipo_impuesto='',$error=0,$message=''){
    
    	
    	
    	
    	return $this->borrraComprobanteImpuestoConBaseCero($error,$message);
    	
    }
    
    
    private function ProcesaDescuento(){
        
         if(isset($this->request->data[$this->model]["descuento"]) && $this->request->data[$this->model]["descuento"]>0){
        
            $this->request->data[$this->model]["subtotal_bruto"] = $this->request->data[$this->model]["total_comprobante"]*(1/$this->request->data[$this->model]["descuento"]);
            $this->request->data[$this->model]["subtotal_neto"]  = $this->request->data[$this->model]["subtotal_bruto"];
    
        }else{
        
            $this->request->data[$this->model]["subtotal_bruto"] = $this->request->data[$this->model]["total_comprobante"];
            $this->request->data[$this->model]["subtotal_neto"]  = $this->request->data[$this->model]["total_comprobante"];
        }
        return;
    }
    
    
    protected function actualizoMontos($id_c,$nro_c,$total_iva,$total_impuestos){
               //sobreescrita
               return;
    
    }
    
    protected function borraImpuestos($id){
        
        return;
    }
    
    
    /**
    * @secured(CONSULTA_REMITO)
    */
    public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
    {
        parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
    }
    
    
    
    /**
     * @secured(BTN_EXCEL_RECIBOS)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Recibos de Cobro");
    	
    	
    	
    }
    
    
    public function CerrarComprobantesAsociados($recibo_objeto){
    	
    	/*traigo los items del recibo que son comprobantes*/
    	
    	$this->loadModel("ComprobanteItem");
    	$this->loadModel("Comprobante");
    	
    	
    	if($recibo_objeto && $recibo_objeto["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion || $recibo_objeto["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::Anulado){
    		
    		
    	/*Traigo los items de un recibo, fc, nc,nd ci,di, etc*/	
    	$recibo = $this->ComprobanteItem->find('all', array(
    			'conditions' => array('ComprobanteItem.id_comprobante' => $recibo_objeto["Comprobante"]["id"]),
    			'contain' =>array('Comprobante','ComprobanteOrigen')
    	));
    	
    		$id_moneda = $recibo_objeto["Comprobante"]["id_moneda"];
    		
    		$comprobante_obj = new Comprobante();
    		
    		foreach($recibo as $comprobante_item){
    			
    			$comprobante_origen = $comprobante_item["ComprobanteOrigen"];
    			
    			$id_comprobante = $comprobante_origen["id"];
    		
    			/*pregunto si es menor a 0,001 para evitar problemas por el redondeo*/
    				
    				switch($recibo_objeto["Comprobante"]["id_estado_comprobante"]){
    					
    					case EnumEstadoComprobante::CerradoReimpresion:
    					//debo cerrar los comprobantes	
    						$saldo = abs($comprobante_obj->getComprobantesImpagos($comprobante_origen["id_persona"],1,$id_comprobante,1,EnumSistema::VENTAS,0,$id_moneda));
    						if($saldo <= 0.01){
    							
    							$this->Comprobante->updateAll(
    									array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Cumplida ),
    									array('Comprobante.id' => $id_comprobante) );  
    						}else{
    							$this->log("ERROR DETECTADO EN RECIBOS el SALDO ES:".$saldo." , El id del comprobante es:".$id_comprobante);
    						}
    					break;
    					
    					case EnumEstadoComprobante::Anulado:
    						
    						if($comprobante_origen["cae"]>0){
    							
    							$this->Comprobante->updateAll(
    									array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::CerradoConCae ),
    									array('Comprobante.id' => $id_comprobante) );  
    						}else{
    							
    							$this->Comprobante->updateAll(
    									array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::CerradoReimpresion ),
    									array('Comprobante.id' => $id_comprobante) );
    						}
    						break;
    				}
    		}
    	}
    }
    
    
    protected function generaNotaPorDiferenciaCambio(){
    	
    	
    	if($this->request->data["Comprobate"]["permite_ajuste_diferecia_cambio"] == 1){
    		
	    	foreach($this->request->data["ComprobanteItemComprobante"] as $comprobante){
	    		
	    		
	    		$subtotal_neto = $comprobante["diferecia_cambio"];
	    		
	    		$cabecera_comprobante = $this->Comprobante->getCabeceraComprobante($comprobante["diferecia_cambio"],$diferencia,$diferencia,EnumTipoComprobante::CreditoInternoVenta,date('Y-m-d'),EnumEstadoComprobante::CerradoReimpresion,"",$this->request->data["Comprobante"]["id_moneda"],$this->request->data["Comprobante"]["valor_moneda"],$this->request->data["Comprobante"]["valor_moneda2"],$this->request->data["Comprobante"]["valor_moneda3"],$observacion,$nro_comprobante_credito_interno,$this->request->data["Comprobante"]["id_persona"],$this->Auth->user('id'),1,0,1,$this->request->data["Comprobante"]["id_punto_venta"]);
	    		
	    		
	    		$items_comprobante = $this->Comprobante->getComprobanteItem(1,$diferencia,$diferencia,1,1,EnumIva::exento,$observacion,1,$id,0);//el credito interno tiene como origen el recibo
	    		$id_credito_nota_credito = $this->Comprobante->InsertaComprobante($cabecera_comprobante,$items_comprobante);
	    		
	    		
	    		
	    		if($comprobante["diferecia_cambio"] >0){ //hago Nota de debito
	    			
	    			
	    			
	    		 }else if($comprobante["diferecia_cambio"] <0){//hago nota de credito
	    			
	    		 	
	    			
	    		}
	    		
	    	}
    }
    	
    }
    
    
    
    /* $output es una variable que controla el flujo
     *
     * $output = 0 ---> genero solo PDF
     * $output = 1 ----> devuelvo PDF como respuesta
     * $output = 3 ----> devuelvo PDF y genero el archivo
     *
     *
     * */
    protected function getPDF($datos_pdf,$datos_empresa,$output=0){
    	
    	
   	App::import('Vendor','tcpdf/mytcpdf');
    	
    	
    $this->loadModel("Comprobante");
    	
    	
    	
    	
    	
    	
    	$html = $this->Comprobante->getHeaderPDF($datos_pdf,$datos_empresa,'RECIBO');
    	
    	
    	$pdf = new MyTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    	
    	$pdf->set_datos_header($html);
    	
    	$descuento = (($datos_pdf["Comprobante"]["total_comprobante"]*$datos_pdf["Comprobante"]["descuento"])/100) ;
    	
    	
    	
    	
    	$impuestos = '';
    	$suma_impuestos = 0;
    	if(isset($datos_pdf["ComprobanteImpuesto"])){
    		
    		foreach($datos_pdf["ComprobanteImpuesto"] as $impuesto){
    			
    			$impuestos .= ' <tr>
                 <td>'.$impuesto["Impuesto"]["d_impuesto"].'</td>
                 <td>'.money_format('%!n',$impuesto["Impuesto"]["base_imponible"]).'</td>
                 <td>'.money_format('%!n',$impuesto["tasa_impuesto"]).'%</td>
                <td>'.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n',$impuesto["importe_impuesto"]).'</td>
               </tr>';
    			
    			$suma_impuestos = $suma_impuestos + $impuesto["importe_impuesto"];
    			
    		}
    	}
    	
    	
    	$total_comprobante = $datos_pdf["Comprobante"]["total_comprobante"]  + $diferencia;
    	
    	App::import('Vendor', 'NumberToLetterConverter', array('file' => 'Classes/NumberToLetterConverter.class.php'));
    	$numero_conversion = new NumberToLetterConverter();
    	$letras_total =  "Importe en letras: ".$numero_conversion->to_word(str_replace(".",",",$total_comprobante),$datos_pdf["Moneda"]["simbolo_internacional"]);
    	
    	
    	
    	
    	
    	
    	$footer='
  <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">
    			
    			
     <tbody>
        <tr >
        <td colspan="2">
    			
        </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody>
</table>
    			
    			
<table width="100%" border="1" style="width:100%;">
   <tr>
     <td width="60%">Observaciones: '.$datos_pdf["Comprobante"]["observacion"].' '.$letras_total.'</td>
     <td width="40%">
     <table width="100%" border="0">
     		
     		
       <tr>
         <td><strong>TOTAL</strong></td>
         <td>&nbsp;</td>
         <td><strong>'.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n',$total_comprobante).'</strong></td>
       </tr>
     </table></td>
   </tr>';
    	
    	
    	
    	
    	$footer .= '<tr>
     <td > Original blanco / copia color</td>
    			
   </tr>';
    	
    	
    	
    	
    	
    	
    	
    	$footer .='</table>';
    	
    	
    	
    	$html .= "<HR>";
    	
    	
    	
    	
    	$html .= '
    			
   <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">
    			
    			
     <tbody>
        <tr >
        <td colspan="2">
    			
        </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody>
</table>
    			
  ';
    	
    	$pdf->set_datos_footer($footer);
    	
    	
    	ob_clean();
    	
    	
    	$pdf->SetCreator(PDF_CREATOR);
    	$pdf->SetAuthor('Sivit by Valuarit');
    	$pdf->SetTitle('Factura');
    	$pdf->SetSubject('Factura');
    	
    	
    	
    	
    	
    	$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));
    	
    	
    	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    	
    	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    	
    	
    	$pdf->SetMargins(PDF_MARGIN_LEFT, 50, 20);
    	$pdf->SetHeaderMargin(10);
    	$pdf->SetFooterMargin(65);
    	
    	
    	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    	
    	
    	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    	
    	$pdf->SetFont('helvetica', '', 12, '', true);
    	
    	$pdf->AddPage();
    	
    	
    	$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
    	
    	
    	$tbody = "";
    	
    	$renglones = $datos_pdf["ComprobanteItemComprobante"];
    	
    	$data = array();
    	
    	App::uses('CakeSession', 'Model/Datasource');
    	$datoEmpresa = CakeSession::read('Empresa');
    	
    	$suma_diferencia_cambio = 0;
    	$suma_comprobantes = 0;
    	
    	
    	foreach($renglones as $renglon){
    		
    		$fecha_vencimiento = new DateTime($renglon['ComprobanteOrigen']['fecha_vencimiento']);
    		
    		
    		
    		if($renglon['ComprobanteOrigen']['id_moneda_enlazada'] == $datoEmpresa["DatoEmpresa"]["id_moneda"])
    			$cotizacion = $renglon['ComprobanteOrigen']['valor_moneda'];
    			elseif ($renglon['ComprobanteOrigen']['id_moneda_enlazada'] == $datoEmpresa["DatoEmpresa"]["id_moneda2"])
    			$cotizacion = 1/$renglon['ComprobanteOrigen']['valor_moneda2'];
    			elseif ($renglon['ComprobanteOrigen']['id_moneda_enlazada'] == $datoEmpresa["DatoEmpresa"]["id_moneda3"])
    			$cotizacion = 1/$renglon['ComprobanteOrigen']['valor_moneda3'];
    			
    			
    			
    			$tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
    			$tbody .= "<td>" .$renglon['ComprobanteOrigen']['TipoComprobante']['codigo_tipo_comprobante'] . "</td>";
    			
    			if(isset($renglon['ComprobanteOrigen']['id_punto_venta']))
    				$tbody .= "<td>" .$this->Comprobante->GetNumberComprobante($renglon['ComprobanteOrigen']['PuntoVenta']['numero'],$renglon['ComprobanteOrigen']['nro_comprobante']) . "</td>";
    				else
    					$tbody .= "<td>" .$this->Comprobante->GetNumberComprobante($renglon['ComprobanteOrigen']['d_punto_venta'],$renglon['ComprobanteOrigen']['nro_comprobante']) . "</td>";
    					
    					$tbody .= "<td>" .$fecha_vencimiento->format('d-m-Y')."</td>";
    					
    					if($renglon['ComprobanteOrigen']["id_moneda"]!= EnumMoneda::Peso)
    					$tbody .= "<td>" .$renglon['ComprobanteOrigen']["Moneda"]["simbolo_internacional"]." ".money_format('%!n',round($renglon['precio_unitario']/$cotizacion,4))."| ARS ".money_format('%!n',$renglon['precio_unitario'])."</td>";
    						else
    							$tbody .= "<td>" .$renglon['ComprobanteOrigen']["Moneda"]["simbolo_internacional"]." ".money_format('%!n',$renglon['precio_unitario'])."</td>";
    							
    							if($renglon['ComprobanteOrigen']['enlazar_con_moneda'] == 1){
    								
    								$tbody .= "<td>Si ".$renglon['ComprobanteOrigen']['MonedaEnlazada']["simbolo_internacional"]."</td>";
    								
    								$tbody .= "<td>".money_format('%!n',round($cotizacion,4))."</td>";
    								
    								if($renglon['monto_diferencia_cambio'] !=0){
    									if($renglon['monto_diferencia_cambio'] > 0)
    										$tbody .= "<td>ND por el valor de " .money_format('%!n',$renglon['monto_diferencia_cambio'])."</td>";
    										else
    											$tbody .= "<td>NC por el valor de " .money_format('%!n',$renglon['monto_diferencia_cambio'])."</td>";
    											
    											$suma_diferencia_cambio += $renglon['monto_diferencia_cambio'];
    											
    								}
    							}else{
    								
    								$tbody .= "<td>No</td>";
    								$tbody .= "<td></td>";
    								$tbody .= "<td></td>";
    							}
    							
    							$suma_comprobantes  += $renglon['precio_unitario']*$renglon['ComprobanteOrigen']['TipoComprobante']['signo_comercial'];
    							
    							
    							
    							$tbody .= "</tr>";
    							
    							
    							if($renglon["importe_descuento_unitario"]>0){//agrego una fila para mostrarle que le hice un descuento
    								
    								
    								
    								$tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
    								$tbody .= "<td>Descuento</td>";
    								$tbody .= "<td></td>";
    								$tbody .= "<td></td>";
    								$tbody .= "<td>" .$renglon['importe_descuento_unitario']."</td>";
    								$tbody .= "<td></td>";
    								$tbody .= "<td></td>";
    								$tbody .= "<td></td>";
    								$tbody .= "</tr>";
    								
    								$suma_comprobantes  = $suma_comprobantes - $renglon['importe_descuento_unitario'];
    								
    							}
    	}
    	
    	
    	
    	
    	
    	$tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
    	$tbody .= "<td><strong>Total</strong></td>";
    	$tbody .= "<td></td>";
    	$tbody .= "<td></td>";
    	$tbody .= "<td><strong>".$datos_pdf["Moneda"]["simbolo_internacional"]." ".money_format('%!n',$suma_comprobantes)."</strong></td>";
    	$tbody .= "<td></td>";
    	$tbody .= "<td></td>";
    	$tbody .= "<td></td>";
    	$tbody .= "</tr>";
    	
    	
    	
    	$renglones_credito_interno = $credito_interno_relacionados;
    	
    	
    	$diferencia = 0;
    	foreach($renglones_credito_interno as $renglon){
    		
    		
    		
    		$tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
    		$tbody .= "<td>" .$renglon['Comprobante']['codigo_tipo_comprobante'] . " (Generado)</td>";
    		$tbody .= "<td>" .$this->Comprobante->GetNumberComprobante($renglon['PuntoVenta']['numero'],$renglon['Comprobante']['nro_comprobante']) . "</td>";
    		$tbody .= "<td>" .$this->Comprobante->formatDate($renglon["Comprobante"]["fecha_vencimiento"])."</td>";
    		$tbody .= "<td>" .money_format('%!n',$renglon['Comprobante']["total_comprobante"])."</td>";
    		$diferencia += $renglon['Comprobante']["total_comprobante"];
    		$tbody .= "</tr>";
    	}
    	
    	
    	
    	$tbody2 = '';
    	
    	$renglones2 = $datos_pdf["ComprobanteValor"];
    	
    	$suma_valores = 0;
    	
    	foreach($renglones2 as $renglon){
    		
    		
    		
    		switch($renglon['Valor']['id']) {
    			
    			case EnumValor::TRANSFERENCIAS:
    				$tbody2 .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
    				$tbody2 .= "<td>" .$renglon['Valor']['d_valor'] . "</td>";
    				$tbody2 .= "<td>" .$renglon['Valor']['id'] . "</td>";
    				$tbody2 .= "<td>" .$renglon['CuentaBancaria']['nro_cuenta']. "</td>";
    				$tbody2 .= "<td>" .money_format('%!n',round($renglon['monto'],2))."</td>";
    				$tbody2 .= "</tr>";
    				
    				break;
    				
    			case EnumValor::EFECTIVO:
    				
    				$tbody2 .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
    				$tbody2 .= "<td>" .$renglon['Valor']['d_valor']. "</td>";
    				$tbody2 .= "<td></td>";
    				$tbody2 .= "<td>" .$renglon['d_comprobante_valor']. "</td>";
    				$tbody2 .= "<td>" .money_format('%!n',round($renglon['monto'],2))."</td>";
    				$tbody2 .= "</tr>";
    				break;
    				
    			case EnumValor::COMPENSACION:
    				
    				$tbody2 .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
    				$tbody2 .= "<td>" .$renglon['Valor']['d_valor']. "</td>";
    				$tbody2 .= "<td></td>";
    				$tbody2 .= "<td>" .$renglon['d_comprobante_valor']. "</td>";
    				$tbody2 .= "<td>" .money_format('%!n',round($renglon['monto'],2))."</td>";
    				$tbody2 .= "</tr>";
    				break;
    				
    		}
    		
    		$suma_valores += round($renglon['monto'],2);
    		
    	}
    	
    	
    	$tbody3 = '';
    	
    	$renglones3 = $datos_pdf["ChequeEntrada"];
    	
    	foreach($renglones3 as $renglon){
    		
    		
    		
    		
    		$tbody3 .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
    		$tbody3 .= "<td>" .$renglon['id'] . "</td>";
    		$tbody3 .= "<td>" .$renglon['nro_cheque'] . "</td>";
    		$tbody3 .= "<td>" .$renglon['Banco']['d_banco']. "</td>";
    		$tbody3 .= "<td>" .$this->Comprobante->formatDate($renglon["fecha_cheque"]). "</td>";
    		$tbody2 .= "<td>" .money_format('%!n',round($renglon['monto'],2))."</td>";
    		$tbody3 .= "</tr>";
    		
    		$suma_valores += round($renglon['monto'],2);
    	}
    	
    	//agrego las observaciones
    	
    	
    	
    	
    	
    	$html='';
    	
    	
    	$html .='
<table width="100%"   style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
  <tr >
                    <td colspan="4"></td>
                  </tr>
  <tr>
  <tr >
                    <td colspan="4"></td>
                  </tr>
  <tr>
    			
    			
 </table>
    			
    			
    			
    			
                    <table width="100%" border="1"  style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
                  <tr  style="background-color:rgb(224, 225, 229);">
                    <td colspan="4"><div align="center"><strong>Cliente</strong></div></td>
                  </tr>
                  <tr>
                    <td width="17%"><strong>Codigo:</strong> '.$datos_pdf["Persona"]["codigo"].'</td>
                    <td width="35%"><strong>Raz&oacute;n Social:</strong> '.$datos_pdf["Persona"]["razon_social"].'</td>
                    <td width="26%"><strong>Cuit:</strong> '.$datos_pdf["Persona"]["cuit"].' </td>';
    	
    	
    	$html.='<td width="22%"></td>';
    	
    	
    	
    	$html.='</tr>
                              <tr>
                                <td colspan="3"><strong>Direccion:</strong> '.$datos_pdf["Persona"]["calle"].' - '.$datos_pdf["Persona"]["numero_calle"].' - '.$datos_pdf["Persona"]["localidad"].' - '.$datos_pdf["Persona"]["ciudad"].' - '.$datos_pdf["Persona"]["Provincia"]["d_provincia"].' - '.$datos_pdf["Persona"]["Pais"]["d_pais"].'  </td>
                                <td><strong>Teléfono:</strong> '.$datos_pdf["Persona"]["tel"].'</td>
                              </tr>
                                		
                                		
                                 </table>
                                		
                                		
                                		
                                		
                                		
<table width="100%" border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;font-family:\'Courier New\', Courier, monospace">
                                		
                                		
     <tbody>
                                		
        <tr >
        <td colspan="2"> </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
                                		
        <tr >
        <td  colspan="6" ><HR></td>
                                		
         </tr>
     </tbody>
</table>
                                		
                                		
<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">
    <tr>
    <td class="sorting"  rowspan="1" colspan="7" style="width:100%;text-decoration:underline;text-align:left"><strong>Comprobantes imputados</strong></td>
    </tr>
    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th style="width:10%;"><strong>Tipo Comp.</strong></th>
        <th style="width:20%;"><strong>Nro. Comp.</strong></th>
        <th style="width:15%;"> <strong>Fcha Vto</strong></th>
        <th style="width:15%;"><strong>Importe</strong></th>
        <th style="width:10%;"><strong>Enlazado Tipo Cambio</strong></th>
        <th style="width:15%;"><strong>Cotizaci&oacute;n</strong></th>
        <th style="width:15%;"><strong>NC/ND</strong></th>
    </tr>
                                		
  <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
    '.$tbody.'
  </tbody>
    		
    		
    		
    		
</table>
    		
    		
    		
<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">
      <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
     <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
    <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;text-decoration:underline;"><strong>Efec./Transf/Compens..</strong></td>
    </tr>
    		
    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong>Valor</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>Id</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:25%;"><strong>Desc.</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:50%;"><strong>Total.</strong></th>
    		
    </tr>
    		
  <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
    		
    '.$tbody2.'
  </tbody>
    		
    		
</table>';
    	
    	
    	
    	if(strlen($tbody3)>0){
    		
    		$html.='<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">
      <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
     <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
    <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;text-decoration:underline;"><strong>Cheques</strong></td>
    </tr>
    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>Id.</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:25%;"><strong>Nro.Cheque</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:25%;"><strong>Banco</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:15%;"><strong>F. Vencimiento</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:30%;"><strong>Monto</strong></th>
    				
    </tr>.';
    		
    		
    		
    		$html.='<tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
        '.$tbody3.'
      </tbody></table>';
    		
    		
    		
    		
    		
    		
    	}
    	
    	$html.='
     <table width="100%"  cellspacing="1" cellpadding="2" style="font-size:10px;font-family:\'Courier New\', Courier, monospace">
 <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
       <tr>
        <td class="sorting"  rowspan="1" colspan="1" style="width:50%;text-align:left;"><strong>Total</strong></td>
        <td class="sorting"  rowspan="1" colspan="1" style="width:50%;text-align:left;"><strong>'.money_format('%!n',$suma_valores).'</strong></td>
       </tr>
      </tbody></table>';
    	
    	
    	if($datos_pdf["Comprobante"]["descuento"]>0){
    		
    		//total_comprobante - subtotal_bruto
    		
    		$html.='<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">
    				
     <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
    <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;text-decoration:underline;"><strong>Descuento</strong></td>
    </tr>
    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:50%;"><strong>Descuento (%)</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:50%;"><strong>Monto</strong></th>
    				
    				
    </tr>';
    		
    		$html.='<tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
      <tr>
        <td>
        '.round($datos_pdf["Comprobante"]["descuento"],2).'%
        </td>
        		
         <td>
        '.round($datos_pdf["Comprobante"]["importe_descuento"],2).'
        </td>
        		
      </tr>
      </tbody></table>';
    		
    	}
    	
    	
    	if(strlen($impuestos)>0){
    		
    		$html.='<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
      <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
     <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
    <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;text-decoration:underline;"><strong>Retenciones</strong></td>
    </tr>
    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:40%;"><strong>Desc</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong>Base Imp.</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong>Tasa</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong>Monto</strong></th>
    				
    </tr>.';
    		
    		
    		
    		$html.='<tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
        '.$impuestos.'
      </tbody></table>';
    		
    		$html.='
     <table width="100%"  cellspacing="1" cellpadding="2" style="font-size:10px;font-family:\'Courier New\', Courier, monospace">
 <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
       <tr>
        <td class="sorting"  rowspan="1" colspan="1" style="width:50%;text-align:left;"><strong>Total</strong></td>
        <td class="sorting"  rowspan="1" colspan="1" style="width:50%;text-align:left;"><strong>'.$suma_impuestos.'</strong></td>
       </tr>
      </tbody></table>';
    		
    	}
    	
    	$pdf->writeHTML($html, true, false, true, false, true);
    	
    	
    	
    	
    	ob_clean();
    	
    	
    	
    	$pdf_name = $this->Comprobante->getNamePDF($datos_pdf);
    	$root_pdf = $this->Comprobante->getRootPDF();
    	
    	if($datos_pdf["TipoComprobante"]["adjunta_pdf_mail"] == 1 && ($output == 0 || $output == 3)) {
    		
    		$path_para_pdf = $datos_empresa["DatoEmpresa"]["local_path"].$root_pdf;
    		
    		$pdf->Output($path_para_pdf.$pdf_name.'.pdf', 'F');//Esta linea guarda un archivo en path_para_pdf
    	}
    	
    	
    	
    	if($output == 1 || $output == 3)
    		$pdf->Output($pdf_name.'.pdf', 'D');
    		
    
    	
    }
    
   
    
    
    
}
?>