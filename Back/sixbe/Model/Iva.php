<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class Iva extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'iva';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 public function getDetallePorAlicuotaAfip($alicuota_afip){
    
    
    $comprobante = $this->find('first', array(
                                                'conditions' => array('Iva.tipo_alicuota_afip' => $alicuota_afip),
                                                'contain' =>false
                                                )); 
    return $comprobante["Iva"]["d_iva_detalle"];    
    
    
} 


public function getTasaIvaPorAlicuotaAfip($alicuota_afip){
	
	
	$comprobante = $this->find('first', array(
			'conditions' => array('Iva.tipo_alicuota_afip' => $alicuota_afip),
			'contain' =>false
	));
	return $comprobante["Iva"]["d_iva"];
	
	
} 
	

}
