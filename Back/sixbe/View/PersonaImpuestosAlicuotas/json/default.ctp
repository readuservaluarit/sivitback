<?php 
     App::uses('PersonaImpuestoAlicuota', 'Model');  
     $pia = new PersonaImpuestoAlicuota();
     $pia->setVirtualFieldsForView();
     
      
     /*seteo los virtual fields*/
     foreach($pia->virtual_fields_view as $key_virtual_fields=>$array_configuracion){
        foreach($array_configuracion as $key_propiedad_field =>$array_field){ 
            $model[$key_virtual_fields][$key_propiedad_field] = $array_field;   
        }   
     }
		$model["id"]["show_grid"] = 1;
        $model["id"]["header_display"] = "Cod.";
        $model["id"]["order"] = "0";
        $model["id"]["width"] = "5";
        
        $model["d_impuesto"]["show_grid"] = 1;
        $model["d_impuesto"]["header_display"] = "Impuesto";
        $model["d_impuesto"]["order"] = "1";
        $model["d_impuesto"]["width"] = "30";
		$model["d_impuesto"]["text_colour"] = "#0000FF";

        $model["porcentaje_alicuota"]["show_grid"] = 1;
        $model["porcentaje_alicuota"]["type"] = EnumTipoDato::decimal_flotante;
        $model["porcentaje_alicuota"]["header_display"] = "Alicuota";
        $model["porcentaje_alicuota"]["order"] = "2";
        $model["porcentaje_alicuota"]["width"] = "8";
		$model["porcentaje_alicuota"]["text_colour"] = "#660077";
        
        $model["d_sistema"]["show_grid"] = 1;
        $model["d_sistema"]["header_display"] = "Usado para";
        $model["d_sistema"]["order"] = "3";
        $model["d_sistema"]["width"] = "8";
        
	  //Agregar la razon social de la persona
	  // $propiedades = array("show_grid"=>"1","header_display" => "Raz&oacute;n Social","minimun_width"=> "10","order" =>"1","text_colour"=>"#000000" );
		
		$model["fecha_vigencia_desde"]["show_grid"] = 1;
        $model["fecha_vigencia_desde"]["header_display"] = "Vigencia Desde";
        $model["fecha_vigencia_desde"]["order"] = "4";
        $model["fecha_vigencia_desde"]["width"] = "15";
		
		$model["fecha_vigencia_hasta"]["show_grid"] = 1;
        $model["fecha_vigencia_hasta"]["header_display"] = "Vigencia Hasta";
        $model["fecha_vigencia_hasta"]["order"] = "5";
        $model["fecha_vigencia_hasta"]["width"] = "15";
        
        
        $model["d_sin_vencimiento"]["show_grid"] = 1;
        $model["d_sin_vencimiento"]["header_display"] = "Sin Vencimiento?";
        $model["d_sin_vencimiento"]["order"] = "6";
        $model["d_sin_vencimiento"]["width"] = "15";
        
        
        $model["d_activo"]["show_grid"] = 1;
        $model["d_activo"]["header_display"] = "Activo?";
        $model["d_activo"]["order"] = "7";
        $model["d_activo"]["width"] = "15";

 $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "PersonaImpuestoAlicuota",
                    "content" => $model
                );
        echo json_encode($output);
         
        

?>