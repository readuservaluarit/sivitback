SELECT 
  `Provincia`.`id`,
  `Provincia`.`d_provincia`,
 REPLACE( SUM(
    (
      subtotal_neto * TipoComprobante.signo_comercial
    ) / Comprobante.valor_moneda2
  ),".",",") AS monto
FROM
  `comprobante` AS `Comprobante` 
  LEFT JOIN `tipo_comprobante` AS `TipoComprobante` 
    ON (
      `Comprobante`.`id_tipo_comprobante` = `TipoComprobante`.`id`
    ) 
  LEFT JOIN `persona` AS `Persona` 
    ON (
      `Comprobante`.`id_persona` = `Persona`.`id`
    ) 
  LEFT JOIN `provincia` AS `Provincia` 
    ON (
      `Persona`.`id_provincia` = `Provincia`.`id`
    ) 
WHERE `Comprobante`.`activo` = 1 
  AND `Comprobante`.`fecha_contable` >= '2017-01-01' 
  AND `Comprobante`.`fecha_contable` <= '2017-12-31' 



  AND `Comprobante`.`id_tipo_comprobante` IN (
    801,
    806,
    811,
    817,
    833,
    804,
    809,
    815,
    836,
    803,
    808,
    813,
    835,
    818,
    802,
    807,
    812,
    834,
    819
  ) 
  AND `Comprobante`.`id_estado_comprobante` IN (115, 9) 
  AND `Persona`.`id_pais` = 302  
  AND 
  IFNULL(Comprobante.`id_detalle_tipo_comprobante`,0)!=5 /*Esto lo tiene porque no reconoce bien el NULL*/
GROUP BY `Provincia`.`id` 
ORDER BY `Provincia`.`d_provincia` ASC 