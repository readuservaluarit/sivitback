INSINSERT INTO orden_produccion (
  id,
  id_componente,
  cantidad,
  fecha_emision,
  observacion,
  id_estado
) 
SELECT 
  siv_op.id,
  producto.id,
  cantidad,
  NOW(),
  observaciones,
  1 
FROM
  siv_op 
  JOIN producto 
    ON producto.`codigo` = siv_op.`id_componente` 
WHERE id_producto_tipo = 2 ;