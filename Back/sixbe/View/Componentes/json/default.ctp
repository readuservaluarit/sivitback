<?php


        include(APP.'View'.DS.'Articulo'.DS.'json'.DS.'articulo.ctp');
          // CONFIG  CON MODE DISPLAYED CELLS
     
     
     
      	App::import('Model','Comprobante');
  		App::import('Lib','SecurityManager');
		$comprobante = new Comprobante();
		$stock = $comprobante->getActivoModulo(EnumModulo::STOCK);
		
		
		 $sm = new SecurityManager(); 
  
         
         
		$talle = $sm->permiso_particular("CONSULTA_TALLE",AuthComponent::user('username'))>0;
		$color = $sm->permiso_particular("CONSULTA_COLOR",AuthComponent::user('username'))>0;
     
     
     
        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "C&oacute;digo";
        $model["codigo"]["width"] = "12";
        $model["codigo"]["order"] = "0";
        $model["codigo"]["text_colour"] = "#000000";
        

        $model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "Descripci&oacute;n";
        $model["d_producto"]["width"] = "35";     
        $model["d_producto"]["order"] = "3";
        $model["d_producto"]["text_colour"] = "#0000FF";
        
        
        $model["d_unidad"]["show_grid"] = 1;
        $model["d_unidad"]["header_display"] = "Unidad";
        $model["d_unidad"]["width"] = "10";
        $model["d_unidad"]["order"] = "4";
        $model["d_unidad"]["text_colour"] = "#0000FF";
        
        if($stock == 1){
        
        	$model["stock"]["show_grid"] = 1;
        	$model["stock"]["header_display"] = "Stock";
        	$model["stock"]["width"] = "10";
        	$model["stock"]["order"] = "11";
        	$model["stock"]["text_colour"] = "#0000FF";
        }
        
        
        if($talle){
        
        	$model["d_talle"]["show_grid"] = 1;
        	$model["d_talle"]["header_display"] = "Talle";
        	$model["d_talle"]["width"] = "10";
        	$model["d_talle"]["order"] = "12";
        	$model["d_talle"]["text_colour"] = "#0000FF";
        }
        
        
        if($color){
        
        	$model["d_color"]["show_grid"] = 1;
        	$model["d_color"]["header_display"] = "Color";
        	$model["d_color"]["width"] = "10";
        	$model["d_color"]["order"] = "13";
        	$model["d_color"]["text_colour"] = "#0000FF";
        }
        
        
        $model["costo"]["show_grid"] = 1;
        $model["costo"]["header_display"] = "Costo";
        $model["costo"]["width"] = "5";
        $model["costo"]["order"] = "14";
        $model["costo"]["text_colour"] = "#0000FF";
        
        
        $model["peso"]["show_grid"] = 1;
        $model["peso"]["header_display"] = "Peso";
        $model["peso"]["width"] = "5";
        $model["peso"]["order"] = "15";
        $model["peso"]["text_colour"] = "#0000FF";
        
        
        
        
         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Componente",
                    "content" => $model
                );
        
          echo json_encode($output);
?>