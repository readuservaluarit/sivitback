<?php

App::uses('AppController', 'Controller');

class StockTracerController extends AppController {
    
    public $components = array('RequestHandler');
    
    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(array('obtenerAutenticacion','obtenerGrupoTransaccion','validarToken','informarMovimiento','informarComposicion','informarTransferencia'));
        
        
        $this->loadModel('WsAutenticacion');
        $this->loadModel('Stock');
        $this->loadModel('ElementoSerie');
        $this->loadModel('ComposicionSerie');
        $this->loadModel('Agente');
        $this->loadModel('PosicionDeposito');
        $this->loadModel('TransaccionStock');
        $this->transaction = $this->Stock->getDataSource();
    }

    /*
        obtenerAutenticacion(usuario, password)
        : auntenticacionResponse {token: xxxxx, errores: [error]}
    */
    public function obtenerAutenticacion() 
    {
        $this->request->data['Usuario']['username'] = $this->request->query['usuario'];
        $this->request->data['Usuario']['password'] = $this->request->query['password'];
        
        $user = $this->Auth->identify($this->request, $this->response);
        if ($user === false) {
            $this->returnToken('', array('Usuario invalido'));
            return;
        }
        
        // Generate Token
        $date = new DateTime('now');
        $token = Security::hash($user['id'].$user['username'].$date->format('Y-m-d H:i:s'));
        
        $this->WsAutenticacion->save(array(
            'TOKEN'         => $token, 
            'USUARIO'       => $user['username'], 
            'ID_USUARIO'    => $user['id'], 
            'FECHA_HORA'    => $date->format('Y-m-d H:i:s'),
            'IP_ORIGEN'     => env('REMOTE_ADDR')
        ));
        
        $this->returnToken($token);
    }


    /*
        obtenerGrupoTransaccion(token)
        : obtenerGrupoTransaccionResponse {id_transaccion: xxxxx, errores: [error]}
    */
    public function obtenerGrupoTransaccion() 
    {
        $token = $this->request->query['token'];
        
        $token = $this->_validateToken($token);
        if ($token === false) {
            $this->returnTransaccion('', array('Token invalido'));
            return;
        }
        
        // Generate ID
		$data = $this->WsAutenticacion->query("SELECT fn_upd_sequence('grupo_transaccion', 1) as grupo_transaccion");
		$grupo_transaccion = $data[0][0]['grupo_transaccion']*1;
        
        $this->returnTransaccion($grupo_transaccion, array());
        return;
    }
    
    /*
        informarMovimiento(id_elemento_presentacion, serie, f_vto, lote, fecha_hora_evento, id_evento, 
            cantidad, id_agente_origen, id_agente_destino, deposito, posición, composicion)
        : informeMovimientoResponse {id_transaccion: xxxxx, errores: [error]}
    */
    public function informarMovimiento(/*$token, $id_elemento_presentacion, $serie, $f_vto, $lote, $fecha_hora_evento, $id_evento, 
        $cantidad, $id_agente_origen, $id_agente_destino, $deposito, $posicion, $composicion*/)
    {
        $token = $this->request->query['token'];
        $id_elemento_presentacion = $this->request->query['id_elemento_presentacion'];
        $serie = isset($this->request->query['serie']) ? $this->request->query['serie'] : null;
        $f_vto = $this->request->query['f_vto'];
        $lote = $this->request->query['lote'];
        $fecha_hora_evento = $this->request->query['fecha_hora_evento'];
        $id_evento = $this->request->query['id_evento'];
        $cantidad = $this->request->query['cantidad'];
        $id_agente_origen = isset($this->request->query['id_agente_origen']) ? $this->request->query['id_agente_origen'] : null;
        $id_agente_destino = isset($this->request->query['id_agente_destino']) ? $this->request->query['id_agente_destino'] : null;
        $deposito = $this->request->query['deposito'];
        $posicion = $this->request->query['posicion'];
        //$composicion = isset($this->request->query['composicion']) ? $this->request->query['composicion'] : null;
        $id_grupo_transaccion = $this->request->query['id_grupo_transaccion'];
        
        $token = $this->_validateToken($token);
        if ($token === false) {
            $this->returnTransaccion('', array('Token invalido'));
            return;
        }
        
        try {
            $this->transaction->begin();
            
            // validar parametros
            
            // informo el movimiento
            $this->_informarMovimiento($id_elemento_presentacion, $serie, $f_vto, $lote, $fecha_hora_evento, $id_evento, $cantidad, $id_agente_origen, $id_agente_destino, $deposito, $posicion, false);
            
            // registrar transaccion
            $transaccion = $this->_registrarTransaccion($token['WsAutenticacion']['ID_AUTENTICACION'], $id_elemento_presentacion, $serie, $f_vto, $lote, $fecha_hora_evento, $id_evento, $cantidad, $id_agente_origen, $id_agente_destino, $posicion, $id_grupo_transaccion);
            
            $this->transaction->commit();
            $this->returnTransaccion($transaccion);
        } catch(Exception $ex) {
            $this->transaction->rollback();
            
            $this->returnTransaccion('', array($ex->getMessage()));
            return;
        }
    }
    
    /*
        informarComposicion(id_elemento_presentacion_padre, id_elemento_presentacion_hijo, serie, f_vto, lote, 
            fecha_hora_evento, id_evento, cantidad, id_agente_origen, id_agente_destino, 
            deposito, posición, composicion)
        : informeComposicionResponse {id_transaccion: xxxxx, errores: [error]}
    */
    public function informarComposicion(/*$token, $id_elemento_presentacion_padre, $id_elemento_presentacion_hijo, $serie, $f_vto, $lote,
        $fecha_hora_evento, $id_evento, $cantidad, $id_agente_origen, $id_agente_destino, $deposito, $posicion, $composicion*/)
    {
        $token = $this->request->query['token'];
        $id_elemento_presentacion_padre = $this->request->query['id_elemento_presentacion_padre'];
        $serie_padre = isset($this->request->query['serie_padre']) ? $this->request->query['serie_padre'] : null;
        $f_vto_padre = $this->request->query['f_vto_padre'];
        $lote_padre = $this->request->query['lote_padre'];
        $id_elemento_presentacion_hijo = $this->request->query['id_elemento_presentacion_hijo'];
        $serie_hijo = isset($this->request->query['serie_hijo']) ? $this->request->query['serie_hijo'] : null;
        $f_vto_hijo = $this->request->query['f_vto_hijo'];
        $lote_hijo = $this->request->query['lote_hijo'];
        $fecha_hora_evento = $this->request->query['fecha_hora_evento'];
        $id_evento = $this->request->query['id_evento'];
        $cantidad = $this->request->query['cantidad'];
        $id_agente_origen = isset($this->request->query['id_agente_origen']) ? $this->request->query['id_agente_origen'] : null;
        $id_agente_destino = isset($this->request->query['id_agente_destino']) ? $this->request->query['id_agente_destino'] : null;
        $deposito = $this->request->query['deposito'];
        $posicion = $this->request->query['posicion'];
        //$composicion = isset($this->request->query['composicion']) ? $this->request->query['composicion'] : null;
        $id_grupo_transaccion = $this->request->query['id_grupo_transaccion'];
        
        $token = $this->_validateToken($token);
        if ($token === false) {
            $this->returnTransaccion('', array('Token invalido'));
            return;
        }
        
        try {
            $this->transaction->begin();
            
            // validar parametros
            
            // llamar a informarMovimiento con los parametros del elemento hijo
            $this->_informarMovimiento($id_elemento_presentacion_hijo, $serie_hijo, $f_vto_hijo, $lote_hijo, $fecha_hora_evento, $id_evento, $cantidad, $id_agente_origen, $id_agente_destino, $deposito, $posicion, true);
            
            // guardar en composicion_serie los datos de la composición
            $this->_informarComposicion($id_elemento_presentacion_padre, $serie_padre, $f_vto_padre, $lote_padre, $id_elemento_presentacion_hijo, $serie_hijo, $f_vto_hijo, $lote_hijo, $cantidad);
            
            // registrar transaccion
            $transaccion = $this->_registrarTransaccion($token['WsAutenticacion']['ID_AUTENTICACION'], $id_elemento_presentacion_hijo, $serie_hijo, $f_vto_hijo, $lote_hijo, $fecha_hora_evento, $id_evento, $cantidad, $id_agente_origen, $id_agente_destino, $posicion, $id_grupo_transaccion);
            
            $this->transaction->commit();
            $this->returnTransaccion($transaccion);
        } catch(Exception $ex) {
            $this->transaction->rollback();
            
            $this->returnTransaccion('', array($ex->getMessage()));
            return;
        }
    }
    
    /*
        informarTransferencia(id_elemento_presentacion, serie, f_vto, lote, posición_origen, posición_destino)
        : informeTransferenciaResponse {id_transaccion: xxxxx, errores: [error]}
    */
    public function informarTransferencia(/*$token, $id_elemento_presentacion, $serie, $f_vto, $lote, $fecha_hora_evento, 
        $id_evento, $cantidad, $id_agente_origen, $id_agente_destino, $posicion_origen, $posicion_destino*/)
    {
        $token = $this->request->query['token'];
        $id_elemento_presentacion = $this->request->query['id_elemento_presentacion'];
        $serie = isset($this->request->query['serie']) ? $this->request->query['serie'] : null;
        $f_vto = $this->request->query['f_vto'];
        $lote = $this->request->query['lote'];
        $fecha_hora_evento = $this->request->query['fecha_hora_evento'];
        $id_evento = $this->request->query['id_evento'];
        $cantidad = $this->request->query['cantidad'];
        $id_agente_origen = $this->request->query['id_agente_origen'];
        $id_agente_destino = $this->request->query['id_agente_destino'];
        $posicion_origen = $this->request->query['posicion_origen'];
        $posicion_destino = $this->request->query['posicion_destino'];
        $id_grupo_transaccion = $this->request->query['id_grupo_transaccion'];
        
        $token = $this->_validateToken($token);
        if ($token === false) {
            $this->returnTransaccion('', array('Token invalido'));
            return;
        }
        
        try {
            $this->transaction->begin();
            
            // validar parametros
            
            $this->_informarTransferencia($id_elemento_presentacion, $serie, $f_vto, $lote, $fecha_hora_evento, $id_evento, $cantidad, $id_agente_origen, $id_agente_destino, $posicion_origen, $posicion_destino);
            
            // registrar transaccion
            $transaccion = $this->_registrarTransaccion($token['WsAutenticacion']['ID_AUTENTICACION'], $id_elemento_presentacion, $serie, $f_vto, $lote, $fecha_hora_evento, $id_evento, $cantidad, $id_agente_origen, $id_agente_destino, $posicion_destino, $id_grupo_transaccion);
            
            $this->transaction->commit();
            $this->returnTransaccion($transaccion);
        } catch(Exception $ex) {
            $this->transaction->rollback();
            
            $this->returnTransaccion('', array($ex->getMessage()));
            return;
        }
    }
    
    
    
    protected function _informarMovimiento($id_elemento_presentacion, $serie, $f_vto, $lote, $fecha_hora_evento, $id_evento, 
        $cantidad, $id_agente_origen, $id_agente_destino, $deposito, $posicion, $composicion) 
    {
        // buscar informacion del evento
        $this->loadModel('Evento');
        $evento = $this->Evento->find('first', array(
            'conditions' => array('Evento.ID' => $id_evento)
        ));
        
        if ($evento['TipoEvento']['ACCION_STOCK'] == '+')
            $this->_informarMovimientoIngreso($id_elemento_presentacion, $serie, $f_vto, $lote, $fecha_hora_evento, $id_evento, $cantidad, $id_agente_origen, $id_agente_destino, $deposito, $posicion, $composicion);
        elseif ($evento['TipoEvento']['ACCION_STOCK'] == '-')
            $this->_informarMovimientoEgreso($id_elemento_presentacion, $serie, $f_vto, $lote, $fecha_hora_evento, $id_evento, $cantidad, $id_agente_origen, $id_agente_destino, $deposito, $posicion, $composicion);
        else
            throw new Exception('Evento invalido');
    }
    protected function _informarMovimientoIngreso($id_elemento_presentacion, $serie, $f_vto, $lote, $fecha_hora_evento, $id_evento, 
        $cantidad, $id_agente_origen, $id_agente_destino, $deposito, $posicion, $composicion) 
    {
        if ($serie) {
            // Busco si la Serie ya existe
            $serieData = $this->ElementoSerie->find('first', array(
                'conditions' => array(
                    'ElementoSerie.ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion, 
                    'ElementoSerie.LOTE' => $lote,
                    'ElementoSerie.VENCIMIENTO' => $f_vto,
                    'ElementoSerie.SERIE' => $serie
                )
            ));
            if ($serieData) {
                throw new Exception('Serie existente');
            }
            
            // Genero el Elemento_serie
            $this->ElementoSerie->save(array(
                'ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion,
                'SERIE' => $serie,
                'LOTE' => $lote,
                'VENCIMIENTO' => $f_vto
                //'ID_AGENTE_ORIGEN' => $id_agente_origen
            ));
            
            // Genero el Stock (Si no viene de una composicion)
            if (!$composicion) {
                $this->Stock->save(array(
                    'ID_POSICION' => $posicion,
                    'ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion,
                    'ID_ELEMENTO_SERIE' => $this->ElementoSerie->id,
                    'CANTIDAD' => $cantidad
                ));
            }
        } else {
            // Busco la informacion de la Serie
            $serieData = $this->ElementoSerie->find('first', array(
                'conditions' => array(
                    'ElementoSerie.ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion,
                    'ElementoSerie.LOTE' => $lote,
                    'ElementoSerie.VENCIMIENTO' => $f_vto,
                    'ElementoSerie.SERIE' => null
                )
            ));
            if (!$serieData) {
                // Si no existe, lo doy de alta
                $serieData = array(
                    'ElementoSerie' => array(
                        'ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion,
                        'SERIE' => null,
                        'LOTE' => $lote,
                        'VENCIMIENTO' => $f_vto
                        //'ID_AGENTE_ORIGEN' => $id_agente_origen
                    )
                );
                $this->ElementoSerie->save($serieData['ElementoSerie']);
                
                $serieData['ElementoSerie']['ID'] = $this->ElementoSerie->id;
            }
            
            // Genero el Stock (Si no viene de una composicion)
            if (!$composicion) {
                // Busco la informacion del Stock origen
                $stockData = $this->Stock->find('first', array(
                    'conditions' => array(
                        'Stock.ID_POSICION' => $posicion, 
                        'Stock.ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion, 
                        'Stock.ID_ELEMENTO_SERIE' => $serieData['ElementoSerie']['ID']
                    )
                ));
                if ($stockData) {
                    // Incremento el Stock
                    $stockData['Stock']['CANTIDAD'] += $cantidad;
                    
                    $this->Stock->save($stockData['Stock']);
                } else {
                    // Si no existe, lo doy de alta
                    $this->Stock->save(array(
                        'ID_POSICION' => $posicion,
                        'ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion,
                        'ID_ELEMENTO_SERIE' => $serieData['ElementoSerie']['ID'],
                        'CANTIDAD' => $cantidad
                    ));
                }
            }
        }
    }
    protected function _informarMovimientoEgreso($id_elemento_presentacion, $serie, $f_vto, $lote, $fecha_hora_evento, $id_evento, 
        $cantidad, $id_agente_origen, $id_agente_destino, $deposito, $posicion, $composicion) 
    {
        // Busco la informacion de la Serie
        $serieData = $this->ElementoSerie->find('first', array(
            'conditions' => array(
                'ElementoSerie.ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion, 
                'ElementoSerie.SERIE' => $serie
            )
        ));
        if (!$serieData) {
            throw new Exception('Elemento invalido');
        }
        
        // Busco la informacion del Stock origen
        $stockData = $this->Stock->find('first', array(
            'conditions' => array(
                'Stock.ID_POSICION' => $posicion, 
                'Stock.ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion, 
                'Stock.ID_ELEMENTO_SERIE' => $serieData['ElementoSerie']['ID']
            )
        ));
        
        if ($serie) {
            if ($stockData) {
                // Si esta en stock, descuenta de ahi (todo)
                $this->Stock->delete($stockData['Stock']['ID']);
            } else {
                //si no esta ahí lo busca en composición y descuenta en composición.
                
                // Busco la informacion en Composicion
                $composicionData = $this->ComposicionSerie->find('first', array(
                    'conditions' => array(
                        'ComposicionSerie.ID_SERIE_COMPONENTE' => $serieData['ElementoSerie']['ID']
                    )
                ));
                if (!$serieData) {
                    throw new Exception('No se encontro Stock');
                }
                
                // Salida de Stock de Composicion
                if ($composicionData['ComposicionSerie']['CANTIDAD'] - $cantidad > 0) {
                    $composicionData['ComposicionSerie']['CANTIDAD'] -= $cantidad;
                    
                    $this->ComposicionSerie->save($composicionData);
                } elseif ($composicionData['ComposicionSerie']['CANTIDAD'] - $cantidad == 0) {
                    $this->ComposicionSerie->delete($composicionData['ComposicionSerie']['ID']);
                } else {
                    throw new Exception('Stock insuficiente');
                }
            }
        } else {
            if (!$stockData) {
                throw new Exception('Origen invalido');
            }
            
            // Salida de Stock
            if ($stockData['Stock']['CANTIDAD'] - $cantidad > 0) {
                $stockData['Stock']['CANTIDAD'] -= $cantidad;
                
                $this->Stock->save($stockData);
            } elseif ($stockData['Stock']['CANTIDAD'] - $cantidad == 0) {
                $this->Stock->delete($stockData['Stock']['ID']);
            } else {
                throw new Exception('Stock insuficiente');
            }
        }
    }
    
    protected function _informarComposicion($id_elemento_presentacion_padre, $serie_padre, $f_vto_padre, $lote_padre, $id_elemento_presentacion_hijo, $serie_hijo, $f_vto_hijo, $lote_hijo, $cantidad)
    {
        // Busco si la Serie ya existe
        $seriePadreData = $this->ElementoSerie->find('first', array(
            'conditions' => array(
                'ElementoSerie.ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion_padre, 
                'ElementoSerie.LOTE' => $lote_padre,
                'ElementoSerie.VENCIMIENTO' => $f_vto_padre,
                'ElementoSerie.SERIE' => $serie_padre
            )
        ));
        if (!$seriePadreData) {
            throw new Exception('Elemento padre invalido');
        }
    
        // Busco si la Serie ya existe
        $serieHijoData = $this->ElementoSerie->find('first', array(
            'conditions' => array(
                'ElementoSerie.ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion_hijo, 
                'ElementoSerie.LOTE' => $lote_hijo,
                'ElementoSerie.VENCIMIENTO' => $f_vto_hijo,
                'ElementoSerie.SERIE' => $serie_hijo
            )
        ));
        if (!$serieHijoData) {
            throw new Exception('Elemento hijo invalido');
        }
        
        $this->ComposicionSerie->save(array(
            'ComposicionSerie' => array(
                'ID_SERIE_COMPONENTE' => $serieHijoData['ElementoSerie']['ID'], 
                'ID_SERIE_COMPOSICION' => $seriePadreData['ElementoSerie']['ID'], 
                'CANTIDAD' => $cantidad
            )
        ));
    }
    
    protected function _informarTransferencia($id_elemento_presentacion, $serie, $f_vto, $lote, $fecha_hora_evento, 
        $id_evento, $cantidad, $id_agente_origen, $id_agente_destino, $posicion_origen, $posicion_destino)
    {
        // Busco la informacion de la Serie
        $serieData = $this->ElementoSerie->find('first', array(
            'conditions' => array(
                'ElementoSerie.ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion, 
                'ElementoSerie.LOTE' => $lote,
                'ElementoSerie.VENCIMIENTO' => $f_vto,
                'ElementoSerie.SERIE' => ($serie ? $serie : null)
            )
        ));
        if (!$serieData) {
            throw new Exception('Elemento invalido');
        }
        
        // Busco la informacion del Stock origen
        $stockOrigenData = $this->Stock->find('first', array(
            'conditions' => array(
                'Stock.ID_POSICION' => $posicion_origen, 
                'Stock.ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion, 
                'Stock.ID_ELEMENTO_SERIE' => $serieData['ElementoSerie']['ID']
            )
        ));
        if (!$stockOrigenData) {
            throw new Exception('Origen invalido');
        }
        
        if ($serie) {
            // cambia el ID_POSICION en el Stock
            $stockOrigenData['Stock']['ID_POSICION'] = $posicion_destino;
            $this->Stock->save($stockOrigenData);
            
        } else {
            if ($stockOrigenData['Stock']['CANTIDAD'] < $cantidad) {
                throw new Exception('Stock insuficiente');
            }
            
            // Busco la informacion del Stock destino
            $stockDestinoData = $this->Stock->find('first', array(
                'conditions' => array(
                    'Stock.ID_POSICION' => $posicion_destino, 
                    'Stock.ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion, 
                    'Stock.ID_ELEMENTO_SERIE' => $serieData['ElementoSerie']['ID']
                )
            ));
            if (!$stockDestinoData) {
                // si no existe en la posición destino, crear el registro.
                $stockDestinoData = array(
                    'Stock' => array(
                        'ID_POSICION' => $posicion_destino, 
                        'ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion, 
                        'ID_ELEMENTO_SERIE' => $serieData['ElementoSerie']['ID'],
                        'CANTIDAD' => 0
                    )
                );
            }
            
            // incrementar la cantidad en destino y restar de la origen
            $stockOrigenData['Stock']['CANTIDAD'] -= $cantidad;
            $stockDestinoData['Stock']['CANTIDAD'] += $cantidad;
            
            // Guardo los cambios
            $this->Stock->save($stockDestinoData);
            if ($stockOrigenData['Stock']['CANTIDAD'] == 0) {
                $this->Stock->delete($stockOrigenData['Stock']['ID']);
            } else {
                $this->Stock->save($stockOrigenData);
            }
        }
    }
    
    protected function _registrarTransaccion($id_autenticacion, $id_elemento_presentacion, $serie, $f_vto, $lote, $fecha_hora_evento, 
        $id_evento, $cantidad, $id_agente_origen, $id_agente_destino, $posicion, $id_grupo_transaccion) 
    {
        $serieData = $this->ElementoSerie->find('first', array(
            'conditions' => array(
                'ElementoSerie.ID_ELEMENTO_PRESENTACION' => $id_elemento_presentacion, 
                'ElementoSerie.LOTE' => $lote,
                'ElementoSerie.VENCIMIENTO' => $f_vto,
                'ElementoSerie.SERIE' => ($serie ? $serie : null)
            )
        ));
        
        $agenteOrigenData = $this->Agente->find('first', array(
            'conditions' => array(
                'Agente.id' => $id_agente_origen
            )
        ));
        $gln_origen = null;
        if ($agenteOrigenData) {
            $gln_origen = $agenteOrigenData['Agente']['gln'];
        }
        
        $agenteDestinoData = $this->Agente->find('first', array(
            'conditions' => array(
                'Agente.id' => $id_agente_destino
            )
        ));
        $gln_destino = null;
        if ($agenteDestinoData) {
            $gln_destino = $agenteDestinoData['Agente']['gln'];
        }
        
        $posicionDepositoData = $this->PosicionDeposito->find('first', array(
            'conditions' => array(
                'PosicionDeposito.id' => $posicion
            )
        ));
        $deposito = null;
        if ($posicionDepositoData) {
            $deposito = $posicionDepositoData['PosicionDeposito']['id_deposito'];
        }
        
        $this->TransaccionStock->save(array(
                'ID_AUTENTICACION' => $id_autenticacion,
                'ID_ELEMENTO_SERIE' => $serieData['ElementoSerie']['ID'],
                //'GTIN',
                'SERIE' => $serie,
                'GLN_ORIGEN' => $gln_origen,
                'GLN_DESTINO' => $gln_destino,
                'ID_EVENTO' => $id_evento,
                'CANTIDAD' => $cantidad,
                'FECHA_HORA' => $fecha_hora_evento,
                'DEPOSITO' => $deposito,
                'POSICION' => $posicion,
                'ID_GRUPO_TRANSACCION' => $id_grupo_transaccion
        ));
        
        return $this->TransaccionStock->getLastInsertID();
    }
    
    
    protected function _validateToken($token) {
        $date = new DateTime('now');
        $date->sub(new DateInterval('P1D'));
        
        $data = $this->WsAutenticacion->find('first', array(
            'conditions' => array('WsAutenticacion.TOKEN' => $token, 'WsAutenticacion.FECHA_HORA >=' => $date->format('Y-m-d H:i:s'))
        ));
        
        return $data;
    }
    
    
    protected function returnToken($token, $errores = array()) {
        $this->set(array(
            'token' => $token,
            'errores' => $errores,
            '_serialize' => array('token','errores')
        ));
    }
    protected function returnTransaccion($id_transaccion, $errores = array()) {
        $this->set(array(
            'id_transaccion' => $id_transaccion,
            'errores' => $errores,
            '_serialize' => array('id_transaccion','errores')
        ));
    }
}
