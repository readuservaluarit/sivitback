<?php

    /**
    * @secured(CONSULTA_ASIENTO)
    */
    class AsientosController extends AppController {
        public $name = 'Asientos';
        public $model = 'Asiento';
        public $sub_model = 'AsientoCuentaContable';
        public $helpers = array ('Session', 'Paginator', 'Js');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
        public $llamada_interna = 0;


        private function RecuperaFiltros(){

            $conditions = array(); 
            

            $id_cuenta_contable = strtolower($this->getFromRequestOrSession('Asiento.id_cuenta_contable'));
            $id_ejercicio_contable = strtolower($this->getFromRequestOrSession('Asiento.id_ejercicio_contable'));
			$id_asiento_efecto = strtolower($this->getFromRequestOrSession('Asiento.id_asiento_efecto'));
            $fecha_desde = strtolower($this->getFromRequestOrSession('Asiento.fecha_desde'));
            $fecha_hasta = strtolower($this->getFromRequestOrSession('Asiento.fecha_hasta'));
            $codigo_cuenta_contable_desde = strtolower($this->getFromRequestOrSession('Asiento.codigo_cuenta_contable_desde'));
            $codigo_cuenta_contable_hasta = strtolower($this->getFromRequestOrSession('Asiento.codigo_cuenta_contable_hasta'));
            $id_comprobante = strtolower($this->getFromRequestOrSession('Asiento.id_comprobante'));
            $id_periodo_contable = strtolower($this->getFromRequestOrSession('Asiento.id_periodo_contable'));
            $id_tipo_comprobante = strtolower($this->getFromRequestOrSession('Asiento.id_tipo_comprobante'));
            $id_punto_venta = strtolower($this->getFromRequestOrSession('Asiento.id_punto_venta'));
            $id_persona = strtolower($this->getFromRequestOrSession('Asiento.id_persona'));
            $razon_social = strtolower($this->getFromRequestOrSession('Asiento.razon_social'));
            $nro_comprobante = strtolower($this->getFromRequestOrSession('Asiento.nro_comprobante'));
            $id_tipo_asiento = strtolower($this->getFromRequestOrSession('Asiento.id_tipo_asiento'));
            $id_estado_asiento = $this->getFromRequestOrSession('Asiento.id_estado_asiento');
            $codigo = $this->getFromRequestOrSession('Asiento.codigo');
            
            $id = strtolower($this->getFromRequestOrSession('Asiento.id'));

		   if ($id != "") 
                array_push($conditions, array('Asiento.id ' => $id ));
			
		   if ($codigo != "") 
                array_push($conditions, array('Asiento.codigo LIKE ' => '%' .$codigo .'%' ));

            if ($id_tipo_asiento != "") 
                array_push($conditions, array('AsientoCuentaContable.id_tipo_asiento ' => $id_tipo_asiento));
                
            if ($id_cuenta_contable != "") 
                array_push($conditions, array('AsientoCuentaContable.id_cuenta_contable ' => $id_cuenta_contable));

            if ($id_asiento_efecto != "") 
                array_push($conditions, array('Asiento.id_asiento_efecto ' => $id_asiento_efecto));
			
			if ($id_ejercicio_contable != "") 
                array_push($conditions, array('Asiento.id_ejercicio_contable ' => $id_ejercicio_contable));

            if ($fecha_desde != "") 
                array_push($conditions, array('DATE(Asiento.fecha) >= ' => $fecha_desde));

            if ($fecha_hasta != "") 
                array_push($conditions, array('DATE(Asiento.fecha) <= ' => $fecha_hasta));

            if ($codigo_cuenta_contable_desde != "" && $id_cuenta_contable=="") 
                array_push($conditions, array('CuentaContable.codigo >=' => $codigo_cuenta_contable_desde));

            if ($codigo_cuenta_contable_hasta != "" && $id_cuenta_contable=="") 
                array_push($conditions, array('CuentaContable.codigo <=' => $codigo_cuenta_contable_hasta));                    


                
            if ($id_comprobante != "") 
                array_push($conditions, array('Asiento.id_comprobante ' => $id_comprobante ));

            if ($id_periodo_contable != "") 
                array_push($conditions, array('Asiento.id_periodo_contable ' => $id_periodo_contable ));
            
            
            if ($id_persona != "") 
                array_push($conditions, array('Comprobante.id_persona ' => $id_persona ));
                
            if ($id_tipo_comprobante != "") 
                array_push($conditions, array('Comprobante.id_tipo_comprobante ' => $id_tipo_comprobante ));
                
            if ($nro_comprobante != "") 
                array_push($conditions, array('Comprobante.nro_comprobante LIKE' => '%' .$nro_comprobante .'%'));         
             
             
            if ($id_punto_venta != "") 
                array_push($conditions, array('Comprobante.id_punto_venta ' => $id_punto_venta ));
                
                
            if ($id_estado_asiento != "") 
                array_push($conditions, array('Asiento.id_estado_asiento ' => $id_estado_asiento ));    
                
            if ($codigo != "") 
                array_push($conditions, array('Asiento.codigo ' => $codigo ));                 
			
            
            
            return $conditions;
        }

        /**
        * @secured(CONSULTA_ASIENTO)
        */
        public function index() {

            if($this->request->is('ajax'))
                $this->layout = 'ajax';

            $this->loadModel($this->model);
            $this->loadModel("Comprobante");
            $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);


            //Recuperacion de Filtros
            $conditions = $this->RecuperaFiltros();


            $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
                //'contain' => array('Asientosucursal'),
              
                'conditions' => $conditions,
                'limit' => $this->numrecords,
                'page' => $this->getPageNum()
                //,'order'=>array("Asiento.id asc")
            );

            if($this->RequestHandler->ext != 'json'){  

          
            }else
            { // vista json
                $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
                $page_count = $this->params['paging'][$this->model]['pageCount'];


                foreach($data as &$record)
                {        
                    
                    $record["Asiento"]["fecha"] = $this->{$this->model}->formatDate($record["Asiento"]["fecha"]);
                    unset($record["Moneda"]);
                    unset($record["TipoAsiento"]);
                    unset($record["AsientoEfecto"]);
                    unset($record["EjercicioContable"]);
                    unset($record["ClaseAsiento"]);
                    unset($record["EstadoAsiento"]);
                    //unset($record["EstadoAsiento"]);
                }       

                $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "list",
                    "content" => $data,
                    "page_count" =>$page_count
                );
                $this->set($output);
                $this->set("_serialize", array("status", "message","page_count", "content"));
            }
        }

        /**
        * @secured(ADD_ASIENTO)
        */
        public function add() 
        {
            if (!$this->request->is('get'))
            {
                $this->loadModel($this->model);
                $id_asiento = '';

                $dataSource = $this->Asiento->getdatasource();
                /* Lo hago transaccional */
           
                $dataSource->begin();
                $this->loadModel("Comprobante"); 
                $this->loadModel("PeriodoContable"); 
                $this->Comprobante->CalcularMonedaBaseSecundaria($this->request->data,"Asiento");
                
                //TODO: Controlar que el periodo no este cerrado.
                if($this->chequeaPeriodo($this->request->data)){
                    
                   
                try{
                    
                     $periodo_contable = $this->PeriodoContable->getPeriodo($this->request->data["Asiento"]["id_periodo_contable"] );
                     $this->request->data["Asiento"]["id_ejercicio_contable"] = $periodo_contable["id_ejercicio_contable"];
                     $this->request->data["Asiento"]["manual"] = 1;//si se genera usando el add es manual
                     $this->request->data["Asiento"]["id_usuario"] = $this->Auth->user('id'); 
           
                    if ($this->{$this->model}->saveAll($this->request->data)){
                         
                        $dataSource->commit();
                        $mensaje = "El Asiento ha sido creado exitosamente";
                        $status = EnumError::SUCCESS;
                        $id_asiento = $this->{$this->model}->id;
                        $codigo = $this->{$this->model}->codigo;
                        $this->loadModel("CentroCosto");
                        
                        $this->CentroCosto->ApropiacionPorAsiento($id_asiento);//https

                    }
                    else{
                        $dataSource->rollback();
                        $errores = $this->{$this->model}->validationErrors;
                        $errores_string = "";
                        foreach ($errores as $error)
                        {    //recorro los errores y armo el mensaje
                            $errores_string.= "&bull; ".$error[0]."\n";
                        }
                        $mensaje = $errores_string;
                        $status = EnumError::ERROR;     
                    }
                }
                catch(Exception $e)
                {
                    $dataSource->rollback();
                    $mensaje = "Ha ocurrido un error,el Asiento no ha podido ser creado.";
                    $status = EnumError::ERROR;
                }
                
                }else{
                    
                   $mensaje = "Ha ocurrido un error,el Asiento no ha podido ser creado ya que el peri&oacute;do seleccionado se encuenta cerrado o no esta disponible.";
                   $status = EnumError::ERROR; 
                    
                    
                }

                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => "",
                    "id_add" =>$id_asiento,
                    "codigo_add" =>$codigo
                );
                
                
                if($this->llamada_interna == 0){ //se pone en uno cuando uso el add desde otro metodo del controller
                //si es json muestro esto
                    if($this->RequestHandler->ext == 'json')
                    { 
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content" ,"id_add","codigo_add"));
                    }
                    else
                    {
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('action' => 'index'));
                    }  
                    
                }else{
                    
                    return $output;
                }   
            }
        }

        /**
        * @secured(MODIFICACION_ASIENTO)
        */  
        public function edit($id) {


            if (!$this->request->is('get')){

                $this->loadModel($this->model);
                $this->{$this->model}->id = $id;
                
                
                if($this->Asiento->getDefinitivoGrabado($id)!=1){
                try{ 
                     $this->loadModel("Comprobante");
                     $this->loadModel("PeriodoContable");
                     $periodo_contable = $this->PeriodoContable->getPeriodo($this->request->data["Asiento"]["id_periodo_contable"] );
                     $ejericio_contable = $this->PeriodoContable->getEjercicioContableFromPeriodo($this->request->data["Asiento"]["id_periodo_contable"] );
                     
                     $this->request->data["Asiento"]["id_ejercicio_contable"] = $periodo_contable["id_ejercicio_contable"];
                     
                     if($this->request->data["AsientoCuentaContable"]){
                         foreach($this->request->data["AsientoCuentaContable"] as &$dato){
                             
                             $dato["AsientoCuentaContableCentroCostoItem"] = json_decode($dato["AsientoCuentaContableCentroCostoItem"]);
                             
                             
                         }
                     }
                     
                     
                     
                     if($periodo_contable["cerrado"] == 1 || $ejericio_contable["id_estado"] == EnumEstado::Cerrada)
                     {
                     	
                     	$message = "El Asiento no es posible editarlo. Ya que el Peri&oacute;do contable en el cual se encuenta se encuentra cerrado o el Ejercicio Contable esta cerrado";
                     	$error = EnumError::ERROR;
                     	
                     }else{
                     	
                     	
                     
	                     $this->Comprobante->CalcularMonedaBaseSecundaria($this->request->data,"Asiento");
	                     
	                    if ($this->{$this->model}->saveAll($this->request->data,array('deep'=>true))){
	                        $mensaje =  "El Asiento ha sido modificado exitosamente";
	                        $status = EnumError::SUCCESS;
	
	
	                    }else{   //si hubo error recupero los errores de los models
	
	                        $errores = $this->{$this->model}->validationErrors;
	                        $errores_string = "";
	                        foreach ($errores as $error){ //recorro los errores y armo el mensaje
	                            $errores_string.= "&bull; ".$error[0]."\n";
	
	                        }
	                        $mensaje = $errores_string;
	                        $status = EnumError::ERROR; 
	                    }
                     }

                    
                }catch(Exception $e){
                   // $this->Session->setFlash('Ha ocurrido un error, el Asiento no ha podido modificarse.', 'error');
                       $status = EnumError::ERROR; 
                       $mensaje = 'Ha ocurrido un error, el Asiento no ha podido modificarse.'.$e->getMessage();
                }

                }else{
                     $mensaje = 'El Asiento en esta instacia no puede ser editado';
                     $status = EnumError::ERROR;   
                    
                }
                
                
                if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 

                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));

                    } 
            }else{ //si me pide algun dato me debe mandar el id
                if($this->RequestHandler->ext == 'json'){ 

                    $this->{$this->model}->id = $id;
                    //$this->{$this->model}->contain('Provincia','Pais');
                    $this->request->data = $this->{$this->model}->read();          

                    $output = array(
                        "status" =>EnumError::SUCCESS,
                        "message" => "list",
                        "content" => $this->request->data
                    );   

                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content")); 
                }else{
                    $this->redirect(array('action' => 'abm', 'M', $id));
                }


            } 

            if($this->RequestHandler->ext != 'json')
                $this->redirect(array('action' => 'abm', 'M', $id));
        }

      
        
        
public function MayorDeCuentasSaldo(){

         

            $this->loadModel("AsientoCuentaContable");
            $this->loadModel("Comprobante");
            $this->loadModel("CuentaContable");

            $conditions = $this->RecuperaFiltros();
            
            $id_tipo_moneda = $this->getFromRequestOrSession('Asiento.id_tipo_moneda');
            $campo_valor = $this->AsientoCuentaContable->getFieldByTipoMoneda($id_tipo_moneda);//obtengo el campo en la cual expresar el reporte
            
            $d_cuenta_contable='';
         

           // $conditions = array_merge($conditions,array("AsientoCuentaContable.id_cuenta_contable=5"));
            
            $conditions2 = array_merge($conditions,array("AsientoCuentaContable.es_debe=1"));
            
            
            
            $cuenta_contable = $this->CuentaContable->find('first', array(
                 
                    'conditions' => array('CuentaContable.id ' => $this->getFromRequestOrSession('Asiento.id_cuenta_contable') ),
                    )
                    
                    );
                    
            
            if($cuenta_contable){
                    $codigo_cuenta_contable = $cuenta_contable["CuentaContable"]["codigo"];      
                    $d_cuenta_contable = $cuenta_contable["CuentaContable"]["d_cuenta_contable"];      
                    
                    $data_debe = $this->AsientoCuentaContable->find('all', array(
                            'fields'=>array("ROUND(SUM(AsientoCuentaContable.monto),2) as monto","Asiento.valor_moneda","Asiento.valor_moneda2","Asiento.valor_moneda3"),  
                            'conditions' => $conditions2,
                            //'contain' =>array('Comprobante'=>array('PuntoVenta','EstadoComprobante','Moneda'),'Producto'),
                            'contain' => array("CuentaContable","Asiento"),
                            'group'=> array('AsientoCuentaContable.es_debe')
                          
                        ));
                        
                      
                     if(isset($data_debe[0][0]["monto"])){    
                        $total_debe =  $data_debe[0][0]["monto"]; 
                     
                        
                     }else
                        $total_debe = 0.00;
                        
                    
                    
                    
                    $conditions3 = array_merge($conditions,array("AsientoCuentaContable.es_debe=0"));    
                    
                    $data_haber = $this->AsientoCuentaContable->find('all', array(
                            'fields'=>array("ROUND(SUM(AsientoCuentaContable.monto),2) as monto","Asiento.valor_moneda","Asiento.valor_moneda2","Asiento.valor_moneda3"),  
                            'conditions' => $conditions3,
                            //'contain' =>array('Comprobante'=>array('PuntoVenta','EstadoComprobante','Moneda'),'Producto'),
                            'contain' => array("CuentaContable","Asiento"),
                            'group'=> array('AsientoCuentaContable.es_debe')
                          
                        ));     
                    
                    if(isset($data_haber[0][0]["monto"]))
                        $total_haber = $data_haber[0][0]["monto"];
                    else
                        $total_haber = 0.00;
                    
                    $resultado =array();
                    
                    
                    $valor["AsientoCuentaContable"]["debe_monto"] = (string) $total_debe;
                    $valor["AsientoCuentaContable"]["haber_monto"] = (string) $total_haber;
                    $valor["AsientoCuentaContable"]["saldo_monto"] =  (string)($total_debe - $total_haber);
                    $valor["AsientoCuentaContable"]["d_cuenta_contable"] =  $d_cuenta_contable;
                    $valor["AsientoCuentaContable"]["codigo_cuenta_contable"] =  $codigo_cuenta_contable;
                 
                    
                    array_push($resultado,$valor);
                    $error = EnumError::SUCCESS;
                    $message = "list";
                    
            }else{
             $error = EnumError::ERROR;   
             $message = "La cuenta contable elegida no existe"; 
             $resultado = "";  
            }
             $output = array(
                "status" =>$error ,
                "message" => $message,
                "content" => $resultado,
                "page_count" =>"0"
            );

           

            echo json_encode($output);
            die(); 
            
            
}



        
        
        
        
        


        /**
        * @secured(CONSULTA_ASIENTO)
        */ 
        public function getModel($vista='default')
        {    
            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
        }
		/* [Deprecate] fueron Arreglado de otra forma*/
        ///MP: negrada por q no se referenciar desde asientos modelo contra  AppController (dos niveles superiores)
        public function getModelCamposDefault2()
        {
            return parent::getModelCamposDefault();
        }
        
		///MP: negrada por q no se referenciar desde asientos modelo contra  AppController (dos niveles superiores)
        public function setDefaultFieldsForView2($model)
        {
            return parent::setDefaultFieldsForView($model);
        }

        private function editforView($model,$vista)
        {  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);    

        }

        
        
        /**
         * @secured(MODIFICACION_ASIENTO)
         */  

        public function revertir($id){


            echo json_encode( $this->Asiento->revertir($id) ); 
            die();
        }
        
   
   
   /*Esta funcion chequea que el periodo enviado en asiento que se esta insertando este abierto*/     
    private function chequeaPeriodo($data){
       
        $this->loadModel("PeriodoContable");
        
        $periodo_contable = $this->PeriodoContable->find('first', array(
                    'conditions' =>array("PeriodoContable.id" =>$data["Asiento"]["id_periodo_contable"])
                )
                );
        
       if( isset($periodo_contable["PeriodoContable"]["cerrado"])  && $periodo_contable["PeriodoContable"]["cerrado"] == 1 && isset($periodo_contable["PeriodoContable"]["disponible"])  && $periodo_contable["PeriodoContable"]["disponible"] == 1){ 
        return false;
       }else{ 
        return true;
       } 
        
    }
    
  
    
   public function importaCsvDetalleAsiento($id_asiento=0){
       ob_start(); 
       $this->loadModel("AsientoCuentaContable");
       $this->loadModel("Comprobante");
       $error = '';
       $message = '';
       $message_type = '';
       
       
    
        if($id_asiento !=0)//si me envia el id asiento o si lo acabo de agregar entonces importo lo que me envia
            $this->ImportarArchivoCsv($model_destino="CsvImportAsiento",$error,$message);
         
         
         if($error == "success"){
         	
         	$sql_cuentas_faltantes = "SELECT codigo_cuenta from csv_import_asiento where codigo_cuenta NOT IN (select codigo from cuenta_contable)";
         	
         	$cuentas_faltantes = $this->AsientoCuentaContable->query($sql_cuentas_faltantes);
         	
         	$array_cuentas_faltantes = array();
         	
         	if(count($cuentas_faltantes)>0){
         		
         		foreach($cuentas_faltantes as $cuenta){
         			
         			if($cuenta["csv_import_asiento"]["codigo_cuenta"] != "codigo_cuenta")
         				array_push($array_cuentas_faltantes, $cuenta["csv_import_asiento"]["codigo_cuenta"]);
         		}
         		
         		
         	}
         	
         	
          //aca debo procesar la tabla  
          try{
          $sql = 'INSERT INTO asiento_cuenta_contable (es_debe,monto,id_cuenta_contable,id_asiento)
                   SELECT 
 
 
                          IF( REPLACE(TRIM(REPLACE(REPLACE(REPLACE(csvia.debe,".",""),"(","-"),")","")),",",".")>0, 1, 0) AS es_debe,
                          IF( REPLACE(TRIM(REPLACE(REPLACE(REPLACE(csvia.debe,".",""),"(","-"),")","")),",",".")>0, REPLACE(TRIM(REPLACE(REPLACE(REPLACE(csvia.debe,".",""),"(","-"),")","")),",","."), REPLACE(TRIM(REPLACE(REPLACE(REPLACE(csvia.haber,".",""),"(","-"),")","")),",",".")) AS monto

                         ,cuenta_contable.id, 
                         
                        '.$id_asiento.' AS id_asiento
                    FROM csv_import_asiento csvia
                    JOIN cuenta_contable ON cuenta_contable.`codigo`= TRIM(csvia.`codigo_cuenta`)
                    WHERE cuenta_contable.id>0';
                    
                    
                    
          $this->AsientoCuentaContable->query($sql);
          $error = EnumError::SUCCESS;
          $message = "Los items del asiento han sido importados exitosamente.";
          
          if(count($array_cuentas_faltantes)>0){
          	$message.= ". Las siguientes cuentas no existen en la base de datos:".implode(",",$array_cuentas_faltantes);
          	$message_type = EnumMessageType::MessageBox;
          }
          
         }catch(Exception $e){
             
            $error = EnumError::ERROR ;
            $message = "No pudieron insertarse los asientos. Intente Nuevamente, revise el formato del csv.".$e->getMessage();
             
         }  
         }
         
        $this->autoRender = false;
        
        ob_clean();
        $output = array(
            "status" => $error,
            "message" => $message,
            "content" => "CONTENT",
            "id_add" => $id_asiento,
        	"message_type"=>$message_type
        );
        //$this->set($output);
        //$this->set("_serialize", array("status", "message","page_count", "content"));
        
        echo json_encode($output);  
         
         
         
   }
   
   public function deleteItem($id_item,$externo=1){
   	
   	$this->loadModel("AsientoCuentaContable");
   	
   	
   	
   	
   	
   	
   	//TODO: BAJA LOGICA
   	$this->AsientoCuentaContable->id = $id_item;
   	try{
   		
   		//TODO: aca se debe chequear si el item es borrable. O sea sino se factura etc.
   		
   		
   
   		

   			
   			if ($this->AsientoCuentaContable->delete()) {
   				
   			
   					
   					$status = EnumError::SUCCESS;
   					$mensaje = "El Item ha sido eliminado exitosamente.";
   					
   			}else{
   				
   				
   				$errores = $this->{$this->model}->validationErrors;
   				$errores_string = "";
   				foreach ($errores as $error){
   					$errores_string.= "&bull; ".$error[0]."\n";
   					
   				}
   				$mensaje = $errores_string;
   				$status = EnumError::ERROR;
   				
   				
   			}
   			
   	
   		
   	}
   	catch(Exception $ex){
   		$mensaje = "Ha ocurrido un error, el item no ha podido ser borrado:".$ex->getMessage();
   		$status = EnumError::ERROR;
   	}
   	
   	
   	
   	$output = array(
   			"status" => $status,
   			"message" => $mensaje,
   			"content" => ""
   	);
   	
   	if($this->RequestHandler->ext == 'json' && $externo == 1){
   		
   		$this->output = $output;
   		$this->set($output);
   		$this->set("_serialize", array("status", "message", "content"));
   	}
   	
   	
   	if($externo == 0 )
   		return $output;
   		
   		
   		
   		
   }
   
   
   /**
    * @secured(BTN_REABRIR_ASIENTO)
    */
   public function abreAsiento($id_asiento){
   	
   	try{
   	$this->loadModel("Asiento");
   	$this->Asiento->id = $id_asiento;
   	$this->Asiento->saveField('id_estado_asiento', EnumEstadoAsiento::EnRevision);
   	
   	$mensaje = "El Asiento ha sido modificado.";
   	$status = EnumError::SUCCESS;
   	
   	}catch(Exception $e){
   		
   		$mensaje = "Ha ocurrido un error, el Asiento no ha podido ser modificado:".$ex->getMessage();
   		$status = EnumError::ERROR;
   		
   	}
   	
   	
   	
   	$output = array(
   			"status" => $status,
   			"message" => $mensaje,
   			"content" => ""
   	);
   	
   	$this->set($output);
   	$this->set("_serialize", array("status", "message", "content"));
   	
   }

}
?>