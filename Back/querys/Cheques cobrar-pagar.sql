﻿/*Tercero a cobrar*/
SELECT SUM(monto) monto,WEEK(fecha_cheque) semana,YEAR(fecha_cheque) año,

DATE_SUB(
  DATE_ADD(MAKEDATE(YEAR(fecha_cheque), 1), INTERVAL WEEK(fecha_cheque) WEEK),
  INTERVAL WEEKDAY(
    DATE_ADD(MAKEDATE(YEAR(fecha_cheque), 1), INTERVAL WEEK(fecha_cheque) WEEK)
  ) -0 DAY) AS fecha_inicio_semana



 FROM cheque WHERE id_tipo_cheque = 1 AND id_estado_cheque = 1


GROUP BY YEAR(fecha_cheque),WEEK(fecha_cheque)

ORDER BY YEAR(fecha_cheque) ASC,WEEK(fecha_cheque) ASC
LIMIT 100000




/* Cheque propio para pagar*/
SELECT SUM(monto) monto,WEEK(fecha_cheque) semana,YEAR(fecha_cheque) año,

DATE_SUB(
  DATE_ADD(MAKEDATE(YEAR(fecha_cheque), 1), INTERVAL WEEK(fecha_cheque) WEEK),
  INTERVAL WEEKDAY(
    DATE_ADD(MAKEDATE(YEAR(fecha_cheque), 1), INTERVAL WEEK(fecha_cheque) WEEK)
  ) -0 DAY) AS fecha_inicio_semana



 FROM cheque WHERE id_tipo_cheque = 2 AND id_estado_cheque = 3
 
 AND YEAR(fecha_cheque)>=YEAR(NOW())


GROUP BY YEAR(fecha_cheque),WEEK(fecha_cheque)

ORDER BY YEAR(fecha_cheque) ASC,WEEK(fecha_cheque) ASC
LIMIT 100000
