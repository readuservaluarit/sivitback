<?php

App::import('Vendor','tcpdf/tcpdf');
App::import('Vendor','tcpdf/tcpdf_barcodes_1d'); 



class MyTCPDF extends TCPDF{
	
	public $datos_header;
	public $datos_footer;
	public $bar_code;
	public $observaciones;
	
	public function set_datos_header($datos_empresa){
		$this->datos_header = $datos_empresa;
		
	}
	public function set_datos_footer($datos_empresa_footer){
		$this->datos_footer = $datos_empresa_footer;
		
	}
	public function set_barcode($bar_code){
		$this->bar_code = $bar_code;
		
	}
	
	public function set_observaciones($observaciones){
		$this->observaciones = $observaciones;
		
	}
	public function Header(){
		
		
		
			$this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_header, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
		
	}
	
	
	protected $last_page_flag = false;
	
	public function Close() {
		$this->last_page_flag = true;
		parent::Close();
	}
	
	
	public function Footer(){
		
		
			$this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_footer, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
			$this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->observaciones, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
			
			$style['text'] = true;
			$style['border'] = true;
			$style['align'] = 'C';
			$this->write1DBarcode($this->bar_code, 'I25', '40', '', '', 9, 0.4, $style, 'N');
			
			//$this->writeHTMLCell($w = 0, $h = 0, $x = '200', $y = '', $this->bar_code, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
			//$this->Cell(0, 20, $this->bar_code , 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(0, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	
	}
	
}
