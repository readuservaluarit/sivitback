<?php

App::uses('ComprobanteItemsController', 'Controller');


class OrdenTrabajoItemsController extends ComprobanteItemsController {
    
    public $name = EnumController::OrdenTrabajoItems;
    public $model = 'ComprobanteItem';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    	  
    
  
        
        
        

	
	
	
	 
   
    
    /**
     * @secured(BAJA_ORDEN_TRABAJO)
     */
    public function delete($id,$externo=1){
    	
    	
    	$this->loadModel("Comprobante");
    	$this->loadModel("ComprobanteItem");
    	$this->loadModel("ComprobanteItemLote");
    	
    	
    	
    	try{
    		$item = $this->ComprobanteItem->find('first',array(
    				'conditions' => array('ComprobanteItem.id' => $id),
    				'contain' =>false
    		));
    		
    		
    		
    		switch ($item["ComprobanteItem"]["id_detalle_tipo_comprobante"]){
    			
    			case EnumDetalleTipoComprobante::EtapaOrdenTrabajo://70
    				
    				/*2 Niveles para borrar*/
    				
    				
    				
    				$items_etapa = $this->ComprobanteItem->find('all',array(
    				'conditions' => array('ComprobanteItem.id_comprobante_item_origen' => $id),
    				'contain' =>false
    				));
    				
    				
    				
    				
    				foreach($items_etapa as $detalle){
    					
    					
    					$conditions = array();
    					array_push($conditions, array('ComprobanteItemLote.id_comprobante_item' => $detalle["ComprobanteItem"]["id"]));
    					
    					$comprobante_item_lotes = $this->ComprobanteItemLote->find('all', array(
    							'fields'=>array("ComprobanteItemLote.id"),
    							'conditions' => $conditions,
    							'contain'=>false,
    							
    					));
    					
    					
    					if(count($comprobante_item_lotes)>0){
    						
    						
    						//$this->ComprobanteItem->deleteAll(array('ComprobanteItem.id_comprobante_item_origen'=>$comprobante_item_equipos));
    						
    						$array_lotes = array();
    						foreach ($comprobante_item_lotes as $lote){
    							
    							array_push($array_lotes, $lote["ComprobanteItemLote"]["id"]);
    							
    						}
    						
    						$array_lotes_comma = implode (", ", $array_lotes);
    						$this->ComprobanteItem->query("DELETE from comprobante_item_lote where id IN(".$array_lotes_comma.")");
    						
    					}
    					
    					
    					
    					//$this->ComprobanteItemLote->deleteAll(array('ComprobanteItem.id_comprobante_item_origen'=>$comprobante_item_lotes));
    					
    					
    					
    					
    					
    					$conditions = array();
    					array_push($conditions, array('ComprobanteItem.id_comprobante_item_origen' => $detalle["ComprobanteItem"]["id"]));
    					
    					
    					
    					$comprobante_item_equipos = $this->ComprobanteItem->find('all', array(
    							
    							'fields'=>array("ComprobanteItem.id"),
    							'conditions' => $conditions,
    							'contain'=>false
    							
    					));
    					
    					
    					if(count($comprobante_item_equipos)>0){
    					
    					
    					//$this->ComprobanteItem->deleteAll(array('ComprobanteItem.id_comprobante_item_origen'=>$comprobante_item_equipos));
    					
    					$array_equipos = array();
    					foreach ($comprobante_item_equipos as $equipo){
    						
    						array_push($array_equipos, $equipo["ComprobanteItem"]["id"]);
    						
    					}
    					
    					$array_equipos_comma = implode (", ", $array_equipos);
    					$this->ComprobanteItem->query("DELETE from comprobante_item where id IN(".$array_equipos_comma.")");
    					
    					}
    					
    					
    					
    					
    					$this->ComprobanteItem->id =  $detalle["ComprobanteItem"]["id"];
    					$this->ComprobanteItem->delete();
    					
    				}
    				
    				
    				
    				$this->ComprobanteItem->id =  $id;
    				$this->ComprobanteItem->delete();
    				
    				break;
    				
    			case EnumDetalleTipoComprobante::ReservadeBiendeUsoOrdenDeTrabajo://71
    			case EnumDetalleTipoComprobante::OperacionOrdenDeTrabajo://72
    				
    				$comprobante_item = new ComprobanteItem();
    		
    				
    				$reserva_items = $comprobante_item->query("Delete from comprobante_item where id_comprobante_item_origen=".$id);
    				$reserva_items = $comprobante_item->query("Delete from comprobante_item_lote where id_comprobante_item=".$id);
    				$reserva_items = $comprobante_item->query("Delete from comprobante_item where id=".$id);
    				
    				
    				
    				
    				
    				
    				
    				
    				break;
    				
    				
    			case EnumDetalleTipoComprobante::InstrumentalDeTrabajo://73
    				
    				$this->ComprobanteItem->id = $id;
    				$this->ComprobanteItem->delete();
    				
    				break;
    				
    				
    		}
    		
    		$status = EnumError::SUCCESS;
    		$mensaje = "El Item ha sido eliminado exitosamente.";
    		
    	}catch(Exception $e){
    		$mensaje = "Ha ocurrido un error, el item no ha podido ser borrado".$e->getMessage();
    		$status = EnumError::ERROR;
    	}
    	
    	
    	
    	$output = array(
    			"status" => $status,
    			"message" => $mensaje,
    			"content" => ""
    	);
    	
    	$this->output = $output;
    	$this->set($output);
    	$this->set("_serialize", array("status", "message", "content"));
    	
    }
    
	
    /**
    * @secured(CONSULTA_ORDEN_TRABAJO)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);
    }
    
    public function prepararRespuesta($data,&$page_count){
        
        
        
    $filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');//dice como mostrar los items    
    $array_id_comprobantes = array();
    

    
    if($filtro_agrupado == EnumTipoFiltroComprobanteItem::agrupadaporComprobante){ //si es la vista agrupada tomo los filtros de los items y agrupo en memoria por id_comprobante
    
    
        
       foreach($data as $key=>$valor){
           if(in_array($valor["ComprobanteItem"]["id_comprobante"],$array_id_comprobantes))
            unset($data[$key]);
           else
            array_push($array_id_comprobantes,$valor["ComprobanteItem"]["id_comprobante"]);
           
       }
   }    
        
    return parent::prepararRespuesta($data,$page_count);
    }
    
  
     /**
    * @secured(CONSULTA_ORDEN_TRABAJO)
    */       
   public function reporte_orden_trabajo_items($view="agrupadaporcomprobante"){
        
        $this->paginado =0;
        
        $filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');
        
        
        
        switch($filtro_agrupado){
        	
        	case EnumTipoFiltroComprobanteItem::individual:
        	case "":
        		$view = "noagrupada";
        	break;
        	case EnumTipoFiltroComprobanteItem::agrupadaporComprobante:
        		$view="agrupadaporcomprobante";
        	break;
        	case EnumTipoFiltroComprobanteItem::agrupadaporPersona:
        		$view="agrupadaporpersona";
        		break;
        		
        }
        
         $this->ExportarExcel($view,"index","Reporte de Pedidos Internos");
                 
        
        
    } 

    
}
?>