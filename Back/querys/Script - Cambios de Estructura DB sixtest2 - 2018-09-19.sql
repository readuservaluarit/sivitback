
/*Mercado libre Peios*/
INSERT INTO `tipo_funcion` (`id`, `id_modulo`, `d_modulo`) VALUES (247, '1300', 'VALUARIT WEB '); 
            
INSERT INTO `funcion` (`c_funcion`,d_funcion, `id_tipo_funcion`) VALUES ('TAB_MERCADO_LIBRE_CLIENTE', 'Tab Mercado libre de cliente','247'); 
INSERT INTO `funcion` (`c_funcion`, d_funcion,`id_tipo_funcion`) VALUES ('TAB_MERCADO_LIBRE_PRODUCTO','Tab Mercado libre de Producto', '247'); 
            
INSERT INTO `funcion` (`c_funcion`, d_funcion,`id_tipo_funcion`) VALUES ('MENU_PEDIDO_WEB','Consulta de Pedido Web', '247'); 
            
INSERT INTO `funcion` (`c_funcion`, d_funcion,`id_tipo_funcion`) VALUES ('CONSULTA_PEDIDO_WEB','Consulta de Pedido Web', '247'); 
INSERT INTO `funcion` (`c_funcion`, d_funcion,`id_tipo_funcion`) VALUES ('MODIFICACION_PEDIDO_WEB','Modificacion de  Pedido Web', '247'); 
INSERT INTO `funcion` (`c_funcion`, d_funcion,`id_tipo_funcion`) VALUES ('BAJA_PEDIDO_WEB','Baja de Pedido Web', '247'); 
INSERT INTO `funcion` (`c_funcion`, d_funcion,`id_tipo_funcion`) VALUES ('ADD_PEDIDO_WEB','Alta de Pedido Web', '247'); 
            
INSERT INTO `funcion` (`c_funcion`, d_funcion,`id_tipo_funcion`) VALUES ('REPORTE_PEDIDO_WEB','Menu de Pedido Web', '247'); 

INSERT INTO `funcion` (`c_funcion`, d_funcion,`id_tipo_funcion`) VALUES ('BTN_PDF_PEDIDO_WEB','Menu de Pedido Web', '247');  /*NUEVO*/
INSERT INTO `funcion` (`c_funcion`, d_funcion,`id_tipo_funcion`) VALUES ('BTN_PDF_PEDIDO_WEB_ETIQUETA_DESPACHO','Menu de Pedido Web', '247');  /*NUEVO*/



/* No se deja null en el detalle del 202 por q con el null no se puede filtrar de esta manera puedo filtrar manuales, webs, o ambos*/
INSERT INTO `detalle_tipo_comprobante` (id,id_tipo_comprobante,`d_detalle_tipo_comprobante`) VALUES (78,202, 'Pedidos Interno Manual '); 
INSERT INTO `detalle_tipo_comprobante` (id,id_tipo_comprobante,`d_detalle_tipo_comprobante`) VALUES (79,202, 'Pedidos Web '); 

/*VALMEC LOTES */
ALTER TABLE `lote` ADD COLUMN `tiene_certificado` TINYINT(4) NULL 


/*Parama nuevo menu de  CALIBRACIONES   VALMEC ("mantenimiento" programdo preveentivo)*/
UPDATE detalle_tipo_comprobante 
SET `d_detalle_tipo_comprobante` = 'Calibración / Mantenimiento Cero Horas' , `descripcion_accion` = 'Calibración / Mantenimiento Cero Horas' 
WHERE `id` = '67'; 

/*VALMEC Periodo de creticidad*/
CREATE TABLE `periodo`( `id` INT NOT NULL AUTO_INCREMENT, `d_periodo` VARCHAR(80), `fecha_rel_inicio` DATETIME, `fecha_rel_fin` DATETIME, PRIMARY KEY (`id`) ); 

UPDATE `periodo` SET `fecha_rel_inicio` = '2018-01-01 01:00:00' WHERE `id` = '1'; 
UPDATE `periodo` SET `fecha_rel_fin` = '2018-01-02 01:00:00' WHERE `id` = '1'; 
INSERT INTO `periodo` (`d_periodo`) VALUES ('semanas'); 
UPDATE `periodo` SET `fecha_rel_inicio` = '2018-01-01 01:00:00' , `fecha_rel_fin` = '2018-01-02 01:00:00' WHERE `id` = '2'; 
INSERT INTO `periodo` (`d_periodo`, `fecha_rel_inicio`) VALUES ('quincenal', '2018-01-01 01:00:00'); 
UPDATE `periodo` SET `fecha_rel_fin` = '2018-01-15 01:00:00' WHERE `id` = '3'; 
INSERT INTO `periodo` (`d_periodo`) VALUES ('mesual'); 
UPDATE `periodo` SET `d_periodo` = 'semanal' WHERE `id` = '2'; 
UPDATE `periodo` SET `d_periodo` = 'diario' WHERE `id` = '1'; 
UPDATE `periodo` SET `fecha_rel_inicio` = '2018-01-01 01:00:00' , `fecha_rel_fin` = '2018-02-01 01:00:00' WHERE `id` = '4'; 
INSERT INTO `periodo` (`d_periodo`, `fecha_rel_inicio`, `fecha_rel_fin`) VALUES ('bimestral', '2018-01-01 01:00:00', '2018-03-01 01:00:00'); 
INSERT INTO `periodo` (`d_periodo`, `fecha_rel_inicio`, `fecha_rel_fin`) VALUES ('semestral', '2018-01-01 01:00:00', '2018-07-01 01:00:00'); 
INSERT INTO `periodo` (`d_periodo`, `fecha_rel_inicio`, `fecha_rel_fin`) VALUES ('anual', '2018-01-01 01:00:00', '2019-01-01 01:00:00'); 

ALTER TABLE `producto` ADD COLUMN `id_periodo` INT(11) NULL , ADD COLUMN `cantidad_por_periodo` DECIMAL(10,4) NULL; 