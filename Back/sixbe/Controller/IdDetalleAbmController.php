<?php
    
    /**
    * Superclase que implementa el controlador para un abm del tipo id Detalle
    */
    
    class IdDetalleAbmController extends AppController {
        
        public $helpers = array ('Session', 'Paginator', 'Js');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
        
        public $name = 'NombreController';
        public $model = 'NombreModel';
        public $idField = 'id';
        public $idLabel = 'Id';
        public $detailField = 'descripcion';
        public $detailLabel = 'Descripci&oacute;n';
        public $indexTitle = 'Listado de XXXX';
        public $formAddTitle = 'Alta de XXXX';
        public $formEditTitle = 'Modificaci&oacute;n de XXXX';
        public $formViewTitle = 'Consulta de XXXX';
        public $detailValidationMessage = "Debe ingresar una descripci&oacute;n";
        public $addSuccessMessage = "El XXXX ha sido creado exitosamente";
        public $addErrorMessage = "Ha ocurrido un error, el XXXX no ha podido crearse.";
        public $editSuccessMessage = "El XXXX ha sido modificado exitosamente.";
        public $editErrorMessage = "Ha ocurrido un error, el XXXX no ha podido modificarse.";
        public $deleteSuccessMessage = "El XXXX ha sido eliminado exitosamente.";
        public $unbindModel = array();

        
        public function index() {

            if($this->request->is('ajax'))
                $this->layout = 'ajax';

            $this->loadModel($this->model);
            
            $this->unBindHasManyFromModel();
            
            $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);

            //Recuperacion de Filtros
            $detalle = $this->getFromRequestOrSession($this->model.'.'.$this->detailField);
            
            $conditions = array(); 

            if ($detalle != "") {                                 
                $conditions = array($this->model.'.'.$this->detailField.' LIKE' => '%' . $detalle . '%');
            }

            $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
                'conditions' => $conditions,
                'limit' => 10,
                'page' => $this->getPageNum()
            );


            App::import('Lib', 'FormBuilder');
            $formBuilder = new FormBuilder();
            $formBuilder->setDataListado($this, $this->indexTitle, $this->indexTitle, $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
            
            //Filters
            $formBuilder->addFilterBeginRow();
            $formBuilder->addFilterInput($this->detailField, $this->detailLabel, array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $detalle));
            $formBuilder->addFilterEndRow();

            //Headers                                     
            $this->addIndexHeaders($formBuilder);
            

            //Fields
            $this->addIndexFields($formBuilder);
            

            $this->set('abm',$formBuilder);
            $this->render('/FormBuilder/index');
        }
        
        public function addIndexHeaders($formBuilder){
            
            $formBuilder->addHeader($this->idLabel, $this->model . '.' . $this->idField, "10%");
            $formBuilder->addHeader($this->detailLabel, $this->model . '.' . $this->detailField, "");
            
        }
        
        public function addIndexFields($formBuilder){
         
            $formBuilder->addField($this->model, $this->idField);
            $formBuilder->addField($this->model, $this->detailField);
            
        }


        public function abm($mode, $id = null) {


            if ($mode != "A" && $mode != "M" && $mode != "C")
                throw new MethodNotAllowedException();

            $this->layout = 'ajax';
            $this->loadModel($this->model);
            if ($mode != "A"){
                $this->{$this->model}->id = $id;
                $this->unBindHasManyFromModel();
                $this->request->data = $this->{$this->model}->read();
            }


            App::import('Lib', 'FormBuilder');
            $formBuilder = new FormBuilder();

            //Configuracion del Formulario
            $formBuilder->setController($this);
            $formBuilder->setModelName($this->model);
            $formBuilder->setControllerName($this->name);

            if ($mode == "A")
                $formBuilder->setTituloForm($this->formAddTitle);
            elseif ($mode == "M")
                $formBuilder->setTituloForm($this->formEditTitle);
            else
                $formBuilder->setTituloForm($this->formViewTitle);

            //Form
            $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

            //Fields
            $this->addFormFields($formBuilder);
            
            $script = "
            function validateForm(){

            Validator.clearValidationMsgs('validationMsg_');

            var form = '" . $this->model . "AbmForm';

            var validator = new Validator(form);

            " . $this->getFormValidations() . 
              
            "
            if(!validator.isAllValid()){
            validator.showValidations('', 'validationMsg_');
            return false;   
            }

            return true;

            } 


            ";

            $formBuilder->addFormCustomScript($script);

            $formBuilder->setFormValidationFunction('validateForm()');

            $this->set('abm',$formBuilder);
            $this->set('mode', $mode);
            $this->set('id', $id);
            $this->render('/FormBuilder/abm');    
        }
        
        public function addFormFields($formBuilder){
            
            $formBuilder->addFormInput($this->detailField, $this->detailLabel, array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));  

            
        }
        
        public function getFormValidations(){
            
            return "validator.validateRequired('" . Inflector::camelize($this->model) . Inflector::camelize($this->detailField) ."', '". $this->detailValidationMessage ."');";
            
        }


        public function view($id) {        
            $this->redirect(array('action' => 'abm', 'C', $id)); 
        }

        /**
        * @secured(READONLY_PROTECTED)
        */
        public function add() {
            if ($this->request->is('post')){
                $this->loadModel($this->model);
                if ($this->{$this->model}->save($this->request->data))
                    $this->Session->setFlash($this->addSuccessMessage, 'success'); 
                else
                    $this->Session->setFlash($this->addErrorMessage, 'error');

                $this->redirect(array('action' => 'index'));
            } 

            $this->redirect(array('action' => 'abm', 'A'));                   
        }

        function edit($id) {
            if (!$this->request->is('get')){
                $this->loadModel($this->model);
                $this->{$this->model}->id = $id;
                if ($this->{$this->model}->save($this->request->data)){
                    $this->Session->setFlash($this->editSuccessMessage, 'success');
                    $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                }else{
                    $this->Session->setFlash($this->editErrorMessage, 'error');
                    $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                }
            } 


            $this->redirect(array('action' => 'abm', 'M', $id));
        }

        /**
        * @secured(READONLY_PROTECTED)
        */
        function delete($id) {
            
            $this->loadModel($this->model);

            try{

                if ($this->{$this->model}->delete($id))
                     $this->Session->setFlash($this->deleteSuccessMessage, 'success');
                else
                    throw new Exception();

            }catch(ChildRecordException $ex){ 
                $this->Session->setFlash($ex->getMessage(), 'error');
            }
            
            
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
        }



    }
?>