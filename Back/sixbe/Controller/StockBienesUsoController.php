<?php


App::uses('StockProductosController', 'Controller');

  /**
    * @secured(CONSULTA_STOCK)
  */
class StockBienesUsoController extends StockProductosController {
	
	
    public $name = 'StockBienesUso';
    public $model = 'StockArticulo';
    public $tipo_producto = EnumProductoTipo::BIENUSO;
    public $padre = "Articulo";
    public $d_padre = "d_producto";
    public $deposito_fijo_principal = 4;

   
    
    
     /**
    * @secured(CONSULTA_STOCK)
    */
    public function index(){
        
        parent::index();
    }
    
    /**
    * @secured(CONSULTA_STOCK)
    */
    public function getModel($vista='default'){
        
        parent::getModel($vista);
    }
}


?>