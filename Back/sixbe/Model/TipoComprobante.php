<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class TipoComprobante extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'tipo_comprobante';
    public $actsAs = array('Containable');
    
 

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'd_comprobante' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar una descripci&oacute;n.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
 

 
 
/**
 * hasMany associations
 *
 * @var array
 */
	/*public $hasMany = array(
		'Region' => array(
			'className' => 'Region',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public $childRecordMessage = "Existe por lo menos una regi&oacute;n, predio, establecimiento o usuario relacionado/a con la jurisdicci&oacute;n.";
    
    */
    
      public $belongsTo = array(
        'DepositoOrigen' => array(
            'className' => 'DepositoOrigen',
            'foreignKey' => 'id_deposito_origen',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'DepositoDestino' => array(
            'className' => 'DepositoDestino',
            'foreignKey' => 'id_deposito_destino',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
		 'MovimientoTipo' => array(
            'className' => 'MovimientoTipo',
            'foreignKey' => 'id_tipo_movimiento',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
		'Producto' => array(
            'className' => 'Producto',
            'foreignKey' => 'id_producto_default',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
		'Persona' => array(
            'className' => 'Persona',
            'foreignKey' => 'id_persona_default',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Sistema' => array(
            'className' => 'Sistema',
            'foreignKey' => 'id_sistema',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'Impuesto' => array(
            'className' => 'Impuesto',
            'foreignKey' => 'id_impuesto',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'EstadoAsientoDefault' => array(
            'className' => 'EstadoAsiento',
            'foreignKey' => 'id_estado_asiento_default',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
      	'CuentaBancaria' => array(//permite asociar un comprobante a una cuenta bancaria para brindar informacion en el compronbante sobre esa cuenta.
      				'className' => 'CuentaBancaria',
      				'foreignKey' => 'id_cuenta_bancaria',
      				'conditions' => '',
      				'fields' => '',
      				'order' => ''
      		)
        );
        
  public $hasMany = array(
        'DetalleTipoComprobante' => array(
            'className' => 'DetalleTipoComprobante',
            'foreignKey' => 'id_tipo_comprobante',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
  		
  		'TipoComprobantePersona' => array(
  				'className' => 'TipoComprobantePersona',
  				'foreignKey' => 'id_tipo_comprobante',
  				'dependent' => false,
  				'conditions' => '',
  				'fields' => '',
  				'order' => '',
  				'limit' => '',
  				'offset' => '',
  				'exclusive' => '',
  				'finderQuery' => '',
  				'counterQuery' => ''
  		),
        );   
       public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
        
			 $this->virtual_fields_view = array(
			 
			 "d_deposito_origen"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			 "d_deposito_destino"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			 "d_producto_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			 "codigo_producto_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			 "razon_social_persona_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			 "codigo_persona_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			 "cuit_persona_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			 "d_movimiento_tipo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			 "tel_persona_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			 "costo_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			 "id_moneda_costo_default"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
			 "id_unidad_default"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
			 "id_iva_default"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
			 "id_tipo_iva_default"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
			 "id_moneda_default"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
			 "id_condicion_pago_default"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
			 "tiene_cuenta_corriente_default"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
			 "calle_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			 "numero_calle_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			 "localidad_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			 "ciudad_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			 "d_provincia_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			 "id_lista_precio_default"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null)
			 );
         
       }     
               
        
        
	 function tiene_iva($id_tipo_comprobante){
		$tipo_comprobante = $this->find('first', array(
													'conditions' => array('TipoComprobante.id' => $id_tipo_comprobante),
													'contain' =>false
													));
													
		return $tipo_comprobante["TipoComprobante"]["requiere_iva"];
	 }
 
 function GeneraAsiento($id_tipo_comprobante){
     
     
     $tipo_comprobante = $this->find('first', array(
                                                'conditions' => array('TipoComprobante.id' => $id_tipo_comprobante),
                                                'contain' =>false
                                                ));
                                                
       return $tipo_comprobante["TipoComprobante"]["genera_asiento"];
 }
 
 function getSistema($id_tipo_comprobante){
     
     
     $tipo_comprobante = $this->find('first', array(
                                                'conditions' => array('TipoComprobante.id' => $id_tipo_comprobante),
                                                'contain' =>false
                                                ));
                                                
       return $tipo_comprobante["TipoComprobante"]["id_sistema"];
 }
 
 function requiereContador($id_tipo_comprobante){
     
     
     $tipo_comprobante = $this->find('first', array(
                                                'conditions' => array('TipoComprobante.id' => $id_tipo_comprobante),
                                                'contain' =>false
                                                ));
                                                
       return $tipo_comprobante["TipoComprobante"]["requiere_contador"];
 }
 
 function AfectaStock($id_tipo_comprobante){
     
     
     $tipo_comprobante = $this->find('first', array(
                                                'conditions' => array('TipoComprobante.id' => $id_tipo_comprobante),
                                                'contain' =>false
                                                ));
                                                
       return $tipo_comprobante["TipoComprobante"]["afecta_stock"];
 }
 
 
 function getRetencionesPropias(){
     
     
     $tipo_comprobante = $this->find('all', array(
     		'conditions' => array('TipoComprobante.id_sistema' => EnumSistema::COMPRAS,'TipoComprobante.id'=>array(EnumTipoComprobante::RetencionIIBB,EnumTipoComprobante::RetencionIIBB,EnumTipoComprobante::RetencionIVA,EnumTipoComprobante::RetencionSUSS,EnumTipoComprobante::RetencionGANANCIA)),
                                                'contain' =>false
                                                ));
      $tipos_comprobante = array();
      
      foreach($tipo_comprobante as $tipo){
          
          array_push($tipos_comprobante,$tipo["TipoComprobante"]["id"]);
      }
                                                
       return $tipos_comprobante;
 }
 
 
 function getEstadoAsientoDefault($id_tipo_comprobante){
     
     
     $tipo_comprobante = $this->find('first', array(
                                                'conditions' => array('TipoComprobante.id' => $id_tipo_comprobante),
                                                'contain' =>false
                                                ));
                                                
       return $tipo_comprobante["TipoComprobante"]["id_estado_asiento_default"];//para parametrizar el estado de un asiento por default al ser creado
 }
 
 /*
 function permiteModificarNroComprobante($id_tipo_comprobante){//NO SE USA MAS
     
     
     $tipo_comprobante = $this->find('first', array(
                                                'conditions' => array('TipoComprobante.id' => $id_tipo_comprobante),
                                                'contain' =>false
                                                ));
                                                
       return $tipo_comprobante["TipoComprobante"]["permite_modificar_nro_comprobante"];
       
       
 }*/
 
 
 function getTipoComportamientoStock($id_tipo_comprobante){
 	
 	
 	$tipo_comprobante = $this->find('first', array(
 			'conditions' => array('TipoComprobante.id' => $id_tipo_comprobante),
 			'contain' =>false
 	));
 	
 	return $tipo_comprobante["TipoComprobante"]["tipo_comportamiento_stock"];//para parametrizar el estado de un asiento por default al ser creado
 }
 
 
 function getEnviarMailAlConformar($id_tipo_comprobante){
 	
 	
 	$tipo_comprobante = $this->find('first', array(
 			'conditions' => array('TipoComprobante.id' => $id_tipo_comprobante),
 			'contain' =>false
 	));
 	
 	return $tipo_comprobante["TipoComprobante"]["envia_mail_al_conformar"];//para parametrizar el estado de un asiento por default al ser creado
 }
 
 
 function getAdjuntaPdfMail($id_tipo_comprobante){
 	
 	
 	$tipo_comprobante = $this->find('first', array(
 			'conditions' => array('TipoComprobante.id' => $id_tipo_comprobante),
 			'contain' =>false
 	));
 	
 	return $tipo_comprobante["TipoComprobante"]["adjunta_pdf_mail"];//para parametrizar el estado de un asiento por default al ser creado
 }
 
 
 
 
 function getCuentaBancaria($id_tipo_comprobante){
 	
 	
 	$tipo_comprobante = $this->find('first', array(
 			'conditions' => array('TipoComprobante.id' => $id_tipo_comprobante),
 			'contain' =>array("CuentaBancaria")
 	));
 	
 	return $tipo_comprobante["CuentaBancaria"];
 }
 
 public function getTiposComprobanteController(&$tipos_comprobante){
 	
 	
 	$comprobante_class = ClassRegistry::init('Comprobante');

 	
 	
 

 	$controllers = App::objects('controller');
 	
 	$array_tipo_comprobante = array();
 	$array_id_tipo_comprobante = array();
 	
 	foreach($controllers as $controller){
 		
 		$num_chars= strlen($controller);
 		$controller_name = substr($controller,0,$num_chars-10);
 		
 		//$controller_excluir = array("TestControladores","Auditorias","PersonasCategorias","App");
 		$array = array();
 		$array[$controller_name] = $comprobante_class->getTiposComprobanteController($controller_name);
 		
 		if(count($array[$controller_name])>0){
 			array_push( $array_tipo_comprobante, $array );
 			$array_id_tipo_comprobante = array_merge($array_id_tipo_comprobante,$array[$controller_name]);
 		}
 		
 		
 		
 		
 	}
 	
 	$tipos_comprobante = $this->find('all',array('conditions'=>array('TipoComprobante.id'=>$array_id_tipo_comprobante),'contain'=>false));
 	
 	
 	
 	foreach($array_tipo_comprobante as $key=>&$tipo_comprobante){
 		
 		foreach($tipo_comprobante as $key2=>&$id_tipo_comprobante){
 			
 			
 			foreach($id_tipo_comprobante as $key3=>&$id_tipo_comprobante_seleccionado){
 				
 				foreach($tipos_comprobante as $tipo_comprobante_db){
 					
 					if($tipo_comprobante_db["TipoComprobante"]["id"] == $id_tipo_comprobante_seleccionado )
 						$id_tipo_comprobante[$key3] =$tipo_comprobante_db;
 						//$id_tipo_comprobante[0] = $tipo_comprobante;
 						
 				}
 			}
 			
 			
 		}
 		
 		
 	}
 	return $array_tipo_comprobante;
 	
 }
 
 
 
 
}
