<?php

class ProvinciaPicker extends EntityPicker{
    
    public $model = "Provincia";
    
    public $pickerTitle = "Selección de Provincia";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Nombre", "field" => "d_provincia", "width" => "90%", "keyOrder" => "d_provincia")
                                );

    public function getPickerConditions($busqueda){
        
        $pickerConditions = array('Provincia.d_provincia LIKE' => '%' . $busqueda['bus_unico'] . '%');
        
        return $pickerConditions;
        
        
    }
  
    /*public function getPickerInitialFilter(){
        
        //Filtra siempre por la Jurisdiccion del usuario si corresponde
        return DataFilter::getJurisdiccionPickerFilter();
        
    }*/
  
  
}