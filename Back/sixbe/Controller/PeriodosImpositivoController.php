<?php

 
    class PeriodosImpositivoController extends AppController {
        public $name = 'PeriodosImpositivo';
        public $model = 'PeriodoImpositivo';
        public $helpers = array ('Session', 'Paginator', 'Js');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    
    
       /**
    * @secured(CONSULTA_PERIODO_IMPOSITIVO)
    */
    public function index() {

            if($this->request->is('ajax'))
                $this->layout = 'ajax';

            $this->loadModel($this->model);
            $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
            $cerrado = $this->getFromRequestOrSession($this->model.'.cerrado');
            $disponible = $this->getFromRequestOrSession($this->model.'.disponible');
            $id_ejercicio_contable = $this->getFromRequestOrSession($this->model.'.id_ejercicio_contable');
            
            
            $conditions = array(); 
                                
            if($cerrado!="")
                array_push($conditions, array($this->model.'.cerrado =' => $cerrado));
                
            if($disponible!="")
                array_push($conditions, array($this->model.'.disponible =' => $disponible));
            
            if($id_ejercicio_contable!="")
                array_push($conditions, array($this->model.'.id_ejercicio_contable =' => $id_ejercicio_contable));
                
                

            
            
            

            $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
              //  'contain'=>false,
                'conditions' => $conditions,
                'limit' => 12,
                'page' => $this->getPageNum(),
                'order'=> 'PeriodoImpositivo.id DESC'
            );

            if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de PeriodoContableCausas', 'Datos de los Tipo de Impuesto', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));

                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_PeriodoContable', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();

                //Headers
                $formBuilder->addHeader('Id', 'PeriodoContable.id', "10%");
                $formBuilder->addHeader('Nombre', 'PeriodoContable.d_PeriodoContable_causa', "50%");
                $formBuilder->addHeader('Nombre', 'PeriodoContable.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_PeriodoContableCausa');
                $formBuilder->addField($this->model, 'cotizacion');

                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
            }
            else
            { // vista json
                $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
                $periodo_impositivo = $this->PeriodoImpositivo->getPeriodoActual();
                
                foreach($data as &$dato){
                    

                    if($dato["PeriodoImpositivo"]["id"] == $periodo_impositivo["id"])
					{
                        $dato["PeriodoImpositivo"]["es_actual"] = "1";
						$dato["PeriodoImpositivo"]["actual"] = "Si";
					}
                    else
					{
                        $dato["PeriodoImpositivo"]["es_actual"] = "0";
						$dato["PeriodoImpositivo"]["actual"] = "No";
					}
                        
                     if(isset($dato["EjercicioContable"]))
                        $dato["PeriodoImpositivo"]["d_ejercicio_contable"] = $dato["EjercicioContable"]["d_ejercicio"];
                        
                        unset($dato["EjercicioContable"]);
                     
                
                }
                
               
                $page_count = $this->params['paging'][$this->model]['pageCount'];

               

                $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "list",
                    "content" => $data,
                    "page_count" =>$page_count
                );
                $this->set($output);
                $this->set("_serialize", array("status", "message","page_count", "content"));

            }
        }


        public function abm($mode, $id = null) {


            if ($mode != "A" && $mode != "M" && $mode != "C")
                throw new MethodNotAllowedException();

            $this->layout = 'ajax';
            $this->loadModel($this->model);
            if ($mode != "A"){
                $this->PeriodoContable->id = $id;
                $this->request->data = $this->PeriodoContable->read();
            }


            App::import('Lib', 'FormBuilder');
            $formBuilder = new FormBuilder();

            //Configuracion del Formulario
            $formBuilder->setController($this);
            $formBuilder->setModelName($this->model);
            $formBuilder->setControllerName($this->name);
            $formBuilder->setTitulo('PeriodoContableCausa');

            if ($mode == "A")
                $formBuilder->setTituloForm('Alta de PeriodoContableCausa');
            elseif ($mode == "M")
                $formBuilder->setTituloForm('Modificaci&oacute;n de PeriodoContableCausa');
            else
                $formBuilder->setTituloForm('Consulta de PeriodoContableCausa');

            //Form
            $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

            //Fields





            $formBuilder->addFormInput('d_PeriodoContableCausa', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
            $formBuilder->addFormInput('cotizacion', 'Cotizaci&oacute;n', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));    


            $script = "
            function validateForm(){

            Validator.clearValidationMsgs('validationMsg_');

            var form = 'PeriodoContableCausaAbmForm';

            var validator = new Validator(form);

            validator.validateRequired('PeriodoContableCausaDPeriodoContableCausa', 'Debe ingresar un nombre');
            validator.validateRequired('PeriodoContableCausaCotizacion', 'Debe ingresar una cotizaci&oacute;n');


            if(!validator.isAllValid()){
            validator.showValidations('', 'validationMsg_');
            return false;   
            }

            return true;

            } 


            ";

            $formBuilder->addFormCustomScript($script);

            $formBuilder->setFormValidationFunction('validateForm()');

            $this->set('abm',$formBuilder);
            $this->set('mode', $mode);
            $this->set('id', $id);
            $this->render('/FormBuilder/abm');    
        }



        /**
        * @secured(ADD_PERIODO_CONTABLE)
        */
        public function add() {
            if ($this->request->is('post')){
                $this->loadModel($this->model);
                $id_periodo_impositivo = '';

                try{
                    if ($this->PeriodoImpositivo->saveAll($this->request->data, array('deep' => true))){
                        $mensaje = "El Periodo Impositivo ha sido creado exitosamente";
                        $tipo = EnumError::SUCCESS;
                        $id_periodo_impositivo = $this->{$this->model}->id;

                    }else{
                        $errores = $this->{$this->model}->validationErrors;
                        $errores_string = "";
                        foreach ($errores as $error){
                            $errores_string.= "&bull; ".$error[0]."\n";

                        }
                        $mensaje = $errores_string;
                        $tipo = EnumError::ERROR; 

                    }
                }catch(Exception $e){

                    $mensaje = "Ha ocurrido un error,el Periodo Impositivo no ha podido ser creado.".$e->getMessage();
                    $tipo = EnumError::ERROR;
                }
                $output = array(
                    "status" => $tipo,
                    "message" => $mensaje,
                    "content" => "",
                    "id_add"=>$id_periodo_impositivo
                );
                //si es json muestro esto
                if($this->RequestHandler->ext == 'json'){ 
                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content","id_add"));
                }else{

                    $this->Session->setFlash($mensaje, $tipo);
                    $this->redirect(array('action' => 'index'));
                }     
            }

            //si no es un post y no es json
            if($this->RequestHandler->ext != 'json')
                $this->redirect(array('action' => 'abm', 'A'));   
        }

        /**
        * @secured(MODIFICACION_PERIODO_CONTABLE)
        */
        public function edit($id) {


            if (!$this->request->is('get')){
                
                
                
                
                $this->loadModel($this->model);
                $this->{$this->model}->id = $id;

                try{ 
                    
                    
                if($this->PeriodoImpositivo->getEstadoGrabado($id) != 1 ){
                    
                    if($this->request->data["PeriodoImpositivo"]["cerrado"]== 1)
                    {
                        
                      $this->request->data["PeriodoImpositivo"]["fecha_cierre"] = date("Y-m-d");
                    }
                    
                    if ($this->{$this->model}->saveAll($this->request->data)){
                        $mensaje =  "El Periodo Impositivo ha sido modificado exitosamente";
                        $status = EnumError::SUCCESS;


                    }else{   //si hubo error recupero los errores de los models

                        $errores = $this->{$this->model}->validationErrors;
                        $errores_string = "";
                        foreach ($errores as $error){ //recorro los errores y armo el mensaje
                            $errores_string.= "&bull; ".$error[0]."\n";

                        }
                        $mensaje = $errores_string;
                        $status = EnumError::ERROR; 
                    }

                }else{
                     $mensaje = "El periodo se encuentra cerrado y no puede editarse";
                     $status = EnumError::ERROR; 
                }
                    
                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 

                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));

                    } 
                }catch(Exception $e){
                    $this->Session->setFlash('Ha ocurrido un error, el Periodo Impositivo no ha podido modificarse.', 'error');

                }

            }else{ //si me pide algun dato me debe mandar el id
                if($this->RequestHandler->ext == 'json'){ 

                    $this->{$this->model}->id = $id;
                    //$this->{$this->model}->contain('Provincia','Pais');
                    $this->request->data = $this->{$this->model}->read();          

                    $output = array(
                        "status" =>EnumError::SUCCESS,
                        "message" => "list",
                        "content" => $this->request->data
                    );   

                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content")); 
                }else{

                    $this->redirect(array('action' => 'abm', 'M', $id));

                }


            } 

            if($this->RequestHandler->ext != 'json')
                $this->redirect(array('action' => 'abm', 'M', $id));
        }

        /**
        * @secured(BAJA_PERIODO_CONTABLE)
        */
        function delete($id) {
            $this->loadModel($this->model);
            $mensaje = "";
            $status = "";
            $this->PeriodoImpositivo->id = $id;

            try{
                if ($this->PeriodoImpositivo->delete() ) {

                    //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');

                    $status = EnumError::SUCCESS;
                    $mensaje = "El Periodo Impositivo ha sido eliminado exitosamente.";
                    $output = array(
                        "status" => $status,
                        "message" => $mensaje,
                        "content" => ""
                    ); 

                }

                else
                    throw new Exception();

            }catch(Exception $ex){ 
                //$this->Session->setFlash($ex->getMessage(), 'error');

                $status = EnumError::ERROR;
                $mensaje = $ex->getMessage();
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
            }

            if($this->RequestHandler->ext == 'json'){
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));

            }else{
                $this->Session->setFlash($mensaje, $status);
                $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            }
        }

        /**
        * @secured(CONSULTA_PERIODO_IMPOSITIVO)
        */
        public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }

        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);

        }

    }
?>