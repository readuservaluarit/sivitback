<?php
App::uses('AppModel', 'Model');



class CsvImportConciliacion extends AppModel {


	public $useTable = 'csv_import_conciliacion';
    public $actsAs = array('Containable');

    
    public function formatearFechas(&$csv_import_conciliacion){
      
      
        if($csv_import_conciliacion["CsvImportConciliacion"]["fecha"]!=null)
            $csv_import_conciliacion["CsvImportConciliacion"]["fecha"] = date("d-m-Y", strtotime($csv_import_conciliacion["CsvImportConciliacion"]["fecha"]));
            
    } 
    
}
