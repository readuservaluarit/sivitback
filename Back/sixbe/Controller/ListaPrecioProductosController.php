<?php 


class ListaPrecioProductosController extends AppController{
    
    public $name = EnumController::ListaPrecioProductos;
    public $model = 'ListaPrecioProducto';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    private function RecuperoFiltros(&$conditions)
	{	
    	$conditions = array();
    	$id_lista_precio = 	$this->getFromRequestOrSession('ListaPrecioProducto.id_lista_precio');
    	$id_producto = 		$this->getFromRequestOrSession('ListaPrecioProducto.id_producto');
    	$codigo_producto = 	$this->getFromRequestOrSession('ListaPrecioProducto.codigo');
    	$d_producto = 		$this->getFromRequestOrSession('ListaPrecioProducto.d_producto');
    	$id_sistema = 		$this->getFromRequestOrSession('ListaPrecioProducto.id_sistema');
    	//$id_producto_clasificacion = 		$this->getFromRequestOrSession('ListaPrecioProducto.id_producto_clasificacion');
    	
    	$id_destino_producto = array();
    	
    	if($id_sistema == EnumSistema::VENTAS)
    		$id_destino_producto = array(EnumDestinoProducto::Venta,EnumDestinoProducto::ComprayVenta);
    	elseif($id_sistema == EnumSistema::COMPRAS) 
    		$id_destino_producto = array(EnumDestinoProducto::Compra,EnumDestinoProducto::ComprayVenta);

  
    	if(count($id_destino_producto)>0) 
    		array_push($conditions, array('ListaPrecioProducto.id_destino_producto' => $id_destino_producto)); 
    		
    		
    	if($id_sistema!="")
    		array_push($conditions, array($this->model.'.id_sistema' => $id_sistema)); 
    	
    	if($id_lista_precio!=""){
    		array_push($conditions, array($this->model.'.id_lista_precio' => $id_lista_precio)); 
    		$this->loadModel("ListaPrecio");
    		array_push($this->array_filter_names,array("Lista de Precio:"=>$this->ListaPrecio->getName($id_lista_precio)));
    	}
    	
    	if($id_producto!="")
    		array_push($conditions, array('ListaPrecioProducto.id_producto' => $id_producto)); 
    	
    	if($codigo_producto!="")
    		array_push($conditions, array('ListaPrecioProducto.codigo LIKE ' => '%'.$codigo_producto.'%')); 
    	
    	if($d_producto!="")
    			array_push($conditions, array('ListaPrecioProducto.d_producto LIKE ' => '%'.$d_producto.'%'));
    	
    			
     //SIEMPRE REAL
       	array_push($conditions, array('ListaPrecioProducto.id_producto_clasificacion' => EnumProductoClasificacion::Articulo_REAL)); 
       	
       	
    	
    }
     /**
    * @secured(CONSULTA_LISTA_PRECIO)
    */
    public function index() {
        
		

		
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
        
        
            
       //$this->paginado = 0;
            
        $this->loadModel($this->model);
        $this->loadModel("DatoEmpresa");
        $this->PaginatorModificado->settings = array('limit' => 50, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $this->RecuperoFiltros($conditions); 
        
        //$this->paginado = 0;

        
		$this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,

            'conditions' => $conditions,
            'limit' => $this->numrecords, //BACK- Ver por q no anda esto 
			//'limit' => 5000,//desde .net esta preparado para usarse con canche siempre
            'page' => $this->getPageNum(),
            'contain' => false,
            //'order'=>$this->{$this->model}->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_'))
        );
        
	if($this->RequestHandler->ext != 'json')
	{  
		//vista formBuilder
    } else{ // vista json
			$this->PaginatorModificado->settings = $this->paginate; 
			$data = $this->PaginatorModificado->paginate($this->model);
			$page_count = $this->params['paging'][$this->model]['pageCount'];
       
        }
        
        
        
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));  
     }
    //fin vista json

     /**
    * @secured(CONSULTA_LISTA_PRECIO)
    */
    public function getModel($vista='default')
    {    
        $model = parent::getModelCamposDefault();
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
    }
    
    private function editforView($model,$vista)
    {  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
      $this->set('model',$model);
      Configure::write('debug',0);
      $this->render($vista);
    }
    
    
    
    /**
     * @secured(BAJA_LISTA_PRECIO)
     */
    function delete($id) {
    	$this->loadModel($this->model);
    	$mensaje = "";
    	$status = "";
    	$this->{$this->model}->id = $id;
    	
    	try{
    		if ($this->{$this->model}->delete() ) {
    			
    			//$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
    			
    			$status = EnumError::SUCCESS;
    			$mensaje = "El Item ha sido eliminado exitosamente de la lista.";
    			$output = array(
    					"status" => $status,
    					"message" => $mensaje,
    					"content" => ""
    			);
    			
    		}
    		
    		else
    			throw new Exception("No puede eliminarse el item");
    			
    	}catch(Exception $ex){
    		//$this->Session->setFlash($ex->getMessage(), 'error');
    		
    		$status = EnumError::ERROR;
    		$mensaje = $ex->getMessage();
    		$output = array(
    				"status" => $status,
    				"message" => $mensaje,
    				"content" => ""
    		);
    	}
    	
    	if($this->RequestHandler->ext == 'json'){
    		$this->set($output);
    		$this->set("_serialize", array("status", "message", "content"));
    		
    	}else{
    		$this->Session->setFlash($mensaje, $status);
    		$this->redirect(array('controller' => $this->name, 'action' => 'index'));
    		
    	}

    }
    
    
    
    
    /**
     * @secured(BAJA_LISTA_PRECIO)
     */
    function deleteAll() {
    	$this->loadModel($this->model);
    	$mensaje = "";
    	$status = "";
    	
    	$this->{$this->model}->setSource($this->{$this->model}->getSource());
        
    	
    	$array_items = $this->getFromRequestOrSession($this->model.'.ids');
    	
    	$id_a_borrar = explode(",", $array_items);
    	
    	
    	if(count($id_a_borrar)>0){
    	try{
    		if ($this->{$this->model}->deleteAll(array($this->model.'.id' => $id_a_borrar), false)) {
    			
    			//$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
    			
    			$status = EnumError::SUCCESS;
    			$mensaje = "El/Los Item/s ha/n sido/s eliminado/s exitosamente de la lista.";
    			$output = array(
    					"status" => $status,
    					"message" => $mensaje,
    					"content" => ""
    			);
    			
    		}
    		
    		else
    			throw new Exception("No puede/n eliminarse el/los item/s");
    			
    	}catch(Exception $ex){
    		//$this->Session->setFlash($ex->getMessage(), 'error');
    		
    		$status = EnumError::ERROR;
    		$mensaje = $ex->getMessage();
    		
    	}
    	
    	}else{
    		$status = EnumError::ERROR;
    		$mensaje = "Debe enviar al menos un item para borrar";
    		
    	}
    	
    	
    	$this->{$this->model}->setSource($this->{$this->model}->getView());
    	
    	
    	
    	if($this->RequestHandler->ext == 'json'){
    		
    		$output = array(
    				"status" => $status,
    				"message" => $mensaje,
    				"content" => ""
    		);
    		$this->set($output);
    		$this->set("_serialize", array("status", "message", "content"));
    		
    	}else{
    		$this->Session->setFlash($mensaje, $status);
    		$this->redirect(array('controller' => $this->name, 'action' => 'index'));
    		
    	}
    	
    }
    
    /**
     * @secured(ADD_PRODUCTO)
     */
    public function add() {
    	if ($this->request->is('post')){
    		$this->loadModel($this->model);
    		$id_cat = '';
    		
    		try{
    			if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
    				$mensaje = "El Articulo ha sido agregado exitosamente";
    				$tipo = EnumError::SUCCESS;
    				$id_cat = $this->{$this->model}->id;
    				
    			}else{
    				$errores = $this->{$this->model}->validationErrors;
    				$errores_string = "";
    				foreach ($errores as $error){
    					$errores_string.= "&bull; ".$error[0]."\n";
    					
    				}
    				$mensaje = $errores_string;
    				$tipo = EnumError::ERROR;
    				
    			}
    		}catch(Exception $e){
    			
    			$mensaje = "Ha ocurrido un error,el Articulo no ha podido ser agregado a la Lista.".$e->getMessage();
    			$tipo = EnumError::ERROR;
    		}
    		$output = array(
    				"status" => $tipo,
    				"message" => $mensaje,
    				"content" => "",
    				"id_add" =>$id_cat
    		);
    		//si es json muestro esto
    		if($this->RequestHandler->ext == 'json'){
    			$this->set($output);
    			$this->set("_serialize", array("status", "message", "content" ,"id_add"));
    		}else{
    			
    			$this->Session->setFlash($mensaje, $tipo);
    			$this->redirect(array('action' => 'index'));
    		}
    		
    	}
    	
    	//si no es un post y no es json
    	if($this->RequestHandler->ext != 'json')
    		$this->redirect(array('action' => 'abm', 'A'));
    		
    		
    }
    
    
    
    
    /**
     * @secured(BTN_EXCEL_LISTAPRECIOS)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Articulos en lista de Precio");
    	
    	
    	
    }
}
?>