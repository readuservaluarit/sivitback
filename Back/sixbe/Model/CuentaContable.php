<?php
App::uses('AppModel', 'Model');
App::uses('AsientoCuentaContable', 'Model');
App::uses('CentroCosto', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class CuentaContable extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'cuenta_contable';

    //public $actsAs = array('Containable');
    public $actsAs = array('Tree');
    
    /*
    public $virtualFields = array(
    'd_codigo_cuenta_contable' => 'CONCAT(CuentaContable.codigo, \'\' , CuentaContable.d_cuenta_contable)'
);
    */
 
 public function __construct($id = false, $table = null, $ds = null) {
    	parent::__construct($id, $table, $ds);
    	$this->virtualFields['d_codigo_cuenta_contable'] = sprintf(
    			'CONCAT(%s.codigo, " ", %s.d_cuenta_contable)', $this->alias, $this->alias
    			
    			);
    }

/**
 * Validation rules
 *
 * @var array
 */
    
   // public $alias = array('CuentaContableCuentaCorriente');
	

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 public $belongsTo = array(
        // 'Estado' => array(
            // 'className' => 'Estado',
            // 'foreignKey' => 'id_estado',
            // 'dependent' => false,
            // 'conditions' => '',
            // 'fields' => '',
  
        // ),
        'TipoCuentaContable' => array(
            'className' => 'TipoCuentaContable',
            'foreignKey' => 'id_tipo_cuenta_contable',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
 		/*
        'CuentaContableParent' => array(
            'className' => 'CuentaContable',
            'foreignKey' => 'parent_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        )
        
        */
);

public $hasMany = array(
        'CuentaContableCentroCosto' => array(
            'className' => 'CuentaContableCentroCosto',
            'foreignKey' => 'id_cuenta_contable',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        );
public function BorroAsociados(&$datos){
    
             
    
                foreach($datos as $key=>$value){
                    
                       unset($datos[$key]["TipoCuentaContable"]);
                       unset($datos[$key]["CuentaContableParent"]);
                       
                       /*inicializo algunos contadores para ciertos reportes*/
                       $datos[$key]["CuentaContable"]["saldo"] = 0.00;
                       $datos[$key]["CuentaContable"]["debe"] = 0.00;
                       $datos[$key]["CuentaContable"]["haber"] = 0.00;
                       $datos[$key]["CuentaContable"]["niveles"] =count($datos[$key]["children"]);    
                       
                       if($datos[$key]["CuentaContable"]["niveles"] >0)
                        $datos[$key]["CuentaContable"]["es_mayor"] = 1;
                       else
                        $datos[$key]["CuentaContable"]["es_mayor"] = 0; 
                        
                       
                       if($datos[$key]["CuentaContable"]["es_mayor"] == 1)
                        $datos[$key]["CuentaContable"]["d_es_mayor"] = "Si";
                       else
                        $datos[$key]["CuentaContable"]["d_es_mayor"] = "No";
                       
                    if ( isset($datos[$key]["children"]) && is_array($datos[$key]["children"])){
                        //si es un array sigo recorriendo
                     
                     $this->BorroAsociados($datos[$key]["children"]);
                     
                     
                   
                  }
         
               }
} 


public function InicializoCentros(&$datos,$centros_costo=array() ){
    
    
    if(count($centros_costo)== 0){
        $centro_costo_obj = ClassRegistry::init('CentroCosto'); 
        $centros_costo = $centro_costo_obj->find('all', array('conditions'=>array('CentroCosto.activo'=>1)));      
    }
                foreach($datos as $key=>$value){
                    
                       unset($datos[$key]["TipoCuentaContable"]);
                       unset($datos[$key]["CuentaContableParent"]);
                       
                       
                       if($centros_costo){//inicializo los centros de costo columnas con 0
                           foreach($centros_costo as $centro_costo){
                               
                             $datos[$key]["CuentaContable"][$centro_costo["CentroCosto"]["id"]] = 0.00;  
                               
                           }
                           
                       }
                       
                       
                       
                       
                       
                       
                       $datos[$key]["CuentaContable"]["niveles"] =count($datos[$key]["children"]);    
                       
                       if($datos[$key]["CuentaContable"]["niveles"] >0)
                        $datos[$key]["CuentaContable"]["es_mayor"] = 1;
                       else
                        $datos[$key]["CuentaContable"]["es_mayor"] = 0; 
                        
                       
                       if($datos[$key]["CuentaContable"]["es_mayor"] == 1)
                        $datos[$key]["CuentaContable"]["d_es_mayor"] = "Si";
                       else
                        $datos[$key]["CuentaContable"]["d_es_mayor"] = "No";
                       
                    if ( isset($datos[$key]["children"]) && is_array($datos[$key]["children"])){
                        //si es un array sigo recorriendo
                     
                     $this->InicializoCentros($datos[$key]["children"],$centros_costo);
                     
                     
                   
                  }
         
               }
} 

function CalculaSaldo(&$datos,&$nodo_padre=0,$fecha_inicio,$fecha_fin,$id_estado_asiento=EnumEstadoAsiento::Registrado,$campo_valor="valor_moneda"){   
             
           
                foreach($datos as $key=>$value){
                    
                    
                      
                    
                    
                    if ( isset($datos[$key]["children"]) && is_array($datos[$key]["children"]) && count($datos[$key]["children"])>0 ){
                     
                     $this->CalculaSaldo($datos[$key]["children"],$datos[$key]["CuentaContable"]["saldo"],$fecha_inicio,$fecha_fin,$id_estado_asiento,$campo_valor); //al pasarle el NODO padre voy acumulando resultados ahi                     
                     $nodo_padre = $nodo_padre + $datos[$key]["CuentaContable"]["saldo"];
                     //$valor = 0;
                       
                   
                  }else{
                      
                      //calcular saldo para las fechas pasadas
                      $saldo = $this->getSaldo($datos[$key]["CuentaContable"]["id"],$fecha_inicio,$fecha_fin,$id_estado_asiento,$campo_valor);
                      
                      
                      $datos[$key]["CuentaContable"]["saldo"] = $saldo;
                      $nodo_padre = $nodo_padre + $saldo;
                      
                      
                   
                  }
                  
                  
                
               }
}





function CalculaDebeHaber(&$datos,&$nodo_padre=0,$fecha_inicio,$fecha_fin,$id_estado_asiento=EnumEstadoAsiento::Registrado,$campo_valor="valor_moneda"){   
             
           
                foreach($datos as $key=>$value){
                    
                    
                      
                    
                    
                    if ( isset($datos[$key]["children"]) && is_array($datos[$key]["children"]) && count($datos[$key]["children"])>0 ){
                     
                     $this->CalculaDebeHaber($datos[$key]["children"],$datos[$key]["CuentaContable"],$fecha_inicio,$fecha_fin,$id_estado_asiento,$campo_valor); //al pasarle el NODO padre voy acumulando resultados ahi                     
                     try{
                     
                     if( isset($nodo_padre["debe"]) && is_numeric($nodo_padre["debe"])){
                         $nodo_padre["debe"] = $nodo_padre["debe"] + $datos[$key]["CuentaContable"]["debe"];
                         $nodo_padre["haber"] = $nodo_padre["haber"] + $datos[$key]["CuentaContable"]["haber"];
                         $nodo_padre["saldo"] = $nodo_padre["debe"] - $nodo_padre["haber"] ;
                        
                     }
                     
                     }catch(Exception $e){
                         
                         $msg = $ex->getMessage() . $ex->getTraceAsString();
                         
                        
                         
                     }
                    // restore_error_handler();  
                   
                  }else{
                      
                      //calcular saldo para las fechas pasadas
                      $debe_haber = $this->getDebeHaber($datos[$key]["CuentaContable"]["id"],$fecha_inicio,$fecha_fin,$id_estado_asiento,$campo_valor);
                      $datos[$key]["CuentaContable"]["debe"] = $debe_haber["debe"];
                      $datos[$key]["CuentaContable"]["haber"] = $debe_haber["haber"];
                      $datos[$key]["CuentaContable"]["saldo"] = $debe_haber["debe"] -  $debe_haber["haber"];
                      
                      
                      if( isset( $nodo_padre["debe"]) && isset($datos[$key]["CuentaContable"]["debe"]) )
                      		$nodo_padre["debe"] = $nodo_padre["debe"] + $datos[$key]["CuentaContable"]["debe"];
                      
                      
                      if( isset( $nodo_padre["haber"]) && isset($datos[$key]["CuentaContable"]["haber"]) )
                      		$nodo_padre["haber"] = $nodo_padre["haber"] + $datos[$key]["CuentaContable"]["haber"];
                      
                      		
                      		
                      if(isset($nodo_padre["debe"]) && isset($nodo_padre["haber"]) )		
                      		$nodo_padre["saldo"] = $nodo_padre["debe"] - $debe_haber["haber"];
                      
                      
                      
                   
                  }
                  
                  
                
               }
}

function CalculaTotalCentro(&$datos,&$nodo_padre=0,$fecha_inicio,$fecha_fin,$id_estado_asiento=EnumEstadoAsiento::Registrado,$campo_valor="valor_moneda",$centros_costo= array()){   
             
           
    
                 if(count($centros_costo)== 0){
                    $centro_costo_obj = ClassRegistry::init('CentroCosto'); 
                    $centros_costo = $centro_costo_obj->find('all', array('conditions'=>array('CentroCosto.activo'=>1)));      
                }
                
                
                foreach($datos as $key=>$value){
                    
                    
                      
                    
                    
                    if ( isset($datos[$key]["children"]) && is_array($datos[$key]["children"]) && count($datos[$key]["children"])>0 ){
                     
                     $this->CalculaTotalCentro($datos[$key]["children"],$datos[$key]["CuentaContable"],$fecha_inicio,$fecha_fin,$id_estado_asiento,$campo_valor,$centros_costo); //al pasarle el NODO padre voy acumulando resultados ahi                     
                     try{
                     
                  
                     
                      if($centros_costo){//inicializo los centros de costo columnas con 0
                           foreach($centros_costo as $centro_costo){
                               
                              $dato = $datos[$key]["CuentaContable"][$centro_costo["CentroCosto"]["id"]];
                              if( isset($nodo_padre[$centro_costo["CentroCosto"]["id"]]) && $nodo_padre[$centro_costo["CentroCosto"]["id"]]>=0 )
                                $nodo_padre[$centro_costo["CentroCosto"]["id"]] = $nodo_padre[$centro_costo["CentroCosto"]["id"]] + $dato;  
                             
                           }
                           
                       }
                     
                     }catch(Exception $e){
                         
                         $msg = $ex->getMessage() . $ex->getTraceAsString();
                         
                        
                         
                     }
                    // restore_error_handler();  
                   
                  }else{
                      
                      //calcular total para cada centro de costo
                      $total_por_centro_costo = $this->getCentroCostoTotalPorCuenta($datos[$key]["CuentaContable"]["id"],$fecha_inicio,$fecha_fin,$id_estado_asiento,$campo_valor,$centros_costo);
                      
                      
                   
                      
                      if($centros_costo){
                           foreach($centros_costo as $centro_costo){
                               
                            $datos[$key]["CuentaContable"][$centro_costo["CentroCosto"]["id"]] = $total_por_centro_costo[$centro_costo["CentroCosto"]["id"]];   
                           }
                      }
                      
                      
                      if($centros_costo){
                           foreach($centros_costo as $centro_costo){
                               
                             $nodo_padre[$centro_costo["CentroCosto"]["id"]] = $nodo_padre[$centro_costo["CentroCosto"]["id"]] + $datos[$key]["CuentaContable"][$centro_costo["CentroCosto"]["id"]];  
                               
                           }
                           
                       }
                      
                      
                      
                   
                  }
                  
                  
                
               }
}


public function getSaldo($id_cuenta_contable,$fecha_inicio,$fecha_fin,$id_estado_asiento,$campo_valor="valor_moneda"){
    
    $asiento_cuenta_contable_obj = ClassRegistry::init('AsientoCuentaContable');
    
    $asiento_cuenta_contable = $asiento_cuenta_contable_obj->find('first', array(
                                                'fields'=>array('IFNULL(SUM(CASE WHEN es_debe=1 THEN monto ELSE (monto*-1) END),0) as saldo'),
                                                'conditions' => array('AsientoCuentaContable.id_cuenta_contable' => $id_cuenta_contable,/*'Asiento.id'=>array(EnumEstadoAsiento::Ingresado,EnumEstadoAsiento::Registrado),*/
                                                                        'Asiento.fecha >='=>$fecha_inicio,   'Asiento.fecha <='=>$fecha_fin,
                                                                        'Asiento.id_estado_asiento'=>$id_estado_asiento
                                                
                                                                        ),
                                                'contain' =>array('Asiento')
                                                ));
                                                
       //echo var_dump($asiento_cuenta_contable);

       return $asiento_cuenta_contable[0]["saldo"];
   
    
       
    
}
public function getDebeHaber($id_cuenta_contable,$fecha_inicio,$fecha_fin,$id_estado_asiento,$campo_valor){
    
    $asiento_cuenta_contable_obj = ClassRegistry::init('AsientoCuentaContable');
    
    $asiento_cuenta_contable = $asiento_cuenta_contable_obj->find('first', array(
                                                'fields'=>array('IFNULL(SUM(CASE WHEN es_debe=1 THEN monto ELSE 0 END),0) as debe','IFNULL(SUM(CASE WHEN es_debe=0 THEN monto ELSE 0 END),0) as haber'),
                                                'conditions' => array('AsientoCuentaContable.id_cuenta_contable' => $id_cuenta_contable,/*'Asiento.id'=>array(EnumEstadoAsiento::Ingresado,EnumEstadoAsiento::Registrado),*/
                                                                        'Asiento.fecha >='=>$fecha_inicio,   'Asiento.fecha <='=>$fecha_fin,
                                                                        'Asiento.id_estado_asiento'=>$id_estado_asiento
                                                
                                                                        ),
                                                'contain' =>array('Asiento')
                                                ));
                                                
       //echo var_dump($asiento_cuenta_contable);

       if(isset($asiento_cuenta_contable[0]))
        return $asiento_cuenta_contable[0];
       else{
           $asiento_cuenta_contable[0]["debe"]=0;
           $asiento_cuenta_contable[0]["haber"]=0;
           return $asiento_cuenta_contable[0];
           
       }
   
    
       
    
}


public function getCentroCostoTotalPorCuenta($id_cuenta_contable,$fecha_inicio,$fecha_fin,$id_estado_asiento,$campo_valor,$centros_costo){
    
    $asiento_cuenta_contable_obj = ClassRegistry::init('AsientoCuentaContable');
    
    
    $array_centros = array();                                         

    if($centros_costo){
       foreach($centros_costo as $centro){
        $array_centros[$centro["CentroCosto"]["id"]] = 0.00;   
           
       }
    }
    
  
    
    $asiento_cuenta_contable = $asiento_cuenta_contable_obj->find('all', array(
                                                //'fields'=>array('IFNULL(SUM(CASE WHEN es_debe=1 THEN monto ELSE 0 END),0) as debe','IFNULL(SUM(CASE WHEN es_debe=0 THEN monto ELSE 0 END),0) as haber'),
                                                'conditions' => array('AsientoCuentaContable.id_cuenta_contable' => $id_cuenta_contable,/*'Asiento.id'=>array(EnumEstadoAsiento::Ingresado,EnumEstadoAsiento::Registrado),*/
                                                                        'Asiento.fecha >='=>$fecha_inicio,   'Asiento.fecha <='=>$fecha_fin,
                                                                        'Asiento.id_estado_asiento'=>$id_estado_asiento,
                                                                        'CuentaContable.utiliza_centro_costos'=>1
                                                
                                                                        ),
                                                'contain' =>array('Asiento','CuentaContable','AsientoCuentaContableCentroCostoItem')
                                                ));
     
       
      
       if($asiento_cuenta_contable){
           
          foreach($asiento_cuenta_contable as $acc){ 
              
               foreach($acc["AsientoCuentaContableCentroCostoItem"] as $item){
                   

                 $array_centros[$item["id_centro_costo"]] += $item["monto"]; 
                 //$array_centros[$item["id_centro_costo"]] +=  $item["monto"];  
                   
               }
           
          }
       }
       
       
       
    
   
    
    
    return $array_centros;   
    
}



public function ModificaDescripcion(&$datos){
                foreach($datos as $key=>$value){
                    
                      
                       $datos[$key]["CuentaContable"]["d_cuenta_contable"] = $datos[$key]["CuentaContable"]["d_cuenta_contable"].' ($'.$datos[$key]["CuentaContable"]["saldo"].')';                       
                       
                    if ( isset($datos[$key]["children"]) && is_array($datos[$key]["children"])){
                        //si es un array sigo recorriendo
                     
                     $this->ModificaDescripcion($datos[$key]["children"]);
                     
                     
                   
                  }
         
               }
}

/*ExistenRaices? = IsNUUL(parent_id) Tienq q haber por lo menos 2 PASIVO Y ACTIVO   */
public function ExistenRaices(){
    
    $count = $this->find(
        'count', array(
            'conditions' => array('CuentaContable.parent_id' => null),
            'contain' =>false
        ));
    return $count;
}




}
