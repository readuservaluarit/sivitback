<?php
App::uses('AppModel', 'Model');

/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class ComprobantePuntoVentaNumero extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'comprobante_punto_venta_numero';
    public $actsAs = array('Containable');
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		/*'id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar un id v&aacute;lido',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
		),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
    );

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
public $belongsTo = array(
        'PuntoVenta' => array(
            'className' => 'PuntoVenta',
            'foreignKey' => 'id_punto_venta',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
        ),
        'TipoComprobante' => array(
            'className' => 'TipoComprobante',
            'foreignKey' => 'id_tipo_comprobante',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
        ),
		'Impuesto' => array(
				'className' => 'Impuesto',
				'foreignKey' => 'id_impuesto',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
		)
  );
  
   public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
		 $this->virtual_fields_view = array(
		 
		 "d_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "d_punto_venta"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "d_comprobante_letra"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "d_sistema"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "d_impuesto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "d_producto_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "codigo_producto_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "costo_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "id_moneda_costo_default"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "id_unidad_default"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "id_iva_default"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "id_sistema"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 ); 
	}    
  
     
    public function permiteModificarNroComprobante($id_tipo_comprobante,$id_punto_venta){
		
     	$cpvn = $this->find('first', array(
     			'conditions' => array('ComprobantePuntoVentaNumero.id_tipo_comprobante' => $id_tipo_comprobante,'ComprobantePuntoVentaNumero.id_punto_venta' => $id_punto_venta),
     			'contain' =>false
     	));
     	
     	if($cpvn){
     		
     		switch($cpvn["ComprobantePuntoVentaNumero"]["id_tipo_comprobante_punto_venta_numero"]){
     			
     			case EnumTipoComprobantePuntoVentaNumero::PropioElectronicoOfflineExportacion:
     			case EnumTipoComprobantePuntoVentaNumero::PropioOfflinePreimpreso:
     			case EnumTipoComprobantePuntoVentaNumero::Ajeno:
     			case EnumTipoComprobantePuntoVentaNumero::PropioNumeradorSistemaEDITABLE:
     				
     				return 1;
     				
     				
     				break;
     			default:
     				return false;
     				break;
     			
     			
     			
     		}

     	}else
     		return false;
     }
     
     /*
     public function ChequeoTipo(&$data,$model){
     	
     	$id_tipo_comprobante = $this->request->data[$model]["id_tipo_comprobante"];
     	$id_punto_venta = $this->request->data[$model]["id_punto_venta"];
     	
     	
     	$cpvn = $this->find('first', array(
     			'conditions' => array('ComprobantePuntoVentaNumero.id_tipo_comprobante' => $id_tipo_comprobante,'ComprobantePuntoVentaNumero.id_punto_venta' => $id_punto_venta),
     			'contain' =>false
     	));
     	
     	if($cpvn){
     		
     		if($cpvn["ComprobantePuntoVentaNumero"]["id_tipo_comprobante_punto_venta_numero"] == EnumTipoComprobantePuntoVentaNumero::PropioOfflinePreimpreso){
     			
     			$data[$model]["cae"] = $cpvn["ComprobantePuntoVentaNumero"]["codigo_legal_talonario"];
     			$data[$model]["vencimiento_cae"] = $cpvn["ComprobantePuntoVentaNumero"]["fecha_vencimiento_codigo_legal_talonario"];
     		}
     		return false;

     	}	else
     			return true;
     }
     */
  
  
	

}
