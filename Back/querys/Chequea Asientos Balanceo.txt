SELECT  * FROM asiento_cuenta_contable
JOIN asiento ON asiento.id = asiento_cuenta_contable.`id_asiento`
WHERE id_estado_asiento=1

GROUP BY id_asiento
HAVING SUM(IF(es_debe=1,monto,0)) - SUM(IF(es_debe=0,monto,0))>0
