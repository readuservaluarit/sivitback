<?php
        include(APP.'View'.DS.'ComprobanteValores'.DS.'json'.DS.'comprobante_valor.ctp');
        
        $model["id"]["show_grid"] = 1;
        $model["id"]["header_display"] = "Nro. Item";
        $model["id"]["order"] = "0";
        $model["id"]["width"] = "5";
        
        $model["codigo_tipo_comprobante"]["show_grid"] = 1;
        $model["codigo_tipo_comprobante"]["header_display"] = "Tipo";
        $model["codigo_tipo_comprobante"]["order"] = "1";
        $model["codigo_tipo_comprobante"]["width"] = "7";
        
        $model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
        $model["pto_nro_comprobante"]["order"] = "2";
        $model["pto_nro_comprobante"]["width"] = "20";
        
        $model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["header_display"] = "Raz&oacute;n social / Concepto";
        $model["razon_social"]["order"] = "3";
        $model["razon_social"]["width"] = "30";
        
        $model["fecha_generacion"]["show_grid"] = 1;
        $model["fecha_generacion"]["header_display"] = "Fecha Gen. Comprobante";
        $model["fecha_generacion"]["order"] = "4";
        $model["fecha_generacion"]["width"] = "10";
        
        $model["fecha_contable"]["show_grid"] = 1;
        $model["fecha_contable"]["header_display"] = "Fecha Contable";
        $model["fecha_contable"]["order"] = "5";
        $model["fecha_contable"]["width"] = "10";
        
        $model["d_valor"]["show_grid"] = 1;
        $model["d_valor"]["header_display"] = "Valor";
        $model["d_valor"]["order"] = "6";
        $model["d_valor"]["width"] = "8";
        
        $model["fecha_valor"]["show_grid"] = 1;
        $model["fecha_valor"]["header_display"] = "Fcha. Valor";
        $model["fecha_valor"]["order"] = "7";
        $model["fecha_valor"]["width"] = "10";
        
        $model["d_comprobante_valor"]["show_grid"] = 1;
        $model["d_comprobante_valor"]["header_display"] = "Desc.";
        $model["d_comprobante_valor"]["order"] = "8";
        $model["d_comprobante_valor"]["width"] = "15";
         
        $model["referencia"]["show_grid"] = 1;
        $model["referencia"]["header_display"] = "Referencia";
        $model["referencia"]["order"] = "9";
        $model["referencia"]["width"] = "10";
       
        $model["monto"]["show_grid"] = 1;
        $model["monto"]["header_display"] = "Tot. Valor";
        $model["monto"]["order"] = "10";
        $model["monto"]["width"] = "10";
		$model["monto"]["decimal_place"] = "2";
		
        $model["d_moneda"]["show_grid"] = 1;
        $model["d_moneda"]["header_display"] = "Moneda";
        $model["d_moneda"]["order"] = "11";
        $model["d_moneda"]["width"] = "8";
                 
        $model["d_banco"]["show_grid"] = 1;
        $model["d_banco"]["header_display"] = "Banco";
        $model["d_banco"]["order"] = "12";
        $model["d_banco"]["width"] = "15";
        
        $model["d_banco_sucursal"]["show_grid"] = 1;
        $model["d_banco_sucursal"]["header_display"] = "Sucursal";
        $model["d_banco_sucursal"]["order"] = "13";
        $model["d_banco_sucursal"]["width"] = "10";

        $model["nro_cuenta"]["show_grid"] = 1;
        $model["nro_cuenta"]["header_display"] = "Cuenta";
        $model["nro_cuenta"]["order"] = "14";
        $model["nro_cuenta"]["width"] = "10";
        
        $model["nro_cheque"]["show_grid"] = 1;
        $model["nro_cheque"]["header_display"] = "Nro.";
        $model["nro_cheque"]["order"] = "15";
        $model["nro_cheque"]["width"] = "5";
		
        $model["d_estado_comprobante"]["show_grid"] = 1;
        $model["d_estado_comprobante"]["header_display"] = "Estado Comprobante";
        $model["d_estado_comprobante"]["order"] = "16";
        $model["d_estado_comprobante"]["width"] = "10";
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteValor",
                    "content" => $model
                );
        echo json_encode($output);
?>