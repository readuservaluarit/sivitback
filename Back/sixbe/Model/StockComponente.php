<?php
App::uses('AppModel', 'Model');
App::uses('StockProducto', 'Model');

/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class StockComponente extends StockProducto {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	

/**
 * Validation rules
 *
 * @var array
 */
	

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 

public $belongsTo = array(
        'Componente' => array(
            'className' => 'Componente',
            'foreignKey' => 'id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'Deposito' => array(
            'className' => 'Deposito',
            'foreignKey' => 'id_deposito',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        
  );
  
  
  
	

}
