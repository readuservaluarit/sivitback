<?php

class PuntoVentaPicker extends EntityPicker{
    
    public $model = "PuntoVenta";
    
    public $pickerTitle = "Selección de Punto de Venta";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Nro.", "field" => "numero", "width" => "90%", "keyOrder" => "numero")
                                );

    public function getPickerConditions($busqueda){
        
        $pickerConditions = array('PuntoVenta.numero LIKE' => '%' . $busqueda['bus_unico'] . '%');
        
        return $pickerConditions;
        
        
    }
  
    /*public function getPickerInitialFilter(){
        
        //Filtra siempre por la Jurisdiccion del usuario si corresponde
        return DataFilter::getJurisdiccionPickerFilter();
        
    }*/
  
  
}