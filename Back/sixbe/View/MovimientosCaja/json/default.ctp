<?php
       include(APP.'View'.DS.'Comprobantes'.DS.'json'.DS.'default.ctp'); 
        
        // CONFIG  CON MODE DISPLAYED CELLS
   
        $model["d_tipo_comprobante"]["show_grid"] = 1;
        $model["d_tipo_comprobante"]["header_display"] = "Tipo Comprobante";
        $model["d_tipo_comprobante"]["width"] = "9";
        $model["d_tipo_comprobante"]["order"] = "1";
        $model["d_tipo_comprobante"]["text_colour"] = "#400040";
        
        $model["d_detalle_tipo_comprobante"]["show_grid"] = 1;
        $model["d_detalle_tipo_comprobante"]["type"] = EnumTipoDato::cadena;
        $model["d_detalle_tipo_comprobante"]["header_display"] = "Detalle";
		$model["d_detalle_tipo_comprobante"]["width"] = "11";
        $model["d_detalle_tipo_comprobante"]["order"] = "2";
        $model["d_detalle_tipo_comprobante"]["text_colour"] = "#700070";
        
        $model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Nro.Comprobante";
        $model["pto_nro_comprobante"]["width"] = "12";
        $model["pto_nro_comprobante"]["order"] = "3";
        $model["pto_nro_comprobante"]["text_colour"] = "#000000";
        
      
        $model["fecha_contable"]["show_grid"] = 1;
        $model["fecha_contable"]["header_display"] = "Fecha Contable";
        $model["fecha_contable"]["width"] = "10";
        $model["fecha_contable"]["order"] = "5";
        $model["fecha_contable"]["text_colour"] = "#0000FF";

        $model["d_moneda_simbolo"]["show_grid"] = 1;
        $model["d_moneda_simbolo"]["header_display"] = "Moneda";
        $model["d_moneda_simbolo"]["width"] = "4";
        $model["d_moneda_simbolo"]["order"] = "6";
        $model["d_moneda_simbolo"]["text_colour"] = "#F0000F";
        
        $model["total_comprobante"]["show_grid"] = 1;
        $model["total_comprobante"]["header_display"] = "Monto";
        $model["total_comprobante"]["width"] = "7";
        $model["total_comprobante"]["order"] = "7";
        $model["total_comprobante"]["text_colour"] = "#F0000F";
        
        $model["d_estado_comprobante"]["show_grid"] = 1;
        $model["d_estado_comprobante"]["header_display"] = "Estado";
        $model["d_estado_comprobante"]["width"] = "7";
        $model["d_estado_comprobante"]["order"] = "8";
        $model["d_estado_comprobante"]["text_colour"] = "#004400";
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Comprobante",
                    "content" => $model
                );
        
        echo json_encode($output);
?>