<?php
        include(APP.'View'.DS.'ComprobanteItemLotes'.DS.'json'.DS.'comprobante_item_lote.ctp');

		/*Se usa desde OT*/
		
        $model["codigo"]["show_grid"] = 1;
		$model["codigo"]["type"] = "string";
        $model["codigo"]["header_display"] = "C&oacute;digo";
        $model["codigo"]["order"] = "0";
        $model["codigo"]["width"] = "20";
        $model["codigo"]["text_colour"] = "#660000";  
		$model["codigo"]["picker"] = EnumMenu::mnuLotePicker;
		
        $model["d_lote"]["show_grid"] = 1;
		$model["d_lote"]["type"] = "string";
		$model["d_lote"]["header_display"] = "Desc.";
        $model["d_lote"]["order"] = "1";
        $model["d_lote"]["width"] = "25";
        $model["d_lote"]["text_colour"] = "#660000"; //ESTA CAMPO AHORA TIENE Q SER CALCULADO en funcion de los comprobantes IRM q tiene asociado el lote  (EBSA 1 lote muchos articulos)
        
        $model["d_tipo_lote"]["show_grid"] = 1;
		$model["d_tipo_lote"]["type"] = "string";
        $model["d_tipo_lote"]["header_display"] = "Tipo Lote";
        $model["d_tipo_lote"]["order"] = "3";
        $model["d_tipo_lote"]["width"] = "15";
        $model["d_tipo_lote"]["text_colour"] = "#660000";


        
        
           
		$output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "ComprobanteItemLote",
				"content" => $model
			);
        
          echo json_encode($output);
?>