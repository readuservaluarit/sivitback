<?php

/**
* @secured(CONSULTA_IMPUESTO)
*/
class ImpuestosController extends AppController {
    public $name = 'Impuestos';
    public $model = 'Impuesto';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    
    public function index() {
    	  
        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        $id_sistema = $this->getFromRequestOrSession($this->model.'.id_sistema');
        $importa_archivo = $this->getFromRequestOrSession($this->model.'.importa_archivo');  //es para saber si el impuesto tiene asociado un archi de arba o alguna entidad fiscal
        $id_tipo_impuesto = $this->getFromRequestOrSession($this->model.'.id_tipo_impuesto');
        $d_impuesto = $this->getFromRequestOrSession($this->model.'.d_impuesto');
        
        $conditions = array(); 
        if($id_sistema!="")
            array_push($conditions, array($this->model.'.id_sistema =' => $id_sistema)); 
            
        if($importa_archivo!="")
            array_push($conditions, array($this->model.'.importa_archivo =' => $importa_archivo));    
        
        if($id_tipo_impuesto!="")
            array_push($conditions, array($this->model.'.id_tipo_impuesto =' => $id_tipo_impuesto));
       
        if($d_impuesto!="")
            array_push($conditions, array($this->model.'.d_impuesto LIKE' => '%'.$d_impuesto .'%'));
            
            
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de ImpuestoCausas', 'Datos de los Tipo de Impuesto', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_Impuesto', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'Impuesto.id', "10%");
                $formBuilder->addHeader('Nombre', 'Impuesto.d_Impuesto_causa', "50%");
                $formBuilder->addHeader('Nombre', 'Impuesto.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_ImpuestoCausa');
                $formBuilder->addField($this->model, 'cotizacion');
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        	
        	
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        
        foreach($data as &$impuesto){
            
            $impuesto["Impuesto"]["d_tipo_impuesto"] = $impuesto["TipoImpuesto"]["d_tipo_impuesto"];
            $impuesto["Impuesto"]["d_sistema"] = $impuesto["Sistema"]["d_sistema"];
            $impuesto["Impuesto"]["d_estado"] = $impuesto["Estado"]["d_estado"];
            $impuesto["Impuesto"]["d_cuenta_contable"] = $impuesto["CuentaContable"]["d_cuenta_contable"];
           
            if(isset($impuesto["Provincia"]["d_provincia"]) && strlen($impuesto["Provincia"]["d_provincia"])>0)
            	$impuesto["Impuesto"]["d_provincia"] = $impuesto["Provincia"]["d_provincia"];
            
            unset( $impuesto["TipoImpuesto"]);
            unset( $impuesto["Sistema"]);
            unset( $impuesto["Estado"]);
            unset( $impuesto["ImpuestoGeografia"]);
            unset( $impuesto["CuentaContable"]);
            unset( $impuesto["SubTipoImpuesto"]);
            unset( $impuesto["TipoComprobante"]);
        }
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->Impuesto->id = $id;
            $this->request->data = $this->Impuesto->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('ImpuestoCausa');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de ImpuestoCausa');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de ImpuestoCausa');
        else
            $formBuilder->setTituloForm('Consulta de ImpuestoCausa');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_ImpuestoCausa', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('cotizacion', 'Cotizaci&oacute;n', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));    
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'ImpuestoCausaAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('ImpuestoCausaDImpuestoCausa', 'Debe ingresar un nombre');
        validator.validateRequired('ImpuestoCausaCotizacion', 'Debe ingresar una cotizaci&oacute;n');


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_IMPUESTO)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $this->loadModel("DatoEmpresaImpuesto");
            $id_add = '';
            
            try{
            	
            	
            	if($this->Impuesto->chequeoIntervaloBase($this->request->data["Impuesto"]["id_sistema"],0,$this->request->data["Impuesto"]["id_tipo_impuesto"],$this->request->data["Impuesto"]["id_sub_tipo_impuesto"],$this->request->data["Impuesto"]["base_imponible_desde"],$this->request->data["Impuesto"]["base_imponible_hasta"],$this->request->data["Impuesto"]["id_tipo_comercializacion"]) == 0){
	                if ($this->Impuesto->saveAll($this->request->data, array('deep' => true))){
	                    $mensaje = "El Impuesto ha sido creado exitosamente";
	                    $tipo = EnumError::SUCCESS;
	                    $id_add = $this->Impuesto->id;
	                    
	                    
	                    $this->agregoDatoEmpresaImpuesto($id_add);
	                    
	                }else{
	                    $errores = $this->{$this->model}->validationErrors;
	                    $errores_string = "";
	                    foreach ($errores as $error){
	                        $errores_string.= "&bull; ".$error[0]."\n";
	                        
	                    }
	                    $mensaje = $errores_string;
	                    $tipo = EnumError::ERROR; 
	                    
	                }
                
            	}else{
            		$tipo = EnumError::ERROR;
            		$mensaje = "La base imponible desde y hasta es incorrecta hay solapamiento con una base existente. Cambiela y vuelva a grabar.";
            	}
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el Impuesto no ha podido ser creado.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" => $id_add
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }

    
    
    
    /**
    * @secured(MODIFICACION_IMPUESTO)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{  
            	
            	
            	
            	if($this->Impuesto->chequeoIntervaloBase($this->request->data["Impuesto"]["id_sistema"],$id,$this->request->data["Impuesto"]["id_tipo_impuesto"],$this->request->data["Impuesto"]["id_sub_tipo_impuesto"],$this->request->data["Impuesto"]["base_imponible_desde"],$this->request->data["Impuesto"]["base_imponible_hasta"],$this->request->data["Impuesto"]["id_tipo_comercializacion"]) == 0){
	                if ($this->{$this->model}->saveAll($this->request->data)){
	                     $mensaje =  "El Impuesto ha sido modificado exitosamente";
	                     $status = EnumError::SUCCESS;
	                    
	                    
	                }else{   //si hubo error recupero los errores de los models
	                    
	                    $errores = $this->{$this->model}->validationErrors;
	                    $errores_string = "";
	                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
	                        $errores_string.= "&bull; ".$error[0]."\n";
	                        
	                    }
	                    $mensaje = $errores_string;
	                    $status = EnumError::ERROR; 
	               }
            	}else{
            		$status = EnumError::ERROR; 
            		$mensaje = "La base imponible desde y hasta es incorrecta hay solapamiento con una base existente. Cambiela y vuelva a grabar.";
            		
            	}
               
               
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, el Impuesto no ha podido modificarse.', 'error');
                $status = EnumError::ERROR;
                $mensaje = "El impuesto no ha podido ser editado.".$e->getMessage();
                
            }
            
            if($this->RequestHandler->ext == 'json'){
            	$output = array(
            			"status" => $status,
            			"message" => $mensaje,
            			"content" => ""
            	);
            	
            	$this->set($output);
            	$this->set("_serialize", array("status", "message", "content"));
            }else{
            	$this->Session->setFlash($mensaje, $status);
            	$this->redirect(array('controller' => $this->name, 'action' => 'index'));
            	
            } 
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            //$this->{$this->model}->contain('Provincia','Pais');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }

    /**
    * @secured(BAJA_IMPUESTO)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->Impuesto->id = $id;
     
       try{
            if ($this->Impuesto->delete() ) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Impuesto ha sido eliminado exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }
    
    
	// public function getModel($vista='normal')
	// {
        // $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        // $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        // $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    // }
    
    
    
    // private function editforView($model,$vista)
	// {  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
        // $this->set('model',$model);
        // Configure::write('debug',0);
        // $this->render($vista);
    // }
    
	
    
     
      /**
    * @secured(CONSULTA_IMPUESTO)
    */
        public function getModel($vista='default')
        {    
            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
        }
        
        
         private function editforView($model,$vista)
         {  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

                $this->set('model',$model);
                Configure::write('debug',0);
                $this->render($vista);    

         }
    
   
    
    
    public function importarImpuestos($id_impuesto = 0,$nombre_archivo_importacion=''){
        
        
        
        //$id_impuesto = 29;
        $this->layout = 'ajax';       //vista le digo q no busque en la VISTAS
        $this->loadModel("Impuesto");
        
        
        set_time_limit(1200);
        
        /**Por default importa ibb buenos aires y la percepcion*/
        
        /*SI $id_impuesto=0 entonces asumo que esta importante PERCEPCIONES DE IIB DE VENTAS y dentro de la funcion le modifico el ID*/
        $nombre_archivo_importacion = $this->determinaNombreArchivo($id_impuesto);
        
        
     
        
        $zip = new ZipArchive();
        
         App::uses('Folder', 'Utility');
         $random = 0;
         $dir_temp = new Folder(APP.'webroot/media_file');
         $this->autoRender = false;
        
        
        
        if($nombre_archivo_importacion != ''){
        if (!empty($_FILES)) {
                    $tempFile = $this->request->params['form']['file']['tmp_name'];
                    $size = $this->request->params['form']['file']['size'];
                    $size_mb = ($size/1024)/1024;
                    $tamanio_maximo_mb = 10;
                    
                    $destino = $dir_temp->path .'//'.$random.'-'.$this->request->params['form']['file']['name'];
                    
                    if(move_uploaded_file($tempFile,$destino)){
                    
                
                            $res = $zip->open($destino);
                            if ($res === TRUE) {
                              $zip->extractTo($dir_temp->path);
                              $zip->close();
                              $direccion_archivo = $dir_temp->path.$nombre_archivo_importacion;
                              
                              if(file_exists($direccion_archivo)){
                                  
                                  $tabla_temporal = $this->Impuesto->getTablaTemporalName($id_impuesto);
                             
                                  if($this->procesarImpuesto($direccion_archivo,$id_impuesto,$tabla_temporal,'\n',';',$message)){
                                    $error = EnumError::SUCCESS;
                                    $message = 'El archivo fue importado exitosamente';  
                                      
                                  }else{
                                      $error = EnumError::ERROR;//el mensaje cuando hay error viene por el procesarImpuesto
                                      
                                  }
                                  
                              }else{
                                  
                                 $error = EnumError::ERROR;
                                 $message = 'El archivo NO puede ser abierto'.$direccion_archivo; 
                              }
                            
                            } else {
                                
                                 $error = EnumError::ERROR;
                                 $message = 'El archivo NO puede ser descomprimido';
                             
                            }
                
                }else{
                    
                     $error = EnumError::ERROR;
                     $message = 'El archivo no subio correctamente cheque permisos en el servidor';
                    
                }
        }else{
            
            $error = EnumError::ERROR;
            $message = 'El archivo no subio correctamente cheque la configuracion del servidor.MAX_UPLOAD_SIZE='.ini_get('upload_max_filesize').'SHOW VARIABLES LIKE "secure_file_priv"; debe aparecer vacio';
            
            
        }
        
        }else{
            
            $error = EnumError::ERROR;
            $message = 'No existe proceso para el impuesto seleccionado';
            
            
        }
         $this->autoRender = false;
        $output = array(
            "status" => $error,
            "message" => $message,
            "content" => "CONTENT"
        );
        //$this->set($output);
        //$this->set("_serialize", array("status", "message","page_count", "content"));
        echo json_encode($output);
    }
    
    
    private function procesarImpuesto($ruta_archivo,$id_impuesto,$tabla_temporal,$salto_linea,$separador_linea,&$message){
        
      
       
           $this->autoRender = false;
           
           //$id_impuesto = 29;// TODO: No esta llegando bien dsd el front.
            //Guardar en tabla temporal y matchear con persona
            
            $this->loadModel("PersonaImpuestoAlicuotaTemporal");
            $this->loadModel("PersonaImpuestoAlicuotaTemporalCaba");
            
            
            
            try{
            
            
            $this->PersonaImpuestoAlicuotaTemporal->query("TRUNCATE ".$tabla_temporal.";");
            
            //Si es cloud deberia conectarse a la DB local dumpear la tabla y luego pasar de a X registros a la otra BD
            
            $query = "LOAD DATA INFILE '".$ruta_archivo."' INTO TABLE ".$tabla_temporal."
                                                                    FIELDS TERMINATED BY '".$separador_linea."'
                                                                    LINES TERMINATED BY '".$salto_linea."';";
          
            $this->PersonaImpuestoAlicuotaTemporal->query($query);
            
                                                     //DELETE FROM DAT
             
            $this->PersonaImpuestoAlicuotaTemporal->query("DELETE FROM persona_impuesto_alicuota where id_impuesto=".$id_impuesto);
             
            $query = $this->getQueryInsercionPorImpuesto($id_impuesto,$tabla_temporal); 
            
            $this->PersonaImpuestoAlicuotaTemporal->query($query); 
            
            
            if($id_impuesto == EnumImpuesto::ExencionPercepCABA ||  $id_impuesto == EnumImpuesto::ExencionRetenCABA){
             $this->PersonaImpuestoAlicuotaTemporal->query("DELETE FROM persona_impuesto_alicuota where id_impuesto=".$id_impuesto." and porcentaje_alicuota >0");//esta linea la pongo porque en el archivo de exentos incluyen alicuotas especiales que ya estan en el archivo enviado. Si esta en el archivo de exentos y el % de ret o perc es 0 cero entonces es exento.
             }                      
            
                                           
             $this->PersonaImpuestoAlicuotaTemporal->query("TRUNCATE ".$tabla_temporal.";");
             
           return true;                                               
            /*actualizo la tabla temporal con el id_persona*/                                                        
            /*
            
            $this->PersonaImpuestoAlicuotaTemporal->query("

                                                            UPDATE  ".$tabla_temporal." piat
                                                            JOIN persona p 


                                                            ON p.cuit= piat.cuit

                                                            SET 

                                                            piat.id_persona = p.id;"
                                                            );
                                                            
                                                            //la fecha debe ser AAAA-MM-DD
                                                            //el proceso lo manda como  DDMMAAAA 24022016
            $this->PersonaImpuestoAlicuotaTemporal->query("
                                                            UPDATE DATO_EMPRESA pia
                                                            JOIN ".$tabla_temporal." piat
                                                            ON piat.id_persona = pia.id_persona
                                                            SET pia.fecha_vigencia_desde = CONCAT(SUBSTRING(piat.fecha_vigencia_desde, 5, 4),  '-', SUBSTRING(piat.fecha_vigencia_desde, 3, 2),'-',SUBSTRING(piat.fecha_vigencia_desde, 1, 2)) ,
                                                                pia.fecha_vigencia_hasta = CONCAT(SUBSTRING(piat.fecha_vigencia_hasta, 5, 4),  '-', SUBSTRING(piat.fecha_vigencia_hasta, 3, 2),'-',SUBSTRING(piat.fecha_vigencia_hasta, 1, 2)),
                                                                pia.porcentaje_alicuota = REPLACE(piat.porcentaje_alicuota, ',', '.')
                                                            WHERE id_impuesto=".$id_impuesto.";"
                                                            );
           
           
           
              */
                                                            
                                                                    
                                                                    
        }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
          $message =  $ex->getMessage();
          return false;
            
            
            
        }
        
    }
    
    
    
    private function getQueryInsercionPorImpuesto($id_impuesto,$tabla_temporal){
        
        
        switch($id_impuesto){
            
            case EnumImpuesto::PERCEPIIBBBUENOSAIRES:
            case EnumImpuesto::RETENCIONIIBBBSAS:
              $id_campo  = "porcentaje_alicuota";
            
            
            break;
            
   
            case EnumImpuesto::ExencionPercepCABA:
            case EnumImpuesto::PERCEPIIBBCABA:   
            $id_campo  = "porcentaje_alicuota_percepcion";
            break;
            
            case EnumImpuesto::ExencionRetenCABA:
            case EnumImpuesto::RETIIBBCABA: 
            $id_campo  = "porcentaje_alicuota_retencion";
            break;
            
             }  
             
            
            
             $query = "INSERT INTO  persona_impuesto_alicuota
                                                           (fecha_vigencia_desde,fecha_vigencia_hasta,porcentaje_alicuota,id_persona,id_impuesto)
                                                           
                                                           
                                                           
                                                 SELECT DISTINCT CONCAT(SUBSTRING(piat.fecha_vigencia_desde, 5, 4),  '-', SUBSTRING(piat.fecha_vigencia_desde, 3, 2),'-',SUBSTRING(piat.fecha_vigencia_desde, 1, 2)) as fecha_vigencia_desde,
                                                      CONCAT(SUBSTRING(piat.fecha_vigencia_hasta, 5, 4),  '-', SUBSTRING(piat.fecha_vigencia_hasta, 3, 2),'-',SUBSTRING(piat.fecha_vigencia_hasta, 1, 2)) as fecha_vigencia_hasta,
                                                        REPLACE(piat.".$id_campo.", ',', '.') AS porcentaje_alicuota,
                                                        p.id,".$id_impuesto."

                                                        FROM ".$tabla_temporal." piat 

                                                        JOIN persona p ON p.`cuit`=piat.`cuit`
                                                        
                                                      
                                                         
                                                        ";
            
            
            // $this->log($query);
             
            return $query;
            
       
        
        
    }
    
    
   private function determinaNombreArchivo(&$id_impuesto){
        
        switch($id_impuesto){
            case 0:
            case EnumImpuesto::PERCEPIIBBBUENOSAIRES:
            
              
           
            $nombre_archivo_importacion = '/PadronRGSPer'.date("m").''.date("Y").'.txt'; //el nombre del archivo tiene el MMYYYY
      
           break;
            case EnumImpuesto::RETENCIONIIBBBSAS:
           
            $nombre_archivo_importacion = '/PadronRGSRet'.date("m").''.date("Y").'.txt'; //el nombre del archivo tiene el MMYYYY 
            break;
           case EnumImpuesto::PERCEPIIBBCABA:
           case EnumImpuesto::RETIIBBCABA:
            $nombre_archivo_importacion = '/padron_caba_ingresos_brutos'.date("m").''.date("Y").'.txt';
           break;
            case EnumImpuesto::RETENCIONIVACOMPRASCABA:
            $nombre_archivo_importacion = '/padron_caba_iva'.date("m").''.date("Y").'.txt';
            break;
            
            case EnumImpuesto::ExencionPercepCABA:
            case EnumImpuesto::ExencionRetenCABA:
            $nombre_archivo_importacion = '/padron_exentos_caba_ingresos_brutos'.date("m").''.date("Y").'.txt';
           break;
           
           default:
            $nombre_archivo_importacion = '';
           break;
        }
        
        
        
        return $nombre_archivo_importacion;
        
    }
    
    
    protected function agregoDatoEmpresaImpuesto($id_impuesto){
    	

    	
    	/*Las Percepciones de Compras se agregan a dato empresa impuesto*/
    	if($this->request->data["Impuesto"]["id_tipo_impuesto"] == EnumTipoImpuesto::PERCEPCIONES && $this->request->data["Impuesto"]["id_sistema"] == EnumSistema::COMPRAS){
    		$this->loadModel(EnumModel::DatoEmpresaImpuesto);
    		$dato_empresa_impuesto["id_impuesto"] = $id_impuesto;
    		$dato_empresa_impuesto["check_mostrar_impuesto_libro_iva"] = 1;
    		$dato_empresa_impuesto["sin_vencimiento"] = 1;
    		$dato_empresa_impuesto["id_dato_empresa"] = 1;
    		$this->{EnumModel::DatoEmpresaImpuesto}->saveAll($dato_empresa_impuesto);//lo agrego al manager de impuestos de la empresa.
    		
    		$this->loadModel(EnumModel::Impuesto);
    		$this->loadModel(EnumModel::PersonaImpuestoAlicuota);
    		
    		if( $this->{EnumModel::Impuesto}->EsGlobal($id_impuesto)==1 )//si el impuesto es global lo agrego
    			$this->{EnumModel::PersonaImpuestoAlicuota}->agregarGlobal($id_impuesto,$this->request->data["Impuesto"]["id_sistema"]);
    		
    	}
    	
    	
    }
    
    
    
    
    
    



}
?>