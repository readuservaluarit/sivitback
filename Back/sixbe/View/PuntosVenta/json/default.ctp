<?php
        // CONFIG  CON MODE DISPLAYED CELLS
        
        $model["d_pais"]["show_grid"] = 0;
        $model["d_pais"]["type"] = EnumTipoDato::cadena;
        
        
        
        $model["d_provincia"]["show_grid"] = 0;
        $model["d_provincia"]["type"] = EnumTipoDato::cadena;
        
      
        
        $model["d_punto_venta"]["show_grid"] = 1;
        $model["d_punto_venta"]["header_display"] = "Descripci&oacute;n";
        $model["d_punto_venta"]["minimun_width"] = "90"; //REstriccion mas de esto no se puede achicar
        $model["d_punto_venta"]["order"] = "1";
        $model["d_punto_venta"]["text_colour"] = "#0000AA";
		
        $model["numero"]["show_grid"] = 1;
        $model["numero"]["header_display"] = "Numero";
        $model["numero"]["minimun_width"] = "90"; //REstriccion mas de esto no se puede achicar
        $model["numero"]["order"] = "2";
        $model["numero"]["text_colour"] = "#550099";
        
        $model["factura_electronica"]["show_grid"] = 1;
        $model["factura_electronica"]["header_display"] = "Factura Electrónica";
        $model["factura_electronica"]["minimun_width"] = "90"; //REstriccion mas de esto no se puede achicar
        $model["factura_electronica"]["order"] = "3";
        $model["factura_electronica"]["text_colour"] = "#770000";
        
        $model["d_moneda"]["show_grid"] = 1;
        $model["d_moneda"]["type"] = EnumTipoDato::cadena; 
        $model["d_moneda"]["header_display"] = "Moneda";
        $model["d_moneda"]["minimun_width"] = "90"; //REstriccion mas de esto no se puede achicar
        $model["d_moneda"]["order"] = "4";
        $model["d_moneda"]["text_colour"] = "#007700";
	
    
		$model["id_provincia"]["show_grid"] = 1;
        $model["id_provincia"]["header_display"] = "Provincia";
        $model["id_provincia"]["minimun_width"] = "90"; //REstriccion mas de esto no se puede achicar
        $model["id_provincia"]["order"] = "6";
        $model["id_provincia"]["text_colour"] = "#000000";
   
         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "PuntoVenta",
                    "content" => $model
                );
        
          echo json_encode($output);
?>