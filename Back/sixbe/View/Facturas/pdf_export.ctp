<?php   

function digitoVerificadorCaeAfip($txt){
  $i = $pares = $impares = 0;

  for( $i = 0; $i < strlen($txt); $i++ ) {
    // OJO: Los caracteres impares esta en la posiciones pares del array y viceversa, porque el array arranca desde la posicion 0
    if( $i % 2 ) {
      $pares += $txt[$i];
    } else {
      $impares += $txt[$i];
    }
  }
  $impares = $impares * 3;
  
  $total = $pares + $impares;
  
  $dv = $total % 10;

  return $dv;
}

/**
 * PDF exportación de Orden Compra
 */

App::import('Vendor','tcpdf/tcpdf'); 
App::import('Vendor','tcpdf/tcpdf_barcodes_1d'); 


// create new PDF document


$html = '<table width="100%" border="0" cellspacing="0" style="font-size:10px;border:2px solid #000000; font-family:\'Courier New\', Courier, monospace">
          <tr>
            <td style="padding: 5px 5px 5px 5px;" width="37%" rowspan="4" align="center" valign="center"  ><img src="http://www.sixbend.com.ar/sixbe/img/header_factura.png" width="476" height="129" />  </td>
            <td width="8%" height="3%" rowspan="2"  style="border:2px solid #000000">
                <div align="center" style="font-size:30px">'.strtoupper($datos_pdf["TipoComprobante"]["letra"]).'</div>
                 <span align="center" style="font-size:10px;">cod 01</span>
            </td>
            <td width="55%" style="padding-left:5px; font-size:17px;font-weight:bold;"> FACTURA: </td>
            
          </tr>
          <tr>
            <td style="padding-left:5px; font-size:15px;"> Nro '.str_pad($datos_pdf["PuntoVenta"]["numero"],4,"0",STR_PAD_LEFT).'-'.str_pad($datos_pdf["Comprobante"]["nro_comprobante_afip"],8,"0",STR_PAD_LEFT).'</td>
          </tr> 
          
          <tr>
            <td width="8%" height="2%" align="center" valign="middle" >&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr >
            <td  width="8%" height="5%" align="center" valign="middle" >&nbsp;</td>
            <td style="padding-left:5px;"> Original Blanco / Copia color </td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["calle"].' '.$datos_empresa["DatoEmpresa"]["numero"].' '.$datos_empresa["DatoEmpresa"]["codigo_postal"].' '.$datos_empresa["Provincia"]["d_provincia"].'</td>
            <td rowspan="5">&nbsp;</td>
            <td style="padding-left:5px;"> Fecha: '.$datos_pdf["Comprobante"]["fecha_contable"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> E-mail: '.$datos_empresa["DatoEmpresa"]["email"].'</td>
            <td style="padding-left:5px;"> CUIT: '.$datos_empresa["DatoEmpresa"]["cuit"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> Tel: '.$datos_empresa["DatoEmpresa"]["tel"].'</td>
            <td style="padding-left:5px;"> Ingresos Brutos: '.$datos_empresa["DatoEmpresa"]["iibb"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["website"].'</td>
            <td style="padding-left:5px;"> Inicio de actividades: '.$datos_empresa["DatoEmpresa"]["fecha_inicio_actividad"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.strtoupper($datos_empresa["TipoIva"]["d_tipo_iva"]).'</td>
            <td style="padding-left:5px;">&nbsp;</td>
          </tr>
        </table>';

$html;



class MyTCPDF extends TCPDF{

    public $datos_header;
    public $datos_footer;
    public $bar_code;
    public $observaciones;
    
    public function set_datos_header($datos_empresa){
        $this->datos_header = $datos_empresa; 

    }
    public function set_datos_footer($datos_empresa_footer){
        $this->datos_footer = $datos_empresa_footer; 

    }
     public function set_barcode($bar_code){
        $this->bar_code = $bar_code; 

    }
    
    public function set_observaciones($observaciones){
        $this->observaciones = $observaciones; 

    }
  public function Header(){
     
        
        
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_header, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
  }
  
  public function Footer(){
     
        
        
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_footer, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->observaciones, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
     
     $style['text'] = true;
     $style['border'] = true;
     $style['align'] = 'C';
     $this->write1DBarcode($this->bar_code, 'I25', '40', '', '', 9, 0.4, $style, 'N');
      
      //$this->writeHTMLCell($w = 0, $h = 0, $x = '200', $y = '', $this->bar_code, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
      //$this->Cell(0, 20, $this->bar_code , 0, false, 'C', 0, '', 0, false, 'T', 'M');
      $this->Cell(0, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
      
  }
  
}

$pdf = new MyTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->set_datos_header($html);


$descuento = (($datos_pdf["Comprobante"]["total_comprobante"]*$datos_pdf["Comprobante"]["descuento"])/100) ;



//genero el codigo de barras para ver las especificaciones http://www.formularioscontinuos.com/nota1.html
if( $datos_pdf["Comprobante"]["cae"] != '' ){

    //genero el string para el codigo de barras
    $cuit =  str_replace("-","",$datos_empresa["DatoEmpresa"]["cuit"]);
    $tipo_doc_afip = str_pad($datos_pdf["TipoComprobante"]["codigo_afip"],2,"0",STR_PAD_LEFT);
    $cae = $datos_pdf["Comprobante"]["cae"];
    $fecha_vencimiento = str_replace("-","",$datos_pdf["Comprobante"]["vencimiento_cae"]);
    
    $codigo_barras = $cuit.$tipo_doc_afip.$cae.$fecha_vencimiento;
    $codigo_barras = $codigo_barras.digitoVerificadorCaeAfip($codigo_barras);
    $pdf->set_barcode($codigo_barras);  
    
   
}  


$impuestos = '';
if(isset($datos_pdf["ComprobanteImpuesto"])){

        foreach($datos_pdf["ComprobanteImpuesto"] as $impuesto){

            $impuestos .= ' <tr>
                 <td>'.$impuesto["Impuesto"]["d_impuesto"].'</td>
                 <td>'.money_format('%!n',$impuesto["tasa_impuesto"]).'%</td>
                <td>'.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n',$impuesto["importe_impuesto"]).'</td>
               </tr>';
               
        }
} 




$footer='
  <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>                     
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody> 
</table>
<table width="100%" border="1">
   <tr>
     <td width="60%">Observaciones: '.$datos_pdf["Comprobante"]["observacion"].' <br/>  Esta factura por un monto de '.$datos_pdf["Moneda"]["simbolo_internacional"].' '.$datos_pdf["Comprobante"]["total_comprobante"].' equivale a USD '.round($datos_pdf["Comprobante"]["total_comprobante"]/$datos_pdf["Comprobante"]["valor_moneda"],2).'  Ref 1 USD= '.$datos_pdf["Moneda"]["simbolo_internacional"].' '.round($datos_pdf["Comprobante"]["valor_moneda"],2).'</td>
     <td width="50%">
     <table width="100%" border="0">
       <tr>
         <td>SUBTOTAL</td>
         <td>&nbsp;</td>
         <td>'.$datos_pdf["Moneda"]["simbolo_internacional"].' '.$datos_pdf["Comprobante"]["subtotal_bruto"].'</td>
       </tr>
       '.$impuestos.'
       <tr>
         <td><strong>TOTAL</strong></td>
         <td>&nbsp;</td>
         <td><strong>'.$datos_pdf["Moneda"]["simbolo_internacional"].' '. money_format("%!n", $datos_pdf["Comprobante"]["total_comprobante"]).'</strong></td>
       </tr>
     </table></td>
   </tr>
   <tr>
     <td > CAE: '.$datos_pdf["Comprobante"]["cae"].'</td>
     <td > Fecha Vto.: '.$datos_pdf["Comprobante"]["vencimiento_cae"].'</td>
   </tr>
</table>
';


   
   $html .= "<HR>";
   
   
   if( $datos_pdf["Comprobante"]["cae"] != '' ){
   
      $html.=$pdf->SetBarcode($codigo_barras,I25);
     
   
   }
   
   
   $html .= '
   
   <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody>
</table>
   
  ';
  
  $pdf->set_datos_footer($footer);
  
  
ob_clean();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sivit by Valuarit');
$pdf->SetTitle('Factura');
$pdf->SetSubject('Factura');
//$pdf->SetKeywords('');



// set default header data
$logo = 'img/header_factura.jpg';
$title = "";
$subtitle = ""; 
//$pdf->SetHeaderData($logo, 190, $title, $subtitle, array(0,0,0), array(0,104,128));
$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 50, 20);
$pdf->SetHeaderMargin(10);
$pdf->SetFooterMargin(85);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
/*$lg = Array();
$lg['a_meta_charset'] = 'ISO-8859-1';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';
*/
$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set default font subsetting mode
//$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
/*
$html = <<<EOD
<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
EOD;
*/

$tbody = "";

$renglones = $datos_pdf["ComprobanteItem"];

$data = array();

foreach($renglones as $renglon){

   $descuento = ($renglon['descuento_unitario']*$renglon['precio_unitario'])/100;
   $precio_subtotal = ($renglon['precio_unitario'] - $descuento ) * $renglon['cantidad']*$datos_pdf["Comprobante"]["valor_moneda"];
   
   $tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
   $tbody .= "<td>" .$renglon['n_item'] . "</td>";
   $tbody .= "<td>" .$renglon['cantidad']. "</td>";
   $tbody .= "<td>" .$comprobante_item->getCodigoFromProducto($renglon) . "</td>";
   $tbody .= "<td>" .$renglon['item_observacion'] . "</td>";       
   $tbody .= "<td>" .$renglon['precio_unitario']*$datos_pdf["Comprobante"]["valor_moneda"]. "</td>";
   $tbody .= "<td>" .$renglon['Producto']['Iva']['d_iva'] . "</td>"; 
   $tbody .= "<td>" .money_format('%!n',$renglon['descuento_unitario']). "</td>";
   $tbody .= "<td>" .money_format('%!n', $precio_subtotal)."</td>";
   $tbody .= "</tr>";
}
 
//agrego las observaciones

$time = strtotime($datos_pdf["Comprobante"]["fecha_vencimiento"]);
$month_venc = date("m",$time);
$year_venc =  date("y",$time);
$day_venc =   date("d",$time);

$html='';


$html .=' 
<table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;font-family:\'Courier New\', Courier, monospace">

   
 
     <tbody>
     

      
     </tbody>
</table>


                  
                    <table width="100%" border="1"  style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
                  <tr  style="background-color:rgb(224, 225, 229);">
                    <td colspan="4"><div align="center"><strong>Cliente</strong></div></td>
                  </tr>
                  <tr>                                     
                    <td width="17%"><strong>Codigo:</strong> '.c.'</td>
                    <td width="35%"><strong>Raz&oacute;n Social:</strong> '.$datos_pdf["Persona"]["razon_social"].'</td>
                    <td width="26%"><strong>Cuit:</strong> '.$datos_pdf["Persona"]["cuit"].' </td>';
                    
              
                        $html.='<td width="22%"><strong>O/C:</strong> '.$datos_pdf["Comprobante"]["orden_compra"].'</td>';
                    
                    

                    $html.='</tr>
                              <tr>
                                <td colspan="3"><strong>Direccion:</strong> '.$datos_pdf["Persona"]["calle"].' - '.$datos_pdf["Persona"]["numero_calle"].' - '.$datos_pdf["Persona"]["localidad"].' - '.$datos_pdf["Persona"]["ciudad"].' - '.$datos_pdf["Persona"]["Provincia"]["d_provincia"].' - '.$datos_pdf["Persona"]["Pais"]["d_pais"].'  </td>
                                <td><strong>Teléfono:</strong> '.$datos_pdf["Persona"]["tel"].'</td>
                              </tr>
                    
                    <tr  style="background-color:rgb(224, 225, 229);">
                                    <td colspan="4"><div align="center"><strong>Condiciones de Venta</strong></div></td>
                    </tr>
                    <tr>
                    <td colspan="2"><div align="center"><strong>'.$datos_pdf["CondicionPago"]["d_condicion_pago"].'</strong></div></td>
                    <td colspan="2"><div align="center"><strong> Vencimiento: '.$day_venc.'-'.$month_venc.'-'.$year_venc.'</strong></div></td>
                    
                    </tr>
                     <tr  style="background-color:rgb(224, 225, 229);">
                                    <td colspan="4"><div align="center"><strong>Referencias</strong></div></td>
                    </tr>
                    <tr>
                    <td colspan="2"><div align="center"><strong>Remito</strong>'.$datos_pdf["CondicionPago"]["d_condicion_pago"].'</div></td>
                    <td colspan="2"><div align="center"><strong>Pedido</strong>'.$day_venc.'-'.$month_venc.'-'.$year_venc.'</div></td>
                    
                    
                    </tr>
                    
                    
                                 </table>


                  
              

<table width="100%" border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;font-family:\'Courier New\', Courier, monospace">

   
     <tbody>
     
        <tr >
        <td colspan="2"> </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
 
        <tr >
        <td  colspan="6" ><HR></td>
      
         </tr>
     </tbody>
</table>


<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">

    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>Item</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>Cant</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:15%;"><strong>C&oacute;digo</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:30%;"><strong>Descripci&oacute;n</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>P.Unitario</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>Iva(%)</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>Des.Unit(%).</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:15%;"><strong>Subtotal</strong></th>
    </tr>

  <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
    '.$tbody.'
  </tbody>  
</table>';
  
  
   
   
   
$pdf->writeHTML($html, true, false, true, false, true);


// QRCODE,L : QR-CODE Low error correction
// set style for barcode
$style = array(
    'border' => 2,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

//$pdf->Text(20, 25, 'QRCODE L');

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->SetBarcode(date("Y-m-d H:i:s", time()));
ob_clean();
$pdf->Output(str_replace(".","",$datos_pdf["TipoComprobante"]["abreviado_tipo_comprobante"].'_'.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'-'.$datos_pdf["Persona"]["razon_social"]).'.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
