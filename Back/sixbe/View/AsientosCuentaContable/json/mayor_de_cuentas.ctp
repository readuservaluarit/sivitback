<?php		

        include(APP.'View'.DS.'AsientosCuentaContable'.DS.'json'.DS.'asiento_cuenta_contable.ctp');
        
        
        $model["fecha"]["show_grid"] = 1;
        $model["fecha"]["header_display"] = "Fecha";
        $model["fecha"]["width"] = "10"; //REstriccion mas de esto no se puede achicar
        $model["fecha"]["order"] = "0";
        $model["fecha"]["text_colour"] = "#000000";
        
        
        $model["nro_asiento"]["show_grid"] = 1;
        $model["nro_asiento"]["header_display"] = "Asiento";
        $model["nro_asiento"]["width"] = "5"; //REstriccion mas de esto no se puede achicar
        $model["nro_asiento"]["order"] = "1";
        $model["nro_asiento"]["text_colour"] = "#000000";
        
         
        $model["codigo_cuenta_contable"]["show_grid"] = 1;
        $model["codigo_cuenta_contable"]["header_display"] = "C&oacute;digo";
        $model["codigo_cuenta_contable"]["width"] = "10"; //REstriccion mas de esto no se puede achicar
        $model["codigo_cuenta_contable"]["order"] = "2";
        $model["codigo_cuenta_contable"]["text_colour"] = "#000000";
            
        $model["d_asiento"]["show_grid"] = 1;
        $model["d_asiento"]["header_display"] = "Desc.";
        $model["d_asiento"]["width"] = "20"; //REstriccion mas de esto no se puede achicar
        $model["d_asiento"]["order"] = "3";
        $model["d_asiento"]["text_colour"] = "#000000";
        
        
        $model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["header_display"] = "Raz&oacute;n Social.";
        $model["razon_social"]["width"] = "25"; //REstriccion mas de esto no se puede achicar
        $model["razon_social"]["order"] = "4";
        $model["razon_social"]["text_colour"] = "#000000";

		$model["debe_monto"]["show_grid"] = 1;
        $model["debe_monto"]["header_display"] = "Debe";
        $model["debe_monto"]["width"] = "15"; //Restriccion mas de esto no se puede achicar
        $model["debe_monto"]["order"] = "5";
        $model["debe_monto"]["text_colour"] = "#0000FF";
		
		$model["haber_monto"]["show_grid"] = 1;
        $model["haber_monto"]["header_display"] = "Haber";
        $model["haber_monto"]["width"] = "15"; //Restriccion mas de esto no se puede achicar
        $model["haber_monto"]["order"] = "6";
        $model["haber_monto"]["text_colour"] = "#0000FF";
		
		$model["saldo_acumulado"]["show_grid"] = 1;
        $model["saldo_acumulado"]["header_display"] = "Saldo";
        $model["saldo_acumulado"]["width"] = "15"; //Restriccion mas de esto no se puede achicar
        $model["saldo_acumulado"]["order"] = "7";
        $model["saldo_acumulado"]["text_colour"] = "#0000FF";
		
		
		
		

         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "AsientoCuentaContable",
                    "content" => $model
                );
        
          echo json_encode($output);
?>