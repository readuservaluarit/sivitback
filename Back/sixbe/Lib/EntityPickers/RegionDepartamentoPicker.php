<?php

class RegionDepartamentoPicker extends EntityPicker{
    
    public $model = "RegionDepartamento";
    
    public $pickerTitle = "Selección de Departamento";
    
     public function getPickerJoins() {
        
            return array(array(
                                'alias' => 'Departamento',
                                'table' => 'departamento',
                                'type' => 'LEFT',
                                'conditions' => '`RegionDepartamento`.`id_departamento` = `Departamento`.`id`'
                                ));
        
    }
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "Departamento.id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Nombre", "field" => "Departamento.d_departamento", "width" => "90%", "keyOrder" => "d_departamento")
                                );

    public function getPickerConditions($busqueda){
        
        $pickerConditions = array('Departamento.d_departamento LIKE' => '%' . $busqueda['bus_unico'] . '%');
        
        return $pickerConditions;
        
        
    }
  /*
    public function getPickerInitialFilter(){
        
        //Filtra siempre por la Jurisdiccion del usuario si corresponde
        return DataFilter::getJurisdiccionPickerFilter();
        
    }
  */
  
}