DELIMITER $$

USE `ebsasql`$$

DROP TRIGGER /*!50032 IF EXISTS */ `inserta_persona`$$

CREATE
    /*!50017 DEFINER = 'root'@'%' */
    TRIGGER `inserta_persona` AFTER INSERT ON `persona_gp` 
    FOR EACH ROW 
BEGIN
  DECLARE persona_tipo VARCHAR (1) ;
  DECLARE persona_codigo INT (11) ;
  DECLARE id_tipo_persona_var INT (11) ;
  
  SET persona_tipo = 
  (SELECT 
    PERSONA_TIPO 
  FROM
    matpers_id 
  WHERE PERSONA_ID = NEW.id) ;
  SET persona_codigo = 
  (SELECT 
    PERSONA_COD 
  FROM
    matpers_id 
  WHERE PERSONA_ID = NEW.id) ;
  
  
  CASE
    WHEN persona_tipo = 'C' THEN 
	SET id_tipo_persona_var = 1 ;
    WHEN persona_tipo = 'P' THEN 
	SET id_tipo_persona_var = 2;
  END CASE;
  
  
  INSERT INTO valuarit_enerbom_test.`persona` (
    id,
    razon_social,
    cuit,
    id_tipo_documento,
    calle,
    numero_calle,
    piso,
    dpto,
    cod_postal,
    id_provincia,
    id_pais,
    id_tipo_iva,
    email,
    id_tipo_persona,
    id_migracion,
    id_persona_figura
  ) 
  VALUES
    (
      NEW.id,
      NEW.razon_social,
      NEW.cuit,
      3,
      NEW.calle,
      NEW.numero_calle,
      NEW.piso,
      NEW.depto,
      NEW.cod_postal,
      NEW.id_provincia,
      NEW.id_pais,
      NEW.id_tipo_iva,
      NEW.email,
      id_tipo_persona_var,
      persona_codigo,
      2
    );
END;
$$

DELIMITER ;