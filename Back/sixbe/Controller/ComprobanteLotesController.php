<?php



class ComprobanteLotesController extends AppController {
    public $name = EnumController::ComprobanteLotes;
    public $model = 'ComprobanteLote';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
 

protected  function RecuperoFiltros($model)
{
        //Recuperacion de Filtros
        $d_comprobante_lote = $this->getFromRequestOrSession('ComprobanteLote.d_comprobante_lote');
       
        $paginado = $this->getFromRequestOrSession('ComprobanteLote.paginado');
        
         if($paginado!="")
            $this->paginado = $paginado;
      
        $conditions = array(); 
        
         if($d_comprobante_lote!="")
            array_push($conditions, array('ComprobanteLote.d_comprobante_lote LIKE' => '%'.$id_comprobante.'%' ));
            
         
        return $conditions;
}    

   /**
    * @secured(CONSULTA_COMPROBANTE_LOTE)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
    
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model); 
    
        $this->paginate = array(
            'paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain'=> array('ComprobanteLoteEjecucion'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
    
        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        //Headers
        $formBuilder->addHeader('id', 'cotizacion.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'cotizacion.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
          
    else{ // vista json
    
        
        
        
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        
        
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    //fin vista json
    }
    
    
    
      /**
    * @secured(ADD_COMPROBANTE_LOTE)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_add = '';
            
            try{
                if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "El Lote ha sido creado exitosamente";
                    $tipo = EnumError::SUCCESS;
                    $id_add = $this->{$this->model}->id;
                   
                }else{
                    $mensaje = "Ha ocurrido un error,el lote NO ha podido ser creado.";
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el lote NO ha podido ser creado.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" => $id_add
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    
    
      /**
    * @secured(MODIFICACION_COMPROBANTE_LOTE)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                    
                    $mensaje = "El lote Ha sido modificado correctamente.";
                    $tipo = EnumError::SUCCESS;
                    
                }else{
                    $mensaje = "Ha ocurrido un error,el lote NO ha podido ser creado.";
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
              
                $mensaje = "Ha ocurrido un error,el lote NO ha podido ser creado.".$e->getMessage();
                $tipo = EnumError::ERROR; 
                
            }
          
        } 
        if($this->RequestHandler->ext == 'json'){  
            $output = array(
                "status" => $tipo,
                "message" =>$mensaje,
                "content" => ""
            ); 
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content"));
        }else{
            $this->Session->setFlash($mensaje, $tipo);
             $this->redirect(array('controller' => 'CotizacionItems', 'action' => 'index'));
        
        } 
      
    }
    
  
    
    

   
    
    
      /**
    * @secured(CONSULTA_COMPROBANTE_LOTE)
    */
    public function getModel($vista = 'default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        
       
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
        
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $this->set('model',$model);
       // $this->set('this',$this);
        Configure::write('debug',0);
        $this->render($vista);
    
        
    }
    
    
    /**
    * @secured(BAJA_COMPROBANTE_LOTE)
    */
    function delete($id) {
        
        $this->loadModel($this->model);
     
        $mensaje = "";
        $status = "";
        $this->ComprobanteLote->id = $id;
       try{  
           
            if ($this->ComprobanteLote->delete()) {
            
             
        
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Lote ha sido eliminado exitosamente.";
               
                
            }
                
            else{
                 //throw new Exception();
                  $status = EnumError::ERROR;
                  $mensaje = "El Lote ha sido eliminado exitosamente.";
            }
             $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                 
          }
          catch(Exception $ex)
          { 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
    }
    
    
    
    
    
    
    
   
    
    

    
    
    
   
    
}
?>