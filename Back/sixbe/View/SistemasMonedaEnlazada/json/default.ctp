<?php
        // CONFIG  CON MODE DISPLAYED CELLS
        
      
		$model["d_sistema"]["show_grid"] = 1;
        $model["d_sistema"]["type"] = EnumTipoDato::cadena;
        $model["d_sistema"]["header_display"] = "Sistema";
        $model["d_sistema"]["width"] = "20"; //REstriccion mas de esto no se puede achicar
        $model["d_sistema"]["order"] = "1";
        $model["d_sistema"]["text_colour"] = "#BB0000";
        
        
        $model["d_moneda_origen"]["show_grid"] = 1;
        $model["d_moneda_origen"]["type"] = EnumTipoDato::cadena;
        $model["d_moneda_origen"]["header_display"] = "Moneda Origen";
        $model["d_moneda_origen"]["width"] = "20"; //REstriccion mas de esto no se puede achicar
        $model["d_moneda_origen"]["order"] = "2";
        $model["d_moneda_origen"]["text_colour"] = "#0000BB";
		
		
		$model["d_moneda_enlazada"]["show_grid"] = 1;
        $model["d_moneda_enlazada"]["type"] = EnumTipoDato::cadena;
        $model["d_moneda_enlazada"]["header_display"] = "Moneda Enlazada.";
        $model["d_moneda_enlazada"]["width"] = "20"; //REstriccion mas de esto no se puede achicar
        $model["d_moneda_enlazada"]["order"] = "3";
        $model["d_moneda_enlazada"]["text_colour"] = "#9900BB";

		$output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "SistemaMonedaEnlazada",
                    "content" => $model
                );
        
          echo json_encode($output);
?>