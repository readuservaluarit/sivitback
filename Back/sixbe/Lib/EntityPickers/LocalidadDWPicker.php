<?php

class LocalidadDWPicker extends EntityPicker{
    public $model = "LocalidadDW";
    
    public $pickerTitle = "Selección de Localidad";
    
    public $pickerFields = array(
                                    array("title" => "Id", "field" => "id_localidad", "width" => "10%", "keyOrder" => "id_localidad"),
                                    array("title" => "Nombre", "field" => "nombre", "width" => "90%", "keyOrder" => "nombre")
                                );

    public function getPickerOrder() {
        return 'LocalidadDW.nombre';
    } 
                                    
    public function getPickerConditions($busqueda){
        $pickerConditions = array('LocalidadDW.nombre LIKE' => '%' . $busqueda['bus_unico'] . '%');
        
        return $pickerConditions;
    }
  /*
    public function getPickerInitialFilter(){
        
        //Filtra siempre por la Jurisdiccion del usuario si corresponde
        return DataFilter::getJurisdiccionPickerFilter();
        
    }
  */
  
}