<?php
App::uses('AppModel', 'ComprobanteItem');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class FacturaItem extends ComprobanteItem{


    
    public function setDecimalPlaces(){//configura los decimal places para este Model
    
     App::uses('Modulo', 'Model');
     $modulo_obj = new Modulo();
     $cantidad_decimales = $modulo_obj->getCantidadDecimalesFormateo(EnumModulo::VENTAS); 
     
     $this->decimal_places = array(
    "cantidad"=>$cantidad_decimales
    
    );
    
    }
    
    



}
