<?php   


App::import('Vendor','tcpdf/tcpdf'); 
App::import('Vendor','tcpdf/tcpdf_barcodes_1d'); 
App::import('Model','Comprobante');
App::import('Vendor','mytcpdf/mytcpdf'); 

	


$comprobante = new Comprobante();


// create new PDF document
$fecha_entrega = new DateTime($datos_pdf['Comprobante']['fecha_contable']);
$fecha_inicio_actividad = new DateTime($datos_empresa["DatoEmpresa"]["fecha_inicio_actividad"]);

$html = '<table width="100%" border="0" cellspacing="0" style="font-size:10px;border:2px solid #000000; font-family:\'Courier New\', Courier, monospace">
          <tr>
            <td style="padding: 5px 5px 5px 5px;" width="37%" rowspan="4" align="center" valign="center"  ><img src="'.$datos_empresa["DatoEmpresa"]["app_path"].'/img/'.$datos_empresa["DatoEmpresa"]["id"].'.png" width="476" height="129" />  </td>
            <td width="8%" height="3%" rowspan="2"  style="border:0px solid #000000;">
                <div align="center" style="font-size:30px">X</div>
                 <span align="center" style="font-size:10px;"></span>
            </td>
            <td width="55%" style="padding-left:5px; font-size:17px;font-weight:bold;"> '.strtoupper($datos_pdf["ComprobanteReferencia"]["TipoComprobante"]['d_tipo_comprobante']).': </td>
            
          </tr>
          <tr>
            <td style="padding-left:5px; font-size:15px;"> Nro '.$datos_pdf["ComprobanteReferencia"]["nro_comprobante_completo"].'</td>
          </tr> 
          
          <tr>
            <td width="8%" height="2%" align="center" valign="middle" >&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr >
            <td  width="8%" height="5%" align="center" valign="middle" >&nbsp;</td>
            <td style="padding-left:5px;"> Original Blanco / Copia color </td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["calle"].' '.$datos_empresa["DatoEmpresa"]["numero"].' '.$datos_empresa["DatoEmpresa"]["codigo_postal"].' '.$datos_empresa["Provincia"]["d_provincia"].'</td>
            <td rowspan="5">&nbsp;</td>
            <td style="padding-left:5px;"> Fecha: '.$fecha_entrega->format('d-m-Y').'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> E-mail: '.$datos_empresa["DatoEmpresa"]["email"].'</td>
            <td style="padding-left:5px;"> CUIT: '.$datos_empresa["DatoEmpresa"]["cuit"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> Tel: '.$datos_empresa["DatoEmpresa"]["tel"].'</td>
            <td style="padding-left:5px;"> Ingresos Brutos: '.$datos_empresa["DatoEmpresa"]["iibb"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["website"].'</td>
            <td style="padding-left:5px;"> Inicio de actividades: '.$fecha_inicio_actividad->format('d-m-Y').'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.strtoupper($datos_empresa["TipoIva"]["d_tipo_iva"]).'</td>
            <td style="padding-left:5px;">&nbsp;</td>
          </tr>
        </table>';

$html;




$pdf = new MyTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
     
$pdf->set_datos_header($html);

 







$impuestos = '';







   
   $html .= "<HR>";
   
   

   
   
   $html .= '
   
   <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody>
</table>
   
  ';
  
  
  
  
  
ob_clean();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sivit by Valuarit');
$pdf->SetTitle('Factura');
$pdf->SetSubject('Factura');
//$pdf->SetKeywords('');

                              

// set default header data
$logo = 'img/header_factura.jpg';
$title = "";
$subtitle = ""; 
//$pdf->SetHeaderData($logo, 190, $title, $subtitle, array(0,0,0), array(0,104,128));
$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 50, 20);
$pdf->SetHeaderMargin(10);
$pdf->SetFooterMargin(65);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
/*$lg = Array();
$lg = Array();
$lg['a_meta_charset'] = 'ISO-8859-1';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';
*/
$pdf->setLanguageArray($lg);
    
// ---------------------------------------------------------

// set default font subsetting mode
//$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
/*
$html = <<<EOD
<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
EOD;
*/

$tbody = "";

   

$data = array();


  
  
 
   $tbody .= "<tr>";
   $tbody .= "<td>" .$datos_pdf['Impuesto']['d_impuesto'] . "</td>";
   $tbody .= "<td align='center'>" .$datos_pdf["ComprobanteImpuesto"]["base_imponible"]. "</td>";    
   $tbody .= "<td>" .$datos_pdf["ComprobanteImpuesto"]["tasa_impuesto"]. "</td>"; 
   $tbody .= "<td>" .$datos_pdf["ComprobanteImpuesto"]["importe_impuesto"]. "</td>";
   $tbody .= "</tr>";
 
 
 
//agrego las observaciones

$time = strtotime($datos_pdf["Comprobante"]["fecha_vencimiento"]);
$month_venc = date("m",$time);
$year_venc =  date("y",$time);
$day_venc =   date("d",$time);


App::import('Vendor', 'NumberToLetterConverter', array('file' => 'Classes/NumberToLetterConverter.class.php'));   
$numero_conversion = new NumberToLetterConverter();
$letras_total =  "Importe en letras: ".$datos_pdf["Moneda"]["simbolo_internacional"]." ".$numero_conversion->to_word(str_replace(".",",",$datos_pdf["ComprobanteImpuesto"]["importe_impuesto"]),$datos_pdf["Moneda"]["simbolo_internacional"]);



$footer='
  <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="6">
        '.$letras_total.'
        </td>                     
       
        </tr>
     </tbody> 
</table>


<table width="100%" border="1" style="width:100%;">
   <tr>
     <td width="60%">Observaciones: '.$datos_pdf["Comprobante"]["observacion"].' <br/>  '.$datos_pdf["ComprobanteReferencia"]["TipoComprobante"]["impresion_observacion_extra"].'</td>
     <td width="40%">
     <table width="100%" border="0">
       
       <tr>
         <td><strong>Total impuesto retenido</strong></td>
         <td>&nbsp;</td>
         <td><strong>'.$datos_pdf["Comprobante"]["Moneda"]["simbolo_internacional"].' '.$datos_pdf["ComprobanteImpuesto"]["importe_impuesto"].'</strong></td>
       </tr>
     </table></td>
   </tr>
</table>
';
  $pdf->set_datos_footer($footer);




$html='';


$html .=' 
  <table width="100%"   style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
  <tr >
                    <td colspan="4"></td>
                  </tr>
  <tr>  
  <tr >
                    <td colspan="4"></td>
                  </tr>
  <tr>  

 
 </table>


                  
                    <table width="100%" border="1"  style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
                  <tr  style="background-color:rgb(224, 225, 229);">
                    <td colspan="4"><div align="center"><strong>Proveedor</strong></div></td>
                  </tr>
                  <tr>                                     
                    <td width="17%"><strong>Codigo:</strong> '.$datos_pdf["ComprobanteReferencia"]["Persona"]["codigo"].'</td>
                    <td width="35%"><strong>Raz&oacute;n Social:</strong> '.$datos_pdf["ComprobanteReferencia"]["Persona"]["razon_social"].'</td>
                    <td width="48%" colspan="2"><strong>CUIT:</strong> '.$datos_pdf["ComprobanteReferencia"]["Persona"]["cuit"].' </td>';
                    
              
                     
                    
                    

                    $html.='</tr>
                              <tr>
                                <td colspan="3"><strong>Direccion:</strong> '.$datos_pdf["ComprobanteReferencia"]["Persona"]["calle"].' - '.$datos_pdf["ComprobanteReferencia"]["Persona"]["numero_calle"].' - '.$datos_pdf["ComprobanteReferencia"]["Persona"]["localidad"].' - '.$datos_pdf["ComprobanteReferencia"]["Persona"]["ciudad"].' - '.$datos_pdf["ComprobanteReferencia"]["Persona"]["Provincia"]["d_provincia"].' - '.$datos_pdf["ComprobanteReferencia"]["Persona"]["Pais"]["d_pais"].'  </td>
                                <td><strong>Teléfono:</strong> '.$datos_pdf["ComprobanteReferencia"]["Persona"]["tel"].'</td>
                              </tr>
                    
                   
                    
                    
                                 </table>


                  
              

<table width="100%" border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;font-family:\'Courier New\', Courier, monospace">

   
     <tbody>
     
        <tr >
        <td colspan="2"> </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
 
        <tr >
        <td  colspan="6" ><HR></td>
      
         </tr>
     </tbody>
</table>


<table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:11px;">

    <tr style="background-color:rgb(224, 225, 229);">
        <th class="sorting"  rowspan="1" colspan="1" style="width:20%;">Concepto</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:30%;">Monto Imponible</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:20%;">Alicuota</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:30%;">Monto Retenido</th>
    </tr>

  <tbody>
    '.$tbody.'
  </tbody>  
</table>';
  
  
   
   
   
$pdf->writeHTML($html, true, false, true, false, true);


// QRCODE,L : QR-CODE Low error correction
// set style for barcode
$style = array(
    'border' => 2,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

//$pdf->Text(20, 25, 'QRCODE L');

// ---------------------------------------------------------



$pdf_name = $comprobante->getNamePDFRetencion($datos_pdf);
$root_pdf = $comprobante->getRootPDF();



if($datos_pdf["ComprobanteReferencia"]["TipoComprobante"]["adjunta_pdf_mail"] == 1) {

	$path_para_pdf = $datos_empresa["DatoEmpresa"]["local_path"].$root_pdf;
          	
	$pdf->Output($path_para_pdf.$pdf_name.'.pdf', 'F');//Esta linea guarda un archivo en path_para_pdf
}





// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->SetBarcode(date("Y-m-d H:i:s", time()));
ob_clean();


$pdf->Output($pdf_name.'.pdf', 'D');





//============================================================+
// END OF FILE
//============================================================+
