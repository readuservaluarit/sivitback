<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class RRHHTipoConcepto extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'rrhh_tipo_concepto';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	
	


}
