<?php

/**
* @secured(ABM_ROLES)
*/
class RolesController extends AppController {
    public $name = 'Roles';
    public $model = 'Rol';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
  
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        $codigo = "";
        $descripcion = "";
        
        //Recuperacion de Filtros
        $codigo = $this->getFromRequestOrSession('Rol.codigo');
        $descripcion = $this->getFromRequestOrSession('Rol.descripcion');
        
        $conditions = array(); 

        if($codigo!=""){
            array_push($conditions, array('Rol.codigo LIKE' => '%' . $codigo  . '%')); 
        }
             
            
        if($descripcion!=""){
            array_push($conditions, array('Rol.descripcion LIKE' => '%' . $descripcion  . '%'));
        }

        
        $this->paginate = array(
            'paginado'=>0,
            'conditions' => $conditions,
            'limit' => 10,
            'page' => $this->getPageNum()
        );
      
      
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
        $formBuilder->setDataListado($this, 'Listado de Roles', 'Datos de las Roles', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        //Filters
        $formBuilder->addFilterBeginRow();
        $formBuilder->addFilterInput('codigo', 'Código', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $codigo));
        $formBuilder->addFilterInput('descripcion', 'Descripción', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $descripcion));
        $formBuilder->addFilterEndRow();

        //Headers
        $formBuilder->addHeader('Id', 'Rol.id', 10);
        $formBuilder->addHeader('C&oacute;digo', 'Rol.codigo', "30%");
        $formBuilder->addHeader('Descripci&oacute;n', 'Rol.descripcion', "60%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'codigo');
        $formBuilder->addField($this->model, 'descripcion');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    }
    
    
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->Rol->id = $id;
            $this->request->data = $this->Rol->read();
            
            // Cargo los campos calculados
            $nivelAut = $this->request->data['NivelAutorizadoInformacion']['id'];
            $asigGeo = $this->request->data['AsignacionGeografica']['id'];
            
            $funciones = array();
            foreach($this->request->data['Funcion'] as $funcion) {
                array_push($funciones, $funcion['id']);
            }
        }
        else
        {
            // Cargo los campos calculados
            $nivelAut = '';
            $asigGeo = 1;
            $funciones = array();
        }
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Roles');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Rol');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Rol');
        else
            $formBuilder->setTituloForm('Consulta de Rol');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields
        $formBuilder->addFormInput('codigo', 'C&oacute;digo', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('descripcion', 'Descripci&oacute;n', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        
        $nivelesAut = $this->Rol->NivelAutorizadoInformacion->find('list', array('fields' => array('NivelAutorizadoInformacion.id', 'NivelAutorizadoInformacion.nombre'), 'conditions' => array('NivelAutorizadoInformacion.id != ' => 7)));
        $formBuilder->addFormInput('id_nivel_autorizado_informacion', 'Nivel Autorizado Informaci&oacute;n', array('class'=>'control-group'), array('class' => 'required', 'label' => false, 'options' => $nivelesAut, 'empty' => false, 'default' => $nivelAut));
        
        //Traigo hasta lote y ninguno
        $asigGeoOpt = $this->Rol->NivelAutorizadoInformacion->find('list', array('fields' => array('NivelAutorizadoInformacion.id', 'NivelAutorizadoInformacion.nombre'), 'conditions' => array('OR' => array('NivelAutorizadoInformacion.id < ' => 5, 'NivelAutorizadoInformacion.id = ' => 7))));
        
        $formBuilder->addFormInput('asignacion_geografica', 'Requerimiento de asignaci&oacute;n geogr&aacute;fica', array('class'=>'control-group'), array('class' => 'required', 'label' => false, 'legend' => false, 'type' => 'radio', 'separator' => '<br />', 'options' => $asigGeoOpt, 'empty' => false, 'default' => $asigGeo));
        
        $funcionOpt = $this->Rol->Funcion->find('list', array('fields' => array('Funcion.id', 'Funcion.d_funcion')));
        $formBuilder->addFormInput('Funcion.id', 'Funciones', array('class'=>'control-group'), array('class' => 'required', 'label' => false, 'multiple' => true, 'options' => $funcionOpt, 'empty' => false, 'default' => $funciones));
        
        $formBuilder->addFormInput('solo_lectura', 'Solo Lectura', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false, 'type' => 'checkbox'));
        
        $script = "
            $(function() {
            
                 var options = {multiple: true,header: '',noneSelectedText: false,selectedList: 0, width: '400px;'};
                 var optionsfilter = {label: 'Buscar', width:'400px;', placeholder: ''};
                 sWidget = $('#FuncionId').multiselect(options).multiselectfilter(optionsfilter);
            
                /*sWidget = $('#FuncionId').multiselect({ 
                    header: false,
                    beforeopen: function() {
                        return false;
                    },
                    beforeclose: function() {
                        return false;
                    }
                });
                
                $('#FuncionId').parent().append(sWidget.multiselect('widget').show().css('position','relative'));
                $('#FuncionId').parent().children('button').hide()
                */
            });
            
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
                
                var form = 'RolAbmForm';
                
                var validator = new Validator(form);
                
                validator.validateRequired('RolCodigo', 'Debe ingresar un c&oacute;digo');
                validator.validateRequired('RolDescripcion', 'Debe ingresar una descripci&oacute;n');
                
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                
                return true;
            } 
            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    public function add() {
        if (!$this->request->is('get')){
            $this->loadModel($this->model);
            
            $funciones = array();
            if (is_array($this->request->data['Funcion']['id'])) {
                foreach($this->request->data['Funcion']['id'] as $funcion) {
                    array_push($funciones, $funcion);
                }
            }
            unset($this->request->data['Funcion']);
            $this->request->data['Funcion']['Funcion'] = $funciones;
            
            try {
                if ($this->Rol->saveAll($this->request->data))
                    $this->Session->setFlash('El rol ha sido creado exitosamente.', 'success');
                else
                    $this->Session->setFlash('Ha ocurrido un error, el rol no ha sido creado.', 'error');
            } catch(Exception $ex) {
                $this->Session->setFlash($ex->getMessage(), 'error');
            }
            
            $this->redirect(array('action' => 'index'));
        } 
        
        $this->redirect(array('action' => 'abm', 'A'));                   
    }
  
    function edit($id) {
        if (!$this->request->is('get')){
            $this->loadModel($this->model);
            $this->Rol->id = $id;
            
            $funciones = array();
            if (is_array($this->request->data['Funcion']['id'])) {
                foreach($this->request->data['Funcion']['id'] as $funcion) {
                    array_push($funciones, $funcion);
                }
            }
            unset($this->request->data['Funcion']);
            $this->request->data['Funcion']['Funcion'] = $funciones;
            
            try {
                if ($this->Rol->saveAll($this->request->data))
                    $this->Session->setFlash('El rol ha sido modificado exitosamente.', 'success');
                else
                    $this->Session->setFlash('Ha ocurrido un error, el rol no ha sido modificado.', 'error');
            } catch(Exception $ex) {
                $this->Session->setFlash($ex->getMessage(), 'error');
            }
            
            $this->redirect(array('action' => 'index'));
        } 
        
        $this->redirect(array('action' => 'abm', 'M', $id));
    }
  
    function delete($id) {
        $this->loadModel($this->model);

        try {
            if ($this->Rol->delete($id)) 
                $this->Session->setFlash('El rol ha sido eliminado exitosamente.', 'success');
            else
                $this->Session->setFlash('Ha ocurrido un error, el rol no ha podido eliminarse.', 'error');
        } catch(Exception $ex) {
            $this->Session->setFlash($ex->getMessage(), 'error');
        }

        $this->redirect(array('action' => 'index'));
    }
}
?>