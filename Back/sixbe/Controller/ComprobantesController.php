
<?php

class ComprobantesController extends AppController
{
    
public $model = "Comprobante";
public $error = 0;//indica si hay error en el add o en el edit.
public $id_asiento = 0;//contiene el numero de asiento que se genera en el edit
public $anulo = 0;//es un flag que se usa para cuando el usuario anula desde adentro del form
public $llamada_interna = 0;
public $array_filter_names = array();
public $output_pdf_file = 3;//se usa para cuando se llama a generarPDF



public function index(){

    echo "Comprobante index"; // CANUTERO... CANUTERO DE FUNCIONALIDADES! 
    die();
}

//getTiposComprobanteFromController
public  function RecuperoFiltros($model)
{
    //Recuperacion de Filtros
        $id_comprobante = $this->getFromRequestOrSession($model.'.id');
        $fecha_generacion = $this->getFromRequestOrSession($model.'.fecha_generacion');
        $fecha_generacion_hasta = $this->getFromRequestOrSession($model.'.fecha_generacion_hasta');
        $nro_comprobante = trim(ltrim($this->getFromRequestOrSession($model.'.nro_comprobante'),'0'));//le quito los ceros de adelante
        $id_persona = $this->getFromRequestOrSession($model.'.id_persona');
        $razon_social = $this->getFromRequestOrSession($model.'.persona_razon_social');
        $id_estado = $this->getFromRequestOrSession($model.'.id_estado_comprobante');
        $id_estado_comprobante_excluir = $this->getFromRequestOrSession($model.'.id_estado_comprobante_excluir');
        $fecha_contable = $this->getFromRequestOrSession($model.'.fecha_contable');
        $fecha_contable_hasta = $this->getFromRequestOrSession($model.'.fecha_contable_hasta');
        $id_tipo_comprobante = $this->getFromRequestOrSession($model.'.id_tipo_comprobante');
        $aprobado = $this->getFromRequestOrSession($model.'.aprobado');
        $id_transportista = $this->getFromRequestOrSession($model.'.id_transportista');
        $fecha_vencimiento = $this->getFromRequestOrSession($model.'.fecha_vencimiento');
        $fecha_vencimiento_hasta = $this->getFromRequestOrSession($model.'.fecha_vencimiento_hasta');
        $fecha_entrega_desde = $this->getFromRequestOrSession($model.'.fecha_entrega_desde');
        $fecha_entrega_hasta = $this->getFromRequestOrSession($model.'.fecha_entrega_hasta');
        $id_punto_venta = $this->getFromRequestOrSession($model.'.id_punto_venta');
        $d_punto_venta = trim(ltrim($this->getFromRequestOrSession($model.'.d_punto_venta'),'0'));//le quito los ceros y los espacios de adelante
        $orden_compra_externa = $this->getFromRequestOrSession($model.'.orden_compra_externa');
        $id_detalle_tipo_comprobante = $this->getFromRequestOrSession($model.'.id_detalle_tipo_comprobante');
        $id_cartera_rendicion = $this->getFromRequestOrSession($model.'.id_cartera_rendicion');
        
        $fecha_aprobacion = $this->getFromRequestOrSession($model.'.fecha_aprobacion');
        $subtotal_neto_desde = $this->getFromRequestOrSession($model.'.subtotal_neto_desde');
        $subtotal_neto_hasta = $this->getFromRequestOrSession($model.'.subtotal_neto_hasta');
        $persona_codigo = $this->getFromRequestOrSession('Persona.persona_codigo');
        $id_usuario = $this->getFromRequestOrSession('Comprobante.id_usuario');
        $inspeccion = $this->getFromRequestOrSession('Comprobante.inspeccion');
        $certificados = $this->getFromRequestOrSession('Comprobante.certificados');
        $meli_nickname = $this->getFromRequestOrSession('Persona.meli_nickname');
        
        
        $conditions = array(); 

        if($id_tipo_comprobante!=""){
        	$tipos_comp = explode(',', $id_tipo_comprobante);
        	$this->id_tipo_comprobante = $tipos_comp;
        }
            
        array_push($conditions, array($model.'.activo =' => 1)); //solo trae las visibles

        if($fecha_generacion!=""){
         array_push($conditions, array('DATE('.$model.'.fecha_generacion) >=' => $fecha_generacion)); //MP: Si la fecha de bd tiene hh:mm:ss distintos de 00:00:00 la compoaracion no anda
         array_push($this->array_filter_names,array("Fecha Generaci&oacute;n Dsd.:"=>$this->{$this->model}->formatDate($fecha_generacion)));
      
       }
         
         
         if($fecha_generacion_hasta!=""){
         	array_push($conditions, array('DATE('.$model.'.fecha_generacion) <=' => $fecha_generacion_hasta)); //MP: Si la fecha de bd tiene hh:mm:ss distintos de 00:00:00 la compoaracion no anda
         	array_push($this->array_filter_names,array("Fecha Generaci&oacute;n Hta.:"=>$this->{$this->model}->formatDate($fecha_generacion_hasta)));
         }	
         	
        if($aprobado!="")
            array_push($conditions, array($model.'.aprobado' => $aprobado));
        
            
        if($inspeccion!="")
            array_push($conditions, array($model.'.inspeccion' => $inspeccion));
            
            
        if($certificados!="")
        	array_push($conditions, array($model.'.certificados' => $certificados));
            	
            
        if($nro_comprobante!=""){
            $nros_comprobante = explode(',', $nro_comprobante);
            array_push($conditions, array($model.'.nro_comprobante' => $nros_comprobante));
            array_push($this->array_filter_names,array("Nro Comprobante/s:"=>$nros_comprobante));
        }    
        
        
        
        if($this->Auth->user('id_persona')>0){
        	
        	$id_persona = $this->Auth->user('id_persona');
        	
        }
        
        
        if($id_persona!=""){
            array_push($conditions, array($model.'.id_persona' => $id_persona));
            array_push($this->array_filter_names,array("Id Persona:"=>$id_persona));
        }
        /*
        $razon_social="";
        if($razon_social!=""){
           // array_push($conditions, array('LOWER(Persona.razon_social) LIKE' => '%' . $razon_social  . '%')); 
           // array_push($conditions, array('LOWER(Comprobante.razon_social) LIKE' => '%' . $razon_social  . '%')); 
        } 
        */   
        if($id_estado!=""){
        	$this->loadModel("EstadoComprobante");
        	
        	if($this->Auth->user('id_persona')>0){ 
            	array_push($conditions, array($model.'.id_estado_comprobante' => array(EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::CerradoConCae,EnumEstadoComprobante::Cumplida)));
        	}else{
        		
        		array_push($conditions, array($model.'.id_estado_comprobante' => $id_estado));
        	}
        	
            array_push($this->array_filter_names,array("Estado.:"=>$this->EstadoComprobante->getDescripcion($id_estado)));
        }
            
        if( $id_estado_comprobante_excluir !="" ){            
            
             $estados_excluir = explode(",", $id_estado_comprobante_excluir);
            array_push($conditions,array( "NOT" => (array( 'Comprobante.id_estado_comprobante' => $estados_excluir  )) )); 
        } 
        
  
            
        if($id_transportista!="")
            array_push($conditions, array($model.'.id_transportista' => $id_transportista));                 
            
        if($fecha_contable!=""){
        	
        	$exp = explode('/', $fecha_contable);//el  front web me envia guiones
        	
        	if(strpos($fecha_contable_hasta, '/')!== false)
        		$fecha_contable = $exp[2].'-'.$exp[1].'-'.$exp[0];
        	
        	
           array_push($conditions, array('DATE('.$model.'.fecha_contable) >= ' =>  $fecha_contable));
        	
            array_push($this->array_filter_names,array("Fecha Contable Dsd:"=>$this->{$this->model}->formatDate($fecha_contable)));
            
            
        }
            
        if($fecha_contable_hasta!=""){
        	
        	$exp = explode('/', $fecha_contable_hasta);//el  front web me envia guiones
        	
    
        	if(strpos($fecha_contable_hasta, '/')!== false)
        		$fecha_contable_hasta= $exp[2].'-'.$exp[1].'-'.$exp[0];
        		
        		
        		
        	array_push($conditions, array('DATE('.$model.'.fecha_contable) <= ' => $fecha_contable_hasta));
        	array_push($this->array_filter_names,array("Fecha Contable Hta.:"=>$this->{$this->model}->formatDate($fecha_contable_hasta)));
        }
            	
            	
        
        if($id_comprobante!=""){
        	$ids_comprobante = explode(',', $id_comprobante);
        	array_push($conditions, array($model.'.id' => $ids_comprobante));
            array_push($this->array_filter_names,array("Id Comp.:"=>$id_comprobante));
        }
        
            
            
        if($razon_social!="" && $id_persona == ""){
            array_push($conditions, array('Comprobante.razon_social LIKE ' => '%' .$razon_social.'%')); 
            array_push($this->array_filter_names,array("Raz&oacute;n Social:"=>$razon_social));
       } 
       
        if($orden_compra_externa !=""){
            array_push($conditions, array('Comprobante.orden_compra_externa LIKE ' => '%' .$orden_compra_externa.'%'));
            array_push($this->array_filter_names,array("OC Externa:"=>$orden_compra_externa));
       } 
       
       
        if($fecha_entrega_desde!=""){
            array_push($conditions, array('DATE('.$model.'.fecha_entrega) >= ' =>  $fecha_entrega_desde ));  
            array_push($this->array_filter_names,array("Fcha Entrega Dsd.:"=>$this->{$this->model}->formatDate($fecha_entrega_desde)));
        }
         
        if($fecha_entrega_hasta!=""){
            array_push($conditions, array('DATE('.$model.'.fecha_entrega) <= ' =>  $fecha_entrega_hasta ));  
            array_push($this->array_filter_names,array("Fcha Entrega Hta.:"=>$this->{$this->model}->formatDate($fecha_entrega_hasta)));
        }
            
            
        if($subtotal_neto_desde!=""){
            array_push($conditions, array($model.'.subtotal_neto >= ' =>  $subtotal_neto_desde ));    
            array_push($this->array_filter_names,array("Sbtotal Neto Dsd.:"=>$subtotal_neto_desde));
        }
         
        if($subtotal_neto_hasta!=""){
            array_push($conditions, array($model.'.subtotal_neto <= ' =>  $subtotal_neto_hasta ));
            array_push($this->array_filter_names,array("Sbtotal Neto Hta.:"=>$subtotal_neto_hasta));
        }
            
                 
        
        if($fecha_vencimiento!=""){
            array_push($conditions, array('DATE('.$model.'.fecha_vencimiento) >= ' =>  $fecha_vencimiento ));  
            array_push($this->array_filter_names,array("Fecha Vto. Dsd.:"=>$this->{$this->model}->formatDate($fecha_vencimiento)));
        }
            
        if($fecha_vencimiento_hasta!=""){
        	array_push($conditions, array('DATE('.$model.'.fecha_vencimiento) <= ' =>  $fecha_vencimiento_hasta));  
        	array_push($this->array_filter_names,array("Fecha Vto. Hta.:"=>$this->{$this->model}->formatDate($fecha_vencimiento_hasta)));
        }
        
            
        if($fecha_aprobacion!=""){
            array_push($conditions, array('DATE('.$model.'.fecha_aprobacion) LIKE' =>  '%' .$fecha_aprobacion.'%' ));
            array_push($this->array_filter_names,array("Fecha Aprob.:"=>$this->{$this->model}->formatDate($fecha_aprobacion)));
        }
            
       if($id_punto_venta!=""){
            array_push($conditions, array('Comprobante.id_punto_venta' =>  $id_punto_venta ));  
            array_push($this->array_filter_names,array("ID Pto Vta.:"=>$id_punto_venta));
            
       }
                   
       if($d_punto_venta!=""){
            array_push($conditions, array('Comprobante.d_punto_venta LIKE' =>   '%' .$d_punto_venta.'%' ));  
            array_push($this->array_filter_names,array("Desc Pto Vta.:"=>$d_punto_venta));
       }
            
       if($id_detalle_tipo_comprobante!=""){
            array_push($conditions, array('Comprobante.id_detalle_tipo_comprobante' =>  $id_detalle_tipo_comprobante )); 
            array_push($this->array_filter_names,array("ID Detall. Tpo Comp.:"=>$id_detalle_tipo_comprobante));
       }
            
       if($id_cartera_rendicion!="")
            array_push($conditions, array('Comprobante.id_cartera_rendicion' =>  $id_cartera_rendicion ));
       
       if($persona_codigo !=""){
       	array_push($conditions, array('Persona.codigo ' => $persona_codigo));
       	array_push($this->array_filter_names,array("Cod Pers.:"=>$persona_codigo));
       } 
       
       
       if($id_usuario!=""){
       	$this->loadModel("Usuario");
       	array_push($conditions, array('Comprobante.id_usuario' =>  $id_usuario));
       	array_push($this->array_filter_names,array("Usuario:"=>$this->Usuario->getNombreYApellido($id_usuario)));
       }
       
       
       
       
       if($meli_nickname!=""){
       	array_push($conditions, array('Persona.meli_nickname LIKE' =>   '%' .$meli_nickname.'%' ));  
       	array_push($this->array_filter_names,array("Meli Nombre Usuario:"=> $meli_nickname));
       }
       
       
       
       
       /*Seguridad Usuario B2B*/
       if($this->Auth->user('id_persona')>0 && $this->Auth->user('id_rol') == EnumRol::SivitB2B){
       	
       	$id_usuario_b2b = $this->Auth->user('id');
       	$id_persona = $this->Auth->user('id_persona');
       	array_push($conditions, array('Comprobante.id_usuario' => $id_usuario_b2b));
       	array_push($conditions, array('Comprobante.id_persona' => $id_persona));
       	
       }
       
     
            
       array_push($conditions, array($model.'.id_tipo_comprobante'=> $this->id_tipo_comprobante)); 
       
       
        return $conditions;
}



protected function add() {
        if ($this->request->is('post') || $this->llamada_interna == 1  ){
            $this->loadModel($this->model);
            $id_c = "";
            $nro_c = "";
            
            $id_sistema = $this->Comprobante->TipoComprobante->getSistema($this->request->data[$this->model]["id_tipo_comprobante"]);
            
            if($id_sistema == EnumSistema::VENTAS)//solo en ventas piso la fecha de generacion
            	$this->request->data[$this->model]["fecha_generacion"] = date("Y-m-d H:i:s");
            
            
            $this->request->data[$this->model]["fecha_add"] = date("Y-m-d H:i:s");
            $this->request->data[$this->model]["id_usuario"] = $this->Auth->user('id'); 
            $datoEmpresa = $this->Session->read('Empresa');
            $id_dato_empresa  = $datoEmpresa["DatoEmpresa"]["id"];
            $this->request->data[$this->model]["id_dato_empresa"] = $id_dato_empresa;
            $id_asiento = '';
            $message = '';
            $nro_cae = '';
            $comprobante = array();
			
            //$this->request->data[$this->model]["id_tipo_comprobante"] = $this->id_tipo_comprobante;
            //antes de impactar chequeo si tiene un contador asociado
            $error = false;
            if( isset($this->request->data[$this->model]["id_punto_venta"]) ){
                
            	$id_punto_venta = $this->request->data[$this->model]["id_punto_venta"];
            	
            	//$error = $this->ComprobantePuntoVentaNumero->ChequeoTipo($this->request->data,$this->model);//Cheque el tipo de comprobante punto venta numero
            }else
                $id_punto_venta = '';
            
            $contador = $this->chequeoContador($this->request->data[$this->model]["id_tipo_comprobante"],$id_punto_venta);
            
            if($contador){
                
				$ds = $this->{$this->model}->getdatasource();
                 
            try{
                $ds->begin(); 
                if(!isset($this->request->data[$this->model]['id'])){
                    
                    
                    $this->Comprobante->CalcularMonedaBaseSecundaria($this->request->data,$this->model);
                    
                    $error = $this->borrarValoresMontoCero($error,$message);
                    
                    if($error == 1){ //si hubo un error agregando algun Item del comprobante entonces muestro errores
                    	
                    	throw new Exception($message);
                    }
                    
                    
                    $this->guardaCodigoProducto();//esta funcion se hizo por ProductoCodigo, pero igual sirve por si le cambian un codigo a un producto.
                    
                    $comprobante = $this->{$this->model}->save($this->request->data);
                    
                    
                    
                    if (!empty($comprobante)) {
                        $nro_comprobante = 0;
                        
                        
                        if(isset($this->request->data[$this->model]["id_punto_venta"]))
                        	$id_pto_venta = $this->request->data[$this->model]["id_punto_venta"];
                        else 
                        	$id_pto_venta = '';
                        
                        
                        
                        
                        	if( ($this->id_tipo_comprobante >0 && $this->NecesitaNroComprobante($this->request->data[$this->model]["id_tipo_comprobante"],$id_pto_venta))){
                            
                            $nro_comprobante = $this->obetener_ultimo_numero_comprobante($this->request->data[$this->model]["id_tipo_comprobante"],$id_punto_venta,5,"+");
                            
                        }else{
                        	
                        	
                            /*No necesito que sea autocalculado por ende utilizo el que me pasan por el request*/
                            $nro_comprobante = $this->request->data[$this->model]["nro_comprobante"];
                            
                            if($this->Comprobante->TipoComprobante->requiereContador($this->request->data[$this->model]["id_tipo_comprobante"]) == 1   &&         $nro_comprobante == "" && $this->request->data[$this->model]["id_estado_comprobante"] != EnumEstadoComprobante::CerradoConCae){// No necesita pero no me manda el numero. Entra a este if proque tiene que ver el if de arriba tambien
                                $nro_comprobante = $this->obetener_ultimo_numero_comprobante($this->request->data[$this->model]["id_tipo_comprobante"],$id_punto_venta,5,"+");
                            }
                        }
                        
                        if(isset($this->request->data[$this->model]["id_otro_sistema"])) // Si envio el id desde otro sistema entonces tomo ese. VER AFTER INSERT COMPROBANTE
                          $this->{$this->model}->id = $this->request->data[$this->model]["id_otro_sistema"];
    
                            
                        $id_c = $this->{$this->model}->id;
                        $this->id = $id_c;
                        
                         
                        $nro_c =  $nro_comprobante;
                        
                       
						 $errores =  $this->SaveItems($this->request->data,$message);//si tiene items los grabo     
						 
						 if($errores == 1){ //si hubo un error agregando algun Item del comprobante entonces muestro errores
							 
							 throw new Exception($message);
						 }
						 
						 $this->RealizarMovimientoStock($this->id,$message);//si me viene el estado definitivo en el alta chequeo stock.
                     

                       //Pueden ser Items de ComprobanteItem
                                            //ComprobanteItemComprobante
                                            //ComprobanteImpuesto
                       
                       $total_iva = $this->Comprobante->CalcularIva($id_c,$this); 
                       $error = 0;
                       $total_impuestos = $this->CalcularImpuestos($id_c,'',$error,$message);
                       if($error == 1){
                           
                           throw new Exception($message);
                       }
                       
                       //if($total_iva > 0 || $total_impuestos > 0 )
                       $this->actualizaComprobanteNumero($id_c,$nro_comprobante);
                       $this->actualizoMontos($id_c,$nro_comprobante,$total_iva,$total_impuestos);
                       
						//$this->revertirItems();  // Es un metodo que revierte el stock para los PI y la Factura NO SE USA
                        
                        $this->cerrarComprobantesAsociados($this->request->data);//le paso el comprobante y los items
                        
                        $this->request->data["Comprobante"]["id"] = $id_c;
                        $this->CerrarConEstado();
                        
                        
                        $id_asiento = $this->generaAsiento($id_c,$message);
                        $this->id_asiento = $id_asiento;
                        
                        $this->Auditar($id_c,$this->request->data,$nro_comprobante);
                    
                        if( $id_asiento >= 0  ){ //es -1 no requiere asiento sigo con el curso
                            $mensaje =  "Ha sido creado/a exitosamente ".$message;
                            $tipo = EnumError::SUCCESS;
                            $this->error = 0;
                            
                            if($this->request->data["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoConCae && isset($this->request->data["Comprobante"]["id_punto_venta_fiscal"]) && $this->request->data["Comprobante"]["id_punto_venta_fiscal"]>0 ){
                            	$this->obtiene_cae_add = 1;//flag que se usa en el obtenerCae si esta seteado es que se pidio el CAE en el ADD
                            	
                            	if(!isset($this->request->data["Comprobante"]["id_punto_venta_fiscal"]) || $this->request->data["Comprobante"]["id_punto_venta_fiscal"] == "" || is_null($this->request->data["Comprobante"]["id_punto_venta_fiscal"]))
                            		throw new Exception("No se esta enviando el id_punto_venta_fiscal");
                            	
                            	$resultado = $this->ObtenerCae($this->id,$this->request->data["Comprobante"]["id_punto_venta_fiscal"]);
                            	$tipo = $resultado["status"];
                            	$mensaje = $resultado["message"];
                            	
                            	if(isset($resultado["content"]["Comprobante"]) && count($resultado["content"]["Comprobante"])>0 ){
	                            	$nro_cae = $resultado["content"]["Comprobante"]["cae"];
	                            	$nro_c =  $resultado["content"]["Comprobante"]["nro_comprobante"];
	                            	$comprobante = $resultado["content"]["Comprobante"];
                            	}
                            	
                            	if($tipo == EnumError::ERROR){
                            		$this->error = 1;
                            		$ds->rollback();  //hago rollback porque falla el obtener CAE lo veo todo como una sola operacion
                            	}else{
                            		
                            		$ds->commit(); //como obtuvo el CAE entonces commiteo todo
                            	}
                            	
                            }else{
                            	
                            	$ds->commit();//si el comprobante no es de impacto en el ADD entonces commiteo
                            	
                            	$comprobante =  $comprobante = $this->Comprobante->find('first', array(
                            			'conditions' => array(
                            					'Comprobante.id' => $id_c
                            			) ,
                            			'contain' => false
                            	));
							}
                            
                        }elseif($id_asiento == -1){//error con el asiento
                            $ds->rollback();    
                            $mensaje = $message;
                            $tipo = EnumError::ERROR;
                            $this->error = 1;
                        }
				   }
                    
                }else{
                    
					if ($this->Comprobante->saveAll($this->request->data, array('deep' => true))){
                        $mensaje = "Ha sido creado exitosamente";
                        $tipo = EnumError::SUCCESS;
                        $this->error = 0;
                       
                    }else{
                        $mensaje = "Ha ocurrido un error,NO ha podido ser creado/a.";
                        $tipo = EnumError::ERROR;
                        $this->error = 1; 
                        
                    }
                    
                }
            }catch(Exception $e){
                $ds->rollback();
                $mensaje = "Ha ocurrido un error, NO ha podido ser creado/a".$e->getMessage();
                $tipo = EnumError::ERROR;
                $this->error = 1;
				}
            }else{
              $mensaje = "Se debe definir el numero inicial del contador para este comprobante.";
              $tipo = EnumError::ERROR;  
              $this->error = 1;
            }
            
            $data_comprobante = array();
           
            
            
            if($tipo == EnumError::ERROR)
            	$comprobante = array();//si hay error en el proceso no envio el comprobante
            
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => $comprobante,
            "id_add" => $id_c,
            "nro_add" => $this->Comprobante->GetNumberComprobante(0,$nro_c),
            "id_asiento" => $id_asiento,
            "message_type"=>$this->type_message,
            "nro_cae"=>$nro_cae
            );
             
            $this->output = $output;
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
            	
            	if($this->llamada_interna !=1){
	                $this->set($output);
	                $this->set("_serialize", array("status", "message", "content","id_add","nro_add","id_asiento","message_type","nro_cae"));
            	}else{
            		return $output;
            	}
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    
    
    
   
protected function edit($id) {
        
    
       
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);

            $this->{$this->model}->id = $id;
            $id_asiento = '';
            $this->id = $id;
         
            $id_estado_comprobante_enviado = $this->request->data[$this->model]["id_estado_comprobante"];
            $id_estado_comprobante_grabado = $this->{$this->model}->getEstadoGrabado($id);
            $anulo_desde_dentro_del_form = false;
            
            
            
            /*Es un control por si en el combo de estado dentro del formulario elige la opcion anulado y apreta aplicar y $this->anulo == 0 para que no sea recursivo TODO: CREO QUE NO VA MAS*/
            if( $id_estado_comprobante_enviado == EnumEstadoComprobante::Anulado && $id_estado_comprobante_grabado == EnumEstadoComprobante::Abierto && $this->anulo == 0 )
            	$anulo_desde_dentro_del_form= true;
            
            
            if($anulo_desde_dentro_del_form){
                $this->request->data[$this->model]["id_estado_comprobante"] = EnumEstadoComprobante::Abierto;
                $this->request->data[$this->model]["definitivo"] = 0;
                
            }    
            
           if($this->Comprobante->getDefinitivoGrabado($id)!=1){/*si en la BD esta definitivo entonces no puede editarlo*/ 
           
           
        
           
            
            $ds = $this->Comprobante->getdatasource();
                            
            try{ 
            	
            	$error = 0;
            	$message = '';
            	
            	
                $ds->begin();
                $this->deleteItemsNoEnviados($id);
                $this->StockOnEdit(); //solo esta sobrescrito en IRM
                
                
                
                
                
                
                
                
           
                
                
                
                $this->Comprobante->CalcularMonedaBaseSecundaria($this->request->data,$this->model);
                
                
                $error = $this->borrarValoresMontoCero($error,$message);
                
                if($error == 1){ //si hubo un error agregando algun Item del comprobante entonces muestro errores
                	
                	throw new Exception($message);
                }
                
                if($this->request->data[$this->model]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion)
                    $this->request->data[$this->model]["fecha_cierre"] = date("Y-m-d"); //si cierra desde adentro del form entonces fecha cierre es la de hoy
                    
                  
                
                $this->guardaCodigoProducto();//esta funcion se hizo por ProductoCodigo, pero igual sirve por si le cambian un codigo a un producto.
                $this->setTasaIva();
                
                if ($this->{$this->model}->saveAll($this->request->data)){
                	
                	$this->RealizarMovimientoStock($id,$message);
                    
                    
                	$this->Auditar($id,$this->request->data);
                    $this->CerrarConEstado(); //cierra y revierte movimientos si es factura o pedido interno por ejemplo
                    $this->borraImpuestos($id);
                    $total_iva = $this->Comprobante->CalcularIva($id,$this);
                    
                   
                    
                    $total_impuestos = $this->CalcularImpuestos($id,'',$error,$message);
                    
                     if($error == 1){ //si hubo un error agregando algun Item del comprobante entonces muestro errores
                         
                         throw new Exception($message);
                     }
                    
                    
                    $this->actualizoMontos($id,0,$total_iva,$total_impuestos);//pasar 0 significa que no actualiza el nro_comprobante
                    $this->cerrarComprobantesAsociados($this->request->data);
       
                    
                            
                      
                    
                    $mensaje =  "Ha sido modificado/a exitosamente ".$message;
                    $status = EnumError::SUCCESS;
                    $message = '';
                    $this->error = 0;
                    $mensaje_mail = "";
                    $this->avisoEmail($id_comprobante,$mensaje_mail);
                    
                    $id_asiento = $this->generaAsiento($id,$message);
                    $this->id_asiento = $id_asiento;
                    
                    if($id_asiento >= 0 ){ //es -1 error con asiento sigo con el curso
                    
                        $ds->commit();
                        $mensaje = $mensaje." ".$message;
                        $this->error = 0;
                       
                        
                    }elseif($id_asiento == -1){//error con el asiento
                        $ds->rollback();    
                        $mensaje = $message;
                        $status = EnumError::ERROR;
                        $this->error = 1;
                    }
                    
                }else{   //si hubo error recupero los errores de los models
                
                    $id_asiento = -1;
                    
                    $errores = $this->{$this->model}->validationErrors;
                    
                    $errores_string = "";
                    //var_dump($errores);
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        if(!is_array($error))
                            $errores_string.= "&bull; ".$error."\n";
                        else
                           $errores_string.= $this->getModelErrorFromArray($error)."\n"; 
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
                    $this->error = 1;
               }
               
               
            }catch(Exception $e){
                
                 $ds->rollback();
                 $mensaje = 'Ha ocurrido un error, NO ha podido modificarse.'.$e->getMessage().$e->queryString;
                 $status = EnumError::ERROR; 
                 $this->error = 1;
                
            }
            
            }else{
                
                $mensaje = 'El comprobante en esta instacia no puede ser editado';
                $status = EnumError::ERROR; 
                $this->error = 1;
            }
            
            
            
            
            if($anulo_desde_dentro_del_form){
                
                $this->Anular($id);
                return;
                
            }else{
            
                 if($this->RequestHandler->ext == 'json'){  
                            $output = array(
                                "status" => $status,
                                "message" => $mensaje,
                                "content" => "",
                                "id_asiento" => $id_asiento
                            ); 
                    $this->output = $output;        
                    $this->set($output);
       
                    $this->set("_serialize", array("status", "message", "content","id_asiento"));        
                 }else{
                            $this->Session->setFlash($mensaje, $status);
                            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                 }
             
             }
             
   
        }
    }
    
protected function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->{$this->model}->id = $id;
       
       
       if($this->Comprobante->getEstado($id)== EnumEstadoComprobante::Abierto)
       
       { 
           try{
                if ($this->{$this->model}->saveField('visible', 0)) {
                    
                   
                    
                    $status = EnumError::SUCCESS;
                    $mensaje = "Ha sido eliminado/a exitosamente.";
                    $output = array(
                        "status" => $status,
                        "message" => $mensaje,
                        "content" => ""
                    ); 
                    
                }else{
                    
                    $status = EnumError::ERROR;
                    $mensaje = "NO ha podido ser eliminado/a.";
                }
                     
              }catch(Exception $ex){ 
                   $status = EnumError::ERROR;
                   $mensaje = $ex->getMessage();
                
        
              } 
       }else{
           
           $status = EnumError::ERROR;
           $mensaje = "El Comprobante ha sido eliminado."; 
           
       }     
           
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        
        $this->output = $output;
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     

        
    }
    

    protected function deleteItem($id_item,$externo=1){
        
        $this->loadModel("ComprobanteItem");
        
        
        
    
        
        
        //TODO: BAJA LOGICA
        $this->ComprobanteItem->id = $id_item;
       try{
           
      
           
           
       	$cantidad_asociados = $this->ComprobanteItem->GetItemprocesadosEnImportacion($id_item,array(),array(),array(EnumEstadoComprobante::Anulado,EnumEstadoComprobante::Cancelado));
           
           
           
           if(count($cantidad_asociados) == 0){
            if ($this->ComprobanteItem->saveField('activo', NULL)) {
                
                    if($this->AceptaStock() == 1)  
                            $this->genera_movimiento_stock($id_item,EnumTipoMovimiento::CanceloItemComprobante);//si esta habilitado descuento stock generando movimiento
        
                $status = EnumError::SUCCESS;
                $mensaje = "El Item ha sido eliminado exitosamente.";
              
            }else{
            	
            		
              
  					$mensaje = "El item que intenta borrar tiene comprobantes asociados.";              
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje .= $errores_string;
                    $status = EnumError::ERROR; 
                
                
            }
            
          }else{
             
          	$asociados = array();
          	foreach($cantidad_asociados as $item){
          		
          		array_push($asociados,$item["Comprobante"]["TipoComprobante"]["codigo_tipo_comprobante"].$this->ComprobanteItem->GetNumberComprobante($item["Comprobante"]["PuntoVenta"]["numero"],$item["Comprobante"]["nro_comprobante"]));
          		
          	}
              
             $status = EnumError::ERROR; 
             $mensaje = "El item que intenta borrar esta relacionado con otros comprobantes. No puede ser borrado.".implode($asociados,",");
             
                
          }  
            
        }
        catch(Exception $ex){ 
              $mensaje = "Ha ocurrido un error, el item no ha podido ser borrado";
               $status = EnumError::ERROR; 
        }
        
        
        
        $output = array(
                "status" => $status,
                "message" => $mensaje,
                "content" => ""
        ); 
        
        if($this->RequestHandler->ext == 'json' && $externo == 1){
          
            $this->output = $output;
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content"));
        }
        
        
        if($externo == 0 )
            return $output;
        
        
        
        
    }
    

protected function pdfExport($id,$vista='')
{        
        
    try{
    	
    $this->loadModel("Comprobante");
    $this->Comprobante->actualizaCountViewPdf($id);
    
    
    
    
        //$this->Comprobante->id = $id;
         /*$this->Comprobante->contain(array('ComprobanteItem' => array('Producto'=>array('Iva'),'Iva','Unidad'),'EstadoComprobante','Moneda',
                                    'Persona'=> array('Pais','Provincia'),'CondicionPago','PuntoVenta','TipoComprobante','CondicionPago',
                                    'ComprobanteImpuesto'=>array('Impuesto'),'Transportista','Sucursal'=>array('Provincia','Pais')));
                                    */
                                    
       $this->request->data = $this->Comprobante->find('first',array('contain'=>array('TipoDocumento','ComprobanteItem' => array( 'conditions'=>array('ComprobanteItem.activo'=>1),
                                                                                                                   'order' => $this->Comprobante->ComprobanteItem->getOrderItems("ComprobanteItem",$id),
                                                                                                                    'Producto'=>array('Iva','Unidad'),'Iva','Unidad','ComprobanteItemOrigen'=>array("Comprobante","Unidad"),"ComprobanteItemLote"=>array("Lote"=>array("TipoLote"))
                                                                                                                    ),
       		'EstadoComprobante','Moneda','DepositoOrigen','DepositoDestino','CarteraRendicion','Persona'=> array('Pais','Provincia','TipoDocumento','TipoIva'),
                                                                                       'CondicionPago','PuntoVenta','TipoComprobante'=>array('CuentaBancaria'),'CondicionPago','Usuario',
                                                                                       'ComprobanteImpuesto'=>array('Impuesto','ComprobanteReferencia'),'Transportista'=>array('Provincia','Pais'),
                                                                                       'Sucursal'=>array('Provincia','Pais'),'ComprobanteItemComprobante'=>array('ComprobanteOrigen'=>array('TipoComprobante','PuntoVenta','MonedaEnlazada','Moneda')),
       		'ComprobanteValor'=>array('Valor','CuentaBancaria'=>array('BancoSucursal'=>array('Banco') ),'Cheque'=>array('Banco','BancoSucursal','Chequera'=>array('CuentaBancaria'=>array('BancoSucursal'=>array('Banco')) ))),'ChequeEntrada'=>array('Banco'),'ChequeSalida'=>array('Banco','Chequera'=>array('CuentaBancaria'=>array('BancoSucursal'=>array('Banco')) ) ),'DetalleTipoComprobante','ComprobanteReferenciaImpuesto'=>array('Comprobante'=>array("ComprobanteItem"=>array("ComprobanteOrigen"=>array("TipoComprobante"))),'Impuesto')),
                                                                                'conditions' =>array('Comprobante.id'=>$id),
                                                                                ));
        //$this->request->data = $this->Comprobante->read();
        $this->request->data = $this->Comprobante->getSucursalEnvio($this->request->data);
        Configure::write('debug',0);
        
        
        $this->Auditar($id, $this->request->data, 0,1);
        
       
        if( $this->request->data["Persona"]["id"] == null )
        {
            $this->request->data["Persona"]["razon_social"] = $this->request->data["Comprobante"]["razon_social"];
            $this->request->data["Persona"]["cuit"] =           $this->request->data["Comprobante"]["cuit"]; 
        }
        
        $this->request->data["Comprobante"]["nro_comprobante_completo"] = 
        $this->Comprobante->GetNumberComprobante($this->request->data["PuntoVenta"]["numero"],$this->request->data["Comprobante"]["nro_comprobante"]);    
        
        $this->set('datos_pdf',$this->request->data);
        $this->set('datos_empresa',$this->Session->read('Empresa'));
        $this->set('array_filters_name',$this->array_filter_names);
            
        $impuestos = '';
        $this->set('id',$id);
        $this->set('output',3);
        Configure::write('debug',0);
        $this->layout = 'pdf'; //esto usara el layout pdf.ctp
        $this->response->type('pdf');
 
        
        if($vista=='')
        {
            $this->render();    
        }else
        {
        	
       // $this->autoRender = false;
       // $this->layout= false;
       // $this->viewPath = 'Recibos/';
       
        	
        	
        
        	//$pdf_obj = $this->getPDF($this->request->data,$this->Session->read('Empresa'),3);
        	
        
        	
        	
        	
       		$this->render($vista);
       		

        	
        
        	/*$view = new View($this,false);
        	
        	if(strlen($this->viewPath)>0)
        		$view->viewPath=$this->viewPath;  // Directory inside view directory to search for .ctp files
        	
        	$view->layout= false; // if you want to disable layout
      
        	
        	$view->set('datos_pdf',$this->request->data);
        	$view->set('datos_empresa',$this->Session->read('Empresa'));
        	$view->set('array_filters_name',$this->array_filter_names);
        	
        	$impuestos = '';
        	$view->set('id',$id);
        	
        	$view->render($vista); 
        */
       
        }
        
    }catch(Exception $e){
          $output = array(
            "status" =>EnumError::ERROR,
          		"message" => $e->getMessage(),
            "content" => "",
        
            ); 
        
    }
    } 
    
                                            //TODO MP : Se deberia refactorizar a id_comprobante por q es una funcion generiaca  qse puede usar de varios comrpobantes
protected function genera_movimiento_stock($id_pi,$action,$cantidad_calculada=0,$data =null,$simbolo_operacion=1){    //esta funcion actualiza el stock a traves de un movimiento
        
    
	
	/*SOLO SE VA A MOVER STOCK SI ESTA CERRADO*/
	if($this->request->data["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoConCae || $this->request->data["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion || $this->request->data["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::Remitido ){
	
		$id_deposito = 0;
        $id_deposito_destino = 0;
            
            if(isset($this->request->data["Comprobante"]["id_deposito_origen"]))
                $id_deposito = $this->request->data["Comprobante"]["id_deposito_origen"];
            
            if(isset($this->request->data["Comprobante"]["id_deposito_destino"]))
                $id_deposito_destino = $this->request->data["Comprobante"]["id_deposito_destino"];
                
            
            
  
        
        
        switch($action){
            
          
            
            

            case EnumTipoMovimiento::Comprobante:
            
            
            
            $data = array();
            $data["Movimiento"]["id_movimiento_tipo"] = $action;
            $data["Movimiento"]["id_comprobante"] = $id_pi;
            $data["Movimiento"]["id_deposito_origen"] = $id_deposito;
            $data["Movimiento"]["id_deposito_destino"] = $id_deposito_destino;
            $data["Movimiento"]["llamada_interna"] = 1;
            $data["Movimiento"]["simbolo_operacion"] = $simbolo_operacion;
            
            
            break;
            
        
            
            case EnumTipoMovimiento::AjustePositivoComprobanteItem:
            case EnumTipoMovimiento::CanceloItemComprobante:
            case EnumTipoMovimiento::AgregoItemComprobante:
            
            
            $data = array();
            $data["Movimiento"]["id_movimiento_tipo"] = $action;
            $data["Movimiento"]["id_comprobante_item"] = $id_pi;
            $data["Movimiento"]["cantidad"] = $cantidad_calculada;
            $data["Movimiento"]["id_deposito_origen"] = $id_deposito;
            $data["Movimiento"]["id_deposito_destino"] = $id_deposito_destino;
            $data["Movimiento"]["llamada_interna"] = 1;
            $data["Movimiento"]["simbolo_operacion"] = $simbolo_operacion;
            
            
            
            break;
            case EnumTipoMovimiento::AjusteNegativoComprobanteItem:
            $data = array();
            $data["Movimiento"]["id_movimiento_tipo"] = $action;
            $data["Movimiento"]["id_comprobante_item"] = $id_pi;
            $data["Movimiento"]["cantidad"] = $cantidad_calculada;
            $data["Movimiento"]["id_deposito_origen"] = $id_deposito;
            $data["Movimiento"]["id_deposito_destino"] = $id_deposito_destino;
            $data["Movimiento"]["llamada_interna"] = 1;
            $data["Movimiento"]["simbolo_operacion"] = $simbolo_operacion;
            
            
            break;
            

           
            
        }
        
        if($id_deposito_destino!=0)
            $data["Movimiento"]["id_deposito_destino"] = $id_deposito_destino;
        
            
           // $this->request->data = array();//IMPORTANTE seteo en vacio porque si me manda por JSON el primer request y despues redirecciono el request->data queda cargado
        //realizo la llamada al controller y le envio el movimiento
     $respuesta =    $this->requestAction(
                    array('controller' => 'Movimientos', 'action' => 'add'),
                    array('data' => $data)
            );
     

            
        
	}
       return true;
        
        
        
    }
    
    
protected function SaveItems(&$data,&$message=''){
        
            $error = 0;
            $auxiliar = $this->request->data;
            
            
            if(isset($data['ComprobanteItem'])){ 
                unset($data['ComprobanteItem']['genera_movimiento_stock']);
                foreach($data['ComprobanteItem'] as &$c_item){
                    $c_item['id_comprobante'] = $this->{$this->model}->id;
                    $c_item['producto_codigo'] = $c_item['codigo'];
                    
                }
                $comprobante_item = $this->request->data['ComprobanteItem'];
                $this->Comprobante->ComprobanteItem->saveAll($comprobante_item);
                  $error = 0;
               }
               
               
               if(isset($data['ComprobanteItemComprobante'])){ 
                unset($data['ComprobanteItemComprobante']['genera_movimiento_stock']);
                foreach($data['ComprobanteItemComprobante'] as &$c_item){
                    $c_item['id_comprobante'] = $this->{$this->model}->id;
                    
                }
                $comprobante_item_comprobante = $this->request->data['ComprobanteItemComprobante'];
                $this->Comprobante->ComprobanteItemComprobante->saveAll($comprobante_item_comprobante);
                  $errror = 0;
               }
               
               
               
               if(isset($data['ComprobanteValor'])){ 
               
                foreach($data['ComprobanteValor'] as &$c_item){
                    $c_item['id_comprobante'] = $this->{$this->model}->id;
                    
                }
                
                  $comprobante_valor = $this->request->data['ComprobanteValor'];
                  $this->Comprobante->ComprobanteValor->saveAll($comprobante_valor);
                  $error = 0;
                  
               }
               

               if(isset($data['ComprobanteImpuesto'])){ 
               
                foreach($data['ComprobanteImpuesto'] as &$i_item){
                    $i_item['id_comprobante'] = $this->{$this->model}->id;
                    
                }
                
                $comprobante_impuesto = $this->request->data['ComprobanteImpuesto'];
                
                
                if(!$this->Comprobante->ComprobanteImpuesto->saveAll($comprobante_impuesto)){
                      
                      $errores = $this->Comprobante->ComprobanteImpuesto->validationErrors;
                      $message = $this->getErrors($errores);
                      $error = 1;
                  }else{
                      $error = 0;//NO hubo errores sigue todo bien
                  }
                  
               } 
               
               
               $this->request->data =  $auxiliar;
               return $error;  
        
        
    }
   
    
    /* NO se usa mas.
protected function ActualizaStock($id_comprobante,$tipo_movimiento){
        
         
       if($this->AceptaStock() == 1)  
            $this->genera_movimiento_stock($id_comprobante,$tipo_movimiento);//si esta habilitado descuento stock generando movimiento  
       
}

*/
    

       
       
    
protected function ObtenerCae($id_comprobante,$id_punto_venta,$recursivo=0){
    
    
        set_time_limit(90);
        
        $this->loadModel("Comprobante");
        $this->loadModel("Moneda");
        $this->loadModel("PeriodoContable");
        $this->loadModel("Modulo");
        $this->loadModel("Persona");
        $this->loadModel("EjercicioContable");
        
        App::import('Vendor', 'config', array('file' => 'fesix/config.php'));//importo el archivo de configuracion de la carpeta Vendor/fesix
        App::import('Vendor', 'AfipWsaa', array('file' => 'fesix/afip/AfipWsaa.php'));//importo el archivo de configuracion de la carpeta Vendor/fesix
        App::import('Vendor', 'AfipWsfev1', array('file' => 'fesix/afip/AfipWsfev1.php'));//imp 
        
        
        
        $actualiza_fecha_contable = 1;
        if(isset($this->request->data["Comprobante"]) && isset($this->request->data["Comprobante"]["fecha_contable"]) && isset($this->request->data["Comprobante"]["id"]) && $this->request->data["Comprobante"]["id"] == $id_comprobante)
        	$actualiza_fecha_contable = $this->actualizaFechaContable($id_comprobante,null,$error,$mensaje);
        
        	
        $actualiza_fecha_vencimiento = 1;
        if(isset($this->request->data["Comprobante"]) && isset($this->request->data["Comprobante"]["fecha_vencimiento"]) && isset($this->request->data["Comprobante"]["id"]) && $this->request->data["Comprobante"]["id"] == $id_comprobante)
        		$actualiza_fecha_contable = $this->actualizaFechaVencimiento($id_comprobante,null,$error,$mensaje);
        
        	
        	
		$this->Comprobante->contain(array('TipoComprobante','PuntoVenta',
									'ComprobanteItem' => array('Producto'=>array('Iva'),
															   'ComprobanteItemOrigen'=>array('Comprobante'=>array('TipoComprobante',
																												    'PuntoVenta'))),
									'Moneda','Persona'=> array('Pais','Provincia','TipoDocumento','TipoIva'),'CondicionPago','ComprobanteImpuesto'=>array('Impuesto')));
									
        $this->Comprobante->id = $id_comprobante;
        $comprobante_guardado = $this->Comprobante->read();
        $fecha_contable = $comprobante_guardado["Comprobante"]["fecha_contable"];
        	
        	        
       if($actualiza_fecha_contable == 1){ 	
	       
	        
	        
	        $saldo = 0;
	        
	        
	        
	     
	        
	        if($recursivo == 0 && $this->Comprobante->PuntoVenta->EsFacturaElectronica($comprobante_guardado["Comprobante"]["id_punto_venta"])== 1 
					&& strlen($comprobante_guardado["Comprobante"]["cae"])== 0 && !isset($this->request->data["Comprobante"]["id_punto_venta_fiscal"]) 
					&& !$this->request->data["Comprobante"]["id_punto_venta_fiscal"]>0){
	         //fallo la vuelta de afip por algun motivo consulto CAE
	                        
	           $resultado = $this->ConsultarCae($id_comprobante,$cae,$vencimiento_cae);
	           if($resultado){//tengo el CAE actualizo
	           
	           	

	           	$time = strtotime($comprobante_guardado["Comprobante"]["fecha_contable"]);
	           	$year_fecha_guardado = date('Y', $time);
	           	
	           	
	           
	           	if(is_null($comprobante_guardado["Comprobante"]["fecha_contable"])  || $year_fecha_guardado!= date('Y') )
	       					$fecha_contable = "'".date('Y-m-d')."'";//fecha en que se aprueba el CAE, no deberia entrar jamas por aca
	            
	       	    
	       	    
	       					
	       	   $comprobante_obj = new Comprobante();
	       	   $saldo = $comprobante_obj->getComprobantesImpagos($comprobante_guardado["Comprobante"]["id_persona"],1,$id_comprobante,1,EnumSistema::VENTAS,0,EnumMoneda::Peso);
	            /*Esto es para las proformas cuando se hacen efectivo*/
	       	   if($saldo <= 0.01)
	       	   	$estado_comprobante = EnumEstadoComprobante::Cumplida;
	       	   else 
	       	   	$estado_comprobante = EnumEstadoComprobante::CerradoConCae;
	            
	            $this->Comprobante->updateAll(
	            		array('Comprobante.cae' => $cae,'Comprobante.vencimiento_cae' => "'".$vencimiento_cae."'",'Comprobante.id_estado_comprobante' => $estado_comprobante),
	                array('Comprobante.id' => $id_comprobante) );
	            
	            
	            
	            
	            if(!is_null($comprobante_guardado["Comprobante"]["fecha_contable"]) && $year_fecha_guardado == date('Y') ){//no deberia pasar jamas, pero por si pasa
	            	
	            	$this->Comprobante->updateAll(
	            			array('Comprobante.cae' => $cae,'Comprobante.fecha_contable' =>$fecha_contable),
	            			array('Comprobante.id' => $id_comprobante) );
	            	
	            }
	           
	            
	            
	            $error = EnumError::SUCCESS;
	            $mensaje = "El Comprobante ha sido validado electronicamente por afip CAE: ".$cae;
	            $mensaje_mail = "";
	            $this->avisoEmail($id_comprobante,$mensaje_mail);
	            
	            
	            
	             /*$habilitado  = $this->Modulo->estaHabilitado(EnumModulo::CONTABILIDAD);
	             
	            if($habilitado == 1){   
	                
	                //$periodo_actual =  $this->PeriodoContable->getPeriodoActual();
	                
	                $ejercicio_contable = $this->EjercicioContable->GetEjercicioPorFecha($fecha_contable,0,1);
	                
	                $periodo_contable = $this->PeriodoContable->GetPeriodoPorFecha($fecha_contable,0,$ejercicio_contable["EjercicioContable"]["id"]);
	                
	                
	               if($ejercicio_contable && isset($periodo_contable["id"]) && $periodo_contable["id"]>0){
	                
		                $this->Comprobante->Asiento->updateAll(
		                    array('Asiento.d_asiento' => "'".
		                    
		                    $comprobante_guardado["TipoComprobante"]["codigo_tipo_comprobante"] ." ".$this->Comprobante->GetNumberComprobante($comprobante_guardado[$this->model]['d_punto_venta'],$comprobante_guardado[$this->model]['nro_comprobante']
		                    )."'",
		                    'Asiento.fecha'=>$fecha_contable,
		                    		'Asiento.id_periodo_contable'=>$periodo_contable["id"],
		                    		'Asiento.id_ejercicio_contable'=>$ejercicio_contable["EjercicioContable"]["id"],
		                    
		                    
		                    ),
		                    array('Asiento.id_comprobante' => $id_comprobante) );
		                
		                
	                
	               }else{
	               	
		               	$error = EnumError::ERROR;
		               	$mensaje = "Para el comprobante seleccionado no existe Ejercicio Contable abierto o Periodo contable, cambie la fecha contable del mismo";
	               	
	               }
	                
	                
	            }    
	                    
	              */  
	          
	               
	           }else{
	               
	               //el comprobante tiene un punto de venta valido pero no fue impactado
	              $this->ObtenerCae($id_comprobante,$id_punto_venta,1); 
	               
	           }
	           
	            
	        }else{
	                
	                        
	                
	
	                $this->request->data = $comprobante_guardado;
	                 
	                 
	              
	              
	                $contador = $this->chequeoContador($this->request->data["TipoComprobante"]["id"],$id_punto_venta);
	              
	                $tipo_comprobantes_factura_array = $this->Comprobante->getTiposComprobanteController(EnumController::Facturas);
	                
	                
	                /*
	                if($this->request->data["Persona"]["cuenta_corriente"] == 0  && in_array($this->request->data["TipoComprobante"]["id"], $tipo_comprobantes_factura_array) ){//si la persona NO tiene habilitada la cuenta corriente entonces chequeo que la factura primero haya sido abonada
	                	
	                	//obtengo el saldo de la factura, sino hizo el recibo de la proforma, no la puede pasar como valida
	                	$saldo = $this->Comprobante->getComprobantesImpagos($this->request->data["Persona"]["id"], 1,$this->request->data["Comprobante"]["id"],1,EnumSistema::VENTAS,0,$this->request->data["Comprobante"]["id_moneda"],0,9999999999999999999999,0,0,0);
	                  
	                	
	                }
	                */
	                
	                $saldo  = 0;//pensar mejor lo de cuenta corriente
	                
	                if($this->Persona->getCondicionIva($this->request->data["Comprobante"]["id_persona"]) == EnumTipoIva::ConsumidorFinal)
	                	$saldo = 0;
	                
	                
	               
	                
	               
	                if($contador &&  strlen($this->request->data["Comprobante"]["cae"])== 0 &&  ($saldo == 0)     ){
	                         
	                      
	                            $nro_comprobante_old = $this->request->data["Comprobante"]["nro_comprobante"];
	                            $punto_venta_old = $this->request->data["PuntoVenta"]["numero"];
	                            $id_punto_venta_old = $this->request->data["PuntoVenta"]["id"];
	                    
	                            
	                            
	                            
	                            
	                             if( $this->Comprobante->PuntoVenta->EsFacturaElectronica($comprobante_guardado["Comprobante"]["id_punto_venta"])!= 1  ) 
	                                $nro_comprobante = $this->obetener_ultimo_numero_comprobante($this->request->data["TipoComprobante"]["id"],$id_punto_venta,5,"+");
	                             else
	                                $nro_comprobante = $comprobante_guardado["Comprobante"]["nro_comprobante"];
	                                
	                               
	                            $this->Comprobante->updateAll(
	                                            array('Comprobante.nro_comprobante' => $nro_comprobante,'Comprobante.nro_comprobante_afip' => $nro_comprobante,'Comprobante.id_punto_venta'=> $id_punto_venta),
	                                            array('Comprobante.id' => $id_comprobante) 
	                                            );
	                                            
	                                       
	                                            
	                                            
	                            $this->Comprobante->contain(array('TipoComprobante','PuntoVenta','ComprobanteItem' => array('Producto'=>array('Iva'),'ComprobanteOrigen'=>array('TipoComprobante','PuntoVenta')),'Moneda','Persona'=> array('Pais','Provincia','TipoDocumento'),'CondicionPago','ComprobanteImpuesto'=>array('Impuesto')));
	                            $this->request->data = $this->Comprobante->read(); //lo leo de nuevo para actualizar el data               
	                            $this->request->data["Comprobante"]["nro_comprobante_old"] = $nro_comprobante_old;
	                            $this->request->data["Comprobante"]["punto_venta_old"] = $punto_venta_old;
	                            $this->request->data["Comprobante"]["id_punto_venta_old"] = $id_punto_venta_old;
	                       
	                       
	                       
	                       
	                       $datoEmpresa = $this->Session->read('Empresa');
	                       
	                       
	                       $empresaCuit  = str_replace("-","",$datoEmpresa["DatoEmpresa"]["cuit"]);
	                       
	                       
	                       $empresaAlias  = $datoEmpresa["DatoEmpresa"]["alias"];
	                       
	                       /* DATOS COMPROBANTE */
	                       $PtoVta   =    $this->request->data["PuntoVenta"]["numero"];
	                       $Concepto = trim($datoEmpresa["DatoEmpresa"]["concepto_afip"]); //Productos y Servicios
	                       $CbteTipo = $this->request->data["TipoComprobante"]["codigo_afip"]; //indica el tipo de documento para la afip, hay una tabla en la afip para esto
	                       
	                       /* FIN DATOS COMPROBANTE */
	
	
	
	                       
	                       
	                       if($this->request->data["Persona"]["id"] != EnumPersonaBase::ClienteNoRegistrado){
	                       	/* DATOS CLIENTE*/
	                       	$DocTipo = $this->Persona->getTipoDocumento($this->request->data["Persona"]["id"]); //tipo de documento para la afip hay tabla en afip que lo explica
	                       	$DocNro = $this->Persona->getNroDocumento($this->request->data["Persona"]["id"]);
	                       }else{
	                       	
	                       	$DocTipo = EnumTipoDocumento::DNI;
	                       	$DocNro = $comprobante_guardado["Comprobante"]["cuit"];
	                       	
	                       	if(!strlen($DocNro)>0){//sino grabo el doc en la factura
	                       		
	                       		
	                       		$DocTipo = 99;
	                       		$DocNro = 0;
	                       		
	                       	}
	                       	
	                       }
	                       
	                       
	                       
	                       $fecha_comprobante = strtotime($fecha_contable);
	                       $CbteFch= date("Ymd", $fecha_comprobante);
	                       
	                       
	                       //$CbteFch = intval(date('Ymd'));
	                       $ImpTotal = $this->request->data["Comprobante"]["total_comprobante"];
	                       
	                       
	                      
	                
	                       
	                       if($this->request->data["TipoComprobante"]["letra"]!="C")
	                       	$ImpTotConc = $this->Comprobante->getNoGravado($id_comprobante);
	                       else
	                        $ImpTotConc  = 0;
	                     
	                       
	                       $ImpOpEx = $this->Comprobante->getExento($id_comprobante);
	                       
	                       
	                       
	                
	                       if($this->request->data["TipoComprobante"]["letra"]!="C")
	                       	$ImpNeto = $this->Comprobante->getGravado($id_comprobante);
	                       else
	                       	$ImpNeto = $ImpTotal;
	                      
	                       
	                       
	                       
	                       
	                       
	                       $FchServDesde = $CbteFch;
	                       $FchServHasta = $CbteFch;
	                       $FchVtoPagoAux = strtotime($this->request->data["Comprobante"]["fecha_vencimiento"]);
	                       $FchVtoPago = date("Ymd", $FchVtoPagoAux);
	                       //$FchVtoPago = $CbteFch;
	                       $MonId = $comprobante_guardado["Moneda"]["abreviacion_afip"]; // Pesos (AR) - Ver - AfipWsfev1::FEParamGetTiposMonedas()
	                       
	                       if($comprobante_guardado["Comprobante"]["id_moneda"] == EnumMoneda::Peso)
	                            $MonCotiz = 1.00;
	                       else{
	                           
	                            $cotizacion = $this->Moneda->getCotizacion(EnumMoneda::Peso);
	                            $MonCotiz = round($cotizacion,6);
	                            
	                       }
	                       
	                       /* FIN DATOS CLIENTE*/
	                       
	                       
	                       
	                       
	                       
	                      $webService   = 'wsfe';//Creando el objeto WSAA (Web Service de Autenticación y Autorización)
	                      
	                      $wsaa = new AfipWsaa($webService,$empresaAlias);
	                      
	                       
	                      try{ 
	                       if ($ta = $wsaa->loginCms())
	                       {
	                           
	                           
	                           
	                                                $token      = $ta['token'];
	                                                $sign       = $ta['sign'];
	                                                $expiration = $ta['expiration'];
	                                                $uniqueid   = $ta['uniqueid'];
	
	                                                //Conectando al WebService de Factura electronica (WsFev1)
	                                                $wsfe = new AfipWsfev1($empresaCuit,$token,$sign);
	                                                
	                                                $this->ArmarIvaAfip($this->request->data['ComprobanteImpuesto'],$ImpIVA,$arrayIVA);
	                                                $this->ArmarImpuestosAfip($this->request->data['ComprobanteImpuesto'],$ImpTrib,$ArrayTrib);
													
													/*logear*/
	                                                $this->ArmarOpcionalesAfip($ArrayOpcional);
	                                                $this->ArmarComprobantesAsociadosAfip($ArrayComprobantesAsoc);
	                                        
	                                           
	                                                
	                                                
	                                                //Obteniendo el ultimo numero de comprobante autorizado
	                                                //$CompUltimoAutorizado = $wsfe->FECompUltimoAutorizado($PtoVta,$CbteTipo);
	                                                $CompUltimoAutorizado['CbteNro'] = $nro_comprobante;
	                                                
	                                             
	                                                if($CompUltimoAutorizado){
	                                                    
	                                                    
	                                                    $CbteDesde = $CompUltimoAutorizado['CbteNro'];
	                                                    $CbteHasta = $CbteDesde;
	                                                    $FeCAEReq = $this->arrayCabeceraAfip($CbteTipo,$PtoVta,$Concepto,$DocTipo,$DocNro,$CbteDesde,$CbteHasta,$CbteFch,$FchServDesde,$FchServHasta,$FchVtoPago,$ImpTotal,$ImpTotConc,$ImpNeto,$ImpOpEx,$ImpIVA,$ImpTrib,$MonId,$MonCotiz);
	                                                    $response = $this->ImpactarComprobanteAfip($FeCAEReq,$wsfe,$id_comprobante,$mensaje,$error,$arrayIVA,$ArrayTrib,$this->request->data,$ArrayOpcional,$ArrayComprobantesAsoc);
	                                                  
	                                                }else{
	                                                    
	                                                    $error = EnumError::ERROR;
	                                                    $mensaje = "Fallo la obtencion del ultimo comprobante";
	                                                }
	                       }else{
	                           
	                           $error = EnumError::ERROR;
	                           $mensaje = "Fall&oacute; la conexi&oacute;n con Afip:  Detalle Error( ".implode(",",$wsaa->getErrLog()).")";
	                           
	                           $this->volverEstadoAnteriorContadoryComprobante($id_comprobante,$this->request->data["TipoComprobante"]["id"],$this->request->data["PuntoVenta"]["id"],$this->request->data["Comprobante"]["nro_comprobante_old"],$this->request->data["Comprobante"]["id_punto_venta_old"]);
	                           
	                          
	                           
	                       }
	                      }catch(Exception $e){
	                          
	                          $error = EnumError::ERROR;
	                          $mensaje = "Fall&oacute; la conexi&oacute;n con Afip: Detalle Error( ".$e->getMessage(). ")";
	                          $this->volverEstadoAnteriorContadoryComprobante($id_comprobante,$this->request->data["TipoComprobante"]["id"],$this->request->data["PuntoVenta"]["id"],$this->request->data["Comprobante"]["nro_comprobante_old"],$this->request->data["Comprobante"]["id_punto_venta_old"]);
	                      }   
	                       
	                }else{
	                    
	                    
	                	
	                	if($saldo == 0){
	                      
	                      $error = EnumError::ERROR;
	                      $mensaje = "Se debe definir el numero inicial del contador para este comprobante o esta factura ya posee CAE.";
	                    
	                	}else{
	                		
	                		
	                		$error = EnumError::ERROR;
	                		$mensaje = "El Cliente al cual le intenta facturar NO tiene habilitada la cuenta corriente, primero debe realizar el Recibo. Una vez realizado puede impactar este comprobante";
	                		
	                		
	                	}
	                      
	                      
	                      
	                }
	       
	       }
	       
	       
	       
	       
	       
	     
	       $message = "";
	       
	       
	       
	       $comprobante = $this->Comprobante->find('first', array(
	       		'conditions' => array(
	       				'Comprobante.id' => $id_comprobante
	       		) ,
	       		'contain' => array("Asiento")
	       ));
	       
	       
	       if($error =="success" && $comprobante["Comprobante"]["cae"]>0 && (!isset($comprobante["Asiento"]) || $comprobante["Asiento"]["id"] == 0  || is_null($comprobante["Asiento"]))){
	       	
	       	 self::generaAsiento($id_comprobante, $message);
	       	 $mensaje = $mensaje.' '.$message;
	       	 
	       }
		}else{//no pudo modificar la fecha_contable
			
			$error = EnumError::ERROR;
			$mensaje = "ERROR: La fecha contable no pudo ser actualizada";
			
		}
		
		if($error == "success"){ //al Obtener el Cae le mando al Front datos para que cargue
		
			
			unset($comprobante["Asiento"]);
       
	        $output = array(
	            "status" => $error,
	            "message" => $mensaje,
	        	"content" => $comprobante,
	        	"page_count"=>0
	        );
        
		}else{
			
			
			
			$output = array(
					"status" => $error,
					"message" => $mensaje,
					"content" => ""
			);
		}
		
		if(isset($this->obtiene_cae_add) && $this->obtiene_cae_add == 1){//quise obtener el cae en el ADD
			
			
			return $output;
			die();
			
		}else{
			
			echo json_encode($output);
			die();
		}
    
        
        
        

      
      
    
    
    
}


protected function StockOnEdit(){
    
    
 
    
}


protected function AceptaStock(){
    
    $this->loadModel("Modulo");
    $this->loadModel("TipoComprobante");
    
    $datoEmpresa = $this->Session->read('Empresa'); //Leo los datos de datoEmpresa en session cargados en Usuarios
    
    $activo = 0;
    
    if(!isset($this->request->data["Comprobante"]["genera_movimiento_stock"]))
       $this->request->data["Comprobante"]["genera_movimiento_stock"] = 0;
    
    
     
            
            //esto es cuando el comprobante tiene mas de un tipo ejemplo factura o NOTA pero NOTA no maneja STOCK
            if(is_array($this->id_tipo_comprobante) && ($this->TipoComprobante->AfectaStock($this->request->data["Comprobante"]["id_tipo_comprobante"]) > 0 && $this->request->data["Comprobante"]["genera_movimiento_stock"]==1 && $this->Modulo->estaHabilitado(EnumModulo::STOCK) == 1) )
                $activo = 1;
            else
                $activo = 0;
      
    
    return $activo;
    
    
    
}


protected function CalcularImpuestos($id_comprobante,$id_tipo_impuesto='',$error=0,$message='')
{
    if(isset($this->id_sistema_comprobante))
	{
        $id_sistema = $this->id_sistema_comprobante;   
    }
	else
	{
        $id_sistema = 0;
    }

   $total_impuestos = 0; 
   
   $tiene_impuesto = 1; 
   
   if(isset($this->request->data[$this->model]["tiene_impuesto"]))
        $tiene_impuesto = $this->request->data[$this->model]["tiene_impuesto"];
            
   if( isset($this->requiere_impuestos) && $this->requiere_impuestos == 1 && $tiene_impuesto == 1 ){
    
       
       
       if($id_sistema == 0)
        $id_sistema = EnumSistema::VENTAS;
   
    
    
    //tomar impuestos de la empresa
    //tomar impuestos del cliente
    //
    /*
            SELECT pia.* FROM persona_impuesto_alicuota pia
        JOIN impuesto i ON i.id = pia.id_impuesto
        WHERE i.id_sistema = 2 AND id_persona=1680

        */
    $this->loadModel("PersonaImpuestoAlicuota");
    $this->loadModel("ComprobanteImpuesto");
 
    
   
        
        /*
  
        $data = $this->PersonaImpuestoAlicuota->find('all', array(
            'joins' => array(
            array(
                'table' => 'impuesto',
                'alias' => 'Impuesto2',
                'type' => 'LEFT',
                'conditions' => array(
                    'Impuesto2.id = PersonaImpuestoAlicuota.id_impuesto'
                )
                
            ),
             array(
                'table' => 'comprobante',
                'alias' => 'Comprobante2',
                'type' => 'LEFT',
                'conditions' => array(
                    'Comprobante2.id ='.$id_comprobante
                )
                
            )
            ),
            'conditions' => array(
                'Impuesto.id_sistema' => $id_sistema,
                'PersonaImpuestoAlicuota.id_persona'=> 'Comprobante2.id_persona'
                
            ),
            )
            );
            */
            
        /*CALCULO LOS IMPUESTOS DE LA PERSONA CON EL COMPROBANTE*/
    $data = $this->getQueryImpuestos($id_comprobante,$id_sistema,$id_tipo_impuesto);
    $comprobantes_impuestos = array();
    $id_impuestos = array();
    
    foreach($data as $valor){
    	
    	if(!isset( $valor["pia"]["sin_vencimiento"]) ||  is_null($valor["pia"]["sin_vencimiento"]) ||  $valor["pia"]["sin_vencimiento"] == 0) 
    		$sin_vencimiento = 0;
    	else
    		$sin_vencimiento = 1;
    	
    		if( ($sin_vencimiento == 1 || ($valor["pia"]["fecha_vigencia_desde"] <= date('Y-m-d') && $valor["pia"]["fecha_vigencia_hasta"] >= date('Y-m-d'))) && $valor[0]["importe_impuesto"]>0)
    		
    	{
         
		         
		         $comprobante_impuesto = array();
		         $comprobante_impuesto["id_comprobante"] = $id_comprobante;   
		         $comprobante_impuesto["id_impuesto"] = $valor["pia"]["id_impuesto"];
		            
		         if(isset($valor["c"]["base_imponible"]) && $valor["c"]["base_imponible"]>0)//esto es por la query desde Ordenes de Pago
		            $comprobante_impuesto["base_imponible"] = (string) round(trim($valor["c"]["base_imponible"]),2);  
		         else
		            $comprobante_impuesto["base_imponible"] = (string) round(trim($valor[0]["base_imponible"]),2);  
		         
		         $comprobante_impuesto["tipo_alicuota_afip"] = trim($valor["ig"]["codigo_afip"]);  
		         $comprobante_impuesto["importe_impuesto"] = (string) round($valor[0]["importe_impuesto"],2);  
		         $comprobante_impuesto["tasa_impuesto"] = $valor["pia"]["tasa_impuesto"];
		         
		         $total_impuestos = $total_impuestos + $comprobante_impuesto["importe_impuesto"];
		         
		         array_push($id_impuestos,$valor["pia"]["id_impuesto"]);
		            
		         
		         /*$iva_item["iva"] = $valor["iva"]["d_iva"];   
		         $iva_item["monto_iva"] = round($valor[0]["monto"],2);
		         $iva_item["tipo_alicuota_afip"] = trim($valor["iva"]["tipo_alicuota_afip"]);
		         $iva_item["base_imponible"] = round(trim($valor[0]["total_sum"]),2);//sumatoria de los items con un mismo tipo de iva sin ese iva. BASE IMPONIBLE
		         
		         */
		         array_push($comprobantes_impuestos,$comprobante_impuesto);
		            
    	}
     }
        
         if(count($comprobantes_impuestos)>0){
            //borro impuestos
            $this->ComprobanteImpuesto->deleteAll(array("ComprobanteImpuesto.id_comprobante"=>$id_comprobante,"ComprobanteImpuesto.id_impuesto"=>$id_impuestos),false);     
            $this->ComprobanteImpuesto->saveAll($comprobantes_impuestos);     
        }    
        
            
            
         
         return round($total_impuestos,2);
   }else
    return 0; 
    
}


    

    
protected function ArmarIvaAfip($impuestos,&$total_iva,&$Iva){
    
    $Iva = array( 'AlicIva' => array ());
                          
                           
                           $total_iva = 0;
                           foreach($impuestos as $ivas){
                              
                               if($ivas['id_impuesto'] == EnumImpuesto::IVANORMAL &&  $ivas["importe_impuesto"]>0 ){
                                      $ivaalicuota = array();
                                      $ivaalicuota["Id"] =   $ivas["tipo_alicuota_afip"];
                                      $ivaalicuota["BaseImp"] =  number_format(abs($ivas["base_imponible"]),2,'.','');
                                      $ivaalicuota["Importe"] =  number_format(abs($ivas["importe_impuesto"]),2,'.','');
                                      $total_iva += $ivas["importe_impuesto"];
                                      array_push($Iva["AlicIva"],$ivaalicuota);    
                               }
                           }
                          
                           $total_iva = number_format(abs($total_iva),2,'.','');
                           
                           
    
}






protected function ArmarOpcionalesAfip(&$Opcional_array){
	

    $this->loadModel(EnumModel::Comprobante);
    $this->loadModel(EnumModel::TipoComprobante);
    
    //FEParamGetTiposOpcional devuelve los ID de los campos opcionales a presentar.
    

   
    
    
    
    //if(in_array($this->request->data["Comprobante"]["id_tipo_comprobante"],array(EnumTipoComprobante::Factu)))
    $facturas_nota_venta_credito = array_merge($this->Comprobante->getTiposComprobanteController(EnumController::FacturasCredito)/*,$this->getTiposComprobanteController(EnumController::NotasCr)*/);
    
    
    $opcionales = array();
	


	if(in_array($this->request->data['Comprobante']['id_tipo_comprobante'], $facturas_nota_venta_credito)){//si el comprobante es algo del regimen MyPime entonces agrego opcionales
		
		$Opcional_array = array( 'Opcional' => array ());
		
		$this->loadModel(EnumModel::TipoComprobante);
		
		$opcional["id"] = 2101;//CBU del EMISOR
		$opcional["valor"] = $this->TipoComprobante->getCuentaBancaria($this->request->data['Comprobante']['id_tipo_comprobante'])["nro_cbu"];
		
		array_push($opcionales, $opcional);
		
		$opcional["id"] = 23;//referencia comercial
		$opcional["valor"] = $this->request->data['Comprobante']['orden_compra_externa'];
		array_push($opcionales, $opcional);
		
		foreach($opcionales as $opcional){
			
			
			$opcion = array();
			
			$opcion["Id"] =   $opcional["id"];
			$opcion["Valor"] =  $opcional["valor"];
			
			
			array_push($Opcional_array["Opcional"],$opcion);
			
		}
		
		
	}
	
	


	
	
	
}



protected function ArmarComprobantesAsociadosAfip(&$Asociados_array){
	
	/*Sobreeescrito en Credito y Debito MiPyme*/
	
	
	
	
	
	
}







protected function ArmarImpuestosAfip($impuestos,&$total_impuestos,&$Tributos){
    
    $Tributos = array('Tributo' => array () );
                          
                           
                           $total_impuestos = 0;
                           
                           foreach($impuestos as $impuesto){
                              
                               if($impuesto['id_impuesto'] != EnumImpuesto::IVANORMAL &&  $impuesto["importe_impuesto"]>0 ){
                                      $ivaalicuota = array();
                                      $ivaalicuota["Id"] =       $impuesto["tipo_alicuota_afip"];
                                      $ivaalicuota["Desc"] =     $impuesto["Impuesto"]["d_impuesto"];
                                      $ivaalicuota["BaseImp"] =  number_format(abs($impuesto["base_imponible"]),2,'.','');
                                      $ivaalicuota["Alic"] =  number_format(abs($impuesto["tasa_impuesto"]),2,'.','');
                                      $ivaalicuota["Importe"] =  number_format(abs($impuesto["importe_impuesto"]),2,'.','');
                                      $total_impuestos += $impuesto["importe_impuesto"];
                                      array_push($Tributos["Tributo"],$ivaalicuota);    
                               }
                           }
                          
                           $total_impuestos = number_format(abs($total_impuestos),2,'.','');
    
}      

protected function arrayCabeceraAfip($CbteTipo,$PtoVta,$Concepto,$DocTipo,$DocNro,$CbteDesde,$CbteHasta,$CbteFch,$FchServDesde,$FchServHasta,$FchVtoPago,$ImpTotal,$ImpTotConc,$ImpNeto,$ImpOpEx,$ImpIVA,$ImpTrib,$MonId,$MonCotiz){
    
	
	/*Nota Credito y Debito MiPyme lo tienen sobreescrito*/
    
    $FeCAEReq = array (
                                'FeCAEReq' => array (
                                    'FeCabReq' => array (
                                        'CantReg' => 1,
                                        'CbteTipo' => $CbteTipo,
                                        'PtoVta' => $PtoVta
                                        ),
                                    'FeDetReq' => array (
                                        'FECAEDetRequest' => array(
                                            'Concepto' => $Concepto,
                                            'DocTipo' => $DocTipo,
                                            'DocNro' => $DocNro,
                                            'CbteDesde' => $CbteDesde,
                                            'CbteHasta' => $CbteHasta,
                                            'CbteFch' => $CbteFch,
                                            'FchServDesde' => $FchServDesde,
                                            'FchServHasta' => $FchServHasta,
                                            'FchVtoPago' => $FchVtoPago,
                                            'ImpTotal' => number_format(abs($ImpTotal),2,'.',''),
                                            'ImpTotConc' => number_format(abs($ImpTotConc),2,'.',''),
                                            'ImpNeto' => number_format(abs($ImpNeto),2,'.',''),
                                            'ImpOpEx' => number_format(abs($ImpOpEx),2,'.',''),
                                            'ImpIVA' => number_format(abs($ImpIVA),2,'.',''),
                                            'ImpTrib' => number_format(abs($ImpTrib),2,'.',''),
                                            'MonId' => $MonId,
                                            'MonCotiz' => $MonCotiz
                                            )
                                        )
                                    ),
                                );
                                
    return $FeCAEReq;
    
    
    
}  



protected function ImpactarComprobanteAfip($FeCAEReq,$wsfe,$id_comprobante,&$mensaje,&$error,$array_ivas_afip,$array_impuestos_afip,$comprobante,$array_opcionales,$array_comprobantes_asoc)
{    
	
	

    $this->loadModel("Modulo");
     try
     {
    if( $FeCAEReq['FeCAEReq']['FeDetReq']['FECAEDetRequest']['ImpTrib'] > 0)
        $FeCAEReq['FeCAEReq']['FeDetReq']['FECAEDetRequest']['Tributos'] = $array_impuestos_afip;
   
    if( $FeCAEReq['FeCAEReq']['FeDetReq']['FECAEDetRequest']['ImpIVA'] > 0 )
        $FeCAEReq['FeCAEReq']['FeDetReq']['FECAEDetRequest']['Iva'] = $array_ivas_afip;
    
    

    if( isset($array_opcionales["Opcional"]) && count($array_opcionales["Opcional"])>0){//si tiene opcionales entonces lo agrego
        	
        	$FeCAEReq['FeCAEReq']['FeDetReq']['FECAEDetRequest']['Opcionales'] = $array_opcionales;
        	
    }
    
    
    if( isset($array_comprobantes_asoc["CbteAsoc"]) && count($array_comprobantes_asoc["CbteAsoc"])>0){//si tiene opcionales entonces lo agrego
    	
    	$FeCAEReq['FeCAEReq']['FeDetReq']['FECAEDetRequest']['CbtesAsoc'] = $array_comprobantes_asoc;

    	
    	
    }
    
    
    
    
    
    $FeCAEResponse = '';
    $FeCAEResponse = $wsfe->FECAESolicitar($FeCAEReq);
    $errores_string = '';
    $conexion_lost = 0;
    if (!$FeCAEResponse)
                {
                    /* Procesando ERRORES */
                    $errores = $wsfe->getErrLog();
                    if (isset($errores))
                    {
                        $errores_string="Error: ";
                        
                        foreach ($errores as $v)
                        {
                            $errores_string.= "&bull; ".$v."\n";
                        }
                    }else{
                      
                        $errores_string.= "ERROR intente nuevamente, se perdio la conexion con afip";
                        $conexion_lost = 1;      
                    }
                }
                elseif (!$FeCAEResponse['FeDetResp']['FECAEDetResponse']['CAE'])
                {
                    /* Procesando OBSERVACIONES */
                    
                    $errores_string.="ERROR AFIP: ";
                     
                    if (isset($FeCAEResponse['FeDetResp']['FECAEDetResponse']['Observaciones']))
                    {
                        foreach ($FeCAEResponse['FeDetResp']['FECAEDetResponse']['Observaciones'] as $v)
                        {
                            foreach($v as $dato){       
                            	$errores_string.= "&bull; Cod.Error:".$dato["Code"]."Desc:".$dato["Msg"]."\n";
                             }
                        }
                    }else
                    {
                       $errores_string.= "ERROR intente nuevamente, observaciones de error perdidas"; 
                       $conexion_lost = 1; 
                    }
                }    

                if(strlen($errores_string) > 0){ //si tuvo errores el impacto

                $error = EnumError::ERROR;
                $mensaje =  $errores_string;
                
                //si "perdi" la respuesta consulto si ya tengo  CAE para ese comprobante
                     
                    $CompUltimoAutorizado = $wsfe->FECompUltimoAutorizado($this->request->data["PuntoVenta"]["numero"],$this->request->data["TipoComprobante"]["codigo_afip"]);
                    
                    if( $CompUltimoAutorizado && $CompUltimoAutorizado['CbteNro'] >0){
                        
                            if( $CompUltimoAutorizado['CbteNro'] != $this->request->data["Comprobante"]["nro_comprobante"] ) { /*Consulto si el ultimo comprobante es diferente del que acabo de pedir autorizacion, si es diferente vuelvo atras*/
                            
                             $this->volverEstadoAnteriorContadoryComprobante($id_comprobante,$this->request->data["TipoComprobante"]["id"],
                             $this->request->data["PuntoVenta"]["id"],$this->request->data["Comprobante"]["nro_comprobante_old"],$this->request->data["Comprobante"]["id_punto_venta_old"],$CompUltimoAutorizado['CbteNro']); 
                             
                             $mensaje .= " Se volvio atras el comprobante, cierre el formulario, abralo nuevamente e impacte otra vez";
                             
                            }else{ //se perdio el CAE a la vuelta entonces lo consulto
                                
                                
                                if($this->ConsultarCae($id_comprobante,$cae,$vencimiento_cae))//esta funcion setea cae y vencimiento_cae
                                {
                                    $this->actualizarCAEComprobante($comprobante,$cae,$vencimiento_cae,$CompUltimoAutorizado['CbteNro'],date('Y-m-d'));//actualizo el cae y los demas datos 
                                    $error = EnumError::SUCCESS;
                                    $mensaje = "El Comprobante ha sido validado electronicamente por afip CAE: ".$cae;
                                    $mensaje_mail = "";
                                    $this->avisoEmail($id_comprobante,$mensaje_mail);
                                }else{
                                
                                    $mensaje.="Se perdi&oacute; la conexi&oacute;n durante la respuesta de AFIP, el comprobante fue aprobado pero el CAE NO FUE recuperado, comuniquese con Valuarit";
                                
                                }
                            }
                     //$FeCAEResponse = $wsfe->FECompConsultar($this->request->data["TipoComprobante"]["codigo_afip"],$this->request->data["Comprobante"]["nro_comprobante"],$this->request->data["PuntoVenta"]["numero"]);
                    }else{
                    	
                    	$error = EnumError::ERROR;
                    	$mensaje .= " Conexi&oacu;ten con AFIP perdida,intente nuevamente";   
                    }
                    
                

                }elseif(isset($FeCAEResponse["FeDetResp"]["FECAEDetResponse"]["CAE"]) && strlen($FeCAEResponse["FeDetResp"]["FECAEDetResponse"]["CAE"])>0){
                                
                                $numero_factura_afip = $FeCAEResponse["FeDetResp"]["FECAEDetResponse"]["CbteDesde"];
                                $cae = $FeCAEResponse["FeDetResp"]["FECAEDetResponse"]["CAE"];
                                $vencimiento_cae = $FeCAEResponse["FeDetResp"]["FECAEDetResponse"]["CAEFchVto"];
                                
                                $fecha_contable = $comprobante["Comprobante"]["fecha_contable"];//fecha en que se aprueba el CAE
                                
                                //se actualiza la factura
                               
                               
                               /*
                                $this->Comprobante->updateAll(
                               
                                    array('Comprobante.cae' => $cae,'Comprobante.nro_comprobante' => $numero_factura_afip,'Comprobante.nro_comprobante_afip' => $numero_factura_afip,'Comprobante.vencimiento_cae' => $vencimiento_cae,'Comprobante.id_estado_comprobante' => EnumEstadoComprobante::CerradoConCae,'Comprobante.fecha_contable'=>$fecha_contable),
                                    array('Comprobante.id' => $id_comprobante) );
                                    
                                    $d_asiento = $comprobante["TipoComprobante"]["codigo_tipo_comprobante"]." ".$this->Comprobante->GetNumberComprobante($comprobante["PuntoVenta"]['numero'],$numero_factura_afip);
                                    
                                    if($this->Modulo->estaHabilitado(EnumModulo::CONTABILIDAD) == 1)
                                        $this->Comprobante->Asiento->updateAll(
                                                                            array('Asiento.d_asiento' => "'".$d_asiento."'" ),
                                                                            array('Asiento.id_comprobante' => $id_comprobante) );
                                    */
                                    
                                    
                                    $this->actualizarCAEComprobante($comprobante,$cae,$vencimiento_cae,$numero_factura_afip,$fecha_contable);
                                    
                                    $error = EnumError::SUCCESS;
                                    $mensaje = "El Comprobante ha sido validado electr&oacute;nicamente por afip CAE: ".$cae;
                                    $mensaje_mail = "";
                                    $this->avisoEmail($id_comprobante,$mensaje_mail);
                            
                            
                            
               }elseif(strlen($errores_string)== 0){
                   
                 $error = EnumError::ERROR;
                 $mensaje =  "Conexi&oacuten perdida,intente nuevamente";   
               }
               
               
               return true;
    
    }catch(Exception $e){
        
        $this->volverEstadoAnteriorContadoryComprobante($id_comprobante,$this->request->data["TipoComprobante"]["id"],$this->request->data["PuntoVenta"]["id"],$this->request->data["Comprobante"]["nro_comprobante_old"],$this->request->data["Comprobante"]["id_punto_venta_old"]); 
        $error = EnumError::ERROR;
        $mensaje =  "ERROR: Conexi&oacuten perdida,intente nuevamente. ".$e->getMessage(); 
        return;
          
        
    }
}



protected function actualizarCAEComprobante($comprobante,$cae,$vencimiento_cae,$numero_comprobante_afip,$fecha_contable){
    
    
        $this->loadModel("Comprobante");
        $this->loadModel("Modulo");
        
        
        /*
        $comprobante_obj = new Comprobante();
        $saldo = $comprobante_obj->getComprobantesImpagos($comprobante["Comprobante"]["id_persona"],1,$comprobante["Comprobante"]["id"],1,EnumSistema::VENTAS,0,EnumMoneda::Peso);
        

        if($saldo <= 0.01)
        	$estado_comprobante = EnumEstadoComprobante::Cumplida;
        else
        	$estado_comprobante = EnumEstadoComprobante::CerradoConCae;
        	*/	
        
        $estado_comprobante = EnumEstadoComprobante::CerradoConCae;
        $this->Comprobante->updateAll(
        		array('Comprobante.cae' => $cae,'Comprobante.nro_comprobante' => $numero_comprobante_afip,'Comprobante.nro_comprobante_afip' => $numero_comprobante_afip,'Comprobante.vencimiento_cae' => "'".$vencimiento_cae."'",'Comprobante.id_estado_comprobante' => $estado_comprobante,'Comprobante.fecha_contable'=> "'".$fecha_contable."'"),
                                    array('Comprobante.id' => $comprobante["Comprobante"]["id"]) );
                                    
                                    $d_asiento = $comprobante["TipoComprobante"]["codigo_tipo_comprobante"]." ".$this->Comprobante->GetNumberComprobante($comprobante["PuntoVenta"]['numero'],$numero_comprobante_afip);
                                    
        if($this->Modulo->estaHabilitado(EnumModulo::CONTABILIDAD) == 1)
            $this->Comprobante->Asiento->updateAll(
                                                array('Asiento.d_asiento' => "'".$d_asiento."'" ),
                                                array('Asiento.id_comprobante' => $comprobante["Comprobante"]["id"]) );
                                                                            
                                                                            
}

protected function getQueryImpuestos($id_comprobante,$id_sistema,$id_tipo_impuesto,$array_impuestos_not_in=array()){
    
    $this->loadModel("PersonaImpuestoAlicuota");
    
    
    $query_tipo_impuesto = '';
    if($id_tipo_impuesto>0)
        $query_tipo_impuesto = "AND i.id_tipo_impuesto=".$id_tipo_impuesto;
    
    $query = "SELECT 
                                                c.id AS id_comprobante,
                                                c.id_moneda AS id_moneda,
                                                pia.id_impuesto,
                                                c.subtotal_neto AS base_imponible,
                                                (c.subtotal_neto * pia.porcentaje_alicuota) / 100 AS importe_impuesto,
                                                pia.porcentaje_alicuota AS tasa_impuesto,
                                                ig.codigo_afip,
												pia.fecha_vigencia_desde,
												pia.fecha_vigencia_hasta,
												pia.sin_vencimiento
                                              FROM
                                                persona_impuesto_alicuota pia 
                                                JOIN impuesto i 
                                                  ON i.id = pia.id_impuesto 
                                                JOIN comprobante c 
                                                  ON c.id = ".$id_comprobante."
                                                LEFT JOIN impuesto_geografia ig
                                                  ON ig.id = i.id_impuesto_geografia 
                                              WHERE i.id_sistema = ".$id_sistema." 
                                                AND pia.id_persona = c.id_persona
                                                ".$query_tipo_impuesto."
                                              AND  c.subtotal_neto/c.valor_moneda2 > i.base_imponible_desde
                                              AND  c.subtotal_neto/c.valor_moneda2 < i.base_imponible_hasta 
                                              AND (c.subtotal_neto * pia.porcentaje_alicuota)>0
											  AND pia.activo = 1
                                          
                                              
                                                                                              
                                              ";
    
    return $this->PersonaImpuestoAlicuota->query($query);
    
}



protected function getImpuestos($comprobante_impuesto_array,$id_comprobante,$array_id_sistema,$in_impuestos,$id_tipo_impuesto,$id_iva=0){
    
    
    //$not_in_impuestos define aquellos impuestos que no queres traer. En el caso de una factura en la cual se piede impuestos normales entonces excluyo el IVA
    //$array_id_sistema define un array de sistemas que quiero traer. Ejemplo compras y ventas
    App::import('Model','Iva');
    
    $data = $comprobante_impuesto_array;//obtengo los impuestos que NO IVA si es vacio trae VENTAS tiene que estar relacionado asi 'ComprobanteImpuesto'=>array('Impuesto'=>array('Sistema'))
    
    
    $iva_obj = new Iva();
    
    $comprobantes_impuestos = array();
    
    
       
     foreach($data as $valor){
         
         
         $comprobante_impuesto = array();
         
         
         if(count($in_impuestos)>0)
            $condicion = in_array($valor["Impuesto"]["Sistema"]["id"],$array_id_sistema) && in_array($valor["id_impuesto"],$in_impuestos) && $valor["Impuesto"]["id_tipo_impuesto"]== $id_tipo_impuesto;
         else
            $condicion =  in_array($valor["Impuesto"]["Sistema"]["id"],$array_id_sistema) && $valor["Impuesto"]["id_tipo_impuesto"]== $id_tipo_impuesto;
         
         
      
         
         
         if($condicion ){
             $comprobante_impuesto["id_comprobante"] = $id_comprobante;   
             $comprobante_impuesto["id_impuesto"] = $valor["id_impuesto"];   
             $comprobante_impuesto["base_imponible"] =(string)  round(trim($valor["base_imponible"]),2);  
             $comprobante_impuesto["d_impuesto"] = $valor["Impuesto"]["d_impuesto"];  
             $comprobante_impuesto["abreviado_impuesto"] = $valor["Impuesto"]["abreviado_impuesto"]; 
             $comprobante_impuesto["d_iva_detalle"] = "";
             
             if(isset($valor["tipo_alicuota_afip"]) && $valor["tipo_alicuota_afip"]>0) 
                $comprobante_impuesto["d_iva_detalle"] = $iva_obj->getDetallePorAlicuotaAfip($valor["tipo_alicuota_afip"]);  
             
             if(isset($valor["Impuesto"]["ImpuestoGeografia"]["codigo_afip"]) && $valor["Impuesto"]["ImpuestoGeografia"]["codigo_afip"]>0)
                $comprobante_impuesto["tipo_alicuota_afip"] = (string) trim($valor["Impuesto"]["ImpuestoGeografia"]["codigo_afip"]);  
             
             $comprobante_impuesto["importe_impuesto"] = (string) round($valor["importe_impuesto"],2);  
             $comprobante_impuesto["tasa_impuesto"] = $valor["tasa_impuesto"];
             
             array_push($comprobantes_impuestos,$comprobante_impuesto);
         
         }
            
        }
        
        return $comprobantes_impuestos;
        
        
        
        
        
    
    
}



/*Este metodo recibe un array de impuestos y los sumariza*/
protected function TotalImpuestos($impuestos){
    
    $total = 0;
    
    foreach($impuestos as $impuesto){
        
        $total = $total + $impuesto["importe_impuesto"];
        
        
    }
    
    return $total;
}

protected function chequeoContador($id_tipo_comprobante,$id_punto_venta='',$id_impuesto=''){
    
    $this->loadModel("ComprobantePuntoVentaNumero");
    
    $conditions = array();
    
    
    if($id_tipo_comprobante != '')
    	array_push($conditions,array('ComprobantePuntoVentaNumero.id_tipo_comprobante' => $id_tipo_comprobante));
    
    
    if($id_punto_venta!= '')
    	array_push($conditions,array('ComprobantePuntoVentaNumero.id_punto_venta' => $id_punto_venta));
    
    
    if($id_impuesto!= '')
    	array_push($conditions,array('ComprobantePuntoVentaNumero.id_impuesto' => $id_impuesto));
    
    	
    	
    if($id_punto_venta!=''){
      
        $cpvn = $this->ComprobantePuntoVentaNumero->find('all', array(
        												'conditions' => $conditions,
                                                'contain' =>false
                                                ));
                                                
                                                
        
    }else{
        
        
        if($this->Comprobante->TipoComprobante->requiereContador($id_tipo_comprobante) == 0  )      /*El comprobante factura de compra no necesita contador al igual que remito de compra*/
        {
            
          $cpvn = true;  
        }else{
            $cpvn = $this->ComprobantePuntoVentaNumero->find('all', array(
                                                'conditions' => array('ComprobantePuntoVentaNumero.id_tipo_comprobante' => $id_tipo_comprobante),
                                                'contain' =>false
                                                ));
        
       } 
        
    }
    
    
    
    if($cpvn && count($cpvn)>0)
        return true;
    else
        return false;
    
    
}
    
    

    
protected function actualizoMontos($id_c,$nro_c,$total_iva,$total_impuestos){
  			
	
			$this->loadModel("Comprobante");
        
            $descuento = 0;
            
            if(isset($this->request->data[$this->model]["descuento"]))
                $descuento = $this->request->data[$this->model]["descuento"];
                
            if(!isset($this->request->data[$this->model]["subtotal_bruto"]))
                $this->request->data[$this->model]["subtotal_bruto"]=0;    
            
            $importe_descuento = round((  round(($descuento/100),4)  )*($this->request->data[$this->model]["subtotal_bruto"]),2);
            $total_comprobante = round(($this->request->data[$this->model]["subtotal_bruto"] - $importe_descuento) + $total_iva + $total_impuestos,2)  ;
            
            $array_actualizacion =   array("Comprobante.importe_descuento" => $importe_descuento,"Comprobante.total_comprobante" => $total_comprobante);  
            $this->Comprobante->updateAll($array_actualizacion,
                                                    array('Comprobante.id' => $id_c) );       
        
        
    }
    
    
protected function getNeto($comprobante,$flag_descuento){ //$flag_descuento en 1 devuelvo sin descuento


            $importe_descuento = round((  round(($comprobante["descuento"]/100),2)  )*($comprobante["subtotal_bruto"]),2);
            $total_neto = round(($comprobante["subtotal_bruto"] - $importe_descuento),2)  ;
            return $total_neto;
    
    
    
}


protected function  getComprobantesRelacionados($id_comprobante,$id_tipo_comprobante=0,$generados=1,$incluir_comprobante_item_comprobante=1,$estados_comprobante_exlcuir= array()){
	
	return $this->Comprobante->getComprobantesRelacionados($id_comprobante,$id_tipo_comprobante,$generados,$incluir_comprobante_item_comprobante,$this->model,$estados_comprobante_exlcuir);
	
}

         
protected function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados=1){ //si el id_tipo_comprobante es 0 devuelvo todos
      /*Esta funcion acepta parametros por get y por post*/
      
      
      if($id_tipo_comprobante == 0)//sino lo manda por URL busco el filtro
        $id_tipo_comprobante = $this->getFromRequestOrSession('Comprobante.id_tipo_comprobante'); 
      if($id_tipo_comprobante == '')//sino lo mando tampoco por el filtro lo seteo en 0
        $id_tipo_comprobante = 0;
        
      $id_sistema = $this->getFromRequestOrSession('Comprobante.id_sistema');  
        
      
        
        
     $comprobantes_relacionados = $this->getComprobantesRelacionados($id_comprobante,$id_tipo_comprobante,1); 
     
     $comprobantes_relacionados_final = array();
     $array_tipos = explode(",",$id_tipo_comprobante); 
     
     
     foreach($comprobantes_relacionados as $key=>$comprobante){


        if($id_tipo_comprobante !=0 ){
            
         if( isset($comprobante["Comprobante"]) && !in_array($comprobante["Comprobante"]["id_tipo_comprobante"],$array_tipos))
            unset($comprobantes_relacionados[$key]);
        }
        
         if($id_sistema !=0 ){
            
             if( isset($comprobante["Comprobante"]) && $comprobante["TipoComprobante"]["id_sistema"] != $id_sistema)
             unset($comprobantes_relacionados[$key]);
         
         }
    


     }
     
     foreach($comprobantes_relacionados as $comprobante){
     
        
        if(!isset($comprobante["ComprobanteItem"])) 
         array_push($comprobantes_relacionados_final,$comprobante);
     }
     
     
     
     $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Comprobante",
                    "content" => $comprobantes_relacionados_final
                );
        
          echo json_encode($output);
          die();
        
        
    }
    
    
protected function borraImpuestos($id_comprobante){
        
        
        $this->Comprobante->ComprobanteImpuesto->deleteAll(array('ComprobanteImpuesto.id_comprobante' => $id_comprobante));
        
    }
    
    
    
protected function CerrarConEstado(){
    
    /*Esta logica solo sirve para FACTURA si el comprobante ES DEFINITIVO se cierra y puede afectar stock y lo demas*/
    
    
   

      
      
   
            if(                                         $this->Comprobante->getDefinitivo($this->request->data) == 1  
            
                                                         &&   $this->request->data["Comprobante"]["id_estado_comprobante"] != EnumEstadoComprobante::CerradoConCae    /*Esto lo pongo por la factura si esta cerrada con CAE ya es definitiva*/
            
                                                        && $this->request->data["Comprobante"]["id_estado_comprobante"] != EnumEstadoComprobante::Anulado       /*Si es anulado no lo cambio a CerradoReimpresion*/
                                                        
              ){
                  
                  
               switch($this->request->data["Comprobante"]["id_tipo_comprobante"]){
   
        
        
                    case EnumTipoComprobante::FacturaA:    
                    case EnumTipoComprobante::FacturaB:    
                    case EnumTipoComprobante::FacturaC:    
                    case EnumTipoComprobante::FacturaE:    
                    case EnumTipoComprobante::FacturaM:    
                    case EnumTipoComprobante::ReciboA:    
                    case EnumTipoComprobante::ReciboB:    
                    case EnumTipoComprobante::ReciboManual:    
                    case EnumTipoComprobante::ReciboAutomatico:    
                    case EnumTipoComprobante::NotaCreditoA:    
                    case EnumTipoComprobante::NotaCreditoB:    
                    case EnumTipoComprobante::NotaCreditoC:    
                    case EnumTipoComprobante::NotaCreditoM:    
                    case EnumTipoComprobante::NotaDebitoA: 
                    case EnumTipoComprobante::NotaDebitoB: 
                    case EnumTipoComprobante::NotaDebitoC:     
                    case EnumTipoComprobante::NotaDebitoM:     
                    case EnumTipoComprobante::FacturaCreditoA: 
                    case EnumTipoComprobante::FacturaCreditoB: 
                    case EnumTipoComprobante::FacturaCreditoC: 
                    case EnumTipoComprobante::NotaCreditoAFacturaCredito: 
                    case EnumTipoComprobante::NotaCreditoBFacturaCredito: 
                    case EnumTipoComprobante::NotaCreditoCFacturaCredito: 
                    case EnumTipoComprobante::NotaDebitoAFacturaCredito: 
                    case EnumTipoComprobante::NotaDebitoBFacturaCredito: 
                    case EnumTipoComprobante::NotaDebitoCFacturaCredito: 
                  
                  
                  
                
                                         //$this->loadModel("Movimiento");
                                         
                    	if(!isset($this->request->data["Comprobante"]["fecha_contable"]) ||$this->request->data["Comprobante"]["fecha_contable"] == null || $this->request->data["Comprobante"]["id_estado_comprobante"] == "")
                    		$fecha_contable = "'".date("Y-m-d")."'" ;
                        else
                        	$fecha_contable =  "'".$this->request->data["Comprobante"]["fecha_contable"]."'";
                                         
                                         
                                         
                                         $this->Comprobante->updateAll(
                                                                            array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::CerradoReimpresion,'Comprobante.fecha_contable' => $fecha_contable,'Comprobante.definitivo'=>1 ),
                                                                            array('Comprobante.id' => $this->request->data["Comprobante"]["id"]) );
                                                                            
                                        $this->request->data["Comprobante"]["id_estado_comprobante"] =  EnumEstadoComprobante::CerradoReimpresion; 
                                                                            
                                        //$this->Movimiento->revertirItems($this->request->data);Esto es para revertir un comprobante
              break; 
             
                                                
               }
               
               
               
               if( isset($this->request->data["Comprobante"]["fecha_cierre"]) && $this->request->data["Comprobante"]["fecha_cierre"] !="" ){
               	$fecha_cierre = "'".date("Y-m-d")."'";
                  $this->Comprobante->updateAll(
                                                                            array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::CerradoReimpresion,'Comprobante.fecha_cierre' => $fecha_cierre,'Comprobante.definitivo'=>1 ),
                                                                            array('Comprobante.id' => $this->request->data["Comprobante"]["id"]) );
               }
}





        
        
        
  
    
    
}


protected function esFactura(){
    
    
    
}


protected function CancelarComprobante($id_comprobante){
    
   
    //busco el item antes de que sea modificado para ver estado anterior
    $comprobante = $this->Comprobante->find('first', array(
                                'conditions' => array('Comprobante.id' => $id_comprobante),
                                'contain' =>false
                                )); 
                                
    if($comprobante){
        $this->loadModel("Movimiento");
        if($this->reversible($comprobante)){
            
            $this->Movimiento->revertirItems($comprobante);//Esto es para revertir un comprobante
            
            $this->Comprobante->updateAll(
                                array('Comprobante.id_estado_comprobante'=>EnumEstadoComprobante::Anulado),
                                array('Comprobante.id' => $id_comprobante) );
                                
            $message = "El comprobante seleccionado ha sido dado de baja";
            $error = EnumError::SUCCESS;
        }else{
            
            $message = "El comprobante seleccionado no es posible cancelarlo";
            $error = EnumError::ERROR;
        } 
    }else{
        
        $message = "El comprobante seleccionado no existe";
        $error = EnumError::ERROR;
    }
    
    
     $output = array(
                "status" => $error,
                "message" => $message,
                "content" => ""
            );
    
      echo json_encode($output);
}



protected function reversible(){ //chequea si un comprobante puede eliminarse


    return true;
    
    
}

//MP: no se usa mas desde que se cambio el modelo de despacho de stock todo se hace por movimiento manual de despacho.
protected function revertirItems(){//NOSE USA
    
    $this->loadModel("Movimiento");
    
      $datoEmpresa = $this->Session->read('Empresa'); //Leo los datos de datoEmpresa en session cargados en Usuarios
      
      
     /*
     
     Se usa solo para factura por ahora para revertir el comprometido requiere un flag (revertir_items_origen) en 1 para ser ejecutado 
                    ademas chequea si los items ya fueron revertidos, esta definida en movimientos model 
     recibe el data porque usa los ComprobanteItems

     */       
     if( $this->AceptaStock() == 1 && $this->Modulo->estaHabilitado(EnumModulo::STOCK) == 1 && isset($this->request->data["Comprobante"]["revertir_items_origen"]) &&  $this->request->data["Comprobante"]["revertir_items_origen"] == 1 && isset($this->request->data["ComprobanteItem"])  &&  $this->Comprobante->getDefinitivo($this->request->data) == 1    )
        $this->Movimiento->revertirItems($this->request->data);
}


/*Este Metodo evalua si hay un campo repetido con un valor determinado en la tabla Comprobantes*/

protected function chequeoValorNoRepetido($nombre_campo,$valor,$id_comprobante,$id_persona=0,$id_tipo_comprobante){
    
    
	/*Seguridad Usuario B2B*/
	if($this->Auth->user('id_persona')>0 && $this->Auth->user('id_rol') == EnumRol::SivitB2B){
			
			$id_persona = $this->Auth->user('id_persona');
			
	}
		
     $this->loadModel("Comprobante");
     $resultados = $this->Comprobante->find('count',array(
                                                    'conditions' =>array(
                                                                    'Comprobante.id !='=>$id_comprobante,
                                                                    'Comprobante.'.$nombre_campo.''=>$valor,
                                                                    'Comprobante.id_tipo_comprobante'=>$id_tipo_comprobante,
                                                                    'Comprobante.id_persona '=>$id_persona
                                                                    )
                                                    ));
     $message = "";
     if($resultados >0){
     	$error = EnumError::ERROR;
     	
     }else{
     	$error = EnumError::SUCCESS;
     }
     
     $output = array(
     		"status" => $error,
     		"message" => $message,
     		"content" => $resultados,
     		"message_type"=>EnumMessageType::Modal
     );
     
     $this->set($output);
     $this->set("_serialize", array("status", "message", "content"));
    
}



protected function cerrarComprobantesAsociados($comprobante){
    
    
    if($comprobante){
    	
    	
    	$valido = 0;
        
        switch($comprobante["Comprobante"]["id_tipo_comprobante"]){
            
            case EnumTipoComprobante::FacturaA:
            case EnumTipoComprobante::FacturaB:
            case EnumTipoComprobante::FacturaC:
            case EnumTipoComprobante::FacturaE:
            case EnumTipoComprobante::FacturaM:
            case EnumTipoComprobante::FacturaCreditoA:
            case EnumTipoComprobante::FacturaCreditoB:
            case EnumTipoComprobante::FacturaCreditoC: 
            $tipos_comprobante = 	$this->Comprobante->getTiposComprobanteController(EnumController::Facturas);
            	$valido = 1;
            	break;
            	
           
            case EnumTipoComprobante::FACTURASACOMPRA:
            case EnumTipoComprobante::FACTURASBCOMPRA:
            case EnumTipoComprobante::FACTURASCCOMPRA:
            case EnumTipoComprobante::GASTOA:
            case EnumTipoComprobante::GASTOB:
            case EnumTipoComprobante::GASTOC:
            case EnumTipoComprobante::ReciboACompra:
            case EnumTipoComprobante::ReciboBCompra:
            case EnumTipoComprobante::ReciboCCompra:
            case EnumTipoComprobante::ReciboMCompra:
            	$tipos_comprobante = 	$this->Comprobante->getTiposComprobanteController(EnumController::FacturasCompra);
            	$valido = 1;
            	break;
            
        }
        
        
        
        if($valido == 1){
        	
	        if( ($comprobante["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion || $comprobante["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoConCae) &&  $this->Comprobante->getDefinitivo($this->request->data) == 1  ){
	        	if(isset($comprobante["ComprobanteItem"])){ //Si tiene Items la factura entonces pregunto por cada ID de PEdido Interno
	        		
	        		foreach($comprobante["ComprobanteItem"] as $item_comprobante){
	        			
	        			if(isset($item_comprobante["id_comprobante_item_origen"])){ // SI es un Item de factura importado proveniente de un PI entonces si lo debe cerrar a ese PI
	        				
	        				//Busco el id_comprobante asociado a ese ITEM
	        				
	        				$comprobante_origen = $this->Comprobante->ComprobanteItem->find('first', array(
	        				'conditions' => array('ComprobanteItem.id' => $item_comprobante["id_comprobante_item_origen"]),
	        				'contain' =>array('Comprobante')
	        				));
	        				
	        				
	        				if($comprobante_origen)
	        					$this->Comprobante->cerrar_comprobantes_asociados($comprobante_origen["ComprobanteItem"]["id_comprobante"],$tipos_comprobante);
	        					
	        			}
	        		}
	        		
	        	}
	        	
	        	
	        }  
        
        }
        
        
        
        
    }
    
    
}


protected function abrirComprobantesAsociados($comprobante){
	
	
	if($comprobante){
		
		switch($comprobante["Comprobante"]["id_tipo_comprobante"]){
			
			case EnumTipoComprobante::FacturaA:
			case EnumTipoComprobante::FacturaB:
			case EnumTipoComprobante::FacturaC:
			case EnumTipoComprobante::FacturaE:
			case EnumTipoComprobante::FACTURASACOMPRA:
			case EnumTipoComprobante::FACTURASBCOMPRA:
			case EnumTipoComprobante::FACTURASCCOMPRA:
			case EnumTipoComprobante::GASTOA:
			case EnumTipoComprobante::GASTOB:
			case EnumTipoComprobante::GASTOC:
			case EnumTipoComprobante::ReciboACompra:
			case EnumTipoComprobante::ReciboBCompra:
			case EnumTipoComprobante::ReciboCCompra:
			case EnumTipoComprobante::ReciboMCompra:
			case EnumTipoComprobante::FacturaCreditoA:
			case EnumTipoComprobante::FacturaCreditoB:
			case EnumTipoComprobante::FacturaCreditoC: 
				
				
				
				if( ($comprobante["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion) &&  $this->Comprobante->getDefinitivo($this->request->data) == 1  ){
					if(isset($comprobante["ComprobanteItem"])){ //Si tiene Items la factura entonces pregunto por cada ID de PEdido Interno
						
						foreach($comprobante["ComprobanteItem"] as $item_comprobante){
							
							if(isset($item_comprobante["id_comprobante_item_origen"])){ // SI es un Item de factura importado proveniente de un PI entonces si lo debe cerrar a ese PI
								
								//Busco el id_comprobante asociado a ese ITEM
								
								$comprobante_origen = $this->Comprobante->ComprobanteItem->find('first', array(
										'conditions' => array('ComprobanteItem.id' => $item_comprobante["id_comprobante_item_origen"]),
										'contain' =>array('Comprobante')
								));
								
								
								if($comprobante_origen)
									$this->Comprobante->abrir_comprobantes_asociados($comprobante_origen["ComprobanteItem"]["id_comprobante"],array($comprobante["Comprobante"]["id_tipo_comprobante"]));
									
							}
						}
						
					}
					
					
				}
				
				
				
				
				
		}
		
		
	}
	
	
}


protected function pdfExportLote($id_comprobante_desde,$id_comprobante_hasta){
    
    
}


protected function Auditar($id,$data,$nro_comprobante =0,$impresion_pdf = 0){
    
    
	
	$this->loadModel("AuditoriaComprobante");
    $id_usuario = $this->Auth->user('id');
    $fecha_modificacion = date("Y-m-d H:i:s");
    
    

    
    $data_auditoria = array();
    $data_auditoria["AuditoriaComprobante"]["fecha"] = $fecha_modificacion;
    $data_auditoria["AuditoriaComprobante"]["impresion_pdf"] = $impresion_pdf;
    $data_auditoria["AuditoriaComprobante"]["id_usuario"] = $id_usuario;
    $data_auditoria["AuditoriaComprobante"]["id_comprobante"] = $id;
    $data_auditoria["AuditoriaComprobante"]["id_estado_comprobante_nuevo"] = $data["Comprobante"]["id_estado_comprobante"];
    
    if($nro_comprobante !=0)
    	$data_auditoria["AuditoriaComprobante"]["nro_comprobante"] = $nro_comprobante;
    else 
    	$data_auditoria["AuditoriaComprobante"]["nro_comprobante"] = $data["Comprobante"]["nro_comprobante"];
    
    if( isset($data["Comprobante"]["id_punto_venta"]) && $data["Comprobante"]["id_punto_venta"]>0)
    	$data_auditoria["AuditoriaComprobante"]["id_punto_venta"] = $data["Comprobante"]["id_punto_venta"];
    
   		 
    if( isset($data["Comprobante"]["d_punto_venta"]) && $data["Comprobante"]["d_punto_venta"]!="")
    		$data_auditoria["AuditoriaComprobante"]["d_punto_venta"] = $data["Comprobante"]["d_punto_venta"];
    
   $this->AuditoriaComprobante->save($data_auditoria);

    
}



public function Anular($id_comprobante){
    
  
  return;
  
    
    
}


public function getComprobantesAsociados($id_comprobante){  
}





private function volverEstadoAnteriorContadoryComprobante($id_comprobante,$id_tipo_comprobante,$id_punto_venta,$nro_comprobante_old,$id_punto_venta_old,$ultimo_comprobante_autorizado=0){
    
    
                  $this->loadModel("Comprobante");
                  
            
                 
                   
                 
                    
                    
                   if($ultimo_comprobante_autorizado !=0){ //si tengo el ultimo numero de afip entonces actualizo mi contador local
                      
                    $this->setNumeroComprobante($id_tipo_comprobante,$id_punto_venta,$ultimo_comprobante_autorizado);//como obtuve el ultimo numero de afip se lo seteo en la BD  
                    //$this->obetener_ultimo_numero_comprobante($id_tipo_comprobante,$id_punto_venta,5,"+");//incremento el contador en 1
                    //$nro_comprobante_old = $ultimo_comprobante_autorizado+1; //la factura que estoy haciendo le seteo el numero de afip + 1
                    
                  }else{
                      
                      $nro_comprobante = $this->obetener_ultimo_numero_comprobante($id_tipo_comprobante,$id_punto_venta,5,"-"); //Le devuelvo el numero al contador
                  }
                  
                  if ($this->Comprobante->PuntoVenta->EsFacturaElectronica($id_punto_venta)!= 1)
                    $nro_factura_afip = "";
                    else
                    $nro_factura_afip =  $nro_comprobante_old;
                    
                    
                  if($nro_comprobante_old>0 && $nro_factura_afip>0 && $id_comprobante>0)
	                  $this->Comprobante->updateAll(
	                                    array('Comprobante.nro_comprobante' => $nro_comprobante_old,
	                                            'Comprobante.nro_comprobante_afip' => $nro_factura_afip,
	                                            'Comprobante.id_punto_venta'=> $id_punto_venta_old),
	                                    array('Comprobante.id' => $id_comprobante) 
	                                    );
}



/*
private function NecesitaPuntoVenta($id_tipo_comprobante){
    
 
 
 

 if( $this->Comprobante->TipoComprobante->requiereContador($id_tipo_comprobante) == 0 ){
     
     return false;
     
 }else{
     return true;
 }   
    
    
}
*/


    
private function NecesitaNroComprobante($id_tipo_comprobante,$id_punto_venta){
	$this->loadModel("ComprobantePuntoVentaNumero");
 
 if($id_punto_venta != ''){

 	if( $this->ComprobantePuntoVentaNumero->permiteModificarNroComprobante($id_tipo_comprobante,$id_punto_venta) == 1 )
  		return false;
 	else
 		return true;
    
 }else{
 	
 	return false;//sino me pasa el id_punto_Venta NO necesita nro_comprobante. Siempre el nro_comprobante esta asociado a un punto venta/sucursal
 }
    
}
    

    

                 
                

    /**
    * @secured(CONSULTA_COMPROBANTE)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
    
    
    /**
     * @secured(CONSULTA_COMPROBANTE_IMPAGO)
     */
    public function getComprobantesCuentaCorriente($id_persona=0, $id_sistema = 0,$id_cartera_rendicion='',$id_moneda=EnumMoneda::Peso){
  
    	
    	
    	
	    
	    	$fecha_entrega_desde = $this->getFromRequestOrSession('Comprobante.fecha_contable');
	    	$fecha_entrega_hasta = $this->getFromRequestOrSession('Comprobante.fecha_contable_hasta');
	    	$nro_comprobante = $this->getFromRequestOrSession('Comprobante.nro_comprobante');
	    	$fecha_vencimiento_desde = $this->getFromRequestOrSession('Comprobante.fecha_vencimiento');
	    	$fecha_vencimiento_hasta = $this->getFromRequestOrSession('Comprobante.fecha_vencimiento_hasta');
	    	$id_sistema = $this->getFromRequestOrSession('Comprobante.id_sistema');
	    	$listado_id_tipo_comprobante = $this->getFromRequestOrSession('TipoComprobante.listado_id_tipo_comprobante'); //separado por coma
	    	$id_comprobante= $this->getFromRequestOrSession('Comprobante.id'); //pueden separado por coma
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    }
        
    /**
    * @secured(CONSULTA_COMPROBANTE_IMPAGO)
    */
    //public function getComprobantesImpagos($id_persona,  $considerar_notas = 0, $id_sistema = EnumSistema::VENTAS){
    public function getComprobantesImpagos($id_persona=0, $id_sistema = 0,$id_cartera_rendicion='',$id_moneda=EnumMoneda::Peso){
            
           $considerar_notas = 1; // TODO: Eliminar no se usa mas
            /*  
            *$considerar_notas  EsTE PARAMETRO DEFINE SI SE RESTAN LAS NC O SE SUMAN LAS ND AUTOMATICAMENTE
            */
            
           $saldo_desde = $this->getFromRequestOrSession('Comprobante.saldo_desde'); 
           $saldo_hasta = $this->getFromRequestOrSession('Comprobante.saldo_hasta'); 
           $fecha_entrega_desde = $this->getFromRequestOrSession('Comprobante.fecha_entrega_desde'); 
           $fecha_entrega_hasta = $this->getFromRequestOrSession('Comprobante.fecha_entrega_hasta'); 
           $nro_comprobante = $this->getFromRequestOrSession('Comprobante.nro_comprobante'); 
           $fecha_vencimiento_desde = $this->getFromRequestOrSession('Comprobante.fecha_vencimiento');
           $fecha_vencimiento_hasta = $this->getFromRequestOrSession('Comprobante.fecha_vencimiento_hasta'); 
		   
		   			
		   $fecha_generacion_desde = $this->getFromRequestOrSession('Comprobante.fecha_generacion');
           $fecha_generacion_hasta = $this->getFromRequestOrSession('Comprobante.fecha_generacion_hasta'); 
		   
           $id_sistema = $this->getFromRequestOrSession('Comprobante.id_sistema'); 
           $listado_id_tipo_comprobante = $this->getFromRequestOrSession('TipoComprobante.listado_id_tipo_comprobante'); //separado por coma
           $id_comprobante= $this->getFromRequestOrSession('Comprobante.id'); //pueden separado por coma
		   
		   
		  if($fecha_generacion_desde !="")
				array_push($this->array_filter_names,array("Fecha Generacion Dsd.:"=>$this->{$this->model}->formatDate($fecha_generacion_desde)));	
           if($fecha_generacion_hasta!="")
				array_push($this->array_filter_names,array("Fecha Generacion Hta.:"=>$this->{$this->model}->formatDate($fecha_generacion_hasta)));
           
           
           if($fecha_vencimiento_desde !="")
				array_push($this->array_filter_names,array("Fecha Vencimiento Dsd.:"=>$this->{$this->model}->formatDate($fecha_vencimiento_desde)));	
           if($fecha_vencimiento_hasta!="")
				array_push($this->array_filter_names,array("Fecha Vencimiento Hta.:"=>$this->{$this->model}->formatDate($fecha_vencimiento_hasta)));
           
           
           if($fecha_entrega_desde!="")
           		array_push($this->array_filter_names,array("Fecha Entrega Dsd.:"=>$this->{$this->model}->formatDate($fecha_entrega_desde)));
           	
           if($fecha_entrega_hasta!="")
           		array_push($this->array_filter_names,array("Fecha Entrega Hta.:"=>$this->{$this->model}->formatDate($fecha_entrega_hasta)));
			
	
           
           if($id_sistema == "")
           		$id_sistema = EnumSistema::VENTAS;
          
           if($id_persona == 0)
           		$id_persona  = $this->getFromRequestOrSession('Comprobante.id_persona'); 
           
           	if($id_persona == "")
           		$id_persona = 0;
           
           if($saldo_desde == "")
           		$saldo_desde = 0;
           
           		
           	if($saldo_hasta== "")
           		$saldo_hasta= 999999999999999999999999;
           
           
           if($this->xlsReport == 1)
           	$llamada_interna = 1;
           else 
           	$llamada_interna = 0;
           
           	$array_tipo_comprobante = array();
           	
           	if($listado_id_tipo_comprobante!=''){
           		
           		$array_tipo_comprobante =  explode(",", $listado_id_tipo_comprobante);
           	}
           	
           $data = 0;
           $this->loadModel("Comprobante"); 
           $this->Comprobante->getComprobantesImpagos($id_persona,0,$id_comprobante,$llamada_interna,$id_sistema,$id_cartera_rendicion,$id_moneda,$saldo_desde,$saldo_hasta,$fecha_entrega_desde,$fecha_entrega_hasta,$nro_comprobante,$data,$fecha_vencimiento_desde,$fecha_vencimiento_hasta,$array_tipo_comprobante);
		   $this->data = $data;
		   
		   
		   if($llamada_interna == 0){
			   $output = array(
			   		"status" =>EnumError::SUCCESS,
			   		"message" => "list",
			   		"content" => $this->data,
			   		"page_count" =>$page_count
			   );
			   $this->set($output);
			   $this->set("_serialize", array("status", "message","page_count", "content"));
		   
		   }else{
		   	
		   	return;
		   }
    }
    
    
    
    /**
     * @secured(CONSULTA_COMPROBANTE_IMPAGO)
     */
    public function getComprobantesImpagosXls(){
    	
    
    	
    $this->xlsReport =1;
    $this->model = "Comprobante";
    $this->ExportarExcel("get_comprobantes_impagos","getComprobantesImpagos","Comprobantes Adeudados");
    
    
    	
    	
    	
    	
    }
    
    
    
    
    protected function  getComprobantesImpagosAgrupadoPorPersonaExterno(){
    	
    	$this->getComprobantesImpagosAgrupadoPorPersona(1);//carga el $this->data
    }
    
    
    
    
    /**
     * @secured(CONSULTA_COMPROBANTE_IMPAGO)
     */
    public function  getComprobantesImpagosAgrupadoPorPersonaExternoXls(){
    	
    	$this->xlsReport =1;
    	$this->model = "Comprobante";
    	$this->ExportarExcel("get_comprobantes_impagos_totalizado_por_persona","getComprobantesImpagosAgrupadoPorPersona","Comprobantes Adeudados agrupados por persona");
    	
    	//$this->getComprobantesImpagosAgrupadoPorPersona(1);//carga el $this->data
    }
    
    
    
    
    
    
      /**
    * @secured(CONSULTA_COMPROBANTE_IMPAGO)
    */
    
    public function getComprobantesImpagosAgrupadoPorPersona($llamada_interna=0){
            
           $considerar_notas = 1; // TODO: Eliminar no se usa mas
            /*  
            *$considerar_notas  EsTE PARAMETRO DEFINE SI SE RESTAN LAS NC O SE SUMAN LAS ND AUTOMATICAMENTE
            */
           
           if($this->xlsReport == 1)
           		$llamada_interna = 1;
    
            $id_persona = $this->getFromRequestOrSession('Comprobante.id_persona');
            $id_sistema = $this->getFromRequestOrSession('Comprobante.id_sistema');
            $fecha_vencimiento_desde = $this->getFromRequestOrSession('Comprobante.fecha_vencimiento_desde');
            $fecha_vencimiento_hasta = $this->getFromRequestOrSession('Comprobante.fecha_vencimiento_hasta');
            $id_cartera_rendicion = $this->getFromRequestOrSession('Comprobante.id_cartera_rendicion');
            $id_moneda = $this->getFromRequestOrSession('Comprobante.id_moneda');
            
            /*Seguridad Usuario B2B*/
            if($this->Auth->user('id_persona')>0 && $this->Auth->user('id_rol') == EnumRol::SivitB2B){
            	
            	$id_persona = $this->Auth->user('id_persona');
            	
            }
            
            
            if($id_persona == ""){
                $id_persona = 0;
            }else{
            	
            	
            }

                
            if($id_sistema == "")
                $id_sistema = EnumSistema::VENTAS;
                
            if($id_moneda == "")
                $id_moneda = EnumMoneda::Peso; 
            
              
                

                
            if($fecha_vencimiento_desde == ""){
            	
                $fecha_vencimiento_desde = 0;  
            }else{
            	
            	array_push($this->array_filter_names,array("Fecha Contable Dsd:"=>$this->{$this->model}->formatDate($fecha_vencimiento_desde)));
            }
            
            if($fecha_vencimiento_hasta == ""){
                $fecha_vencimiento_hasta = 0;  
            }else{
            	
            	array_push($this->array_filter_names,array("Fecha Contable Hta:"=>$this->{$this->model}->formatDate($fecha_vencimiento_hasta)));
            }
    
           $this->loadModel("Comprobante"); 
           
           
           $respuesta = $this->Comprobante->getComprobantesImpagosAgrupadoPorPersona($id_persona,$considerar_notas,'',$llamada_interna,$id_sistema,$fecha_vencimiento_desde,$fecha_vencimiento_hasta,$id_cartera_rendicion,$id_moneda);
		  
           $this->data = $respuesta;
           
           if($llamada_interna == 1)
           	return;
           else{
           	
           	
           }
    }
    
   
        
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
    }
    
    
    protected function generaAsiento($id_comprobante,&$message)
	{        /*la variable mensaje se carga con un mensaje proveniente de la funcion*/
		// TODO: Tipo Comercializacion para Servicios por default son bienes de cambio

		//El asiento debe ser en moneda corriente
		
		$this->loadModel("Comprobante");
		$this->loadModel("PeriodoContable");
		$this->loadModel("EjercicioContable");
		$this->loadModel("Modulo");
		$this->loadModel("Persona");
		$this->loadModel("CentroCosto");
		$this->loadModel("Asiento");
		
		
		$habilitado  = $this->Modulo->estaHabilitado(EnumModulo::CONTABILIDAD);
		
		//Busca Comprobante por id  y trae  objetos hijos
		if($habilitado == 1) {                
			$factura = $this->Comprobante->find('first', array(
				'conditions' => array('Comprobante.id' => $id_comprobante),
				'contain' =>array('Persona','TipoComprobante'=>array('Sistema'=>
																		array('SistemaParametro')
														  ),
								  'ComprobanteImpuesto' =>array('Impuesto'=>array('CuentaContable')),
								  'CondicionPago',
								  'ComprobanteItem'=>array( 'conditions'=>array('ComprobanteItem.activo'=>1),
																"Producto"=>
																		array('CuentaContableVenta','CuentaContableCompra')
														  ),
								  'ComprobanteValor'=>array('Valor',"CuentaBancaria"=>
																		array('CuentaContable'),
															"Cheque"=>
																		array('BancoSucursal','TipoCheque','Chequera'=>array("CuentaBancaria"=>array('CuentaContable')))
														  ),'Asiento','CarteraRendicion'
								  )
			));
		
			if($this->PermiteAsiento($factura) == 1 ){
		
					if( !$factura["Asiento"]["id"]>0 ){ /*La Orden de Pago de Gasto permite el total en 0*/
					   /*La Orden de Pago de Gasto permite el total en 0*/										   
					$condicion = true;
					if($this->Comprobante->esOrdenPagoGasto($factura)!= 1){
						if($factura["Comprobante"]["total_comprobante"]<=0)
							$condicion = false;
					}
						
					if($factura && $condicion){/*El asiento solo se hace si el comprobante es mayor a 0*/
						$descuento_general = $factura["Comprobante"]["descuento"];
					
					 if(isset($factura["TipoComprobante"]["Sistema"]["SistemaParametro"])){  
						 $id_cuenta_contable_descuento_descuento_condicionado = 
								$factura["TipoComprobante"]["Sistema"]["SistemaParametro"]['id_cuenta_contable_descuento_condicionado'];//me debo fijar si tiene descuento global y si es Recibo
						// $id_cuenta_contable_compensacion = $factura["TipoComprobante"]["Sistema"]["SistemaParametro"]['id_cuenta_contable_compensacion'];
						
					 if( ($this->Comprobante->esRecibo($factura)== 1 || $this->Comprobante->esOrdenPago($factura)== 1 ) && 
							!$id_cuenta_contable_descuento_descuento_condicionado>0 && 
							($factura["Comprobante"]["descuento"]>0  || $this->ComprobanteItemsTieneDescuento($factura) ==1 ) 
					 ){
						$error = EnumError::ERROR;  
						$message = "Si el Recibo/Orden de Pago tiene un descuento condicionado aplicado o un descuento en algun Item debe definir la cuenta contable 'Cuenta Contable Descuento Condicionado' "; 
						return -1; 
					 }else{  
							 $contracuenta = 0;
							 $requiere_iva = $factura["TipoComprobante"]["requiere_iva"];
							 $id_sistema =   $factura["TipoComprobante"]["id_sistema"];
							
							 $signo_contable = $factura["TipoComprobante"]["signo_contable"]; //-1 columna debe principal 1 principal haber
							
							 $id_cuenta_contable_total = $this->getCuentaContablePrincipalAsiento($factura);
							 $id_cuenta_contable_iva = $factura["TipoComprobante"]["Sistema"]["SistemaParametro"]['id_cuenta_contable_iva'];
							
							 $cuentas_detalle = array();
							 $es_debe_cuenta_contable_detalle = ($this->Asiento->esDebe($signo_contable) - 1) *-1; //Si esDebe devuelve 1 entonces el resultado de esta cuenta devuelve 0 si devuelve 0 devuelve 1
							 $total_cuenta_contable_detalle = 0;
							/*-------------------------------------------*/
							//incluye_items_en_asiento  (ComprobanteItem) 
							/*-------------------------------------------*/
							if($factura["TipoComprobante"]["incluye_items_en_asiento"] == 1){  /*Este flag dice si los items del comprobante se usan para el asiento*/
								
									foreach($factura["ComprobanteItem"] as $item){
										
										$monto = round($item["precio_unitario"] * $item["cantidad"],2);
										
										if($descuento_general >0){ //tiene descuento general
											
										   $descuento_general_proporcion =   round($descuento_general/100,4);
										   $monto =  round(($item["precio_unitario"] * $item["cantidad"]) -  ($descuento_general_proporcion * $item["precio_unitario"] * $item["cantidad"]),2);
										 }
										
										if($factura["TipoComprobante"]["id_sistema"] == EnumSistema::VENTAS){
											 if(  isset($item["Producto"]["CuentaContableVenta"]) && count($item["Producto"]["CuentaContableVenta"])>0  /*&& $item["Producto"]["CuentaContableVenta"]["id"]>0 */){
											  
												$item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] = $item["Producto"]["CuentaContableVenta"]["id"];
												$item_asiento["AsientoCuentaContable"]["monto"] = $monto ;
												$item_asiento["AsientoCuentaContable"]["es_debe"] = $es_debe_cuenta_contable_detalle;
												array_push($cuentas_detalle,$item_asiento);
												
											 }else{
												$total_cuenta_contable_detalle += $monto;  
												}  
										 }else{
												if($factura["TipoComprobante"]["id_sistema"] == EnumSistema::COMPRAS){
												  
														 if( count($item["Producto"]["CuentaContableCompra"])>0 && isset($item["Producto"]["CuentaContableCompra"])){
														  
															$item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] = $item["Producto"]["CuentaContableCompra"]["id"];
															$item_asiento["AsientoCuentaContable"]["monto"] = $monto ;
															$item_asiento["AsientoCuentaContable"]["es_debe"] = $es_debe_cuenta_contable_detalle;
															array_push($cuentas_detalle,$item_asiento);
														}else{
															$total_cuenta_contable_detalle += $monto;  
															} 
											
												}
													if($factura["TipoComprobante"]["id_sistema"] == EnumSistema::CAJA){
														if( count($item["Producto"]["CuentaContableCompra"])>0 && isset($item["Producto"]["CuentaContableCompra"])){
														  
															$item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] = $item["Producto"]["CuentaContableCompra"]["id"];
															$item_asiento["AsientoCuentaContable"]["monto"] = $monto ;
															$item_asiento["AsientoCuentaContable"]["es_debe"] = $es_debe_cuenta_contable_detalle;
															array_push($cuentas_detalle,$item_asiento);
															
														 }else{
															$total_cuenta_contable_detalle += $monto;  
														} 
													}
											} 
									}
								}
								
							/*-------------------------------------------*/
							//N* (ComprobanteValor) 
							/*-------------------------------------------*/
							 foreach($factura["ComprobanteValor"] as $item_valor){
							  
								$monto = round($item_valor["monto"],2);
								
								$id_cuenta_contable = $this->getCuentaContableValor($item_valor,$factura["TipoComprobante"]["Sistema"]["SistemaParametro"]);
							  
								if($id_cuenta_contable>0){
									$item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] =$id_cuenta_contable;  
									$item_asiento["AsientoCuentaContable"]["monto"] = $monto ;
									$item_asiento["AsientoCuentaContable"]["es_debe"] = $es_debe_cuenta_contable_detalle;
									array_push($cuentas_detalle,$item_asiento); 
								}
								else  
									$total_cuenta_contable_detalle += $monto;    
							 }
							 
							/*-------------------------------------------*/
							//N* (ComprobanteImpuesto) 
							/*-------------------------------------------*/					 
							foreach($factura["ComprobanteImpuesto"] as $item_impuesto){
								if ( $factura["TipoComprobante"]["id_sistema"] == EnumSistema::VENTAS){
									//if($factura["Comprobante"]["id_moneda"] != EnumMoneda::Peso)
									if($this->Comprobante->esRecibo($factura)== 1)
										$monto = $item_impuesto["importe_impuesto"];
									else
										$monto = round ( ($item_impuesto["base_imponible"]*$item_impuesto["tasa_impuesto"])/100,2);
								 }else{
										 $monto = $item_impuesto["importe_impuesto"];
									 }
								 //$monto =round ( ($item_impuesto["base_imponible"]*$item_impuesto["tasa_impuesto"])/100,2);//lo recalculo por que si es en usd en la BD se recortan decimales
							  
								if($monto > 0){//lo agrego solo si es mayor que cero al impuesto. Ejemplo el Exento en IVA es 0 el monto
								  
									  if( $item_impuesto["Impuesto"]["id"] == EnumImpuesto::IVANORMAL || $item_impuesto["Impuesto"]["id"] == EnumImpuesto::IVACOMPRAS){ //el iva tiene cuenta contable definida como parametro 
										$item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] = $id_cuenta_contable_iva;
										$item_asiento["AsientoCuentaContable"]["monto"] = $monto ;
										$item_asiento["AsientoCuentaContable"]["es_debe"] = $es_debe_cuenta_contable_detalle;
										array_push($cuentas_detalle,$item_asiento); 
									  }else{            
										
										  if( count($item_impuesto["Impuesto"]["CuentaContable"])>0 && isset($item_impuesto["Impuesto"]["CuentaContable"]) && $item_impuesto["Impuesto"]["CuentaContable"]["id"]>0){
											$item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] = $item_impuesto["Impuesto"]["CuentaContable"]["id"];  
											$item_asiento["AsientoCuentaContable"]["monto"] = $monto ;
											$item_asiento["AsientoCuentaContable"]["es_debe"] = $es_debe_cuenta_contable_detalle;
											array_push($cuentas_detalle,$item_asiento); 
										  }
										  else  
										   $total_cuenta_contable_detalle += $monto;    
										 }
								}  
							 }
							 
							/*-------------------------------------------*/
							//Un Descuento global
							/*-------------------------------------------*/	
							if(($this->Comprobante->esRecibo($factura)== 1 || $this->Comprobante->esOrdenPago($factura)== 1) 
								  && $factura["Comprobante"]["descuento"]>0){ 
							  
								if( $this->Persona->PersonaImpuestoAlicuota->tieneImpuesto($factura["Comprobante"]["id_persona"],EnumImpuesto::IVANORMAL) ||
									$this->Persona->PersonaImpuestoAlicuota->tieneImpuesto($factura["Comprobante"]["id_persona"],EnumImpuesto::IVACOMPRAS)
								   )//si la persona tiene IVA entonces proceso el descuento con iva
								   {
										$total_neto_factura = round($factura["Comprobante"]["total_comprobante"]/1.21,2);//que parte del comprobante es IVA
										$total_iva_recibo =  $factura["Comprobante"]["total_comprobante"] - $total_neto_factura;
										//ahora le calculo el % del iva ese
										$total_a_imputar_iva = round($total_iva_recibo *(($factura["Comprobante"]["descuento"]/100)),2);
										
										$item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] = $id_cuenta_contable_iva;  
										$item_asiento["AsientoCuentaContable"]["monto"] = $total_a_imputar_iva ;
										$item_asiento["AsientoCuentaContable"]["es_debe"] = $es_debe_cuenta_contable_detalle;
										
										array_push($cuentas_detalle,$item_asiento); 
								
										$total_descuento =  round($factura["Comprobante"]["total_comprobante"]*($factura["Comprobante"]["descuento"]/100),2) - $total_a_imputar_iva;
								
										$item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] = $id_cuenta_contable_descuento_descuento_condicionado;  
										$item_asiento["AsientoCuentaContable"]["monto"] = $total_descuento ;
										$item_asiento["AsientoCuentaContable"]["es_debe"] = $es_debe_cuenta_contable_detalle;
										
										array_push($cuentas_detalle,$item_asiento); 
									}else{
								  
									   $total_factura = $factura["Comprobante"]["total_comprobante"];
									   $total_descuento = round($total_factura *(($factura["Comprobante"]["descuento"]/100)),2);
									  
									   $item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] = $id_cuenta_contable_descuento_descuento_condicionado;  
									   $item_asiento["AsientoCuentaContable"]["monto"] = $total_descuento ;
									   $item_asiento["AsientoCuentaContable"]["es_debe"] = $es_debe_cuenta_contable_detalle;
									   array_push($cuentas_detalle,$item_asiento); 
									}
							  } 
							  
							/*-------------------------------------------*/
							//si tiene descuento en algun item
							/*-------------------------------------------*/	
							 if(($this->Comprobante->esRecibo($factura)== 1 || $this->Comprobante->esOrdenPago($factura)== 1) 
								   && $this->ComprobanteItemsTieneDescuento($factura)==1){ 
							  
								$sumatoria_descuentos_items = $this->ComprobanteItemsSumatoriaDescuentos($factura);
								
								if( $this->Persona->PersonaImpuestoAlicuota->tieneImpuesto($factura["Comprobante"]["id_persona"],EnumImpuesto::IVANORMAL) ||
									$this->Persona->PersonaImpuestoAlicuota->tieneImpuesto($factura["Comprobante"]["id_persona"],EnumImpuesto::IVACOMPRAS)
								   )//si la persona tiene IVA entonces proceso el descuento con iva
								   {
									$total_neto_descuentos = round($sumatoria_descuentos_items/1.21,2);//que parte de los descuentos es IVA
									$total_iva_descuentos =  $sumatoria_descuentos_items - $total_neto_descuentos;
									  //ahora le calculo el % del iva ese
									$total_a_imputar_iva = round($total_iva_descuentos *(($factura["Comprobante"]["descuento"]/100)),2);
									
									$item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] = $id_cuenta_contable_iva;  
									$item_asiento["AsientoCuentaContable"]["monto"] = $total_iva_descuentos ;
									$item_asiento["AsientoCuentaContable"]["es_debe"] = $es_debe_cuenta_contable_detalle;
									array_push($cuentas_detalle,$item_asiento);
									
									$item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] = $id_cuenta_contable_descuento_descuento_condicionado;  
									$item_asiento["AsientoCuentaContable"]["monto"] = $total_neto_descuentos ;
									$item_asiento["AsientoCuentaContable"]["es_debe"] = $es_debe_cuenta_contable_detalle;
									array_push($cuentas_detalle,$item_asiento);  
								}else{
									$item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] = $id_cuenta_contable_descuento_descuento_condicionado;  
									$item_asiento["AsientoCuentaContable"]["monto"] = $sumatoria_descuentos_items ;
									$item_asiento["AsientoCuentaContable"]["es_debe"] = $es_debe_cuenta_contable_detalle;
									array_push($cuentas_detalle,$item_asiento);  
								}
							}
							
							/*-------------------------------------------*/
							// NO BALANCEADAS - Significa que por algo no balanceo lo que "sobra" lo mando a la cuenta sobrante, se utiliza cuando no estan definidas las CC de los items o del iva
							/*-------------------------------------------*/	
							if($total_cuenta_contable_detalle>0){ 
								$item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] = $factura["TipoComprobante"]["Sistema"]["SistemaParametro"]['id_cuenta_contable'];  
								$item_asiento["AsientoCuentaContable"]["monto"] = $total_cuenta_contable_detalle ;
								$item_asiento["AsientoCuentaContable"]["es_debe"] = $es_debe_cuenta_contable_detalle;
								array_push($cuentas_detalle,$item_asiento);  
							}
								$cabecera_asiento = $this->getCabeceraAsientoGeneradoPorComprobante($id_sistema,$factura);
							 
												
							/*-------------------------------------------*/
							// chequeo si salio todo bien al generar la cabecera
							/*-------------------------------------------*/	
							if($cabecera_asiento == 0){
								  $error = EnumError::ERROR;
								  $message = "Ocurri&oacute; un error, el asiento no pudo ser creado.No hay Periodo Contable Actual abierto y disponible para la fecha contable seleccionada en el comprobante".$detalle_error;
								  return -1;
							}
							$cuenta_total["AsientoCuentaContable"]["id_cuenta_contable"] = $id_cuenta_contable_total;
							$cuenta_total["AsientoCuentaContable"]["monto"] = round($this->getMontoPrincipalAsiento($factura),2);
							$cuenta_total["AsientoCuentaContable"]["es_debe"] = $this->Asiento->esDebe($signo_contable);
							 
							array_push($cuentas_detalle,$cuenta_total);  //uno los array hijos para luego insertar el asiento
							  
							$this->ConvertirAsientoAMonedaSecundaria($cabecera_asiento,"Asiento", $cuentas_detalle,$factura);
							  
							$this->loadModel("Asiento");
							$id_asiento = $this->Asiento->InsertaAsiento($cabecera_asiento,$cuentas_detalle,$detalle_error);
							  
							if($id_asiento > 0){

								  $this->CentroCosto->ApropiacionPorAsiento($id_asiento);//https://docs.google.com/document/d/1-4uDVXbyn2OKmtamNxXRACuYElK2-ZWJCbybORqdlJI/edit
								  
								  $codigo_asiento = $this->Asiento->getCodigo($id_asiento);
								  $error = EnumError::SUCCESS;
								  $message = "El asiento fue creado correctamente. Nro: ".$codigo_asiento.". ID(".$id_asiento.")";
									
								  return $id_asiento;
							}else{
							  
								$error = EnumError::ERROR;  
								$message = "Ocurrio un error, el asiento no pudo ser creado. Cheque la configuraci&oacute;n de  sistema_parametro".$detalle_error; 
								return -1;
							}
						}    
						
					}else{
					   $error = EnumError::ERROR;  
					   $message = "No esta parametrizado el modulo de Asientos. (_parametro)"; 
					   return -1; 
					}
					
					 }else{
						
						  if($this->Comprobante->esRecibo($factura)== 1 || $this->Comprobante->esOrdenPago($factura)== 1){
						   $error = EnumError::SUCCESS;
						   $message = "Si el Total del comprobante(Recibo u Orden de Pago) es igual a cero se realiza solo un ajuste, no se genera asiento.";
						   return 0;  
							
						 }else{
							 $error = EnumError::ERROR;
							  $message = "El total del comprobante es cero o ya tiene un asiento ingresado";
							  return -1;
						  }
					 }
					 }else{
						  $error = EnumError::ERROR;
						  $message = "El Comprobante ya tiene un asiento ingresado";
						  return -1;
					 }
			 }else{
				 return 0;//no requiere asiento
			} 
		
	} else{
		//$error = EnumError::ERROR;
		//$message = "No tiene habilitado el m&oacute;dulo de Contabilidad";
		return 0;
	}    
		
}


    
      
      protected function getCuentaContableValor($item_valor,$sistema_parametro = 0){
          $id_cuenta_contable = 0;
          $datoEmpresa = $this->Session->read('Empresa');
          switch($item_valor["id_valor"]){
              
              case EnumValor::EFECTIVO:

               if($datoEmpresa["DatoEmpresa"]["id_cuenta_contable_caja"] >0)
               $id_cuenta_contable  = $datoEmpresa["DatoEmpresa"]["id_cuenta_contable_caja"];
               
              break;
              
              case EnumValor::CHEQUE:
              
                    if($item_valor["Cheque"]["TipoCheque"]["id"] == EnumTipoCheque::Terceros){
                       $datoEmpresa = $this->Session->read('Empresa');
                       if($datoEmpresa["DatoEmpresa"]["id_cuenta_contable_cheque_cartera"] >0)
                        $id_cuenta_contable  = $datoEmpresa["DatoEmpresa"]["id_cuenta_contable_cheque_cartera"];  
                        
                    }else{//cheque propio
                    
                      if($item_valor["Cheque"]["Chequera"]["CuentaBancaria"]["id_cuenta_contable"]>0)
                        $id_cuenta_contable  = $item_valor["Cheque"]["Chequera"]["CuentaBancaria"]["id_cuenta_contable"]; 
                        
                        
                        
                    }
              break;
              
               case EnumValor::DEPOSITOS:

                    $id_cuenta_contable = $item_valor["CuentaBancaria"]['id_cuenta_contable'];

              break;
              
              case EnumValor::TRANSFERENCIAS:

                    $id_cuenta_contable = $item_valor["CuentaBancaria"]['id_cuenta_contable'];

              break;
              
              case EnumValor::EXTRACCION:

                    $id_cuenta_contable = $item_valor["CuentaBancaria"]['id_cuenta_contable'];
              
              break;
              
              case EnumValor::COMPENSACION:
              	
              	$id_cuenta_contable = $sistema_parametro['id_cuenta_contable_compensacion'];

              break;

              case EnumValor::TARJETA:

              	$id_cuenta_contable =  $item_valor["TarjetaCredito"]['id_cuenta_contable'];
              	
              	break;
              
              
          }
          
          
          return $id_cuenta_contable;
          
          
          
          
      }
    

    protected function actualizaComprobanteNumero($id,$nro_c){
        
        
        if($nro_c>0)
        $this->Comprobante->updateAll(array("Comprobante.nro_comprobante" => $nro_c),
                                                    array('Comprobante.id' => $id) );  
        
        return;
    } 
    
    
    
    
    
    protected function ConsultarCae($id_comprobante,&$cae,&$vencimiento_cae){
        
            $cae = 0;
            $webService   = 'wsfe';//Creando el objeto WSAA (Web Service de Autenticación y Autorización)
            $datoEmpresa = $this->Session->read('Empresa');
            $empresaCuit  = str_replace("-","",$datoEmpresa["DatoEmpresa"]["cuit"]);
            $empresaAlias  = $datoEmpresa["DatoEmpresa"]["alias"];          
            $wsaa = new AfipWsaa($webService,$empresaAlias);
            $this->Comprobante->id = $id_comprobante;
            $this->Comprobante->contain(array('TipoComprobante','PuntoVenta','ComprobanteItem' => array('Producto'=>array('Iva')),'Moneda','Persona'=> array('Pais','Provincia','TipoDocumento'),'CondicionPago','ComprobanteImpuesto'=>array('Impuesto')));
            $this->request->data = $this->Comprobante->read();
            $CbteTipo = $this->request->data["TipoComprobante"]["codigo_afip"];
            $CbteNro  =  $this->request->data["Comprobante"]["nro_comprobante"];
            $PtoVta = $this->request->data["PuntoVenta"]["numero"];

            if ($ta = $wsaa->loginCms())
            {
                $token      = $ta['token'];
                $sign       = $ta['sign'];
                $expiration = $ta['expiration'];
                $uniqueid   = $ta['uniqueid'];

                //Conectando al WebService de Factura electronica (WsFev1)
                $wsfe = new AfipWsfev1($empresaCuit,$token,$sign);
                $comprobante = $wsfe->FECompConsultar($CbteTipo,$CbteNro,$PtoVta);
                
                if($comprobante){
                    $cae = $comprobante["CodAutorizacion"];
                    $vencimiento_cae = $comprobante["FchVto"];
                    return true;
                }else
                    return false;
                
                
            }              
        
        
    }  
    
 
    
    public function ConsultarComprobanteAfip(){
    	
    	
    	App::import('Vendor', 'config', array('file' => 'fesix/config.php'));//importo el archivo de configuracion de la carpeta Vendor/fesix
    	App::import('Vendor', 'AfipWsaa', array('file' => 'fesix/afip/AfipWsaa.php'));//importo el archivo de configuracion de la carpeta Vendor/fesix
    	App::import('Vendor', 'AfipWsfev1', array('file' => 'fesix/afip/AfipWsfev1.php'));//imp 
    	
    	
    	
    	$id_tipo_comprobante   = $this->getFromRequestOrSession('Comprobante.id_tipo_comprobante');
    	$nro_comprobante_desde = $this->getFromRequestOrSession('Comprobante.nro_comprobante');
    	$nro_comprobante_hasta = $this->getFromRequestOrSession('Comprobante.nro_comprobante_hasta');
    	$id_punto_venta = $this->getFromRequestOrSession('Comprobante.id_punto_venta');

    	
    	
    	
    	
    	$this->loadModel("PuntoVenta");
    	$this->loadModel("TipoComprobante");
    	$cae = 0;
    	$webService   = 'wsfe';//Creando el objeto WSAA (Web Service de Autenticación y Autorización)
    	$datoEmpresa = $this->Session->read('Empresa');
    	$empresaCuit  = str_replace("-","",$datoEmpresa["DatoEmpresa"]["cuit"]);
    	$empresaAlias  = $datoEmpresa["DatoEmpresa"]["alias"];
    	$wsaa = new AfipWsaa($webService,$empresaAlias);
    	$this->PuntoVenta->id = $id_punto_venta;
    	$this->request->data = $this->PuntoVenta->read();
    	
    	$tipo_comprobante = $this->TipoComprobante->find('first', array(
    			'conditions' => array("TipoComprobante.id"=>$id_tipo_comprobante),
    			'contain' =>false
    	));
    	
    	
    	
    	$CbteTipo = $tipo_comprobante["TipoComprobante"]["codigo_afip"];
    	$CbteNro  =  $nro_comprobante_desde;
    	
    	$PtoVta = $this->request->data["PuntoVenta"]["numero"];
    	
 
    	
    	$array_comprobantes = array();
    	
    	
    	if ($ta = $wsaa->loginCms())
    	{
    		$token      = $ta['token'];
    		$sign       = $ta['sign'];
    		$expiration = $ta['expiration'];
    		$uniqueid   = $ta['uniqueid'];
    		
    		//Conectando al WebService de Factura electronica (WsFev1)
    		$wsfe = new AfipWsfev1($empresaCuit,$token,$sign);
    		
    		
    		/*
    		
    		$array = array(11478,11480,11483,11484,11490,11500,11501,11504,11507,11511,11512,11514,11515,11517,11526,11529,11530,11534,11540,11541,11542,11543,11548,11549,11550,11555,11559,11562,11565,11566,11569,11581,11582,11583,11583,11591,11592,11593,11594,11606,11607,11678);
    		
    		
    		foreach($array as $numero){
    			
    			$comprobante = $wsfe->FECompConsultar($CbteTipo,$numero,$PtoVta);
    			
    			if($comprobante){
    				
    				$comprobante_aux["Comprobante"]["pto_nro_comprobante"] = $numero;
    				$comprobante_aux["Comprobante"]["cae"] = $comprobante["CodAutorizacion"];
    				$comprobante_aux["Comprobante"]["vencimiento_cae"] = $comprobante["FchVto"];
    				$comprobante_aux["Comprobante"]["ComprobanteIvaTotal"] = $comprobante["ImpIVA"];
    				$comprobante_aux["Comprobante"]["subtotal_neto"] = $comprobante["ImpNeto"];
    				$comprobante_aux["Comprobante"]["ComprobanteImpuestoTotal"] = $comprobante["ImpTrib"];
    				$comprobante_aux["Comprobante"]["total_comprobante"] = $comprobante["ImpTotal"];
    				//$comprobante_aux["Comprobante"]["fecha_contable"] = $comprobante["CbteFch"];
    				$comprobante_aux["Comprobante"]["fecha_contable"] = date('Y-m-d') ;
    				$comprobante_aux["Comprobante"]["d_moneda"] = $comprobante["MonId"];
    				$comprobante_aux["Comprobante"]["valor_moneda"] = $comprobante["MonCotiz"];
    				$comprobante_aux["Comprobante"]["cuit"] = $comprobante["DocNro"];
    				$comprobante_aux["Comprobante"]["d_tipo_documento"]= $comprobante["DocTipo"];
    				
    				array_push($array_comprobantes,$comprobante_aux);
    				
    			}
    			
    			
    		}
    		
    		*/
    		
    	
    		
    		for($i=$nro_comprobante_desde;$i<=$nro_comprobante_hasta;++$i){
    			
    			
    			
    			$comprobante = $wsfe->FECompConsultar($CbteTipo,$i,$PtoVta);
    			
    			if($comprobante){
    				
    				$comprobante_aux["Comprobante"]["pto_nro_comprobante"] = $comprobante["PtoVta"]."-".$i;
    				$comprobante_aux["Comprobante"]["cae"] = $comprobante["CodAutorizacion"];
    				$comprobante_aux["Comprobante"]["vencimiento_cae"] = $comprobante["FchVto"];
    				$comprobante_aux["Comprobante"]["ComprobanteIvaTotal"] = $comprobante["ImpIVA"];
    				$comprobante_aux["Comprobante"]["total_comprobante"] = $comprobante["ImpTotal"];
    				$comprobante_aux["Comprobante"]["subtotal_neto"] = $comprobante["ImpNeto"];
    				$comprobante_aux["Comprobante"]["ComprobanteImpuestoTotal"] = $comprobante["ImpTrib"] + $comprobante["ImpIVA"];
    			
    				$fecha_comprobante = strtotime($comprobante["FchServHasta"]);
    				$CbteFch= date("Y-m-d", $fecha_comprobante);
    				
    				$comprobante_aux["Comprobante"]["fecha_contable"] = $CbteFch;
    				$fecha_vencimiento = strtotime($comprobante["FchVtoPago"]);
    				$CbteFchVto= date("Y-m-d", $fecha_vencimiento);
    				$comprobante_aux["Comprobante"]["fecha_vencimiento"] = $CbteFchVto;
    				//$comprobante_aux["Comprobante"]["fecha_contable"] = date('Y-m-d');
    				$comprobante_aux["Comprobante"]["d_moneda"] = $comprobante["MonId"];
    				$comprobante_aux["Comprobante"]["valor_moneda"] = $comprobante["MonCotiz"];
    				$comprobante_aux["Comprobante"]["cuit"] = $comprobante["DocNro"];
    				$comprobante_aux["Comprobante"]["d_tipo_documento"]= $comprobante["DocTipo"];
    				
    				array_push($array_comprobantes,$comprobante_aux);
    	
    			}
    				
    			
    		}
    
    		
    		
    		$error = EnumError::SUCCESS;
    		$messagge = "list";
    		$content = $array_comprobantes;
    			
    	}else{
    		
    		$error = EnumError::ERROR;
    		$messagge = "No es posible generar el access token. ".implode (", ", $wsaa->getErrLog());
    		$content = array();
    		
    	}
    	
    	
    	$output = array(
    			"status" => $error,
    			"message" => $messagge,
    			"content" => $content,
    			"page_count" =>0
    	);
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message","page_count", "content"));
    	
    	
    }  
  
  
  protected function PermiteAsiento($comprobante){
      
      if($this->Comprobante->getDefinitivoGrabado($comprobante["Comprobante"]["id"]) == 1 && $this->Comprobante->TipoComprobante->GeneraAsiento($comprobante["Comprobante"]["id_tipo_comprobante"])> 0  && $comprobante["Comprobante"]["comprobante_genera_asiento"]==1 && $comprobante["Comprobante"]["id_estado_comprobante"] != EnumEstadoComprobante::Anulado)
        return 1;
      else
        return 0;
      
      
  }
  
  private function getCuentaContablePrincipalAsiento($factura){
    
      
      /*Para Gastos hago un tratamiento especial ya que se toma como cuenta principal la de la cartera de rendicion*/
    if( in_array($factura["Comprobante"]["id_tipo_comprobante"], array(EnumTipoComprobante::GASTOA,EnumTipoComprobante::GASTOB,EnumTipoComprobante::GASTOC,EnumTipoComprobante::OrdenPagoRenGasto,EnumTipoComprobante::Tique,EnumTipoComprobante::TiqueFacturaA,EnumTipoComprobante::TiqueFacturaB))  ){
        
        return   $factura["CarteraRendicion"]["id_cuenta_contable"];
        
    }else{
    	
    
    	if( isset($factura["Persona"]) && $factura["Persona"]["id"]>0 && ($factura["Persona"]["id_cuenta_contable"] >0 || $factura["Persona"]["id_cuenta_contable_cuenta_corriente"] >0)) {//si la persona tiene asignada una cuenta corriente
    		
    		if($factura["Persona"]["id_cuenta_contable"] >0 && $factura["CondicionPago"]["id"] == EnumCondicionPago::CONTADO)
    			return $factura["Persona"]["id_cuenta_contable"];
    		else {
    			
    			if($factura["Persona"]["id_cuenta_contable_cuenta_corriente"]> 0)
    				return $factura["Persona"]["id_cuenta_contable_cuenta_corriente"];
    			else 
    				return   $id_cuenta_contable_total = $factura["TipoComprobante"]["Sistema"]["SistemaParametro"]['id_cuenta_contable_cuenta_corriente']; 
    		}
    		
    		
    	}else{
    	
     
    		if(isset($factura["CondicionPago"]["id"]) && $factura["CondicionPago"]["id"] == EnumCondicionPago::CONTADO){
			     	
			       return $id_cuenta_contable_total = $factura["TipoComprobante"]["Sistema"]["SistemaParametro"]['id_cuenta_contable_cuenta_contado'];   
			       
			}else{
			         
			       return   $id_cuenta_contable_total = $factura["TipoComprobante"]["Sistema"]["SistemaParametro"]['id_cuenta_contable_cuenta_corriente']; 
			         
			         
			} 
     
    	}
     
    }
      
  }
  
  
  
  protected function getMontoPrincipalAsiento($comprobante){
  	
  	
  	$this->loadModel("Comprobante");
  	
  	if($this->Comprobante->esOrdenPagoGasto($comprobante)== 1)
  		return $this->getSumaValoresComprobante($comprobante);//solo en el caso de orden de pago de Gasto devuelvo esto.
  		else{
  			
  			if($comprobante["TipoComprobante"]["id_sistema"] == EnumSistema::VENTAS){
  				//puede pasar que no este en moneda corriente entonces recalculo la presicion
  				
  				if($comprobante["Comprobante"]["id_moneda"] == EnumMoneda::Peso)
  					return $comprobante["Comprobante"]["total_comprobante"];
  					else{
  						
  						$total_comprobante  = $comprobante["Comprobante"]["subtotal_neto"] + $this->Comprobante->getSumaImpuestos($comprobante);
  						return $total_comprobante;
  					}
  					
  			}else{
  				
  				return $comprobante["Comprobante"]["total_comprobante"];
  			
  				if($comprobante["Comprobante"]["id_moneda"] == EnumMoneda::Peso)
  					return $comprobante["Comprobante"]["total_comprobante"];
  					else{
  						
  						$total_comprobante  = $comprobante["Comprobante"]["subtotal_neto"] + $this->Comprobante->getSumaImpuestos($comprobante);
  						return $total_comprobante;
  					}
  					
  					
  			}
  			
  		}
  }
  

  protected function existe_comprobante()
  {
    $this->loadModel("Comprobante"); 
    
    //los filtros que se le deben pasar son 
    /*id_persona, 
    id_tipo_comprobante,
    d_punto_venta,
    nro_comprobante */
    $conditions = $this->RecuperoFiltros($this->model);
    
    $indexCompleted = array_search('Comprobante.id', $conditions);
    
    /*borro la KEY del conditions*/
    foreach($conditions as $key =>$value){
        if( isset($value['Comprobante.id']))
            unset($conditions[$key]);
    }
    
    if($indexCompleted > 0)
        unset($conditions[$indexCompleted]);
    
    $id_comprobante = $this->getFromRequestOrSession('Comprobante.id');
    
    if($id_comprobante!="")
        array_push($conditions,array("Comprobante.id !="=>$id_comprobante));

    $existe = $this->Comprobante->find('count', array(
                                    'conditions' => $conditions,
                                    'contain' =>false
                                    ));
    $data = $existe;    
    //$count = $existe;    
    
    if($data >0){
        $error = EnumError::ERROR;
        $message = "El comprobante ya se encuentra ingresado";
    }else{
        $error = EnumError::SUCCESS;
        $message = "";
    }    
     
    $output = array(
                    "status" => $error,
                    "message" => $message,
                    "content" => $data
                );
    $this->set($output);
    $this->set("_serialize", array("status", "message", "content"));                          
  }
  
  
  
  protected function getTotalNetoAcumuladoXTipoComprobante($id_persona,$mes,$id_tipo_comprobante,$id_comprobante=0,$comprobante_not_in=0,$array_estado_comprobante,$id_tipo_comercializacion=0){
      /*Esta funcion no tiene en cuenta los exentos y No gravados  para retenciones de ganancia calculadas o sea si el id_tipo_comprobante = OP no se toma en el NETO la sumatoria de los items que tienen no gravado o exento*/
      
      
      $comprobante_aux["Comprobante"]["id_tipo_comprobante"]=$id_tipo_comprobante;
  
      $year = date('Y');
      
      $conditions = array('Comprobante.id_persona'=>$id_persona,'MONTH(Comprobante.fecha_contable)'=>$mes,'YEAR(Comprobante.fecha_contable)'=>$year,'Comprobante.id_tipo_comprobante'=>$id_tipo_comprobante,'Comprobante.definitivo'=>1);
      
      
          
      if($id_comprobante >0){
        array_push($conditions,array('Comprobante.id_tipo_comprobante'=>$id_tipo_comprobante));  
        array_push($conditions,array('Comprobante.id'=>$id_comprobante));
        
        
      }else{
      	array_push($conditions,array('Comprobante.id_estado_comprobante'=>$array_estado_comprobante));
      }
      
      if($comprobante_not_in >0){
          
          array_push($conditions,array("NOT"=>array('Comprobante.id'=>$comprobante_not_in))); 
      }
      
      if($this->Comprobante->esRecibo($comprobante_aux) !=1 && $this->Comprobante->esOrdenPago($comprobante_aux) !=1){
          
      $comprobante = $this->Comprobante->find('all', array(
                                    'fields'=>array('IFNULL(SUM(subtotal_neto),0) as total_neto_acumulado'),
                                    'conditions' => $conditions,
                                    'contain' =>false
                                    ));
      }else{  //para el caso de recibo y orden de pago debo calcular el neto de los comprobantes que estan dentro de ellos (facturas, NC, ND)
      
          $this->loadModel("ComprobanteItem");
          
          
          //$conditions = array('Comprobante.id_persona'=>$id_persona,'MONTH(Comprobante.fecha_contable)'=>$mes,'Comprobante.id_tipo_comprobante'=>$id_tipo_comprobante);
          
        
          if($id_comprobante >0){
            $conditions = array('Comprobante.id_tipo_comprobante'=>$id_tipo_comprobante);  
            array_push($conditions,array('Comprobante.id'=>$id_comprobante));
          }else{
             
          	$conditions = array('Comprobante.id_persona'=>$id_persona,'MONTH(Comprobante.fecha_contable)'=>$mes,'YEAR(Comprobante.fecha_contable)'=>$year,'Comprobante.id_tipo_comprobante'=>$id_tipo_comprobante);
            array_push($conditions,array( 'Comprobante.definitivo'=>1)); //si mando el id_comprobante necesito calcular el monto de la OP que en el edit no es definitiva por eso saco este filtro
            array_push($conditions,array('Comprobante.id_estado_comprobante'=>$array_estado_comprobante));
          }
          
          if($comprobante_not_in >0){
          
                 array_push($conditions,array("NOT"=>array('Comprobante.id'=>$comprobante_not_in))); 
          }
          
          
          
          
          if($id_tipo_comercializacion>0)/*Con esto excluyo los comprobantes que no quiero que sumen para el calculo de ganancias*/
          		array_push($conditions,array('ComprobanteOrigen.id_tipo_comercializacion'=>$id_tipo_comercializacion));
          
          		
        
         
          
          $comprobante = $this->ComprobanteItem->find('all', 
                                array(
                                
                                    'joins' => array(
                                                        array(
                                                            'table' => 'tipo_comprobante',
                                                            'alias' => 'TipoComprobante',
                                                            'type' => 'LEFT',
                                                            'conditions' => array(
                                                                'TipoComprobante.id = ComprobanteOrigen.id_tipo_comprobante'
                                                            ),
                                                        		
                                                     
                                                        		)
                                    ),//Le resto los items exentos y no gravados de la factura y las notas ya que no van en el calculo de la retencion
                                    'fields'=>array('IFNULL(



													SUM(ROUND(  (ComprobanteItem.precio_unitario / (ComprobanteOrigen.total_comprobante/ComprobanteOrigen.subtotal_neto))

															- IFNULL((

																	SELECT SUM(precio_unitario*cantidad) from comprobante_item 
																	WHERE
																	id_comprobante = ComprobanteOrigen.id and comprobante_item.id_iva IN (4,8,9) 

																	),0)    

															


														,4)* TipoComprobante.signo_comercial)



													,0) AS total_neto_acumulado'),
                                    
                                    'conditions' => $conditions,
                                    'contain' =>array('Comprobante','ComprobanteOrigen'=>array('TipoComprobante','ComprobanteItem'),'Producto'=>array('ProductoTipo'))
                       
                                    )
                                ); 
          
      }
      
      
                                      
     
      	return $comprobante[0][0]["total_neto_acumulado"];
   
      
      
      
  }
  
  
  protected function getRecibosRelacionados($id_comprobante){
  /*Busco los recibos relacionados con este comprobante (factura)*/
  
        $conditions = array();
        
        array_push($conditions,array("Comprobante.id_estado_comprobante"=>array(EnumEstadoComprobante::Abierto,EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::CerradoConCae)));
        array_push($conditions,array("Comprobante.id_tipo_comprobante"=>$this->Comprobante->getArrayRecibos())); //el comprobante contenedor debe ser recibo
        array_push($conditions,array("ComprobanteItem.id_comprobante_origen"=>$id_comprobante));
        
        
        $this->loadModel("Comprobante");
        $comprobante = $this->Comprobante->ComprobanteItem->find('all', array(
                                    //'fields'=>array('IFNULL(SUM(subtotal_neto),0) as total_neto_acumulado'),
                                    'conditions' => $conditions,
                                    'contain' =>'Comprobante'
                                    ));
                                    
        return $comprobante;                            
      
      
  }
  
  
    protected function getOrdenesPagoRelacionados($id_comprobante){
  /*Busco las OP relacionadas con este comprobante (factura COMPRA)*/
  
        $conditions = array();
        
        array_push($conditions,array("Comprobante.id_estado_comprobante"=>array(EnumEstadoComprobante::Abierto,EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::CerradoConCae)));
        array_push($conditions,array("Comprobante.id_tipo_comprobante"=>$this->Comprobante->getArrayOrdenesPago())); //el comprobante contenedor debe ser recibo
        array_push($conditions,array("ComprobanteItem.id_comprobante_origen"=>$id_comprobante));
        
        
        $this->loadModel("Comprobante");
        $comprobante = $this->Comprobante->ComprobanteItem->find('all', array(
                                    //'fields'=>array('IFNULL(SUM(subtotal_neto),0) as total_neto_acumulado'),
                                    'conditions' => $conditions,
                                    'contain' =>'Comprobante'
                                    ));
                                    
        return $comprobante;                            
      
      
  }
  
  
  
  
  protected function getOrdenesPagoEnGastoRelacionados($id_comprobante)
  {
        $conditions = array();
        
        array_push($conditions,array("Comprobante.id_estado_comprobante"=>array(EnumEstadoComprobante::Abierto,EnumEstadoComprobante::CerradoConCae,EnumEstadoComprobante::CerradoConCae)));
        array_push($conditions,array("Comprobante.id_tipo_comprobante"=>$this->Comprobante->getArrayOrdenesPagoRendicionGasto())); //el comprobante contenedor debe ser recibo
        array_push($conditions,array("ComprobanteItem.id_comprobante_origen"=>$id_comprobante));
        
        
        $this->loadModel("Comprobante");
        $comprobante = $this->Comprobante->ComprobanteItem->find('all', array(
                                    //'fields'=>array('IFNULL(SUM(subtotal_neto),0) as total_neto_acumulado'),
                                    'conditions' => $conditions,
                                    'contain' =>'Comprobante'
                                    ));               
        return $comprobante;                            
  }
  
  
  /**
  * @secured(CONSULTA_COMPROBANTE_RELACIONADOS)
  * 
  */
  public function getComprobantesRelacionadosAll($id_comprobante,$id_tipo_comprobante=0,$generados=1){
      /* Devuelve los items relacionados por items o por comprobante */
      
      
      $comprobantes_relaciondos = $this->getComprobantesRelacionados($id_comprobante,$id_tipo_comprobante,$generados);
      
      
      foreach($comprobantes_relaciondos  as &$comprobante){
          
          $comprobante["Comprobante"]["d_tipo_comprobante"] = $comprobante["TipoComprobante"]["d_tipo_comprobante"];
          
           if(isset($comprobante['TipoComprobante'])){
            $comprobante['Comprobante']['d_tipo_comprobante'] =   $comprobante['TipoComprobante']['d_tipo_comprobante'];
            
            $comprobante['Comprobante']['codigo_tipo_comprobante'] = $comprobante['TipoComprobante']['codigo_tipo_comprobante'];
            unset($comprobante['TipoComprobante']);
        }

        if(isset($comprobante['PuntoVenta'])){
            $comprobante['Comprobante']['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($comprobante['PuntoVenta']['numero'],$comprobante['Comprobante']['nro_comprobante']);
            unset($comprobante['PuntoVenta']);
        }else{
           $comprobante['Comprobante']['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($comprobante['Comprobante']['d_punto_venta'],$comprobante['Comprobante']['nro_comprobante']);
            
        } 
          
      }
      
      $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "list",
                    "content" => $comprobantes_relaciondos,
                    "page_count" =>0
                );
    $this->set($output);
    $this->set("_serialize", array("status", "message","page_count", "content"));
  }
  
  function ConvertirAsientoAMonedaSecundaria(&$cabecera,$model, &$items_detalle_asiento,$comprobante=null){
    
    
    // if($comprobante["Comprobante"]["id_moneda"] != EnumMoneda::Peso){ //debo convertir a pesos
    
  	$datoEmpresa  = $this->Session->read('Empresa');
  	$id_moneda_corriente  = $datoEmpresa["DatoEmpresa"]["id_moneda2"];
  	$id_moneda_base  = $datoEmpresa["DatoEmpresa"]["id_moneda"];
  	$id_moneda_secundaria  = $datoEmpresa["DatoEmpresa"]["id_moneda3"];
  	
  	
  	
    
  	if($cabecera["Asiento"]["id_moneda"] != $id_moneda_corriente){//si el asiento no esta en moneda corriente entonces lo expreso en moneda corriente
  		
  		$cabecera["Asiento"]["id_moneda"] = $id_moneda_corriente;
  		
  		
  		
  		foreach($items_detalle_asiento as &$item_asiento){
  			
  			
  		
  			$item_asiento["AsientoCuentaContable"]["monto"] = round($item_asiento["AsientoCuentaContable"]["monto"]/$comprobante["Comprobante"]["valor_moneda2"],4);
  			
  		} 
  		
  	}
  	
  	if($comprobante!=null){/*si el comprobante me manda alguna moneda enlazada entonces se la asigno al asiento*/
  		
  		
  		
  		if($comprobante["Comprobante"]["id_moneda"] == $id_moneda_corriente){
  			
  		if(isset($comprobante["Comprobante"]["valor_moneda"])){
  			
  			$cabecera["Asiento"]["valor_moneda"] = $comprobante["Comprobante"]["valor_moneda"];
  		}
  		
  		
  		
  		if(isset($comprobante["Comprobante"]["valor_moneda2"])){
  			
  			$cabecera["Asiento"]["valor_moneda2"] = $comprobante["Comprobante"]["valor_moneda2"];
  		}
  		
  		
  		if(isset($comprobante["Comprobante"]["valor_moneda3"])){
  			
  			$cabecera["Asiento"]["valor_moneda3"] = $comprobante["Comprobante"]["valor_moneda3"];
  		}
  		
  		}else{
  			
  			
  			if($comprobante["Comprobante"]["id_moneda"] == $id_moneda_base){
  				$cabecera["Asiento"]["valor_moneda"] = round(1/$comprobante["Comprobante"]["valor_moneda2"],4);
  				$cabecera["Asiento"]["valor_moneda2"] = 1.000;
  				
  				if(isset($comprobante["Comprobante"]["valor_moneda3"]))
  					$cabecera["Asiento"]["valor_moneda3"] = $comprobante["Comprobante"]["valor_moneda3"];
  				
  			}
  			
  			if($comprobante["Comprobante"]["id_moneda"] == $id_moneda_secundaria){
  				$cabecera["Asiento"]["valor_moneda"] = round(1/$comprobante["Comprobante"]["valor_moneda3"],4);
  				$cabecera["Asiento"]["valor_moneda3"] = 1.000;
  				if(isset($comprobante["Comprobante"]["valor_moneda2"]))
  					$cabecera["Asiento"]["valor_moneda3"] = $comprobante["Comprobante"]["valor_moneda3"];
  			}
  			
  			
  			
  		}
  		
  		
  	}
  	
  	
  
      
  	$this->Comprobante->CalcularMonedaBaseSecundaria($cabecera,$model,1);//le paso un 1 para decirle que la cotizacion la tome del formulario y no de la DB
        
       /*
       
        foreach($items_detalle_asiento as &$item_asiento){
            
            
            $item_asiento["AsientoCuentaContable"]["monto"] = round($item_asiento["AsientoCuentaContable"]["monto"]/$comprobante["Comprobante"]["valor_moneda2"],4); 
        } 
        */ 
         
     //} 
      
}


function ComprobanteItemsTieneDescuento($comprobante){
    
    $tiene_descuento = 0;
    if( isset($comprobante["ComprobanteItem"]) ){//si tiene items
        
        
        foreach($comprobante["ComprobanteItem"] as $item){
            
            if($item["importe_descuento_unitario"]>0 || $item["descuento_unitario"]> 0 )
                $tiene_descuento = 1;
            
        }
        
       
        
    }
    
     return $tiene_descuento;
    
    
}


function ComprobanteItemsSumatoriaDescuentos($comprobante){
    
    $sumatoria_descuentos = 0;
    if( isset($comprobante["ComprobanteItem"]) ){//si tiene items
	
        foreach($comprobante["ComprobanteItem"] as $item){
           $sumatoria_descuentos +=$item["importe_descuento_unitario"];   
        }   
    }
     return $sumatoria_descuentos;
}


public function beforeFilter(){
        
         
     $this->loadModel("Comprobante");
     $this->id_tipo_comprobante = $this->Comprobante->getTiposComprobanteController($this->name);
     parent::beforeFilter();
}




protected function deleteItemsNoEnviados($id_comprobante){
    
    $this->loadModel($this->model);
    $items_grabados = $this->Comprobante->getItems($id_comprobante,$activo = 1);
    
    
    /*Esta funcion busca si desde el FRONT se piso un id comprobante_item con otro id_producto y lo borra*/
    
    if($items_grabados!=0){
        
        foreach($items_grabados as $item_grabado){
            
            $borrar = false;
            
            if(isset($this->request->data["ComprobanteItem"])){
                foreach($this->request->data["ComprobanteItem"]  as $key=> &$item_enviado){
                    
                    if($item_grabado["ComprobanteItem"]["id_producto"] != $item_enviado["id_producto"]  && isset($item_grabado["ComprobanteItem"]["id"]) && isset($item_enviado["id"]) && $item_grabado["ComprobanteItem"]["id"] == $item_enviado["id"] ){
                        $borrar = true;
                        $id_item = $item_grabado["ComprobanteItem"]["id"];
                        unset($this->request->data["ComprobanteItem"][$key]["id"]);//si esta enviando un nuevo item con un id ya grabado entonces le saco ese id
                        $this->deleteItem($id_item,0);
                    }    
                }
            }
            
            
                
            
            
            
            
        }
        
        
    }
    
    
    
    
        
    
}

 
  protected function CalcularBaseImponibleImpuestoOnTheFly($base_imponible){
            
            $this->loadModel("Comprobante");
            
            
            if(isset($this->request->data["ComprobanteImpuesto"])){
                
                
                foreach($this->request->data["ComprobanteImpuesto"] as &$impuesto){
                    
                   
                    
                  if($impuesto["id_impuesto"]!=EnumImpuesto::IVACOMPRAS ){
                      
                      
                      if(isset($impuesto["importe_impuesto"]) && $impuesto["importe_impuesto"]>=0){
                      if(!isset($impuesto["base_imponible"]))
                        $impuesto["base_imponible"] = 0;
                        
                      
                        //if($impuesto["base_imponible"] == 0)
                        $impuesto["base_imponible"] = $base_imponible;
                        $impuesto["tasa_impuesto"]  = round(($impuesto["importe_impuesto"]*100)/$this->request->data["Comprobante"]["subtotal_neto"],2);
                    
                  }    
                  }  
                    
                }
                
                
            }
            
            
        }   
        
        
        public function ConvertirPreciosUnitariosAPositivo(&$items){
        	
        	/*Los Recibos y las Ordenes de Pago tienen los items positivos*/
        	
        	if(isset($items["ComprobanteItemComprobante"]) && $items["ComprobanteItemComprobante"]){
        		
        		
        		foreach($items["ComprobanteItemComprobante"] as &$item){
        			
        			$item["precio_unitario"] = abs($item["precio_unitario"]);
        		}
        	}
        	
        	
        	
        	
        }
        
        
        
        protected function borrraComprobanteImpuestoConBaseCero(&$error,&$message){
        	
        	
        	$this->loadModel("ComprobanteImpuesto");
        	
        	
        	try{
        		if(isset($this->request->data['ComprobanteImpuesto'])){
        			
        			foreach($this->request->data['ComprobanteImpuesto'] as $key=>&$i_item){
        				
        				if($i_item['importe_impuesto'] == 0 &&  $this->request->data['Comprobante']['definitivo'] == 1){
        					
        					
        					
        					if( isset($i_item['id']) && $i_item['id']>0){
        						
        						
        						
        						$this->ComprobanteImpuesto->id = $i_item['id'];
        						$this->ComprobanteImpuesto->delete();
        						
        					}
        					
        					unset($this->request->data['ComprobanteImpuesto'][$key]);
        				}
        				
        			}
        			
        			
        			
        			$array_aux = array();
        			
        			
        			foreach($this->request->data['ComprobanteImpuesto'] as $dato){
        				
        				array_push($array_aux,$dato);
        				
        			}
        			
        			$this->request->data['ComprobanteImpuesto'] = $array_aux;
        			
        		}
        		
        		
        		
        		
        		
        	}catch(Exception $e){
        		
        		$error = 1;
        		$message = $e->getMessage();
        		
        	}
        	
        	
        	return 0;
        	
        }
        
        
        
        protected function calcularTasaImpuestoOnTheFly(){
        	
        	
        	$this->loadModel("ComprobanteImpuesto");
        	
        	
        	
        		if(isset($this->request->data['ComprobanteImpuesto'])){
        			
        			foreach($this->request->data['ComprobanteImpuesto'] as $key=>&$i_item){
        				
        				
        				if(isset($i_item["base_imponible"]) && $i_item["base_imponible"]>0 && $i_item["importe_impuesto"]>0)
        					$i_item["tasa_impuesto"] = round(($i_item["importe_impuesto"]*100)/$i_item["base_imponible"],2);
        				
        				
        			}
        			
        			
        			
        		
        			
        		}
        		
        		
        		
        		
        		
       
        	
        }
        
        
     
        protected function borrarValoresMontoCero(&$error,&$message){
        	
        	
        	$this->loadModel("ComprobanteValor");
        	
        	
        	try{
        		if(isset($this->request->data['ComprobanteValor'])){
        			
        			foreach($this->request->data['ComprobanteValor'] as $key=>&$i_item){
        				
        				if($i_item['monto'] == 0 &&  $this->request->data['Comprobante']['definitivo'] == 1){
        					
        					
        					
        					if( isset($i_item['id']) && $i_item['id']>0){
        						
        						
        						
        						$this->ComprobanteValor->id = $i_item['id'];
        						$this->ComprobanteValor->delete();
        						
        					}
        					
        					unset($this->request->data['ComprobanteValor'][$key]);
        				}
        				
        			}
        			
        			
        			
        			$array_aux = array();
        			
        			
        			foreach($this->request->data['ComprobanteValor'] as $dato){
        				
        				array_push($array_aux,$dato);
        				
        			}
        			
        			$this->request->data['ComprobanteValor'] = $array_aux;
        			
        		}
        		
        		
        		
        		
        		
        	}catch(Exception $e){
        		
        		$error = 1;
        		$message .= $e->getMessage();
        		
        	}
        	
        	
        	return 0;
        	
        }
        
        
        protected function actualizaFechaContable($id_comprobante,$fecha_contable = null,&$error = "",&$mensaje){
        	
        	
        try{ 	
        if(isset($this->request->data["Comprobante"]["fecha_contable"])){	
        	
	        $this->Comprobante->updateAll(
	        			array('Comprobante.fecha_contable' => "'".$this->request->data["Comprobante"]["fecha_contable"]."'"),
	        			array('Comprobante.id' => $id_comprobante) );
	        
	        $error = EnumError::SUCCESS;
	        $mensaje = "Ha sido actualizada correctamente la fecha contable";
        }	
        	
        
 
        }catch(Exception $e){
        	
        	$error = EnumError::ERROR;
        	$mensaje = "Ha ocurrido un error, NO ha podido ser actualizada correctamente la fecha contable".$e->getMessage();
        }
  
        
  
        
        if($error == "error")
        	return 0;
        else
        	return 1;
        
        	
        	
  
        }
        
        
        
        protected function actualizaFechaVencimiento($id_comprobante,$fecha_vencimiento = null,&$error = "",&$mensaje){
        	
        	
        	try{
        		if(isset($this->request->data["Comprobante"]["fecha_contable"])){
        			
        			$this->Comprobante->updateAll(
        					array('Comprobante.fecha_vencimiento' => "'".$this->request->data["Comprobante"]["fecha_vencimiento"]."'"),
        					array('Comprobante.id' => $id_comprobante) );
        			
        			$error = EnumError::SUCCESS;
        			$mensaje = "Ha sido actualizada correctamente la fecha de Vencimiento";
        		}
        		
        		
        		
        	}catch(Exception $e){
        		
        		$error = EnumError::ERROR;
        		$mensaje = "Ha ocurrido un error, NO ha podido ser actualizada correctamente la fecha de vencimiento".$e->getMessage();
        	}
        	
        	
        	
        	
        	if($error == "error")
        		return 0;
        		else
        			return 1;
        			
        			
        			
        			
        }
        
        
        
        protected function getTipoRetencion($id_impuesto){
        	
        	
        	
        	
        	
        	$this->loadModel("TipoComprobante");
        	$this->loadModel("Impuesto");
        	
        	
        	$impuesto = $this->Impuesto->find('first', array(
        			'conditions' => array('Impuesto.id' => $id_impuesto),
        			'contain' =>false
        	));
        	
        	
        	$id_tipo_impuesto = $impuesto["Impuesto"]["id_tipo_impuesto"];
        	$id_sub_tipo_impuesto= $impuesto["Impuesto"]["id_sub_tipo_impuesto"];
        	
        	
        	
        	if($id_tipo_impuesto == EnumTipoImpuesto::RETENCIONES && $id_sub_tipo_impuesto == EnumSubTipoImpuesto::RETENCION_GANANCIA)
        		
        		return EnumTipoComprobante::RetencionGANANCIA;//hay muchos impuestos de retencion
        		
        		elseif ($id_tipo_impuesto == EnumTipoImpuesto::RETENCIONES  && $id_sub_tipo_impuesto == EnumSubTipoImpuesto::RETENCION_IIBB)
        		
        		return  EnumTipoComprobante::RetencionIIBB;
        		
        		elseif ($id_tipo_impuesto == EnumTipoImpuesto::RETENCIONES && $id_sub_tipo_impuesto == EnumSubTipoImpuesto::RETENCION_IVA)
        		
        		return  EnumTipoComprobante::RetencionIVA;
        		
        		elseif ($id_tipo_impuesto == EnumTipoImpuesto::RETENCIONES && $id_sub_tipo_impuesto == EnumSubTipoImpuesto::RETENCION_SUSS)
        		
        		return  EnumTipoComprobante::RetencionSUSS;
        		
        		
        }
        
        protected function getSumaValoresComprobante($factura){
        	
        	
        	$monto = 0;
        	if(isset($factura["ComprobanteValor"]) && count($factura["ComprobanteValor"])>0){
	        	foreach($factura["ComprobanteValor"] as $item_valor){
	        		
	        		$monto = $monto + $item_valor["monto"];
	        		
	        	}
        	}
        	return $monto;
        }
        
        
        
        protected function chequeoStock($data,&$array_productos_no_cumplen_stock){
       	
       	
       	$this->loadModel("ComprobanteItem");
       	$this->loadModel("TipoComprobante");
       	$this->loadModel("StockProducto");
       	
       	$habilitado  = $this->Modulo->estaHabilitado(EnumModulo::STOCK);
       	$id_tipo_comprobante = $data["Comprobante"]["id_tipo_comprobante"];
       	$id_tipo_comportamiento_stock = $this->TipoComprobante->getTipoComportamientoStock($id_tipo_comprobante);
       	$array_productos_no_cumplen_stock = array();
       	
       	
       	
       	
       	if(isset($data["ComprobanteItem"])  && isset($data["Comprobante"]["id_deposito_origen"]) && $data["Comprobante"]["id_deposito_origen"]>0 && $id_tipo_comportamiento_stock>0){
       		
       		$array_id_productos = $this->ComprobanteItem->getIdArticulosFromComprobante($data);
       		
       		
       		//busco el stock de estos productos para el deposito donde se quieren sacar.
       		$stock_productos = $this->StockProducto->find('all', array(
       				'conditions' => array('StockProducto.id' => $array_id_productos,'StockProducto.id_deposito'=>$data["Comprobante"]["id_deposito_origen"]),
       				'contain' =>array("Producto")
       		));
       		
       		
       		
       		
       		foreach($data["ComprobanteItem"] as $item){//por cada item cheque si tiene stock para sacar
       			

       			
       			foreach($stock_productos as $stock_producto){
       				
       				if($stock_producto["StockProducto"]["id"] == $item["id_producto"]){//si el producto es el mismo chequeo
       					
       					if(( $stock_producto["StockProducto"]["stock"] - $item["cantidad"])<0) //si lo que quiero sacar no cumple lo guardo en el array
       						array_push($array_productos_no_cumplen_stock, "Art&iacute;culo:".$stock_producto["Producto"]["codigo"]." Stock:".$stock_producto["StockProducto"]["stock"]);
									
       				}
       				
       			}
       			
       			
       			
       		}
       		
       		
       		
       		
       		
       		
       		
       	}
       	
       	
       	return $id_tipo_comportamiento_stock;
       	
       	
       	
       }
       
       
       protected function RealizarMovimientoStock($id_comprobante,&$message){
       	
       	
       	$array_productos_no_cumplen_stock = array();
       	
       	if( $this->AceptaStock() == 1){
       		
       		$this->loadModel("Comprobante");
       		
       		$separador = " \n&bull;";
       		$chequeo_stock = $this->chequeoStock($this->request->data,$array_productos_no_cumplen_stock);
       		
       		$id_tipo_movimiento_defecto = $this->Comprobante->getTipoMovimientoDefecto($id_comprobante);
       		
       		if($id_tipo_movimiento_defecto == 0)
       			throw new Exception( "El comprobante no tiene definido el tipo de movimiento por defecto");
       		
       		
       		if($chequeo_stock!=2){
       			
       			$this->genera_movimiento_stock($id_comprobante,$id_tipo_movimiento_defecto,0,$this->request->data);//si esta habilitado descuento stock generando movimiento
       			
       			if($chequeo_stock == 1)
       				$message .= " Estos Art&iculos no cumplen con el stock suficiente pero el mov se hizo igual: ".implode($separador,$array_productos_no_cumplen_stock);
       				
       		}else{//hay un error con alguno de los articulos a enviar, no puede cerrar el comprobante
       			
       			
       			if($this->request->data["Comprobante"]["definitivo"] == 1)
       				throw new Exception( "Estos Art&iacute;culo/s no cumplen con el stock suficiente:".implode($separador,$array_productos_no_cumplen_stock));
       		}
       	}
       }
       
       
       protected function PeriodoValidoParaAnular($comprobante){
       	
       	
       $this->loadModel("Modulo");
       	
       	
       	$modulo_contabilidad  = $this->Modulo->estaHabilitado(EnumModulo::CONTABILIDAD);
   
       	$periodo_valido = 1;
       	
       	if($modulo_contabilidad == 1){
       		$this->loadModel("EjercicioContable");
       		$this->loadModel("PeriodoContable");
       		
       		
       		$id_ejercicio_contable = $this->EjercicioContable->GetEjercicioPorFecha($comprobante["Asiento"]["fecha"],0,1);
       		$periodo_seleccionado = $this->PeriodoContable->GetPeriodoPorFecha($comprobante["Asiento"]["fecha"],0,$id_ejercicio_contable["EjercicioContable"]["id"]);//busco el periodo en base a la fecha contable
       		
       		
       		if($periodo_seleccionado){
       			
       			if($periodo_seleccionado["cerrado"] == 1 || $periodo_seleccionado["disponible"] == 0)
       				$periodo_valido = 0;
       		}
       		
       		
       	}
       	
       	return  $periodo_valido;
       	
       }
       
       
       protected function guardaCodigoProducto(){
       	
       	if(isset($this->request->data['ComprobanteItem'])){

       		foreach($this->request->data['ComprobanteItem'] as &$c_item){
     
       			$c_item['producto_codigo'] = $c_item['codigo'];
       			
       		}
     
       	}
       	
       	
       	
       	
       }
       
       public function getTipoMovimientoDefecto($id_comprobante){
       	
       }
       
       
       protected function avisoEmail($id_comprobante=0,&$mensaje_error){//guardo en una tabla de envio de mail para que el proceso que la envia sepa que hay que enviar un mail.
       	
       try{
       	$this->loadModel(EnumModel::TipoComprobante);
       	$this->loadModel(EnumModel::MailToSend);
       	
       	

       	
       	if(isset($this->request->data["Comprobante"]["chk_envia_mail"]))
       		$comprobante_evia_mail = $this->request->data["Comprobante"]["chk_envia_mail"];
       	else
       		$comprobante_evia_mail = 0;
       	
       		
       		
       	$comprobante_row = $this->Comprobante->find('first', array(
       			'conditions' => array(
       					'Comprobante.id' => $id_comprobante
       			) ,
       			'contain' => array('ComprobanteImpuesto')
       	));
       
       	
       	
       	$valido = 1;
       	$facturas_nota_venta  = array_merge($this->Comprobante->getTiposComprobanteController(EnumController::Facturas),$this->Comprobante->getTiposComprobanteController(EnumController::Notas));
       	
       	
       	
       	if(in_array($comprobante_row["Comprobante"]["id_tipo_comprobante"],$facturas_nota_venta)){
       		$valido = 0;//si es factura o nota de venta tiene que ser cerrado con CAE para poder enviarse
       		
       	if($comprobante_row["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoConCae)
       			$valido = 1;
       	}else{
       		
       		if($comprobante_row["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion)
       			$valido = 1;
       		else 
       			$valido = 0;
       		
       	}
       	
       
       	

       		
       		
   
       	
       	
       	
       	
       	
       	if(	$valido == 1 && $this->{EnumModel::TipoComprobante}->getEnviarMailAlConformar($comprobante_row["Comprobante"]["id_tipo_comprobante"])>=1
       			&& $comprobante_evia_mail == 1
       	   )
       	{
       		
       		

       		
       		//$this->pdfExport($id_comprobante);
       		
       		
       	    
       		//realizo la llamada al controller y genero PDF
       		$this->requestAction(
       				array('controller' => $this->name.'/pdfExport/'.$id_comprobante)
       				);
       		
       		
       		
       		if(	$comprobante_row["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::OrdenPagoManual ||
       				
       				$comprobante_row["Comprobante"]["id_tipo_comprobante"]== EnumTipoComprobante::OrdenPagoAutomatica){
       					
       					foreach($comprobante_row["ComprobanteImpuesto"] as $impuesto){
       						
       					
       						$this->requestAction(
       								array('controller' => EnumController::Retenciones.'/pdfExport/'.$impuesto["id"])
       								);
       						
       					}
       		}
       		
       		
    		
       	   
       	   	
       	   	$data["MailToSend"]["id_comprobante"] = $comprobante_row["Comprobante"]["id"];
       	   	$data["MailToSend"]["intento_fallidos"] = 0;
       	   	$data["MailToSend"]["enviado"] = 0;
       	   	

       	   	
       	   	$this->{EnumModel::MailToSend}->SaveAll($data);
       	
    		return true;
		
       		
       	}
       	
       }catch (Exception $e){
       	
       	
       	$mensaje_error = "No fue posible enviar el correo";
       	return false;
       	
       	
       }
       	
       }
       
       
       public function getCotizacionConMonedaCorriente($valor){
       	
       	$datoEmpresa = $this->Session->read('Empresa');
       	$cotizacion = 1;
       	if($valor['Comprobante']['id_moneda_enlazada'] == $datoEmpresa["DatoEmpresa"]["id_moneda"])
       		$cotizacion = $valor['Comprobante']['valor_moneda'];
       		elseif ($valor['Comprobante']['id_moneda_enlazada'] == $datoEmpresa["DatoEmpresa"]["id_moneda2"])
       		$cotizacion = 1/$valor['Comprobante']['valor_moneda2'];
       		elseif ($valor['Comprobante']['id_moneda_enlazada'] == $datoEmpresa["DatoEmpresa"]["id_moneda3"])
       		$cotizacion = 1/$valor['Comprobante']['valor_moneda3'];
       		
       		
       	return $cotizacion;
       	
       	
       	
       }
       
       
       
       protected function regenerarAsientos(){
       	
       	//5948
       	
       	$this->loadModel("Comprobante");
       	
      
       	$this->Comprobante->query("DELETE 
FROM
  asiento_cuenta_contable 
WHERE id_asiento IN 
  (SELECT 
    asiento.id 
  FROM
    asiento 
    JOIN comprobante 
      ON comprobante.id = asiento.id_comprobante 
  WHERE id_comprobante = 21915 
    AND manual <> 1 
    AND id_estado_comprobante NOT IN (1, 2, 3)   AND asiento.id_comprobante IS NOT NULL  ) ");
       	
       	$this->Comprobante->query("delete from asiento where id_comprobante=21915 and manual<>1");
    
       	
       	
       	$comprobantes_a_regenerar = $this->Comprobante->query("SELECT id FROM comprobante WHERE id=21915 AND comprobante_genera_asiento=1 and id_estado_comprobante NOT IN(1,2,3)" );
       	
       	
       	foreach($comprobantes_a_regenerar as $comp){
       		$message == "";
       		
       		try{
       			
       			$this->requestAction(
       					array('controller' => 'Comprobantes', 'action' => 'generaAsiento/'.$comp["comprobante"]["id"]),
       					array('data' => $data)
       					);
       			
       			//$this->generaAsiento($comp["comprobante"]["id"], $message);
       			
       			
       			
       		}catch(Exception $e){
       			
       			$e;
       			
       		}
       		
       		
       	}
       	
       	
       	echo "listo";
       	die();
       	
       	
       	
       }
       
       
       /*
       protected function getDataCuitFromAfip($cuit="",$llamada_interna=0){
       	
       	
       	
       	App::import('Vendor', 'config', array('file' => 'fesix/config.php'));//importo el archivo de configuracion de la carpeta Vendor/fesix
       	App::import('Vendor', 'AfipWsaa', array('file' => 'fesix/afip/AfipWsaa.php'));//importo el archivo de configuracion de la carpeta Vendor/fesix
       	App::import('Vendor', 'AfipWsr', array('file' => 'fesix/afip/AfipWsr.php'));//imp 
       	
       	
       	$webService   = 'ws_sr_padron_a5';//Creando el objeto WSAA (Web Service de Autenticación y Autorización)
       	
       	
       	if($cuit !="")
       		$cuit = $this->getFromRequestOrSession('Persona.cuit');
       	
      // 	$cuit = "20335245637";
       	
       	$wsaa = new AfipWsaa($webService,"certitest");
       	
       	
       	$data = array();
       	
       	
       	if($cuit!=""){
       	
       	try{
       		if ($ta = $wsaa->loginCms())
       		{
       			
       			
       			
       
       			
       			$datoEmpresa = $this->Session->read('Empresa');
       			$empresaCuit = $datoEmpresa["DatoEmpresa"]["cuit"];
       			
       			
       			$token      = $ta['token'];
       			$sign       = $ta['sign'];
       			$expiration = $ta['expiration'];
       			$uniqueid   = $ta['uniqueid'];
       			
       			//Conectando al WebService de Factura electronica (WsFev1)
       			$wsr = new AfipWsr($empresaCuit,$token,$sign);
       			
       			$results = 	$wsr->getPersona($cuit);
       			
       			
       			if($results->error_interno == EnumError::SUCCESS){
       				
       				
       				$error = EnumError::SUCCESS;
	       			$this->loadModel(EnumModel::Provincia);
	       			$this->loadModel(EnumModel::PersonaFigura);
	       			
	       			
	       			$datos_generales = $results->personaReturn->datosGenerales;
	       			$provincia =  $this->Provincia->getProvinciaPorCodigoAfip($datos_generales->domicilioFiscal->idProvincia);
	       			
	       			if($provincia)
	       				$id_provincia = $provincia["Provincia"]["id"];
	       			
	       				
	       			$id_persona_figura = $this->PersonaFigura->getPersonaFiguraPorCodigoAfip( $tipo_persona = $datos_generales->tipoPersona);	//FISICA, JURIDICA
	       			$codigo_postal = $datos_generales->domicilioFiscal->codPostal;
	       			$direccion = $datos_generales->domicilioFiscal->direccion;
	       			
	       			$localidad = $datos_generales->domicilioFiscal->localidad;
	       			$razon_social = $datos_generales->razonSocial;
	       			//$tipo_clave = $datos_generales->tipoClave;
	       			
	       			$data["Persona"]["id_provincia"] = $id_provincia;
	       			$data["Persona"]["id_tipo_persona"] = $id_tipo_persona;
	       			$data["Persona"]["cod_postal"] = $codigo_postal;
	       			$data["Persona"]["id_persona_figura"] = $id_persona_figura;
	       			$data["Persona"]["calle"] = $direccion;
	       			$data["Persona"]["localidad"] = $localidad;
	       			$data["Persona"]["razon_socail"] = $razon_social;
	       			
	       			$datos_extra = $results->personaReturn->datosRegimenGeneral;//datos copados
       			
       			}else{
       				
       				$error = EnumError::ERROR;
       				$messagge = $results->message_interno;
       			}
       			
       			
       }
       	}catch(Exception $a){
       		
       		$error = EnumError::ERROR;
       		$message = $a->getMessage();
       		
       	}
       	
       	
       }else{
       	
       	$error = EnumError::ERROR;
       	$messagge = "Debe especificar un documento para poder consultar el servicio";
       	
       	
       	
       }
       	
       	$output = array(
       			"status" => $error,
       			"message" => $messagge,
       			"content" => $data,
       			"page_count" =>0,
       			"message_type"=>EnumMessageType::MessageBox
       	);
       	
       	if($llamada_interna == 0){
       		$this->set($output);
       		$this->set("_serialize", array("status", "message","page_count", "content"));
       	}else{
       		
       		
       		return $output;
       	}
    
       	
       
        
        
       }*/
	    
	    
	    public function getTiposComprobanteFromController(){ //TODO: Pasar al front.
			
			$this->loadModel("Comprobante");
			$id_tipo_comprobante = $this->id_tipo_comprobante;
			
			$signo_comercial = $this->getFromRequestOrSession('TipoComprobante.signo_comercial');
			$excluir_invalidos = $this->getFromRequestOrSession('TipoComprobante.excluir_invalidos');
			$excluir_invalidos = 1;
			
			$datoEmpresa  = $this->Session->read('Empresa');
			$id_tipo_iva  = $datoEmpresa["DatoEmpresa"]["id_tipo_iva"];
			
			$conditions = array();
			
			
			if($signo_comercial != "")
				array_push($conditions,array("TipoComprobante.signo_comercial"=>$signo_comercial));
				
				array_push($conditions,array("TipoComprobante.id"=>$id_tipo_comprobante));
				
				if($excluir_invalidos == 1 ){
					
					if($id_tipo_iva == EnumTipoIva::IvaResponsableInscripto){//no van los B para compras ni gastos ni los C para Ventas
						//TODO: Mejorar
						if( $this->id_sistema_comprobante == EnumSistema::COMPRAS || $this->id_sistema_comprobante == EnumSistema::CAJA ){
							
							array_push($conditions,array("NOT"=>array("TipoComprobante.letra"=>"B")));
							
						}else{
							array_push($conditions,array("NOT"=>array("TipoComprobante.letra"=>"C")));
						}
					}else{ //no van los A  para compras ni gastos ni los A ni B para Ventas
						
						if(isset($this->id_sistema_comprobante) &&
								($this->id_sistema_comprobante == EnumSistema::COMPRAS || $this->id_sistema_comprobante == EnumSistema::CAJA )){
									
									array_push($conditions,array("NOT"=>array("TipoComprobante.letra"=>"A")));
						}else{
							array_push($conditions,array("NOT"=>array("TipoComprobante.letra"=>"A")));
							array_push($conditions,array("NOT"=>array("TipoComprobante.letra"=>"B")));
						}
					}
				}
				$tipos_comprobante = $this->Comprobante->TipoComprobante->find('all', array(
						'conditions' => $conditions,
						'contain' =>array('Persona'=>array('Provincia'),'Producto','DepositoOrigen','DepositoDestino','CuentaBancaria')
				));
				
				$data = $this->cleanTipoComprobante_forOutput($tipos_comprobante);
				
				$output = array(
						"status" =>EnumError::SUCCESS,
						"message" => "TipoComprobante",
						"content" => $data,
						"page_count"=>0
				);
				
				$this->set($output);
				$this->set("_serialize", array("status", "message","page_count", "content"));
		}
	       			
	       			

		protected function cleanTipoComprobante_forOutput(&$data)
		{
			$this->loadModel("ComprobanteItem");
			foreach($data as &$valor){

				$valor["TipoComprobante"]["d_deposito_origen"] =  "";
				$valor["TipoComprobante"]["d_deposito_destino"] =  "";
				$valor['TipoComprobante']['d_producto_default'] = "";
				$valor['TipoComprobante']['codigo_producto_default'] = "";
				$valor['TipoComprobante']['costo_default'] = "";
				$valor['TipoComprobante']['id_moneda_costo_default'] = "";
				$valor['TipoComprobante']['id_unidad_default'] = "";
				$valor['TipoComprobante']['id_iva_default'] = "";
				
				if(isset($valor["DepositoOrigen"]) && $valor["DepositoOrigen"]["id"]>0)
					$valor["TipoComprobante"]["d_deposito_origen"] =  $valor["DepositoOrigen"]["d_deposito_origen"];
				unset($valor["DepositoOrigen"]);
					
				if(isset($valor["DepositoDestino"]) && $valor["DepositoDestino"]["id"]>0)
					$valor["TipoComprobante"]["d_deposito_destino"] =  $valor["DepositoDestino"]["d_deposito_destino"];
				unset($valor["DepositoDestino"]);
				
				if(isset($valor["CuentaBancaria"]) && $valor["CuentaBancaria"]["id"]>0)
					$valor["TipoComprobante"]["nro_cuenta_bancaria"] =  $valor["CuentaBancaria"]["nro_cuenta"];
				unset($valor["CuentaBancaria"]);
						
				if( isset($valor['Persona']) &&  $valor['TipoComprobante']['id_persona_default']!= null )
				{
					$valor['TipoComprobante']['codigo_persona_default'] = $valor['Persona']['codigo'];
					$valor['TipoComprobante']['razon_social_persona_default'] = 	  $valor['Persona']['razon_social'];
					$valor['TipoComprobante']['cuit_persona_default'] =   $valor['Persona']['cuit'];
					$valor['TipoComprobante']['tel_persona_default'] = 	  $valor['Persona']['tel'];
					$valor['TipoComprobante']['id_tipo_iva_default'] =      $valor['Persona']['id_tipo_iva'];
					$valor['TipoComprobante']['id_moneda_default'] =      $valor['Persona']['id_moneda'];
					
					$valor['TipoComprobante']['id_condicion_pago_default'] = 	  	 $valor['Persona']['id_condicion_pago'];
					$valor['TipoComprobante']['id_forma_pago_default'] = 	  	 $valor['Persona']['id_forma_pago'];
					$valor['TipoComprobante']['tiene_cuenta_corriente_default'] =$valor['Persona']['cuenta_corriente'];
					
					$valor['TipoComprobante']['calle_default'] =$valor['Persona']['calle'];
					$valor['TipoComprobante']['numero_calle_default'] =$valor['Persona']['numero_calle'];
					$valor['TipoComprobante']['localidad_default'] =$valor['Persona']['localidad'];
					$valor['TipoComprobante']['ciudad_default'] =$valor['Persona']['ciudad'];
					$valor['TipoComprobante']['d_provincia_default'] =$valor['Persona']['Provincia']['d_provincia'];
					$valor['TipoComprobante']['id_lista_precio_default'] =$valor['Persona']['id_lista_precio'];
				}
				unset($valor['Persona']);
				
				if( isset($valor['Producto']) &&  $valor['TipoComprobante']['id_producto_default']!= null )
				{
					$valor['TipoComprobante']['d_producto_default'] = $valor['Producto']['d_producto'];
					$valor['TipoComprobante']['codigo_producto_default'] = $this->ComprobanteItem->getCodigoFromProducto($valor);
					$valor['TipoComprobante']['costo_default'] = $valor['Producto']['costo'];
					$valor['TipoComprobante']['id_moneda_costo_default'] = $valor['Producto']['id_moneda_costo'];
					
					$valor['TipoComprobante']['id_unidad_default'] = $valor['Producto']['id_unidad'];
					$valor['TipoComprobante']['id_iva_default'] = $valor['Producto']['id_iva'];
				}
				unset($valor['Producto']);
			}
			return $data;
		}
	       			
	       			
	private function setTasaIva(){
		
		
		if(isset($this->request->data["ComprobanteImpuesto"])){
			
			$this->loadModel(EnumModel::TipoAlicuotaAfip);
			
			foreach($this->request->data["ComprobanteImpuesto"] as &$data){
				
				
				if(isset($data["tipo_alicuota_afip"]) && isset($data["id_impuesto"])  && ($data["id_impuesto"] == EnumImpuesto::IVANORMAL || $data["id_impuesto"] == EnumImpuesto::IVACOMPRAS )){
					
					$data["tasa_impuesto"] = $this->TipoAlicuotaAfip->getTasaIva($data["tipo_alicuota_afip"]);
					
				}
				
			}
			
		}
		
	}
	
	
	protected function abreComprobante(){
		
		/*Funcion para abrir comprobante*/
		
		$this->loadModel(EnumModel::Comprobante);
		$id_comprobante = $this->getFromRequestOrSession('Comprobante.id');
		
		$comprobante = $this->Comprobante->find('first', array(
				'conditions' => array(
						'Comprobante.id' => $id_comprobante
				) ,
				'contain' => array("TipoComprobante","Asiento")
		));
		
		
		$error = EnumError::SUCCESS;
		
		if($comprobante){
			
			if($comprobante["Comprobante"]["cae"] >0){
				$error = EnumError::ERROR;
				$message = "El comprobante seleccionado tiene CAE no es posible abrirlo";
			}
			
			
			
			
		}else{
			
			$error = EnumError::ERROR;
			$message = "El comprobante que ingreso no existe en la base de datos";
			
		}
		
		
		$output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "TipoComprobante",
				"content" => $data,
				"page_count"=>0
		);
		
		$this->set($output);
		$this->set("_serialize", array("status", "message","page_count", "content"));
		
	}
	
	
	       			
    
}

?>