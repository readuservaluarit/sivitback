<?php 
         include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');
         
         
         App::import('Lib','SecurityManager');
          $sm = new SecurityManager(); 
         $recursos_humanos = $sm->permiso_particular("CONSULTA_EMPLEADO",AuthComponent::user('username'))>0;
         
         
		$model["orden"]["show_grid"] = 0; //INVISIBLE PARA NO PERDER EL ORDEN DE LOS Q NO TIENE IDS
         $model["orden"]["header_display"] = "Orden";
         $model["orden"]["order"] = "0";
		 $model["orden"]["width"] = "3";
		 $model["orden"]["read_only"] = "0";
         
         
		$model["n_item"]["show_grid"] = 1;
        $model["n_item"]["header_display"] = "Nro.";
        $model["n_item"]["order"] = "1";
		$model["n_item"]["width"] = "3";
		$model["n_item"]["read_only"] = "0";
        
        
		$model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "Etapa";
        $model["d_producto"]["order"] = "2";
		$model["d_producto"]["width"] = "25";
		$model["d_producto"]["read_only"] = "1";
		$model["d_producto"]["picker"] = EnumMenu::mnuArticulosGeneral;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
		
		$model["codigo_observacion"]["type"] = "float"; //sobre escrito para q lo formatee lindo
		$model["codigo_observacion"]["show_grid"] = 1;
        $model["codigo_observacion"]["header_display"] = "T.Ciclo[Min]";
        $model["codigo_observacion"]["order"] = "4";
        $model["codigo_observacion"]["width"] = "7";
		$model["codigo_observacion"]["read_only"] = "0";
        $model["codigo_observacion"]["text_colour"] = "#000000";
		
		$model["item_observacion"]["show_grid"] = 1;
        $model["item_observacion"]["header_display"] = "F. verif.";
        $model["item_observacion"]["order"] = "6";
        $model["item_observacion"]["width"] = "6";
		$model["item_observacion"]["read_only"] = "0";
		$model["item_observacion"]["text_colour"] = "#000000";
		
		$model["cantidad"]["show_grid"] = 1;
        $model["cantidad"]["header_display"] = "Cant.";
        $model["cantidad"]["order"] = "8";
        $model["cantidad"]["width"] = "4";
		$model["cantidad"]["read_only"] = "0";
        $model["cantidad"]["min_value"] = "0"; 
		$model["cantidad"]["text_colour"] = "#0000FF"; //BLUE

		$model["fecha_generacion"]["show_grid"] = 1;
		$model["fecha_generacion"]["type"] = "datetime";
		$model["fecha_generacion"]["mask"] = "00/00/0000 00:00"; //esto convierte la grilla
        $model["fecha_generacion"]["header_display"] = "Fch. Inicio";
        $model["fecha_generacion"]["order"] = "10";
        $model["fecha_generacion"]["width"] = "10";
		$model["fecha_generacion"]["read_only"] = "0";
		$model["fecha_generacion"]["text_colour"] = "#000000";
		
		$model["fecha_cierre"]["show_grid"] = 1;
		$model["fecha_cierre"]["type"] = "datetime";
		$model["fecha_cierre"]["mask"] = "00/00/0000 00:00"; //esto convierte la grilla 
        $model["fecha_cierre"]["header_display"] = "Fch. Finalizacion";
        $model["fecha_cierre"]["order"] = "12";
        $model["fecha_cierre"]["width"] = "10";
		$model["fecha_cierre"]["read_only"] = "0";
		$model["fecha_cierre"]["text_colour"] = "#000000";
               
	    if($recursos_humanos == 1){    
			$model["d_persona"]["show_grid"] = 1;
	        $model["d_persona"]["header_display"] = "Liberada Por";
	        $model["d_persona"]["order"] = "14";
	        $model["d_persona"]["width"] = "9";
			$model["d_persona"]["read_only"] = "1";
			$model["d_persona"]["text_colour"] = "#C0000C";
			$model["d_persona"]["picker"] = EnumMenu::mnuEmpleadoGeneral;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
		
		 }
		
		$output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItem",
                    "content" => $model
                );
        echo json_encode($output);
         
        

?>