<?php

    App::import('Lib', 'PagingHelper');

    $this->Paginator->options(array(
            'update' => '.fancybox-inner',
            'evalScripts' => true,
            'before' => $this->Js->get('#zk_proc')->effect('show', array('buffer' => false)),
            'complete' => $this->Js->get('#zk_proc')->effect('hide', array('buffer' => false)) . '$(".bus_unico:first").focus();',
        ));

?>

<div class="well"  style="width:<?php echo $width; ?>">

  <div class="container-fluid">
              <div class="row">
               

 <!-- Statistics -->
                <div class="statistics col-sm-12">
                <div class="card-header"><h4><?php echo __($title); ?></h4></div>
    <div class='top'>
        <div class='picker_filter' id='picker_filter'>
            <?php echo $this->Form->create($model); 
                echo $pickerConfiguration->getFilterFields($this, $model, $busqueda, $pickerId, $selectFields, $callbacksJS, $filterQueryFields);
            ?>
        </div>
        <div class='clear'></div>
    </div>

    <div style="width:100%;text-align:right;" class="pull-right">
        <a id="btnNinguno" href="#" onclick="selectNinguno();"><span class="add-on"><i class="icon-remove-circle" id="btnNuevo"></i></span>&nbsp;Ninguno</a>&nbsp;&nbsp;
    </div>
    <table id="picker" class='table table-striped table-bordered table-hover'>
        <thead>
            <tr>
                <?php

                    foreach ($fields as $field){

                        if(!isset($field['visible']))
                            $field['visible'] = true;
                        
                        if(isset($field['keyOrder']) and $field['keyOrder'] != false and trim($field['keyOrder']) != "" )
                            PagingHelper::SortHeader($this, $field['keyOrder'], __($field['title']), $field['width'], $field['visible'], array($model, 'escape' => false, 'visible' => false));
                        else
                            PagingHelper::NonSortedHeader($this, __($field['title'], $field['visible']));

                    }
                ?>
                <th>Seleccionar</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i=0;
                foreach ($rows as $row){
                    $i++;
                    $class = PagingHelper::getRowClass($i);
                ?>
                <tr class='<?php echo $class; ?>'>

                    <?php

                        foreach ($fields as $field){
                            $aux = explode(".", $field['field']);
                            if (count($aux) > 1) {
                                $val = $row;
                                foreach($aux as $aux2){
                                    $val = $val[$aux2];
                                }
                            } else {
                                $val = $row[$model][$field['field']];
                            }
                            $display = "";
                            if(isset($field['visible']) && !$field['visible'])
                                $display = "display:none;";
                            echo "<td style='". $display ."'>".__($val)."</td>";
                        }
                    ?>
                    <td class="center" style="width:10%;text-align:center;">
                        
                        
                        <?php 
                       
                        
                            echo $this->Html->link("<i class='fa fa-check-circle'></i>", 
                                array(), 
                                array( 'escape' => false, 
                                    'class' => '', 
                                    'title' => 'Seleccionar', 
                                    'data-gravity' => 's',
                                    'onclick' => getOnSelect($model, $row, $arraySelectFields, $pickerConfiguration, $arrayCallbacksJS)                                                                          )
                            );?>
                    </td>   
                </tr>

                <?php
                }
            ?>
        </tbody>
    </table>
    <div class='block-actions' style='text-align:center;'>
        <div class='picker_info' id='picker_info'>
            <?php echo $this->Paginator->counter('Registros {:start} a {:end} de {:count}') ?>
        </div>
        
     
<nav aria-label="Page navigation example">
  <ul class="pagination">
    <li class="page-item"><?php echo $this->Paginator->first('<<') ?></li>
    <li class="page-item"><?php echo $this->Paginator->prev('<', null, null, array('class' => 'page-link')) ?></li>
     <?php echo $this->Paginator->numbers(array('tag'=>'li','currentTag' => 'a', 'class' =>'page-item', 'separator' => '', 'currentClass' => 'active')) ?>
     
    <li class="page-item"><?php echo $this->Paginator->next('>', null, null, array('class' => 'page-link')) ?></li>
    <li class="page-item"><?php echo $this->Paginator->last('>>') ?></li>
  </ul>
</nav>



    </div>
</div> 
</div> 




<?php

    echo $this->Js->writeBuffer();


    function getOnSelect($model, $row, $selectFields, $pickerConfiguration, $callbacksJS){

        //$str = "function(){";
        $str="";

        foreach($selectFields as $field){
            $aux = explode(".", getFieldName($field['fieldIndex'], $pickerConfiguration));
            if (count($aux) > 1) {
                $val = $row;
                foreach($aux as $aux2)
                    $val = $val[$aux2];
            } else {
                $val = $row[$model][getFieldName($field['fieldIndex'], $pickerConfiguration)];
            }

            $str .= "$('#". $field['elementId'] ."').val('" . $val . "');";
            $str .= "$('#". $field['elementId'] ."').text('" . $val . "');";

        }

        if($callbacksJS!=null){

            foreach($callbacksJS as $callback){
                $str .= $callback; 
            }

        }
        $pedo = "$('#". $field['elementId'] ."').val('" . $val . "');";

        $str .= " pickerSelected = 1; $.fancybox.close(); return false;";
        //$str .= "} return false;";
        

        return $str;


    }

    function getFieldName($fieldIndex, $pickerConfiguration){

        return $pickerConfiguration->pickerFields[$fieldIndex]["field"];

        $i=0;
        foreach($this->pickerConfiguration->pickerFields as $field){

            $i++;
            if (trim(upper($field["field"])) == trim(upper($fieldName)))
                return $i; 
        }

        return 0;

    }


?>

<script type="text/javascript">

    function selectNinguno(){
        
        pickerSelected = 0;
        pickerClose(true);
        $.fancybox.close(); 
        return false;
        
        
    }


    var pickerSelected = 0;

    function pickerClose(clean){
        if(pickerSelected == 0){
            if(clean != undefined && clean==true){
            <?php

                $str = "";

                foreach($arraySelectFields as $field){

                    $str .= "$('#". $field['elementId'] ."').val('');";
                    $str .= "$('#". $field['elementId'] ."').text('');";


                }
                echo $str;
            ?>
            }
            <?php    
                if($arrayCallbacksJS!=null){

                    foreach($arrayCallbacksJS as $callback){
                        echo $callback; 
                        }

                }

            ?>

        }

    }
    
    
    $('#botonBuscarPicker').closest('form').keydown(function(e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        $('#botonBuscarPicker').click();
        return false;
    }
});

$( "li.page-item" ).find( "a" ).addClass( "page-link" );//seteo clase boostrap

</script>
