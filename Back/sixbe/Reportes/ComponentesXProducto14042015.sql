/*Este reporte muestra la cantidad de productos en los que estan presentes cada componente*/

SELECT cm.`codigo` AS codigo_componente,p.`codigo` AS codigo_producto FROM componente_producto cp
LEFT JOIN componente cm ON cm.`id` = cp.`id_componente`
LEFT JOIN producto p ON cp.`id_producto`= p.`id`
WHERE cm.`codigo` LIKE 'VNR%' OR cm.`codigo` LIKE 'VCR%' OR cm.`codigo` LIKE 'H00%' OR cm.`codigo` LIKE 'EMC%' 
OR cm.`codigo` LIKE 'EHU%' OR cm.`codigo` LIKE 'EGV%' OR cm.`codigo` LIKE 'ABF%' OR cm.`codigo` LIKE 'ANF%' 
OR cm.`codigo` LIKE 'ANR%' OR cm.`codigo` LIKE 'H00%' OR cm.`codigo` LIKE 'ASM%'
AND p.`activo`=1

ORDER BY cm.`codigo`,p.`codigo` LIMIT 0 , 100000

