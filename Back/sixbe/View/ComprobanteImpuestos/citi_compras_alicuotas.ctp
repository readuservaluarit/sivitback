<?php


    App::import('Model','Comprobante'); 
    App::import('Model','DatoEmpresa'); 
    App::import('Model','ComprobanteImpuesto'); 
    
      if($nombre_archivo  == "")
        $nombre_archivo = "CitiComprasAlicuotas";
        
    header("Content-Disposition: attachment; filename=".$nombre_archivo.".txt"); 
    header("Pragma: no-cache"); 
    header("Expires: 0");
    
    $comprobante_obj  = new Comprobante();
    $dato_empresa_obj  = new DatoEmpresa();
    $comprobante_impuesto_obj  = new ComprobanteImpuesto();
    
    $id_tipo_moneda = $dato_empresa_obj->getNombreCampoMonedaPorIdMoneda(EnumMoneda::Peso);
    $field_valor_moneda = $comprobante_obj->getFieldByTipoMoneda($id_tipo_moneda);
    
    
    
  
    $ya_imprimi_tasa_cero = array();
    foreach($datos as $comprobante_impuesto){
    
    
    
    	$no_imprimo =0; 
        
        if($comprobante_impuesto["Comprobante"]["TipoComprobante"]["letra"] !="C"  || $comprobante_impuesto["Comprobante"]["TipoComprobante"]["letra"] !="B"){ 




			       $tipo_comprobante_afip_01 = str_pad($comprobante_impuesto["Comprobante"]["TipoComprobante"]["codigo_afip"], 3, '0', STR_PAD_LEFT);
			       
			       
			      
			      //fuerzo a que la alicuota sea 0% (tipo 3)
			       if($comprobante_impuesto["ComprobanteImpuesto"]["tipo_alicuota_afip"] == 1 || $comprobante_impuesto["ComprobanteImpuesto"]["tipo_alicuota_afip"] == 2)
			       		$comprobante_impuesto["ComprobanteImpuesto"]["tipo_alicuota_afip"] = 3;
			       		
			       		
			       $alicuota_iva_afip_07 =  str_pad($comprobante_impuesto["ComprobanteImpuesto"]["tipo_alicuota_afip"], 4, '0', STR_PAD_LEFT);		
			       
			        if($comprobante_impuesto["PuntoVenta"]["numero"]>0)
			            $punto_venta_02 = str_pad($comprobante_impuesto["PuntoVenta"]["numero"], 5, '0', STR_PAD_LEFT);
			        else
			            $punto_venta_02 = str_pad($comprobante_impuesto["Comprobante"]["d_punto_venta"], 5, '0', STR_PAD_LEFT);
			       
			       
			       
			       $nro_comprobante_03 = str_pad($comprobante_impuesto["Comprobante"]["nro_comprobante"], 20, '0', STR_PAD_LEFT);
			       $codigo_documento_comprador_04 = str_pad($comprobante_impuesto["Comprobante"]["Persona"]["TipoDocumento"]["abreviacion_afip"], 2, '0', STR_PAD_LEFT);
			       $cuit_final_05 =  str_pad($comprobante_impuesto["Comprobante"]["Persona"]["cuit"], 20, '0', STR_PAD_LEFT);
			       
			       
			       $cantidad_ivas_tasa_mayor_cero = $comprobante_obj->getCantidadIvasTasaMayorACero($comprobante_impuesto["Comprobante"]["id"],$comprobante_impuesto["Comprobante"]["TipoComprobante"]["id_sistema"]);
			       
			       
			       /*
			       if($cantidad_ivas_tasa_mayor_cero>0 && $comprobante_impuesto["ComprobanteImpuesto"]["tasa_impuesto"] == 0)
			       		$no_imprimo =1; 
			       	*/
			       	
			      	$codigo_operacion = $comprobante_obj->getCodigoOperacionAfip($comprobante_impuesto["Comprobante"]["id"], $comprobante_obj->getNoGravado($comprobante_impuesto["Comprobante"]["id"]),$comprobante_obj->getExento($comprobante_impuesto["Comprobante"]["id"]));
			      	
			      	
			  
			       if($comprobante_impuesto["ComprobanteImpuesto"]["tasa_impuesto"] == 0 && !isset($ya_imprimi[$comprobante_impuesto["Comprobante"]["id"]]) ){
			         
					 $ya_imprimi[$comprobante_impuesto["Comprobante"]["id"]] = 1;
			         $total_gravado = 0.00;
			         $alicuota_iva_afip_07 =  str_pad(3, 4, '0', STR_PAD_LEFT);
			
			         $total_gravado_06 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_gravado,13,2,"0");
			         
			         $impuesto_liquidado_08 = $comprobante_impuesto_obj->getTotalImpuestoFromComprobanteCitiAlicuotas($comprobante_impuesto["Comprobante"]["id"], $impuestos_tasa_cero);
			         $impuesto_liquidado_08 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros(0,13,2,"0");
			         
			       }elseif( isset($ya_imprimi[$comprobante_impuesto["Comprobante"]["id"]]) && $comprobante_impuesto["ComprobanteImpuesto"]["tasa_impuesto"] == 0){
			       
			       		$no_imprimo = 1;
			       
			       
			       }else{
		
			           
			         
			         $ya_imprimi[$comprobante_impuesto["Comprobante"]["id"]] = 1;
			         
			         $total_gravado_06 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($comprobante_impuesto["ComprobanteImpuesto"]["base_imponible"],$field_valor_moneda, $comprobante_impuesto["Comprobante"]);
			         $total_gravado_06 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_gravado_06,13,2,"0"); 
			         
			         
			         $impuesto_liquidado_08 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($comprobante_impuesto["ComprobanteImpuesto"]["importe_impuesto"],$field_valor_moneda, $comprobante_impuesto["Comprobante"]); 
			
			         $impuesto_liquidado_08 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($impuesto_liquidado_08,13,2,"0"); 
			         
			         
			       } 
			
			       
			       
			       
			 
			      
			       
			       
			       
			       
			       
			       $linea_texto = $tipo_comprobante_afip_01.$punto_venta_02.$nro_comprobante_03.$codigo_documento_comprador_04.$cuit_final_05.$total_gravado_06.$alicuota_iva_afip_07.$impuesto_liquidado_08;
			       
			       if($no_imprimo == 0)
			       	echo $linea_texto.  "\r\n";      
			       
			          
			    
		}
     
     }
        


?>