<?php
/**
* @secured(CONSULTA_TIPO_COMPROBANTE)
*/
class EstadosTipoComprobanteController extends AppController {
    public $name = 'EstadosTipoComprobante';
    public $model = 'EstadoTipoComprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    public function index() {

    	$this->loadModel($this->model);
        //Recuperacion de Filtros
        $id_tipo_comprobante = strtolower($this->getFromRequestOrSession('EstadoTipoComprobante.id_tipo_comprobante'));
        $conditions = array(); 

		// $id_tipo_comprobante = 103;		
	   if ($id_tipo_comprobante!= "") {
        	$conditions = array('EstadoTipoComprobante.id_tipo_comprobante' => $id_tipo_comprobante);
        }
			$data = $this->{$this->model}->getindex($conditions,$this->model);
        	
        	$this->data = $data;
        	
        	$output = array(
        			"status" =>EnumError::SUCCESS,
        			"message" => "list",
        			"content" => $data,
        			"page_count" =>0
        	);
        	$this->set($output);
        	$this->set("_serialize", array("status", "message","page_count", "content"));
	}

    /**
    * @secured(ADD_TIPO_COMPROBANTE,READONLY_PROTECTED)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            if ($this->EstadoTipoComprobante->save($this->request->data))
                $this->Session->setFlash('El Estado Tipo Comprobante  ha sido creado exitosamente.', 'success'); 
            else
                $this->Session->setFlash('Ha ocurrido un error, el Estado Tipo Comprobante no ha podido ser creada.', 'error');

            $this->redirect(array('action' => 'index'));
        } 

        $this->redirect(array('action' => 'abm', 'A'));                   
    }

    /**
    * @secured(MODIFICACION_TIPO_COMPROBANTE,READONLY_PROTECTED)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->EstadoTipoComprobante->id = $id;
            
            try{ 
            	if ($this->EstadoTipoComprobante->saveAll($this->request->data)){
                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "El Estado Tipo Comprobante ha sido modificado exitosamente",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('El Estado Tipo Comprobante ha sido modificado exitosamente.', 'success');
                        $this->redirect(array('controller' => 'Monedas', 'action' => 'index'));
                        
                    } 
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, el Estado Tipo Comprobante no ha podido modificarse.', 'error');
                
            }
            $this->redirect(array('action' => 'index'));
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }

    public function getModel($vista = 'default'){
    	
    	$model = parent::getModelCamposDefault();//esta en APPCONTROLLER
    	
    	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    	
    }
    
    public function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    	
    	$model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
    	$this->set('model',$model);
    	// $this->set('this',$this);
    	Configure::write('debug',0);
    	$this->render($vista);
    }
    


}
?>