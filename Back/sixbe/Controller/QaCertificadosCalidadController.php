<?php

  /**
    * @secured(CONSULTA_QACERTIFICADOCALIDAD)
  */
class QaCertificadosCalidadController extends AppController {
    public $name = 'QaCertificadosCalidad';
    public $model = 'QaCertificadoCalidad';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
  
  
    /**
    * @secured(CONSULTA_QACERTIFICADOCALIDAD)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        /*
        TODO: AGREGAR FILTROS DE CODIGO Y DESCIPRCION de id_producto
        [codigo]", this.codigo.Text);
        [descripcion]", this.descripcion.Text);
        */
        //Recuperacion de Filtros
        
        
        $fecha = $this->getFromRequestOrSession('QaCertificadoCalidad.fecha');
        $fecha_hasta = $this->getFromRequestOrSession('QaCertificadoCalidad.fecha_hasta');
        $nro_comprobante = $this->getFromRequestOrSession('QaCertificadoCalidad.nro_comprobante');
        $id_persona = $this->getFromRequestOrSession('QaCertificadoCalidad.id_persona');
        $nro_orden_armado = $this->getFromRequestOrSession('QaCertificadoCalidad.nro_orden_armado');
        $nro_pedido_interno  = $this->getFromRequestOrSession('QaCertificadoCalidad.nro_pedido_interno');
        
        
        
        
        if($this->Auth->user('id_persona')>0){
        	
        	$id_persona = $this->Auth->user('id_persona');
        	
        }

        $razon_social = $this->getFromRequestOrSession('QaCertificadoCalidad.persona_razon_social');
        
        $id_orden_armado = $this->getFromRequestOrSession('QaCertificadoCalidad.id_orden_armado');
        $id_pedido_interno = $this->getFromRequestOrSession('QaCertificadoCalidad.id_pedido_interno');
        
        $id_estado = $this->getFromRequestOrSession('QaCertificadoCalidad.id_estado');
        
        
        $codigo_producto = $this->getFromRequestOrSession('QaCertificadoCalidad.codigo_producto');
         
        $conditions = array(); 
        
        
        if($fecha!=""){
        	
        	$exp = explode('/', $fecha);//el  front web me envia guiones
        	
        	if(count($exp) != 0)
        		$fecha= $exp[2].'-'.$exp[1].'-'.$exp[0];
        		
            array_push($conditions, array('QaCertificadoCalidad.fecha >=' => $fecha)); 
        }
            
        if($fecha_hasta!=""){
        	
        	$exp = explode('/', $fecha_hasta);//el  front web me envia guiones
        	
        	if(count($exp) != 0)
        		$fecha_hasta= $exp[2].'-'.$exp[1].'-'.$exp[0];
        	
        	array_push($conditions, array('QaCertificadoCalidad.fecha <=' => $fecha_hasta));
        }
            
        if($nro_comprobante!="")
            array_push($conditions, array('QaCertificadoCalidad.id ' => $nro_comprobante));
        
            
        if($this->Auth->user('id_persona')>0){
            	
            	$id_persona = $this->Auth->user('id_persona');
        }
        
        
        
        
        
        if($id_persona!="")
            array_push($conditions, array('QaCertificadoCalidad.id_persona ' => $id_persona));
            
         if($razon_social!="")
            array_push($conditions, array('Persona.razon_social LIKE' => '%' . $razon_social  . '%')); 
        
        if($id_orden_armado!="")
            array_push($conditions, array('QaCertificadoCalidad.id_orden_armado' => $id_orden_armado));
        
        if($id_pedido_interno!="")
            array_push($conditions, array('QaCertificadoCalidad.id_pedido_interno' => $id_pedido_interno));
        
        if($id_estado!="")
            array_push($conditions, array('QaCertificadoCalidad.id_estado ' => $id_estado));      
         
        if($nro_orden_armado!="")
        	array_push($conditions, array('OrdenArmado.nro_comprobante' => $nro_orden_armado));   
        
        if($nro_orden_armado!="")
        		array_push($conditions, array('OrdenArmado.nro_comprobante' => $nro_orden_armado));
        
        if($nro_pedido_interno!="")
        	array_push($conditions, array('PedidoInterno.nro_comprobante' => $nro_pedido_interno));
         
         if($codigo_producto!="")
         	array_push($conditions, array('Producto.codigo LIKE ' =>'%'.$codigo_producto.'%'));                
                           
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>array('Persona','Estado','QaCertificadoCalidadItem'=>array('Componente'=>array('Categoria')),'Producto','PedidoInterno','OrdenArmado'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => 'QaCertificadoCalidad.id desc'
        );
        
      
      
      
      
      
          

        
        
        
        
        
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor)
        {
             $valor['QaCertificadoCalidad']['QaCertificadoCalidadItem'] = $valor['QaCertificadoCalidadItem'];
             unset($valor['QaCertificadoCalidadItem']);
             
            if(isset($valor['QaCertificadoCalidad']['QaCertificadoCalidadItem'])){
                 foreach($valor['QaCertificadoCalidad']['QaCertificadoCalidadItem'] as &$componente ){
                     
                     $componente["d_producto_hijo"] = $componente["Componente"]["d_producto"];
                     //$producto["d_producto"] = $this->setFieldProperties("d_producto","Producto",$producto["Producto"]["d_producto"],"1","3");
                     //$producto["codigo_producto"] = $this->setFieldProperties("codigo_producto","Codigo",$producto["Producto"]["codigo"],"1","4");
                     
                     $componente["codigo_hijo"] = $componente["Componente"]["codigo"];
                     
                     if(isset($componente["Categoria"]))
                        $componente["d_categoria"] = $componente["Categoria"]["d_categoria"];
                     
                     unset($componente["Componente"]);
                 }
            }
            
            
             //$valor['QaCertificadoCalidad']['estado'] = $valor['Estado'];
             $valor['QaCertificadoCalidad']['d_estado'] = $valor['Estado']['d_estado'];
             unset($valor['Estado']);
             //$valor['QaCertificadoCalidad']['Persona'] = $valor['Persona'];
             $valor['QaCertificadoCalidad']['razon_social'] = $valor['Persona']['razon_social'];
             $valor['QaCertificadoCalidad']['id_persona'] = $valor['Persona']['id'];
             $valor['QaCertificadoCalidad']['d_producto'] = $valor['Producto']['d_producto'];
             unset($valor['Cliente']);
             $valor['QaCertificadoCalidad']['Comprobante'] = $valor['Comprobante'];
             $valor['QaCertificadoCalidad']['nro_pedido_interno'] = $valor['PedidoInterno']['nro_comprobante'];
             unset($valor['Comprobante']);
             $valor['QaCertificadoCalidad']['OrdenArmado'] = $valor['OrdenArmado'];
             $valor['QaCertificadoCalidad']['nro_orden_armado'] = $valor['OrdenArmado']['nro_comprobante'];
             
             unset($valor['Comprobante']);
             unset($valor['OrdenArmado']);
             unset($valor['Persona']);
             unset($valor['Producto']);
      
            }
             
             $seteo_form_builder = array("showActionColumn" =>true,"showNewButton" =>false,"showDeleteButton"=>false,"showEditButton"=>false,"showPrintButton"=>true,"showFooter"=>true,"showHeaderListado"=>true,"showXlsButton"=>false);
             $this->data = $data;
             $this->title_form = "Listado de Certificados de calidad";
             $this->preparaHtmLFormBuilder($this,$seteo_form_builder,"");
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
 
        
        
        
        
    //fin vista json
        
    }
    
    /**
    * @secured(ABM_USUARIOS)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->QaCertificadoCalidad->id = $id;
            $this->QaCertificadoCalidad->contain();
            $this->request->data = $this->QaCertificadoCalidad->read();
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('QaCertificadoCalidad');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de QaCertificadosCalidad');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de QaCertificadosCalidad');
        else
            $formBuilder->setTituloForm('Consulta de QaCertificadosCalidad');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('cuit', 'Cuit', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('calle', 'Calle', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  
        $formBuilder->addFormInput('tel', 'Tel&eacute;fono', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('ciudad', 'Ciudad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descuento', 'Descuento', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descripcion', 'Descripcion', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('localidad', 'Localidad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
         
        
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'ClienteAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('ClienteRazonSocial', 'Debe ingresar una Raz&oacute;n Social');
                validator.validateRequired('ClienteCuit', 'Debe ingresar un CUIT');
                validator.validateNumeric('ClienteDescuento', 'El Descuento ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_cuit',
                        'data': 'cuit=' + $('#ClienteCuit').val()+'&id='+$('#ClienteId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('ClienteCuit','El Cuit que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

    /**
    * @secured(CONSULTA_QACERTIFICADOCALIDAD)
    */
    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

     /**
    * @secured(ADD_QACERTIFICADOCALIDAD)
    */
   public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_c = "";
           $this->request->data["QaCertificadoCalidad"]["fecha"] = date("Y-m-d H:i:s");
            try{
                
                if(!isset($this->request->data['QaCertificadoCalidad']['id'])){
              
                    $QaCertificadoCalidad = $this->QaCertificadoCalidad->save($this->request->data);
                    
                    if (!empty($QaCertificadoCalidad)) {
                        
                       if(isset($this->request->data['QaCertificadoCalidadItem'])){
                            foreach($this->request->data['QaCertificadoCalidadItem'] as &$c_item){
                                $c_item['id_qa_certificado_calidad'] = $this->QaCertificadoCalidad->id;
                                
                            }
                       
                            $this->QaCertificadoCalidad->QaCertificadoCalidadItem->saveAll($this->request->data['QaCertificadoCalidadItem']);
                          }
                          $mensaje = "El  Certificado de Calidad ha sido creado exitosamente";
                          $tipo = EnumError::SUCCESS;
                          $id_c = $this->QaCertificadoCalidad->id;
                          
                          
                    }
                    
                }else{
                    
                
                    if ($this->QaCertificadoCalidad->saveAll($this->request->data, array('deep' => true))){
                        $mensaje = "El Certificado de Calidad ha sido creado exitosamente";
                        $tipo = EnumError::SUCCESS;
                       
                    }else{
                        $mensaje = "Ha ocurrido un error,el Certificado de Calidad no ha podido ser creado.";
                        $tipo = EnumError::ERROR; 
                        
                    }
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error, el Certificado de Calidad no ha podido ser creado.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" => $id_c
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
       /**
    * @secured(MODIFICACION_QACERTIFICADOCALIDAD)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->QaCertificadoCalidad->id = $id;
            
            try{ 
                if ($this->QaCertificadoCalidad->saveAll($this->request->data,array('deep' => true))){
                    
                        $error = EnumError::SUCCESS;
                        $message = "El Certificado de Calidad ha sido modificado exitosamente.";
                        
                       
                    
                }else{
                    $error = EnumError::ERROR;
                    $message = "El Certificado de Calidad NO ha sido modificado.";
                }
            }catch(Exception $e){
                 $error = EnumError::ERROR;
                 $message = "El Certificado de Calidad NO ha sido modificado.".$e->getMessage();
                
            }
           
        }
        
        if($this->RequestHandler->ext != 'json'){
          $this->Session->setFlash('El Certificado de Calidad ha sido modificado exitosamente.', 'success');

            $this->redirect(array('action' => 'abm', 'M', $id));
       }else{
             $output = array(
                            "status" => $error,
                            "message" => $message,
                            "content" => ""
                        ); 
              $this->set($output);
              $this->set("_serialize", array("status", "message", "content"));
            
            
        }
    }
    
     /**
    * @secured(BAJA_QACERTIFICADOCALIDAD)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->QaCertificadoCalidad->id = $id;
       try{
            if ($this->QaCertificadoCalidad->saveField('activo', "0")) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Certificado de Calidad ha sido eliminado exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }

  /**
    * @secured(BAJA_QACERTIFICADOCALIDAD)
    */
    public function deleteItem($id_item){
        
        $this->loadModel($this->model);
        
        try{
            if ($this->QaCertificadoCalidad->QaCertificadoCalidadItem->delete($id_item)) {
        
                $status = EnumError::SUCCESS;
                $mensaje = "El Item ha sido eliminado exitosamente.";
              
            }else{
              
                
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
                
                
            }
        }
        catch(Exception $ex){ 
              $mensaje = "Ha ocurrido un error, el item no ha podido ser borrado";
               $status = EnumError::ERROR; 
        }
        
        
        
        
        if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
        }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
        }
        
        
        
        
    }

   
    
    
    
    
    public function home(){
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
    
    /*
    public function cerrar($id){
        
        $this->loadModel($this->model);
        $this->loadModel("Movimiento");
        
        $ds = $this->QaCertificadoCalidad->getdatasource();
        try{
            $ds->begin();
            $this->request->data["QaCertificadoCalidad"]["Estado"] = EnumEstado::Cerrada;
            
            
            
            $ds->commit();
        }catch(Exception $e){
                $ds->rollback();
                $mensaje = "Ha ocurrido un error,la QaCertificadoCalidad no ha podido ser cerrada.";
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => ""
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
        
        
        
        
    }*/
    
    
    
    /**
    * @secured(REPORTE_QACERTIFICADOCALIDAD)
    */
    public function pdfExport($id){
        
        
        $this->loadModel("QaCertificadoCalidad");
        $this->loadModel("ArticuloRelacion");
        $this->QaCertificadoCalidad->id = $id;
        $this->QaCertificadoCalidad->contain(array('Producto','OrdenArmado','QaCertificadoCalidadItem' => array('Componente'=>array('Categoria','ArticuloRelacion'=>array('ArticuloHijo'))),'Persona'=> array('Pais','Provincia')));
        //$this->QaCertificadoCalidad->order(array('QaCertificadoCalidadItem.orden asc'));
        $this->request->data = $this->QaCertificadoCalidad->read(); 
        Configure::write('debug',0);
        
        if( $this->request->data["Persona"]["id"] == null ){
            $this->request->data["Persona"]["razon_social"] = $this->request->data["QaCertificadoCalidad"]["razon_social"];
            $this->request->data["Persona"]["cuit"] = $this->request->data["QaCertificadoCalidad"]["cuit"]; 
        
        }
        
         

        foreach($this->request->data['QaCertificadoCalidadItem'] as &$dato){
            
           $id_mp_padre = $dato["Componente"]["ArticuloRelacion"][0]["ArticuloHijo"]["id"];//id de la MP 
           
           $costo = $this->ArticuloRelacion->find('first',array(
                                    'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_mp_padre),
                                    'contain' =>'ArticuloHijo'
                                    ));
                                    
           if($costo){
               
               $dato["Componente"]["ArticuloRelacion"][0]["ArticuloHijo"]["d_costo"] = $costo["ArticuloHijo"]["d_producto"];
               
           }else{
                $dato["Componente"]["ArticuloRelacion"][0]["ArticuloHijo"]["d_costo"] = $dato["Componente"]["ArticuloRelacion"][0]["ArticuloHijo"]["d_producto_extensa"];
           }
            
   
            
        }
            
        $this->set('datos_pdf',$this->request->data); 
        $this->set('id',$id);
        Configure::write('debug',0);
        $this->response->type('pdf');
        $this->layout = 'pdf'; //esto usara el layout pdf.ctp
        $this->render();
        
  
     
        
    }
    
    public function initialize(){ //Seteo los ordenes del Listado
        
        $this->addCalculatedField($this->setFieldProperties("QaCertificadoCalidadItem","Items de las QaCertificadosCalidad","","0","0"));
        $this->addCalculatedField($this->setFieldProperties("moneda","Moneda","","0","0"));;
        $this->addCalculatedField($this->setFieldProperties("estado","Estado","","0","0"));;
        $this->addCalculatedField($this->setFieldProperties("Cliente","Cliente","","0","0"));;
        
        
        //seteo las propiedades de los Items Agregados en runtime
        $this->addCalculatedFieldItem($this->setFieldProperties("d_producto","Producto","","1","3"));
        $this->addCalculatedFieldItem($this->setFieldProperties("codigo_producto","Codigo","","1","4"));
        
    }
    
    
    
    
  
    /**
     * @secured(CONSULTA_QACERTIFICADOCALIDAD)
     */
    public function getModel($vista='default'){
    	
    	$model = parent::getModelCamposDefault();
    	$model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
    	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
    	
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    	
    	
    	
    	$this->set('model',$model);
    	Configure::write('debug',0);
    	$this->render($vista);
    	
    	
    	
    	
    	
    	
    }
    
    
    protected function addWebVisualFilters($formBuilder){
    	
    	
    	
    	$formBuilder->addFilterBeginRow();
    	//$formBuilder->addFilterInput('codigo', 'C&oacute;digo', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $codigo));
    	$formBuilder->addFilterInput('fecha', 'Fecha', array('class'=>'control-group span5'), array('type' => 'text', 'class' => 'datepicker', 'label' => false, 'value' => $this->getFromRequestOrSession($this->model.'.fecha') ));
    	$formBuilder->addFilterInput('fecha_hasta', 'Fecha Hasta', array('class'=>'control-group span5'), array('type' => 'text', 'class' => 'datepicker', 'label' => false, 'value' => $this->getFromRequestOrSession($this->model.'.fecha_hasta')));
    	$formBuilder->addFilterInput('nro_comprobante', 'Nro. Comprobante', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $this->getFromRequestOrSession($this->model.'.nro_comprobante')));
    	$formBuilder->addFilterInput('codigo_producto', 'C&oacute;digo', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $this->getFromRequestOrSession($this->model.'.nro_comprobante')));
    	$formBuilder->addFilterEndRow();
    	
    }
    
   
    
}
?>