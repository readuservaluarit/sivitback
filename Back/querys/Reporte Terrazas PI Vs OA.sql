SELECT comprobante.`nro_comprobante` AS nro_pedido,persona.`razon_social`,comprobante.`fecha_generacion`,comprobante.`fecha_entrega` AS fecha_global_entrega, comprobante_item.`dias` dias_item , orden_armado.id AS nro_orden_armado, producto.`codigo`,estado.`d_estado` AS estado_oa, orden_armado.`cantidad` cant_oa, comprobante_item.`cantidad` AS cant_ped,
comprobante_item.`n_item`,
comprobante_item.`item_observacion`,orden_armado.`observacion` AS obs_oa,orden_armado.`fecha_entrega`,
orden_armado.`fecha_cierre` AS fecha_cierre_orden_armado,
orden_armado.fecha_cumplida AS fecha_cumplida_oa

FROM comprobante_item 

JOIN comprobante ON comprobante.id = comprobante_item.id_comprobante
JOIN orden_armado ON orden_armado.`id_pedido_interno` = comprobante.`id`
JOIN producto ON producto.id = comprobante_item.`id_producto`
JOIN estado ON estado.id = orden_armado.`id_estado`
JOIN persona ON comprobante.`id_persona` = persona.id

WHERE id_tipo_comprobante=202 AND id_estado_comprobante=1
AND orden_armado.`id_producto` = comprobante_item.`id_producto`

ORDER BY razon_social,nro_pedido,n_item
LIMIT 100000000