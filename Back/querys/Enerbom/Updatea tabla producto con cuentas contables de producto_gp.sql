


UPDATE valuarit_enerbom_test.`producto` 
JOIN ebsasql.`producto_gp` ON producto_gp.codigo = producto.codigo
SET id_cuenta_contable_compra = ebsasql.`producto_gp`.`id_ctacpras`
WHERE id_ctacpras IS NOT NULL AND id_ctacpras>0;

UPDATE valuarit_enerbom_test.`producto` 
JOIN ebsasql.`producto_gp` ON producto_gp.codigo = producto.codigo
SET id_cuenta_contable_venta = ebsasql.`producto_gp`.`id_ctavtas`
WHERE id_ctavtas IS NOT NULL AND id_ctavtas>0;