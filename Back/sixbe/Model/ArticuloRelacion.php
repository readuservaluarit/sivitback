<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class ArticuloRelacion extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'producto_relacion';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
       
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
        'ArticuloPadre' => array(
            'className' => 'Articulo',
            'foreignKey' => 'id_producto_padre',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ArticuloHijo' => array(
            'className' => 'Articulo',
            'foreignKey' => 'id_producto_hijo',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
   );
   
   
   
    public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     "codigo_hijo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_producto_hijo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "costo_unitario_hijo"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     "costo_hijo"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     "id_referencia"=>array("type"=>EnumTipoDato::biginteger,"show_grid"=>0,"default"=>null,"length"=>null),
     "ArticuloPadre"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null),
     "codigo_producto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_producto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "id_producto"=>array("type"=>EnumTipoDato::biginteger,"show_grid"=>0,"default"=>null,"length"=>null),
    
     );
     
     }
     
     
   
     
  public function afterFind($results, $primary = false) {
    
     App::uses('Modulo', 'Model');
     $modulo_obj = new Modulo();
     $cantidad_decimales = $modulo_obj->getCantidadDecimalesFormateo(EnumModulo::STOCK); 
    
    foreach ($results as $key => $val) {
        
        if (isset($val['ArticuloRelacion']['cantidad_hijo'])) {
            //$results[$key]['ArticuloRelacion']['cantidad_hijo'] = (string)  round($results[$key]['ArticuloRelacion']['cantidad_hijo'],$cantidad_decimales);
    }
    
  
    }
    

    return $results;
}     

}
