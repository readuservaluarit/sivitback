<?php

         include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');
		 
		$model["n_item"]["show_grid"] = 1;
        $model["n_item"]["header_display"] = "Nro. Item";
        $model["n_item"]["order"] = "0";
        $model["n_item"]["width"] = "3";
		$model["n_item"]["read_only"] = "0";
		$model["n_item"]["text_colour"] = "#0000AA"; //BLUE
		 
        $model["dias"]["show_grid"] = 1;
        $model["dias"]["header_display"] = "Dias Entr.";
        $model["dias"]["order"] = "1";
		$model["dias"]["width"] = "3";
		$model["dias"]["read_only"] = "0";
		
	
        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "[C&oacute;digo]";
        $model["codigo"]["order"] = "2";
		$model["codigo"]["width"] = "12";
		$model["codigo"]["read_only"] = "1";//AHROA SE PUEDE HACER DOBLE CLICK
		$model["codigo"]["picker"] = EnumMenu::mnuArticulosGeneral;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
        
        $model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "[Descripci&oacute;n]";
        $model["d_producto"]["order"] = "3";
		$model["d_producto"]["width"] = "15";
		$model["d_producto"]["read_only"] = "1";
		$model["d_producto"]["picker"] = EnumMenu::mnuArticulosGeneral;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
        
		$model["item_observacion"]["show_grid"] = 1;
		$model["item_observacion"]["type"] = "string";
        $model["item_observacion"]["header_display"] = "Item Obs.";
        $model["item_observacion"]["order"] = "4";
        $model["item_observacion"]["width"] = "12";
		$model["item_observacion"]["read_only"] = "0";
		
		$model["cantidad"]["show_grid"] = 1;
        $model["cantidad"]["header_display"] = "Cant.";
        $model["cantidad"]["order"] = "5";
        $model["cantidad"]["width"] = "4";
		$model["cantidad"]["read_only"] = "0";
        $model["cantidad"]["min_value"] = "0"; 
		$model["cantidad"]["text_colour"] = "#0000FF"; //BLUE
		
		$model["id_unidad"]["show_grid"] = 1;
        $model["id_unidad"]["header_display"] = "Unidad";
        $model["id_unidad"]["order"] = "6";
        $model["id_unidad"]["width"] = "5";
		$model["id_unidad"]["read_only"] = "0";
		$model["id_unidad"]["text_colour"] = "#F0000F";
		
		
		
		$model["precio_unitario_bruto"]["show_grid"] = 1;
        $model["precio_unitario_bruto"]["header_display"] = "Precio U. Bruto";
        $model["precio_unitario_bruto"]["order"] = "8";
        $model["precio_unitario_bruto"]["width"] = "8";
		$model["precio_unitario_bruto"]["read_only"] = "0";
		$model["precio_unitario_bruto"]["text_colour"] = "#C0000C";
		
		$model["descuento_unitario"]["show_grid"] = 1;
        $model["descuento_unitario"]["header_display"] = "Desc. Unitario";
        $model["descuento_unitario"]["order"] = "9";
        $model["descuento_unitario"]["width"] = "4";
		$model["descuento_unitario"]["read_only"] = "0";
		$model["descuento_unitario"]["text_colour"] = "#C0000C";
		
		$model["precio_unitario"]["show_grid"] = 1;
        $model["precio_unitario"]["header_display"] = "Precio Unitario";
        $model["precio_unitario"]["order"] = "10";
        $model["precio_unitario"]["width"] = "8";
		$model["precio_unitario"]["read_only"] = "1";
		$model["precio_unitario"]["text_colour"] = "#C0000C";
		
		$model["cantidad_pendiente_a_remitir"]["show_grid"] = 1;
        $model["cantidad_pendiente_a_remitir"]["header_display"] = "Pendiente Rmto.";
        $model["cantidad_pendiente_a_remitir"]["order"] = "11";
        $model["cantidad_pendiente_a_remitir"]["width"] = "4";
		$model["cantidad_pendiente_a_remitir"]["text_colour"] = "#004400";
		$model["cantidad_pendiente_a_remitir"]["read_only"] = "1";

		$model["cantidad_recibida_irm"]["show_grid"] = 1;
        $model["cantidad_recibida_irm"]["header_display"] = "Recibido IRM";
        $model["cantidad_recibida_irm"]["order"] = "11";
        $model["cantidad_recibida_irm"]["width"] = "4";
		$model["cantidad_recibida_irm"]["text_colour"] = "#004400";
		$model["cantidad_recibida_irm"]["read_only"] = "1";
		
		$model["cantidad_pendiente_irm"]["show_grid"] = 1;
        $model["cantidad_pendiente_irm"]["header_display"] = "Pendiente IRM";
        $model["cantidad_pendiente_irm"]["order"] = "12";
        $model["cantidad_pendiente_irm"]["width"] = "4";
		$model["cantidad_pendiente_irm"]["text_colour"] = "#F0000F";
		$model["cantidad_pendiente_irm"]["read_only"] = "1";
        
        $model["cantidad_pendiente_a_facturar"]["show_grid"] = 1;
        $model["cantidad_pendiente_a_facturar"]["header_display"] = "Cant. Pend FC";
        $model["cantidad_pendiente_a_facturar"]["order"] = "13";
        $model["cantidad_pendiente_a_facturar"]["width"] = "4";
        $model["cantidad_pendiente_a_facturar"]["text_colour"] = "#F0000F";
		$model["cantidad_pendiente_a_facturar"]["read_only"] = "1";
		
		$model["id_iva"]["show_grid"] = 1;
        $model["id_iva"]["header_display"] = "IVA";
        $model["id_iva"]["order"] = "14";
        $model["id_iva"]["width"] = "6";
		$model["id_iva"]["read_only"] = "0";
		
		$model["requiere_conformidad"]["show_grid"] = 1;
        $model["requiere_conformidad"]["header_display"] = "Req. Conformidad";
        $model["requiere_conformidad"]["order"] = "15";
        $model["requiere_conformidad"]["width"] = "6";
		$model["requiere_conformidad"]["read_only"] = "0";
		// $model["requiere_conformidad"]["length"] = "12";
		// $model["requiere_conformidad"]["value_length"] = "12";
		// $model["requiere_conformidad"]["type"] = "string"; //HAGO Q ESTA CENTRADo el checkbox ()
        
		$output = array(
						"status" =>EnumError::SUCCESS,
						"message" => "ComprobanteItem",
						"content" => $model
						);
						
        echo json_encode($output);
?>