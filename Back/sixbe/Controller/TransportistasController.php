<?php

App::uses('PersonasController', 'Controller');


class TransportistasController extends PersonasController {
    public $name = 'Transportistas';
    public $model = 'Persona';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $id_tipo_persona = EnumTipoPersona::Transportista;
    
  
  
    /**
    * @secured(CONSULTA_TRANSPORTISTA)
    */
    public function index() {
        
        
        $this->request->data['Persona']['id_tipo_persona'] = $this->id_tipo_persona;
        parent::index();
        
    }
    
    /**
    * @secured(MODIFICACION_TRANSPORTISTA)
    */
    public function abm($mode, $id = null) {
        
        parent::abm($mode,$id);
        
    }

    /**
    * @secured(ABM_TRANSPORTISTA)
    */
    public function view($id) {        
        parent::view($id);
    }

    /**
    * @secured(ADD_TRANSPORTISTA)
    */
    public function add() {
    
        $this->request->data["Persona"]["id_tipo_persona"] = $this->id_tipo_persona;
        parent::add(); 
        
      
    }
   
   
   /**
    * @secured(MODIFICACION_TRANSPORTISTA)
    */ 
   public function edit($id) {
        
        parent::edit($id);      
  }
    
  
    /**
    * @secured(BAJA_TRANSPORTISTA)
    */
    function delete($id) {
        
        parent::delete($id);
        
    }
    
  
    
    
    public function home(){
        parent::home();
    }
    
  
    /**
    * @secured(CONSULTA_TRANSPORTISTA)
    */
    public function existe_cuit(){
        
       parent::existe_cuit();
    }
    
    
     /**
    * @secured(CONSULTA_TRANSPORTISTA)
    */
     public function existe_codigo($codigo='',$id_persona='',$llamada_interna=0){
        
       $dato = parent::existe_codigo($codigo,$id_persona,$llamada_interna);
       if($llamada_interna == 1)
        return $dato;
       else
        return;
    }
    
     /**
    * @secured(CONSULTA_TRANSPORTISTA)
    */
    public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

    }
    
    
    
   
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
            //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);

            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);
    }
  
}
?>