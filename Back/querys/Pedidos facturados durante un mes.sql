
SELECT comprobante.`nro_comprobante`,comprobante.`razon_social`,comprobante_item.`n_item`,producto.`codigo`,comprobante.`fecha_generacion`,comprobante_item.`dias`,
factura.`fecha_contable` AS fecha_factura,@start :=comprobante_item.id,
 CAST((`comprobante`.`fecha_generacion` + INTERVAL IF(ISNULL(`comprobante_item`.`dias`),0,`comprobante_item`.`dias`) DAY) AS DATE) AS fecha_entrega_item_pi

,

(SELECT comprobante_item.id FROM comprobante_item 

JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`
WHERE id_comprobante_item_origen = @start
AND id_tipo_comprobante =202
AND comprobante_item.`activo`=1
)/*Chequea y te da si se hizo un remito o sea la fecha*/

 FROM comprobante_item

JOIN comprobante ON comprobante.`id` = comprobante_item.`id_comprobante`

LEFT JOIN comprobante_item fact_item ON fact_item.`id_comprobante_item_origen` = comprobante_item.`id`

LEFT JOIN comprobante factura ON factura.id = fact_item.`id_comprobante`

JOIN producto ON producto.id  = comprobante_item.`id_producto`


WHERE comprobante.id_tipo_comprobante=202

AND comprobante_item.`activo` = 1

AND factura.`id_tipo_comprobante` = 801
AND factura.fecha_contable>='2018-09-01'
AND factura.`cae`>0

LIMIT 100000