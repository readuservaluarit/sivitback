<?php
App::uses('ComprobanteImpuestosController', 'Controller');


  
class RetencionesAjenasController extends ComprobanteImpuestosController {
    
    public $name = 'RetencionesAjenas';
    public $model = 'ComprobanteImpuesto';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $requiere_impuestos = 0;

    
    /**
    * @secured(CONSULTA_RETENCION_AJENA)
    */
    public function index() {
        
        
        
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->loadModel("Comprobante");
        
        
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros($this->model);
        
        
        array_push($conditions, array("Impuesto.id_tipo_impuesto"=>EnumTipoImpuesto::RETENCIONES));
        
        array_push($conditions, array("Impuesto.id_sistema"=>EnumSistema::VENTAS));
        array_push($conditions, array("Comprobante.id_tipo_comprobante"=>$this->Comprobante->getTiposComprobanteController(EnumController::Recibos) ));
        
        
        
        
            
            
                       
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>array('Comprobante'=>array('Persona','EstadoComprobante','Moneda','TipoComprobante'),"Impuesto"),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.id desc'
        ); //el contacto es la sucursal
        
     
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
            
         
             if(isset($valor["Comprobante"]['TipoComprobante'])){
             	$valor[$this->model]['d_tipo_comprobante'] = $valor["Comprobante"]['TipoComprobante']['d_tipo_comprobante'];
             	
             	$valor[$this->model]['codigo_tipo_comprobante'] = $valor["Comprobante"]['TipoComprobante']['codigo_tipo_comprobante'];
             	unset($valor["Comprobante"]['TipoComprobante']); 
             }
             
             if(isset($valor['Impuesto'])){
             	$valor[$this->model]['d_impuesto'] = $valor["Impuesto"]['d_impuesto'];
           
             }
             
             
             
             $valor[$this->model]['nro_comprobante'] = (string) str_pad($valor["Comprobante"]['nro_comprobante'], 8, "0", STR_PAD_LEFT);
             $valor[$this->model]['fecha_contable'] = $valor["Comprobante"]['fecha_contable'];
             
             
             
             
             $valor[$this->model]['pto_nro_comprobante'] =  (string)  $this->Comprobante->GetNumberComprobante($valor["Comprobante"]['id_punto_venta'],str_pad($valor["Comprobante"]['nro_comprobante'], 8, "0", STR_PAD_LEFT));
             $valor[$this->model]['pto_nro_comprobante_retencion'] =  (string)  $this->Comprobante->GetNumberComprobante($valor["ComprobanteImpuesto"]['d_punto_venta'],str_pad($valor["ComprobanteImpuesto"]['d_comprobante_impuesto'], 8, "0", STR_PAD_LEFT));
             
             /*
             foreach($valor[$this->model]['ComprobanteItem'] as &$producto ){
                 
                 $producto["d_producto"] = $producto["Producto"]["d_producto"];
                 $producto["codigo_producto"] = $producto["Producto"]["codigo"];
                
                 
                 unset($producto["Producto"]);
             }
             */
           
             if(isset($valor["Comprobante"]['Moneda'])){
                 $valor[$this->model]['d_moneda'] = $valor["Comprobante"]['Moneda']['d_moneda'];
                 unset($valor["Comprobante"]['Moneda']);
             }
             
             if(isset($valor["Comprobante"]['EstadoComprobante'])){
             	$valor[$this->model]['d_estado_comprobante'] = $valor["Comprobante"]['EstadoComprobante']['d_estado_comprobante'];
             	unset($valor["Comprobante"]['EstadoComprobante']);
             }
            
           
          
             if(isset($valor["Comprobante"]['Persona'])){
            
            
             	if($valor["Comprobante"]['Persona']['id']== null){
                     
             		$valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social']; 
             		$valor[$this->model]['codigo_persona'] =  "";
                 }else{
                     
                 	$valor[$this->model]['razon_social'] = $valor["Comprobante"]['Persona']['razon_social'];
                 	$valor[$this->model]['codigo_persona'] = $valor["Comprobante"]['Persona']['codigo'];
                 }
                 
                 unset($valor["Comprobante"]['Persona']);
             }
             
             


             
             
             unset($valor["Comprobante"]);
             unset($valor["Impuesto"]);
            
            
        }
        
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
  
        
        
        
    //fin vista json
        
    }
    
   

    /**
    * @secured(ADD_RETENCION_AJENA)
    */
    public function add(){
        

     parent::add();    
        
    }
    
    
    /**
    * @secured(MODIFICACION_RETENCION_AJENA)
    */
    public function edit($id){
    
   
     parent::edit($id);    
        
    }
    
    
    /**
    * @secured(BAJA_RETENCION_AJENA)
    */
    public function delete($id){
        
     parent::delete($id);    
        
    }
    
   /**
    * @secured(BAJA_RETENCION_AJENA)
    */
    public function deleteItem($id,$externo=1){
        
        parent::deleteItem($id,$externo);    
        
    }
    
    
      /**
    * @secured(CONSULTA_RETENCION_AJENA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
  
    /**
    * @secured(REPORTE_RETENCION_AJENA)
    */
    public function pdfExport($id,$vista=''){
           
            parent::pdfExport($id);  
    
            
       
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
    }
    
    
    /**
     * @secured(CONSULTA_RETENCION_AJENA)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Retenciones Sufridas");
    	
    	
    	
    }
}
?>