<?php
App::uses('AppModel', 'Model');

class ComprobanteItemLote extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	
	public $useTable = 'comprobante_item_lote';
	

    public $actsAs = array('Containable');
    
    // public $virtualFields = array(
    // 'nro_comprobante_origen' => 'nro_comprobante'
// );
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

public $belongsTo = array(
        'ComprobanteItem' => array(
            'className' => 'ComprobanteItem',
            'foreignKey' => 'id_comprobante_item',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Lote' => array(
            'className' => 'Lote',
            'foreignKey' => 'id_lote',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
 );

public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
	
	$this->virtual_fields_view = array(
			
			"orden"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
			"codigo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			"pto_nro_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			"pto_nro_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			"d_lote"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			"fecha_lote"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
			"d_tipo_lote"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			"d_tiene_certificado"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			"d_validado"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null)
			// "ComprobanteItem"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null),
			// "Detalle"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null),
			// "Lotes"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null),
		);
	}
}
