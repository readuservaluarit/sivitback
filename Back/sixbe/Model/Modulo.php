<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class Modulo extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'modulo';

    public $actsAs = array('Containable');
	
	// public $virtualFields = array(
    // 'd_estado' => '');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Your custom message here',
                'allowEmpty' => false,
                'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),

	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 

 
  public function getModulos(){
      
    
    $resultados = $this->find('all', array());
   
  
  
   $array = array();
   $array2 = array();
   
   foreach($resultados as $dato){
       
       array_push($array,$dato["Modulo"]);
       
   }
   
   
  return $array2["Modulo"] = $array;
   
      
  }
  
  public function estaHabilitado($id_modulo){
      
      
      App::uses('CakeSession', 'Model/Datasource');
      $modulos = CakeSession::read('Modulo');
      
      foreach($modulos as $modulo){
          
          if($modulo["id"]== $id_modulo)
            return  $modulo["habilitado"];
      }
      
      return 0;//sino encontre el modulo no esta habilitado
      
      
  }
  
  
  public function getCantidadDecimalesFormateo($id_modulo){
      
     App::uses('CakeSession', 'Model/Datasource');
      $modulos = CakeSession::read('Modulo');
      
      foreach($modulos as $modulo){
          
          if($modulo["id"]== $id_modulo)
            return  $modulo["cantidad_decimales_formateo"];
      }  
  }
}
