<?php

App::uses('DetallesTipoComprobanteController', 'Controller');

class ConceptosCajaController extends DetallesTipoComprobanteController {
    public $name = 'ConceptosCaja';
    public $model = 'DetalleTipoComprobante';
    public $helpers = array ( 'Paginator', 'Js');
    public $components = array('Paginator', 'RequestHandler');
  
    /**
    * @secured(CONSULTA_CONCEPTO_CAJA)
    */
    public function index() {
        
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        
        $conditions = $this->RecuperoFiltros($this->model);

   
    
        $array_conditions = array('TipoComprobante','CuentaContable');
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            /* 'joins' => array(
                array(
                    'table' => 'persona',
                    'alias' => 'Persona',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Persona.id = Comprobante.id_persona'
                    )
                )
                ),*/
            'conditions' => $conditions,
            'contain' => $array_conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
        	'order'=> 'DetalleTipoComprobante.d_detalle_tipo_comprobante ASC'
        
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
    }
    else{ // vista json    
          
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];

         
         //parseo el data para que los models queden dentro del objeto de respuesta
         foreach($data as &$valor){
             
             $this->cleanforOutput($valor);
             
             
            
            
        }
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }


    

	
	
    /**
    * @secured(ADD_CONCEPTO_CAJA)
    */
    public function add() {
       
       
        
    
            
            
        parent::add();
        
  
      
    }
    

    
    /**
    * @secured(MODIFICACION_CONCEPTO_CAJA)
    */
    public function edit($id) {
    
       
        parent::edit($id);
        return;  
		
    }
    
  
     /**
    * @secured(BAJA_CONCEPTO_CAJA, READONLY_PROTECTED)
    */
   public function delete($id){
        
     parent::delete($id);    
        
    }
    
    
     private function cleanforOutput(&$valor){
         
         
       
        if(isset($valor['TipoComprobante'])){
            $valor['DetalleTipoComprobante']['codigo_tipo_comprobante'] =$valor['TipoComprobante']['codigo_tipo_comprobante'];
        }
         
        if(isset($valor['CuentaContable'])){
            $valor['DetalleTipoComprobante']['d_cuenta_contable'] = $valor['CuentaContable']['codigo'].' '.$valor['CuentaContable']['d_cuenta_contable'];
        }
      
        
        unset($valor['TipoComprobante']);  
        unset($valor['CuentaContable']);  
            
           
     
     
     }
     
     
      /**
    * @secured(CONSULTA_CONCEPTO_CAJA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
      
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        
        
        
         $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);          
    }
    
    
  
    
   
    
    
   
}
?>