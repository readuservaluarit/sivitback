<!doctype html>
<html lang="es">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
	<title>Reporte SIVIT</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" >
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    
  
    

       <?php echo $this->Html->css('jqueryui/jquery-ui-1.9.2.custom.css');?>  


      
       <?php echo $this->Html->css('font-awesome/css/font-awesome.min.css');?>
       
       
       
        
    
      <?php echo $this->Html->script('jquery/jquery-1.8.3.min.js');?>
      <?php echo $this->Html->script('jquery/jquery.js');?>
      <?php echo $this->Html->script('jquery/jquery-ui-1.9.2.custom.js');?>
      
      
        <?php
            echo $this->Html->script('fancybox/jquery.fancybox.js?v=2.1.3');
            echo $this->Html->css('fancybox/jquery.fancybox.css?v=2.1.2');

            echo $this->Html->css('fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5');
            echo $this->Html->script('fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5');

            echo $this->Html->css('fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7');
            echo $this->Html->script('fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7');

            echo $this->Html->script('fancybox/helpers/jquery.fancybox-media.js?v=1.0.5');
    
        
        ?>
        
      
       <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    
    
    <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet">
    <script  src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script  src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script  src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    
      <link href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css" rel="stylesheet">
      <script  src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
    
    <?php
    
      /*Validator*/
            echo $this->Html->script('validator.js');
            
            /*Solo Numeros*/
            echo $this->Html->script('mylibs/numericInput.min.js');
            
    ?>



    


  <?php echo $this->Html->script('Chart.js/dist/Chart.min.js');?>
 <?php
 
    /*jQuery datepicker espaniol*/
            echo $this->Html->script('mylibs/jquery.ui.datepicker-es.js'); //jQuery datepicker espaniol
            echo $this->Html->script('mylibs/jquery-ui-timepicker-addon.js'); //jQuery timepicker
            
            ?>
  <?php echo $this->Html->css('dashboard/style.blue.css');?>
 <?php echo $this->Html->css('dashboard/custom.css');?>
 <?php echo $this->Html->css('rightsidebar/styles.css');?> 	
</head>


<?php

App::import('Model', 'Modulo');
App::import('Model', 'DatoEmpresa');
App::import('Lib', 'SecurityManager');
$modulo_obj = new Modulo();
$datoempresa_obj = new DatoEmpresa();


$sm = new SecurityManager();
         

         


?>



<body>
     <div id="main-content">
<div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <nav class="side-navbar shrinked">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar">
            <a href="<?php echo $datoempresa_obj->getAppPath();?>RightSidebar">
            <?php echo $this->Html->image(Configure::read("desktopLogo"), array('plugin' => false, 'alt' => 'App Logo', 'class' => 'img-fluid rounded-circle'));?>
            
            </a>
            </div>
            <div class="title">
              <h1 class="h4"><?php echo AuthComponent::user('nombre');?>
                
              </h1>

            </div>
          </div>
          <!-- Sidebar Navidation Menus--><span class="heading"><!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span>>><span></span></a></span>
          <ul class="list-unstyled">
                    <li class="<?php if($this->request->params['controller'] == 'RightSidebar') echo "active" ?>"><a href="<?php echo $datoempresa_obj->getAppPath();?>/RightSidebar"> <i class="fa fa-home"></i>Home </a></li>
                 
  
                   
                  <?php if(($modulo_obj->estaHabilitado(EnumModulo::SIVITCLIENTES) == 1 || $modulo_obj->estaHabilitado(EnumModulo::SIVITPROVEEDORES) == 1 || $modulo_obj->estaHabilitado(EnumModulo::MELI) == 1) 
                  		  && $sm->permiso_particular('MODIFICACION_DATOSEMPRESA',AuthComponent::user('username')) >=1
                  
                     ){ ?>
  					 <li class='<?php if($this->request->params['controller'] == 'DatosEmpresa') echo "active" ?>' ><a href="<?php echo $datoempresa_obj->getAppPath();?>/DatosEmpresa/abm/M/1"> <i class="fa fa-cogs"></i>Configuraci&oacute;n Web </a></li>
				<?php }?>
<!--
                    <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>Example dropdown </a>
                      <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                        <li><a href="<?php echo $datoempresa_obj->getAppPath();?>/Facturas">Facturas</a></li>
                        <li><a href="#">Page</a></li>
                        <li><a href="#">Page</a></li>
                      </ul>
                    </li>
                    
                    -->
                    
             
     
          </ul><span class="heading">Extras</span>
          <ul class="list-unstyled">
            <li class='<?php if($this->request->params['controller'] == 'MisDatos') echo "active" ?>' > <a href="<?php echo $datoempresa_obj->getAppPath();?>/MisDatos/MisDatosPanel"> <i class="fa fa-user"></i>Mi perfil </a></li>
           
          </ul>
          
          
         <?php 
         
         if($sm->permiso_particular('MODIFICACION_CLIENTE_PEDIDO_INTERNO',AuthComponent::user('username')) >=1){?>
           <ul class="list-unstyled">
          
            <li class='<?php if($this->request->params['controller'] == 'PedidosInternos') echo "active" ?>' > <a href="<?php echo $datoempresa_obj->getAppPath();?>/PedidosInternos/CambiarRazonSocial"> <i class="fa fa-edit"></i>Pedidos </a></li>
           
          </ul>
          
        <?php }  ?>
          
        </nav>
        <div class="content-inner">
        
          <!-- Page Header-->
         <div class="alert alert-success" role="alert" id="alertSuccess" style="display:none;">
  			Se ha grabado correctamente
		</div>
		
		<div class="alert alert-danger" role="alert" id="alertError" style="display:none;">
  Ha ocurrido un error, no pudo grabarse
		</div>
      

     <?php echo $this->Session->flash(); ?>
     

     <?php echo $this->fetch('content'); ?>
     
          
        </div>
      </div>
      
      </div>
</body>

<script>
$('#toggle-btn').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');

        $('.side-navbar').toggleClass('shrinked');
        $('.content-inner').toggleClass('active');
        $(document).trigger('sidebarChanged');

        if ($(window).outerWidth() > 1183) {
            if ($('#toggle-btn').hasClass('active')) {
                $('.navbar-header .brand-small').hide();
                $('.navbar-header .brand-big').show();
            } else {
                $('.navbar-header .brand-small').show();
                $('.navbar-header .brand-big').hide();
            }
        }

        if ($(window).outerWidth() < 1183) {
            $('.navbar-header .brand-small').show();
        }
    });
    
     function showProcessing(show){

                if(show)
                    $("#zk_proc").css("display","block"); 
                else
                    $("#zk_proc").css("display","none");

            }
            
            function showSuccess(msg){
                $('#alertSuccess').html(msg);
                $('#alertSuccess').fadeIn(1000, function() {
                // Animation complete
                     $('#alertSuccess').fadeOut(8000, function() {
                    // Animation complete
                    });
                });
            }
            
            function showError(msg){
                $('#alertError').html(msg);
                $('#alertError').fadeIn(1000, function() {
                // Animation complete
                     $('#alertError').fadeOut(8000, function() {
                    // Animation complete
                    });
                });
            }
            
            
$(".side-navbar").css("height",screen.height-108);//seteo la barra izquierda con toda la resolucion de la pantalla
          
</script>





</html>



