<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
  //$this->layout = 'error_layout';
  
     if ($this->RequestHandler->ext != 'json'){
  
?>

<style>
  .center {text-align: center; margin-left: auto; margin-right: auto; margin-bottom: auto; margin-top: auto;}
</style>
<title>
<?php echo $name; ?>
</title>
<body>
  <div class="hero-unit center">
    <h2>Recurso no encontrado</h2>
    <br />
    <p>No se puede acceder a la página solicitada.</p>
    <!--<strong><?php echo $url;?></strong>-->
    <p><b>Por favor verifique que la url ingresada sea correcta, muchas gracias.</b></p>
    <!--<p><b>Error 500 - <?php echo date("d-m-Y H:i:s") . ' ' . $this->here; ?></b></p>-->
    <?php 
        echo $this->Html->link("<i class='icon-home icon-white'></i> Ir al Inicio" , array('controller' => 'Usuarios', 'action' => 'home'), array('class' => 'btn btn-large btn-primary', 'escape' => false)); 
    ?>
  </div>
  <br />
  
<?php
if (Configure::read('debug') > 0 ):
    echo $this->element('exception_stack_trace');
endif;
}
?>