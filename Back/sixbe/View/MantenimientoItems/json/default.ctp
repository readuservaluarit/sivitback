<?php   
        include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');   
        
        $model["n_item"]["show_grid"] = 1;
        $model["n_item"]["header_display"] = "Nro. Item";
        $model["n_item"]["order"] = "0";
        $model["n_item"]["width"] = "10";
        
        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "Codigo";
        $model["codigo"]["order"] = "1";
        $model["codigo"]["width"] = "10";
		$model["codigo"]["read_only"] = "1";//AHROA SE PUEDE HACER DOBLE CLICK
		$model["codigo"]["picker"] = EnumMenu::mnuArticulosGeneral;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
		
		$model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "[Descripci&oacute;n]";
        $model["d_producto"]["order"] = "2";
		$model["d_producto"]["width"] = "15";
		$model["d_producto"]["read_only"] = "1";
		$model["d_producto"]["picker"] = EnumMenu::mnuArticulosGeneral;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
        
        $model["id_unidad"]["show_grid"] = 1;
        $model["id_unidad"]["header_display"] = "Unidad";
        $model["id_unidad"]["order"] = "3";
        $model["id_unidad"]["width"] = "5";
		$model["id_unidad"]["read_only"] = "0";
		$model["id_unidad"]["text_colour"] = "#F0000F";
		
        $model["item_observacion"]["show_grid"] = 1;
        $model["item_observacion"]["header_display"] = "Item Observ.";
        $model["item_observacion"]["order"] = "4";
        $model["item_observacion"]["width"] = "20";
        $model["item_observacion"]["read_only"] = "0";
        
        $model["cantidad"]["show_grid"] = 1;
        $model["cantidad"]["header_display"] = "Cantidad";
        $model["cantidad"]["order"] = "5";
        $model["cantidad"]["width"] = "5";
        
        
        $model["fecha_vencimiento"]["show_grid"] = 1;
        $model["fecha_vencimiento"]["header_display"] = "Fcha Venc.";
        $model["fecha_vencimiento"]["order"] = "6";
        $model["fecha_vencimiento"]["width"] = "20";
        $model["fecha_vencimiento"]["read_only"] = "0";
        		
       
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItem",
                    "content" => $model
                );
        echo json_encode($output);
?>