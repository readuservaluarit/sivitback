<?php
App::uses('AppModel', 'Model');
App::uses('AsientoCuentaContable', 'Model');
App::uses('AsientoCuentaContableCentroCostoItem', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class CentroCosto extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'centro_costo';
    public $actsAs = array('Containable');
	
	public $virtualFields = array(
    'd_codigo_centro_costo' => 'CONCAT(CentroCosto.codigo_centro_costo, " " , CentroCosto.d_centro_costo)');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'd_centro_costo' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar un Nombre.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
            'unico' => array(
            'rule' => array('isUnique'),
            'message' => 'El centro de costo ya existe debe escribir uno que no exista'
        )
		),
        
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	/*public $hasMany = array(
		'Region' => array(
			'className' => 'Region',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public $childRecordMessage = "Existe por lo menos una regi&oacute;n, predio, establecimiento o usuario relacionado/a con la jurisdicci&oacute;n.";
    
    */
    
    
    
    public $belongsTo = array(
                        
                      
                       'Area' => array(
                                    'className' => 'Area',
                                    'foreignKey' => 'id_area',
                                    'dependent' => false,
                                    'conditions' => '',
                                    'fields' => '',
                          
                                )
        
    
                        );
                        
                        
    public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     "d_codigo_centro_costo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_area_centro_costo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
  
     
     );
     
     }                     
                        
                        
  public function ApropiacionPorAsiento($id_asiento){
      
    $AsientoCuentaContableCentroCostoItem = ClassRegistry::init('AsientoCuentaContable');  
      
    $items_asiento = $AsientoCuentaContableCentroCostoItem->find('all', array(
                                                'conditions' => array('AsientoCuentaContable.id_asiento' => $id_asiento),
                                                'contain' => array('CuentaContable'=>array('CuentaContableCentroCosto'))
                                                ));  
    $array_item_asiento_cuenta_contable_centro_costo = array();                                            
    foreach($items_asiento as $item){ //por cada item del asiento debo ver los centros de costo y distribuir ese monto
        
       $monto = $item["AsientoCuentaContable"]["monto"];
       
       if( $item["CuentaContable"]["utiliza_centro_costos"] == 1){//esta cuenta contable utiliza centros de costos debo distribuir ese monto
       
             
            foreach($item["CuentaContable"]["CuentaContableCentroCosto"] as $cuenta_contable_centro_costo){
                
                $auxiliar_cuenta_contable_centro_costo["id_asiento_cuenta_contable"] =  $item["AsientoCuentaContable"]["id"];
                $auxiliar_cuenta_contable_centro_costo["id_centro_costo"] =  $cuenta_contable_centro_costo["id_centro_costo"];
                $auxiliar_cuenta_contable_centro_costo["porcentaje"] =  $cuenta_contable_centro_costo["porcentaje"];
                $auxiliar_cuenta_contable_centro_costo["monto"] =  ($cuenta_contable_centro_costo["porcentaje"]/100)*$monto; //saco el % monto total que le corresponde al centro de costo
                
                array_push($array_item_asiento_cuenta_contable_centro_costo,$auxiliar_cuenta_contable_centro_costo);
                
                
                
                
                
            }
           
           
           
       } 
        
        
    } 
    
    if(count($array_item_asiento_cuenta_contable_centro_costo)>0){
        $AsientoCuentaContableCentroCostoItem = ClassRegistry::init('AsientoCuentaContableCentroCostoItem');
        $AsientoCuentaContableCentroCostoItem->saveAll($array_item_asiento_cuenta_contable_centro_costo); 
    }
                                                   
      
      
  }  
  
  
  public function getCentroCostoActivo(){
      
        
        $centro_costo = $this->find('all', array('conditions'=>array('CentroCosto.activo'=>1)));
        return $centro_costo;
  }                    
                        
}
