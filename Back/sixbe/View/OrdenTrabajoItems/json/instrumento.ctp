<?php 
         include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');
        
		$model["fecha_vencimiento"]["show_grid"] = 0;
		$model["fecha_vencimiento"]["type"] = "date";
        $model["fecha_vencimiento"]["header_display"] = "Fecha Vto.";
        $model["fecha_vencimiento"]["order"] = "3";
        $model["fecha_vencimiento"]["read_only"] = "1";
		$model["fecha_vencimiento"]["text_colour"] = "#990000";		
		
		$model["codigo_producto"]["show_grid"] = 1;
        $model["codigo_producto"]["header_display"] = "Codigo";
        $model["codigo_producto"]["order"] = "4";
		$model["codigo_producto"]["read_only"] = "1";
		$model["codigo_producto"]["picker"] = EnumMenu::mnuArticulosGeneral;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
		
		$model["d_fecha_vencimiento"]["show_grid"] = 1;
		$model["d_fecha_vencimiento"]["type"] = "string";
        $model["d_fecha_vencimiento"]["header_display"] = "Vencimiento";
        $model["d_fecha_vencimiento"]["order"] = "5";
        $model["d_fecha_vencimiento"]["read_only"] = "1";
		$model["d_fecha_vencimiento"]["text_colour"] = "#990000";
		
		$model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "Descripcion";
        $model["d_producto"]["order"] = "6";
		$model["d_producto"]["read_only"] = "1";
		$model["d_producto"]["picker"] = EnumMenu::mnuArticulosGeneral;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
		
		$model["item_observacion"]["show_grid"] = 1;
		$model["item_observacion"]["type"] = "string";
        $model["item_observacion"]["header_display"] = "Observacion";
        $model["item_observacion"]["order"] = "7";
		$model["item_observacion"]["read_only"] = "0";
		$model["item_observacion"]["text_colour"] = "#000000";
	
		$output = array(
			"status" =>EnumError::SUCCESS,
			"message" => "ComprobanteItem",
			"content" => $model
                );
        echo json_encode($output);
         
        

?>