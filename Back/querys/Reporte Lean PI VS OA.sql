SELECT 
  orden_armado.id AS nro_orden_armado,
  producto.`codigo`,
  comprobante_item.`item_observacion`,
  orden_armado.fecha_emision AS fecha_emision_OA,
  orden_armado.`cantidad` cant_oa,
  orden_armado.`observacion` AS obs_oa,
  orden_armado.`fecha_entrega` AS Fecha_entrega_compo_armado,
  orden_armado.`fecha_cumplida`  AS orden_armado_completa,
  ADDDATE(ADDDATE(comprobante.`fecha_generacion`, INTERVAL comprobante_item.`dias` DAY), INTERVAL -2 DAY) AS fecha_limite_oa,
  comprobante.`nro_comprobante` AS nro_pedido,
  persona.`razon_social`,
  comprobante.`fecha_generacion` AS fecha_generacion_pedido,
  comprobante.`fecha_entrega` AS fecha_global_entrega,
  comprobante_item.`dias` dias_item,
  ADDDATE(comprobante.`fecha_generacion`, INTERVAL comprobante_item.`dias` DAY) AS fecha_entrega_item_pi,
  estado.`d_estado` AS estado_oa,
  comprobante_item.`cantidad` AS cant_ped,
  comprobante_item.`n_item`,
  usuario.`nombre`
  
FROM
  comprobante_item 
  JOIN comprobante 
    ON comprobante.id = comprobante_item.id_comprobante 
  JOIN orden_armado 
    ON orden_armado.`id_pedido_interno` = comprobante.`id` 
  JOIN producto 
    ON producto.id = comprobante_item.`id_producto` 
  JOIN estado 
    ON estado.id = orden_armado.`id_estado` 
  JOIN persona 
    ON comprobante.`id_persona` = persona.id 
   JOIN usuario
   ON usuario.id = comprobante.`id_usuario`
WHERE id_tipo_comprobante = 202 
  AND id_estado_comprobante = 1 
  AND orden_armado.`id_producto` = comprobante_item.`id_producto` 
  AND orden_armado.`id_estado` = 6
ORDER BY   orden_armado.id ASC
LIMIT 100000000 