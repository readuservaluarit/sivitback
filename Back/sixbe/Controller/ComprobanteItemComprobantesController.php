<?php

    App::uses('ComprobanteItemsController', 'Controller');


class ComprobanteItemComprobantesController extends ComprobanteItemsController {
    public $name = 'ComprobanteItemComprobantes';
    public $model = 'ComprobanteItemComprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
	public function index() {
        
		
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 100000, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model);
    
        
        /*Borro Filtros que no van*/
        foreach($conditions as $key=>$condition){
        	
        foreach($condition as $key2 =>$valor_key){	
        	if($key2 == "Producto.id_producto_tipo"){
        		unset($conditions[$key]);
        	break;
        	}
        }
        	
        	
        }
        
      
        
        array_push($conditions, array($this->model.'.id_comprobante_origen >'=> 0)); //solo trae las visibles
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
        		'joins' => array(
								array(
										'table' => 'tipo_comprobante',
										'alias' => 'TipoComprobante',
										'type' => 'LEFT',
										'conditions' => array(
												'TipoComprobante.id = Comprobante.id_tipo_comprobante'
										),
								)
						),
            'contain'=>array('ComprobanteOrigen'=>array('PuntoVenta','TipoComprobante'),'Comprobante'=>array('TipoComprobante','PuntoVenta','EstadoComprobante')),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
		//Headers
        $formBuilder->addHeader('id', 'cotizacion.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'cotizacion.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$producto ){
                 //producto["ComprobanteItemComprobante"]["d_producto"] = $producto["Producto"]["d_producto"];
                 $producto["ComprobanteItemComprobante"]["precio_unitario"] = 		 		$producto["ComprobanteItemComprobante"]["precio_unitario"];
                 $producto["ComprobanteItemComprobante"]["nro_comprobante_origen"] = 		$producto["ComprobanteOrigen"]["nro_comprobante"];
                 $producto["ComprobanteItemComprobante"]["d_tipo_comprobante_origen"] = 	$producto["ComprobanteOrigen"]["TipoComprobante"]["codigo_tipo_comprobante"];
                 $producto["ComprobanteItemComprobante"]["d_tipo_comprobante"] = 			$producto["Comprobante"]["TipoComprobante"]["codigo_tipo_comprobante"];
                 $producto["ComprobanteItemComprobante"]["d_estado_comprobante"] = 			$producto["Comprobante"]["EstadoComprobante"]["d_estado_comprobante"];
                 $producto["ComprobanteItemComprobante"]["fecha_contable_origen"] = 		$producto["ComprobanteOrigen"]["fecha_contable"];
                 $producto["ComprobanteItemComprobante"]["fecha_contable"] = 				$producto["Comprobante"]["fecha_contable"];
                 $producto["ComprobanteItemComprobante"]["fecha_vencimiento_origen"] = 		$producto["ComprobanteOrigen"]["fecha_vencimiento"];
                 $producto["ComprobanteItemComprobante"]["total_comprobante_origen"] = 		$producto["ComprobanteOrigen"]["total_comprobante"];
                 $producto["ComprobanteItemComprobante"]["subtotal_comprobante_origen"] = 	$producto["ComprobanteOrigen"]["subtotal_neto"];
                 
                 if(isset($producto["ComprobanteOrigen"]["PuntoVenta"]["numero"]))
                    $id_punto_venta_origen = $producto["ComprobanteOrigen"]["PuntoVenta"]["numero"];
                 else
                 	$id_punto_venta_origen= 0;
                 
                 if(isset($producto["Comprobante"]["PuntoVenta"]["numero"]))
                 	$id_punto_venta = $producto["Comprobante"]["PuntoVenta"]["numero"];
                 else
                 	$id_punto_venta = 0;
                    
                    
                 	$producto["ComprobanteItemComprobante"]["pto_nro_comprobante_origen"] = $this->ComprobanteItemComprobante->GetNumberComprobante($id_punto_venta_origen,$producto["ComprobanteOrigen"]["nro_comprobante"]);
                    
                 	$producto["ComprobanteItemComprobante"]["pto_nro_comprobante"] = $this->ComprobanteItemComprobante->GetNumberComprobante($id_punto_venta,$producto["Comprobante"]["nro_comprobante"]);

                 unset($producto["ComprobanteOrigen"]);
        }
        
        
        
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    protected function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            
            try{
                if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "El Item ha sido creado exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.";
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => ""
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    protected function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                    
                    $mensaje = "El Item Ha sido modificado correctamente.";
                    $tipo = EnumError::ERROR;
                    
                }else{
                    $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.";
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
              
                $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.".$e->getMessage();
                $tipo = EnumError::ERROR; 
                
            }
          
        } 
        if($this->RequestHandler->ext == 'json'){  
            $output = array(
                "status" => $tipo,
                "message" =>$mensaje,
                "content" => ""
            ); 
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content"));
        }else{
            $this->Session->setFlash($mensaje, $tipo);
             $this->redirect(array('controller' => 'CotizacionItems', 'action' => 'index'));
        
        } 
      
    }
    

    
    
    
    public function excelExport($vista="noagrupada",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Comprobantes imputados");
    	
    	
    	
    }
    
  
    public function getModel($vista = 'default'){
    	
    	$model = parent::getModelCamposDefault();//esta en APPCONTROLLER
    	
    	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    	
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    	
    	$model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
    	$this->set('model',$model);
    	// $this->set('this',$this);
    	Configure::write('debug',0);
    	$this->render($vista);
    }
    
    
   
    
    

    
    
    
   
    
}
?>