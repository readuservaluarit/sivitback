<?php
App::uses('AppModel', 'Model');

class RRHHTipoLiquidacionClaseLiquidacion  extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'rrhh_tipo_liquidacion_clase_liquidacion';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
       
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
        'RRHHTipoLiquidacion' => array(
            'className' => 'RRHHTipoLiquidacion',
            'foreignKey' => 'id_rrhh_tipo_liquidacion',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'RRHHClaseLiquidacion' => array(
            'className' => 'RRHHClaseLiquidacion',
            'foreignKey' => 'id_rrhh_clase_liquidacion',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
   );
   
   
   
   

}
