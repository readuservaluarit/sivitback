<?php

    /**
    * @secured(CONSULTA_PERIODO_CONTABLE)
    */
    class PeriodosContableController extends AppController {
        public $name = 'PeriodosContable';
        public $model = 'PeriodoContable';
        public $helpers = array ('Session', 'Paginator', 'Js');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    
    public function index() {

			if($this->request->is('ajax'))
                $this->layout = 'ajax';

            $this->loadModel($this->model);
            $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
            $cerrado = $this->getFromRequestOrSession($this->model.'.cerrado');
            $disponible = $this->getFromRequestOrSession($this->model.'.disponible');
            $id_ejercicio_contable = $this->getFromRequestOrSession($this->model.'.id_ejercicio_contable');
            $id_estado_ejercicio_contable = $this->getFromRequestOrSession('EjercicioContable.id_estado_ejercicio_contable');
            
            
            $conditions = array(); 
                                
            if($cerrado!="")
                array_push($conditions, array($this->model.'.cerrado =' => $cerrado));
                
            if($disponible!="")
                array_push($conditions, array($this->model.'.disponible =' => $disponible));
            
            if($id_ejercicio_contable!="")
                array_push($conditions, array($this->model.'.id_ejercicio_contable =' => $id_ejercicio_contable));
                
                
            if($id_estado_ejercicio_contable!="")
                array_push($conditions, array('EjercicioContable.id_estado =' => $id_estado_ejercicio_contable));    
                
            

            $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
              'contain'=>array('AsientoUltimo','EjercicioContable'),
                'conditions' => $conditions,
                'limit' => 12,
                'page' => $this->getPageNum(),
                'order'=> 'PeriodoContable.id DESC'
            );

            if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de PeriodoContableCausas', 'Datos de los Tipo de Impuesto', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));

                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_PeriodoContable', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();

                //Headers
                $formBuilder->addHeader('Id', 'PeriodoContable.id', "10%");
                $formBuilder->addHeader('Nombre', 'PeriodoContable.d_PeriodoContable_causa', "50%");
                $formBuilder->addHeader('Nombre', 'PeriodoContable.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_PeriodoContableCausa');
                $formBuilder->addField($this->model, 'cotizacion');

                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
            }
            else
            { // vista json
                $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
                $periodo_contable = $this->PeriodoContable->getPeriodoActual();
                
                foreach($data as &$dato){
                    

                    if($dato["PeriodoContable"]["id"] == $periodo_contable["id"])
					{
                        $dato["PeriodoContable"]["es_actual"] = "1";
						$dato["PeriodoContable"]["actual"] = "Si";
					}
                    else
					{
                        $dato["PeriodoContable"]["es_actual"] = "0";
						$dato["PeriodoContable"]["actual"] = "No";
					}
                    
                    
                      if($dato["PeriodoContable"]["disponible"] ==  1)
                    {
                        
                        $dato["PeriodoContable"]["d_disponible"] = "Si";
                    }
                    else
                    {
                       
                        $dato["PeriodoContable"]["d_disponible"] = "No";
                    }
                    
                    
                    
                    if($dato["PeriodoContable"]["cerrado"] ==  1)
                    {
                    	
                    	$dato["PeriodoContable"]["d_cerrado"] = "Si";
                    }
                    else
                    {
                    	
                    	$dato["PeriodoContable"]["d_cerrado"] = "No";
                    }
                    
                    
                        
                     if(isset($dato["EjercicioContable"]))
                        $dato["PeriodoContable"]["d_ejercicio_contable"] = $dato["EjercicioContable"]["d_ejercicio"];
                        
                        unset($dato["EjercicioContable"]);
                        unset($dato["AsientoUltimo"]);
                
                }
                
               
                $page_count = $this->params['paging'][$this->model]['pageCount'];

               

                $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "list",
                    "content" => $data,
                    "page_count" =>$page_count
                );
                $this->set($output);
                $this->set("_serialize", array("status", "message","page_count", "content"));

            }
        }


        public function abm($mode, $id = null) {


            if ($mode != "A" && $mode != "M" && $mode != "C")
                throw new MethodNotAllowedException();

            $this->layout = 'ajax';
            $this->loadModel($this->model);
            if ($mode != "A"){
                $this->PeriodoContable->id = $id;
                $this->request->data = $this->PeriodoContable->read();
            }


            App::import('Lib', 'FormBuilder');
            $formBuilder = new FormBuilder();

            //Configuracion del Formulario
            $formBuilder->setController($this);
            $formBuilder->setModelName($this->model);
            $formBuilder->setControllerName($this->name);
            $formBuilder->setTitulo('PeriodoContableCausa');

            if ($mode == "A")
                $formBuilder->setTituloForm('Alta de PeriodoContableCausa');
            elseif ($mode == "M")
                $formBuilder->setTituloForm('Modificaci&oacute;n de PeriodoContableCausa');
            else
                $formBuilder->setTituloForm('Consulta de PeriodoContableCausa');

            //Form
            $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

            //Fields





            $formBuilder->addFormInput('d_PeriodoContableCausa', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
            $formBuilder->addFormInput('cotizacion', 'Cotizaci&oacute;n', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));    


            $script = "
            function validateForm(){

            Validator.clearValidationMsgs('validationMsg_');

            var form = 'PeriodoContableCausaAbmForm';

            var validator = new Validator(form);

            validator.validateRequired('PeriodoContableCausaDPeriodoContableCausa', 'Debe ingresar un nombre');
            validator.validateRequired('PeriodoContableCausaCotizacion', 'Debe ingresar una cotizaci&oacute;n');


            if(!validator.isAllValid()){
            validator.showValidations('', 'validationMsg_');
            return false;   
            }

            return true;

            } 


            ";

            $formBuilder->addFormCustomScript($script);

            $formBuilder->setFormValidationFunction('validateForm()');

            $this->set('abm',$formBuilder);
            $this->set('mode', $mode);
            $this->set('id', $id);
            $this->render('/FormBuilder/abm');    
        }



        /**
        * @secured(ADD_PERIODO_CONTABLE)
        */
        public function add() {
            if ($this->request->is('post')){
                $this->loadModel($this->model);
                $id_PERIODO_CONTABLE = '';

                try{
                    
                      if($this->PeriodoContable->FechaEstaEnPeriodoAbierto($this->request->data["PeriodoContable"]["fecha_desde"]) == 0 &&  $this->PeriodoContable->FechaEstaEnPeriodoAbierto($this->request->data["PeriodoContable"]["fecha_hasta"]) == 0  ){ 
                            if ($this->PeriodoContable->saveAll($this->request->data, array('deep' => true))){
                                $mensaje = "El Periodo Contable ha sido creada exitosamente";
                                $tipo = EnumError::SUCCESS;
                                $id_PERIODO_CONTABLE = $this->{$this->model}->id;

                            }else{
                                $errores = $this->{$this->model}->validationErrors;
                                $errores_string = "";
                                foreach ($errores as $error){
                                    $errores_string.= "&bull; ".$error[0]."\n";

                                }
                                $mensaje = $errores_string;
                                $tipo = EnumError::ERROR; 

                            }
                      }else{
                          
                         $mensaje = "Hay Solapamiento de Fechas no es posible Editar el Peri&oacute;do";
                         $tipo = EnumError::ERROR;   
                          
                      }
                }catch(Exception $e){

                    $mensaje = "Ha ocurrido un error,el Periodo Contable no ha podido ser creada.".$e->getMessage();
                    $tipo = EnumError::ERROR;
                }
                $output = array(
                    "status" => $tipo,
                    "message" => $mensaje,
                    "content" => "",
                    "id_add"=>$id_PERIODO_CONTABLE
                );
                //si es json muestro esto
                if($this->RequestHandler->ext == 'json'){ 
                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content","id_add"));
                }else{

                    $this->Session->setFlash($mensaje, $tipo);
                    $this->redirect(array('action' => 'index'));
                }     
            }

            //si no es un post y no es json
            if($this->RequestHandler->ext != 'json')
                $this->redirect(array('action' => 'abm', 'A'));   
        }

        /**
        * @secured(MODIFICACION_PERIODO_CONTABLE)
        */
        public function edit($id) {


            if (!$this->request->is('get')){
                
                
                
                
                $this->loadModel($this->model);
                $this->loadModel("Asiento");
                $this->loadModel("EjercicioContable");
                $this->loadModel("Comprobante");
                
                $this->{$this->model}->id = $id;

                try{ 
                	
                	
                	
                    
                $periodo_contable = $this->PeriodoContable->getPeriodo($id);
                $id_ejercicio_contable = $periodo_contable["id_ejercicio_contable"];
                $ejercicio_contable = $this->EjercicioContable->getEjercicioContable($id_ejercicio_contable);
                
                
                
                $comprobantes_abiertos = array();
                
                /*Si voy a cerrar el periodo chequeo que no haya comprobantes Abiertos, si los hay le informo*/
                if( ($this->request->data["PeriodoContable"]["cerrado"]== 1 || $this->request->data["PeriodoContable"]["disponible"]== 0) && $periodo_contable["check_comprobantes_al_cierre"]== 1){
                	$array_estado_comprobante = array(EnumEstadoComprobante::Abierto);
                	$comprobantes_abiertos = $this->Comprobante->getComprobantesPorTipoYFechaYEstado(1,$array_estado_comprobante,$periodo_contable["fecha_desde"],$periodo_contable["fecha_hasta"],"fecha_contable");
                	
                
                }
                
                if(count($comprobantes_abiertos)== 0){
                
		                if($ejercicio_contable["id_estado"] != EnumEstado::Cerrada){    
			                if($this->PeriodoContable->FechaEstaEnPeriodoAbierto($this->request->data["PeriodoContable"]["fecha_desde"],$id,$id_ejercicio_contable) == 0 &&  $this->PeriodoContable->FechaEstaEnPeriodoAbierto($this->request->data["PeriodoContable"]["fecha_hasta"],$id,$id_ejercicio_contable) == 0  ){    
			                if($this->PeriodoContable->getEstadoGrabado($id) != 1 ){
			                    
			                    if($this->request->data["PeriodoContable"]["cerrado"]== 1)
			                    {
			                        
			                      $this->request->data["PeriodoContable"]["fecha_cierre"] = date("Y-m-d");
			                    }
			                    
			                      if($this->request->data["PeriodoContable"]["cerrado"]== 1 && $this->Asiento->getCantidadAsientos($this->request->data["PeriodoContable"]["id"],EnumEstadoAsiento::EnRevision)>0)
			                    {
			                          
			                        
			                          $mensaje = "Se encontraron Asientos en Revisi&oacute;n para el Peri&oacute;do que intenta cerrar.";
			                          $status = EnumError::ERROR;  
			                        
			                        
			                        
			                    }else{
			                    
			                  
			                            if ($this->{$this->model}->saveAll($this->request->data)){
			                                $mensaje =  "El Peri&oacute;do Contable ha sido modificado exitosamente";
			                                $status = EnumError::SUCCESS;
			                            }else{   //si hubo error recupero los errores de los models
			
			                                $errores = $this->{$this->model}->validationErrors;
			                                $errores_string = "";
			                                foreach ($errores as $error){ //recorro los errores y armo el mensaje
			                                    $errores_string.= "&bull; ".$error[0]."\n";
			
			                                }
			                                $mensaje = $errores_string;
			                                $status = EnumError::ERROR; 
			                            }
			                   } 
			                    
			                    
			
			                }else{
			                     $mensaje = "El Peri&oacute;do se encuentra cerrado y no puede editarse";
			                     $status = EnumError::ERROR; 
			                }
			                
			                }else{
			                     $mensaje = "Hay Solapamiento de Fechas no es posible Editar el Peri&oacute;do";
			                     $status = EnumError::ERROR; 
			                }
		                }else{
		                	
		                	$mensaje = "El EJercicio Contable del Peri&oacute;do se encuentra cerrado y no puede editarse";
		                	$status = EnumError::ERROR; 
		                }
                }else{
                	
                	$mensaje = "Existe comprobantes Abiertos debe cerrarlos/anularlos o realizar alguna acci&oacute;n para poder cerrar el peri&oacute;do"."\n";
                	$comprobantes_abiertos_array = $this->Comprobante->getNumerosComprobante($comprobantes_abiertos);
                	$mensaje.= "Los comprobantes que se encuentran abiertos son:".implode(",", $comprobantes_abiertos_array);
                	$status = EnumError::ERROR; 
                	
                }
                
                
                
                    
                }catch(Exception $e){
                	$mensaje = "No es posible editar el periodo".$e->getMessage();
                	$status = EnumError::ERROR; 

                }
                
                
                if($this->RequestHandler->ext == 'json'){
                	$output = array(
                			"status" => $status,
                			"message" => $mensaje,
                			"content" => ""
                	);
                	
                	$this->set($output);
                	$this->set("_serialize", array("status", "message", "content"));
                }else{
                	$this->Session->setFlash($mensaje, $status);
                	$this->redirect(array('controller' => $this->name, 'action' => 'index'));
                	
                } 

            }else{ //si me pide algun dato me debe mandar el id
                if($this->RequestHandler->ext == 'json'){ 

                    $this->{$this->model}->id = $id;
                    //$this->{$this->model}->contain('Provincia','Pais');
                    $this->request->data = $this->{$this->model}->read();          

                    $output = array(
                        "status" =>EnumError::SUCCESS,
                        "message" => "list",
                        "content" => $this->request->data
                    );   

                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content")); 
                }else{

                    $this->redirect(array('action' => 'abm', 'M', $id));

                }


            } 

            if($this->RequestHandler->ext != 'json')
                $this->redirect(array('action' => 'abm', 'M', $id));
        }

        /**
        * @secured(BAJA_PERIODO_CONTABLE)
        */
        function delete($id) {
            $this->loadModel($this->model);
            $mensaje = "";
            $status = "";
            $this->PeriodoContable->id = $id;

            try{
                if ($this->PeriodoContable->delete() ) {

                    //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');

                    $status = EnumError::SUCCESS;
                    $mensaje = "El Peri&oacute;do Contable ha sido eliminado exitosamente.";
                    $output = array(
                        "status" => $status,
                        "message" => $mensaje,
                        "content" => ""
                    ); 

                }

                else
                    throw new Exception();

            }catch(Exception $ex){ 
                //$this->Session->setFlash($ex->getMessage(), 'error');

                $status = EnumError::ERROR;
                $mensaje = $ex->getMessage();
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
            }

            if($this->RequestHandler->ext == 'json'){
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));

            }else{
                $this->Session->setFlash($mensaje, $status);
                $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            }
        }

        /**
        * @secured(CONSULTA_PERIODO_CONTABLE)
        */
        public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }

        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);

        }
        
		/**
        * @secured(CONSULTA_PERIODO_CONTABLE)
        */
        public function FechaEstaEnPeriodoAbierto($fecha){
            //TODO: refactor - evaluart impacto y Renombrar como el metodo de abajo FechaEstaEnPeriodoAbiertoYDisponible
           $this->loadModel("PeriodoContable");
           $this->loadModel("EjercicioContable");
           
           $existe = $this->PeriodoContable->FechaEstaEnPeriodoAbierto($fecha);
           
           $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "",
                    "content" => $existe
                ); 
                
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content"));     
        }
		
		/**
        * @secured(CONSULTA_PERIODO_CONTABLE)
        */
        public function FechaEstaEnPeriodoAbiertoYDisponible($fecha){
            
           $this->loadModel("PeriodoContable");
           $this->loadModel("EjercicioContable");
           
           $existe = $this->PeriodoContable->FechaEstaEnPeriodoAbierto($fecha);
           
           $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "",
                    "content" => $existe
                ); 
                
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content"));     
        }

    }
?>