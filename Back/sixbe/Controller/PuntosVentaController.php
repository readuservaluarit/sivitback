<?php

/**
* @secured(CONSULTA_PUNTO_VENTA)
*/
class PuntosVentaController extends AppController {
    public $name = 'PuntosVenta';
    public $model = 'PuntoVenta';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
/**
* @secured(CONSULTA_PUNTO_VENTA)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        //Recuperacion de Filtros
        $d_punto_venta = strtolower($this->getFromRequestOrSession('PuntoVenta.d_punto_venta'));
		$numero = strtolower($this->getFromRequestOrSession('PuntoVenta.numero'));
        $id = $this->getFromRequestOrSession('PuntoVenta.id');
        $factura_electronica = $this->getFromRequestOrSession('PuntoVenta.factura_electronica');
        $id_moneda = $this->getFromRequestOrSession('PuntoVenta.id_moneda');
		$id_dato_empresa = $this->getFromRequestOrSession('PuntoVenta.id_dato_empresa');
		
		$conditions = array(); 
        
        if ($d_punto_venta != "") {
			array_push($conditions, array('LOWER(PuntoVenta.d_punto_venta) LIKE' => '%' . $d_punto_venta . '%')); 	
        }
		
		if ($numero != "") {
			array_push($conditions, array('LOWER(PuntoVenta.numero) LIKE' => '%' . $numero . '%')); 	
        }
		if ($id != "") {
            array_push($conditions, array('PuntoVenta.id  = ' => $id )); 
        }
        
        if ($factura_electronica != "") {
            array_push($conditions, array('PuntoVenta.factura_electronica  = ' => $factura_electronica )); 
        }
        
        if ($id_moneda != "") {
            array_push($conditions, array('PuntoVenta.id_moneda  = ' => $id_moneda )); 
        }
        
        if ($id_dato_empresa != "") {
            array_push($conditions, array('PuntoVenta.id_dato_empresa  = ' => $id_dato_empresa )); 
        }

        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de Puntos Venta', 'Datos de los Puntos Venta', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_punto_venta', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'PuntoVenta.id', "10%");
                $formBuilder->addHeader('Nombre', 'PuntoVenta.d_PUNTO_VENTA', "50%");
             

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_PUNTO_VENTA');
        
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }
		else
		{ // vista json
			$this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
			$page_count = $this->params['paging'][$this->model]['pageCount'];
			
			//parseo el data para que los models queden dentro del objeto de respuesta
			foreach($data as &$valor)
			{
				 $this->cleanforOutput($valor);
			}
			
			$output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "list",
				"content" => $data,
				"page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->PuntoVenta->id = $id;
            $this->request->data = $this->PuntoVenta->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('PuntoVenta');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de PuntoVenta');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de PuntoVenta');
        else
            $formBuilder->setTituloForm('Consulta de PuntoVenta');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields
        $formBuilder->addFormInput('d_PUNTO_VENTA', 'Nombre de PuntoVenta', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
     
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'PuntoVentaAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('PuntoVentaDPuntoVenta', 'Debe ingresar un nombre');
        


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) 
	{        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }
    /**
    * @secured(ADD_PUNTO_VENTA)
    */
    public function add() 
	{
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            
            try{
                if ($this->PuntoVenta->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "El Punto de venta ha sido creada exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el Punto de venta no ha podido ser creada.";
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => ""
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }        
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
    }

     /**
    * @secured(MODIFICACION_PUNTO_VENTA)
    */
    public function edit($id) 
	{
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "El Punto de venta ha sido modificada exitosamente";
                     $status = EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
               }
               
               if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, El Punto de venta no ha podido modificarse.', 'error');
                
            }
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            $this->{$this->model}->contain('Provincia','Pais','Moneda');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    /**
    * @secured(BAJA_PUNTO_VENTA)
    */
    function delete($id) 
	{
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->PuntoVenta->id = $id;
     
       try{
            if ($this->PuntoVenta->delete() ) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "La Forma de Pago ha sido eliminada exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
    }
	
	private function cleanforOutput(&$valor)
	 {   
         if(isset($valor['Moneda'])){
          $valor['PuntoVenta']['d_moneda'] =$valor['Moneda']['d_moneda'];
         }
          if(isset($valor['Pais'])){
          $valor['Pais']['d_pais'] =$valor['Pais']['d_pais'];
         }
         
         if(isset($valor['Provincia'])){
          $valor['Provincia']['d_provincia'] =$valor['Provincia']['d_provincia'];
         }

          unset($valor['Moneda']);
          unset($valor['DatoEmpresa']);
          unset($valor['Pais']);
          unset($valor['Provincia']);
     }
	
	
	/**
	* @secured(CONSULTA_PUNTO_VENTA)
	*/ 
	public function getModel($vista = 'default'){

		$model = parent::getModelCamposDefault();//esta en APPCONTROLLER

		$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

	}

	private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

		$model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
		$this->set('model',$model);
		// $this->set('this',$this);
		Configure::write('debug',0);
		$this->render($vista);
	}
}
?>