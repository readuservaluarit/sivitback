<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class FormaPago extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'forma_pago';
    public $actsAs = array('Containable');
    
    
    


    
    public $virtualFields = array(
    		'd_forma_pago_abreviado' => 'CONCAT(FormaPago.codigo,"-",IFNULL(FormaPago.interes,"0"),"%")'
    ); 
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'd_forma_pago' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar una descripci&oacute;n.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
        
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	
	
	public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
		
		
		
		$this->virtual_fields_view = array(
				
				"d_forma_pago_abreviado"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_valor"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
	
				
				
		);
		
		
	}
	
	
	
	
	
	public $belongsTo = array(
			'Valor' => array(
					'className' => 'Valor',
					'foreignKey' => 'id_valor',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
			));
	
/**
 * hasMany associations
 *
 * @var array
 */
	/*public $hasMany = array(
		'Region' => array(
			'className' => 'Region',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public $childRecordMessage = "Existe por lo menos una regi&oacute;n, predio, establecimiento o usuario relacionado/a con la jurisdicci&oacute;n.";
    
    */
}
