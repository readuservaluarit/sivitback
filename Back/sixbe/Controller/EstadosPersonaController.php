<?php
class EstadosPersonaController extends AppController {
    public $name = 'EstadosPersona';
    public $model = 'EstadoPersona';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    protected function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
		//Recuperacion de Filtros
     
        $id = $this->getFromRequestOrSession('EstadoPersona.id');
     
        $conditions = array(); 
        
        if ($id != "") {
           array_push($conditions,array('EstadoPersona.id' =>$id ));
        }
		array_push($conditions,array('EstadoPersona.id_tipo_persona' =>  $this->id_tipo_persona));
  
     
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            
            //'contain'=>array('EstadoTipoCheque'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de EstadosPersona', 'Datos de los EstadosPersona', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_ESTADO_PERSONA', 'Nombre de EstadoPersona', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'EstadoPersona.id', "10%");
                $formBuilder->addHeader('Nombre', 'EstadoPersona.d_moneda', "50%");
             

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_ESTADO_PERSONA');
        
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
     }
    }


    
  
    protected function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            if ($this->EstadoPersona->save($this->request->data))
                $this->Session->setFlash('El EstadoPersona ha sido creada exitosamente.', 'success'); 
            else
                $this->Session->setFlash('Ha ocurrido un error, el EstadoPersona no ha podido ser creada.', 'error');

            $this->redirect(array('action' => 'index'));
        } 

        $this->redirect(array('action' => 'abm', 'A'));                   
    }

    
    protected function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->EstadoPersona->id = $id;
            
            try{ 
                if ($this->EstadoPersona->saveAll($this->request->data)){
                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "El EstadoPersona ha sido modificado exitosamente",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('El EstadoPersona ha sido modificado exitosamente.', 'success');
                        $this->redirect(array('controller' => 'Monedas', 'action' => 'index'));
                        
                    } 
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, el EstadoPersona no ha podido modificarse.', 'error');
                
            }
            $this->redirect(array('action' => 'index'));
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }

   
    
   
    protected  function delete($id) {
    	$this->loadModel($this->model);
    	$mensaje = "";
    	$status = "";
    	$this->{$this->model}->id = $id;
    	
    	try{
    		if ($this->{$this->model}->delete() ) {
    			
    			//$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
    			
    			$status = EnumError::SUCCESS;
    			$mensaje = "El Origen ha sido eliminado exitosamente.";
    			$output = array(
    					"status" => $status,
    					"message" => $mensaje,
    					"content" => ""
    			);
    			
    		}
    		
    		else
    			throw new Exception();
    			
    	}catch(Exception $ex){
    		//$this->Session->setFlash($ex->getMessage(), 'error');
    		
    		$status = EnumError::ERROR;
    		$mensaje = $ex->getMessage();
    		$output = array(
    				"status" => $status,
    				"message" => $mensaje,
    				"content" => ""
    		);
    	}
    	
    	if($this->RequestHandler->ext == 'json'){
    		$this->set($output);
    		$this->set("_serialize", array("status", "message", "content"));
    		
    	}else{
    		$this->Session->setFlash($mensaje, $status);
    		$this->redirect(array('controller' => $this->name, 'action' => 'index'));
    		
    	}
    	
    	
    	
    	
    	
    }


}
?>