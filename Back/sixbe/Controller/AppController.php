<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');
App::uses('PaginatorModificadoComponent', 'Controller/Component');
App::import('Lib', 'SecurityManager');
App::import('Lib', 'GeneralFunctions');  
App::uses('EntityPicker', 'Lib/EntityPickers');
App::uses("ChildRecordException", "Lib/Exceptions");
App::uses("UniqueKeyException", "Lib/Exceptions");
App::uses('ApplicationModel', 'Model'); 
App::uses('EnumRol', 'Lib/Enums'); 
App::uses('EnumEstado', 'Lib/Enums'); 
App::uses('EnumTipoMovimiento', 'Lib/Enums');     
App::uses('EnumTipoMovimientoCuentaCorriente', 'Lib/Enums');     
App::uses('EnumOrigen', 'Lib/Enums');     
App::uses('EnumTipoAlicuotaAfip', 'Lib/Enums');     
App::uses('EnumMediaFileTipo', 'Lib/Enums');     
App::uses('EnumOrdenPagoConcepto', 'Lib/Enums');     
App::uses('EnumIva', 'Lib/Enums');     
App::uses('EnumMoneda', 'Lib/Enums');      
App::uses('EnumDestinoProducto', 'Lib/Enums');     
App::uses('EnumMensaje', 'Lib/Enums');     
App::uses('EnumTipoPersona', 'Lib/Enums');     
App::uses('EnumTipoComprobante', 'Lib/Enums');     
App::uses('EnumImpuesto', 'Lib/Enums');     
App::uses('EnumSistema', 'Lib/Enums');     
App::uses('EnumEstadoComprobante', 'Lib/Enums');         
App::uses('EnumProductoTipo', 'Lib/Enums');     
App::uses('EnumDebeHaber', 'Lib/Enums');     
App::uses('EnumTipoFiltroComprobanteItem', 'Lib/Enums');     
App::uses('EnumTipoCheque', 'Lib/Enums');     
App::uses('EnumTipoMoneda', 'Lib/Enums');     
App::uses('EnumValor', 'Lib/Enums');     
App::uses('EnumReporte', 'Lib/Enums');     
App::uses('EnumEstadoCheque', 'Lib/Enums');     
App::uses('EnumCondicionPago', 'Lib/Enums');     
App::uses('EnumEstadoAsiento', 'Lib/Enums');     
App::uses('EnumAsientoEfecto', 'Lib/Enums');     
App::uses('EnumModulo', 'Lib/Enums');     
App::uses('EnumTipoImpuesto', 'Lib/Enums');     
App::uses('EnumAgrupacionAsiento', 'Lib/Enums');     
App::uses('EnumDetalleTipoComprobante', 'Lib/Enums');     
App::uses('DataFilter', 'Lib');  
App::uses('EnumController', 'Lib/Enums');             
App::uses('EnumTipoIva', 'Lib/Enums');             
App::uses('EnumSubTipoImpuesto', 'Lib/Enums');             
App::uses('EnumProductoClasificacion', 'Lib/Enums');             
App::uses('EnumPersonaFigura', 'Lib/Enums');             
App::uses('EnumImpuestoGeografia', 'Lib/Enums');             
App::uses('EnumTipoDocumento', 'Lib/Enums');             
App::uses('EnumEstadoPersona', 'Lib/Enums');             
App::uses('EnumTipoDato', 'Lib/Enums');             
App::uses('EnumTipoListaPrecio', 'Lib/Enums');             
App::uses('EnumListarTipo', 'Lib/Enums');             
App::uses('EnumMenu', 'Lib/Enums');          
App::uses('EnumClaseAsiento', 'Lib/Enums');          
App::uses('EnumRowStyleDefault', 'Lib/Enums');          
App::uses('EnumRowStyleAlert', 'Lib/Enums');          
App::uses('EnumRowStyleWarning', 'Lib/Enums');          
App::uses('EnumRowStyleOk', 'Lib/Enums');          
App::uses('EnumEstadoChequera', 'Lib/Enums');          
App::uses('EnumTipoComprobantePuntoVentaNumero', 'Lib/Enums');          
App::uses('EnumError', 'Lib/Enums');          
App::uses('EnumPersonaBase', 'Lib/Enums');          
App::uses('EnumMessageType', 'Lib/Enums');          
App::uses('EnumModel', 'Lib/Enums');               
App::uses('EnumCondicionPago', 'Lib/Enums');          
App::uses('EnumCacheName', 'Lib/Enums');          
App::uses('EnumTipoLote', 'Lib/Enums');          
App::uses('EnumUnidad', 'Lib/Enums');          
App::uses('EnumEntregaPedido', 'Lib/Enums');          
App::uses('EnumTipoReporteTablero', 'Lib/Enums');          
App::uses('EnumAsse', 'Lib/Enums');
App::uses('EnumRRHHVariable', 'Lib/Enums');          
App::uses('EnumRRHHTipoConcepto', 'Lib/Enums');          
App::uses('EnumRRHHAntiguedad', 'Lib/Enums');          
App::uses('EnumRRHHTipoTotal', 'Lib/Enums');          
App::uses('EnumRRHHTipoTotalCalculo', 'Lib/Enums');          
App::uses('EnumRRHHClaseFormulaConcepto', 'Lib/Enums');          
App::uses('EnumRRHHTipoCampoConcepto', 'Lib/Enums');          
App::uses('EnumRRHHEstadoLiquidacion', 'Lib/Enums');          

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller 
{        
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $calculated_fields = array();
    public $calculated_fields_items = array();
    public $acepta_json_request = 1;  //acepta Json
    public $paginado = 1;  //por default todo paginado
    public $data = null;
    public $llamada_interna = 0;
    public $output ='';//almacena el array de salida de respuesta
    public $type_message = array(EnumMessageType::MessageBox);
    public $tiene_permisos = 1;
    public $array_filter_names = array();
    public $tiene_cache_cliente = 0;

    
    //public $title_form = "Listado";
    
    
  
    
    public $components = array(
        'Session', 'RequestHandler', 
        'Auth' => array(
            'loginAction' => array('controller' => '/','action' => 'login'),
            'loginRedirect' => array('controller' => 'pages', 'action' => 'display', 'home'),
            'logoutRedirect' => array('controller' => '/', 'action' => 'login'),
            'authenticate' => array(
                                    'Form' => array (
                                                        'userModel' => 'Usuario', 
                                                        /*'fields' => array(
                                                            'username' => 'c_usuario' , 
                                                            'password' => 'd_password'
                                                        )*/
                                                    )
                                    ),
            'authError' => "Su sesión ha expirado debido al tiempo de inactividad, por favor ingrese nuevamente..."            
        ),
        
    );
    
    
    public $numrecords = 20; //numero de registros que se ven en un grilla
    public $maxLimitRows = 100; //numero de registros que se ven en un grilla

    
    
    
    
    public function beforeFilter() {
		
		
		$this->log(json_encode($_REQUEST));
		
		
		
		// Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
        // you want to allow, and if so:
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            // may also be using PUT, PATCH, HEAD etc
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

     
    }


    	
    	
    	$cloud = Configure::read("cloud");
    	
    	
    	
    	
    	if(isset($this)){
	    	if($cloud){
	    		
	    		$this->numrecords  = Configure::read("cloud_numrecords");
	    		$this->maxLimitRows = Configure::read("cloud_maxLimitRows");
	    		
	    		
	    	}else{
	    		
	    		$this->numrecords = Configure::read("numrecords");;
	    		$this->maxLimitRows= Configure::read("maxLimitRows");;
	    	}
        
    	}
         
    	$data = array();
    	
    	
        $data = $this->request->input('json_decode',true); 
        $this->log(json_encode($data));
       // $this->log(file_get_contents("php://input")); devulve el post sin procesar
       /* die();*/
        //Si envia Json y esta aceptado json
        
        
        if($this->request->controller != EnumController::Movimientos){// si primero envia con json el front y luego hago RequestAction entonces se pisa el request->data
            if( $this->acepta_json_request  == 1 && is_array($data) && count($data)>0){
	            
	            $this->request->data = $this->request->input('json_decode',true); //piso el data de HTML por el Json 
	            
	        }
        }
        
        //$this->useDbConfig = "Pepe";
        
        setlocale(LC_MONETARY, 'it_IT');
        //Para que los mensajes de auth tomen el template que corresponde
        $this->Auth->flash['element'] = 'auth'; 
        
        $this->genericFilters($this->model);//obtengo una lista de filtros genericos
        
        //Tomo el controller y el action
        $controller = $this->params['controller'];
        $action = $this->params['action'];            
        
        //Verificacion de Seguridad
        if($this->Session->read('Auth.User.username'))
            SecurityManager::checkSecurity($this, $controller . 'Controller', $action);
        
        //Envia los header que fuerzan a no cachear las paginas (IE)
        $this->disableCache();
        
        $request_log_enabled = Configure::read("request_log_enable");
        
        if($request_log_enabled){
        	$this->addRequestLog();
        }

        /*
        if($this->request->action == "index" &&  $this->RequestHandler->ext != 'json')
        	$this->request->action = "indexHtml";
        */
        
        
        
        /*if(!isset($_SESSION)) //ojo es un fix
            session_start(); 
        */
        //CSRF
        //$this->Security->csrfExpires = '+1 minutes';
        
    }
    
    public function addRequestLog(){
    	
    	$this->loadModel(EnumModel::RequestLog);
    	
    	$controller = $this->params['controller'];
    	$action = $this->params['action'];    
    	if($this->Session->read('Auth.User.username')){
    		
    		$request["RequestLog"]["id_usuario"] = $this->Session->read('Auth.User.id');
    	}
    	$request["RequestLog"]["controller"] = $this->params['controller'];
    	$request["RequestLog"]["action"] = $this->params['action'];
    	$request["RequestLog"]["date"] = date('Y-m-d');
    	
    	$this->RequestLog->save($request);
    	
    }
    
    public function afterFilter(){
        
    	
        //Renuevo la session (esto es para que cada vez que tuvo actividad, le renueve la cookie de session)
       // $this->Session->renew();
        
    }
    
    public function genericFilters($model_name){
        
         $limite_registros =  $this->getFromRequestOrSession($model_name.'.limite_registro');
		  // $limite_registros =  10000;
         $paginado =  $this->getFromRequestOrSession($model_name.'.paginado');
         
         if($paginado != '')
            $this->paginado = $paginado;
       
        if( $limite_registros !=null && $limite_registros !='' && is_numeric($limite_registros))
            $this->numrecords = (int) $limite_registros;
        
        
    }
    
    /**
    * Busca la variable de filtro en la session o en el request segun corresponda
    * 
    * @param mixed $index
    * @return mixed
    */
    public function getFromRequestOrSession($index){
        
        $sessionKey = $this->request->params['controller'].'.'.$this->request->params['action'].'.'.$index;
        
        $aux = explode(".", $index);
        $fieldName = implode($aux, "']['");
        
        $valorDato = '';
        
        if($this->request->is('ajax')){
            //Si es enviado por AJAX desde el browser una pagina HTML 
            if($this->request->is('post')){
                //lo saco del request y lo pongo en session    
                eval("\$valorDato = isset(\$this->request->data['".$fieldName."'])?\$this->request->data['".$fieldName."']:'';");
                $this->Session->write($sessionKey, $valorDato);
                
            }else{
                $valorDato = $this->Session->read($sessionKey);
           }
            
        }else{
            //MP: Si no es AJAX lo mismo
            if($this->request->is('post')){
                //lo saco del request y lo pongo en session    
                eval("\$valorDato = isset(\$this->request->data['".$fieldName."'])?\$this->request->data['".$fieldName."']:'';");
                $this->Session->write($sessionKey, $valorDato);
                
            }else{
                $valorDato = $this->Session->read($sessionKey);
           }
            
            //MP: $this->Session->delete($sessionKey);
            
        }              
           
        return $valorDato;
        
        
    }
    
    
     public function ordenarray($a, $b) {
        return $a['order'] - $b['order'];
    }  
        
    public function getPageNum(){
        
        $index = 'named.page';
        
        $sessionKey = $this->model.'.'.$index;
        
        $aux = explode(".", $index);
        $fieldName = implode($aux, "']['");
        
        $valorDato = '';
        
        //Lo saco del request
        eval("\$valorDato = isset(\$this->request->params['".$fieldName."'])?\$this->request->params['".$fieldName."']:'';");
        
       
        
        if($this->request->is('ajax')){
             
             
            if($this->request->is('post')){
             
                $this->Session->delete($sessionKey); 
                
            }
            else{
                
                //Si no esta lo saco de session, si esta lo escribo en session
                if($valorDato == '')
                $valorDato = $this->Session->read($sessionKey);
                else
                    $this->Session->write($sessionKey, $valorDato);
            }
             
            
        }else{
            
            $this->Session->delete($sessionKey);
            
        }
           
        return $valorDato;
        
        
    }
    
    /**
    * Hace el unbind de los hasMany del model, para que no levante todos los datos relacionados
    * 
    */
    public function unBindHasManyFromModel(){
        
        $arr_hm = array();
        $hm = $this->{$this->model}->hasMany;
        
        foreach($hm as $clave => $valor){
            
            array_push($arr_hm, $clave);
            
        }
        
        $this->{$this->model}->unBindModel(array('hasMany' => $arr_hm), true);
        
    }
    
    public function unBindHasMany($model){
        
        $arr_hm = array();
        $hm = $model->hasMany;
        
        foreach($hm as $clave => $valor){
            
            array_push($arr_hm, $clave);
            
        }
        
        $model->unBindModel(array('hasMany' => $arr_hm), true);
        
    }
    
    public function unBindHasOne($model){
        
        $arr_ho = array();
        $ho = $model->hasOne;
        
        foreach($ho as $clave => $valor){
            
            array_push($arr_ho, $clave);
            
        }
        
        $model->unBindModel(array('hasOne' => $arr_ho), true);
        
    }
    
    public function unBindBelongsTo($model){
        
        $arr_bt = array();
        $bt = $model->belongsTo;
        
        foreach($bt as $clave => $valor){
            
            array_push($arr_bt, $clave);
            
        }
        
        $model->unBindModel(array('belongsTo' => $arr_bt), true);
        
    }
    
    
    public function getModel($vista){
        
          $this->autoRender = false;
          
          if(isset($this->model))
            $model =  $this->model;
          else
            $model = "Comprobante";
         
          //$this->layout = 'ajax';
           $this->loadModel($this->model);
           $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => $this->model,
                    "content" => $this->{$this->model}->schema()
                );
           $this->set($output);
           $this->set("_serialize", array("status", "message", "content"));
    }
    
    public function getModelInterno(){
        
          $this->autoRender = false;
         
          //$this->layout = 'ajax';
          if(isset($this->model)){ //si tiene model leo la tabla sino es solo vista es un reporte armado
           $this->loadModel($this->model);
         return $this->{$this->model}->schema();
        }else
            return false;
    }
    
    public function getModelCamposDefault(){
        
        $model = $this->getModelInterno();
       
        
        if($model){
        foreach ($model as $key=>&$campo){
         
            if( $model[$key]["default"] >0){
                
            if(substr($key,0,3)== 'id_'){  
             $default_value = $model[$key]["default"]; 
             $model_name = substr($key,3);
             //$model_name[0] = strtoupper($model_name[0]);
             $model_name_camelized = Inflector::camelize($model_name);
             
             $this->loadModel($model_name_camelized);
              $item =  $this->{$model_name_camelized}->find('first', array(
                                                        'conditions' => array($model_name_camelized.'.id' => $default_value),
                                                        'contain' =>false
                                                 ));
                                                 
              if($item && $default_value>0){   
                                                  
                $model[$key]["d_".$model_name] = $item[$model_name_camelized]["d_".$model_name];
             } else{
                $model[$key]["d_".$model_name] = 0;
             }   
                
              
            }
         }
            
        }
        
       return $model;
     }  
    }
    
    public function setFieldProperties($bd_name,$name,$value,$visible,$orden){
        
        $field = array();
        
        $field["name"] = $bd_name;
        $field["label"] = $name;
        $field["value"] = $value;
        $field["visible"] = $visible;
        $field["orden"] = $orden;
        
        return $field;
        
        
        
        
    }
    
    public function getCalculatedFields(){
        
          $this->autoRender = false;
          $this->initialize();
          
         
          //$this->layout = 'ajax';
           $this->loadModel($this->model);
           $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => $this->model,
                    "content" => $this->calculated_fields //es un array con los campos calculados
                );
          echo json_encode($output);
    }
    
    public function getCalculatedFieldsItem(){
        
          $this->autoRender = false;
          $this->initialize();
          
         
          //$this->layout = 'ajax';
           $this->loadModel($this->model);
           $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => $this->model."Item",
                    "content" => $this->calculated_fields_items //es un array con los campos calculados
                );
          echo json_encode($output);
    }
    
    public function addCalculatedField($field){
        
          array_push($this->calculated_fields,$field);
        
    }                 
    
     public function addCalculatedFieldItem($field){
        
          array_push($this->calculated_fields_items,$field);
        
    }
    public function initialize(){
        return '';
    }
    
    
     public function redirect($url, $status = null, $exit = true) {
         //$url;
         // Si esta usando Json no lo redirijo, solo mando un mensaje
        if($this->RequestHandler->ext == 'json' && !$this->Auth->loggedIn()) {
             $output = array(
                    "status" => "error_session_expired",
                    "message" => "Su sesion ha expirado o no se encuentra logueado.",
                    "content" => ""
                );
                echo    json_encode($output);
            
            return $this->_stop();
        }else{
         return parent::redirect($url, $status, $exit); 
        }
        
     
       
    }
    
    protected function setDefaultFieldsForView($model){
        
        
        foreach($model as $key=> &$dato){ //por default no se muesta ningun campo
            
           $model[$key]["show_grid"] = 0;
           $model[$key]["order"] = 1;
           $model[$key]["text_color"] = "#000000";
           
           if(isset($model[$key]["unsigned"])){
           if($model[$key]["unsigned"])
               $model[$key]["unsigned"] = 1;
           else
                $model[$key]["unsigned"] = 0;
           }else{
               $model[$key]["unsigned"] = 0;
           } 
        }
        
        return $model; 
        
        
    }
    
    
     protected function checkTypeInModel($model){
        
        
        foreach($model as $key=> &$dato){ //por default no se muesta ningun campo
            
           $model[$key]["show_grid"] = 0;
           $model[$key]["order"] = 1;
           $model[$key]["text_color"] = "#000000";
            
        }
        
        return $model; 
        
        
    }
    
    protected function setFieldView($indice,$propiedades,&$model){
        
        
        foreach($propiedades as $key=>$value){
            
            
            $model[$indice][$key] = $value;
            
            
        }
        
       
       return $model; 
        
        
        
    } 
    
     
    
    
    protected function ClearCache() {
        
        Cache::clear();
        clearCache();

        $files = array();
        $files = array_merge($files, glob(CACHE . '*')); // remove cached css
        $files = array_merge($files, glob(CACHE . 'css' . DS . '*')); // remove cached css
        $files = array_merge($files, glob(CACHE . 'js' . DS . '*'));  // remove cached js           
        $files = array_merge($files, glob(CACHE . 'models' . DS . '*'));  // remove cached models           
        $files = array_merge($files, glob(CACHE . 'persistent' . DS . '*'));  // remove cached persistent           

        foreach ($files as $f) {
            if (is_file($f)) {
                unlink($f);
            }
        }

        if(function_exists('apc_clear_cache')):      
        apc_clear_cache();
        apc_clear_cache('user');
        endif;

        $this->set(compact('files'));
        $this->layout = 'ajax';
        
        
        echo "hola";
        die();
    }    
    
   /** Actual month last day **/
  function _data_last_month_day($month='',$year = '') { 
      
      if($month == '')
        $month = date('m');
      
       if($year == '')
        $year = date('Y');
        
      $day = date("d", mktime(0,0,0, $month+1, 0, $year));
 
      return date('Y-m-d', mktime(0,0,0, $month, $day, $year));
  }
 
  /** Actual month first day **/
  function _data_first_month_day($month='',$year = '' ) {
      
      if($month == '')
        $month = date('m');
      
     if($year == '')
        $year = date('Y');
        
      return date('Y-m-d', mktime(0,0,0, $month, 1, $year));
  }
  
  
  function nombremes($mes){
     setlocale(LC_TIME, 'spanish');  
     $nombre=strftime("%B",mktime(0, 0, 0, $mes, 1, 2000)); 
     return $nombre;
 }  
 
 function getDiffInMonths($fechainicial,$fechafinal){    //AAAA-MM-DD
     
     
     $fechainicial = new DateTime($fechainicial);
     $fechafinal = new DateTime($fechafinal);
     $diferencia = $fechainicial->diff($fechafinal);
     
     $meses = ( $diferencia->y * 12 ) + $diferencia->m;
     return $meses;
 }  
 
 public function xlsExport($data,$vista,$datos_extra='')
	{
		
		


		$this->set('datos_pdf',$data);
		$this->set('datos_extra',$datos_extra);


		$impuestos = '';
		Configure::write('debug',0);
		$this->layout = 'xls'; //esto usara el layout pdf.ctp
		$this->response->type('xls');

		if($vista=='')
		{
			$this->render();
		}
		else
		{
			$this->render($vista);
		}
	} 
        
        
        
        
 
 
        
 protected function ExportarExcel($vista,$funcion_de_datos="index",$titulo){ /*Funcion de datos es la funcion que devuelve los datos y setea la variable $this->data*/
     
     //TODO: Pasar filtros
     /*Para que funcione se debe tocar el metodo index del controller de donde es llamado*/
 	
 	Configure::write('debug',0);
 	$file = $titulo.".xls";
 	// $this->layout = 'xls'; //esto usara el layout pdf.ctp
 
 	$sobreescribir = strtolower($this->getFromRequestOrSession('Excel.sobreescribir'));
 	
 	

 	 ob_start();
     
     $this->RequestHandler->ext = 'json';
     $this->paginado = 0;
     
     try{
     	$this->{$funcion_de_datos}();
     	$this->response->type('xls');
     	header("Content-Disposition: attachment; filename=".$file);
     	header("Content-type: application/vnd.ms-excel");
     	header ("Pragma: no-cache");
     	
     }catch (Exception $e){
     	
     $e;	
     	
     }
     
     $model = $this->getModelCamposDefault();
     $model =  $this->setDefaultFieldsForView($model);
    // App::uses('View', $this->name.'/default.ctp');
     include_once(APP.'View'.DS.$this->name.DS.'json'.DS.$vista.'.ctp');  //esta linea importa el php y le setea al model los campos, me carga la variable model con lo visible
     ob_clean();
     
     if($sobreescribir == "" || $sobreescribir == 1)
     	echo ' <html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">
      <meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head>
      <!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>'.$titulo.'</x:Name>
      <x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body>';
     	
      
   
     
    
     
     
     $array_visibles = array();
     $array_visibles_con_orden = array();
     $html = '';
     
     
    $subtitulo1 = "";
	
     
    
  
    
     		$cant_columnas = 0;
            foreach($model as $key=>$campo){
                
               if($campo["show_grid"]==1){
               	
               	++$cant_columnas;
                
                $campo["clave"] = $key;
                $campo["header_display"] = html_entity_decode($campo["header_display"]);
                $campo["order"] = $campo["order"]; 
                array_push($array_visibles_con_orden,$campo);
                
               }
             }
             
             
            $filtros = ' Filtros: '; 
             foreach($this->array_filter_names as $key=>$filtro){
             	
             	foreach($filtro as $key2=> $value){
             		$filtros.= ' <strong>'.$key2.'</strong>'.$value.' ';
             	}
             	
             	
             	
             }
             
   echo '
             		
        <table width="100%" border="0">
        <tr>
        <td colspan ="'.$cant_columnas.'"><strong>'.$titulo.'</strong> '.$filtros.'</td>
        		
        </tr>
        <tr>
        <td>'.$subtitulo1.'</td>
        		
        </tr>
        </table>';
   
   
    echo '<table width="100%" border="0"><tr style="color:white;">';
                
    uasort($array_visibles_con_orden, array(&$this, 'ordenarray')); 
     
     foreach($array_visibles_con_orden as $key=>$campo){
         
        echo '<th  height="25" align="center" bgcolor="#0033FF"><strong>'.$campo["header_display"].'</strong></th>';
         array_push($array_visibles,$campo['clave']);
         
     }     
    
    echo '</tr>';
    
    
     foreach($this->data as $key=>$valor){
     
     echo '<tr>';
     
               foreach($array_visibles as $columa_a_imprimir){
                 
                   if(is_numeric($valor[$this->model][$columa_a_imprimir])) 
                    echo '<td>'.str_replace(".",",",$valor[$this->model][$columa_a_imprimir]).'</td>';
                   else {
                   	
                   	if(isset($valor[$this->model][$columa_a_imprimir]))
                   		echo '<td>'.$valor[$this->model][$columa_a_imprimir].'</td>';
                    else 
                    	echo '<td></td>';
                   }
                }    
         
               
     echo '</tr>';
     
     
     }
 
     echo '</table>' ;
    
     echo '</body></html>';
     ob_flush();

 } 
 
 
 // función de gestión de errores
function myFunctionErrorHandler($errno, $errstr, $errfile, $errline)
{
    /* Según el típo de error, lo procesamos */
    switch ($errno) {
       case E_WARNING:
                /*
                echo "Hay un WARNING.<br />\n";
                echo "El warning es: ". $errstr ."<br />\n";
                echo "El fichero donde se ha producido el warning es: ". $errfile ."<br />\n";
                echo "La línea donde se ha producido el warning es: ". $errline ."<br />\n";
                
                */
                /* No ejecutar el gestor de errores interno de PHP, hacemos que lo pueda procesar un try catch */
                return true;
                break;
            
            case E_NOTICE:
               // echo "Hay un NOTICE:<br />\n";
                /* No ejecutar el gestor de errores interno de PHP, hacemos que lo pueda procesar un try catch */
                return true;
                break;
            
            default:
                /* Ejecuta el gestor de errores interno de PHP */
                return false;
                break;
            }
}

                                                                             

protected function procesarArchivoCsvToMysql($ruta_archivo,$model_destino,$separador_linea=';',$salto_linea='\n'){
    
    
           $this->autoRender = false;
           
           
            //Guardar en tabla temporal y matchear con persona
            
            $this->loadModel($model_destino);
            $csv_import = new $model_destino();
            $table_name = $csv_import->useTable;
            
            
            try{
            
            $this->{$model_destino}->query("TRUNCATE ".$table_name.";");
            $this->{$model_destino}->query("LOAD DATA INFILE '".$ruta_archivo."' INTO TABLE ".$table_name."
                                                                    FIELDS TERMINATED BY '".$separador_linea."'
                                                                    LINES TERMINATED BY '".$salto_linea."';");
    
             return 1;
            }catch(Exception $e){
                return 0;
            }
}

protected function ImportarArchivoCsv($model_destino="CsvImport",&$error,&$message){
        
    
        
         App::uses('Folder', 'Utility');
         $random = rand(0,1000);
         $dir_temp = new Folder(APP.'webroot/media_file');
         $this->autoRender = false;
        
        
        
        
        if (!empty($_FILES)) {
                    $tempFile = $this->request->params['form']['file']['tmp_name'];
                    $size = $this->request->params['form']['file']['size'];
                    $size_mb = ($size/1024)/1024;
                    $tamanio_maximo_mb = 10;
                    
                    $destino = $dir_temp->path .DS.$random.'-'.$this->request->params['form']['file']['name'];
                    
                    if(move_uploaded_file($tempFile,$destino)){
                        
                              //$direccion_archivo = $dir_temp->path.'//'.'resumen.csv';
                              if(file_exists($destino)){
                                  if($this->procesarArchivoCsvToMysql($destino,$model_destino,';','\n') == 1){
                                      $error = EnumError::SUCCESS;
                                      $message = 'El archivo fue importado exitosamente';
                                  }else{
                                      $error = EnumError::ERROR;
                                      $message = 'El archivo NO pudo ser importado a la base de datos'; 
                                  }
                              }else{
                                  
                                 $error = EnumError::ERROR;
                                 $message = 'El archivo NO puede ser abierto'; 
                              }
                    }else{
                    
                     $error = EnumError::ERROR;
                     $message = 'El archivo no subio correctamente cheque permisos en el servidor';
                    
                }
        }else{
            
            $error = EnumError::ERROR;
            $message = 'El archivo no subio correctamente chequee la configuracion del servidor';
            
            
        }
        
        
        
        $this->autoRender = false;
        $output = array(
            "status" => $error,
            "message" => $message,
            "content" => "CONTENT"
        );
        //$this->set($output);
        //$this->set("_serialize", array("status", "message","page_count", "content"));
        echo json_encode($output);
    }
    
    
    
    public function getErrors($errores){
        
         
        $errores_string = "";
        
        
        foreach ($errores as $error){ //recorro los errores y armo el mensaje
        
            if(!is_array($error))
                $errores_string.= "&bull; ".$error."\n";
            else
                 $errores_string.= "&bull; ".$this->getModelErrorFromArray($error)."\n"; 
            
        }
        
        return $errores_string;
    }
    
    public function getModelErrorFromArray($errores){ /*errores es un array*/
    
    
        $mensaje = '';
        
        
        foreach($errores as $key=>$error){
            
           // $mensaje.= $error[$key];
            
           if(is_array($error)>0){
                foreach($error as $detalle){
                    
                 
                        if(!is_array($detalle))
                             $mensaje.="&bull; Error:".$detalle."\n";
                        else
                             $mensaje.="&bull; Error:".$this->getModelErrorFromArray($detalle)."\n"; 
            
                }
            
           }else{
               
              $mensaje.="&bull; Error:".$error."\n";//no es array 
           } 
            
        }
        return $mensaje;     
    }
    
   
 
    protected function GetTipoNotaDebitoABCME($cliente_id_tipo_iva, $cliente_id_provincia)
        {
            
            
             App::uses('CakeSession', 'Model/Datasource');
             
             $datoEmpresa = CakeSession::read('Empresa');
             

            if ($datoEmpresa["DatoEmpresa"]["id_tipo_iva"] == EnumTipoIva::IvaResponsableInscripto ||
               $datoEmpresa["DatoEmpresa"]["id_tipo_iva"] == EnumTipoIva::IVAResponsableInscriptoAgenteDePercepcion)
            {
                if ($cliente_id_tipo_iva == EnumTipoIVA::IvaResponsableInscripto ||
                    $cliente_id_tipo_iva == EnumTipoIVA::IVAResponsableInscriptoAgenteDePercepcion)
                    return EnumTipoComprobante::NotaDebitoA;

                if ($cliente_id_tipo_iva == EnumTipoIVA::MonotribSocial ||
                    $cliente_id_tipo_iva == EnumTipoIVA::ResponsableMonotributo)
                    return EnumTipoComprobante::NotaDebitoB; //Si es monotributo

                if ($cliente_id_tipo_iva == EnumTipoIVA::IVASujetoExento)
                    return EnumTipoComprobante::NotaDebitoB; //Si el Cliente es Excento

                if ($cliente_id_tipo_iva == EnumTipoIVA::PeqContEvent ||
                    $cliente_id_tipo_iva == EnumTipoIVA::PeqContSocial ||
                    $cliente_id_tipo_iva == EnumTipoIVA::ConsumidorFinal)
                    return EnumTipoComprobante::NotaDebitoB; //Si CONSUMIDOR FINAL

                if ($cliente_id_tipo_iva == EnumTipoIVA::IVALiberadoLeyN19640) //LOS Q ESTAN EN TIERRA DEL FUEGO deberian tener esta Condicion IVA
                {
                    return EnumTipoComprobante::NotaDebitoPorOperacionesExterior; //Si el Cliente es Tierra del fuego enconces EXPORTACIOn sin IVA discriminado
                }
                if ($cliente_id_tipo_iva == EnumTipoIVA::TierraDelFuegoIndustriaDelPetroleo)
                    return EnumTipoComprobante::NotaDebitoA;

                if ($cliente_id_tipo_iva == EnumTipoIVA::ClienteDelExterrior)
                    return EnumTipoComprobante::NotaDebitoPorOperacionesExterior; //Si el Cliente es Extrajero EXPORTACIOn sin IVA discriminado

                return EnumTipoComprobante::NotaDebitoA;
                //return -1;//el cliente no tiene una condicion IVA INCORRECTA NO aplica
            }
            else if ($datoEmpresa["DatoEmpresa"]["id_tipo_iva"] == EnumTipoIVA::MonotribSocial ||
                    $datoEmpresa["DatoEmpresa"]["id_tipo_iva"] == EnumTipoIVA::ResponsableMonotributo)
            {
                return EnumTipoComprobante::NotaDebitoC;
            }
            return EnumTipoComprobante::NotaDebitoA;
        }
 
 
   
 protected function obetener_ultimo_numero_comprobante($id_tipo_comprobante,$id_punto_venta='',$id_tipo_factura=5,$suma){      /*El parametro Suma indica si suma o resta el contador*/
    
    
   //UPDATE numeracion_comprobante SET numero_actual = LAST_INSERT_ID(numero_actual + 1);
   //SELECT LAST_INSERT_ID();
   //$numero_comprobante = 0;
   
   // try{
   // $this->{$this->model}->query("UPDATE numeracion_comprobante SET numero_actual = LAST_INSERT_ID(numero_actual + 1)  WHERE id_tipo_comprobante=".$id_tipo_comprobante." and id_punto_venta=".$id_punto_venta.";");
   // $result = $this->{$this->model}->query("SELECT LAST_INSERT_ID() as numero_comprobante;");
   // $numero_comprobante =$result[0][0]["numero_comprobante"];
  // }catch(Exception $ex){
      // $numero_comprobante = 0;
      
  // }
  
  
  
  //cheuqear si tiene asociado un numero sino no dejar dar de alta
  
 	$condicion_impuesto = '';
 	if(isset($this->id_impuesto) &&  $this->id_impuesto >0 )/*Esta variable se carga en Retenciones cuando se genera el PDF en la OP*/
 		$condicion_impuesto = " and id_impuesto=".$this->id_impuesto;
  
   try{
    
       if($id_punto_venta!='')
       	$this->{$this->model}->query("UPDATE comprobante_punto_venta_numero SET numero_actual = LAST_INSERT_ID(numero_actual ".$suma." 1)  WHERE id_tipo_comprobante=".$id_tipo_comprobante." and id_punto_venta=".$id_punto_venta.$condicion_impuesto.";");
        else
        	$this->{$this->model}->query("UPDATE comprobante_punto_venta_numero SET numero_actual = LAST_INSERT_ID(numero_actual ".$suma." 1)  WHERE id_tipo_comprobante=".$id_tipo_comprobante.$condicion_impuesto."");
    
    $result = $this->{$this->model}->query("SELECT LAST_INSERT_ID() as numero_comprobante;");
    $numero_comprobante =$result[0][0]["numero_comprobante"];
    }catch(Exception $ex){
      // $numero_comprobante = 0;
      
   }
  
  return $numero_comprobante; 
   
}  


protected function setNumeroComprobante($id_tipo_comprobante,$id_punto_venta,$ultimo_comprobante_autorizado_afip){
    
    try{
    $this->{$this->model}->query("UPDATE comprobante_punto_venta_numero SET numero_actual = ".$ultimo_comprobante_autorizado_afip." WHERE id_tipo_comprobante=".$id_tipo_comprobante." and id_punto_venta=".$id_punto_venta.";");
    return true;
    }catch(Exception $ex){
        return false;   
    }
    
}


protected function getConciliados($id_cuenta_contable){
       
       $this->loadModel("CsvImportConciliacion");
       
   
       
       
       $datos = $this->CsvImportConciliacion->query("SELECT acc.id,csvc.id
                                FROM csv_import_conciliacion csvc
                                LEFT JOIN asiento_cuenta_contable acc ON acc.monto= csvc.`monto`
                                LEFT JOIN asiento ON asiento.id = acc.id_asiento

                                WHERE csvc.fecha > 0 AND acc.id_cuenta_contable=".$id_cuenta_contable."
                                AND asiento.`fecha`= csvc.fecha
                                AND acc.`conciliado` =0");
                                
       $array_conciliados = array();
       
       
       foreach($datos as $dato){
           
           $auxiliar["id_csv_import_conciliacion"] = $dato["csvc"]["id"];
           $auxiliar["id_asiento_cuenta_contable"] = $dato["acc"]["id"];
           array_push($array_conciliados,$auxiliar);
       } 
       
       return $array_conciliados;                        
   } 
   
   public function getIdCsvImportacionConciliados($array_conciliados){
       
       
     $array_id_csv = array();
       
     if($array_conciliados){
         
         foreach($array_conciliados as $elemento){
             
           array_push($array_id_csv,$elemento["id_csv_import_conciliacion"]);
             
             
             
         }
         
     }
     
     return $array_id_csv;  
       
   }
   
   
   public function getIdAsientoCuentaContablemportacionConciliados($array_conciliados){
       
       
      $array_id_csv = array();
       
     if($array_conciliados){
         
         foreach($array_conciliados as $elemento){
             
           array_push($array_id_csv,$elemento["id_asiento_cuenta_contable"]);

         }
     }
     
     return $array_id_csv;  
       
   }
   
   public function ordenarArray(&$aux,$datos){
       
       foreach($datos as $dato){
           
           array_push($aux,$dato);
       }
   }
   
   
   public function prepararRespuesta($data,&$page_count){
       
       
        $array_aux = array();
        
        
        foreach($data as $dato){
            
            array_push($array_aux,$dato);
            
        }
        
        if(isset($this->params['paging'][$this->model]['count']))
        	$total_registros = $this->params['paging'][$this->model]['count'];
        	else
        		$total_registros = 0;
        	
        		
        
       if(isset($this->paginate["paginado"])){ 		
         if($this->paginate["paginado"] == 1){
        			
         	$cantidad_borrados = $this->numrecords  - count($data);
         	
         	
        }else{
        	
        	
        	$cantidad_borrados = $total_registros - count($data)  ;
        	
        }
        		
       }	
        
        
        if(isset($this->params['paging'][$this->model]['count']))
        	$total_registros = $this->params['paging'][$this->model]['count'];
        else
        	$total_registros = 0;
        
        	
        if($this->numrecords !=0)	
        	$page_count = round( ($total_registros -  $cantidad_borrados) / $this->numrecords , 0 );
        else 
        	$page_count = 0;
        
        
        
        return $array_aux;
   }  
   
   protected function ConviertoArbolEnRecordset(&$datos,&$array_final){
            
            
                foreach($datos as $key=>$value){ 
                    
                       if($this->xlsReport == 1){ //esto lo hago xq la grilla de .net castea todo, corre las comas, sobreinterpreta que es un numero y no un string
                           $auxiliar["CuentaContable"]["saldo"]  = (string) str_replace(".",",",($datos[$key]["CuentaContable"]["debe"]- $datos[$key]["CuentaContable"]["haber"]) ); 
                           $auxiliar["CuentaContable"]["debe"]   = (string) str_replace(".",",",$datos[$key]["CuentaContable"]["debe"]); 
                           $auxiliar["CuentaContable"]["haber"]  = (string) str_replace(".",",", $datos[$key]["CuentaContable"]["haber"]); 
                       }else{
                           $auxiliar["CuentaContable"]["saldo"]  = (string) ($datos[$key]["CuentaContable"]["debe"] - $datos[$key]["CuentaContable"]["haber"]); 
                           $auxiliar["CuentaContable"]["debe"]   = (string) $datos[$key]["CuentaContable"]["debe"]; 
                           $auxiliar["CuentaContable"]["haber"]  = (string) $datos[$key]["CuentaContable"]["haber"];
                           
                       }
                       
                       
                       $auxiliar["CuentaContable"]["id"] = (string)  $datos[$key]["CuentaContable"]["id"]; 
                       $auxiliar["CuentaContable"]["codigo"] = (string)  $datos[$key]["CuentaContable"]["codigo"]; 
                       $auxiliar["CuentaContable"]["d_cuenta_contable"] = $datos[$key]["CuentaContable"]["d_cuenta_contable"]; 
                       $auxiliar["CuentaContable"]["es_mayor"] = $datos[$key]["CuentaContable"]["es_mayor"]; 
                       $auxiliar["CuentaContable"]["d_es_mayor"] = $datos[$key]["CuentaContable"]["d_es_mayor"]; 
                       
                       
                       array_push($array_final,$auxiliar);
                       
                    if ( isset($datos[$key]["children"]) && is_array($datos[$key]["children"])){
                        //si es un array sigo recorriendo
                     
                     $this->ConviertoArbolEnRecordset($datos[$key]["children"],$array_final);
                     
                     
                   
                  }
         
               }
   }   
   
   protected function BalanceGeneral($nombre_funcion="CalculaSaldo",$id_ejercicio="",$id_estado_asiento = "", $id_moneda){
            
           $fecha_desde = $this->getFromRequestOrSession('R.fecha_desde'); 
           $fecha_hasta = $this->getFromRequestOrSession('R.fecha_hasta'); 
           
           if($id_ejercicio == "")
            $id_ejercicio = $this->getFromRequestOrSession('R.id_ejercicio_contable'); 
           
           $id_periodo = $this->getFromRequestOrSession('R.id_periodo_contable');
           $get_tree = $this->getFromRequestOrSession('R.get_tree');
           
           if($id_estado_asiento == "")
            $id_estado_asiento = $this->getFromRequestOrSession('R.id_estado_asiento');
           
           if($id_moneda == "")
            $id_moneda = $this->getFromRequestOrSession('R.id_moneda');
           
           $solo_imputables = $this->getFromRequestOrSession('R.solo_imputables');
           
           if($solo_imputables == "")
            $solo_imputables = 0;
           
           $this->loadModel("CuentaContable");
           $this->loadModel("DatoEmpresa");
          
          
          //$id_ejercicio = 20;
          //$fecha_hasta = '2016-08-03';
          $id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda);
          $campo_valor = $this->CuentaContable->getFieldByTipoMoneda($id_tipo_moneda);//obtengo el campo en la cual expresar el reporte
          
          $conditions = array();
          
          
        
           
           if($id_ejercicio!=""){
             $this->loadModel("EjercicioContable");
             $fecha_inicio = $this->EjercicioContable->getFechaInicio($id_ejercicio);
             $fecha_fin = $this->EjercicioContable->getFechaFin($id_ejercicio);;
               
           }elseif($id_periodo !=""){
              $this->loadModel("PeriodoContable"); 
             // $id_periodo = 145;
              $fecha_inicio = $this->PeriodoContable->getFechaInicio($id_periodo);
              $fecha_fin = $this->PeriodoContable->getFechaFin($id_periodo);
               
           }else{
               
              $fecha_inicio = $fecha_desde; 
              $fecha_fin = $fecha_hasta; 
               
           }
           
           //$fecha_inicio = '2016-11-29';
           //$fecha_hasta = '2016-12-29';
           
           $data = $this->CuentaContable->find('threaded', array(
                        'conditions'=>$conditions,   
                        'contain' => false,                        // 'fields' => array('id', 'CuentaContable.parent_id'),
                        'order' => array('CuentaContable.codigo ASC'), // or array('id ASC')
                         // or array('id ASC')
                    ));
                    
           $this->CuentaContable->BorroAsociados($data);
           
           //$fecha_inicio = '2016-06-30';    
          // $fecha_hasta = '2016-07-30';
           $a='';    
           $this->CuentaContable->{$nombre_funcion}($data,$a,$fecha_inicio,$fecha_fin,$id_estado_asiento,$campo_valor);    
           $this->CuentaContable->ModificaDescripcion($data);
           
           
           $array_final = array(); 
           $this->ConviertoArbolEnRecordset($data,$array_final); 
           $this->data = $array_final; //se lo asigno para que si se llama por excel imprima el listado y no el arbol
           
              
           
           
           $page_count = 0;
           
           $output = array(
                "status" =>EnumError::SUCCESS,
                "message" => "list",
                "content" => $data,
                "page_count" =>$page_count
            );
            $this->set($output);
            $this->set("_serialize", array("status", "message","page_count", "content")); 
            
            
        }
        
        
        protected function BalanceSumasYSaldos($externo = 0,$incluye_mayores="",$id_ejercicio="",$id_estado_asiento = "",$id_moneda= ""){
           
           set_error_handler(array(&$this, 'myFunctionErrorHandler'), E_WARNING); 
            
            $this->BalanceGeneral("CalculaDebeHaber",$id_ejercicio,$id_estado_asiento,$id_moneda); //llamo al balance general  y  le digo que use la funcion   CalculaDebeHaber
            ob_clean();//limpio el flujo si es que hubo
            $array_final = array();
            $this->ConviertoArbolEnRecordset($this->data,$array_final);      //En la variable $this->data almaceno el resultado de BalanceGeneral
            //$array_final es el array SIN los saldos iniciales
             if($incluye_mayores == "")
                $incluye_mayores = $this->getFromRequestOrSession('R.incluye_mayores');
        
            $aux = $array_final;  
             
            if($incluye_mayores == 0){ //pregunto por el filtro mayores si me lo manda en 0 significa que no los quiere. Por default el reporte devuelve siempre los mayores
                
            foreach($array_final  as  $key=>$item){
                
               
                if($item["CuentaContable"]["es_mayor"] == 1)
                    unset($array_final[$key]);
                
                
                
                
            }
            
            $aux = array();
            $this->ordenarArray($aux,$array_final);
            
            }
            
            
            
            $this->data = $aux;
            
            
            
            
            
            
            
            if($externo == 0){
            
                $output = array(
                        "status" =>EnumError::SUCCESS,
                        "message" => "list",
                        "content" => $this->data,
                        "page_count" =>"1"
                );
                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "page_count", "content")); 
            
            }else{
                
                return $this->data;
            }
            
            
        }
        
        
   protected function getCabeceraAsientoGeneradoPorComprobante($id_sistema,$factura){
        
        $this->loadModel("PeriodoContable");
        $this->loadModel("TipoComprobante");
        $this->loadModel("EjercicioContable");
        
        $cabecera_asiento["Asiento"]["d_asiento"] = $factura["TipoComprobante"]["codigo_tipo_comprobante"]. " ".$this->Comprobante->GetNumberComprobante(0,$factura["Comprobante"]["nro_comprobante"]);
        
        
        if(!is_null($factura["Comprobante"]["fecha_contable"]))
            $cabecera_asiento["Asiento"]["fecha"] = $factura["Comprobante"]["fecha_contable"];//le asigno la fecha contable del comprobante   
        else
             $cabecera_asiento["Asiento"]["fecha"] = $factura["Comprobante"]["fecha_generacion"];
             
             
        $cabecera_asiento["Asiento"]["id_comprobante"] = $factura["Comprobante"]["id"];
        $cabecera_asiento["Asiento"]["valor_moneda"] = $factura["Comprobante"]["valor_moneda"];
        $cabecera_asiento["Asiento"]["id_moneda"] = $factura["Comprobante"]["id_moneda"];
        $cabecera_asiento["Asiento"]["id_clase_asiento"] = EnumClaseAsiento::Basico;
       
        
        if(isset($factura["Comprobante"]["valor_moneda2"]))
        	$cabecera_asiento["Asiento"]["valor_moneda2"] = $factura["Comprobante"]["valor_moneda2"];
        	
        
        if(isset($factura["Comprobante"]["valor_moneda3"]))
        	$cabecera_asiento["Asiento"]["valor_moneda3"] = $factura["Comprobante"]["valor_moneda3"];
        		
        	
        $id_estado_asiento_default = $this->TipoComprobante->getEstadoAsientoDefault($factura["Comprobante"]["id_tipo_comprobante"]);
        
        if($id_estado_asiento_default>0)
            $cabecera_asiento["Asiento"]["id_estado_asiento"] = $id_estado_asiento_default;
        else
            $cabecera_asiento["Asiento"]["id_estado_asiento"] = EnumEstadoAsiento::EnRevision;
        
        
        
        $cabecera_asiento["Asiento"]["manual"] = 0;

        if($id_sistema == EnumSistema::VENTAS)
            $cabecera_asiento["Asiento"]["id_agrupacion_asiento"] = EnumAgrupacionAsiento::ventas;
        elseif($id_sistema == EnumSistema::COMPRAS)
            $cabecera_asiento["Asiento"]["id_agrupacion_asiento"] = EnumAgrupacionAsiento::compras;
        elseif($id_sistema == EnumSistema::CAJA)
            $cabecera_asiento["Asiento"]["id_agrupacion_asiento"] = EnumAgrupacionAsiento::fondos;
            
        $cabecera_asiento["Asiento"]["id_asiento_efecto"] = EnumAsientoEfecto::Fiscal;  

        
        
        $id_ejercicio_contable = $this->EjercicioContable->GetEjercicioPorFecha($cabecera_asiento["Asiento"]["fecha"],0,1);
        $periodo_seleccionado = $this->PeriodoContable->GetPeriodoPorFecha($cabecera_asiento["Asiento"]["fecha"],0,$id_ejercicio_contable["EjercicioContable"]["id"]);//busco el periodo en base a la fecha contable
        
        
        if($periodo_seleccionado == 0){ //sino encuentra un periodo por default pone el actual
            $periodo_actual =  $this->PeriodoContable->getPeriodoActual();
            
            if($periodo_actual == 0)
            	return 0;//si tiene falla el getPeriodoActual devuelvo 0
            
            $cabecera_asiento["Asiento"]["id_periodo_contable"] = $periodo_actual["id"];
            $cabecera_asiento["Asiento"]["id_ejercicio_contable"] = $periodo_actual["id_ejercicio_contable"];  
        }else{
            
            $cabecera_asiento["Asiento"]["id_periodo_contable"] = $periodo_seleccionado["id"];
            $cabecera_asiento["Asiento"]["id_ejercicio_contable"] = $periodo_seleccionado["id_ejercicio_contable"];
        }
    
        
     //$this->Comprobante->CalcularMonedaBaseSecundaria($cabecera_asiento,"Asiento");
     return $cabecera_asiento;
    }
    
    
    protected function generaCabeceraAsientoManual($CabeceraAsiento){
        
        
        
    }
    
    protected function ChequeoCotizacionBase($id){//cheque que si es la base tenga cotizacion 1. esta funcion se usa desde datoempresa y dsd moneda
        
        $this->loadModel("Moneda");
        
        $datoEmpresa = $this->Session->read('Empresa');
        $id_moneda_base  = $datoEmpresa["DatoEmpresa"]["id_moneda"];
        
        if(isset($this->request->data["Moneda"]["cotizacion"]))
            $moneda_cotizacion = $this->request->data["Moneda"]["cotizacion"];
        else
            {//busco la cotizacion de la moneda que la modificacion de datoempresa me dice que es la base
            
                $moneda = $this->Moneda->find('first',array('conditions'=>array('Moneda.id'=>$id)));
                $moneda_cotizacion = $moneda["Moneda"]["cotizacion"];
                
            }
        
        
        if($id == $id_moneda_base && $moneda_cotizacion != 1){
            
            return 1;
            
            
        }elseif(!isset($this->request->data["Moneda"]["cotizacion"]) && isset($this->request->data["DatoEmpresa"]) && $moneda_cotizacion !=1  ){
            
            return 1;      
        }else{
            return 0;
        }
     
        
    }
    
    
    public function getAnotations(){
        App::import('Lib', 'AnnotationsManager');
        App::uses('SessionHelper', 'View/Helper');
         //Obtengo las annotations del metodo de la accion
         
           //Obtengo las annotations de la clase
            $annotations = AnnotationsManager::getClassAnnotations("ClientesController");
            $securedAnnotations = SecurityManager::getSecuredAnnotations($annotations);
            
            $annotations = AnnotationsManager::getMethodAnnotations($this->name."Controller", "index");
            $securedAnnotations = array_merge($securedAnnotations, SecurityManager::getSecuredAnnotations($annotations));
                  echo str_replace("CONSULTA_","",$securedAnnotations[1]);
                  echo "-";
                  echo strtoupper($this->name);
                     echo "-";
                     echo $this->name."Controller";
                     echo "\r\n";
                  die();
        
    }
    
    
     protected function excelExport($vista="default",$metodo = "index",$titulo_repore= "Listado"){
        
       //TODO: Chequear que la vista exista  
        $this->paginado =0;
        
         $this->ExportarExcel($vista,$metodo,$titulo_repore);
                 
        
        
    }
    
    protected function importarArchivo(&$error,&$message,&$destino_archivo){
    	
    	
    	
    	//$id_impuesto = 29;
    	$this->layout = 'ajax';       //vista le digo q no busque en la VISTAS
    
    	
    	
    	set_time_limit(1200);
    	

    	
    	
    	App::uses('Folder', 'Utility');
    	$random = rand(0,1000000);
    	$dir_temp = new Folder(APP.'webroot/media_file');
    	$this->autoRender = false;
    	$destino = '';
    	
    	

    		if (!empty($_FILES)) {
    			
    			
    			
    			$tempFile = $this->request->params['form']['file']['tmp_name'];
    			$size = $this->request->params['form']['file']['size'];
    			$size_mb = ($size/1024)/1024;
    			$tamanio_maximo_mb = 10;
    			
    			$destino = $dir_temp->path .'//'.$random.'-'.$this->request->params['form']['file']['name'];
    			
    			if(move_uploaded_file($tempFile,$destino)){
    				
    			
    		
    				
    	
    					
    				if(file_exists($destino)){
    						
    						$error = EnumError::SUCCESS;
    						
    						
    						
    					}else{
    						
    						$error = EnumError::ERROR;
    						$message = 'El archivo NO puede ser abierto'.$direccion_archivo;
    					}
    					
    		
    				
    				
    			}else{
    				
    				$error = EnumError::ERROR;
    				$message = 'El archivo no subio correctamente cheque permisos en el servidor';
    				
    			}
    		}else{
    			
    			$error = EnumError::ERROR;
    			$message = 'El archivo no subio correctamente cheque la configuracion del servidor.MAX_UPLOAD_SIZE='.ini_get('upload_max_filesize').'SHOW VARIABLES LIKE "secure_file_priv"; debe aparecer vacio';
    			
    			
    		}
    	
    	
    }
    
    
    protected function cerrarComprobantesAsociados($comprobante){
   	
   	
   }
   
   
  
   
   
   
   
   
   function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
   	$sort_col = array();
   	foreach ($arr as $key=> $row) {
   		$sort_col[$key] = $row[$col];
   	}
   	
   	array_multisort($sort_col, $dir, $arr);
   }
   
   
 
   
   protected function addWebVisualFilters($formBuilder){
   	
   	
   	
   	$formBuilder->addFilterBeginRow();
   	//$formBuilder->addFilterInput('codigo', 'C&oacute;digo', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $codigo));
   	$formBuilder->addFilterInput('fecha_contable', 'Fecha', array('class'=>'control-group span5'), array('type' => 'text', 'class' => 'datepicker', 'label' => false, 'value' => $this->getFromRequestOrSession($this->model.'.fecha_contable') ));
   	$formBuilder->addFilterInput('fecha_contable_hasta', 'Fecha Hasta', array('class'=>'control-group span5'), array('type' => 'text', 'class' => 'datepicker', 'label' => false, 'value' => $this->getFromRequestOrSession($this->model.'.fecha_contable_hasta')));
   	$formBuilder->addFilterInput('nro_comprobante', 'Nro. Comprobante', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $this->getFromRequestOrSession($this->model.'.nro_comprobante')));
   	$formBuilder->addFilterEndRow();
   	
   }
    
    
   protected function preparaHtmLFormBuilder($point,$formBuilderInit=array(),$layout= "",$print_menu=true){
   	
   	
   

   
   	
   	 if($this->request->action == "index" &&  $this->RequestHandler->ext != 'json')
   	 {//si no es json
   	 	App::import('Lib', 'FormBuilder');
   	 	$formBuilder = new FormBuilder();
   	 	$formBuilder->setController($point);
   	 	
   	 	
   	 	
   	 	
   	 	/*
   	 	//Filters
   	 	$formBuilder->addFilterBeginRow();
   	 	//$formBuilder->addFilterInput('codigo', 'C&oacute;digo', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $codigo));
   	 	$formBuilder->addFilterInput('nombre', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $nombre));
   	 	$formBuilder->addFilterEndRow();
   	 	*/
   	 	
   	 	ob_start();
   	 	$route = APP.'View'.DS.$point->name.DS.'json'.DS.'index.ctp';
   	 	include_once($route);
   	 	ob_clean();
   	 	
   	 	
   	 	$this->addWebVisualFilters($formBuilder);
   	 	
  
   	 	
   	 	$this->array_sort_by_column($model, 'order');
   	 	
   	 	//seteo las propiedad del formbuilder
   	 	foreach($formBuilderInit as $key=>$propiedad_form_builder){
   	 		
   	 	
   	 
   	 			
   	 			
   	 		$formBuilder->{$key}($propiedad_form_builder);
   	 		
   	 	
   	 	}
   	 	
   	 

   	 	$title = $point->title_form;
   	 	
   	 	$formBuilder->setDataListado($point, $title, $this->sub_title_form, $point->model, $point->name, $point->data);
   	 	//Filters
   	 	
   	 	
   	 	

   	 	
   	 	foreach($model as $key=>$field){
   	 		
   	 		if($model[$key]["show_grid"] == 1){
   	 			
   	 			if(isset($model[$key]["web_width"]))
   	 				$web_width = $model[$key]["web_width"]."%";
   	 			else
   	 				$web_width = "";
   	 			
   	 			
   	 				$formBuilder->addHeader($model[$key]["header_display"], $this->model.".".$key, $web_width);
   	 			
   	 				$formBuilder->addField($point->model, $key);
   	 		}
   	 	}
   	 	
   	
   	 	if($layout!="")
   	 		$this->layout = $layout;
   	 	
   	 	
   	 	//$formBuilder->setController($point);
   	 	//$a = $formBuilder->tableListado();
   	 	
   	 	
   	 	//$a = $formBuilder->tableListado();
   	 	
   	 	
   	 	
   	 	
   	 	$this->set('print_menu',$print_menu);
   	 	$this->set('abm',$formBuilder);
   	 	$this->render('/FormBuilder/index');
   	 	
   	 	
   	 	
   	 	
   	 	
   	 	
   	 
   	 }
   }
   
   
   protected function preparaHTMLListadoFormBuilder($point,$title="",$formBuilderInit=array()){
   	
   	
   	
   	
   	
  
   		App::import('Lib', 'FormBuilder');
   		$formBuilder = new FormBuilder();
   		$formBuilder->setController($point);
   		
   		
   		$formBuilder->setDataListado($point, $title, 'Datos de los Personas', $point->model, $point->name, $point->data);
   		
   		ob_start();
   		$route = APP.'View'.DS.$point->name.DS.'json'.DS.'default.ctp';
   		include_once($route);
   		ob_clean();
   		
   		
   		
   		
   		
   		//seteo las propiedad del formbuilder
   		foreach($formBuilderInit as $key=>$propiedad_form_builder){
   			
   			
   			
   			
   			
   			$formBuilder->{$key}($propiedad_form_builder);
   			
   			
   		}
   		
   		
   		
   		$title = $point->title_form;

   		//Filters
   		
   		
   		foreach($model as $key=>$field){
   			
   			if($model[$key]["show_grid"] == 1){
   				
   				if(isset($model[$key]["web_width"]))
   					$web_width = $model[$key]["web_width"]."%";
   					else
   						$web_width = "";
   						
   						
   						$formBuilder->addHeader($model[$key]["header_display"], $point->name.".".$key, $web_width);
   						
   						$formBuilder->addField($point->model, $key);
   			}
   		}
   		
   		
   		
   		ob_start();
   		
   		$formBuilder->tableListado();//devuelve listado
   		
   		return ob_get_clean();
   		
   		
   		
   	
   		
   	
   		
   		
   	
   }
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   public function getCache(){
   	
   	
   		$this->index();
   		
   		
   		$output = array(
   				"status" =>EnumError::SUCCESS,
   				"message" => "list",
   				"content" => $this->data,
   				"page_count" =>0
   		);
   		
   		
   		$entidad2["entidad"]["controllerName"] = $this->name;
   		$entidad2["entidad"]["index"] = $this->data;
   		$entidad2["entidad"]["modelName"] = $this->model;
   		
   		if(!file_exists(EnumCacheName::getCacheIndexTablasEstaticaSistema))
   		{
   			
   			$array_index = array();
   			array_push($array_index, $entidad2);
   			$getIndexCache = $array_index;
   			
   			
   		}else{
   			$getIndexCache = unserialize(file_get_contents(EnumCacheName::getCacheIndexTablasEstaticaSistema));
   			
   			array_push($getIndexCache, $entidad2);
   			
   		}
   		
   		
   		
   		

   		
   		
   		
   		
   		
   		file_put_contents(EnumCacheName::getCacheIndexTablasEstaticaSistema,serialize($getIndexCache));

   }

   protected function generateRandomString($length = 10) {
   	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ()$!|@=?';
   	$charactersLength = strlen($characters);
   	$randomString = '';
   	for ($i = 0; $i < $length; $i++) {
   		$randomString .= $characters[rand(0, $charactersLength - 1)];
   	}
   	return $randomString;
   }
   
   
   
   public function afterScaffoldSave($method){
   	/*
   	if((isset($this->tiene_cache_cliente) && $this->tiene_cache_cliente == 1)){//regenero la cache
   			
   		$this->generar_cache = 1;
   		$this->index();
   		
   	}*/
   }

}



