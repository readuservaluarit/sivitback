<?php        
        
        App::uses('ArticuloRelacion', 'Model');  
        $articulo_relacion_obj = new ArticuloRelacion();
        $articulo_relacion_obj->setVirtualFieldsForView();
        
        App::import('Model','Comprobante');
  		App::import('Lib','SecurityManager');
		$comprobante = new Comprobante();
		$stock = $comprobante->getActivoModulo(EnumModulo::STOCK);
     
      
        $articulo_relacion_obj->SetFieldsToView($articulo_relacion_obj->virtual_fields_view,$model);
        
        
        $model["codigo_hijo"]["show_grid"] = 1;
        $model["codigo_hijo"]["header_display"] = "C&oacute;digo";
        $model["codigo_hijo"]["order"] = "1";
		$model["codigo_hijo"]["width"] = "20"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["codigo_hijo"]["text_colour"] = "#000000";
       
        $model["d_producto_hijo"]["show_grid"] = 1;
        $model["d_producto_hijo"]["header_display"] = "Descripci&oacute;n";
        $model["d_producto_hijo"]["order"] = "2";
		$model["d_producto_hijo"]["width"] = "40"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["d_producto_hijo"]["text_colour"] = "#0000FF";
         
        $model["cantidad_hijo"]["show_grid"] = 1;
        $model["cantidad_hijo"]["header_display"] = "Cantidad";
        $model["cantidad_hijo"]["order"] = "3";
		$model["cantidad_hijo"]["width"] = "8";
        $model["cantidad_hijo"]["text_colour"] = "#0000FF";
        
        $model["costo_unitario_hijo"]["show_grid"] = 1;
        $model["costo_unitario_hijo"]["header_display"] = "Costo Unitario";
        $model["costo_unitario_hijo"]["order"] = "4";
		$model["costo_unitario_hijo"]["width"] = "8";
        $model["costo_unitario_hijo"]["text_colour"] = "#0000FF";
        
        $model["costo_hijo"]["show_grid"] = 1;
        $model["costo_hijo"]["header_display"] = "Costo";
        $model["costo_hijo"]["order"] = "5";
		$model["costo_hijo"]["width"] = "8";
        $model["costo_hijo"]["text_colour"] = "#0000FF";
        
        
      
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => $model_name,
                    "content" => $model
                );
        echo json_encode($output);
?>