SELECT comprobante.nro_comprobante AS 'Nro. OA',
 persona.`razon_social` AS 'Cliente' ,
 producto.`codigo` AS 'Codigo',
 comprobante_item.`cantidad` AS 'Cantidad', 
comprobante.`fecha_cierre` AS 'Fecha Cierre de OA',
comprobante.fecha_entrega AS 'Fecha Entrega Comp Armado',
comprobante.`fecha_generacion` AS 'Fecha Generacion OA',
auditoria_comprobante.`fecha` AS 'Fecha Completo',
usuario.`nombre` AS 'Usuario de Carga Completo'

FROM comprobante_item

LEFT JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`
LEFT JOIN comprobante_item piitem ON piitem.id = comprobante_item.`id_comprobante_item_origen`
LEFT JOIN comprobante pedido ON pedido.id = piitem.`id_comprobante`
LEFT JOIN persona ON pedido.`id_persona` = persona.`id`
LEFT JOIN producto ON producto.id = comprobante_item.`id_producto`
LEFT JOIN auditoria_comprobante ON auditoria_comprobante.`id_comprobante` = comprobante.`id`
LEFT JOIN usuario ON usuario.`id` = auditoria_comprobante.`id_usuario`


WHERE comprobante.id_tipo_comprobante = 104
AND auditoria_comprobante.fecha>='2020-01-6'
AND auditoria_comprobante.fecha<='2020-01-10'

/*AND auditoria_comprobante.`id_estado_comprobrante_anterior` = 1/*ABIERTO*/
AND auditoria_comprobante.`id_estado_comprobante_nuevo` = 5/*COMPLETO*/