<?php
App::uses('ComprobantesController', 'Controller');


  /**
    * @secured(CONSULTA_ORDEN_COMPRA)
  */
class OrdenCompraController extends ComprobantesController {
    
    public $name = EnumController::OrdenCompra;
    public $model = 'Comprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $requiere_impuestos = 1; //si el comprobante acepta impuesto, esto llama a las funciones de IVA E IMPUESTOS
   
    /**
    * @secured(CONSULTA_ORDEN_COMPRA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros($this->model);
            
        $array_conditions = array('Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante','ComprobanteImpuesto'=>array('Impuesto'=>array('Sistema','ImpuestoGeografia')),'PuntoVenta','Sucursal'=>array('Provincia','Pais'),'Usuario'); //contacto es la sucursal
            
        //Si pasa el ID traigo diferentes models relacionados,sino lo basico
        /*
        if(in_array('Comprobante.id',$conditions))//elijo los models a traer dependiendo si es consulta o index
            $array_conditions = array('Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante','ComprobanteImpuesto'=>array('Impuesto'=>array('Sistema','ImpuestoGeografia')),'PuntoVenta');
        else
             $array_conditions = array('Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante','ComprobanteImpuesto','PuntoVenta');
        */ 
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>$array_conditions,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.nro_comprobante desc'
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
		//Headers
        $formBuilder->addHeader('id', 'cotizacion.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'cotizacion.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
        	
        	
            
             //$valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
             //unset($valor['ComprobanteItem']);
             
             if(isset($valor['TipoComprobante'])){
                 $valor[$this->model]['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                 $valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
                 unset($valor['TipoComprobante']);
             }
             
             if(isset($valor['PuntoVenta'])){
                $valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
             unset($valor['PuntoVenta']);
             }
             /*
             foreach($valor[$this->model]['ComprobanteItem'] as &$producto ){
                 
                 $producto["d_producto"] = $producto["Producto"]["d_producto"];
                 $producto["codigo_producto"] = $this->getProductoCodigo($producto);
				 unset($producto["Producto"]);
             }
             */
           
             if(isset($valor['Moneda'])){
                 $valor[$this->model]['d_moneda'] = $valor['Moneda']['simbolo'];
                 unset($valor['Moneda']);
             }
             
              if(isset($valor['ComprobanteImpuesto'])){

                   

                        $valor[$this->model]['ComprobanteImpuestoIva'] =  $this->getImpuestos($valor['ComprobanteImpuesto'],$valor["Comprobante"]["id"],array(EnumSistema::COMPRAS),array(EnumImpuesto::IVACOMPRAS),EnumTipoImpuesto::IVA_IMPUESTO_VALOR_AGREGADO);
                        $valor[$this->model]['ComprobanteIvaTotal'] = (string) $this->TotalImpuestos($valor[$this->model]['ComprobanteImpuestoIva']);

             }
             
             if(isset($valor['EstadoComprobante'])){
                $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
                unset($valor['EstadoComprobante']);
             }
             if(isset($valor['CondicionPago'])){
                $valor[$this->model]['d_condicion_pago'] = $valor['CondicionPago']['d_condicion_pago'];
                unset($valor['CondicionPago']);
             }
             
             if(isset($valor['Persona'])){
            
            
                 if($valor['Persona']['id']== null){
                     
                    $valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social']; 
                 }else{
                     
                     $valor[$this->model]['razon_social'] =   $valor['Persona']['razon_social'];
					 $valor[$this->model]['id_tipo_iva'] =    $valor['Persona']['id_tipo_iva'];
                     $valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
                     $valor[$this->model]['id_lista_precio_defecto_persona'] = $valor['Persona']['id_lista_precio'];
                 }
                 
                 unset($valor['Persona']);
             }
             
              if(isset($valor['DepositoOrigen'])){
                  $valor['Comprobante']['d_deposito_origen'] =$valor['DepositoOrigen']['d_deposito_origen'];
              }
                 
              if(isset($valor['DepositoDestino'])){
                    $valor['Comprobante']['d_deposito_destino'] =$valor['DepositoDestino']['d_deposito_destino'];
                 }
             
               if(isset($valor['TipoComprobante'])){
                  $valor['Comprobante']['d_tipo_comprobante'] =$valor['TipoComprobante']['d_tipo_comprobante'];
              }
              
              if(isset($valor['Usuario'])){
              	$valor['Comprobante']['d_usuario'] =$valor['Usuario']['nombre'];
              }
              
           /* if(isset())
              $comprobantes_relacionados = $this->getComprobantesRelacionados($id);
           */
           
           unset($valor['Sucursal']);
           unset($valor['ComprobanteImpuesto']);
         
            $this->{$this->model}->formatearFechas($valor);
            
        }
        
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $this->data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    /**
    * @secured(ABM_USUARIOS)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->Cotizacion->id = $id;
            $this->Cotizacion->contain();
            $this->request->data = $this->Cotizacion->read();
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Cotizacion');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de OrdenCompra');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de OrdenCompra');
        else
            $formBuilder->setTituloForm('Consulta de OrdenCompra');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('cuit', 'Cuit', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('calle', 'Calle', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  
        $formBuilder->addFormInput('tel', 'Tel&eacute;fono', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('ciudad', 'Ciudad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descuento', 'Descuento', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descripcion', 'Descripcion', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('localidad', 'Localidad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
         
        
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'ClienteAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('ClienteRazonSocial', 'Debe ingresar una Raz&oacute;n Social');
                validator.validateRequired('ClienteCuit', 'Debe ingresar un CUIT');
                validator.validateNumeric('ClienteDescuento', 'El Descuento ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_cuit',
                        'data': 'cuit=' + $('#ClienteCuit').val()+'&id='+$('#ClienteId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('ClienteCuit','El Cuit que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

    /**
    * @secured(ADD_ORDEN_COMPRA)
    */
    public function add(){
        
  
        
     parent::add();    
        
    }
    
    
    /**
    * @secured(MODIFICACION_ORDEN_COMPRA)
    */
    public function edit($id){
        
        
        
         
     parent::edit($id);  
     return;  
        
    }
    
    
    /**
    * @secured(BAJA_ORDEN_COMPRA)
    */
    public function delete($id){
     
     
     parent::delete($id);    
        
    }
    
   /**
    * @secured(BAJA_ORDEN_COMPRA)
    */
    public function deleteItem($id,$externo=1){
        

        parent::deleteItem($id,$externo);    
        
    }
     
    
      /**
    * @secured(CONSULTA_ORDEN_COMPRA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

    }
    
  
    /**
    * @secured(REPORTE_ORDEN_COMPRA)
    */
    public function pdfExport($id,$vista=''){
        
    
    $this->loadModel("Comprobante");
    $factura = $this->Comprobante->find('first', array(
                                                        'conditions' => array('Comprobante.id' => $id),
                                                        'contain' =>array('TipoComprobante')
                                                        ));
                                                        
     $comprobantes_relacionados = $this->getComprobantesRelacionados($id);
     
                                       
      if($factura){
  
        
        
        parent::pdfExport($id,$vista);//envio la vista con la letra que corresponde A o B
      }else{
          
          
      }
     
        
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
        
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);
    }

    
    
     /**
    * @secured(CONSULTA_ORDEN_COMPRA)
  */
    public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados=1){
        
        
        parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante);
        
    }
    
    /**
    * @secured(MODIFICACION_ORDEN_COMPRA)
    */
    public function Anular($id_comprobante){
     
     $this->loadModel("Comprobante");
        $this->loadModel("Asiento");
        $this->loadModel("Cheque");
        
        
          $oc = $this->Comprobante->find('first', array(
                    'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                    'contain' => array('ComprobanteValor','Asiento','ChequeEntrada')
                ));
                
          $ds = $this->Comprobante->getdatasource(); 
      
          
          
          
                   
            if( $oc && $oc["Comprobante"]["id_estado_comprobante"]!= EnumEstadoComprobante::Anulado ){
            	
            	
            	$comprobante_relacionados = $this->getComprobantesRelacionados($id_comprobante,0,1,1,array(EnumEstadoComprobante::Anulado,EnumEstadoComprobante::Cancelado));
            	
                
            	if(count($comprobante_relacionados)== 0){
                
            		
            		try{
                    $ds->begin();
                        
                        $this->Comprobante->updateAll(
                                                        array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
                                                        array('Comprobante.id' => $id_comprobante) );        
                         
                        
                        
                        
                     $orden_compra = $this->Comprobante->find('first', array(
                        		'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                        		'contain' => false
                        ));
                        
                     $this->Auditar($id_comprobante,$orden_compra);
                    
                     
                     
                    
                    
                    $ds->commit(); 
                    $tipo = EnumError::SUCCESS;
                    $mensaje = "Se Anulo correctamente el comprobante";
                       
		               }catch(Exception $e){ 
		                
		                $ds->rollback();
		                $tipo = EnumError::ERROR;
		                $mensaje = "No es posible anular el Comprobante ERROR:".$e->getMessage();
		                
		                
		              }  
              
            	} else{
            		
            		$tipo = EnumError::ERROR;
            		$mensaje = "No es posible anular el Comprobante ya se encuentra asociado a otros comprobantes. (Ver comprobantes Relacionados).";
            	}
              
              
              
            
            }else{
                
               $tipo = EnumError::ERROR;
               $mensaje = "No es posible anular el Comprobante ya se encuenta ANULADO";
            }
            
              $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                );      
              echo json_encode($output);
              die(); 
    }
    
    
    
    /**
     * @secured(BTN_EXCEL_ORDENCOMPRA)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Ordenes de Compra");
    	
    	//TEST COMMITEALO GIT
    }
    
    
    public function CalcularImpuestos($id_comprobante,$id_tipo_impuesto='',$error=0,$message=''){
    	
    	return 0;
    }
    
     
    
    
  
}
?>