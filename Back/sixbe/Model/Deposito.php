<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class Deposito extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'deposito';
    //public $actsAs = array('Tree');
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
    
		'd_deposito' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar un Nombre.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
            'unico' => array(
            'rule' => array('isUnique'),
            'message' => 'El Deposito ya existe debe escribir uno que no exista'
        )
		),
        'codigo' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Debe ingresar un Codigo.',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'unico' => array(
            'rule' => array('isUnique'),
            'message' => 'El C&oacute;digo ya existe debe escribir uno que no exista'
        )
        ),
        
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	/*public $hasMany = array(
		'Region' => array(
			'className' => 'Region',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public $childRecordMessage = "Existe por lo menos una regi&oacute;n, predio, establecimiento o usuario relacionado/a con la jurisdicci&oacute;n.";
    
    */
    
     public $belongsTo = array(
        'Persona' => array(
            'className' => 'Persona',
            'foreignKey' => 'id_persona',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'DepositoCategoria' => array(
            'className' => 'DepositoCategoria',
            'foreignKey' => 'id_deposito_categoria',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'DatoEmpresa' => array(
            'className' => 'DatoEmpresa',
            'foreignKey' => 'id_dato_empresa',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'DepositoParent' => array(
            'className' => 'Deposito',
            'foreignKey' => 'parent_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        
   );
   
   public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
        
         
         
         $this->virtual_fields_view = array(
         
         "d_deposito_categoria"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "sub_deposito"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_persona"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_deposito_padre"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_dato_empresa"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null)
         
         
         );

         
         }
         
         
         
         
  public function get($id_deposito){
  	
  	
  	$deposito = $this->find('first', array(
  			'conditions' => array('Deposito.id' => $id_deposito),
  			'contain' =>false
  	)); 
  	
  	
  	return $deposito;
  	
  	
  	
  }
         
         
         
   
   
   /*
   public $hasMany = array(
        'DepositoHijo' => array(
            'className' => 'Deposito',
            'foreignKey' => 'id_deposito',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        );
    
   */  
}
