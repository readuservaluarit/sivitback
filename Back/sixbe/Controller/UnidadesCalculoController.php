<?php

/**
* @secured(CONSULTA_UNIDAD)
*/
class UnidadesCalculoController extends AppController {
    public $name = 'UnidadCalculoesCalculo';
    public $model = 'UnidadCalculo';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
/**
* @secured(CONSULTA_UNIDAD)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
        $nombre = strtolower($this->getFromRequestOrSession('UnidadCalculo.d_UNIDAD'));
        
        $conditions = array(); 
        if ($nombre != "") {
            $conditions = array('LOWER(UnidadCalculo.d_UNIDAD) LIKE' => '%' . $nombre . '%');
        }

        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => 10,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de UnidadCalculoesCalculo', 'Datos de las UnidadCalculoesCalculo', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('UnidadCalculo.d_UNIDAD', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'id', "10%");
                $formBuilder->addHeader('Nombre', 'd_UNIDAD', "90%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_UNIDAD');

                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->UnidadCalculo->id = $id;
            $this->request->data = $this->UnidadCalculo->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('UnidadCalculoesCalculo');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de UnidadCalculoesCalculo');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de UnidadCalculoesCalculo');
        else
            $formBuilder->setTituloForm('Consulta de UnidadCalculoesCalculo');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_UNIDAD', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));  
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'UnidadCalculoAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('UnidadCalculoDUnidadCalculo', 'Debe ingresar un nombre');


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ALTA_UNIDAD)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            if ($this->UnidadCalculo->save($this->request->data))
                $this->Session->setFlash('La UnidadCalculo ha sido creada exitosamente.', 'success'); 
            else
                $this->Session->setFlash('Ha ocurrido un error,la UnidadCalculo no ha podido ser creada.', 'error');

            $this->redirect(array('action' => 'index'));
        } 

        $this->redirect(array('action' => 'abm', 'A'));                   
    }

    /**
    * @secured(MODIFICACION_UNIDAD)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->UnidadCalculo->id = $id;
            
            try{ 
                if ($this->UnidadCalculo->saveAll($this->request->data)){
                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "La UnidadCalculo ha sido modificado exitosamente",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('La UnidadCalculo ha sido modificado exitosamente.', 'success');
                        $this->redirect(array('controller' => 'UnidadCalculoesCalculo', 'action' => 'index'));
                        
                    } 
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, La UnidadCalculo no ha podido modificarse.', 'error');
                
            }
            $this->redirect(array('action' => 'index'));
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }

    /**
    * @secured(BAJA_UNIDAD)
    */
    function delete($id) {
        
        $this->loadModel($this->model);

        try{

            if ($this->UnidadCalculo->delete($id))
                 $this->Session->setFlash('La UnidadCalculo ha sido eliminada exitosamente.', 'success');
            else
                throw new Exception();

        }catch(ChildRecordException $ex){ 
            $this->Session->setFlash($ex->getMessage(), 'error');
        }
        
        
        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
    }


    
    
 
    public function getModel($vista='default'){
    	
    	$model = parent::getModelCamposDefault();
    	$model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
    	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
    	
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    	
    	
    	
    	$this->set('model',$model);
    	Configure::write('debug',0);
    	$this->render($vista);
    	
    	
    	
    	
    	
    	
    }

}
?>