<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class Provincia extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'provincia';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'd_provincia' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar un Nombre.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	/*public $hasMany = array(
		'Region' => array(
			'className' => 'Region',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public $childRecordMessage = "Existe por lo menos una regi&oacute;n, predio, establecimiento o usuario relacionado/a con la jurisdicci&oacute;n.";
    
    */
    
    public $belongsTo = array(
        'Pais' => array(
            'className' => 'Pais',
            'foreignKey' => 'id_pais',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
        );
        
        
         
  public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     "d_pais"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
    
     );
     
     }
     
     
     public function getProvinciaPorCodigoAfip($codigo_afip){
     	
     	$prov = $this->find('first', array('conditions' => array('Provincia.codigo_afip' => $codigo_afip)));
     	
     	return $prov;
     }
              
}
