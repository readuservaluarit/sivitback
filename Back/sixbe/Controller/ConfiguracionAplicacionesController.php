<?php

/**
* @secured(ABM_GEOGRAFICO)
*/
class ConfiguracionAplicacionesController extends AppController {
    public $name = 'ConfiguracionAplicaciones';
    public $model = 'ConfiguracionAplicacion';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    public function index() {
        $id = 1;
        $mode = "M";
        $this->loadModel($this->model);
        $this->ConfiguracionAplicacion->id = $id;
        $this->request->data = $this->ConfiguracionAplicacion->read();
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('ConfiguracionAplicacion');
        $formBuilder->setTituloForm('Modificaci&oacute;n de ConfiguracionAplicacion');


        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('url_informe', 'url_informe', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('test', 'Test', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('url_crt', 'url_crt', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('url_key', 'url_key', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('direccion_empresa', 'Direcci&oacute;n empresa', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('urlAutenticacionPruebaAfip', 'url Autenticacion Prueba Afip', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('urlNegocioPruebaAfip', 'url Negocio Prueba Afip', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('urlAutenticacionProduccionAfip', 'url Autenticacion Produccion Afip', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('urlNegocioProduccionAfip', 'url Negocio Produccion Afip', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        
      /*  $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'ConfiguracionAplicacionAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('ConfiguracionAplicacionDConfiguracionAplicacion', 'Debe ingresar un nombre');
        validator.validateRequired('ConfiguracionAplicacionCotizacion', 'Debe ingresar una cotizaci&oacute;n');


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";    */

        //$formBuilder->addFormCustomScript($script);

        //$formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
    }


   

    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(READONLY_PROTECTED)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            if ($this->ConfiguracionAplicacion->save($this->request->data))
                $this->Session->setFlash('La ConfiguracionAplicacion ha sido creada exitosamente.', 'success'); 
            else
                $this->Session->setFlash('Ha ocurrido un error,la ConfiguracionAplicacion no ha podido ser creada.', 'error');

            $this->redirect(array('action' => 'index'));
        } 

        $this->redirect(array('action' => 'abm', 'A'));                   
    }

    
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->ConfiguracionAplicacion->id = $id;
            
            try{ 
                if ($this->ConfiguracionAplicacion->saveAll($this->request->data)){
                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "La Configuracion de la Aplicaci&oacute;n ha sido modificado exitosamente",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('La ConfiguracionAplicacion ha sido modificado exitosamente.', 'success');
                        $this->redirect(array('controller' => 'ConfiguracionAplicaciones', 'action' => 'index'));
                        
                    } 
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, la Configuracion de la Aplicaci&oacute;n no ha podido modificarse.', 'error');
                
            }
         
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }

    /**
    * @secured(READONLY_PROTECTED)
    */
    function delete($id) {
        
        $this->loadModel($this->model);

        try{

            if ($this->ConfiguracionAplicacion->delete($id))
                 $this->Session->setFlash('La Configuracion de la Aplicaci&oacute;n ha sido eliminado exitosamente.', 'success');
            else
                throw new Exception();

        }catch(ChildRecordException $ex){ 
            $this->Session->setFlash($ex->getMessage(), 'error');
        }
        
        
        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
    }



    
}
?>