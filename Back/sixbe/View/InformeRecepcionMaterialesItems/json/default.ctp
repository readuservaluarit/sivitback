<?php  include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');   
        $model["n_item"]["show_grid"] = 1;
        $model["n_item"]["header_display"] = "Nro. Item";
        $model["n_item"]["order"] = "0";
        $model["n_item"]["width"] = "4";
        
		$model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "Codigo";
        $model["codigo"]["order"] = "1";
        $model["codigo"]["width"] = "8";
		$model["codigo"]["read_only"] = "1";//AHORA SE PUEDE HACER DOBLE CLICK
		
		$model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "[Descripci&oacute;n]";
        $model["d_producto"]["order"] = "2";
		$model["d_producto"]["width"] = "11";
		$model["d_producto"]["read_only"] = "1";
		
		$model["codigo_observacion"]["show_grid"] = 1;
        $model["codigo_observacion"]["header_display"] = "Articulo Obs."; //OBSERVACION GENERAL
        $model["codigo_observacion"]["order"] = "3";
        $model["codigo_observacion"]["width"] = "10";
        
        $model["id_unidad"]["show_grid"] = 1;
        $model["id_unidad"]["header_display"] = "Unidad";
        $model["id_unidad"]["order"] = "4";
        $model["id_unidad"]["width"] = "5";
		$model["id_unidad"]["read_only"] = "0";
		$model["id_unidad"]["text_colour"] = "#F0000F";
		
		//cantidad importada del items de recibo
		$model["cantidad"]["show_grid"] = 1;
        $model["cantidad"]["header_display"] = "Cantidad Recibida";
        $model["cantidad"]["order"] = "5";
        $model["cantidad"]["width"] = "5";
		$model["cantidad"]["read_only"] = "1";
		$model["cantidad"]["text_colour"] = "#660099";//RED VIOLET
        
        $model["cantidad_cierre"]["show_grid"] = 1;
        $model["cantidad_cierre"]["header_display"] = "Cant Aprobada";
        $model["cantidad_cierre"]["order"] = "6";
        $model["cantidad_cierre"]["width"] = "5";
		$model["cantidad_cierre"]["text_colour"] = "#009900";//GRENN
		 
        $model["cantidad_rechazada"]["show_grid"] = 1;
        $model["cantidad_rechazada"]["header_display"] = "Cant. No Conforme";
        $model["cantidad_rechazada"]["order"] = "7";
        $model["cantidad_rechazada"]["width"] = "5";
		$model["cantidad_rechazada"]["type"] = "string";
		$model["cantidad_rechazada"]["read_only"] = "1";
		$model["cantidad_rechazada"]["text_colour"] = "#FF0000";//RED
		
		$model["Lotes"]["show_grid"] = 1;
        $model["Lotes"]["header_display"] = "2Lotes asociados."; //ASOCIADA CON LA OBSERVACION DE LOS RECHAZOS Y QUIEN HACE LA INSPECCION
        $model["Lotes"]["order"] = "8";
        $model["Lotes"]["width"] = "5";
		$model["Lotes"]["type"] = "jarray"; //Columnas q contienen un json q se usa de MasterDetail
		
		/*contiene el objecto json  */
		$model["comprobante_item_lote"]["show_grid"] = 0;
        $model["comprobante_item_lote"]["header_display"] = "LOtes asociados."; //ASOCIADA CON LA OBSERVACION DE LOS RECHAZOS Y QUIEN HACE LA INSPECCION
        $model["comprobante_item_lote"]["order"] = "9";
        $model["comprobante_item_lote"]["width"] = "25";
		
		$model["item_observacion"]["show_grid"] = 0;
        $model["item_observacion"]["header_display"] = "Observ."; //NO va mas
        $model["item_observacion"]["order"] = "10";
        $model["item_observacion"]["width"] = "25";
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItem",
                    "content" => $model
                );
        echo json_encode($output);
?>
