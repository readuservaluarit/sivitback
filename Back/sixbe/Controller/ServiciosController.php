<?php

App::uses('ArticuloController', 'Controller');

class ServiciosController extends ArticuloController {
    
    
    public $name = 'Servicio';
    public $model = 'Servicio';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $tipo_producto = EnumProductoTipo::Servicio;
    public $nombre_producto = "Servicios";
    
    
  
  
    /**
    * @secured(CONSULTA_SERVICIO)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = array();
        $this->RecuperoFiltros($conditions);
        
         array_push($conditions, array($this->model.'.id_producto_tipo =' => $this->tipo_producto)); 
       
            
    
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
        		'contain' =>array('Color','Talle','FamiliaProducto','Origen','Iva','Categoria', 'ListaPrecioProducto'=>array('conditions'=>array('ListaPrecioProducto.id_lista_precio'=>$this->id_lista_precio),"ListaPrecio","Moneda"),'Unidad','CuentaContableVenta','CuentaContableCompra','ProductoTipo'=>array('DestinoProducto')),
            // 'contain' =>array('ArticuloRelacion'/*=>array('Componente'=>array('Categoria')*/), 'FamiliaProducto','StockProducto','Origen'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.codigo desc'
        );
          //$this->Security->unlockedFields = array('costo','precio'); 
      if($this->RequestHandler->ext != 'json'){
          
          //HTML
      }else{
          
       
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
       //parseo el data para que los models queden dentro del objeto de respuesta
         foreach($data as &$valor){
               
             $this->cleanforOutput($valor,2);
         }
         
         $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     
          
          
          
      }  
    }
    
    
    /**
    * @secured(ADD_SERVICIO)
    */
    public function add() {
        
        parent::add();
      
    }
    
    
    /**
    * @secured(MODIFICACION_SERVICIO)
    */
    function edit($id) {
        
        parent::edit($id);
    }
    
  
    /**
    * @secured(BAJA_SERVICIO)
    */
    function delete($id) {
        
        parent::delete($id);
     
        
        
    }
    

    
    
  
    /**
    * @secured(CONSULTA_SERVICIO)
    */
    public function existe_codigo(){
        
        parent::existe_codigo();
    }
    
    
    
    
    
    
    /**
    * @secured(CONSULTA_SERVICIO)
    */
  public function getModel($vista='default'){
       parent::getModel($vista);
  }
    
    
  
  /**
    * @secured(CONSULTA_SERVICIO)
    */
  public function getCosto($id_articulo){
    parent::getCosto($id_articulo);
  }
    
 
    
    
    
}
?>