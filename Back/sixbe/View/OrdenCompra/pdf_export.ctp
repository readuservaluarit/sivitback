<?php

/**
 * PDF exportación de Orden Compra
 */

App::import('Vendor','tcpdf/tcpdf');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
ob_clean();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sivit by Valuarit');
$pdf->SetTitle('Orden de Compra');
$pdf->SetSubject('Orden de Compra');
//$pdf->SetKeywords('');



// set default header data
$logo = '';
$title = "";
$subtitle = "";


class MyTCPDF extends TCPDF{
	
	public $datos_header;
	public $datos_footer;
	public $bar_code;
	public $observaciones;
	
	public function set_datos_header($datos_empresa){
		$this->datos_header = $datos_empresa;
		
	}
	public function set_datos_footer($datos_empresa_footer){
		$this->datos_footer = $datos_empresa_footer;
		
	}
	public function set_barcode($bar_code){
		$this->bar_code = $bar_code;
		
	}
	
	public function set_observaciones($observaciones){
		$this->observaciones = $observaciones;
		
	}
	public function Header(){
		
		
		     if ($this->page == 1) { 
				$this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_header, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
			}
			
	}
	
	
	
   protected $last_page_flag = false;

    public function Close() {
    $this->last_page_flag = true;
    parent::Close();
  }
  
  	
	public function Footer(){
		
	if ($this->last_page_flag) {    
		
		$this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_footer, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
		$this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->observaciones, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
		
		$style['text'] = true;
		$style['border'] = true;
		$style['align'] = 'C';
		$this->write1DBarcode($this->bar_code, 'I25', '40', '', '', 9, 0.4, $style, 'N');
		
		//$this->writeHTMLCell($w = 0, $h = 0, $x = '200', $y = '', $this->bar_code, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
		//$this->Cell(0, 20, $this->bar_code , 0, false, 'C', 0, '', 0, false, 'T', 'M');
		$this->Cell(0, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
	
	
	}
	
}

$pdf = new MyTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);




// Set some content to print
/*
 $html = <<<EOD
 <h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
 <i>This is the first example of TCPDF library.</i>
 <p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
 <p>Please check the source code documentation and other examples for further information.</p>
 <p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
 EOD;
 */

$tbody = "";

$renglones = $datos_pdf["ComprobanteItem"];

$data = array();

foreach($renglones as $renglon){
	
	$descuento = ($renglon['precio_unitario_bruto']-$renglon['precio_unitario']);
	$precio_subtotal = ($renglon['precio_unitario'])* $renglon['cantidad'];
	
	$tbody .= "<tr>";
	$tbody .= "<td>" .$renglon['orden'] . "</td>";
	$tbody .= "<td>" .$renglon['item_observacion'] . "</td>";
	$tbody .= "<td>" .$renglon['cantidad'] . "</td>";
	$tbody .= "<td>" .$renglon['Unidad']['d_unidad'] . "</td>";
	$tbody .= "<td>" .money_format('%!n', $renglon['precio_unitario_bruto']). "</td>";
	$tbody .= "<td>" .$renglon['Iva']['d_iva_detalle'] . "</td>";
	$tbody .= "<td>" .money_format('%!n',$renglon['descuento_unitario']). "</td>";
	$tbody .= "<td>" .money_format('%!n', $precio_subtotal)."</td>";
	$tbody .= "</tr>";
}

//agrego las observaciones


$impuestos = '';
if(isset($datos_pdf["ComprobanteImpuesto"])){
	
	foreach($datos_pdf["ComprobanteImpuesto"] as $impuesto){
		
		if($impuesto["Impuesto"]["id"] == EnumImpuesto::IVACOMPRAS){
			$impuestos .= ' <tr><td colspan="8">'.$impuesto["Impuesto"]["d_impuesto"].' '.money_format('%!n',$impuesto["tasa_impuesto"]).'%</td></tr>';
			$impuestos .= ' <tr><td colspan="8">Importe '.$impuesto["Impuesto"]["d_impuesto"].': '.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n',$impuesto["importe_impuesto"]).'</td></tr>';
			
		}
	}
}
	
	
	$html='';
	
	$fecha_entrega = new DateTime($datos_pdf['Comprobante']['fecha_entrega']);
	$fecha_generacion = new DateTime($datos_pdf['Comprobante']['fecha_generacion']);
	
	$fecha_contable = new DateTime($datos_pdf['Comprobante']['fecha_contable']);
	$fecha_inicio_actividad = new DateTime($datos_empresa["DatoEmpresa"]["fecha_inicio_actividad"]);
	
	$aviso_anulado = '';
	if($datos_pdf['Comprobante']['id_estado_comprobante'] == EnumEstadoComprobante::Anulado)
		$aviso_anulado = ' ANULADO';
		
		
		
		$html = '<table width="100%" border="0" cellspacing="0" style="font-size:10px;border:2px solid #000000; font-family:\'Courier New\', Courier, monospace">
          <tr>
            <td style="padding: 5px 5px 5px 5px;" width="37%" rowspan="4" align="center" valign="center"  ><img src="'.$datos_empresa["DatoEmpresa"]["app_path"].'/img/'.$datos_empresa["DatoEmpresa"]["id"].'.png" width="420" height="121" />  </td>
            <td width="8%" height="3%" rowspan="2"  style="border:2px solid #000000">
                <div align="center" style="font-size:30px">'.strtoupper($datos_pdf["TipoComprobante"]["letra"]).'</div>
                 <span align="center" style="font-size:10px;">cod 01</span>
            </td>
            <td width="55%" style="padding-left:5px; font-size:17px;font-weight:bold;"> Orden Compra '.$proforma.' '.$aviso_anulado.': </td>
            		
          </tr>
          <tr>
            <td style="padding-left:5px; font-size:15px;"> Nro '.str_pad($datos_pdf["PuntoVenta"]["numero"],4,"0",STR_PAD_LEFT).'-'.str_pad($datos_pdf["Comprobante"]["nro_comprobante"],8,"0",STR_PAD_LEFT).'</td>
          </tr>
            		
          <tr>
            <td width="8%" height="2%" align="center" valign="middle" >&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr >
            <td  width="8%" height="5%" align="center" valign="middle" >&nbsp;</td>
            <td style="padding-left:5px;"> Original Blanco / Copia color </td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["calle"].' '.$datos_empresa["DatoEmpresa"]["numero"].' '.$datos_empresa["DatoEmpresa"]["codigo_postal"].' '.$datos_empresa["Provincia"]["d_provincia"].'</td>
            <td rowspan="5">&nbsp;</td>
            <td style="padding-left:5px;"> Fecha: '.$fecha_generacion->format('d-m-Y').'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> E-mail: '.$datos_empresa["DatoEmpresa"]["email"].'</td>
            <td style="padding-left:5px;"> CUIT: '.$datos_empresa["DatoEmpresa"]["cuit"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> Tel: '.$datos_empresa["DatoEmpresa"]["tel"].'</td>
            <td style="padding-left:5px;"> Ingresos Brutos: '.$datos_empresa["DatoEmpresa"]["iibb"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["website"].'</td>
            <td style="padding-left:5px;"> Inicio de actividades: '.$fecha_inicio_actividad->format('d-m-Y').'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.strtoupper($datos_empresa["TipoIva"]["d_tipo_iva"]).'</td>
            <td style="padding-left:5px;">&nbsp;</td>
          </tr>
        </table>';
		
		$html;
		
		$pdf->set_datos_header($html);
		
		$html ='';
		
		
		$html .='
				
<table width="100%"   style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
  <tr >
                    <td colspan="4"></td>
                  </tr>
  <tr>
  <tr >
                    <td colspan="4"></td>
                  </tr>
  <tr>
				
				
 </table>
<table width="100%" border="1" style="width:100%;font-size:10px;">
                  <tr  style="background-color:rgb(224, 225, 229);">
                    <td colspan="4"><div align="center">Proveedor</div></td>
                  </tr>
                  <tr>
                    <td width="17%">Codigo: '.$datos_pdf["Persona"]["id"].'</td>
                    <td width="35%">Raz&oacute;n Social: '.$datos_pdf["Persona"]["razon_social"].'</td>
                    <td width="26%">Cuit: '.$datos_pdf["Persona"]["cuit"].' </td>
                    <td width="22%">O/C: '.$datos_pdf["Comprobante"]["orden_compra_externa"].'</td>
                  </tr>
                  <tr>
                    <td colspan="3">Direccion: '.$datos_pdf["Persona"]["calle"].' - '.$datos_pdf["Persona"]["numero_calle"].' - '.$datos_pdf["Persona"]["localidad"].' - '.$datos_pdf["Persona"]["ciudad"].' - '.$datos_pdf["Persona"]["Provincia"]["d_provincia"].' - '.$datos_pdf["Persona"]["Pais"]["d_pais"].'  </td>
                    <td>Telefono: '.$datos_pdf["Persona"]["tel"].'</td>
                  </tr>
                </table>
                    		
                    		
                    		
                    		
                    		
                <table width="100%" border="1" style="width:100%;font-size:10px;">
                  <tr  style="background-color:rgb(224, 225, 229);">
                    <td colspan="3"><div align="center">Entrega</div></td>
                  </tr>
                  <tr>
                    <td width="50%">Fecha Entrega global: '.$fecha_entrega->format('d-m-Y').'</td>
                    		
                    		
                    <td width="50%">Lugar: '.$datos_pdf["Comprobante"]["lugar_entrega"].'</td>
                    		
                  </tr>
                </table>
                    		
                    		
                    		
                    		
<table width="100%" border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">
                    		
                    		
     <tbody>
                    		
        <tr >
        <td colspan="2"> </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
                    		
        <tr >
        <td  colspan="6" ><HR></td>
                    		
         </tr>
     </tbody>
</table>
                    		
                    		
<table border="1px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">
                    		
    <tr style="background-color:rgb(224, 225, 229);">
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;">Ord.</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:30%;">Descripci&oacute;n</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">Cant.</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;">Un.</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:15%;">P.Unit.</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;">IVA (%)</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">Desc. %</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:20%;">P. subtotal</th>
    </tr>
                    		
  <tbody>
    '.$tbody.'
  </tbody>
</table>';
		
		
		
		$comprobante  =  new Comprobante;
		$suma_descuentos_individuales = $comprobante->getSumaDescuentosIndividuales($renglones,0);
		
		
		$html .= '<table border="0px" cellspacing="1" cellpadding="20" style="width:100%;font-size:12px;">';
		
		$html .= "<tr>";
		$html .= "<td colspan='8' style='line-height:8px;'>Observaciones:".nl2br($datos_pdf["Comprobante"]["observacion"]). "</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td colspan='8'>CONDICIONES COMERCIALES</td>";
		$html .= "</tr>";
		$html .= "</table>";
		
		$html .= "<HR>";
		
		$html .= '<table border="0px" cellspacing="1" cellpadding="1" style="width:100%;font-size:12px;">';
		$html .= "<tr>";
		$html .= "<td colspan='8'>Moneda: ".$datos_pdf['Moneda']['d_moneda']."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td colspan='8'>Forma de Pago: ".$datos_pdf['CondicionPago']['d_condicion_pago']."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td colspan='8'>Plazo de Entrega: ".$datos_pdf['Comprobante']['plazo_entrega']." dias</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td colspan='8'>Lugar de Entrega: ".$datos_pdf['Comprobante']['lugar_entrega']."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td colspan='8'>Importe: ".$datos_pdf['Moneda']['simbolo_internacional']." ".money_format('%!n',$datos_pdf['Comprobante']['subtotal_bruto'])."</td>";
		$html .= "</tr>";
		
		if($suma_descuentos_individuales>0){
			$html .= "<tr>";
			$html .= "<td colspan='8' >Suma Desc. Unitarios: ".$datos_pdf['Moneda']['simbolo_internacional']." ".money_format('%!n',round($suma_descuentos_individuales,2))." </td>";
			$html .= "</tr>";
		}
		
		$html .= "<tr>";
		$html .= "<td colspan='8' >Descuento Global: ".money_format('%!n',round($datos_pdf['Comprobante']['descuento'],2))."%</td>";
		$html .= "</tr>";
		
		$html .= "<tr>";
		$html .= "<td colspan='8' >Monto Descuento Global: ".$datos_pdf['Moneda']['simbolo_internacional']." ".money_format('%!n',round($datos_pdf['Comprobante']['importe_descuento'],2))." </td>";
		$html .= "</tr>";
		
		
		$html.= $impuestos;
		
		$html .= "<tr>";
		$html .= "<td colspan='8'>Total: ".$datos_pdf['Moneda']['simbolo_internacional']." ".money_format('%!n',$datos_pdf['Comprobante']['total_comprobante'])."</td>";
		$html .= "</tr>";
		
		$html .= "</table>";
		
		
		
		
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Sivit by Valuarit');
		$pdf->SetTitle('Factura');
		$pdf->SetSubject('Factura');
		//$pdf->SetKeywords('');
		
		
		
		// set default header data
		$logo = 'img/header_factura.jpg';
		$title = "";
		$subtitle = "";
		//$pdf->SetHeaderData($logo, 190, $title, $subtitle, array(0,0,0), array(0,104,128));
		$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));
		
		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, 50, 20);
		$pdf->SetHeaderMargin(10);
		$pdf->SetFooterMargin(65);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		
		//set some language-dependent strings
		/*$lg = Array();
		 $lg = Array();
		 $lg['a_meta_charset'] = 'ISO-8859-1';
		 $lg['a_meta_dir'] = 'rtl';
		 $lg['a_meta_language'] = 'fa';
		 $lg['w_page'] = 'page';
		 */
		//$pdf->setLanguageArray($lg);
		
		// ---------------------------------------------------------
		
		// set default font subsetting mode
		//$pdf->setFontSubsetting(true);
		
		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('helvetica', '', 12, '', true);
		
		// Add a page
		// This method has several options, check the source code documentation for more information.
		$pdf->AddPage();
		
		// set text shadow effect
		$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
		
		
		$pdf->writeHTML($html, true, false, true, false, '');
		
		
		
		
		// QRCODE,L : QR-CODE Low error correction
		// set style for barcode
		$style = array(
				'border' => 2,
				'vpadding' => 'auto',
				'hpadding' => 'auto',
				'fgcolor' => array(0,0,0),
				'bgcolor' => false, //array(255,255,255)
				'module_width' => 1, // width of a single module in points
				'module_height' => 1 // height of a single module in points
		);
		
		//$pdf->Text(20, 25, 'QRCODE L');
		
		// ---------------------------------------------------------
		
		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
		//$pdf->SetBarcode(date("Y-m-d H:i:s", time()));
		ob_clean();
		$pdf->Output(str_replace(".","",$datos_pdf["TipoComprobante"]["abreviado_tipo_comprobante"].'_'.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'-'.$datos_pdf["Persona"]["razon_social"]).'.pdf', 'D');
		
		//============================================================+
		// END OF FILE
		//============================================================+
		