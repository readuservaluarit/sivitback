<?php
App::uses('AppModel', 'Model');
/**
 * AudiAtributo Model
 *
 */
class AudiAtributo extends AppModel {
    public $name = "AudiAtributo";
   
    public $useTable = 'audi_atributo';
    
    public $belongsTo = array(
        'AudiEntidad' => array(
            'className' => 'AudiEntidad',
            'foreignKey' => 'id_entidad',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
}

?>
