<?php  include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');   
        
		$model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "Codigo";
        $model["codigo"]["order"] = "1";
        $model["codigo"]["width"] = "12";
		$model["codigo"]["read_only"] = "1";//AHORA SE PUEDE HACER DOBLE CLICK
		// $model["codigo"]["picker"] = EnumMenu::mnuArticulosGeneral; //SOLO PARA OA DE STOCK VER COMO HACER EL CONDICIONAL
		
		$model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "[Descripci&oacute;n]";
        $model["d_producto"]["order"] = "2";
		$model["d_producto"]["width"] = "57";
		$model["d_producto"]["read_only"] = "1";
		// $model["d_producto"]["picker"] = EnumMenu::mnuArticulosGeneral; //SOLO PARA OA DE STOCK VER COMO HACER EL CONDICIONAL
		
		$model["item_observacion"]["show_grid"] = 1;
        $model["item_observacion"]["header_display"] = "Pres. Ensayo";
        $model["item_observacion"]["order"] = "4";
        $model["item_observacion"]["width"] = "10";
		$model["item_observacion"]["read_only"] = "0";
		$model["item_observacion"]["text_colour"] = "#F0000F";
		
		//cantidad importada del items de recibo
		$model["cantidad"]["show_grid"] = 1;
        $model["cantidad"]["header_display"] = "Cant. Planif";
        $model["cantidad"]["order"] = "5";
        $model["cantidad"]["width"] = "8";
		$model["cantidad"]["text_colour"] = "#0000AA";//BLUE
        
        $model["cantidad_cierre"]["show_grid"] = 1;
        $model["cantidad_cierre"]["header_display"] = "Cantidad";
        $model["cantidad_cierre"]["order"] = "6";
        $model["cantidad_cierre"]["width"] = "8";
		$model["cantidad_cierre"]["text_colour"] = "#008500";//GRENN
		
		$model["id_unidad"]["show_grid"] = 1;
        $model["id_unidad"]["header_display"] = "Unidad";
        $model["id_unidad"]["order"] = "7";
        $model["id_unidad"]["width"] = "5";
		$model["id_unidad"]["read_only"] = "0";
		$model["id_unidad"]["text_colour"] = "#F0000F";
		
		
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItem",
                    "content" => $model
                );
        echo json_encode($output);
        
        
?>
