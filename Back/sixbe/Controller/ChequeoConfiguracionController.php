<?php


class ChequeoConfiguracionController extends AppController {
    public $name = 'ChequeoConfiguracion';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    public $errores = array();
    
    
    
    
    
    public function index() {
    	
    	/*TODO: Brindar soluciones a los errores*/
    	
    	$this->loadModel(EnumModel::ListaPrecio);
    	
    	$this->loadModel(EnumModel::Modulo);
    	$this->loadModel(EnumModel::DepositoMovimientoTipo);
    	$this->loadModel(EnumModel::ProductoTipo);
    	$this->loadModel(EnumModel::TipoComprobante);
    	$this->loadModel(EnumModel::FuncionRol);
    	$this->loadModel(EnumModel::DatoEmpresa);
    	
    	
    	$this->permisosDirectorios();
    	
    	


    	
    	
    	$this->Session->write("Modulo", $this->Modulo->getModulos());
    	
    	
    	$datoEmpresa = 	 $this->DatoEmpresa->find('first', array(
    			'conditions' => array('DatoEmpresa.id' => 1),
    			'contain' =>false
    	));
    	
    	
    	if( strlen($datoEmpresa["DatoEmpresa"]["local_path"]) == 0){
    		
    		array_push($this->errores,"Se Debe definir el local Path en Dato Empresa. Tiene que ser la ruta por default del directorio donde corre el BackEnd");
    	}
    	
    	
    	/*
    	if(!file_exists($datoEmpresa["DatoEmpresa"]["local_path"])){
    		
    		array_push($this->errores,"Se Debe definir el local Path en Dato Empresa. Tiene que ser la ruta por default del directorio donde corre el BackEnd, actualmente no es correcto.");
    	}
    	*/
    	
    	/*if( $datoEmpresa["DatoEmpresa"]["app_path"] != 1 ){
    		
    		array_push($this->errores,"Se Debe definir el local Path en Dato Empresa. Tiene que ser la ruta por default del directorio donde corre el BackEnd");
    	}*/
    	
    	if(!file_exists(EnumCacheName::getModel))
		{
			array_push($this->errores,"No existe el archivo de cache para los getModel");
    	}
    	
    	if(!file_exists(EnumCacheName::getCacheIndexTablasEstaticaSistema)){
    		
    		array_push($this->errores,"No existe el archivo de cache para los Index estaticos");
    	}
    	
    	////////////////////////////////////TIPO COMPROBANTE/////////////////////////////////////////////////////////////////////////
    	
    	
    	
    	$producto_tipo = $this->TipoComprobante->find('count',array('conditions'=>array('TipoComprobante.letra IS NULL'),'contain'=>false));
    	
    	
    	if( $producto_tipo > 0 ){
    		array_push($this->errores,"Existen Tipos Comprobante que no tienen letra, por defecto llevan la letra X");
    	}
    	
    	
    	
    	
    	$envia_mail_sin_mensaje = $this->TipoComprobante->find('count',array('conditions'=>array('TipoComprobante.id'=>1,'TipoComprobante.html_mail IS NULL'),'contain'=>false));
    	
    	if( $envia_mail_sin_mensaje > 0 ){
    		array_push($this->errores,"Existen Tipos Comprobante que no tienen cargado el cuerpo del Mail y estan configurados para enviar mails.");
    	}
    	////////////////////////////////FIN TIPO COMPROBANTE//////////////////////////////////////////////////////////////////////
    	
    	
    	
    	
    	
    	////////////////////////////////////SIVIT CLIENTES/////////////////////////////////////////////////////////////////////////
    	$roles = $this->{EnumModel::FuncionRol}->find('count',array('conditions'=>array('FuncionRol.id_rol'=>EnumRol::SivitCliente),'contain'=>false));
    	
    	if( $roles == 0 ){
    		array_push($this->errores,"El Rol Sivit Clientes No tiene funciones asociadas");
    	}
    	////////////////////////////////////FIN SIVIT CLIENTES/////////////////////////////////////////////////////////////////////////
    	
    	
    	
    	
    	////////////////////////////////////SIVIT PROVEEDORES/////////////////////////////////////////////////////////////////////////
    	$roles = $this->{EnumModel::FuncionRol}->find('count',array('conditions'=>array('FuncionRol.id_rol'=>EnumRol::SivitProveedor),'contain'=>false));
    	
    	if( $roles == 0 ){
    		array_push($this->errores,"El Rol Sivit Proveedor No tiene funciones asociadas");
    	}
    	////////////////////////////////////FIN SIVIT PROVEEDORES/////////////////////////////////////////////////////////////////////////
    	
    	
    	//////////////////////////////////////STOCK///////////////////////////////////////////////////////////////////////////////////
    	
    	$modulo_stock  = $this->Modulo->estaHabilitado(EnumModulo::STOCK);
    	
    	
    	if($modulo_stock == 1){
    		
    		$producto_tipo = $this->ProductoTipo->find('all',array('conditions'=>array('ProductoTipo.tiene_stock'=>1,'ProductoTipo.id_deposito_fijo_principal'=>null),'contain'=>false));
    		
    		if(count($producto_tipo)>0){
    			
    			array_push($this->errores,"Existen Productos Tipo que no tienen definido el Dep&oacute;sito fijo Principal.");
    		}
    		
    		
    		$deposito_movimiento_tipo = $this->DepositoMovimientoTipo->find('count',array('conditions'=>array(),'contain'=>false));
    		
    		
    		if($deposito_movimiento_tipo == 0){
    			array_push($this->errores,"No esta parametrizada la tabla Deposito Movimiento Tipo");
    		}
    		
    	}
    	
    	////////////////////////////////////FIN STOCK////////////////////////////////////////////////////////////////////////////////
    	
    	var_dump($this->errores);
    	
    	
    	
    	echo "Configuraciones:<br>";
    	echo "<strong>APP PATH:</strong>".$datoEmpresa["DatoEmpresa"]["app_path"]."</br>";
    	echo "<strong>LOCAL PATH:</strong>".$datoEmpresa["DatoEmpresa"]["local_path"]."</br>";
    	echo "<strong>ERROR DEBUG en CORE:</strong>".Configure::read('debug');
    	
    	die();
    	
        
        
    }
    
    
    public function permisosDirectorios(){
    	//775 o 755 si es propietario
    	

    	
    	
    	
    	
    	$ruta_archivo = APP.'/tmp';
    	
    	$this->chequePermisos($ruta_archivo);
    	
    	
    	//permisos cache de afip Vendor/fesix/tmp
    	$ruta_archivo = APP.'/Vendor/fesix/tmp';
    	
    	$this->chequePermisos($ruta_archivo);
    	
    	
    	//permiso subida de archivos  webroot/media_file
    	$ruta_archivo = APP.'/webroot/media_file';
    	
    	$this->chequePermisos($ruta_archivo);
    	
    	
    	//subida de logo APP.'webroot/img'
    	$ruta_archivo = APP.'/webroot/img';
    	
    	$this->chequePermisos($ruta_archivo);
    	
    	
    	//permisos directorio de generacion de comprobantes webroot/files/comprobantes
    	
    	$ruta_archivo = APP.'/webroot/files/comprobantes';
    	
    	$this->chequePermisos($ruta_archivo);
    	
    	
    	
    	
    	//permiso subida de ultima version para la red  webroot/media_file
    	$ruta_archivo = APP.'/webroot/last_version';
    	
    	if(!file_exists($ruta_archivo))
    		mkdir($ruta_archivo, 0755);
    	
    	$this->chequePermisos($ruta_archivo);
    	
    	
    	
    	
    	
    	

    	
    	

    	
    	
    	
    	
    	
    }
    
    public function  chequePermisos($ruta_archivo){
    	
    	
    	$usuario_linux = "www-data";
    	
    	$permisos = fileperms($ruta_archivo);
    	
    	$propietario = fileowner($ruta_archivo);
    	
    	$usuario = posix_getpwuid($propietario);
    	
    	
    	$nombre_usuario = $usuario["name"];
    	
    	
    	if($nombre_usuario != $usuario_linux){
    		
    		$permisos_grupo = $this->permisosGrupo($permisos);
    		if($permisos_grupo !='rwx')
    			array_push($this->errores, "El directorio ".$ruta_archivo." No tiene permisos");
    			
    	}
    	
    }
    
    
    public function permisosGrupo($permisos){
    	
    	$info = "";
    	
    	$info .= (($permisos & 0x0020) ? 'r' : '-');
    	$info .= (($permisos & 0x0010) ? 'w' : '-');
    	$info .= (($permisos & 0x0008) ?
    			(($permisos & 0x0400) ? 's' : 'x' ) :
    			(($permisos & 0x0400) ? 'S' : '-'));
    	
    	return $info;
    	
    	
    	
    	
    	
    }
    
    
    public function beforeFilter(){
    	
    	App::uses('ConnectionManager', 'Model');
    	
    	$this->Auth->allow('index');
    	
    	parent::beforeFilter();
    }
    
    
   
    
    
    
   
    
}
?>