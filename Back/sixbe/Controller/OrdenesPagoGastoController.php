<?php
App::uses('OrdenesPagoController', 'Controller');


 
class OrdenesPagoGastoController extends OrdenesPagoController{
    
    public $name = EnumController::OrdenesPagoGasto;
    public $id_sistema_comprobante = EnumSistema::CAJA;
    
    
    /**
     * @secured(REPORTE_ORDEN_PAGO)
     */
    public function pdfExport($id,$vista=''){
    	
    	
    	$this->loadModel("Comprobante");
    	$orden_pago = $this->Comprobante->find('first', array(
    			'conditions' => array('Comprobante.id' => $id),
    			'contain' =>array('TipoComprobante')
    	));
    	
    	
    	
    	
    	if($orden_pago){
    		
    		$letra = $orden_pago["TipoComprobante"]["letra"];
    		$vista = "ordenPago".$letra;
    		parent::pdfExport($id,$vista);
    		
    		
    	}
    	
    	
    }
    
    public function CerrarComprobantesAsociados($recibo_objeto){
    	
    	/*traigo los items del recibo que son comprobantes*/
    	
    	$this->loadModel("ComprobanteItem");
    	$this->loadModel("Comprobante");
    	
    	
    	if($recibo_objeto && $recibo_objeto["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion || $recibo_objeto["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::Anulado){
    		
    		
    		$recibo = $this->ComprobanteItem->find('all', array(
    				'conditions' => array('ComprobanteItem.id_comprobante' => $recibo_objeto["Comprobante"]["id"]),
    				'contain' =>array('Comprobante','ComprobanteOrigen')
    		));
    		
    		
    		
    		
    		
    		$id_moneda = $recibo_objeto["Comprobante"]["id_moneda"];
    		
    		$comprobante_obj = new Comprobante();
    		
    		foreach($recibo as $comprobante_item){
    			
    			$comprobante_origen = $comprobante_item["ComprobanteOrigen"];
    			
    			$id_comprobante = $comprobante_origen["id"];
    			
    			
    			$saldo = 0;
    			if($recibo_objeto["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion )/*solo pregunto el saldo si es cerrado*/
    				$saldo = $comprobante_obj->getComprobantesImpagos($comprobante_origen["id_persona"],1,$id_comprobante,1,EnumSistema::CAJA,0,$id_moneda);
    				
    				
    				
    				if($saldo == 0 && $recibo_objeto["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion ){
    					
    					$this->Comprobante->updateAll(
    							array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Cumplida ),
    							array('Comprobante.id' => $id_comprobante) );
    					
    				}else{
    					
    				
    						
    						$this->Comprobante->updateAll(
    								array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::CerradoReimpresion ),
    								array('Comprobante.id' => $id_comprobante) );
    			
    					
    					
    				}
    				
    		}
    		
    	}
    }
    
    private function totalRetencionesManual(&$error=0,&$message=''){
    	
    	return 0;
    }
    
    private function totalRetencionesAutomatico(&$error=0,&$message=''){
    	
    	return 0;
    }
    
    
    public function CalcularImpuestos($id_comprobante,$id_tipo_impuesto='',$error=0,$message=''){
    	
    	
    	return 0;
    }
   
  
}
?>