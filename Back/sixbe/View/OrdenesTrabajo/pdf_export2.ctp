<?php
/**
 * PDF exportación de Orden TRABAJO
 */
App::import('Vendor','tcpdf/tcpdf'); 
App::import('Model','ComprobanteItem');
$html_composicion = '';
// if(isset($datos_pdf['ComprobanteItem'][0]['Producto']['ArticuloRelacion']) 
//	&& count($datos_pdf['ComprobanteItem'][0]['Producto']['ArticuloRelacion'])>0){
// $componentes_producto = $datos_pdf['ComprobanteItem'][0]['Producto']['ArticuloRelacion'];
// foreach($componentes_producto as $comp){
   // $html_composicion .='<tr  style="font-size:9px">
        // <td align="center" valign="middle">'.trim($comp['ArticuloHijo']["codigo"]).'</td>
        // <td align="center" valign="middle">'.trim($comp['ArticuloHijo']["d_producto"]).'</td>
        // <td align="center" valign="middle">'.round(trim($comp['cantidad_hijo'])).'</td>
        // <td align="center" valign="middle">&nbsp;</td>
        // <td align="center" valign="middle">&nbsp;</td>
      // </tr>';
// }

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetHeaderMargin(0);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Valuarit.com');
$pdf->SetTitle('Orden Trabajo');
$pdf->setPrintHeader(true);
$pdf->setPrintFooter(false);


$pdf->AddPage('P', 'A4');





//SETEO TODAS LAS VARIABLES CON EL COMPROBANTE 1
$fecha_emision = new DateTime($ot1['Comprobante']['fecha_generacion']);

$fecha_entrega = new DateTime($ot1['Comprobante']['fecha_entrega']);

$fecha_emision2 = new DateTime($ot2['Comprobante']['fecha_generacion']);

$fecha_entrega2 = new DateTime($ot2['Comprobante']['fecha_entrega']);


$comprobante_item = new ComprobanteItem();


$lotesComprobranteItem1 = '';

$lotes = array();


foreach($ot1['ComprobanteItem'] as $item){

if( $item["id_detalle_tipo_comprobante"] == EnumDetalleTipoComprobante::OrdenDeTrabajoCabecera){
	$producto_codigo = $item['Producto']['codigo'];
	$cantidad = $item['cantidad'];
	if($item['cantidad_cierre'] == 0)
		$cantidad_cierre = ' ';
	else
		$cantidad_cierre = $item['cantidad_cierre'];
	$producto_descripcion = $item['Producto']['d_producto'];
	}
	$lotesComprobranteItem1 ='';
}


foreach($ot2['ComprobanteItem'] as $item){

if( $item["id_detalle_tipo_comprobante"] == EnumDetalleTipoComprobante::OrdenDeTrabajoCabecera){
	$producto_codigo2 = $item['Producto']['codigo'];
	$cantidad2 = $item['cantidad'];
		if($item['cantidad_cierre'] == 0)
		$cantidad_cierre2 = ' ';
	else
		$cantidad_cierre2 = $item['cantidad_cierre'];
	$producto_descripcion2 = $item['Producto']['d_producto'];
	}
	$lotesComprobranteItem1 ='';
}


$logo = '/img/'.$datos_empresa["DatoEmpresa"]["id"].'.png';



if(!is_null($etapas_ot1[0]["fecha_generacion"]))
	$etapa_ot1_0_fecha_generacion = (new DateTime($etapas_ot1[0]["fecha_generacion"]))->format('d-m-y H:m');
else
	$etapa_ot1_0_fecha_generacion = "";
	
if(!is_null($etapas_ot1[0]["fecha_vencimiento"]))	
	$etapa_ot1_0_fecha_vencimiento = (new DateTime($etapas_ot1[0]["fecha_vencimiento"]))->format('d-m-y H:m');
else
	$etapa_ot1_0_fecha_vencimiento = "";	
	
	

if(!is_null($etapas_ot1[1]["fecha_generacion"]))
	$etapa_ot1_1_fecha_generacion = (new DateTime($etapas_ot1[1]["fecha_generacion"]))->format('d-m-y H:m');
else
	$etapa_ot1_1_fecha_generacion = "";
	
if(!is_null($etapas_ot1[1]["fecha_vencimiento"]))	
	$etapa_ot1_1_fecha_vencimiento = (new DateTime($etapas_ot1[1]["fecha_vencimiento"]))->format('d-m-y H:m');
else
	$etapa_ot1_1_fecha_vencimiento = "";	
	
	
	

if(!is_null($etapas_ot1[2]["fecha_generacion"]))
	$etapa_ot1_2_fecha_generacion = (new DateTime($etapas_ot1[2]["fecha_generacion"]))->format('d-m-y H:m');
else
	$etapa_ot1_2_fecha_generacion = "";
	
if(!is_null($etapas_ot1[2]["fecha_vencimiento"]))	
	$etapa_ot1_2_fecha_vencimiento = (new DateTime($etapas_ot1[2]["fecha_vencimiento"]))->format('d-m-y H:m');
else
	$etapa_ot1_2_fecha_vencimiento = "";		
	
	


if(!is_null($etapas_ot1[3]["fecha_generacion"]))
	$etapa_ot1_3_fecha_generacion = (new DateTime($etapas_ot1[3]["fecha_generacion"]))->format('d-m-y H:m');
else
	$etapa_ot1_3_fecha_generacion = "";
	
if(!is_null($etapas_ot1[3]["fecha_vencimiento"]))	
	$etapa_ot1_3_fecha_vencimiento = (new DateTime($etapas_ot1[3]["fecha_vencimiento"]))->format('d-m-y H:m');
else
	$etapa_ot1_3_fecha_vencimiento = "";		
	
	
		


if(!is_null($etapas_ot1[4]["fecha_generacion"]))
	$etapa_ot1_4_fecha_generacion = (new DateTime($etapas_ot1[4]["fecha_generacion"]))->format('d-m-y H:m');
else
	$etapa_ot1_4_fecha_generacion = "";
	
if(!is_null($etapas_ot1[4]["fecha_vencimiento"]))	
	$etapa_ot1_4_fecha_vencimiento = (new DateTime($etapas_ot1[4]["fecha_vencimiento"]))->format('d-m-y H:m');
else
	$etapa_ot1_4_fecha_vencimiento = "";		
	
		
	


if(!is_null($etapas_ot1[5]["fecha_generacion"]))
	$etapa_ot1_5_fecha_generacion = (new DateTime($etapas_ot1[5]["fecha_generacion"]))->format('d-m-y H:m');
else
	$etapa_ot1_5_fecha_generacion = "";
	
if(!is_null($etapas_ot1[5]["fecha_vencimiento"]))	
	$etapa_ot1_5_fecha_vencimiento = (new DateTime($etapas_ot1[5]["fecha_vencimiento"]))->format('d-m-y H:m');
else
	$etapa_ot1_5_fecha_vencimiento = "";		
	
	
	
	
		

if(!is_null($etapas_ot2[0]["fecha_generacion"]))
	$etapa_ot2_0_fecha_generacion = (new DateTime($etapas_ot2[0]["fecha_generacion"]))->format('d-m-y H:m');
else
	$etapa_ot2_0_fecha_generacion = "";
	
if(!is_null($etapas_ot2[0]["fecha_vencimiento"]))	
	$etapa_ot2_0_fecha_vencimiento = (new DateTime($etapas_ot2[0]["fecha_vencimiento"]))->format('d-m-y H:m');
else
	$etapa_ot2_0_fecha_vencimiento = "";	
	
	

if(!is_null($etapas_ot2[1]["fecha_generacion"]))
	$etapa_ot2_1_fecha_generacion = (new DateTime($etapas_ot2[1]["fecha_generacion"]))->format('d-m-y H:m');
else
	$etapa_ot2_1_fecha_generacion = "";
	
if(!is_null($etapas_ot2[1]["fecha_vencimiento"]))	
	$etapa_ot2_1_fecha_vencimiento = (new DateTime($etapas_ot2[1]["fecha_vencimiento"]))->format('d-m-y H:m');
else
	$etapa_ot2_1_fecha_vencimiento = "";	
	
	
	

if(!is_null($etapas_ot2[2]["fecha_generacion"]))
	$etapa_ot2_2_fecha_generacion = (new DateTime($etapas_ot2[2]["fecha_generacion"]))->format('d-m-y H:m');
else
	$etapa_ot2_2_fecha_generacion = "";
	
if(!is_null($etapas_ot2[2]["fecha_vencimiento"]))	
	$etapa_ot2_2_fecha_vencimiento = (new DateTime($etapas_ot2[2]["fecha_vencimiento"]))->format('d-m-y H:m');
else
	$etapa_ot2_2_fecha_vencimiento = "";		
	
	
	

if(!is_null($etapas_ot2[3]["fecha_generacion"]))
	$etapa_ot2_3_fecha_generacion = (new DateTime($etapas_ot2[3]["fecha_generacion"]))->format('d-m-y H:m');
else
	$etapa_ot2_3_fecha_generacion = "";
	
if(!is_null($etapas_ot2[3]["fecha_vencimiento"]))	
	$etapa_ot2_3_fecha_vencimiento = (new DateTime($etapas_ot2[3]["fecha_vencimiento"]))->format('d-m-y H:m');
else
	$etapa_ot2_3_fecha_vencimiento = "";		
	
	
	

if(!is_null($etapas_ot2[4]["fecha_generacion"]))
	$etapa_ot2_4_fecha_generacion = (new DateTime($etapas_ot2[4]["fecha_generacion"]))->format('d-m-y H:m');
else
	$etapa_ot2_4_fecha_generacion = "";
	
if(!is_null($etapas_ot2[4]["fecha_vencimiento"]))	
	$etapa_ot2_4_fecha_vencimiento = (new DateTime($etapas_ot2[4]["fecha_vencimiento"]))->format('d-m-y H:m');
else
	$etapa_ot2_4_fecha_vencimiento = "";		
	
	
				

if(!is_null($etapas_ot2[5]["fecha_generacion"]))
	$etapa_ot2_5_fecha_generacion = (new DateTime($etapas_ot2[5]["fecha_generacion"]))->format('d-m-y H:m');
else
	$etapa_ot2_5_fecha_generacion = "";
	
if(!is_null($etapas_ot2[5]["fecha_vencimiento"]))	
	$etapa_ot2_5_fecha_vencimiento = (new DateTime($etapas_ot2[5]["fecha_vencimiento"]))->format('d-m-y H:m');
else
	$etapa_ot2_5_fecha_vencimiento = "";			
	


$TablaLado1 = 
'
<table border="1" cellspacing="0"  width="100%" style="font-size:9px;">
  <tr>
    <td width="10%" rowspan="2" valign="top"> <img src="'.$datos_empresa["DatoEmpresa"]["app_path"].$logo.'" width="150" height="44" /></td>
	<td width="66%" colspan="5"  ><h6 style="font-size:12px;"> PLANILLA DE PRODUCCION EN PROCESO (PPP)</h6></td>
	<td width="12%" valign="top"><p align="center">Fch Emision</p></td>
    <td width="12%" valign="top"><p align="center">Fch Entrega </p></td>
  </tr>
  <tr valign="center">
	<td colspan="7" width="66%" valign="center" >
		<p style="font-size:17px;"> OP '.$ot1["Comprobante"]["nro_comprobante_completo"].' ['.$producto_codigo.']</p>
      </td>
	<td width="12%" ><p valign="center" align="center" style="font-size:10px;">
      '.$fecha_emision->format('d-m-y').'</p></td>
    <td width="12%" ><p valign="center" align="center" style="font-size:10px;" >
      '.$fecha_entrega->format('d-m-y').'</p></td>
  </tr>
  <tr >
    <td rowspan = "2" width="10%"><p style="font-size:6px;" >Form.03 Rev. 03  Vigencia 08/2018</p></td>
    <td rowspan = "2" colspan="4" width="40%"><p style="font-size:9px;">'.$producto_descripcion.' </p></td>
    <td colspan="2" valign="top" width="26%"><p style="font-size:7px;" align="center">Nº Plano - Letra /Rev. / Fecha:</p></td>
    <td width="12%"><p style="font-size:7px;" align="center">CANT. PLANIF.</p></td>
    <td width="12%"><p style="font-size:7px;" align="center">CANT. PROD.</p></td>
  </tr>
  <tr>
    <td width="26%" colspan="7"  style="text-align:center;font-size:10px;"><p></p></td>
    <td width="12%"><p align="center">'.$cantidad.'</p></td>
    <td width="12%"><p align="center">'.$cantidad_cierre.'</p></td>
  </tr>
  <tr class="lotesh">
    <td colspan="9" width="100%" ><p align="left" style="font-size:10px;" >Lotes de MP*: '.$lotesComprobranteItem1.'</p></td>
  </tr>
  <tr>
    <td width="6%"  rowspan="2"><h4 align="center">Etapa</h4></td>
    <td width="26%" ><h4 align="center">Operación </h4></td>
    <td width="6%"  ><h4 align="center">MAQ</h4></td>
    <td width="16%" rowspan="2" colspan="2"><h4 align="center">Fecha Inicio</h4></td>
    <td width="16%" rowspan="2"><h4 align="center">Fecha Fin</h4></td>
    <td width="6%" valign="top"><h4 align="center">FVerif</h4></td>
    <td width="24%" colspan= "3" valign="top"><h4 align="center">Liberada Por:</h4></td>
  </tr>
  <tr>
	<td width="26%" ><h4 align="center">Ejecutor </h4></td>
	<td width="6%" valign="top"><h4 align="center">TC</h4></td>
    <td width="6%" valign="top"><h4 align="center">CNF</h4></td>
    <td width="8%" valign="top"><h4 align="center">NC</h4></td>
	<td width="8%" valign="top"><h4 align="center">RP</h4></td>
	<td width="8%" valign="top"><h4 align="center">SCRP</h4></td>
  </tr>
  <tr>
    <td  valign="top" style="color:blue;"><h2 align="center"><strong>1-0</strong></h2></td>
        <td  valign="top"><p>'.substr($etapas_ot1[0]["Producto"]["d_producto"],0,24).'</p></td>
    <td  valign="top"><p align="center">'.$etapas_ot1[0]["maquina"]["Producto"]["codigo"].'</p></td>
    <td colspan="2" valign="top"><p align="center">'.$etapa_ot1_0_fecha_generacion.'</p></td>
    <td  valign="top"><p align="center">'.$etapa_ot1_0_fecha_vencimiento.'</p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  width="24%" colspan= "2" valign="top"><p align="center"></p></td>
  </tr>
  <tr class="amano" >
    <td  valign="top"><h2 align="center"><strong>1-2</strong><strong> </strong></h2></td>
    <td  valign="top"><p align="center"></p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  valign="top" colspan="2"><p align="center"></p></td>

    <td  valign="top"><p align="center"></p></td>
	<td width="6%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
  </tr>
  <tr class="amano2" align="left" >
    <td colspan="3"  valign="top">  Instru:</td> 
	<td colspan="3"> Obs: </td>
    <td colspan="4" align="left" valign="top">Lotes NC:</td>
  </tr>
  <tr>
    <td  valign="top" style="color:blue;"><h2 align="center"><strong>2-0</strong></h2></td>
    <td  valign="top"><p>'.substr($etapas_ot1[1]["Producto"]["d_producto"],0,24).'</p></td>
 	<td  valign="top"><p align="center">'.$etapas_ot1[1]["maquina"]["Producto"]["codigo"].'</p></td>
    <td colspan="2" valign="top"><p align="center">'.$etapa_ot1_1_fecha_generacion.'</p></td>
    <td  valign="top"><p align="center">'.$etapa_ot1_1_fecha_vencimiento.'</p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  width="24%" colspan= "2" valign="top"><p align="center"></p></td>
  </tr>
    <tr class="amano" >
    <td  valign="top"  ><h2 align="center"><strong>2-2</strong><strong> </strong></h2></td>
    <td  valign="top"><p align="center"></p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  valign="top" colspan="2"><p align="center"></p></td>

    <td  valign="top"><p align="center"></p></td>
	<td width="6%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
  </tr>
  <tr class="amano2" align="left" >
    <td colspan="3"  valign="top">  Instru:</td> 
	<td colspan="3"> Obs: </td>
    <td colspan="4" align="left" valign="top">Lotes NC:</td>
  </tr>
  <tr>
    <td  valign="top" style="color:blue;"><h2 align="center"><strong>3-0</strong></h2></td>
    <td  valign="top"><p>'.substr($etapas_ot1[2]["Producto"]["d_producto"],0,24).'</p></td>
    <td  valign="top"><p align="center">'.$etapas_ot1[2]["maquina"]["Producto"]["codigo"].'</p></td>
    <td colspan="2" valign="top"><p align="center">'.$etapa_ot1_2_fecha_generacion.'</p></td>
    <td  valign="top"><p align="center">'.$etapa_ot1_2_fecha_vencimiento.'</p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  width="24%" colspan= "2" valign="top"><p align="center"></p></td>
  </tr>
  
   <tr class="amano" >
    <td  valign="top"  ><h2 align="center"><strong>3-2</strong><strong> </strong></h2></td>
    <td  valign="top"><p align="center"></p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  valign="top" colspan="2"><p align="center"></p></td>

    <td  valign="top"><p align="center"></p></td>
	<td width="6%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
  </tr>
  
  
  <tr class="amano2" align="left" >
    <td colspan="3"  valign="top">  Instru:</td> 
	<td colspan="3"> Obs: </td>
    <td colspan="4" align="left" valign="top">Lotes NC:</td>
  </tr>
  
  
  
  
</table>

<table width="100%" border="1">
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>

<table border="1" cellspacing="0"  width="100%" style="font-size:9px;">
  <tr>
    <td width="10%" rowspan="2" valign="top"> <img src="'.$datos_empresa["DatoEmpresa"]["app_path"].$logo.'" width="150" height="44" /></td>
	<td width="66%" colspan="5"  ><h6 style="font-size:12px;"> PLANILLA DE PRODUCCION EN PROCESO (PPP)</h6></td>
	<td width="12%" valign="top"><p align="center">Fch Emision</p></td>
    <td width="12%" valign="top"><p align="center">Fch Entrega </p></td>
  </tr>
  <tr valign="center">
	<td colspan="7" width="66%" valign="center" >
		<p style="font-size:17px;"> OP '.$ot2["Comprobante"]["nro_comprobante_completo"].' ['.$producto_codigo2.']</p>
      </td>
	<td width="12%" ><p valign="center" align="center" style="font-size:10px;">
      '.$fecha_emision2->format('d-m-y').'</p></td>
    <td width="12%" ><p valign="center" align="center" style="font-size:10px;" >
      '.$fecha_entrega2->format('d-m-y').'</p></td>
  </tr>
  <tr>
    <td rowspan = "2" width="10%"><p style="font-size:6px;" >Form.03 Rev. 03  Vigencia 08/2018</p></td>
    <td rowspan = "2" colspan="4" width="40%"><p style="font-size:9px;">'.$producto_descripcion2.' </p></td>
    <td colspan="2" valign="top" width="26%"><p style="font-size:7px;" align="center">Nº Plano - Letra /Rev. / Fecha:</p></td>
    <td width="12%"><p style="font-size:7px;" align="center">CANT. PLANIF.</p></td>
    <td width="12%"><p style="font-size:7px;" align="center">CANT. PROD.</p></td>
  </tr>
  <tr>
    <td width="26%" colspan="7" style="text-align:center;font-size:10px;"><p></p></td>
    <td width="12%"><p align="center">'.$cantidad2.'</p></td>
    <td width="12%"><p align="center">'.$cantidad_cierre2.'</p></td>
  </tr>
  <tr class="lotesh">
    <td colspan="9" width="100%" ><p align="left" style="font-size:10px;" >Lotes de MP*:</p></td>
  </tr>
  
  <tr>
    <td width="6%"rowspan="2"><h4 align="center">Etapa</h4></td>
    <td width="26%"><h4 align="center">Operación </h4></td>
    <td width="6%"><h4 align="center">MAQ</h4></td>
    <td width="16%" rowspan="2" colspan="2"><h4 align="center">Fecha Inicio</h4></td>
    <td width="16%" rowspan="2"><h4 align="center">Fecha Fin</h4></td>
    <td width="6%" valign="top"><h4 align="center">FVerif</h4></td>
    <td width="24%" colspan= "3" valign="top"><h4 align="center">Liberada Por:</h4></td>
  </tr>
  <tr>
	<td width="26%" ><h4 align="center">Ejecutor </h4></td>
	<td width="6%" ><h4 align="center">TC </h4></td>
    <td width="6%" valign="top"><h4 align="center">CNF</h4></td>
    <td width="8%" valign="top"><h4 align="center">NC</h4></td>
	<td width="8%" valign="top"><h4 align="center">RP</h4></td>
	<td width="8%" valign="top"><h4 align="center">SCRP</h4></td>
  </tr>
  <tr>
    <td  valign="top" style="color:blue;"><h2 align="center"><strong>1-0</strong></h2></td>
    
    <td  valign="top"><p>'.substr($etapas_ot2[0]["Producto"]["d_producto"],0,24).'</p></td>
    <td  valign="top"><p align="center">'.$etapas_ot2[0]["maquina"]["Producto"]["codigo"].'</p></td>
    <td colspan="2" valign="top"><p align="center">'.$etapa_ot2_0_fecha_generacion.'</p></td>
    <td  valign="top"><p align="center">'.$etapa_ot2_0_fecha_vencimiento.'</p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  width="24%" colspan= "2" valign="top"><p align="center"></p></td>
  </tr>
  <tr class="amano" >
    <td  valign="top"  ><h2 align="center"><strong>1-2</strong><strong> </strong></h2></td>
    <td  valign="top"><p align="center"></p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  valign="top" colspan="2"><p align="center"></p></td>

    <td  valign="top"><p align="center"></p></td>
	<td width="6%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
  </tr>
  <tr class="amano2" align="left" >
    <td colspan="3"  valign="top">  Instru:</td> 
	<td colspan="3"> Obs: </td>
    <td colspan="4" align="left" valign="top">Lotes NC:</td>
  </tr>
  <tr>
    <td  valign="top" style="color:blue;"><h2 align="center"><strong>2-0</strong></h2></td>
   
    <td  valign="top"><p>'.substr($etapas_ot2[1]["Producto"]["d_producto"],0,24).'</p></td>
 	<td  valign="top"><p align="center">'.$etapas_ot2[1]["maquina"]["Producto"]["codigo"].'</p></td>
    <td colspan="2" valign="top"><p align="center">'.$etapa_ot2_1_fecha_generacion.'</p></td>
    <td  valign="top"><p align="center">'.$etapa_ot2_1_fecha_vencimiento.'</p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  width="24%" colspan= "2" valign="top"><p align="center"></p></td>
  </tr>
    <tr class="amano" >
    <td  valign="top"  ><h2 align="center"><strong>2-2</strong><strong> </strong></h2></td>
    <td  valign="top"><p align="center"></p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  valign="top" colspan="2"><p align="center"></p></td>

    <td  valign="top"><p align="center"></p></td>
	<td width="6%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
  </tr>
  <tr class="amano2" align="left" >
    <td colspan="3"  valign="top">  Instru:</td> 
	<td colspan="3"> Obs: </td>
    <td colspan="4" align="left" valign="top">Lotes NC:</td>
  </tr>
  <tr>
    <td  valign="top" style="color:blue;"><h2 align="center"><strong>3-0</strong></h2></td>
   
    <td  valign="top"><p>'.substr($etapas_ot2[2]["Producto"]["d_producto"],0,24).'</p></td>
    <td  valign="top"><p align="center">'.$etapas_ot2[2]["maquina"]["Producto"]["codigo"].'</p></td>
    <td colspan="2" valign="top"><p align="center">'.$etapa_ot2_2_fecha_generacion.'</p></td>
    <td  valign="top"><p align="center">'.$etapa_ot2_2_fecha_vencimiento.'</p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  width="24%" colspan= "2" valign="top"><p align="center"></p></td>
  </tr>
  
   <tr class="amano" >
    <td  valign="top"  ><h2 align="center"><strong>3-2</strong><strong> </strong></h2></td>
    <td  valign="top"><p align="center"></p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  valign="top" colspan="2"><p align="center"></p></td>

    <td  valign="top"><p align="center"></p></td>
	<td width="6%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
  </tr>
  
  
  <tr class="amano2" align="left" >
    <td colspan="3"  valign="top">  Instru:</td> 
	<td colspan="3"> Obs: </td>
    <td colspan="4" align="left" valign="top">Lotes NC:</td>
  </tr>
  
  
  
  
</table>

';




$Tablas  =  $TablaLado1;


//$producto_codigo = 'UPR-CAPO0-015';/*$datos_pdf['ComprobanteItem'][0]['Producto']['codigo'];*/
//$producto_descripcion ='TAPA GRANDE'; /* $datos_pdf['ComprobanteItem'][0]['Producto']['d_producto'];*/
$pdf->StartTransform();




$html = <<<EX
<style type="text/css">
body p {
	font-family: Arial, Helvetica, sans-serif;
}
tr.amano {
    line-height: 20px;
	color:green;
}
tr.amano2 {
    line-height: 33px;
	color:green;
}
tr.lotesh {
    line-height: 40px;
	color:green;
}
p { 
  margin: 0 auto;
}
 /*th, td {
    border: 1px solid blue;
    height: 20px;
}*/
</style>


{$Tablas}



EX;
$pdf->writeHTML($html, true, false, true, false, '');


$pdf->AddPage('P', 'A4');
//$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', "", $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);


//<td width="6%" valign="top"><h4 align="center">TC</h4></td>


$TablaLado2 = 
'
<table border="1" cellspacing="0"  width="100%" style="font-size:9px;">
  
 
  <tr>
    <td width="6%"  rowspan="2"><h4 align="center">Etapa</h4></td>
    <td width="26%" ><h4 align="center">Operación </h4></td>
    <td width="6%"  ><h4 align="center">MAQ</h4></td>
    <td width="16%" rowspan="2" colspan="2"><h4 align="center">Fecha Inicio</h4></td>
    <td width="16%" rowspan="2"><h4 align="center">Fecha Fin</h4></td>
    <td width="6%" valign="top"><h4 align="center">FVerif</h4></td>
    <td width="24%" colspan= "3" valign="top"><h4 align="center">Liberada Por:</h4></td>
  </tr>
  <tr>
	<td width="26%" ><h4 align="center">Ejecutor </h4></td>
	<td width="6%" valign="top"><h4 align="center">TC</h4></td>
    <td width="6%" valign="top"><h4 align="center">CNF</h4></td>
    <td width="8%" valign="top"><h4 align="center">NC</h4></td>
	<td width="8%" valign="top"><h4 align="center">RP</h4></td>
	<td width="8%" valign="top"><h4 align="center">SCRP</h4></td>
  </tr>
  <tr>
    <td  valign="top" style="color:blue;"><h2 align="center"><strong>4-0</strong></h2></td>
   
    <td  valign="top"><p>'.substr($etapas_ot1[3]["Producto"]["d_producto"],0,24).'</p></td>
    <td  valign="top"><p align="center">'.$etapas_ot1[3]["maquina"]["Producto"]["codigo"].'</p></td>
    <td colspan="2" valign="top"><p align="center">'.$etapa_ot1_3_fecha_generacion.'</p></td>
    <td  valign="top"><p align="center">'.$etapa_ot1_3_fecha_vencimiento.'</p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  width="24%" colspan= "2" valign="top"><p align="center"></p></td>
  </tr>
  <tr class="amano" >
    <td  valign="top"  ><h2 align="center"><strong>4-2</strong><strong> </strong></h2></td>
    <td  valign="top"><p align="center"></p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  valign="top" colspan="2"><p align="center"></p></td>

    <td  valign="top"><p align="center"></p></td>
	<td width="6%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
  </tr>
  <tr class="amano2" align="left" >
    <td colspan="3"  valign="top">  Instru:</td> 
	<td colspan="3"> Obs: </td>
    <td colspan="4" align="left" valign="top">Lotes NC:</td>
  </tr>
  <tr>
    <td  valign="top" style="color:blue;"><h2 align="center"><strong>5-0</strong></h2></td>
   
    <td  valign="top"><p>'.substr($etapas_ot1[4]["Producto"]["d_producto"],0,24).'</p></td>
 	<td  valign="top"><p align="center">'.$etapas_ot1[4]["maquina"]["Producto"]["codigo"].'</p></td>
    <td colspan="2" valign="top"><p align="center">'.$etapa_ot1_4_fecha_generacion.'</p></td>
    <td  valign="top"><p align="center">'.$etapa_ot1_4_fecha_vencimiento.'</p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  width="24%" colspan= "2" valign="top"><p align="center"></p></td>
  </tr>
    <tr class="amano" >
    <td  valign="top"  ><h2 align="center"><strong>5-2</strong><strong> </strong></h2></td>
    <td  valign="top"><p align="center"></p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  valign="top" colspan="2"><p align="center"></p></td>

    <td  valign="top"><p align="center"></p></td>
	<td width="6%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
  </tr>
  
  <tr class="amano2" align="left" >
    <td colspan="3"  valign="top">  Instru:</td> 
	<td colspan="3"> Obs: </td>
    <td colspan="4" align="left" valign="top">Lotes NC:</td>
  </tr>
  <tr>
    <td  valign="top" style="color:blue;"><h2 align="center"><strong>6-0</strong></h2></td>
   
    <td  valign="top"><p>'.substr($etapas_ot1[5]["Producto"]["d_producto"],0,24).'</p></td>
    <td  valign="top"><p align="center">'.$etapas_ot1[5]["maquina"]["Producto"]["codigo"].'</p></td>
    <td colspan="2" valign="top"><p align="center">'.$etapa_ot1_5_fecha_generacion.'</p></td>
    <td  valign="top"><p align="center">'.$etapa_ot1_5_fecha_vencimiento.'</p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  width="24%" colspan= "2" valign="top"><p align="center"></p></td>
  </tr>
  
   <tr class="amano" >
    <td  valign="top"  ><h2 align="center"><strong>6-2</strong><strong> </strong></h2></td>
    <td  valign="top"><p align="center"></p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  valign="top" colspan="2"><p align="center"></p></td>

    <td  valign="top"><p align="center"></p></td>
	<td width="6%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
  </tr>
  
  
  <tr class="amano2" align="left" >
    <td colspan="3"  valign="top">  Instru:</td> 
	<td colspan="3"> Obs: </td>
    <td colspan="4" align="left" valign="top">Lotes NC:</td>
  </tr>

  <tr class="lotesh">
    <td colspan="9" width="100%"  ><p align="left" style="font-size:10px;" >Observaciones:</p></td>
  </tr>
  <tr class="amanoCabecera" border="0" align="left" >
    <td colspan="9"  valign="top"></td> 
	
  </tr>
  
</table>

<table width="100%" border="1">
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>

<table border="1" cellspacing="0"  width="100%" style="font-size:9px;">

  <tr>
    <td width="6%"  rowspan="2"><h4 align="center">Etapa</h4></td>
    <td width="26%" ><h4 align="center">Operación </h4></td>
    <td width="6%"  ><h4 align="center">MAQ</h4></td>
    <td width="16%" rowspan="2" colspan="2"><h4 align="center">Fecha Inicio</h4></td>
    <td width="16%" rowspan="2"><h4 align="center">Fecha Fin</h4></td>
    <td width="6%" valign="top"><h4 align="center">FVerif</h4></td>
    <td width="24%" colspan= "3" valign="top"><h4 align="center">Liberada Por:</h4></td>
  </tr>
  <tr>
	<td width="26%" ><h4 align="center">Ejecutor </h4></td>
	<td width="6%" valign="top"><h4 align="center">TC</h4></td>
    <td width="6%" valign="top"><h4 align="center">CNF</h4></td>
    <td width="8%" valign="top"><h4 align="center">NC</h4></td>
	<td width="8%" valign="top"><h4 align="center">RP</h4></td>
	<td width="8%" valign="top"><h4 align="center">SCRP</h4></td>
  </tr>
  <tr>
    <td  valign="top" style="color:blue;"><h2 align="center"><strong>4-0</strong></h2></td>
   
    <td  valign="top"><p>'.substr($etapas_ot2[3]["Producto"]["d_producto"],0,24).'</p></td>
    <td  valign="top"><p align="center">'.$etapas_ot2[3]["maquina"]["Producto"]["codigo"].'</p></td>
    <td colspan="2" valign="top"><p align="center">'.$etapa_ot2_3_fecha_generacion.'</p></td>
    <td  valign="top"><p align="center">'.$etapa_ot1_3_fecha_vencimiento.'</p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  width="24%" colspan= "2" valign="top"><p align="center"></p></td>
  </tr>
  <tr class="amano" >
    <td  valign="top"  ><h2 align="center"><strong>4-2</strong><strong> </strong></h2></td>
    <td  valign="top"><p align="center"></p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  valign="top" colspan="2"><p align="center"></p></td>

    <td  valign="top"><p align="center"></p></td>
	<td width="6%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
  </tr>
  
  <tr class="amano2" align="left" >
    <td colspan="3"  valign="top">  Instru:</td> 
	<td colspan="3"> Obs: </td>
    <td colspan="4" align="left" valign="top">Lotes NC:</td>
  </tr>
  
  <tr>
    <td  valign="top" style="color:blue;"><h2 align="center"><strong>5-0</strong></h2></td>
   
    <td  valign="top"><p>'.substr($etapas_ot2[4]["Producto"]["d_producto"],0,24).'</p></td>
 	<td  valign="top"><p align="center">'.$etapas_ot2[4]["maquina"]["Producto"]["codigo"].'</p></td>
    <td colspan="2" valign="top"><p align="center">'.$etapa_ot2_4_fecha_generacion.'</p></td>
    <td  valign="top"><p align="center">'.$etapa_ot2_4_fecha_vencimiento.'</p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  width="24%" colspan= "2" valign="top"><p align="center"></p></td>
  </tr>
    <tr class="amano" >
    <td  valign="top"  ><h2 align="center"><strong>5-2</strong><strong> </strong></h2></td>
    <td  valign="top"><p align="center"></p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  valign="top" colspan="2"><p align="center"></p></td>

    <td  valign="top"><p align="center"></p></td>
	<td width="6%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
  </tr>
  <tr class="amano2" align="left" >
    <td colspan="3"  valign="top">  Instru:</td> 
	<td colspan="3"> Obs: </td>
    <td colspan="4" align="left" valign="top">Lotes NC:</td>
  </tr>
  <tr>
    <td  valign="top" style="color:blue;"><h2 align="center"><strong>6-0</strong></h2></td>
   
    <td  valign="top"><p>'.substr($etapas_ot2[5]["Producto"]["d_producto"],0,24).'</p></td>
    <td  valign="top"><p align="center">'.$etapas_ot2[5]["maquina"]["Producto"]["codigo"].'</p></td>
    <td colspan="2" valign="top"><p align="center">'.$etapa_ot2_5_fecha_generacion.'</p></td>
    <td  valign="top"><p align="center">'.$etapa_ot2_5_fecha_vencimiento.'</p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  width="24%" colspan= "2" valign="top"><p align="center"></p></td>
  </tr>
  
   <tr class="amano" >
    <td  valign="top"  ><h2 align="center"><strong>6-2</strong><strong> </strong></h2></td>
    <td  valign="top"><p align="center"></p></td>
    <td  valign="top"><p align="center">&nbsp;</p></td>
    <td  valign="top" colspan="2"><p align="center"></p></td>

    <td  valign="top"><p align="center"></p></td>
	<td width="6%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
	<td width="8%" valign="top"><h4 align="center"></h4></td>
  </tr>
  
  
  <tr class="amano2" align="left" >
    <td colspan="3"  valign="top">  Instru:</td> 
	<td colspan="3"> Obs: </td>
    <td colspan="4" align="left" valign="top">Lotes NC:</td>
  </tr>
  
    <tr class="lotesh">
    <td colspan="9" width="100%"  ><p align="left" style="font-size:10px;" >Observaciones:</p></td>
  </tr>
  <tr class="amanoCabecera" border="0" align="left" >
    <td colspan="9"  valign="top"></td> 
	
  </tr>

  
</table>

';

$html = <<<EX
<style type="text/css">
body p {
	font-family: Arial, Helvetica, sans-serif;
}
tr.amano {
    line-height: 20px;
	color:green;
}
tr.amano2 {
    line-height: 33px;
	color:green;
}
tr.lotesh {
    line-height: 40px;
	color:green;
}
/*tr.amanoCabecera = Esto es equivalente con la cabecera del cabecera (los primero 2 TRs Planilla y Vigencia)*/
tr.amanoCabecera {
    line-height: 61px; 
	color:green;
}
p { 
  margin: 0 auto;
}
/*th, td {
    border: 1px solid blue;
    height: 20px;
}*/
</style>


{$TablaLado2}



EX;

$pdf->writeHTML($html, true, false, true, false, '');


ob_clean();
$pdf->Output($ot1["TipoComprobante"]["abreviado_tipo_comprobante"].'_'.$ot1['Comprobante']['nro_comprobante_completo'].'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+