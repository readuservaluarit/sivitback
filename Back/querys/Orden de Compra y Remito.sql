/*
Se para sobre items de OC de un año determinado y te trae los remitos de compra asociados



*/

SELECT 
  comprobante.`nro_comprobante` AS nro_oc,
   comprobante_item.`n_item`,
   estado_comprobante.`d_estado_comprobante` AS estado_oc,
     producto.`codigo`,
  comprobante.`razon_social`,
  comprobante.`fecha_generacion` AS fecha_carga_oc,
  comprobante.`fecha_entrega` AS fecha_entrega_prometida_oc,
  moneda.`simbolo_internacional`,
  REPLACE(comprobante_item.`precio_unitario`,".",",") AS precio_unitario,
  comprobante_item.`cantidad` cantidad_item_oc,

  remito_item.cantidad AS cantidad_recibida,
  comprobante_item.`n_item`,

 
  comprobante_item.`dias`,
   

  CAST(
    (
      `comprobante`.`fecha_generacion` + INTERVAL IF(
        ISNULL(`comprobante_item`.`dias`),
        0,
        `comprobante_item`.`dias`
      ) DAY
    ) AS DATE
  ) AS fecha_entrega_item_prometida_oc,
  
  remito.`nro_comprobante` AS numero_remito,
  
  remito.`fecha_generacion` AS fecha_remito
  
  
  
 
  
  


  
FROM
  comprobante_item 
  LEFT JOIN comprobante 
    ON comprobante.`id` = comprobante_item.`id_comprobante` 
  LEFT JOIN moneda 
    ON moneda.id = comprobante.`id_moneda` 
  LEFT JOIN persona 
    ON persona.id = comprobante.id_persona 
 
  LEFT JOIN comprobante_item remito_item 
    ON remito_item.`id_comprobante_item_origen` = comprobante_item.`id` 
  LEFT JOIN comprobante remito 
    ON remito.id = remito_item.`id_comprobante` 

  LEFT JOIN producto 
    ON producto.id = comprobante_item.`id_producto` 
  LEFT JOIN estado_comprobante ON comprobante.`id_estado_comprobante` = estado_comprobante.`id`
    
    
WHERE comprobante.id_tipo_comprobante = 302 


AND comprobante_item.`activo` = 1 
AND remito.id_tipo_comprobante=303
AND remito_item.activo=1
AND YEAR(comprobante.fecha_generacion)=2019

AND comprobante.`id_estado_comprobante` <>2
AND remito.`id_estado_comprobante` <>2

ORDER BY comprobante.`nro_comprobante`,comprobante_item.`n_item` ASC

LIMIT 1000000000000