    <?php
App::uses('AppModel', 'Model');
/**
 * Usuario Model
 *
 * @property Rol $Rol
 * @property Jurisdiccion $Jurisdiccion
 * @property Region $Region
 * @property Lote $Lote
 * @property Segmento $Segmento
 * @property Asignacion $Asignacion
 * @property Auditorium $Auditorium
 */
class Usuario extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'usuario';
    public $actsAs = array('Containable');
    public $array_open_form_chequeo = array();

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id_rol' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
        'nombre' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar un nombre y apellido.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
    	'email' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar un e-mail valido.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
    	'username' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar un usuario.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
    	'password' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar una contrase&ntilde;a.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	
 public $belongsTo = array(
        'Rol' => array(
            'className' => 'Rol',
            'foreignKey' => 'id_rol',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'PuntoVenta' => array(
            'className' => 'PuntoVenta',
            'foreignKey' => 'id_punto_venta_fiscal',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Persona' => array(
        'className' => 'Persona',
        'foreignKey' => 'id_persona',
        'dependent' => false,
        'conditions' => '',
        'fields' => '',
        
        )
       
        ); 
        
public $hasMany = array(
        'NotaUsuario' => array(
            'className' => 'NotaUsuario',
            'foreignKey' => 'id_usuario',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        );        



public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
	
	
	
	$this->virtual_fields_view = array(
	
	"d_rol"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
	
	);
	
}
         

	public function beforeSave($options = array()) {
        // solo en el alta
		if (empty($this->data[$this->alias]['id']) && isset($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}
        
		return true;
	}
    public function beforeDelete($options = array()) {
        $count = $this->Asignacion->find("count", array(
            "conditions" => array("id_usuario" => $this->id)
        ));
        
        if ($count > 0)
            throw new Exception("El Usuario tiene Asignaciones.");
        
       $count_asociaciones = $this->AsociacionUsuario->find("count", array(
            "conditions" => array("id_usuario_primario" => $this->id)
        ));
        
        
        if ($count_asociaciones > 0)
            throw new Exception("El Usuario tiene Asociado otros usuarios, no se puede borrar."); 
        
        $count_asignaciones = $this->AsociacionUsuario->find("count", array(
            "conditions" => array("id_usuario_secundario" => $this->id)
        ));
        
        
        if ($count_asignaciones > 0)
            throw new Exception("El Usuario esta Asociado, no se puede borrar.");
            
           
            
        $count_asignaciones_lote = $this->UsuarioLote->find("count", array(
            "conditions" => array("id_usuario" => $this->id)
        ));
        
        
        if ($count_asignaciones_lote > 0)
            throw new Exception("El Usuario Tiene lotes asociados, no se puede borrar.");      
        
          
       
        $count_asignaciones_segmentos= $this->UsuarioSegmento->find("count", array(
            "conditions" => array("id_usuario" => $this->id)
        ));
        
        
        if ($count_asignaciones_segmentos > 0)
            throw new Exception("El Usuario Tiene Segmentos asociados, no se puede borrar.");
            
        return true;
    }
    
    
    
    function getNombreYApellido($id_usuario){
    	
    	
    	$persona = $this->find('first', array(
    			'conditions' => array('Usuario.id' => $id_usuario),
    			'contain' =>false
    	));
    	
    	return $persona["Usuario"]["nombre"];
    }
    
    
    
    public function MisDatos($data,$controller_pointer){
    	
    	
    	$tipo_comprobante_class = ClassRegistry::init('TipoComprobante');
    	$tipo_comprobante_persona_class = ClassRegistry::init('TipoComprobantePersona');
    	$sistema_moneda_enlazada_class = ClassRegistry::init('SistemaMonedaEnlazada');
    	$entidad_class = ClassRegistry::init('Entidad');
    	
    	//$data = $this->request->data;
    	
    	unset($data["Usuario"]["password"]);
    	
    	$data["Usuario"]["nro_punto_venta_fiscal"] = $data["PuntoVenta"]["numero"];
    	$data["Usuario"]["d_punto_venta_fiscal"] = $data["PuntoVenta"]["d_punto_venta"];
    	
    	$data["Usuario"]["d_rol"] = $data["Rol"]["descripcion"];
    	
    	$data['Server']['fecha_hoy'] = (string) date("Y-m-d H:i:s");      //TODO: IMPORTANTE que el front LEA ESTO es para que el front no tome la hora local de la maquina sino la del servidor
    	$data['Server']['fecha_formato'] = (string) 'YYYY-MM-DD HH:MM:SS';      //TODO: que el front LEA ESTO
    	
    	
    	//$data['Cache']['cache_index_estatica_cliente'] = (string) md5_file(EnumCacheName::getCacheIndexTablasEstaticaSistema);
    	
    	
    	$chequeos = $this->chequeos();
    	
    	
    	if(count($chequeos)>0)
    		$data["Chequeo"]["mensaje"] = "&bull;".(string)implode("\n\r \n\r &bull;", $chequeos); /**/
    	else
    		$data["Chequeo"]["mensaje"] = ""; /**/
    			
    		$data["Chequeo"]["mensaje_open_form"] = $this->array_open_form_chequeo;
    			/*--------------------------------------------------------------------*/
    			
    		//$data["TipoComprobanteController"]["content"] =  $tipo_comprobante_class->getTiposComprobanteController($tipo_comprobante);
    			
    			
    		
    	
    		
    		
    		
    			//$data["CacheModel"]["fecha_modificacion"] =
    			// $data["Moneda"]["content"] =  $this->Moneda->find('all',array("contain"=>false));
    			
    			//$this->getTiposComprobanteController($tipo_comprobante);
    			//$data["TipoComprobanteController"]["tipo_comprobante"] =  $tipo_comprobante;
    			
    			unset($data["PuntoVenta"]);
    			
    			//$permisos = $this->allpermision();
    			//$menu = $this->getMenu($controller_pointer);
    			//$estados_comprobante = $this->EstadoTipoComprobante->getEstados();
    			//$data["EstadoTipoComprobante"] =   $estados_comprobante;
    			//$data["Permisos"] = $permisos;
    			//$data["Menu"] = $menu;								  
    			$data["PaginadoSettings"]["MaxRowsByPage"] = (string) $controller_pointer->numrecords;//pasarlo a archivo config
    			
    			
    			
    			
    			return $data;
    }
    
    
    
    
    
    
    public function allpermision(){
    	
    	
    	$sm = ClassRegistry::init('SecurityManager');
    	
    	$permisos = $sm->getallpermision(AuthComponent::user('username')); //retorno todos los permisos del usuario
    	return $permisos;
    	
    	
    }
    
    public function chequeos(){
    	
    	
    	
    	$periodo_contable_class = ClassRegistry::init('PeriodoContable');
    	$ejercicio_contable_class = ClassRegistry::init('EjercicioContable');
    	$modulo_class = ClassRegistry::init('Modulo');
    	$chequera_class = ClassRegistry::init('Chequera');
    	$cuenta_contable_class = ClassRegistry::init('CuentaContable');
    	$lista_precio_class = ClassRegistry::init('ListaPrecio');
    	$producto_tipo_class = ClassRegistry::init('ProductoTipo');
    	$deposito_movimiento_tipo_class = ClassRegistry::init('DepositoMovimientoTipo');
    	$comprobante_class = ClassRegistry::init('Comprobante');
    	
    	App::uses('CakeSession', 'Model/Datasource');
    	$datoEmpresa = CakeSession::read('Empresa');
    	
    	
    
    	
    	$comprobante = new Comprobante();
    	
    	$mensajes = array();
    	
    	
    	$modulo_contable  = $modulo_class->estaHabilitado(EnumModulo::CONTABILIDAD);
    	$modulo_stock  = $modulo_class->estaHabilitado(EnumModulo::STOCK);
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	if($modulo_contable== 1){
    		
    		$periodo_actual =  $periodo_contable_class->getPeriodoActual();
    		$ejercicio_actual =  $ejercicio_contable_class->GetEjercicioPorFecha(date("Y-m-d"),0,1);//le pido el actual segun la fecha
    		
    		if($periodo_actual == 0){
    			
    			array_push($mensajes,"Debe Haber al menos un Peri&oacute;do Contable Actual(Disponible y abierto) que este dentro del rango de la fecha actual con la fecha actual. Ademas debe tener activo al menos un Ejercicio Contable.");
    			array_push($this->array_open_form_chequeo,EnumMenu::mnuPeriodoContable);
    		}
    		
    		if($ejercicio_actual == 0){
    			
    			array_push($mensajes,"Debe Haber al menos un Ejercicio Contable (Disponible y abierto)");
    			
    			array_push($this->array_open_form_chequeo,EnumMenu::mnuEjercicio);
    			
    		}else{
    			if(isset($ejercicio_actual["EjercicioContable"]) && !$ejercicio_actual["EjercicioContable"]["id_contador"]>0){
    				array_push($mensajes,"Debe definir un Contador para el Ejercicio Contable en curso.");
    				array_push($this->array_open_form_chequeo,EnumMenu::mnuEjercicio);
    			}
    		}
    		
    		$id_cuenta_contable_resultado_ejercicio = $datoEmpresa["DatoEmpresa"]["id_cuenta_contable_resultado_ejercicio"];
    		$id_cuenta_contable_resultado_no_asignado = $datoEmpresa["DatoEmpresa"]["id_cuenta_resultado_no_asignado"];
    		
    		if($id_cuenta_contable_resultado_ejercicio == ""){
    			
    			array_push($mensajes,"Debe definir una cuenta Resultado de ejercicio en Dato Empresa.");
    		}
    		
    		if($id_cuenta_contable_resultado_no_asignado == ""){
    			
    			array_push($mensajes,"Debe definir una cuenta Resultado de ejercicio NO asignado en Dato Empresa.");
    		}
    		
    		if(!$cuenta_contable_class->ExistenRaices()>0){
    			array_push($mensajes,"No tiene ninguna Cuenta Contable raiz (parent_id = null) configurada. \n EJ: Activos,Pasivos, Patrimonio Neto .");
    			array_push($this->array_open_form_chequeo,EnumMenu::mnuCuentaContable);
    		}
    	}
    	
    	if( $modulo_class->estaHabilitado(EnumModulo::TESORERIA) &&  $modulo_class->estaHabilitado(EnumModulo::COMPRAS)
    			&&  $modulo_class->estaHabilitado(EnumModulo::CHEQUES)
    			){//chequeo que haya una chequera disponible
    				if(!$chequera_class->ExisteChequeraDisponible()>0){
    					array_push($mensajes,"No tiene cargada una chequera con cheques disponibles. Ingrese a Tesoreria->Chequeras para poder dar de alta una nueva.");
    					array_push($this->array_open_form_chequeo,EnumMenu::mnuChequera);
    				}
    	}
    	
    	
    	
    	if($lista_precio_class->getDefault() == 0){
    		
    		array_push($mensajes,"Debe definir una Lista de Precio por defecto.");
    		array_push($this->array_open_form_chequeo,EnumMenu::mnuListaPrecioVenta);
    	}
    	
    	
    	
    	return   $mensajes;
    }
    
    
    
    public function getMenu($controller_pointer){
    	
    	
    	
    
    		App::import('Lib', 'Menu');
    		$menu = new Menu($controller_pointer);
    		$menu_aux = $menu->getMenujson();
    		$menu_envio = array();
    		
    		foreach ($menu_aux as &$item){
    			$menu_children = array();
    			
    			if(count($item["childrens"]) > 0 ){
    				foreach ($item["childrens"] as $children){
    					$menu_subchildren = array();
    					
    					if(isset($children["childrens"])){
    						if(count($children["childrens"]) > 0 ){
    							foreach ($children["childrens"] as $subchildren){
    								array_push($menu_subchildren,$subchildren);
    								
    							}
    							$children["childrens"] =  $menu_subchildren;
    						}
    					}
    					
    					array_push($menu_children,$children);
    				}
    				$item["childrens"] =  $menu_children;
    			}
    			array_push($menu_envio,$item);
    		}
    		//$newMenu = array_values( (array)$menu->getMenujson() );
    		
    		
    		//$menu_envio = json_encode($menu_envio), FALSE);
    		
    		return $menu_envio;
    		//$this->layout = 'ajax';
    		//die();
    		
    		
    	
    	
    	
    }
    
    
    
    public function getUsuario($id_usuario,$contain=array()){
    	
    	return $this->find('first',array('conditions'=>array("Usuario.id"=>$id_usuario),'contain'=>$contain));
    	
    }
    
    
}
