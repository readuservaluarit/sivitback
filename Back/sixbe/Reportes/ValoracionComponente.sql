/* INVENTARIO valoracion de la materia prima que hay en stock*/
SELECT mp.codigo ,smp.`stock`,mp.`costo` AS valor_unitario, mp.`costo`*smp.`stock` AS valor_inventario FROM stock_componente smp 
JOIN componente mp ON mp.id = smp.`id`
WHERE smp.stock>0
ORDER BY stock DESC