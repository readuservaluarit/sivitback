SELECT  provincia.`d_provincia` AS 'Provincia',

SUM(  ROUND((comprobante.`subtotal_neto`/comprobante.valor_moneda2)*tipo_comprobante.signo_comercial,2)) 
-
/*LE RESTO LOS NO GRAVADOS Y LOS EXENTOS*/
(SELECT 
 SUM( ROUND((comprobante_impuesto.`base_imponible`/comp.valor_moneda2)*tipo_comprobante.signo_comercial,2)) total
   FROM comprobante_impuesto
   JOIN comprobante comp ON comp.id = comprobante_impuesto.`id_comprobante`
   JOIN tipo_comprobante ON comp.`id_tipo_comprobante` = tipo_comprobante.`id`
   JOIN persona ON persona.id = comp.`id_persona`
  WHERE id_tipo_comprobante 
  /*IN (801, 806,817,803,808,802,807) -- VIEJO */
  IN (801,806,811,817,833,804,809,815,836,803,808,813,835,818,802,807,812,834,819)  /*Exactamente igual que el IN de abajo */
 AND cae > 0 
 AND YEAR(fecha_contable) = 2019
 AND MONTH(fecha_contable)= MONTH(comprobante.fecha_contable)
 AND comprobante_impuesto.`id_impuesto` = 1  /*I.V.A. NORMAL*/
 AND comprobante_impuesto.`tasa_impuesto` =0  /*0 % indica NO GRABADOS Y EXENTOS*/
 /* IFNULL - Esto lo tiene porque no reconoce bien el NULL*/
 /*AND  IFNULL(Comprobante.`id_detalle_tipo_comprobante`,0)!=5 
 /*Detalle de las notas de debito(803) = Cheque rechazado*/
)   		              AS 'Total'

  /*
  SUM(IF(id_tipo_comprobante IN (321,326) AND id_detalle_tipo_comprobante IN (51,56),0,comprobante.`subtotal_neto`*tipo_comprobante.`signo_comercial`/comprobante.valor_moneda2))
  */
FROM
  comprobante 
  JOIN tipo_comprobante ON comprobante.`id_tipo_comprobante` = tipo_comprobante.`id`
  JOIN persona ON persona.id = comprobante.`id_persona`
  JOIN provincia ON provincia.id = persona.`id_provincia`
WHERE 
 id_estado_comprobante IN (115,9)
  AND id_tipo_comprobante IN (
  801, /*FCA*/
  806, /*FCB*/
  811, /*FCC*/
  817, /*FACTE aca estan las de tierra del fuego*/ 
  833, /*FCM - por las dudas para ser generico de la empresa*/
  804, /*RBA -(recibo) VALMEC no tiene ninguno - no es el recibo de cobro (208)*/
  809, /*RBB*/
  815, /*RBC*/
  836, /*RBM*/
  803, /*NCA - todos los Creditos para ser generico de la empresa*/
  808, /*NCB*/
  813, /*NCC*/
  835, /*NCM*/
  818, /*NDOE todos los Debitos para ser generico de la empresa*/
  802, /*NDA*/
  807, /*NDB*/
  812, /*NDC*/
  834, /*NDM*/
  818, /*NDOE - NOTA DE DEBITO DE Exportacion - VAlmec no tiene ninguna pero para ser GENERICO*/
  819) /*NCOE - NOTAS DE CREDITO POR OPERACIONES CON EL EXTERIOR*/
  
  AND cae > 0 
  AND YEAR(fecha_contable) = 2019
  AND persona.`id_pais` =302 /*solo argentina con esto excluye las facturas de exportacion que no son de TIERRA DEL FUEGO*/
/* IFNULL - Esto lo tiene porque no reconoce bien el NULL*/
 /*AND  IFNULL(Comprobante.`id_detalle_tipo_comprobante`,0)!=5 
/*Detalle de las notas de debito(803) = Cheque rechazado*/

GROUP BY persona.`id_provincia`
