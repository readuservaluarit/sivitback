<?php



class MovimientosCarteraRendicionController extends AppController {
    
    
    public $name = EnumController::MovimientosCarteraRendicion;
    public $model = 'Comprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    
/**
* @secured(CONSULTA_CARTERA_RENDICION)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->loadModel("CarteraRendicion");

        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        

        $id_cartera_rendicion = strtolower($this->getFromRequestOrSession('CarteraRendicion.id_cartera_rendicion'));
        
        
        $fecha_inicio = strtolower($this->getFromRequestOrSession('CarteraRendicion.fecha_contable'));
        
        
        
        $fecha_fin = strtolower($this->getFromRequestOrSession('CarteraRendicion.fecha_contable_hasta'));
        
        $conditions = array(); 
        
        
        /*
        $id_cartera_rendicion  =2;
        $fecha_inicio = '2017-07-10';
        $fecha_fin = '2017-12-15';
        
    */
        
           
            array_push($conditions, array('Comprobante.id_cartera_rendicion'=>$id_cartera_rendicion));
            array_push($this->array_filter_names,array("Cartera Rend.:"=>$this->CarteraRendicion->getDescripcion($id_cartera_rendicion)));
            
            
            
            if($fecha_inicio!=""){
           	 array_push($conditions, array('Comprobante.fecha_contable >='=>$fecha_inicio));
            	array_push($this->array_filter_names,array("Fecha Contable Dsd:"=>$this->{$this->model}->formatDate($fecha_inicio)));
            }
            
            
            if($fecha_fin!=""){
	            array_push($conditions, array('Comprobante.fecha_contable <='=>$fecha_fin));
	            array_push($this->array_filter_names,array("Fecha Contable Hta:"=>$this->{$this->model}->formatDate($fecha_fin)));
            }
            
            
            array_push($conditions, array('Comprobante.id_estado_comprobante'=>array(EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::Cumplida)));
            $tc_gastos = $this->Comprobante->getTiposComprobanteController(EnumController::Gastos);
            $tc_gastos_op = $this->Comprobante->getTiposComprobanteController(EnumController::OrdenesPagoGasto);
            $tipos_comprobantes = array_merge($tc_gastos,$tc_gastos_op);
            array_push($conditions, array('Comprobante.id_tipo_comprobante'=>$tipos_comprobantes));
     
  
        /*ESTA ES LA QUERY QUE PASO A CAKE, trae solamente los asientos_cuenta_contable del par asociado. O sea trae los items de un asiento relacionado a una cuenta contable
        o sea los debe y los haber y dsp le sca el par asociado, quedando solamente el que no figura en la query original obtengo asi el PAR ASOCIADO*/    
        /*SELECT * FROM asiento_cuenta_contable
JOIN asiento_cuenta_contable ascc ON ascc.`id_asiento`=asiento_cuenta_contable.`id_asiento`
JOIN asiento ON asiento_cuenta_contable.`id_asiento`=asiento.`id`
WHERE ascc.id_cuenta_contable =38 AND asiento_cuenta_contable.`id_cuenta_contable`<>38 AND asiento.`fecha`>='2016-07-01' AND asiento.`fecha`<='2016-07-30'*/


         $comprobantes = $this->Comprobante->find('all', array(
                                               
                                                'conditions' => $conditions,
         										'contain' =>array('TipoComprobante','PuntoVenta')
                                                )); 
        
        
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de CarteraRendicions', 'Datos de las CarteraRendicions', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_CARTERA_RENDICION', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'CarteraRendicion.id', "10%");
                $formBuilder->addHeader('Nombre', 'CarteraRendicion.d_CARTERA_RENDICION', "50%");
                $formBuilder->addHeader('Nombre', 'CarteraRendicion.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_CARTERA_RENDICION');
                $formBuilder->addField($this->model, 'cotizacion');
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
     
        $page_count = 0;
        $total_debe = 0;
        $total_haber = 0;
        $saldo = 0;
        
        foreach($comprobantes as &$comprobante){
           
        	
        	
        	if(isset($comprobante['TipoComprobante'])){
        		$comprobante['Comprobante']['d_tipo_comprobante'] = $comprobante['TipoComprobante']['d_tipo_comprobante'];
        		$comprobante[$this->model]['codigo_tipo_comprobante'] =  $comprobante['TipoComprobante']['codigo_tipo_comprobante'];
        	}
        	
        	
        	if(isset($comprobante['PuntoVenta']) && $comprobante['PuntoVenta']['id']>0){
        		$comprobante[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($comprobante['PuntoVenta']['numero'],$comprobante[$this->model]['nro_comprobante']);
        		unset($comprobante['PuntoVenta']);
        	}else{
        		
        		$comprobante[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($comprobante['Comprobante']['d_punto_venta'],$comprobante[$this->model]['nro_comprobante']);
        	}
        	
            
        	if($comprobante["TipoComprobante"]["signo_contable"] == -1){
                
        		$comprobante["Comprobante"]["ingreso"] =  $comprobante["Comprobante"]["total_comprobante"];
        		$comprobante["Comprobante"]["egreso"] =  0;
                $total_debe = $total_debe +  $comprobante["Comprobante"]["total_comprobante"];
                
                
                
            }else{
            	$comprobante["Comprobante"]["egreso"] =  $comprobante["Comprobante"]["total_comprobante"];
            	$comprobante["Comprobante"]["ingreso"] = 0;
                $total_haber = $total_haber + $comprobante["Comprobante"]["total_comprobante"];
            }
            
            unset($comprobante["TipoComprobante"]);
           
            
         
             
           
        }
        
        
        $resultado["ResultadosComprobante"]["total_debe"] =(string) $total_debe;//total de los comprobantes
        $resultado["ResultadosComprobante"]["total_haber"] =(string) $total_haber;//total de los comprobantes
        $resultado["ResultadosComprobante"]["saldo"] =(string) ($total_debe - $total_haber);//total de los comprobantes  
        
       
        $this->data = $comprobantes;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
        		"content" => $comprobantes,
            "resultset_content"=>$resultado,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content","resultset_content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->CarteraRendicion->id = $id;
            $this->request->data = $this->CarteraRendicion->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('CarteraRendicion');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de CarteraRendicion');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de CarteraRendicion');
        else
            $formBuilder->setTituloForm('Consulta de CarteraRendicion');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_CARTERA_RENDICION', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('cotizacion', 'Cotizaci&oacute;n', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));    
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'CarteraRendicionAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('CarteraRendicionDCarteraRendicion', 'Debe ingresar un nombre');
        validator.validateRequired('CarteraRendicionCotizacion', 'Debe ingresar una cotizaci&oacute;n');


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_CARTERA_RENDICION)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_cat = '';
            
            try{
                if ($this->CarteraRendicion->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "La Cartera Rendicion ha sido creada exitosamente";
                    $tipo = EnumError::SUCCESS;
                    $id_cat = $this->CarteraRendicion->id;
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,la Cartera Rendicion no ha podido ser creada.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" =>$id_cat
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content" ,"id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }

    /**
    * @secured(MODIFICACION_CARTERA_RENDICION)
    */
     
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "La Cartera Rendicion ha sido modificada exitosamente";
                     $status = EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
               }
               
               if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, La Cartera Rendicion no ha podido modificarse.', 'error');
                
            }
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            //$this->{$this->model}->contain('Provincia','Pais');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }

    /**
    * @secured(BAJA_CARTERA_RENDICION,READONLY_PROTECTED)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->CarteraRendicion->id = $id;
     
       try{
            if ($this->CarteraRendicion->delete() ) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "La Cartera Rendicion ha sido eliminada exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
    }
    
    
   
	
	public function getModel($vista='default')
	{    
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    }
    
         
    
    private function editforView($model,$vista)
	{  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);    
    }
    
    /**
     * @secured(BTN_EXCEL_MOVIMIENTOSCARTERARENDICION)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Movimientos de Gastos");
    	
    	
    	
    }

}
?>