<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class ListaPrecio extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'lista_precio';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'd_lista_precio' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar un Nombre.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
            'unico' => array(
            'rule' => array('isUnique'),
            'message' => 'La Lista ya existe debe escribir una que no exista'
        )
		),
        
	);
	
	

	public $hasMany = array(
        'ListaPrecioProducto' => array(
            'className' => 'ListaPrecioProducto',
            'foreignKey' => 'id_lista_precio',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        
);




public $belongsTo = array(
        'ListaPrecioPadre' => array(
            'className' => 'ListaPrecio',
            'foreignKey' => 'id_lista_precio_padre',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'Moneda' => array(
            'className' => 'Moneda',
            'foreignKey' => 'id_moneda',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'id_usuario',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'TipoListaPrecio' => array(
            'className' => 'TipoListaPrecio',
            'foreignKey' => 'id_tipo_lista_precio',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
          'Persona' => array(
            'className' => 'Persona',
            'foreignKey' => 'id_persona',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
        )
	);


		public function setVirtualFieldsForView()
		{//configura los virtual fields  para este Model
			$this->virtual_fields_view = array(
			"d_tipo_lista_precio"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>1,"default"=>null,"length"=>null),
			"d_tipo_lista_precio_padre"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>1,"default"=>null,"length"=>null),
			"id_tipo_lista_precio_padre"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
			"d_lista_padre"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>1,"default"=>null,"length"=>null),
			"moneda_simbolo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
			);
		}
        
        
        public function getDefault(){

                $lp = $this->find('first', array(
                                                    'conditions' => array('ListaPrecio.por_defecto' => 1),
                                                    'contain' =>false
                                                    ));
                if($lp)                                    
                	return $lp["ListaPrecio"]["id"];
                else 
                	return 0;


        }
        
        
        public function getName($id_producto){
        	
        	$lp = $this->find('first', array(
        			'conditions' => array('ListaPrecio.id' => $id_producto),
        			'contain' =>false
        	));
        	if($lp)
        		return $lp["ListaPrecio"]["d_lista_precio"];
        		else
        			return 0;
        			
        			
        }
    


}
