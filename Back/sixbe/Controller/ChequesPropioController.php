<?php



App::uses('ChequesController', 'Controller');

class ChequesPropioController extends ChequesController {
    public $name = 'Cheques';
    public $model = 'Cheque';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
  
    /**
    * @secured(CONSULTA_CHEQUE)
    */
    public function index() {
        
      
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $id_cartera_cheque = $this->getFromRequestOrSession('Cheque.id_cartera_cheque');
        $id_tipo_cheque = $this->getFromRequestOrSession('Cheque.id_tipo_cheque');
        $nro_cheque = $this->getFromRequestOrSession('Cheque.nro_cheque');
        $id_banco = $this->getFromRequestOrSession('Cheque.id_banco');
        $id_estado_cheque = $this->getFromRequestOrSession('Cheque.id_estado_cheque');
      
        
       
       $conditions = array();   
       
            
        if($id_cartera_cheque!="")
            array_push($conditions, array('Cheque.id_cartera_cheque =' => $id_cartera_cheque)); 
            
               
        if($id_tipo_cheque!="")
            array_push($conditions, array('Cheque.id_tipo_cheque =' =>  $id_tipo_cheque )); 
             
        if($nro_cheque!="")
            array_push($conditions, array('Cheque.nro_chque' =>  $nro_cheque )); 
            
        if($id_banco!="")
            array_push($conditions, array('Cheque.id_banco' =>  $id_banco ));
        
        if($id_estado_cheque!="")
            array_push($conditions, array('Cheque.id_estado_cheque' =>  $id_estado_cheque )); 
        
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' => array( 'Banco','BancoSucursal','EstadoCheque','ComprobanteEntrada'=>array('TipoComprobante'),'ComprobanteSalida'=>array('TipoComprobante'),'Chequera' ),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
        
        
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        $comprobante = New Comprobante();
       
        foreach($data as &$valor){
            
            if(isset($valor['Banco']))
             $valor['Cheque']['d_banco'] = $valor['Banco']['d_banco'];
             
            if(isset($valor['BancoSucursal']))
                $valor['Cheque']['d_banco_sucursal'] = $valor['BancoSucursal']['d_banco_sucursal'];
            
            
            if(isset($valor['EstadoCheque']))
                $valor['Cheque']['d_estado_cheque'] = $valor['EstadoCheque']['d_estado_cheque'];
                
            if(isset($valor['ComprobanteEntrada']))
                 if(isset($valor['ComprobanteEntrada']['TipoComprobante']))
                    $codigo_tipo_comprobante_entrada = $valor['ComprobanteEntrada']['TipoComprobante']['codigo_tipo_comprobante'];
                else
                    $codigo_tipo_comprobante_entrada = '';
            
                $valor['Cheque']['d_comprobante_entrada'] = $codigo_tipo_comprobante_entrada.'-'.$comprobante->GetNumberComprobante(0,$valor['ComprobanteEntrada']['nro_comprobante']);
                
            if(isset($valor['ComprobanteSalida'])){
                if(isset($valor['ComprobanteSalida']['TipoComprobante']))
                    $codigo_tipo_comprobante_salida = $valor['ComprobanteSalida']['TipoComprobante']['codigo_tipo_comprobante'];
                else
                    $codigo_tipo_comprobante_salida = '';
                    
                $valor['Cheque']['d_comprobante_salida'] = $codigo_tipo_comprobante_salida.'-'.$comprobante->GetNumberComprobante(0,$valor['ComprobanteSalida']['nro_comprobante']);    
            }        
             unset($valor['Banco']);
             unset($valor['BancoSucursal']);
             unset($valor['EstadoCheque']);
             unset($valor['ComprobanteEntrada']);
             unset($valor['ComprobanteSalida']);
             
            
        }
        
      
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    

    /**
    * @secured(ADD_CHEQUE, READONLY_PROTECTED)
    */
    public function add() {
        
        $this->request->data["Cheque"]["id_tipo_cheque"] = EnumTipoCheque::Propio;
        parent::add(); 
        
      
    }
    
	/**
    * @secured(MODIFICACION_CHEQUE, READONLY_PROTECTED)
    */
    public function edit($id) {
        
       $this->request->data["Cheque"]["id_tipo_cheque"] = EnumTipoCheque::Propio;
       parent::edit($id);
        
    }
    
  
  
    



   /**
    * @secured(CONSULTA_CHEQUE)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
        
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        
            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);
        
               
        
        
      
    } 

    public function home(){
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
    
    public function transferenciaChequeABanco($array_id_cheques){
        
        
        
        
    }
    
   
    
    
       
}
?>