<?php
App::uses('AppModel', 'Model');

class Melibre extends AppModel {
	
	
	public $useTable = false; 
	
	public function actualizarPrecios($id_lista_precio){
		
		App::import('Vendor', 'meli', array('file' => 'Meli/meli.php'));
		
		
		$lista_precio_producto_obj = ClassRegistry::init(EnumModel::ListaPrecioProducto);
		
		$lista_precio_producto  = $lista_precio_producto_obj->find('all', array(
				'conditions' => array(
						'ListaPrecioProducto.id_lista_precio' => $id_lista_precio
				) ,
				'contain' => array("Producto")
		));
		
		$meli_data = $this->setDatosUsuario();
		
		
		foreach ($lista_precio_producto as $producto){
			
			
			if(strlen($producto["Producto"]["meli_id"])>0){//si tiene id entonces es un producto de MercadoLibre. Este control es para eviar que en la lista de ML se pongan productos que NO son de ML
				
				
				$meli = new Meli(Configure::read('MeliConstants.appId'), Configure::read('MeliConstants.secretKey'));
				
				
				
				
				
				$url =  $meli->make_path('/items/'.$producto["Producto"]["meli_id"],array('access_token'=>$meli_data["access_token"]));
				
				
				$data = $this->setPutDataToMeli($url,array("price"=>round($producto["ListaPrecioProducto"]["precio"],0))); 
				
				$data;
				
				
				
			}
			
			
		}
		
		
		
		
	}
	
	
	public function setPutDataToMeli($meli,$url,$data){
		
		
		$meli->put($url,$data); //no se le deben pasar decimales
		
		
	}
	
	
	public function setPostDataToMeli($meli,$url,$body,$data){
		
		
		$meli->post($url,$body,$data); //no se le deben pasar decimales
		
		
	}
	
	
	public function getDataFromMeli($url){
		
		
		$ch = curl_init();
		// Establece la URL y otras opciones apropiadas
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		//curl_setopt($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_POSTFIELDS,implode("&",$this->RecuperoFiltros()));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		
		
		
		// Cierrar el recurso cURLy libera recursos del sistema
		$server =  curl_exec($ch);
		return json_decode($server, true);
		
		
	}
	
	
	public function getMoneySiv($currency_id_meli){
		
		
		switch ($currency_id_meli){
			
			
			case "ARS":
				return EnumMoneda::Peso;
				break;
				
			CASE "DOL":
				return EnumMoneda::Dolar;
				break;
		}
		
		
	}
	
	
	public function SaveMeliCategory($meli,$id_meli_category){
		
		///categories/{Category_id}
	
	
		
		
		$categoria_obj = ClassRegistry::init(EnumModel::Categoria);
		
		$categoria =   $categoria_obj->find('first', array(
				'conditions' => array(
						'Categoria.id_meli' => $id_meli_category
				) ,
				'contain' => false
		));
		
		
		
		if(!$categoria){
			$url =  $meli->make_path('categories/'.$id_meli_category);
			$categoria_ml = $this->getDataFromMeli($url);
			
			
			$categoria["Categoria"]["d_categoria"] = $categoria_ml["name"];
			$categoria["Categoria"]["id_meli"] = $id_meli_category;
			
			
			
			$categoria_obj->saveAll($categoria);
			return $categoria_obj->id;
			
			
		}else{
			
			return $categoria["Categoria"]["id"];
		}
		
		
	}
	
	
	
	
	public function saveUser($buyer_user){//obtengo el comprador de Mercadolibre
		
		//$this->loadModel(EnumModel::Persona);
		
		
		$persona_obj = ClassRegistry::init(EnumModel::Persona);
		
		
		$id_meli_user = $buyer_user["id"];
		$nickname = $buyer_user["nickname"];
		
		$cliente_meli  = $persona_obj->find('first', array(
				'conditions' => array("OR"=>array(
						'Persona.meli_id_usuario' => $id_meli_user,'Persona.meli_nickname'=>$nickname)
				) ,
				'contain' => false
		));
		
		
		if(count($cliente_meli)>0){
			
			
			return $cliente_meli["Persona"]["id"];
		}else{//doy de alta esa persona con los datos de MELI
			
			
			$id_persona = $persona_obj->savePersonFromMeli($buyer_user);
			
			return $id_persona;
			
			
			
		}
		
		
		
		
		
		
	}
	
	
	public function saveClienteSecundario($id_cliente,$buyer_user){
		
		
		$persona_obj = ClassRegistry::init(EnumModel::Persona);
		
		$cliente_meli  = $persona_obj->find('first', array(
				'conditions' => array("OR"=>array(
						'Persona.id' => $id_cliente)
				) ,
				'contain' => false
		));
		
		
		if($cliente_meli){
			
			
			if($cliente_meli["Persona"]["cuit"] != $buyer_user["billing_info"]["doc_number"] && $buyer_user["billing_info"]["doc_number"]>0 && !is_null($buyer_user["billing_info"]["doc_number"])){
				
				/*Me esta mandando un cliente diferente para facturar entonces lo guardo*/
				
				
				
				$cliente_meli_secundario  = $persona_obj->find('first', array(
						'conditions' => array("OR"=>array(
								'Persona.cuit' => $buyer_user["billing_info"]["doc_number"])
						) ,
						'contain' => false
				));
				
				
				if($cliente_meli_secundario){
					
					return  $cliente_meli_secundario["Persona"]["id"];
				}else{
					
					
					
					return $persona_obj->savePersonFromBillingInfoMeli($buyer_user);
				}
				
				
				
			}else{
				
				return null;
			}
			
			
			
		}else{
			
			return null;
		}
		
		
		
	}
	
	
	
	
	function setDatosUsuario($access_token,$expires_in,$refresh_token){
		
		
		
		
		//App::import('Vendor', 'configApp', array('file' => 'Meli/configApp.php'));
		
		Configure::load('meliconfig');
		//$this->loadModel(EnumModel::DatoEmpresa);
		$dato_empresa_obj = ClassRegistry::init(EnumModel::DatoEmpresa);
	
		App::import('Vendor', 'meli', array('file' => 'Meli/meli.php'));
		$actualizo = 0;
		
		
		
		
		
		
		
		$dato_empresa = $dato_empresa_obj->find('first', array(
				'conditions' => array(
						'DatoEmpresa.id' => 1
				) ,
				'contain' => false
		));
		
		
		
		
		
		
		if(strlen($dato_empresa["DatoEmpresa"]["mercadolibre_access_token"]) == 0 && !strlen($dato_empresa["DatoEmpresa"]["mercadolibre_refresh_token"])>0){//es la primera vez no tengo access token en la BD
			
			/*
			$access_token   = $this->getFromRequestOrSession('Meli.access_token');
			$expires_in    = $this->getFromRequestOrSession('Meli.expires_in');
			$refresh_token = $this->getFromRequestOrSession('Meli.refresh_token');
			
			*/
			
			
			
			
			$meli = new Meli(Configure::read('MeliConstants.appId'), Configure::read('MeliConstants.secretKey'));
			
			
			
			$url =  $meli->make_path('/users/me',array('access_token'=>$access_token));
			
			
			$usuario = $this->getDataFromMeli($url);
			
			
			
			$id_usuario = $usuario["id"];//id usuario
			$nick_usuario = $usuario["nickname"];//id usuario
			
			$actualizo = 1;
			
			
			
			
			
		}else{//ya tengo los datos para acceder a Meli pero no se si son viejos instacio y chequeo
			
			$meli = new Meli(Configure::read('MeliConstants.appId'), Configure::read('MeliConstants.secretKey'),$dato_empresa["DatoEmpresa"]["mercadolibre_access_token"],$dato_empresa["DatoEmpresa"]["mercadolibre_refresh_token"]);
			
			$expires_in = $dato_empresa["DatoEmpresa"]["mercadolibre_access_token_expire_seconds"];
			
			$id_usuario = $dato_empresa["DatoEmpresa"]["mercadolibre_user_id"];
			$access_token= $dato_empresa["DatoEmpresa"]["mercadolibre_access_token"];
			
			
			
			
			
			
			if($expires_in < time()) {
				
				$refresh = $meli->refreshAccessToken();
				
				
				
				
				
				// Now we create the sessions with the new parameters
				/*
				if($refresh["httpCode"] == 200){
					
					$access_token = $refresh['body']->access_token;
					$expires_in  = time() + $refresh['body']->expires_in;
					$refresh_token = $refresh['body']->refresh_token;
					$actualizo = 1;
					
				}else{
					
					
					$dato_empresa_obj->updateAll(
							array('DatoEmpresa.mercadolibre_access_token' => null
							),
							array('DatoEmpresa.id' => 1) );
					
					$this->setDatosUsuario();
					
					
				}
				
				*/
				
				
			}
			
		}
		
		
		if(strlen($access_token) > 0 && $actualizo == 1){
			
			$dato_empresa_obj->updateAll(
					array('DatoEmpresa.mercadolibre_access_token' => "'".$access_token."'",'DatoEmpresa.mercadolibre_access_token_expire_seconds' => $expires_in,'DatoEmpresa.mercadolibre_refresh_token' =>"'". $refresh_token."'",
							'DatoEmpresa.mercadolibre_user_id' => $id_usuario,'DatoEmpresa.mercadolibre_username' => "'".$nick_usuario."'"
					),
					array('DatoEmpresa.id' => 1) );
			
			echo "actualizado el usuario";
			
			
			
			
			
		}else{
			
			$data = array();//hay un error muy raro
		}
		
		if(strlen($access_token)>0){
			
			$data["access_token"] = $access_token;
			$data["id_usuario"] = $id_usuario;
		}
		
		return $data;
		
	}
	
	
	public function EnviarMensajeComprador($comprobante){
		
		
		App::import('Vendor', 'meli', array('file' => 'Meli/meli.php'));
		$meli_data = $this->setDatosUsuario();
		
		
		$meli = new Meli(Configure::read('MeliConstants.appId'), Configure::read('MeliConstants.secretKey'));
		
		
		$dato_empresa_obj = ClassRegistry::init(EnumModel::DatoEmpresa);
		
		
		
		$datoEmpresa =   $dato_empresa_obj->find('first', array(
				'conditions' => array(
						'DatoEmpresa.id' => 1
				) ,
				'contain' => false
		));
		
		
		
		
		
		if($datoEmpresa["DatoEmpresa"]["mercadolibre_enviar_mensaje_post_venta"] == 1){
		
			$url =  $meli->make_path('/messages/',array('access_token'=>$meli_data["access_token"],'application_id'=>Configure::read('MeliConstants.appId')));
			
			
			/*
			 * "from": {
	        "user_id": 236900XXX
	    },
	    "to": [
	        {
	            "user_id": 237052XXX,
	            "resource": "orders",
	            "resource_id": 1236236XXX,
	            "site_id": "MLA"
	        }
	    ],
	    "subject": "Prueba",
	    "text": {
	    
	        "plain": "Prueba Mensajeria"
	    }
			 * 
			 * */
			
			
			$body = array();
			
			$user["from"]["user_id"];
			
			array_push($body, $user);
			
			$to["user_id"] =  $datoEmpresa["DatoEmpresa"]["mercadolibre_user_id"];
			$to["resource"] ="orders";
			$to["resource_id"] = $comprobante["Comprobante"]["meli_order_id"];
			$to["site_id"] = Configure::read('MeliConstants.siteId');
			
			
			array_push($body, $to);
			
			
			$asunto["subjetct"] = "Prueba";
			
			array_push($body, $asunto);
			$mensaje["text"]["plain"] = $datoEmpresa["DatoEmpresa"]["mercadolibre_mensaje_post_venta"];
			
			array_push($body, $mensaje);
			
			
			$data = $this->setPostDataToMeli($meli,$url,$body); 
		}
		
		
		
	}
	
	
	public function SaveMeliAttributes($attributes){
		
		foreach($attributes as $attribute){
			
			switch($attribute["id"]){
				
				case "BRAND":
					return $this->SaveMeliBrand($attribute);
					break;
				
			}
			
			
			
		}
		
		
				
		
	}
	
	
	
	public function SaveMeliBrand($attribute){
		
		
		$marca_obj = ClassRegistry::init(EnumModel::Marca);
		
		
		
		$marca =   $marca_obj->find('first', array(
				'conditions' => array(
						'Marca.d_marca' => $attribute["value_name"]
				) ,
				'contain' => false
		));
		
		
		
		if(!$marca){
			
			
			try{
			$marca["Marca"]["d_marca"] = $attribute["value_name"];
			$marca_obj->saveAll($marca);
			return $marca_obj->id;
			}catch (Exception $e){
				
				
				return null;
			}
			
		}else{
			
			return $marca["Marca"]["id"];
		}
		
		
	}
	
	
	public function  setEntregaDomicilio(&$cabecera_comprobante,$pedido_venta){
		
		
		
		if( isset($pedido_venta["shipping"]["shipping_mode"]) ){
		switch($pedido_venta["shipping"]["shipping_mode"]){
			
			case "not_specified":
			case "":
				break;
				
			case "me1":
				break;
			case "me2":
				break;
			case "custom":
				break;
			
			
		}
		}
		
		
		
		if(isset($pedido_venta["shipping"]["status"])){
			switch($pedido_venta["shipping"]["status"]){
				
				case "to_be_agreed":
					$cabecera_comprobante["entrega_domicilio"] == EnumEntregaPedido::RetiraEnEmpresa;
					
					break;
				case "ready_to_ship":
					$cabecera_comprobante["entrega_domicilio"] == EnumEntregaPedido::DomicilioParticular;
					break;
			
			}
		}
		
		
	}
	
	
	



}
