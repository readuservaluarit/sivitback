<?php


App::uses('DatabaseSession', 'Model/Datasource/Session');



class ComboSession extends DatabaseSession implements CakeSessionHandlerInterface {
    public $cacheKey;
    public $name_class;

    public function __construct() {
        $this->cacheKey = Configure::read('Session.handler.cache');
       if(!is_null($this->_model))
        $this->name_class = get_class($this->_model);
        parent::__construct();
    }

    // read data from the session.
    public function read($id) {
        $result = Cache::read($id, $this->cacheKey);
        

        if ($result) {
            return $result;
        }
        return parent::read($id);
    }

    // write data into the session.
    public function write($id, $data) {
    	
    	try{
		    	Cache::write($id, $data, $this->cacheKey);
		    	$response = parent::write($id, $data);
		    	
		    	
		    	if(isset($_REQUEST["data"]["Usuario"]["password"]) && is_array($data)  && count($data)>0){
					   
					      
					     
					      $this->_model->updateAll(
					      		array($this->name_class.'principal' =>1),
					       		array($this->_model->primaryKey => $id) );
					      
					     
				    
				    }
				    
				   
				    return  $response;
    
    
    	}catch (Exception $e){
    		
    		
    		
    	}
    
    
    }

    // destroy a session.
    public function destroy($id,$force=false) {
    	
    	try{
	    	if($this->es_principal($id) !=1){
		    	$this->old_session = $id;
		        Cache::delete($id, $this->cacheKey);
		        return parent::destroy($id);
		
	    	}else{
	    		
	    		if($force){
	    			
	    			$this->old_session = $id;
	    			Cache::delete($id, $this->cacheKey);
	    			return parent::destroy($id);
	    		
	    		
	    		}else{
	    			
	    			
	    			
	    			$expires = time() + $this->_timeout;
	    			$this->_model->updateAll(
	    					array($this->name_class.'expires' =>$expires),
	    					array($this->_model->primaryKey => $id) );
	    		}
	    	}
    	}catch (Exception $e){
    		
    		
    		
    	}
        
    }
    
    
    
	public function es_principal($id){
		
		$row = $this->_model->find('first', array(
				'conditions' => array($this->_model->primaryKey => $id)
		));
		
		if($row)
			return $row[$this->_model->alias]['principal'];
		else 
			return 0;
		
		
	}
    
    
    // removes expired sessions.
    public function gc($expires =null ){
        Cache::gc($this->cacheKey);
        return parent::gc($expires);
    }
}

?>