<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class SubTipoImpuesto extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'sub_tipo_impuesto';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Your custom message here',
                'allowEmpty' => false,
                'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),

	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 
  public $belongsTo = array(
        'TipoImpuesto' => array(
            'className' => 'TipoImpuesto',
            'foreignKey' => 'id_tipo_impuesto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        );
 
 public function getDescripcion($id){
    
    $tipo_impuesto = $this->find('first', array(
                                                'conditions' => array('SubTipoImpuesto.id' => $id),
                                                'contain' =>false
                                                )); 
        
    
   return $tipo_impuesto["SubTipoImpuesto"]["d_sub_tipo_impuesto"]; 
}

}
