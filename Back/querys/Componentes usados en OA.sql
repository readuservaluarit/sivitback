SELECT producto.id,producto.codigo,producto.d_producto,

SUM(
IFNULL(

(SELECT SUM(comprobante_item.`cantidad`) FROM comprobante_item 
JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante` 

WHERE
id_tipo_comprobante=104 AND YEAR(comprobante.fecha_generacion) IN (2017,2018)
AND comprobante_item.`id_producto` = valvula.`id`
),0) )AS cant

 FROM producto 

JOIN producto_relacion ON producto_relacion.`id_producto_hijo` = producto.`id`

JOIN producto valvula ON valvula.id = producto_relacion.`id_producto_padre`

WHERE producto.d_producto LIKE '%esfera%' AND producto.codigo LIKE 'EM%'

GROUP BY producto.`codigo`
LIMIT 10000000000

