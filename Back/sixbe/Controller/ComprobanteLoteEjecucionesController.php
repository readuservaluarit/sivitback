<?php



class ComprobanteLoteEjecucionesController extends AppController {
    
    public $name = EnumController::ComprobanteLoteEjecuciones;
    public $model = 'ComprobanteLoteEjecucion';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
 

    protected  function RecuperoFiltros($model)
    {
            //Recuperacion de Filtros
            $d_comprobante_lote = $this->getFromRequestOrSession('ComprobanteLoteEjecucion.d_comprobante_lote');
            $id_comprobante_lote = $this->getFromRequestOrSession('ComprobanteLoteEjecucion.id_comprobante_lote');
           
            $paginado = $this->getFromRequestOrSession('ComprobanteLoteEjecucion.paginado');
            
             if($paginado!="")
                $this->paginado = $paginado;
          
            $conditions = array(); 
            
             if($d_comprobante_lote!="")
                array_push($conditions, array('ComprobanteLote.d_comprobante_lote LIKE' => '%'.$id_comprobante.'%' ));
                
             if($id_comprobante_lote!="")
                array_push($conditions, array('ComprobanteLoteEjecucion.id_comprobante_lote LIKE' => $id_comprobante_lote ));   
                
             
            return $conditions;
    }    

   /**
    * @secured(CONSULTA_COMPROBANTE_LOTE)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
    
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model); 
    
        $this->paginate = array(
            'paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain'=> array('ComprobanteLote'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
    
        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        //Headers
        $formBuilder->addHeader('id', 'cotizacion.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'cotizacion.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
          
    else{ // vista json
    
        
        
        
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        
        
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    //fin vista json
    }
    
    
    
     
    
    
    
     
    
  
    
    

   
    
    
      /**
    * @secured(CONSULTA_COMPROBANTE_LOTE)
    */
    public function getModel($vista = 'default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        
       
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
        
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $this->set('model',$model);
       // $this->set('this',$this);
        Configure::write('debug',0);
        $this->render($vista);
    
        
    }
    
    
    
    
    
   
    
}
?>