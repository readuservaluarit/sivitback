<!doctype html>
<html lang="es">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
	<title>Reporte SIVIT</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" >
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    
  
    

       <?php echo $this->Html->css('jqueryui/jquery-ui-1.9.2.custom.css');?>  


      
       <?php echo $this->Html->css('font-awesome/css/font-awesome.min.css');?>
       
       
       
        
    
      <?php echo $this->Html->script('jquery/jquery-1.8.3.min.js');?>
      <?php echo $this->Html->script('jquery/jquery.js');?>
      <?php echo $this->Html->script('jquery/jquery-ui-1.9.2.custom.js');?>
      
      
        <?php
            echo $this->Html->script('fancybox/jquery.fancybox.js?v=2.1.3');
            echo $this->Html->css('fancybox/jquery.fancybox.css?v=2.1.2');

            echo $this->Html->css('fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5');
            echo $this->Html->script('fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5');

            echo $this->Html->css('fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7');
            echo $this->Html->script('fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7');

            echo $this->Html->script('fancybox/helpers/jquery.fancybox-media.js?v=1.0.5');
    
        
        ?>
        
      
       <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    
    
    <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet">
    <script  src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script  src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script  src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    
    <?php
    
      /*Validator*/
            echo $this->Html->script('validator.js');
            
            /*Solo Numeros*/
            echo $this->Html->script('mylibs/numericInput.min.js');
            
    ?>



    
 <?php echo $this->Html->css('dashboard/style.blue.css');?>
 <?php echo $this->Html->css('dashboard/custom.css');?>
 <?php echo $this->Html->css('rightsidebar/styles.css');?>
  <?php echo $this->Html->script('Chart.js/dist/Chart.min.js');?>
 <?php
 
    /*jQuery datepicker espaniol*/
            echo $this->Html->script('mylibs/jquery.ui.datepicker-es.js'); //jQuery datepicker espaniol
            echo $this->Html->script('mylibs/jquery-ui-timepicker-addon.js'); //jQuery timepicker
            
            ?>
 
    	
    	
  <?php //Inicio Scripts para menu multinivel ?>
        
        <script>
 
            /* bootstrap-dropdown.js v2.0.2
 
             * http://twitter.github.com/bootstrap/javascript.html#dropdowns
             * ============================================================
             * Copyright 2012 Twitter, Inc.
             *
             * Licensed under the Apache License, Version 2.0 (the "License");
             * you may not use this file except in compliance with the License.
             * You may obtain a copy of the License at
             *
             * http://www.apache.org/licenses/LICENSE-2.0
             *
             * Unless required by applicable law or agreed to in writing, software
             * distributed under the License is distributed on an "AS IS" BASIS,
             * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
             * See the License for the specific language governing permissions and
             * limitations under the License.
           */
     !function( $ ){
       "use strict"
      /* DROPDOWN CLASS DEFINITION
             * ========================= */
            var toggle = '[data-toggle="dropdown"]'
            , Dropdown = function ( element ) {
                var $el = $(element).on('click.dropdown.data-api', this.toggle)
                $('html').on('click.dropdown.data-api', function () {
                    $el.parent().removeClass('open')
                })
            }
            Dropdown.prototype = {
                constructor: Dropdown
                , toggle: function ( e ) {
                    var $this = $(this)
                    , selector = $this.attr('data-target')
                    , $parent
                    , isActive
 
                    if (!selector) {
                        selector = $this.attr('href')
                        selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
                    }
 
                    $parent = $(selector)
                    $parent.length || ($parent = $this.parent())
                    isActive = $parent.hasClass('open')
                    clearMenus()
                        !isActive && $parent.toggleClass('open')
                    return false
                }
            }
 
            function clearMenus() {
                $(toggle).parent().removeClass('open')
            }
            /* DROPDOWN PLUGIN DEFINITION
             * ========================== */
            $.fn.dropdown = function ( option ) {
                return this.each(function () {
                    var $this = $(this)
                    , data = $this.data('dropdown')
                    if (!data) $this.data('dropdown', (data = new Dropdown(this)))
                    if (typeof option == 'string') data[option].call($this)
                })
            }
            $.fn.dropdown.Constructor = Dropdown
            /* APPLY TO STANDARD DROPDOWN ELEMENTS
             * =================================== */
            $(function () {
                $('html').on('click.dropdown.data-api', clearMenus)
                $('body').on('click.dropdown.data-api', toggle, Dropdown.prototype.toggle)
            })
        }( window.jQuery );
        
        
        //Limpia un formulario
        function clearInputs(idForm){
                    $(':input','#'+idForm)
                      .not(":button, :submit, :reset, [name='_method']")
                      .val('')
                      .removeAttr('checked')
                      .removeAttr('selected');

        }
        
        //Muestra / oculta pestañas
        
        function showTab(idTab){
            $('#'+idTab).show();
            $('#'+idTab+'-link').show();
        }
        
        function hideTab(idTab){
            $('#'+idTab).hide();
            $('#'+idTab+'-link').hide();
        }
        
        function changeTitleForm(tituloModel, valor){
            
            $('#'+tituloModel+'-title-link').html(valor);
        }
        
        function hideLabel(idLabel){
            
            $('label[for="'+idLabel+'"]').hide();
            
        }
        
        function showLabel(idLabel){
            
            $('label[for="'+idLabel+'"]').show();
            
        }
        
        //Devuelve length para arrays asociativos
        function getLength(arr){
            
            var count = 0;
            for (var indice in arr)
                count++;
            
            return count;
        }
        
        </script>
         
        <script type='text/javascript'>//<![CDATA[
        $(window).load(function(){
            jQuery('.submenu').hover(function () {
                jQuery(this).children('ul').removeClass('submenu-hide').addClass('submenu-show');
            }, function () {
                jQuery(this).children('ul').removeClass('.submenu-show').addClass('submenu-hide');
            }).find("a:first").append(" &raquo; ");
        });//]]>
        </script>
         
        <style>
            .submenu-show
            {
                border-radius: 3px;
                display: block;
                left: 100%;
                margin-top: -25px !important;
                moz-border-radius: 3px;
                position: absolute;
                webkit-border-radius: 3px;
            }
            .submenu-hide
            {
                display: none !important;
                float: right;
                position: relative;
                top: auto;
            }
            .navbar .submenu-show:before
            {
                border-bottom: 7px solid transparent;
                border-left: none;
                border-right: 7px solid rgba(0, 0, 0, 0.2);
                border-top: 7px solid transparent;
                left: -7px;
                top: 10px;
            }
 
            .navbar .submenu-show:after
            {
                border-bottom: 6px solid transparent;
                border-left: none;
                border-right: 6px solid #fff;
                border-top: 6px solid transparent;
                left: 10px;
                left: -6px;
                top: 11px;
            }
        </style>
        
        <?php //Fin Scripts para menu multinivel ?>
        
</head>
<body>
  <?php
        
        
        $printear_menu = true;
        
        if(isset($print_menu))
        	$printear_menu = $print_menu;

            App::import('Lib', 'Menu');
            $menu = new Menu($this);
            
            if ($this->Session->read('Auth.User') && $printear_menu)
                $menu->printMenu();

        ?>


     <div id="main-content">
<div class="page-content d-flex align-items-stretch"> 
       
        <div class="content-inner">
        
          <!-- Page Header-->
         <div class="alert alert-success" role="alert" id="alertSuccess" style="display:none;">
  			Se ha grabado correctamente
		</div>
		
		<div class="alert alert-danger" role="alert" id="alertError" style="display:none;">
  Ha ocurrido un error, no pudo grabarse
		</div>
      

     <?php echo $this->Session->flash(); ?>
     

     <?php echo $this->fetch('content'); ?>
     
          
        </div>
      </div>
      
      </div>
</body>

<script>
$('#toggle-btn').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');

        $('.side-navbar').toggleClass('shrinked');
        $('.content-inner').toggleClass('active');
        $(document).trigger('sidebarChanged');

        if ($(window).outerWidth() > 1183) {
            if ($('#toggle-btn').hasClass('active')) {
                $('.navbar-header .brand-small').hide();
                $('.navbar-header .brand-big').show();
            } else {
                $('.navbar-header .brand-small').show();
                $('.navbar-header .brand-big').hide();
            }
        }

        if ($(window).outerWidth() < 1183) {
            $('.navbar-header .brand-small').show();
        }
    });
    
     function showProcessing(show){

                if(show)
                    $("#zk_proc").css("display","block"); 
                else
                    $("#zk_proc").css("display","none");

            }
            
            function showSuccess(msg){
                $('#alertSuccess').html(msg);
                $('#alertSuccess').fadeIn(1000, function() {
                // Animation complete
                     $('#alertSuccess').fadeOut(8000, function() {
                    // Animation complete
                    });
                });
            }
            
            function showError(msg){
                $('#alertError').html(msg);
                $('#alertError').fadeIn(1000, function() {
                // Animation complete
                     $('#alertError').fadeOut(8000, function() {
                    // Animation complete
                    });
                });
            }
</script>





</html>



