<?php

class EstadosComprobanteController extends AppController {
    public $name = 'EstadosComprobante';
    public $model = 'EstadoComprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        //Recuperacion de Filtros
        $nombre = strtolower($this->getFromRequestOrSession('EstadoComprobante.d_estado_comprobante'));
		$activo = strtolower($this->getfromrequestorsession('EstadoComprobante.activo'));
        
        $conditions = array(); 
		
		if($activo!="")
			 array_push($conditions, array('EstadoComprobante.activo' => $activo));
			
        if ($nombre != "") {
			array_push($conditions, array('LOWER(EstadoComprobante.d_estado_comprobante) LIKE' => '%' . $nombre . '%'));
        }

        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

			App::import('Lib', 'FormBuilder');
			$formBuilder = new FormBuilder();
			$formBuilder->setDataListado($this, 'Listado de EstadosComprobante', 'Datos de los EstadosComprobante', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
			
			//Filters
			$formBuilder->addFilterBeginRow();
			$formBuilder->addFilterInput('d_ESTADO_COMPROBANTE', 'Nombre de EstadoComprobante', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
			$formBuilder->addFilterEndRow();
			
			//Headers
			$formBuilder->addHeader('Id', 'EstadoComprobante.id', "10%");
			$formBuilder->addHeader('Nombre', 'EstadoComprobante.d_moneda', "50%");
		 

			//Fields
			$formBuilder->addField($this->model, 'id');
			$formBuilder->addField($this->model, 'd_ESTADO_COMPROBANTE');
	
			
			$this->set('abm',$formBuilder);
			$this->render('/FormBuilder/index');
        }else{ // vista json
			$this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
			$page_count = $this->params['paging'][$this->model]['pageCount'];
			
			
			
			$this->data = $data;
			$output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "list",
				"content" => $data,
				"page_count" =>$page_count
			);
			$this->set($output);
			$this->set("_serialize", array("status", "message","page_count", "content"));
		}
    }

    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->EstadoComprobante->id = $id;
            $this->request->data = $this->EstadoComprobante->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('EstadoComprobante');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de EstadoComprobante');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de EstadoComprobante');
        else
            $formBuilder->setTituloForm('Consulta de EstadoComprobante');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        

        $formBuilder->addFormInput('d_ESTADO_COMPROBANTE', 'Nombre de EstadoComprobante', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
     
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'EstadoAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('EstadoDEstado', 'Debe ingresar un nombre');
        
        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }
        return true;
        } 
        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }

  
	/**
	* @secured(CONSULTA_ESTADO_COMPROBANTE)
	*/
	public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }
		
		
        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);

        }



}
?>