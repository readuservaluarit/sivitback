data[Comprobante][id]=57395
data[Comprobante][id_unidad]=1
data[Comprobante][id_moneda_item]=2
data[Comprobante][id_iva]=1
data[Comprobante][id_lista_precio]=
data[Comprobante][importe_declarado]=1
data[Comprobante][id_unidad_valor]=
data[Comprobante][valor_unidad_total]=0
data[Comprobante][cantidad_bultos]=0
data[Comprobante][t_direccion]= 
data[Comprobante][t_razon_social]=
data[Comprobante][sumatoria_items]=491.80
data[Comprobante][id_transportista]=
data[Comprobante][observacion]=Importado de  - 00000000
data[Comprobante][d_deposito_destino]=Almacen
data[Comprobante][id_deposito_destino]=4
data[Comprobante][d_deposito_origen]=
data[Comprobante][id_deposito_origen]=
data[Comprobante][definitivo]=1
data[Comprobante][id_estado_comprobante]=1
data[Comprobante][id_moneda]=2
data[Comprobante][valor_moneda]=1
data[Comprobante][plazo_entrega]=
data[Comprobante][id_tipo_comprobante]=101
data[Comprobante][orden_compra_externa]=
data[Comprobante][nro_comprobante]=00012998
data[Comprobante][fecha_generacion]=2018-06-15
data[Comprobante][id_punto_venta]=3
data[Comprobante][d_sucursal]=
data[Comprobante][id_sucursal]=
data[Comprobante][codigo_persona]=2254
data[Comprobante][revertir_items_origen]=0
data[Comprobante][certificados]=0
data[Comprobante][inspeccion]=0
data[Comprobante][fecha_entrega]=2018-06-15
data[Comprobante][plazo_entrega_dias]=0
data[Comprobante][razon_social]=Materiales Technology Argentina SRL
data[Comprobante][id_persona]=20431
data[Comprobante][lugar_entrega]=
data[Comprobante][genera_movimiento_stock]=1
data[Comprobante][chkOpenFolder]=1
data[Comprobante][chkCargaMasiva]=0
data[ComprobanteItem][0][id_comprobante]=57395
data[ComprobanteItem][0][id]=149975
data[ComprobanteItem][0][cantidad]=2.00
data[ComprobanteItem][0][descuento_unitario]=0.00
data[ComprobanteItem][0][precio_unitario_bruto]=169.00
data[ComprobanteItem][0][precio_unitario]=169.00
data[ComprobanteItem][0][orden]=1
data[ComprobanteItem][0][n_item]=3
data[ComprobanteItem][0][id_iva]=1
data[ComprobanteItem][0][id_producto]=16101
data[ComprobanteItem][0][item_observacion]=REPARACION DE *** BASES MAGNETICAS **
data[ComprobanteItem][0][dias]=15
data[ComprobanteItem][0][id_comprobante_item_origen]=149972
data[ComprobanteItem][0][activo]=1
data[ComprobanteItem][0][cantidad_cierre]=2.00
data[ComprobanteItem][0][id_unidad]=1
data[ComprobanteItem][0][precio_minimo_producto]=2.00
data[ComprobanteItem][0][requiere_conformidad]=0
data[ComprobanteItem][0][cantidad_ceros_decimal]=2
data[ComprobanteItem][0][ajusta_diferencia_cambio]=0
data[ComprobanteItem][0][fecha_entrega]=2018-06-30 00:00
data[ComprobanteItem][0][fecha_generacion]=2018-05-08 00:00
data[ComprobanteItem][0][d_producto]=Herremientas para fabrica "Insertos, Fresas, Mechas, Machos,etc." concepto general.
data[ComprobanteItem][0][codigo]=HER-FAB-001
data[ComprobanteItem][0][total_remito_origen]=2.00
data[ComprobanteItem][0][total_pendiente_irm]=0.00
data[ComprobanteItem][0][Lotes]=[
  {
    "id": "14",
    "id_comprobante_item": "149975",
    "id_lote": "1700",
    "orden": "2",
    "Lote": {
      "id": "1700",
      "fecha_emision": "2017-05-11 07:27:14",
      "utilizado": null,
      "d_lote": "CUERPO 6\" PT S-150 WCB",
      "id_tipo_lote": "2",
      "codigo": "EB5307"
    },
    "codigo": "EB5307",
    "id_tipo_lote": "2",
    "fecha_emision": "2017-05-11 07:27:14",
    "d_tipo_lote": "Proveedor"
  },
  {
    "id": "15",
    "id_comprobante_item": "149975",
    "id_lote": "19",
    "orden": null,
    "Lote": {
      "id": "19",
      "fecha_emision": "2015-03-18 15:55:32",
      "utilizado": null,
      "d_lote": "barra red. 82.57 mm 304",
      "id_tipo_lote": "1",
      "codigo": "v150331"
    },
    "codigo": "v150331",
    "id_tipo_lote": "1",
    "fecha_emision": "2015-03-18 15:55:32",
    "d_tipo_lote": "Interno"
  }
]
data[ComprobanteItem][0][cantidad_rechazada]=0
data[ComprobanteItem][1][id_comprobante]=57395
data[ComprobanteItem][1][id]=149976
data[ComprobanteItem][1][cantidad]=10.00
data[ComprobanteItem][1][descuento_unitario]=0.00
data[ComprobanteItem][1][precio_unitario_bruto]=7.69
data[ComprobanteItem][1][precio_unitario]=7.69
data[ComprobanteItem][1][orden]=3
data[ComprobanteItem][1][n_item]=1
data[ComprobanteItem][1][id_iva]=1
data[ComprobanteItem][1][id_producto]=16101
data[ComprobanteItem][1][item_observacion]=CNMG 120408-NRM  T7335
data[ComprobanteItem][1][dias]=15
data[ComprobanteItem][1][id_comprobante_item_origen]=149974
data[ComprobanteItem][1][activo]=1
data[ComprobanteItem][1][cantidad_cierre]=10.00
data[ComprobanteItem][1][id_unidad]=1
data[ComprobanteItem][1][precio_minimo_producto]=2.00
data[ComprobanteItem][1][requiere_conformidad]=0
data[ComprobanteItem][1][cantidad_ceros_decimal]=2
data[ComprobanteItem][1][ajusta_diferencia_cambio]=0
data[ComprobanteItem][1][fecha_entrega]=2018-06-30 00:00
data[ComprobanteItem][1][fecha_generacion]=2018-05-08 00:00
data[ComprobanteItem][1][d_producto]=Herremientas para fabrica "Insertos, Fresas, Mechas, Machos,etc." concepto general.
data[ComprobanteItem][1][codigo]=HER-FAB-001
data[ComprobanteItem][1][total_remito_origen]=10.00
data[ComprobanteItem][1][total_pendiente_irm]=0.00
data[ComprobanteItem][1][Lotes]=[
  {
    "id": "16",
    "id_comprobante_item": "149976",
    "id_lote": "48",
    "orden": null,
    "Lote": {
      "id": "48",
      "fecha_emision": "2015-03-27 08:07:03",
      "utilizado": null,
      "d_lote": "TAPA 2\" PT 3/P",
      "id_tipo_lote": "2",
      "codigo": "EB2793"
    },
    "codigo": "EB2793",
    "id_tipo_lote": "2",
    "fecha_emision": "2015-03-27 08:07:03",
    "d_tipo_lote": "Proveedor"
  }
]
data[ComprobanteItem][1][cantidad_rechazada]=0
data[ComprobanteItem][2][id_comprobante]=57395
data[ComprobanteItem][2][id]=149977
data[ComprobanteItem][2][cantidad]=10.00
data[ComprobanteItem][2][descuento_unitario]=0.00
data[ComprobanteItem][2][precio_unitario_bruto]=7.69
data[ComprobanteItem][2][precio_unitario]=7.69
data[ComprobanteItem][2][orden]=2
data[ComprobanteItem][2][n_item]=2
data[ComprobanteItem][2][id_iva]=1
data[ComprobanteItem][2][id_producto]=16101
data[ComprobanteItem][2][item_observacion]=CNMG 120408-RM  T8315
data[ComprobanteItem][2][dias]=15
data[ComprobanteItem][2][id_comprobante_item_origen]=149973
data[ComprobanteItem][2][activo]=1
data[ComprobanteItem][2][cantidad_cierre]=10.00
data[ComprobanteItem][2][id_unidad]=1
data[ComprobanteItem][2][precio_minimo_producto]=2.00
data[ComprobanteItem][2][requiere_conformidad]=0
data[ComprobanteItem][2][cantidad_ceros_decimal]=2
data[ComprobanteItem][2][ajusta_diferencia_cambio]=0
data[ComprobanteItem][2][fecha_entrega]=2018-06-30 00:00
data[ComprobanteItem][2][fecha_generacion]=2018-05-08 00:00
data[ComprobanteItem][2][d_producto]=Herremientas para fabrica "Insertos, Fresas, Mechas, Machos,etc." concepto general.
data[ComprobanteItem][2][codigo]=HER-FAB-001
data[ComprobanteItem][2][total_remito_origen]=10.00
data[ComprobanteItem][2][total_pendiente_irm]=0.00
data[ComprobanteItem][2][Lotes]=[]
data[ComprobanteItem][2][cantidad_rechazada]=0