<?php
	    include(APP.'View'.DS.'ComprobanteValores'.DS.'json'.DS.'comprobante_valor.ctp');
		
		
		
		$model["id"]["show_grid"] = 1;
        $model["id"]["header_display"] = "Id";
        $model["id"]["order"] = "0";
        $model["id"]["width"] = "5";
		$model["id"]["read_only"] = "1";
		$model["id"]["text_colour"] = "#CC0022";
		
		
		$model["d_banco"]["show_grid"] = 1;
        $model["d_banco"]["header_display"] = "Banco.";
        $model["d_banco"]["order"] = "1";
        $model["d_banco"]["width"] = "20";
		$model["d_banco"]["read_only"] = "1";
		$model["d_banco"]["text_colour"] = "#CC0022";
		
		$model["d_banco_sucursal"]["show_grid"] = 1;
        $model["d_banco_sucursal"]["header_display"] = "Sucursal.";
        $model["d_banco_sucursal"]["order"] = "2";
        $model["d_banco_sucursal"]["width"] = "20";
		$model["d_banco_sucursal"]["read_only"] = "1";
		$model["d_banco_sucursal"]["text_colour"] = "#CC0022";
		
        $model["monto"]["show_grid"] = 1;
        $model["monto"]["header_display"] = "Monto";
        $model["monto"]["order"] = "3";
        $model["monto"]["width"] = "20";
		$model["monto"]["read_only"] = "1";		
		$model["monto"]["text_colour"] = "#006600";
		
		
		$model["d_moneda"]["show_grid"] = 1;
        $model["d_moneda"]["header_display"] = "Moneda";
        $model["d_moneda"]["order"] = "4";
        $model["d_moneda"]["width"] = "20";
		$model["d_moneda"]["read_only"] = "1";
		$model["d_moneda"]["text_colour"] = "#006600";	
		
        
        $model["nro_cheque"]["show_grid"] = 1;
        $model["nro_cheque"]["header_display"] = "Nro. Cheque";
        $model["nro_cheque"]["order"] = "5";
        $model["nro_cheque"]["width"] = "20"; 
		$model["nro_cheque"]["read_only"] = "1";		
		$model["nro_cheque"]["text_colour"] = "#770000";	
	
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteValor",
                    "content" => $model
                );
        echo json_encode($output);
?>