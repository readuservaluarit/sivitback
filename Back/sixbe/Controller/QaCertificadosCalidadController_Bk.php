<?php
App::uses('ComprobantesController', 'Controller');

  /**
    * @secured(CONSULTA_QACERTIFICADOCALIDAD)
  */
class QaCertificadosCalidadNUEVOController extends ComprobantesController {
	
	
	/*NUEVO CONTROLLER PARA QA A MEDIO A HACER*/
	
	
    public $name = EnumController::QaCertificadosCalidad;
    public $model = EnumModel::Comprobante;
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $requiere_impuestos = 0;
    public $id_sistema_comprobante = EnumSistema::PRODUCCION;
    
    
    
    
	public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';/*SIN PUNTO Y COMA A PROPOSITO*/
            
        $this->loadModel($this->model);                                                                                 
        $conditions = $this->RecuperoFiltros($this->model);
                   
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>array('Persona','EstadoComprobante',/*'ComprobanteItem'=>array('Producto')*/'TipoComprobante','PuntoVenta','Usuario'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.nro_comprobante desc'
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
		
        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));

        //Headers
        $formBuilder->addHeader('id', 'cotizacion.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'cotizacion.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }

    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
            
             if(isset($valor['TipoComprobante'])){
                 $valor[$this->model]['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                 $valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
                 unset($valor['TipoComprobante']);
             }
             
             if(isset($valor['PuntoVenta']))
                $valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
                          
             if(isset($valor['EstadoComprobante'])){
                $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
                unset($valor['EstadoComprobante']);
             }
             if(isset($valor['CondicionPago'])){
                $valor[$this->model]['d_condicion_pago'] = $valor['CondicionPago']['d_condicion_pago'];
                unset($valor['CondicionPago']);
             }
             
             if(isset($valor['Persona'])){
                 if($valor['Persona']['id']== 1){
                     
                    $valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social']; 
                    $valor[$this->model]['cuit'] = $valor['Comprobante']['cuit']; 
                 }else{
                     $valor[$this->model]['razon_social'] = $valor['Persona']['razon_social'];
                     $valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
                     $valor[$this->model]['id_lista_precio_defecto_persona'] = $valor['Persona']['id_lista_precio'];
                 }
                 unset($valor['Persona']);
             }

		     if(isset($valor['Usuario'])){
                    $valor['Comprobante']['d_usuario'] = $valor['Usuario']['nombre'];
		     }
             $this->{$this->model}->formatearFechas($valor);
             
             unset($valor['PuntoVenta']);  
             unset($valor['Usuario']); 
        }
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
     }
    //fin vista json
    }
	
	/**
    * @secured(CONSULTA_QACERTIFICADOCALIDAD)
    */
    /*public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        //Recuperacion de Filtros
        
        $fecha = $this->getFromRequestOrSession('QaCertificadoCalidad.fecha');
        $fecha_hasta = $this->getFromRequestOrSession('QaCertificadoCalidad.fecha_hasta');
        $nro_comprobante = $this->getFromRequestOrSession('QaCertificadoCalidad.nro_comprobante');
        
        $id_persona = $this->getFromRequestOrSession('QaCertificadoCalidad.id_persona');
        
        if($this->Auth->user('id_persona')>0){
        	$id_persona = $this->Auth->user('id_persona');
        	
        }
		$razon_social = $this->getFromRequestOrSession('QaCertificadoCalidad.persona_razon_social');
        $id_orden_armado = $this->getFromRequestOrSession('QaCertificadoCalidad.id_orden_armado');
        $id_pedido_interno = $this->getFromRequestOrSession('QaCertificadoCalidad.id_pedido_interno');
        $id_estado = $this->getFromRequestOrSession('QaCertificadoCalidad.id_estado');
        $codigo_producto = $this->getFromRequestOrSession('QaCertificadoCalidad.codigo_producto');
        $conditions = array(); 
        
        if($fecha!=""){
        	
        	$exp = explode('/', $fecha);//el  front web me envia guiones
        	
        	if(count($exp) != 0)
        		$fecha= $exp[2].'-'.$exp[1].'-'.$exp[0];
        		
            array_push($conditions, array('QaCertificadoCalidad.fecha >=' => $fecha)); 
        }
            
        if($fecha_hasta!=""){
        	
        	$exp = explode('/', $fecha_hasta);//el  front web me envia guiones
        	
        	if(count($exp) != 0)
        		$fecha_hasta= $exp[2].'-'.$exp[1].'-'.$exp[0];
        	
        	array_push($conditions, array('QaCertificadoCalidad.fecha <=' => $fecha_hasta));
        }
            
        if($nro_comprobante!="")
            array_push($conditions, array('QaCertificadoCalidad.id ' => $nro_comprobante));
        
            
        if($this->Auth->user('id_persona')>0){
            	
            	$id_persona = $this->Auth->user('id_persona');
        }
        
        
        if($id_persona!="")
            array_push($conditions, array('QaCertificadoCalidad.id_persona ' => $id_persona));
            
         if($razon_social!="")
            array_push($conditions, array('Persona.razon_social LIKE' => '%' . $razon_social  . '%')); 
        
        if($id_orden_armado!="")
            array_push($conditions, array('QaCertificadoCalidad.id_orden_armado' => $id_orden_armado));
        
        if($id_pedido_interno!="")
            array_push($conditions, array('QaCertificadoCalidad.id_pedido_interno' => $id_pedido_interno));
        
         if($id_estado!="")
            array_push($conditions, array('QaCertificadoCalidad.id_estado ' => $id_estado));         
         
         if($codigo_producto!="")
         	array_push($conditions, array('Producto.codigo LIKE ' =>'%'.$codigo_producto.'%'));                
                           
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>array('Persona','Estado','QaCertificadoCalidadItem'=>array('Componente'=>array('Categoria')),'Producto','PedidoInterno','OrdenArmado'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => 'QaCertificadoCalidad.id desc'
        );
        
        
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor)
        {
             $valor['QaCertificadoCalidad']['QaCertificadoCalidadItem'] = $valor['QaCertificadoCalidadItem'];
             unset($valor['QaCertificadoCalidadItem']);
             
            if(isset($valor['QaCertificadoCalidad']['QaCertificadoCalidadItem'])){
                 foreach($valor['QaCertificadoCalidad']['QaCertificadoCalidadItem'] as &$componente ){
                     
                     $componente["d_producto_hijo"] = $componente["Componente"]["d_producto"];
                     //$producto["d_producto"] = $this->setFieldProperties("d_producto","Producto",$producto["Producto"]["d_producto"],"1","3");
                     //$producto["codigo_producto"] = $this->setFieldProperties("codigo_producto","Codigo",$producto["Producto"]["codigo"],"1","4");
                     
                     $componente["codigo_hijo"] = $componente["Componente"]["codigo"];
                     
                     if(isset($componente["Categoria"]))
                        $componente["d_categoria"] = $componente["Categoria"]["d_categoria"];
                     
                     unset($componente["Componente"]);
                 }
            }
            
            
             //$valor['QaCertificadoCalidad']['estado'] = $valor['Estado'];
             $valor['QaCertificadoCalidad']['d_estado'] = $valor['Estado']['d_estado'];
             unset($valor['Estado']);
             //$valor['QaCertificadoCalidad']['Persona'] = $valor['Persona'];
             $valor['QaCertificadoCalidad']['razon_social'] = $valor['Persona']['razon_social'];
             $valor['QaCertificadoCalidad']['id_persona'] = $valor['Persona']['id'];
             $valor['QaCertificadoCalidad']['d_producto'] = $valor['Producto']['d_producto'];
             unset($valor['Cliente']);
             $valor['QaCertificadoCalidad']['Comprobante'] = $valor['Comprobante'];
             $valor['QaCertificadoCalidad']['nro_pedido_interno'] = $valor['PedidoInterno']['nro_comprobante'];
             unset($valor['Comprobante']);
             $valor['QaCertificadoCalidad']['OrdenArmado'] = $valor['OrdenArmado'];
             $valor['QaCertificadoCalidad']['nro_orden_armado'] = $valor['OrdenArmado']['nro_comprobante'];
             
             unset($valor['Comprobante']);
             unset($valor['OrdenArmado']);
             unset($valor['Persona']);
             unset($valor['Producto']);
      
            }
             
             $seteo_form_builder = array("showActionColumn" =>true,"showNewButton" =>false,"showDeleteButton"=>false,"showEditButton"=>false,"showPrintButton"=>true,"showFooter"=>true,"showHeaderListado"=>true,"showXlsButton"=>false);
             $this->data = $data;
             $this->title_form = "Listado de Certificados de calidad";
             $this->preparaHtmLFormBuilder($this,$seteo_form_builder,"");
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
    //fin vista json
    }*/
    
    

    /**
    * @secured(CONSULTA_QACERTIFICADOCALIDAD)
    */
    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

     /**
    * @secured(ADD_QACERTIFICADOCALIDAD)
    */
     public function add(){
		parent::add();    
    }
    
       /**
    * @secured(MODIFICACION_QACERTIFICADOCALIDAD)
    */
	 public function edit($id){
		 parent::edit($id);
		 return;  
    }
	
	 
 /**
    * @secured(MODIFICACION_QACERTIFICADOCALIDAD)
 */    
public function Anular($id_comprobante){
            
            $this->loadModel("Comprobante");
            $this->loadModel("Asiento");
            
            
          $factura = $this->Comprobante->find('first', array(
                    'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                    'contain' => array('ComprobanteValor','Asiento','ChequeEntrada')
                ));
                
         $ds = $this->Comprobante->getdatasource(); 
                 
                
            
            if(
                
            
                $this->Comprobante->getEstadoGrabado($id_comprobante)!= EnumEstadoComprobante::Anulado &&
                
               
                
                $factura 
             
                
            
                
            
            ){ 
            
             try{
                 $ds->begin();
                //le clavo el estado ANULADO
                $this->Comprobante->updateAll(
                                                            array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
                                                            array('Comprobante.id' => $id_comprobante) );  
                                                            
                                                            
                
                $reclamo = $this->Comprobante->find('first', array(
                		'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                		'contain' => false
                ));
                
                $this->Auditar($id_comprobante,$reclamo);
                
                
                                                            
                $ds->commit(); 
                $tipo = EnumError::SUCCESS; 
                $mensaje = "El Comprobante se anulo correctamente ";   
                
                
                        
             }catch(Exception $e){ 
                
                $ds->rollback();
                $tipo = EnumError::ERROR;
                $mensaje = "No es posible anular el Comprobante ERROR:".$e->getMessage();
                
              }     
            }else{
                $tipo = EnumError::ERROR;
                $mensaje = "No es posible anular el Comprobante ya que o ya ha sido ANULADO"."\n"; 
                
            }
            
             $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                );      
            echo json_encode($output);
            die();
}
	
    
     /**
    * @secured(BAJA_QACERTIFICADOCALIDAD)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->QaCertificadoCalidad->id = $id;
       try{
            if ($this->QaCertificadoCalidad->saveField('activo', "0")) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Certificado de Calidad ha sido eliminado exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }

  /**
    * @secured(BAJA_QACERTIFICADOCALIDAD)
    */
    public function deleteItem($id,$externo=1){
        
        parent::deleteItem($id,$externo);    
        
    }

   
    
    
   
    /*
    public function cerrar($id){
        
        $this->loadModel($this->model);
        $this->loadModel("Movimiento");
        
        $ds = $this->QaCertificadoCalidad->getdatasource();
        try{
            $ds->begin();
            $this->request->data["QaCertificadoCalidad"]["Estado"] = EnumEstado::Cerrada;
            
            
            
            $ds->commit();
        }catch(Exception $e){
                $ds->rollback();
                $mensaje = "Ha ocurrido un error,la QaCertificadoCalidad no ha podido ser cerrada.";
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => ""
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
    }*/
    
    

	/*
        $this->loadModel("QaCertificadoCalidad");
        $this->loadModel("ArticuloRelacion");
        $this->QaCertificadoCalidad->id = $id;
        $this->QaCertificadoCalidad->contain(array('Producto','OrdenArmado','QaCertificadoCalidadItem' => array('Componente'=>array('Categoria','ArticuloRelacion'=>array('ArticuloHijo'))),'Persona'=> array('Pais','Provincia')));
        //$this->QaCertificadoCalidad->order(array('QaCertificadoCalidadItem.orden asc'));
        $this->request->data = $this->QaCertificadoCalidad->read(); 
        Configure::write('debug',0);
        
        if( $this->request->data["Persona"]["id"] == null ){
            $this->request->data["Persona"]["razon_social"] = $this->request->data["QaCertificadoCalidad"]["razon_social"];
            $this->request->data["Persona"]["cuit"] = $this->request->data["QaCertificadoCalidad"]["cuit"]; 
        }
		
        foreach($this->request->data['QaCertificadoCalidadItem'] as &$dato){
            
           $id_mp_padre = $dato["Componente"]["ArticuloRelacion"][0]["ArticuloHijo"]["id"];//id de la MP 
           
           $costo = $this->ArticuloRelacion->find('first',array(
                                    'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_mp_padre),
                                    'contain' =>'ArticuloHijo'
                                    ));              
           if($costo){
               $dato["Componente"]["ArticuloRelacion"][0]["ArticuloHijo"]["d_costo"] = $costo["ArticuloHijo"]["d_producto"];
           }else{
                $dato["Componente"]["ArticuloRelacion"][0]["ArticuloHijo"]["d_costo"] = $dato["Componente"]["ArticuloRelacion"][0]["ArticuloHijo"]["d_producto_extensa"];
           }
       }
       $this->set('datos_pdf',$this->request->data); 
        $this->set('id',$id);
        Configure::write('debug',0);
        $this->response->type('pdf');
        $this->layout = 'pdf'; //esto usara el layout pdf.ctp
        $this->render();
     }*/
    
	/*
    public function initialize(){ //Seteo los ordenes del Listado
        
        $this->addCalculatedField($this->setFieldProperties("QaCertificadoCalidadItem","Items de las QaCertificadosCalidad","","0","0"));
        $this->addCalculatedField($this->setFieldProperties("moneda","Moneda","","0","0"));;
        $this->addCalculatedField($this->setFieldProperties("estado","Estado","","0","0"));;
        $this->addCalculatedField($this->setFieldProperties("Cliente","Cliente","","0","0"));;
        
        
        //seteo las propiedades de los Items Agregados en runtime
        $this->addCalculatedFieldItem($this->setFieldProperties("d_producto","Producto","","1","3"));
        $this->addCalculatedFieldItem($this->setFieldProperties("codigo_producto","Codigo","","1","4"));
        
    }*/
    
    /**
     * @secured(CONSULTA_QACERTIFICADOCALIDAD)
     */
    public function getModel($vista='default'){
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    }
	
	/**
    * @secured(REPORTE_QACERTIFICADOCALIDAD)
    */
    public function pdfExport($id,$vista=''){
      parent::pdfExport($id,$vista);  
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);

        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);
    }
	
	    /**
     * @secured(BTN_EXCEL_REPORTE_QACERTIFICADOCALIDAD)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Cotizaciones");

    }
	
	
	    /**
     * @secured(CONSULTA_QACERTIFICADOCALIDAD)
     */
    
    public function chequeoValorNoRepetido($nombre_campo,$valor,$id_comprobante,$id_persona,$id_tipo_comprobante=EnumTipoComprobante::PedidoInterno){
    	
    	
    	parent::chequeoValorNoRepetido($nombre_campo,$valor,$id_comprobante,$id_persona,$id_tipo_comprobante);
    }
    
    

    
   
    
}
?>