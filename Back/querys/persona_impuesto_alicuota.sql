/*
SQLyog Ultimate v9.63 
MySQL - 5.5.38-0+wheezy1 : Database - dbarcangel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbarcangel` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dbarcangel`;

/*Table structure for table `persona_impuesto_alicuota` */

CREATE TABLE `persona_impuesto_alicuota` (
  `Id_Persona_Impuesto_Alicuota` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha_Vigencia_Desde` date NOT NULL DEFAULT '0000-00-00',
  `Fecha_Vigencia_Hasta` date NOT NULL DEFAULT '0000-00-00',
  `Porcentaje_Alicuota` decimal(6,2) NOT NULL DEFAULT '0.00',
  `Id_Impuesto_Alicuota` int(11) NOT NULL DEFAULT '0',
  `Id_Persona` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id_Persona_Impuesto_Alicuota`),
  UNIQUE KEY `XAK1Persona_Impuesto_Alicuota` (`Id_Impuesto_Alicuota`,`Id_Persona`),
  KEY `XIF1Persona_Impuesto_Alicuota` (`Id_Impuesto_Alicuota`),
  KEY `XIF2Persona_Impuesto_Alicuota` (`Id_Persona`)
) ENGINE=InnoDB AUTO_INCREMENT=47124 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
