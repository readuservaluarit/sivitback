<?php
class EntityPicker{
    
    public $model = "Funcion";
    
    public $pickerTitle = "Selección de Entidad";
    
    public $pickerFields = array();
    
    public $width = "600px";
    
    public function getPickerConditions($busqueda){
        return array();
    }
    
    public function getPickerGroup() {
        return array();
    }
    
    public function getPickerJoins(){
        
        return array();
        
    }
    
    public function getPickerContain(){
        
        return array();
        
    }
    
    public function getPickerFields(){
        return '*';
    }
    
     public function getPickerOrder() {
        return '';
    }   
        
    public function getFilterFields($controller, $model, $busqueda, $pickerId, $selectFields, $callbackJS, $filterQueryFields){
               $filterFields = "<label class='col-lg-2 control-label'>Buscar:";
               $filterFields .= '<div class="input-append" style="padding-left:10px;">';
               $filterFields .= $controller->Form->input('bus_unico', array('class' => 'form-horizontal', 'label' => false, 'div' => false, 'class' => 'bus_unico', 'value' => (isset($busqueda['bus_unico'])?$busqueda['bus_unico']:""))); 
               $filterFields .= '<span class="add-on"><i id="botonBuscarPicker" class="icon-search">';
               $filterFields .= '</i></span></div>';          
                $controller->Js->get('#botonBuscarPicker')->event('click',
                    $controller->Js->request(
                        array('controller' => 'EntityPicker', 'action' => 'index', get_class($this), $pickerId, $selectFields, $callbackJS, $filterQueryFields),
                        array('update' => '.fancybox-inner', 'async' => true,
                            'dataExpression' => true, 'method' => 'post',
                            'data' => $controller->Js->serializeForm(array('isPost' => true, 'inline' => true)),
                            'before' => 'showProcessing(true);', 
                            'complete' => 'showProcessing(false); $(".bus_unico:first").focus();/*$.fancybox.update();*/'
                        )
                    )                                   
                );

               $filterFields .= "</label>";
               
               return  $filterFields;
        
    }
    
    public function prepareData($data){
        
        return $data;
        
    }
    
    public function getPickerInitialFilter(){
        
        return array();
        
    }

}