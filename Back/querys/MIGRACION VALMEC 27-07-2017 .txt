DELIMITER $$

ALTER ALGORITHM=UNDEFINED DEFINER=`six`@`%` SQL SECURITY DEFINER VIEW `comprobante_item_view` AS (
SELECT
   comprobante_item.*,
  `tipo_comprobante`.`cantidad_ceros_decimal`     AS `cantidad_ceros_decimal`

FROM ((`comprobante_item`
    JOIN `comprobante`
      ON ((`comprobante`.`id` = `comprobante_item`.`id_comprobante`)))
   JOIN `tipo_comprobante`
     ON ((`comprobante`.`id_tipo_comprobante` = `tipo_comprobante`.`id`))))$$

DELIMITER ;


/ Alter table in target /
ALTER TABLE `comprobante_item` 
 ADD COLUMN `requiere_conformidad` tinyint(4)   NULL DEFAULT 0 after `id_tipo_lote` ;
/ Alter table in target /
ALTER TABLE `producto_tipo` 
 ADD COLUMN `requiere_conformidad` tinyint(4)   NULL after `id_tipo_comercializacion` ;
 
 
 
 /* Alter table in target */
ALTER TABLE `comprobante` 
	CHANGE `id_estado_comprobante` `id_estado_comprobante` int(11)   NULL DEFAULT 1 COMMENT 'no_visible' after `valida_cuenta_corriente` , 
	CHANGE `orden_compra_externa` `orden_compra_externa` varchar(50)  COLLATE latin1_swedish_ci NULL after `certificados` , 
	ADD COLUMN `enlazar_con_moneda` tinyint(4)   NULL after `permite_ajuste_diferencia_cambio` , 
	ADD COLUMN `id_moneda_enlazada` int(4)   NULL after `enlazar_con_moneda` , 
	DROP COLUMN `numero_orden_compra_externo` , 
	ADD KEY `id_moneda_enlazada`(`id_moneda_enlazada`) ;
	
	



/* Alter table in target */
ALTER TABLE `comprobante_item` 
	ADD COLUMN `ajusta_por_diferencia_cambio` TINYINT(4)   NULL AFTER `requiere_conformidad` , 
	ADD COLUMN `monto_diferencia_cambio` DECIMAL(20,2)   NULL AFTER `ajusta_por_diferencia_cambio` ;
	

/*updatear el tipo_comprobante` 310*/

INSERT INTO funcion (c_funcion,d_funcion)
SELECT  REPLACE(c_funcion,'TIPO_FACTURA','DEBITO_INTERNO_PROVEEDOR') AS c_funcion,
        REPLACE(d_funcion,'Tipo Factura','Debito Interno PROVEEDOR') AS d_funcion FROM funcion
WHERE c_funcion LIKE '%TIPO_FACTURA%'




/* INSERTAR AL ADMINISTRADOR */
INSERT INTO funcion_rol (id_funcion,id_rol)
SELECT id,1 AS id_rol FROM funcion WHERE c_funcion LIKE '%DEBITO_INTERNO_PROVEEDOR%'




INSERT INTO funcion (c_funcion,d_funcion)
SELECT  REPLACE(c_funcion,'BTN_EXCEL_CREDITOSINTERNOPROVEEDOR','BTN_EXCEL_DEBITOSINTERNOPROVEEDOR') AS c_funcion,
        REPLACE(d_funcion,'Tipo Factura','Debito Interno PROVEEDOR') AS d_funcion FROM funcion
WHERE c_funcion LIKE '%TIPO_FACTURA%'

INSERT INTO funcion_rol (id_funcion,id_rol)
SELECT id,1 AS id_rol FROM funcion WHERE c_funcion LIKE '%BTN_EXCEL_CREDITOSINTERNOPROVEEDOR%'





