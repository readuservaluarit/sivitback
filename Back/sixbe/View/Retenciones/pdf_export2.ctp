<?php

/**
 * PDF exportación de Cotizacion
 */

App::import('Vendor','tcpdf/tcpdf'); 

// create new PDF document
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
     
     public $id;
     public $name;
     
     public function setid($id){
        $this->id = $id;
     }
     
     public function getid(){
        return $this->id;
     }
     
     public function setName($name){
        $this->name = $name;
     }
     
     public function getName(){
        return $this->name;
     }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, $this->getName().' Nro '.$this->getid().'                                                       Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->setid($datos_pdf["Comprobante"]["nro_comprobante_completo"]);
$pdf->setName($datos_pdf["TipoComprobante"]["d_tipo_comprobante"]);


ob_clean();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sivit by Valuarit');
$pdf->SetTitle('Comprobante');
$pdf->SetSubject('Comprobante');
//$pdf->SetKeywords('');



// set default header data
$logo = '/img/header.jpg';
$title = "";
$subtitle = ""; 
//$pdf->SetHeaderData($logo, 190, $title, $subtitle, array(0,0,0), array(0,104,128));
$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 50, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(20);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
/*$lg = Array();
$lg['a_meta_charset'] = 'ISO-8859-1';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';
*/
$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set default font subsetting mode
//$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
/*
$html = <<<EOD
<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
EOD;
*/

$tbody = "";

$renglon = $datos_pdf["ComprobanteReferenciaImpuesto"];

$data = array();



 $descuento = ($renglon['descuento_unitario']);
  $precio_subtotal = ($renglon['precio_unitario'])* $renglon['cantidad'];
  
  
  foreach($renglon['Comprobante']["ComprobanteItem"] as $item){ 
   $tbody .= "<tr>";
   $tbody .= "<td>" .$renglon['Impuesto']['d_impuesto'] . "</td>";
   $tbody .= "<td>" .$item['Comprobante']['fecha_contable']. "</td>";
   $tbody .= "<td align='center'>" .$item['Comprobante']["nro_comprobante"] . "</td>";
   $tbody .= "<td align='center'>" .$item['Comprobante']["total_comprobante"] ."</td>";
   $tbody .= "<td align='center'>" .$item['Comprobante']["subtotal_neto"]. "</td>";    
   $tbody .= "<td>" .$renglon["tasa_impuesto"]. "</td>";    
   $tbody .= "<td>" .round(($item['Comprobante']["subtotal_neto"]*$renglon["tasa_impuesto"])/100,2). "</td>";
   $tbody .= "</tr>";
   }
 
//agrego las observaciones

   

$html='';

$time_cot = strtotime($datos_pdf["Comprobante"]["fecha_generacion"]);
// create new PDF document
$fecha_entrega = new DateTime($datos_pdf['Comprobante']['fecha_contable']);
$fecha_inicio_actividad = new DateTime($datos_empresa["DatoEmpresa"]["fecha_inicio_actividad"]);

$html = '<table width="100%" border="0" cellspacing="0" style="font-size:10px;border:2px solid #000000; font-family:\'Courier New\', Courier, monospace">
          <tr>
            <td style="padding: 5px 5px 5px 5px;" width="37%" rowspan="4" align="center" valign="center"  ><img src="'.$datos_empresa["DatoEmpresa"]["app_path"].'/img/'.$datos_empresa["DatoEmpresa"]["id"].'.png" width="476" height="129" />  </td>
            <td width="8%" height="3%" rowspan="2"  style="border:2px solid #000000">
                <div align="center" style="font-size:30px">'.strtoupper($datos_pdf["TipoComprobante"]["letra"]).'</div>
                 <span align="center" style="font-size:10px;">cod 01</span>
            </td>
            <td width="55%" style="padding-left:5px; font-size:17px;font-weight:bold;"> FACTURA: </td>
            
          </tr>
          <tr>
            <td style="padding-left:5px; font-size:15px;"> Nro '.str_pad($datos_pdf["PuntoVenta"]["numero"],4,"0",STR_PAD_LEFT).'-'.str_pad($datos_pdf["Comprobante"]["nro_comprobante_afip"],8,"0",STR_PAD_LEFT).'</td>
          </tr> 
          
          <tr>
            <td width="8%" height="2%" align="center" valign="middle" >&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr >
            <td  width="8%" height="5%" align="center" valign="middle" >&nbsp;</td>
            <td style="padding-left:5px;"> Original Blanco / Copia color </td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["calle"].' '.$datos_empresa["DatoEmpresa"]["numero"].' '.$datos_empresa["DatoEmpresa"]["codigo_postal"].' '.$datos_empresa["Provincia"]["d_provincia"].'</td>
            <td rowspan="5">&nbsp;</td>
            <td style="padding-left:5px;"> Fecha: '.$fecha_entrega->format('d-m-Y').'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> E-mail: '.$datos_empresa["DatoEmpresa"]["email"].'</td>
            <td style="padding-left:5px;"> CUIT: '.$datos_empresa["DatoEmpresa"]["cuit"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> Tel: '.$datos_empresa["DatoEmpresa"]["tel"].'</td>
            <td style="padding-left:5px;"> Ingresos Brutos: '.$datos_empresa["DatoEmpresa"]["iibb"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["website"].'</td>
            <td style="padding-left:5px;"> Inicio de actividades: '.$fecha_inicio_actividad->format('d-m-Y').'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.strtoupper($datos_empresa["TipoIva"]["d_tipo_iva"]).'</td>
            <td style="padding-left:5px;">&nbsp;</td>
          </tr>
        </table>';


$html.='<table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:10px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody>
</table>


<table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:11px;">

    <tr style="background-color:rgb(224, 225, 229);">
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;">Conecpto</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:50%;">Fecha Comp.</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">Nro. Comp.</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;">Monto del Comp.</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">Monto Imponible</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">Alicuota</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">Monto Retenido</th>
    </tr>

  <tbody>
    '.$tbody.'
  </tbody>  
</table>';

   $html .= '<table border="0px" cellspacing="1" cellpadding="20" style="width:100%;font-size:12px;">';
   $html .= "<tr>";
   $html .= "<td colspan='8'>Observaciones:".nl2br($datos_pdf["Comprobante"]["observacion"])." ".nl2br($datos_pdf["TipoComprobante"]["impresion_observacion_extra"])."</td>";
   $html .= "</tr>";
   $html .= "</table>";
   
   $html .= "<HR>";
   
   $html .= '<table border="0px" cellspacing="1" cellpadding="1" style="width:100%;font-size:12px;">';
   $html .= "<tr>";
   $html .= "<td colspan='8'>Moneda: ".$datos_pdf['Moneda']['d_moneda']."</td>";
   $html .= "</tr>";

   $html .= "<tr>";
   $html .= "<td colspan='8'>Total impuesto retenido: ".$datos_pdf['Moneda']['simbolo']." ".$datos_pdf['Comprobante']['total_comprobante']."</td>";
   $html .= "</tr>";
   $html .= "</table>";
   
   
$pdf->writeHTML($html, true, false, true, false, '');


// QRCODE,L : QR-CODE Low error correction
// set style for barcode
$style = array(
    'border' => 2,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

//$pdf->Text(20, 25, 'QRCODE L');

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->SetBarcode(date("Y-m-d H:i:s", time()));
ob_clean();
$pdf->Output(str_replace(".","",$datos_pdf["TipoComprobante"]["abreviado_tipo_comprobante"].'_'.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'-'.$datos_pdf["Persona"]["razon_social"]).'.pdf', 'D');


//============================================================+
// END OF FILE
//============================================================+