<?php

		App::uses('AuditoriaComprobante', 'Model');  
        $auditoriaComprobante_obj = new AuditoriaComprobante();
        $auditoriaComprobante_obj->setVirtualFieldsForView();
        
        $auditoriaComprobante_obj->SetFieldsToView($auditoriaComprobante_obj->virtual_fields_view,$model);
        // CONFIG  CON MODE DISPLAYED CELLS
        
        
        $model["d_tipo_comprobante"]["show_grid"] = 1;
        $model["d_tipo_comprobante"]["header_display"] = "Tipo. Comprobante";
        $model["d_tipo_comprobante"]["width"] = "20";
        $model["d_tipo_comprobante"]["order"] = "0";
        $model["d_tipo_comprobante"]["text_colour"] = "#000000";
        
        $model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
        $model["pto_nro_comprobante"]["width"] = "20";
        $model["pto_nro_comprobante"]["order"] = "1";
        $model["pto_nro_comprobante"]["text_colour"] = "#000000";
        
        
        
        $model["d_estado"]["show_grid"] = 1;
        $model["d_estado"]["header_display"] = "Estado";
        $model["d_estado"]["width"] = "15"; 
        $model["d_estado"]["order"] = "2";
        $model["d_estado"]["text_colour"] = "#000000";
        
        
        
        $model["fecha"]["show_grid"] = 1;
        $model["fecha"]["header_display"] = "Fecha";
        $model["fecha"]["width"] = "15"; 
        $model["fecha"]["order"] = "3";
        $model["fecha"]["text_colour"] = "#000000";
        
        
        $model["d_usuario"]["show_grid"] = 1;
        $model["d_usuario"]["header_display"] = "Usuario";
        $model["d_usuario"]["width"] = "15"; 
        $model["d_usuario"]["order"] = "4";
        $model["d_usuario"]["text_colour"] = "#000000";
        
   
         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "AuditoriaComprobante",
                    "content" => $model
                );
        
          echo json_encode($output);
?>