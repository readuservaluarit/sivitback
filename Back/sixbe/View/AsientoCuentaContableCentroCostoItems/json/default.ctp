<?php
        // CONFIG  CON MODE DISPLAYED CELLS
    

        $model["id_cuenta_contable"]["show_grid"] = 0;//INVISIBLE
		$model["id_cuenta_contable"]["type"] = EnumTipoDato::biginteger;//INVISIBLE
        $model["id_cuenta_contable"]["header_display"] = "id_cuenta_contable";
        $model["id_cuenta_contable"]["minimun_width"] = "20"; //Restriccion mas de esto no se puede achicar
        $model["id_cuenta_contable"]["order"] = "1";
        $model["id_cuenta_contable"]["text_colour"] = "#0000EE";
		$model["id_cuenta_contable"]["read_only"] = 1; //significa que esta columna arranca inmodificable => nunca se puede modificar por usuario

		//esto se presetea agrega en codigo como un CBO
		$model["id_centro_costo"]["show_grid"] = 1;
		$model["id_centro_costo"]["read_only"] = 1;//funciona con Combos el read only
        $model["id_centro_costo"]["header_display"] = "Centro Costo";
		$model["id_centro_costo"]["width"] = "100"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["id_centro_costo"]["minimun_width"] = "50"; //Restriccion mas de esto no se puede achicar
        $model["id_centro_costo"]["order"] = "2";
        $model["id_centro_costo"]["text_colour"] = "#000099";
		
		$model["porcentaje"]["show_grid"] = 1;
		$model["porcentaje"]["validate"] = 1;
		$model["porcentaje"]["read_only"] = 0; // EL UNICO Q ES MODIFICABLE
        $model["porcentaje"]["header_display"] = "Porcentaje";
		$model["porcentaje"]["width"] = "25"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["porcentaje"]["minimun_width"] = "20"; //Restriccion mas de esto no se puede achicar
        $model["porcentaje"]["order"] = "3";
        $model["porcentaje"]["text_colour"] = "#002288";
		
		$model["monto"]["show_grid"] = 1;
		$model["monto"]["read_only"] = 1;
        $model["monto"]["header_display"] = "Monto";
		$model["monto"]["width"] = "25"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["monto"]["minimun_width"] = "20"; //Restriccion mas de esto no se puede achicar
        $model["monto"]["order"] = "3";
        $model["monto"]["text_colour"] = "#002200";
		
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "AsientoCuentaContableCentroCostoItem",
                    "content" => $model
                );
        echo json_encode($output);
?>