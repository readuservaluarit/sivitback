<?php

class DepositoMovimientoTiposController extends AppController {
    public $name = 'DepositoMovimientoTipos';
    public $model = 'DepositoMovimientoTipo';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    /**
    * @secured(CONSULTA_DEPOSITO_MOVIMIENTO_TIPO)
    */
    public function index($id_producto = null) {
        
        $this->loadModel($this->model);
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $id_movimiento_tipo = $this->getFromRequestOrSession('DepositoMovimientoTipo.id_movimiento_tipo');
		$d_movimiento_tipo = $this->getFromRequestOrSession('DepositoMovimientoTipo.d_movimiento_tipo');
		
		$id_deposito =  $this->getFromRequestOrSession('DepositoMovimientoTipo.id_deposito');
        $es_origen =  $this->getFromRequestOrSession('DepositoMovimientoTipo.es_origen');
        $id_punto_venta =  $this->getFromRequestOrSession('DepositoMovimientoTipo.id_punto_venta');
        $id_tipo_comprobante =  $this->getFromRequestOrSession('DepositoMovimientoTipo.id_tipo_comprobante');
       
		$conditions = array(); 
        
        if($id_movimiento_tipo!=''){
            array_push($conditions, array('DepositoMovimientoTipo.id_movimiento_tipo' => $id_movimiento_tipo)); 
		}
		if($d_movimiento_tipo!=''){
        	array_push($conditions, array('DepositoMovimientoTipo.d_movimiento_tipo LIKE' => '%'.trim($d_movimiento_tipo).'%' ));
		}
    
        if($id_deposito!=''){
            array_push($conditions, array('DepositoMovimientoTipo.id_deposito' => $id_deposito)); 
            array_push($conditions, array('Deposito.id_deposito' => $id_deposito)); 
        }
        
        if($es_origen!=''){
            array_push($conditions, array('DepositoMovimientoTipo.es_origen' => $es_origen)); 
        }
        
        if($id_punto_venta!=''){
        	array_push($conditions, array('DepositoMovimientoTipo.id_punto_venta' => $id_punto_venta));
        }
		
        if($id_tipo_comprobante!=''){
        	array_push($conditions, array('DepositoMovimientoTipo.id_tipo_comprobante' => $id_tipo_comprobante));
        }
		

        
    
    	$data = $this->{EnumModel::DepositoMovimientoTipo}->index($conditions);
        
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     
        
        
        
        
    //fin vista json
        
    }
    
    /**
    * @secured(ABM_USUARIOS)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->Componente->id = $id;
            $this->Componente->contain();
            $this->request->data = $this->Componente->read();
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Componente');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Componentes');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Componentes');
        else
            $formBuilder->setTituloForm('Consulta de Componente');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('codigo', 'C&oacute;digo', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('descripcion', 'Descripci&oacute;n', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('costo', 'Costo', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('stock', 'Stock', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('stock_minimo', 'Stock M&iacute;nimo', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        
       
         
        
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'ComponenteAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('ComponenteCodigo', 'Debe ingresar un C&oacute;digo');
                validator.validateRequired('ComponenteDescripcion', 'Debe ingresar una descripcion');
                validator.validateNumeric('ComponenteStock', 'El stock ingresado debe ser num&eacute;rico');
                validator.validateNumeric('ComponenteStockMinimo', 'El stock ingresado debe ser num&eacute;rico');
                validator.validateRequired('ComponenteCosto', 'El costo ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_codigo',
                        'data': 'codigo=' + $('#ComponenteCodigo').val()+'&id='+$('#ComponenteId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('ComponenteCodigo','El C&oacute;digo que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

    /**
    * @secured(ABM_USUARIOS)
    */
    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

     /**
    * @secured(ADD_DEPOSITO_MOVIMIENTO_TIPO)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_add = "";
            
            try{
                if ($this->DepositoMovimientoTipo->saveAll($this->request->data, array('deep' => false))){
                    $mensaje = "El Tipo de Movimiento en el deposito ha sido asignada exitosamente";
                    $tipo = EnumError::SUCCESS;
                    $id_add = $this->DepositoMovimientoTipo->id;
                   
                }else{
                    $mensaje = "Ha ocurrido un error,El Tipo de Movimiento en el deposito no ha podido ser asignada.";
                  
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,El Tipo de Movimiento en el deposito no ha podido ser asiganda.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" =>$id_add
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    
      /**
    * @secured(MODIFICACION_DEPOSITO_MOVIMIENTO_TIPO)
    */
   function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "El Tipo de Movimiento en el deposito ha sido modificado exitosamente";
                     $status = EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
               }
               
               if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, El Tipo de Movimiento en el deposito no ha podido modificarse.', 'error');
                
            }
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            //$this->{$this->model}->contain('Unidad');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
  
  /**
    * @secured(BAJA_DEPOSITO_MOVIMIENTO_TIPO)
  */
   function delete($id) {
        
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->DepositoMovimientoTipo->id = $id;
       try{
            if ($this->DepositoMovimientoTipo->delete()) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Tipo de Movimiento en el deposito ha sido eliminado exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }


    
    
    
    public function home(){
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
    
  
    
    public function existe_codigo(){
        
        $codigo = $this->request->query['codigo'];
        $id_componente = $this->request->query['id'];
        $this->loadModel("Producto");
      
        $existe = $this->Componente->find('count',array('conditions' => array(
                                                                            'codigo' => $codigo,
                                                                            'id !=' =>$id_componente
                                                                            )
                                                                            ));
        
        $data = $existe;
        
        echo json_encode($data);
        die();
    }
    
    
    public function getModel($vista='default')
    {
    	$model = parent::getModelCamposDefault();//esta en APPCONTROLLER
    	
    	$model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
    	
    	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    
    	
    }
    
    
    
    private function editforView($model,$vista)
    {  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    	
    	$this->set('model',$model);
    	Configure::write('debug',0);
    	$this->render($vista);
    	
    }
    
    
     
    
}
?>