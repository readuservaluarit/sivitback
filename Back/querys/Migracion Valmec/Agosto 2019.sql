﻿/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;



/* Foreign Keys must be dropped in the target to ensure that requires changes can be done*/




/* Alter table in target */
ALTER TABLE `area` 
	CHANGE `d_area` `d_area` VARCHAR(300)  COLLATE latin1_swedish_ci NULL AFTER `id` , 
	ADD COLUMN `abreviado_d_area` VARCHAR(30)  COLLATE latin1_swedish_ci NULL AFTER `d_area` ;

/* Alter table in target */
ALTER TABLE `cake_sessions` 
	ADD COLUMN `principal` INT(11)   NULL AFTER `id_usuario` ;

/* Alter table in target */
ALTER TABLE `categoria` 
	ADD COLUMN `id_meli` VARCHAR(20)  COLLATE latin1_swedish_ci NULL AFTER `importable_desde_certificado_calidad` ;


/* Alter table in target */
ALTER TABLE `comprobante` 
	ADD COLUMN `meli_order_id` BIGINT(20)   NULL AFTER `mail_enviado` , 
	ADD COLUMN `entrega_domicilio` INT(4)   NULL AFTER `meli_order_id` , 
	ADD COLUMN `id_persona_secundaria` INT(11)   NULL AFTER `entrega_domicilio` , 
	ADD COLUMN `meli_shippment_id` INT(11)   NULL AFTER `id_persona_secundaria` , 
	ADD COLUMN `meli_shippment_cost` DECIMAL(10,2)   NULL AFTER `meli_shippment_id` , 
	ADD COLUMN `imprimir_obs_cuerpo` TINYINT(4)   NULL AFTER `meli_shippment_cost` ;

/* Alter table in target */
ALTER TABLE `comprobante_item` 
	ADD UNIQUE KEY `id_otro_sistema`(`id_otro_sistema`) ;



/* Alter table in target */
ALTER TABLE `comprobante_punto_venta_numero` 
	ADD COLUMN `id_estado_comprobante_defecto_sin_uso` INT(11)   NULL AFTER `id_impuesto` , 
	ADD KEY `id_estado_comprobante_defecto`(`id_estado_comprobante_defecto_sin_uso`) ;

/* Alter table in target */
ALTER TABLE `condicion_pago` 
	DROP KEY `codigo` ;


/* Alter table in target */
ALTER TABLE `dato_empresa` 
	ADD COLUMN `mercadolibre_username` VARCHAR(50)  COLLATE latin1_swedish_ci NULL AFTER `email_intentos_fallidos` , 
	ADD COLUMN `mercadolibre_access_token` VARCHAR(200)  COLLATE latin1_swedish_ci NULL AFTER `mercadolibre_username` , 
	ADD COLUMN `mercadolibre_user_id` BIGINT(20)   NULL AFTER `mercadolibre_access_token` , 
	ADD COLUMN `mercadolibre_access_token_expire_seconds` BIGINT(20)   NULL AFTER `mercadolibre_user_id` , 
	ADD COLUMN `mercadolibre_refresh_token` VARCHAR(200)  COLLATE latin1_swedish_ci NULL AFTER `mercadolibre_access_token_expire_seconds` , 
	ADD COLUMN `mercadolibre_id_punto_venta_defecto_pedido` INT(11)   NULL AFTER `mercadolibre_refresh_token` , 
	ADD COLUMN `mercadolibre_id_lista_precio` INT(11)   NULL AFTER `mercadolibre_id_punto_venta_defecto_pedido` , 
	ADD COLUMN `mercadolibre_conexion_activa` TINYINT(4)   NULL AFTER `mercadolibre_id_lista_precio` , 
	ADD COLUMN `mercadolibre_mensaje_post_venta` LONGTEXT  COLLATE latin1_swedish_ci NULL AFTER `mercadolibre_conexion_activa` , 
	ADD COLUMN `mercadolibre_enviar_mensaje_post_venta` TINYINT(4)   NULL AFTER `mercadolibre_mensaje_post_venta` , 
	ADD COLUMN `url_sivit` VARCHAR(400)  COLLATE latin1_swedish_ci NULL AFTER `mercadolibre_enviar_mensaje_post_venta` , 
	ADD COLUMN `sivit_clientes_conexion_activa` TINYINT(4)   NULL AFTER `url_sivit` , 
	ADD COLUMN `sivit_proveedores_conexion_activa` TINYINT(4)   NULL AFTER `sivit_clientes_conexion_activa` ;

/* Alter table in target */
ALTER TABLE `lote` 
	ADD COLUMN `tiene_certificado` TINYINT(4)   NULL AFTER `codigo` , 
	ADD COLUMN `validado` TINYINT(4)   NULL AFTER `tiene_certificado` ;

/* Alter table in target */
ALTER TABLE `mail_to_send` 
	ADD COLUMN `fecha` DATETIME   NULL AFTER `error_log` ;

/* Alter table in target */
ALTER TABLE `orden_armado` 
	DROP COLUMN `fecha_cumplida` ;

/* Alter table in target */
ALTER TABLE `orden_produccion` 
	CHANGE `observacion` `observacion` VARCHAR(800)  COLLATE latin1_swedish_ci NULL AFTER `fecha_cierre` ;

/* Create table in target */
CREATE TABLE `periodo`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_periodo` VARCHAR(80) COLLATE latin1_swedish_ci NULL  , 
	`fecha_rel_inicio` DATETIME NULL  , 
	`fecha_rel_fin` DATETIME NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Alter table in target */
ALTER TABLE `persona` 
	CHANGE `id_media_file` `id_media_file` INT(11)   NULL AFTER `id_forma_pago` , 
	CHANGE `id_condicion_pago` `id_condicion_pago` INT(11)   NULL AFTER `id_tipo_comercializacion_por_defecto` , 
	ADD COLUMN `meli_nickname` VARCHAR(50)  COLLATE latin1_swedish_ci NULL AFTER `id_condicion_pago` , 
	ADD COLUMN `meli_id_usuario` BIGINT(20)   NULL AFTER `meli_nickname` , 
	ADD COLUMN `lugar_entrega` VARCHAR(100)  COLLATE latin1_swedish_ci NULL AFTER `meli_id_usuario` , 
	ADD COLUMN `id_persona_origen` INT(11)   NULL AFTER `lugar_entrega` , 
	ADD COLUMN `fecha_antiguedad` DATETIME   NULL AFTER `id_persona_origen` , 
	ADD COLUMN `id_rrhh_convenio` INT(11)   NULL AFTER `fecha_antiguedad` , 
	ADD KEY `id_condicion_pago`(`id_condicion_pago`) , 
	ADD KEY `id_persona_origen`(`id_persona_origen`) ;



/* Create table in target */
CREATE TABLE `persona_origen`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_persona_origen` VARCHAR(50) COLLATE latin1_swedish_ci NOT NULL  , 
	`id_tipo_persona` INT(11) NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `id_tipo_persona`(`id_tipo_persona`) , 
	CONSTRAINT `persona_origen_ibfk_1` 
	FOREIGN KEY (`id_tipo_persona`) REFERENCES `tipo_persona` (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Alter table in target */
ALTER TABLE `producto` 
	ADD COLUMN `meli_id` VARCHAR(100)  COLLATE latin1_swedish_ci NULL AFTER `d_ubicacion` , 
	ADD COLUMN `meli_d_producto` VARCHAR(200)  COLLATE latin1_swedish_ci NULL AFTER `meli_id` , 
	ADD COLUMN `meli_url_foto1` VARCHAR(300)  COLLATE latin1_swedish_ci NULL AFTER `meli_d_producto` , 
	ADD COLUMN `meli_html_descripcion` LONGTEXT  COLLATE latin1_swedish_ci NULL AFTER `meli_url_foto1` , 
	ADD COLUMN `meli_url_producto` VARCHAR(300)  COLLATE latin1_swedish_ci NULL AFTER `meli_html_descripcion` , 
	CHANGE `id_periodo` `id_periodo` INT(11)   NULL AFTER `meli_url_producto` ;

/* Alter table in target */
ALTER TABLE `qa_certificado_calidad` 
	DROP KEY `id_orden_armado` ;

/* Create table in target */
CREATE TABLE `reporte_tablero`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_reporte_tablero` VARCHAR(100) COLLATE latin1_swedish_ci NULL  , 
	`activa` TINYINT(4) NULL  , 
	`sql_vista_resumen` LONGTEXT COLLATE latin1_swedish_ci NULL  , 
	`sql_vista_detalle` LONGTEXT COLLATE latin1_swedish_ci NULL  , 
	`d_mensaje_reporte_tablero` LONGTEXT COLLATE latin1_swedish_ci NULL  , 
	`color` VARCHAR(20) COLLATE latin1_swedish_ci NULL  , 
	`id_menu` VARCHAR(50) COLLATE latin1_swedish_ci NULL  , 
	`muestra_inicio` TINYINT(4) NULL  , 
	`muestra_fin` TINYINT(4) NULL  , 
	`tipo_reporte_tablero` INT(11) NULL  , 
	`html_widget` LONGTEXT COLLATE latin1_swedish_ci NULL  , 
	`js_widget` LONGTEXT COLLATE latin1_swedish_ci NULL  , 
	`orden` INT(11) NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `reporte_tablero_usuario`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`id_reporte_tablero` INT(11) NULL  , 
	`id_usuario` INT(11) NULL  , 
	`orden` INT(11) NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `request_log`(
	`controller` VARCHAR(50) COLLATE latin1_swedish_ci NULL  , 
	`id_usuario` INT(11) NULL  , 
	`action` VARCHAR(50) COLLATE latin1_swedish_ci NULL  , 
	`date` DATETIME NULL  
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_categoria`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_categoria` VARCHAR(100) COLLATE latin1_swedish_ci NULL  , 
	`id_rrhh_convenio` INT(11) NULL  , 
	`abreviado` VARCHAR(20) COLLATE latin1_swedish_ci NULL  , 
	`importe` DECIMAL(20,2) NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_concepto`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_concepto` VARCHAR(100) COLLATE latin1_swedish_ci NOT NULL  , 
	`codigo` INT(11) NOT NULL  , 
	`activo` TINYINT(4) NOT NULL  DEFAULT 1 , 
	`id_cuenta_contable` INT(11) NULL  , 
	`orden_impresion` INT(11) NULL  DEFAULT 1 , 
	`texto_mostrar` VARCHAR(50) COLLATE latin1_swedish_ci NULL  , 
	`id_rrhh_tipo_concepto` INT(11) NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_convenio`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_convenio` VARCHAR(100) COLLATE latin1_swedish_ci NOT NULL  , 
	`activo` TINYINT(4) NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_modelo_liquidacion`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_modelo_liquidacion` VARCHAR(100) COLLATE latin1_swedish_ci NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_modelo_liquidacion_concepto_item`(
	`id` BIGINT(20) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_modelo_liquidacion` INT(11) NOT NULL  , 
	`id_rrhh_concepto` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_periodo_liquidacion`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_periodo_liquidacion` VARCHAR(50) COLLATE latin1_swedish_ci NOT NULL  , 
	`id_rrhh_modelo_liquidacion` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_persona_concepto`(
	`id` BIGINT(20) NOT NULL  AUTO_INCREMENT , 
	`id_persona` INT(11) NOT NULL  , 
	`id_rrhh_concepto` INT(11) NOT NULL  , 
	`valor` DECIMAL(20,2) NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_tipo_concepto`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_tipo_concepto` VARCHAR(50) COLLATE latin1_swedish_ci NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_tipo_liquidacion`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_tipo_liquidacion` VARCHAR(50) COLLATE latin1_swedish_ci NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Alter table in target */
ALTER TABLE `tipo_comprobante` 
	CHANGE `letra` `letra` CHAR(1)  COLLATE latin1_swedish_ci NULL DEFAULT 'X' AFTER `id_deposito_destino` , 
	ADD KEY `id_cuenta_bancaria`(`id_cuenta_bancaria`) ;
ALTER TABLE `tipo_comprobante`
	ADD CONSTRAINT `tipo_comprobante_ibfk_6` 
	FOREIGN KEY (`id_cuenta_bancaria`) REFERENCES `cuenta_bancaria` (`id`) ;


/* Alter table in target */
ALTER TABLE `tipo_comprobante_punto_venta_numero` 
	ADD COLUMN `id_estado_comprobante_defecto` INT(11)   NULL AFTER `d_tipo_comprobante_punto_venta_numero` , 
	ADD KEY `id_estado_comprobante`(`id_estado_comprobante_defecto`) ;

/* Alter table in target */
ALTER TABLE `usuario` 
	ADD COLUMN `url_avatar` VARCHAR(200)  COLLATE latin1_swedish_ci NULL AFTER `hash_sesion` , 
	ADD COLUMN `id_persona` INT(11)   NULL AFTER `url_avatar` , 
	ADD KEY `id_persona`(`id_persona`) , 
	ADD UNIQUE KEY `username_unique`(`username`) ;
ALTER TABLE `usuario`
	ADD CONSTRAINT `usuario_ibfk_2` 
	FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id`) ;
	
	
	
	



/*  Alter View in target  */
DELIMITER $$
ALTER ALGORITHM=UNDEFINED DEFINER=`six`@`%` SQL SECURITY DEFINER VIEW `comprobante_item_view` AS (SELECT `comprobante_item`.`id` AS `id`,`comprobante_item`.`id_comprobante` AS `id_comprobante`,`comprobante_item`.`cantidad` AS `cantidad`,`comprobante_item`.`descuento_unitario` AS `descuento_unitario`,`comprobante_item`.`precio_unitario_bruto` AS `precio_unitario_bruto`,`comprobante_item`.`precio_unitario` AS `precio_unitario`,`comprobante_item`.`orden` AS `orden`,`comprobante_item`.`n_item` AS `n_item`,`comprobante_item`.`id_iva` AS `id_iva`,`comprobante_item`.`id_producto` AS `id_producto`,`comprobante_item`.`codigo_observacion` AS `codigo_observacion`,`comprobante_item`.`item_observacion` AS `item_observacion`,`comprobante_item`.`dias` AS `dias`,`comprobante_item`.`id_comprobante_item_origen` AS `id_comprobante_item_origen`,`comprobante_item`.`activo` AS `activo`,`comprobante_item`.`cantidad_cierre` AS `cantidad_cierre`,`comprobante_item`.`id_unidad` AS `id_unidad`,`comprobante_item`.`id_comprobante_origen` AS `id_comprobante_origen`,`comprobante_item`.`id_migracion` AS `id_migracion`,`comprobante_item`.`importe_descuento_unitario` AS `importe_descuento_unitario`,`comprobante_item`.`precio_unitario2` AS `precio_unitario2`,`comprobante_item`.`precio_unitario3` AS `precio_unitario3`,`comprobante_item`.`id_lista_precio` AS `id_lista_precio`,`comprobante_item`.`precio_minimo_producto` AS `precio_minimo_producto`,`comprobante_item`.`id_tipo_lote` AS `id_tipo_lote`,`comprobante_item`.`requiere_conformidad` AS `requiere_conformidad`,`tipo_comprobante`.`cantidad_ceros_decimal` AS `cantidad_ceros_decimal`,`comprobante_item`.`ajusta_diferencia_cambio` AS `ajusta_diferencia_cambio`,`comprobante_item`.`monto_diferencia_cambio` AS `monto_diferencia_cambio`,`comprobante_item`.`producto_codigo` AS `producto_codigo`,CAST((`comprobante`.`fecha_generacion` + INTERVAL IF(ISNULL(`comprobante_item`.`dias`),0,`comprobante_item`.`dias`) DAY) AS DATE) AS `fecha_entrega`,`comprobante_item`.`id_persona` AS `id_persona`,`comprobante_item`.`id_detalle_tipo_comprobante` AS `id_detalle_tipo_comprobante`,`comprobante_item`.`fecha_generacion` AS `fecha_generacion`,`comprobante_item`.`fecha_cierre` AS `fecha_cierre`,`comprobante_item`.`fecha_vencimiento` AS `fecha_vencimiento`,`comprobante_item`.`cantidad2` AS `cantidad2`,`comprobante_item`.`cantidad3` AS `cantidad3`,(`comprobante_item`.`precio_unitario` * `comprobante_item`.`cantidad`) AS `total`,(((`comprobante_item`.`precio_unitario` * `comprobante_item`.`cantidad`) * (1 + (`iva`.`d_iva` / 100))) - (`comprobante_item`.`precio_unitario` * `comprobante_item`.`cantidad`)) AS `total_iva`,`comprobante`.`lugar_entrega` AS `lugar_entrega`,`comprobante`.`entrega_domicilio` AS `entrega_domicilio`,`comprobante`.`id_transportista` AS `id_transportista` FROM (((`comprobante_item` JOIN `comprobante` ON((`comprobante`.`id` = `comprobante_item`.`id_comprobante`))) JOIN `tipo_comprobante` ON((`comprobante`.`id_tipo_comprobante` = `tipo_comprobante`.`id`))) LEFT JOIN `iva` ON((`comprobante_item`.`id_iva` = `iva`.`id`))))$$
DELIMITER ;


/*  Alter View in target  */
DELIMITER $$
ALTER ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `lista_precio_producto_view` AS (SELECT IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),`lpp_padre`.`id`,`lista_precio_producto`.`id`) AS `id`,`producto`.`id_producto_tipo` AS `id_producto_tipo`,`producto`.`id_producto_clasificacion` AS `id_producto_clasificacion`,`producto`.`id_origen` AS `id_origen`,`lista_precio`.`id` AS `id_lista_precio`,IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),`lpp_padre`.`id_producto`,`lista_precio_producto`.`id_producto`) AS `id_producto`,IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),`lpp_padre`.`id_moneda`,`lista_precio`.`id_moneda`) AS `id_moneda`,IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),(`lpp_padre`.`precio` * (1 + (`lista_precio`.`porcentaje_variacion` / 100))),`lista_precio_producto`.`precio`) AS `precio`,`lista_precio`.`id_tipo_lista_precio` AS `id_tipo_lista_precio`,IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),`lpp_padre`.`precio_minimo`,`lista_precio_producto`.`precio_minimo`) AS `precio_minimo`,IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),`lpp_padre`.`porcentaje_utilidad_minima`,`lista_precio_producto`.`porcentaje_utilidad_minima`) AS `porcentaje_utilidad_minima`,IF((`lista_precio`.`id_tipo_lista_precio` = 1),0,IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),(`producto`.`costo` * ((100 + `lpp_padre`.`porcentaje_utilidad_minima`) / 100)),(`producto`.`costo` * ((100 + `lista_precio_producto`.`porcentaje_utilidad_minima`) / 100)))) AS `precio_minimo_por_utilidad`,`producto`.`codigo` AS `codigo`,`producto`.`costo` AS `costo`,`producto`.`d_producto` AS `d_producto`,`iva`.`d_iva` AS `d_iva`,`moneda`.`simbolo` AS `moneda_simbolo`,`producto_tipo`.`id_destino_producto` AS `id_destino_producto`,ROUND((IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),(`lpp_padre`.`precio` * (1 + (`lista_precio`.`porcentaje_variacion` / 100))),`lista_precio_producto`.`precio`) * (1 + (`iva`.`d_iva` / 100))),4) AS `precio_final`,IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),`lpp_padre`.`precio`,0) AS `precio_base`,`lista_precio`.`d_lista_precio` AS `d_lista_precio` FROM ((((((`lista_precio` LEFT JOIN `lista_precio_producto` ON((`lista_precio`.`id` = `lista_precio_producto`.`id_lista_precio`))) LEFT JOIN `lista_precio_producto` `lpp_padre` ON((`lista_precio`.`id_lista_precio_padre` = `lpp_padre`.`id_lista_precio`))) JOIN `producto` ON((`producto`.`id` = IF((`lista_precio`.`id_lista_precio_padre` IS NOT NULL),`lpp_padre`.`id_producto`,`lista_precio_producto`.`id_producto`)))) JOIN `iva` ON((`iva`.`id` = `producto`.`id_iva`))) JOIN `moneda` ON((`moneda`.`id` = `lista_precio`.`id_moneda`))) JOIN `producto_tipo` ON((`producto_tipo`.`id` = `producto`.`id_producto_tipo`))) WHERE (`producto`.`activo` = 1))$$
DELIMITER ;


/*  Alter View in target  */
DELIMITER $$
ALTER ALGORITHM=UNDEFINED DEFINER=`six`@`%` SQL SECURITY DEFINER VIEW `persona_view` AS SELECT `persona`.`id` AS `id`,`persona`.`razon_social` AS `razon_social`,`persona`.`email` AS `email`,`persona`.`id_pais` AS `id_pais`,`persona`.`id_provincia` AS `id_provincia`,`persona`.`localidad` AS `localidad`,`persona`.`ciudad` AS `ciudad`,`persona`.`calle` AS `calle`,`persona`.`numero_calle` AS `numero_calle`,`persona`.`piso` AS `piso`,`persona`.`dpto` AS `dpto`,`persona`.`tel` AS `tel`,`persona`.`interno1` AS `interno1`,`persona`.`fax` AS `fax`,`persona`.`posicion` AS `posicion`,`persona`.`cuit` AS `cuit`,`persona`.`ib` AS `ib`,`persona`.`id_cuenta` AS `id_cuenta`,`persona`.`cod_postal` AS `cod_postal`,`persona`.`id_tipo_iva` AS `id_tipo_iva`,`persona`.`id_persona_figura` AS `id_persona_figura`,`persona`.`activo` AS `activo`,`persona`.`website` AS `website`,`persona`.`id_tipo_documento` AS `id_tipo_documento`,`persona`.`observaciones` AS `observaciones`,`persona`.`cuenta_corriente` AS `cuenta_corriente`,`persona`.`sis_sistema` AS `sis_sistema`,`persona`.`sis_acceso` AS `sis_acceso`,`persona`.`sis_usuario_web` AS `sis_usuario_web`,`persona`.`sis_password` AS `sis_password`,`persona`.`observacion_extra` AS `observacion_extra`,`persona`.`id_persona_categoria` AS `id_persona_categoria`,`persona`.`id_cuenta_contable` AS `id_cuenta_contable`,`persona`.`id_moneda` AS `id_moneda`,`persona`.`requiere_orden_compra` AS `requiere_orden_compra`,`persona`.`id_cuenta_contable_cuenta_corriente` AS `id_cuenta_contable_cuenta_corriente`,`persona`.`monto_autorizacion_orden_compra` AS `monto_autorizacion_orden_compra`,`persona`.`id_transportista` AS `id_transportista`,`persona`.`flete_paga_proveedor` AS `flete_paga_proveedor`,`persona`.`entrega_en_domicilio` AS `entrega_en_domicilio`,`persona`.`nombre_fantasia` AS `nombre_fantasia`,`persona`.`descuento` AS `descuento`,`persona`.`id_forma_pago` AS `id_forma_pago`,`persona`.`id_condicion_pago` AS `id_condicion_pago`,`persona`.`id_media_file` AS `id_media_file`,`persona`.`valoracion` AS `valoracion`,`persona`.`fecha_creacion` AS `fecha_creacion`,`persona`.`id_iva` AS `id_iva`,`persona`.`fecha_nacimiento` AS `fecha_nacimiento`,`persona`.`pasaporte` AS `pasaporte`,`persona`.`pasaporte_fecha_vto` AS `pasaporte_fecha_vto`,`persona`.`persona_extranjero` AS `persona_extranjero`,`persona`.`id_tipo_actividad` AS `id_tipo_actividad`,`persona`.`limite_credito` AS `limite_credito`,`persona`.`limiite_credito_documento` AS `limiite_credito_documento`,`persona`.`cuenta_corriente_deshabilitada` AS `cuenta_corriente_deshabilitada`,`persona`.`dias_limite_cuenta_corriente` AS `dias_limite_cuenta_corriente`,`persona`.`id_tipo_persona` AS `id_tipo_persona`,`persona`.`pasaporte_fecha_creacion` AS `pasaporte_fecha_creacion`,`persona`.`observacion` AS `observacion`,`persona`.`id_migracion` AS `id_migracion`,`persona`.`fecha_borrado` AS `fecha_borrado`,`persona`.`email2` AS `email2`,`persona`.`email3` AS `email3`,`persona`.`email4` AS `email4`,`persona`.`tel2` AS `tel2`,`persona`.`interno2` AS `interno2`,`persona`.`horario_contacto` AS `horario_contacto`,`persona`.`pasaporte_nacionalidad` AS `pasaporte_nacionalidad`,`persona`.`nombre` AS `nombre`,`persona`.`apellido` AS `apellido`,`persona`.`id_area` AS `id_area`,`persona`.`fecha_ingreso` AS `fecha_ingreso`,`persona`.`fecha_egreso` AS `fecha_egreso`,`persona`.`sexo` AS `sexo`,`persona`.`celular` AS `celular`,`persona`.`interno` AS `interno`,`persona`.`direccion_latitud` AS `direccion_latitud`,`persona`.`direccion_longitud` AS `direccion_longitud`,`persona`.`id_tipo_contrato` AS `id_tipo_contrato`,`persona`.`id_persona_padre` AS `id_persona_padre`,`persona`.`d_persona` AS `d_persona`,`persona`.`id_estado_persona` AS `id_estado_persona`,`persona`.`id_situacion_ib` AS `id_situacion_ib`,`persona`.`codigo` AS `codigo`,`persona`.`id_otro_sistema` AS `id_otro_sistema`,`persona`.`nro_pasaporte` AS `nro_pasaporte`,`persona`.`id_tipo_comercializacion_por_defecto` AS `id_tipo_comercializacion_por_defecto`,`pais`.`d_pais` AS `d_pais`,`provincia`.`d_provincia` AS `d_provincia`,`tipo_iva`.`d_tipo_iva` AS `d_tipo_iva`,`tipo_documento`.`d_tipo_documento` AS `d_tipo_documento`,`estado_persona`.`d_estado_persona` AS `d_estado_persona`,`cuenta_contable`.`d_cuenta_contable` AS `d_cuenta_contable`,`cc2`.`d_cuenta_contable` AS `d_cuenta_contable_cuenta_corriente`,`persona_categoria`.`d_persona_categoria` AS `d_persona_categoria`,IF(ISNULL(`persona`.`id_lista_precio`),(SELECT `lista_precio`.`id` FROM `lista_precio` WHERE (`lista_precio`.`por_defecto` = 1) LIMIT 1),`persona`.`id_lista_precio`) AS `id_lista_precio`,`persona`.`meli_nickname` AS `meli_nickname`,`persona`.`meli_id_usuario` AS `meli_id_usuario`,`persona`.`lugar_entrega` AS `lugar_entrega`,`usuario`.`id` AS `id_usuario`,`usuario`.`username` AS `sivit_usuario`,IFNULL(`usuario`.`activo`,1) AS `sivit_usuario_activo`,`persona`.`id_persona_origen` AS `id_persona_origen` FROM (((((((((`persona` LEFT JOIN `pais` ON((`pais`.`id` = `persona`.`id_pais`))) LEFT JOIN `provincia` ON((`provincia`.`id` = `persona`.`id_provincia`))) LEFT JOIN `tipo_iva` ON((`tipo_iva`.`id` = `persona`.`id_tipo_iva`))) LEFT JOIN `tipo_documento` ON((`tipo_documento`.`id` = `persona`.`id_tipo_documento`))) LEFT JOIN `estado_persona` ON((`estado_persona`.`id` = `persona`.`id_estado_persona`))) LEFT JOIN `cuenta_contable` ON((`cuenta_contable`.`id` = `persona`.`id_cuenta_contable`))) LEFT JOIN `cuenta_contable` `cc2` ON((`cc2`.`id` = `persona`.`id_cuenta_contable_cuenta_corriente`))) LEFT JOIN `persona_categoria` ON((`persona_categoria`.`id` = `persona`.`id_persona_categoria`))) LEFT JOIN `usuario` ON((`usuario`.`id_persona` = `persona`.`id`)))$$
DELIMITER ;

/*  Alter Procedure in target  */

DELIMITER $$
DROP PROCEDURE IF EXISTS `actualiza_codigo_asiento`$$
CREATE DEFINER=`six`@`%` PROCEDURE `actualiza_codigo_asiento`(IN id_ejercicio_contable_var INT
)
BEGIN
  DECLARE contador BIGINT DEFAULT 1;
  DECLARE id_asiento_var BIGINT;
-- Variable para controlar el fin del bucle
  DECLARE findelbucle INTEGER DEFAULT 0;
 
-- La SELECT que queremos
  DECLARE cursor_asiento CURSOR FOR 
    SELECT id FROM asiento WHERE id_ejercicio_contable =  id_ejercicio_contable_var ;
    
   
 
-- Cuando no existan mas datos findelbucle se pondra a 1
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET findelbucle=1;
 
  OPEN cursor_asiento;
  bucle: LOOP
    FETCH cursor_asiento INTO id_asiento_var;
    IF findelbucle = 1 THEN
       LEAVE bucle;
    END IF;
 
  UPDATE asiento SET codigo= contador WHERE id=id_asiento_var;
  
    SET contador=contador+1;
 
  END LOOP bucle;
 
  CLOSE cursor_asiento;
END$$
DELIMITER ;


/*  Alter Procedure in target  */

DELIMITER $$
DROP PROCEDURE IF EXISTS `inserta_registro_stock`$$
CREATE DEFINER=`full`@`%` PROCEDURE `inserta_registro_stock`()
BEGIN
	
	
      INSERT INTO stock_producto (id_producto) 
	SELECT id FROM producto WHERE id NOT IN (SELECT id_producto FROM stock_producto);
      
      INSERT INTO stock_componente (id_componente) 
	SELECT id FROM componente WHERE id NOT IN (SELECT id_componente FROM stock_componente);
	
     INSERT INTO stock_materia_prima (id_materia_prima) 
	SELECT id FROM materia_prima WHERE id NOT IN (SELECT id_materia_prima FROM stock_materia_prima);
    
    END$$
DELIMITER ;


/*  Alter Procedure in target  */

DELIMITER $$
DROP PROCEDURE IF EXISTS `pedido_remito_factura`$$
CREATE DEFINER=`six`@`%` PROCEDURE `pedido_remito_factura`()
BEGIN
 
 DECLARE v_finished INTEGER DEFAULT 0;
 DECLARE v_pedido_item VARCHAR(100) DEFAULT "";
 DECLARE v_item_pedido_id INTEGER DEFAULT 0;
 DECLARE v_nro_comprobante INTEGER DEFAULT 0;
 DECLARE v_fecha_generacion DATE;
 DECLARE v_codigo_producto VARCHAR(100);
 DECLARE v_cantidad_item INTEGER DEFAULT 0;
 DECLARE v_dias INTEGER DEFAULT 0;
 DECLARE v_precio DECIMAL;
 DECLARE v_razon_social VARCHAR (200);
 DECLARE v_n_item INTEGER DEFAULT 0;
 
 -- declare cursor for employee email
 DECLARE pedido_item_cursor CURSOR FOR 
 SELECT comprobante_item.id AS v_item_pedido_id ,comprobante.`nro_comprobante`,comprobante.`fecha_generacion`,producto.`codigo`,
comprobante_item.`cantidad`, comprobante_item.`dias`,comprobante_item.`precio_unitario`,persona.`razon_social`,
comprobante_item.`n_item`
 
 
  FROM comprobante_item
 JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`
 JOIN producto ON comprobante_item.`id_producto` = producto.id
 JOIN persona ON persona.id = comprobante.`id_persona`
 JOIN moneda ON moneda.id = comprobante.`id_moneda`
  WHERE id_tipo_comprobante= 202 AND  YEAR(comprobante.`fecha_generacion`) >= 2018
AND id_estado_comprobante <>2  AND comprobante_item.`activo` = 1;
 
 -- declare NOT FOUND handler
 DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET v_finished = 1;
 
 OPEN pedido_item_cursor;
 
 get_pedido_item: LOOP
 
 FETCH pedido_item_cursor INTO v_item_pedido_id;
 
 IF v_finished = 1 THEN 
 LEAVE get_pedido_item;
 END IF;
 
 
 END LOOP get_pedido_item;
 
 CLOSE pedido_item_cursor;
 
END$$
DELIMITER ;


/* Create Procedure in target  */

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `xplore`(IN_DB VARCHAR(100))
BEGIN 
DECLARE DB VARCHAR(100);
DECLARE NO_TABLES INT(10);
DECLARE NO_VIEWS INT(10);
DECLARE NO_FUNCTIONS INT(10);
DECLARE NO_PROCEDURES INT(10);
DECLARE NO_TRIGGERS INT(10);
DECLARE SUMMARY VARCHAR(200); 
SET DB=IN_DB;
 
DROP TEMPORARY TABLE IF EXISTS objects;
CREATE TEMPORARY TABLE objects
(
object_type VARCHAR(100),
object_name VARCHAR(100),
object_schema VARCHAR(100)
)
ENGINE=MYISAM; 
INSERT INTO objects
/* query for triggers */
(SELECT 'TRIGGER',TRIGGER_NAME ,TRIGGER_SCHEMA
FROM information_schema.triggers
WHERE ACTION_STATEMENT LIKE DB)
UNION
/* query for views*/
(SELECT 'VIEW', TABLE_NAME,TABLE_SCHEMA
FROM information_schema.tables
WHERE table_type='VIEW' AND TABLE_SCHEMA LIKE DB)
UNION
/* query for procedure*/
(SELECT 'PROCEDURE', SPECIFIC_NAME, ROUTINE_SCHEMA
FROM information_schema.routines
WHERE routine_type='PROCEDURE' AND ROUNTINE_DEFINITION LIKE DB)
UNION
/* query for function*/
(SELECT 'FUNCTION', SPECIFIC_NAME, ROUTINE_SCHEMA
FROM information_schema.routines
WHERE routine_type='FUNCTION' AND ROUNTINE_DEFINITION LIKE DB)
UNION
/* query for tables*/
(SELECT CONCAT(ENGINE,' TABLE'), TABLE_NAME, TABLE_SCHEMA
FROM information_schema.tables
WHERE table_type='BASE TABLE' AND TABLE_SCHEMA LIKE DB
GROUP BY ENGINE, TABLE_NAME);
 
/* show gathered information from temporary table */
SELECT object_name,object_type,object_schema
FROM objects;
 
/* Prepare and show summary */
SELECT object_schema AS `DATABASE`,
SUM(IF(object_type LIKE '%TABLE', 1, 0)) AS 'TABLES',
SUM(IF(object_type='VIEW', 1, 0)) AS 'VIEWS',
SUM(IF(object_type='TRIGGER', 1, 0)) AS 'TRIGGERS',
SUM(IF(object_type='FUNCTION', 1, 0)) AS 'FUNCTIONS',
SUM(IF(object_type='PROCEDURE', 1, 0)) AS 'PROCEDURES'
FROM objects
GROUP BY object_schema;
 
END$$
DELIMITER ;


/*  Alter Function in target  */

DELIMITER $$
DROP FUNCTION IF EXISTS `mask`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `mask`(unformatted_value BIGINT, format_string CHAR(32)) RETURNS CHAR(32) CHARSET latin1
    DETERMINISTIC
BEGIN
DECLARE input_len TINYINT;
DECLARE output_len TINYINT;
DECLARE temp_char CHAR;
SET input_len = LENGTH(unformatted_value);
SET output_len = LENGTH(format_string);
WHILE ( output_len > 0 ) DO
SET temp_char = SUBSTR(format_string, output_len, 1);
IF ( temp_char = '#' ) THEN
IF ( input_len > 0 ) THEN
SET format_string = INSERT(format_string, output_len, 1, SUBSTR(unformatted_value, input_len, 1));
SET input_len = input_len - 1;
ELSE
SET format_string = INSERT(format_string, output_len, 1, '0');
END IF;
END IF;
SET output_len = output_len - 1;
END WHILE;
RETURN format_string;
END$$
DELIMITER ;


/*  Create Function in target  */

DELIMITER $$
CREATE DEFINER=`six`@`%` FUNCTION `proper_case`(str VARCHAR(128)) RETURNS VARCHAR(128) CHARSET latin1
BEGIN
DECLARE n, pos INT DEFAULT 1;
DECLARE sub, proper VARCHAR(128) DEFAULT '';
IF LENGTH(TRIM(str)) > 0 THEN
    WHILE pos > 0 DO
        SET pos = LOCATE(' ',TRIM(str),n);
        IF pos = 0 THEN
            SET sub = LOWER(TRIM(SUBSTR(TRIM(str),n)));
        ELSE
            SET sub = LOWER(TRIM(SUBSTR(TRIM(str),n,pos-n)));
        END IF;
        SET proper = CONCAT_WS(' ', proper, CONCAT(UPPER(LEFT(sub,1)),SUBSTR(sub,2)));
        SET n = pos + 1;
    END WHILE;
END IF;
RETURN TRIM(proper);
END$$
DELIMITER ;


/* Create Trigger in target */

DELIMITER $$
CREATE
    /*!50017 DEFINER = 'six'@'%' */
    TRIGGER `borra_registro_stock` BEFORE DELETE ON `producto` 
    FOR EACH ROW BEGIN
    
    
    DELETE FROM stock_producto WHERE id=OLD.id;
    END;
$$
DELIMITER ;


/* Create Trigger in target */

DELIMITER $$
CREATE

    TRIGGER `crea_registro_stock_producto` AFTER INSERT ON `producto` 
    FOR EACH ROW BEGIN
    
	
	 INSERT INTO stock_producto (id,id_deposito) 
		SELECT NEW.id,id FROM deposito;
	
		
	
  
    END;
$$
DELIMITER ;


/* Create Trigger in target */

DELIMITER $$
CREATE
    /*!50017 DEFINER = 'six'@'%' */
    TRIGGER `deposito_registro_stock` AFTER INSERT ON `deposito` 
    FOR EACH ROW BEGIN
    
  
	
	
	
	INSERT INTO stock_producto (id,id_deposito)
	SELECT id,NEW.id FROM producto;
		
	
		
	
  
    END;
$$
DELIMITER ;


/* Alter Trigger in target */

DELIMITER $$
/*!50003 DROP TRIGGER *//*!50032 IF EXISTS*//*!50003 `tg_persona_check_descuento_insert`*/$$
CREATE
    /*!50017 DEFINER = 'six'@'%' */
    TRIGGER `tg_persona_check_descuento_insert` BEFORE INSERT ON `persona` 
    FOR EACH ROW BEGIN 
IF NEW.descuento > 100 THEN 
SET NEW.descuento = 0; 
END IF; 
END;
$$
DELIMITER ;


/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;



	
 INSERT INTO `modulo` (`id`, `d_modulo`, `habilitado`, `cantidad_decimales_formateo`) VALUES (203, 'SIVIT CLIENTES', '1', '2'); 
 INSERT INTO `modulo` (`id`, `d_modulo`, `habilitado`, `cantidad_decimales_formateo`) VALUES ('303', 'SIVIT PROVEEDORES', '1', '4'); 
 INSERT INTO `modulo` (`id`, `d_modulo`, `habilitado`) VALUES ('204', 'MELI', '1'); 



	INSERT INTO `modulo` (`id`, `d_modulo`, `habilitado`) VALUES ('201', 'B2C', '1'); 
 INSERT INTO `modulo` (`id`, `d_modulo`, `habilitado`) VALUES ('202', 'B2B', '1');
	


INSERT INTO `detalle_tipo_comprobante` (`id`, `d_detalle_tipo_comprobante`, `id_tipo_comprobante`, `descripcion_accion`) VALUES ('78', 'Pedido Interno', '202', 'Pedido Interno'); 
INSERT INTO `detalle_tipo_comprobante` (`id`, `d_detalle_tipo_comprobante`, `id_tipo_comprobante`, `descripcion_accion`) VALUES ('79', 'Pedido Web', '202', 'Pedido Web'); 
INSERT INTO `detalle_tipo_comprobante` (`id`, `d_detalle_tipo_comprobante`, `id_tipo_comprobante`, `descripcion_accion`) VALUES ('80', 'Pedido B2B', '202', 'Pedido B2B'); 
INSERT INTO detalle_tipo_comprobante (`id`, `d_detalle_tipo_comprobante`, `id_tipo_comprobante`, `descripcion_accion`) VALUES ('81', 'Pedido B2C', '202', 'Pedido B2C');



UPDATE comprobante SET id_detalle_tipo_comprobante = 78 WHERE id_tipo_comprobante=202;


/*Pasar ROL Sivit Clientes y Sivit Proveedores*/
/*Pasar entidad, tipo_funcion,funcion,estado_tipo_comprobante*/
 
 /*Sincronizar la tabla estado_comprobante y estado_tipo_comprobante */
 
 
 

UPDATE `tipo_comprobante_punto_venta_numero` SET `id_estado_comprobante_defecto` = '116' WHERE `id` = '-1'; 

UPDATE `tipo_comprobante_punto_venta_numero` SET `id_estado_comprobante_defecto` = '116' WHERE `id` = '0'; 

 UPDATE `tipo_comprobante_punto_venta_numero` SET `id_estado_comprobante_defecto` = '115' WHERE `id` = '2'; 

UPDATE `tipo_comprobante_punto_venta_numero` SET `id_estado_comprobante_defecto` = '116' WHERE `id` = '20'; 

UPDATE `tipo_comprobante_punto_venta_numero` SET `id_estado_comprobante_defecto` = '116' WHERE `id` = '1'; 

 INSERT INTO `detalle_tipo_comprobante` (`id_tipo_comprobante`) VALUES ('268'); 
 UPDATE `detalle_tipo_comprobante` SET `d_detalle_tipo_comprobante` = 'Plazo de entrega vencido' WHERE `id` = '82'; 
UPDATE `detalle_tipo_comprobante` SET `descripcion_accion` = 'Plazo de entrega vencido' WHERE `id` = '82'; 
 INSERT INTO `detalle_tipo_comprobante` (id,`d_detalle_tipo_comprobante`, `id_tipo_comprobante`) VALUES (83,'NO corresponde a pedido', '268'); 
INSERT INTO `detalle_tipo_comprobante` (id,`d_detalle_tipo_comprobante`, `id_tipo_comprobante`) VALUES (84,'Embalaje NO conforme', '268'); 
INSERT INTO `detalle_tipo_comprobante` (id,`d_detalle_tipo_comprobante`,id_tipo_comprobante) VALUES (85,'Documentación de calidad',268); 
INSERT INTO `detalle_tipo_comprobante` (id,`d_detalle_tipo_comprobante`, `id_tipo_comprobante`) VALUES (86,'Fallas en operacion - Pérdidas', '268'); 
 INSERT INTO `detalle_tipo_comprobante` (id,`d_detalle_tipo_comprobante`, `id_tipo_comprobante`) VALUES (87,'Roturas diversas', '268'); 

 
 
 INSERT INTO `modulo` (`id`, `d_modulo`, `habilitado`) VALUES ('2500', 'RMA', '1'); 
 
 
 


INSERT INTO comprobante_punto_venta_numero (`id`, `id_tipo_comprobante`, `id_punto_venta`, `numero_actual`, `observacion`, `id_contador`, `por_defecto`, `permite_modificar_nro_comprobante`, `codigo_legal_talonario`, `fecha_vencimiento_codigo_legal_talonario`, `id_tipo_comprobante_punto_venta_numero`, `id_impuesto`) VALUES (NULL, '268', '3', '1', '', NULL, '1', NULL, '', '2079-06-06', '1', NULL); 

UPDATE `tipo_comprobante` SET `id_menu` = 'mnuRmaVentaListado' WHERE `id` = '268'; 

UPDATE `rol` SET `descripcion` = 'SivitCliente' , `codigo` = 'SivitCliente' WHERE `id` = '2'; 
UPDATE `rol` SET `descripcion` = 'SivitProveedor' , `codigo` = 'SivitProveedor' WHERE `id` = '3'; 
UPDATE `rol` SET `descripcion` = 'SivitB2B' , `codigo` = 'SivitB2B' WHERE `id` = '4'; 



///////////////*TODO JUNTO*////////////

SET FOREIGN_KEY_CHECKS = 0;



UPDATE rol SET id = id+100 WHERE id>1;
DELETE FROM rol WHERE id= 1;
INSERT  INTO `rol`(`id`,`id_nivel_autorizado_informacion`,`descripcion`,`asignacion_geografica`,`codigo`,`solo_lectura`) VALUES (1,1,'ADMINISTRADOR',1,'ADMINISTRADOR',0),(2,1,'SivitCliente',1,'SivitCliente',1),(3,1,'SivitProveedor',1,'SivitProveedor',0),(4,1,'SivitB2B',1,'SivitB2B',0);

UPDATE usuario SET id_rol = id_rol+100 WHERE id_rol>1;
SET FOREIGN_KEY_CHECKS = 1;



///////////////*FIN TODO JUNTO*////////////

/*Parametrizar RMA*/

INSERT  INTO `tipo_comprobante`(`id`,`id_sistema`,`codigo_tipo_comprobante`,`d_tipo_comprobante`,`abreviado_tipo_comprobante`,`signo_comercial`,`signo_contable`,`Fl_Importe`,`Fl_Valor`,`Fl_Minuta`,`Fl_Envio`,`requiere_iva`,`clave_numeracion`,`d2_tipo_comprobante`,`afecta_stock`,`tipo_comportamiento_stock`,`codigo_afip`,`id_deposito_origen`,`id_deposito_destino`,`letra`,`id_tipo_movimiento`,`flag_cargar_codigo_en_item_descripcion`,`controller`,`genera_asiento`,`id_asiento_modelo`,`cambia_modelo_asiento_al_ingreso`,`edita_modelo_asiento_al_ingreso`,`muestra_asiento_al_ingreso`,`incluye_items_en_asiento`,`id_producto_default`,`requiere_contador`,`id_impuesto`,`permite_item_sin_id_producto`,`id_estado_asiento_default`,`impresion_observacion_extra`,`impresion_footer_left`,`impresion_footer_right`,`permite_multimoneda`,`back_color`,`id_generar_comprobante_simultaneo`,`permite_total_cero`,`permite_agregar_item_libre_importacion`,`codigo_agip`,`letra_arba`,`requiere_definicion_menu`,`id_menu`,`cantidad_ceros_decimal`,`plazo_entrega_dias`,`permite_modificar_nro_comprobante`,`permite_ingresar_d_punto_venta`,`id_persona_default`,`permite_ajuste_diferencia_cambio`,`contiene_comprobante_valor`,`envia_mail_al_conformar`,`adjunta_pdf_mail`,`email`,`html_mail`,`usa_mail_usuario_envio`,`id_cuenta_bancaria`) VALUES
(268,2,'RECV','Reclamo de Ventas','RCV',0,0,0,0,0,0,0,'.','Reclamos de Venta',0,0,NULL,NULL,NULL,'X',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,'F23 - ','',NULL,NULL,NULL,0,NULL,0,'',1,'mnuRmaVentaListado',NULL,30,0,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL);

 UPDATE tipo_comprobante SET afecta_stock = 3 WHERE id=105;


 INSERT INTO `detalle_tipo_comprobante` (`id_tipo_comprobante`) VALUES ('268'); 
 UPDATE `detalle_tipo_comprobante` SET `d_detalle_tipo_comprobante` = 'Plazo de entrega vencido' WHERE `id` = '82'; 
UPDATE `detalle_tipo_comprobante` SET `descripcion_accion` = 'Plazo de entrega vencido' WHERE `id` = '82'; 
 INSERT INTO `detalle_tipo_comprobante` (id,`d_detalle_tipo_comprobante`, `id_tipo_comprobante`) VALUES (83,'NO corresponde a pedido', '268'); 
INSERT INTO `detalle_tipo_comprobante` (id,`d_detalle_tipo_comprobante`, `id_tipo_comprobante`) VALUES (84,'Embalaje NO conforme', '268'); 
INSERT INTO `detalle_tipo_comprobante` (id,`d_detalle_tipo_comprobante`,id_tipo_comprobante) VALUES (85,'Documentación de calidad',268); 
INSERT INTO `detalle_tipo_comprobante` (id,`d_detalle_tipo_comprobante`, `id_tipo_comprobante`) VALUES (86,'Fallas en operacion - Pérdidas', '268'); 
 INSERT INTO `detalle_tipo_comprobante` (id,`d_detalle_tipo_comprobante`, `id_tipo_comprobante`) VALUES (87,'Roturas diversas', '268'); 
 
 
 UPDATE `tipo_comprobante` SET `id_menu` = 'mnuOrdenProduccion' WHERE id = 103;
 
 

 
 /*Pasar funciones, tipo_funcion, entidad,estado,estado_comprobante,estado_tipo_comprobante correr roles nuevamente*/

DELETE  FROM funcion_rol;

INSERT INTO funcion_rol (id_funcion,id_rol)
SELECT id,1 FROM funcion;

 
/*Agregar punto de venta para FCEA*/
UPDATE tipo_comprobante
SET id_cuenta_bancaria = 3
WHERE id IN( 
890,
893,
896);


update tipo_comprobante set envia_mail_al_conformar = 2 where id IN (207,208);
ALTER TABLE `comprobante` CHANGE `envia_mail` `chk_envia_mail` TINYINT(4) NULL; 

ALTER TABLE tipo_iva ADD COLUMN `asociado_comprobante_exportacion` TINYINT(4) NULL 
UPDATE `tipo_iva` SET `asociado_comprobante_exportacion` = '1' WHERE `id` = '17'; 
UPDATE `tipo_iva` SET `asociado_comprobante_exportacion` = '1' WHERE `id` = '14'; 
 
