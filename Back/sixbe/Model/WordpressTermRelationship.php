<?php

App::uses('WordpressAppModel', 'Wordpress.Model');

class WordpressTermRelationship extends AppModel {
    
    public $useDbConfig = 'wordpress_database';
    public $name = 'WordpressTermRelationship';
    public $primaryKey = 'object_id';
    public $useTable = 'six_wp_term_relationships';
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    public $belongsTo = array(
        'TermTaxonomy' => array(
            'className' => 'WordpressTermTaxonomy',
            'foreignKey' => 'term_taxonomy_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
?>