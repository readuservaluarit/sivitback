/* INVENTARIO valoracion de la materia prima que hay en stock*/
SELECT mp.codigo ,smp.`stock`,mp.`costo` AS valor_unitario, mp.`costo`*smp.`stock` AS valor_inventario FROM stock_materia_prima smp 
JOIN materia_prima mp ON mp.id = smp.`id`
WHERE smp.stock>0
ORDER BY stock DESC