<?php
		include(APP.'View'.DS.'StockArticulo'.DS.'json'.DS.'stock_articulo.ctp');
               
        // CONFIG  CON MODE DISPLAYED CELLS
		$model["codigo"]["show_grid"] = 1;
		$model["codigo"]["header_display"] = "C&oacute;digo";
		$model["codigo"]["minimun_width"] = "90"; //REstriccion mas de esto no se puede achicar
		$model["codigo"]["order"] = "1";
		$model["codigo"]["text_colour"] = "#000000";
		
		$model["d_producto"]["show_grid"] = 1;
		$model["d_producto"]["header_display"] = "Descripci&oacute;n";
		$model["d_producto"]["minimun_width"] = "90"; //REstriccion mas de esto no se puede achicar
		$model["d_producto"]["order"] = "0";
		$model["d_producto"]["text_colour"] = "#000000";

		$model["stock"]["show_grid"] = 1;
		$model["stock"]["header_display"] = "Cant. Stock";
		$model["stock"]["order"] = "2";
		$model["stock"]["text_colour"] = "#0000FF";

		$model["stock_minimo"]["show_grid"] = 1;
		$model["stock_minimo"]["header_display"] = "Stock M&iacute;nimo";
		$model["stock_minimo"]["order"] = "3";
		$model["stock_minimo"]["text_colour"] = "#000000";

		$model["distancia_porcentual"]["show_grid"] = 1;
		$model["distancia_porcentual"]["header_display"] = "Dist.%";
		$model["distancia_porcentual"]["order"] = "4";
		$model["distancia_porcentual"]["text_colour"] = "#000000";

		$model["d_unidad"]["show_grid"] = 1;
		$model["d_unidad"]["header_display"] = "Unidad";
		$model["d_unidad"]["order"] = "5";
		$model["d_unidad"]["text_colour"] = "#0000FF";

		$model["id_deposito"]["show_grid"] = 1;
		$model["id_deposito"]["header_display"] = "Nro. Dep&oacute;sito";
		$model["id_deposito"]["order"] = "6";
		$model["id_deposito"]["text_colour"] = "#FF0000";//RED

		$model["d_deposito"]["show_grid"] = 1;
		$model["d_deposito"]["header_display"] = "Dep&oacute;sito";
		$model["d_deposito"]["order"] = "7";
		$model["d_deposito"]["text_colour"] = "#F0000F";
        
		 $output = array(
					"status" =>EnumError::SUCCESS,
					"message" => "StockProducto",
					"content" => $model
				);
		
		  echo json_encode($output);
?>