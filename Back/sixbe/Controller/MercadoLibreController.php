<?php


class MercadoLibreController extends AppController {
    public $name = 'MercadoLibre';
    //public $model = 'Area';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    

    public function index() {/*la primera vez que tiene que unir el SIV con Mercadolibre usa esto*/
    	
    	
    	App::import('Vendor', 'meli', array('file' => 'Meli/meli.php'));
    	//App::import('Vendor', 'configApp', array('file' => 'Meli/configApp.php'));
    	
    	Configure::load('meliconfig');
    	
    	
    	$meli = new Meli(Configure::read('MeliConstants.appId'), Configure::read('MeliConstants.secretKey'));
    	
    	echo '<a href="' . $meli->getAuthUrl(Configure::read('MeliConstants.redirectURI'), Meli::$AUTH_URL[Configure::read('MeliConstants.siteId')]) . '">Login</a>';
    	
    	die();

    }
    
    
    
    
    
    public function getOrders(){
    	
    	
    	$this->sincronizeProducts();//sincronizo productos al traer nuevas ordenes ya que pueden haber dado de alta y no haber sincronizado
    	
    	Configure::load('meliconfig');
    	$this->loadModel(EnumModel::DatoEmpresa);
    	$this->loadModel(EnumModel::Melibre);
    	App::import('Vendor', 'meli', array('file' => 'Meli/meli.php'));
    	
    	
    	
    	$meli = new Meli(Configure::read('MeliConstants.appId'), Configure::read('MeliConstants.secretKey'));
    	
    	
    	
    	
    	
    	
    	$meli_data = $this->setDatosUsuario();
    	
    	
    	
    	
    	
    	
    	if(count($meli_data)>0){
    		
    		
    		
    	
    		
    		$dato_empresa =  $comprobante = $this->{EnumModel::DatoEmpresa}->find('first', array(
    				'conditions' => array(
    						'DatoEmpresa.id' => 1
    				) ,
    				'contain' => false
    		));
    		
    		
    		//https://api.mercadolibre.com/orders/search?seller=seller_id&order.status=paid&access_token={...}
    	
    		//obtengo solo las pagadas
    		$url =  $meli->make_path('orders/search/recent',array('seller'=>$meli_data["id_usuario"],'access_token'=>$meli_data["access_token"],'order.status'=>'paid'));
    		
    		
    		
    		
    		$data = $this->Melibre->getDataFromMeli($url);
    		

    		
    		
    		if(count($data["results"])>0){
    			
    			
    			foreach($data["results"] as $pedido_venta){
    				
    				
    			
    				
    			
    				
    				$this->saveOrder($pedido_venta,$dato_empresa["DatoEmpresa"]["mercadolibre_id_punto_venta_defecto_pedido"]);
    				
    				
    				
    			}
    			
    			
    		}
    		
    		$a;
    		
    	}else{//falla
    		
    		die();
    	}
    }
    
    
    
   
    
    public function setDatosUsuario($access_token="",$expires_in="",$refresh_token=""){
    	
    	$this->loadModel(EnumModel::Melibre);
    	
    	$access_token   = $this->getFromRequestOrSession('Meli.access_token');
    	$expires_in    = $this->getFromRequestOrSession('Meli.expires_in');
    	$refresh_token = $this->getFromRequestOrSession('Meli.refresh_token');
    	
    	return $this->{EnumModel::Melibre}->setDatosUsuario($access_token,$expires_in,$refresh_token);
    	
    	
    } 
    
   
    
    
    
    
    protected function saveOrder($pedido_venta,$id_punto_venta){
    	
    	//confirmed Estado inicial de un orden; a�n sin haber sido pagada.
    	//payment_required Es necesario que se confirme el pago de la orden necesita para mostrar la informaci�n del usuario
    	//payment_in_process Existe un pago relacionado con la orden, pero a�n no se acredit�
    	//partially_paid La orden tiene un pago asociado acreditado, pero no es suficiente.
    	//paid La orden tiene un pago asociado acreditado.
    	//cancelled Por alguna raz�n, la orden no se complet�
    	//invalid La orden fue invalidada porque proven�a de un comprador malicioso
    	
    	
    	
    	$this->loadModel(EnumModel::Comprobante);
    	$this->loadModel(EnumModel::Moneda);
    	$this->loadModel(EnumModel::Melibre);
    	$this->loadModel(EnumModel::Articulo);
    	$this->model = EnumModel::Comprobante;

    	
    	$id_orden = $pedido_venta["id"];//id orden de pedido Meli

    	$orden_meli  = $this->{EnumModel::Comprobante}->find('first', array(
    			'conditions' => array("OR"=>array(
    					'Comprobante.meli_order_id' => $id_orden)
    			) ,
    			'contain' => false
    	));
    	
    	
    	
    	if(!$orden_meli){//sino se grabo en la base lo grabo
    	
    			$moneda = new Moneda();
    			
    			$id_cliente = $this->Melibre->saveUser($pedido_venta["buyer"]);//devuelve el ID de cliente que hizo el pedido.
    			
    			
    			
    			/*Puede pasar que el comprador no me envie los datos de facturacion entonces NO le puedo facturar.
    			 * Si es la primera vez que me compra entonces obtengo los datos que me envia en BILLING INFO los paso por el webservice de afip y ahi le cargo los datos al usuario,siempre
    			 * y cuando haya cargado los datos de la factura.
    			 * Si me compra el USUARIO XXX y quiere que le facture a otro entonces tomo el billing info y uso el WS de AFIP y obtengo los datos, solo si es CUIT sirve.
    			 * 
    			 * 
    			 * */
    			$id_cliente_secundario = $this->Melibre->saveClienteSecundario($id_cliente,$pedido_venta["buyer"]);
    	
		    	$status   = $pedido_venta["status"];//estado del pedido 
		    	$currency_id = $pedido_venta["currency_id"];//moneda del pedido ARS,
		    	$items_pedido = $pedido_venta["order_items"];//items
		    	$fecha_generacion = $pedido_venta["date_created"];//items
		    	$date_closed = $pedido_venta["date_closed"];//Fecha de confirmaci�n de la orden
		    	$expiration_date = $pedido_venta["expiration_date"];//Fecha l�mite que tiene el usuario para calificar porque, luego de la misma, se vuelve visible el feedback, se emiten los pagos (si hubiese) y se crean los cargos.
		    	$date_last_updated = $pedido_venta["date_last_updated"];//items
		    	$total_comprobante = $pedido_venta["total_amount"];//items
		    	
		    	
		    	$cabecera_comprobante["Comprobante"]["id_persona"] = $id_cliente;
		    	if($id_cliente_secundario>0)
		    		$cabecera_comprobante["Comprobante"]["id_persona_secundaria"] = $id_cliente_secundario;
		    	
		    		
		    	$cabecera_comprobante["Comprobante"]["id_tipo_comprobante"] = EnumTipoComprobante::PedidoInterno;
		    	$cabecera_comprobante["Comprobante"]["fecha_generacion"] = $fecha_generacion;
		    	$cabecera_comprobante["Comprobante"]["id_moneda"] = $this->Melibre->getMoneySiv($currency_id);
		    	$cabecera_comprobante["Comprobante"]["id_estado_comprobante"] = EnumEstadoComprobante::Abierto;
		    	$cabecera_comprobante["Comprobante"]["valor_moneda"] = $moneda->getCotizacion(EnumMoneda::Peso);
		    	$cabecera_comprobante["Comprobante"]["valor_moneda2"] = 1;
		    	$cabecera_comprobante["Comprobante"]["nro_comprobante"] = $this->obetener_ultimo_numero_comprobante(EnumTipoComprobante::PedidoInterno,$id_punto_venta,5,"+");
		    	$cabecera_comprobante["Comprobante"]["activo"] = 1;
		    	$cabecera_comprobante["Comprobante"]["comprobante_genera_asiento"] = 0;
		    	$cabecera_comprobante["Comprobante"]["id_detalle_tipo_comprobante"] = EnumDetalleTipoComprobante::PedidosWeb;
		    	$cabecera_comprobante["Comprobante"]["definitivo"] = 0;
		    	$cabecera_comprobante["Comprobante"]["id_punto_venta"] = $id_punto_venta;
		    	$cabecera_comprobante["Comprobante"]["meli_order_id"] = $id_orden;
		    	$cabecera_comprobante["Comprobante"]["orden_compra_externa"] = $id_orden;
		    	
		    	
		    	
		    	
		    	
		    	if(isset($pedido_venta["shipping"])){
		    		
		    		$cabecera_comprobante["Comprobante"]["meli_shippment_id"] = $pedido_venta["shipping"]["id"];//se necesita para levantar el PDF del envio luego Metodo---> getUrlPDFEtiquetaEnvio
		    		$cabecera_comprobante["Comprobante"]["meli_shippment_cost"] = $pedido_venta["shipping"]["costo"];
		    		
		    	//	$status = $pedido_venta["shipping"]["status"]; //to_be_agreed (parece que es acuerdo con el vendedor)
		    		
		    		$this->Melibre->setEntregaDomicilio($cabecera_comprobante,$pedido_venta); 
		    		
		    		$costo_envio = $pedido_venta["cost"];//esto es facturable
		    		$shipping_mode = $pedido_venta["shipping_mode"];//modo de envio ME2, ME1,custom,not_specified,null https://developers.mercadolibre.com/es_ar/envio-de-productos
		    		
		    	}
		    	
		    	
		    	
		    	
		    	
		    	
		    	$array_items_pedido = array();
		    	
		    	$total_comprobante = 0;
		    	foreach($items_pedido as $key=>$item){
		    		
		    		$id_meli_product = $item["item"]["id"];
		    		
		    		$producto = $this->Articulo->getProductoMeli($id_meli_product);//lo tengo en la base porque sincronize antes 
		    		
		    		
		    		if(count($producto) == 0){
		    		
		    			$d_iva = 21;
		    			$id_iva = EnumIva::veintiuno;
		    		}else{
		    			
		    			$item_comprobante["ComprobanteItem"]["id_producto"] = $producto["Articulo"]["id"];
		    			$d_iva = $producto["Iva"]["d_iva"];
		    			$id_iva = $producto["Articulo"]["id_iva"];
		    			
		    		}

		    		$precio_mercado_libre = $item["full_unit_price"];
		    		
		    		$item_comprobante["ComprobanteItem"]["cantidad"] = $item["quantity"];
		    		$item_comprobante["ComprobanteItem"]["precio_unitario_bruto"] = round($precio_mercado_libre/(1+$d_iva/100),2);
		    		$item_comprobante["ComprobanteItem"]["precio_unitario"] = round($precio_mercado_libre/(1+$d_iva/100),2);
		    		$item_comprobante["ComprobanteItem"]["orden"] = $key+1;
		    		$item_comprobante["ComprobanteItem"]["n_item"] = $key+1;
		    		$item_comprobante["ComprobanteItem"]["id_iva"] = $id_iva;
		    		$item_comprobante["ComprobanteItem"]["item_observacion"] = $item["item"]["title"];
		    		$item_comprobante["ComprobanteItem"]["activo"] = 1;
		    		$item_comprobante["ComprobanteItem"]["id_unidad"] = EnumUnidad::U;
		    		array_push($array_items_pedido, $item_comprobante);
		    		$total_comprobante += $item_comprobante["ComprobanteItem"]["cantidad"]*$item_comprobante["ComprobanteItem"]["precio_unitario"];
		    		
		    	}
		    	
		    	
		    	$cabecera_comprobante["Comprobante"]["total_comprobante"] = $total_comprobante;
		    	
		    	
		    	$id_pedido_interno_web = $this->Comprobante->InsertaComprobante($cabecera_comprobante,$array_items_pedido);
		    	
		    	$this->Melibre->EnviarMensajeComprador($comprobante);
		    	
		    	
		    	//$contador = $this->chequeoContador(EnumTipoComprobante::PedidoInterno,$id_punto_venta);
		    	
		    	/*if($contador){
		    		
		    		
		    	
		    		
		    		if($id_nota > 0){
		    			$error = EnumError::SUCCESS;
		    			$message = "El pedido fue creado correctamente.";
		    			return $id_pedido_interno_web;
		    			
		    		}else{
		    			
		    			$error = EnumError::ERROR;
		    			$message = "Ocurrio un error, la Nota no pudo ser creada";
		    			return -1;
		    		}
		    		
		    		
		    		
		    		
		    		
		    	}else{
		    		
		    		$error = EnumError::ERROR;
		    		$message= "Se debe definir el numero inicial del contador para este comprobante (Pedido interno).Anule este Pedido y vuelva a generarlo";
		    		return -1;
		    	}                   
		    	
		    	
		    	*/
		    	
		    	
    	
    	
    	}
    	
    	
    	
    }
    
    
    
   
    
    
    public function sincronizeProducts(){
    	
    	
    	/*Atributos de Productos
    	 * BRAND: Marca
    	 * MPN: N�mero de Parte del Fabricante
    	 * GTIN: N�meros Mundial del Art�culo Comercial [Global Trade Item Number] para el Art�culo.
    	 * ISBN: International Standard Book Number. Es un identificador �nico para libros, previsto para uso comercial. Engloba los (ISBN-10, ISBN-13) y guardar� todos los PI en este campo, concatenando los valores.
    	 *
    	 * */
    	
    	Configure::load('meliconfig');
    	$this->loadModel(EnumModel::DatoEmpresa);
    	$this->loadModel(EnumModel::Melibre);
    	App::import('Vendor', 'meli', array('file' => 'Meli/meli.php'));
    	
    	
    	
    	$meli = new Meli(Configure::read('MeliConstants.appId'), Configure::read('MeliConstants.secretKey'));
    	
    	
    	
    	
    	
    	$meli_data = $this->setDatosUsuario();
    	
    	
    	
    	
    	
    	
    	if(count($meli_data)>0){
    		
    		
    		
    		
    		$dato_empresa =  $comprobante = $this->{EnumModel::DatoEmpresa}->find('first', array(
    				'conditions' => array(
    						'DatoEmpresa.id' => 1
    				) ,
    				'contain' => false
    		));
    		
    		
    		//You should provide an id or a list of ids. e.g. /items/MLA1 or /items?ids=MLA1,MLA2,MLA3
    		
    		
    		//https://api.mercadolibre.com/orders/search?seller=seller_id&order.status=paid&access_token={...}
    		
    		//obtengo solo las pagadas
    		
    		///users/{Cust_id}/items/search?access_token=$ACCESS_TOK
    		$url =  $meli->make_path('users/'.$meli_data["id_usuario"].'/items/search',array('access_token'=>$meli_data["access_token"]));
    		

    		
    		
    		
    		$data = $this->Melibre->getDataFromMeli($url);    		//esta paginado y hay que ir consumiendo
    		
    		if(isset($data["results"]) && count(($data["results"]))>0){
    			
    			$total_productos = $data["paging"]["total"];
    			$limite_por_pagina = $data["paging"]["limit"];
    			$cantidad_paginas = ceil($total_productos/$limite_por_pagina);
    			
    			for($i=1;$i<=$cantidad_paginas;$i++){
    				
    				
    				if($i!=1){//sino es la primera pagina entonces pido la query
    					
    					$url =  $meli->make_path('users/'.$meli_data["id_usuario"].'/items/search',array('access_token'=>$meli_data["access_token"],'offset'=>$limite_por_pagina*$i));
    					$data = $this->Melibre->getDataFromMeli($url);   
    					
    				}
    				
    				
    				foreach($data["results"] as $producto){
    					
    					$producto;//id de ML
    					$url =  $meli->make_path('items/'.$producto);
    					$producto_ml = $this->Melibre->getDataFromMeli($url);
    				
    					$this->SaveMeliProduct($meli,$producto_ml,$dato_empresa["DatoEmpresa"]["mercadolibre_id_lista_precio"]);
    					
    					
    					
    				}
    				
    				
    			}
    			
    			
    			
    			
    			
    		}
    		
    	
    	}
    
    }
    
    protected function SaveMeliProduct($meli,$producto_ml,$mercadolibre_id_lista_precio){
    	
    	
    	$this->loadModel(EnumModel::Producto);
    	$this->loadModel(EnumModel::ListaPrecio);
    	$this->loadModel(EnumModel::ListaPrecioProducto);
    	$this->loadModel(EnumModel::Moneda);
    	$this->loadModel(EnumModel::Melibre);
    	$id = $producto_ml["id"];
    	
    	
    	
    	$producto =  $this->{EnumModel::Producto}->find('first', array(
    			'conditions' => array(
    					'Producto.meli_id' => $id
    			) ,
    			'contain' => false
    	));
    	
    
    	
    	
    	
    	if(!$producto){
    		
    		
	    	$titulo = $producto_ml["title"];
	    	$precio = $producto_ml["price"];
	    	$attributes = $producto_ml["attributes"];
	    	
	    	
	    	
	    	$categoria = $producto_ml["category_id"];
	    	$id_categoria = $this->Melibre->SaveMeliCategory($meli,$categoria);
	    	$id_marca = $this->Melibre->SaveMeliAttributes($attributes);
	    	
	    	$producto["Producto"]["id_producto_tipo"] = EnumProductoTipo::MRETAIL;
	    	$producto["Producto"]["id_producto_clasificacion"] = EnumProductoClasificacion::Articulo_REAL;
	    	$producto["Producto"]["id_categoria"] = $id_categoria;
	    	$producto["Producto"]["id_marca"] = $id_marca;
	    	$producto["Producto"]["d_producto"] = $titulo;
	    	$producto["Producto"]["meli_d_producto"] = $titulo;
	    	$producto["Producto"]["meli_id"] = $id;
	    	$producto["Producto"]["codigo"] = $id;
	    	$producto["Producto"]["id_iva"] = EnumIva::veintiuno;
	    	$producto["Producto"]["fecha_alta"] = date("Y-m-d H:i:s");
	    	$producto["Producto"]["meli_url_producto"] = $producto_ml["permalink"];
	    	$producto["Producto"]["meli_url_foto1"] = $producto_ml["thumbnail"];
	    	
	    	$this->{EnumModel::Producto}->saveAll($producto);
	    	
	    	$id_producto = $this->{EnumModel::Producto}->id;
	    	
	    	if($mercadolibre_id_lista_precio>0){//si se definio la lista de precio para ML entonces doy de alta el producto ahi
	    		
	    		$lista_precio =   $this->{EnumModel::ListaPrecio}->find('first', array(
	    				'conditions' => array(
	    						'ListaPrecio.id' => $mercadolibre_id_lista_precio
	    				) ,
	    				'contain' => false
	    		));
	    		
	    		$id_moneda_producto_siv = $this->Melibre->getMoneySiv($producto_ml["currency_id"]);//en Mercado libre argentina solo publica en PESOS
	    		
	    		if($lista_precio["ListaPrecio"]["id_moneda"] != $id_moneda_producto_siv){
	    			
	    			
	    			if($lista_precio["ListaPrecio"]["id_moneda"] == EnumMoneda::Dolar){//si la lista esta en dolares
	    				$cotizacion = $this->{EnumModel::Moneda}->getCotizacion(EnumMoneda::Peso);
	    				$lista_precio_producto["ListaPrecioProducto"]["precio"] = round($precio/$cotizacion,4);
	    				
	    			}elseif($lista_precio["ListaPrecio"]["id_moneda"] == EnumMoneda::Peso){//aca no deberia entrar porque MLA etsa en pesos argentino
	    				$lista_precio_producto["ListaPrecioProducto"]["precio"] = $precio;
	    				
	    			}
	    			
	    			
	    			
	    		}else{
	    			
	    			
	    			$lista_precio_producto["precio"] = $precio;
	    		}
	    		
	    		
	    		$lista_precio_producto["ListaPrecioProducto"]["id_producto"] = $id_producto;
	    		$lista_precio_producto["ListaPrecioProducto"]["id_lista_precio"] = $mercadolibre_id_lista_precio;
	    		
	    		$this->{EnumModel::ListaPrecioProducto}->saveAll($lista_precio_producto);
	    		
	    		$this->getItemDescription($id);//actualizo la descripcion de la base porque es otro webservice aparte
	    		
	    		
	    	}
	    	
    	}else{
    		
    		return $producto["Producto"]["id"];
    	}
    	
    	
    	
    	
    }
    
    
    public function getItemDescription($meli_id){
    	
    	
    	Configure::load('meliconfig');
    	$this->loadModel(EnumModel::DatoEmpresa);
    	$this->loadModel(EnumModel::Melibre);
    	$this->loadModel(EnumModel::Producto);
    	App::import('Vendor', 'meli', array('file' => 'Meli/meli.php'));
    	
    	
    	
    	$meli = new Meli(Configure::read('MeliConstants.appId'), Configure::read('MeliConstants.secretKey'));
    	
    	
    	
    	
    	
    	$meli_data = $this->setDatosUsuario();
    	
    	
    	
    	
    	
    	
    	if(count($meli_data)>0){
    		
    		$url =  $meli->make_path('items/'.$meli_id.'/description');
    		$producto_ml = $this->Melibre->getDataFromMeli($url);
    		//$producto["meli_html_descripcion"] = $producto_ml["plain_text"];
    		//$producto["meli_d_producto"] = $producto_ml["text"];
    		
    		
    		
    		$this->{EnumModel::Producto}->updateAll(
    				array('Producto.meli_html_descripcion' => "'".$producto_ml["plain_text"]."'"  ),
    				array('Producto.meli_id' => $meli_id) );   
    		
    		
    		
    		
    	}
    	
    	
    	
    }
    
    public function getUrlPDFEtiquetaEnvio($id_meli_shipment){/*retorna un PDF*/
    	
    	
    	$meli_data = $this->setDatosUsuario();
    	
    	
    	//https://api.mercadolibre.com/shipment_labels?shipment_ids=27687723852&savePdf=Y&access_token=APP_USR-7653972342010988-091416-8c89b1f7047e2114f1533445c5bc05a3-73746432
    	
    	//https://api.mercadolibre.com/shipment_labels/?shipment_ids=27687723852&savePdf=Y&access_token=APP_USR-7653972342010988-091416-8c89b1f7047e2114f1533445c5bc05a3-73746432
    	if(count($meli_data)>0){
    		
    		$meli = new Meli(Configure::read('MeliConstants.appId'), Configure::read('MeliConstants.secretKey'));
    		
    		$url =  $meli->make_path('shipment_labels',array('shipment_ids'=>$id_meli_shipment,'savePdf'=>'Y','access_token'=>$meli_data["access_token"]));
    		
    		echo  $url;
    	
    	die();

    	
    	}
    	
    	
    	
    }
    
    
    
    

}
?>