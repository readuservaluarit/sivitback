INSERT INTO valuarit_enerbom_test.`persona` (id,razon_social,cuit,calle,numero_calle,piso,dpto,cod_postal,localidad,id_provincia,id_pais,id_tipo_iva,id_estado_persona,id_tipo_persona,codigo)

SELECT id,razon_social,cuit,calle,numero_calle,piso,depto,cod_postal,localidad,id_provincia,id_pais,id_tipo_iva,1 AS id_estado_persona,1 AS id_tipo_persona,matpers_id.PERSONA_COD
 FROM persona_gp
JOIN matpers_id ON matpers_id.PERSONA_ID = persona_gp.id

WHERE matpers_id.persona_tipo = 'C' AND cuit > 0 
AND id NOT IN (


/*Estos son clientes repetidos el SIV no los admite*/
SELECT id FROM persona_gp
JOIN matpers_id ON matpers_id.PERSONA_ID = persona_gp.id

WHERE matpers_id.persona_tipo = 'C' AND cuit > 0 
GROUP BY cuit HAVING COUNT(*)>1
) AND id_provincia >0