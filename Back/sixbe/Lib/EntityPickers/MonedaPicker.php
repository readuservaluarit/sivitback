<?php
class MonedaPicker extends EntityPicker{
    
    public $model = "Moneda";
    
    public $pickerTitle = "Selección de la Moneda con la que trabajará el sistema";
    
    
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "5%", "keyOrder" => "id"),
                                  array("title" => "d_moneda", "field" => "d_moneda", "width" => "30%", "keyOrder" => "d_moneda"),
                             
                                );

    public function getPickerConditions($busqueda){
        
        return array('Moneda.d_moneda LIKE' => '%' . $busqueda['bus_unico'] . '%');
    }

}