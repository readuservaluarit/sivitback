<?php		




        $model["es_mayor"]["show_grid"] = 0;
        $model["es_mayor"]["type"] = EnumTipoDato::entero;
        
        $model["d_codigo_cuenta_contable"]["show_grid"] = 0;
        $model["d_codigo_cuenta_contable"]["type"] = EnumTipoDato::cadena;
        
                
        $model["niveles"]["show_grid"] = 0;
        $model["niveles"]["type"] = EnumTipoDato::cadena;
        
         
        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "C&oacute;digo";
        $model["codigo"]["type"] = EnumTipoDato::cadena;
        $model["codigo"]["width"] = "15"; //REstriccion mas de esto no se puede achicar
        $model["codigo"]["order"] = "0";
        $model["codigo"]["text_colour"] = "#000000";
            
        $model["d_cuenta_contable"]["show_grid"] = 1;
        $model["d_cuenta_contable"]["type"] = EnumTipoDato::cadena;
        $model["d_cuenta_contable"]["header_display"] = "Cuenta Contable";
        $model["d_cuenta_contable"]["width"] = "20"; //REstriccion mas de esto no se puede achicar
        $model["d_cuenta_contable"]["order"] = "1";
        $model["d_cuenta_contable"]["text_colour"] = "#000000";
        
        
        $model["d_es_mayor"]["show_grid"] = 1;
        $model["d_es_mayor"]["type"] = EnumTipoDato::cadena;
        $model["d_es_mayor"]["header_display"] = "Mayor?";
        $model["d_es_mayor"]["width"] = "5"; //REstriccion mas de esto no se puede achicar
        $model["d_es_mayor"]["order"] = "2";
        $model["d_es_mayor"]["text_colour"] = "#000000";

		$model["debe"]["show_grid"] = 1;
        $model["debe"]["type"] = EnumTipoDato::decimal_flotante;
        $model["debe"]["header_display"] = "Debe";
        $model["debe"]["width"] = "15"; //Restriccion mas de esto no se puede achicar
        $model["debe"]["order"] = "3";
        $model["debe"]["text_colour"] = "#0000FF";
		
		$model["haber"]["show_grid"] = 1;
        $model["haber"]["type"] = EnumTipoDato::decimal_flotante;
        $model["haber"]["header_display"] = "Haber";
        $model["haber"]["width"] = "15"; //Restriccion mas de esto no se puede achicar
        $model["haber"]["order"] = "4";
        $model["haber"]["text_colour"] = "#0000FF";
		
		$model["saldo"]["show_grid"] = 1;
        $model["saldo"]["type"] = EnumTipoDato::decimal_flotante;
        $model["saldo"]["header_display"] = "Saldo";
        $model["saldo"]["width"] = "15"; //Restriccion mas de esto no se puede achicar
        $model["saldo"]["order"] = "5";
        $model["saldo"]["text_colour"] = "#0000FF";
		
		
		
		

         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "CuentaContable",
                    "content" => $model
                );
        
          echo json_encode($output);
?>