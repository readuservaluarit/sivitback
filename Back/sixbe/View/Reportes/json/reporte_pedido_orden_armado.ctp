<?php


            $model["nro_comprobante"]["show_grid"] = 1;
            $model["nro_comprobante"]["header_display"] = "Nro Pedido";
            $model["nro_comprobante"]["order"] = "1";
            $model["nro_comprobante"]["width"] = "15";
            
            
             $model["estado_pedido"]["show_grid"] = 1;
            $model["estado_pedido"]["header_display"] = "Estado Pedido";
            $model["estado_pedido"]["order"] = "2";
            $model["estado_pedido"]["width"] = "15";
            
            
            
            $model["razon_social"]["show_grid"] = 1;
            $model["razon_social"]["header_display"] = "Cliente";
            $model["razon_social"]["order"] = "3";
            $model["razon_social"]["width"] = "15";
            
            
            
            $model["persona_codigo"]["show_grid"] = 1;
            $model["persona_codigo"]["header_display"] = "C&oacute;digo Cliente";
            $model["persona_codigo"]["order"] = "4";
            $model["persona_codigo"]["width"] = "5";
            
            
             
            $model["persona_codigo"]["show_grid"] = 1;
            $model["persona_codigo"]["header_display"] = "C&oacute;digo Cliente";
            $model["persona_codigo"]["order"] = "5";
            $model["persona_codigo"]["width"] = "5";
            
            
            
            $model["fecha_generacion_pedido"]["show_grid"] = 1;
            $model["fecha_generacion_pedido"]["header_display"] = "Fecha Gen. Pedido";
            $model["fecha_generacion_pedido"]["order"] = "6";
            $model["fecha_generacion_pedido"]["width"] = "10";
            
            
            $model["fecha_entrega_global_pedido"]["show_grid"] = 1;
            $model["fecha_entrega_global_pedido"]["header_display"] = "Fecha Global. Pedido";
            $model["fecha_entrega_global_pedido"]["order"] = "15";
            $model["fecha_entrega_global_pedido"]["width"] = "10";
            
            
            
            
            
            $model["fecha_entrega_item"]["show_grid"] = 1;
            $model["fecha_entrega_item"]["header_display"] = "Fecha Entrega Item";
            $model["fecha_entrega_item"]["order"] = "16";
            $model["fecha_entrega_item"]["width"] = "10";
            
 			
 			$model["dias"]["show_grid"] = 1;
            $model["dias"]["header_display"] = "Dias.Entrega Item";
            $model["dias"]["order"] = "20";
            $model["dias"]["width"] = "5";
            
            
            $model["dias_pi"]["show_grid"] = 1;
            $model["dias_pi"]["header_display"] = "Dias.Entrega Pedido";
            $model["dias_pi"]["order"] = "25";
            $model["dias_pi"]["width"] = "5";
            
            
            $model["codigo"]["show_grid"] = 1;
            $model["codigo"]["header_display"] = "C&oacute;digo";
            $model["codigo"]["order"] = "30";
            $model["codigo"]["width"] = "12";
            
            
            $model["d_producto"]["show_grid"] = 1;
            $model["d_producto"]["header_display"] = "Desc. Producto";
            $model["d_producto"]["order"] = "35";
            $model["d_producto"]["width"] = "30";
            
            
            
            $model["cantidad"]["show_grid"] = 1;
            $model["cantidad"]["header_display"] = "Cant Ped.";
            $model["cantidad"]["order"] = "36";
            $model["cantidad"]["width"] = "30";
            
            
            
            
            
            $model["observacion_pedido_interno"]["show_grid"] = 1;
            $model["observacion_pedido_interno"]["header_display"] = "Obs Item Pedido";
            $model["observacion_pedido_interno"]["order"] = "40";
            $model["observacion_pedido_interno"]["width"] = "30";
            
            
            
             $model["observacion_pi"]["show_grid"] = 1;
            $model["observacion_pi"]["header_display"] = "Obs Pedido Cabecera";
            $model["observacion_pi"]["order"] = "41";
            $model["observacion_pi"]["width"] = "30";
            
            
            
            
            $model["inspeccion_pedido_interno"]["show_grid"] = 1;
            $model["inspeccion_pedido_interno"]["header_display"] = "Inspecci&oacute;n";
            $model["inspeccion_pedido_interno"]["order"] = "45";
            $model["inspeccion_pedido_interno"]["width"] = "5";
            
            
            
            $model["certificados_pedido_interno"]["show_grid"] = 1;
            $model["certificados_pedido_interno"]["header_display"] = "Certificados";
            $model["certificados_pedido_interno"]["order"] = "50";
            $model["certificados_pedido_interno"]["width"] = "5";
            
            
            
            $model["nro_orden_armado"]["show_grid"] = 1;
            $model["nro_orden_armado"]["header_display"] = "Nro. Orden Armado";
            $model["nro_orden_armado"]["order"] = "55";
            $model["nro_orden_armado"]["width"] = "10";
            
            
            
            $model["cantidad_orden_armado"]["show_grid"] = 1;
            $model["cantidad_orden_armado"]["header_display"] = "Cant OA.";
            $model["cantidad_orden_armado"]["order"] = "57";
            $model["cantidad_orden_armado"]["width"] = "30";
            
            
            
            $model["fecha_completo"]["show_grid"] = 1;
            $model["fecha_completo"]["header_display"] = "Fecha Completo";
            $model["fecha_completo"]["order"] = "60";
            $model["fecha_completo"]["width"] = "10";
           
            $model["fecha_cumplido"]["show_grid"] = 1;
            $model["fecha_cumplido"]["header_display"] = "Fecha Cumplido";
            $model["fecha_cumplido"]["order"] = "65";
            $model["fecha_cumplido"]["width"] = "10";
            
            
            $model["fecha_generacion_oa"]["show_grid"] = 1;
            $model["fecha_generacion_oa"]["header_display"] = "Fecha Gen Orden";
            $model["fecha_generacion_oa"]["order"] = "70";
            $model["fecha_generacion_oa"]["width"] = "10";
            
            
            
            $model["fecha_entrega_orden_armado"]["show_grid"] = 1;
            $model["fecha_entrega_orden_armado"]["header_display"] = "Fecha Entrega Orden";
            $model["fecha_entrega_orden_armado"]["order"] = "71";
            $model["fecha_entrega_orden_armado"]["width"] = "10";
            
            
            
            $model["observacion_oa"]["show_grid"] = 1;
            $model["observacion_oa"]["header_display"] = "Obs Orden";
            $model["observacion_oa"]["order"] = "71";
            $model["observacion_oa"]["width"] = "10";
            
            
            $model["ubicacion_oa"]["show_grid"] = 1;
            $model["ubicacion_oa"]["header_display"] = "Ubicaci&oacute;n OA";
            $model["ubicacion_oa"]["order"] = "72";
            $model["ubicacion_oa"]["width"] = "10";
            
            
            $model["estado_orden_armado"]["show_grid"] = 1;
            $model["estado_orden_armado"]["header_display"] = "Estado Orden";
            $model["estado_orden_armado"]["order"] = "75";
            $model["estado_orden_armado"]["width"] = "15";
            
            
            

 $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "pedidos_con_orden_ensamble_view",
                    "content" => $model
                );
        
          echo json_encode($output);
?>