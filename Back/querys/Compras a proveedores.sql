
SELECT comprobante.`razon_social`,comprobante.`nro_comprobante` AS nro_oc,comprobante.`fecha_generacion`,comprobante.`fecha_entrega`,
producto.codigo,producto.`d_producto`

,comprobante_item.`cantidad`,
moneda.`simbolo_internacional`,1/comprobante.`valor_moneda2`,precio_unitario,comprobante_item.`descuento_unitario` FROM comprobante 
JOIN comprobante_item ON comprobante_item.`id_comprobante` = comprobante.id 
JOIN moneda ON moneda.id = comprobante.`id_moneda`
JOIN producto ON producto.id = comprobante_item.`id_producto`
    
WHERE comprobante.id_tipo_comprobante = 302 

AND comprobante.id_persona IN (
18811,18726,19005,18889,20337,19051,19013,20383)

AND YEAR(comprobante.`fecha_generacion`) IN (YEAR(NOW())-1,YEAR(NOW()))
AND comprobante.`id_estado_comprobante`<>2

LIMIT 100000000