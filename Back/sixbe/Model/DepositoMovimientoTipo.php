<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class DepositoMovimientoTipo extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'deposito_movimiento_tipo';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
       
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
        'MovimientoTipo' => array(
            'className' => 'MovimientoTipo',
            'foreignKey' => 'id_movimiento_tipo',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Deposito' => array(
            'className' => 'Deposito',
            'foreignKey' => 'id_deposito',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ), 
			'TipoComprobante' => array(
					'className' => 'TipoComprobante',
					'foreignKey' => 'id_tipo_comprobante',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
			
			'PuntoVenta' => array(
					'className' => 'PuntoVenta',
					'foreignKey' => 'id_punto_venta',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
        
       
   );     
	
	
	public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
		
		
		
		$this->virtual_fields_view = array(
				
				"d_movimiento_tipo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_deposito"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_es_origen"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_punto_venta"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_por_defecto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				
				
				
		);
		
	}
	
	
	public function index($conditions = array()){
		
		
		$deposito = ClassRegistry::init(EnumModel::Deposito);
		
		
		$dmt = $this->find('all', array(
				'conditions' => $conditions,
				'contain' => array('MovimientoTipo','Deposito','TipoComprobante','PuntoVenta')
		));
		
		
		foreach($dmt as &$prodcom){
			
			
			
			
			
			$data_arbol = $deposito->find('all', array(
					'conditions' => array('Deposito.parent_id' => $prodcom["Deposito"]["id"]),
					'contain' =>false
			));
			
			
			
			
			$prodcom["Deposito"]["children"] = $data_arbol;
			
			$prodcom["DepositoMovimientoTipo"]["d_movimiento_tipo"] = $prodcom["MovimientoTipo"]["id"].' '.$prodcom["MovimientoTipo"]["d_movimiento_tipo"];
			$prodcom["DepositoMovimientoTipo"]["d_deposito"] = $prodcom["Deposito"]["d_deposito"];
			
			if($prodcom["DepositoMovimientoTipo"]["es_origen"] == 1)
				$prodcom["DepositoMovimientoTipo"]["d_es_origen"] = "Si";
			else
				$prodcom["DepositoMovimientoTipo"]["d_es_origen"] = "No";
					
					
			if(isset($prodcom["PuntoVenta"]))
				$prodcom["DepositoMovimientoTipo"]["d_punto_venta"] = $prodcom["PuntoVenta"]["numero"];
						
						
			$prodcom["DepositoMovimientoTipo"]["d_tipo_comprobante"] = $prodcom["TipoComprobante"]["id"].' '.$prodcom["TipoComprobante"]["codigo_tipo_comprobante"];
						
			if($prodcom["DepositoMovimientoTipo"]["por_defecto"] == 1)
				$prodcom["DepositoMovimientoTipo"]["d_por_defecto"] = "Si";
			else
				$prodcom["DepositoMovimientoTipo"]["d_por_defecto"] = "No";
								
								
								
								
		}
		
		return $dmt;
		
	}
	

}
