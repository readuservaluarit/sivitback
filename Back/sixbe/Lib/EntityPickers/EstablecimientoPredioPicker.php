<?php

class EstablecimientoPredioPicker extends EntityPicker{
    
    public $model = "EstablecimientoPredio";
    
    public $pickerTitle = "Selección de Establecimiento Relacionado con el Pedio";
    
    public function getPickerJoins() {
        
            return array(array(
                                'alias' => 'Establecimiento',
                                'table' => 'establecimiento',
                                'type' => 'LEFT',
                                'conditions' => '`EstablecimientoPredio`.`id_establecimiento` = `Establecimiento`.`id`'
                                ));
        
    }
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "Establecimiento.id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Cue", "field" => "Establecimiento.cue_anexo", "width" => "30%", "keyOrder" => "cue_anexo"),
                                  array("title" => "Denominacion", "field" => "Establecimiento.denominacion", "width" => "30%", "keyOrder" => "denominacion"),
                                  array("title" => "CueAnexo", "field" => "Establecimiento.cue_anexo", "width" => "30%", "keyOrder" => "denominacion")
                                );

    public function getPickerConditions($busqueda){
        
        return array('OR' => array('Establecimiento.cue_anexo LIKE'  => '%' . $busqueda['bus_unico'] . '%', 'Establecimiento.denominacion LIKE' => '%' . $busqueda['bus_unico'] . '%'));
    }

}