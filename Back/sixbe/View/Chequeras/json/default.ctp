<?php
        // CONFIG  CON MODE DISPLAYED CELLS
        
        
        $model["d_banco_sucursal"]["show_grid"] = 0;
        $model["d_banco_sucursal"]["type"] = EnumTipoDato::cadena;
        
        $model["d_chequera"]["show_grid"] = 0;
        $model["d_chequera"]["type"] = EnumTipoDato::cadena;

        $model["nro_cuenta"]["show_grid"] = 0;
        $model["nro_cuenta"]["type"] = EnumTipoDato::cadena;
        
        
        $model["id_banco"]["show_grid"] = 0;
        $model["id_banco"]["type"] = EnumTipoDato::entero;
        
        
        $model["id_banco"]["show_grid"] = 0;
        $model["id_banco"]["type"] = EnumTipoDato::entero;
        
        
        $model["id_banco_sucursal"]["show_grid"] = 0;
        $model["id_banco_sucursal"]["type"] = EnumTipoDato::entero;
        
        
        
        $model["d_banco"]["show_grid"] = 0;
        $model["d_banco"]["type"] = EnumTipoDato::cadena;
       
      
        
        $model["nro_cuenta"]["show_grid"] = 1;
        $model["nro_cuenta"]["header_display"] = "Cuenta Bancaria";
        $model["nro_cuenta"]["width"] = "10"; //REstriccion mas de esto no se puede achicar
        $model["nro_cuenta"]["order"] = "1";
        $model["nro_cuenta"]["text_colour"] = "#0000EE";
        
        $model["observaciones"]["show_grid"] = 1;
        $model["observaciones"]["header_display"] = "Obs.";
        $model["observaciones"]["width"] = "15";
        $model["observaciones"]["minimun_width"] = "90"; //REstriccion mas de esto no se puede achicar
        $model["observaciones"]["order"] = "2";
        $model["observaciones"]["text_colour"] = "#0000EE";
        
        
        $model["numero_chequera"]["show_grid"] = 1;
        $model["numero_chequera"]["header_display"] = "Nro. Chequera";
        $model["numero_chequera"]["width"] = "15"; //REstriccion mas de esto no se puede achicar
        $model["numero_chequera"]["order"] = "3";
        $model["numero_chequera"]["text_colour"] = "#00BB00";
        
        
        $model["numero_actual"]["show_grid"] = 1;
        $model["numero_actual"]["header_display"] = "Nro. Actual";
        $model["numero_actual"]["width"] = "15"; //REstriccion mas de esto no se puede achicar
        $model["numero_actual"]["order"] = "4";
        $model["numero_actual"]["text_colour"] = "#00BB00";
        
        $model["cheque_desde"]["show_grid"] = 1;
        $model["cheque_desde"]["header_display"] = "Cheque Desde";
        $model["cheque_desde"]["width"] = "15"; //REstriccion mas de esto no se puede achicar
        $model["cheque_desde"]["order"] = "5";
        $model["cheque_desde"]["text_colour"] = "#0000EE";
		
		
		$model["cheque_hasta"]["show_grid"] = 1;
        $model["cheque_hasta"]["header_display"] = "Cheque Hasta";
        $model["cheque_hasta"]["width"] = "15"; //REstriccion mas de esto no se puede achicar
        $model["cheque_hasta"]["order"] = "6";
        $model["cheque_hasta"]["text_colour"] = "#EE0000";
		
		$model["cantidad_cheques"]["show_grid"] = 1;
        $model["cantidad_cheques"]["header_display"] = "Cantidad Cheques";
        $model["cantidad_cheques"]["width"] = "15"; //REstriccion mas de esto no se puede achicar
        $model["cantidad_cheques"]["order"] = "7";
        $model["cantidad_cheques"]["text_colour"] = "#000000";
        
   
   
        $model["d_estado_chequera"]["show_grid"] = 1;
        $model["d_estado_chequera"]["type"] = EnumTipoDato::cadena;
        $model["d_estado_chequera"]["header_display"] = "Estado";
        $model["d_estado_chequera"]["width"] = "15"; //REstriccion mas de esto no se puede achicar
        $model["d_estado_chequera"]["order"] = "8";
        $model["d_estado_chequera"]["text_colour"] = "#770000";
        
        
         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Chequera",
                    "content" => $model
                );
        
          echo json_encode($output);
?>