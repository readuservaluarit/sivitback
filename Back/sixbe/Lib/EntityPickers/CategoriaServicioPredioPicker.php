<?php

class CategoriaServicioPredioPicker extends EntityPicker{
    
    public $model = "CategoriaServicioPredio";
    
    public $pickerTitle = "Selección de Categor&iacute;a Servicio Predio";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Descripcion", "field" => "descripcion", "width" => "80%", "keyOrder" => "descripcion")
                                );

    public function getPickerConditions($busqueda){
        
        return array('CategoriaServicioPredio.descripcion LIKE' => '%' . $busqueda['bus_unico'] . '%');
    }

}