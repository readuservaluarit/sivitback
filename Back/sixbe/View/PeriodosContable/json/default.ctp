<?php include(APP.'View'.DS.'PeriodosContable'.DS.'json'.DS.'periodo_contable.ctp');
        
     
        
        $model["d_periodo_contable"]["show_grid"] = 1;
        $model["d_periodo_contable"]["header_display"] = "Desc.";
        $model["d_periodo_contable"]["order"] = "1";
        $model["d_periodo_contable"]["width"] = "30";
		$model["d_periodo_contable"]["text_colour"] = "#000088";
				
        $model["nro_periodo"]["show_grid"] = 1;
        $model["nro_periodo"]["header_display"] = "NºPerdo.";
        $model["nro_periodo"]["order"] = "2";
        $model["nro_periodo"]["width"] = "5";
		$model["nro_periodo"]["text_colour"] = "#770000";
        
        $model["d_ejercicio_contable"]["show_grid"] = 1;
        $model["d_ejercicio_contable"]["header_display"] = "Ejercicio Contable.";
        $model["d_ejercicio_contable"]["order"] = "3";
        $model["d_ejercicio_contable"]["width"] = "10";
        
        $model["actual"]["show_grid"] = 1;
        $model["actual"]["header_display"] = "Actual?";
        $model["actual"]["order"] = "4";
        $model["actual"]["width"] = "7";
		$model["actual"]["text_colour"] = "#008800";
        
        $model["desc_periodo"]["show_grid"] = 1;
        $model["desc_periodo"]["header_display"] = "Desc. Secund.";
        $model["desc_periodo"]["order"] = "5";
        $model["desc_periodo"]["width"] = "30";
		$model["desc_periodo"]["text_colour"] = "#990000";
        
        $model["fecha_desde"]["show_grid"] = 1;
        $model["fecha_desde"]["header_display"] = "Fch Desde";
        $model["fecha_desde"]["order"] = "6";
        $model["fecha_desde"]["width"] = "12";
		$model["fecha_desde"]["text_colour"] = "#000066";
        
        $model["fecha_hasta"]["show_grid"] = 1;
        $model["fecha_hasta"]["header_display"] = "Fch Hasta";
        $model["fecha_hasta"]["order"] = "7";
        $model["fecha_hasta"]["width"] = "12";
		$model["fecha_hasta"]["text_colour"] = "#0000AA";
        
        $model["id_ultimo_asiento"]["show_grid"] = 1;
        $model["id_ultimo_asiento"]["header_display"] = "Ult. Asiento id";
        $model["id_ultimo_asiento"]["order"] = "8";
        $model["id_ultimo_asiento"]["width"] = "6";
		$model["id_ultimo_asiento"]["text_colour"] = "#004455";
        
        $model["d_disponible"]["show_grid"] = 1;   
        $model["d_disponible"]["header_display"] = "Disponible";
        $model["d_disponible"]["order"] = "9";
        $model["d_disponible"]["width"] = "7";
        $model["d_disponible"]["text_colour"] = "#AA0033";
        
        $model["d_cerrado"]["show_grid"] = 1;   
        $model["d_cerrado"]["header_display"] = "Cerrado";
        $model["d_cerrado"]["order"] = "9";
        $model["d_cerrado"]["width"] = "7";
		$model["d_cerrado"]["text_colour"] = "#770000";
     
        $model["fecha_cierre"]["show_grid"] = 1;
        $model["fecha_cierre"]["header_display"] = "Fcha. Cierre";
        $model["fecha_cierre"]["order"] = "10";
        $model["fecha_cierre"]["width"] = "12";
		$model["fecha_cierre"]["text_colour"] = "#550000";

        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "PeriodoContable",
                    "content" => $model
                );
        echo json_encode($output);
         
        

?>