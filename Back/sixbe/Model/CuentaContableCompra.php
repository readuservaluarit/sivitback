<?php

App::uses('AppModel', 'Model');
App::uses('CuentaContable', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class CuentaContableCompra extends CuentaContable {

/**
 * Use table
 *
 * @var mixed False or table name
 */



    public $virtualFields = array(
    'd_codigo_cuenta_contable' => 'CONCAT(CuentaContableCompra.codigo, " " , CuentaContableCompra.d_cuenta_contable)'
);

/**
 * Validation rules
 *
 * @var array
 */
	




}
