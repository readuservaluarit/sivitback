<?php
       
        include(APP.'View'.DS.'Articulo'.DS.'json'.DS.'articulo.ctp');
        
 
   		App::import('Model','Comprobante');
		$comprobante = new Comprobante();
		$produccion = $comprobante->getActivoModulo(EnumModulo::PRODUCCION);
		
        
        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "C&oacute;digo";
        $model["codigo"]["width"] = "12";
        $model["codigo"]["order"] = "2";
        $model["codigo"]["text_colour"] = "#000000";
        
        
        $model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "Descripci&oacute;n";
        $model["d_producto"]["width"] = "35";
        $model["d_producto"]["order"] = "3";
        $model["d_producto"]["text_colour"] = "#0000FF";
        
        
        $model["d_unidad"]["show_grid"] = 1; 
        $model["d_unidad"]["header_display"] = "Unidad";
        $model["d_unidad"]["width"] = "10";
        $model["d_unidad"]["order"] = "4";
        $model["d_unidad"]["text_colour"] = "#0000DD";
        
        
       
	        $model["costo"]["show_grid"] = 1;
	        $model["costo"]["header_display"] = "Costo";
	        $model["costo"]["order"] = "5";
	        $model["costo"]["width"] = "5";
	        $model["costo"]["text_colour"] = "#DD0000";
       
        
        $model["d_moneda_precio"]["show_grid"] = 1;
        $model["d_moneda_precio"]["header_display"] = "Moneda";
        $model["d_moneda_precio"]["order"] = "6";
        $model["d_moneda_precio"]["width"] = "10";
        $model["d_moneda_precio"]["text_colour"] = "#DD0000";
        
        $model["precio"]["show_grid"] = 1;
        $model["precio"]["header_display"] = "Precio";
        $model["precio"]["order"] = "7";
        $model["precio"]["width"] = "10";
        $model["precio"]["text_colour"] = "#DD0000";
        
        
        $model["peso"]["show_grid"] = 1;
        $model["peso"]["header_display"] = "Peso";
        $model["peso"]["order"] = "8";
        $model["peso"]["width"] = "5";
        $model["peso"]["text_colour"] = "#AA0077";
        
		
		
        
        
        
         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Producto",
                    "content" => $model
                );
        
          echo json_encode($output);
?>