<?php   
        include(APP.'View'.DS.'ComprobanteItemComprobantes'.DS.'json'.DS.'default.ctp');
		
        $model["n_item"]["show_grid"] = 1;
        $model["n_item"]["header_display"] = "Ref. ";
        $model["n_item"]["order"] = "1";
		$model["n_item"]["width"] = "3";
		$model["n_item"]["type"] = "string";
		$model["n_item"]["text_colour"] = "#000000";
		$model["n_item"]["read_only"] = "1";
		
		$model["id_producto"]["show_grid"] = 0;
        $model["id_producto"]["header_display"] = "Producto";
        $model["id_producto"]["order"] = "1";
		
		$model["id_producto"]["width"] = "3";
		$model["id_producto"]["text_colour"] = "#000000";
		$model["id_producto"]["read_only"] = "1";
		
		$model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "Codigo";
        $model["codigo"]["order"] = "2";
		$model["codigo"]["width"] = "4";
		$model["codigo"]["text_colour"] = "#000000";
		$model["codigo"]["read_only"] = "1";
	
        $model["pto_nro_comprobante_origen"]["show_grid"] = 1;
        $model["pto_nro_comprobante_origen"]["header_display"] = "Nro. Comprobante";
        $model["pto_nro_comprobante_origen"]["order"] = "4";
		$model["pto_nro_comprobante_origen"]["width"] = "8";
		$model["pto_nro_comprobante_origen"]["text_colour"] = "#000000";
		$model["pto_nro_comprobante_origen"]["read_only"] = "1";
		
        $model["item_observacion"]["show_grid"] = 1;
        $model["item_observacion"]["header_display"] = "Observ.";
        $model["item_observacion"]["order"] = "8";
		$model["item_observacion"]["width"] = "15";
		$model["item_observacion"]["text_colour"] = "#0000AA";
		$model["item_observacion"]["read_only"] = "0";
		
        $output = array(
                    "status" =>EnumError::SUCCESS,
                   
                    "message" => "ComprobanteItem",
                    "content" => $model
                );
        echo json_encode($output);
?>



