<?php
class FuncionPicker extends EntityPicker{
    
    public $model = "Funcion";
    
    public $pickerTitle = "Selección de Función";
    
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Código", "field" => "c_funcion", "width" => "30%", "keyOrder" => "c_funcion"),
                                  array("title" => "Descripción", "field" => "d_funcion", "width" => "50%", "keyOrder" => "d_funcion")
                                );

    public function getPickerConditions($busqueda){
        
        return array('Funcion.d_funcion LIKE' => '%' . $busqueda['bus_unico'] . '%');
    }

}