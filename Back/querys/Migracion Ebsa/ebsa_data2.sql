/*
SQLyog Job Agent v12.08 (64 bit) Copyright(c) Webyog Inc. All Rights Reserved.

MySQL - 5.5.55-0+deb8u1-log  
*********************************************************************
*/
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

/* SYNC DB : valuarit_enerbom_test */ 
  SET FOREIGN_KEY_CHECKS = 0;
SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;
 START TRANSACTION;
/* SYNC TABLE : `entidad` */

	/*Start of batch : 1 */
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'localidad' AND `id` = 81) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'afiplog' AND `id` = 1) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'tur_cotizacion_item' AND `id` = 161) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'tur_cotizacion_cliente_asociado' AND `id` = 159) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'tur_cotizacion' AND `id` = 158) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'tur_cliente' AND `id` = 157) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'localidades_temporal' AND `id` = 82) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'tipo_reclamo' AND `id` = 156) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'tipo_movimiento_cuenta_corriente' AND `id` = 154) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'tipo_empleador' AND `id` = 148) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'tipo_contrato' AND `id` = 144) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'auditoria_producto' AND `id` = 11) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'audi_atributo' AND `id` = 12) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'audi_entidad' AND `id` = 13) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'tipo_comprobante_impuesto' AND `id` = 141) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'tipo_actividad' AND `id` = 134) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'cake_sessions' AND `id` = 16) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'campo_extra' AND `id` = 17) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'campo_extra_registro' AND `id` = 18) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'tesoreria_clase_transaccion' AND `id` = 133) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'licencia' AND `id` = 77) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'sub_familia_producto' AND `id` = 131) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'media_file_tipo' AND `id` = 86) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'mime_type' AND `id` = 87) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'modulo' AND `id` = 88) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'historico_login' AND `id` = 73) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'gen_audi' AND `id` = 72) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'clearing' AND `id` = 26) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'funcion_rol' AND `id` = 71) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'movimiento_item' AND `id` = 91) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'nivel_autorizado_informacion' AND `id` = 93) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'filtro' AND `id` = 68) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'orden_armado_item' AND `id` = 96) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'parametro_contabilidad' AND `id` = 100) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'equipo' AND `id` = 58) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'entidad' AND `id` = 57) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'empleado' AND `id` = 56) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'persona_impuesto_alicuota_temporal' AND `id` = 107) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'persona_impuesto_alicuota_temporal_caba' AND `id` = 108) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'persona_valor_tipo_comprobante' AND `id` = 109) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'csv_import_asiento' AND `id` = 42) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'controlador' AND `id` = 41) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'qa_origen_falla' AND `id` = 120) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'comprobante_recepcion_item' AND `id` = 37) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'recepcion_material' AND `id` = 121) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'comprobante_lote_item' AND `id` = 35) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'recepcion_material_item' AND `id` = 122) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'reporte' AND `id` = 124) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'rubros' AND `id` = 126) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'lista_precio_producto' AND `id` = 79) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'comprobante_item' AND `id` = 31) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'comprobante_digital' AND `id` = 29) ;
DELETE FROM `valuarit_enerbom_test`.`entidad`  WHERE (`d_entidad` = 'comprobante_cartera_rendicion' AND `id` = 28) ;
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('812', 'comprobante', NULL, '0', NULL, NULL, NULL, 'NotasDebitoCompraController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('813', 'comprobante', NULL, '0', NULL, NULL, NULL, 'NotasDebitoVentaController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('814', 'comprobante', NULL, '0', NULL, NULL, NULL, 'OrdenCompraController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('819', 'comprobante', NULL, '0', NULL, NULL, NULL, 'OrdenesPagoAnticipoController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('820', 'comprobante', NULL, '0', NULL, NULL, NULL, 'OrdenesPagoController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('821', 'comprobante', NULL, '0', NULL, NULL, NULL, 'OrdenesPagoGastoController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('824', 'comprobante', NULL, '0', NULL, NULL, NULL, 'PedidosInternosController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('832', 'comprobante', NULL, '0', NULL, NULL, NULL, 'QaInformesController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('833', 'comprobante', NULL, '0', NULL, NULL, NULL, 'RechazoChequesController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('835', 'comprobante', NULL, '0', NULL, NULL, NULL, 'RecibosController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('838', 'comprobante', NULL, '0', NULL, NULL, NULL, 'RemitosCompraController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('839', 'comprobante', NULL, '0', NULL, NULL, NULL, 'RemitosController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('840', 'comprobante', NULL, '0', NULL, NULL, NULL, 'RetencionesAjenasController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('841', 'comprobante', NULL, '0', NULL, NULL, NULL, 'RetencionesController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('844', 'comprobante', NULL, '0', NULL, NULL, NULL, 'SaldosInicialesClienteController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('845', 'comprobante', NULL, '0', NULL, NULL, NULL, 'SaldosInicialesProveedorController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('855', 'comprobante', NULL, '0', '0', '0', '1', 'OrdenesTrabajoController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('859', 'comprobante', NULL, '0', '0', '0', '1', 'OperacionesInternaController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('862', 'comprobante', NULL, '0', '0', '0', '0', 'OrdenesTrabajoController', 'OrdenTrabajo');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('811', 'comprobante', NULL, '0', NULL, NULL, NULL, 'NotasCreditoVentaController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('810', 'comprobante', NULL, '0', NULL, NULL, NULL, 'NotasCreditoCompraController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('847', 'stock_producto', NULL, '0', NULL, NULL, NULL, 'StockBienesUsoController', 'StockArticulo');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('809', 'comprobante', NULL, '0', NULL, NULL, NULL, 'NotasController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('856', 'comprobante_item_lote', NULL, '0', '0', '0', '0', 'ComprobanteItemLotesController', 'ComprobanteItemLote');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('808', 'comprobante', NULL, '0', NULL, NULL, NULL, 'NotasCompraController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('805', 'comprobante', NULL, '0', NULL, NULL, NULL, 'MovimientosCarteraRendicionController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('760', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'AnticipoCompraItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('768', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'ComprobanteItemComprobantesController', 'ComprobanteItemComprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('769', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'ComprobanteItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('773', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'CotizacionItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('791', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'FacturaCompraItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('792', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'FacturaItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('795', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'GastoItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('799', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'InformeRecepcionMaterialesItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('806', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'NotaCompraItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('807', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'NotaItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('815', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'OrdenCompraItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('816', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'OrdenPagoAnticipoItemsController', 'ComprobanteItemComprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('817', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'OrdenPagoGastoItemsController', 'ComprobanteItemComprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('818', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'OrdenPagoItemsController', 'ComprobanteItemComprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('823', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'PedidoInternoItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('831', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'QaInformeItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('834', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'ReciboItemsController', 'ComprobanteItemComprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('836', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'RemitoCompraItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('837', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'RemitoItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('842', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'SaldosInicialClienteItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('843', 'comprobante_item_view', NULL, '0', NULL, NULL, NULL, 'SaldosInicialProveedorItemsController', 'ComprobanteItem');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('804', 'comprobante', NULL, '0', NULL, NULL, NULL, 'MovimientosCajaController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('803', 'comprobante', NULL, '0', NULL, NULL, NULL, 'MovimientosBancariosController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('798', 'comprobante', NULL, '0', NULL, NULL, NULL, 'InformeRecepcionMaterialesController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('797', 'comprobante', NULL, '0', NULL, NULL, NULL, 'GastosController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('794', 'comprobante', NULL, '0', NULL, NULL, NULL, 'FacturasController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('793', 'comprobante', NULL, '0', NULL, NULL, NULL, 'FacturasCompraController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('846', 'producto', NULL, '0', NULL, NULL, NULL, 'ScrapController', 'Scrap');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('828', 'producto', NULL, '0', NULL, NULL, NULL, 'ProductosRetailController', 'ProductoRetail');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('782', 'comprobante', NULL, '0', NULL, NULL, NULL, 'DebitosInternoProveedorController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('781', 'comprobante', NULL, '0', NULL, NULL, NULL, 'DebitosInternoClienteController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('827', 'producto', NULL, '0', NULL, NULL, NULL, 'ProductosController', 'Producto');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('822', 'producto', NULL, '0', NULL, NULL, NULL, 'PackagingController', 'Producto');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('801', 'producto', NULL, '0', NULL, NULL, NULL, 'MateriasPrimasController', 'MateriaPrima');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('800', 'producto', NULL, '0', NULL, NULL, NULL, 'InsumosInternoController', 'InsumoInterno');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('796', 'producto', NULL, '0', NULL, NULL, NULL, 'GastosArticuloController', 'GastoArticulo');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('767', 'producto', NULL, '0', NULL, NULL, NULL, 'ComponentesController', 'Componente');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('763', 'producto', NULL, '0', NULL, NULL, NULL, 'BienesUsoController', 'BienUso');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('762', 'producto', NULL, '0', NULL, NULL, NULL, 'ArticuloController', 'Articulo');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('780', 'comprobante', NULL, '0', NULL, NULL, NULL, 'CuentasCorrienteProveedorController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('779', 'comprobante', NULL, '0', NULL, NULL, NULL, 'CuentasCorrienteController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('778', 'comprobante', NULL, '0', NULL, NULL, NULL, 'CuentasCorrienteClienteController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('829', 'persona_categoria', NULL, '0', NULL, '1', '1', 'ProveedoresCategoriasController', 'PersonaCategoria');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('771', 'detalle_tipo_comprobante', NULL, '0', NULL, NULL, NULL, 'ConceptosCajaController', 'DetalleTipoComprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('825', 'persona_categoria', NULL, '0', NULL, NULL, NULL, 'PersonasCategoriasController', 'PersonaCategoria');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('777', 'comprobante', NULL, '0', NULL, NULL, NULL, 'CreditosInternoProveedorController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('776', 'comprobante', NULL, '0', NULL, NULL, NULL, 'CreditosInternoClienteController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('775', 'comprobante', NULL, '0', NULL, NULL, NULL, 'CotizacionesController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('783', 'persona_categoria', NULL, '0', NULL, '1', '1', 'EmpleadosCategoriasController', 'PersonaCategoria');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('765', 'persona_categoria', NULL, '0', NULL, '1', '1', 'ClientesCategoriasController', 'PersonaCategoria');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('854', 'persona', NULL, '0', NULL, NULL, NULL, 'SucursalesController', 'Persona');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('830', 'persona', NULL, '0', NULL, NULL, NULL, 'ProveedoresController', 'Persona');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('826', 'persona', NULL, '0', NULL, NULL, NULL, 'PersonasController', 'Persona');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('784', 'persona', NULL, '0', NULL, NULL, NULL, 'EmpleadosController', 'Persona');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('785', 'estado_persona', NULL, '0', NULL, NULL, NULL, 'EstadosClientesController', 'EstadoPersona');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('786', 'estado_persona', NULL, '0', NULL, NULL, NULL, 'EstadosContactoController', 'EstadoPersona');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('787', 'estado_persona', NULL, '0', NULL, '1', '1', 'EstadosEmpleadosController', 'EstadoPersona');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('788', 'estado_persona', NULL, '0', NULL, '1', '1', 'EstadosPersonaController', 'EstadoPersona');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('789', 'estado_persona', NULL, '0', NULL, '1', '1', 'EstadosProveedoresController', 'EstadoPersona');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('772', 'persona', NULL, '0', NULL, NULL, NULL, 'ContactosController', 'Persona');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('766', 'persona', NULL, '0', NULL, NULL, NULL, 'ClientesController', 'Persona');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('774', 'comprobante', NULL, '0', NULL, NULL, NULL, 'CotizacionesCompraController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('770', 'comprobante', NULL, '0', NULL, NULL, NULL, 'ComprobantesController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('761', 'comprobante', NULL, '0', NULL, NULL, NULL, 'AnticiposCompraController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('167', 'comprobante', NULL, '0', '0', '0', '0', 'ReparacionesController', 'Comprobante');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('848', 'stock_producto', NULL, '0', NULL, NULL, NULL, 'StockComponentesController', 'StockComponente');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('849', 'stock_producto', NULL, '0', NULL, NULL, NULL, 'StockInsumosInternoController', 'StockArticulo');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('850', 'stock_producto', NULL, '0', NULL, NULL, NULL, 'StockMateriaPrimasController', 'StockMateriaPrima');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('851', 'stock_producto', NULL, '0', NULL, NULL, NULL, 'StockPackagingController', 'StockArticulo');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('852', 'stock_producto', NULL, '0', NULL, NULL, NULL, 'StockProductosController', 'StockProducto');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('764', 'cheque', NULL, '0', NULL, NULL, NULL, 'ChequesController', 'Cheque');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('853', 'stock_producto', NULL, '0', NULL, NULL, NULL, 'StockProductosRetailController', 'StockProducto');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('857', 'tarjeta_credito', NULL, '0', '0', '1', '1', 'TarjetasCreditoController', 'TarjetaCredito');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('858', 'tipo_tarjeta_credito', NULL, '0', '1', '1', '0', 'TiposTarjetaCreditoController', 'TipoTarjetaCredito');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('863', 'comprobante_item_lote', NULL, '0', '0', '0', '0', 'ComprobanteItemLotesController', 'ComprobanteItemLote');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('790', 'estado_persona', NULL, '0', NULL, '1', '1', 'EstadosSucursalController', 'EstadoPersona');
INSERT INTO `valuarit_enerbom_test`.`entidad` VALUES ('802', 'usuario', NULL, '0', NULL, NULL, NULL, 'MisDatosController', 'Usuario');
	/*End   of batch : 1 */
/* SYNC TABLE : `funcion` */

	/*Start of batch : 1 */
DELETE FROM `valuarit_enerbom_test`.`funcion`  WHERE (`id` = 250) ;
	/*End   of batch : 1 */
	/*Start of batch : 2 */
DELETE FROM `valuarit_enerbom_test`.`funcion`  WHERE (`id` = 2683) ;
DELETE FROM `valuarit_enerbom_test`.`funcion`  WHERE (`id` = 2693) ;
DELETE FROM `valuarit_enerbom_test`.`funcion`  WHERE (`id` = 2694) ;
DELETE FROM `valuarit_enerbom_test`.`funcion`  WHERE (`id` = 2684) ;
DELETE FROM `valuarit_enerbom_test`.`funcion`  WHERE (`id` = 2704) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2716', `c_funcion`='MENU_OPERACION_INTERNA', `d_funcion`='Menu de Operaciones internas', `id_tipo_funcion`='242'  WHERE (`id` = 2716) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2715', `c_funcion`='ADD_OPERACION_INTERNA', `d_funcion`='Alta de Operaciones internas', `id_tipo_funcion`='242'  WHERE (`id` = 2715) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2714', `c_funcion`='CONSULTA_OPERACION_INTERNA', `d_funcion`='Consulta de Operaciones internas', `id_tipo_funcion`='242'  WHERE (`id` = 2714) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2708', `c_funcion`='BTN_PDF_TARJETASCREDITO', `d_funcion`='Boton duplicar para TarjetasCredito', `id_tipo_funcion`='241'  WHERE (`id` = 2708) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2707', `c_funcion`='BTN_EXCEL_TARJETASCREDITO', `d_funcion`='Boton xls para reporte de TarjetasCredito', `id_tipo_funcion`='241'  WHERE (`id` = 2707) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2706', `c_funcion`='BTN_DUPLICAR_TARJETASCREDITO', `d_funcion`='Boton duplicar para TarjetasCredito', `id_tipo_funcion`='241'  WHERE (`id` = 2706) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2705', `c_funcion`='ADD_TARJETA_CREDITO_IMPORTADO', `d_funcion`='Agregar Tarjeta de Credito importando Ordenes de Compra', `id_tipo_funcion`='241'  WHERE (`id` = 2705) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2717', `c_funcion`='BAJA_OPERACION_INTERNA', `d_funcion`='Baja de Operaciones internas', `id_tipo_funcion`='242'  WHERE (`id` = 2717) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2703', `c_funcion`='MODIFICACION_TARJETA_CREDITO', `d_funcion`='Modificacion de Tarjeta de Credito', `id_tipo_funcion`='241'  WHERE (`id` = 2703) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2702', `c_funcion`='BAJA_TARJETA_CREDITO', `d_funcion`='Baja de Tarjeta de Credito', `id_tipo_funcion`='241'  WHERE (`id` = 2702) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2701', `c_funcion`='MENU_TARJETA_CREDITO', `d_funcion`='Menu de Tarjeta de Credito', `id_tipo_funcion`='241'  WHERE (`id` = 2701) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2718', `c_funcion`='MODIFICACION_OPERACION_INTERNA', `d_funcion`='Modificacion de Operaciones internas', `id_tipo_funcion`='242'  WHERE (`id` = 2718) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2699', `c_funcion`='CONSULTA_TARJETA_CREDITO', `d_funcion`='Consulta de Tarjeta de Credito', `id_tipo_funcion`='241'  WHERE (`id` = 2699) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2698', `c_funcion`='AUDITORIA_ORDEN_ARMADO', `d_funcion`='Auditoria para Orden Armado', `id_tipo_funcion`='245'  WHERE (`id` = 2698) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2697', `c_funcion`='BTN_PDF_ComprobanteItemLotes', `d_funcion`='Boton duplicar para ComprobanteItemLotes', `id_tipo_funcion`='245'  WHERE (`id` = 2697) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2696', `c_funcion`='BTN_EXCEL_ComprobanteItemLotes', `d_funcion`='Boton xls para reporte de ComprobanteItemLotes', `id_tipo_funcion`='245'  WHERE (`id` = 2696) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2695', `c_funcion`='BTN_DUPLICAR_ComprobanteItemLotes', `d_funcion`='Boton duplicar para ComprobanteItemLotes', `id_tipo_funcion`='245'  WHERE (`id` = 2695) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2719', `c_funcion`='BTN_OPERACION_INTERNA_LIBRE', `d_funcion`='Boton Factura de Compra Manual', `id_tipo_funcion`='242'  WHERE (`id` = 2719) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2720', `c_funcion`='ADD_OPERACION_INTERNA_IMPORTADO', `d_funcion`='Agregar Operaciones internas importando Ordenes de Compra', `id_tipo_funcion`='242'  WHERE (`id` = 2720) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2692', `c_funcion`='MODIFICACION_COMPROBANTE_ITEM_LOTE', `d_funcion`='Modificacion de Comprobante Item Lote', `id_tipo_funcion`='245'  WHERE (`id` = 2692) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2691', `c_funcion`='BAJA_COMPROBANTE_ITEM_LOTE', `d_funcion`='Baja de Comprobante Item Lote', `id_tipo_funcion`='245'  WHERE (`id` = 2691) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2690', `c_funcion`='MENU_COMPROBANTE_ITEM_LOTE', `d_funcion`='Menu de Comprobante Item Lote', `id_tipo_funcion`='245'  WHERE (`id` = 2690) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2689', `c_funcion`='ADD_COMPROBANTE_ITEM_LOTE', `d_funcion`='Alta de Comprobante Item Lote', `id_tipo_funcion`='245'  WHERE (`id` = 2689) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2688', `c_funcion`='CONSULTA_COMPROBANTE_ITEM_LOTE', `d_funcion`='Consulta de Comprobante Item Lote', `id_tipo_funcion`='245'  WHERE (`id` = 2688) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2686', `c_funcion`='BTN_EXCEL_ORDENESTRABAJO', `d_funcion`='Boton xls para reporte de OrdenesTrabajo', `id_tipo_funcion`='244'  WHERE (`id` = 2686) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2685', `c_funcion`='BTN_DUPLICAR_ORDENESTRABAJO', `d_funcion`='Boton duplicar para OrdenesTrabajo', `id_tipo_funcion`='244'  WHERE (`id` = 2685) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2700', `c_funcion`='ADD_TARJETA_CREDITO', `d_funcion`='Alta de Tarjeta de Credito', `id_tipo_funcion`='241'  WHERE (`id` = 2700) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2722', `c_funcion`='BTN_EXCEL_OPERACIONESINTERNA', `d_funcion`='Boton xls para reporte de OperacionesInterna', `id_tipo_funcion`='242'  WHERE (`id` = 2722) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2682', `c_funcion`='MODIFICACION_ORDEN_TRABAJO', `d_funcion`='Modificacion de Orden de Trabajo', `id_tipo_funcion`='244'  WHERE (`id` = 2682) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2681', `c_funcion`='BAJA_ORDEN_TRABAJO', `d_funcion`='Baja de Orden de Trabajo', `id_tipo_funcion`='244'  WHERE (`id` = 2681) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2680', `c_funcion`='MENU_ORDEN_TRABAJO', `d_funcion`='Menu de Orden de Trabajo', `id_tipo_funcion`='244'  WHERE (`id` = 2680) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2679', `c_funcion`='ADD_ORDEN_TRABAJO', `d_funcion`='Alta de Orden de Trabajo', `id_tipo_funcion`='244'  WHERE (`id` = 2679) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2678', `c_funcion`='CONSULTA_ORDEN_TRABAJO', `d_funcion`='Consulta de Orden de Trabajo', `id_tipo_funcion`='244'  WHERE (`id` = 2678) ;
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2721', `c_funcion`='BTN_DUPLICAR_OPERACIONESINTERNA', `d_funcion`='Boton duplicar para OperacionesInterna', `id_tipo_funcion`='242'  WHERE (`id` = 2721) ;
	/*End   of batch : 2 */
	/*Start of batch : 3 */
UPDATE `valuarit_enerbom_test`.`funcion` SET `id`='2723', `c_funcion`='BTN_PDF_OPERACIONESINTERNA', `d_funcion`='Boton duplicar para OperacionesInterna', `id_tipo_funcion`='242'  WHERE (`id` = 2723) ;
	/*End   of batch : 3 */
/* SYNC TABLE : `tipo_funcion` */

insert into `valuarit_enerbom_test`.`tipo_funcion` values('242' , '1700' , '859' , 'PRODUCCION' , 'comprobante' , 'OperacionesInternaController');
insert into `valuarit_enerbom_test`.`tipo_funcion` values('244' , '1700' , '862' , 'PRODUCCION' , 'comprobante' , 'OrdenesTrabajoController');
insert into `valuarit_enerbom_test`.`tipo_funcion` values('245' , '1700' , '863' , 'PRODUCCION' , 'comprobante_item_lot' , 'ComprobanteItemLotesController');

UPDATE funcion SET id_tipo_funcion =3 WHERE id=626;

UPDATE  `detalle_tipo_comprobante`
SET  `id_tipo_comprobante` = 806
WHERE id = 74

INSERT INTO `detalle_tipo_comprobante` (`id`, `d_detalle_tipo_comprobante`, `id_tipo_comprobante`, `descripcion_accion`, `default`, `afecta_stock_origen`, `tiene_iva`, `tiene_impuestos`, `puede_editar_precio_unitario`, `id_impuesto`, `id_cuenta_contable`, `genera_nd`, `dias`, `dias2`) VALUES('74','Consumidor Final','806','FB - Consumidor Final',NULL,'0','0','0','0',NULL,NULL,'0','0','0');
INSERT INTO `detalle_tipo_comprobante` (`id`, `d_detalle_tipo_comprobante`, `id_tipo_comprobante`, `descripcion_accion`, `default`, `afecta_stock_origen`, `tiene_iva`, `tiene_impuestos`, `puede_editar_precio_unitario`, `id_impuesto`, `id_cuenta_contable`, `genera_nd`, `dias`, `dias2`) VALUES('75','Consumidor Final','811','FC -  Consumidor Final',NULL,'0','0','0','0',NULL,NULL,'0','0','0');
COMMIT;
  SET FOREIGN_KEY_CHECKS = 1;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


UPDATE `tipo_funcion` SET `id_modulo` = '1700' WHERE `id` = '69' AND `id_modulo` = '2100' AND `id_entidad` = '827' AND `d_modulo` = 'PARAMETRICAS' AND `d_entidad` = 'producto' AND `controller` = 'ProductosController'; 

UPDATE `tipo_funcion` SET `id_modulo` = '1700' WHERE `id` = '204' AND `id_modulo` = '2100' AND `id_entidad` = '767' AND `d_modulo` = 'PARAMETRICAS' AND `d_entidad` = 'producto' AND `controller` = 'ComponentesController'; 

UPDATE `tipo_funcion` SET `id_modulo` = '1700' WHERE `id` = '44' AND `id_modulo` = '2100' AND `id_entidad` = '801' AND `d_modulo` = 'PARAMETRICAS' AND `d_entidad` = 'producto' AND `controller` = 'MateriasPrimasController'; 


INSERT INTO .`funcion` (`c_funcion`, `d_funcion`, `id_tipo_funcion`) VALUES ('MENU_STOCK_PRODUCTO', 'Stock de Productos', '69'); 
INSERT INTO .`funcion` (`c_funcion`, `d_funcion`) VALUES ('MENU_STOCK_PRODUCTO_RETAIL', 'Stock de Productos Retail'); 
UPDATE .`funcion` SET `d_funcion` = 'Menu Stock de Productos' WHERE `id` = '2725'; 
UPDATE .`funcion` SET `d_funcion` = 'Menu Stock de Productos Retail' WHERE `id` = '2726'; 
UPDATE .`funcion` SET `id_tipo_funcion` = '70' WHERE `id` = '2726'; 
INSERT INTO .`funcion` (`c_funcion`) VALUES ('MENU_STOCK_MATERIA_PRIMA'); 
UPDATE .`funcion` SET `d_funcion` = 'Menu Stock de Materia Prima' , `id_tipo_funcion` = '44' WHERE `id` = '2727'; 
INSERT INTO .`funcion` (`c_funcion`, `d_funcion`, `id_tipo_funcion`) VALUES ('MENU_STOCK_COMPONENTE', 'Menu Stock de Componente', '204'); 
 INSERT INTO .`funcion` (`c_funcion`) VALUES ('MENU_STOCK_PACKAGING'); 
 UPDATE .`funcion` SET `d_funcion` = 'Menu Stock de Packaging' , `id_tipo_funcion` = '65' WHERE `id` = '2729'; 
 INSERT INTO .`funcion` (`c_funcion`) VALUES ('MENU_STOCK_INSUMO_INTERNO'); 
 UPDATE .`funcion` SET `d_funcion` = 'Menu Stock de Insumo Interno' , `id_tipo_funcion` = '43' WHERE `id` = '2730'; 
 INSERT INTO .`funcion` (`c_funcion`) VALUES ('MENU_STOCK_SCRAP'); 
 UPDATE .`funcion` SET `d_funcion` = 'Menu Stock de Scrap' , `id_tipo_funcion` = '88' WHERE `id` = '2731'; 

