<?php


class PersonaImpuestosAlicuotasController extends AppController {
    public $name = 'PersonaImpuestosAlicuotas';
    public $model = 'PersonaImpuestoAlicuota';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    
    
     /**
    * @secured(CONSULTA_PERSONA_IMPUESTO_ALICUOTA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $id_persona = $this->getFromRequestOrSession('PersonaImpuestoAlicuota.id_persona');
        $id_impuesto = $this->getFromRequestOrSession('PersonaImpuestoAlicuota.id_impuesto');        
        $id_tipo_impuesto = $this->getFromRequestOrSession('PersonaImpuestoAlicuota.id_tipo_impuesto');        
        $id_sistema_impuesto = $this->getFromRequestOrSession('PersonaImpuestoAlicuota.id_sistema_impuesto');        
		
		$autogenerado = $this->getFromRequestOrSession('PersonaImpuestoAlicuota.autogenerado');        
		
        $conditions = array(); 

        if($id_persona!="")
            array_push($conditions, array('PersonaImpuestoAlicuota.id_persona =' =>  $id_persona )); 
         
        
        if($id_impuesto!="")
            array_push($conditions, array('PersonaImpuestoAlicuota.id_impuesto =' =>  $id_impuesto));
            
        if($id_tipo_impuesto!="")
            array_push($conditions, array('Impuesto.id_tipo_impuesto =' =>  $id_tipo_impuesto));      
		
		if($autogenerado!="")
            array_push($conditions, array('Impuesto.autogenerado =' =>  $autogenerado));      
        
        
        if($id_sistema_impuesto!="")
            array_push($conditions, array('Impuesto.id_sistema =' =>  $id_sistema_impuesto));        
        
            
            $this->paginado = 0;
            
            
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' => array("Impuesto"=>array('Sistema')),
            'conditions' => $conditions,
            'limit' => 10,
            'page' => $this->getPageNum(),
        
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'DescuentoFamiliaPersona.id', "50%");
        $formBuilder->addHeader('id_cliente', 'DescuentoFamiliaPersona.id_cliente', "20%");
        $formBuilder->addHeader('descuento', 'DescuentoFamiliaPersona.descuento', "30%");
        // $formBuilder->addHeader('Email', 'cliente.email', "30%");
        // $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'descuento');
		$formBuilder->addField($this->model, 'id_cliente');
        // $formBuilder->addField($this->model, 'cuit');
        // $formBuilder->addField($this->model, 'email');
        // $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];

        
        
        if($data){
	        foreach($data as &$dato){
	        	
	        	
	            
	            $dato["PersonaImpuestoAlicuota"]["d_impuesto"] =  $dato["Impuesto"]["d_impuesto"];
				$dato["PersonaImpuestoAlicuota"]["id_sistema"] =  $dato["Impuesto"]["id_sistema"];
	            $dato["PersonaImpuestoAlicuota"]["d_sistema"] =  $dato["Impuesto"]["Sistema"]["d_sistema"];
	            $dato["PersonaImpuestoAlicuota"]["alicuota_impuesto_original"] =  $dato["Impuesto"]["alicuota"];
	            
	            if($dato["PersonaImpuestoAlicuota"]["sin_vencimiento"] == 1){
	            	$dato["PersonaImpuestoAlicuota"]["fecha_vigencia_desde"] = "";
	            	$dato["PersonaImpuestoAlicuota"]["fecha_vigencia_hasta"] = "";
	            	$dato["PersonaImpuestoAlicuota"]["d_sin_vencimiento"] = "Si";
	            	
	            }else{
	            	
	            	$dato["PersonaImpuestoAlicuota"]["d_sin_vencimiento"] = "No";
	            }
	            
	            
	            if($dato["PersonaImpuestoAlicuota"]["activo"] == 1){
	            	
	            	$dato["PersonaImpuestoAlicuota"]["d_activo"] = "Si";
	            }else{
	            	$dato["PersonaImpuestoAlicuota"]["d_activo"] = "No";
	            }
	            	
	            	
	            // $dato["PersonaImpuestosAlicuota"]["razon_social"] =  $dato["Persona"]["razon_social"];
	            unset($dato["Impuesto"]);
	          
	            
	        }
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
        		"content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
   

    /**
    * @secured(CONSULTA_PERSONA_IMPUESTO_ALICUOTA)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_add = '';
            
            try{
                if ($this->PersonaImpuestoAlicuota->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "El Impuesto ha sido creado exitosamente";
                    $tipo = EnumError::SUCCESS;
                    $id_add = $this->PersonaImpuestoAlicuota->id;
                   
                }else{
                    $mensaje = "Ha ocurrido un error, el Impuesto no ha podido ser creado.";
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error, el Impuesto NO ha podido ser creado. ".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
            
            
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" =>$id_add
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->PersonaImpuestoAlicuota->id = $id;
            
            try{ 
                if ($this->PersonaImpuestoAlicuota->saveAll($this->request->data)){
                    
                    $error = EnumError::SUCCESS;
                    $message = "El Impuesto ha sido modificado exitosamente";
                   
                }else{
                    $error = EnumError::ERROR;
                    $message = "El Impuesto No sido modificado";   
                    
                }
            }catch(Exception $e){
                    $error = EnumError::ERROR;
                    $message = "El Impuesto No sido modificado".$e->getMessage();   
                
            }
            
             if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "El Impuesto ha sido modificado exitosamente",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
            }else{
                $this->Session->setFlash('El Impuesto ha sido modificado exitosamente.', 'success');
                $this->redirect(array('controller' => 'OrdenCompra', 'action' => 'index'));
                
            } 
           
        }
        
      
    }
    
  
    /**
    * @secured(ABM_USUARIOS, READONLY_PROTECTED)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        
        $this->PersonaImpuestoAlicuota->id = $id;
       try{
       	
       	
       	$persona_impuesto_alicuota = $this->PersonaImpuestoAlicuota->find('first', array(
       			'conditions' => array('PersonaImpuestoAlicuota.id' => $id),
       			'contain' => false
       	));
       	
       	
       	
       	
       	
       	
       	if($persona_impuesto_alicuota){//si existe el ID que me pasaron
       		
       		$this->loadModel("ComprobanteImpuesto");
       		$id_persona = $persona_impuesto_alicuota["PersonaImpuestoAlicuota"]["id_persona"];
       		$id_impuesto= $persona_impuesto_alicuota["PersonaImpuestoAlicuota"]["id_impuesto"];
       		
       		
       		$comprobante_impuesto = $this->ComprobanteImpuesto->find('count', array(
       				'conditions' => array('ComprobanteImpuesto.id_impuesto' => $id_impuesto,'Comprobante.id_persona' => $id_persona),
       				'contain' => array('Comprobante')
       		));//busco si el impuesto a borrarle a la persona se asigno a algun comprobante.
       		
       		
       		
       		
       		if($comprobante_impuesto == 0){
	            if ($this->PersonaImpuestoAlicuota->delete()) {
	                
	                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
	                
	                $status = EnumError::SUCCESS;
	                $mensaje = "El Impuesto ha sido eliminado exitosamente.";
	             
	                
	            }else
	                 throw new Exception();
            
       		}else{
       			
       			
       			$status = EnumError::ERROR;
       			$mensaje = "ERROR: El Impuesto no puede borrarse ya que se han generado comprobantes con &eacute;l.";
       			
       		}
            
       	}else {
       		
       		$status = EnumError::ERROR;
       		$mensaje = "El impuesto que indic&oacute; no existe";
       	}
                 
                 
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
           
        }
        
        if($this->RequestHandler->ext == 'json'){
        	
        	$output = array(
        			"status" => $status,
        			"message" => $mensaje,
        			"content" => ""
        	); 
        	
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
    }
    

	 // public function getModel( $vista='normal')
	 // {    
         // $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
         // $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
         // $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
     // }
    
    
     // private function editforView($model,$vista)
	 // {  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
       // $this->set('model',$model);
       // Configure::write('debug',0);
       // $this->render($vista);    
     // }
	 
     
     /**
    * @secured(CONSULTA_PERSONA_IMPUESTO_ALICUOTA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
     private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
 
    }
    
    
     
    
   
    
}
?>