<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class DatoEmpresa extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'dato_empresa';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar un id.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
       
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	/*public $hasMany = array(
		'Region' => array(
			'className' => 'Region',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public $childRecordMessage = "Existe por lo menos una regi&oacute;n, predio, establecimiento o usuario relacionado/a con la jurisdicci&oacute;n.";
    
    */
    
    
    public $belongsTo = array(
        'Pais' => array(
            'className' => 'Pais',
            'foreignKey' => 'id_pais',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Provincia' => array(
            'className' => 'Provincia',
            'foreignKey' => 'id_provincia',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'TipoIva' => array(
            'className' => 'TipoIva',
            'foreignKey' => 'id_tipo_iva',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'TipoDocumento' => array(
            'className' => 'TipoDocumento',
            'foreignKey' => 'id_tipo_documento',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        
        'TipoEmpresa' => array(
            'className' => 'TipoEmpresa',
            'foreignKey' => 'id_tipo_empresa',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Moneda' => array(
            'className' => 'Moneda',
            'foreignKey' => 'id_moneda',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'ListaPrecio' => array(
            'className' => 'ListaPrecio',
            'foreignKey' => 'id_lista_precio',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    		'PuntoVentaMeli' => array(
    				'className' => 'PuntoVenta',
    				'foreignKey' => 'mercadolibre_id_punto_venta_defecto_pedido',
    				'conditions' => '',
    				'fields' => '',
    				'order' => ''
    		),
        'Moneda2' => array(
            'className' => 'Moneda',
            'foreignKey' => 'id_moneda2',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
		'Moneda3' => array(
            'className' => 'Moneda',
            'foreignKey' => 'id_moneda3',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    		
    		
         'CuentaContableResultadoEjercicio' => array(
            'className' => 'CuentaContableNoTree',
            'foreignKey' => 'id_cuenta_contable_resultado_ejercicio',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
         	'order' => ''
  
        ),
    	'CuentaContableChequeRechazado' => array(
    				'className' => 'CuentaContableNoTree',
    				'foreignKey' => 'id_cuenta_contable_cheque_rechazado',
    				'dependent' => false,
    				'conditions' => '',
    				'fields' => '',
    				'order' => ''
    				
    		),
    	'CuentaContableChequeCartera' => array(
    				'className' => 'CuentaContableNoTree',
    				'foreignKey' => 'id_cuenta_contable_cheque_cartera',
    				'dependent' => false,
    				'conditions' => '',
    				'fields' => '',
    				'order' => ''
    				
    		),
    	
    'CuentaContableCaja' => array(
    				'className' => 'CuentaContableNoTree',
    				'foreignKey' => 'id_cuenta_contable_caja',
    				'dependent' => false,
    				'conditions' => '',
    				'fields' => '',
    				'order' => ''
    				
    		),
   'CuentaContableResultadoNoAsignado' => array(
    				'className' => 'CuentaContableNoTree',
    				'foreignKey' => 'id_cuenta_resultado_no_asignado',
    				'dependent' => false,
    				'conditions' => '',
    				'fields' => '',
   					'order' => ''
    				
    		),
  'SituacionIb' => array(
    				'className' => 'SituacionIb',
    				'foreignKey' => 'id_situacion_ib',
    				'dependent' => false,
    				'conditions' => '',
    				'fields' => '',
    				
    		)
    		
 
        
        
   );
   
   
   public $hasMany = array(
        'DatoEmpresaImpuesto' => array(
            'className' => 'DatoEmpresaImpuesto',
            'foreignKey' => 'id_dato_empresa',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
         'PuntoVenta' => array(
            'className' => 'PuntoVenta',
            'foreignKey' => 'id_dato_empresa',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
);   

 public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
        
         
         
         $this->virtual_fields_view = array(
         
         "d_pais"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_provincia"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_moneda"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_moneda_simbolo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "moneda_default_cotizacion"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "moneda2_default_cotizacion"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "moneda3_default_cotizacion"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_moneda2"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_moneda3"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_moneda2_simbolo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_moneda3_simbolo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_lista_precio"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "EjercioContableActual"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null),
         );
         
         }     
              



public function getImpuestos($id_sistema,$id_tipo_impuesto){
    
    $dato_empresa = CakeSession::read("Empresa");
    
    $impuestos_sucursal = $this->DatoEmpresaImpuesto->find('all', array(
                                                'conditions' => array('Impuesto.id_tipo_impuesto' => $id_tipo_impuesto,'Impuesto.id_sistema' => $id_sistema,'DatoEmpresaImpuesto.id_dato_empresa' => $dato_empresa["DatoEmpresa"]["id"]),
                                                'contain' =>array('Impuesto')
                                                )); 
        
    
   return $impuestos_sucursal;
   
   
}

public function getDatoEmpresaImpuesto($id){
    
    
    
     $impuesto = $this->DatoEmpresaImpuesto->find('first', array(
                                                'conditions' => array('DatoEmpresaImpuesto.id' => $id),
                                                'contain' =>false
                                                ));
                                                 
        
    
     return $impuesto;   
    
    
}


public function getImpuestosGlobalCliente(){
    
    
    $impuesto = $this->DatoEmpresaImpuesto->find('first', array(
                                                'conditions' => array('Impuesto.global_cliente' => 1),
                                                'contain' =>array("Impuesto")
                                                ));
                                                 
        
    
     return $impuesto;    
     
     
     
}   
 
 
 public function getImpuestosGlobalProveedor(){
     
      $impuesto = $this->DatoEmpresaImpuesto->find('all', array(
                                                'conditions' => array('Impuesto.global_proveedor' => 1),
                                                'contain' =>array("Impuesto")
                                                ));
                                                 
        
    
     return $impuesto;    
     
     
 }   
 
 
 public function getNombreCampoMonedaPorIdMoneda($id_moneda){
     /*Devuelve el nombre del campo de DatoEmpresa buscando por id_moneda.
        Ejemplo le paso id_moneda = 1 (pesos) y me devuelve id_moneda2 (secundaria) SOLO UN EJEMPLO CON UNA PARAMETRIZACION BASE: DOLares SECU:Pesos
     */
     
      $dato_empresa = CakeSession::read("Empresa");
      
      $dato_empresa_record = $this->find('first', array(
                                                'conditions' => array('DatoEmpresa.id' => $dato_empresa["DatoEmpresa"]["id"]),
                                                'contain' =>false
                                                ));
                                                
     if($dato_empresa["DatoEmpresa"]["id_moneda"] == $id_moneda)
        return "id_moneda";
     elseif($dato_empresa["DatoEmpresa"]["id_moneda2"] == $id_moneda)   
        return "id_moneda2";                                         
     elseif($dato_empresa["DatoEmpresa"]["id_moneda3"] == $id_moneda)   
        return "id_moneda3";   
     
     
 }
 
    function getMonedaByNombreCampo($campo_nombre){
         
      $dato_empresa = CakeSession::read("Empresa");
      
      $dato_empresa_record = $this->find('first', array(
                                                'conditions' => array('DatoEmpresa.id' => $dato_empresa["DatoEmpresa"]["id"]),
                                                'contain' =>false
                                                ));
                                                
      return $dato_empresa_record["DatoEmpresa"][$campo_nombre];                                           
         
     }
     
     
      function getIdMonedaCorriente(){
        
        $dato_empresa = CakeSession::read("Empresa");
      
      $dato_empresa_record = $this->find('first', array(
                                                'conditions' => array('DatoEmpresa.id' => $dato_empresa["DatoEmpresa"]["id"]),
                                                'contain' =>false
                                                ));
                                                
      return $dato_empresa_record["DatoEmpresa"]["id_moneda2"];                                           
          
        
      }
      
      
      
      function getAppPath(){
      	
      	$dato_empresa = CakeSession::read("Empresa");
      	
      	$dato_empresa_record = $this->find('first', array(
      			'conditions' => array('DatoEmpresa.id' => $dato_empresa["DatoEmpresa"]["id"]),
      			'contain' =>false
      	));
      	
      	return $dato_empresa_record["DatoEmpresa"]["app_path"];
      	
      	
      }
      
      
      public function index(&$dato){
      	
      	$ejercicio_contable_obj = ClassRegistry::init(EnumModel::EjercicioContable);
      	$cpvn_obj= ClassRegistry::init(EnumModel::ComprobantePuntoVentaNumero);
      	$dato_empresa_obj= ClassRegistry::init(EnumModel::DatoEmpresa);
      	$modulo_obj= ClassRegistry::init(EnumModel::Modulo);
      	$tipo_comprobante_persona_class = ClassRegistry::init('TipoComprobantePersona');
      	$sistema_moneda_enlazada_class = ClassRegistry::init('SistemaMonedaEnlazada');
      	$entidad_class = ClassRegistry::init('Entidad');
      	
      	
      	$cpvn = $cpvn_obj->find('all', array(
      			//'conditions' => array('ComprobanteItem.id_comprobante' => $id,'ComprobanteItem.activo'=>1),
      			'contain' => false
      	));
      	
      	$dato['ComprobantePuntoVentaNumero'] = array();
      	$dato['Modulo'] = $modulo_obj->getModulos();
      	$dato['EjercioContableActual'] =  $ejercicio_contable_obj->GetEjercicioPorFecha(date("Y-m-d"),0,1)["EjercicioContable"];//le pido el actual segun la fecha;      	
      	
      	
		$dato["SistemaMonedaEnlazada"]["content"] =  $sistema_moneda_enlazada_class->find('all',array("contain"=>false));
      	$dato["TipoComprobantePersona"]["content"] =  $tipo_comprobante_persona_class->find('all',array("contain"=>false));
		$dato["Entidad"]["content"] =  $entidad_class->find('all',array("contain"=>false));
		
		
      	if(isset($dato['Pais']))
      		$dato['DatoEmpresa']['d_pais']	=	$dato['Pais']['d_pais'];
      		
      		
      	if(isset($dato['Provincia'])){
      		$dato['DatoEmpresa']['d_provincia']	=	$dato['Provincia']['d_provincia'];
      	}	
      			$aux = array();
      			
      			foreach($cpvn as $cpvnumero){
      				
      				
      				array_push($aux, $cpvnumero["ComprobantePuntoVentaNumero"]);
      			}
      			
      			
      			
      			$dato['ComprobantePuntoVentaNumero']= $aux;
      			
      			if(isset($dato['TipoIva']))
      				$dato['TipoIva']['d_tipo_iva']=$dato['TipoIva']['d_tipo_iva'];
      				
      				if(isset($dato['TipoDocumento']))
      					$dato['TipoDocumento']['d_tipo_documento']=$dato['TipoDocumento']['d_tipo_documento'];
      					
      					if(isset($dato['Moneda'])){//esta es la BASE
      						$dato['DatoEmpresa']['d_moneda']= $dato['Moneda']['d_moneda'];
      						$dato['DatoEmpresa']['d_moneda_simbolo']=$dato['Moneda']['simbolo'];
      						$dato['DatoEmpresa']['moneda_default_cotizacion']=$dato['Moneda']['cotizacion'];
      					}
      					
      					if(isset($dato['Moneda2'])){
      						$dato['DatoEmpresa']['d_moneda2']=$dato['Moneda2']['d_moneda'];
      						$dato['DatoEmpresa']['d_moneda2_simbolo']=$dato['Moneda2']['simbolo'];
      						$dato['DatoEmpresa']['moneda2_default_cotizacion']=$dato['Moneda2']['cotizacion'];
      					}
      					
      					if(isset($dato['Moneda3'])){
      						$dato['DatoEmpresa']['d_moneda3']=$dato['Moneda3']['d_moneda'];
      						$dato['DatoEmpresa']['d_moneda3_simbolo']=$dato['Moneda3']['simbolo'];
      						$dato['DatoEmpresa']['moneda3_default_cotizacion']=$dato['Moneda3']['cotizacion'];
      					}
      					
      					if(isset($dato['ListaPrecio']))
      						$dato['DatoEmpresa']['d_lista_precio']=$dato['ListaPrecio']['d_lista_precio'];
      						
      						
      						
      						if(isset($dato['CuentaContableResultadoEjercicio']))
      							$dato['DatoEmpresa']['d_cuenta_contable_resultado_ejercicio']=$dato['CuentaContableResultadoEjercicio']['d_codigo_cuenta_contable'];
      							
      							
      							if(isset($dato['CuentaContableChequeRechazado']))
      								$dato['DatoEmpresa']['d_cuenta_contable_cheque_rechazado']=$dato['CuentaContableChequeRechazado']['d_codigo_cuenta_contable'];
      								
      								
      								if(isset($dato['CuentaContableChequeCartera']))
      									$dato['DatoEmpresa']['d_cuenta_contable_cheque_cartera']=$dato['CuentaContableChequeCartera']['d_codigo_cuenta_contable'];
      									
      									
      									if(isset($dato['CuentaContableCaja']))
      										$dato['DatoEmpresa']['d_cuenta_contable_caja']=$dato['CuentaContableCaja']['d_codigo_cuenta_contable'];
      										
      										
      										if(isset($dato['CuentaContableResultadoNoAsignado']))
      											$dato['DatoEmpresa']['d_cuenta_contable_resultado_no_asignado']=$dato['CuentaContableResultadoNoAsignado']['d_codigo_cuenta_contable'];
      											
      											unset($dato['DatoEmpresa']['email_password_envio']);//quito este campo por seguridad se deberia hashear
      											unset($dato['Provincia']);
      											unset($dato['Pais']);
      											unset($dato['TipoIva']);
      											unset($dato['TipoDocumento']);
      											unset($dato['PuntoVenta']);
      											unset($dato['Moneda']);
      											unset($dato['ListaPrecio']);
      											unset($dato['TipoEmpresa']);
      											unset($dato['DatoEmpresaImpuesto']);
      											unset($dato['Moneda2']);
      											unset($dato['Moneda3']);
      											unset($dato['CuentaContableResultadoEjercicio']);
      											unset($dato['CuentaContableChequeRechazado']);
      											unset($dato['CuentaContableChequeCartera']);
      											unset($dato['CuentaContableCaja']);
      											unset($dato['CuentaContableResultadoNoAsignado']);
      											
      	
      											return $dato;
      	
      	
      }
 
 

  
}
