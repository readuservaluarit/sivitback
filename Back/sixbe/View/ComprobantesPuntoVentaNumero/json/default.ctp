<?php
        //CONFIG  CON MODE DISPLAYED CELLS
        include(APP.'View'.DS.'ComprobantesPuntoVentaNumero'.DS.'json'.DS.'comprobante_punto_venta_numero.ctp');
           
        $model["id_tipo_comprobante"]["show_grid"] = 1;
        $model["id_tipo_comprobante"]["header_display"] = "Id.";
        $model["id_tipo_comprobante"]["order"] = "1";
        $model["id_tipo_comprobante"]["width"] = "4";
        $model["id_tipo_comprobante"]["text_colour"] = "#330055";
		   
        $model["d_tipo_comprobante"]["show_grid"] = 1;
        $model["d_tipo_comprobante"]["header_display"] = "Comprobante";
        $model["d_tipo_comprobante"]["order"] = "2";
        $model["d_tipo_comprobante"]["width"] = "18";
        $model["d_tipo_comprobante"]["text_colour"] = "#660088";
        
        $model["d_punto_venta"]["show_grid"] = 1;
        $model["d_punto_venta"]["header_display"] = "Punto Venta";
        $model["d_punto_venta"]["order"] = "3";
        $model["d_punto_venta"]["width"] = "4";
        $model["d_punto_venta"]["text_colour"] = "#005522";
        
        $model["d_sistema"]["show_grid"] = 1;
        $model["d_sistema"]["header_display"] = "M&oacute;dulo";
        $model["d_sistema"]["order"] = "4";
        $model["d_sistema"]["width"] = "6";
        $model["d_sistema"]["text_colour"] = "#0000FF";
        
        $model["por_defecto"]["show_grid"] = 1;
        $model["por_defecto"]["header_display"] = "Por Defecto";
        $model["por_defecto"]["order"] = "5";
        $model["por_defecto"]["width"] = "5";
        $model["por_defecto"]["text_colour"] = "#220000";
		
		$model["factura_electronica"]["show_grid"] = 1;
        $model["factura_electronica"]["header_display"] = "FC Electronica";
        $model["factura_electronica"]["order"] = "6";
        $model["factura_electronica"]["width"] = "5";
        $model["factura_electronica"]["text_colour"] = "#009900";
		$model["factura_electronica"]["type"] = "string";
 
        // $model["d_tipo_comprobante_punto_venta_numero"]["show_grid"] = 1;
        // $model["d_tipo_comprobante_punto_venta_numero"]["header_display"] = "Tipo";
        // $model["d_tipo_comprobante_punto_venta_numero"]["order"] = "7";
        // $model["d_tipo_comprobante_punto_venta_numero"]["width"] = "5";
        // $model["d_tipo_comprobante_punto_venta_numero"]["text_colour"] = "#0000FF";
 
        //$model["permite_modificar_nro_comprobante"]["show_grid"] = 1;
        //$model["permite_modificar_nro_comprobante"]["header_display"] = "Permite Mod. Nro.Comp.";
        //$model["permite_modificar_nro_comprobante"]["order"] = "7";
        //$model["permite_modificar_nro_comprobante"]["width"] = "5";
        //$model["permite_modificar_nro_comprobante"]["text_colour"] = "#0000FF";
		
		$model["numero_actual"]["show_grid"] = 1;
        $model["numero_actual"]["header_display"] = "Numero Actual";
        $model["numero_actual"]["order"] = "8";
        $model["numero_actual"]["width"] = "5";
        $model["numero_actual"]["text_colour"] = "#AA0000";
		$model["numero_actual"]["type"] = "string";
		
		$output = array(
					"status" =>EnumError::SUCCESS,
					"message" => "ComprobantePuntoVentaNumero",
					"content" => $model
				);
        
        echo json_encode($output);
?>