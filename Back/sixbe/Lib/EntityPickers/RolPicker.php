<?php

class RolPicker extends EntityPicker{
    
    public $model = "Rol";
    
    public $pickerTitle = "Selección de Rol";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Código", "field" => "codigo", "width" => "30%", "keyOrder" => "codigo"),
                                  array("title" => "Descripción", "field" => "detalle", "width" => "50%", "keyOrder" => "detalle")
                                );

    public function getPickerConditions($busqueda){
        
        return array('Rol.detalle LIKE' => '%' . $busqueda['bus_unico'] . '%');
    }

}