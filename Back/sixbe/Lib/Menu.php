<?php


App::import('Lib', 'SecurityManager');


class Menu{
     private $menu = array(
                        // array(//////////////////////////////MODULO/////////////////////////
                              // 'caption' => 'Inicio',
                              // 'id' => 'mnuInicio',
                              // 'img' => 'icons/packs/fugue/16x16/dashboard.png',
                              // 'root' =>true,
                              // 'action' => array('form' =>  'CarpeDiem.App.frmInicio',
												// 'controller' => 'Usuarios', 'action' => 'home', 'form_desktop' => null),
                              // 'ajax' => false,
                              // 'childrens' => null,
                              // 'functions' => null
                             // ),
					/*		 
                        array(//////////////////////////////MODULO/////////////////////////
                              'caption' => 'Inicio',
                              'id' => 'mnuSeguridad',
                              'img' => 'icons/packs/fugue/16x16/user-white.png',
                              'root' =>true,
                              'action' => null,
                              'ajax' => false,
                        	  'html_menu'=>true,
                              'functions' => array('RAIZ_USUARIO'),
                              'childrens' => array(
                                                    array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Usuarios',
                                                    	   'html_menu'=>true,
                                                           'id' => 'mnuUsuarios',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmUsuario',
																			 'controller' => 'Usuarios', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_USUARIO'),
                                                    	   'modules'=>array()
                                                         ),
												   array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Nuevo Usuario',
                                                    	   'html_menu'=>true,
                                                           'id' => 'mnuUsuarioA',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmUsuarioA',
																			 'controller' => 'Usuarios', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('ADD_USUARIO'),
                                                    	   'modules'=>array()
                                                         ),
													array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Roles',
                                                     		'html_menu'=>true,
                                                           'id' => 'mnuRoles',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmRoles',
																			 'controller' => 'Roles', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_ROL'),
                                                         ),
													array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Cerrar Sesion',
                                                           'id' => 'mnuCerrarSession',
                                                           'action' => array('form' =>  '',
																			 'controller' => 'Usuarios', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_USUARIO'),
                                                         ),
													   array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Cerrar Sistema',
                                                           'id' => 'mnuCerrar',
                                                           'action' => array('form' =>  '',
																			 'controller' => 'Usuarios', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_USUARIO'),
                                                         ),
                                                )
                             ),*/
                             array(//////////////////////////////MODULO/////////////////////////
                                  'caption' => 'Art&iacute;culos',
                                  'id' => 'mnuArticulos',
                                  'img' => 'icons/packs/fugue/16x16/user-white.png',
                                  'root' =>true,
                                  'action' => null,
                                  'ajax' => false,
                                  'functions' => array('RAIZ_ARTICULOS'),
                                  'childrens' => array(
														array(
															   'img' => 'icons/packs/fugue/16x16/application-form.png',
															   'caption' => '',//vacio =>Invisible permite menues que los usa el sistema -> ej Picker Ventas
															   'id' => 'mnuArticulosGeneral',
															   'action' => array('form' =>  'CarpeDiem.App.frmArticulo',
																				 'listarTipo' => EnumListarTipo::Picker, 
																				 'controller' => 'Articulo', /*'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::Producto) ),*/
																				 'action' => 'index',
																				 'context_menu'=> array( array('control' => 'gv', 'options' => array(
																																		array('text' =>'Ver &amp;Stock','type' => 'text',
																																				'action' =>'msVerStock_Click',/*MODIFICAR DESPUES DEL DEPLOY POR MENU_STOCK */
																																				'functions' => array('MENU_STOCK_RAPIDO') ),/*esto es para q todavia valmec no tenga disponible esta opcion*/
																																		// array('text' =>'Ver &amp;Stock','type' => 'text',
																																				// 'action' =>'msVerAsientos_Click',
																																				// 'functions' => array('MENU_STOCK') ),
																																				)
																												)
																										)
															   
																				),
															   'ajax' => true,
															   'functions' => null,
															   ),
														array(
															'img' => 'icons/packs/fugue/16x16/application-form.png',
															'caption' => '',//vacio =>Invisible permite menues que los usa el sistema -> ej Atajo: Modificacion de Clientes RAPIDA
															'id' => 'mnuListaPrecioPicker',
															'action' => array('form' =>  'CarpeDiem.App.frmListaPrecio',
																	
																	'controller' => 'ListaPrecios', 'action' => 'index'),
															'ajax' => true,
															'functions' => array(),
															),	
                             
                                                        array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Productos(Manufacturados)',
                                                           'id' => 'mnuProductoList',
                                                           'action' => false,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_PRODUCTO'),
                                                           'childrens' =>array(                                    
																				array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Listado de Productos(Manufacturados)',
																					   'id' => 'mnuProducto',
																					   'action' => array('form' =>  'CarpeDiem.App.frmArticulo',
																										 'controller' => 'Productos','form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::Producto)
																										 
																										 		),
																										 'action' => 'index',
																										'context_menu'=> array( array('control' => 'gv', 'options' => array(
																																											array('text' =>'Ver Stock',
																																											'type' => 'text',
																																												  'action' =>'msVerStock_Click',
																																												  'functions' => array('MENU_STOCK') ), 
																																								)
																													) )
																										 ),
																					   'ajax' => true,
																					   'functions' => array('MENU_PRODUCTO')
																					),
                                                                               array(
                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                               'caption' => 'Alta Productos(Manufacturados)',
                                                                               'id' => 'mnuProductoA',
                                                                               'action' => array('form' =>  'CarpeDiem.App.frmArticuloA',
                                                                                                 'controller' => 'Productos','form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::Producto),
                                                                                                 		'id_producto_clasificacion' => array(EnumProductoClasificacion::Articulo_REAL)),
                                                                                                 'action' => 'index'),
                                                                               'ajax' => true,
                                                                               'functions' => array('ADD_PRODUCTO'),
                                                                             )
                                                         ,)
                                                         ),

                                                           array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Producto Retail',
                                                           'id' => 'mnuProductoRetailListado',
                                                           'action' => false,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_PRODUCTO_RETAIL'),
                                                           'childrens' =>array(
																				 array(
																						   'img' => 'icons/packs/fugue/16x16/application-form.png',
																						   'caption' => 'Listado de Producto Retail',
																						   'id' => 'mnuProductoRetail',
																						   'action' => array('form' =>  'CarpeDiem.App.frmArticulo',
																											 
																											 'controller' => 'ProductosRetail','form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::MRETAIL) ),
																											 'action' => 'index',
																										   		'context_menu'=> array( array('control' => 'gv', 'options' => array(
																										   				array('text' =>'Ver Stock',
																										   						'type' => 'text',
																										   						'action' =>'msVerStock_Click',
																										   						'functions' => array('MENU_STOCK') ),
																										   		)
																										   		) )
																						   ),
																						   'ajax' => true,
																						   'functions' => array('MENU_PRODUCTO_RETAIL')
																					),
														   
                                                                                 array(
                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                               'caption' => 'Alta Productos Retail',
                                                                               'id' => 'mnuProductoRetailA',
                                                                               'action' => array('form' =>  'CarpeDiem.App.frmArticuloA',
                                                                                                 'controller' => 'ProductosRetail','form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::MRETAIL),
                                                                                                 		'id_producto_clasificacion' => array(EnumProductoClasificacion::Articulo_REAL)),
                                                                                                 'action' => 'index'),
                                                                               'ajax' => true,
                                                                               'functions' => array('ADD_PRODUCTO_RETAIL'),
                                                                             )
                                                           )
                                                         ),
                                                    array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Componentes / Semielaborados',
                                                           'id' => 'mnuComponenteList',
                                                           'action' => false,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_COMPONENTE'),
                                                           'childrens' =>array(
														   
																				 array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Listado de Componentes / Semielaborados',
																					   'id' => 'mnuComponente',
																					   'action' => array('form' =>  'CarpeDiem.App.frmArticulo',
																										 
																										 'controller' => EnumController::Componentes,'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::Componente)),
																										 'action' => 'index',
																									   		'context_menu'=> array( array('control' => 'gv', 'options' => array(
																									   				array('text' =>'Ver Stock',
																									   						'type' => 'text',
																									   						'action' =>'msVerStock_Click',
																									   						'functions' => array('MENU_STOCK') ),
																									   		)
																									   		) )
																					   					
																					   
																					   ),
																					   'ajax' => true,
																					   'functions' => array('MENU_COMPONENTE')
																						),
                                                                                 array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Alta Componentes / Semielaborados',
																					   'id' => 'mnuComponenteA',
																					   'action' => array('form' =>  'CarpeDiem.App.frmArticuloA',
																					   		'controller' => EnumController::Componentes,'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::Componente),'id_producto_clasificacion' => array(EnumProductoClasificacion::Articulo_REAL)),
																										 'action' => 'index'),
																					   'ajax' => true,
																					   'functions' => array('ADD_COMPONENTE'),
																					 )
                                                           )
                                                         ),
                                                       array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Materias Primas',
                                                           'id' => 'mnuMateriaPrimaList',
                                                           'action' => false,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_MATERIA_PRIMA'),
                                                            'childrens' =>array(
															
																				   array(
																						   'img' => 'icons/packs/fugue/16x16/application-form.png',
																						   'caption' => 'Listado de Materias Primas',
																						   
																						   'id' => 'mnuMateriaPrima',
																						   'action' => array('form' =>  'CarpeDiem.App.frmArticulo',
																											 'controller' => EnumController::MateriasPrimas,
																											 'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::MateriaPrima),), 
																						   					'action' => 'index',
																									   		'context_menu'=> array( array('control' => 'gv', 'options' => array(
																									   				array('text' =>'Ver Stock',
																									   						'type' => 'text',
																									   						'action' =>'msVerStock_Click',
																									   						'functions' => array('MENU_STOCK') ),
																									   		)
																									   		) )
																						   ),
																						   'ajax' => true,
																						   'functions' => array('MENU_MATERIA_PRIMA')
																						  ),
                                                                                 array(
                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                               'caption' => 'Alta Materias Primas',
                                                                               'id' => 'mnuMateriaPrimaA',
                                                                               'action' => array('form' =>  'CarpeDiem.App.frmArticuloA',
                                                                               		'controller' => EnumController::MateriasPrimas,'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::MateriaPrima),'id_producto_clasificacion' => array(EnumProductoClasificacion::Articulo_REAL)),
                                                                                                 'action' => 'index'),
                                                                               'ajax' => true,
                                                                               'functions' => array('ADD_MATERIA_PRIMA'),
                                                                             )
                                                           )
                                                         ),
                                                         array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Packaging',
                                                           'id' => 'mnuPackagingList',
                                                           'action' => false,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_PACKAGING'),
                                                            'childrens' =>array(
																				    array(
																						   'img' => 'icons/packs/fugue/16x16/application-form.png',
																						   'caption' => 'Listado de Packaging',
																						   'id' => 'mnuPackaging',
																						   'action' => array('form' =>  'CarpeDiem.App.frmArticulo','form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::MPE) ),
																											 'controller' => 'Packaging', 'action' => 'index',
																						   
																						   		'context_menu'=> array( array('control' => 'gv', 'options' => array(
																						   				array('text' =>'Ver Stock',
																						   						'type' => 'text',
																						   						'action' =>'msVerStock_Click',
																						   						'functions' => array('MENU_STOCK') ),
																						   		)
																						   		) )
																						   ),
																						   'ajax' => true,
																						   'functions' => array('MENU_PACKAGING')
																						   ),
                                                                                 array(
                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                               'caption' => 'Alta Packaging',
                                                                               'id' => 'mnuPackagingA',
                                                                               'action' => array('form' =>  'CarpeDiem.App.frmArticuloA',
                                                                                                 'controller' => EnumController::Packaging,'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::MPE) ),
                                                                                                 'action' => 'index'),
                                                                               'ajax' => true,
                                                                               'functions' => array('ADD_PACKAGING'),
                                                                             )
                                                           )
                                                         ), 
                                                      
                                                         array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Bienes de uso',
                                                           'id' => 'mnuBienUso',
                                                           'action' => false,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_BIEN_USO'),
                                                           
                                                            'childrens' =>array(
															
																				array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Listado de Bienes de uso',
																					   'id' => 'mnuBienUso',
																					   'action' => array('form' =>  'CarpeDiem.App.frmArticulo','form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::BIENUSO) ),
																										 'controller' => 'BienesUso', 'action' => 'index',
																									   		'context_menu'=> array( array('control' => 'gv', 'options' => array(
																									   				array('text' =>'Ver Stock',
																									   						'type' => 'text',
																									   						'action' =>'msVerStock_Click',
																									   						'functions' => array('MENU_STOCK') ),
																									   		)
																									   		) )
																					   ),
																					   'ajax' => true,
																					   'functions' => array('MENU_BIEN_USO')
																					   ),
                                                                                 array(
                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                               'caption' => 'Alta Bien de uso',
                                                                               'id' => 'mnuBienUsoA',
                                                                               'action' => array('form' =>  'CarpeDiem.App.frmArticuloA',
                                                                                                 'controller' => EnumController::BieneUso,'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::BIENUSO) ),
                                                                                                 'action' => 'index'),
                                                                               'ajax' => true,
                                                                               'functions' => array('ADD_BIEN_USO'),
                                                                             )
                                                           )
                                                         ),
                                                         array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Insumo Interno',
                                                           'id' => 'mnuInsumoInterno',
                                                           'action' => false,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_INSUMO_INTERNO'),
                                                            'childrens' =>array(
																				array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Listado de Insumos Internos',
																					   'id' => 'mnuInsumoInterno',
																					   'action' => array('form' =>  'CarpeDiem.App.frmArticulo','form_param'=>array('id_producto_tipo' => array(EnumProductoTipo::INSUMOINTERNO) ),
																										 'controller' => 'InsumosInterno', 'action' => 'index',
																					   
																										   		'context_menu'=> array( array('control' => 'gv', 'options' => array(
																										   				array('text' =>'Ver Stock',
																										   						'type' => 'text',
																										   						'action' =>'msVerStock_Click',
																										   						'functions' => array('MENU_STOCK') ),
																										   		)
																										   		) )
																					   ),
																					   'ajax' => true,
																					   'functions' => array('MENU_INSUMO_INTERNO')	
																					  ),
                                                                                 array(
                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                               'caption' => 'Alta de Insumo Interno',
                                                                               'id' => 'mnuInsumoInternoA',
                                                                               'action' => array('form' =>  'CarpeDiem.App.frmArticuloA',
                                                                                                 'controller' => EnumController::InsumosInterno,'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::INSUMOINTERNO) ),
                                                                                                 'action' => 'index'),
                                                                               'ajax' => true,
                                                                               'functions' => array('ADD_INSUMO_INTERNO'),
                                                                             )
                                                           )
                                                         ),
                                                         
                                                           array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Servicios',
                                                           'id' => 'mnuServicios',
                                                           'action' => false,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_SERVICIO'),
                                                            'childrens' =>array(
															
																				array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Listado de Servicios',
																					   'id' => 'mnuServicios',
																					   'action' => array('form' =>  'CarpeDiem.App.frmArticulo','form_param'=>array('id_producto_tipo' => array(EnumProductoTipo::Servicio) ),
																										 'controller' => EnumController::Servicios, 'action' => 'index'),
																					   'ajax' => true,
																					   'functions' => array('MENU_SERVICIO')
																					  ),                                                                                 array(
                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                               'caption' => 'Alta de Servicio',
                                                                               'id' => 'mnuServicioA',
                                                                               'action' => array('form' =>  'CarpeDiem.App.frmArticuloA',
                                                                                                 'controller' => EnumController::Servicios,'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::Servicio) ),
                                                                                                 'action' => 'index'),
                                                                               'ajax' => true,
                                                                               'functions' => array('ADD_SERVICIO'),
                                                                             )
                                                           )
                                                         ),
														 
														 array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Scrap',
                                                           'id' => 'mnuScrap',
                                                           'action' => false,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_SCRAP'),
                                                            'childrens' =>array(
															
																				array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Listado de Scrap',
																					   'id' => 'mnuScrap',
																					   'action' => array('form' =>  'CarpeDiem.App.frmArticulo','form_param'=>array('id_producto_tipo' => array(EnumProductoTipo::SCRAP) ),
																										 'controller' => EnumController::Scrap, 'action' => 'index',
																					   
																					   		'context_menu'=> array( array('control' => 'gv', 'options' => array(
																					   				array('text' =>'Ver Stock',
																					   						'type' => 'text',
																					   						'action' =>'msVerStock_Click',
																					   						'functions' => array('MENU_STOCK') ),
																					   		)
																					   		) )
																					   
																					   ),
																					   'ajax' => true,
																					   'functions' => array('MENU_SCRAP')
																					  ),                                                                                 array(
                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                               'caption' => 'Alta de Scrap',
                                                                               'id' => 'mnuScrapA',
                                                                               'action' => array('form' =>  'CarpeDiem.App.frmArticuloA',
                                                                                                 'controller' => EnumController::Scrap,'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::SCRAP) ),
                                                                                                 'action' => 'index'),
                                                                               'ajax' => true,
                                                                               'functions' => array('ADD_SCRAP'),
                                                                             )
                                                           )
                                                         ),
														 
                                                         
                                                          array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Tablas',
                                                                                   'id' => 'mnuTablasProducto',
                                                                                   'action' => false,
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_CATEGORIA'),
                                                                                   'childrens' => array(
                                                                                                           array('img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                                           'caption' => 'Categorias de Art&iacute;culos',
                                                                                                           'id' => 'mnuCategoria',
                                                                                                           'action' => array('form' =>  'CarpeDiem.App.frmCategoria',
                                                                                                                         'controller' => 'Categorias', 'action' => 'index'),
                                                                                                           'ajax' => true,
                                                                                                           'functions' => array('MENU_CATEGORIA'),
                                                                                                           'childrens' => array()
                                                                                                            ),array(
                                                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                                                   'caption' => 'Familia de Articulos',
                                                                                                                   'id' => 'mnuFamiliaProductos',
                                                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmFamiliaProducto',
                                                                                                                                     'controller' => 'FamiliasProducto', 'action' => 'index'),
                                                                                                                   'ajax' => true,
                                                                                                                   'functions' => array('MENU_FAMILIA_PRODUCTO'),
                                                                                                                 ),
                                                                                                                    array(
                                                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                                                   'caption' => 'Unidades',
                                                                                                                   'id' => 'mnuUnidades',
                                                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmUnidad',
                                                                                                                                     'controller' => 'Unidades', 'action' => 'index'),
                                                                                                                   'ajax' => true,
                                                                                                                   'functions' => array('MENU_PRODUCTO'),
                                                                                                                 ),
                                                                                                                  array(
                                                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                                                   'caption' => 'Marcas',
                                                                                                                   'id' => 'mnuMarca',
                                                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmMarca',
                                                                                                                                     'controller' => 'Marcas', 'action' => 'index'),
                                                                                                                   'ajax' => true,
                                                                                                                   'functions' => array('MENU_MARCA'),
                                                                                                                 ),
						                                                                                   		array(
						                                                                                   				'img' => 'icons/packs/fugue/16x16/application-form.png',
						                                                                                   				'caption' => 'Talles',
						                                                                                   				'id' => 'mnuTalle',
						                                                                                   				'action' => array('form' =>  'CarpeDiem.App.frmTalle',
						                                                                                   						'controller' => 'Talles', 'action' => 'index'),
						                                                                                   				'ajax' => true,
						                                                                                   				'functions' => array('MENU_TALLE'),
								                                                                                   		),
									                                                                                   		array(
									                                                                                   				'img' => 'icons/packs/fugue/16x16/application-form.png',
									                                                                                   				'caption' => 'Nuevo Talle',
									                                                                                   				'id' => 'mnuTalleA',
									                                                                                   				'action' => array('form' =>  'CarpeDiem.App.frmTalleA',
									                                                                                   						'controller' => 'Talles', 'action' => 'index'),
									                                                                                   				'ajax' => true,
									                                                                                   				'functions' => array('ADD_TALLE'),
									                                                                                   				'childrens' => array()
									                                                                                   		),
						                                                                                   		array(
						                                                                                   				'img' => 'icons/packs/fugue/16x16/application-form.png',
						                                                                                   				'caption' => 'Colores',
						                                                                                   				'id' => 'mnuColor',
						                                                                                   				'action' => array('form' =>  'CarpeDiem.App.frmColor',
						                                                                                   						'controller' => 'Colores', 'action' => 'index'),
						                                                                                   				'ajax' => true,
						                                                                                   				'functions' => array('MENU_COLOR'),
						                                                                                   		),
						                                                                                   		array(
						                                                                                   				'img' => 'icons/packs/fugue/16x16/application-form.png',
						                                                                                   				'caption' => 'Nuevo Color',
						                                                                                   				'id' => 'mnuColorA',
						                                                                                   				'action' => array('form' =>  'CarpeDiem.App.frmColorA',
						                                                                                   						'controller' => 'Colores', 'action' => 'index'),
						                                                                                   				'ajax' => true,
						                                                                                   				'functions' => array('ADD_COLOR'),
						                                                                                   				'childrens' => array()
						                                                                                   		),
							                                                                                   		array(
							                                                                                   				'img' => 'icons/packs/fugue/16x16/application-form.png',
							                                                                                   				'caption' => 'Tipos de Art&iacute;culos',
							                                                                                   				'id' => 'mnuProductoTipo',
							                                                                                   				'action' => array('form' =>  'CarpeDiem.App.frmProductoTipo',
							                                                                                   						'controller' => 'ProductosTipo', 'action' => 'index'),
							                                                                                   				'ajax' => true,
							                                                                                   				'functions' => array('CONSULTA_PRODUCTO_TIPO'),
							                                                                                   				'childrens' => array()
							                                                                                   		),
                                                                                   		
						                                                                                   		array(/*Para que Levante el Alta es un item vacio*/
						                                                                                   				'img' => 'icons/packs/fugue/16x16/application-form.png',
						                                                                                   				'caption' => '',
						                                                                                   				'id' => 'mnuProductoTipoA',
						                                                                                   				'action' => array('form' =>  'CarpeDiem.App.frmProductoTipo',
						                                                                                   						'controller' => 'ProductosTipo', 'action' => 'index'),
						                                                                                   				'ajax' => true,
						                                                                                   				'functions' => array('CONSULTA_PRODUCTO_TIPO'),
						                                                                                   				'childrens' => array()
						                                                                                   		),
						                                                                                   								
                                                                                    
                                                                                    
                                                                                    )
                                                                                   
                                                                                   
                                                                                   
                                                          ),
                                                         
                                                         
																								 
                                                         
                                                     
                                                       )
                             ),
                                                       
						  array(//////////////////////////////MODULO/////////////////////////
								  'caption' => 'Calidad',
								  'id' => 'mnuGestionCalidad',
								  'img' => 'icons/packs/fugue/16x16/user-white.png',
								  'root' =>true,
								  'action' => null,
						  		  'html_menu'=>false,
								  'ajax' => false,
								  'functions' => array('RAIZ_GESTION_CALIDAD'),
								  'childrens' => array(
														/*array(
															   'img' => 'icons/packs/fugue/16x16/application-form.png',
															   'caption' => 'Acciones Correctivas',
															   'id' => 'mnuQaAccionCorrectiva',
															   'action' => array('form' =>  'CarpeDiem.App.frmQaAccionCorrectiva' ,
																				 'controller' => 'QaAccionesCorrectivas', 
																				 'action' => 'index'),
															   'ajax' => true,
															   'functions' => array('MENU_QAACCIONCORRECTIVA'),
													   ),*/
                                                       
                                                      /* array(
                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               'caption' => 'No conformidades',
                                                               'id' => 'mnuQaConformidad',
                                                               'action' => array('form' =>  'CarpeDiem.App.frmQaConformidad' ,
																				 'controller' => 'QaConformidad', 'action' => 'index'),
                                                               'ajax' => true,
                                                               'functions' => array('MENU_QACONFORMIDAD'),
                                                       ),*/
													   array(
															   'img' => 'icons/packs/fugue/16x16/application-form.png',
															   'caption' => 'Certificado Calidad',
															   'id' => 'mnuQaCertificadoCalidad',
													   		   'html_menu'=>true,
															   'action' => array('form' =>  'CarpeDiem.App.frmQaCertificadoCalidad' ,
																				 'controller' => 'QaCertificadosCalidad', 'action' => 'index'),
															   'ajax' => true,
															   'functions' => array('MENU_QACERTIFICADOCALIDAD'),
															   ),	
														array(
															   'img' => 'icons/packs/fugue/16x16/application-form.png',
															   'caption' => 'Alta Certificado Calidad',
															   'id' => 'mnuQaCertificadoCalidadA',
													   		   'html_menu'=>true,
															   'action' => array('form' =>  'CarpeDiem.App.frmQaCertificadoCalidadA' ,
																				 'controller' => 'QaCertificadosCalidad', 'action' => 'index'),
															   'ajax' => true,
															   'functions' => array('MENU_QACERTIFICADOCALIDAD'),
															   ),																   
													)
                             ),
							 
                             array(//////////////////////////////MODULO/////////////////////////
                              'caption' => 'Ventas',
                              'id' => 'mnuGestionComercial',
                              'img' => 'icons/packs/fugue/16x16/user-white.png',
                              'root' =>true,
                              'action' => null,
                              'html_menu'=>true,
                              'ajax' => false,
                              'functions' => array('RAIZ_GESTION_COMERCIAL'),
                              'childrens' => array(
                                                     array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Clientes',
                                                           'id' => 'mnuClienteRaiz',
                                                           'action' => null,
                                                     	   'html_menu'=>true,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_CLIENTE_RAIZ'),
                                                           'childrens' => array(
                                                           		
/*
                                                           		array(
                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           				'caption' => 'Meli',
                                                           				'id' => 'mnuMeni',
                                                           				'action' => array('form' =>  'CarpeDiem.App.frmMeli',
                                                           						'controller' => 'Meli', 'action' => 'index'),
                                                           						
                                                           				'ajax' => true,
                                                           				'functions' => array('MENU_DESCUENTO_FAMILIA_CLIENTE'),
                                                           		),*/
                                                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Descuento por Familia',
                                                                                   'id' => 'mnuDescuentoPorFamilia',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmDescuentoFamiliaPersona',
																									'controller' => 'DescuentoFamiliaPersonas', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_DESCUENTO_FAMILIA_CLIENTE'),
                                                                                   ),
                                                                                   array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Categoria de Cliente',
                                                                                       'id' => 'mnuClienteCategoria',
                                                                                        'action' => array('form' =>  'CarpeDiem.App.frmPersonaCategoria',
																										  'controller' => 'ClientesCategorias', 'action' => 'index',
																										  'form_param'=>  array('id_tipo_persona' =>array(EnumTipoPersona::Cliente) ) 
                                                                                        
                                                                                        ),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('MENU_CLIENTE_CATEGORIA'),
                                                                                       'childrens' => array()
                                                                                ), 
				                                                           		array(
				                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
				                                                           				'caption' => 'Origen del Cliente',
				                                                           				'id' => 'mnuClienteOrigen',
				                                                           				'action' => array('form' =>  'CarpeDiem.App.frmPersonaOrigen',
				                                                           						'controller' => EnumController::ClientesOrigen, 'action' => 'index',
				                                                           						'form_param'=>    array('id_tipo_persona' =>
				                                                           								array(EnumTipoPersona::Cliente))
				                                                           						
				                                                           				),
				                                                           				'ajax' => true,
				                                                           				'functions' => array('MENU_CLIENTE_ORIGEN'),
				                                                           				'childrens' => array()
				                                                           		),
                                                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Administraci&oacute;n de Clientes',
                                                                                   'id' => 'mnuCliente',
                                                                                   'html_menu'=>true,
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmCliente',
																									
																									'controller' => 'Clientes', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_CLIENTE'),
                                                                                   ),
																				   
																			   array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Nuevo Cliente',
                                                                                   'id' => 'mnuClienteA',
                                                                                   'html_menu'=>true,
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmClienteA',
																									
																									'controller' => 'Clientes', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('ADD_CLIENTE'),
                                                                                   ),


																				   
																			   array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Administraci&oacute;n de Contactos',
																			   		'html_menu'=>true,
                                                                                   'id' => 'mnuContacto',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmContacto',
																								 'controller' => 'Contactos', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_CONTACTOS'),
                                                                                   ),
                                                                                
																				   
																			  array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Cuentas Corrientes',
																			  			'html_menu'=>true,
																					   'id' => 'mnuCuentaCorrienteCliente',
																					   'action' => array('form' =>  'CarpeDiem.App.frmCuentaCorrienteCliente',
																										 'controller' => 'CuentasCorrienteCliente', 'action' => 'index', 'form_desktop' => 'frmCuentaCorrienteCliente',
																										 //MP:CUANDO CON EL COPY PASTE - sin no tiene los parentesis bien no puede parsear el menuContextual (click derecho)
																										  'context_menu'=> array( array('control' => 'gv', 'options' => array(
																																									array('text' =>'Ver &amp;Asientos','type' => 'text',
																																											'action' =>'msVerAsientos_Click',
																																											'functions' => array('MENU_CONTABILIDAD') ),
																																									array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
																																											'action' =>'msVerComprobanteRelacionado_Click',
																																											'functions' => array('CONSULTA_FACTURA_COMPRA') ),
																																									)
																																		)
																																)
																							),
																					   'ajax' => true,
																					   'functions' => array('MENU_CUENTA_CORRIENTE_CLIENTE'),
																					),
																				array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Deudas Totales Agrupadas por Cliente',
                                                                                   'id' => 'mnuComprobantesImpagosAgrupado',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmComprobantesImpagosAgrupado', 'form_param'=> array('id_sistema' => array(EnumSistema::VENTAS) ),
																							 'controller' => 'Comprobantes', 'action' => 'getComprobantesImpagosAgrupadoPorPersona'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_CUENTA_CORRIENTE_CLIENTE'),
                                                                                   ),
																				array(
																					'img' => 'icons/packs/fugue/16x16/application-form.png',
																					'caption' => 'Listado de Comprobantes Adeudados',
																					'id' => 'mnuComprobantesImpagosListado',
																					'action' => array('form' =>  'CarpeDiem.App.frmComprobante', 
																									 'form_param'=> array('id_sistema' => array(EnumSistema::VENTAS),
																														  'facturas_impagas' => '1',
																									 					  'form_title'=>'Listado de Comprobantes Adeudados'
																														  ),
																							'controller' => 'Comprobantes', 'action' => 'getComprobantesImpagos'),
																					'ajax' => true,
																					'functions' => array('MENU_CUENTA_CORRIENTE_CLIENTE'),
					                                                           		)
                                                                                )
                                                          ),
                                                          
                                                          
                                                          array(
                                                            'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                            'caption' => 'Recibo de Cobro',
                                                          	'html_menu'=>true,
                                                            'id' => 'mnuReciboMenu',
                                                            'action' => array(),
                                                            'ajax' => true,
                                                            'functions' => array('MENU_RECIBO'),
                                                            'childrens' => array(
                                                                                  array(
                                                                                        'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                        'caption' => 'Listado de Recibo de Cobro',
                                                                                  		'html_menu'=>true,
                                                                                        'id' => 'mnuRecibo',
                                                                                        'action' => array('form' =>  'CarpeDiem.App.frmRecibo',
                                                                                                       'controller' => 'Recibos', 'action' => 'index',  'form_desktop' => 'frmRecibos',
																									   'context_menu'=> array( array('control' => 'gv', 'options' => array(
                                                                                                   
                                                                                                                                                              array('text' =>'Ver &amp;Asientos','type' => 'text',
                                                                                                                                                                        'action' =>'msVerAsientos_Click',
																																										'functions' => array('MENU_ASIENTO') ),
																									   		
																																						   		array('text' =>'Enviar por Mail','type' => 'text',
																																						   				'action' =>'msEnviarMail_Click',
																																						   				'functions' => array('MENU_ENVIAR_MAIL') ),
																																										
                                                                                                                                                                array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                                                                                                                                                                      'action' =>'msVerComprobanteRelacionado_Click',
																																									  'functions' => array('CONSULTA_RECIBO') ), 
																																								
                                                                                                                                                                      
                                                                                                                                                                array('text' =>'ANULAR',    'type' => 'text',
                                                                                                                                                                      'action' =>'btnAnular_Click',
																																									  'functions' => array('BAJA_RECIBO') ),
                                                                                                                                                                array('text' =>'Modificar',     'type' => 'text',
                                                                                                                                                                      'action' =>'btnEditar_Click',
																																									  'functions' => array('ADD_RECIBO') ),
                                                                                                                                                                )
                                                                                                                           ) )
																									   
																									   
																									   
																									   ),
                                                                                        'ajax' => true,
                                                                                        'functions' => array('CONSULTA_RECIBO'),
                                                                                      ),
                                                                                       array(
                                                                                        'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                        'caption' => 'Alta de Recibo de Cobro',
                                                                                        'id' => 'mnuReciboMD',
                                                                                        'action' => array('form' =>  'CarpeDiem.App.frmReciboMD',
                                                                                                       'controller' => 'Recibos', 'action' => 'index',  'form_desktop' => 'frmReciboMD'),
                                                                                        'ajax' => true,
                                                                                        'functions' => array('ADD_RECIBO'),
                                                                                      ),
					                                                            		array(
					                                                            				'img' => 'icons/packs/fugue/16x16/application-form.png',
					                                                            				'caption' => 'Listado de Comprobantes Imputados en Recibos',
					                                                            				'id' => 'mnuComprobanteImputadoPagoVenta',
					                                                            				'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem',
					                                                            						'form_param'=>array('id_sistema' => array(EnumSistema::VENTAS),'id_tipo_comprobante' => array(EnumTipoComprobante::ReciboManual),
					                                                            											'form_title'=>'Listado de Comprobantes Imputados en Recibos'
					                                                            						),
					                                                            						'controller' => 'ComprobanteItemComprobantes', 'action' => 'index'),
					                                                            				'ajax' => true,
					                                                            				'functions' => array('CONSULTA_RECIBO'),
					                                                            				
					                                                            		),
					                                                            		array(
					                                                            				'img' => 'icons/packs/fugue/16x16/application-form.png',
					                                                            				'caption' => 'Listado de Valores en Recibos',
					                                                            				'id' => 'mnuComprobanteValorVenta',
					                                                            				'action' => array('form' =>  'CarpeDiem.App.frmComprobanteValor',
					                                                            						'form_param'=>array('id_sistema' => array(EnumSistema::VENTAS),'form_title'=>'Listado de Valores en Recibos' ),
					                                                            						'controller' => 'ComprobanteValores', 'action' => 'index'),
					                                                            				'ajax' => true,
					                                                            				'functions' => array('MENU_COMPROBANTE_VALOR'),
					                                                            				
					                                                            		),
                                                                      )
                                                             ),
							                              		array(
							                              				'img' => 'icons/packs/fugue/16x16/application-form.png',
							                              				'caption' => 'Retenciones',
							                              				'id' => 'mnuRetencionAjenaListado',
							                              				'action' => null,
							                              				'ajax' => true,
							                              				'functions' => array('MENU_RETENCION_AJENA'),
							                              				'childrens' => array(
							                              						array(
							                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
							                              								'caption' => 'Listado de Retenciones',
							                              								'id' => 'mnuRetencionAjena',
							                              								'action' => array('form' =>  'CarpeDiem.App.frmRetencion',
							                              										'form_param'=> array('id_sistema' => array(EnumSistema::VENTAS) ),
							                              										
							                              										'controller' => 'RetencionesAjenas', 'action' => 'index'),
							                              								'ajax' => true,
							                              							
							                              								'functions' => array('CONSULTA_RETENCION_AJENA'),
							                              						),
							                              						
							                              				)
							                              		),
														array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Facturacion',
                                                           'id' => 'mnuFacturaMenu',
														   'html_menu'=>true,
                                                           'action' => array(),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_FACTURA'),
														   'childrens' => array(
																			 array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Listado de Facturas',
																			 		'html_menu'=>true,
																				   'id' => 'mnuFactura',
																				   'action' => array('form' =>  'CarpeDiem.App.frmFactura',
																									'controller' => 'Facturas', 'action' => 'index',  'form_desktop' => 'frmFactura',
																									'context_menu'=> array( array('control' => 'gv', 'options' => array(
																									
																																							array('text' =>'Ver &amp;Asientos','type' => 'text',
																																									'action' =>'msVerAsientos_Click',
																																									'functions' => array('MENU_ASIENTO') ) ,
																																  
																																							array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
																																								  'action' =>'msVerComprobanteRelacionado_Click',
																																								  'functions' => array('CONSULTA_FACTURA') ), 
																										
																																							array('text' =>'Ver Comp. &amp;Auditor&iacute;a','type' => 'text',
																																									'action' =>'msVerComprobanteAuditoria_Click',
																																									'functions' => array('AUDITORIA_FACTURA') ),
																																				
																																								  
																																							array('text' =>'Anular',    'type' => 'text',
																																								  'action' =>'btnAnular_Click',
																																								  'functions' => array('BAJA_FACTURA') ),
																																								  
																																							array('text' =>'Modificar',     'type' => 'text',
																																								  'action' =>'btnEditar_Click',
																																								  'functions' => array('ADD_FACTURA') ),
																																							)
                                                                                                                           ) ) 
																									),
																				   'ajax' => true,
																				   'functions' => array('CONSULTA_FACTURA'),
																				 ),
																				/* DISTINTAS ALTAS DENTRO DE FACTURA */																				
																				array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
								
																				   'caption' => 'Factura punto de venta (Consumidor Final)',
																				   'id' => 'mnuFacturaMDLibreNoRegistradoConsumidorFinal',
																				   'action' => array('form' =>  'CarpeDiem.App.frmFacturaMD',
																									'controller' => 'Facturas', 'action' => 'index',  'form_desktop' => 'frmFacturaMD'),
																				   'ajax' => true,
																				   'functions' => array('MENU_FACTURA_PUNTO_VENTA'),
																				 ),
																				 
																			  array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
						
																				   'caption' => 'Factura sin Pedido ',
																				   'id' => 'mnuFacturaMDLibre',
																				   'action' => array('form' =>  'CarpeDiem.App.frmFacturaMD',
																									 'form_param'=>
																										array('id_tipo_comprobante_punto_venta_numero' => array(EnumTipoComprobantePuntoVentaNumero::PropioElectronicoOnline)),
																									'controller' => 'Facturas', 'action' => 'index',  'form_desktop' => 'frmFacturaMD'),
																				   'ajax' => true,
																				   'functions' => array('MENU_FACTURA_LIBRE'),
																				 ),
													
																				array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Factura por Pedido',
																				   'id' => 'mnuFacturaMD_PI',
																				   'action' => array('form' =>  'CarpeDiem.App.frmFacturaMD', 
																									'form_param'=>
																										array('PI','id_tipo_comprobante_punto_venta_numero' => array(EnumTipoComprobantePuntoVentaNumero::PropioElectronicoOnline)),
																									'controller' => 'Facturas', 'action' => 'index',  'form_desktop' => 'frmFacturaMD'),
																				   'ajax' => true,
																				   'functions' => array('MENU_FACTURA_PI'),
																				 ),
																				 
																				array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Factura Proforma',
																				   'id' => 'mnuFacturaMDProformaLibre',
																				   'action' => array('form' =>  'CarpeDiem.App.frmFacturaMD', 
																									//si esta configurado en el menu la id_tipo_comprobante_punto_venta entonces se setea y se bloquea el combo
																									'form_param'=>array('id_tipo_comprobante_punto_venta_numero' => array(EnumTipoComprobantePuntoVentaNumero::PropioNumeradorSistema),
																									//al estar config esto: EnumTipoComprobantePuntoVentaNumero::PropioNumeradorSistema => es proforma
																														'form_title' => 'Alta Factura PROFORMA'),
																									'controller' => 'Facturas', 'action' => 'index', 'form_desktop' => 'frmFacturaMD'),
																				   'ajax' => true,
																				   'functions' => array('MENU_FACTURA_PRO_FORMA'),//TODO: 'functions' => array('MENU_FACTURA_PROFORMA'),
																				 ),
																				 array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Factura Proforma por Pedido',
																				   'id' => 'mnuFacturaMDProforma_PI',
																				   'action' => array('form' =>  'CarpeDiem.App.frmFacturaMD', 
																									  //si esta configurado en el menu la id_tipo_comprobante_punto_venta entonces se setea y se bloquea el combo
																									 'form_param'=>array('PI'=> '1',
																														'form_title' =>'Alta Factura PROFORMA por Pedido', 
																														'id_tipo_comprobante_punto_venta_numero' => array(EnumTipoComprobantePuntoVentaNumero::PropioNumeradorSistema)),
																									 'controller' => 'Facturas', 'action' => 'index', 'form_desktop' => 'frmFacturaMD'),
																				   'ajax' => true,
																				   'functions' => array('MENU_FACTURA_PRO_FORMA_PI'),//TODO: 'functions' => array('MENU_FACTURA_PROFORMA_PI'),
																				 ),
																				 
																				 array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Factura Offline Libre',
																				   'id' => 'mnuFacturaMDOfflineLibre',
																				   'action' => array('form' =>  'CarpeDiem.App.frmFacturaMD', 
																									  //si esta configurado en el menu la id_tipo_comprobante_punto_venta entonces se setea y se bloquea el combo
																									 'form_param'=>array(
																														'form_title' =>'Alta Factura Offline Libre', 
																														'id_tipo_comprobante_punto_venta_numero' => array(EnumTipoComprobantePuntoVentaNumero::PropioNumeradorSistemaEDITABLE)
																														),
																									 'controller' => 'Facturas', 'action' => 'index', 'form_desktop' => 'frmFacturaMD'),
																				   'ajax' => true,
																				   'functions' => array('MENU_FACTURA_OFFLINE_LIBRE'),//TODO: 'functions' => array('MENU_FACTURA_PROFORMA_PI'),
																				 ),
																				  array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Factura Offline Por PI',
																				   'id' => 'mnuFacturaMDOffline_PI',
																				   'action' => array('form' =>  'CarpeDiem.App.frmFacturaMD', 
																									  //si esta configurado en el menu la id_tipo_comprobante_punto_venta entonces se setea y se bloquea el combo
																									 'form_param'=>array('PI'=> '0',
																														'form_title' =>'Alta Factura Offline por Pedido Interno', 
																														'id_tipo_comprobante_punto_venta_numero' => array(EnumTipoComprobantePuntoVentaNumero::PropioNumeradorSistemaEDITABLE)
																														),
																									 'controller' => 'Facturas', 'action' => 'index', 'form_desktop' => 'frmFacturaMD'),
																				   'ajax' => true,
																				   'functions' => array('MENU_FACTURA_OFFLINE_PI'),//TODO: 'functions' => array('MENU_FACTURA_PROFORMA_PI'),
																				 ),
																				 array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Factura Offline Exportacion Libre',
																				   'id' => 'mnuFacturaMDManualExportacionLibre',
																				   'action' => array('form' =>  'CarpeDiem.App.frmFacturaMD', 
																									  //si esta configurado en el menu la id_tipo_comprobante_punto_venta entonces se setea y se bloquea el combo
																									 'form_param'=>array(
																														'form_title' =>'Alta Factura de Exportacion Libre', 
																														'id_tipo_comprobante_punto_venta_numero' => array(EnumTipoComprobantePuntoVentaNumero::PropioElectronicoOfflineExportacion)
																														),
																									 'controller' => 'Facturas', 'action' => 'index', 'form_desktop' => 'frmFacturaMD'),
																				   'ajax' => true,
																				   'functions' => array('MENU_FACTURA_EXPORTACION_LIBRE'),//TODO: 'functions' => array('MENU_FACTURA_PROFORMA_PI'),
																				 ),
																				 array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Factura Offline Exportacion por PI',
																				   'id' => 'mnuFacturaMDManualExportacion_PI',
																				   'action' => array('form' =>  'CarpeDiem.App.frmFacturaMD', 
																									  //si esta configurado en el menu la id_tipo_comprobante_punto_venta entonces se setea y se bloquea el combo
																									 'form_param'=>array(
																														'form_title' =>'Alta Factura de Exportacion por PI', 
																														'id_tipo_comprobante_punto_venta_numero' => array(EnumTipoComprobantePuntoVentaNumero::PropioElectronicoOfflineExportacion)
																														),
																									 'controller' => 'Facturas', 'action' => 'index', 'form_desktop' => 'frmFacturaMD'),
																				   'ajax' => true,
																				   'functions' => array('MENU_FACTURA_EXPORTACION_PI'),//TODO: 'functions' => array('MENU_FACTURA_PROFORMA_PI'),
																				 ),
																				array(
																						'img' => 'icons/packs/fugue/16x16/application-form.png',
																						'caption' => 'Factura-Remito por Pedido',
																						'id' => 'mnuFacturaRemitoMD_PI',
																						'action' => array('form' =>  'CarpeDiem.App.frmFacturaMD',
																										  'controller' => 'Facturas',
																										  'action' => 'index',  
																										  'form_desktop' => 'frmFacturaMD',
																										  'form_param'=> array('PI'=> "1",
																															   'RemitoSimultaneo'=>'1'/* array('id_tipo_comprobante_punto_venta_numero' => 
																																							array(EnumTipoComprobantePuntoVentaNumero::PropioOfflinePreimpreso))*/
																															   )
																										  ),
																						'ajax' => true,
																						'functions' => array('MENU_FACTURA_REMITO_PI'),
																				),
																		   		array(
																		   				'img' => 'icons/packs/fugue/16x16/application-form.png',
																		   				'caption' => 'Factura Credito MiPyme-Remito por Pedido',
																		   				'id' => 'mnuFacturaCreditoRemitoMD_PI',
																		   				'action' => array('form' =>  'CarpeDiem.App.frmFacturaCreditoMD',
																		   						'controller' => EnumController::FacturasCredito,
																		   						'action' => 'index',
																		   						'form_desktop' => 'frmFacturaCreditoMD',
																		   						'form_param'=> array('PI'=> "1",
																													 'RemitoSimultaneo'=>'1',
																													 'id_tipo_comprobante_punto_venta_numero' =>
																														array(EnumTipoComprobantePuntoVentaNumero::PropioElectronicoOnline))
																		   						),
																		   				'ajax' => true,
																		   				'functions' => array('MENU_FACTURA_CREDITO_REMITO_PI'),
																		   		),
																				
																				
																				
																				
																				
																				/*REPORTES - DENTRO DE FACTURA*/
																				array(
																						'img' => 'icons/packs/fugue/16x16/application-form.png',
																						'caption' => 'Reporte - Art&iacute;culos presentes en Facturas', // INVISIBLE
																						'id' => 'mnuFacturaItems',
																						'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem', //'form_param'=>'PI_Item',
																								
																								'controller' => EnumController::FacturaItems, 'action' => 'index',
																								'form_desktop' => 'frmComprobanteItem',
																								'form_param'=>array('id_sistema' => array(EnumSistema::VENTAS))
																						),
																						'ajax' => true,
																						'functions' => array('CONSULTA_FACTURA'),
																				),
																				array(
																						'img' => 'icons/packs/fugue/16x16/application-form.png',
																						'caption' => 'Reporte - Ventas realizadas agrupadas por forma de pago (sin Recibo)',
																						'id' => 'mnuMovimientoTesoreriaConsumidorFinal',
																						'action' => array('form' =>  'CarpeDiem.App.frmMovimientoFondosFormaPago',
																								
																								'controller' => 'Facturas', 'action' => 'getMovimientoFondosFormaPago'),
																						
																						'ajax' => true,
																						'functions' => array('REPORTE_FACTURA_SIN_RECIBO'),
																						
																				)
														   		
														   		
															)		
                                                         ),
                              		// array(
                              				// 'img' => 'icons/packs/fugue/16x16/application-form.png',
                              				// 'caption' => 'Facturacion Offline',
                              				// 'id' => 'mnuFacturaMenuOffline',
                              				// 'html_menu'=>true,
                              				// 'action' => array(),
                              				// 'ajax' => true,
                              				// 'functions' => array('MENU_FACTURA'),
                              				// 'childrens' => array(
                              						// array(
                              								// 'img' => 'icons/packs/fugue/16x16/application-form.png',
                              								// 'caption' => 'Listado de Facturas Offline',
                              								// 'html_menu'=>true,
                              								// 'id' => 'mnuFacturaOffline',
                              								// 'action' => array('form' =>  'CarpeDiem.App.frmFactura',
                              										// 'form_param'=>array('id_tipo_comprobante_punto_venta_numero' => array(EnumTipoComprobantePuntoVentaNumero::PropioElectronicoOfflineExportacion)),
                              										// 'controller' => 'Facturas', 'action' => 'index',  'form_desktop' => 'frmFactura',
                              										// 'context_menu'=> array( array('control' => 'gv', 'options' => array(
                              												
                              												// array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                              														// 'action' =>'msVerComprobanteRelacionado_Click',
                              														// 'functions' => array('CONSULTA_FACTURA') ),
                              												
                              												// array('text' =>'Ver Comp. &amp;Auditor&iacute;a','type' => 'text',
                              														// 'action' =>'msVerComprobanteAuditoria_Click',
                              														// 'functions' => array('AUDITORIA_FACTURA') ),
                              												
                              												
                              												// array('text' =>'Anular',    'type' => 'text',
                              														// 'action' =>'btnAnular_Click',
                              														// 'functions' => array('BAJA_FACTURA') ),
                              												
                              												// array('text' =>'Modificar',     'type' => 'text',
                              														// 'action' =>'btnEditar_Click',
                              														// 'functions' => array('ADD_FACTURA') ),
                              										// )
                              										// ) )
                              										
                              										
                              										
                              										
                              								// ),
                              								// 'ajax' => true,
                              								// 'functions' => array('CONSULTA_FACTURA'),
                              						// ),
                              						// array(
                              								// 'img' => 'icons/packs/fugue/16x16/application-form.png',
                              								// 'caption' => 'Alta de Factura offline sin Pedido',
                              								// 'id' => 'mnuFacturaMDOffline',
                              								// 'action' => array('form' =>  'CarpeDiem.App.frmFacturaMD',
                              										// 'controller' => 'Facturas', 'action' => 'index',  'form_desktop' => 'frmFacturaMD'),
                              								// 'form_param'=>array('id_tipo_comprobante_punto_venta_numero' => array(EnumTipoComprobantePuntoVentaNumero::PropioElectronicoOfflineExportacion)),
                              								// 'ajax' => true,
                              								// 'functions' => array('MENU_FACTURA_LIBRE'),
                              						// ),
                              						
                              						// array(
                              								// 'img' => 'icons/packs/fugue/16x16/application-form.png',
                              								// 'caption' => 'Alta de Factura offline por Pedido',
                              								// 'id' => 'mnuFacturaMDOffline_PI',
                              								// 'action' => array('form' =>  'CarpeDiem.App.frmFacturaMD',
                              										// 'controller' => 'Facturas', 'action' => 'index',  'form_desktop' => 'frmFacturaMD'),
                              								// 'form_param'=>array('PI','id_tipo_comprobante_punto_venta_numero' => array(EnumTipoComprobantePuntoVentaNumero::PropioElectronicoOfflineExportacion)),
                              								// 'ajax' => true,
                              								// 'functions' => array('MENU_FACTURA_PI'),
                              						// )
													
						
                              				// )
                              		// ),
									array(
									   'img' => 'icons/packs/fugue/16x16/application-form.png',
									   'caption' => 'Remito de Ventas',
									   'id' => 'mnuRemitoRaiz',
									   'html_menu'=>true,
									   'action' => array(),
									   'ajax' => true,
									   'functions' => array('MENU_REMITO'),
									   'childrens' => array(
                                                                                 array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Listado de Remitos',
                                                                                 		'html_menu'=>true,
                                                                                       'id' => 'mnuRemitoListado',
                                                                                       'action' => array('form' => 'CarpeDiem.App.frmRemito',
																										'form_param'=>array('id_sistema' => array(EnumSistema::VENTAS) ),
                                                                                                        'controller' => 'Remitos', 'action' => 'index',  'form_desktop' => 'frmRemito',
																										'context_menu'=> array( array('control' => 'gv', 'options' => array(
                                                                                                   
                                                                                                                                                           
                                                                                                                                                                array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                                                                                                                                                                      'action' =>'msVerComprobanteRelacionado_Click',
																																									  'functions' => array('CONSULTA_REMITO') ), 
																																									/*  
                                                                                                                                                                array('text' =>'&amp;Nuevo Mov','type' => 'text',
                                                                                                                                                                      'action' =>'msNuevoMov302_Click',//DESPACHA
																																									  'functions' => array('ADD_MOVIMIENTO') ),
																																									  */
                                                                                                                                                                      
                                                                                                                                                                array('text' =>'ANULAR',    'type' => 'text',
                                                                                                                                                                      'action' =>'btnAnular_Click',
																																									  'functions' => array('BAJA_REMITO') ),
                                                                                                                                                                array('text' =>'Modificar',     'type' => 'text',
                                                                                                                                                                      'action' =>'btnEditar_Click',
																																									  'functions' => array('ADD_REMITO') ),
                                                                                                                                                                )
                                                                                                                           ) )
																										
																										
																										
																										),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('CONSULTA_REMITO')
                                                                                     ),
                                                                                      array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Remito Manual',
                                                                                       'id' => 'mnuRemitoMD',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmRemitoMD',
                                                                                                        'controller' => 'Remitos', 'action' => 'index',  'form_desktop' => 'frmRemitoMD'
																								
																										
																										
																										
),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('ADD_REMITO'),
                                                                                     ),
																				array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Remito por Pedido',
																				   'id' => 'mnuRemitoMD_PI',
																				   'action' => array('form' =>  'CarpeDiem.App.frmRemitoMD', 
																									'form_param'=>'PI',
																									'controller' => 'Remitos', 'action' => 'index',  'form_desktop' => 'frmRemitoMD'),
																				   'ajax' => true,
																				   'functions' => array('ADD_REMITO_IMPORTADO'),
																				 ),
				                                                           		array(
				                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
				                                                           				'caption' => 'Control de Art&iacute;culos en Remitos', // INVISIBLE
				                                                           				'id' => 'mnuRemitoItems',
				                                                           				'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem', //'form_param'=>'PI_Item',
				                                                           						
				                                                           						'controller' => EnumController::RemitoItems, 'action' => 'index',
				                                                           						'form_desktop' => 'frmComprobanteItem',
				                                                           						'form_param'=>array('id_sistema' => array(EnumSistema::VENTAS),'id_tipo_comprobante' => array(EnumTipoComprobante::Remito))
				                                                           				),
				                                                           				'ajax' => true,
				                                                           				'functions' => array('CONSULTA_REMITO'),
				                                                           		)
                                                                                     
                                                    )    ),
                                                     
														 
														 array(
                                                           	'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           	'caption' => 'Notas Credito / Notas Debito',
                                                           	'id' => 'mnuNotaMenu',
														 	'html_menu'=>true,
														 	'action' => array(),
                                                           	'ajax' => true,
                                                           	'functions' => array('MENU_NOTA'),
														   	'childrens' => array(
																			 array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Listado de Notas Credito / Notas Debito',
																				   'id' => 'mnuNota',
																			 		'html_menu'=>true,
																				   'action' => array('form' =>  'CarpeDiem.App.frmNota',
																									'controller' => 'Notas', 
																									'action' => 'index',  
																									'form_desktop' => 'frmNota',
																									'context_menu'=> array( array('control' => 'gv', 'options' => array(
																																		array('text' =>'Ver &amp;Asientos','type' => 'text',
																																				'action' =>'msVerAsientos_Click',
																																				'functions' => array('MENU_ASIENTO') ) ,
																																		array('text' =>'Ver Comp. &amp;Relacionado','type' => 'text',
																																			  'action' =>'msVerComprobanteRelacionado_Click',
																																			  'functions' => array('MENU_NOTA') ), 
																																		array('text' =>'Anular',    'type' => 'text',
																																			  'action' =>'btnAnular_Click',
																																			  'functions' => array('BAJA_NOTA') ),
																																		array('text' =>'Modificar',     'type' => 'text',
																																			  'action' =>'btnEditar_Click',
																																			  'functions' => array('MODIFICACION_NOTA') ),
                                                                                                                                                                )
                                                                                                                           ) ) 
																									),
																				   'ajax' => true,
																				   'functions' => array('CONSULTA_NOTA'),
																				 ),
																			  array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Nota de Credito',
																				   'id' => 'mnuNotaMDC',
																				   'action' => array('form' =>  'CarpeDiem.App.frmNotaMD','form_param'=>'C',
																									'controller' => 'NotasCreditoVenta', 'action' => 'index',  'form_desktop' => 'frmNotaMD'),
																				   'ajax' => true,
																				   'functions' => array('BTN_NOTA_CREDITO_MANUAL'),
																				 ),
																			  array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Nota de Credito por Factura',
																				   'id' => 'mnuNotaMDC_FC',
																				   'action' => array('form' =>  'CarpeDiem.App.frmNotaMD','form_param'=>'C_FC',
																									'controller' => 'NotasCreditoVenta', 'action' => 'index',  'form_desktop' => 'frmNotaMD'),
																				   'ajax' => true,
																				   'functions' => array('ADD_NOTA'),
																				 ),
																				 // array(
																				   // 'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   // 'caption' => 'Nueva Nota de Credito por FC',
																				   // 'id' => 'mnuNotaMDC_FC',
																				   // 'action' => array('form' =>  'CarpeDiem.App.frmNotaMD','form_param'=>'C_FC',
																									// 'controller' => 'Notas', 'action' => 'index',  'form_desktop' => 'frmNotaMD'),
																				   // 'ajax' => true,
																				   // 'functions' => array('ADD_NOTA'),
																				 // ),
																				 
																				array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Nota de Debito',
																				   'id' => 'mnuNotaMDD',
																				   'action' => array('form' =>  'CarpeDiem.App.frmNotaMD', 'form_param'=>'D',
																									'controller' => 'NotasDebitoVenta', 'action' => 'index',  'form_desktop' => 'frmNotaMD'),
																				   'ajax' => true,
																				   'functions' => array('ADD_NOTA'),
																				 ),
																				 /*INI  NOTAS DE CREDITO MiPYME*/
																				array(
																					'img' => 'icons/packs/fugue/16x16/application-form.png',
																					'caption' => 'Notas Mi Pyme ',
																					'id' => 'mnuNotaCreditoMiPymeMenu',
																					'html_menu'=>true,
																					'action' => array(),
																					'ajax' => true,
																					'functions' => array('MENU_NOTA'),
																					'childrens' => array(
																									  array(
																										   'img' => 'icons/packs/fugue/16x16/application-form.png',
																										   'caption' => 'Nota de Credito',
																										   'id' => 'mnuNotaMiPymeCredito',
																										   'action' => array('form' =>  'CarpeDiem.App.frmNotaCreditoMD','form_param'=>'C',
																															'controller' => 'NotasCreditoMiPymeVentaController', 'action' => 'index',  'form_desktop' => 'frmNotaCreditoMD'),
																										   'ajax' => true,
																										   'functions' => array('BTN_NOTA_CREDITO_MANUAL'),
																										 ),
																									  array(
																										   'img' => 'icons/packs/fugue/16x16/application-form.png',
																										   'caption' => 'Nota de Credito por Factura',
																										   'id' => 'mnuNotaMiPymeCredito_PorFactura',
																										   'action' => array('form' =>  'CarpeDiem.App.frmNotaCreditoMD','form_param'=>'C_FC',
																															'controller' => 'NotasCreditoMiPymeVentaController', 'action' => 'index',  'form_desktop' => 'frmNotaCreditoMD'),
																										   'ajax' => true,
																										   'functions' => array('ADD_NOTA'),
																										 ),

																										array(
																										   'img' => 'icons/packs/fugue/16x16/application-form.png',
																										   'caption' => 'Nota de Debito',
																										   'id' => 'mnuNotaMiPymeDebito',
																										   'action' => array('form' =>  'CarpeDiem.App.frmNotaCreditoMD', 'form_param'=>'D',
																															'controller' => 'NotasDebitoMiPymeVentaController', 'action' => 'index',  'form_desktop' => 'frmNotaCreditoMD'),
																										   'ajax' => true,
																										   'functions' => array('ADD_NOTA'),
																										 ),
																									)		
																				 ), 
																				/*FIN  NOTAS DE CREDITO MiPYME*/
																			)		
                                                         ), 
                              		/*INI  NOTAS DE CREDITO MiPYME*/
									array(
										'img' => 'icons/packs/fugue/16x16/application-form.png',
										'caption' => 'Notas Mi Pyme ',
										'id' => 'mnuNotaCreditoMiPymeMenu',
										'html_menu'=>true,
										'action' => array(),
										'ajax' => true,
										'functions' => array('MENU_NOTA_MIPYME'),
										'childrens' => array(
														 
														  array(
															   'img' => 'icons/packs/fugue/16x16/application-form.png',
															   'caption' => 'Nota de Credito por Factura',
															   'id' => 'mnuNotaMiPymeCredito_PorFactura',
															   'action' => array('form' =>  'CarpeDiem.App.frmNotaCreditoMD','form_param'=>'C_FC',
																				'controller' => 'NotasCreditoMiPymeVenta', 'action' => 'index',  'form_desktop' => 'frmNotaCreditoMD'),
															   'ajax' => true,
															   'functions' => array('ADD_NOTA_MYPIME'),
															 ),

														
														)		
									 ), 
									/*FIN  NOTAS DE CREDITO MiPYME*/
                              		array(
                              				'img' => 'icons/packs/fugue/16x16/application-form.png',
                              				'caption' => 'Consulta Comprobante Afip',
                              				'id' => 'mnuConsultaComprobanteAfipMenu',
                              				'action' => array(),
                              				'ajax' => true,
                              				'functions' => array('MENU_FACTURA'),
                              				'childrens' => array(
                              						array(
                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
                              								'caption' => 'Consulta Comprobante Afip',
                              								'id' => 'mnuConsultaComprobanteAfip',
                              								'action' => array('form' =>  'CarpeDiem.App.frmConsultaComprobante',
                              										'controller' => EnumController::Comprobantes,
                              										'action' => 'ConsultarComprobanteAfip',
                              										'form_desktop' => 'frmConsultaComprobante'
                              										
                              										
                              										 ),
                              								
                              								'ajax' => true,
                              								'functions' => array('CONSULTA_NOTA')
                              								),
                              								
                              						)),
									                         

													 
                                                          array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Pedido Interno / Orden de Venta',
                                                           'id' => 'mnuPedidoInterno',
                                                           'html_menu'=>true,
                                                           'action' => array(
                                                                 'controller' => 'PedidosInternos', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_PEDIDO_INTERNO'),
                                                           'childrens' => array(
                                                                                 array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Listado de Pedidos',
                                                                                 		'html_menu'=>true,
                                                                                       'id' => 'mnuPedidoInterno',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmPedidoInterno',
                                                                                                        'controller' => 'PedidosInternos', 'action' => 'index',  'form_desktop' => 'frmPedidoInterno',
																										'context_menu'=> array( array('control' => 'gv', 'options' => array(
                                                                                                                                                                array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                                                                                                                                                                      'action' =>'msVerComprobanteRelacionado_Click',
																																									  'functions' => array('CONSULTA_PEDIDO_INTERNO') ), 
                                                                                                                                                                array('text' =>'ANULAR',    'type' => 'text',
                                                                                                                                                                      'action' =>'btnAnular_Click',
																																									  'functions' => array('BAJA_PEDIDO_INTERNO') ),
                                                                                                                                                                array('text' =>'Modificar',     'type' => 'text',
                                                                                                                                                                      'action' =>'btnEditar_Click',
																																									  'functions' => array('ADD_PEDIDO_INTERNO') ),
                                                                                                                                                                )
                                                                                                                            )
																														)
																										),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('CONSULTA_PEDIDO_INTERNO'),
                                                                                     ),
                                                                                      array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Nuevo Pedido',
                                                                                       'id' => 'mnuPedidoInternoMD',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmPedidoInternoMD',
                                                                                                        'controller' => 'PedidosInternos', 'action' => 'index',  'form_desktop' => 'frmPedidoInternoMD'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('ADD_PEDIDO_INTERNO'),
                                                                                     ),
                                                           		
																					array(
						                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
						                                                           				'caption' => 'Nuevo Pedido por Cotizaci&oacute;n',
						                                                           				'id' => 'mnuPedidoInternoMDCOTI',
						                                                           				'action' => array('form' =>  'CarpeDiem.App.frmPedidoInternoMD',
						                                                           						'controller' => EnumController::PedidosInternos,
						                                                           						'form_param'=>'CT',
						                                                           						'action' => 'index'),
						                                                           				'ajax' => true,
						                                                           				'functions' => array('MENU_PEDIDO_INTERNO')
						                                                           				
						                                                           		),
																											 
																					// array(
                                                                                       // 'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       // 'caption' => '', // INVISIBLE
                                                                                       // 'id' => 'mnuPedidoInternoItemImportar',
                                                                                       // 'action' => array('form' =>  'CarpeDiem.App.frmPedidoInternoItem', 'form_param'=>'PI_Item',
                                                                                                        // 'controller' => 'PedidoInternoItems', 'action' => 'index',  'form_desktop' => 'frmPedidoInternoItem'),
                                                                                       // 'ajax' => true,
                                                                                       // 'functions' => array('CONSULTA_PEDIDO_INTERNO'),
                                                                                     // )
																					 
																				/*	array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Control de Pedidos Internos',
                                                                                       'id' => 'mnuPedidoInternoItem',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmPedidoInternoItem', 'form_param'=>'PI_Item',
                                                                                                        'controller' => 'PedidoInternoItems', 'action' => 'index',  'form_desktop' => 'frmPedidoInternoItem'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('CONSULTA_PEDIDO_INTERNO'),
                                                                                     ),
                                                                                     */
																					array(
																							'img' => 'icons/packs/fugue/16x16/application-form.png',
																							'caption' => 'Control de Pedidos Internos', // INVISIBLE
																							'id' => 'mnuPedidoInternoItem',
																							'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem', //'form_param'=>'PI_Item',
																									
																									'controller' => EnumController::PedidoInternoItems, 'action' => 'index',
																									'form_desktop' => 'frmComprobanteItem',
																									'form_param'=>array('id_sistema' => array(EnumSistema::VENTAS),
																														'id_tipo_comprobante' => array(EnumTipoComprobante::PedidoInterno) )
																							),
																							'ajax' => true,
																							'functions' => array('MENU_PEDIDO_INTERNO'),
																					), 
																					
																					

																				    array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Pedidos Internos agrupados por cliente totalizado',
																					   'id' => 'mnuReportePedidoInterno',
																					   'action' => array('form' =>  'CarpeDiem.App.frmReportePedidoInterno',
																										 'controller' => 'Reportes', 'action' => 'VentasPorPedidoInterno'),
																					   'ajax' => true,
																					   'functions' => array('REPORTE_PEDIDO_INTERNO_AGRUPADO_PRODUCTO'),
                                                                                   ),

																		)    
															),
                              		
                              		
                              		array(
                              		'img' => 'icons/packs/fugue/16x16/application-form.png',
                              		'caption' => 'Pedido B2B',
                              		'id' => 'mnuPedidoB2B',
                              		'html_menu'=>true,
                              		'action' => array(
                              				'controller' => EnumController::PedidosB2B, 'action' => 'index'),
                              		'ajax' => true,
                              		'functions' => array('MENU_PEDIDO_B2B'),
                              		'childrens' => array(
                              				array(
                              						'img' => 'icons/packs/fugue/16x16/application-form.png',
                              						'caption' => 'Listado de Pedidos',
                              						'html_menu'=>true,
                              						'id' => 'mnuPedidoB2B',
                              						'action' => array('form' =>  'CarpeDiem.App.frmPedidoInterno',
                              								'controller' => EnumController::PedidosB2B, 'action' => 'index',  'form_desktop' => 'frmPedidoInterno',
                              								'context_menu'=> array( array('control' => 'gv', 'options' => array(
                              										array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                              												'action' =>'msVerComprobanteRelacionado_Click',
                              												'functions' => array('CONSULTA_PEDIDO_B2B') ),
                              										array('text' =>'ANULAR',    'type' => 'text',
                              												'action' =>'btnAnular_Click',
                              												'functions' => array('BAJA_PEDIDO_B2B') ),
                              										array('text' =>'Modificar',     'type' => 'text',
                              												'action' =>'btnEditar_Click',
                              												'functions' => array('ADD_PEDIDO_B2B') ),
                              								)
                              								)
                              								)
                              						),
                              						'ajax' => true,
                              						'functions' => array('CONSULTA_PEDIDO_B2B'),
                              				),
                              			
                              				
                              				
                              				
                              				
                              				
                              			
                              				
                              		)
                              		),
                              		
                              		
                              		array(
                              				'img' => 'icons/packs/fugue/16x16/application-form.png',
                              				'caption' => 'Pedido Web',
                              				'id' => 'mnuPedidoWeb',
                              				'html_menu'=>true,
                              				'action' => array(
                              						'controller' => EnumController::PedidosWeb, 'action' => 'index'),
                              				'ajax' => true,
                              				'functions' => array('MENU_PEDIDO_WEB'),
                              				'childrens' => array(
                              						array(
                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
                              								'caption' => 'Listado de Pedidos',
                              								'html_menu'=>true,
                              								'id' => 'mnuPedidoWeb',
                              								'action' => array('form' =>  'CarpeDiem.App.frmPedidoWeb',
                              										'controller' => EnumController::PedidosWeb, 'action' => 'index',  'form_desktop' => 'frmPedidoWeb',
                              										'context_menu'=> array( array('control' => 'gv', 'options' => array(
                              												array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                              														'action' =>'msVerComprobanteRelacionado_Click',
                              														'functions' => array('CONSULTA_PEDIDO_WEB') ),
                              												array('text' =>'ANULAR',    'type' => 'text',
                              														'action' =>'btnAnular_Click',
                              														'functions' => array('BAJA_PEDIDO_WEB') ),
                              												array('text' =>'Modificar',     'type' => 'text',
                              														'action' =>'btnEditar_Click',
                              														'functions' => array('ADD_PEDIDO_WEB') ),
                              										)
                              										)
                              										)
                              								),
                              								'ajax' => true,
                              								'functions' => array('CONSULTA_PEDIDO_WEB'),
                              						)
                              					
                              						
                              						
                              						
                              						
                              						
                              						
                              						
                              				)
                              		),
                              		array(
                              				'img' => 'icons/packs/fugue/16x16/application-form.png',
                              				'caption' => 'Pedido B2C',
                              				'id' => 'mnuPedidoB2C',
                              				'html_menu'=>true,
                              				'action' => array(
                              						'controller' => EnumController::PedidosB2C, 'action' => 'index'),
                              				'ajax' => true,
                              				'functions' => array('MENU_PEDIDO_B2C'),
                              				'childrens' => array(
                              						array(
                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
                              								'caption' => 'Listado de Pedidos',
                              								'html_menu'=>true,
                              								'id' => 'mnuPedidoB2C',
                              								'action' => array('form' =>  'CarpeDiem.App.frmPedidoInterno',
                              										'controller' => EnumController::PedidosB2C, 'action' => 'index',  'form_desktop' => 'frmPedidoInterno',
                              										'context_menu'=> array( array('control' => 'gv', 'options' => array(
                              												array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                              														'action' =>'msVerComprobanteRelacionado_Click',
                              														'functions' => array('CONSULTA_PEDIDO_B2C') ),
                              												array('text' =>'ANULAR',    'type' => 'text',
                              														'action' =>'btnAnular_Click',
                              														'functions' => array('BAJA_PEDIDO_B2C') ),
                              												array('text' =>'Modificar',     'type' => 'text',
                              														'action' =>'btnEditar_Click',
                              														'functions' => array('ADD_PEDIDO_B2C') ),
                              										)
                              										)
                              										)
                              								),
                              								'ajax' => true,
                              								'functions' => array('CONSULTA_PEDIDO_B2C'),
                              						),
                              					
                              						
                              						
                              						
                              						
                              						
                              						
                              						
                              						
                              				)
                              		),
                                                            
                                                            array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Cotizaciones',
                                                           'id' => 'mnuCotizacion',
                                                           'action' => array(
                                                                 'controller' => 'Cotizaciones', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_COTIZACION'),
                                                           'childrens' => array(
                                                                                 array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Listado de Cotizaciones',
                                                                                       'id' => 'mnuCotizacionListado',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmCotizacion',
                                                                                                        'controller' => 'Cotizaciones', 'action' => 'index',  'form_desktop' => 'frmCotizacion',
																										'context_menu'=> array( array('control' => 'gv', 'options' => array(
                                                                                                   
                                                                                                                                                        
																																										
                                                                                                                                                                array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                                                                                                                                                                      'action' =>'msVerComprobanteRelacionado_Click',
																																									  'functions' => array('CONSULTA_COTIZACION') ), 
																																								
                                                                                                                                                                      
                                                                                                                                                                array('text' =>'ANULAR',    'type' => 'text',
                                                                                                                                                                      'action' =>'btnAnular_Click',
																																									  'functions' => array('BAJA_COTIZACION') ),
                                                                                                                                                                array('text' =>'Modificar',     'type' => 'text',
                                                                                                                                                                      'action' =>'btnEditar_Click',
																																									  'functions' => array('ADD_COTIZACION') ),
                                                                                                                                                                )
                                                                                                                           ) )
																										),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('CONSULTA_COTIZACION'),
                                                                                     ),
																					 																					 
                                                                                      array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Nueva Cotizaci&oacute;n',
                                                                                       'id' => 'mnuCotizacionMD',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmCotizacionMD',
                                                                                                        'controller' => 'Cotizaciones', 'action' => 'index',  'form_desktop' => 'frmCotizacionMD'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('CONSULTA_COTIZACION'),
                                                                                     ),
                                                           							
                                                           								
                                                           		/*
																					 
																					 array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Listado de Cotizaciones Plus',
                                                                                       'id' => 'mnuTurCotizacionListado',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmTurCotizacion',
                                                                                                        'controller' => 'TurCotizaciones', 'action' => 'index',  'form_desktop' => 'frmTurCotizacion',
																										'context_menu'=> array( array('control' => 'gv', 'options' => array(
                                                                                                                                                                array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                                                                                                                                                                      'action' =>'msVerComprobanteRelacionado_Click',
																																									  'functions' => array('CONSULTA_TUR_COTIZACION') ), 
                                                                                                                                                                array('text' =>'ANULAR',    'type' => 'text',
                                                                                                                                                                      'action' =>'btnAnular_Click',
																																									  'functions' => array('BAJA_TUR_COTIZACION') ),
                                                                                                                                                                array('text' =>'Modificar',     'type' => 'text',
                                                                                                                                                                      'action' =>'btnEditar_Click',
																																									  'functions' => array('MODIFICACION_TUR_COTIZACION') ),
                                                                                                                                                                )
                                                                                                                           ) )
																										),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('CONSULTA_TUR_COTIZACION'),
                                                                                     )
																					 																					 ,
                                                                                      array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Alta de Cotizacion Plus',
                                                                                       'id' => 'mnuTurCotizacionMD',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmTurCotizacionMD2',
                                                                                                        'controller' => 'TurCotizaciones', 
																										'action' => 'index',  
																										'form_desktop' => 'frmTurCotizacionMD2'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('CONSULTA_TUR_COTIZACION'),
                                                                                     ),          
				                                                           		*/
                                                         		
				                                                           		array(
				                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
				                                                           				'caption' => 'Art&iacute;culos presentes en Cotizaciones', // INVISIBLE
				                                                           				'id' => 'mnuCotizacionesItems',
				                                                           				'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem', //'form_param'=>'PI_Item',
				                                                           					
				                                                           						'controller' => EnumController::CotizacionItems, 'action' => 'index',  
				                                                           						'form_desktop' => 'frmComprobanteItem',
				                                                           						'form_param'=>array('id_sistema' => array(EnumSistema::VENTAS),'id_tipo_comprobante' => array(EnumTipoComprobante::Cotizacion))
				                                                           				),
				                                                           				'ajax' => true,
				                                                           				'functions' => array('CONSULTA_COTIZACION'),
				                                                           		) ,
                                                    )    ), 
                                                    
                                                    
														   
														   array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Cr&eacute;ditos internos',
                                                           'id' => 'mnuSaldosInicio',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_CREDITO_INTERNO_CLIENTE'),
                                                           'childrens' => array( 
																				 array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Listado de Cr&eacute;ditos Internos',
                                                                                   'id' => 'mnuCreditoInternoVentas',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmSaldosIniciales',
																									 'form_param'=>array('id_sistema' => array(EnumSistema::VENTAS) ),
                                                                                                     'controller' => 'CreditosInternoCliente', 'action' => 'index',
																									 'context_menu'=> array( array('control' => 'gv', 
																															'options' => array(
																																			array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
																																				'action' =>'msVerComprobanteRelacionado_Click',
																																				'functions' => array('CONSULTA_CREDITO_INTERNO_CLIENTE') ),
																																  			array('text' =>'ANULAR',    'type' => 'text',
																																  				'action' =>'btnAnular_Click',
																																  				'functions' => array('BAJA_CREDITO_INTERNO_CLIENTE') ),
																																		)
																																)
																														)
																									),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('CONSULTA_CREDITO_INTERNO_CLIENTE'),
                                                                                   ), 
																				)
                                                           ),
						                              		array(
						                              				'img' => 'icons/packs/fugue/16x16/application-form.png',
						                              				'caption' => 'D&eacute;bitos internos',
						                              				'id' => 'mnuDebitoInternoVentas',
						                              				'action' => null,
						                              				'ajax' => true,
						                              				'functions' => array('MENU_DEBITO_INTERNO_CLIENTE'),
						                              				'childrens' => array(
						                              						array(
						                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
						                              								'caption' => 'Listado de D&eacute;bitos Internos',
						                              								'id' => 'mnuDebitoInternoVentasL',
						                              								'action' => array('form' =>  'CarpeDiem.App.frmSaldosIniciales',
						                              										'form_param'=>array('id_sistema' => array(EnumSistema::VENTAS) ),
						                              										'controller' => 'DebitosInternoCliente', 'action' => 'index',
						                              										'context_menu'=> array( array('control' => 'gv',
						                              												'options' => array(
						                              														array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
						                              																'action' =>'msVerComprobanteRelacionado_Click',
						                              																'functions' => array('CONSULTA_DEBITO_INTERNO_CLIENTE') ),
						                              														
						                              														array('text' =>'ANULAR',    'type' => 'text',
						                              																'action' =>'btnAnular_Click',
						                              																'functions' => array('BAJA_DEBITO_INTERNO_CLIENTE') )
						                              												)
						                              										)
						                              										)
						                              								),
						                              								'ajax' => true,
						                              								'functions' => array('CONSULTA_DEBITO_INTERNO_CLIENTE'),
						                              						),
						                              				)
						                              		),
														   array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Saldos al inicio',
                                                           'id' => 'mnuSaldosInicio',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_SALDO_INICIAL_CLIENTE'),
                                                           'childrens' => array( 
																				 array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Listado de Saldos al inicio',
                                                                                   'id' => 'mnuSaldosInicioListado',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmSaldosIniciales',
																									 'form_param'=>array('id_sistema' => array(EnumSistema::VENTAS) ),
                                                                                                     'controller' => 'SaldosInicialesCliente', 'action' => 'index',
                                                                                   
				                                                                                   		'context_menu'=> array( array('control' => 'gv', 'options' => array(
				                                                                                   				
				                                                                                   				
				                                                                                   				array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
				                                                                                   						'action' =>'msVerComprobanteRelacionado_Click',
				                                                                                   						'functions' => array('CONSULTA_SALDO_INICIAL_CLIENTE') ),
				                                                                                   				
				                                                                                   		)
				                                                                                   		)
				                                                                                   		)
                                                                                   ),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('CONSULTA_SALDO_INICIAL_CLIENTE'),
                                                                                   ), 
                                                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Nuevo Saldo al inicio',
                                                                                   'id' => 'mnuSaldosInicialesClienteMD',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmSaldosInicialesMD',
																									 'form_param'=>array('id_sistema' => array(EnumSistema::VENTAS) ),
                                                                                                     'controller' => 'SaldosInicialesCliente', 'action' => 'add'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('ADD_SALDO_INICIAL_CLIENTE'),
                                                                                   )
																	)
                                                           ),
                              		
					                              		array(
					                              				'img' => 'icons/packs/fugue/16x16/application-form.png',
					                              				'caption' => 'Lista de Precio',
					                              				'id' => 'mnuRaizListaPrecioVenta',
					                              				'action' => array(),
					                              				'ajax' => true,
					                              				'functions' => array('MENU_LISTA_PRECIO'),
					                              				'childrens' => array(
					                              						array(
					                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
					                              								'caption' => 'Consulta de Listas de Precio',
					                              								'id' => 'mnuListaPrecioVenta',
					                              								'action' => array('form' =>  'CarpeDiem.App.frmListaPrecio',
																							'listarTipo' => EnumListarTipo::ABM, 
					                              										'form_param'=>    array('id_sistema' => array(EnumSistema::VENTAS) ),
					                              										'controller' => 'ListaPrecios', 'action' => 'index',  'form_desktop' => 'frmListaPrecio',
					                              										'context_menu'=> array(  	array('control' => 'gv', 'options' => 
																							array(
					                              												array('text' =>'Anular','type' => 'text',
					                              														'action' =>'btnAnular_Click',
					                              														'functions' => array('BAJA_LISTA_PRECIO') ),
					                              												array('text' =>'Modificar','type' => 'text',
					                              														'action' =>'btnEditar_Click',
					                              														'functions' => array('ADD_LISTA_PRECIO') ),
																							)
					                              										) )
					                              								),
					                              								'ajax' => true,
					                              								'functions' => array('CONSULTA_LISTA_PRECIO'),
					                              						),
					                              						array(
					                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
					                              								'caption' => 'Alta de Lista Precio Base',
					                              								'id' => 'mnuListaPrecioMDBase',
					                              								'action' => array('form' =>  'CarpeDiem.App.frmListaPrecioMD',
					                              										'controller' => 'ListaPrecios', 'action' => 'index',
					                              										'form_desktop' => 'mnuListaPrecioMD',
					                              										'form_param'=>    array('id_sistema' => array(EnumSistema::VENTAS),'id_tipo_lista_precio'=>EnumTipoListaPrecio::Base)	
					                              								),
					                              								'ajax' => true,
					                              								'functions' => array('ADD_LISTA_PRECIO'),
					                              						),
					                              						array(
					                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
					                              								'caption' => 'Alta de Lista Precio Enlazada',
					                              								'id' => 'mnuListaPrecioMDEnlazada',
					                              								'action' => array('form' =>  'CarpeDiem.App.frmListaPrecioMD',
					                              										'controller' => 'ListaPrecios', 'action' => 'index',
					                              										'form_desktop' => 'mnuListaPrecioMD',
					                              										'form_param'=>    array('id_sistema' => array(EnumSistema::VENTAS),'id_tipo_lista_precio'=>EnumTipoListaPrecio::Enlazada)
					                              								),
					                              								'ajax' => true,
					                              								'functions' => array('ADD_LISTA_PRECIO'),
					                              						),
					                              						array(
					                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
					                              								'caption' => 'Alta de Lista Precio x Costo',
					                              								'id' => 'mnuListaPrecioMDXCosto',
					                              								'action' => array('form' =>  'CarpeDiem.App.frmListaPrecioMD',
					                              										'controller' => 'ListaPrecios', 'action' => 'index',
					                              										'form_desktop' => 'mnuListaPrecioMD',
					                              										'form_param'=>    array('id_sistema' => array(EnumSistema::VENTAS),'id_tipo_lista_precio'=>EnumTipoListaPrecio::BaseCosto )
					                              								),
					                              								'ajax' => true,
					                              								'functions' => array('ADD_LISTA_PRECIO'),
					                              						)
					                              				)
					                              		),
                              		
                              		/*
													 array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Calendario de Vencimientos de Pedidos',
                                                           'id' => 'mnuPedidoInternoCalendario',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmPedidoInternoCalendario',
																			 'controller' => 'PedidosInternos', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_PI_CALENDARIO_VTO'),
                                                         ),
                                                         */
                                                     array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Logistica',
                                                           'id' => 'mnuLogistiica',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('RAIZ_LOGISTICA'),
                                                           'childrens' => array(
																				array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Transportistas',
																					   'id' => 'mnuTransportista',
																					   'action' => array('form' =>  'CarpeDiem.App.frmTransportista',
																					   'controller' => 'Transportistas', 'action' => 'index'),
																					   'ajax' => true,
																					   'functions' => array('MENU_TRANSPORTISTA'),
																					 )
                                                           )
                                                         ),
														 
														 
														 array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Reportes',
                                                           'id' => 'mnuVentasReporte',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_VENTAS_REPORTE'),
                                                           'childrens' => array(
                                                           		
                                                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Libro IVA Venta',
                                                                                   'id' => 'mnuLibroIvaVenta',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmLibroIVAVentas','form_param'=>'VENTAS',
																									 'controller' => 'Reportes', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_VENTAS_REPORTE'),
                                                                                   ), 
																				   array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Impuestos por Jurisdicci&oacute;n',
                                                                                   'id' => 'mnuImpuestoPorJurisdiccionVentas', // PRIMERA VEZ
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmComprobanteImpuestoJurisdiccion',
																									'form_param'=>    array('id_sistema' => array(EnumSistema::VENTAS) ),	
																									 'controller' => 'ComprobanteImpuestos', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_VENTAS_REPORTE'),
                                                                                   ),
                                                           		
                                                           		/*
																				array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Libro Ventas por Jurisdicci&oacute;n',
                                                                                   'id' => 'mnuLibroVentaPorJurisdiccion',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmLibroVentasPorJurisdiccion',
																									 'controller' => 'Reportes', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_VENTAS_REPORTE'),
                                                                                   ),*/
                                                           		array(
                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           				'caption' => 'Reporte de Ventas por Articulo',
                                                           				'id' => 'mnuReprote1',
                                                           				'action' => array('form' =>  'CarpeDiem.App.frmReporte',
                                                           						'form_param'=>
                                                           						array('id_reporte' =>
                                                           								array(EnumReporte::VentasPorArticulo),
                                                           								'id_sistema' =>
                                                           								array(EnumSistema::VENTAS),
                                                           								'form_title' =>
                                                           								"Listado de Ventas por articulo",
                                                           								'form_view' =>
                                                           								"ventas_por_pedido_interno",
                                                           								'id_tipo_comprobante' =>
                                                           								array(EnumTipoComprobante::PedidoInterno),
                                                           						),
                                                           						'controller' => 'Reportes', 'action' => 'UnidadesPorArticulo'),
                                                           				'ajax' => true,
                                                           				'functions' => array('MENU_VENTAS_REPORTE'),
                                                           		),
                                                           		
                                                           		array(
                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           				'caption' => 'Reporte de Ventas por Jurisdici&oacute;n',
                                                           				'id' => 'mnuReporteVentasPorJurisdiccion',
                                                           				'action' => array('form' =>  'CarpeDiem.App.frmReporte',
                                                           						'form_param'=>
                                                           						array('id_reporte' =>
                                                           								array(EnumReporte::VentasPorJurisdiccion),
                                                           								'id_sistema' =>
                                                           								array(EnumSistema::VENTAS),
                                                           								'form_title' =>
                                                           								"Listado de Ventas por Jurisdici&oacute;n",
                                                           								'form_view' =>
                                                           								"ventas_por_jurisdiccion"
                                                           							
                                                           						),
                                                           						'controller' => 'Reportes', 'action' => 'VentasPorJurisdiccion'),
                                                           				'ajax' => true,
                                                           				'functions' => array('MENU_VENTAS_REPORTE'),
                                                           		),
                                                           		
                                                           		
                                                           		
                                                           		array(
                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           				'caption' => 'Impuestos en Comprobantes',
                                                           				'id' => 'mnuComprobanteImpuestoVentas',
                                                           				'action' => array('form' =>  'CarpeDiem.App.frmComprobanteImpuesto',
                                                           						'form_param'=>
                                                           						array('id_sistema' =>
                                                           								array(EnumSistema::VENTAS) ),
                                                           						'controller' => 'ComprobanteImpuestos', 'action' => 'ReporteComprobanteImpuesto'),
                                                           				'ajax' => true,
                                                           				'functions' => array('MENU_VENTAS_REPORTE'),
                                                           		)
                                                                                
																				   
																					
																				
																				   
																				
																				

																																				   																				   
                                                                                )
                                                           ),
														    
                                                         
                                                          array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Tablas',
                                                           'id' => 'mnuTablaComercialRaiz',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_TABLA_COMERCIAL'),
                                                           'childrens' => array(
                                                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Tipos de Impuestos',
                                                                                   'id' => 'mnuTipoImpuestoVentas',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmTipoImpuesto',
																									 'controller' => 'TiposImpuesto', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_TIPO_IMPUESTO'),
                                                                                   ), 
																				/*array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Sistemas',
                                                                                   'id' => 'mnuSistema',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmSistema',
																									 'controller' => 'Sistemas', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_SISTEMA'),
                                                                                   ), */ 
                                                                                 array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Impuestos',
                                                                                   'id' => 'mnuImpuestoVentas',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmImpuesto', 
																									 'form_param'=>    array('id_sistema' =>
                                                                                                                            array(EnumSistema::VENTAS) ),
																									 'controller' => 'Impuestos', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_IMPUESTO'),
                                                                                   ),
																				   
																				  array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Lotes de Comprobantes',
                                                                                   'id' => 'mnuComprobanteLote',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmComprobanteLote', 
																									
																									 'controller' => EnumController::ComprobanteLotes, 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_COMPROBANTE_LOTE'),
                                                                                   ),
																				   
																				   
                                                                                    																		   
                                                                                )
                                                           ),
                                                            
                                                         
                                                         
                                                )
                             ),
     		
     		
     		
     		array(//////////////////////////////MODULO/////////////////////////
     				'caption' => 'RMA',
     				'id' => 'mnuRma',
     				'img' => 'icons/packs/fugue/16x16/user-white.png',
     				'root' =>true,
     				'action' => null,
     				'html_menu'=>true,
     				'ajax' => false,
     				'functions' => array('RAIZ_RMA'),
     				'childrens' => array(
     						
     						
     						array(
     								'img' => 'icons/packs/fugue/16x16/application-form.png',
     								'caption' => 'RMA Ventas',
     								'id' => 'mnuRmaVentaMenu',
     								'action' => array('controller' => EnumController::ReclamosVenta, 'action' => 'index'),
     								'ajax' => true,
     								
     								'functions' => array('MENU_RECLAMO_VENTA'),
     								'childrens' => array(
				     										array(
				     												'img' => 'icons/packs/fugue/16x16/application-form.png',
				     												'caption' => 'Listado de RMA',
				     												'id' => 'mnuRmaVentaListado',
				     												'action' => array('form' =>  'CarpeDiem.App.frmRMA',
				     														'controller' => EnumController::ReclamosVenta, 'action' => 'index',  'form_desktop' => 'frmRMA',
				     														'context_menu'=> array( array('control' => 'gv', 'options' => array(
				     															
																							array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
																									'action' =>'msVerComprobanteRelacionado_Click',
																									'functions' => array('CONSULTA_RECLAMO_VENTA') ),
																							
																							array('text' =>'ANULAR',    'type' => 'text',
																									'action' =>'btnAnular_Click',
																									'functions' => array('BAJA_RECLAMO_VENTA') ),
				     																
																							)
																				)
																			)
				     												),
				     												'ajax' => true,
				     												'functions' => array('CONSULTA_RECLAMO_VENTA'),
				     										),
     										array(
     												'img' => 'icons/packs/fugue/16x16/application-form.png',
     												'caption' => 'Nuevo RMA',
     												'id' => 'mnuRMAMD',
     												'action' => array('form' =>  'CarpeDiem.App.frmRMAMD',
     														'controller' => EnumController::ReclamosVenta, 'action' => 'index',  'form_desktop' => 'frmRMAMD'),
     												'ajax' => true,
     												'functions' => array('ADD_RECLAMO_VENTA'),
     										),
				     						
     												)
     				
     				))),
							 
							  array(//////////////////////////////MODULO/////////////////////////
							  'caption' => 'Stock',
							  'id' => 'mnuStock',
							  'img' => 'icons/packs/fugue/16x16/user-white.png',
							  'root' =>true,
							  'html_menu'=>true,
							  'action' => null,
							  'ajax' => false,
							  'functions' => array('RAIZ_STOCK'),
							  'childrens' => array(
                                                    array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Depositos',
                                                           'id' => 'mnuDeposito',
                                                           'action' => array('controller' => 'Depositos', 'action' => 'index'),
                                                           'ajax' => true,
                                                
                                                           'functions' => array('MENU_DEPOSITO'),
                                                           'childrens' => array(
                                                                                 array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Listado de Depositos',
                                                                                       'id' => 'mnuDepositoListado',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmDeposito',
                                                                                                        'controller' => EnumController::Depositos, 'action' => 'index',  'form_desktop' => 'frmDeposito'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('CONSULTA_DEPOSITO'),
                                                                                     ),
                                                                                 array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Nuevo Dep&oacute;sito',
                                                                                       'id' => 'mnuDepositoMD',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmDepositoA',
                                                                                                        'controller' => EnumController::Depositos, 'action' => 'index',  'form_desktop' => 'frmDepositoA'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('ADD_DEPOSITO'),
                                                                                     ),
                                                                                 array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Categor&iacute;as de Dep&oacute;sito',
                                                                                       'id' => 'mnuDepositoCategorias',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmDepositoCategoriaA',
                                                                                                        'controller' => 'DepositoCategorias', 'action' => 'index',  'form_desktop' => 'frmDepositoCategoriaA'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('CONSULTA_DEPOSITO_CATEGORIA'),
                                                                                     ),
																					 
																				 array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Listado de Categor&iacute;as de Dep&oacute;sito ',
                                                                                       'id' => 'mnuDepositoCategoriasListado',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmDepositoCategoria',
                                                                                                       'controller' => 'DepositoCategorias', 'action' => 'index',  'form_desktop' => 'frmDepositoCategoria'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('CONSULTA_DEPOSITO_CATEGORIA'),
                                                                                     ),
                                                           		
                                                           		array(
                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           				'caption' => 'Movimientos de Stock por Deposito y Tipos de Comprobante',
                                                           				'id' => 'mnuDepositoMovimientoTipos',
                                                           				'action' => array('form' =>  'CarpeDiem.App.frmDepositoMovimientoTipo',
                                                           						'controller' => 'DepositoMovimientoTipos', 'action' => 'index',  'form_desktop' => 'frmDepositoMovimientoTipo'),
                                                           				'ajax' => true,
                                                           				'functions' => array('CONSULTA_DEPOSITO'),
                                                           		),
                                                           		
                                                           		array(
                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           				'caption' => 'Nuevo Movimiento de Stock por Deposito y Tipo de Comprobante',
                                                           				'id' => 'mnuDepositoMovimientoTiposA',
                                                           				'action' => array('form' =>  'CarpeDiem.App.frmDepositoMovimientoTipoA',
                                                           						'controller' => 'DepositoMovimientoTipos', 'action' => 'index',  'form_desktop' => 'frmDepositoMovimientoTipoA'),
                                                           				'ajax' => true,
                                                           				'functions' => array('CONSULTA_DEPOSITO'),
                                                           		),
                                                                                     
                                                                                     
                                                    )    ),
													array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Informe Recepci&oacute;n de Materiales',
                                                           'id' => 'mnuInformeRecepcionMaterialesRaiz',
                                                           'ajax' => true,
                                                           'functions' => array('MENU_INFORME_RECEPCION_MATERIAL'),
                                                           'childrens' => array(
                                                                               array(
                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                               'caption' => 'Listado de Informes de Recepci&oacute;n de Materiales',
                                                                               'id' => 'mnuInformeRecepcionMaterialesLista',
                                                                               'action' => array('form' =>  'CarpeDiem.App.frmInformeRecepcionMaterial',
																								 'controller' => EnumController::InformeRecepcionMateriales, 'action' => 'index',
																								 'context_menu'=> array( array('control' => 'gv', 'options' => array(
                                                                                                                                                                array('text' =>'Ver &amp;Asientos','type' => 'text',
                                                                                                                                                                        'action' =>'msVerAsientos_Click',
																																										'functions' => array('MENU_ASIENTO') ),
																																										
                                                                                                                                                                array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                                                                                                                                                                      'action' =>'msVerComprobanteRelacionado_Click',
																																									  'functions' => array('CONSULTA_INFORME_RECEPCION_MATERIAL') ), 
																																								
                                                                                                                                                                array('text' =>'ANULAR',    'type' => 'text',
                                                                                                                                                                      'action' =>'btnAnular_Click',
																																									  'functions' => array('BAJA_INFORME_RECEPCION_MATERIAL') ),
																																									  
                                                                                                                                                                array('text' =>'Modificar',     'type' => 'text',
                                                                                                                                                                      'action' =>'btnEditar_Click',
																																									  'functions' => array('ADD_INFORME_RECEPCION_MATERIAL') ),
                                                                                                                                                                )
                                                                                                                           ) )
																								),
                                                                               'ajax' => true,
                                                                               'functions' => array('MENU_INFORME_RECEPCION_MATERIAL')
                                                                               ),
                                                           		
			                                                           		array(
			                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
			                                                           				'caption' => 'Art&iacute;culos presentes en el IRM', // INVISIBLE
			                                                           				'id' => 'mnuIRMItems',
			                                                           				'action' => array('form' =>  'CarpeDiem.App.frmInformeRecepcionMaterialItem', //'form_param'=>'PI_Item',
			                                                           						
			                                                           						'controller' => EnumController::InformeRecepcionMaterialesItems, 'action' => 'index',
			                                                           						'form_desktop' => 'frmInformeRecepcionMaterialItem',
			                                                           						'form_param'=>array('id_sistema' => array(EnumSistema::STOCK),'id_tipo_comprobante' => array(EnumTipoComprobante::InformeRecepcionMateriales))
			                                                           				),
			                                                           				'ajax' => true,
			                                                           				'functions' => array('MENU_INFORME_RECEPCION_MATERIAL'),
			                                                           		),
																			   
																			   //TODOS LOS COMPROBANTES Q TENGAN Q USAR frmCOmprobanteItem -> esta pensando para reutilizar los comprobantes de compras
																			 array(
																			   'img' => 'icons/packs/fugue/16x16/application-form.png',
																			   'caption' => '', // INVISIBLE
																			   'id' => 'mnuInformeRecepcionMaterialImportaRMC',
																			   'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem', //'form_param'=>'PI_Item',
																								'listarTipo' => EnumListarTipo::Picker, 
																								'controller' => EnumController::RemitoCompraItems, 'action' => 'index',  'form_desktop' => 'frmComprobanteItem'),
																			   'ajax' => true,
																			   'functions' => array('MENU_INFORME_RECEPCION_MATERIAL'),
																			 ) ,

																			   
                                                                               array(
                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                               'caption' => 'Informe Recepci&oacute;n de Materiales Por Remito de Compra',
                                                                               'id' => 'mnuInformeRecepcionMaterialesMDRMC',
                                                                               'action' => array('form' =>  'CarpeDiem.App.frmInformeRecepcionMaterialMD',
																								 'controller' => EnumController::InformeRecepcionMateriales,
																								 'form_param'=>'RMC',
																								 'action' => 'index'),
                                                                               'ajax' => true,
                                                                               'functions' => array('MENU_INFORME_RECEPCION_MATERIAL')
                                                                               ),
                                                         )
                                                         ),
							  		
							  		array(
							  				'img' => 'icons/packs/fugue/16x16/application-form.png',
							  				'caption' => 'Remito Interno',
							  				'id' => 'mnuRemitoInternoMenu',
							  				'action' => array('controller' => EnumController::RemitosInterno, 'action' => 'index'),
							  				'ajax' => true,
							  				
							  				'functions' => array('MENU_REMITO_INTERNO'),
							  				'childrens' => array(
							  						array(
							  								'img' => 'icons/packs/fugue/16x16/application-form.png',
							  								'caption' => 'Listado de Remito Interno',
							  								'id' => 'mnuRemitoInternoListado',
							  								'action' => array('form' =>  'CarpeDiem.App.frmRemitoInterno',
							  										'controller' => EnumController::RemitosInterno, 'action' => 'index',  'form_desktop' => 'frmRemitoInterno',
							  										'context_menu'=> array( array('control' => 'gv', 'options' => array(
							  												
							  										
							  												array('text' =>'ANULAR',    'type' => 'text',
							  														'action' =>'btnAnular_Click',
							  														'functions' => array('BAJA_REMITO_INTERNO') ),
							  												
							  										)
							  										)
							  										)
							  								),
							  								'ajax' => true,
							  								'functions' => array('CONSULTA_REMITO_INTERNO'),
							  						),
							  						array(
							  								'img' => 'icons/packs/fugue/16x16/application-form.png',
							  								'caption' => 'Nuevo Remito Interno',
							  								'id' => 'mnuRemitoInternoMD',
							  								'action' => array('form' =>  'CarpeDiem.App.frmRemitoInternoMD',
							  										'controller' => EnumController::RemitosInterno, 'action' => 'index',  'form_desktop' => 'frmRemitoInternoMD'),
							  								'ajax' => true,
							  								'functions' => array('ADD_REMITO_INTERNO'),
							  						),
							  						
							  						array(
							  								'img' => 'icons/packs/fugue/16x16/application-form.png',
							  								'caption' => 'Control de Art&iacute;culos en Remitos', // INVISIBLE
							  								'id' => 'mnuRemitoCompraItems',
							  								'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem', //'form_param'=>'PI_Item',
							  										
							  										'controller' => EnumController::RemitoCompraItems, 'action' => 'index',
							  										'form_desktop' => 'frmComprobanteItem',
							  										'form_param'=>array('id_sistema' => array(EnumSistema::STOCK),'id_tipo_comprobante' => array(EnumTipoComprobante::RemitoInterno))
							  								),
							  								'ajax' => true,
							  								'functions' => array('MENU_REMITO_INTERNO'),
							  						)
							  						
							  				)
							  				
							  		),
                                                     array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Movimientos de Materiales',
                                                           'id' => 'mnuMovimiento',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmMovimiento',
																			 'controller' => EnumController::Movimientos, 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_MOVIMIENTO')
                                                         ),
														 
													array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Stock Producto Manufacturado',
															'html_menu'=>true,
                                                           'id' => 'mnuStockProductos',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmStockArticulos',
																			'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::Producto)),
																			 'controller' => EnumController::StockProductos, 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_STOCK_PRODUCTO')
                                                         ),
                                                          array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Stock Producto Retail',
                                                           'html_menu'=>true,
                                                           'id' => 'mnuStockProductosRetail',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmStockArticulos',
																			'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::MRETAIL)),
                                                                             'controller' => EnumController::StockProductosRetail, 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_STOCK_PRODUCTO_RETAIL')
                                                         ), 
                                                         array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Stock Materia Prima',
                                                           'html_menu'=>true,
                                                           'id' => 'mnuStockMateriaPrimas',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmStockArticulos',
																			'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::MateriaPrima)),
                                                                             'controller' => EnumController::StockMateriaPrimas, 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_STOCK_MATERIA_PRIMA')
                                                         ),
                                                         
                                                         
                                                        array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Stock Componente / Semielaborado',
                                                           'id' => 'mnuStockComponentes',
                                                        	'html_menu'=>true,
                                                           'action' => array('form' =>  'CarpeDiem.App.frmStockArticulos','form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::Componente)),
                                                                             'controller' => EnumController::StockComponentes, 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_STOCK_COMPONENTE')
                                                         ),
                                                          array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Stock Packaging',
                                                          	'html_menu'=>true,
                                                           'id' => 'mnuStockPackaging',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmStockArticulos','form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::MPE)),
                                                                             'controller' => EnumController::StockPackaging, 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_STOCK_PACKAGING')
                                                         ),
                                                         array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Stock Bien de Uso',
                                                         	'html_menu'=>true,
                                                           'id' => 'mnuStockBienesUso',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmStockArticulos','form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::BIENUSO)),
                                                                             'controller' => EnumController::StockBienesUso, 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_STOCK_BIEN_USO')
                                                         ),
                                                         array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Stock Insumo Interno',
                                                         	'html_menu'=>true,
                                                           'id' => 'mnuStockInsumosInternos',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmStockArticulos','form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::INSUMOINTERNO)),
                                                                             'controller' => EnumController::StockInsumosInterno, 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_STOCK_INSUMO_INTERNO')
                                                         ),
														  array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Stock Scrap',
														  	'html_menu'=>true,
                                                           'id' => 'mnuStockScrap',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmStockArticulos','form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::SCRAP)),
                                                                             'controller' => EnumController::StockScrap, 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_STOCK_SCRAP')
                                                         ),
								)
							  ),
                             array(//////////////////////////////MODULO/////////////////////////
                              'caption' => 'Compras',
                              'id' => 'mnuCompra',
                              'img' => 'icons/packs/fugue/16x16/user-white.png',
                              'root' =>true,
                              'action' => null,
                              'ajax' => false,
                              'functions' => array('RAIZ_COMPRAS'),
                              'childrens' => array(
												   //TODO: MOVER TODOS LOS COMPROBANTES Q TENGAN Q USAR frmCOmprobanteItem -> esta pensando para reutilizar los comprobantes de compras
													 array(
													   'img' => 'icons/packs/fugue/16x16/application-form.png',
													   'caption' => '', //-------------------------- INVISIBLE -- BOTON IMPORTAR DENTRO DEL FORM - LEVANTE COMPROBANTE ITEM -------------------------------------------------------------
													   'id' => 'mnuFacturaCompraImportaOC',
													   'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem', //'form_param'=>'PI_Item',
																		'listarTipo' => EnumListarTipo::Picker, 
																		'controller' =>  EnumController::OrdenCompraItems, 'action' => 'index',  'form_desktop' => 'frmComprobanteItem'),	
													   'ajax' => true,
													   'functions' => array('ADD_FACTURA_COMPRA_IMPORTADO'),
													 ),
												
													 //TODO: MOVER TODOS LOS COMPROBANTES Q TENGAN Q USAR frmCOmprobanteItem -> esta pensando para reutilizar los comprobantes de compras
													 array(
													   'img' => 'icons/packs/fugue/16x16/application-form.png',
													   'caption' => '', //-------------------------- INVISIBLE -- BOTON IMPORTAR DENTRO DEL FORM - LEVANTE COMPROBANTE ITEM -------------------------------------------------------------
													   'id' => 'mnuRemitoCompraImportaOC',
													   'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem', //'form_param'=>'PI_Item',
																		'listarTipo' => EnumListarTipo::Picker, 
																		'controller' =>  EnumController::OrdenCompraItems, 'action' => 'index',  'form_desktop' => 'frmComprobanteItem'),
													   'ajax' => true,
													   'functions' => array('ADD_REMITO_COMPRA_IMPORTADO'),
													 ) ,
										
													array(
														'img' => 'icons/packs/fugue/16x16/application-form.png',
														'caption' => '', //-------------------------- INVISIBLE -- BOTON IMPORTAR DENTRO DEL FORM - LEVANTE COMPROBANTE ITEM -------------------------------------------------------------
														'id' => 'mnuFacturaCompraImportaRemito',
														'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem', //'form_param'=>'PI_Item',
																'listarTipo' => EnumListarTipo::Picker,
																'controller' =>  EnumController::RemitoCompraItems, 'action' => 'index',  'form_desktop' => 'frmComprobanteItem'),
														'ajax' => true,
														'functions' => array('ADD_REMITO_COMPRA_IMPORTADO'),
													),
										
													array(
															'img' => 'icons/packs/fugue/16x16/application-form.png',
															'caption' => '', //-------------------------INVISIBLE  -- BOTON IMPORTAR DENTRO DEL FORM - LEVANTE COMPROBANTE ITEM -------------------------------------------------------------
															'id' => 'mnuPedidoVentaImportaCotizacion',
															'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem', 
																'form_param'=>'PI_Item',
																'listarTipo' => EnumListarTipo::Picker,
																'controller' =>  EnumController::CotizacionItems, 'action' => 'index',  'form_desktop' => 'frmComprobanteItem'),
															'ajax' => true,
															'functions' => array('ADD_PEDIDO_INTERNO_IMPORTADO'),
													),
													//TODOS LOS COMPROBANTES Q TENGAN Q USAR frmCOmprobanteItem -> esta pensando para reutilizar los comprobantes de compras
													 array(
													   'img' => 'icons/packs/fugue/16x16/application-form.png',
													   'caption' => '', //-------------------------- INVISIBLE -- BOTON IMPORTAR DENTRO DEL FORM - LEVANTE COMPROBANTE ITEM -------------------------------------------------------------
													   'id' => 'mnuNotaCompraImportaFCC',
													   'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem', //'form_param'=>'PI_Item',
																		'listarTipo' => EnumListarTipo::Picker, 
																		'controller' =>  EnumController::FacturaCompraItems, 'action' => 'index',  'form_desktop' => 'frmComprobanteItem'),
													   'ajax' => true,
													   'functions' => array('ADD_NOTA_COMPRA_IMPORTADO'),
													 ) ,
                                                    array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Proveedores',
                                                           'id' => 'mnuProveedoresRaiz',
                                                            'action' => array(//'form' =>  'CarpeDiem.App.frmProveedor',
																			  'controller' => 'Proveedores', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_PROVEEDOR'),
                                                           'childrens' => array(
                                                                               array(
                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                               'caption' => 'Administraci&oacute;n  de Proveedores',
                                                                               'id' => 'mnuProveedores',
                                                                               'action' => array('form' =>  'CarpeDiem.App.frmProveedor',
																						 'controller' => EnumController::Proveedores, 'action' => 'index'),
                                                                               'ajax' => true,
                                                                               'functions' => array('MENU_PROVEEDOR')
                                                                               
                                                                               ),
                                                                                array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => 'Categorias de Proveedor',
                                                                                           'id' => 'mnuProveedorCategoria',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmPersonaCategoria',
                                                                                                         'controller' => 'ProveedoresCategorias', 'action' => 'index',
                                                                                            		'form_param'=>    array('id_tipo_persona' =>
                                                                                            				array(EnumTipoPersona::Proveedor)) 
                                                                                            ),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('MENU_PROVEEDOR_CATEGORIA'),
                                                                                           'childrens' => array()
                                                                                    ), 
				                                                           		array(
				                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
				                                                           				'caption' => 'Origen del Proveedor',
				                                                           				'id' => 'mnuOrigenProveedor',
				                                                           				'action' => array('form' =>  'CarpeDiem.App.frmPersonaOrigen',
				                                                           						'controller' => EnumController::ProveedoresOrigen, 'action' => 'index',
				                                                           						'form_param'=>    array('id_tipo_persona' =>
				                                                           								array(EnumTipoPersona::Proveedor))
				                                                           						
				                                                           				),
				                                                           				'ajax' => true,
				                                                           				'functions' => array('MENU_PROVEEDOR_ORIGEN'),
				                                                           				'childrens' => array()
				                                                           		),
                                                                               array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Nuevo Proveedor',
																				   'id' => 'mnuProveedorA',
																				   'action' => array('form' =>  'CarpeDiem.App.frmProveedorA',
																							 'controller' => EnumController::Proveedores, 'action' => 'index'),
																				   'ajax' => true,
																				   'functions' => array('ADD_PROVEEDOR')	
                                                                               ),
                                                                                array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Cuentas Corrientes',
																				   'id' => 'mnuCuentaCorrienteProveedor',
																				   'action' => array('form' =>  'CarpeDiem.App.frmCuentaCorrienteProveedor',
																							 'controller' => 'CuentasCorrienteProveedor', 'action' => 'index'),
																				   'ajax' => true,
																				   'functions' => array('MENU_CUENTA_CORRIENTE_PROVEEDOR')
                                                                               
                                                                               ),
                                                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Deudas Totales Agrupadas por proveedor',
                                                                                   'id' => 'mnuDeudaTotal',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmComprobantesImpagosAgrupado', 
																									 'form_param'=>    array('id_sistema' => array(EnumSistema::COMPRAS),
																									 'form_title'=>'Deudas Totales Agrupadas por proveedor'	
																									 		
																									 ),
                                                                                                     'controller' => 'Comprobantes', 
																									 'action' => 'getComprobantesImpagosAgrupadoPorPersona'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_CUENTA_CORRIENTE_PROVEEDOR'),
                                                                                   ),
																				array(
																						'img' => 'icons/packs/fugue/16x16/application-form.png',
																						'caption' => 'Listado de Comprobantes Adeudados',
																						'id' => 'mnuComprobantesImpagosCompraListado',
																						'action' => array('form' =>  'CarpeDiem.App.frmComprobante',
																								'form_param'=> array('id_sistema' => array(EnumSistema::COMPRAS),
																										'facturas_impagas' => '1',
																										'form_title'=>'Listado de Comprobantes Adeudados'
																								),
																								'controller' => 'Comprobantes', 'action' => 'getComprobantesImpagos'),
																						'ajax' => true,
																						'functions' => array('MENU_CUENTA_CORRIENTE_PROVEEDOR'),
																				)
                                                           ) 
                                                        ),
                                                        
                                                         array(
                                                            'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                            'caption' => 'Orden de Pago',
                                                            'id' => 'mnuOrdenPagoMenu',
                                                            'action' => array(),
                                                            'ajax' => true,
                                                            'functions' => array('MENU_ORDEN_PAGO'),
                                                            'childrens' => array(
                                                                                  array(
                                                                                        'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                        'caption' => 'Listado de Ordenes de Pago',
                                                                                        'id' => 'mnuOrdenPago',
                                                                                        'action' => array('form' =>  'CarpeDiem.App.frmOrdenPago',
                                                                                                       'controller' => 'OrdenesPago', 'action' => 'index',  'form_desktop' => 'frmOrdenPago',
																									   'context_menu'=> array( array('control' => 'gv', 'options' => array(
                                                                                                   
                                                                                                                                                              array('text' =>'Ver &amp;Asientos','type' => 'text',
                                                                                                                                                                        'action' =>'msVerAsientos_Click',
																																										'functions' => array('MENU_ASIENTO') ),
																																										
                                                                                                                                                                array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                                                                                                                                                                      'action' =>'msVerComprobanteRelacionado_Click',
																																									  'functions' => array('CONSULTA_ORDEN_PAGO') ), 
																																								
                                                                                                                                                                      
                                                                                                                                                                array('text' =>'ANULAR',    'type' => 'text',
                                                                                                                                                                      'action' =>'btnAnular_Click',
																																									  'functions' => array('BAJA_ORDEN_PAGO') ),
                                                                                                                                                                array('text' =>'Modificar',     'type' => 'text',
                                                                                                                                                                      'action' =>'btnEditar_Click',
																																									  'functions' => array('ADD_ORDEN_PAGO') ),
																									   		
																									   		
																																						   		array('text' =>'Ver Comp. &amp;Auditor&iacute;a','type' => 'text',
																																						   				'action' =>'msVerComprobanteAuditoria_Click',
																																						   				'functions' => array('AUDITORIA_ORDEN_PAGO') ),
                                                                                                                                                                )
																									   												
                                                                                                                           ) )
																									   ),
                                                                                        'ajax' => true,
                                                                                        'functions' => array('CONSULTA_ORDEN_PAGO'),
																						
                                                                                      ),
                                                                                       array(
                                                                                        'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                        'caption' => 'Nueva Orden de Pago',
                                                                                        'id' => 'mnuOrdenPagoMD',
                                                                                        'action' => array('form' =>  'CarpeDiem.App.frmOrdenPagoMD',
                                                                                                       'controller' => 'OrdenesPago', 'action' => 'index',  'form_desktop' => 'frmOrdenPagoMD'),
                                                                                        'ajax' => true,
                                                                                        'functions' => array('ADD_ORDEN_PAGO'),
                                                                                      ),
					                                                            		array(
					                                                            				'img' => 'icons/packs/fugue/16x16/application-form.png',
					                                                            				'caption' => 'Listado de Comprobantes Imputados en Pagos',
					                                                            				'id' => 'mnuComprobanteImputadoPagoCompra',
					                                                            				'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem',
					                                                            						'form_param'=>array('id_sistema' => array(EnumSistema::COMPRAS),'id_tipo_comprobante'=>array(EnumTipoComprobante::OrdenPagoManual),'form_title' => 'Listado de Comprobantes Imputados en Pagos'),
					                                                            						'controller' => 'ComprobanteItemComprobantes', 'action' => 'index'),
					                                                            				'ajax' => true,
					                                                            				'functions' => array('CONSULTA_ORDEN_PAGO'),
					                                                            				
					                                                            		),
					                                                            		array(
					                                                            				'img' => 'icons/packs/fugue/16x16/application-form.png',
					                                                            				'caption' => 'Listado de Valores en Pagos',
					                                                            				'id' => 'mnuComprobanteValorCompra',
					                                                            				'action' => array('form' =>  'CarpeDiem.App.frmComprobanteValor',
					                                                            						'form_param'=>array('id_sistema' => array(EnumSistema::COMPRAS),'form_title'=>'Listado de Valores en Pagos' ),
					                                                            						'controller' => 'ComprobanteValores', 'action' => 'index'),
					                                                            				'ajax' => true,
					                                                            				'functions' => array('MENU_COMPROBANTE_VALOR'),
					                                                            				
					                                                            		)
                                                                      )
                                                             ),
                                                     array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Remitos',
                                                           'id' => 'mnuRemitoCompraRaiz',
                                                           'ajax' => true,
                                                           'functions' => array('MENU_REMITO_COMPRA'),
                                                           'childrens' => array(
                                                                               array(
                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                               'caption' => 'Listado de Remitos',
                                                                               'id' => 'mnuRemitoCompra',
                                                                               'action' => array('form' =>  'CarpeDiem.App.frmRemito',
																								 'controller' => EnumController::RemitosCompra, 'action' => 'index',
																								 'context_menu'=> array( array('control' => 'gv', 'options' => array(
                                                                                                   
                                                                                                                                                              array('text' =>'Ver &amp;Asientos','type' => 'text',
                                                                                                                                                                        'action' =>'msVerAsientos_Click',
																																										'functions' => array('MENU_ASIENTO') ),
																																										
                                                                                                                                                                array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                                                                                                                                                                      'action' =>'msVerComprobanteRelacionado_Click',
																																									  'functions' => array('CONSULTA_REMITO_COMPRA') ), 
																																								
																																								//Se puede cambiar por uno permiso  especifico ADD_INFORME_RECEPCION_MATERIAL_SIMULTANEO_POR_REMITO
																																								array('text' =>'Nuevo IRM Simulteaneo',    'type' => 'text',
                                                                                                                                                                      'action' =>'btnSimultaneo_Click',
																																									  'functions' => array('ADD_INFORME_RECEPCION_MATERIAL') ), 
                                                                                                                                                                      
                                                                                                                                                                array('text' =>'ANULAR',    'type' => 'text',
                                                                                                                                                                      'action' =>'btnAnular_Click',
																																									  'functions' => array('BAJA_REMITO_COMPRA') ),
                                                                                                                                                                array('text' =>'Modificar',     'type' => 'text',
                                                                                                                                                                      'action' =>'btnEditar_Click',
																																									  'functions' => array('ADD_REMITO_COMPRA') ),
                                                                                                                                                                )
                                                                                                                           ) )
																								),
                                                                               'ajax' => true,
                                                                               'functions' => array('MENU_REMITO_COMPRA')
                                                                               ),
																			   
                                                                               array(
                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                               'caption' => 'Nuevo Remito por Orden de Compra',
                                                                               'id' => 'mnuRemitoCompraMDOC',
                                                                               'action' => array('form' =>  'CarpeDiem.App.frmRemitoMD',
																								 'controller' => EnumController::RemitosCompra,
																								 'form_param'=>'OC',
																								 'action' => 'index'),
                                                                               'ajax' => true,
                                                                               'functions' => array('MENU_REMITO_COMPRA')
                                                                               
                                                                               ),
																			 /*  
                                                                               array(
                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                               'caption' => 'Nuevo Remito Manual',
                                                                               'id' => 'mnuRemitoCompraMD',
                                                                               'action' => array('form' =>  'CarpeDiem.App.frmRemitoMD',
																								 'controller' => EnumController::RemitosCompra,  'action' => 'index'),
                                                                               'ajax' => true,
                                                                               'functions' => array('MENU_REMITO_COMPRA')
                                                                               
                                                                               ),
                                                           	
                                                           					*/
			                                                           		array(
			                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
			                                                           				'caption' => 'Control de Art&iacute;culos en Remitos', // INVISIBLE
			                                                           				'id' => 'mnuRemitoCompraItems',
			                                                           				'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem', //'form_param'=>'PI_Item',
			                                                           						
			                                                           						'controller' => EnumController::RemitoCompraItems, 'action' => 'index',
			                                                           						'form_desktop' => 'frmComprobanteItem',
			                                                           						'form_param'=>array('id_sistema' => array(EnumSistema::COMPRAS),'id_tipo_comprobante' => array(EnumTipoComprobante::RemitoCompra))
			                                                           				),
			                                                           				'ajax' => true,
			                                                           				'functions' => array('MENU_REMITO_COMPRA'),
			                                                           		)
                                                         )
                                                         ),
														 
														 array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Factura / Recibos',
                                                           'id' => 'mnuFacturaCompra',
                                                           'ajax' => true,
                                                           'functions' => array('MENU_FACTURA_COMPRA'),
                                                           'childrens' => array(
                                                                             array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Listado de Facturas/Recibos',
                                                                                   'id' => 'mnuFacturaCompraL',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmFacturaCompra',
                                                                                                    'controller' => EnumController::FacturasCompra, 'action' => 'index',  'form_desktop' => 'frmFacturaCompra',
																									'context_menu'=> array( array('control' => 'gv', 'options' => array(
																																								array('text' =>'Ver &amp;Asientos','type' => 'text',
                                                                                                                                                                        'action' =>'msVerAsientos_Click',
																																										'functions' => array('MENU_ASIENTO') ),
                                                                                                                                                                array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                                                                                                                                                                      'action' =>'msVerComprobanteRelacionado_Click',
																																									  'functions' => array('CONSULTA_FACTURA_COMPRA') ), 
                                                                                                                                                                array('text' =>'ANULAR',    'type' => 'text',
                                                                                                                                                                      'action' =>'btnAnular_Click',
																																									  'functions' => array('BAJA_FACTURA_COMPRA') ),
                                                                                                                                                                array('text' =>'Modificar',     'type' => 'text',
                                                                                                                                                                      'action' =>'btnEditar_Click',
																																									  'functions' => array('ADD_FACTURA_COMPRA') ),
                                                                                                                                                                )
                                                                                                                           ) 
																													   )
																									),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('CONSULTA_FACTURA_COMPRA'),
                                                                                 ),
                                                                                  array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Nueva Factura',
                                                                                   'id' => 'mnuFacturaCompraMD',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmFacturaCompraMD',
                                                                                                    'controller' => EnumController::FacturasCompra, 'action' => 'index',  'form_desktop' => 'frmFacturaCompraMD'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('BTN_FACTURA_COMPRA_LIBRE'),
                                                                                 ),
                                                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Nueva Factura por Orden de Compra',
                                                                                   'id' => 'mnuFacturaCompraMD_OC',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmFacturaCompraMD', 
																									 'form_param'=>'OC',
                                                                                                    'controller' => EnumController::FacturasCompra, 'action' => 'index',  'form_desktop' => 'frmFacturaCompraMD'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('ADD_FACTURA_COMPRA'),
                                                                                 ),
				                                                           		array(
																					'img' => 'icons/packs/fugue/16x16/application-form.png',
																					'caption' => 'Nueva Factura por Remito de Compra',
																					'id' => 'mnuFacturaCompraMD_RC',
																					'action' => array('form' =>  'CarpeDiem.App.frmFacturaCompraMD',
																							'form_param'=>'RC',
																							'controller' => EnumController::FacturasCompra, 'action' => 'index',  'form_desktop' => 'frmFacturaCompraMD'),
																					'ajax' => true,
																					'functions' => array('ADD_FACTURA_COMPRA'),
				                                                           		),
				                                                           		array(
				                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
				                                                           				'caption' => 'Art&iacute;culos presentes en Facturas', // INVISIBLE
				                                                           				'id' => 'mnuFacturaItemsCompra',
				                                                           				'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem', //'form_param'=>'PI_Item',
				                                                           						
				                                                           						'controller' => EnumController::FacturaCompraItems, 'action' => 'index',
				                                                           						'form_desktop' => 'frmComprobanteItem',
				                                                           						'form_param'=>array('id_sistema' => array(EnumSistema::COMPRAS))
				                                                           				),
				                                                           				'ajax' => true,
				                                                           				'functions' => array('CONSULTA_FACTURA_COMPRA'),
				                                                           		) 
                                                           		
                                                                            )        
                                                         ),
                              		
                              		array(
                              				'img' => 'icons/packs/fugue/16x16/application-form.png',
                              				'caption' => 'Anticipos',

                              				'id' => 'mnuOrdenPagoAnticipo',
                              				'ajax' => true,
                              				'functions' => array('MENU_ANTICIPO_COMPRA'),
                              				'childrens' => array(
                              						array(
                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
                              								'caption' => 'Listado de Anticipos',
                              								'id' => 'mnuOrdenPagoAnticipoL',
                              								'action' => array('form' =>  'CarpeDiem.App.frmOrdenPago',
                              										'controller' => EnumController::OrdenesPagoAnticipo, 'action' => 'index',  'form_desktop' => 'frmOrdenPago',
                              										'context_menu'=> array( array('control' => 'gv', 'options' => array(
                              												
                              												array('text' =>'Ver &amp;Asientos','type' => 'text',
                              														'action' =>'msVerAsientos_Click',
                              														'functions' => array('MENU_ASIENTO') ),
                              												
                              												array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                              														'action' =>'msVerComprobanteRelacionado_Click',
                              														'functions' => array('MENU_ANTICIPO_COMPRA')
                              														
                              														
                              												)
                              												)
                              												)
                              											
                              										
                              										
                              										
                              										)
                              								),
                              								'ajax' => true,
                              								'functions' => array('CONSULTA_ANTICIPO_COMPRA'),
                              						),
                              						array(
                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
                              								'caption' => 'Nueva Orden Pago de Anticipo',
                              								'id' => 'mnuOrdenPagoAnticipoMD',
                              								'action' => array('form' =>  'CarpeDiem.App.frmOrdenPagoMD',
                              										'controller' => EnumController::OrdenesPagoAnticipo, 'action' => 'index',  'form_desktop' => 'frmOrdenPagoMD'),
                              								'ajax' => true,
                              								'functions' => array('ADD_ANTICIPO_COMPRA'),
                              						),		
                              				)
                              		),
                                                         array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Notas Credito / Notas Debito',
                                                           'id' => 'mnuNota',
                                                           // 'action' => array(//'form' =>  'CarpeDiem.App.frmFactura',
                                                                             // 'controller' => 'NotasCompra', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_NOTA_COMPRA'),
                                                           'childrens' => array(
                                                                             array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Listado de Notas de Credito / Notas Debito',
                                                                                   'id' => 'mnuNotaCompraListado',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmFacturaCompra',
                                                                                                    'controller' => 'NotasCompra', 'action' => 'index',  'form_desktop' => 'frmNotaCompra',
																									'context_menu'=> array( array('control' => 'gv', 'options' => array(
                                                                                                                                                              array('text' =>'Ver &amp;Asientos','type' => 'text',
                                                                                                                                                                        'action' =>'msVerAsientos_Click',
																																										'functions' => array('MENU_ASIENTO') ),
																																										
                                                                                                                                                                array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                                                                                                                                                                      'action' =>'msVerComprobanteRelacionado_Click',
																																									  'functions' => array('CONSULTA_NOTA_COMPRA') ), 
																																								
                                                                                                                                                                      
                                                                                                                                                                array('text' =>'ANULAR',    'type' => 'text',
                                                                                                                                                                      'action' =>'btnAnular_Click',
																																									  'functions' => array('BAJA_NOTA_COMPRA') ),
                                                                                                                                                                array('text' =>'Modificar',     'type' => 'text',
                                                                                                                                                                      'action' =>'btnEditar_Click',
																																									  'functions' => array('ADD_NOTA_COMPRA') ),
                                                                                                                                                                )
																															   ) 
																														   )
																									),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('CONSULTA_NOTA_COMPRA'),
                                                                                 ),
																				 
																			
																			 array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Nueva Nota de Credito Manual',
                                                                                   'id' => 'mnuNotaCompra_NCM',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmFacturaCompraMD',
                                                                                                    
                                                                                                     'controller' => 'NotasCreditoCompra', 'action' => 'index',  'form_desktop' => 'frmFacturaCompraMD'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('BTN_NOTA_CREDITO_MANUAL_COMPRA'),
                                                                                 ),
                                                                              array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Nueva Nota de Credito por FC',
                                                                                   'id' => 'mnuNotaCompra_NC_FC',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmFacturaCompraMD','form_param'=>'C_FC',
                                                                                                    'controller' => 'NotasCreditoCompra', 'action' => 'index',  'form_desktop' => 'frmFacturaCompraMD'),
                                                                                   'ajax' => true,
                                                                                   
                                                                                   'functions' => array('ADD_NOTA_COMPRA_IMPORTADO'),
                                                                                 ),
											
                                                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Nueva Nota de Debito Manual',
                                                                                   'id' => 'mnuNotaCompra_ND',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmFacturaCompraMD',
                                                                                                    'controller' => 'NotasDebitoCompra', 'action' => 'index',
																									'form_desktop' => 'frmFacturaCompraMD'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('ADD_NOTA_COMPRA'),
                                                                                 ),
                                                                            )        
                                                         ),
														 array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Cr&eacute;ditos internos',
                                                           'id' => 'mnuCreditoInternoComprasL',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_CREDITO_INTERNO_PROVEEDOR'),
                                                           'childrens' => array( 
                                                           
                                                                                     array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Listado de Cr&eacute;ditos Internos',
                                                                                   'id' => 'mnuCreditoInternoCompras',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmSaldosIniciales',
																								     'form_param'=>array('id_sistema' => array(EnumSistema::COMPRAS) ),
                                                                                                     'controller' => 'CreditosInternoProveedor', 'action' => 'index',
                                                                                   		
                                                                                   		'context_menu'=> array( array('control' => 'gv', 'options' => array(
                                                                                   			
                                                                                   				
                                                                                   				array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                                                                                   						'action' =>'msVerComprobanteRelacionado_Click',
                                                                                   						'functions' => array('CONSULTA_CREDITO_INTERNO_PROVEEDOR') ),
                                                                                   		
                                                                                   		)
                                                                                   		)
                                                                                   		)
                                                                                   
                                                                                   
                                                                                   
                                                                                   
                                                                                   ),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('CONSULTA_CREDITO_INTERNO_PROVEEDOR'),
                                                                                   ), 
                                                           )
                                                           ),
							                              		array(
							                              				'img' => 'icons/packs/fugue/16x16/application-form.png',
							                              				'caption' => 'D&eacute;bitos internos',
							                              				'id' => 'mnuDebitoInternoComprasL',
							                              				'action' => null,
							                              				'ajax' => true,
							                              				'functions' => array('MENU_DEBITO_INTERNO_PROVEEDOR'),
							                              				'childrens' => array(
							                              						
							                              						array(
							                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
							                              								'caption' => 'Listado de D&eacute;bitos Internos',
							                              								'id' => 'mnuDebitoInternoCompras',
							                              								'action' => array('form' =>  'CarpeDiem.App.frmSaldosIniciales',
							                              										'form_param'=>array('id_sistema' => array(EnumSistema::COMPRAS) ),
							                              										'controller' => 'DebitosInternoProveedor', 'action' => 'index',
							                              										'context_menu'=> array( array('control' => 'gv', 'options' => array(
							                              												
							                              												
							                              												array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
							                              														'action' =>'msVerComprobanteRelacionado_Click',
							                              														'functions' => array('CONSULTA_DEBITO_INTERNO_PROVEEDOR') ),
							                              												
							                              										)
							                              										)
							                              										)
							                              								
							                              								),
							                              								'ajax' => true,
							                              								'functions' => array('CONSULTA_DEBITO_INTERNO_PROVEEDOR'),
							                              						),
							                              				)
							                              		),
									
                                                         array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Saldos al inicio',
                                                           'id' => 'mnuSaldosInicio',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_SALDO_INICIAL_PROVEEDOR'),
                                                           'childrens' => array( 
                                                           
                                                                                     array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Listado de Saldos al inicio',
                                                                                   'id' => 'mnuSaldosInicioListadoProveedor',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmSaldosIniciales',
																									'form_param'=>array('id_sistema' => array(EnumSistema::COMPRAS) ),
                                                                                                    'controller' => 'SaldosInicialesProveedor', 'action' => 'index',
				                                                                                   		'context_menu'=> array( array('control' => 'gv', 'options' => array(
				                                                                                   				
				                                                                                   				
				                                                                                   				array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
				                                                                                   						'action' =>'msVerComprobanteRelacionado_Click',
				                                                                                   						'functions' => array('MENU_SALDO_INICIAL_PROVEEDOR') ),
				                                                                                   				
				                                                                                   		)
				                                                                                   		)
				                                                                                   		)
                                                                                   
                                                                                   ),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_SALDO_INICIAL_PROVEEDOR'),
                                                                                   ), 
                                                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Nuevo Saldo al inicio',
                                                                                   'id' => 'mnuSaldosInicialesProveedorMD',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmSaldosInicialesMD',
																									 'form_param'=>array('id_sistema' => array(EnumSistema::COMPRAS) ),
                                                                                                     'controller' => 'SaldosInicialesProveedor', 'action' => 'add'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_SALDO_INICIAL_PROVEEDOR'),
                                                                                   )
                                                           )
                                                           ),
                                                           
                                                            array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Orden de Compra',
                                                           'id' => 'mnuOrdenDeCompra2',
                                                           'action' => false,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_ORDEN_COMPRA'),
                                                           'childrens' => array(
                                                                                 array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Listado de Orden de Compra',
                                                                                   'id' => 'mnuOrdenDeCompra',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmOrdenCompra',
                                                                                         'controller' => 'OrdenCompra', 'action' => 'index',
																						 'context_menu'=> array( array('control' => 'gv', 'options' => array(
                                                                                                   
                                                                                                                                                            
																																										
                                                                                                                                                                array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
                                                                                                                                                                      'action' =>'msVerComprobanteRelacionado_Click',
																																									  'functions' => array('CONSULTA_ORDEN_COMPRA') ), 
																																								
                                                                                                                                                                      
                                                                                                                                                                array('text' =>'ANULAR',    'type' => 'text',
                                                                                                                                                                      'action' =>'btnAnular_Click',
																																									  'functions' => array('BAJA_ORDEN_COMPRA') ),
                                                                                                                                                                array('text' =>'Modificar',     'type' => 'text',
                                                                                                                                                                      'action' =>'btnEditar_Click',
																																									  'functions' => array('ADD_ORDEN_COMPRA') ),
                                                                                                                                                                )
                                                                                                                           ) )
																						 ),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_ORDEN_COMPRA'),
                                                                                 ),
                                                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Nueva Orden de Compra',
                                                                                   'id' => 'mnuOrdenDeCompraMD',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmOrdenCompraMD',
                                                                                         'controller' => 'OrdenCompra', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_ORDEN_COMPRA'),
                                                                                 ),
                                                                                   array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Articulos pendientes por Orden de Compra',
                                                                                   'id' => 'mnuOrdenDeCompraItem',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmOrdenCompraItem',
                                                                                         'controller' => 'OrdenCompraItems', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_ORDEN_COMPRA'),
                                                                                 ),
                                                         )
                                                           
                                                         ),
                                                         
                                                         array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Retenciones',
                                                           'id' => 'mnuVentasReporte',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_RETENCION_PROPIA'),
                                                           'childrens' => array(
                                                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Listado de Retenciones',
                                                                                   'id' => 'mnuRetencion',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmRetencion',
																									'form_param'=> array('id_sistema' => array(EnumSistema::COMPRAS) ),
																									'controller' => 'Retenciones', 'action' => 'index',
																									'context_menu'=> array( array('control' => 'gv', 'options' => array(
																																	array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
																																		  'action' =>'msVerComprobanteRelacionado_Click',
																																		  'functions' => array('CONSULTA_RETENCION_PROPIA') ),
																																	)						
																																) 
																														   )
																									),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('CONSULTA_RETENCION_PROPIA'),
                                                                                   ), 
                                                                                )
                                                           ),
                                                           array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Reportes',
                                                           'id' => 'mnuComprasReporte',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_COMPRAS_REPORTE'),
                                                           'childrens' => array(
                                                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Libro IVA Compras',
                                                                                   'id' => 'mnuLibroIvaCompra',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmLibroIVAVentas','form_param'=>'COMPRAS',
                                                                                                     'controller' => 'Reportes', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_COMPRAS_REPORTE'),
                                                                                   ),
																				    array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Impuestos por Jurisdicci&oacute;n',
                                                                                   'id' => 'mnuImpuestoPorJurisdiccionCompras',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmComprobanteImpuestoJurisdiccion',
																									'form_param'=>    array('id_sistema' => array(EnumSistema::COMPRAS) ) ,	
																									 'controller' => 'ComprobanteImpuestos', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_COMPRAS_REPORTE'),
                                                                                   ),
                                                                                   array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Impuestos en Comprobantes',
                                                                                   'id' => 'mnuComprobanteImpuesto',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmComprobanteImpuesto',
																									 'form_param'=> 
																												array('id_sistema' =>
																															array(EnumSistema::COMPRAS) ),
                                                                                                     'controller' => 'ComprobanteImpuestos', 'action' => 'ReporteComprobanteImpuesto'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_COMPRAS_REPORTE'),
                                                                                   ),
						                                                           		array(
						                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
						                                                           				'caption' => 'Reporte de Compras por Proveedor',
						                                                           				'id' => 'mnuReproteComprasPorProveedor',
						                                                           				'action' => array('form' =>  'CarpeDiem.App.frmReporte',
						                                                           						'form_param'=>
						                                                           						array('id_reporte' =>
						                                                           								array(EnumReporte::TotalComprobantePorPersona),
						                                                           								'id_sistema' =>
						                                                           								array(EnumSistema::COMPRAS),
						                                                           								'form_title' =>
						                                                           								"Listado de Compras por proveedor",
						                                                           								'form_view' =>
						                                                           								"total_comprobante_por_persona",
						                                                           								'id_tipo_comprobante' =>
						                                                           								array(EnumTipoComprobante::OrdenCompra),
						                                                           						),
						                                                           						'controller' => 'Reportes', 'action' => 'TotalComprobantePorPersona'),
						                                                           				'ajax' => true,
						                                                           				'functions' => array('MENU_COMPRAS_REPORTE'),
						                                                           		),
                                                           		array(
                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           				'caption' => 'Reporte de Compras por Art&iacute;culo',
                                                           				'id' => 'mnuComprasporArticulo',
                                                           				'action' => array('form' =>  'CarpeDiem.App.frmReporte',
                                                           						'form_param'=>
                                                           						array('id_reporte' =>
                                                           								array(EnumReporte::ComprasPorArticulo),
                                                           								'id_sistema' =>
                                                           								array(EnumSistema::COMPRAS),
                                                           								'form_title' =>
                                                           								"Listado de Compras por Art&iacute;culo",
                                                           								'form_view' =>
                                                           								"compras_por_orden_compra",
                                                           								'id_tipo_comprobante' =>
                                                           								array(EnumTipoComprobante::OrdenCompra),
                                                           						),
                                                           						'controller' => 'Reportes', 'action' => 'UnidadesPorArticulo'),
                                                           				'ajax' => true,
                                                           				'functions' => array('MENU_COMPRAS_REPORTE'),
                                                           		),
																				  																				   
                                                                                )
                                                           ),
							                              		array(
							                              				'img' => 'icons/packs/fugue/16x16/application-form.png',
							                              				'caption' => 'Lista de Precio',
							                              				'id' => 'mnuRaizListaPrecioCompra',
							                              				'action' => array(),
							                              				'ajax' => true,
							                              				'functions' => array('MENU_LISTA_PRECIO'),
							                              				'childrens' => array(
							                              						array(
							                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
							                              								'caption' => 'Consulta de listas de precio',
							                              								'id' => 'mnuListaPrecioCompra',
							                              								'action' => array('form' =>  'CarpeDiem.App.frmListaPrecio',
							                              										'form_param'=>    array('id_sistema' => array(EnumSistema::COMPRAS) ),
							                              										'controller' => 'ListaPrecios', 'action' => 'index',  'form_desktop' => 'frmListaPrecio',
							                              										'context_menu'=> array( array('control' => 'gv', 'options' => array(
							                              												array('text' =>'Anular',    'type' => 'text',
							                              														'action' =>'btnAnular_Click',
							                              														'functions' => array('BAJA_LISTA_PRECIO') ),
							                              												
							                              												array('text' =>'Modificar',     'type' => 'text',
							                              														'action' =>'btnEditar_Click',
							                              														'functions' => array('ADD_LISTA_PRECIO') ),
							                              										)
							                              										) )
							                              								),
							                              								'ajax' => true,
							                              								'functions' => array('CONSULTA_LISTA_PRECIO'),
							                              						),
							                              						
							                              						
							                              						
							                              						array(
							                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
							                              								'caption' => 'Alta de Lista Precio Base',
							                              								'id' => 'mnuListaPrecioMDBaseCompra',
							                              								'action' => array('form' =>  'CarpeDiem.App.frmListaPrecioMD',
							                              										'controller' => 'ListaPrecios', 'action' => 'index',
							                              										'form_desktop' => 'mnuListaPrecioMD',
							                              										'form_param'=>    array('id_sistema' => array(EnumSistema::COMPRAS),'id_tipo_lista_precio'=>EnumTipoListaPrecio::Base)
							                              								),
							                              								'ajax' => true,
							                              								'functions' => array('ADD_LISTA_PRECIO'),
							                              						),
							                              						array(
							                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
							                              								'caption' => 'Alta de Lista Precio Enlazada',
							                              								'id' => 'mnuListaPrecioMDEnlazadaCompra',
							                              								'action' => array('form' =>  'CarpeDiem.App.frmListaPrecioMD',
							                              										'controller' => 'ListaPrecios', 'action' => 'index',
							                              										'form_desktop' => 'mnuListaPrecioMD',
							                              										'form_param'=>    array('id_sistema' => array(EnumSistema::COMPRAS),'id_tipo_lista_precio'=>EnumTipoListaPrecio::Enlazada)
							                              								),
							                              								'ajax' => true,
							                              								'functions' => array('ADD_LISTA_PRECIO'),
							                              						)
							                              						
							                              				)
							                              		),
                                                           array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Tablas',
                                                           'id' => 'mnuTablaComercialRaiz',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_TABLA_COMERCIAL'),
                                                           'childrens' => array(
                                                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Tipos de Impuestos',
                                                                                   'id' => 'mnuTipoImpuestoCompras',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmTipoImpuesto',
                                                                             'controller' => 'TiposImpuesto', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_TIPO_IMPUESTO'),
                                                                                   ), 
                                                           		
					                                                           	
					                                                           		array(
					                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
					                                                           				'caption' => 'Tipos Comercializacion',
					                                                           				'id' => 'mnuTipoComercializacion',
					                                                           				'action' => array('form' =>  'CarpeDiem.App.frmTipoComercializacion',
					                                                           						'form_param'=>    array('id_sistema' => array(EnumSistema::COMPRAS) ),
					                                                           						'controller' => 'TiposComercializacion', 'action' => 'index'),
					                                                           				'ajax' => true,
					                                                           				'functions' => array('MENU_TIPO_COMERCIALIZACION'),
					                                                           		),
					                                                           		array(
					                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
					                                                           				'caption' => 'Alta de Tipos Comercializacion',
					                                                           				'id' => 'mnuTipoComercializacionA',
					                                                           				'action' => array('form' =>  'CarpeDiem.App.frmTipoComercializacionA',
					                                                           						'form_param'=>    array('id_sistema' => array(EnumSistema::COMPRAS) ),
					                                                           						'controller' => 'TiposComercializacion', 'action' => 'index'),
					                                                           				'ajax' => true,
					                                                           				'functions' => array('MENU_TIPO_COMERCIALIZACION'),
					                                                           		),
                                                                               
                                                                                 array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Impuestos',
                                                                                   'id' => 'mnuImpuestoCompras',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmImpuesto', 
																									 'form_param'=>    array('id_sistema' => array(EnumSistema::COMPRAS) ),
																									 'controller' => 'Impuestos', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_IMPUESTO'),
                                                                                   ), 
                                                           		
                                                           		
                                                                                )
                                                           )
                                                )
                             ), 
							 
							  /*
                              array(//////////////////////////////MODULO/////////////////////////
							  'caption' => 'Reportes',
							  'id' => 'mnuReportes',
							  'img' => 'icons/packs/fugue/16x16/user-white.png',
							  'root' =>true,
							  'action' => null,
							  'ajax' => false,
							  'functions' => array('RAIZ_REPORTES'),
							  'childrens' => array(
													 array(
														   'img' => 'icons/packs/fugue/16x16/application-form.png',
														   'caption' => 'Iva Ventas',
														   'id' => 'mnuIvaVentas',
														   'action' => array('form' =>  'CarpeDiem.App.frmIvaVentas',
																						 'controller' => '', 'action' => 'index'),
														   'ajax' => true,
														   'functions' => array('MENU_IVA_VENTAS')
														 ),
														 array(
														   'img' => 'icons/packs/fugue/16x16/application-form.png',
														   'caption' => 'Matriz de Necesidades',
														   'id' => 'mnuMatrizNecesidades',
														   'action' => array('form' =>  'CarpeDiem.App.frmMatrizNecesidades',
																			 'controller' => 'Reportes/MatrizNecesidades', 'action' => 'index'),
														   'ajax' => true,
														   'functions' => array('MENU_MATRIZNECESIDADES')
														 ),
														 
														 
														array(
														   'img' => 'icons/packs/fugue/16x16/application-form.png',
														   'caption' => 'Tablero Control',
														   'id' => 'mnuTableroControl',
														   'action' => array('form' =>  'CarpeDiem.App.frmTableroControl',
																			 'controller' => 'Reportes', 'action' => 'index'),
														   'ajax' => true,
														   'functions' => array('MENU_IVA_VENTAS')
														 ),
														 
														array(
														   'img' => 'icons/packs/fugue/16x16/application-form.png',
														   'caption' => 'Reporte Ventas - Capa',
														   'id' => 'mnuVentas',
														   'action' => array('form' =>  'CarpeDiem.App.frmVentas',
																			 'controller' => 'Reportes', 'action' => 'index'),
														   'ajax' => true,
														   'functions' => array('MENU_VENTAS_CAPA')
														 ),
								)
							  ),
							  */
							  
						  array(
							   'img' => 'icons/packs/fugue/16x16/application-form.png',
							   'caption' => '',//vacio =>Invisible permite menues que los usa el sistema -> ej Picker Ventas
							   'id' => 'mnuOrdenArmadoPickerGeneral',
							   'action' => array('form' =>  'CarpeDiem.App.frmOrdenArmado', 
												 'listarTipo' => EnumListarTipo::Picker, 
												 'controller' => 'OrdenArmado', /*'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::Producto) ),*/
												 'action' => 'index',
												 'context_menu'=> array( array('control' => 'gv', 'options' => array(
																										array('text' =>'Ver &amp;Stock','type' => 'text',
																												'action' =>'msVerStock_Click',/*MODIFICAR DESPUES DEL DEPLOY POR MENU_STOCK */
																												'functions' => array('MENU_STOCK_RAPIDO') ),/*esto es para q todavia valmec no tenga disponible esta opcion*/
																										// array('text' =>'Ver &amp;Stock','type' => 'text',
																												// 'action' =>'msVerAsientos_Click',
																												// 'functions' => array('MENU_STOCK') ),
																												)
																				)
																		)
							   
												),
							   'ajax' => true,
							   'functions' => null,
							   ),  
                             array(//////////////////////////////MODULO/////////////////////////
                              'caption' => 'Producci&oacute;n',
                              'id' => 'mnuGestionOperativa',
                              'img' => 'icons/packs/fugue/16x16/user-white.png',
                              'root' =>true,
                              'action' => null,
                              'ajax' => false,
                              'functions' => array('RAIZ_GESTION_OPERATIVA'),
                              'childrens' => array(  
                                                     array(
                                                            'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                            'caption' => 'Ordenes de Armado',
                                                            'id' => 'mnuOrdenArmado',
                                                            'action' => false,
                                                            'ajax' => true,
                                                            'functions' => array('MENU_ORDEN_ARMADO'),
                                                     		'childrens' => array( 
																		                                                   				
																		array(
                                                     						'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                     						'caption' => 'Listado de Orden de Armado',
                                                     						'id' => 'mnuOrdenArmadoListado',
                                                     						'action' => array('form' =>  'CarpeDiem.App.frmOrdenArmado',
                                                     								
                                                     								
																						'controller' => 'OrdenArmado', 'action' => 'index',
																						'context_menu'=> array( array('control' => 'gv', 'options' => array(
																						
																						array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
																								'action' =>'msVerComprobanteRelacionado_Click',
																								'functions' => array('CONSULTA_ORDEN_ARMADO') ),
																						
																						array('text' =>'ANULAR',    'type' => 'text',
																								'action' =>'btnAnular_Click',
																								'functions' => array('BAJA_ORDEN_ARMADO') ),
																						array('text' =>'Modificar',     'type' => 'text',
																								'action' =>'btnEditar_Click',
																								'functions' => array('ADD_ORDEN_ARMADO') ),
																								
																						array('text' =>'Ver Comp. &amp;Auditor&iacute;a','type' => 'text',
																										'action' =>'msVerComprobanteAuditoria_Click',
																										'functions' => array('AUDITORIA_ORDEN_ARMADO') ),
																				)
																				)
																				)),
																			'ajax' => true,
																			'functions' => array('CONSULTA_ORDEN_ARMADO')
                                                     						),
																	array(
																			'img' => 'icons/packs/fugue/16x16/application-form.png',
																			'caption' => 'Configuracion Tipos Orden Armado',
																			'id' => 'mnuDetalleOrderArmado',
																			'action' => array('form' =>  'CarpeDiem.App.frmDetalleTipoComprobante',
																					'form_param'=>
																			    array('id_tipo_comprobante' => array(EnumTipoComprobante::OrdenArmado) ),
																					'controller' => 'DetallesTipoComprobante', 'action' => 'index'),
																			'ajax' => true,
																			'functions' => array('CONSULTA_DETALLE_TIPO_COMPROBANTE'),
																		  ),
                                                     				array(
                                                     						'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                     						'caption' => 'Nueva Orden de Armado',
                                                     						'id' => 'mnuOrdenArmadoMD',
                                                     						'action' => array('form' =>  'CarpeDiem.App.frmOrdenArmadoMD',
                                                     								'controller' => 'OrdenArmado', 'action' => 'index'),
                                                     						'ajax' => true,
                                                     						'functions' => array('ADD_ORDEN_ARMADO')
																		  ),
                                                     				array(
                                                     						'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                     						'caption' => 'Art&iacute;culos en Ordenes de Armado',
                                                     						'id' => 'mnuOrdenArmadoItems',
                                                     						'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem',
                                                     								'form_param'=>array('id_sistema' => array(EnumSistema::STOCK),'id_tipo_comprobante' => array(EnumTipoComprobante::OrdenArmado)),
                                                     								'controller' => 'OrdenArmadoItems', 'action' => 'index'),
                                                     						'ajax' => true,
                                                     						'functions' => array('CONSULTA_ORDEN_ARMADO')
																		  )
																)
                                                     		),
                                                        
                              		// array(
                              				// 'img' => 'icons/packs/fugue/16x16/application-form.png',
                              				// 'caption' => 'Orden de Armado En Serie (Stock)',
                              				// 'id' => 'mnuOrdenArmadoMD',
                              				// 'action' => array('form' =>  'CarpeDiem.App.frmOrdenArmadoMD',
                              						// 'controller' => 'OrdenArmado', 'action' => 'index',
                              						// 'form_param'=>
                              						// array('id_tipo_comprobante' =>
                              								// array(EnumTipoComprobante::OrdenArmado),                              								
                              								// 'id_detalle_tipo_comprobante' =>
                              								// array(EnumDetalleTipoComprobante::OrdenArmadoEnSerieParaStock)
                              						// )
                              				// ),
                              				// 'ajax' => true,
                              				// 'functions' => array('MENU_ORDEN_ARMADO'),
                              		// ), 
												   array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Operaciones Internas',
                                                           'id' => 'mnuOperacionInternaListado',
                                                           'action' => false,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_OPERACION_INTERNA'),
                                                           'childrens' =>array(
																				 array(
																						   'img' => 'icons/packs/fugue/16x16/application-form.png',
																						   'caption' => 'Listado de Operaciones Internas',
																						   'id' => 'mnuOperacionInterna',
																						   'action' => array('form' =>  'CarpeDiem.App.frmArticulo',
																											 'controller' => 'OperacionesInterna','form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::OPERACIONINTERNA) ),
																											 'action' => 'index',
																						   ),
																						   'ajax' => true,
																						   'functions' => array('MENU_OPERACION_INTERNA')
																					),
														   
                                                                                 array(
                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                               'caption' => 'Nueva Operacion Interna',
                                                                               'id' => 'mnuOperacionInternaA',
                                                                               'action' => array('form' =>  'CarpeDiem.App.frmArticuloA',
                                                                                                 'controller' => 'OperacionesInterna','form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::OPERACIONINTERNA) ),
                                                                                                 'action' => 'index'),
                                                                               'ajax' => true,
                                                                               'functions' => array('ADD_OPERACION_INTERNA'),
                                                                             )
                                                           )
                                                         ),
									
												 array(
													   'img' => 'icons/packs/fugue/16x16/application-form.png',
													   'caption' => 'Orden de Trabajo',
													   'id' => 'mnuOrdenTrabajoListado',
													   'action' => null,
													   'ajax' => true,
													   'functions' => array('MENU_ORDEN_TRABAJO'),
														'childrens' => array(
																
																
																			array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Listado de Orden Produccion (Vieja)',
								
																					   'id' => 'mnuOrdenProduccion',
																					   'action' => array('form' =>  'CarpeDiem.App.frmOrdenProduccion',
																										 'controller' => 'OrdenesProduccion', 'action' => 'index',
																										  'context_menu'=> array( array('control' => 'gv', 'options' => array(
																																								// array('text' =>'Ver &amp;Asientos','type' => 'text',
																																										// 'action' =>'msVerAsientos_Click',
																																										// 'functions' => array('MENU_ORDEN_TRABAJO') ),
																																								array('text' =>'ANULAR',    'type' => 'text',
																																									  'action' =>'btnAnular_Click',
																																									  'functions' => array('BAJA_ORDEN_TRABAJO') ),
																																								array('text' =>'Modificar',     'type' => 'text',
																																									  'action' =>'btnEditar_Click',
																																									  'functions' => array('ADDNN_ORDEN_TRABAJO') ),
																																								)
																																							) 
																																)
																																),
																					   'ajax' => true,
																					   'functions' => array('CONSULTA_ORDEN_TRABAJO'),
																				  ),
																			array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Listado de Ordenes de Trabajo',
																				   'id' => 'mnuOrdenTrabajo',
																				   'action' => array('form' =>  'CarpeDiem.App.frmOrdenTrabajo',
																									 'controller' => EnumController::OrdenesTrabajo, 'action' => 'index',
																									  'context_menu'=> array( array('control' => 'gv', 'options' => array(
																																							// array('text' =>'Ver &amp;Asientos','type' => 'text',
																																									// 'action' =>'msVerAsientos_Click',
																																									// 'functions' => array('MENU_ORDEN_TRABAJO') ),
																																							array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
																																								  'action' =>'msVerComprobanteRelacionado_Click',
																																								  'functions' => array('CONSULTA_ORDEN_TRABAJO') ), 
																																							array('text' =>'ANULAR',    'type' => 'text',
																																								  'action' =>'btnAnular_Click',
																																								  'functions' => array('BAJA_ORDEN_TRABAJO') ),
																																							array('text' =>'Modificar',     'type' => 'text',
																																								  'action' =>'btnEditar_Click',
																																								  'functions' => array('ADD_ORDEN_TRABAJO') ),
																																							)
																																						) 
																															)
																									),
																				   'ajax' => true,
																				   'functions' => array('CONSULTA_ORDEN_TRABAJO'),
																				  ),
																			array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Nueva Orden de Trabajo',
																				   'id' => 'mnuOrdenTrabajoMD',
																				   'action' => array('form' =>  'CarpeDiem.App.frmOrdenTrabajoMD',
																									 'controller' =>  EnumController::OrdenesTrabajo, 'action' => 'index'),
																				   'ajax' => true,
																				   'functions' => array('ADD_ORDEN_TRABAJO'),
																				),
																			array(
																					'img' => 'icons/packs/fugue/16x16/application-form.png',
																					'caption' => 'Listado de Etapas Productivas',
																					'id' => 'mnuEtapasProductivas',
																					'action' => array('form' =>  'CarpeDiem.App.frmDetalleTipoComprobante',
																							'form_param'=>
																							array('id_tipo_comprobante' =>
																									array(EnumController::OrdenesTrabajo) ),
																							'controller' => 'DetallesTipoComprobante', 'action' => 'index'),
																					'ajax' => true,
																					'functions' => array('CONSULTA_DETALLE_TIPO_COMPROBANTE'),
																				  ),
																
																
																			)
													 ), 
				                              		array(
				                              				'img' => 'icons/packs/fugue/16x16/application-form.png',
				                              				'caption' => 'Mantenimiento',
				                              				'id' => 'mnuMantenimiento',
				                              				'action' => array('form' =>  'CarpeDiem.App.frmMantenimiento',
				                              						'controller' => EnumController::Mantenimientos, 'action' => 'index'),
				                              				'ajax' => true,
				                              				'functions' => array('MENU_MANTENIMIENTO'),
				                              				'childrens' => array(
				                              						array(
				                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
				                              								'caption' => 'Listado de Mantenimiento',
				                              								'id' => 'mnuMantenimientoL',
				                              								'action' => array('form' =>  'CarpeDiem.App.frmMantenimiento',
				                              										'controller' => EnumController::Mantenimientos, 'action' => 'index'),
				                              								'ajax' => true,
				                              								'functions' => array('MENU_MANTENIMIENTO'),
				                              						),
				                              						array(
				                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
				                              								'caption' => 'Nuevo Mantenimiento',
				                              								'id' => 'mnuMantenimientoMD',
				                              								'action' => array('form' =>  'CarpeDiem.App.frmMantenimientoMD',
				                              										'controller' => EnumController::Mantenimientos, 'action' => 'index'),
				                              								'ajax' => true,
				                              								'functions' => array('ADD_MANTENIMIENTO'),
				                              						),
																	
																	array(
																			'img' => 'icons/packs/fugue/16x16/application-form.png',
																			'caption' => 'Nueva Calibracion',
																			'id' => 'mnuMantenimientoMDCalibracion',
																			'action' => array('form' =>  'CarpeDiem.App.frmMantenimientoMD',
																					'controller' => EnumController::Mantenimientos, 
																					 'form_param'=>
																						 array('id_detalle_tipo_comprobante' =>
																							   array(EnumDetalleTipoComprobante::Calibracion) ),
																					'action' => 'index'),
																			'ajax' => true,
																			'functions' => array('ADD_MANTENIMIENTO'),
				                              						),
																	
																	
				                              						array(
				                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
				                              								'caption' => 'Art&iacute;culos de Mantenimiento',
				                              								'id' => 'mnuMantenimientoItems',
				                              								'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItem',
				                              							    'form_param'=>array('id_sistema' => array(EnumSistema::PRODUCCION),'id_tipo_comprobante' => array(EnumTipoComprobante::Mantenimiento)),
				                              								'controller' => EnumController::MantenimientoItems, 'action' => 'index',
				                              								'form_title'=> 'Art&iacute;culos de Mantenimiento'),
				                              								'ajax' => true,
				                              								'functions' => array('CONSULTA_MANTENIMIENTO')
				                              						)
															)
				                              		), 
													array(
														   'img' => 'icons/packs/fugue/16x16/application-form.png',
														   'caption' => '',//vacio =>Invisible permite menues que los usa el sistema -> ej Picker Ventas
														   'id' => 'mnuLotePicker',
														   'action' => array('form' =>  'CarpeDiem.App.frmLote',
																			 'listarTipo' => EnumListarTipo::Picker, 
																			 'controller' => 'Lotes',/*'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::Producto) ),*/
																			 'action' => 'index',
																			 'context_menu'=> array( array('control' => 'gv',// 'options' => //array(
																																	//array('text' =>'Ver &amp;Stock','type' => 'text',
																																			//'action' =>'msVerStock_Click',/*MODIFICAR DESPUES DEL DEPLOY POR MENU_STOCK */
																																			//'functions' => array('MENU_STOCK_RAPIDO') ),/*esto es para q todavia valmec no tenga disponible esta opcion*/
																																	// array('text' =>'Ver &amp;Stock','type' => 'text',
																																			// 'action' =>'msVerAsientos_Click',
																																			// 'functions' => array('MENU_STOCK') ),
																																			//)
																											)
																									)
														   
																			),
														   'ajax' => true,
														   'functions' => null,
												   ),
													
                                                    array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Lotes',
                                                           'id' => 'mnuLote',
                                                           'action' => false,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_LOTE'),
                                                    		'childrens' => array(
                                                    				
                                                    				
                                                    				array(
                                                    						'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                    						'caption' => 'Listado de Lotes',
                                                    						'id' => 'mnuLote',
                                                    						'action' => array('form' =>  'CarpeDiem.App.frmLote',
                                                    								'controller' => 'Lotes', 'action' => 'index'),
                                                    						'ajax' => true,
                                                    						'functions' => array('MENU_LOTE')
                                                    						
                                                    						),
                                                    				array(
                                                    						'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                    						'caption' => 'Lotes en Comprobantes',
                                                    						'id' => 'mnuDeposito',
                                                    						'action' => array('form' =>  'CarpeDiem.App.frmComprobanteItemLote','controller' => EnumController::ComprobanteItemLotes, 'action' => 'index'),
                                                    						'ajax' => true,
                                                    						
                                                    						'functions' => array('MENU_LOTE')
                                                    						
                                                    				)
                                                    		)
                                                         ),
														 
														 array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Materiales Costos (MP. Base)',
                                                           'id' => 'mnuCosto',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmArticulo',
                                                                             'controller' => EnumController::MateriasPrimas,
																			 'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::MateriaPrima),
																								  'id_producto_clasificacion' => array(EnumProductoClasificacion::Articulo_BASE) ),
																			 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('CONSULTA_COSTO'),
                                                         ),
														array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Alta Materiales Costos (MP. Base)',
                                                           'id' => 'mnuCostoA',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmArticuloA',
                                                                             'controller' => EnumController::MateriasPrimas,
																			 'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::MateriaPrima),
																								  'id_producto_clasificacion' => array(EnumProductoClasificacion::Articulo_BASE) ),
																			 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('ADD_COSTO'),
                                                         ),

                                                       array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Equipos',
                                                           'id' => 'mnuEquipo',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmEquipo',
																			 'controller' => 'Equipos', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_EQUIPOS'),
                                                         ),
                                                         
                                                         array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Reportes',
                                                           'id' => 'mnuGoperativaReporte',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_GESTION_OPERATIVA_REPORTE'),
                                                           'childrens' => array(
                                                                                
                                                                                /*array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Necesidad de Mecanizado',
                                                                                   'id' => 'mnuNecesidadMecanizado',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmNecesidadMecanizado',
                                                                                                     'controller' => 'Reportes', 'action' => 'NecesidadDeMecanizado'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('REPORTE_NECESIDAD_MECANIZADO'),
                                                                                   
                                                                                   ),*/
																				//1181
																				array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Listado de relaciones de Art&iacute;culos.',
																					   'id' => 'mnuProductoPorComponente',
																					   'action' => array('form' =>  'CarpeDiem.App.frmArticuloRelacion',
																										 'controller' => 'ArticulosRelacion', 'action' => 'index'),
																					   'ajax' => true,
																					   'functions' => array('CONSULTA_PRODUCTO'),
																					 ), 
                                                           		
                                                           		array(
                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           				'caption' => 'Reporte de Pedidos con Orden Armado',
                                                           				'id' => 'mnuReprotePedidosOrdenArmado',
                                                           				'action' => array('form' =>  'CarpeDiem.App.frmReporteListado',
                                                           						'form_param'=>
                                                           						array(
                                                           								'id_sistema' =>
                                                           								array(EnumSistema::PRODUCCION),
                                                           								'form_title' =>
                                                           								"Listado de Pedidos Con Orden de Armado",
                                                           								'form_view' =>
                                                           								"reporte_pedido_orden_armado",
                                                           								'id_tipo_comprobante' =>
                                                           								array(EnumTipoComprobante::PedidoInterno),
                                                           						),
                                                           						'controller' => 'Reportes', 'action' => 'PedidosConOrdenEnsamble'),
                                                           				'ajax' => true,
                                                           				'functions' => array('CONSULTA_ORDEN_ARMADO'),
                                                           		),
                                                                               
																	   )
                                                       ),
                                                )
                             ),
						
						array(//////////////////////////////modulo/////////////////////////
                               'caption' => 'RRHH',
                               'id' => 'mnuRRHH',
                               'img' => 'icons/packs/fugue/16x16/user-white.png',
                               'root' =>true,
                               'action' => null,


                               'ajax' => false,
                               'functions' => array('RAIZ_RRHH'),
                               'childrens' => array(
													array(
														   'img' => 'icons/packs/fugue/16x16/application-form.png',
														   'caption' => '',//vacio =>Invisible permite menues que los usa el sistema -> ej Picker Ventas
														   'id' => 'mnuEmpleadoGeneral',
														   'action' => array('form' =>  'CarpeDiem.App.frmEmpleado',
																			 'listarTipo' => EnumListarTipo::Picker, 
																			 'controller' => 'Empleados',/*'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::Producto) ),*/
																			 'action' => 'index'
																			),
														   'ajax' => true,
															'functions' => array('CONSULTA_EMPLEADO'),
														   ),
							   
													 array(
														    'img' => 'icons/packs/fugue/16x16/application-form.png',
														    'caption' => 'Empleados',
														    'id' => 'mnuEmpleado',
															 'action' => null,
														    'ajax' => true,
														    'functions' => array('MENU_EMPLEADO'),
													 		'childrens' =>array(
																				array(
																						'img' => 'icons/packs/fugue/16x16/application-form.png',
																						'caption' => 'Categorias de Empleados',
																						'id' => 'mnuEmpleadosCategoria',
																						'action' => array(  'form' =>  'CarpeDiem.App.frmPersonaCategoria',
																											'controller' => 'EmpleadosCategorias', 'action' => 'index',
																											'form_param'=>    array('id_tipo_persona' =>
																													array(EnumTipoPersona::Empleado)) 
																						),
																						'ajax' => true,
																						'functions' => array('ADD_EMPLEADO'),
																				),
																				array(
																						'img' => 'icons/packs/fugue/16x16/application-form.png',
																						'caption' => 'Administraci&oacute;n de Empleados',
																						'id' => 'mnuEmpleadoL',
																						'action' => array('form' =>  'CarpeDiem.App.frmEmpleado',				
																										  'controller' => EnumController::Empleados, 'action' => 'index'),
																						'ajax' => true,
																						'functions' => array('MENU_EMPLEADO'),
																				),
																				array(
																						'img' => 'icons/packs/fugue/16x16/application-form.png',
																						'caption' => 'Nuevo Empleado',
																						'id' => 'mnuEmpleadoA',
																						'action' => array('form' =>  'CarpeDiem.App.frmEmpleadoA',
																									      'controller' => EnumController::Empleados, 'action' => 'index'),
																						'ajax' => true,
																						'functions' => array('ADD_EMPLEADO')
																				),
																		)
															 ),
													 ),
								 ),
                        array(//////////////////////////////MODULO/////////////////////////
                              'caption' => 'Parametricas',
                              'id' => 'mnuParametricas',
                              'img' => 'icons/packs/fugue/16x16/user-white.png',
                              'root' =>true,
                              'action' => null,
                              'ajax' => false,
                              'functions' => array('MENU_CONFIGURACION'),
                              'childrens' => array(
                                                    array(
                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               'caption' => 'Tipos Comprobante',
                                                               'id' => 'mnuTiposComprobantes',
                                                                'action' => array('form' =>  'CarpeDiem.App.frmTipoComprobante',
																			 'controller' => 'TiposComprobante', 'action' => 'index'),
                                                               'ajax' => true,
                                                               'functions' => array('MENU_TIPO_COMPROBANTE'),
                                                               'childrens' => array()
                                                        ),
													
													array(
                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               'caption' => 'Estado',
                                                               'id' => 'mnuEstado',
                                                                'action' => array('form' =>  'CarpeDiem.App.frmEstado',
																			 'controller' => 'Estados', 'action' => 'index'),
                                                               'ajax' => true,
                                                               'functions' => array('MENU_ESTADO'),
                                                               'childrens' => array()
                                                        ),
                                                    array(
                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               'caption' => 'Iva',
                                                               'id' => 'mnuIva',
                                                                'action' => array('form' =>  'CarpeDiem.App.frmIva',
																			 'controller' => 'Ivas', 'action' => 'index'),
                                                               'ajax' => true,
                                                               'functions' => array('MENU_IVA'),
                                                               'childrens' => array()
                                                        ),
													array(
                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               'caption' => '',//invisible
                                                               'id' => 'mnuIvaA',
                                                                'action' => array('form' =>  'CarpeDiem.App.frmIvaA',
																			 'controller' => 'Ivas', 'action' => 'index'),
                                                               'ajax' => true,
                                                               'functions' => array('MODIFICACION_IVA'),
                                                               'childrens' => array()
                                                        ),
													array(
                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               'caption' => 'Tipo Iva',
                                                               'id' => 'mnuTipoIva',
                                                                'action' => array('form' =>  'CarpeDiem.App.frmTipoIva',
																			 'controller' => 'TipoIva', 'action' => 'index'),
                                                               'ajax' => true,
                                                               'functions' => array('MENU_TIPO_IVA'),
                                                               'childrens' => array()
                                                        ),
													array(
                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               'caption' => '',//invisible
                                                               'id' => 'mnuTipoIvaA',
                                                                'action' => array('form' =>  'CarpeDiem.App.frmTipoIvaA',
																			 'controller' => 'TipoIva', 'action' => 'index'),
                                                               'ajax' => true,
                                                               'functions' => array('MODIFICACION_TIPO_IVA'),
                                                               'childrens' => array()
                                                        ),
														
													array(
														   'img' => 'icons/packs/fugue/16x16/application-form.png',
														   'caption' => 'Area',
														   'id' => 'mnuArea',
															'action' => array('form' =>  'CarpeDiem.App.frmArea',
																			 'controller' => 'Areas', 'action' => 'index'),
														   'ajax' => true,
														   'functions' => array('MENU_AREA'),
														   'childrens' => array()
													),
													
													array(
														   'img' => 'icons/packs/fugue/16x16/application-form.png',
														   'caption' => 'Nueva Area',
														   'id' => 'mnuAreaA',
															'action' => array('form' =>  'CarpeDiem.App.frmArea',
																			 'controller' => 'Areas', 'action' => 'index'),
														   'ajax' => true,
														   'functions' => array('ADD_AREA'),
														   'childrens' => array()
													),

                                                    array(
                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               'caption' => 'Categorias',
                                                               'id' => 'mnuCategoria',
                                                               'action' => false,
                                                               'ajax' => true,
                                                               'functions' => array('MENU_CATEGORIA'),
                                                               'childrens' => array(
                                                                           
                                                                                   
                                                                                    
                                                                                   
                                                                                    array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Categorias de Transporte',
                                                                                       'id' => 'mnuTransporteCategoria',
                                                                                        'action' => array('form' =>  'CarpeDiem.App.frmPersonaCategoria',
                                                                                                     'controller' => 'TransportistasCategorias', 'action' => 'index',
                                                                                        		'form_param'=>    array('id_tipo_persona' =>
                                                                                        				array(EnumTipoPersona::Transportista)) ),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('MENU_TRASPORTE_CATEGORIA'),
                                                                                       'childrens' => array()
                                                                                ), 
                                                               
                                                               )
                                                        ),
                                                      

													array(
                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               'caption' => 'Formas de Pago',
                                                               'id' => 'mnuFormasPago',
                                                                'action' => array('form' =>  'CarpeDiem.App.frmFormasPago',
																			 'controller' => 'FormasPago', 'action' => 'index'),
                                                               'ajax' => true,
                                                               'functions' => array('MENU_FORMA_PAGO'),
                                                               'childrens' => array()
                                                        ),
					                              		array(
					                              				'img' => 'icons/packs/fugue/16x16/application-form.png',
					                              				'caption' => 'Condici&oacute;n de Pago',
					                              				'id' => 'mnuCondicionesPago',
					                              				'action' => array('form' =>  'CarpeDiem.App.frmCondicionPago',
					                              						'controller' => 'CondicionesPago', 'action' => 'index'),
					                              				'ajax' => true,
					                              				'functions' => array('MENU_CONDICION_PAGO'),
					                              				'childrens' => array()
					                              		),
                                                   array(
                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               'caption' => 'Condiciones Frente al IVA',
                                                               'id' => 'mnuTiposIva',
                                                                'action' => array('form' =>  'CarpeDiem.App.frmTipoIva',
																			 'controller' => 'TiposIva', 'action' => 'index'),
                                                               'ajax' => true,
                                                               'functions' => array('MENU_TIPO_IVA'),
                                                               'childrens' => array()
                                                        ),         
                                                        	
													array(
                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               'caption' => 'Administrador de Empresas / Sucursales',
                                                               'id' => 'mnuDatosEmpresa',
                                                                'action' => null,
                                                               'ajax' => true,
                                                               'functions' => array('MENU_DATOSEMPRESA'),
                                                               'childrens' => array(
																					
																					
																			array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Administraci&oacute;n de Sucursales',
																				   'id' => 'mnuDatosEmpresa',
																					'action' => array('form' =>  'CarpeDiem.App.frmDatosEmpresa',
																								'controller' => 'DatosEmpresa', 'action' => 'index'),
																				   'ajax' => true,
																				   'functions' => array('ADD_DATOSEMPRESA'),
																				   'childrens' => array()
																				   ),
                                                               
																				   array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Numeraci&oacute;n de Comprobante',
																				   'id' => 'mnuComprobantePuntoVentaNumero',
																					'action' => array('form' =>  'CarpeDiem.App.frmComprobantePuntoVentaNumero',
																								 'controller' => 'ComprobantesPuntoVentaNumero', 'action' => 'index'),
																				   'ajax' => true,
																				   'functions' => array('MENU_COMPROBANTE_PUNTO_VENTA_NUMERO'),
																				   'childrens' => array()
																				   ),
                                                               
                                                               
                                                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Puntos de Venta',
                                                                                   'id' => 'mnuPuntoDeVenta',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmPuntoVenta',
                                                                                                     'form_param'=>    array('id_sistema' =>
                                                                                                                            array(EnumSistema::VENTAS) ),
                                                                                                     'controller' => EnumController::PuntosVenta, 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_PUNTO_VENTA'),
                                                                                   ),
																				 array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Contadores',
                                                                                   'id' => 'mnuContador',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmContador',
                                                                                                  
                                                                                                     'controller' => EnumController::Contadores, 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_DATOSEMPRESA'),
                                                                                   ),   
																				array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Modulos',
                                                                                   'id' => 'mnuModulo',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmModulo',
                                                                                                  
                                                                                                     'controller' => EnumController::Modulos, 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_MODULO'),
                                                                                   ),
                                                               )
                                                        ),
                                                        
														array(
                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               'caption' => 'Administracion',
                                                               'id' => 'mnuAdministracion',
                                                                'action' => array('form' =>  'CarpeDiem.App.frmAdministracion',
																			 'controller' => '', 'action' => 'index'),
                                                               'ajax' => true,
                                                               'functions' => array('MENU_ADMINISTRACION'),
                                                               'childrens' => array()
                                                        ),
                                                        array(//////////////////////////////MODULO/////////////////////////
                                                                  'caption' => 'Georeferencia',
                                                                  'id' => 'mnuGeoreferencia',
                                                                  'img' => 'icons/packs/fugue/16x16/user-white.png',
                                                                  'root' =>true,
                                                                  'action' => null,
                                                                  'ajax' => false,
                                                                  'functions' => array('RAIZ_GEOGRAFICO'),
                                                                  'childrens' => array(
                                                                                         array(
                                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                               'caption' => 'Pais',
                                                                                               'id' => 'mnuPais',
                                                                                               'action' => array('form' =>  'CarpeDiem.App.frmPais',
                                                                                                                 'controller' => 'Paises', 'action' => 'index'),
                                                                                               'ajax' => true,
                                                                                               'functions' => array('MENU_PAIS'),
                                                                                             ),
                                                                                        array(
                                                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                               'caption' => 'Provincias',
                                                                                               'id' => 'mnuProvincia',
                                                                                               'action' => array('form' =>  'CarpeDiem.App.frmProvincia',
                                                                                                                 'controller' => 'Provincias', 'action' => 'index'),
                                                                                               'ajax' => true,
                                                                                               'functions' => array('MENU_PROVINCIA'),
                                                                                             ),
                                                                                    )
                                                                 )
                                                        
                                                        														
                          
                                                       
													),
                          ),
                          
                          array(//////////////////////////////MODULO/////////////////////////
                              'caption' => 'Contabilidad',
                              'id' => 'mnuContabilidad',
                              'img' => 'icons/packs/fugue/16x16/user-white.png',
                              'root' =>true,
                              'action' => null,
                              'ajax' => false,
                              'functions' => array('MENU_CONTABILIDAD'),
                              'childrens' => array(
                              
                                              array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Asientos',
                                                           'id' => 'mnuAsiento',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_ASIENTO'),
                                                           'childrens' => array(
                                                                                array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => 'Nuevo Asiento Manual',
                                                                                           'id' => 'mnuAsientoMD',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmAsientoMD',
                                                                                                         'controller' => 'Asientos', 'action' => 'add'),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('ADD_ASIENTO'),
                                                                                           //'childrens' => array()
                                                                                    ),
																				array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => 'Libro Diario',
                                                                                           'id' => 'mnuLibroDiario',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmAsiento',
                                                                                                         'controller' => EnumController::AsientosCuentaContable, 'action' => 'index'),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('CONSULTA_ASIENTO'),
                                                                                           //'childrens' => array()
                                                                                    ),
                                                                                array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => 'Mayor de Cuentas',
                                                                                           'id' => 'mnuMayorCuenta',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmMayorCuentas',
                                                                                                         'controller' => EnumController::AsientosCuentaContable, 'action' => 'MayorDeCuentas'),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('CONSULTA_ASIENTO'),
                                                                                           //'childrens' => array()
                                                                                    ), 
                                                                                    
                                                                                array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => 'Balance General',
                                                                                           'id' => 'mnuBalanceGeneral',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmBalanceGeneral',
                                                                                                         'controller' => 'Reportes', 'action' => 'BalanceGeneral'),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('CONSULTA_ASIENTO'),
                                                                                           //'childrens' => array()
                                                                                    ),
                                                                                    
                                                                                array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => 'Balance Sumas Y Saldos',
                                                                                           'id' => 'mnuBalanceSumaYSaldo',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmBalanceSumasYSaldos',
                                                                                                         'controller' => 'Reportes', 'action' => 'BalanceSumasYSaldos'),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('CONSULTA_BALANCE_SUMAS_SALDOS'),
                                                                                           //'childrens' => array()
                                                                                    ),   
                                                                                    
                                                                                    
                                                                                array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => 'Asientos por Comprobante',
                                                                                           'id' => 'mnuAsientoComprobante',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmAsientoComprobante',
                                                                                                         'controller' => EnumController::AsientosCuentaContable, 'action' => 'AsientoComprobante'),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('CONSULTA_ASIENTO'),
                                                                                           //'childrens' => array()
                                                                                    ),
																				array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => 'Asientos Modelo',
                                                                                           'id' => 'mnuAsientoModelo',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmAsientoModelo',
                                                                                                         'controller' => 'AsientosModelo', 'action' => 'index'),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('CONSULTA_ASIENTO'),
                                                                                           //'childrens' => array()
                                                                                    ),
                                                                                array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => 'Tipos de Asiento',
                                                                                           'id' => 'mnuTipoAsiento',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmTipoAsiento',
                                                                                                         'controller' => 'TiposAsiento', 'action' => 'index'),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('MENU_TIPO_ASIENTO'),
                                                                                           //'childrens' => array()
                                                                                    ),
																				array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => 'Leyendas por Tipo de Asiento',
                                                                                           'id' => 'mnuLeyendaTipoAsientoLeyenda',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmTipoAsientoLeyenda',
                                                                                                         'controller' => 'TiposAsientoLeyenda', 'action' => 'index'),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('MENU_TIPO_ASIENTO'),
                                                                                           'childrens' => array()
                                                                                    ),
																					
																				array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => 'Agrupacion de Asientos (Tipos Asientos)',
                                                                                           'id' => 'mnuGrupoAsiento',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmAgrupacionAsiento',
                                                                                                         'controller' => 'AgrupacionesAsiento', 'action' => 'index'),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('MENU_AGRUPACION_ASIENTO'),
                                                                                           'childrens' => array()
                                                                                    ),
                                                                                    
                                                                             /*   array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => 'Efectos de Asiento',
                                                                                           'id' => 'mnuAsientoEfecto',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmTipoAsiento',
																										 'listarTipo' => EnumListarTipo::ABM,
                                                                                                         'controller' => 'AsientosEfecto', 'action' => 'index'),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('MENU_ASIENTO_EFECTO'),
                                                                                           //'childrens' => array()
                                                                                    ),
																					*/
																				array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => '', //'Picker de Cuentas Contables',
                                                                                           'id' => 'mnuCuentaContablePicker',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmCuentaContable',
																											  'listarTipo' => EnumListarTipo::Picker,
                                                                                                              'controller' => 'CuentasContable', 
																											  'action' => 'index'),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('MENU_CUENTA_CONTABLE'),
                                                                                           //'childrens' => array()
                                                                                    ),
																				array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => 'Cuentas Contables',
                                                                                           'id' => 'mnuCuentaContable',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmCuentaContable',
																											 'listarTipo' => EnumListarTipo::ABM,
																											 'controller' => 'CuentasContable', 
																											 'action' => 'index'),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('MENU_CUENTA_CONTABLE'),
                                                                                           //'childrens' => array()
                                                                                    ),
                                                           		/*
                                                           		array(
                                                           				'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           				'caption' => 'Modulo y Funciones',
                                                           				'id' => 'mnuCuentaContable2',
                                                           				'action' => array('form' =>  'CarpeDiem.App.frmModuloFuncionEntidad',
                                                           						'controller' => 'ModulosFuncionEntidad', 'action' => 'index'),
                                                           				'ajax' => true,
                                                           				'functions' => array('MENU_CUENTA_CONTABLE'),
                                                           				//'childrens' => array()
                                                           		),*/
																				array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => 'Planes de Cuentas Contables',
                                                                                           'id' => 'mnuPlanDeCuenta',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmPlanDeCuenta',
                                                                                                         'controller' => 'PlanesDeCuenta', 'action' => 'index'),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('MENU_PLAN_DE_CUENTA'),
                                                                                           //'childrens' => array()
                                                                                    ),

																				)

                                                       
                                                    ),
                                                    
                                                    array(
                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               'caption' => 'Centros de Costo',
                                                               'id' => 'mnuCentroCosto',
                                                                'action' => false,
                                                               'ajax' => true,
                                                               'functions' => array('MENU_CENTRO_COSTO'),
                                                               'childrens' => array( 
															   
															   
																				   array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Administraci&oacute de Centros de Costo',
																				   'id' => 'mnuCentroCosto',
																					'action' => array('form' =>  'CarpeDiem.App.frmCentroCosto',
																								 'controller' => 'CentrosCosto', 'action' => 'index'),
																				   'ajax' => true,
																				   'functions' => array('MENU_CENTRO_COSTO'),
																				   'childrens' => array()
																					),
																					array(
                                                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                           'caption' => 'Planilla de centros de costo',
                                                                                           'id' => 'mnuReporteCentroCostoPorCuentaContable',
                                                                                            'action' => array('form' =>  'CarpeDiem.App.frmReporteCentroCostoPorCuentaContable',
                                                                                                         'controller' => 'Reportes', 'action' => 'ReporteCentroCostoPorCuentaContable'),
                                                                                           'ajax' => true,
                                                                                           'functions' => array('MENU_CENTRO_COSTO'),
                                                                                           //'childrens' => array()
                                                                                    )
															   
															   
															   
															   )
                                                        ),
                                                        
                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Ejercicios Contables',
                                                                                   'id' => 'mnuEjercicio',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmEjercicio',
                                                                             'controller' => 'EjerciciosContable', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_EJERCICIO_CONTABLE'),
                                                                                 ),         
                                                        
                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Peri&oacute;dos Contables',
                                                                                   'id' => 'mnuPeriodoContable',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmPeriodoContable',
                                                                             'controller' => 'PeriodosContable', 'action' => 'index'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_PERIODO_CONTABLE'),
                                                                                 ), 
                                                    
                                                    
                                                    )
                          ),
                        
                          
                          array(//////////////////////////////MODULO/////////////////////////
                              'caption' => 'Tesorer&iacute;a',
                              'id' => 'mnuTesoreria',
                              'img' => 'icons/packs/fugue/16x16/user-white.png',
                              'root' =>true,
                              'action' => null,
                              'ajax' => false,
                              'functions' => array('MENU_TESORERIA'),
                              'childrens' => array(
                              
                              
                                                    array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Movimientos Bancario',
                                                           'id' => 'mnuMovimientosBancarios',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_MOVIMIENTO_BANCARIO'),
                                                           'childrens' => array(
                              
                                                    array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Listado de Movimientos Bancarios',
                                                                                       'id' => 'mnuMovimientosbancarios',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmMovimientoBancario',
                                                                                                         'controller' => 'MovimientosBancarios', 'action' => 'index',
																										  'context_menu'=> array( array('control' => 'gv', 'options' => array(
									   
																														  array('text' =>'Ver &amp;Asientos','type' => 'text',
																																	'action' =>'msVerAsientos_Click',
																																	'functions' => array('MENU_ASIENTO') ),
																																	
																															array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
																																  'action' =>'msVerComprobanteRelacionado_Click',
																																  'functions' => array('CONSULTA_MOVIMIENTO_BANCARIO') ), 
																															
																																  
																															array('text' =>'ANULAR',    'type' => 'text',
																																  'action' =>'btnAnular_Click',
																																  'functions' => array('BAJA_MOVIMIENTO_BANCARIO') ),
																															array('text' =>'Modificar',     'type' => 'text',
																																  'action' =>'btnEditar_Click',
																																  'functions' => array('ADD_MOVIMIENTO_BANCARIO') ),
																															)
																																				   ) 
																																)
																										 
																										 ),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('CONSULTA_MOVIMIENTO_BANCARIO'),
                                                    ),
													array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Nuevo Movimiento Bancario',
                                                           'id' => 'mnuMovimientosBancariosMD',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_MOVIMIENTO_BANCARIO'),
                                                           'childrens' => array(
																				
																					 array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Caja - Banco',
																					   'id' => 'mnuMovimientosbancarios_CB',
																					   'action' => array('form' => 'CarpeDiem.App.frmMovimientoBancarioMD','form_param'=>EnumDetalleTipoComprobante::CajaBanco,
																										 'controller' => 'MovimientosBancarios', 'action' => 'index'),
																					   'ajax' => true,
																					   'functions' => array('ADD_MOVIMIENTO_BANCARIO'),
																					 ),
                                                                                      array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Banco - Caja',
                                                                                       'id' => 'mnuMovimientosbancarios_BC',
                                                                                       'action' => array('form' => 'CarpeDiem.App.frmMovimientoBancarioMD','form_param'=>EnumDetalleTipoComprobante::BancoCaja,
                                                                                                         'controller' => 'MovimientosBancarios', 'action' => 'index'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('ADD_MOVIMIENTO_BANCARIO'),
                                                                                     ),
                                                                                     array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Movimiento entre bancos',
                                                                                       'id' => 'mnuMovimientosbancarios_BB',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmMovimientoBancarioMD','form_param'=>EnumDetalleTipoComprobante::BancoBanco,
                                                                                                         'controller' => 'MovimientosBancarios', 'action' => 'index'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('ADD_MOVIMIENTO_BANCARIO'),
                                                                                     ),
                                                                                     array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Cobro de Cheque',
                                                                                       'id' => 'mnuMovimientosbancarios_CC',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmMovimientoBancarioMD','form_param'=>EnumDetalleTipoComprobante::CobroCheque,
                                                                                                         'controller' => 'MovimientosBancarios', 'action' => 'index'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('ADD_MOVIMIENTO_BANCARIO'),
                                                                                     )
																				)
														),
                                                         
                                                        
                                                        
                                                    )
                                                    
                                                    ),
                                                    array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Ingreso/Egreso Caja',
                                                           'id' => 'mnuMovimientoCaja',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_MOVIMIENTO_CAJA'),
                                                           'childrens' => array(
																				array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Listado de Movimientos de Caja',
																					   'id' => 'mnuMovimientoCajaListado',
																					   'action' => array('form' =>  'CarpeDiem.App.frmMovimientoCaja',
																										 'controller' => 'MovimientosCaja', 'action' => 'index',
																										  'context_menu'=> array( array('control' => 'gv', 'options' => array(
									   
																														  array('text' =>'Ver &amp;Asientos','type' => 'text',
																																	'action' =>'msVerAsientos_Click',
																																	'functions' => array('MENU_ASIENTO') ),
																																	
																															array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
																																  'action' =>'msVerComprobanteRelacionado_Click',
																																  'functions' => array('CONSULTA_MOVIMIENTO_CAJA') ), 
																															
																																  
																															array('text' =>'ANULAR',    'type' => 'text',
																																  'action' =>'btnAnular_Click',
																																  'functions' => array('BAJA_MOVIMIENTO_CAJA') ),
																															array('text' =>'Modificar',     'type' => 'text',
																																  'action' =>'btnEditar_Click',
																																  'functions' => array('ADD_MOVIMIENTO_CAJA') ),
																															)
																																				   ) 
																																)),
																					   'ajax' => true,
																					   'functions' => array('CONSULTA_MOVIMIENTO_CAJA'),
																				),
																				array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Nuevo Ingreso',
																					   'id' => 'mnuMovimientoCajaMD',
																					   'action' => array('form' =>  'CarpeDiem.App.frmMovimientoCajaMD',
																										 'form_param'=> 
																														array('id_tipo_comprobante' =>
																																		array(EnumTipoComprobante::IngresoCaja)) ,																							   
																										 'controller' => 'MovimientosCaja', 'action' => 'index'),
																					   'ajax' => true,
																					   'functions' => array('ADD_MOVIMIENTO_CAJA'),
																				),
																				array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Nuevo Egreso',
																					   'id' => 'mnuMovimientoCaja_Egreso',
																					   'action' => array('form' =>  'CarpeDiem.App.frmMovimientoCajaMD',
																										 'form_param'=> 
																														array('id_tipo_comprobante' =>
																																		array(EnumTipoComprobante::EgresoCaja)) ,
																										 'controller' => 'MovimientosCaja', 'action' => 'index'),
																					   'ajax' => true,
																					   'functions' => array('ADD_MOVIMIENTO_CAJA'),
																				),
																				array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Listado de Conceptos de Ingreso',
																					   'id' => 'mnuMovimientoCajaListadoConceptoIngreso',
																					   'action' => array('form' =>  'CarpeDiem.App.frmDetalleTipoComprobante', 
																										 'form_param'=> 
																														array('id_tipo_comprobante' =>
																																			array(EnumTipoComprobante::IngresoCaja) ),
																										 'controller' => 'DetallesTipoComprobante', 'action' => 'index'),
																					   'ajax' => true,
																					   'functions' => array('CONSULTA_DETALLE_TIPO_COMPROBANTE'),
																				),
																				array(
																					   'img' => 'icons/packs/fugue/16x16/application-form.png',
																					   'caption' => 'Listado de Conceptos de Egreso',
																					   'id' => 'mnuMovimientoCajaListadoConceptoEgreso',
																					   'action' => array('form' =>  'CarpeDiem.App.frmDetalleTipoComprobante', 
																										 'form_param'=> 
																														array('id_tipo_comprobante' =>
																																			array(EnumTipoComprobante::EgresoCaja) ),
																										 'controller' => 'DetallesTipoComprobante', 'action' => 'index'),
																					   'ajax' => true,
																					   'functions' => array('CONSULTA_DETALLE_TIPO_COMPROBANTE'),
																				)
																			)
                                                    ),
                              
                                                      array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Monedas',
                                                           'id' => 'mnuMoneda',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmMoneda',
																			 'controller' => 'Monedas', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_MONEDA'),
                                                         ),
														 
													  array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => '',//invisible
                                                           'id' => 'mnuMonedaA',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmMonedaA',
																			 'controller' => 'Monedas', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MODIFICACION_MONEDA'),
                                                         ),
                              		
					                              		array(
					                              				'img' => 'icons/packs/fugue/16x16/application-form.png',
					                              				'caption' => 'Configuracio&oacute;n de Monedas Enlazadas',
					                              				'id' => 'mnuSisteMonedaEnlazada',
					                              				'action' => array('form' =>  'CarpeDiem.App.frmSistemasMonedaEnlazada',
					                              						'controller' => 'SistemasMonedaEnlazada', 'action' => 'index'),
					                              				'ajax' => true,
					                              				'functions' => array('MENU_SISTEMA'),
					                              		),
							
                                                       
														 array(
                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               'caption' => 'Bancos',
                                                               'id' => 'mnuBanco',
                                                               'action' => false,
                                                               'ajax' => true,
                                                               'functions' => array('MENU_BANCO'),
                                                               'childrens' => array(
                                                                           
                                                                                   
                                                                                    
                                                                                   
                                                                                    array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Listado de Bancos',
                                                                                       'id' => 'mnuBancosListado',
                                                                                        'action' => array('form' =>  'CarpeDiem.App.frmBanco',
                                                                                                     'controller' => EnumController::Bancos, 'action' => 'index'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('MENU_BANCO'),
                                                                                       'childrens' => array()
                                                                                ), 
																				array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Alta de Bancos y sucursales',
                                                                                       'id' => 'mnuBancoA',
                                                                                        'action' => array('form' =>  'CarpeDiem.App.frmBancoA',
                                                                                                     'controller' => EnumController::Bancos, 'action' => 'index'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('ADD_BANCO'),
                                                                                       'childrens' => array()
                                                                                ),
                                                               		
                                                               		array(
                                                               				'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               				'caption' => 'Concilacion Bancaria',
                                                               				'id' => 'mnufrmConciliacionBancariaMovimiento',
                                                               				'action' => array('form' =>  'CarpeDiem.App.frmConciliacionBancariaMovimiento',
                                                               						'controller' => EnumController::ComprobanteValores, 'action' => 'getConciliacionBancaria'),
                                                               				'ajax' => true,
                                                               				'functions' => array('MENU_CONCILIACION_BANCARIA'),
                                                               				'childrens' => array()
                                                               		)
                                                               
                                                               )
                                                        ),
                                                     array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Cuentas Bancarias',
                                                           'id' => 'mnuCuentaBancaria',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmCuentaBancaria',
                                                                             'controller' => 'CuentasBancaria', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_CUENTA_BANCARIA'),
                                                         ),
														 
                              		
				                              		array(
				                              				'img' => 'icons/packs/fugue/16x16/application-form.png',
				                              				'caption' => 'Tarjetas Cr&eacute;dito / D&eacute;bito',
				                              				'id' => 'mnuTarjetaCredito',
				                              				'action' => array('form' =>  'CarpeDiem.App.frmTarjetaCredito',
				                              						'controller' => 'TarjetasCredito', 'action' => 'index'),
				                              				'ajax' => true,
				                              				'functions' => array('MENU_TARJETA_CREDITO'),
				                              		),
													array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Conciliaci&oacute;n Bancaria',
                                                           'id' => 'mnuConciliacionBancaria',		   
                                                           'action' => array('form' =>  'CarpeDiem.App.frmConciliacionBancaria',
                                                                             'controller' => 'ConciliacionBancaria', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_CONCILIACION_BANCARIA'),
                                                         ),	 
                                                      array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Chequera',
                                                           'id' => 'mnuChequera',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmChequera',
                                                                                 'controller' => 'Chequeras', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_CHEQUERA'),
                                                          
                                                      
                                                         ),
                                                         
                                                         array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Cheques',
                                                           'id' => 'mnuCheque',
                                                           'action' => false,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_CHEQUE'),
                                                            'childrens' => array(
                                                                                 array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Cheques',
                                                                                       'id' => 'mnuCheques',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmCheque',
                                                                                                         'controller' => 'Cheques', 'action' => 'index'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('MENU_CHEQUE'),
                                                                                ),
																				array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Listado de Cheques Propios',
                                                                                       'id' => 'mnuChequesPropios',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmCheque',
																										 'form_param'=>    array('id_tipo_cheque' => array(EnumTipoCheque::Propio) ),
                                                                                                         'controller' => 'Cheques', 
																										 'action' => 'index'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('MENU_CHEQUE'),
                                                                                ),
																				array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Listado de Cheques Cheques Tercero',
                                                                                       'id' => 'mnuChequesTerceros',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmCheque',
																										 'form_param'=>    array('id_tipo_cheque' => array(EnumTipoCheque::Terceros) ),
                                                                                                         'controller' => 'Cheques', 
																										 'action' => 'index'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('MENU_CHEQUE'),
                                                                                ),
																				array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Nuevo Cheque Rechazado',
                                                                                       'id' => 'mnuChequesRechazadoAlta',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmRechazoChequeMD',
                                                                                                         'controller' => 'RechazoCheques', 'action' => 'index'),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('ADD_RECHAZO_CHEQUE'),
                                                                                ),
																				
                                                                                array(
                                                                                       'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                       'caption' => 'Consulta Cheques Rechazados',
                                                                                       'id' => 'mnuChequesRechazados',
                                                                                       'action' => array('form' =>  'CarpeDiem.App.frmRechazoCheque',
                                                                                                         'controller' => 'RechazoCheques', 'action' => 'index',
				                                                                                       		'context_menu'=> array( array('control' => 'gv', 'options' => array(
				                                                                                       				
				                                                                                       				array('text' =>'Ver &amp;Asientos','type' => 'text',
				                                                                                       						'action' =>'msVerAsientos_Click',
				                                                                                       						'functions' => array('MENU_ASIENTO') ),
				                                                                                       				
				                                                                                       				array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
				                                                                                       						'action' =>'msVerComprobanteRelacionado_Click',
				                                                                                       						'functions' => array('CONSULTA_RECHAZO_CHEQUE') ),
				                                                                                       				
				                                                                                       				
				                                                                                       				array('text' =>'ANULAR',    'type' => 'text',
				                                                                                       						'action' =>'btnAnular_Click',
				                                                                                       						'functions' => array('BAJA_RECHAZO_CHEQUE') ),
				                                                                                       				array('text' =>'Modificar',     'type' => 'text',
				                                                                                       						'action' =>'btnEditar_Click',
				                                                                                       						'functions' => array('ADD_RECHAZO_CHEQUE') ),
				                                                                                       		)
				                                                                                       		) )
                                                                                       
                                                                                       
                                                                                       
                                                                                       ),
                                                                                       'ajax' => true,
                                                                                       'functions' => array('CONSULTA_RECHAZO_CHEQUE'),
                                                                                )
                                                                                )
                                                         ),
													
                                                        
                                                       
						                              		array(
						                              				'img' => 'icons/packs/fugue/16x16/application-form.png',
						                              				'caption' => 'Listado de Valores',
						                              				'id' => 'mnuListadoComprobanteValor',
						                              				'action' => false,
						                              				'ajax' => true,
						                              				'functions' => array('MENU_COMPROBANTE_VALOR'),
						                              				'childrens' => array(
						                              						array(
						                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
						                              								'caption' => 'Listado de Valores Entregados/Recibidos',
						                              								'id' => 'mnuComprobanteValor',
						                              								'action' => array('form' =>  'CarpeDiem.App.frmComprobanteValor',
						                              										
						                              										'controller' => 'ComprobanteValores', 'action' => 'index',
						                              										'form_param'=> array(
						                              												'form_title'=>'Listado de Valores Entregados/Recibidos'
						                              										),
						                              								
						                              								
						                              								),
						                              								'ajax' => true,
						                              								'functions' => array('MENU_COMPROBANTE_VALOR'),
						                              								
						                              						)
						                              						
						                              				)
						                              		),
                              		
                              		array(
                              				'img' => 'icons/packs/fugue/16x16/application-form.png',
                              				'caption' => 'Reportes',
                              				'id' => 'mnuReporteComprobanteValor',
                              				'action' => false,
                              				'ajax' => true,
                              				'functions' => array('MENU_COMPROBANTE_VALOR'),
                              				'childrens' => array(
                              						array(
                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
                              								'caption' => 'Balance de Tesoreria por forma de pago (con Recibo)',
                              								'id' => 'mnuMovimientoTesoreria',
                              								'action' => array('form' =>  'CarpeDiem.App.frmMovimientoTesoreria',
                              										
                              										'controller' => 'ComprobanteValores', 'action' => 'getMovimientoFondos'),
                              								
                              								'ajax' => true,
                              								'functions' => array('MENU_COMPROBANTE_VALOR'),
                              								
                              						),
                              						
                              						array(
                              								'img' => 'icons/packs/fugue/16x16/application-form.png',
                              								'caption' => 'Valores Entregados/Recibidos Totalizado',
                              								'id' => 'mnuMovimientoTesoreriaMovTottoal',
                              								'action' => array('form' =>  'CarpeDiem.App.frmCefsharpReport',
                              										
                              										'controller' => 'ComprobanteValores', 'action' => 'getMovimientoFondos'),
                              								
                              								'ajax' => true,
                              								'functions' => array('MENU_COMPROBANTE_VALOR'),
                              								
                              						),
                              						
                              				)
                              		)
                              		
                              		
						                              		
                              							
												)
							),
     		array(
     				'img' => 'icons/packs/fugue/16x16/application-form.png',
     				'caption' => 'Gastos',
     				'id' => 'mnuGasto',
     				'action' => false,
     				'ajax' => true,
     				'functions' => array('MENU_GASTO'),
     				'childrens' => array(
     						array(
     								'img' => 'icons/packs/fugue/16x16/application-form.png',
     								'caption' => 'Listado de Gastos',
     								'id' => 'mnuGastosListado',
     								'action' => array('form' =>  'CarpeDiem.App.frmFacturaCompra',
     										'controller' => EnumController::Gastos, 'action' => 'index',  'form_desktop' => 'frmFacturaCompra',
     										'context_menu'=> array( array('control' => 'gv', 'options' => array(
     												
     												array('text' =>'Ver &amp;Asientos','type' => 'text',
     														'action' =>'msVerAsientos_Click',
     														'functions' => array('MENU_ASIENTO') ),
     												
     												array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
     														'action' =>'msVerComprobanteRelacionado_Click',
     														'functions' => array('CONSULTA_GASTO') ),
     												
     												
     												array('text' =>'ANULAR',    'type' => 'text',
     														'action' =>'btnAnular_Click',
     														'functions' => array('BAJA_GASTO') ),
     												array('text' =>'Modificar',     'type' => 'text',
     														'action' =>'btnEditar_Click',
     														'functions' => array('ADD_GASTO') ),
     										)
     										) )
     								),
     								'ajax' => true,
     								'functions' => array('CONSULTA_GASTO'),
     						),
     						array(
     								'img' => 'icons/packs/fugue/16x16/application-form.png',
     								'caption' => 'Conceptos de Gasto',
     								'id' => 'mnuTipoGasto',
     								'action' => false,
     								'ajax' => true,
     								'functions' => array('MENU_SERVICIO'),
     								'childrens' =>array(
     										
     										array(
     												'img' => 'icons/packs/fugue/16x16/application-form.png',
     												'caption' => 'Listado de Conceptos de Gasto',
     												'id' => 'mnuGastoArticulo',
     												'action' => array('form' =>  'CarpeDiem.App.frmArticulo','form_param'=>array('id_producto_tipo' => array(EnumProductoTipo::GAST) ),
     														'controller' => EnumController::GastosArticulo, 'action' => 'index'),
     												'ajax' => true,
     												'functions' => array('MENU_GASTO')
     										),                                                                                 array(
     												'img' => 'icons/packs/fugue/16x16/application-form.png',
     												'caption' => 'Alta de Conceptos de Gasto',
     												'id' => 'mnuGastoArticuloA',
     												'action' => array('form' =>  'CarpeDiem.App.frmArticuloA',
     														'controller' => EnumController::GastosArticulo,'form_param'=> array('id_producto_tipo' => array(EnumProductoTipo::GAST) ),
     														'action' => 'index'),
     												'ajax' => true,
     												'functions' => array('ADD_GASTO'),
     										)
     								)
     						),
     						array(
     								'img' => 'icons/packs/fugue/16x16/application-form.png',
     								'caption' => 'Rendiciones',
     								'id' => 'mnuFondoFijo',
     								'action' => false,
     								'ajax' => true,
     								'functions' => array('MENU_CARTERA_RENDICION'),
     								'childrens' => array(
     										array(
     												'img' => 'icons/packs/fugue/16x16/application-form.png',
     												'caption' => 'Movimientos de Carteras',
     												'id' => 'mnuMovimientoCartera',
     												'action' => array('form' =>  'CarpeDiem.App.frmMovimientoCarteraRendicion',
     														'controller' => 'MovimientosCarteraRendicion', 'action' => 'index'),
     												'ajax' => true,
     												'functions' => array('MENU_CARTERA_RENDICION'),
     												'childrens' => array()
     										),
     										
     										array(
     												'img' => 'icons/packs/fugue/16x16/application-form.png',
     												'caption' => 'Carteras de Rendici&oacute;n',
     												'id' => 'mnuCarteraRendicion',
     												'action' => array('form' =>  'CarpeDiem.App.frmCarteraRendicion',
     														'controller' => 'CarterasRendicion', 'action' => 'index'),
     												'ajax' => true,
     												'functions' => array('CONSULTA_CARTERA_RENDICION'),
     												'childrens' => array()
     										),
     										
     										/*
     										 array(
     										 'img' => 'icons/packs/fugue/16x16/application-form.png',
     										 'caption' => 'Nueva Cartera',
     										 'id' => 'mnuTipoAsiento',
     										 'action' => array('form' =>  'CarpeDiem.App.frmCarteraRendicionA',
     										 'controller' => 'CarterasRendicion', 'action' => 'index'),
     										 'ajax' => true,
     										 'functions' => array('ADD_CARTERA_RENDICION'),
     										 'childrens' => array()
     										 )
     
     										 */
     										
     								)
     						),
     						array(
     								'img' => 'icons/packs/fugue/16x16/application-form.png',
     								'caption' => 'Nuevo Gasto',
     								'id' => 'mnuGastosMD',
     								'action' => array('form' =>  'CarpeDiem.App.frmFacturaCompraMD',
     										'controller' =>  EnumController::Gastos,
     										'action' => 'index',  'form_desktop' => 'frmFacturaCompraMD'),
     								'ajax' => true,
     								'functions' => array('ADD_GASTO'),
     						),
     						array(
     								'img' => 'icons/packs/fugue/16x16/application-form.png',
     								'caption' => 'Nuevo Gasto por OC',
     								'id' => 'mnuGastosMDOC',
     								'action' => array('form' =>  'CarpeDiem.App.frmFacturaCompraMD',
     										'form_param'=>'OC',
     										'controller' =>  EnumController::Gastos,
     										'action' => 'index',  'form_desktop' => 'frmFacturaCompraMD'),
     								'ajax' => true,
     								'functions' => array('ADD_GASTO'),
     						),
     						array(
     								'img' => 'icons/packs/fugue/16x16/application-form.png',
     								'caption' => 'Listado de Orden de Pago de Gasto',
     								'id' => 'mnuOrdenPagoRendicionGasto',
     								'action' => array('form' =>'CarpeDiem.App.frmOrdenPago',
     										'controller' => EnumController::OrdenesPagoGasto , 'action' => 'index',
     										'context_menu'=> array( array('control' => 'gv', 'options' => array(
     												array('text' =>'Ver &amp;Asientos','type' => 'text',
     														'action' =>'msVerAsientos_Click',
     														'functions' => array('MENU_ASIENTO') ),
     												
     												array('text' =>'Ver Comp. &amp;Relacionados','type' => 'text',
     														'action' =>'msVerComprobanteRelacionado_Click',
     														'functions' => array('CONSULTA_ORDEN_PAGO_GASTO') ),
     												array('text' =>'ANULAR',    'type' => 'text',
     														'action' =>'btnAnular_Click',
     														'functions' => array('BAJA_ORDEN_PAGO_GASTO') ),
     												array('text' =>'Modificar',     'type' => 'text',
     														'action' =>'btnEditar_Click',
     														'functions' => array('ADD_ORDEN_PAGO_GASTO') ),
     										)
     										) )),
     								'ajax' => true,
     								'functions' => array('MENU_ORDEN_PAGO_GASTO'),
     						),
     						array(
     								'img' => 'icons/packs/fugue/16x16/application-form.png',
     								'caption' => 'Nueva Orden de Pago de Gasto',
     								'id' => 'mnuOrdenPagoRendicionGastoMD',
     								'action' => array('form' =>  'CarpeDiem.App.frmOrdenPagoMD',
     										'controller' => EnumController::OrdenesPagoGasto, 'action' => 'index'),
     								'ajax' => true,
     								'functions' => array('ADD_ORDEN_PAGO_GASTO'),
     						),
     						
     						array(
     								'img' => 'icons/packs/fugue/16x16/application-form.png',
     								'caption' => 'Reportes',
     								'id' => 'mnuGastosReporte',
     								'action' => false,
     								'ajax' => true,
     								'functions' => array('MENU_CARTERA_RENDICION'),
     								'childrens' => array(
     										array(
     												'img' => 'icons/packs/fugue/16x16/application-form.png',
     												'caption' => 'Listado de Gastos sin imputar',
     												'id' => 'mnuGastoImpagoTotalizado',
     												'action' => array('form' =>  'CarpeDiem.App.frmComprobantesImpagosAgrupado', 'form_param'=>    array('id_sistema' => array(EnumSistema::CAJA) ),
     														'controller' => 'Comprobantes', 'action' => 'getComprobantesImpagosAgrupadoPorPersona'),
     												'ajax' => true,
     												'functions' => array('CONSULTA_GASTO'),
     										),
     										array(
     												'img' => 'icons/packs/fugue/16x16/application-form.png',
     												'caption' => 'Listado de Valores en Pagos',
     												'id' => 'mnuComprobanteValorCaja',
     												'action' => array('form' =>  'CarpeDiem.App.frmComprobanteValor',
     														'form_param'=>array('id_sistema' => array(EnumSistema::CAJA) ),
     														'controller' => 'ComprobanteValores', 'action' => 'index'),
     												'ajax' => true,
     												'functions' => array('MENU_COMPROBANTE_VALOR'),
     												
     										),
     										array(
     												'img' => 'icons/packs/fugue/16x16/application-form.png',
     												'caption' => 'Listado de Comprobantes Adeudados',
     												'id' => 'mnuComprobantesImpagosCajaListado',
     												'action' => array('form' =>  'CarpeDiem.App.frmComprobante',
     														'form_param'=> array('id_sistema' => array(EnumSistema::CAJA),
     																'facturas_impagas' => '1',
     																'form_title'=>'Listado de Comprobantes sin rendir'
     														),
     														'controller' => 'Comprobantes', 'action' => 'getComprobantesImpagos'),
     												'ajax' => true,
     												'functions' => array('MENU_CUENTA_CORRIENTE_PROVEEDOR'),
     										),
     										array(
     												'img' => 'icons/packs/fugue/16x16/application-form.png',
     												'caption' => 'Impuestos en Comprobantes',
     												'id' => 'mnuComprobanteImpuestoCaja',
     												'action' => array('form' =>  'CarpeDiem.App.frmComprobanteImpuesto',
     														'form_param'=>
     														array('id_sistema' =>
     																array(EnumSistema::CAJA) ),
     														'controller' => 'ComprobanteImpuestos', 'action' => 'ReporteComprobanteImpuesto'),
     												'ajax' => true,
     												'functions' => array('MENU_COMPRAS_REPORTE'),
     										)
     										)
     								)
     						
     				)
     		),
							 array(//////////////////////////////MODULO/////////////////////////
                              'caption' => 'Impuestos',
                              'id' => 'mnuImpuesto',
                              'img' => 'icons/packs/fugue/16x16/user-white.png',
                              'root' =>true,
                              'action' => null,
                              'ajax' => false,
                              'functions' => array('MENU_IMPUESTO'),
                              'childrens' => array(
													  array(
														   'img' => 'icons/packs/fugue/16x16/application-form.png',
														   'caption' => 'Reportes / Salidas',
														   'id' => 'mnuReporteSalidaImpuesto',
														   'action' => false,
														   'ajax' => true,
														   'functions' => array('MENU_IMPUESTO'),
														   'childrens' => array(
																				   array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Generar Reporte Citi Ventas ',
																				   'id' => 'mnuGenerarReporteCitiVentas',
																				   'action' => array('form' =>  'CarpeDiem.App.frmCitiVentasCompras',
																									 'form_param'=>    array('id_sistema' => array(EnumSistema::VENTAS) ),
																									 'controller' => 'ComprobanteImpuestos', 
																									 'action' => 'ReporteComprobanteImpuesto'),
																				   'ajax' => true,
																				   'functions' => array('MENU_VENTAS_REPORTE_CITI_VENTA'),
																				   ),
																				   array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Generar Reporte Citi Compras ',
																				   'id' => 'mnuGenerarReporteCitiCompras',
																				   'action' => array('form' =>  'CarpeDiem.App.frmCitiVentasCompras',
																									 'form_param'=>    array('id_sistema' => array(EnumSistema::COMPRAS)),
																									 'controller' => 'ComprobanteImpuestos', 
																									 'action' => 'ReporteComprobanteImpuesto'),
																				   'ajax' => true,
																				   'functions' => array('MENU_VENTAS_REPORTE_CITI_COMPRA'),
																				   ),
																				   array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Generar Reporte de Percepci&oacute;n IIBB',
																				   'id' => 'mnuPercepcionesRetencionesIIBB',
																				   'action' => array('form' =>  'CarpeDiem.App.frmPercepcionesRetenciones',
																									 'form_param'=>    array(
																													'id_tipo_impuesto' => array(EnumTipoImpuesto::PERCEPCIONES),	
																													'id_sub_tipo_impuesto' => array(EnumSubTipoImpuesto::PERCEPCION_IIBB) ),
																									 'controller' => 'Reportes', 
																									 'action' => 'PercepcionesRetenciones'),
																				   'ajax' => true,
																				   'functions' => array('MENU_VENTAS_REPORTE_PERCEPCION_IIBB'),
																				   ),
																				 array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Generar Reporte de Retenci&oacute;n IIBB',
																				   'id' => 'mnuPercepcionesRetencionesIIBBRetencion',
																				   'action' => array('form' =>  'CarpeDiem.App.frmPercepcionesRetenciones',
																									 'form_param'=>    array(
																													'id_tipo_impuesto' => array(EnumTipoImpuesto::RETENCIONES),	
																													'id_sub_tipo_impuesto' => array(EnumSubTipoImpuesto::RETENCION_IIBB) ),
																									 'controller' => 'Reportes', 
																									 'action' => 'PercepcionesRetenciones'),
																				   'ajax' => true,
																				   'functions' => array('MENU_VENTAS_REPORTE_RETENCION_IIBB'),
																				   ),
																				   
																				   array(
																				   'img' => 'icons/packs/fugue/16x16/application-form.png',
																				   'caption' => 'Generar Reporte de Percepci&oacute;n/Retenci&oacute;n',
																				   'id' => 'mnuPercepcionesRetenciones',
																				   'action' => array('form' =>  'CarpeDiem.App.frmPercepcionesRetenciones',
																									 'controller' => 'Reportes', 
																									 'action' => 'PercepcionesRetenciones'),
																				   'ajax' => true,
																				   'functions' => array('MENU_VENTAS_REPORTE_PERCEPCION_RETENCION_GENERICO'),
																				   ),
																				   
																				)
														   ),
																array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Importacion Archivos Impositivos',
                                                           'id' => 'mnuProcesosPeriodicos',
                                                           'action' => null,
                                                           'ajax' => true,
                                                           'functions' => array('MENU_PROCESOS_PERIODICOS'),
                                                           'childrens' => array(
                                                                                array(
                                                                                   'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                                                   'caption' => 'Importaci&oacute;n general',
                                                                                   'id' => 'mnuPadronArba',
                                                                                   'action' => array('form' =>  'CarpeDiem.App.frmProcesoPadronArbaAlicuotaIIBB',
																									 'controller' => 'Impuestos', 'action' => 'importarImpuestos'),
                                                                                   'ajax' => true,
                                                                                   'functions' => array('MENU_PROCESOS_PERIODICOS'),
                                                                                   )
											
                                                                                )
                                                           ),
															)
							  
							  
							  ),
                            
                             array(//////////////////////////////MODULO/////////////////////////
                              'caption' => 'Auditor&iacute;a',
                              'id' => 'mnuAuditoriaMenu',
                              'img' => 'icons/packs/fugue/16x16/user-white.png',
                              'root' =>true,
                              'action' => null,
                              'ajax' => false,
                              'functions' => array('MENU_AUDITORIA'),
                              'childrens' => array(
													array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Auditoria Comprobante',
                                                           'id' => 'mnuAuditoriaComprobante',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmAuditoriaComprobante',
																			 'controller' => 'AuditoriaComprobantes', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('CONSULTA_AUDITORIA_COMPROBANTE')
                                                           ),
                                                     array(
                                                           'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                           'caption' => 'Panel de Auditor&iacute;a',
                                                           'id' => 'mnuPanelAuditoria',
                                                           'action' => array('form' =>  'CarpeDiem.App.frmPanelAuditoria',
																			 'controller' => 'Auditorias', 'action' => 'index'),
                                                           'ajax' => true,
                                                           'functions' => array('MENU_AUDITORIA2')
                                                           )
                                                    )
                             ),
						  array(//////////////////////////////MODULO/////////////////////////
                              'caption' => 'Ayuda',
                              'id' => 'mnuAyudaMenu',
                              'img' => 'icons/packs/fugue/16x16/user-white.png',
                              'root' =>true,
                              'action' => null,
                              'ajax' => false,
                              'functions' => array(''),
                              'childrens' => array(
													array(
                                                               'img' => 'icons/packs/fugue/16x16/application-form.png',
                                                               'caption' => 'Sobre el sistema...',
                                                               'id' => 'mnuAbout',
                                                                'action' => array('form' =>  'CarpeDiem.App.frmAboutS1',
																			 'controller' => '', 'action' => 'index'),
                                                               'ajax' => true,
                                                               'functions' => array(''),
                                                               'childrens' => array()
                                                        )
                                                    )
                             )
                    );
     
     private $view;
     
     function Menu($view){
         $this->view = $view;
     }
     
     private function configureSecurity(){//es para HTML
         
         $aux = array();
         
         
         foreach($this->menu as $option){
             
             $functions = array();
             
             if(isset($option['childrens'])){
                 
                 foreach($option['childrens'] as $child){
                     
                 	if(is_array($functions) && is_array($child['functions']))
						$functions = array_merge($functions, $child['functions']);
                     
                 }
             }
             
             
                 $opt_aux = $option;
                 $opt_aux['functions'] = $functions;
                 $opt_aux['functionsOperator'] = 'OR';
                 
                
                 	array_push($aux, $opt_aux);
             
         }
         
         $this->menu = $aux;
         
         
     }
     
     public function printMenu(){
         
         $menu = $this->configureSecurity();
         
         echo '	  <div class="navbar navbar-fixed-top">
					<div class="navbar-inner">
					  <div class="container">
						<a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						</a>
						<a href="#" class="brand">' .Configure::read("menuAppName") . '</a>
						<div id="main-menu" class="nav-collapse collapse">
						  <ul id="main-menu-left" class="nav">
							<li class="">&nbsp;</li>';
         
         foreach($this->menu as $option){
            $this->printOption($option); 
         }
         
         
        //Levanto el sessionhelper
        $sessionHelper = $this->view->loadHelper('Session');
         
         echo '</ul>
				  
                    <div class="pull-right">
                        <ul class="nav pull-right">
					        <li class="dropdown">
					          <a data-toggle="dropdown" class="dropdown-toggle" href="#">' . $sessionHelper->read('Auth.User.username') . '<b class="caret"></b></a>
					          <ul class="dropdown-menu">
						        <li><a href="'.Router::url(array('controller' => 'MisDatos', 'action' => 'index')).'"><i class="icon-user"></i>&nbsp;Mis Datos</a></li>
                                <li><a href="'.Router::url(array('controller' => 'Usuarios', 'action' => 'logoutAllSessions')).'"><i class="icon-off"></i>&nbsp;Salir</a></li>
					          </ul>
					        </li>
                        </ul>
                    </div>
				  
				</div><!-- /.nav-collapse -->
			  </div>
			</div><!-- /navbar-inner -->
		  </div><!-- /navbar -->
	';
         
     }
     
     
     public function printOption($option){
         
         $closeul = false;
         $modules = array();
         
         if(isset($option['modules']) && count($option['modules'])>0)
         	$modules = $option['modules'];
         	
         
         if (!$this->userHasPermission($option) ||  !isset($option['html_menu'])     )
            return;
            
         if($option['caption'] == 'Submenu'){
             $a="";
         }
            
         
		  if ($option['childrens'] != null){
			
            if(isset($option['root']) && $option['root'] == true){
                echo "<li class='dropdown'>";
			    echo "<a data-toggle='dropdown' class='dropdown-toggle' href='#'>".$option["caption"]. "<b class='caret'></b></a>";
                echo "<ul class='dropdown-menu'>";
                $closeul = true;
            }else{
                echo "<li class='dropdown submenu'>";
                //La clase submenu-disable la agregue yo para luego deshabilitarle el click
                echo "<a class='dropdown-toggle submenu-disable' data-toggle='dropdown' href='#'>".$option["caption"]. "</a>";
                echo "<ul class='dropdown-menu submenu-show submenu-hide'>";
                
                $closeul = true;
            }
		  }
		  else{
			echo "<li>";
			echo $this->view->Html->link($option['caption'] , $option['action'], array('escape' => false)); 
		  }
			
          
          
		  
		  
		  /*if($option['action'] != null){
              
              echo $this->view->Html->link($this->view->Html->image($option['img'], array('plugin' => false)) . $option['caption'] , $option['action'], array('escape' => false));        
          }else{
              
               echo '<a href="javascript:void(0);">' . $this->view->Html->image($option['img'], array('plugin' => false)) . $option["caption"] .'</a>';  
          }*/
		  
		  
          
          $chil = $this->printChildrens($option['childrens']);
          
          if($closeul == true)
            echo "</ul>";
          
          echo "</li>";
         
         
     }
     
     function printChildrens($childrens){
         
         if($childrens == null)
            return;
            
         //echo "<ul class='dropdown-menu'>";
         
         foreach($childrens as $children){
         	
         	$modules = array();
         	
         	if(isset($children['modules']) && count($children['modules'])>0)
         		$modules = $children['modules'];
             
         	if ($this->userHasPermission($children) && isset($children['html_menu'])){
              
                  if(isset($children['childrens']) && $children['childrens'] != null){
                      $this->printOption($children, true);
                  }else{
                        echo "<li class='dropdown'>"; 
                        echo $this->view->Html->link($children['caption'] , $children['action'], array('escape' => false));
                        echo "</li>"; 
                      
                  }
                 
                  
            
            }
              
         }
         
         //echo "</ul>";
         
     }
     
     function userHasPermission($option){   
         
         $sm = new SecurityManager();
         
         $functionsOperator = (isset($option["functionsOperator"])?$option["functionsOperator"]:"AND");
         
         return $sm->userHasPermission($this->view, $option['functions'], $functionsOperator);
         
         
     }
     /*function userHasPermission($option){
         
         
         if($option['functions'] == null)
            return true;
         
         $sessionHelper = $this->view->loadHelper('Session');
         
         $username = $sessionHelper->read('Auth.User.username');
         
         $in = "";
         foreach($option['functions'] as $function){
     
            if(trim($in) != '')
                $in .= ",";
            
             $in .=  "'" . $function . "'";
             
         }
         
         $in = "(" . $in .")";
         
         App::uses('Usuario', 'Model');
         $model = new Usuario();
         
         $sql = "select count(1) as cant 
                    from usuario u
                    join rol r on (r.id = u.id_rol)
                    join funcion_x_rol fr on (r.id = fr.id_rol)
                    join funcion f on (fr.id_funcion = f.id)
                    where u.username = '"  . $username . "' and f.c_funcion in " . $in;  
         
         $res = $model->query($sql);
         
         $cant = $res[0][0]['cant'];
         
         if ($cant < 1)
            return false;
         else
            return true;
         
     }*/
     
	 
     function getMenujson(){
         
       $menu = $this->menu; 
       
       foreach($menu as $k => $item ){
           
            if ($this->userHasPermission($item)){ //si tiene permisos el usuario sigo adelante
            
                if( count($item["childrens"]) >0 ){
                     foreach($item["childrens"] as $i =>$children ){
                         
                         if( isset($children["childrens"])){
                             if( count($children["childrens"]) >0 ){
                                 foreach($children["childrens"] as $z =>$item_children ){
                                    if (!$this->userHasPermission($item_children)){
                                        unset($menu[$k]["childrens"][$i]["childrens"][$z]);
                                    } 
                                     
                                 }
                            }
                       }
                    
                        if (!$this->userHasPermission($children)){
                            unset($menu[$k]["childrens"][$i]);
                        } 
                     }
                }  
            }else{   
                unset($menu[$k]);
            }   
       }
       return $menu;         
     }

}

?>