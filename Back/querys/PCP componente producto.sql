SELECT comprobante_item.id ,pedido.`razon_social`, pedido.`nro_comprobante` AS numero_pedido,
 comprobante_item.`n_item`,
 comprobante_item.`cantidad` AS cantidad_pedido,
prod.`codigo` AS codigo_producto,
DATE(pedido.`fecha_generacion`) AS fecha_generacion_pedido,

DATE_FORMAT(CAST((`pedido`.`fecha_generacion` + INTERVAL IF(ISNULL(`comprobante_item`.`dias`),0,`comprobante_item`.`dias`) DAY) AS DATE),'%d/%m/%Y') 
AS fecha_entrega_item,

stock_producto.`stock`,producto.`codigo` codigo_componente,producto.`d_producto` AS componente, 
 producto_relacion.`cantidad_hijo` cantidad_componente_producto,
oa.`nro_comprobante` AS orden_Armado,
estado_comprobante.`d_estado_comprobante` AS estado_oa,
@id_pedido_item:=comprobante_item.id,

( SELECT  SUM(remito_item.cantidad) FROM comprobante_item remito_item
  JOIN   comprobante ON comprobante.id = remito_item.id_comprobante
  AND    comprobante.`id_tipo_comprobante` = 203
  WHERE  remito_item.id_comprobante_item_origen = @id_pedido_item ) remitido1

 FROM stock_producto 
JOIN producto ON producto.id = stock_producto.`id`

JOIN producto_relacion ON producto_relacion.`id_producto_hijo`= producto.`id`
JOIN producto AS prod ON prod.id = producto_relacion.`id_producto_padre`

JOIN comprobante_item ON comprobante_item.id_producto = prod.`id`
JOIN comprobante pedido ON pedido.id = comprobante_item.`id_comprobante`


LEFT JOIN comprobante_item oa_item ON oa_item.`id_comprobante_item_origen` = comprobante_item.`id`

 JOIN comprobante oa ON oa_item.`id_comprobante` = oa.`id`
 JOIN estado_comprobante ON oa.`id_estado_comprobante` = estado_comprobante.`id`

AND producto.`id_categoria` IN (1,3,5,9,24,36,44,57,319,320,321,323,324,325,326,327,328,329,330,331,
                332,333,334,335,336,337,339,341,343,344,345,346,347,348,349,350,351,352,
                353,354,355,356,357,359,360,361,372,373,374,377,378,379,380,381,382,383,
                384,385,386,387,388,389,390,391,395,396,399,400,402,403,404,405,406,411,412,
                413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,
                432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,448,449,454,455,456,457,
                458,459,460,461,462,466,467,463,468,469,470,471,472,474,477,478,479,480,481,482,483,484,490,
                491,492,493,494)
                
WHERE stock<=0

AND stock_producto.id_deposito=4 
AND  producto.id_producto_tipo=2

AND pedido.`id_estado_comprobante` = 1
AND pedido.`id_tipo_comprobante` = 202
AND oa.`id_tipo_comprobante` = 104
AND comprobante_item.`activo`=1
AND prod.`id_producto_tipo` =1 
                
                     
ORDER BY DATE_FORMAT(CAST((`pedido`.`fecha_generacion` + INTERVAL IF(ISNULL(`comprobante_item`.`dias`),0,`comprobante_item`.`dias`) DAY) AS DATE),'%d/%m/%Y')  ASC

LIMIT 10000000000000000