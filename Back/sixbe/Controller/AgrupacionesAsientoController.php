<?php
    
	
	
	
	
	//ojo
    class AgrupacionesAsientoController extends AppController {
        public $name = 'AgrupacionesAsiento';
        public $model = 'AgrupacionAsiento';
        public $helpers = array ('Session', 'Paginator', 'Js');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

        public function index() {

            if($this->request->is('ajax'))
                $this->layout = 'ajax';

            $this->loadModel($this->model);
            $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);

            $conditions = array(); 

            $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
                'conditions' => $conditions,
                'limit' => 10,
                'page' => $this->getPageNum()
            );

            if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de TipoAsientoCausas', 'Datos de los Tipo de Impuesto', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));

                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_TipoAsiento', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();

                //Headers
                $formBuilder->addHeader('Id', 'TipoAsiento.id', "10%");
                $formBuilder->addHeader('Nombre', 'TipoAsiento.d_TipoAsiento_causa', "50%");
                $formBuilder->addHeader('Nombre', 'TipoAsiento.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_TipoAsientoCausa');
                $formBuilder->addField($this->model, 'cotizacion');

                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
            }
            else
            { // vista json
                $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
                $page_count = $this->params['paging'][$this->model]['pageCount'];

                foreach($data as &$registro )
                {
                    // $registro["AgrupacionAsiento"]["d_tipo_asiento_leyenda_defecto"] = ""; //MP: Preseteo la variable
                    // if(isset($registro['TipoAsientoLeyenda']))
                    // { 
                         // //recorro el ARRAY lo puedo usar como $key  es un valor numerico y Leyenda es el objecto
                        // foreach( $registro['TipoAsientoLeyenda'] as  $key =>$leyenda)
                        // {
                            // if(isset( $leyenda))
                            // {   //MP: Solo la sobre escribo si es por_defecto = "1"
                                // if($leyenda["por_defecto"]=="1" )
                                // {             
                                    // $registro["AgrupacionAsiento"]["d_tipo_asiento_leyenda_defecto"]  = $leyenda['d_tipo_asiento_leyenda'];
                                // }
                            // }
                        // } 
                    // }
                    unset($registro["TipoAsiento"]);  //MP: se puede sacar tranquilamente
                }   

                $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "list",
                    "content" => $data,
                    "page_count" =>$page_count
                );
                $this->set($output);
                $this->set("_serialize", array("status", "message","page_count", "content"));

            }
        }


        public function abm($mode, $id = null) {


            if ($mode != "A" && $mode != "M" && $mode != "C")
                throw new MethodNotAllowedException();

            $this->layout = 'ajax';
            $this->loadModel($this->model);
            if ($mode != "A"){
                $this->TipoAsiento->id = $id;
                $this->request->data = $this->TipoAsiento->read();
            }


            App::import('Lib', 'FormBuilder');
            $formBuilder = new FormBuilder();

            //Configuracion del Formulario
            $formBuilder->setController($this);
            $formBuilder->setModelName($this->model);
            $formBuilder->setControllerName($this->name);
            $formBuilder->setTitulo('TipoAsientoCausa');

            if ($mode == "A")
                $formBuilder->setTituloForm('Alta de TipoAsientoCausa');
            elseif ($mode == "M")
                $formBuilder->setTituloForm('Modificaci&oacute;n de TipoAsientoCausa');
            else
                $formBuilder->setTituloForm('Consulta de TipoAsientoCausa');

            //Form
            $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

            //Fields





            $formBuilder->addFormInput('d_TipoAsientoCausa', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
            $formBuilder->addFormInput('cotizacion', 'Cotizaci&oacute;n', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));    


            $script = "
            function validateForm(){

            Validator.clearValidationMsgs('validationMsg_');

            var form = 'TipoAsientoCausaAbmForm';

            var validator = new Validator(form);

            validator.validateRequired('TipoAsientoCausaDTipoAsientoCausa', 'Debe ingresar un nombre');
            validator.validateRequired('TipoAsientoCausaCotizacion', 'Debe ingresar una cotizaci&oacute;n');


            if(!validator.isAllValid()){
            validator.showValidations('', 'validationMsg_');
            return false;   
            }

            return true;

            } 


            ";

            $formBuilder->addFormCustomScript($script);

            $formBuilder->setFormValidationFunction('validateForm()');

            $this->set('abm',$formBuilder);
            $this->set('mode', $mode);
            $this->set('id', $id);
            $this->render('/FormBuilder/abm');    
        }



        /**
        * @secured(ADD_TIPO_ASIENTO)
        */
        public function add() {
            if ($this->request->is('post')){
                $this->loadModel($this->model);

                try{
                    if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
                        $mensaje = "La Agrupacion ha sido creada exitosamente";
                        $tipo = EnumError::SUCCESS;

                    }else{
                        $errores = $this->{$this->model}->validationErrors;
                        $errores_string = "";
                        foreach ($errores as $error){
                            $errores_string.= "&bull; ".$error[0]."\n";

                        }
                        $mensaje = $errores_string;
                        $tipo = EnumError::ERROR; 

                    }
                }catch(Exception $e){

                    $mensaje = "Ha ocurrido un error,La Agrupacion no ha podido ser creada.".$e->getMessage();
                    $tipo = EnumError::ERROR;
                }
                $output = array(
                    "status" => $tipo,
                    "message" => $mensaje,
                    "content" => ""
                );
                //si es json muestro esto
                if($this->RequestHandler->ext == 'json'){ 
                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content"));
                }else{

                    $this->Session->setFlash($mensaje, $tipo);
                    $this->redirect(array('action' => 'index'));
                }     
            }

            //si no es un post y no es json
            if($this->RequestHandler->ext != 'json')
                $this->redirect(array('action' => 'abm', 'A'));   
        }

        /**
        * @secured(MODIFICACION_TIPO_ASIENTO)
        */
        public function edit($id) {


            if (!$this->request->is('get')){

                $this->loadModel($this->model);
                $this->{$this->model}->id = $id;

                try{ 
                    if ($this->{$this->model}->saveAll($this->request->data)){
                        $mensaje =  "La Agrupacion ha sido modificado exitosamente";
                        $status = EnumError::SUCCESS;


                    }else{   //si hubo error recupero los errores de los models

                        $errores = $this->{$this->model}->validationErrors;
                        $errores_string = "";
                        foreach ($errores as $error){ //recorro los errores y armo el mensaje
                            $errores_string.= "&bull; ".$error[0]."\n";

                        }
                        $mensaje = $errores_string;
                        $status = EnumError::ERROR; 
                    }

                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 

                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));

                    } 
                }catch(Exception $e){
                    $this->Session->setFlash('Ha ocurrido un error, La Agrupacion no ha podido modificarse.', 'error');

                }

            }else{ //si me pide algun dato me debe mandar el id
                if($this->RequestHandler->ext == 'json'){ 

                    $this->{$this->model}->id = $id;
                    //$this->{$this->model}->contain('Provincia','Pais');
                    $this->request->data = $this->{$this->model}->read();          

                    $output = array(
                        "status" =>EnumError::SUCCESS,
                        "message" => "list",
                        "content" => $this->request->data
                    );   

                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content")); 
                }else{

                    $this->redirect(array('action' => 'abm', 'M', $id));

                }


            } 

            if($this->RequestHandler->ext != 'json')
                $this->redirect(array('action' => 'abm', 'M', $id));
        }

        /**
        * @secured(BAJA_TIPO_ASIENTO)
        */
        function delete($id) {
            $this->loadModel($this->model);
            $mensaje = "";
            $status = "";
            $this->TipoAsiento->id = $id;

            try{
                if ($this->{$this->model}->delete() ) {

                    //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');

                    $status = EnumError::SUCCESS;
                    $mensaje = "La Agrupacion ha sido eliminada exitosamente.";
                    $output = array(
                        "status" => $status,
                        "message" => $mensaje,
                        "content" => ""
                    ); 

                }

                else
                    throw new Exception();

            }catch(Exception $ex){ 
                //$this->Session->setFlash($ex->getMessage(), 'error');

                $status = EnumError::ERROR;
                $mensaje = $ex->getMessage();
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
            }

            if($this->RequestHandler->ext == 'json'){
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));

            }else{
                $this->Session->setFlash($mensaje, $status);
                $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            }
        }

        
        /**
        * @secured(CONSULTA_TIPO_ASIENTO)
        */
        public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }

        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);

        }

    }
?>