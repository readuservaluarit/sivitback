<?php

class DepartamentoPicker extends EntityPicker{
    
    public $model = "Departamento";
    
    public $pickerTitle = "Selección de Departamento";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Nombre", "field" => "d_departamento", "width" => "90%", "keyOrder" => "d_departamento")
                                );

    public function getPickerConditions($busqueda){
        
        $pickerConditions = array('Departamento.d_departamento LIKE' => '%' . $busqueda['bus_unico'] . '%');
        
        return $pickerConditions;
        
        
    }
  /*
    public function getPickerInitialFilter(){
        
        //Filtra siempre por la Jurisdiccion del usuario si corresponde
        return DataFilter::getJurisdiccionPickerFilter();
        
    }
  */
  
}