<?php


class MediaFilesController extends AppController {
    public $name = 'MediaFiles';
    public $model = 'MediaFile';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
  
    /**
    * @secured(CONSULTA_MEDIA_FILE)
    */
    public function index() {
        
      
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $d_media_file_original = $this->getFromRequestOrSession('MediaFile.d_media_file_original');
        $id_media_file_tipo = $this->getFromRequestOrSession('MediaFile.id_media_file_tipo');
        
        $conditions = array(); 
           
        if($d_media_file_original!="")
            array_push($conditions, array('MediaFile.d_media_file_original LIKE' => '%' . $d_media_file_original  . '%')); 
             
        if($id_media_file_tipo!="")
            array_push($conditions, array('MediaFile.id_media_file_tipo =' => $id_media_file_tipo  ));
            
         
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' => array('MediaFileTipo','Estado'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
        
        ////////////////////
        //Filters
        ///////////////////
        
          //CUE
        $formBuilder->addFilterInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $nombre));
        
        $formBuilder->addFilterInput('cuit', 'CUIT', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $cuit));
        
        
        $formBuilder->setDataListado($this, 'Listado de MediaFiles', 'Datos de los MediaFiles', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'MediaFile.id', "10%");
        $formBuilder->addHeader('Razon Social', 'MediaFile.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'MediaFile.cuit', "20%");
        $formBuilder->addHeader('Email', 'MediaFile.email', "20%");
        $formBuilder->addHeader('Provincia', 'd_provincia', "20%");
        $formBuilder->addHeader('Tel&eacute;fono', 'MediaFile.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField('Provincia', 'd_provincia');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
       
        foreach($data as &$valor){
             $valor['MediaFile']['tipo'] = $valor['MediaFileTipo']['d_media_file_tipo'];
             unset($valor['MediaFileTipo']);
              $valor['MediaFile']['estado'] = $valor['Estado']['d_estado'];
             unset($valor['Estado']);
            
        }
        
        
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    /**
    * @secured(ABM_MEDIA_FILE)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->MediaFile->id = $id;
            $this->MediaFile->contain();
            $this->request->data = $this->MediaFile->read();
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('MediaFile');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de MediaFiles');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de MediaFiles');
        else
            $formBuilder->setTituloForm('Consulta de MediaFiles');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('cuit', 'Cuit', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('calle', 'Calle', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  
        $formBuilder->addFormInput('tel', 'Tel&eacute;fono', 				array('class'=>'control-group'), array('class' => 'required', 'label' => false));
		$formBuilder->addFormInput('ciudad', 'Ciudad', 						array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax',							array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descuento', 'Descuento', 				array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('codigo_postal', 'Codigo Postal', 		array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
		$formBuilder->addFormInput('email', 'E-Mail', 						array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
		$formBuilder->addFormInput('percepcion', 'Percepcion', 				array('class'=>'control-group'), array('class' => 'required', 'label' => false));
		$formBuilder->addFormInput('numero', 'Numero', 						array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('observaciones', 'observaciones', 		array('class'=>'control-group'), array('class' => 'required', 'label' => false));
		$formBuilder->addFormInput('porc_percepcion', 'Porc Percepcion', 	array('class'=>'control-group'), array('class' => 'required', 'label' => false));
		$formBuilder->addFormInput('condicion_pago', 'Forma Pago', 				array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('localidad', 'Localidad', 				array('class'=>'control-group'), array('class' => 'required', 'label' => false));
		$formBuilder->addFormInput('id_pais', 'Pais', 						array('class'=>'control-group'), array('class' => 'required', 'label' => false));
		$formBuilder->addFormInput('id_provincia', 'Provincia', 			array('class'=>'control-group'), array('class' => 'required', 'label' => false));
								/*`razon_social` varchar(200) NOT NULL,
								  `cuit` varchar(30) NOT NULL,
								  `calle` varchar(100) DEFAULT NULL,
								  `tel` varchar(100) DEFAULT NULL,
								  `ciudad` varchar(100) DEFAULT NULL,
								  `fax` varchar(30) DEFAULT NULL,
								  `descuento` float(8,2) DEFAULT NULL,
								  `codigo_postal` varchar(30) DEFAULT NULL,
								  `email` varchar(150) DEFAULT NULL,
								  `percepcion` varchar(20) DEFAULT NULL,
								  `numero` int(20) DEFAULT NULL,
								  `observaciones` varchar(200) DEFAULT NULL,
								  `porc_percepcion` float(8,2) DEFAULT NULL,
								  `condicion_pago` int(11) DEFAULT NULL,
								  `localidad` varchar(200) DEFAULT NULL,
								  `id_pais` int(11) DEFAULT NULL,
								  `id_provincia` int(11) DEFAULT NULL,
								  `activo` tinyint(4) DEFAULT '1' COMMENT 'no_visible',*/
										
         
        
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'MediaFileAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('MediaFileRazonSocial', 'Debe ingresar una Raz&oacute;n Social');
                validator.validateRequired('MediaFileCuit', 'Debe ingresar un CUIT');
                validator.validateNumeric('MediaFileDescuento', 'El Descuento ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_cuit',
                        'data': 'cuit=' + $('#MediaFileCuit').val()+'&id='+$('#MediaFileId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('MediaFileCuit','El Cuit que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

    /**
    * @secured(ABM_MEDIA_FILE)
    */
    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_MEDIA_FILE, READONLY_PROTECTED)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
             $this->request->data["MediaFile"]["fecha"] = date("Y-m-d H:i:s");   
            try{
                if ($this->MediaFile->saveAll($this->request->data, array('deep' => true))){
                  
                    $id_MEDIA_FILE = $this->MediaFile->id;
                  
                    $mensaje = "El MediaFile ha sido creado exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el Media File no ha podido ser creado.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => ""
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
	/**
    * @secured(MODIFICACION_MEDIA_FILE, READONLY_PROTECTED)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "El Media File ha sido modificado exitosamente";
                     $status = EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
               }
               
               if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, el Media File no ha podido modificarse.', 'error');
                
            }
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            $this->{$this->model}->contain('Provincia','Pais');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
  
    /**
    * @secured(BAJA_MEDIA_FILE, READONLY_PROTECTED)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->MediaFile->id = $id;
       try{
            if ($this->MediaFile->delete()) {
                
                //$this->Session->setFlash('El MediaFile ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Archivo ha sido eliminado exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }
    



   
    
    
    
    
    public function home(){
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
    
 /**
    * @secured(UPLOAD_FILE_CLIENTE)
    */
    public function uploadFile(){
        
       App::uses('Folder', 'Utility');
        
        
        
        $dir_temp = new Folder(APP.'webroot/media_file');
        
        $random= rand(0,500);
        $id_mf = "";
      
      
        if (!empty($_FILES)) {
            $tempFile = $this->request->params['form']['file']['tmp_name'];
            $size = $this->request->params['form']['file']['size'];
            $size_mb = ($size/1024)/1024;
            $tamanio_maximo_mb = 10;
            
            $destino = $dir_temp->path .'//'.$random.'-'.$this->request->params['form']['file']['name'];
            // Validate the file type
            $fileTypes_foto = array('jpg','JPG','jpeg','JPEG','bmp','BMP','tif','TIF','tiff','TIFF','png','PNG','gif','GIF'); // File extensions
            $fileTypes_documento = array('doc','DOC','docx','DOCX','xml','XML','xls','XLS','xlsx','XLSX','docx','csv','CSV'); // File extensions
            $fileTypes_video = array('avi','AVI'); // File extensions
            $fileParts = pathinfo($this->request->params['form']['file']['name']);
            
            $id_media_file_tipo = '';
            if(in_array($fileParts['extension'],$fileTypes_foto))
                $id_media_file_tipo = EnumMediaFileTipo::Foto;
            elseif(in_array($fileParts['extension'],$fileTypes_documento))
                $id_media_file_tipo = EnumMediaFileTipo::Documento;
            elseif(in_array($fileParts['extension'],$fileTypes_video))
                $id_media_file_tipo = EnumMediaFileTipo::Video;
            
                       
            
            
            
            if ($id_media_file_tipo != '' && ($size/1024)/1024 <= $tamanio_maximo_mb) {
                move_uploaded_file($tempFile,$destino);
                $status = EnumError::SUCCESS;
                $mensaje = 'El archivo subio correctamente';
                $this->loadModel("MediaFile");
                $this->MediaFile->save(array("d_media_file_original"=>$this->request->params['form']['file']['name'],
                                             "d_media_file"=>$random.'-'.$this->request->params['form']['file']['name'],
                                             "tamanio" =>$size,
                                             "id_media_file_tipo"=>$id_media_file_tipo,
                                             "id_estado" =>EnumEstado::Aprobada,
                                             "fecha_creacion" => "'".date("Y-m-d H:i:s")."'",
                                             "ruta"=>'\webroot\media_file\\',
                                             "extension" =>$fileParts['extension']
                                      ));
                $id_mf = $this->MediaFile->id;
            } else {
                $status = EnumError::ERROR;
                $mensaje = 'Fallo la carga del archivo';
            }
            
           
                
             
            
            
            
        }else{
              
             $status = EnumError::ERROR;
              $mensaje = 'No fue enviado un archivo';
            
            
        }
        
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => "",
                    "id_add" => $id_mf
                ); 
                
                
            
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content", "id_add"));
            
     
        
       
    }
 
    
    
       
}
?>