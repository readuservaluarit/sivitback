/*Agrupa los productos por cantidad y total (precio_unitario*cantidad) pasa todo a USD. Si se le cambia el tipo de comprobante trae las facturas*/

SELECT 
  id_producto,
  producto.`codigo`,
  d_producto,
    COUNT(*),
  REPLACE(
    SUM(comprobante_item.`cantidad`),
    ".",
    ","
  ) AS cantidad,
    REPLACE(
    ROUND(
      IF(
        comprobante.`id_moneda` = 1,
        SUM(
          (comprobante_item.`precio_unitario` / comprobante.`valor_moneda`)
        ) ,
        SUM(
          comprobante_item.`precio_unitario`
        )
      ),
      2
    ),
    ".",
    ","
  ) AS sum_precios_unitarios,
  REPLACE(
    ROUND(
      IF(
        comprobante.`id_moneda` = 1,
        SUM(
          (comprobante_item.`precio_unitario` / comprobante.`valor_moneda`)*comprobante_item.`cantidad`
        ) ,
        SUM(
          comprobante_item.`precio_unitario`*comprobante_item.`cantidad`
        )
      ),
      2
    ),
    ".",
    ","
  ) AS total

FROM
  comprobante_item 
  JOIN comprobante 
    ON comprobante.id = comprobante_item.`id_comprobante` 
  JOIN producto 
    ON producto.id = comprobante_item.`id_producto` 
WHERE comprobante.`id_tipo_comprobante` IN (202)

  AND YEAR(comprobante.`fecha_generacion`) = '2016' 
  AND comprobante_item.`activo`=1
AND comprobante.`id_estado_comprobante` NOT IN (2,3)
GROUP BY id_producto 
LIMIT 100000 