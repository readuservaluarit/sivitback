
SELECT p.`id_provincia`, prov.d_provincia,SUM(fc.`subtotal`) AS subtotal_prov FROM factura fc
JOIN cliente c ON c.numero = fc.id_cliente
JOIN valuarit_valmec.`persona` p ON p.`cuit` = REPLACE(c.cuit,'-','') AND p.id_tipo_persona = 1
JOIN valuarit_valmec.`provincia` prov ON prov.id = p.`id_provincia`

WHERE fc.cae > 0 AND fc.fecha_emision >= '2015-09-01' AND fc.fecha_emision <= '2015-10-01'   
GROUP BY id_provincia
