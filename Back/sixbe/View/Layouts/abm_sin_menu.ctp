<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
        <META HTTP-EQUIV="Expires" CONTENT="-1">
        <title><?php echo Configure::read("appTitle"); ?></title>
        <meta name="description" content="">
        <meta name="author" content="">

        <?php

            //Favicon
            echo $this->Html->meta('favicon','/favicon.ico',array('type' => 'icon'));


            /*Bootstrap*/
            echo $this->Html->css('bootstrap/css/bootstrap.css');
            echo $this->Html->css('bootstrap/css/bootstrap-responsive.min.css');
            echo $this->Html->css('bootstrap/css/bootstrap-custom.css');
            //echo $this->Html->css('bootstrap/css/variables.less.css');
            //echo $this->Html->css('bootstrap/css/bootswatch.less.css');
            echo $this->Html->script('jquery/jquery-1.8.3.min.js');
            echo $this->Html->script('bootstrap/js/bootstrap.js');

            /*Fancybox*/
            echo $this->Html->script('fancybox/jquery.fancybox.js?v=2.1.3');
            echo $this->Html->css('fancybox/jquery.fancybox.css?v=2.1.2');

            echo $this->Html->css('fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5');
            echo $this->Html->script('fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5');

            echo $this->Html->css('fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7');
            echo $this->Html->script('fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7');

            echo $this->Html->script('fancybox/helpers/jquery.fancybox-media.js?v=1.0.5');

            /*Colorbox*/
            echo $this->Html->script('colorbox/jquery.colorbox.js');
            echo $this->Html->css('colorbox/colorbox.css');

            /*JqueryUI*/
            echo $this->Html->script('mylibs/jquery-ui-1.9.2.custom.min.js');
            echo $this->Html->css('jquery-ui-1.9.2.custom.css'); //jQuery UI, optional
            
            /*BlockUI*/
            echo $this->Html->script('mylibs/jquery.blockUI.js');

            /*JQuery MultiSelect*/
            echo $this->Html->css('jquery.multiselect.css');
            echo $this->Html->script('mylibs/jquery.multiselect.js');
            
            /*JQuery MultiSelectFilter*/
            echo $this->Html->css('jquery.multiselect.filter.css');
            echo $this->Html->script('mylibs/jquery.multiselect.filter.js');
            
            /*jQuery datepicker espaniol*/
            echo $this->Html->script('mylibs/jquery.ui.datepicker-es.js'); //jQuery datepicker espaniol
            echo $this->Html->script('mylibs/jquery-ui-timepicker-addon.js'); //jQuery timepicker

            /*MiniColors*/
            echo $this->Html->script('mylibs/jquery.minicolors.js');
            echo $this->Html->css('jquery.minicolors.css');
            
            /*uploadify: Uploader para archivos basados en flash y jQuery*/
           /* echo $this->Html->script('mylibs/jquery.uploadify.js');
            echo $this->Html->script('mylibs/jquery.uploadify.min.js');
            echo $this->Html->css('uploadify.css');   */
           
            /*uploadify: Uploader para archivos basados en flash y jQuery*/
            echo $this->Html->script('mylibs/jquery.uploadifive.js');
            //echo $this->Html->script('mylibs/jquery.uploadifive.min.js');
            echo $this->Html->css('uploadifive.css'); 
            

            /*Validator*/
            echo $this->Html->script('validator.js');
            
            /*Solo Numeros*/
            echo $this->Html->script('mylibs/numericInput.min.js');
            
            
            /*DataTables*/
            
            echo $this->Html->css('DataTables-1.9.4/media/css/jquery.dataTables.css');
            echo $this->Html->css('DataTables-1.9.4/media/css/jquery.dataTables_themeroller.css');
            
            echo $this->Html->script('DataTables-1.9.4/media/js/jquery.dataTables.js');
            
            /*DataTables TableTools*/
            echo $this->Html->script('DataTables-1.9.4/extras/TableTools/media/js/ZeroClipboard.js');
            echo $this->Html->script('DataTables-1.9.4/extras/TableTools/media/js/TableTools.js');
            echo $this->Html->script('DataTables-1.9.4/extras/TableTools/media/js/customApi.js');
            
            
            

            //echo $this->Html->script('gmaps_js/gmaps.js');

        ?>
        <!-- GMAPS -->
      <!--  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script> -->
        
        <?php //Inicio Scripts para menu multinivel ?>
        
        <script>
 
            /* bootstrap-dropdown.js v2.0.2
 
             * http://twitter.github.com/bootstrap/javascript.html#dropdowns
             * ============================================================
             * Copyright 2012 Twitter, Inc.
             *
             * Licensed under the Apache License, Version 2.0 (the "License");
             * you may not use this file except in compliance with the License.
             * You may obtain a copy of the License at
             *
             * http://www.apache.org/licenses/LICENSE-2.0
             *
             * Unless required by applicable law or agreed to in writing, software
             * distributed under the License is distributed on an "AS IS" BASIS,
             * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
             * See the License for the specific language governing permissions and
             * limitations under the License.
           */
     !function( $ ){
       "use strict"
      /* DROPDOWN CLASS DEFINITION
             * ========================= */
            var toggle = '[data-toggle="dropdown"]'
            , Dropdown = function ( element ) {
                var $el = $(element).on('click.dropdown.data-api', this.toggle)
                $('html').on('click.dropdown.data-api', function () {
                    $el.parent().removeClass('open')
                })
            }
            Dropdown.prototype = {
                constructor: Dropdown
                , toggle: function ( e ) {
                    var $this = $(this)
                    , selector = $this.attr('data-target')
                    , $parent
                    , isActive
 
                    if (!selector) {
                        selector = $this.attr('href')
                        selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
                    }
 
                    $parent = $(selector)
                    $parent.length || ($parent = $this.parent())
                    isActive = $parent.hasClass('open')
                    clearMenus()
                        !isActive && $parent.toggleClass('open')
                    return false
                }
            }
 
            function clearMenus() {
                $(toggle).parent().removeClass('open')
            }
            /* DROPDOWN PLUGIN DEFINITION
             * ========================== */
            $.fn.dropdown = function ( option ) {
                return this.each(function () {
                    var $this = $(this)
                    , data = $this.data('dropdown')
                    if (!data) $this.data('dropdown', (data = new Dropdown(this)))
                    if (typeof option == 'string') data[option].call($this)
                })
            }
            $.fn.dropdown.Constructor = Dropdown
            /* APPLY TO STANDARD DROPDOWN ELEMENTS
             * =================================== */
            $(function () {
                $('html').on('click.dropdown.data-api', clearMenus)
                $('body').on('click.dropdown.data-api', toggle, Dropdown.prototype.toggle)
            })
        }( window.jQuery );
        
        
        //Limpia un formulario
        function clearInputs(idForm){
                    $(':input','#'+idForm)
                      .not(":button, :submit, :reset, [name='_method']")
                      .val('')
                      .removeAttr('checked')
                      .removeAttr('selected');

        }
        
        //Muestra / oculta pestañas
        
        function showTab(idTab){
            $('#'+idTab).show();
            $('#'+idTab+'-link').show();
        }
        
        function hideTab(idTab){
            $('#'+idTab).hide();
            $('#'+idTab+'-link').hide();
        }
        
        function changeTitleForm(tituloModel, valor){
            
            $('#'+tituloModel+'-title-link').html(valor);
        }
        
        function hideLabel(idLabel){
            
            $('label[for="'+idLabel+'"]').hide();
            
        }
        
        function showLabel(idLabel){
            
            $('label[for="'+idLabel+'"]').show();
            
        }
        
        //Devuelve length para arrays asociativos
        function getLength(arr){
            
            var count = 0;
            for (var indice in arr)
                count++;
            
            return count;
        }
        
        </script>
         
        <script type='text/javascript'>//<![CDATA[
        $(window).load(function(){
            jQuery('.submenu').hover(function () {
                jQuery(this).children('ul').removeClass('submenu-hide').addClass('submenu-show');
            }, function () {
                jQuery(this).children('ul').removeClass('.submenu-show').addClass('submenu-hide');
            }).find("a:first").append(" &raquo; ");
        });//]]>
        </script>
         
        <style>
            .submenu-show
            {
                border-radius: 3px;
                display: block;
                left: 100%;
                margin-top: -25px !important;
                moz-border-radius: 3px;
                position: absolute;
                webkit-border-radius: 3px;
            }
            .submenu-hide
            {
                display: none !important;
                float: right;
                position: relative;
                top: auto;
            }
            .navbar .submenu-show:before
            {
                border-bottom: 7px solid transparent;
                border-left: none;
                border-right: 7px solid rgba(0, 0, 0, 0.2);
                border-top: 7px solid transparent;
                left: -7px;
                top: 10px;
            }
 
            .navbar .submenu-show:after
            {
                border-bottom: 6px solid transparent;
                border-left: none;
                border-right: 6px solid #fff;
                border-top: 6px solid transparent;
                left: 10px;
                left: -6px;
                top: 11px;
            }
        </style>
        
        <?php //Fin Scripts para menu multinivel ?>

    </head>

    <body>

      



        <!-- Begin of #container -->
        <div class="container">
            <div id="main-content" class="container">

                <?php echo $this->Session->flash(); ?>
                <?php echo $this->fetch('content'); ?>

            </div> <!--! end of #main-content -->
        </div>


        <div id="zk_proc" style="display: none;">
            <div class="z-loading" id="zk_proc-t">
                <div class="z-loading-indicator">
                    <span class="z-loading-icon"></span> Procesando...
                </div>
            </div>
        </div>
        
        
        <div id="alertSuccess" class="alert alert-success" style="display:none;position: absolute;top:55px;margin-left:10px;" align="center">
        </div>
        
        <div id="alertError" class="alert alert-danger" style="display:none;position: absolute;top:55px;margin-left:10px;" align="center">
        </div>



        <script type="text/javascript">
            $().ready(function() {

                    $.ajaxSetup({ cache: false });
                
                    //$('#datatable').dataTable();
                    $( ".datepicker" ).datepicker();
                    
                    //Deshabilita el click en los submenus
                    $('a.submenu-disable').unbind('click');
                    $('a.submenu-disable').click(function(e) {
                        e.stopPropagation();
                    });
                    
                    
                    $( document ).ajaxError(function( event, xhr,options,exc) {
                        if(xhr.status == 403){ //Forbidden
                            var msg = 'Url Origen: ' + document.URL + ' - Couldn\'t load ' + options.url + ' because (' + xhr.status + ' - ' + xhr.statusText + ') ';
                            //alert(msg);
                            //if(exc.message != undefined)
                                //msg += exc.message;
                            location.href = '<?php echo Router::url(array('controller' => 'Usuarios', 'action' => 'expired')); ?>?msg=' + encodeURIComponent(msg);
                        
                        }
                    });
                    

            });    

            function showProcessing(show){

                if(show)
                    $("#zk_proc").css("display","block"); 
                else
                    $("#zk_proc").css("display","none");

            }
            
            function showSuccess(msg){
                $('#alertSuccess').html(msg);
                $('#alertSuccess').fadeIn(1000, function() {
                // Animation complete
                     $('#alertSuccess').fadeOut(8000, function() {
                    // Animation complete
                    });
                });
            }
            
            function showError(msg){
                $('#alertError').html(msg);
                $('#alertError').fadeIn(1000, function() {
                // Animation complete
                     $('#alertError').fadeOut(8000, function() {
                    // Animation complete
                    });
                });
            }
            
        </script>

        <?php echo $this->Js->writeBuffer(); ?>

    </body>
</html>