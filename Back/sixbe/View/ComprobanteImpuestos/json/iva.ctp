<?php
        
         include(APP.'View'.DS.'ComprobanteImpuestos'.DS.'json'.DS.'comprobante_impuesto.ctp');  
        
        $model["abreviado_impuesto"]["show_grid"] = 1;
        $model["abreviado_impuesto"]["header_display"] = "Impuesto";
        $model["abreviado_impuesto"]["order"] = "0";
        $model["abreviado_impuesto"]["width"] = "10";
        
      
        $model["base_imponible"]["show_grid"] = 1;
        $model["base_imponible"]["header_display"] = "Base";
        $model["base_imponible"]["order"] = "1";
        $model["base_imponible"]["width"] = "11";
        
        
        $model["d_iva_detalle"]["show_grid"] = 1;
        $model["d_iva_detalle"]["header_display"] = "Alic(%)";
        $model["d_iva_detalle"]["order"] = "2";
        $model["d_iva_detalle"]["width"] = "8";
        
        $model["importe_impuesto"]["show_grid"] = 1;
        $model["importe_impuesto"]["header_display"] = "Importe";
        $model["importe_impuesto"]["order"] = "3";
        $model["importe_impuesto"]["width"] = "20";
        
       
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteImpuesto",
                    "content" => $model
                );
        echo json_encode($output);
?>