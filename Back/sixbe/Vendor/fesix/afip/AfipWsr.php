<?php


ini_set("soap.wsdl_cache_enabled", "0");
include_once LIB_PATH."ErrorLog.php";
class AfipWsr
{
    protected $wsdl;       // The WSDL corresponding to WSAA
    protected $url;        // The url of WebService

    protected $token;
    protected $sign;
    protected $cuit;

    protected $wsFilePath;

    protected $errLog;

    protected $client;

    protected $wsVersion = 'wsr';

    public function __Construct($cuit, $token, $sign)
    {
        $prmErr=null;
        if (!$cuit)
            $prmErr = "<li>AfipWsr::__Construct() - Se debe especificar el parametro cuit</li>";
        elseif (!$token)
            $prmErr .= "<li>AfipWsr::__Construct() - Se debe especificar el parametro token</li>";
        elseif (!$sign)
            $prmErr .= "<li>AfipWsr::__Construct() - Se debe especificar el parametro sign</li>";

        if ($prmErr)
            criticalExit($prmErr);


        $entorno = SERVER_ENTORNO;

        $this->token = $token;
        $this->sign  = $sign;
        $this->cuit  = $cuit;

        $this->wsFilePath = dirname(__FILE__).'/wsfiles/';
        

        if ($entorno == 'Test'){
            $this->url    = "https://awshomo.afip.gov.ar/sr-padron/webservices/personaServiceA5";
            $this->wsdl       = $this->wsFilePath."ws_sr_padron_a5.wsdl";
        }elseif ($entorno == 'Prod'){
            $this->url    = "https://aws.afip.gov.ar/sr-padron/webservices/personaServiceA5";
            $this->wsdl       = $this->wsFilePath."ws_sr_padron_a5-production.wsdl";
        }else
            criticalExit('AfipWsr::__Construct() - No se ha definido la constante SERVER_ENTORNO (Test - Prod)');

        $this->errLog = new ErrorLog();

        $this->client = new SoapClient( $this->wsdl, array(
                                'soap_version'   => SOAP_1_1,
                                'location'       => $this->url,
                                'trace'          => 1,
                                'exceptions'     => 0
                                ));

        file_put_contents(TMP_PATH."wsfr_functions.txt",print_r($this->client->__getFunctions(),TRUE));
        file_put_contents(TMP_PATH."wsfr_types.txt",print_r($this->client->__getTypes(),TRUE));


    }
    
    
    
    public function getPersona($cuit_identificador){
    	
    	
    	$params = array(
    			'token' 			=> $this->token,
    			'sign' 				=> $this->sign,
    			'cuitRepresentada' 	=> $this->cuit,
    			'idPersona' 		=> $cuit_identificador
    	);
    	
    	
    	$message = "";
    	$results = array();
    	
    	$results = $this->client->getPersona($params);
    	
    	
    	if (!$this->getErrors($results, 'getPersona') && $results && count($results)>0)
    	{
    		
    		//json_decode($results;
    	
    		
    		if( (isset($results->personaReturn->errorConstancia) && strlen($results->personaReturn->errorConstancia)>0)
    				
    				|| (isset($results->personaReturn->errorMonotributo) && strlen($results->personaReturn->errorMonotributo)>0)
    				
    				
    				|| (isset($results->personaReturn->errorRegimenGeneral) && strlen($results->personaReturn->errorRegimenGeneral)>0)
    						
    						
    				){
    			
    			$error = EnumError::ERROR;
    			
    			$message = '';
    			
    			
    			if( (isset($results->personaReturn->errorConstancia) && strlen($results->personaReturn->errorConstancia)>0))
    					$message = $results->personaReturn->errorConstancia;
    			
    					
    			if( (isset($results->personaReturn->errorMonotributo) && strlen($results->personaReturn->errorMonotributo)>0))
    				$message .= $results->personaReturn->errorMonotributo;
    			
    			if( (isset($results->personaReturn->errorRegimenGeneral) && strlen($results->personaReturn->errorRegimenGeneral)>0))
    				$message .= $results->personaReturn->errorRegimenGeneral;
    			
    		}else{
    			
    		
    			$error = EnumError::SUCCESS;
    			
    			
    		
    		 
    		
    		
    			
    			
    		}
    	
    	}else{
    		
    		$error_log = $this->getErrLog();
    		$error = EnumError::ERROR;
    		$message = $error_log[0];
    	}
    	
    	
    	$results->error_interno = $error;
    	$results->message_interno = $message;
    	
    	return $results;
    	
    	
    }
    
    


    private function getErrors($results, $method)
    {
        // Quitar comentarios para ver el log generado

        file_put_contents(TMP_PATH.'CUIT_'.$this->cuit."_WSR_request_".$method.".xml",$this->client->__getLastRequest());
        file_put_contents(TMP_PATH.'CUIT_'.$this->cuit."_WSR_response_".$method.".xml",$this->client->__getLastResponse());

        if(isset($results->personaReturn->errorConstancia) && count($results->personaReturn->errorConstancia)>0 ){
        	
        	
        	
        	if(is_array($results->personaReturn->errorConstancia->error)){
	        	foreach ($results->personaReturn->errorConstancia->error as $error){
	        		
	        		$errores = $error;
	        	}
        	}else{
        		
        		$errores = $results->personaReturn->errorConstancia->error;
        	}
        	
        	$this->errLog->add("Error:".$errores);
        	
        	return true;
        }
        
        
        if(isset($results->personaReturn->errorRegimenGeneral) && count($results->personaReturn->errorRegimenGeneral)>0 ){
        	
        	
        	$this->errLog->add("Error:".$results->personaReturn->errorRegimenGeneral->error);
        	
        	return true;
        }
        
        
        
        if(isset($results->personaReturn->errorMonotributo) && count($results->personaReturn->errorMonotributo)>0 ){
        	
        	
        	$this->errLog->add("Error:".$results->personaReturn->errorMonotributo->error);
        	
        	return true;
        }
        
        

        if (is_soap_fault($results))
        {
            $this->errLog->add("Conexion SOAP: <b>".$results->faultstring.".</b> [AFIP::WSR::".$method." - Error Code: ".$results->faultcode."]");
            return true;
        }

        if ($method == 'FEDummy')
            return false;

        $mr = $method.'Result';
        
        if (isset($results->$mr->Errors))
        {
            $errors = $results->$mr->Errors;
            if (is_array($errors))
            {
                foreach ($errors as $error)
                    $this->errLog->add("WebService AFIP::WSR::".$method." <b>".$error->Err->Msg.".</b> [Codigo de error: ".$error->Err->Code."]");
            }
            else
            {
                $this->errLog->add("WebService AFIP::WSR::".$method." <b>".$errors->Err->Msg.".</b> [Codigo de error: ".$errors->Err->Code."]");
            }
            return true;
        }
        return false;
    }

    /**
     * Devuelve un array con los errores registrados mediante $this->errlog->add()
     */
    public function getErrLog()
    {
        return $this->errLog->get();
    }

    private function resultToArray($obj)
    {
        if (!is_object($obj))
        {
            $arr = $obj;
        }
        else
        {
            $arr = get_object_vars($obj);
            if (is_array($arr))
            {
                foreach ($arr as $k => $v)
                {
                    if (is_object($v))
                        $arr[$k] = $this->resultToArray($v);
                    else
                        $arr[$k] = $v;
                }
            }
        }
        return $arr;
    }
}
?>
