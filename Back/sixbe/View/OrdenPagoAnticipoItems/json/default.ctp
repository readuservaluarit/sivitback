<?php   
        include(APP.'View'.DS.'ComprobanteItemComprobantes'.DS.'json'.DS.'default.ctp');
        
      
        
        $model["d_tipo_comprobante_origen"]["show_grid"] = 1;
        $model["d_tipo_comprobante_origen"]["header_display"] = "Tipo";
        $model["d_tipo_comprobante_origen"]["order"] = "2";
		$model["d_tipo_comprobante_origen"]["text_colour"] = "#000000";
		$model["d_tipo_comprobante_origen"]["width"] = "6";
		$model["d_tipo_comprobante_origen"]["read_only"] = "1";
		
        $model["pto_nro_comprobante_origen"]["show_grid"] = 1;
        $model["pto_nro_comprobante_origen"]["header_display"] = "Nro.";
        $model["pto_nro_comprobante_origen"]["order"] = "3";
		$model["pto_nro_comprobante_origen"]["text_colour"] = "#000000";
		$model["pto_nro_comprobante_origen"]["width"] = "12";
		$model["pto_nro_comprobante_origen"]["read_only"] = "1";	
        
        $model["fecha_generacion_origen"]["show_grid"] = 1;
        $model["fecha_generacion_origen"]["header_display"] = "Fcha. Gen.";
        $model["fecha_generacion_origen"]["order"] = "4";
		$model["fecha_generacion_origen"]["read_only"] = "1";
		$model["fecha_generacion_origen"]["width"] = "4";
		$model["fecha_generacion_origen"]["text_colour"] = "#0000FF";
        
        $model["item_observacion"]["show_grid"] = 1;
        $model["item_observacion"]["header_display"] = "Observ.";
        $model["item_observacion"]["order"] = "5";
        $model["item_observacion"]["read_only"] = "0";
		$model["item_observacion"]["width"] = "5";
		$model["item_observacion"]["text_colour"] = "#0000AA";
		
		$model["total_comprobante_origen"]["show_grid"] = 1;
        $model["total_comprobante_origen"]["header_display"] = "Total Origen.";
        $model["total_comprobante_origen"]["order"] = "6";
        $model["total_comprobante_origen"]["read_only"] = "1";
		$model["total_comprobante_origen"]["width"] = "3";
		$model["total_comprobante_origen"]["text_colour"] = "#F0000F";
		$model["total_comprobante_origen"]["type"] = "float";
				
		$output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItemComprobante",
                    "content" => $model
                );
        echo json_encode($output);
?>