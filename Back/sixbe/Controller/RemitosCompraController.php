<?php
App::uses('ComprobantesController', 'Controller');



class RemitosCompraController extends ComprobantesController {
    
    public $name = 'RemitosCompra';
    public $model = 'Comprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $id_tipo_comprobante = EnumTipoComprobante::RemitoCompra;
    public $requiere_impuestos = 0;
    
    /**
    * @secured(CONSULTA_REMITO_COMPRA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros($this->model);
            
        
		$this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>array('Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante','DepositoOrigen','DepositoDestino','Transportista','Usuario'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.id desc'
        ); //el contacto es la sucursal
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
		$formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        //Headers
        $formBuilder->addHeader('id', 'REMITO.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'REMITO.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
            
             //$valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
             //unset($valor['ComprobanteItem']);
             
             if(isset($valor['TipoComprobante'])){
                 $valor[$this->model]['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                 $valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
                 unset($valor['TipoComprobante']);
             }
             
             $valor[$this->model]['pto_nro_comprobante'] =  (string)  $this->Comprobante->GetNumberComprobante($valor[$this->model]['d_punto_venta'],str_pad($valor[$this->model]['nro_comprobante'], 8, "0", STR_PAD_LEFT));
            
            /*
             foreach($valor[$this->model]['ComprobanteItem'] as &$producto ){
                 
                 $producto["d_producto"] = $producto["Producto"]["d_producto"];
                 $producto["codigo_producto"] = $producto["Producto"]["codigo"];
                 unset($producto["Producto"]);
             }
             */
           
             if(isset($valor['Moneda'])){
                 $valor[$this->model]['d_moneda'] = $valor['Moneda']['d_moneda'];
                 unset($valor['Moneda']);
             }
             
             if(isset($valor['EstadoComprobante'])){
                $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
                unset($valor['EstadoComprobante']);
             }
             if(isset($valor['CondicionPago'])){
                $valor[$this->model]['d_condicion_pago'] = $valor['CondicionPago']['d_condicion_pago'];
                unset($valor['CondicionPago']);
             }
             
             if(isset($valor['DepositoOrigen'])){
                $valor[$this->model]['id_deposito_origen'] = $valor['DepositoOrigen']['id'];
                $valor[$this->model]['d_deposito_origen'] = $valor['DepositoOrigen']['d_deposito'];
                unset($valor['DepositoOrigen']);
             }
             
              if(isset($valor['DepositoDestino'])){
                $valor[$this->model]['id_deposito_destino'] = $valor['DepositoDestino']['id'];
                $valor[$this->model]['d_deposito_destino'] = $valor['DepositoDestino']['d_deposito'];
                unset($valor['DepositoDestino']);
             }
             
             if(isset($valor['Transportista'])){
                $valor[$this->model]['t_razon_social'] = $valor['Transportista']['razon_social'];
                $valor[$this->model]['t_direccion'] = $valor['Transportista']['calle'].' '.$valor['Transportista']['numero_calle'];
                unset($valor['Transportista']);
             }
             
             
             
             if(isset($valor['Persona'])){
            
            
                 if($valor['Persona']['id']== null){
                     
                    $valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social']; 
                 }else{
                     
                     $valor[$this->model]['razon_social'] = $valor['Persona']['razon_social'];
                     $valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
                 }
                 
                 unset($valor['Persona']);
             }
             
                if(isset($valor['DepositoOrigen'])){
                  $valor['Comprobante']['d_deposito_origen'] =$valor['DepositoOrigen']['d_deposito_origen'];
              }
                 
              if(isset($valor['DepositoDestino'])){
                    $valor['Comprobante']['d_deposito_destino'] =$valor['DepositoDestino']['d_deposito_destino'];
               }
               
             
             if(isset($valor['Usuario'])){
               	
               	$valor['Comprobante']['d_usuario'] = $valor['Usuario']['nombre'];
               }
               
               
      
            
        }
        
        
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
   

    /**
    * @secured(ADD_REMITO_COMPRA)
    */
    public function add(){
   
     parent::add();    
        
    }
    
    
    /**
    * @secured(MODIFICACION_REMITO_COMPRA)
    */
    public function edit($id){
     parent::edit($id); 
     return;   
        
    }
    
    
    /**
    * @secured(BAJA_REMITO_COMPRA)
    */
    public function delete($id){
        
     parent::delete($id);    
        
    }
    
   /**
    * @secured(BAJA_REMITO_COMPRA)
    */
    public function deleteItem($id,$externo=1){
        
        parent::deleteItem($id,$externo);    
        
    }
    
    
      /**
    * @secured(CONSULTA_REMITO_COMPRA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
  
    /**
    * @secured(REPORTE_REMITO_COMPRA)
    */
    public function pdfExport($id,$vista=''){
        
        
        
        
        
    
        
        
        
        
        
    $this->loadModel("Comprobante");
    $remito = $this->Comprobante->find('first', array(
                                                       
                                                        'conditions' => array('Comprobante.id' => $id),
                                                        'contain' =>array('TipoComprobante','Persona')
                                                        ));
                                                        
                                                        
    
     
     
    
     
                                                        
                                                        
        
        
        
        if($remito){
            $comprobantes_relacionados = $this->getComprobantesRelacionados($id);
         
                                           
          
            
            
              
            $facturas_relacionados = array();
            $orden_compra_relacionados = array();


            foreach($comprobantes_relacionados as $comprobante){


             if(  isset($comprobante["Comprobante"]["id_tipo_comprobante"]) && in_array($comprobante["Comprobante"]["id_tipo_comprobante"],array(EnumTipoComprobante::FACTURASACOMPRA,EnumTipoComprobante::FACTURASBCOMPRA,EnumTipoComprobante::FACTURASCCOMPRA,EnumTipoComprobante::ReciboACompra,EnumTipoComprobante::ReciboBCompra,EnumTipoComprobante::ReciboCCompra,EnumTipoComprobante::ReciboMCompra) ) )
                array_push($facturas_relacionados,$this->Comprobante->GetNumberComprobante($comprobante["PuntoVenta"]["numero"],$comprobante["Comprobante"]["nro_comprobante"]));
            
            if(isset($comprobante["Comprobante"]["id_tipo_comprobante"]) && $comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::OrdenCompra)
                        array_push($orden_compra_relacionados,$this->Comprobante->GetNumberComprobante($comprobante["PuntoVenta"]["numero"],$comprobante["Comprobante"]["nro_comprobante"]));


            }
           
            
            
            
            $this->set('facturas_relacionados',$facturas_relacionados);  
            $this->set('orden_compra_relacionados',$orden_compra_relacionados);     
            $this->set('datos_proveedor',$remito['Persona']);     
             
            parent::pdfExport($id,$vista);  
         }
            
       
     
        
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
        
        
      
 
    }
    
    
     /**
    * @secured(CONSULTA_REMITO_COMPRA)
    */
    public function existe_comprobante() {
  
        parent::existe_comprobante();
        
        
        
    }
    
    
    
    /**
     * @secured(CONSULTA_REMITO_COMPRA)
     */
    public function Anular($id_comprobante){
    	
    	$this->loadModel("Comprobante");
    	$this->anulo = 1;
    	
    	
    	$remito_compra = $this->Comprobante->find('first', array(
    			'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
    			'contain' => array()
    	));
    	
    	$ds = $this->Comprobante->getdatasource(); 
    	
    	if( $remito_compra && $remito_compra["Comprobante"]["id_estado_comprobante"]!= EnumEstadoComprobante::Anulado ){
    		
    		try{
    			$ds->begin();
    			
    			/*Chequeo sino tiene IRM */
    			$comprobantes_relacionados = $this->getComprobantesRelacionados($id_comprobante);
    			
    			$tipo_comprobante_factura = $this->Comprobante->getTiposComprobanteController(EnumController::FacturasCompra);
    			
    			$puede_anular = 1;
    			if($comprobantes_relacionados){
	    			foreach($comprobantes_relacionados as $comprobante){
	    				
	    				
	    				if( isset($comprobante["Comprobante"]) && 
	    						
	    				  
	    						($comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::InformeRecepcionMateriales &&  $comprobante["Comprobante"]["id_estado_comprobante"] !=EnumEstadoComprobante::Anulado)
	    						
	    						||
	    						
	    						
	    						( in_array($comprobante["Comprobante"]["id_tipo_comprobante"],$tipo_comprobante_factura) &&  $comprobante["Comprobante"]["id_estado_comprobante"] !=EnumEstadoComprobante::Anulado)
	    						
	    						
	    						
	    				){
	    					$puede_anular = 0;
	    					
	    				}
	    					
	    				
	    						
	    			}
    			}
    			
    			
    			if($puede_anular == 1){
    				
    				$status = EnumError::SUCCESS;
    				$messagge = "El Remito de Compra se anul&oacute; correctamente";
    				
    				$this->Comprobante->updateAll(
    						array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
    						array('Comprobante.id' => $id_comprobante) );  
    				
    				
    				
    				
    				$remito_compra = $this->Comprobante->find('first', array(
    						'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
    						'contain' => false
    				));
    				
    				$this->Auditar($id_comprobante,$remito_compra);
    				
    				
    				$ds->commit();
    				
    				
    			}else{
    				$ds->rollback();
    				$status = EnumError::ERROR;
    				$messagge = "El remito de compra tiene asignado uno o mas IRM o se cargaron Facturas relacionadas con este Remito, debe anular estos para poder continuar con esta anulaci&oacute;n ";
    			}
    			
    		}catch(Exception $e){
    			$ds->rollback();
    			$status = EnumError::ERROR;
    			$messagge = $e->getMessage();
    			
    		}
    	}else{
    		$ds->rollback();
    		$status = EnumError::ERROR;
    		$messagge = "El remito de compra ya se encuenta Anulado";
    		
    	}
    	
    	
    	$output = array(
    			"status" => $status,
    			"message" => $messagge,
    			"content" => ""
    	);
    	echo json_encode($output);
    	die();
    	
    	
    }
      /**
        * @secured(CONSULTA_REMITO_COMPRA)
        */
        public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
        {
            parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
        }
    
        
        /**
         * @secured(BTN_EXCEL_REMITOSCOMPRA)
         */
        public function excelExport($vista="default",$metodo="index",$titulo=""){
        	
        	parent::excelExport($vista,$metodo,"Listado de Remitos de Compra");
        	
        	
        	
        }
    
    
    
}
?>