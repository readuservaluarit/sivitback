<?php
App::uses('AppModel', 'Model');

/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class PuntoVenta extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'punto_venta';

    public $actsAs = array('Containable');
    

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
        'numero' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Debe ingresar un n&uacute;mero',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            
            
        ),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
   
   public $belongsTo = array(
        'DatoEmpresa' => array(
                'className' => 'DatoEmpresa',
                'foreignKey' => 'id_dato_empresa',
                'conditions' => '',
                'fields' => '',
                'order' => ''
            ),
        'Pais' => array(
            'className' => 'Pais',
            'foreignKey' => 'id_pais',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Provincia' => array(
            'className' => 'Provincia',
            'foreignKey' => 'id_provincia',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Moneda' => array(
            'className' => 'Moneda',
            'foreignKey' => 'id_moneda',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
        
   );   
   
   public $hasMany = array(
        'ComprobantePuntoVentaNumero' => array(
            'className' => 'ComprobantePuntoVentaNumero',
            'foreignKey' => 'id_punto_venta',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        ); 
   
   
   public function EsFacturaElectronica($id_punto_venta){
       
       $punto_venta = $this->find('first', array(
                                                'conditions' => array('PuntoVenta.id' => $id_punto_venta)
                                                //'contain' =>array('Comprobante')
                                                ));
                                                
       return $punto_venta["PuntoVenta"]["factura_electronica"];
   } 
}
