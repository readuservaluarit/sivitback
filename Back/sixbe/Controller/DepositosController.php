<?php

class DepositosController extends AppController {
    public $name = 'Depositos';
    public $model = 'Deposito';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    //public $actsAs = array('Contanaible');
    
/**
* @secured(CONSULTA_DEPOSITO)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
        $nombre = strtolower($this->getFromRequestOrSession('Deposito.d_deposito'));
        $id = strtolower($this->getFromRequestOrSession('Deposito.id'));
        $id_deposito = strtolower($this->getFromRequestOrSession('Deposito.id_deposito'));
        $tree = strtolower($this->getFromRequestOrSession('Deposito.tree'));//para que muestre estilo arbol
        $id_deposito_categoria = $this->getFromRequestOrSession('Deposito.id_deposito_categoria');
        $id_persona= $this->getFromRequestOrSession('Deposito.id_persona');
        $activo = $this->getFromRequestOrSession('Deposito.activo');
        
        
        
        
          
          
          
        $conditions = array(); 
        
       // array_push($conditions, array('Deposito.id_deposito'=>null));
        if ($nombre != "" && $id == "" && $id_deposito == "") {
            array_push($conditions, array('LOWER(Deposito.d_deposito) LIKE' => '%' . $nombre . '%'));
        }
        
        if ($id != "") {
             array_push($conditions, array('Deposito.id' => $id));
        }
        
        if ($activo!= "") {
        	array_push($conditions, array('Deposito.activo' => $activo));
        }
        
        if ($id_deposito != "") {
             array_push($conditions, array('Deposito.id' =>  $id_deposito ));
        }
        
        if ($id_deposito_categoria!= "") {
        	array_push($conditions, array('Deposito.id_deposito_categoria' =>  $id_deposito_categoria));
        }
        
        if ($id_persona!= "") {
        	array_push($conditions, array('Deposito.id_deposito_categoria' =>  $id_persona));
        }

        
        
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' => array('Persona','DepositoParent','DatoEmpresa','DepositoCategoria'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
       
        
    
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de Bancos', 'Datos de las Bancos', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_DEPOSITO', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'Banco.id', "10%");
                $formBuilder->addHeader('Nombre', 'Banco.d_DEPOSITO', "50%");
                $formBuilder->addHeader('Nombre', 'Banco.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_DEPOSITO');
                $formBuilder->addField($this->model, 'cotizacion');
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        
            $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
            $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        
            if($tree == 1)
             $data = $this->Deposito->find('threaded', array(
                                //'conditions' => array('article_id' => 50)
                                ));

        
        
        foreach($data as &$deposito){
            
            
            if(isset( $deposito["Persona"])){
                $deposito["Deposito"]["d_persona"] = $deposito["Persona"]["razon_social"];
                
                unset($deposito["Persona"]);
            }
            
            
            if(isset($deposito["DepositoParent"]))
                   $deposito["Deposito"]["d_deposito_padre"] = $deposito["DepositoParent"]["d_deposito"];
                   
             unset($deposito["DepositoParent"]);     
             
            if(isset($deposito["DatoEmpresa"]))
                   $deposito["Deposito"]["d_dato_empresa"] = $deposito["DatoEmpresa"]["d_dato_empresa"];   
                   
             unset($deposito["DatoEmpresa"]);    
             
             
             if(isset($deposito["DepositoCategoria"]))
                   $deposito["Deposito"]["d_deposito_categoria"] = $deposito["DepositoCategoria"]["d_deposito_categoria"];
                   
             unset($deposito["DepositoCategoria"]);       
          
            
            if($deposito["Deposito"]["parent_id"]!='')
                $deposito["Deposito"]["sub_deposito"] = "Si";
            else    
                $deposito["Deposito"]["sub_deposito"] = "No";
            
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->Banco->id = $id;
            $this->request->data = $this->Banco->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Banco');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Banco');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Banco');
        else
            $formBuilder->setTituloForm('Consulta de Banco');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_DEPOSITO', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('cotizacion', 'Cotizaci&oacute;n', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));    
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'BancoAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('BancoDBanco', 'Debe ingresar un nombre');
        validator.validateRequired('BancoCotizacion', 'Debe ingresar una cotizaci&oacute;n');


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_DEPOSITO)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_cat = '';
            
              $ds = $this->Deposito->getdatasource();
            try{
                $ds->begin();
                if ($this->Deposito->saveAll($this->request->data)){
                    
                    
                    $mensaje = "El Deposito ha sido creado exitosamente";
                    $tipo = EnumError::SUCCESS;
                    $id_cat = $this->Deposito->id;
                    $this->CreateStock($id_cat);
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
              $ds->commit();
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el Deposito no ha podido ser creada.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" =>$id_cat
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content" ,"id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }

    /**
    * @secured(MODIFICACION_DEPOSITO)
    */
     
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->loadModel(EnumModel::StockProducto);
            $this->{$this->model}->id = $id;
            
            try{ 
            	
            	
            	
            	/*si desactiva el deposito realizo este chequeo*/
            	if($this->request->data["Deposito"]["activo"] == 0){
            		
            		
            		
            		
            		$stock_producto = $this->StockProducto->find('count', array(
            				'conditions' => array(
            						'StockProducto.id_deposito' => $id,'StockProducto.stock !='=>0
            				) ,
            				'contain' => false
            		));
            		
            		
            		if($stock_producto != 0){
            			
            			throw new Exception(".El deposito no se puede desactivar ya que posee productos con Stock distinto de cero.");
            			$status =    EnumError::ERROR;
            			
            		}
            		
            	}
            	
            	
            	
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "El Deposito ha sido modificado exitosamente";
                     $status = EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
                    
               }
               
              
            }catch(Exception $e){
                //$this->Session->setFlash('Ha ocurrido un error, el Deposito no ha podido modificarse.', 'error');
                $mensaje =  "Ha ocurrido un error, el Deposito NO ha podido modificarse".$e->getMessage();
                $status =    "error";
                
            }
            
            
             if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            //$this->{$this->model}->contain('Provincia','Pais');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }

    /**
    * @secured(BAJA_DEPOSITO,READONLY_PROTECTED)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $this->loadModel("StockProducto");
        $mensaje = "";
        $status = "";
        $this->Deposito->id = $id;
        
        $ds = $this->Deposito->getdatasource();
            
             
     
       try{
           
               $ds->begin();
               
               
               
               $stock_producto = $this->StockProducto->find('count', array(
               		'conditions' => array(
               				'StockProducto.id_deposito' => $id,'StockProducto.stock !='=>0
               		) ,
               		'contain' => false
               ));
               
               
               
               
               $this->loadModel("StockProducto");
               
               
               if($stock_producto == 0){
               
               
		         $this->StockProducto->deleteAll(array("StockProducto.id_deposito"=>$id));
		               
		               
		               
		               
		        if ($this->Deposito->delete() ) {
		                
		                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
		                
		                $status = EnumError::SUCCESS;
		                $mensaje = "El Dep&oacute;osito ha sido eliminada exitosamente.";
		               
		                
		                
		                
		            $ds->commit();    
		       }else{
		                 throw new Exception("No es Posible eliminar el dep&oacute;sito");
		                 $ds->rollback();
		            }
		            
            
            
               }else{
               	
               	$status = EnumError::ERROR;
               	$mensaje = "No es posible borrar el dep&oacute;sito ya que existen art&iacute;culos que poseen stock distinto de cero.";
               }
          }catch(Exception $ex){ 
              
              $ds->rollback();
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
           
            
            
        }
        
        
        
        
        if($this->RequestHandler->ext == 'json'){
        	$output = array(
        			"status" => $status,
        			"message" => $mensaje,
        			"content" => ""
        	); 
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }
    
    
    /**
    * @secured(CONSULTA_DEPOSITO)
    */
     public function getModel($vista = 'default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER

            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }

        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
            $this->set('model',$model);
            // $this->set('this',$this);
            Configure::write('debug',0);
            $this->render($vista);
        }
    
    
    
    
    private function CreateStock($id){
        
        
        
        //ejecuta trigger
        return true;
        
        
        
        
        
        
    }
    
     
    



}
?>