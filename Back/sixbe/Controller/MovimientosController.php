<?php

/**
* @secured(CONSULTA_MOVIMIENTO)
*/
class MovimientosController extends AppController {
    public $name = 'Movimientos';
    public $model = 'Movimiento';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
  /**
  * @secured(CONSULTA_MOVIMIENTO)
  */
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
        
        $fecha_desde = $this->getFromRequestOrSession('Movimiento.fecha_inicio');
        $hora_desde = $this->getFromRequestOrSession('Movimiento.hora_inicio');//HH
        $fecha_hasta = $this->getFromRequestOrSession('Movimiento.fecha_fin');
        $hora_hasta = $this->getFromRequestOrSession('Movimiento.hora_fin');//HH
        $id_deposito_origen = $this->getFromRequestOrSession('Movimiento.id_deposito_origen');//HH
        $id_deposito_destino = $this->getFromRequestOrSession('Movimiento.id_deposito_destino');//HH
        
        
        $id_movimiento_tipo = $this->getFromRequestOrSession('Movimiento.id_movimiento_tipo');
        $id_producto = $this->getFromRequestOrSession('Movimiento.id_producto');

   
        $id_usuario = $this->getFromRequestOrSession('Movimiento.id_usuario');
        $id_comprobante = $this->getFromRequestOrSession('Movimiento.id_comprobante');
        $id_lote = $this->getFromRequestOrSession('Movimiento.id_lote');
        $lote_codigo = $this->getFromRequestOrSession('Movimiento.lote_codigo');
   
        
        $nro_comprobante = str_replace(",","", $this->getFromRequestOrSession('Movimiento.nro_comprobante'));
        
        $conditions = array(); 
        
        if($fecha_desde!=""){
            array_push($conditions, array('Movimiento.fecha >= ' => $hora_desde )); 
            array_push($this->array_filter_names,array("Fecha Dsd.:"=>$hora_desde));
        }
            
            
            
            
       if($fecha_hasta!=""){
            array_push($conditions, array('Movimiento.fecha <= ' => $fecha_hasta.' '.$hora_hasta ));
            array_push($this->array_filter_names,array("Fecha Hta.:"=>$hora_hasta));
       }
       
            
            
       /*     
       if($hora_desde!="")
       		array_push($conditions, array('DATE_FORMAT(Movimiento.fecha, "%H:%I" ) >='=>$hora_desde));
            	

       
       	if($hora_hasta!="")
       		array_push($conditions, array('DATE_FORMAT(Movimiento.fecha, "%H:%I" ) <='=>$hora_hasta));
       
        */    
            
        
        if($id_movimiento_tipo!=""){
            array_push($conditions, array('Movimiento.id_movimiento_tipo ' => $id_movimiento_tipo )); 
            array_push($this->array_filter_names,array("Tipo Movimiento.:"=>$id_movimiento_tipo));
        
        }
            
 
        
        if($id_producto!="")
            array_push($conditions, array('Movimiento.id_producto ' => $id_producto ));  
        
            
            
        if($id_deposito_origen !=""){
        	$this->loadModel(EnumModel::Deposito);
        	array_push($conditions, array('Movimiento.id_deposito_origen ' => $id_deposito_origen));  
        	array_push($this->array_filter_names,array("Dep. Origen.:"=>$this->Deposito->get($id_deposito_origen)["Deposito"]["d_deposito"]));
        }
        
        	
       if($id_deposito_destino !=""){
       	
       	$this->loadModel(EnumModel::Deposito);
       	array_push($conditions, array('Movimiento.id_deposito_destino ' => $id_deposito_destino));  
       	array_push($this->array_filter_names,array("Dep. Destino.:"=>$this->Deposito->get($id_deposito_destino)["Deposito"]["d_deposito"]));
       	
       }
            
     
 
        if($id_usuario!="")
            array_push($conditions, array('Movimiento.id_usuario' => $id_usuario ));   
        
        
            
        if($id_lote!="")
        	array_push($conditions, array('Movimiento.id_lote' => $id_lote));   
        
            
       if($id_comprobante!=""){

            	array_push($conditions, array('Movimiento.id_comprobante' => $id_comprobante));  
       }
       
       
       if($nro_comprobante!="" && $nro_comprobante!=","){

       	array_push($conditions, array('Comprobante.nro_comprobante' => $nro_comprobante));  
       }
       
       
       if($lote_codigo!="" && $id_lote == "")
       	array_push($conditions, array('Lote.codigo LIKE ' => '%'. $lote_codigo. '%'));
       	
            
            
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' => array('MovimientoTipo','Comprobante'=>array("PuntoVenta","TipoComprobante"),'Usuario','MateriaPrima','Producto','Componente','ComprobanteItem'=>array('Producto'),"DepositoOrigen","DepositoDestino","Lote"),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => 'Movimiento.id desc'
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de Movimiento', 'Datos de los Movimientos', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_MovimientoCausa', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'MovimientoCausa.id', "10%");
                $formBuilder->addHeader('Nombre', 'MovimientoCausa.d_movimiento_causa', "50%");
                $formBuilder->addHeader('Nombre', 'MovimientoCausa.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_MovimientoCausa');
                $formBuilder->addField($this->model, 'cotizacion');
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        	
        $this->loadModel("Comprobante");
        	
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
            
            $valor["Movimiento"]["tipo_movimiento"] = $valor["MovimientoTipo"]["d_movimiento_tipo"];
            
            
            if(isset($valor["Comprobante"]['PuntoVenta']["numero"]) && $valor["Comprobante"]['PuntoVenta']["numero"]>0){
            	$valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor["Comprobante"]['PuntoVenta']['numero'],$valor["Comprobante"]['nro_comprobante']);
            	unset($valor["Comprobante"]['PuntoVenta']);
            }else{
            	
            	$valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor["Comprobante"]['d_punto_venta'],$valor["Comprobante"]['nro_comprobante']);
            }
            
            
            if(isset($valor["Comprobante"]['TipoComprobante']))
            	$valor[$this->model]['codigo_tipo_comprobante'] = $valor["Comprobante"]['TipoComprobante']["codigo_tipo_comprobante"];

            
            	if(isset($valor["DepositoOrigen"]) && $valor["DepositoOrigen"]["id"]>0)
            		$valor[$this->model]['d_deposito_origen'] = $valor["DepositoOrigen"]['d_deposito'];
           
            if(isset($valor["DepositoDestino"]) && $valor["DepositoDestino"]["id"]>0)
            	$valor[$this->model]['d_deposito_destino'] = $valor["DepositoDestino"]['d_deposito'];
           
            		
            		
            $valor["Movimiento"]["d_usuario_nombre"] = $valor["Usuario"]["nombre"];
            
			$valor["Movimiento"]["fecha"] = $this->{$this->model}->formatDateTime($valor["Movimiento"]["fecha"]);
            
            if(isset($valor["Producto"]))
                $valor["Movimiento"]["d_producto"] = $valor["Producto"]["d_producto"];
            
            if(isset($valor["MateriaPrima"]))
                $valor["Movimiento"]["d_materia_prima"] = $valor["MateriaPrima"]["d_producto"];
            
            
            if(isset($valor["Componente"]))
                $valor["Movimiento"]["d_componente"] = $valor["Componente"]["d_producto"];
            
                
            if(isset($valor["ComprobanteItem"]["Producto"]))
                	$valor["Movimiento"]["producto_codigo"] = $valor["ComprobanteItem"]["Producto"]["codigo"];
             else
                $valor["Movimiento"]["producto_codigo"] = $valor["Producto"]["codigo"];
             
            
            if(isset($valor["Lote"]))
                	$valor["Movimiento"]["lote_codigo"] = $valor["Lote"]["codigo"];
                	
            
            
            unset($valor["MovimientoTipo"]);
            unset($valor["Comprobante"]);
			unset($valor["Usuario"]);
        }
        
        
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    

  

    /**
    * @secured(ADD_MOVIMIENTO)
    */
    public function add($id_movimiento_padre = 0) {
        App::uses('EnumTipoMoviento', 'Lib/Enums');
        if ($this->request->is('post') || $this->request->is('get')){
            $this->loadModel("Movimiento");
            
            $this->request->data["Movimiento"]["fecha"] = date("Y-m-d H:i:s"); 
            $this->request->data["Movimiento"]["id_usuario"]= $this->Auth->user('id'); 
            $id_usuario = $this->Auth->user('id'); 
            
            if($id_movimiento_padre !=0 )
                    $this->request->data["Movimiento"]["id_movimiento_padre"] = $id_movimiento_padre; //se usa para revertir un movimiento
                
            $id_movimiento = '';
            
            
            if(isset($this->request->data["Movimiento"]["id_comprobante"]))
                $id_comprobante = $this->request->data["Movimiento"]["id_comprobante"];
            else
                $id_comprobante = '';
            
            
            if($this->existeMovimientoAnteriorPorComprobante($id_comprobante,$this->request->data["Movimiento"]["id_movimiento_tipo"]) == 0){
            
                    $ds = $this->Movimiento->getdatasource();
                    try{
                        $ds->begin();
                     
                     
                     $this->request->data2 = $this->request->data;
                     
                     
                     if(isset($this->request->data2["Movimiento"]["id_deposito_origen"]) && $this->request->data2["Movimiento"]["id_deposito_origen"] == 0)
                        unset($this->request->data2["Movimiento"]["id_deposito_origen"]); 
                     
                     
                      if(isset($this->request->data2["Movimiento"]["id_deposito_destino"]) && $this->request->data2["Movimiento"]["id_deposito_destino"] == 0)
                        unset($this->request->data2["Movimiento"]["id_deposito_destino"]);   
                          
                        
                      if ($this->Movimiento->saveAll($this->request->data2, array('deep' => false))){
                            
                          $mensaje = "El Movimiento ha sido creado exitosamente";
                          $tipo = EnumError::SUCCESS;
                          $id_movimiento = $this->Movimiento->id;
                          
                          if( isset($this->request->data["Movimiento"]["cantidad"]))
                            $cantidad = $this->request->data["Movimiento"]["cantidad"]; 
                          
                          
                        switch($this->request->data["Movimiento"]["id_movimiento_tipo"]){
                            
                            
                            
					        case EnumTipoMovimiento::IngresoMateriaPrima:
                            case EnumTipoMovimiento::EgresoMateriaPrima:
                            case EnumTipoMovimiento::RecepcionComponentes:
                            case EnumTipoMovimiento::EgresoComponente:
                            case EnumTipoMovimiento::EgresoComponenteScrap:
                            case EnumTipoMovimiento::RecepcionProducto:
                            case EnumTipoMovimiento::DeteccionProductoNc:
                            case EnumTipoMovimiento::ReprocesoProducto:
                            case EnumTipoMovimiento::ProductoScrap:
                            case EnumTipoMovimiento::EgresoProductoScrap:
                            case EnumTipoMovimiento::DevolucionMateriaPrimaAlProveedor:
                            case EnumTipoMovimiento::IngresoMateriaPrimaNoConforme:
                            case EnumTipoMovimiento::ReprocesoComponente:
                            case EnumTipoMovimiento::ComponenteScrap:
                            case EnumTipoMovimiento::AjusteStock:
                            case EnumTipoMovimiento::AjusteStockNegativo:
                            case EnumTipoMovimiento::AjusteStockComprometidoNegativo:
                            case EnumTipoMovimiento::AjusteStockComprometidoPositivo:
                            case EnumTipoMovimiento::Ajuste_MP_ComprometidoPositivo1551:
                            case EnumTipoMovimiento::Ajuste_MP_ComprometidoNegativo1552:
                            case EnumTipoMovimiento::Ajuste_C_ComprometidoPositivo1601:
                            case EnumTipoMovimiento::Ajuste_C_ComprometidoNegativo1602:
                            case EnumTipoMovimiento::Ajuste_P_ComprometidoPositivo1301:
                            case EnumTipoMovimiento::Ajuste_P_ComprometidoNegativo1302:
                             
                             
                             
                                    //Egreso de Materia Prima
                                    $this->loadModel("StockArticulo");
                                    $this->loadModel("Articulo");
                                    $id_producto = $this->request->data["Movimiento"]["id_producto"];
                                    $id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
                                    $id_deposito_origen = $this->request->data["Movimiento"]["id_deposito_origen"];
                                    $codigo_barra = $this->request->data["Movimiento"]["codigo_barra"];
                                    $codigo= $this->request->data["Movimiento"]["codigo_producto"];
                                    
                                    if($codigo!="" && $id_producto == ""){
                                    	
                                    	$articulo = $this->Articulo->find('first', array(
                                    			'conditions' => array(
                                    					'Articulo.codigo' => $codigo
                                    			) ,
                                    			'contain' => false
                                    	));
                                    	
                                    	if($articulo){
                                    		$id_producto = $articulo["Articulo"]["id"];
                                    		$this->request->data["Movimiento"]["id_producto"] = $id_producto;
                                    		
                                    	}else{
                                    		
                                    		$error = EnumError::ERROR;
                                    		$mensaje = "No existe ning&uacute;n art&iacute;culo con el c&oacute;digo indicado";
                                    		
                                    		throw new Exception($mensaje);
                                    		break;
                                    	}
                                    	
                                    	
                                    	
                                    }
                                    
                                    
                                    if($codigo_barra!="" && $id_producto == ""){
                                    	
                                    	$articulo = $this->Articulo->find('first', array(
                                    			'conditions' => array(
                                    					'OR' => array(
                                    							array('Articulo.codigo_barra'=> $codigo_barra),
                                    							array('Articulo.codigo_barra2'=> $codigo_barra),
                                    							array('Articulo.codigo_barra3'=> $codigo_barra),
                                    							array('Articulo.codigo_barra4'=> $codigo_barra),
                                    							array('Articulo.codigo_barra5'=> $codigo_barra),
                                    							
                                    					),
                                    			) ,
                                    			'contain' => false
                                    	));
                                    	
                                    	if($articulo){
                                    		
                                    		$id_producto = $articulo["Articulo"]["id"];
                                    		$this->request->data["Movimiento"]["id_producto"] = $id_producto;
                                    		
                                    	}else{
                                    		
                                    		$error = EnumError::ERROR;
                                    		$mensaje = "No existe ning&uacute;n art&iacute;culo con el codigo de barra indicado";
                                    		throw new Exception($mensaje);
                                    	
                                    	}
                                    	
                                    	
                                    	
                                    }
                                    
                                    
                                    $this->Movimiento->updateAll(
                                    		array('Movimiento.id_producto' => $id_producto),
                                    		array('Movimiento.id' => $this->Movimiento->id) );
                                    
                                    
                                    if($id_deposito_destino>0) 
                                        $this->StockArticulo->updateAll(
                                          array('StockArticulo.stock' => "StockArticulo.stock + ".$cantidad ),
                                          array('StockArticulo.id' => $id_producto,'StockArticulo.id_deposito' => $id_deposito_destino) );
                                          
                                    
                                   if($id_deposito_origen>0) 
                                        $this->StockArticulo->updateAll(
                                          array('StockArticulo.stock' => "StockArticulo.stock - ".$cantidad ),
                                          array('StockArticulo.id' => $id_producto,'StockArticulo.id_deposito' => $id_deposito_origen) );      
					        break; 
                          
                                    
                           
							        
						   case EnumTipoMovimiento::RetirodeMateriaPrimaPorOrdenProduccion: //500
                                    
                                     $this->loadModel("StockMateriaPrima");
                                     $this->loadModel("StockComponente");
                                     $this->loadModel("Comprobante");
                                     $this->loadModel("ComprobanteItem");
                                     $this->loadModel("ArticuloRelacion");
                                     $id_deposito_origen = $this->request->data["Movimiento"]["id_deposito_origen"];
                                     $id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
                                    
                                     $op = $this->ComprobanteItem->find('first', array(
                                        'conditions' => array('ComprobanteItem.id_comprobante' => $this->request->data["Movimiento"]["id_comprobante"],'ComprobanteItem.id_detalle_tipo_comprobante'=>EnumDetalleTipoComprobante::OrdenDeTrabajoCabecera ),
                                        'contain' =>array('Comprobante')
                                    ));
                                     
                                          
                             
                                     	
                        
                                     $id_componente = $op["ComprobanteItem"]["id_producto"];
                               
                                    
                                    
                             
                                    
                                     $mpc = $this->ArticuloRelacion->find('all', array(
                                        'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_componente),
                                        'contain' =>false
                                    ));
                                    if($mpc){ //por cada materiaPrima encontrada actualizo el stock y le resto lo que insume de materia prima ese componente
                                         foreach($mpc as $materia_prima_componente){
                                          $id_materia_prima = $materia_prima_componente["ArticuloRelacion"]["id_producto_hijo"];
                                          
                                 
                                          
                                          
                                          $cantidad_grabar = $materia_prima_componente["ArticuloRelacion"]["cantidad_hijo"]*$cantidad;
                                          $movimiento = array();
                                          $movimientos = array();
                                          $movimiento["id_comprobante"] = $this->request->data["Movimiento"]["id_comprobante"];
                                          $movimiento["cantidad"] = $cantidad_grabar;
                                          $movimiento["id_movimiento_tipo"] = EnumTipoMovimiento::AjusteStock;
                                          $movimiento["fecha"] = date("Y-m-d H:i:s");
                                          $movimiento["id_deposito_origen"] = $id_deposito_origen;
                   						  $movimiento["id_usuario"] = $id_usuario;
                   						  array_push($movimientos, $movimiento);
                   						  
                   						  
                   						  
                   						  $movimiento["id_comprobante"] = $this->request->data["Movimiento"]["id_comprobante"];
                   						  $movimiento["cantidad"] = $cantidad_grabar;
                   						  $movimiento["id_movimiento_tipo"] = EnumTipoMovimiento::AjusteStock;
                   						  $movimiento["fecha"] = date("Y-m-d H:i:s");
                   						  $movimiento["id_deposito_origen"] = $id_deposito_destino;
                   						  $movimiento["id_usuario"] = $id_usuario;
                   						  array_push($movimientos, $movimiento);
                   						  
                   						  /*Dejo un registro de todo lo que movi para la OP que materias primas movi*/
                   						  $this->Movimiento->Save($movimientos); 
                   						  
                   						  
                   						  $this->StockMateriaPrima->updateAll(
                   						  		array('StockMateriaPrima.stock' => "StockMateriaPrima.stock - ".$cantidad_grabar),
                   						  		array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito' => $id_deposito_origen)
                   						  		) ; 
                                            
                                           $this->StockMateriaPrima->updateAll(
                                           		array('StockMateriaPrima.stock' => "StockMateriaPrima.stock + ".$cantidad_grabar),
                                            array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito' => $id_deposito_destino) 
                                            );
                                         
                                         }
                                    }
                                    
                                    /////////////////////////////////////////////////////////////////////
                            break;
                            
                            
                            
						   case EnumTipoMovimiento::RetirodeMateriaPrimaPorOrdenProduccionViejo: //500
						   	
						   	$this->loadModel("StockMateriaPrima");
						   	$this->loadModel("StockComponente");
						   	$this->loadModel("OrdenProduccion");
						   	$this->loadModel("ArticuloRelacion");
						   	$id_deposito_origen = $this->request->data["Movimiento"]["id_deposito_origen"];
						   	$id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
						   	
						   	$op = $this->OrdenProduccion->find('first', array(
						   			'conditions' => array('OrdenProduccion.id' => $this->request->data["Movimiento"]["id_orden_produccion"]),
						   			'contain' =>false
						   	));
						   	
						   	$id_componente = $op["OrdenProduccion"]["id_componente"];
						   	//$cantidad = $op["OrdenProduccion"]["cantidad"]; //MP Commitiado para volver al movimiento parcial
						   	
						   	
						   	//busco las materias primas asociadas al componente en Valmec es solo 1
						   	
						   	$mpc = $this->ArticuloRelacion->find('all', array(
						   			'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_componente),
						   			'contain' =>false
						   	));
						   	if($mpc){ //por cada materiaPrima encontrada actualizo el stock y le resto lo que insume de materia prima ese componente
						   		foreach($mpc as $materia_prima_componente){
						   			$id_materia_prima = $materia_prima_componente["ArticuloRelacion"]["id_producto_hijo"];
						   			
						   			$this->StockMateriaPrima->updateAll(
						   					array('StockMateriaPrima.stock' => "StockMateriaPrima.stock - ".$materia_prima_componente["ArticuloRelacion"]["cantidad_hijo"]*$cantidad),
						   					array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito' => $id_deposito_origen)
						   					) ;
						   			
						   			$this->StockMateriaPrima->updateAll(
						   					array('StockMateriaPrima.stock' => "StockMateriaPrima.stock + ".$materia_prima_componente["ArticuloRelacion"]["cantidad_hijo"]*$cantidad),
						   					array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito' => $id_deposito_destino)
						   					);
						   			
						   		}
						   	}
						   	
						   	/////////////////////////////////////////////////////////////////////
						   	break;
                            
						   	
						   	
						   case EnumTipoMovimiento::IngresoDeComponenteParcialPorOrdenProduccionViejo: //501
						   	
						   	$this->loadModel("StockMateriaPrima");
						   	$this->loadModel("StockComponente");
						   	$this->loadModel("OrdenProduccion");
						   	$this->loadModel("Componente");
						   	$this->loadModel("ArticuloRelacion");
						   	
						   	
						   	//busco si hay un 500 para hacer un 501
						   	$mov_previo = $this->Movimiento->find('first',array(
						   			'conditions' => array('Movimiento.id_orden_produccion' => $this->request->data["Movimiento"]["id_orden_produccion"],'Movimiento.id_movimiento_tipo'=>array(EnumTipoMovimiento::RetirodeMateriaPrimaPorOrdenProduccion,EnumTipoMovimiento::RetirodeMateriaPrimaPorOrdenProduccionViejo)),
						   			'contain' =>false
						   	));
						   	
						   	
						   	if($mov_previo){
						   		
						   		/* $id_deposito_origen = $mov_previo["Movimiento"]["id_deposito_origen"];
						   		 $id_deposito_destino = $mov_previo["Movimiento"]["id_deposito_destino"];
						   		 */
						   		
						   		$id_deposito_origen = $this->request->data["Movimiento"]["id_deposito_origen"];
						   		$id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
						   		
						   		$op = $this->OrdenProduccion->find('first', array(
						   				'conditions' => array('OrdenProduccion.id' => $this->request->data["Movimiento"]["id_orden_produccion"]),
						   				'contain' =>false
						   		));
						   		//$cantidad = $op["OrdenProduccion"]["cantidad"];
						   		
						   		$id_componente = $op["OrdenProduccion"]["id_componente"];
						   		
						   		
						   		//busco las materias primas asociadas al componente en Valmec es solo 1
						   		
						   		$mpc = $this->ArticuloRelacion->find('all', array(
						   				'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_componente),
						   				'contain' =>false
						   		));
						   		
						   		if($mpc){ //por cada materiaPrima encontrada actualizo el stock y le resto lo que insume de materia prima ese componente
						   			foreach($mpc as $materia_prima_componente){
						   				$id_materia_prima = $materia_prima_componente["ArticuloRelacion"]["id_producto_hijo"];
						   				
						   				$this->StockMateriaPrima->updateAll(
						   						array('StockMateriaPrima.stock' => "StockMateriaPrima.stock - ".$materia_prima_componente["ArticuloRelacion"]["cantidad_hijo"]*$cantidad  ),
						   						array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito' => $id_deposito_origen) );
						   				
						   			}
						   		}
						   		$this->StockComponente->updateAll(
						   				array('StockComponente.stock' => "StockComponente.stock + ".$cantidad  ),
						   				array('StockComponente.id' => $id_componente,'StockComponente.id_deposito' => $id_deposito_destino) );
						   	}else{
						   		
						   		//muestro mensaje para que
						   		//throw new Exception('El Movimiento NO puede ser creado, previamente debe generar un movimiento 500');
						   		$mensaje = "El Movimiento NO puede ser creado, previamente debe generar un movimiento 500";
						   		$tipo = EnumError::ERROR;
						   		break;
						   		
						   	}
						   	
						   	
						   	break; 
						   	
						   	
						   	
						   	
						   	
						   case EnumTipoMovimiento::IngresoMateriaPrimaSobrantePorOrdenProduccionViejo: //502
						   	
						   	$this->loadModel("StockMateriaPrima");
						   	$this->loadModel("StockComponente");
						   	$this->loadModel("OrdenProduccion");
						   	$this->loadModel("Componente");
						   	$this->loadModel("ArticuloRelacion");
						   	$id_deposito_origen = $this->request->data["Movimiento"]["id_deposito_origen"];
						   	$id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
						   	
						   	$op = $this->OrdenProduccion->find('first', array(
						   			'conditions' => array('OrdenProduccion.id' => $this->request->data["Movimiento"]["id_orden_produccion"]),
						   			'contain' =>false
						   	));
						   	
						   	$id_componente = $op["OrdenProduccion"]["id_componente"];
						   	
						   	
						   	//busco las materias primas asociadas al componente en Valmec es solo 1
						   	
						   	$mpc = $this->ArticuloRelacion->find('all', array(
						   			'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_componente),
						   			'contain' =>false
						   	));
						   	if($mpc){ //por cada materiaPrima encontrada actualizo el stock y le resto lo que insume de materia prima ese componente
						   		foreach($mpc as $materia_prima_componente){
						   			$id_materia_prima = $materia_prima_componente["ArticuloRelacion"]["id_producto_hijo"];
						   			
						   			$this->StockMateriaPrima->updateAll(
						   					array('StockMateriaPrima.stock' => "StockMateriaPrima.stock - ".$cantidad),
						   					array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito' => $id_deposito_origen) );
						   			
						   			$this->StockMateriaPrima->updateAll(
						   					array('StockMateriaPrima.stock' => "StockMateriaPrima.stock + ".$cantidad),
						   					array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito' => $id_deposito_destino) );
						   			
						   		}
						   	}
						   	break;
						   	
						   	
						   	
						   	
						   	
                            
                            case EnumTipoMovimiento::Inversodel500: //1500 (500 inverso)
                                    
                                     $this->loadModel("StockMateriaPrima");
                                     $this->loadModel("StockComponente");
                                     $this->loadModel("Comprobante");
                                     $this->loadModel("ArticuloRelacion");
                                  
                                  
                                  
                                    $mov_500 = $this->Movimiento->find('first',array(
                                        'conditions' => array('Movimiento.id' => $this->request->data['Movimiento']['id_movimiento_padre']),
                                        'contain' =>false
                                    ));  
                                    
                                   //busco si hay un 501 para hacer un 1500 si hay 501 no lo puedo hacer
                                    
                                    $mov_previo = $this->Movimiento->find('first',array(
                                        'conditions' => array('Movimiento.id_comprobante' => $mov_500["Movimiento"]["id_comprobante"],'Movimiento.id_movimiento_tipo'
											         =>EnumTipoMovimiento::IngresoDeComponenteParcialPorOrdenProduccion),
                                        'contain' =>false
                                    ));
                                    
                                  
                                    
                                    
                                   if(!$mov_previo || !$mov_500){ //si NO  hay 501 entonces puedo hacer la inversa o sea revertir el movimiento 500 o si no encontro al 500
                                      
                                    
                                    $op = $this->OrdenProduccion->find('first', array(
                                        'conditions' => array('OrdenProduccion.id' => $mov_500["Movimiento"]["id_orden_produccion"]),
                                        'contain' =>false
                                    ));
                                    
                                    $cantidad = $mov_500["Movimiento"]["cantidad"]; //recupero la misma cantidad del movimiento 500 para hacer la inversa
                                    
                                    $id_componente = $op["OrdenProduccion"]["id_componente"];
                                    
                                     $cantidad = $mov_500["Movimiento"]["cantidad"];
                                     
                                      $id_deposito_origen = $mov_500["Movimiento"]["id_deposito_origen"];
                                      $id_deposito_destino = $mov_500["Movimiento"]["id_deposito_destino"];
                           
                                  
                                          
                                    //busco las materias primas asociadas al componente en Valmec es solo 1
                                    
                                     $mpc = $this->ArticuloRelacion->find('all', array(
                                        'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_componente),
                                        'contain' =>false
                                    ));
                                    if($mpc){ //por cada materiaPrima encontrada actualizo el stock y le resto lo que insume de materia prima ese componente
                                         foreach($mpc as $materia_prima_componente){
                                          $id_materia_prima = $materia_prima_componente["ArticuloRelacion"]["id_producto_hijo"];
                                          
                                          $this->StockMateriaPrima->updateAll(
                                            array('StockMateriaPrima.stock' => "StockMateriaPrima.stock + ".$materia_prima_componente["ArticuloRelacion"]["cantidad_hijo"]*$cantidad),
                                            array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito' => $id_deposito_origen) ); 
                                            
                                            
                                          $this->StockMateriaPrima->updateAll(
                                            array('StockMateriaPrima.stock' => "StockMateriaPrima.stock - ".$materia_prima_componente["ArticuloRelacion"]["cantidad_hijo"]*$cantidad),
                                            array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito' => $id_deposito_destino) ); 
                                         
                                         }
                                    }
                                    
                                   }else{
                                      //muestro mensaje para que 
                                         //throw new Exception('El Movimiento NO puede ser creado, previamente debe generar un movimiento 500');
                                         $mensaje = "El Movimiento NO puede ser creado, ya que realizo un 501";
                                         $tipo = EnumError::ERROR;
                                         break; 
                                       
                                   }
                                    
                                    /////////////////////////////////////////////////////////////////////
                            break;
                            
                            case EnumTipoMovimiento::IngresoDeComponenteParcialPorOrdenProduccion: //501
                                    
                                    $this->loadModel("StockMateriaPrima");
                                    $this->loadModel("StockComponente");
                                    $this->loadModel("Comprobante");
                                    $this->loadModel("Componente");
                                    $this->loadModel("ArticuloRelacion");
                                     
                                    
                                    //busco si hay un 500 para hacer un 501
                                    $mov_previo = $this->Movimiento->find('first',array(
                                        'conditions' => array('Movimiento.id_comprobante' => $this->request->data["Movimiento"]["id_comprobante"],'Movimiento.id_movimiento_tipo'=>EnumTipoMovimiento::RetirodeMateriaPrimaPorOrdenProduccion),
                                        'contain' =>false
                                    ));
                                    
                                    
                                    if($mov_previo){
                                        
                                           /* $id_deposito_origen = $mov_previo["Movimiento"]["id_deposito_origen"];
                                            $id_deposito_destino = $mov_previo["Movimiento"]["id_deposito_destino"];
                                           */
                                           
                                           $id_deposito_origen = $this->request->data["Movimiento"]["id_deposito_origen"]; 
                                           $id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
                                            
                                           
                                           
                                           $op = $this->Comprobante->ComprobanteItem->find('first', array(
                                           		'conditions' => array('ComprobanteItem.id_comprobante' => $this->request->data["Movimiento"]["id_comprobante"],'ComprobanteItem.id_detalle_tipo_comprobante'=>EnumDetalleTipoComprobante::OrdenDeTrabajoCabecera ),
                                           		'contain' =>false
                                           ));
                                           
                                           
                                           
                                           
                                           
                                           $id_componente = $op["ComprobanteItem"]["id_producto"];
                                           
                                           
                                          
                                                  
                                            //busco las materias primas asociadas al componente en Valmec es solo 1
                                            
                                             $mpc = $this->ArticuloRelacion->find('all', array(
                                                'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_componente),
                                                'contain' =>false
                                            ));
                                            
                                            if($mpc){ //por cada materiaPrima encontrada actualizo el stock y le resto lo que insume de materia prima ese componente
                                                 foreach($mpc as $materia_prima_componente){
                                                  $id_materia_prima = $materia_prima_componente["ArticuloRelacion"]["id_producto_hijo"];
                                                  
                                                  $this->StockMateriaPrima->updateAll(
                                                    array('StockMateriaPrima.stock' => "StockMateriaPrima.stock - ".$materia_prima_componente["ArticuloRelacion"]["cantidad_hijo"]*$cantidad  ),
                                                    array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito' => $id_deposito_origen) ); 
                                                 
                                                 }
                                            }
                                             $this->StockComponente->updateAll(
                                                    array('StockComponente.stock' => "StockComponente.stock + ".$cantidad  ),
                                                    array('StockComponente.id' => $id_componente,'StockComponente.id_deposito' => $id_deposito_destino) ); 
                                    }else{
                                        
                                        //muestro mensaje para que 
                                         //throw new Exception('El Movimiento NO puede ser creado, previamente debe generar un movimiento 500');
                                         $mensaje = "El Movimiento NO puede ser creado, previamente debe generar un movimiento 500";
                                         $tipo = EnumError::ERROR;
                                         break;
                                        
                                    }        
                                         
                              
                            break; 
					          
					          
					        case EnumTipoMovimiento::Inversodel501: //1501 (501 inverso)
                                    
                                     $this->loadModel("StockMateriaPrima");
                                     $this->loadModel("StockComponente");
                                     $this->loadModel("OrdenProduccion");
                                     $this->loadModel("ArticuloRelacion");
                                  
                                    $mov_501 = $this->Movimiento->find('first',array(
                                        'conditions' => array('Movimiento.id' => $this->request->data['Movimiento']['id_movimiento_padre']),
                                        'contain' =>false
                                    ));  
                                    
                                   // //busco si hay un 501 para hacer un 1500 si hay 501 no lo puedo hacer
                                    
                                    // $mov_previo = $this->Movimiento->find('first',array(
                                        // 'conditions' => array('Movimiento.id_orden_produccion' => $mov_501["Movimiento"]["id_orden_produccion"],'Movimiento.id_movimiento_tipo'
											         // => EnumTipoMovimiento::IngresoDeComponenteParcialPorOrdenProduccion),
                                        // 'contain' =>false
                                    // ));
                                   
                                   // if(!$mov_previo || !$mov_501){ //si NO  hay 501 entonces puedo hacer la inversa o sea revertir el movimiento 501 o si no encontro al 501
						           
							        if( $mov_501){ //si NO  hay 501 entonces puedo hacer la inversa
                                    
                                    $op = $this->OrdenProduccion->find('first', array(
                                        'conditions' => array('OrdenProduccion.id' => $mov_501["Movimiento"]["id_orden_produccion"]),
                                        'contain' =>false
                                    ));
                                    
                                    $cantidad = $mov_501["Movimiento"]["cantidad"]; //recupero la misma cantidad del movimiento 501 para hacer la inversa
                                    
                                    $id_componente = $op["OrdenProduccion"]["id_componente"];
                                    
                                    $cantidad = $mov_501["Movimiento"]["cantidad"];
                                     
							        $id_deposito_origen = $mov_501["Movimiento"]["id_deposito_origen"];
							        $id_deposito_destino = $mov_501["Movimiento"]["id_deposito_destino"];
                                          
                                    //busco las materias primas asociadas al componente en Valmec es solo 1
                                    
                                     $mpc = $this->ArticuloRelacion->find('all', array(
                                        'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_componente),
                                        'contain' =>false
                                    ));
                                    if($mpc){ //por cada materiaPrima encontrada actualizo el stock y le resto lo que insume de materia prima ese componente
                                         foreach($mpc as $materia_prima_componente){
                                          $id_materia_prima = $materia_prima_componente["ArticuloRelacion"]["id_producto_hijo"];
                                          
                                          $this->StockMateriaPrima->updateAll(
                                            array('StockMateriaPrima.stock' => "StockMateriaPrima.stock + ".$materia_prima_componente["ArticuloRelacion"]["cantidad_hijo"]*$cantidad),
                                            array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito' => $id_deposito_origen) ); 
                                                
                                          //$this->StockMateriaPrima->updateAll(
                                            // array('StockMateriaPrima.stock' => "StockMateriaPrima.stock - ".$materia_prima_componente["ArticuloRelacion"]["cantidad_hijo"]*$cantidad),
                                            // array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito' => $id_deposito_destino) ); 
                                         }
                                    }
							        $this->StockComponente->updateAll(
								        array('StockComponente.stock' => "StockComponente.stock - ".$cantidad  ),
								        array('StockComponente.id' => $id_componente,'StockComponente.id_deposito' => $id_deposito_destino) ); 
                                    
                                   }else{
                                      //muestro mensaje para que 
                                         //throw new Exception('El Movimiento NO puede ser creado, previamente debe generar un movimiento 500');
                                         $mensaje = "El Movimiento NO puede ser creado, no se encontró un 501";
                                         $tipo = EnumError::ERROR;
                                         break;                                
                                   }
                                    /////////////////////////////////////////////////////////////////////
                            break;
                              
                                 
                            case EnumTipoMovimiento::IngresoMateriaPrimaSobrantePorOrdenProduccion: //502
                                    
                                    $this->loadModel("StockMateriaPrima");
                                    $this->loadModel("StockComponente");
                                    $this->loadModel("OrdenProduccion");
                                    $this->loadModel("Componente");
                                    $this->loadModel("ArticuloRelacion");
                                    $id_deposito_origen = $this->request->data["Movimiento"]["id_deposito_origen"];
                                    $id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
                                    
                                    $op = $this->OrdenProduccion->find('first', array(
                                        'conditions' => array('OrdenProduccion.id' => $this->request->data["Movimiento"]["id_orden_produccion"]),
                                        'contain' =>false
                                    ));
                                    
                                    $id_componente = $op["OrdenProduccion"]["id_componente"];
                                  
                                          
                                    //busco las materias primas asociadas al componente en Valmec es solo 1
                                    
                                     $mpc = $this->ArticuloRelacion->find('all', array(
                                        'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_componente),
                                        'contain' =>false
                                    ));
                                    if($mpc){ //por cada materiaPrima encontrada actualizo el stock y le resto lo que insume de materia prima ese componente
                                         foreach($mpc as $materia_prima_componente){
                                          $id_materia_prima = $materia_prima_componente["ArticuloRelacion"]["id_producto_hijo"];
                                          
                                          $this->StockMateriaPrima->updateAll(
                                            array('StockMateriaPrima.stock' => "StockMateriaPrima.stock - ".$cantidad),
                                            array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito' => $id_deposito_origen) ); 
                                         
                                              $this->StockMateriaPrima->updateAll(
                                                    array('StockMateriaPrima.stock' => "StockMateriaPrima.stock + ".$cantidad),
                                                    array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito' => $id_deposito_destino) );    
                                         
                                         }
                                    }
                            break;
					          
					          
					        case EnumTipoMovimiento::IngresoDeComponentePorOrdenProduccion: //503
                                    
                                    $this->loadModel("StockMateriaPrima");
                                    $this->loadModel("StockComponente");
                                    $this->loadModel("OrdenProduccion");
                                    $this->loadModel("Componente");
                                    $this->loadModel("ArticuloRelacion");
                                    $id_deposito_origen = $this->request->data["Movimiento"]["id_deposito_origen"];
                                    $id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
                                    
                                    $op = $this->OrdenProduccion->find('first', array(
                                        'conditions' => array('OrdenProduccion.id' => $this->request->data["Movimiento"]["id_orden_produccion"]),
                                        'contain' =>false
                                    ));
                                    $cantidad = $op["OrdenProduccion"]["cantidad"];
                                    
                                    $id_componente = $op["OrdenProduccion"]["id_componente"];
                                  
                                          
                                    //busco las materias primas asociadas al componente en Valmec es solo 1
                                    
                                     $mpc = $this->ArticuloRelacion->find('all', array(
                                        'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_componente),
                                        'contain' =>false
                                    ));
                                    if($mpc){ //por cada materiaPrima encontrada actualizo el stock y le resto lo que insume de materia prima ese componente
                                         foreach($mpc as $materia_prima_componente){
                                          $id_materia_prima = $materia_prima_componente["ArticuloRelacion"]["id_producto_hijo"];
                                          
                                          $this->StockMateriaPrima->updateAll(
                                            array('StockMateriaPrima.stock' => "StockMateriaPrima.stock - ".$materia_prima_componente["ArticuloRelacion"]["cantidad_hijo"]*$cantidad  ),
                                            array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito' => $id_deposito_origen) ); 
                                         
                                         }
                                    }
                                     $this->StockComponente->updateAll(
                                            array('StockComponente.stock' => "StockComponente.stock + ".$cantidad  ),
                                            array('StockComponente.id' => $id_componente,'StockComponente.id_deposito' => $id_deposito_destino) ); 
                                         
                                         
                              
                            break; 
                              
                            
                            case EnumTipoMovimiento::DeteccionComponenteNc:
                                   
                                   $id_componente = $this->request->data["Movimiento"]["id_componente"];
                                   $id_deposito_origen = $this->request->data["Movimiento"]["id_deposito_origen"];
                                   $id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
                                   
                                   $this->loadModel("StockComponente");
                                   $this->loadModel("ArticuloRelacion");
                                   
                                   
                                   $this->StockComponente->updateAll(
                                            array('StockComponente.stock' => "StockComponente.stock + ".$cantidad  ),
                                            array('StockComponente.id' => $id_componente,'StockComponente.id_deposito'=>$id_deposito_destino) );
                                            
                                    //busco las materias primas asociadas al componente en Valmec es solo 1
                                    
                                     $mpc = $this->ArticuloRelacion->find('all', array(
                                        'conditions' => array('ArticuloRelacion.id_componente' => $id_componente),
                                        'contain' =>false
                                    ));
                                    if($mpc){ //por cada materiaPrima encontrada actualizo el stock y le resto lo que insume de materia prima ese componente
                                         foreach($mpc as $materia_prima_componente){
                                          $id_materia_prima = $materia_prima_componente["ArticuloRelacion"]["id_materia_prima"];
                                          $cantidad_consume = $materia_prima_componente["ArticuloRelacion"]["cantidad"];
                                          
                                          $this->StockMateriaPrima->updateAll(
                                            array('StockMateriaPrima.stock' => "StockMateriaPrima.stock - ".$cantidad_consume*$cantidad
                                                  ),
                                            array('StockMateriaPrima.id' => $id_materia_prima,'StockMateriaPrima.id_deposito'=>$id_deposito_origen) ); 
                                         
                                         }
                                    }          
                              
                                    
                            break;
                              
                              
                            
                            case EnumTipoMovimiento::OrdenArmadoAlta://600
                                
                                         $this->loadModel("ComprobanteItem");
                                         $this->loadModel("Componente");
                                         $this->loadModel("ArticuloRelacion");
                                         $this->loadModel("StockComponente");
                                      
                                         $id_deposito_origen = $this->request->data["Movimiento"]["id_deposito_origen"];
                                         $id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
                                     
                                        
                                         
                                         $oa = $this->ComprobanteItem->find('first', array(
                                        'conditions' => array('ComprobanteItem.id_comprobante' => $this->request->data["Movimiento"]["id_comprobante"],"ComprobanteItem.activo"=>1),
                                        'contain' =>false
                                        ));
                                        
                                        $cantidad = $oa["ComprobanteItem"]["cantidad_cierre"];
                                    
                                        $id_producto = $oa["ComprobanteItem"]["id_producto"];
                                        
                                        
                                        //logueo la cantidad en el movimiento
                                        $this->Movimiento->updateAll(
                                                array('Movimiento.cantidad' => $cantidad ),
                                                array('Movimiento.id' => $id_movimiento) ); 
                                        
                                        //busco los componentes del producto
                                        $prod_item = $this->ArticuloRelacion->find('all', array(
                                                    'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_producto),
                                                    'contain' =>false
                                                    ));
                                         
                                         
                                          if($prod_item){ //por cada producto encontrado actualizo el stock
                                          
                                          //por cada componente actualizo su stock
                                             foreach($prod_item as $componente){
                                              
                                              $id_componente = $componente["ArticuloRelacion"]["id_producto_hijo"];
                                              $cantidad_consume = $componente["ArticuloRelacion"]["cantidad_hijo"];
                                              
                                              
                                             if($id_deposito_origen > 0 ){
                                              $this->StockComponente->updateAll(
                                                array('StockComponente.stock' => "StockComponente.stock - ".$cantidad_consume*$cantidad),
                                                array('StockComponente.id' => $id_componente,'StockComponente.id_deposito' => $id_deposito_origen) );
                                                
                                             }
                                            if($id_deposito_destino > 0 ) {
                                              $this->StockComponente->updateAll(
                                                array('StockComponente.stock' => "StockComponente.stock + ".$cantidad_consume*$cantidad),
                                                array('StockComponente.id' => $id_componente,'StockComponente.id_deposito' => $id_deposito_destino) ); 
                                             
                                            }
                                             
                                             }
                                         }
                                         
                                
                            break;
                                
                                
                                
                            case EnumTipoMovimiento::OrdenArmadoCierre:
                                
                                         $this->loadModel("ComprobanteItem");
                                         $this->loadModel("Componente");
                                         $this->loadModel("ArticuloRelacion");
                                         $this->loadModel("StockComponente");
                                         $this->loadModel("StockProducto");
                                         
                                         $id_deposito_origen = $this->request->data["Movimiento"]["id_deposito_origen"];
                                         $id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
                                        
                                         
                                         $oa = $this->ComprobanteItem->find('first', array(
                                        'conditions' => array('ComprobanteItem.id_comprobante' => $this->request->data["Movimiento"]["id_comprobante"],"ComprobanteItem.activo"=>1),
                                        'contain' =>false
                                        ));
                                        
                                        $cantidad = $oa["ComprobanteItem"]["cantidad_cierre"];
                                        $cantidad_armada_cierre = $oa["ComprobanteItem"]["cantidad_cierre"];//es la cantidad que realmente se hizo
                                        
                                        
                                        //logueo la cantidad en el movimiento
                                        $this->Movimiento->updateAll(
                                                array('Movimiento.cantidad' => $cantidad ),
                                                array('Movimiento.id' => $id_movimiento) ); 
                                                
                                                
                                        $cantidad_a_volver_atras = $cantidad -   $cantidad_armada_cierre;
                                    
                                        $id_producto = $oa["ComprobanteItem"]["id_producto"];
                                        
                                    
                                        
                                        //busco los componentes del producto
                                        $prod_item = $this->ArticuloRelacion->find('all', array(
                                                    'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_producto),
                                                    'contain' =>false
                                                    ));
                                         
                                         
                                          if($prod_item){ //por cada producto encontrado actualizo el stock
                                          
                                          //por cada componente actualizo su stock
                                             foreach($prod_item as $componente){
                                              
                                              $id_componente = $componente["ArticuloRelacion"]["id_producto_hijo"];
                                              $cantidad_consume = $componente["ArticuloRelacion"]["cantidad_hijo"];
                                              
                                              $this->StockComponente->updateAll(
                                                array("StockComponente.stock" => "StockComponente.stock - ".$cantidad_consume*$cantidad  ),
                                                array('StockComponente.id' => $id_componente,'StockComponente.id_deposito' => $id_deposito_origen) ); 
                                             
                                             }
                                         }
                                         //incremento StockProducto en la cantidad que realmente arme
                                         $this->StockProducto->updateAll(
                                                array("StockProducto.stock" => "StockProducto.stock + ".$cantidad  ),
                                                array('StockProducto.id' => $id_producto,'StockProducto.id_deposito' => $id_deposito_destino) );
                                         
                                        
                                        //genero movimiento 602 por aquellos componentes que no fueron usados xq se hizo menos
                                        
                                        
                                       
                                       
                            break;
                                  
                                        
                            case EnumTipoMovimiento::OrdenArmadoAjuste:
                                 
                                     $this->loadModel("ComprobanteItem");
                                     $this->loadModel("Componente");
                                     $this->loadModel("ArticuloRelacion");
                                     $this->loadModel("StockComponente");
                                     $this->loadModel("StockProducto");
                                 
                                     $id_deposito = $this->request->data["Movimiento"]["id_deposito_origen"];
                                     $id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
                                     $diferencia = $this->request->data["Movimiento"]["diferencia"];
                                        
                                         
                                     $oa = $this->ComprobanteItem->find('first', array(
                                     		'conditions' => array('ComprobanteItem.id_comprobante' => $this->request->data["Movimiento"]["id_comprobante"],"ComprobanteItem.activo"=>1),
                                        'contain' =>array('Comprobante')
                                        ));
                                        
                                     $id_producto = $oa["ComprobanteItem"]["id_producto"];
                                     $cantidad = $oa["ComprobanteItem"]["cantidad_cierre"];
                                    
                                     
                                     $prod_item = $this->ArticuloRelacion->find('all', array(
                                                    'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_producto),
                                                    'contain' =>false
                                                    )); 
                                 
                                  if($diferencia > 0){ //agrego. SACO DE ALMACEN Y SUMOO COMPROMETIDO   
                                            
                                     $simbolo1 = '- ';
                                     $simbolo2 = '+ ';    
                                  }else{//devuelvo
                                      
                                     $simbolo1 = '+ ';
                                     $simbolo2 = '- ';
                                      
                                      
                                  }
                                  
                                   if($prod_item){
                                                 
                                                 //por cada componente actualizo su stock
                                             foreach($prod_item as $componente){
                                              
                                                  $id_componente = $componente["ArticuloRelacion"]["id_producto_hijo"];
                                                  $cantidad_consume = $componente["ArticuloRelacion"]["cantidad_hijo"];
                                                  
                                                  
                                                 if($id_deposito > 0 ) 
                                                  $this->StockComponente->updateAll(
                                                    array('StockComponente.stock' => "StockComponente.stock  ".$simbolo1.$cantidad_consume*abs($diferencia)),
                                                    array('StockComponente.id' => $id_componente,'StockComponente.id_deposito' => $id_deposito) );
                                                    
                                                if($id_deposito_destino > 0 ) 
                                                  $this->StockComponente->updateAll(
                                                    array('StockComponente.stock' => "StockComponente.stock ".$simbolo2.$cantidad_consume*abs($diferencia)),
                                                    array('StockComponente.id' => $id_componente,'StockComponente.id_deposito' => $id_deposito_destino) ); 
                                                    
                                                    
                                                    
                                                
                                                 
                                             
                                             
                                             }
                                         }
                                         
                                                $movimiento = array();
                                                $movimientos = array();
                                                $movimiento["id_comprobante"] = $this->request->data["Movimiento"]["id_comprobante"];
                                                $movimiento["cantidad"] = $diferencia;
                                                $movimiento["id_movimiento_tipo"] = EnumTipoMovimiento::OrdenArmadoAjuste;
                                                $movimiento["fecha"] = date("Y-m-d H:i:s");
                                                       
                                                if($id_deposito > 0)
                                                    $movimiento["id_deposito_origen"] = $id_deposito;
                                                          
                                                if($id_deposito_destino > 0)
                                                    $movimiento["id_deposito_destino"] = $id_deposito_destino;
                                                       
                                                $movimiento["id_usuario"] = $id_usuario;
                                                       
                                                    
                                                      
                                                      $this->Movimiento->Save($movimiento); 
                            break;
                                         
                                
                                
                                
                                
                            case EnumTipoMovimiento::Inversodel600:
                                
                                         $this->loadModel("Componente");
                                         $this->loadModel("ArticuloRelacion");
                                         $this->loadModel("StockComponente");
                                         $this->loadModel("OrdenArmado");
                                     
                                         //busco si hay un 601 para hacer un 1500 si hay 501 no lo puedo hacer
                                    
                                   
                                    $mov_600 = $this->Movimiento->find('first',array(
                                        'conditions' => array('Movimiento.id' => $this->request->data['Movimiento']['id_movimiento_padre']),
                                        'contain' =>false
                                    ));
                                   
                                    $mov_previo = $this->Movimiento->find('first',array(
                                        'conditions' => array('Movimiento.id_comprobante' => $mov_600["Movimiento"]["id_comprobante"],'Movimiento.id_movimiento_tipo'=>EnumTipoMovimiento::OrdenArmadoCierre),
                                        'contain' =>false
                                    ));
                                    
                                    
                                    
                                   
                                    
                                    
                                    
                                 if(!$mov_previo || !$mov_600){ //si NO  hay 601 entonces puedo hacer la inversa o sea revertir el movimiento 600 o si no encontro al 600          
                                         $oa = $this->OrdenArmado->find('first', array(
                                        'conditions' => array('OrdenArmado.id' => $mov_600["Movimiento"]["id_comprobante"]),
                                        'contain' =>false
                                        ));
                                        
                                        $cantidad = $oa["OrdenArmado"]["cantidad"];
                                    
                                        $id_producto = $oa["OrdenArmado"]["id_producto"];
                                        
                                        $id_deposito_origen = $mov_600["Movimiento"]["id_deposito_origen"];
                                        $id_deposito_destino = $mov_600["Movimiento"]["id_deposito_destino"];
                                        
                                        
                                        //logueo la cantidad en el movimiento
                                        $this->Movimiento->updateAll(
                                                array('Movimiento.cantidad' => $cantidad ),
                                                array('Movimiento.id' => $id_movimiento) ); 
                                        
                                        //busco los componentes del producto
                                        $prod_item = $this->ArticuloRelacion->find('all', array(
                                                    'conditions' => array('ArticuloRelacion.id_producto' => $id_producto),
                                                    'contain' =>false
                                                    ));
                                         
                                         
                                          if($prod_item){ //por cada producto encontrado actualizo el stock
                                          
                                          //por cada componente actualizo su stock
                                             foreach($prod_item as $componente){
                                              
                                              $id_componente = $componente["ArticuloRelacion"]["id_componente"];
                                              $cantidad_consume = $componente["ArticuloRelacion"]["cantidad"];
                                              
                                              $this->StockComponente->updateAll(
                                                array('StockComponente.stock' => "StockComponente.stock + ".$cantidad_consume*$cantidad),
                                                array('StockComponente.id' => $id_componente,'StockComponente.id_deposito' => $id_deposito_origen) ); 
                                                
                                                 $this->StockComponente->updateAll(
                                                array('StockComponente.stock' => "StockComponente.stock - ".$cantidad_consume*$cantidad),
                                                array('StockComponente.id' => $id_componente,'StockComponente.id_deposito' => $id_deposito_destino) ); 
                                             
                                             }
                                         }
                                }else{//error
                                     //muestro mensaje para que 
                                     
                                     
                                     //throw new Exception('El Movimiento NO puede ser creado, previamente debe generar un movimiento 600');
                                     $mensaje = "El Movimiento NO puede ser creado, ya que realizo un 601";
                                     $tipo = EnumError::ERROR;
                                     break; 
                                
                                }              
                                
                            break;
                              
                                
                                
                                
                                
                          
                       
                            case EnumTipoMovimiento::Comprobante:   
                            case EnumTipoMovimiento::DespachoMercaderia:   
                      
                                
                                
                                        $this->loadModel("StockProducto");
                                        $this->loadModel("Comprobante");
                                        $this->loadModel("ComprobanteItem");
                                        $this->loadModel("Producto");
                                        
                                        $id_deposito_origen = '';
                                        $id_deposito_destino = '';
                                        
                                        
                                        
                                        $id_comprobante = $this->request->data["Movimiento"]["id_comprobante"];
                                        if(isset($this->request->data["Movimiento"]["id_deposito_origen"]))
                                            $id_deposito_origen = $this->request->data["Movimiento"]["id_deposito_origen"];
                                        if(isset($this->request->data["Movimiento"]["id_deposito_destino"]))
                                            $id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
                                        
                                        
                                        if(isset($this->request->data["Movimiento"]["simbolo_operacion"]))
                                            $simbolo_operacion = $this->request->data["Movimiento"]["simbolo_operacion"];
                                        else
                                            $simbolo_operacion = 1;
                                        
                                            
                                        $comprobante = $this->Comprobante->find('first', array(
                                            		'conditions' => array('Comprobante.id' => $id_comprobante),
                                            		'contain' =>false
                                            ));
                                        
                                        $comprobante_items = $this->ComprobanteItem->find('all', array(
                                                    'conditions' => array('ComprobanteItem.id_comprobante' => $id_comprobante,'ComprobanteItem.activo'=>1),
                                                    'contain' =>false
                                                    ));
                                                    
                                                    
                                        $movimientos_items = array();
                                        
                                        if($comprobante_items){
                                        	
        
                                        	if($comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::InformeRecepcionMateriales)
                                        		$campo_cantidad = "cantidad_cierre";
                                        	else 
                                        		$campo_cantidad = "cantidad";
                                        	
                                        	
                                            foreach($comprobante_items as $prod_pedido_interno){ //por cada producto del PI actualizo el stock
                                                          $id_producto = $prod_pedido_interno["ComprobanteItem"]["id_producto"];
                                                          $cantidad = $prod_pedido_interno["ComprobanteItem"][$campo_cantidad];
                                                          $tiene_stock_producto = $this->Producto->AceptaStock($id_producto);
                                                          
                                                          
                                                          if($id_deposito_origen > 0 && $tiene_stock_producto == 1)
                                                            $this->StockProducto->updateAll(
                                                                array('StockProducto.stock' => "StockProducto.stock -".$cantidad*$simbolo_operacion  ),
                                                                array('StockProducto.id' => $id_producto,'StockProducto.id_deposito'=>$id_deposito_origen) );
                                                            
                                                           
                                                           if($id_deposito_destino > 0 && $tiene_stock_producto == 1)
                                                            $this->StockProducto->updateAll(
                                                                array('StockProducto.stock' => "StockProducto.stock +".$cantidad*$simbolo_operacion  ),
                                                                array('StockProducto.id' => $id_producto,'StockProducto.id_deposito'=>$id_deposito_destino) );
                                                                
                                                                
                                                           $movimiento = array();
                                                           $movimiento["id_comprobante"] = $id_comprobante;
                                                           $movimiento["cantidad"] = $cantidad;
                                                           $movimiento["id_comprobante_item"] = $prod_pedido_interno["ComprobanteItem"]["id"];
                                                           $movimiento["id_movimiento_tipo"] = EnumTipoMovimiento::AjustePositivoComprobanteItem;
                                                           $movimiento["fecha"] = date("Y-m-d H:i:s");
                                                           if($id_deposito_origen > 0)
                                                            $movimiento["id_deposito_origen"] = $id_deposito_origen;
                                                           if($id_deposito_destino > 0)
                                                            $movimiento["id_deposito_destino"] = $id_deposito_destino;
                                                           
                                                           $movimiento["id_usuario"] = $id_usuario;
                                                           array_push($movimientos_items,$movimiento);
                                                         
                                            }
                                            
                                            if(count($movimientos_items))
                                            	$this->Movimiento->saveAll($movimientos_items);
                                            
                                           
                                                                                                                                   
                                                                                                                               
                                          if( $this->Comprobante->getTipoComprobante($id_comprobante) == EnumTipoComprobante::Remito ){ //Si es REMITO ENTOCES DESCUENTO EL COMPROMETIDO
                                        
                                            
                                            $this->Comprobante->id = $id_comprobante;
                                            $this->Comprobante->saveField('id_estado_comprobante', EnumEstadoComprobante::Remitido);
                                            $this->Comprobante->saveField('definitivo',1);
                                            
                                            //$pi_items["Comprobante"]["id_tipo_comprobante"] =  EnumTipoMovimiento::Facturacion;
                                            
                                            $auxiliar["ComprobanteItem"] = array();
                                            
                                            foreach($comprobante_items as $key=>$item){
                                                
                                                array_push($auxiliar["ComprobanteItem"],$item["ComprobanteItem"]);
                                                unset($comprobante_items[$key]);
                                            }
                                            
                                            $comprobante = $this->Comprobante->find('first', array(
                                                    'conditions' => array('Comprobante.id' => $id_comprobante),
                                                    'contain' =>false
                                                    )); 
                                            
                                            $auxiliar["Comprobante"]["id_tipo_comprobante"] =  $comprobante["Comprobante"]["id_tipo_comprobante"]; 
                                            
                                            
                                           if($this->Comprobante->esImportado($auxiliar["ComprobanteItem"]) == 1 ) {
                                           
                                           	if(in_array($this->Comprobante->origenComprobante($auxiliar["ComprobanteItem"]),array(EnumTipoComprobante::FacturaCreditoA,EnumTipoComprobante::FacturaCreditoB,EnumTipoComprobante::FacturaCreditoC,EnumTipoComprobante::FacturaA,EnumTipoComprobante::FacturaB,EnumTipoComprobante::FacturaC,EnumTipoComprobante::PedidoInterno))) 
                                                $this->revertirItems($auxiliar);
                                           } 
                                            
                                        }    
                                            
                                        }
                                        
                                        
                                      
                                    
                                    
                                            
                            break;
                                                           //cancelo o cierre
                         
                        
                            case EnumTipoMovimiento::CanceloComprobante:  //si desea cancelar el pedido interno borrarlo 
                                
                                
                                        $this->loadModel("StockProducto");
                                        $this->loadModel("Comprobante");
                                        $this->loadModel("ComprobanteItem");
                                        
                                        $id_comprobante = $this->request->data["Movimiento"]["id_pedido_interno"];
                                        
                                        
                                        $id_deposito = $this->request->data["Movimiento"]["id_deposito_origen"];
                                        $id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
                                        
                                        $pi_items = $this->ComprobanteItem->find('all', array(
                                                    'conditions' => array('ComprobanteItem.id_comprobante' => $id_comprobante,'ComprobanteItem.activo'=>1),
                                                    'contain' =>false
                                                    ));
                                        
                                        foreach($pi_items as $prod_pedido_interno){ //por cada producto del PI actualizo el stock
                                                      $id_producto = $prod_pedido_interno["ComprobanteItem"]["id_producto"];
                                                      $cantidad = $prod_pedido_interno["ComprobanteItem"]["cantidad"];
                                                      
                                                      $total = 0;
                                                      
                                                  
                                                      $pi_items_factura = $this->FacturaItem->find('all', array(
                                                                'conditions' => array('FacturaItem.id_pedido_interno_item' => $prod_pedido_interno["id"]),
                                                                'contain' =>false
                                                                ));
                                            
                                
                                  
                                                      foreach($pi_items_factura as $item_entrega){
                                                        $total =  $total +    $item_entrega["FacturaItem"]["cantidad"];
                                                   
                                                        }
                                                      $pendiente = $prod_pedido_interno["PedidoInternoItem"]["cantidad"] - $total; 
                                                      
                                                      $this->StockProducto->updateAll(
                                                        array('StockProducto.stock_comprometido' => "StockProducto.stock_comprometido - ".$pendiente  ),
                                                        array('StockProducto.id' => $id_producto) ); 
                                                    
                                                    
                                                       $movimiento = array();
                                                       $movimientos = array();
                                                       $movimiento["id_comprobante"] = $id_comprobante;
                                                       $movimiento["cantidad"] = $cantidad;
                                                       $movimiento["id_comprobante_item"] = $id_comprobante_item;
                                                       $movimiento["id_movimiento_tipo"] = EnumTipoMovimiento::AgregoItemComprobante;
                                                       $movimiento["fecha"] = date("Y-m-d H:i:s");
                                                       
                                                       if($id_deposito > 0)
                                                        $movimiento["id_deposito_origen"] = $id_deposito;
                                                       
                                                       if($id_deposito_destino > 0)
                                                        $movimiento["id_deposito_destino"] = $id_deposito_destino;
                                                       
                                                       $movimiento["id_usuario"] = $id_usuario;
                                                       
                                                      array_push($movimientos,$movimiento);
                                                      
                                                      $this->Movimiento->Save($movimiento);    
                                                        
                                                     
                                        }
                             
                                    
                                    
                                            
                            break;
                                
                                
                                
                        
                           
                            case EnumTipoMovimiento::CanceloItemComprobante:  //si desea cancelar el item del pedido interno borrarlo 
                                
                                
                                        $this->loadModel("StockProducto");
                                        $this->loadModel("Comprobante");
                                        $this->loadModel("ComprobanteItem");
                                        
                                        $id_comprobante_item = $this->request->data["Movimiento"]["id_comprobante_item"];
                                        $id_deposito = $this->request->data["Movimiento"]["id_deposito_origen"];
                                        $id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
                                        
                                        $pi_item = $this->Comprobante->ComprobanteItem->find('first', array(
                                                    'conditions' => array('ComprobanteItem.id' => $id_comprobante_item),
                                                    'contain' =>false
                                                    ));
                                        
                                         if($pi_item){
                                                      $id_producto = $pi_item["ComprobanteItem"]["id_producto"];
                                                      $cantidad = $pi_item["ComprobanteItem"]["cantidad"] * -1;
                                                      $id_comprobante = $pi_item["ComprobanteItem"]["id_comprobante"];
                                                      
                                                     
                                                     if($id_deposito > 0)
                                                      $this->StockProducto->updateAll(
                                                        array('StockProducto.stock' => "StockProducto.stock - ".$cantidad  ),
                                                        array('StockProducto.id' => $id_producto,'StockProducto.id_deposito' => $id_deposito) ); 
                                                        
                                                    if($id_deposito_destino > 0)
                                                      $this->StockProducto->updateAll(
                                                        array('StockProducto.stock' => "StockProducto.stock + ".$cantidad  ),
                                                        array('StockProducto.id' => $id_producto,'StockProducto.id_deposito' => $id_deposito_destino) );     
                                                        
                                                        
                                                       $movimiento = array();
                                                       $movimientos = array();
                                                       $movimiento["id_comprobante"] = $id_comprobante;
                                                       $movimiento["cantidad"] = $cantidad;
                                                       $movimiento["id_comprobante_item"] = $id_comprobante_item;
                                                       $movimiento["id_movimiento_tipo"] = EnumTipoMovimiento::CanceloItemComprobante;
                                                       $movimiento["fecha"] = date("Y-m-d H:i:s");
                                                       
                                                       if($id_deposito > 0)
                                                        $movimiento["id_deposito_origen"] = $id_deposito;
                                                       
                                                       if($id_deposito_destino > 0)
                                                        $movimiento["id_deposito_destino"] = $id_deposito_destino;
                                                       
                                                       $movimiento["id_usuario"] = $id_usuario;
                                                       array_push($movimientos,$movimiento);
                                                       
                                                      
                                                      
                                                      $this->Movimiento->save($movimiento);
                                                     
                                       }
                                     
                                    
                                    
                                            
                            break;
                                
                          
                        
                            case EnumTipoMovimiento::AgregoItemComprobante:  //si desea cancelar el pedido interno borrarlo 
                                
                                
                                        $this->loadModel("StockProducto");
                                        $this->loadModel("Comprobante");
                                        $this->loadModel("ComprobanteItem");
                                        
                                        //$this->Movimiento->revertir(449);
                                        
                                        $id_comprobante_item = $this->request->data["Movimiento"]["id_comprobante_item"];
                                        $id_deposito = $this->request->data["Movimiento"]["id_deposito_origen"];
                                        $id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
                                        
                                        $pi_item = $this->Comprobante->ComprobanteItem->find('first', array(
                                                    'conditions' => array('ComprobanteItem.id' => $id_comprobante_item),
                                                    'contain' =>false
                                                    ));
                                        
                                        foreach($pi_item as $prod_pedido_interno){ //por cada producto del PI actualizo el stock
                                                      
                                                      $id_producto = $prod_pedido_interno["id_producto"];
                                                      $cantidad = $prod_pedido_interno["cantidad"];
                                                      $id_comprobante = $prod_pedido_interno["id_comprobante"];
                                                      
                                                      if($id_deposito>0){
                                                      $this->StockProducto->updateAll(
                                                        array('StockProducto.stock' => "StockProducto.stock - ".$cantidad  ),
                                                        array('StockProducto.id' => $id_producto,'StockProducto.id_deposito' => $id_deposito) ); 
                                                      }
                                                      
                                                         
                                                      if($id_deposito_destino>0){
                                                       $this->StockProducto->updateAll(
                                                        array('StockProducto.stock' => "StockProducto.stock + ".$cantidad  ),
                                                        array('StockProducto.id' => $id_producto,'StockProducto.id_deposito' => $id_deposito_destino) ); 
                                                     
                                                      }
                                                      
                                                     
                                                      
                                                       $movimiento = array();
                                                       
                                                       $movimientos = array();
                                                       $movimiento["id_comprobante"] = $id_comprobante;
                                                       $movimiento["cantidad"] =  $cantidad;
                                                       $movimiento["id_comprobante_item"] = $id_comprobante_item;
                                                       $movimiento["id_movimiento_tipo"] = EnumTipoMovimiento::AgregoItemComprobante;
                                                       $movimiento["fecha"] = date("Y-m-d H:i:s");
                                                       
                                                       if($id_deposito > 0)
                                                        $movimiento["id_deposito_origen"] = $id_deposito;
                                                       
                                                       if($id_deposito_destino > 0)
                                                        $movimiento["id_deposito_destino"] = $id_deposito_destino;
                                                       
                                                       $movimiento["id_usuario"] = $id_usuario;
                                                       
                                                      
                                                       //array_push($movimientos,$movimiento);
                                                      
                                                      $this->Movimiento->Save($movimiento);   
                                                      
                                        
                                        
                                                    
                                                     
                                        }
                             
                                    
                                    
                                            
                            break;
                                
                          
                            case EnumTipoMovimiento::AjusteNegativoComprobanteItem:  //si desea cancelar el pedido interno borrarlo 
                                
                                
                                        $this->loadModel("StockProducto");
                                        $this->loadModel("Comprobante");
                                        $this->loadModel("ComprobanteItem");
                                        
                                        $id_comprobante_item = $this->request->data["Movimiento"]["id_comprobante_item"];
                                        $id_deposito = $this->request->data["Movimiento"]["id_deposito_origen"];
                                        $id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
                                        $cantidad = $this->request->data["Movimiento"]["cantidad"];
                                        
                                        
                                        $pi_item = $this->Comprobante->ComprobanteItem->find('first', array(
                                                    'conditions' => array('ComprobanteItem.id' => $id_comprobante_item),
                                                    'contain' =>false
                                                    ));
                                        
                                         if($pi_item){
                                                      $id_producto = $pi_item["ComprobanteItem"]["id_producto"];
                                                      $id_comprobante = $pi_item["ComprobanteItem"]["id_comprobante"];
                                
                                                      
                                                      if($id_deposito>0)
                                                      $this->StockProducto->updateAll(
                                                        array('StockProducto.stock' => "StockProducto.stock - ".$cantidad  ),
                                                        array('StockProducto.id' => $id_producto,'StockProducto.id_deposito' => $id_deposito) ); 
                                                        
                                                      if($id_deposito_destino>0)
                                                      $this->StockProducto->updateAll(
                                                        array('StockProducto.stock' => "StockProducto.stock - ".$cantidad  ),
                                                        array('StockProducto.id' => $id_producto,'StockProducto.id_deposito' => $id_deposito_destino) ); 
                                                        
                                                      
                                                      $movimiento = array();
                                                      $movimientos = array();
                                                       $movimiento["id_comprobante"] = $id_comprobante;
                                                       $movimiento["cantidad"] = $cantidad;
                                                       $movimiento["id_comprobante_item"] = $id_comprobante_item;
                                                       $movimiento["id_movimiento_tipo"] = EnumTipoMovimiento::AjusteNegativoComprobanteItem;
                                                       $movimiento["fecha"] = date("Y-m-d H:i:s");
                                                       
                                                       if($id_deposito > 0)
                                                        $movimiento["id_deposito_origen"] = $id_deposito;
                                                       
                                                       if($id_deposito_destino > 0)
                                                        $movimiento["id_deposito_destino"] = $id_deposito_destino;
                                                       
                                                       $movimiento["id_usuario"] = $id_usuario;
                                                       
                                                      
                                                       //array_push($movimientos,$movimiento);
                                                      
                                                      $this->Movimiento->Save($movimiento);    
                                                        
                                                        
                                                     
                                       }
                             
                                    
                                    
                                            
                            break;
                               
                               
                               
                               
                          
                           case EnumTipoMovimiento::AjustePositivoComprobanteItem:  //si desea cancelar el pedido interno borrarlo 
                                
                                
                                        $this->loadModel("StockProducto");
                                        $this->loadModel("Comprobante");
                               
                                        
                                        $id_comprobante_item = $this->request->data["Movimiento"]["id_comprobante_item"];
                                        $id_deposito = $this->request->data["Movimiento"]["id_deposito_origen"];
                                        $id_deposito_destino = $this->request->data["Movimiento"]["id_deposito_destino"];
                                        $cantidad = $this->request->data["Movimiento"]["cantidad"];
                                        
                                        $pi_item = $this->Comprobante->ComprobanteItem->find('first', array(
                                                    'conditions' => array('ComprobanteItem.id' => $id_comprobante_item),
                                                    'contain' =>false
                                                    ));
                                        
                                         if($pi_item){
                                                      $id_producto = $pi_item["ComprobanteItem"]["id_producto"];
                                
                                                       $id_comprobante = $pi_item["ComprobanteItem"]["id_comprobante"];
                                
                                                      
                                                      if($id_deposito>0)
                                                      $this->StockProducto->updateAll(
                                                        array('StockProducto.stock' => "StockProducto.stock + ".$cantidad  ),
                                                        array('StockProducto.id' => $id_producto,'StockProducto.id_deposito' => $id_deposito) ); 
                                                        
                                                      if($id_deposito_destino>0)
                                                      $this->StockProducto->updateAll(
                                                        array('StockProducto.stock' => "StockProducto.stock + ".$cantidad  ),
                                                        array('StockProducto.id' => $id_producto,'StockProducto.id_deposito' => $id_deposito_destino) ); 
                                                        
                                                      
                                                      $movimiento = array();
                                                      $movimientos = array();
                                                       $movimiento["id_comprobante"] = $id_comprobante;
                                                       $movimiento["cantidad"] = $cantidad;
                                                       $movimiento["id_comprobante_item"] = $id_comprobante_item;
                                                       $movimiento["id_movimiento_tipo"] = EnumTipoMovimiento::AjustePositivoComprobanteItem;
                                                       $movimiento["fecha"] = date("Y-m-d H:i:s");
                                                       
                                                       if($id_deposito > 0)
                                                        $movimiento["id_deposito_origen"] = $id_deposito;
                                                       
                                                       if($id_deposito_destino > 0)
                                                        $movimiento["id_deposito_destino"] = $id_deposito_destino;
                                                       
                                                       $movimiento["id_usuario"] = $id_usuario;
                                                       
                                                      
                                                       array_push($movimientos,$movimiento);
                                                      
                                                      $this->Movimiento->Save($movimientos);    
                                                     
                                       }
                             
                                    
                                    
                                            
                           break; 
                              
                        }
                        
                       
                        
                      if($this->request->data["Movimiento"]["id_deposito_origen"] == "" && $this->request->data["Movimiento"]["id_deposito_destino"] == "")
                      	throw new Exception("No pudo completarse el movimiento ya que no tiene registrado ningun deposito");
                      else{  		
                        $ds->commit();
                      }
                           
                        }else{
                            $errores = $this->{$this->model}->validationErrors;
                            $errores_string = "";
                            foreach ($errores as $error){
                                $errores_string.= "&bull; ".$error[0]."\n";
                                
                            }
                            $mensaje = $errores_string;
                            $tipo = EnumError::ERROR; 
                            
                        }
                    }catch(Exception $e){
                        $ds->rollback();
                        $mensaje = "Ha ocurrido un error,el Movimiento no ha podido ser creado.".$e->getMessage();
                        $tipo = EnumError::ERROR;
                    }
            
            }else{
                $mensaje = "Ha ocurrido un error,el Movimiento no ha podido ser creado ya que existe un movimiento anterior para ese comprobante";
                $tipo = EnumError::ERROR;
                
            }
            
             
            if(!isset($this->request->data["Movimiento"]["llamada_interna"])){
                     $output = array(
                    "status" => $tipo,
                    "message" => $mensaje,
                    "content" => "",
                    "id_add" => $id_movimiento
                    );
                    
                   
                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content","id_add"));
            
            }else{
                
                return true;
            }
            
            /* esto es via web
            else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }  
            */   
            
        }
        
        //si no es un post y no es json
        /*
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
        */
    }
    
    
    public function revertirMovimiento($id_movimiento_padre = 0){
        
        
          if($id_movimiento_padre!=0){
          
              
              $movimiento_padre = $this->Movimiento->find('first',array(
                                'conditions' => array('Movimiento.id' => $id_movimiento_padre),
                                'contain' =>array('MovimientoTipo')
                            ));
              
              $data = array();
              $data["Movimiento"]["id_movimiento_tipo"] =  $movimiento_padre['MovimientoTipo']['id_movimiento_tipo_revertir'];
              $data["Movimiento"]["id_movimiento_padre"] = $id_movimiento_padre;
              
               //realizo la llamada al controller y le envio el movimiento
               $this->requestAction(
                    array('controller' => 'Movimientos', 'action' => 'add'),
                    array('data' => $data)
               );
               die();
               
          }
        
    }
    

 public function seiscientosunomasivo(){
     
                                 
                                 $this->loadModel("Componente");
                                 $this->loadModel("ArticuloRelacion");
                                 $this->loadModel("StockComponente");
                                 $this->loadModel("StockProducto");
                                 $this->loadModel("OrdenArmado");
                                
                             $oaarray = array(


55768
,55798
,55801
,55804
,55826
,55831
,55833
,55834
,55845
,55848
,55849
,55850
,55851
,55861
,55862
,55870
,55871
,55872
,55873
,55882
,55883
,55893
,55906
,55909
,55911
,55912
,55913
,55919
,55924
,55929
,55932
,55940
,55941
,55942
,55943
,55944
,55945
,55946
,55952
,55953
,55977
,55998
,56000
,56001
,56004
,56026
,56044
,56052
,56053
,56054
,56068
,56069
,56071
,56072
,56120
,56185
,56207
,56208
,56209
,56235
,56256
,56359
,56361
,56369
,56370
,56371
,56390
,56436
,56489
,56490);
                             
                             foreach($oaarray as $idoaitem ){
                             
                                $movimiento = array();
                                $movimiento["Movimiento"]["fecha"]= date("Y-m-d H:i:s");
                                $movimiento["Movimiento"]["id_usuario"]= 26;
                                $movimiento["Movimiento"]["id_movimiento_tipo"]= 601;
                                $movimiento["Movimiento"]["id_orden_armado"]= $idoaitem;
                                $movimiento["Movimiento"]["observacion"]= "masivo";
                                 
                                
                                $id_movimiento = '';
            
                                
                                $mov = $this->Movimiento->find('first', array(
                                'conditions' => array('Movimiento.id_comprobante' => $idoaitem,'Movimiento.id_movimiento_tipo'=>601),
                                'contain' =>false
                                ));
            
       
                                if(count($mov)>0){     
                
                                            $this->Movimiento->saveAll($movimiento);//guardo el movimiento
                                         
                                            $id_movimiento = $this->Movimiento->id;
                                            
                                             
                                             $oa = $this->OrdenArmado->find('first', array(
                                            'conditions' => array('OrdenArmado.id' => $idoaitem),
                                            'contain' =>false
                                            ));
                                            
                                            $cantidad = $oa["OrdenArmado"]["cantidad"];
                                            $cantidad_armada_cierre = $oa["OrdenArmado"]["cantidad_armada_cierre"];//es la cantidad que realmente se hizo
                                            
                                            
                                            //logueo la cantidad en el movimiento
                                            $this->Movimiento->updateAll(
                                                    array('Movimiento.cantidad' => $cantidad ),
                                                    array('Movimiento.id' => $id_movimiento) ); 
                                                    
                                                    
                                            $cantidad_a_volver_atras = $cantidad -   $cantidad_armada_cierre;
                                        
                                            $id_producto = $oa["OrdenArmado"]["id_producto"];
                                            
                                        
                                            
                                            //busco los componentes del producto
                                            $prod_item = $this->ArticuloRelacion->find('all', array(
                                                        'conditions' => array('ArticuloRelacion.id_producto' => $id_producto),
                                                        'contain' =>false
                                                        ));
                                             
                                             
                                              if($prod_item){ //por cada producto encontrado actualizo el stock
                                              
                                              //por cada componente actualizo su stock
                                                 foreach($prod_item as $componente){
                                                  
                                                  $id_componente = $componente["ArticuloRelacion"]["id_componente"];
                                                  $cantidad_consume = $componente["ArticuloRelacion"]["cantidad"];
                                                  
                                                  $this->StockComponente->updateAll(
                                                    array("StockComponente.stock_comprometido" => "StockComponente.stock_comprometido - ".$cantidad_consume*$cantidad_armada_cierre  ),
                                                    array('StockComponente.id' => $id_componente) ); 
                                                 
                                                 }
                                             }
                                             //incremento StockProducto en la cantidad que realmente arme
                                             $this->StockProducto->updateAll(
                                                    array("StockProducto.stock" => "StockProducto.stock + ".$cantidad  ),
                                                    array('StockProducto.id' => $id_producto) );
                                             
                                            
                                            //genero movimiento 602 por aquellos componentes que no fueron usados xq se hizo menos
                                            
                                            if($cantidad_a_volver_atras > 0){
                                                
                                                 if($prod_item){ //por cada producto encontrado actualizo el stock
                                              
                                              //por cada componente actualizo su stock
                                                 foreach($prod_item as $componente){
                                                  
                                                  $id_componente = $componente["ArticuloRelacion"]["id_componente"];
                                                  $cantidad_consume = $componente["ArticuloRelacion"]["cantidad"];
                                                  
                                                  $this->StockComponente->updateAll(
                                                    array("StockComponente.stock_comprometido" => "StockComponente.stock_comprometido - ".$cantidad_consume*$cantidad_a_volver_atras,"StockComponente.stock" => "StockComponente.stock + ".$cantidad_consume*$cantidad_a_volver_atras  ),
                                                    array('StockComponente.id' => $id_componente) ); 
                                                 
                                                 }
                                             }
                                            }
                                            
                                
                              }  
     
     }
     
 }  
 
 
   /**
    * @secured(CONSULTA_MOVIMIENTO)
    */
    public function getModel($vista = 'default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER

            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }

        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
            $this->set('model',$model);
            // $this->set('this',$this);
            Configure::write('debug',0);
            $this->render($vista);
        }
    
    
    protected function revertirItems($items){
    
    $this->loadModel("Movimiento");
    $this->loadModel("Modulo");
    
      $datoEmpresa = $this->Session->read('Empresa'); //Leo los datos de datoEmpresa en session cargados en Usuarios
      
      
     /*
     
     Se usa solo para factura por ahora para revertir el comprometido requiere un flag (revertir_items_origen) en 1 para ser ejecutado 
                    ademas chequea si los items ya fueron revertidos, esta definida en movimientos model 
     recibe el data porque usa los COmprobanteItems

     */
                    
     $array_items = array();               
     if( $this->Modulo->estaHabilitado(EnumModulo::STOCK) == 1 && isset($items)   ) {
        
    
        $this->Movimiento->revertirItems($items);
           
     }
}



protected function existeMovimientoAnteriorPorComprobante($id_comprobante,$id_movimiento_tipo){
 
    
    
    $this->loadModel("Movimiento");
        
        
       switch($id_movimiento_tipo){ 
        
      
        case EnumTipoMovimiento::Comprobante:
        
            $existe = $this->Movimiento->find('count',array('conditions' => array(
                                                                            'Movimiento.id_comprobante' => $id_comprobante,
                                                                            'Movimiento.id_movimiento_tipo' =>$id_movimiento_tipo
                                                                            )
                                                                            ));
             return $existe;
             break;
        default:
                return 0;
                break;
        

        
        
        }
        
        
      
    
}



public function Masivo302(){
    
    
    $this->loadModel("StockProducto");
    $this->loadModel("Comprobante");
    $this->loadModel("ComprobanteItem");
    $this->loadModel("Movimiento");
    
    
    
    $array_comprobantes = array(13345 ,13352,13354,13356,13358,13362,13365,13367,13373,13377,13379,13382,13387,13389,13393,13395,13397,
                            13403,13408,13426,13430,13433,13435,13438,13447,13449,13451,13453,13459,13461,13464,13466,13469,13524,
                            13527,13529,13531,13533,13535,13538,13541,13543,13545,13549,13551,13553,13556,13561,13563,13565,13572,
                            13582,13585,13590,13618,13620,13625,13645,13647,13649,13652,13658,13664,13666,13674,13676,13759,13761,
                            13766,13768,13771,13775,13777,13779,13781,13783,13785,13788,13790,13792,13796,13798,13800,13802,13805,
                            13807,13809,13811,13813,13815,13817,13819,13821,13823,13828,13836,13839,13855,13857,13860,13867,13870,
                            13873,13875,13878,13880,13882,13884,13886,13889,13891,13893,13900,13904,13907,13911,13928,13930,13932,
                            13936,13938,13940,13953,13978,13981,13983,13985,13989,13991,13993,14022,14024,14027,14029,14032,14035,
                            14037,14039,14055,14057,14061,14063,14066,14068,14071,14075,14078,14080,14083,14100,14106,14114,14116,
                            14118,14152,14154,14156,14158,14161,14164,14166,14168,14170,14172,14176,14179,14216,14221,14223,14225,
                            14229,14232,14234,14238,14240,14246,14250,14252,14255,14258,14293,14298,14301,14307,14310,14312,14318,
                            14321,14323,14325,14327,14329,14331,14333,14335,14338,14340,14345,14357,14359,14361,14385,14387,14389,
                            14391,14393,14395,14397,14399,14403,14406,14408,14410,14414,14417,14419,14426,14428,14434,14436,14438,
                            14441,14443,14447,14450,14452,14454,14457,14463,14465,14486,14489,14491,14494,14497,14499,14501,14507,
                            14515,14517,14519,14521,14523,14525,14527,14529,14531,14533,14535,14538,14540,14542,14544,14546,14550,
                            14552,14557,14577,14590,14594,14596,14598,14600,14616,14618,14620,14622,14625,14629,14658,14660,14666,
                            14690,14724,14726,14728,14730,14739,14741,14743,14745,14747,14749,14751,14753,14755,14760,14763,14765,
                            14767,14769,14771,14774,14776,14784,14798,14800,14802,14804,14807,14814,14823,14825,14827,14829,14840,
                            14843,14845,14847,14851,14853,14855,14857,14859,14861,14863,14865,14867,14870,14872,14896,14898,14912,
                            14914,14916,14918,14921,14924,14932,14942,14946,14955,14982,14988,14991,14993,14995,15002,15005,15007,
                            15015,15019,15021,15023,15058,15065,15068,15081,15083,15085,15088,15092,15094,15097,15099,15106,15108,
                            15115,15117,15123,15125,15130,15134,15136,15146,15148,15152,15154,15156,15158,15160,15162,15164,15166,15168);
    
    
    foreach ($array_comprobantes as $id_comprobante){
        
       $this->request->data = array(); 
       
       $this->request->data["Movimiento"]["fecha"]= date("Y-m-d H:i:s"); 
       $this->request->data["Movimiento"]["id_usuario"]= $this->Auth->user('id');
       $this->request->data["Movimiento"]["id_comprobante"] = $id_comprobante;
       $this->request->data["Movimiento"]["id_movimiento_tipo"] = 302;
       $this->request->data["Movimiento"]["id_deposito_origen"] = 4;
       $this->request->data["Movimiento"]["id_comprobante"] = $id_comprobante;
       
       $this->Movimiento->saveAll($this->request->data);
        
        
        
                                    
                                        
                                        $id_deposito_origen = 4;
                                        $id_deposito_destino = '';
                                        
                                        
                                        
                                      
                                        
                                        if(isset($this->request->data["Movimiento"]["simbolo_operacion"]))
                                            $simbolo_operacion = $this->request->data["Movimiento"]["simbolo_operacion"];
                                        else
                                            $simbolo_operacion = 1;
                                        
                                        
                                        $comprobante_items = $this->ComprobanteItem->find('all', array(
                                                    'conditions' => array('ComprobanteItem.id_comprobante' => $id_comprobante,'ComprobanteItem.activo'=>1),
                                                    'contain' =>false
                                                    ));
                                                    
                                                    
                                        $movimientos_items = array();
                                        
                                        if($comprobante_items){
                                            foreach($comprobante_items as $prod_pedido_interno){ //por cada producto del PI actualizo el stock
                                                          $id_producto = $prod_pedido_interno["ComprobanteItem"]["id_producto"];
                                                          $cantidad = $prod_pedido_interno["ComprobanteItem"]["cantidad"];
                                                          
                                                          
                                                          if($id_deposito_origen > 0)
                                                            $this->StockProducto->updateAll(
                                                                array('StockProducto.stock' => "StockProducto.stock -".$cantidad*$simbolo_operacion  ),
                                                                array('StockProducto.id' => $id_producto,'StockProducto.id_deposito'=>$id_deposito_origen) );
                                                            
                                                           
                                                           if($id_deposito_destino > 0)
                                                            $this->StockProducto->updateAll(
                                                                array('StockProducto.stock' => "StockProducto.stock +".$cantidad*$simbolo_operacion  ),
                                                                array('StockProducto.id' => $id_producto,'StockProducto.id_deposito'=>$id_deposito_destino) );
                                                                
                                                                
                                                                
                                                     
                                                           $movimiento = array();
                                                           $movimiento["id_comprobante"] = $id_comprobante;
                                                           $movimiento["cantidad"] = $cantidad;
                                                           $movimiento["id_comprobante_item"] = $prod_pedido_interno["ComprobanteItem"]["id"];
                                                           $movimiento["id_movimiento_tipo"] = EnumTipoMovimiento::AjustePositivoComprobanteItem;
                                                           $movimiento["fecha"] = date("Y-m-d H:i:s");
                                                           if($id_deposito_origen > 0)
                                                            $movimiento["id_deposito_origen"] = $id_deposito_origen;
                                                           if($id_deposito_destino > 0)
                                                            $movimiento["id_deposito_destino"] = $id_deposito_destino;
                                                           
                                                           $movimiento["id_usuario"] = $id_usuario;
                                                           array_push($movimientos_items,$movimiento);
                                                         
                                            }
                                            
                                            if(count($movimientos_items))
                                            	$this->Movimiento->saveAll($movimientos_items);
                                            
                                           
                                                                                                                                   
                                                                                                                               
                                          if($this->request->data["Movimiento"]["id_movimiento_tipo"] == EnumTipoMovimiento::Comprobante){ //Si es REMITO ENTOCES DESCUENTO EL COMPROMETIDO
                                        
                                            
                                            $this->Comprobante->id = $id_comprobante;
                                            $this->Comprobante->saveField('id_estado_comprobante', EnumEstadoComprobante::Remitido);
                                            $this->Comprobante->saveField('definitivo', 1);
                                            
                                            //$pi_items["Comprobante"]["id_tipo_comprobante"] =  EnumTipoMovimiento::Facturacion;
                                            
                                            $auxiliar["ComprobanteItem"] = array();
                                            
                                            foreach($comprobante_items as $key=>$item){
                                                
                                                array_push($auxiliar["ComprobanteItem"],$item["ComprobanteItem"]);
                                                unset($comprobante_items[$key]);
                                            }
                                            
                                            $comprobante = $this->Comprobante->find('first', array(
                                                    'conditions' => array('Comprobante.id' => $id_comprobante),
                                                    'contain' =>false
                                                    )); 
                                            
                                            $auxiliar["Comprobante"]["id_tipo_comprobante"] =  $comprobante["Comprobante"]["id_tipo_comprobante"]; 
                                            
                                            
                                           if($this->Comprobante->esImportado($auxiliar["ComprobanteItem"]) == 1 ) {
                                           
                                           	if(in_array($this->Comprobante->origenComprobante($auxiliar["ComprobanteItem"]),array(EnumTipoComprobante::FacturaCreditoA,EnumTipoComprobante::FacturaCreditoB,EnumTipoComprobante::FacturaCreditoC,EnumTipoComprobante::FacturaA,EnumTipoComprobante::FacturaB,EnumTipoComprobante::FacturaC,EnumTipoComprobante::PedidoInterno))) 
                                                $this->revertirItems($auxiliar);
                                           } 
                                            
                                        }    
                                            
                                        }
        
        
        
        
        
    }
}




/**
 * @secured(BTN_EXCEL_MOVIMIENTOS)
 */
public function excelExport($vista="default",$metodo="index",$titulo=""){
	
	parent::excelExport($vista,$metodo,"Listado de Movimientos");
	
	
	
}

    
    


}
?>