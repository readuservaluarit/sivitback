<?php
	$model["codigo"]["show_grid"] = 1;
	$model["codigo"]["header_display"] = "[C&oacute;digo]";
	$model["codigo"]["width"] = "10";
	$model["codigo"]["order"] = "0";
	$model["codigo"]["text_colour"] = "#000000";

	$model["d_condicion_pago"]["show_grid"] = 1;
	$model["d_condicion_pago"]["header_display"] = "Descripci&oacute;n";
	$model["d_condicion_pago"]["width"] = "30";
	$model["d_condicion_pago"]["order"] = "1";
	$model["d_condicion_pago"]["text_colour"] = "#000000";
	

	$output = array(
			"status" =>EnumError::SUCCESS,
			"message" => "CondicionPago",
			"content" => $model
		);

	echo json_encode($output);
?>