<?php
App::uses('AppModel', 'Model');
App::uses('SistemaParametro', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class Cheque extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'cheque';

    public $actsAs = array('Containable');
    
    

/**
 * Validation rules
 *
 * @var array
 */
	/*
    public $validate = array(
		'cheque_desde' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar el comienzo de la cartera',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);
    */
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 public $belongsTo = array(
        'PersonaIngreso' => array(
            'className' => 'Persona',
            'foreignKey' => 'id_persona_ingreso',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'PersonaEgreso' => array(
            'className' => 'Persona',
            'foreignKey' => 'id_persona_egreso',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Banco' => array(
            'className' => 'Banco',
            'foreignKey' => 'id_banco',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'BancoSucursal' => array(
            'className' => 'BancoSucursal',
            'foreignKey' => 'id_banco_sucursal',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'EstadoCheque' => array(
            'className' => 'EstadoCheque',
            'foreignKey' => 'id_estado_cheque',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'TipoCheque' => array(
            'className' => 'TipoCheque',
            'foreignKey' => 'id_tipo_cheque',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'Moneda' => array(
            'className' => 'Moneda',
            'foreignKey' => 'id_moneda',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Chequera' => array(
            'className' => 'Chequera',
            'foreignKey' => 'id_chequera',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'ComprobanteEntrada' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante_entrada',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'ComprobanteSalida' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante_salida',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'ComprobanteRechazo' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante_rechazo',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Chequera' => array(
            'className' => 'Chequera',
            'foreignKey' => 'id_chequera',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        
        
);


public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     "d_tipo_cheque"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_banco"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "id_banco"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_banco_sucursal"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "id_banco_sucursal"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_moneda"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_estado_cheque"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_comprobante_entrada"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_comprobante_salida"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_comprobante_rechazo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
	 "diferido"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "conciliado"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "razon_social_comprobante_entrada"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "razon_social_comprobante_salida"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     );

}


function ultimoNumeroChequePropio($id_chequera,$suma='+'){
	
    $chequera = ClassRegistry::init('Chequera');
    try{
    
        $this->query("UPDATE chequera SET numero_actual = LAST_INSERT_ID(numero_actual ".$suma." 1)  WHERE id=".$id_chequera."");
        $result = $this->query("SELECT LAST_INSERT_ID() as numero_cheque;");
        $numero_cheque =$result[0][0]["numero_cheque"];
    
    
      $resultado_chequera = $chequera->find('first', array(
											  'conditions' => array('Chequera.id' => $id_chequera), 
											  'fields' => array('cheque_hasta')
											  )
                     );
      
      
      
      /*El cheque que me asigno el sistem ya fue usado? ya que pudo haberlo cargado manualmente por q' no cargo secuencial*/
      $cheque = $this->find('count', array(
      		'conditions' => array('Cheque.nro_cheque' => $numero_cheque,'Cheque.id_chequera' => $id_chequera,'Cheque.id_tipo_cheque'=>EnumTipoCheque::Propio,'NOT'=>array('Cheque.id_estado_cheque'=>EnumEstadoCheque::Anulado)),
      	
      )
      		);
      
      
      
      if($cheque == 0){
	      if($numero_cheque <= $resultado_chequera["Chequera"]["cheque_hasta"] ){      //todavia puedo usar el cheque retorno valor valido
	          
	          return $numero_cheque;   
	      }else{
	          
	          /*Le retorno 1 al valor o sea quedaria en cheque_hasta*/
	          $this->query("UPDATE chequera SET numero_actual = LAST_INSERT_ID(numero_actual  - 1)  WHERE id=".$id_chequera."");
	          return -1; //esta chequera no tiene mas cheques disponibles
	          
	      }
      }else{
      	// el cheque que selecciono el algoritmo ya fue usado asique no puede asignarse automaticamente
      	$this->query("UPDATE chequera SET numero_actual = LAST_INSERT_ID(numero_actual  - 1)  WHERE id=".$id_chequera."");
      	return -1;
      }
    
    }catch(Exception $ex){
      // $numero_comprobante = 0;
      
      return -1;
      
   }
  
   
    
    
}


public function validarIngresoCheque($data,&$cheques_error){
	
	
	
	App::uses('ComprobanteValor', 'Model');
	$cpv_obj = new ComprobanteValor();
	
	
	
	App::uses('Comprobante', 'Model');
	$comprobante_obj = new Comprobante();
   
   $cheques_error = '';
    
   if($data && $this->tieneCheques($data) == 1 ){
   
       $cheques = $this->getArrayCheques($data);
       
       if(isset($data["Comprobante"]["id"]))
        $id_comprobante_origen = $data["Comprobante"]["id"];
       else
          $id_comprobante_origen = 0;
       
       
       foreach($cheques as $cheque){ 
           
        /*Si alguno de los cheques elegidos esta asignado ERROR*/   
        $resultado = $this->find('first', array(
                                                    'conditions' => array('Cheque.id' => $cheque['id']),
                                                    'contain' =>false
                      
        		
        ));
        
        
        
        $resultado_cpv = $cpv_obj->find('first', array(
        		'conditions' => array('ComprobanteValor.id_cheque' => $cheque['id'],'Comprobante.definitivo'=>0,'Comprobante.id <>'=>$id_comprobante_origen),
        		'contain' =>array('Comprobante'=>array('TipoComprobante','PuntoVenta'))
        ));
        
        
        if($resultado_cpv){
        	
        	if(isset($resultado_cpv["ComprobanteValor"]["id"]) && $resultado_cpv["ComprobanteValor"]["id"]>0) {
        		
        		$pto_nro_comp = $comprobante_obj->GetNumberComprobante($resultado_cpv['Comprobante']['PuntoVenta']['numero'],$resultado_cpv['Comprobante']['nro_comprobante']);
        		$tipo_comprobante = $resultado_cpv['Comprobante']['TipoComprobante']['d_tipo_comprobante'];
        		$cheques_error = "Un cheque utilizado en el comprobante ya se encuenta asignado a un comprobante que no es definitivo. Nro Cheque:".$resultado["Cheque"]["nro_cheque"]."Nro Comprobante: ".$tipo_comprobante.' '.$pto_nro_comp;
        		return 0;
        	}
        	
        }
        
        
        
        if($resultado){
            
            if( $resultado["Cheque"]["id_comprobante_entrada"]>0 && $resultado["Cheque"]["id_comprobante_entrada"]!= $id_comprobante_origen){
                $cheques_error = "Un cheque utilizado en el comprobante ya se encuenta asignado Nro Cheque:".$resultado["Cheque"]["nro_cheque"];
                return 0; 
             }
            
       }
      }
        
 }   
   
   return 1; 
}


public function validarEgresoCheque($data,&$cheques_error){
	
	
	
	App::uses('ComprobanteValor', 'Model');
	$cpv_obj = new ComprobanteValor();
	
	
	
	App::uses('Comprobante', 'Model');
	$comprobante_obj = new Comprobante();
   
   $cheques_error = '';
    
   if($data && $this->tieneCheques($data) == 1 ){
   
       $cheques = $this->getArrayCheques($data);
       
       if(isset($data["Comprobante"]["id"]))
        $id_comprobante_origen = $data["Comprobante"]["id"];
       else
          $id_comprobante_origen = 0;
       
       
       foreach($cheques as $cheque){ 
           
        /*Si alguno de los cheques elegidos esta asignado ERROR*/   
        $resultado = $this->find('first', array(
                                                    'conditions' => array('Cheque.id' => $cheque['id']),
                                                    'contain' =>false
                                                    ));
        
        
        
        $resultado_cpv = $cpv_obj->find('first', array(
        		'conditions' => array('ComprobanteValor.id_cheque' => $cheque['id'],'Comprobante.definitivo'=>0,'Comprobante.id <>'=>$id_comprobante_origen),
        		'contain' =>array('Comprobante'=>array('TipoComprobante','PuntoVenta'))
        ));
        
        
        
       
        
        if($resultado_cpv){
        	
        	if(isset($resultado_cpv["ComprobanteValor"]["id"]) && $resultado_cpv["ComprobanteValor"]["id"]>0) {
        		
        		$pto_nro_comp = $comprobante_obj->GetNumberComprobante($resultado_cpv['Comprobante']['PuntoVenta']['numero'],$resultado_cpv['Comprobante']['nro_comprobante']);
        		$tipo_comprobante = $resultado_cpv['Comprobante']['TipoComprobante']['d_tipo_comprobante'];
        		$cheques_error = "&bull; Un cheque utilizado en el comprobante ya se encuenta asignado a un comprobante que no es definitivo. Nro Cheque: ".$resultado["Cheque"]["nro_cheque"]."\n Nro Comprobante: ".$tipo_comprobante.' '.$pto_nro_comp;
        		return 0;
        	}
        	
        }
        
        
        if($resultado){
            
            if( $resultado["Cheque"]["id_comprobante_salida"]>0 && $resultado["Cheque"]["id_comprobante_salida"]!= $id_comprobante_origen){
            	
            
                $cheques_error = "Un cheque utilizado en el comprobante ya se encuenta asignado. Nro Cheque:".$resultado["Cheque"]["nro_cheque"];
                return 0; 
             }
            
       }
      }
        
 }   
   
   return 1; 
}


public function validarRechazoCheque($data,&$cheques_error){
	
	$cheques_error = '';
	
	if($data && $this->tieneCheques($data) == 1 ){
		
		$cheques = $this->getArrayCheques($data);
		
		if(isset($data["Comprobante"]["id"]))
			$id_comprobante_origen = $data["Comprobante"]["id"];
			else
				$id_comprobante_origen = 0;
				
				
				foreach($cheques as $cheque){
					
					/*Si alguno de los cheques elegidos esta asignado ERROR*/
					$resultado = $this->find('first', array(
							'conditions' => array('Cheque.id' => $cheque['id']),
							'contain' =>false
					));
					if($resultado){
						
						if( $resultado["Cheque"]["id_comprobante_rechazo"]>0 && $resultado["Cheque"]["id_comprobante_rechazo"]!= $id_comprobante_origen){
							$cheques_error = "Un cheque utilizado en el comprobante ya se encuenta asignado. Nro Cheque:".$resultado["Cheque"]["nro_cheque"];
							return 0;
						}
						
					}
				}
				
	}
	
	return 1;
}

public function tieneCheques($data){
    
  if(isset($data["ComprobanteValor"])){
      
      foreach($data["ComprobanteValor"] as $valor){
          if($valor["id_valor"] == EnumValor::CHEQUE)
            return 1;
          
      }
      
  }  
    
    
    return 0;
}


public function getArrayCheques($data){  /*le pasas un request->data y te devuelve un array con los cheques*/
   
    $array_cheques = array();
    
   foreach($data["ComprobanteValor"] as $valor){
       
          if($valor["id_valor"] == EnumValor::CHEQUE){ 
            $cheque["id"] = $valor["id_cheque"];
            array_push($array_cheques,$cheque);
            }
   }
   
   return $array_cheques;  
}

public function actualizaComprobanteCheques($id_comprobante,$array_cheques,$campo_actualizar_cheque){
   $array_cheques_formateado = array();
   
   foreach($array_cheques as $cheque){
       
       array_push($array_cheques_formateado,$cheque["id"]);
   }
   
    
   
    
    
     $this->updateAll(
                                  array('Cheque.'.$campo_actualizar_cheque => $id_comprobante ),
                                  array('Cheque.id' => $array_cheques_formateado) );
    
    
}


public function actualizaEstadoCheques($id_estado_cheque,$array_cheques){
     $array_cheques_formateado = array();
     
     
     foreach($array_cheques as $cheque){
       
         if(!isset($cheque["id"]))
            array_push($array_cheques_formateado,$cheque);
         else
            array_push($array_cheques_formateado,$cheque["id"]);
   }
   
     $this->updateAll(
                                  array('Cheque.id_estado_cheque' => $id_estado_cheque ),
                                  array('Cheque.id' => $array_cheques_formateado) );
   
    
}


  public function SeparaCheques($array_cheques,&$array_cheque_terceros,&$array_cheque_propios){
        
      
     
     
        foreach($array_cheques as $cheque){
           
          if( $this->getTipoCheque($cheque["id"]) == EnumTipoCheque::Terceros )
            array_push($array_cheque_terceros,$cheque["id"]);
          else//es propio
            array_push($array_cheque_propios,$cheque["id"]);
            
        } 
        
        
        
        
     
        
        
    }
    
    
    function getTipoCheque($id_cheque){
     
     
     $cheque = $this->find('first', array(
                                                'conditions' => array('Cheque.id' => $id_cheque),
                                                'contain' =>false
                                                ));
                                                
       return $cheque["Cheque"]["id_tipo_cheque"];
 }
 
 
 
 /*A esta funcion le pasas un id de un cheque y retorna la cuenta contable del banco asociada a la cuenta bancaria de cuando el cheque fue depositado*/
 function getCuentaContableChequePorRechazo($id_cheque,$id_detalle_tipo_comprobante,$id_tipo_persona = '',$id_sistema=EnumSistema::VENTAS)
 
 {
     
     
     switch($id_detalle_tipo_comprobante){
         
     case EnumDetalleTipoComprobante::TercerosEntregadoaProveedorSinND:    
     
         
       
         $sistema_parametro = new SistemaParametro();
         
         $resultado = $sistema_parametro->find('first', array(
                                                    'conditions' => array('SistemaParametro.id_sistema' => $id_sistema),
                                                    'contain' =>false
                                                    ));
       
         
             
         return $resultado["SistemaParametro"]["id_cuenta_contable_cuenta_corriente"];
         
        
         
        /* App::uses('CakeSession', 'Model/Datasource');
         $datoEmpresa = CakeSession::read('Empresa');
         return $datoEmpresa["DatoEmpresa"]["id_cuenta_contable_cheque_cartera"]; */
     break;
     
      
     case EnumDetalleTipoComprobante::Propiossalidaporegreso:
        $cheque = $this->find('first', array(
                                                    'conditions' => array('Cheque.id' => $id_cheque),
                                                    'contain' =>array('ComprobanteSalida'=>array('ComprobanteValor'=>array("CuentaBancaria","Cheque"=>array("Chequera"=>array("CuentaBancaria")) )))
                                                    ));
         foreach($cheque["ComprobanteSalida"]["ComprobanteValor"] as $valor){
             
             if($valor["id_valor"] == EnumValor::CHEQUE && $valor["id_cheque"] == $id_cheque){
                 
                 return   $id_cuenta_contable = $valor["Cheque"]["Chequera"]["CuentaBancaria"]["id_cuenta_contable"];    
             }
             
             
         }
     break;
     
     
     
     case EnumDetalleTipoComprobante::TercerosEntregadoaProveedorconND:
     
        //ComprobanteEntrada va a ser un COBRO(RECIBO)
        //ComprobanteSalida un PAGO
       
       
        $cheque = $this->find('first', array(
                                                    'conditions' => array('Cheque.id' => $id_cheque),
                                                    'contain' =>array('ComprobanteEntrada'=>array('Asiento'=>array('AsientoCuentaContable')),'ComprobanteSalida'=>array('Asiento'=>array('AsientoCuentaContable') ) )
                                                    ));
        
        if($id_tipo_persona == EnumTipoPersona::Proveedor){    //debo buscar la cuenta DEBE en el caso del proveedor
             
            App::uses('CakeSession', 'Model/Datasource');
             $datoEmpresa = CakeSession::read('Empresa');
             if($datoEmpresa["DatoEmpresa"]["id_cuenta_contable_cheque_rechazado"] > 0)
                return $datoEmpresa["DatoEmpresa"]["id_cuenta_contable_cheque_rechazado"]; 
             else
                 return $datoEmpresa["DatoEmpresa"]["id_cuenta_contable_cheque_cartera"];  
         
       }
       
        if($id_tipo_persona == EnumTipoPersona::Cliente){   
             
                    //devuelvo la CC CLIENTE
                    
        $sistema_parametro = new SistemaParametro();
         
         $resultado = $sistema_parametro->find('first', array(
                                                    'conditions' => array('SistemaParametro.id_sistema' => EnumSistema::VENTAS),
                                                    'contain' =>false
                                                    ));
       
         
             
         return $resultado["SistemaParametro"]["id_cuenta_contable_cuenta_corriente"];
         
       }  
     break;
     
     
       case EnumDetalleTipoComprobante::Tercerossalidaporegreso:
       
       //leer variable DatoEmpresa y devolver la Cuenta de Cheques
       App::uses('CakeSession', 'Model/Datasource');
       $datoEmpresa = CakeSession::read('Empresa');
       
       
       if($datoEmpresa["DatoEmpresa"]["id_cuenta_contable_cheque_rechazado"] > 0)
                return $datoEmpresa["DatoEmpresa"]["id_cuenta_contable_cheque_rechazado"]; 
             else
                 return $datoEmpresa["DatoEmpresa"]["id_cuenta_contable_cheque_cartera"];
       
       break;
       
       
       
     
     case EnumDetalleTipoComprobante::ChequePropioRechazado:
     	
     	App::uses('Cheque', 'Model');
     	$cheque_obj = new Cheque();  
     	
     	

     	$cheque = $cheque_obj->find('first', array(
                                                    'conditions' => array('Cheque.id' => $id_cheque),
                                                    'contain' =>array('ComprobanteSalida'=>array('ComprobanteValor'=>array("CuentaBancaria","Cheque"=>array("Chequera"=>array("CuentaBancaria")) )))
                                                    ));
       
     	
         foreach($cheque["ComprobanteSalida"]["ComprobanteValor"] as $valor){
             
             if($valor["id_valor"] == EnumValor::CHEQUE && $valor["id_cheque"]== $id_cheque){
                 
                 return   $id_cuenta_contable = $valor["Cheque"]["Chequera"]["CuentaBancaria"]["id_cuenta_contable"];    
             }
             
             
         }
     break;
     
     
     
     
     case EnumDetalleTipoComprobante::DetercerosRechazadoporBanco:
     	
     	App::uses('Cheque', 'Model');
     	$cheque_obj = new Cheque();  
     	
     	
     	$cheque = $cheque_obj->find('first', array(
                                                    'conditions' => array('Cheque.id' => $id_cheque),
                                                    'contain' =>array('ComprobanteSalida'=>array('ComprobanteValor'=>array("CuentaBancaria")))
                                                    ));
      //DEBO DEVOLVER LA CUENTA CONTABLE DE LA CUENTA BANCARIA EN LA CUAL LO DEPOSITE
      
      foreach($cheque["ComprobanteSalida"]["ComprobanteValor"] as $valor){
             
             if($valor["id_valor"] == EnumValor::DEPOSITOS){
                 
                 return   $id_cuenta_contable = $valor["CuentaBancaria"]["id_cuenta_contable"];    
             }
             
             
         }
     break;
     
     
     
     }
     
     
       
     
 }
 
 
 /*A esta funcion le pasas un id de un cheque y retorna la cuenta contable del banco asociada a la cuenta bancaria de cuando el cheque fue depositado*/
 function getCuentaContablePrincipalPorAsiento($id_cheque,$id_detalle_tipo_comprobante,$id_tipo_persona = '')
 
 {
     
     
     switch($id_detalle_tipo_comprobante){
         
         case EnumDetalleTipoComprobante::DetercerosRechazadoporBanco:
         
                 
              /*   
                 
                 $cheque = $this->find('first', array(
                                                    'conditions' => array('Cheque.id' => $id_cheque),
                                                    'contain' =>array('ComprobanteEntrada'=>array('Asiento'=>array('AsientoCuentaContable')),'ComprobanteSalida'=>array('Asiento'=>array('AsientoCuentaContable') ) )
                                                    ));
        
               //debo buscar la cuenta DEBE en el caso del proveedor
                     
                    foreach($cheque["ComprobanteSalida"]["Asiento"]["AsientoCuentaContable"] as $asiento){
                         
                         if($asiento["es_debe"] == 0){ //la cuenta contable en el recibo esta en el haber
                             
                             return   $id_cuenta_contable = $asiento["id_cuenta_contable"];    
                         }
                         
                         
                     }
                 
              */
              
              App::uses('CakeSession', 'Model/Datasource');
             $datoEmpresa = CakeSession::read('Empresa');
             if($datoEmpresa["DatoEmpresa"]["id_cuenta_contable_cheque_rechazado"] > 0)
                return $datoEmpresa["DatoEmpresa"]["id_cuenta_contable_cheque_rechazado"]; 
             else
                 return $datoEmpresa["DatoEmpresa"]["id_cuenta_contable_cheque_cartera"];       
                 
         break;
         
         
          case  EnumDetalleTipoComprobante::ChequePropioRechazado:
          case EnumDetalleTipoComprobante::TercerosEntregadoaProveedorconND:
          case  EnumDetalleTipoComprobante::Propiossalidaporegreso:
          case  EnumDetalleTipoComprobante::Tercerossalidaporegreso:
          
          
           $cheque = $this->find('first', array(
                                                    'conditions' => array('Cheque.id' => $id_cheque),
                                                    'contain' =>array('ComprobanteEntrada'=>array('Asiento'=>array('AsientoCuentaContable')),'ComprobanteSalida'=>array('Asiento'=>array('AsientoCuentaContable') ) )
                                                    ));
        
               
                     
                    foreach($cheque["ComprobanteSalida"]["Asiento"]["AsientoCuentaContable"] as $asiento){
                         
                         if($asiento["es_debe"] == 1){    //debo buscar la cuenta DEBE en el caso del proveedor
                             
                             return   $id_cuenta_contable = $asiento["id_cuenta_contable"];    
                         }
                         
                         
                     }
          
          break;
          
          
        
          
          
         
         
     }
     
 }
 
 public function getClientePorComprobanteEntrada($id_cheque){
     
            $cheque = $this->find('first', array(
                                                    'conditions' => array('Cheque.id' => $id_cheque),
                                                    'contain' =>array('ComprobanteEntrada')
                                                    ));
           if(isset($cheque["ComprobanteEntrada"]["id_persona"]) && $cheque["ComprobanteEntrada"]["id_persona"]>0 )
                return $cheque["ComprobanteEntrada"]["id_persona"];
            else
                return 0;
            
     
 }
 
 
 public function getNumeroCheque($id_cheque){
     
            $cheque = $this->find('first', array(
                                                    'conditions' => array('Cheque.id' => $id_cheque),
                                                    'contain' =>false
                                                    ));
           return $cheque["Cheque"]["nro_cheque"];
     
 }
 
  public function formatearFechas(&$cheque){
      
      
        if($cheque["Cheque"]["fecha_cheque"]!=null)
            $cheque["Cheque"]["fecha_cheque"] = date("d-m-Y", strtotime($cheque["Cheque"]["fecha_cheque"]));
            
        if($cheque["Cheque"]["fecha_ingreso"]!=null)
            $cheque["Cheque"]["fecha_ingreso"] = date("d-m-Y", strtotime($cheque["Cheque"]["fecha_ingreso"]));    
            
        if($cheque["Cheque"]["fecha_clearing"]!=null)
            $cheque["Cheque"]["fecha_clearing"] = date("d-m-Y", strtotime($cheque["Cheque"]["fecha_clearing"]));  
        
        
        if($cheque["Cheque"]["fecha_emision"]!=null)
            $cheque["Cheque"]["fecha_emision"] = date("d-m-Y", strtotime($cheque["Cheque"]["fecha_emision"]));       
  }
  
  
  
  public function conciliado($id_cheque,$registro_comprobante_salida=false){
   /*busca el asiento de salida del cheque y determina si ese asiento fue conciliado*/   
   $conciliado = 0;
       
   
     
    $cheque = $this->find('first', array(
                                                    'conditions' => array('Cheque.id' => $id_cheque),
                                                    'contain' =>array('ComprobanteSalida'=>array('Asiento'=>array("AsientoCuentaContable")),'Chequera'=>array('CuentaBancaria'),'TipoCheque')
                                                    ));
                                                    
    if($cheque["Cheque"]["id_comprobante_salida"] >0){ /*Pregunto si el item del asiento del comprobante de salida que se corresponde con
    la cuenta bancaria asociada al banco del cheque esta conciliado*/
     
    if($cheque["TipoCheque"] == EnumTipoCheque::Propio){
        $id_cuenta_contable_banco = $cheque["Chequera"]["CuentaBancaria"]["id_cuenta_contable"];
        $condicion = ($item_asiento["id_cuenta_contable"] == $id_cuenta_contable_banco);
        
    }else{
        
        $condicion = 1; //esto devuelve siempre true en la condicion
    }    
     if(isset($cheque["ComprobanteSalida"])){
         
         if(isset($cheque["ComprobanteSalida"]["Asiento"]["AsientoCuentaContable"])){         
             foreach($cheque["ComprobanteSalida"]["Asiento"]["AsientoCuentaContable"] as $item_asiento){
                
                if($item_asiento["conciliado"] == 1 && $item_asiento["monto"] == $cheque["Cheque"]["monto"] && $condicion  )
                    $conciliado = 1; 
                 
             }
         }
     
     }else{
         $conciliado = 0;
     }
     
     
     
        
     $registro_comprobante_salida = $cheque["ComprobanteSalida"];   
    }else{
        
        $conciliado = 0;
    }                                                
                                                    
  
   

   

   
   
   
                                                           
   return $conciliado;  
      
      
  }

 
 


}
