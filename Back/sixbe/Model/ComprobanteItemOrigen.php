<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class ComprobanteItemOrigen extends ComprobanteItem {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	//public $useTable = 'comprobante_item';

    public $actsAs = array('Containable');
    
   
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

public $belongsTo = array(
        'Comprobante' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Producto' => array(
            'className' => 'Producto',
            'foreignKey' => 'id_producto',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
		'ComprobanteItemOrigen' => array(
				'className' => 'ComprobanteItem',
				'foreignKey' => 'id_comprobante_item_origen',
				'conditions' => '',
				'fields' => '',
				'order' => ''
		),
		'Unidad' => array(
				'className' => 'Unidad',
				'foreignKey' => 'id_unidad',
				'conditions' => '',
				'fields' => '',
				'order' => ''
		),
		'Iva' => array(
				'className' => 'Iva',
				'foreignKey' => 'id_iva',
				'conditions' => '',
				'fields' => '',
				'order' => ''
		),
);
 
 
 
   /*
 public $hasMany = array(
        'ArticuloRelacion' => array(
            'className' => 'ArticuloRelacion',
            'foreignKey' => 'id_producto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        
);
	  */

}
