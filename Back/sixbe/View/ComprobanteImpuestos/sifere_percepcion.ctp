<?php


    //percepciones sufridas
    
    
    
    if($nombre_archivo  == "")
        $nombre_archivo = "SiferePercepciones";
        
    header("Content-Disposition: attachment; filename=".$nombre_archivo.".txt");
    
    
    
    header("Pragma: no-cache"); 
    header("Expires: 0");
    $comprobante_obj  = new Comprobante();
    $persona_obj  = new Persona();
    
    foreach($datos as $comprobante){
        
       
       if($comprobante["ComprobanteImpuesto"]["importe_impuesto"]>0){ 
	       $punto_venta = str_pad($comprobante["Comprobante"]["d_punto_venta"], 4, '0', STR_PAD_LEFT);
	       $nro_comprobante = str_pad($comprobante["Comprobante"]["nro_comprobante"], 8, '0', STR_PAD_LEFT);
	       $fecha = new DateTime($comprobante["Comprobante"]['fecha_contable']); //esta debe ser la fecha de emision de la factura.
	       $fecha =$fecha->format('d/m/Y'); 
	       $codigo_jurisdiccion_01 = str_pad($comprobante['Impuesto']["Provincia"]["sifere_jurisdiccion"], 3, '0', STR_PAD_LEFT);//tomo la jurisdiccion del impuesto
	       $nro_doc_final_01 =  $comprobante["Comprobante"]["Persona"]["cuit"];
	       $nro_doc_final_01 = $persona_obj->getCuitConGuiones($nro_doc_final_01);
	       $importe_impuesto = $comprobante_obj->ExpresarEnTipoMonedaMejorado($comprobante["ComprobanteImpuesto"]["importe_impuesto"],"valor_moneda2", $comprobante["Comprobante"],1,$comprobante["ComprobanteImpuesto"]["base_imponible"],$comprobante["ComprobanteImpuesto"]["tasa_impuesto"]);
	       
	       
	       if($comprobante["Comprobante"]["TipoComprobante"]["signo_comercial"] < 0){
		      	$cantidad_digitos_enteros = 7;
		      	$primer_caracter = '-';
	     	}else{
		     	$cantidad_digitos_enteros = 8;
		     	$primer_caracter = '';
	     	}
	      
	       $importe_impuesto = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($importe_impuesto,$cantidad_digitos_enteros,2,"0");
	       $parte_entera = substr($importe_impuesto,0,$cantidad_digitos_enteros);
	       $parte_decimal = substr($importe_impuesto,$cantidad_digitos_enteros,2);
	       $importe_impuesto_07 = $primer_caracter.$parte_entera.','.$parte_decimal;
	       
	       
	     
	       
	       //$tipo_comprobante //F,R,D,C,O   (*) F – Factura, D - Nota de Débito, C - Nota de Crédito, R – Recibo, O - Otros
	
	       switch($comprobante["Comprobante"]["id_tipo_comprobante"]){
	           
	           case EnumTipoComprobante::FACTURASACOMPRA:
	               $tipo_comprobante = "F";
	               $letra = "A";
	               break;
	           case EnumTipoComprobante::FACTURASBCOMPRA:
	               $tipo_comprobante = "F";
	               $letra = "B";
	               break;
	           case EnumTipoComprobante::FACTURASCCOMPRA:
	               $tipo_comprobante = "F";
	               $letra = "C";
	                break;
	           case EnumTipoComprobante::NOTASDEDEBITOACOMPRA:
	               $tipo_comprobante = "D";
	               $letra = "A";
	               break;
	           case EnumTipoComprobante::NOTASDEDEBITOBCOMPRA:
	               $tipo_comprobante = "D";
	               $letra = "B";
	               break;
	           case EnumTipoComprobante::NOTASDEDEBITOCCOMPRA:
	                $tipo_comprobante = "D";
	                $letra = "C";   
	                break;
	            
	           case EnumTipoComprobante::NOTASDECREDITOACOMPRA:
	                $tipo_comprobante = "C";
	                $letra = "A";
	                break;
	           case EnumTipoComprobante::NOTASDECREDITOBCOMPRA:
	                $tipo_comprobante = "C";
	                $letra = "B";
	                break;
	           case EnumTipoComprobante::NOTASDECREDITOCCOMPRA:
	                $tipo_comprobante = "C";
	                $letra = "C"; 
	                break;
	           case EnumTipoComprobante::ReciboACompra:
	                $tipo_comprobante = "R";
	                $letra = "A"; 
	                break; 
	           case EnumTipoComprobante::ReciboBCompra:
	                $tipo_comprobante = "R";
	                $letra = "B"; 
	                break;
	           case EnumTipoComprobante::ReciboCCompra:
	                $tipo_comprobante = "R";
	                $letra = "C"; 
	                break;             
	           default:
	                $tipo_comprobante = "O";
	                break;
	                
	          case EnumTipoComprobante::GASTOA:
	               $tipo_comprobante = "F";
	               $letra = "A";
	               break;      
	           
	          case EnumTipoComprobante::GASTOB:
	               $tipo_comprobante = "F";
	               $letra = "B";
	               break;       
	           
	          case EnumTipoComprobante::GASTOC:
	               $tipo_comprobante = "F";
	               $letra = "C";
	               break;     
	                        
	       }
	       
	       $linea_texto = $codigo_jurisdiccion_01.$nro_doc_final_01.$fecha.$punto_venta.$nro_comprobante.$tipo_comprobante.$letra.$importe_impuesto_07; 
	       echo $linea_texto.  "\r\n";  
	       
	   }     
    }
        


?>