<?php
App::uses('AppModel', 'Model');
/**
 * WsAutenticacion Model
 *
 */
class WsAutenticacion extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'ws_autenticacion';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'ID_AUTENTICACION';

}
