<?php
App::uses('AppModel', 'Model');

class PersonaFigura extends AppModel {


	public $useTable = 'persona_figura';
    public $actsAs = array('Containable');


	public $validate = array(
		'd_persona_figura' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar un Nombre.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
            'unico' => array(
            'rule' => array('isUnique'),
            'message' => 'El nombre ya existe debe escribir una que no exista'
        )
		),
        
	);
	
	
	public function getPersonaFiguraPorCodigoAfip($codigo_afip){//es un string
		
		switch($codigo_afip){
			
			
			case "FISICA":
				return EnumPersonaFigura::Fisica;
				break;
			case "JURIDICA":
				return EnumPersonaFigura::Juridica;
				break;
		}
		
		
	}

    
}
