<?php
App::uses('AppModel', 'Model');
App::uses('DatoEmpresa', 'Model');


class CacheFileSiv extends AppModel {


	public $useTable = false;


    
	public function getCacheIndexEstaticaSistema(){
		
		
		//$getModelCache = unserialize(Cache::read(EnumCacheName::getModel));
		
		$getModelCache = unserialize(file_get_contents(APP."webroot/media_file/cache_get_index_estatica_sistema.txt"));
		
		
		if ($getModelCache == false) { //esto no va
			
			
			$error = EnumError::ERROR;
			$message = "La cache no existe en el servidor";
			
			
		}else {
			
			$error = EnumError::SUCCESS;
			
		}
		
		
		// echo 'true';
		// die();
		
		$output = array(
				"status" => $error,
				"message" => "list",
				"content" =>$getModelCache,
				"page_count" =>0
		);
		echo json_encode($output);
		die();
		
		
	}
	
	
	public function getCacheIndexTablasEstaticaCliente(){
		
		
		//$getModelCache = unserialize(Cache::read(EnumCacheName::getModel));
		
		$getModelCache = unserialize(file_get_contents(EnumCacheName::getCacheIndexTablasEstaticaSistema));
		
		
		if ($getModelCache == false) { //si por algo no esta la regenero
			
			
			$error = EnumError::ERROR;
			$message = "La cache no existe en el servidor";
			
			
		}
		
		
		// echo 'true';
		// die();
		
		$output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "list",
				"content" =>$getModelCache,
				"page_count" =>0
		);
		echo json_encode($output);
		die();
		
		
	}
	
	
	
	
	
	
	
	
	
	

}
