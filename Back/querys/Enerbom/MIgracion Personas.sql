SET FOREIGN_KEY_CHECKS = 0;

DELETE FROM persona;
SET FOREIGN_KEY_CHECKS = 1;




INSERT INTO persona (id_otro_sistema,razon_social,cuit,calle,numero_calle,piso,dpto,cod_postal,localidad,id_pais,id_tipo_iva,tel,email,id_tipo_persona,id_provincia)
SELECT id,razon_social,cuit,calle,numero_calle,piso,depto,cod_postal,localidad,id_pais,id_tipo_iva,tel,email,
(	CASE 

	WHEN PERSONA_TIPO = "C" THEN 1 
	WHEN PERSONA_TIPO = "P" THEN 2
	END 
	
	)AS id_tipo_persona,
	
(	CASE 

	WHEN id_provincia = 0 THEN 27
	ELSE  id_provincia
	END 
	
	)AS id_provincia_var



FROM ebsasql.`persona_gp`
JOIN ebsasql.`matpers_id` AS mp ON PERSONA_ID = persona_gp.`id`




WHERE id_provincia IS NOT NULL AND id_tipo_iva IS NOT NULL;


SELECT* FROM persona_gp WHERE id_tipo_iva=6  9,12,17,1,4,6