<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class PersonaSexo extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'persona_sexo';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	

     
     
     function getDescripcion($id){
     	
     	
     	$persona = $this->find('first', array(
     			'conditions' => array('PersonaCategoria.id' => $id),
     			'contain' =>false
     	));
     	
     	return $persona["PersonaCategoria"]["d_persona_categoria"];
     }
    
}
