<?php
App::uses('AppModel', 'Model');
/**
 * Rol Model
 *
 * @property NivelAutorizadoInformacion $NivelAutorizadoInformacion
 * @property Usuario $Usuario
 */
class Rol extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'rol';


    public $actsAs = array('Containable');
    public $uses = array('Funcion','Usuario','NivelAutorizadoInformacion');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id_nivel_autorizado_informacion' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'descripción' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar una descripcion.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'asignacion_geografica' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'NivelAutorizadoInformacion' => array(
			'className' => 'NivelAutorizadoInformacion',
			'foreignKey' => 'id_nivel_autorizado_informacion',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
    	'AsignacionGeografica' => array(
			'className' => 'NivelAutorizadoInformacion',
			'foreignKey' => 'asignacion_geografica',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'id_rol',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
    
    
    public $hasAndBelongsToMany = array(
		'Funcion' => array(
			'className' => 'Funcion',
			'joinTable' => 'funcion_rol',
			'foreignKey' => 'id_rol',
			'associationForeignKey' => 'id_funcion',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

    public function beforeSave($options = array()) {
     /*   $count = $this->Usuario->find("count", array(
            "conditions" => array("Usuario.id_rol" => $this->id)
        ));
        
        $cambioAsignacionGeografica = false;
        $this->old = $this->find('first', array(
            "conditions" => array("Rol.id" => $this->id)
        ));
        if ($this->old && $this->old['Rol']['asignacion_geografica'] != $this->data['Rol']['asignacion_geografica'])
            $cambioAsignacionGeografica = true;
        
        if ($count > 0 && $cambioAsignacionGeografica)
            throw new Exception("No se puede cambiar el requerimiento de asignación geográfica ya que existen usuarios asignados al rol");
       */ 
        return true;
    }
    public function beforeDelete($options = array()) {
        $count = $this->Usuario->find("count", array(
            "conditions" => array("id_rol" => $this->id)
        ));
        
        if ($count > 0)
            throw new Exception("El Rol esta siendo usado por un Usuario.");
        
        return true;
    }
}
