<?php
App::uses('AppModel', 'Model');
App::uses('Comprobante', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class RecepcionMaterial extends Comprobante{


 
 public function NombreComprobanteItemAfectaStock(){
 	
 	return "cantidad_cierre";
 }
	  

}
