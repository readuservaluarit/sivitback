<?php

/**
 * PDF exportación de Cotizacion
 */

App::import('Vendor','tcpdf/tcpdf'); 

App::import('Model','Comprobante');

$comprobante = new Comprobante();

// create new PDF document
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
     
     public $id;
     public $name;
     
     public function setid($id){
        $this->id = $id;
     }
     
     public function getid(){
        return $this->id;
     }
     
     public function setName($name){
        $this->name = $name;
     }
     
     public function getName(){
        return $this->name;
     }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, $this->getName().' Nro '.$this->getid().'                                                       Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->setid($datos_pdf["Comprobante"]["nro_comprobante_completo"]);
$pdf->setName($datos_pdf["TipoComprobante"]["d_tipo_comprobante"]);


ob_clean();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sivit by Valuarit');
$pdf->SetTitle('Comprobante');
$pdf->SetSubject('Comprobante');
//$pdf->SetKeywords('');



// set default header data
$logo = '/img/header.jpg';
$title = "";
$subtitle = ""; 
$pdf->SetHeaderData($logo, 190, $title, $subtitle, array(0,0,0), array(0,104,128));
$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 50, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(20);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
/*$lg = Array();
$lg['a_meta_charset'] = 'ISO-8859-1';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';
*/
$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set default font subsetting mode
//$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
/*
$html = <<<EOD
<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
EOD;
*/

$tbody = "";

$renglones = $datos_pdf["ComprobanteItem"];

$data = array();

foreach($renglones as $renglon){

 $descuento = ($renglon['descuento_unitario']);
  $precio_subtotal = ($renglon['precio_unitario'])* $renglon['cantidad'];
   
   $tbody .= "<tr>";
   $tbody .= "<td>" .$renglon['n_item'] . "</td>";
   $tbody .= "<td>" .$renglon['item_observacion']. "</td>";
   $tbody .= "<td align='center'>" .$renglon['cantidad'] . "</td>";
   $tbody .= "<td align='center'>" .$renglon['dias'] ."</td>";
      $tbody .= "<td align='center'>" .money_format('%!n',$renglon['precio_unitario_bruto']). "</td>";       
   $tbody .= "<td>" .money_format('%!n',$descuento). "</td>";   
    $tbody .= "<td>" .money_format('%!n',$precio_subtotal). "</td>";
   $tbody .= "</tr>";
}
 
//agrego las observaciones

   
   
   
   if($datos_pdf["Comprobante"]["id_persona"] == 1){
   
   		$razon_social = $datos_pdf["Comprobante"]["razon_social"];
   		$cuit = $datos_pdf["Comprobante"]["cuit"];
   
   }else{
   
   		 $razon_social = $datos_pdf["Persona"]["razon_social"];
   		 $cuit = $datos_pdf["Persona"]["cuit"];
   
   }

$html='';

$aviso_anulado = '';
    if($datos_pdf['Comprobante']['id_estado_comprobante'] == EnumEstadoComprobante::Anulado)
        $aviso_anulado = ' ANULADO';

$time_cot = strtotime($datos_pdf["Comprobante"]["fecha_generacion"]);
$html .=' 
<table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

    <tr>
         <th class="sorting" rowspan="1" colspan="1" style="width:50%;font-size:14px;"> '.$datos_pdf["TipoComprobante"]["d_tipo_comprobante"].$aviso_anulado.' NRO '.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'</th>
         
        <th  class="sorting" rowspan="1" colspan="1"  style="width:25%;"> Fecha: '.date('d-m-Y',$time_cot).' </th>
        <th  class="sorting" rowspan="1" colspan="1"  style="width:25%;"></th>
       
        
 </tr>
     <tbody>
        <tr >
        <td   ></td>
        <td  ></td>
        <td  ></td>
         </tr>
         <tr >
        <td   > </td>
        <td   ></td>
        <td   ></td>
         </tr>
        <tr >
        
        <td   >Sres: '.$razon_social.' </td>
        <td   >Cuit: '.$cuit.' </td>
        <td   >Referencia: '. $datos_pdf["Comprobante"]["referencia_comprobante_externo"].' </td>
     
         
         </tr>
         
        <tr >
        <? if( $datos_pdf["Cliente"]["id"] != null ){ ?>
        <td colspan="2">'.$datos_pdf["Persona"]["calle"].' - '.$datos_pdf["Persona"]["numero_calle"].' - '.$datos_pdf["Persona"]["localidad"].' - '.$datos_pdf["Persona"]["ciudad"].' - '.$datos_pdf["Persona"]["Provincia"]["d_provincia"].' - '.$datos_pdf["Persona"]["Pais"]["d_pais"].'  
        <? } else { ?>
         <td colspan="2">'.$datos_pdf["Comprobante"]["direccion"].'
        <?}?>
        </td>
        <td   ></td>
        <td   ></td>
        </tr>
        
        <tr >
        <td   colspan="3" ><HR></td>
        </tr>
        <tr >
         <td  colspan="3" ></td>
         </tr >
         
         <tr >
         <td  colspan="3" >Nos dirigimos a Uds. a efectos de cotizarles el/los item/s que se detalla/n a continuaci&oacute;n:</td>
         </tr >
     </tbody>
</table>


<table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:10px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody>
</table>


<table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:11px;">

    <tr style="background-color:rgb(224, 225, 229);">
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;">Item</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:50%;">Descripci&oacute;n</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">Cantidad</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;">D&iacute;as</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">Precio Unitario</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">Descuento</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">Precio subtotal</th>
    </tr>

  <tbody>
    '.$tbody.'
  </tbody>  
</table>';

   $html .= '<table border="0px" cellspacing="1" cellpadding="20" style="width:100%;font-size:12px;">';
   $html .= "<tr>";
   $html .= "<td colspan='8'>Observaciones:".nl2br($datos_pdf["Comprobante"]["observacion"])." ".nl2br($datos_pdf["TipoComprobante"]["impresion_observacion_extra"])."</td>";
   $html .= "</tr>";
   $html .= "</tr>";
   $html .= "<tr>";
   $html .= "<td colspan='8'>CONDICIONES COMERCIALES</td>";
   $html .= "</tr>";
   $html .= "</table>";
   
   $html .= "<HR>";
   
   $html .= '<table border="0px" cellspacing="1" cellpadding="1" style="width:100%;font-size:12px;">';
   $html .= "<tr>";
   $html .= "<td colspan='8'>Moneda: ".$datos_pdf['Moneda']['d_moneda']."</td>";
   $html .= "</tr>";
   $html .= "<tr>";
   $html .= "<td colspan='8'>Condici&oacute;n de Pago: ".$datos_pdf['CondicionPago']['d_condicion_pago']."</td>";
   $html .= "</tr>";
   if(strlen($datos_pdf['Comprobante']['plazo_entrega'])>0){
       $html .= "<tr>";
       $html .= "<td colspan='8'>Plazo de Entrega: ".$datos_pdf['Comprobante']['plazo_entrega']." dias</td>";
       $html .= "</tr>";
   }
   
     if($datos_pdf['Comprobante']['descuento']>0){ 
   $html .= "<tr>";
    $html .= "<td colspan='8'>Monto: ".$datos_pdf['Moneda']['simbolo']." ".money_format('%!n',$datos_pdf['Comprobante']['subtotal_bruto'])."</td>";
   $html .= "</tr>";
   
 
   $html .= "<tr>";
   
 
   $html .= "<td colspan='8'>Descuento: ".money_format('%!n',round($datos_pdf['Comprobante']['descuento'],2))." %</td>";
   $html .= "</tr>";
   
   }  
 
   $html .= "<tr>";
    $html .= "<td colspan='8'>Total: ".$datos_pdf['Moneda']['simbolo']." ".money_format('%!n',$datos_pdf['Comprobante']['total_comprobante'])."</td>";
   $html .= "</tr>";
   
    if($datos_pdf['Comprobante']['validez_dias']>0){ 
   $html .= "<tr>";
   
 
   $html .= "<td colspan='8'>Validez de Oferta: ".$datos_pdf['Comprobante']['validez_dias']." d&iacute;as </td>";
   $html .= "</tr>";
   }  
   $html .= "</table>";
   
   
$pdf->writeHTML($html, true, false, true, false, '');


// QRCODE,L : QR-CODE Low error correction
// set style for barcode
$style = array(
    'border' => 2,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

//$pdf->Text(20, 25, 'QRCODE L');

// ---------------------------------------------------------




$pdf_name = $comprobante->getNamePDF($datos_pdf);
$root_pdf = $comprobante->getRootPDF();


if($datos_pdf["TipoComprobante"]["adjunta_pdf_mail"] == 1) {

	$path_para_pdf = $datos_empresa["DatoEmpresa"]["local_path"].$root_pdf;
          	
	$pdf->Output($path_para_pdf.$pdf_name.'.pdf', 'F');//Esta linea guarda un archivo en path_para_pdf
}



// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->SetBarcode(date("Y-m-d H:i:s", time()));
ob_clean();
$pdf->Output(str_replace(".","",$datos_pdf["TipoComprobante"]["abreviado_tipo_comprobante"].'_'.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'-'.$razon_social).'.pdf', 'D');


//============================================================+
// END OF FILE
//============================================================+