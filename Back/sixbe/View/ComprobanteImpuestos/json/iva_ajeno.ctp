<?php
 /* Registro de ejemplo ESTA VISTA SE USA para agregar las PERCEPCIONES DESDE LA FACTURA DE COMPRA
id   
id_comprobante   = NRO_RECIBO_relacionado
id_moneda =
id_impuesto
id_categoria_impuesto
id_item_asiento_contable
base_imponible
importe_impuesto
tasa_impuesto
porcentaje_reduccion
id_provincia
tipo_alicuota_afip
numero = NRO_RETENCION
fecha = fecha _ cargada por usuario 
d_comprobante_impuesto =  Observacion de la retencion
*/
         include(APP.'View'.DS.'ComprobanteImpuestos'.DS.'json'.DS.'comprobante_impuesto.ctp');  
        
		// $model["tipo_alicuota_afip"]["show_grid"] = 1;
        // $model["tipo_alicuota_afip"]["header_display"] = "Alic(%)";
        // $model["tipo_alicuota_afip"]["order"] = "0";
        // $model["tipo_alicuota_afip"]["width"] = "10";
		// $model["tipo_alicuota_afip"]["read_only"] = "0";
		// $model["tipo_alicuota_afip"]["text_colour"] = "#0000AA";
        
        
        // $model["base_imponible"]["show_grid"] = 1;
        // $model["base_imponible"]["header_display"] = "Base Imp.";
        // $model["base_imponible"]["order"] = "1";
        // $model["base_imponible"]["read_only"] = "0";
        // $model["base_imponible"]["width"] = "10";
        
        // $model["importe_impuesto"]["show_grid"] = 1;
        // $model["importe_impuesto"]["header_display"] = "Importe";
        // $model["importe_impuesto"]["order"] = "2";
        // $model["importe_impuesto"]["width"] = "20";
		// $model["importe_impuesto"]["read_only"] = "0";
		// $model["importe_impuesto"]["text_colour"] = "#004400";	
		
		$model["abreviado_impuesto"]["show_grid"] = 1;
        $model["abreviado_impuesto"]["header_display"] = "Impuesto";
        $model["abreviado_impuesto"]["order"] = "0";
		$model["abreviado_impuesto"]["read_only"] = "1";
        $model["abreviado_impuesto"]["width"] = "10";
        
        $model["base_imponible"]["show_grid"] = 1;
        $model["base_imponible"]["header_display"] = "Base";
        $model["base_imponible"]["order"] = "1";
        $model["base_imponible"]["width"] = "11";
		$model["base_imponible"]["read_only"] = "0";
        
        $model["d_iva_detalle"]["show_grid"] = 1;
        $model["d_iva_detalle"]["header_display"] = "Alic(%)";
        $model["d_iva_detalle"]["order"] = "2";
		$model["d_iva_detalle"]["read_only"] = "1";
        $model["d_iva_detalle"]["width"] = "8";
        
        $model["importe_impuesto"]["show_grid"] = 1;
        $model["importe_impuesto"]["header_display"] = "Importe";
        $model["importe_impuesto"]["order"] = "3";
        $model["importe_impuesto"]["width"] = "20";
		$model["importe_impuesto"]["read_only"] = "0";
		
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteImpuesto",
                    "content" => $model
                );
        echo json_encode($output);
?>