<?php

App::uses('ComprobanteItemsController', 'Controller');


class PedidoB2BItemsController extends ComprobanteItemsController {
    
	public $name = EnumController::PedidoB2BItems;
    public $model = 'ComprobanteItem';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    
     /**
    * @secured(CONSULTA_PEDIDO_B2B)
    */              
    public function index() {
        
    	set_time_limit(1200);
    	ini_set('memory_limit', '2048M');
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model); 
        $filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');
        
        $entrega_parcial = $this->getFromRequestOrSession('Comprobante.entrega_parcial');
        
        
        
        /*Es B2B lo filtro*/
        array_push($conditions, array('Comprobante.id_detalle_tipo_comprobante' => EnumDetalleTipoComprobante::PedidosB2B));
        
      

        if($entrega_parcial == 1){/*Si esta Chequeado entrega parcial entonces solo selecciono aquellos items que la fecha del item difiera de la general del pedido*/
        	
        	array_push($conditions,array("ComprobanteItem.dias != Comprobante.plazo_entrega_dias"));
        	array_push($this->array_filter_names,array("Entrega Parcial:"=>"Si"));
        }
        
        
        $id_moneda_expresion = $this->getFromRequestOrSession('Comprobante.id_moneda_expresion');

        //$filtro_agrupado = 3; //MP antes tenia uno 1 si manda un /index plano me agrupaba los items y no me traie los campos q necesito para el form PIMasterDetail0
        $this->ComprobanteItem->paginateAgrupado($this,$filtro_agrupado,$conditions,
                                                 $this->numrecords,$this->getPageNum(),
                                                 array('Unidad','Comprobante'=>array('EstadoComprobante','Moneda','PuntoVenta','Persona','PersonaSecundaria'),
                                                 		'Producto'=>array('ProductoTipo'),'ComprobanteItemOrigen'),$this->ComprobanteItem->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_comprobante')),$id_moneda_expresion); // AGREGADO PARA EL ORDEN DE ISCO
        
       
      
        
        
      //unset($this->paginate["limit"]);
               
      if($this->RequestHandler->ext != 'json'){  
     
    }
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        $total_remitidos = 0;
        

        $produccion = $this->{$this->model}->getActivoModulo(EnumModulo::PRODUCCION);
        
        
        
        
        try{
        foreach($data as $key=>&$producto ){
        	
        	
        	
        	
        	
        	switch($filtro_agrupado){
        		
        		
        		
        		case "":
        		case EnumTipoFiltroComprobanteItem::individual:
        		case EnumTipoFiltroComprobanteItem::agrupadaporComprobante:
        	
            
		            $total_remitidos = 0;
		           //sino mando el filtro de agrupar      
		                 $producto["ComprobanteItem"]["id_iva"] = $producto["Producto"]["id_iva"];
		                 
		                 if(isset($producto["Comprobante"]["PuntoVenta"]["numero"]))
		                    $producto["ComprobanteItem"]["pto_nro_comprobante"] = $this->ComprobanteItem->GetNumberComprobante($producto["Comprobante"]["PuntoVenta"]["numero"],$producto["Comprobante"]["nro_comprobante"]);
		                 
		                 if(isset($producto["Comprobante"]["Moneda"])){
		                     $producto["ComprobanteItem"]["id_moneda"] = $producto["Comprobante"]["Moneda"]["id"];
		                     $producto["ComprobanteItem"]["d_moneda_simbolo"] = $producto["Comprobante"]["Moneda"]["simbolo"];
		                 }
		                 
		                 $producto["ComprobanteItem"]["valor_moneda"] = $producto["Comprobante"]["valor_moneda"];
		                 $producto["ComprobanteItem"]["orden_compra_externa"] = $producto["Comprobante"]["orden_compra_externa"];
		                 
		                 if(isset( $producto["Comprobante"]["EstadoComprobante"]))
		                    $producto["ComprobanteItem"]["d_estado_comprobante"] = $producto["Comprobante"]["EstadoComprobante"]["d_estado_comprobante"];
		                    
		                 $producto["ComprobanteItem"]["d_producto"] = $producto["Producto"]["d_producto"];
		                 
		                 $producto["ComprobanteItem"]["razon_social"] = $producto["Comprobante"]["Persona"]["razon_social"];
		                 $producto["ComprobanteItem"]["persona_codigo"] = $producto["Comprobante"]["Persona"]["codigo"];
		                 $producto["ComprobanteItem"]["id_persona"] = $producto["Comprobante"]["Persona"]["id"];
		                 $producto["ComprobanteItem"]["persona_tel"]  =  $producto["Comprobante"]["Persona"]["tel"];
		                 
		                 
		                 
		                 
		                 if(isset($producto["Comprobante"]["PersonaSecundaria"]) && $producto["Comprobante"]["PersonaSecundaria"]["id"]>0){
		                 	
		                 	
		                 	$producto["ComprobanteItem"]["razon_social_secundaria"] = $producto["Comprobante"]["PersonaSecundaria"]["razon_social"];
		                 	$producto["ComprobanteItem"]["persona_codigo_secundaria"] = $producto["Comprobante"]["PersonaSecundaria"]["codigo"];
		                 	$producto["ComprobanteItem"]["id_persona_secundaria"] = $producto["Comprobante"]["PersonaSecundaria"]["id"];
		                 	$producto["ComprobanteItem"]["persona_tel_secundaria"]  =  $producto["Comprobante"]["PersonaSecundaria"]["tel"];
		                 }
		                 
		                 
		                 
		                 
		                 $producto["ComprobanteItem"]["moneda_simbolo"] = $producto["Comprobante"]["Moneda"]["simbolo"];
		                 $producto["ComprobanteItem"]["codigo"] = $this->ComprobanteItem->getCodigoFromProducto($producto);
		                 
		               
		                 
		                 if( isset($producto["ComprobanteItem"]["fecha_entrega"]) && $this->{$this->model}->validateDate($producto["ComprobanteItem"]["fecha_entrega"],'d-m-Y') )
		                    $producto["ComprobanteItem"]["dias_atraso_item"] = $this->ComprobanteItem->getDiasAtraso($producto["ComprobanteItem"]["fecha_entrega"]); //TODO: REVISAR
		                 else
		                    $producto["ComprobanteItem"]["dias_atraso_item"] = 0;
		
		                    
		                    
		                 if( $producto["ComprobanteItem"]["fecha_entrega"] && $this->{$this->model}->validateDate($producto["Comprobante"]["fecha_entrega"],'Y-m-d') )
		                    $producto["ComprobanteItem"]["dias_atraso_global"] = $this->ComprobanteItem->getDiasAtraso($producto["Comprobante"]["fecha_entrega"]);
		                 else
		                     $producto["ComprobanteItem"]["dias_atraso_global"] = 0;
		                     
		                 $fecha = new DateTime($producto["Comprobante"]["fecha_generacion"]);
		                 $producto["ComprobanteItem"]["fecha_generacion"] = $fecha->format('d-m-Y');;
		                 
		           
		                // $producto["ComprobanteItem"]["total"] = (string) $this->{$this->model}->getTotalItem($producto);
		                 
		                 $facturados = null;
		                 $remitidos = null;
		                 $notas_credito = null;
		                 $total_notas_credito = 0;
		                 
		                 //Si es un hijo entonces traigo los relacionados
		                 //if(isset($producto["ComprobanteItem"]["id_comprobante_item_origen"]) && $producto["ComprobanteItem"]["id_comprobante_item_origen"]>0){
		                     
		                     
		                     
		                 $facturados =$this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItem"], $this->Comprobante->getTiposComprobanteController(EnumController::Facturas) );
		                     
		                     
		                     
		                     
		                 $remitidos = $this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItem"], $this->Comprobante->getTiposComprobanteController(EnumController::Remitos) );
		                 
		                 $total_remitidos = $this->ComprobanteItem->getTotalItemsGenerados2Niveles($producto["ComprobanteItem"]["id"],$this->Comprobante->getTiposComprobanteController(EnumController::Facturas),EnumTipoComprobante::Remito);
		                     
		                     
		                 $total_notas_credito = $this->ComprobanteItem->getTotalItemsGenerados2Niveles($producto["ComprobanteItem"]["id"],$this->Comprobante->getTiposComprobanteController(EnumController::Facturas),$this->Comprobante->getTiposComprobanteController(EnumController::NotasCreditoVenta));
		                     
		                     
		                     
		               
		      
		                     
		                     
		                     
		                     
		                     
		                // }
		                 $producto["ComprobanteItem"]["total_notas_credito_origen"] = (string) number_format( (float) $total_notas_credito, $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		                 $producto["ComprobanteItem"]["total_facturado"] = (string)  number_format((float) ($this->ComprobanteItem->GetCantidadItemProcesados($facturados) - $producto["ComprobanteItem"]["total_notas_credito_origen"]), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		                 
		                 $producto["ComprobanteItem"]["total_remitido"] = (string) number_format((float)($this->ComprobanteItem->GetCantidadItemProcesados($remitidos) + $total_remitidos), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		               
		                 
		                 
		                 $producto["ComprobanteItem"]["cantidad_pendiente_a_facturar"] = (string) number_format( (float)($producto["ComprobanteItem"]["cantidad"] - 
		                                                                                                                 $producto["ComprobanteItem"]["total_facturado"]), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		                 $producto["ComprobanteItem"]["cantidad_pendiente_a_remitir"] = (string) number_format((float)($producto["ComprobanteItem"]["cantidad"] 
		                                                                                                            -  $producto["ComprobanteItem"]["total_remitido"]), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		                 $producto["ComprobanteItem"]["d_moneda"] = $producto["Comprobante"]["Moneda"]["simbolo"]; 
		                
		                 
		                 
		                 if($produccion == 1){
		                 	
		                 	
		                 	$armados = $this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItem"], $this->Comprobante->getTiposComprobanteController(EnumController::OrdenArmado) );
		                 	$producto["ComprobanteItem"]["total_armado"] = (string) number_format((float)($this->ComprobanteItem->GetCantidadItemProcesados($armados)), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		                 	$producto["ComprobanteItem"]["cantidad_pendiente_a_armar"] = (string) number_format((float)($producto["ComprobanteItem"]["cantidad"]
		                 			-  $producto["ComprobanteItem"]["total_armado"]), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		                 	
		                 	$producto["ComprobanteItem"]["d_orden_armado"] = '';
		                 	
		                 	foreach($armados as $oa){
		                 		
		                 		if(isset($oa["Comprobante"]["nro_comprobante"]) && $oa["Comprobante"]["nro_comprobante"]>0){
		                 			$producto["ComprobanteItem"]["d_orden_armado"] .= " OA:".$oa["Comprobante"]["nro_comprobante"]."(".$oa["EstadoComprobante"]["d_estado_comprobante"].")";
		                 			$producto["ComprobanteItem"]["fecha_entrega_orden_armado"] .= $oa["Comprobante"]["fecha_entrega"];
		                 			$producto["ComprobanteItem"]["fecha_limite_armado"] .= $oa["Comprobante"]["fecha_vencimiento"];
		                 		}
		                 	}
		                 	
		                 }
		                 
		                 
		                 
		                 $this->ComprobanteItem->CalculaUnidad($producto);
		           
		                 $this->FiltrarCamposOnTheFly($data,$producto,$key);
		                 
		                 unset($producto["Producto"]);
		                 unset($producto["Comprobante"]);
		                 unset($producto["ComprobanteItemOrigen"]);
		          break;
		          
        		case EnumTipoFiltroComprobanteItem::agrupadaporPersona:
        			
        			$producto["ComprobanteItem"]["total"] = $producto[0]["total"] ;
        			$producto["ComprobanteItem"]["razon_social"] = $producto["Persona"]["razon_social"];
        			
        			break;
                 
         
                 
        	}
        	
        	
        }
        
        
        }catch (Exception $e){
        	
        	$e;
        	
        }
        
        $this->data = $this->prepararRespuesta($data,$page_count);
        
        
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $this->data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
  

    
      /**
    * @secured(CONSULTA_PEDIDO_B2B)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);
        
        
        
        
    }
    
    public function prepararRespuesta($data,&$page_count){
        
        
        
    $filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');//dice como mostrar los items    
    $array_id_comprobantes = array();
    

    
    if($filtro_agrupado == EnumTipoFiltroComprobanteItem::agrupadaporComprobante){ //si es la vista agrupada tomo los filtros de los items y agrupo en memoria por id_comprobante
    
    
        
       foreach($data as $key=>$valor){
           if(in_array($valor["ComprobanteItem"]["id_comprobante"],$array_id_comprobantes))
            unset($data[$key]);
           else
            array_push($array_id_comprobantes,$valor["ComprobanteItem"]["id_comprobante"]);
           
       }
       
       
       
        
        
        
    }    
        
    return parent::prepararRespuesta($data,$page_count);
    }
    
  
     /**
    * @secured(CONSULTA_PEDIDO_B2B)
    */       
   public function reporte_pedido_interno_items($view="agrupadaporcomprobante"){
        
        $this->paginado =0;
        
        $filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');
        
        
        
        switch($filtro_agrupado){
        	
        	case EnumTipoFiltroComprobanteItem::individual:
        	case "":
        		$view = "noagrupada";
        	break;
        	case EnumTipoFiltroComprobanteItem::agrupadaporComprobante:
        		$view="agrupadaporcomprobante";
        	break;
        	case EnumTipoFiltroComprobanteItem::agrupadaporPersona:
        		$view="agrupadaporpersona";
        		break;
        		
        }
        
         $this->ExportarExcel($view,"index","Reporte de Pedidos B2B");
                 
        
        
    } 

    
}
?>