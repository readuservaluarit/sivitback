<?php


$tbody = "";

$renglones = $datos_pdf;

$total_registros = count($renglones);



$data = array();

$corte_control = 24;


$cantidad_paginas =    round($total_registros/$corte_control, 0, PHP_ROUND_HALF_UP);


$total_gravado = 0;
$total_exento = 0;
$total_no_gravado = 0;
$total_comprobante = 0;
$total_iva_27 = 0;
$total_iva_21 = 0;
$total_iva_105 = 0;
$total_iva_5 = 0;
$total_iva_2coma5 = 0;
$total_ib = array();
$total_exento = 0;
$total_no_gravado = 0;
$i=1;
$z=1;
$cant_pag=1;
$titulo = $datos_extra["titulo"].' '.$datos_extra["subtitulo1"];
$subtitulo1 = $datos_extra["subtitulo2"];
$percepciones = $datos_extra["percepciones"];
$cantidad_percepciones = count($percepciones);
$tbody .='
        <table width="100%" border="0" style="font-size:20px;font-weight:bold;">
     <tr>
  
    <td align="center" colspan='.(10+$cantidad_percepciones).'></td>
    ';
    
     
    
   
  $tbody .='</tr> 
  
    
  <tr>
  
    
    <td align="center" colspan='.(10+$cantidad_percepciones).'>'.$titulo.'</td>
   ';
    
    
   
    
    
   
  $tbody .='</tr>
  <tr>
   
    <td align="center" colspan='.(10+$cantidad_percepciones).'>'.$subtitulo1.'</td>
    ';
    
    
    
  $tbody .='</tr>
</table>

        <table width="100%"  cellspacing="1" cellpadding="2" style="font-family:\'Courier New\', Courier, monospace">
        
        
        ';
        
   
       
       
   
       
       

       
$tbody .='<tr style="background-color:rgb(91, 155, 213);text-align:center;font-size:12px;">
    
        <th class="sorting"  rowspan="1" colspan="1" style="width:7%;"><strong>Fecha</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:3%;"><strong>Tipo</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:12%;"><strong>CUIT</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:18%;"><strong>R. Social</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>Nro Comprobante</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>N. Gravado</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>Exento</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>No Gravado</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:6%;"><strong>I.V.A 27%</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:6%;"><strong>I.V.A 21%</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:6%;"><strong>I.V.A 10,5%</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:6%;"><strong>I.V.A 5%</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:6%;"><strong>I.V.A 2.5%</strong></th>';
        
        foreach($percepciones as $percepcion){
        
             if($percepcion["DatoEmpresaImpuesto"]["check_mostrar_impuesto_libro_iva"] == 1)
            	$tbody.='<th class="sorting"  rowspan="1" colspan="1" style="width:7%;"><strong>'.$percepcion["Impuesto"]["abreviado_impuesto"].'</strong></th>';
        }
        
$tbody .='<th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>Total</strong></th>
        
    </tr>
';

foreach($renglones as $renglon){

  
  
   
   
   
   if($i-1 == $corte_control ) {
   
   
   
       $tbody .= "
         
       
       
       
       <tr style=\"font-size:12px;font-family:'Courier New', Courier, monospace\">";
      
       $tbody .= "<td>Total</td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";       
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td align='right' >" .str_replace(".",",",$total_gravado). "</td>"; 
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_exento). "</td>";       
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_no_gravado). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_iva_27). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_iva_21). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_iva_105)."</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_iva_5)."</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_iva_2coma5)."</td>";
       
       foreach($percepciones as $percepcion){
        
        if($percepcion["DatoEmpresaImpuesto"]["check_mostrar_impuesto_libro_iva"] == 1)
        	$tbody .= "<td align='right'>" .str_replace(".",",",$total_ib[$percepcion["Impuesto"]["id"]])."</td>";
       }
       $tbody .= "<td  align='right'>" .str_replace(".",",",$total_comprobante)."</td>";
       $tbody .= "</tr>";
       $tbody .= "<tr style=\"font-size:12px;font-family:'Courier New', Courier, monospace\">";
      /*
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";       
       $tbody .= "<td></td>";
       $tbody .= "<td></td>"; 
       $tbody .= "<td></td>"; 
       $tbody .= "<td></td>"; 
       $tbody .= "<td></td>"; 
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td ></td>";
       $tbody .= "</tr>";
       
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";       
       $tbody .= "<td></td>";
       $tbody .= "<td></td>"; 
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td ></td>";
       $tbody .= "</tr></table>";
        */
       $tbody .= ' <table width="100%" border="0" style="font-size:20px;font-weight:bold;">
  <tr>
  
   
    <td align="center" colspan='.(10+$cantidad_percepciones).'>'.$titulo.'</td>
    ';
    
      
   
  $tbody.='</tr>
  <tr>
   
    <td align="center" colspan='.(10+$cantidad_percepciones).'>'.$subtitulo1.'</td>
    ';
    
    
   
  $tbody.='</tr>
</table>';       
              
              
       $tbody .= '<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:12px;font-family:\'Courier New\', Courier, monospace">';
       $tbody .= "<td>Transporte</td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";       
       $tbody .= "<td></td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_gravado). "</td>"; 
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_exento). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_no_gravado). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_iva_27). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_iva_21). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_iva_105)."</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_iva_5)."</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_iva_2coma5)."</td>";
        foreach($percepciones as $percepcion){
         if($percepcion["DatoEmpresaImpuesto"]["check_mostrar_impuesto_libro_iva"] == 1)
        	$tbody .= "<td align='right'>" .str_replace(".",",",$total_ib[$percepcion["Impuesto"]["id"]])."</td>";
       }
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_comprobante)."</td>";
       $tbody .= "</tr>";
       $tbody .= "</table>";
       $i = 1;
       ++$cant_pag;
     //$pdf->AddPage();
      //$pdf->setPage($cant_pag);
      $tbody .='
       <table width="100%"  cellspacing="1" cellpadding="2" style="font-family:\'Courier New\', Courier, monospace">
      <tr style="background-color:rgb(91, 155, 213);text-align:center;font-size:10px;">
    
       <th class="sorting"  rowspan="1" colspan="1" style="width:7%;"><strong>Fecha</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:3%;"><strong>Tipo</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:12%;"><strong>CUIT</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:18%;"><strong>R. Social</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>Nro Comprobante</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>N. Gravado</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>Exento</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>No Gravado</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:6%;"><strong>I.V.A 27%</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:6%;"><strong>I.V.A 21%</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:6%;"><strong>I.V.A 10,5%</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:6%;"><strong>I.V.A 5%</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:6%;"><strong>I.V.A 2.5%</strong></th>';
        
        foreach($percepciones as $percepcion){
        
             if($percepcion["DatoEmpresaImpuesto"]["check_mostrar_impuesto_libro_iva"] == 1)
            	$tbody.='<th class="sorting"  rowspan="1" colspan="1" style="width:7%;"><strong>'.$percepcion["Impuesto"]["abreviado_impuesto"].'</strong></th>';
        }
        
        $tbody .='<th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>Total</strong></th>
        
    </tr>
    
';


       $total_gravado = $total_gravado + $renglon['Comprobante']['gravado']; 
       $total_no_gravado +=$renglon['Comprobante']['no_gravado']; 
       $total_exento +=$renglon['Comprobante']['exento']; 
       $total_iva_27 +=$renglon['Comprobante']['iva_27']; 
       $total_iva_21 +=$renglon['Comprobante']['iva_21']; 
       $total_iva_105 +=$renglon['Comprobante']['iva_105']; 
       $total_iva_5 +=$renglon['Comprobante']['iva_5']; 
       $total_iva_2coma5 +=$renglon['Comprobante']['iva_2coma5']; 
       
       foreach($percepciones as $percepcion){
        if($percepcion["DatoEmpresaImpuesto"]["check_mostrar_impuesto_libro_iva"] == 1)
        	$total_ib[$percepcion["Impuesto"]["id"]] +=$renglon['Comprobante']["array_impuestos"][$percepcion["Impuesto"]["id"]]; 
       } 
       $total_comprobante +=$renglon['Comprobante']['total_comprobante'];
       
       
       
      $tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
       $tbody .= "<td align='left'>" .$renglon['Comprobante']['fecha_generacion'] . "</td>";
       $tbody .= "<td align='left'>" .$renglon['Comprobante']['tipo_comprobante']. "</td>";
       $tbody .= "<td align='left'>" .$renglon['Comprobante']['cuit'] . "</td>";
       $tbody .= "<td align='left'>" .trim(substr($renglon['Comprobante']['razon_social'],0,30)) . "</td>";       
       $tbody .= "<td align='left'>" .$renglon['Comprobante']['nro_comprobante']. "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['gravado']). "</td>"; 
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['exento']). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['no_gravado']). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['iva_27']). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['iva_21']). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['iva_105'])."</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['iva_5'])."</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['iva_2coma5'])."</td>";
       
       foreach($percepciones as $percepcion){
        if($percepcion["DatoEmpresaImpuesto"]["check_mostrar_impuesto_libro_iva"] == 1)
        	$tbody .= "<td align='right'>" .str_replace(".",",",$renglon["Comprobante"]["array_impuestos"][$percepcion["Impuesto"]["id"]])."</td>";
       }
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['total_comprobante'])."</td>";
       $tbody .= "</tr>";
   }else{
   
       $total_gravado = $total_gravado + $renglon['Comprobante']['gravado']; 
       $total_no_gravado +=$renglon['Comprobante']['no_gravado']; 
       $total_exento +=$renglon['Comprobante']['exento']; 
       $total_iva_27 +=$renglon['Comprobante']['iva_27']; 
       $total_iva_21 +=$renglon['Comprobante']['iva_21']; 
       $total_iva_105 +=$renglon['Comprobante']['iva_105']; 
       $total_iva_5 +=$renglon['Comprobante']['iva_5']; 
       $total_iva_2coma5 +=$renglon['Comprobante']['iva_2coma5']; 
       
       foreach($percepciones as $percepcion){
       
        if($percepcion["DatoEmpresaImpuesto"]["check_mostrar_impuesto_libro_iva"] == 1)
        	$total_ib[$percepcion["Impuesto"]["id"]] +=$renglon['Comprobante']["array_impuestos"][$percepcion["Impuesto"]["id"]]; 
       }
       $total_comprobante +=$renglon['Comprobante']['total_comprobante'];
      
   
   
       $tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
       $tbody .= "<td align='left'>" .$renglon['Comprobante']['fecha_generacion'] . "</td>";
       $tbody .= "<td align='left'>" .$renglon['Comprobante']['tipo_comprobante']. "</td>";
       $tbody .= "<td align='left'>" .$renglon['Comprobante']['cuit'] . "</td>";
       $tbody .= "<td align='left'>" .trim(substr($renglon['Comprobante']['razon_social'],0,30)) . "</td>";       
       $tbody .= "<td align='left'>" .$renglon['Comprobante']['nro_comprobante']. "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['gravado']). "</td>"; 
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['exento']). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['no_gravado']). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['iva_27']). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['iva_21']). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['iva_105'])."</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['iva_5'])."</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['iva_2coma5'])."</td>";
        foreach($percepciones as $percepcion){
        
         if($percepcion["DatoEmpresaImpuesto"]["check_mostrar_impuesto_libro_iva"] == 1)
        	$tbody .= "<td align='right'>" .str_replace(".",",",$renglon["Comprobante"]["array_impuestos"][$percepcion["Impuesto"]["id"]])."</td>";
       }
       $tbody .= "<td align='right'>" .str_replace(".",",",$renglon['Comprobante']['total_comprobante'])."</td>";
       $tbody .= "</tr>";
   
   
   
   }
   ++$i;
   ++$z;
   
   
   if($z > $total_registros){
   
       $tbody .= "<tr style=\"font-size:12px;font-family:'Courier New', Courier, monospace\">";
       $tbody .= "<td>Totales</td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";       
       $tbody .= "<td></td>";       
       $tbody .= "<td></td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_gravado). "</td>"; 
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_exento). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_no_gravado). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_iva_27). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_iva_21). "</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_iva_105)."</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_iva_5)."</td>";
       $tbody .= "<td align='right'>" .str_replace(".",",",$total_iva_2coma5)."</td>";
       foreach($percepciones as $percepcion){
        if($percepcion["DatoEmpresaImpuesto"]["check_mostrar_impuesto_libro_iva"] == 1)
        	$tbody .= "<td align='right'>" .str_replace(".",",",$total_ib[$percepcion["Impuesto"]["id"]])."</td>";
       }
       $tbody .= "<td  align='right'>" .str_replace(".",",",$total_comprobante)."</td>";
       $tbody .= "</tr>";
    
   
   }
   
   
   
}





 
//agrego las observaciones







$html='';


$html .=' 
    
 
    '.$tbody.'
';

//ob_end_clean(); 
$file=$titulo." ".$subtitulo1.".xls";
header("Content-Disposition: attachment; filename=".$file);
echo $tbody;







?>
