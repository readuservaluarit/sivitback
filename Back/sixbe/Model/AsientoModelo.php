<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class AsientoModelo extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'asiento_modelo';

    public $actsAs = array('Containable');
    

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'd_asiento_modelo' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar un nombre v&aacute;lida',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 public $belongsTo = array(
        'ClaseAsiento' => array(
            'className' => 'ClaseAsiento',
            'foreignKey' => 'id_clase_asiento',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'TipoAsiento' => array( 
            'className' => 'TipoAsiento',
            'foreignKey' => 'id_tipo_asiento',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Moneda' => array(
            'className' => 'Moneda',
            'foreignKey' => 'id_moneda',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
        )
);
public $hasMany = array(
        'AsientoModeloCuentaContable' => array(
            'className' => 'AsientoModeloCuentaContable',
            'foreignKey' => 'id_asiento_modelo',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        );





}
