<?php
App::uses('ComprobantesController', 'Controller');


  /**
    * @secured(CONSULTA_CREDITO_INTERNO_CLIENTE)
  */
class CreditosInternoClienteController extends ComprobantesController {
    
    public $name = EnumController::CreditosInternoCliente;
    public $model = 'Comprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $requiere_impuestos = 0;
    public $id_sistema_comprobante = EnumSistema::VENTAS;
    
    /**
    * @secured(CONSULTA_CREDITO_INTERNO_CLIENTE)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros($this->model);
            
            
                       
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>array('Persona','EstadoComprobante','Moneda','TipoComprobante','PuntoVenta'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.id desc'
        ); //el contacto es la sucursal
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
    
    
        
        $formBuilder->setDataListado($this, 'Listado de Proveedors', 'Datos de los Proveedors', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'SALDOINICIAL.id', "10%");
        $formBuilder->addHeader('Razon Social', 'Proveedor.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'SALDOINICIAL.cuit', "20%");
        $formBuilder->addHeader('Email', 'Proveedor.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'Proveedor.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
            
             //$valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
             //unset($valor['ComprobanteItem']);
             
             if(isset($valor['TipoComprobante'])){
                 $valor[$this->model]['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                 $valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
                 unset($valor['TipoComprobante']);
             }
             
             $valor[$this->model]['nro_comprobante'] = (string) str_pad($valor[$this->model]['nro_comprobante'], 8, "0", STR_PAD_LEFT);
    
             
             /*
             foreach($valor[$this->model]['ComprobanteItem'] as &$producto ){
                 
                 $producto["d_producto"] = $producto["Producto"]["d_producto"];
                 $producto["codigo_producto"] = $producto["Producto"]["codigo"];
                 
                 
                 unset($producto["Producto"]);
             }
             */
           
             if(isset($valor['Moneda'])){
                 $valor[$this->model]['d_moneda'] = $valor['Moneda']['d_moneda'];
                 unset($valor['Moneda']);
             }
             
             if(isset($valor['EstadoComprobante'])){
                $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
                unset($valor['EstadoComprobante']);
             }
             if(isset($valor['CondicionPago'])){
                $valor[$this->model]['d_condicion_pago'] = $valor['CondicionPago']['d_condicion_pago'];
                unset($valor['CondicionPago']);
             }
             
          
          
             if(isset($valor['Persona'])){
            
            
                 if($valor['Persona']['id']== null){
                     
                    $valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social']; 
                 }else{
                     
                     $valor[$this->model]['razon_social'] = $valor['Persona']['razon_social'];
                     $valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
                 }
                 
                 unset($valor['Persona']);
             }
             
       
               
                 if(isset($valor['PuntoVenta'])){
                    $valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
                    unset($valor['PuntoVenta']);
             }
            $this->{$this->model}->formatearFechas($valor);
        }
        
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
   


    
 
    
 
      /**
    * @secured(CONSULTA_CREDITO_INTERNO_CLIENTE)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
  
    /**
    * @secured(BTN_PDF_CREDITOSINTERNOCLIENTE)
    */
    public function pdfExport($id,$vista=''){
           
            
    	
    	$output = array(
    			"status" =>EnumError::ERROR,
    			"message" => "El Cr&eacute;dito interno no es un documento v&aacute;lido para imprimir",
    			"content" => "",
    	
    	);
    	$this->set($output);
    	$this->set("_serialize", array("status", "message","page_count", "content"));
            
       
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
    }
    
    
    
   protected function actualizoMontos($id_c,$nro_c,$total_iva,$total_impuestos) {
    
        return;
    }
    
    /**
     * @secured(BTN_EXCEL_CREDITOSINTERNOCLIENTE)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Creditos internos de proveedor");
    	
    	
    	
    }
    
    
    /**
     * @secured(CONSULTA_CREDITO_INTERNO_CLIENTE)
     */
    public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
    {
    	parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
    }
    
    
    
    /**
     * @secured(MODIFICACION_CREDITO_INTERNO_CLIENTE)
     */
    
    public function Anular($id_comprobante){
    	
    	$this->loadModel("Comprobante");
    	
    	
    	$dic = $this->Comprobante->find('first', array(
    			'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
    			'contain' => false
    	));
    	
    	
    	if(
    			$this->Comprobante->getEstadoGrabado($id_comprobante)!= EnumEstadoComprobante::Anulado &&
    			count($this->getRecibosRelacionados($id_comprobante))==0    &&
    			$dic
    			){
    				
    				
    				
    				$this->Comprobante->updateAll(
    						array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,
    								'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
    						array('Comprobante.id' => $id_comprobante) );
    				
    				
    				$tipo = EnumError::SUCCESS;
    				$mensaje = "El Comprobante se anulo correctamente ";
    				
    				
    	}else{
    		$tipo = EnumError::ERROR;
    		$mensaje = "No es posible anular el Comprobante ya que o ya ha sido ANULADO o esta presente en algun RECIBO"."\n";
    		
    		$recibos_relacionados = $this->getRecibosRelacionados($id_comprobante);
    		if(count($recibos_relacionados)>0){
    			foreach($recibos_relacionados as $recibo){
    				$mensaje.= "&bull; Recibo Nro: ".$recibo["Comprobante"]["nro_comprobante"]."\n";
    			}
    		}
    	}
    	
    	$output = array(
    			"status" => $tipo,
    			"message" => $mensaje,
    			"content" => "",
    			"id_asiento"=>$output_asiento_revertir["id_asiento"]
    	);
    	
    	echo json_encode($output);
    	die();
    	
    	
    	
    }
     
  
    
}
?>