<?php


    App::import('Model','Comprobante');
    App::import('Model','DatoEmpresa'); 
     
    if($nombre_archivo  == "")
        $nombre_archivo = "CitiCompras";
        
    header("Content-Disposition: attachment; filename=".$nombre_archivo.".txt");
    header("Pragma: no-cache"); 
    header("Expires: 0"); 
    
      
    $comprobante_obj  = new Comprobante();
    $dato_empresa_obj  = new DatoEmpresa();
    
    $id_tipo_moneda = $dato_empresa_obj->getNombreCampoMonedaPorIdMoneda(EnumMoneda::Peso);
    $field_valor_moneda = $comprobante_obj->getFieldByTipoMoneda($id_tipo_moneda);
    
    
    
    foreach($datos as $comprobante){
        
        
       $fecha = new DateTime($comprobante["Comprobante"]['fecha_generacion']); 
       $fecha_comprobante_01 =$fecha->format('Ymd'); 
       $tipo_comprobante_afip_02 = str_pad($comprobante["TipoComprobante"]["codigo_afip"], 3, '0', STR_PAD_LEFT);
       
       
       
       if($comprobante["PuntoVenta"]["numero"]>0)
        $punto_venta_03 = str_pad($comprobante["PuntoVenta"]["numero"], 5, '0', STR_PAD_LEFT);
       else
        $punto_venta_03 = str_pad($comprobante["Comprobante"]["d_punto_venta"], 5, '0', STR_PAD_LEFT);
       
       
       
       $nro_comprobante_04 = str_pad($comprobante["Comprobante"]["nro_comprobante"], 20, '0', STR_PAD_LEFT);
       $nro_despacho_importacion_05 = ' ';
       $nro_despacho_importacion_05 = str_pad($nro_despacho_importacion_05, 16, ' ', STR_PAD_LEFT);
       $codigo_doc_afip_06 = str_pad($comprobante["Persona"]["TipoDocumento"]["abreviacion_afip"], 2, '0', STR_PAD_LEFT);
       $cuit_final_07 =  str_pad($comprobante["Persona"]["cuit"], 20, '0', STR_PAD_LEFT);
       
       $razon_social = utf8_decode(trim($comprobante["Persona"]["razon_social"]));
       
       if(strlen($razon_social)>30)
        $razon_social = substr(trim($razon_social),0,30);
       
       $razon_social_08 =  str_pad(trim($razon_social), 30, ' ', STR_PAD_RIGHT);
       
       
     
       $total_comprobante = $comprobante["Comprobante"]["total_comprobante"];
       $importe_total_operacion_09 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($total_comprobante,$field_valor_moneda, $comprobante["Comprobante"]);
       $importe_total_operacion_09 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($importe_total_operacion_09,13,2,"0");
       
      
       $id_comprobante = $comprobante["Comprobante"]['id'];
       
       
       
       
       
       
       
       
           
       $importe_total_concepto_no_integran_neto_gravado_010 = $comprobante_obj->getNoGravado($id_comprobante);/*Esto es los items que tienen iva 0%*/
       $total_no_gravado_aux = $importe_total_concepto_no_integran_neto_gravado_010;
       
       if($comprobante["TipoComprobante"]["letra"] == 'B' || $comprobante["TipoComprobante"]["letra"] == 'C')
       	$importe_total_concepto_no_integran_neto_gravado_010 = 0.00;
       
       
       
       $importe_total_concepto_no_integran_neto_gravado_010 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($importe_total_concepto_no_integran_neto_gravado_010,$field_valor_moneda, $comprobante["Comprobante"]);
       
       
       $importe_total_concepto_no_integran_neto_gravado_010 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($importe_total_concepto_no_integran_neto_gravado_010,13,2,"0");  
         
     
       //le mando solo los items que tenga "EXENTO" en el iva, esto es en caso de la FC Exportacion, tanto franca como internacional.    
       
       $exento = $comprobante_obj->getExento($id_comprobante) ;
       
       $importe_operaciones_exento_11 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($exento,$field_valor_moneda, $comprobante["Comprobante"]);
         
       $importe_operaciones_exento_11 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($importe_operaciones_exento_11,13,2,"0");  
          
           
 
        
       /*PERCEPCIONES DE IVA QUE RECIBI*/
       $total_impuestos_nacionales_iva_12   = $comprobante_obj->getTotalPorImpuestoGeografia($comprobante["Comprobante"]["id"],EnumImpuestoGeografia::ImpuestoNacional,0,EnumTipoImpuesto::PERCEPCIONES,EnumSubTipoImpuesto::PERCEPCION_IVA);
       
       
       $total_impuestos_nacionales_iva_aux = $total_impuestos_nacionales_iva_12;
       
       
       
  
       $total_impuestos_nacionales_iva_12 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($total_impuestos_nacionales_iva_12,$field_valor_moneda, $comprobante["Comprobante"]);
       
       
       
       $total_impuestos_nacionales_iva_12   = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_impuestos_nacionales_iva_12,13,2,"0");
       
       
       /*OTRAS PERCEPCIONES NACIONALES MENOS LA DE IVA*/
       
       $total_impuestos_nacionales_13   = $comprobante_obj->getTotalPorImpuestoGeografia($comprobante["Comprobante"]["id"],EnumImpuestoGeografia::ImpuestoNacional,0,EnumTipoImpuesto::PERCEPCIONES) - $total_impuestos_nacionales_iva_aux;
       
       
       $total_impuestos_nacionales_13 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($total_impuestos_nacionales_13,$field_valor_moneda, $comprobante["Comprobante"]);
       
       
       $total_impuestos_nacionales_13   = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_impuestos_nacionales_13,13,2,"0");
       
       
       
       
       $total_impuestos_provinciales_14 = $comprobante_obj->getTotalPorImpuestoGeografia($comprobante["Comprobante"]["id"],EnumImpuestoGeografia::ImpuestoProvincial,0,EnumTipoImpuesto::PERCEPCIONES);
       
       
       $total_impuestos_provinciales_14 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($total_impuestos_provinciales_14,$field_valor_moneda, $comprobante["Comprobante"]);
        
       $total_impuestos_provinciales_14 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_impuestos_provinciales_14,13,2,"0");
       
       
       $total_impuestos_municipales_15  = $comprobante_obj->getTotalPorImpuestoGeografia($comprobante["Comprobante"]["id"],EnumImpuestoGeografia::ImpuestoMunicipal,0,EnumTipoImpuesto::PERCEPCIONES);
       
       $total_impuestos_municipales_15 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($total_impuestos_municipales_15,$field_valor_moneda, $comprobante["Comprobante"]);
          
          
       $total_impuestos_municipales_15  = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_impuestos_municipales_15,13,2,"0");
       
       $total_impuestos_interno_16      = $comprobante_obj->getTotalPorImpuestoGeografia($comprobante["Comprobante"]["id"],EnumImpuestoGeografia::ImpuestoInterno,0,EnumTipoImpuesto::PERCEPCIONES);  
       
       $total_impuestos_interno_16 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($total_impuestos_interno_16,$field_valor_moneda, $comprobante["Comprobante"]);
        
       $total_impuestos_interno_16      = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_impuestos_interno_16,13,2,"0");
       
       $codigo_moneda_17 = 'PES';
       
      
     
      
       
       $tipo_cambio = 1.00;
       $tipo_cambio_18 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($tipo_cambio,4,6,"0"); 
       
       
       
       
       
       $cantidad_alicuota_iva_19 = $comprobante_obj->getCantidadIvas($comprobante["Comprobante"]["id"],$comprobante["TipoComprobante"]["id_sistema"],0,0);
     
	 
		
	  
   
      
     
     /*
      if($cantidad_alicuota_iva_19 == 0 && $comprobante["TipoComprobante"]["letra"] !="C" && $comprobante["TipoComprobante"]["letra"] !="B"){
       
       $cantidad_alicuota_iva_19 = $comprobante_obj->getCantidadIvas($comprobante["Comprobante"]["id"],$comprobante["TipoComprobante"]["id_sistema"],0,0,0,0);
       
       
       }
     
      */
      
      $cantidad_ivas_tasa_mayor_cero = $comprobante_obj->getCantidadIvasTasaMayorACero($comprobante["Comprobante"]["id"],$comprobante["TipoComprobante"]["id_sistema"]);
      
      
      if( $cantidad_ivas_tasa_mayor_cero == 0) //esto se da cuando hay un 21 y exento por ejemplo
      	$cantidad_alicuota_iva_19 = 1;
	else
		$cantidad_alicuota_iva_19 = $cantidad_ivas_tasa_mayor_cero;
	
	
	
	   if($comprobante["TipoComprobante"]["letra"] =="C"  || $comprobante["TipoComprobante"]["letra"] =="B")
         	$cantidad_alicuota_iva_19 = 0;
      
      
      
      $codigo_operacion_20 = $comprobante_obj->getCodigoOperacionAfip($comprobante["Comprobante"]["id"],$total_no_gravado_aux,$exento);
        
      
      /*
      if(($total_no_gravado_aux + $exento) == $comprobante["Comprobante"]["subtotal_neto"])
      	$cantidad_alicuota_iva_19 = 1;
        */
        
   
        
       $total_iva_21   = $comprobante_obj->getTotalPorImpuestoGeografia($comprobante["Comprobante"]["id"],EnumImpuestoGeografia::ImpuestoNacional,EnumImpuesto::IVACOMPRAS);
       $total_iva_21 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($total_iva_21,$field_valor_moneda, $comprobante["Comprobante"]);
       $total_iva_21      = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_iva_21,13,2,"0");
       
        
       $credito_fiscal_computable_21 = $total_iva_21;
       
       $otros_tributos_22 = "000000000000000";
       $cuit_emisor_corredor_23 = "00000000000";
       
       $denominacion_emisor_corredor_24 = "                              ";
       $iva_comision_25 = "000000000000000";
       
       
       
       
       $linea_texto = $fecha_comprobante_01.$tipo_comprobante_afip_02.$punto_venta_03.$nro_comprobante_04.$nro_despacho_importacion_05.$codigo_doc_afip_06.$cuit_final_07.$razon_social_08.$importe_total_operacion_09.$importe_total_concepto_no_integran_neto_gravado_010.$importe_operaciones_exento_11.$total_impuestos_nacionales_iva_12.$total_impuestos_nacionales_13.$total_impuestos_provinciales_14.$total_impuestos_municipales_15.$total_impuestos_interno_16.$codigo_moneda_17.$tipo_cambio_18.$cantidad_alicuota_iva_19.$codigo_operacion_20.$credito_fiscal_computable_21.$otros_tributos_22.$cuit_emisor_corredor_23.$denominacion_emisor_corredor_24.$iva_comision_25;
       echo $linea_texto.  "\r\n";       
    }
        


?>