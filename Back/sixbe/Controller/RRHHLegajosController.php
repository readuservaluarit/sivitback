<?php

App::uses('PersonasController', 'Controller');

class RRHHLegajosController extends AppController { 

    public $name = 'RRHHLegajos';
    public $model = Legajo::class;
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $id_tipo_persona = EnumTipoPersona::Legajo;
  
  
    /**
    * @secured(CONSULTA_RRHH_LEGAJO)
    */
    public function index() {
      
    	if($this->request->is('ajax'))
    		$this->layout = 'ajax';
    		
    		$this->loadModel($this->model);

    		$this->PaginatorModificado->settings = array('limit' => $this->numrecords,
    				'update' => 'main-content', 'evalScripts' => true);
    		
    		
    		
    		//$conditions = $this->RecuperoFiltros($this->model);
    		

    		
    		
    		$this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
    				'contain' =>array(RRHHPersonaConcepto::class =>array(RRHHConcepto::class)),
    				'conditions' => $conditions,
    				'limit' => $this->numrecords,
    				'page' => $this->getPageNum(),
    				'order'=>"Legajo.codigo asc"
    		);
    		$this->PaginatorModificado->settings = $this->paginate;
    		
    		$this->paginado = 0;
    		$data = $this->PaginatorModificado->paginate($this->model);
    		$page_count = $this->params['paging'][$this->model]['pageCount'];
    		
    		foreach($data as &$persona){
    			
    			
    			foreach ($persona[RRHHPersonaConcepto::class] as &$concepto){
    				
    				$concepto["d_rrhh_concepto"] = $concepto[RRHHConcepto::class]["d_rrhh_concepto"];
    			
    				unset($concepto[RRHHConcepto::class]);
    			}
    			
    			
    			$persona[$this->model][RRHHPersonaConcepto::class] = $persona[RRHHPersonaConcepto::class];
    			
    			
    			
    			unset($persona[RRHHPersonaConcepto::class]);
    		}
    		
    		
    		
    		$this->data = $data;
    		
    		$this->set('data',$this->data );
    		
    		
    		$seteo_form_builder = array("showActionColumn" =>false,"showNewButton" =>false,"showActionColumn"=>false,"showFooter"=>true,"showHeaderListado"=>true);
    		
    		$this->title_form = "Listado";
    		
    		$this->preparaHtmLFormBuilder($this,$seteo_form_builder,"default");
    		
    		$output = array(
    				"status" =>EnumError::SUCCESS,
    				"message" => "list",
    				"content" => $data,
    				"page_count" =>$page_count
    		);
    		$this->set($output);
    		$this->set("_serialize", array("status", "message","page_count", "content"));
    		
    		

    }
    
    
    
   

    /**
    * @secured(ABM_RRHH_LEGAJO)
    */
    public function view($id) {        
        parent::view($id);
        return;
    }

    /**
    * @secured(ADD_RRHH_LEGAJO)
    */
    public function add() {
        
        
        
       $this->request->data["Persona"]["id_tipo_persona"] = $this->id_tipo_persona;
        parent::add(); 
        return;
        
      
    }
   
   
   /**
    * @secured(MODIFICACION_RRHH_LEGAJO)
    */ 
   public function edit($id) {
   	
   	
   	
   	try{
   		
       $this->loadModel("Persona");
       $this->loadModel("Comprobante");
       
       
       $cliente = $this->Persona->find('first', array(
       		'conditions' => array('Persona.id' => $id),
       		'contain' =>false
       )); 
   	    
       
      
   	    	$this->request->data["Persona"]["id_tipo_persona"] = $this->id_tipo_persona;
        
        	parent::edit($id);  
        

   	    
   	}catch(Exception $e){
   		
   		
   		$output = array(
   				"status" =>EnumError::ERROR,
   				"message" => "ERROR: ".$e->getMessage(),
   				"content" => ""
   		);
   		
   		$this->set($output);
   		$this->set("_serialize", array("status", "message", "content"));

   	}
        
        return;    
  }
    
  
    /**
    * @secured(BAJA_RRHH_LEGAJO)
    */
    function delete($id) {
        
        parent::delete($id);
        return;
        
    }
    
    
         /**
        * @secured(CONSULTA_RRHH_LEGAJO)
        */
        public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
            //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
            $this->set('model',$model);
            
            Configure::write('debug',0);
            $this->render($vista);
            
            
                
    
     
        }
        
   
    /**
    * @secured(CONSULTA_RRHH_LEGAJO)
    */
    public function existe_cuit(){
        
       parent::existe_cuit();
       return;
    }
    
    
     /**
    * @secured(CONSULTA_RRHH_LEGAJO)
    */
    public function existe_codigo($codigo='',$id_persona='',$llamada_interna=0){
        
       
       $dato = parent::existe_codigo($codigo,$id_persona,$llamada_interna);
       if($llamada_interna == 1)
        return $dato;
       else
        return;
    }
    
    /**
     * @secured(BTN_EXCEL_RRHH_LEGAJOS)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Clientes");
    	
    	
    	
    }
    
    
    /**
     * @secured(CONSULTA_RRHH_LEGAJO)
     */
    public function getDomicilio(){
    	
    	
     parent::getDomicilio();
     return ;
    	
    }
    
    /**
     * @secured(CONSULTA_RRHH_LEGAJO)
     */
    public function getDataCuitFromAfip(){
    	
    	
    	parent::getDataCuitFromAfip();
    	return ;
    	
    }
    
    public function formula(){
    
    App::import('Vendor', 'EvalMath', array('file' => 'Classes/evalmath.class.php'));
    $m = new EvalMath();
    $m->suppress_errors = true;
    if ($m->evaluate('y(x) = ((1+900)/3)*5')) {
    	
    	echo $m->e("y(0);");
    	die();
    
    
    
    }
    
    }
    
}
?>