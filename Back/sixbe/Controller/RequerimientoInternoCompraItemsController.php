<?php

App::uses('ComprobanteItemsController', 'Controller');

class RequerimientoInternoCompraItemsController extends ComprobanteItemsController {
    
    public $name = EnumController::RequerimientoInternoCompraItems;
    public $model = 'ComprobanteItem';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    

     /**
    * @secured(CONSULTA_REQUERIMIENTO_COMPRA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model); 
        
          
       
        
		/*
        $this->paginate = array(
            'paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' =>array('Comprobante'=>array('EstadoComprobante','Moneda','PuntoVenta','Persona'),'Producto'=>array('ProductoTipo'),'Unidad','Iva'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order'=>$this->{$this->model}->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_comprobante') )
        );
        
        */
        
        
        
        
        $filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');
        $id_moneda_expresion = $this->getFromRequestOrSession('Comprobante.id_moneda_expresion');
        
        
        //$filtro_agrupado = 3; //MP antes tenia uno 1 si manda un /index plano me agrupaba los items y no me traie los campos q necesito para el form PIMasterDetail0
        $this->ComprobanteItem->paginateAgrupado($this,$filtro_agrupado,$conditions,
        		$this->numrecords,$this->getPageNum(),
        		array('Unidad','Comprobante'=>array('EstadoComprobante','Moneda','PuntoVenta','Persona','TipoComprobante'),'Unidad','Iva',
        				'Producto'=>array('ProductoTipo'),'ComprobanteItemOrigen'),$this->ComprobanteItem->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_comprobante')),$id_moneda_expresion); // AGREGADO PARA EL ORDEN DE ISCO
        		
        		
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));


        //Headers
        $formBuilder->addHeader('id', 'Factura.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'Factura.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
    else{ // vista json

        $this->PaginatorModificado->settings = $this->paginate; 
        

        $data = $this->PaginatorModificado->paginate($this->model);
    
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        $this->loadModel("Comprobante");

        foreach($data as $key=>&$producto ){
            
        	
        	
        	
        	switch($filtro_agrupado){
        		
        		
        		
        		case "":
        		case EnumTipoFiltroComprobanteItem::individual:
        		case EnumTipoFiltroComprobanteItem::agrupadaporComprobante:
        			
	                 $this->Comprobante->formatearFechas($producto);
	                 
	                 $producto["ComprobanteItem"]["d_producto"] = $producto["Producto"]["d_producto"];
	                 $producto["ComprobanteItem"]["id_moneda"] = $producto["Comprobante"]["id_moneda"];
	                 $producto["ComprobanteItem"]["d_iva"] = $producto["Iva"]["d_iva"];
	                 
	                  $producto["ComprobanteItem"]["razon_social"] = $producto["Comprobante"]["Persona"]["razon_social"];
	                  $producto["ComprobanteItem"]["persona_tel"] = $producto["Comprobante"]["Persona"]["tel"];
	                 
	                 
	                 $producto["ComprobanteItem"]["valor_moneda"] = $producto["Comprobante"]["valor_moneda"];
	                 $producto["ComprobanteItem"]["moneda_simbolo"] = $producto["Comprobante"]["Moneda"]["simbolo"];
	                 
	                
	                 if( isset($producto["ComprobanteItem"]["fecha_entrega"]) && $this->{$this->model}->validateDate($producto["ComprobanteItem"]["fecha_entrega"],'d-m-Y') )
	                 	$producto["ComprobanteItem"]["dias_atraso_item"] = $this->ComprobanteItem->getDiasAtraso($producto["ComprobanteItem"]["fecha_entrega"]); //TODO: REVISAR
	                 else
	                 	$producto["ComprobanteItem"]["dias_atraso_item"] = 0;
	                 			
	                 if( $producto["ComprobanteItem"]["fecha_entrega"] && $this->{$this->model}->validateDate($producto["Comprobante"]["fecha_entrega"],'Y-m-d') )
	                 	$producto["ComprobanteItem"]["dias_atraso_global"] = $this->ComprobanteItem->getDiasAtraso($producto["Comprobante"]["fecha_entrega"]);
	                 else
	                    $producto["ComprobanteItem"]["dias_atraso_global"] = 0;
	                 					
	                 
	                 
	                 
	                 $producto["ComprobanteItem"]["d_estado_comprobante"] = $producto["Comprobante"]["EstadoComprobante"]["d_estado_comprobante"];
	                 
	                 $producto["ComprobanteItem"]["codigo"] = $this->ComprobanteItem->getCodigoFromProducto($producto);
	                 
	                 $this->ComprobanteItem->CalculaUnidad($producto);
	          
	                 //$producto["ComprobanteItem"]["total"] = (string) $this->{$this->model}->getTotalItem($producto);
	                 
	                 
	                
	                 if(isset($producto["Comprobante"]["PuntoVenta"]["numero"]) && $producto["Comprobante"]["PuntoVenta"]["numero"]>0)
	                 	$producto["ComprobanteItem"]["pto_nro_comprobante"] = $this->ComprobanteItem->GetNumberComprobante($producto["Comprobante"]["PuntoVenta"]["numero"],$producto["Comprobante"]["nro_comprobante"]);
				     else 
				     	$producto["ComprobanteItem"]["pto_nro_comprobante"] = $this->ComprobanteItem->GetNumberComprobante($producto["Comprobante"]["d_punto_venta"],$producto["Comprobante"]["nro_comprobante"]);
	                 
	                 $facturados =$this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItem"], $this->Comprobante->getTiposComprobanteController(EnumController::FacturasCompra) );
	                 
	                 $remitidos = $this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItem"], $this->Comprobante->getTiposComprobanteController(EnumController::RemitosCompra));
	                 
	                 $total_remitidos = 0;
	                  
	                 
	                 
	                 $producto["ComprobanteItem"]["total_facturado"] = (string)  number_format((float)$this->ComprobanteItem->GetCantidadItemProcesados($facturados), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
	                 $producto["ComprobanteItem"]["total_remitido"] = (string) number_format((float)($this->ComprobanteItem->GetCantidadItemProcesados($remitidos)), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
	                 $producto["ComprobanteItem"]["cantidad_pendiente_a_facturar"] = (string) number_format( (float)($producto["ComprobanteItem"]["cantidad"] - $producto["ComprobanteItem"]["total_facturado"]), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
	                 $producto["ComprobanteItem"]["cantidad_pendiente_a_remitir"] = (string) number_format((float)($producto["ComprobanteItem"]["cantidad"] -  $producto["ComprobanteItem"]["total_remitido"]), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
	                 
	                
	                 $producto["ComprobanteItem"]["nro_comprobante"] = $producto["Comprobante"]["nro_comprobante"];
	
	                 
	                 $fecha = new DateTime($producto["Comprobante"]["fecha_generacion"]);
	                 $producto["ComprobanteItem"]["fecha_generacion"] = $fecha->format('d-m-Y');;
	                 
	                 
	                 
	                 $joins = array(
	                                array(
	                                    "table"=>'comprobante',
	                                    //"type"=>'JOIN',
	                                    "alias"=>'RemitosVenta',
	                                    "conditions"=>array("ComprobanteItem.id_comprobante = RemitosVenta.id")
	                                    ),
	                                    
	                                 array(
	                                    "table"=>'comprobante_item',
	                                    //"type"=>'JOIN',
	                                    "alias"=>'irm_item',
	                                    "conditions"=>array("irm_item.id_comprobante_item_origen = ComprobanteItem.id")
	                                    ),
	                                  array(
	                                    "table"=>'comprobante',
	                                    //"type"=>'JOIN',
	                                    "alias"=>'irm',
	                                    "conditions"=>array("irm.id = irm_item.id_comprobante")
	                                    )
	                            
	                            
	                            );
	                            
	                 $conditions = array();
	                 
	                 
	                 array_push($conditions,array("ComprobanteItem.id_comprobante_item_origen"=>$producto["ComprobanteItem"]["id"]));
	                 array_push($conditions,array("RemitosVenta.id_tipo_comprobante"=>EnumTipoComprobante::RemitoCompra));
	                 array_push($conditions,array("irm.id_tipo_comprobante"=>EnumTipoComprobante::InformeRecepcionMateriales));
	                 array_push($conditions,array("ComprobanteItem.activo"=>1));
	                 array_push($conditions,array("irm.id_estado_comprobante"=>array(EnumEstadoComprobante::CerradoReimpresion)));
	                 
	                $cantidad_irm = $this->ComprobanteItem->find('all',
	                                                  array(
	                                                'joins'=>$joins,
	                                                'fields'=>array('IFNULL(SUM(irm_item.cantidad_cierre),0) as cantidad'),
	                                            'conditions' => $conditions,
	                                            )
	                 
	                 );
	                 
	                $cant_recibida = $cantidad_irm[0][0]["cantidad"]; 
	                 //el Irm importa de Remito de Venta y el remito de venta de OC
	                 
	                 $producto["ComprobanteItem"]["cantidad_recibida_irm"] = (string) $cant_recibida;
	                 $producto["ComprobanteItem"]["cantidad_pendiente_irm"] = (string)  ($producto["ComprobanteItem"]["cantidad"] - $cant_recibida); 
	                 
	                 
	                 if($producto["ComprobanteItem"]["requiere_conformidad"] == 1){
	                 	
	                 	$producto["ComprobanteItem"]["cantidad_aprobada_a_facturar"] = (string) number_format( (float)($producto["ComprobanteItem"]["cantidad_recibida_irm"] - $producto["ComprobanteItem"]["total_facturado"]), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
	                 	
	                 }else{
	                 	
	                 	$producto["ComprobanteItem"]["cantidad_aprobada_a_facturar"] = (string) number_format( (float) ($producto["ComprobanteItem"]["cantidad_pendiente_a_facturar"]), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
	                 	
	                 }
	                 
	                 unset($producto["Producto"]);
	                 unset($producto["Comprobante"]);
	                 unset($producto["Unidad"]);
	                
	                 $this->FiltrarCamposOnTheFly($data,$producto,$key);
	                 
	                 break;
	         
	                 
        		case EnumTipoFiltroComprobanteItem::agrupadaporPersona:
        			
        			$producto["ComprobanteItem"]["total"] = $producto[0]["total"] ;
        			$producto["ComprobanteItem"]["razon_social"] = $producto["Persona"]["razon_social"];
        			
        			break;
        			
        			
	                 
        	}
		}
        
        $this->data = $this->prepararRespuesta($data,$page_count);
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $this->data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    
     /**
    * @secured(CONSULTA_REQUERIMIENTO_COMPRA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
      
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        
     
      $this->set('model',$model);
      Configure::write('debug',0);
      $this->render($vista);
        
       
   
        

        
    }
    
    
    
    
    /**
     * @secured(CONSULTA_REQUERIMIENTO_COMPRA)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	$this->paginado =0;
    	
    	$filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');
    	
    	
    	
    	switch($filtro_agrupado){
    		
    		case EnumTipoFiltroComprobanteItem::individual:
    		case "":
    			$view = "resumen_cabecera";
    			break;
    		case EnumTipoFiltroComprobanteItem::agrupadaporComprobante:
    			$view="agrupadaporcomprobante";
    			break;
    		case EnumTipoFiltroComprobanteItem::agrupadaporPersona:
    			$view="agrupadaporpersona";
    			break;
    			
    	}
    	
    	$this->ExportarExcel($view,$metodo,"Listado de Requerimientos de Compra");
    	
    	
    	
    }
    
    
    
    
     public function prepararRespuesta($data,&$page_count){
        
        
        
    $filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');//dice como mostrar los items    
    $array_id_comprobantes = array();
    
    
    
    
    
    if($filtro_agrupado == EnumTipoFiltroComprobanteItem::agrupadaporComprobante){ //si es la vista agrupada tomo los filtros de los items y agrupo en memoria por id_comprobante
    
    
        
       foreach($data as $key=>$valor){
           if(in_array($valor["ComprobanteItem"]["id_comprobante"],$array_id_comprobantes))
            unset($data[$key]);
           else
            array_push($array_id_comprobantes,$valor["ComprobanteItem"]["id_comprobante"]);
           
       }
       
        
        
        
    }    
        
    return parent::prepararRespuesta($data,$page_count);
    }
    

    
   
    
}
?>