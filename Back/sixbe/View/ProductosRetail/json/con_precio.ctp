<?php
        include(APP.'View'.DS.'Articulo'.DS.'json'.DS.'articulo.ctp');
        
        
        App::import('Model','Comprobante');
  		App::import('Lib','SecurityManager');
		$comprobante = new Comprobante();
		$stock = $comprobante->getActivoModulo(EnumModulo::STOCK);
		
		
		 $sm = new SecurityManager(); 
  
         
         
		$talle = $sm->permiso_particular("CONSULTA_TALLE",AuthComponent::user('username'))>0;
		$color = $sm->permiso_particular("CONSULTA_COLOR",AuthComponent::user('username'))>0;
        
        
     
        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "C&oacute;digo";
        $model["codigo"]["width"] = "12";
        $model["codigo"]["order"] = "2";
        $model["codigo"]["text_colour"] = "#000000";
        
        $model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "Descripci&oacute;n";
        $model["d_producto"]["width"] = "35";
        $model["d_producto"]["order"] = "3";
        $model["d_producto"]["text_colour"] = "#0000FF";
        
        $model["d_unidad"]["show_grid"] = 1; 
        $model["d_unidad"]["header_display"] = "Unidad";
        $model["d_unidad"]["width"] = "5";
        $model["d_unidad"]["order"] = "4";
        $model["d_unidad"]["text_colour"] = "#0000DD";
        
        $model["costo"]["show_grid"] = 1;
        $model["costo"]["header_display"] = "Costo";
        $model["costo"]["width"] = "10";
        $model["costo"]["order"] = "5";
        $model["costo"]["text_colour"] = "#DD0000";
        
        $model["d_moneda_precio"]["show_grid"] = 1;
        $model["d_moneda_precio"]["header_display"] = "Moneda";
        $model["d_moneda_precio"]["width"] = "10";
        $model["d_moneda_precio"]["order"] = "6";
        $model["d_moneda_precio"]["text_colour"] = "#DD0000";
        
        $model["precio"]["show_grid"] = 1;
        $model["precio"]["header_display"] = "Precio";
        $model["precio"]["width"] = "10";
        $model["precio"]["order"] = "7";
        $model["precio"]["text_colour"] = "#DD0000";
        
		$model["precio_minimo_producto"]["show_grid"] = 1;
        $model["precio_minimo_producto"]["header_display"] = "Precio m&iacute;nimo";
        $model["precio_minimo_producto"]["width"] = "10";
        $model["precio_minimo_producto"]["order"] = "9";
        $model["precio_minimo_producto"]["text_colour"] = "#CC0033";
        
        $model["peso"]["show_grid"] = 1;
        $model["peso"]["header_display"] = "Peso";
        $model["peso"]["width"] = "10";
        $model["peso"]["order"] = "10";
        $model["peso"]["text_colour"] = "#AA0077";
        
        
        
         
        if($stock == 1){
        
        	$model["stock"]["show_grid"] = 1;
        	$model["stock"]["header_display"] = "Stock";
        	$model["stock"]["width"] = "10";
        	$model["stock"]["order"] = "11";
        	$model["stock"]["text_colour"] = "#0000FF";
        }
        
        
        if($talle){
        
        	$model["d_talle"]["show_grid"] = 1;
        	$model["d_talle"]["header_display"] = "Talle";
        	$model["d_talle"]["width"] = "10";
        	$model["d_talle"]["order"] = "12";
        	$model["d_talle"]["text_colour"] = "#0000FF";
        }
        
        
        if($color){
        
        	$model["d_color"]["show_grid"] = 1;
        	$model["d_color"]["header_display"] = "Color";
        	$model["d_color"]["width"] = "10";
        	$model["d_color"]["order"] = "13";
        	$model["d_color"]["text_colour"] = "#0000FF";
        }
        
		
		$model["revision"]["show_grid"] = 1;
        $model["revision"]["header_display"] = "RV";
        $model["revision"]["width"] = "5";
        $model["revision"]["order"] = "14";
        $model["revision"]["text_colour"] = "#0000FF";

         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ProductoRetail",
                    "content" => $model
                );
        
          echo json_encode($output);
?>