SELECT 
  persona.razon_social,
persona_categoria.`d_persona_categoria`,
producto.`codigo`,
comprobante.`fecha_generacion`,
ROUND((comprobante_item.`precio_unitario`/comprobante.valor_moneda),2) AS precio_unitario, 
comprobante_item.`cantidad`,
comprobante_item.`descuento_unitario`,

ROUND((((100 - comprobante_item.`descuento_unitario`)*(comprobante_item.`precio_unitario`/comprobante.valor_moneda))/100)*comprobante_item.`cantidad`,2) AS total



FROM comprobante_item
JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`
JOIN persona ON persona.id = comprobante.id_persona
JOIN persona_categoria ON persona.`id_persona_categoria` = persona_categoria.`id`
JOIN producto ON producto.id = comprobante_item.`id_producto`
WHERE comprobante.`id_tipo_comprobante` = 202
AND id_estado_comprobante<>2
AND YEAR(comprobante.`fecha_generacion`)=2018
AND comprobante_item.`activo` = 1
LIMIT 1000000000000