<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class ReporteTableroUsuario extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'reporte_tablero_usuario';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
       
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
        'ReporteTablero' => array(
            'className' => 'ReporteTablero',
            'foreignKey' => 'id_reporte_tablero',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'id_usuario',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
   );
   
   
   
    public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
//     "codigo_hijo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),

    
     );
     
     }
     
     
  

}
