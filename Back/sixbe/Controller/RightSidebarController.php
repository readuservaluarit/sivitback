<?php
    App::uses('ComprobantesController', 'Controller');
    App::uses('AlarmasUsuarioController', 'Controller');

  
    class RightSidebarController extends AppController{

    	public $name = EnumController::RightSidebar;
        //public $model = 'AlarmaUsuario';
        public $helpers = array ('Session', 'Js');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
        public $requiere_impuestos = 1; //si el comprobante acepta impuesto, esto llama a las funciones de IVA E IMPUESTOS
        public $type_message = array();

       
        /**
        * @secured(CONSULTA_FACTURA)
        */
        public function index() 
        {
            if($this->request->is('ajax'))
                $this->layout = 'ajax';/*sin ";" throw exception ERROR 505*/

                
            $this->model = EnumModel::AlarmaUsuario;
            $this->loadModel($this->model);
            $this->loadModel(EnumModel::Moneda);
            //$this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

          
			
			

            $array_contain = array('Usuario','Alarma'); //sucursal

        
            //$conditions = array("Alarma.activa"=>1);
			$conditions = array();
			
			array_push($conditions, array('Alarma.activa' => 1));
			array_push($conditions, array("AlarmaUsuario.id_usuario"=>$this->Auth->user('id')));
            		
            $this->paginate = array(
         //       'fields'=>array('Comprobante.*'),
                'paginado' =>$this->paginado,
            	'contain' =>$array_contain,
                'conditions' => $conditions,
                'limit' => $this->numrecords,
            	'maxLimit'=>$this->maxLimitRows,
                'page' => $this->getPageNum(),
                'order' => $this->model.'.id desc'
            );  
            
            $this->PaginatorModificado->settings = $this->paginate;
             

            $data = $this->PaginatorModificado->paginate($this->model);
            
            
            foreach($data as $key=>&$valor){
            	
            	$count = $this->{$this->model}->query($valor["Alarma"]["sql_vista_resumen"]);//esta query siempre debe devolver una cantidad
            	if($count[0][0]["cant"]>0){
            		
            		$valor["AlarmaUsuario"]["id_alarma"] = $valor["Alarma"]["id"];
            		$valor["AlarmaUsuario"]["d_alarma"] = $valor["Alarma"]["d_alarma"];
            		$valor["AlarmaUsuario"]["d_mensaje_alarma"] = $valor["Alarma"]["d_mensaje_alarma"];
            		$valor["AlarmaUsuario"]["color"] = $valor["Alarma"]["color"]; //red orange yellow
            		$valor["AlarmaUsuario"]["cantidad"] = $count[0][0]["cant"]; //red orange yellow
      
            	}else{
            		
            		unset($data[$key]);
            		
            	}
            }
            
            
            
            
                
            
            
         //   $this->data = $data;
            
            
           // $seteo_form_builder = array("showActionColumn" =>false,"showNewButton" =>false,"showNewButton"=>false,"showActionColumn"=>false,"showFooter"=>true,"showHeaderListado"=>false,"showXlsButton"=>false);
            
            $alarma_usuario = new AlarmasUsuarioController();
            $alarma_usuario->name = EnumController::AlarmasUsuario;
            $alarma_usuario->model = EnumModel::AlarmaUsuario;
            $alarma_usuario->data = $data;
            
           // $listado = $this->preparaHTMLListadoFormBuilder($alarma_usuario,"Alarmas",$seteo_form_builder);
            
            
            $this->set('data_listado_alarmas',$data);
            
            
            
            
            
            
            
            $this->model = EnumModel::ReporteTableroUsuario;
            $this->loadModel($this->model);
            //$this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
            
            
            
            
            
            $array_contain = array('Usuario','ReporteTablero'); //sucursal
            
            
            //$conditions = array("Alarma.activa"=>1);
            $conditions = array();
            
            array_push($conditions, array("ReporteTablero.tipo_reporte_tablero"=>EnumTipoReporteTablero::Tabla));
            
            array_push($conditions, array("ReporteTableroUsuario.id_usuario"=>$this->Auth->user('id')));
            
            $this->paginate = array(
            		//       'fields'=>array('Comprobante.*'),
            		'paginado' =>$this->paginado,
            		'contain' =>$array_contain,
            		'conditions' => $conditions,
            		'limit' => $this->numrecords,
            		'maxLimit'=>$this->maxLimitRows,
            		'page' => $this->getPageNum(),
            		'order' => $this->model.'.id desc'
            );
            
            $this->PaginatorModificado->settings = $this->paginate;
            
            
            $data = $this->PaginatorModificado->paginate($this->model);
            
            
            foreach($data as $key=>&$valor){
            	
          
            		
            		$valor["ReporteTableroUsuario"]["id_reporte_tablero_usuario"] = $valor["ReporteTablero"]["id"];
            		$valor["ReporteTableroUsuario"]["d_reporte_tablero_usuario"] = $valor["ReporteTablero"]["d_reporte_tablero"];
            		$valor["ReporteTableroUsuario"]["d_mensaje_reporte_tablero_usuario"] = $valor["ReporteTablero"]["d_mensaje_reporte_tablero"];
            		$valor["ReporteTableroUsuario"]["color"] = $valor["ReporteTablero"]["color"]; //red orange yellow
            		
            		if(isset($count[0][0]["cant"]))
            			$valor["ReporteTableroUsuario"]["cantidad"] = $count[0][0]["cant"]; //red orange yellow
            		
      
            }
            
            
            
            
            
            
            
            //   $this->data = $data;
            
            
            // $seteo_form_builder = array("showActionColumn" =>false,"showNewButton" =>false,"showNewButton"=>false,"showActionColumn"=>false,"showFooter"=>true,"showHeaderListado"=>false,"showXlsButton"=>false);
            
            $alarma_usuario = new AlarmasUsuarioController();
            $alarma_usuario->name = EnumController::AlarmasUsuario;
            $alarma_usuario->model = EnumModel::AlarmaUsuario;
            $alarma_usuario->data = $data;
            
            
            
            // $listado = $this->preparaHTMLListadoFormBuilder($alarma_usuario,"Alarmas",$seteo_form_builder);
            
            
            $this->set('data_listado_reporte_tablero',$data);
            $this->set('data_sidebar_widgets',$this->getSidebarWidgets());//obtengo los widgets del sidebar home
            $this->set('data_cotizacion_dolar_sivit',round($this->Moneda->getCotizacion(EnumMoneda::Peso),2));//obtengo los widgets del sidebar home
            
            $usuario["Usuario"]["id"] = $this->Auth->user('id');
            $usuario["Usuario"]["nombre"] = $this->Auth->user('nombre');
            $this->set('usuario',$usuario);
            $this->set('app_path',$this->Session->read('Empresa')["DatoEmpresa"]["app_path"]);
      
            
            
			$this->layout = "desktop_panel";
                
            
           
   

            
            //fin vista json

        }
        
        
        protected function getSidebarWidgets(){
        	
        	$this->loadModel(EnumModel::ReporteTableroUsuario);
        	
     
        	
        	$widgets =  $comprobante = $this->ReporteTableroUsuario->find('all', array(
        			'conditions' => array(
        					'ReporteTablero.tipo_reporte_tablero' => array(EnumTipoReporteTablero::SidebarWidget,EnumTipoReporteTablero::SidebarWidgetNumeroCount,EnumTipoReporteTablero::SidebarWidgetDobleNumeroCount),
        					'ReporteTablero.activa'=>1,
        					"ReporteTableroUsuario.id_usuario"=>$this->Auth->user('id')
        			) ,
        			'contain' => array("ReporteTablero"),
        			'order'=>'ReporteTableroUsuario.orden ASC'
        	));
        	
        	
        	$contenido = "";
        	$js = "";//se muestra al final del contenido
        	
        	
        	foreach($widgets as  $widget){
        		
        		
        	/*	
        		
        		if($widget["ReporteTablero"]["tipo_reporte_tablero"] == EnumTipoReporteTablero::SidebarWidgetNumeroCount){
        			
        			$count = $this->{$this->model}->query($widget["ReporteTablero"]["sql_vista_resumen"]);//esta query siempre debe devolver una cantidad
        			
        			$titulo = $widget["ReporteTablero"]["d_reporte_tablero"];
        			
        			$texto = str_replace("@numero", $count[0][0]["cant"], $widget["ReporteTablero"]["html_widget"]);
        			$contenido.= str_replace("@titulo", $widget["ReporteTablero"]["d_reporte_tablero"],$texto);
        			
        		}else{
        			
        			
        			$contenido.= $widget["ReporteTablero"]["html_widget"];
        			
        		}*/
        		
        		
        		
        		switch($widget["ReporteTablero"]["tipo_reporte_tablero"]){
		        		case EnumTipoReporteTablero::SidebarWidgetNumeroCount:
		        			
		        			$count = $this->{$this->model}->query($widget["ReporteTablero"]["sql_vista_resumen"]);//esta query siempre debe devolver una cantidad
		        			
		        			$titulo = $widget["ReporteTablero"]["d_reporte_tablero"];
		        			
		        			$texto =  str_replace("@numero", money_format('%!n', $count[0][0]["cant"]), $widget["ReporteTablero"]["html_widget"]);
		        			$contenido.= str_replace("@titulo", $widget["ReporteTablero"]["d_reporte_tablero"],$texto);
		        			break;
		        			
		        		case EnumTipoReporteTablero::SidebarWidgetDobleNumeroCount:
		        			
		        			$count = $this->{$this->model}->query($widget["ReporteTablero"]["sql_vista_resumen"]);//esta query siempre debe devolver una cantidad
		        			
		        			$titulo = $widget["ReporteTablero"]["d_reporte_tablero"];
		        			
		        			$texto = str_replace("@numero", money_format('%!n', $count[0][0]["cant"]), $widget["ReporteTablero"]["html_widget"]);
		        			
		        			$texto2 = str_replace("@pin", money_format('%!n', $count[0][0]["cant2"]), $texto);
		        			$contenido.= str_replace("@titulo", $titulo,$texto2);
		        			break;
		        			
		        			
		        			
		        		case 	EnumTipoReporteTablero::SidebarWidget:
		        			$contenido.= $widget["ReporteTablero"]["html_widget"];
		        			
		        			
		        			break;
		        			
		        			
        			
        		}	
        	
        		
        			$js.= $widget["ReporteTablero"]["js_widget"];
        		
        		
        		
        		
        	}
        	
        	$data["Widget"]["contenido_html"] = $contenido;
        	
        	$data["Widget"]["contenido_js"] = $js;
        	
        	return $data;
        	
        }
        
        
       
        
        
        
     
        
        
		
		
    
   


    }
?>