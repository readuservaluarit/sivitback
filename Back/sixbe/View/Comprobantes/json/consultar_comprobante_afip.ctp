<?php
	    include(APP.'View'.DS.'Comprobantes'.DS.'json'.DS.'default.ctp');
        
		$model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
        $model["pto_nro_comprobante"]["width"] = "11"; //REstriccion mas de esto no se puede achicar
        $model["pto_nro_comprobante"]["order"] = "2";
        $model["pto_nro_comprobante"]["text_colour"] = "#000000";
		$model["pto_nro_comprobante"]["read_only"] = 1;
		
		$model["cuit"]["show_grid"] = 1;
        $model["cuit"]["header_display"] = "Raz&oacute;n Social";
        $model["cuit"]["width"] = "20"; //REstriccion mas de esto no se puede achicar
        $model["cuit"]["order"] = "3";
        $model["cuit"]["text_colour"] = "#000000";
		$model["cuit"]["read_only"] = 1;
        
        $model["subtotal_neto"]["show_grid"] = 1;
        $model["subtotal_neto"]["header_display"] = "Total Neto";
        $model["subtotal_neto"]["width"] = "10";
        $model["subtotal_neto"]["order"] = "4";
        $model["subtotal_neto"]["text_colour"] = "#F0000F";
		$model["subtotal_neto"]["read_only"] = 1;
		
		$model["ComprobanteImpuestoTotal"]["show_grid"] = 1;
        $model["ComprobanteImpuestoTotal"]["header_display"] = "Total Impuestos";
        $model["ComprobanteImpuestoTotal"]["width"] = "10";
        $model["ComprobanteImpuestoTotal"]["order"] = "5";
        $model["ComprobanteImpuestoTotal"]["text_colour"] = "#F0000F";
		$model["ComprobanteImpuestoTotal"]["read_only"] = 1;
		
        $model["total_comprobante"]["show_grid"] = 1;
        $model["total_comprobante"]["header_display"] = "Total Comprobante";
        $model["total_comprobante"]["width"] = "10";
        $model["total_comprobante"]["order"] = "6";
        $model["total_comprobante"]["text_colour"] = "#F0000F";
		$model["total_comprobante"]["read_only"] = 1;
		
		$model["fecha_contable"]["show_grid"] = 1;
        $model["fecha_contable"]["header_display"] = "Fecha generación";
        $model["fecha_contable"]["width"] = "10";
        $model["fecha_contable"]["order"] = "7";
        $model["fecha_contable"]["text_colour"] = "#0000FF";
		$model["fecha_contable"]["read_only"] = 1;
		
		
		$model["fecha_vencimiento"]["show_grid"] = 1;
        $model["fecha_vencimiento"]["header_display"] = "Fecha Venc.";
        $model["fecha_vencimiento"]["width"] = "10";
        $model["fecha_vencimiento"]["order"] = "8";
        $model["fecha_vencimiento"]["text_colour"] = "#0000FF";
		$model["fecha_vencimiento"]["read_only"] = 1;
        
        $model["d_moneda"]["show_grid"] = 1;
        $model["d_moneda"]["header_display"] = "Valor Moneda";
        $model["d_moneda"]["width"] = "10";
        $model["d_moneda"]["order"] = "10";
        $model["d_moneda"]["text_colour"] = "#000000";
		$model["d_moneda"]["read_only"] = 1;
     
		$model["valor_moneda"]["show_grid"] = 1;
        $model["valor_moneda"]["header_display"] = "Valor Moneda";
        $model["valor_moneda"]["width"] = "10";
        $model["valor_moneda"]["order"] = "11";
        $model["valor_moneda"]["text_colour"] = "#000000";
		$model["valor_moneda"]["read_only"] = 1;
		
		$model["total_comprobante"]["show_grid"] = 1;
        $model["total_comprobante"]["header_display"] = "Total";
        $model["total_comprobante"]["width"] = "10";
        $model["total_comprobante"]["order"] = "12";
        $model["total_comprobante"]["text_colour"] = "#000000";
		$model["total_comprobante"]["read_only"] = 1;
		
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Comprobante",
                    "content" => $model
                );
        
        echo json_encode($output);
?>