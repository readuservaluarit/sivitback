<?php

App::uses('ArticuloController', 'Controller');

class ComprobantesPuntoVentaNumeroController extends AppController {
    public $name = 'ComprobantesPuntoVentaNumero';
    public $model = 'ComprobantePuntoVentaNumero';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    /**
    * @secured(CONSULTA_COMPROBANTE_PUNTO_VENTA_NUMERO)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
		$id = $this->getFromRequestOrSession($this->model.'.id');
        $id_tipo_comprobante = $this->getFromRequestOrSession($this->model.'.id_tipo_comprobante');
        $id_punto_venta = $this->getFromRequestOrSession($this->model.'.id_punto_venta');
		
		$d_tipo_comprobante = $this->getFromRequestOrSession($this->model.'.d_tipo_comprobante');
		
        $id_sistema = $this->getFromRequestOrSession($this->model.'.id_sistema');
        $signo_comercial_in = $this->getFromRequestOrSession($this->model.'.signo_comercial');//me envia un array -1,1
        $punto_venta_agrupado= $this->getFromRequestOrSession($this->model.'.punto_venta_agrupado');//flag para agrupar o no
        
		$factura_electronica = $this->getFromRequestOrSession($this->model.'.factura_electronica');
		
		$dato_empresa = $this->getFromRequestOrSession($this->model.'.dato_empresa');
		
        $conditions = array(); 
        
		 if($id_tipo_comprobante!="")
			array_push($conditions, array($this->model.'.id_tipo_comprobante =' => $id_tipo_comprobante )); 
		 if($id_punto_venta!="")
			array_push($conditions, array($this->model.'.id_punto_venta =' => $id_punto_venta )); 
		 if($factura_electronica!="")
			array_push($conditions, array('PuntoVenta.factura_electronica =' => $factura_electronica )); 

		 if($dato_empresa!="")
			array_push($conditions, array('PuntoVenta.dato_empresa =' => $dato_empresa ));

		 if($id_sistema!="")
			array_push($conditions, array('TipoComprobante.id_sistema =' => $id_sistema));
		
		if($d_tipo_comprobante !="")
			array_push($conditions, array('TipoComprobante.d_tipo_comprobante LIKE' => '%'.$d_tipo_comprobante.'%'));
          
		if($signo_comercial_in!=""){
			$signos_comerciales = explode(",",$signo_comercial_in);
			array_push($conditions, array('TipoComprobante.signo_comercial' => $signos_comerciales));
		}
		
		if($punto_venta_agrupado == ''){     
				$this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
					'contain' =>array('TipoComprobante'=>array('Sistema'),'PuntoVenta','Impuesto'),
					/*'contain' =>array('TipoComprobante'=>array('Sistema'),'PuntoVenta','Impuesto','TipoComprobantePuntoVentaNumero'),*/
					'conditions' => $conditions,
					'limit' => $this->numrecords,
					'page' => $this->getPageNum(),
					'order' => 'ComprobantePuntoVentaNumero.id_tipo_comprobante desc'
				);
		}else{
			$this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
				
					'contain' =>array('TipoComprobante'=>array('Sistema'),'PuntoVenta','Impuesto'),
					/*'contain' =>array('TipoComprobante'=>array('Sistema'),'PuntoVenta','Impuesto','TipoComprobantePuntoVentaNumero'),*/
					'conditions' => $conditions,
					'limit' => $this->numrecords,
					'page' => $this->getPageNum(),
					'order' => 'ComprobantePuntoVentaNumero.id_tipo_comprobante desc',
					'group' => array('ComprobantePuntoVentaNumero.id_punto_venta')
			);
		}	
          //$this->Security->unlockedFields = array('costo','precio'); 
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
        
        ////////////////////
        //Filters
        ///////////////////
        
        //CUE
        $formBuilder->addFilterInput('codigo', 'C&oacute;digo', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $codigo));
        $formBuilder->addFilterInput('d_COMPROBANTE_PUNTO_VENTA_NUMERO', 'Descripci&oacute;n', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $descripcion));
        $formBuilder->setDataListado($this, 'Listado de COMPROBANTE_PUNTO_VENTA_NUMEROs', 'Datos de los COMPROBANTE_PUNTO_VENTA_NUMEROs', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));

        //Headers
        $formBuilder->addHeader('id', 'COMPROBANTE_PUNTO_VENTA_NUMERO.id', "10%");
        $formBuilder->addHeader('C&oacute;digo', 'COMPROBANTE_PUNTO_VENTA_NUMERO.codigo', "20%");
        $formBuilder->addHeader('Precio', 'COMPROBANTE_PUNTO_VENTA_NUMERO.precio', "20%");
        $formBuilder->addHeader('Descripci&oacute;n', 'COMPROBANTE_PUNTO_VENTA_NUMERO.d_COMPROBANTE_PUNTO_VENTA_NUMERO', "30%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'codigo');
        $formBuilder->addField($this->model, 'precio');
        $formBuilder->addField($this->model, 'd_COMPROBANTE_PUNTO_VENTA_NUMERO');
 
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }

    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
       //parseo el data para que los models queden dentro del objeto de respuesta
         foreach($data as &$valor){
             $this->cleanforOutput($valor);
         }
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
     }

    //fin vista json
        
    }
    
    /**
    * @secured(ABM_USUARIOS)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->COMPROBANTE_PUNTO_VENTA_NUMERO->id = $id;
            $this->COMPROBANTE_PUNTO_VENTA_NUMERO->contain("Iva");
            $this->request->data = $this->COMPROBANTE_PUNTO_VENTA_NUMERO->read();
        } 
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Cliente');
        $id_iva = '';
        $d_iva = '';
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de COMPROBANTE_PUNTO_VENTA_NUMEROs');
        elseif ($mode == "M"){
            $formBuilder->setTituloForm('Modificaci&oacute;n de COMPROBANTE_PUNTO_VENTA_NUMERO');
            $id_iva=$this->request->data['Iva']['id'];
            $d_iva=$this->request->data['Iva']['d_iva'];
        }
        else
            $formBuilder->setTituloForm('Consulta de COMPROBANTE_PUNTO_VENTA_NUMERO');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('codigo', 'C&oacute;digo', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('d_COMPROBANTE_PUNTO_VENTA_NUMERO', 'Descripci&oacute;n', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('precio', 'Precio', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  
        //$formBuilder->addFormInput('stock', 'Stock', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        //$formBuilder->addFormInput('stock_minimo', 'Stock M&iacute;nimo', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        
        $selectionList = array($this->model."IdIva" => "id", $this->model."DIva" => "d_iva");
        $formBuilder->addFormPicker('IvaPicker', $selectionList, 'id_iva', 
                                   array('value' => $id_iva), 'd_iva', 'Iva', 
                                   array('class'=>'control-group'), array('class' => '', 'label' => false, 'value' => $d_iva)
                                   );  
       
         
        
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'COMPROBANTE_PUNTO_VENTA_NUMEROAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('COMPROBANTE_PUNTO_VENTA_NUMEROCodigo', 'Debe ingresar un C&oacute;digo');
                validator.validateRequired('COMPROBANTE_PUNTO_VENTA_NUMEROPrecio', 'Debe ingresar un Precio');
                validator.validateNumeric('COMPROBANTE_PUNTO_VENTA_NUMEROPrecio', 'El precio debe ser un n&uacute;mero');
                validator.validateRequired('COMPROBANTE_PUNTO_VENTA_NUMERODCOMPROBANTE_PUNTO_VENTA_NUMERO', 'Debe ingresar una descripcion');
                //validator.validateNumeric('COMPROBANTE_PUNTO_VENTA_NUMEROStock', 'El stock ingresado debe ser num&eacute;rico');
                //validator.validateNumeric('COMPROBANTE_PUNTO_VENTA_NUMEROStockMinimo', 'El stock ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_codigo',
                        'data': 'codigo=' + $('#COMPROBANTE_PUNTO_VENTA_NUMEROCodigo').val()+'&id='+$('#COMPROBANTE_PUNTO_VENTA_NUMEROId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('COMPROBANTE_PUNTO_VENTA_NUMEROCodigo','El C&oacute;digo que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

   
   
    
    /**
    * @secured(CONSULTA_COMPROBANTE_PUNTO_VENTA_NUMERO)
    */
    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    
   
    /**
    * @secured(ADD_COMPROBANTE_PUNTO_VENTA_NUMERO)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            
            $id_p = "";
            
            try{
                
				if ($this->ComprobantePuntoVentaNumero->saveAll($this->request->data, array('deep' => true))){
					$mensaje = "El Numero ha sido creado exitosamente";
					$tipo = EnumError::SUCCESS;
					$id_p = $this->ComprobantePuntoVentaNumero->id;
				   
				}else{
					$mensaje = "Ha ocurrido un error,el Numero no ha podido ser creado.";
					$tipo = EnumError::ERROR; 
				}                
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el Numero no ha podido ser creado .".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" => $id_p
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    
    
    /**
    * @secured(MODIFICACION_COMPROBANTE_PUNTO_VENTA_NUMERO)
    */
    function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
          
            $this->ComprobantePuntoVentaNumero->id = $id;
            $this->request->data["ComprobantePuntoVentaNumero"]["fecha_ultima_modificacion"] = date("Y-m-d H:i:s");
            
            try{ 
                if ($this->ComprobantePuntoVentaNumero->saveAll($this->request->data["ComprobantePuntoVentaNumero"]) ){

                  $mensaje = "El Numero ha sido modificado exitosamente";
                  $error = EnumError::SUCCESS;                  
                }else{
                    
                     $mensaje = "Ha ocurrrido un error el Numero NO ha sido modificado exitosamente";
                     $error = EnumError::ERROR;
                }
            }catch(Exception $e){
                $mensaje = "Ha ocurrrido un error el Numero NO ha sido modificado exitosamente".$e->getMessage();
                $error = EnumError::ERROR;
                
            }
            
		  if($this->RequestHandler->ext == 'json'){  
					$output = array(
						"status" => $error,
						"message" => $mensaje,
						"content" => ""
					); 
					
					$this->set($output);
					$this->set("_serialize", array("status", "message", "content"));
				}else{
					$this->Session->setFlash($mensaje, $error);
					$this->redirect(array('controller' => 'COMPROBANTE_PUNTO_VENTA_NUMEROs', 'action' => 'index'));
					
				} 
            
            
           
        }else{
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->COMPROBANTE_PUNTO_VENTA_NUMERO->id = $id;
            $this->COMPROBANTE_PUNTO_VENTA_NUMERO->contain();
            $this->request->data = $this->COMPROBANTE_PUNTO_VENTA_NUMERO->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
            }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
            }
        } 
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
  
    
    /**
    * @secured(BAJA_COMPROBANTE_PUNTO_VENTA_NUMERO)
    */
    function delete($id) {
        
        $this->loadModel($this->model);
     
        $mensaje = "";
        $status = "";
        $this->ComprobantePuntoVentaNumero->id = $id;
       try{  
           
            if ($this->ComprobantePuntoVentaNumero->delete()) {
                $status = EnumError::SUCCESS;
                $mensaje = "El Numero ha sido eliminado exitosamente.";
            }  
            else{
                 //throw new Exception();
                  $status = EnumError::ERROR;
                  $mensaje = "El Numero ha sido eliminado exitosamente.";
            }
             $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                 
          }
		  catch(Exception $ex)
		  { 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
    }
    
    /**
    * @secured(CONSULTA_COMPROBANTE_PUNTO_VENTA_NUMERO)
    */
    public function home(){
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
   
     /**
    * @secured(CONSULTA_COMPROBANTE_PUNTO_VENTA_NUMERO)
    */
    public function getModel($vista = 'default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER

            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }

        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
            $this->set('model',$model);
            // $this->set('this',$this);
            Configure::write('debug',0);
            $this->render($vista);
        }

    
  private function cleanforOutput(&$valor)
  {
             if(isset($valor['TipoComprobante']) && $valor['TipoComprobante']['id']!= null ){
                $valor['ComprobantePuntoVentaNumero']['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                $valor['ComprobantePuntoVentaNumero']['id_sistema'] = $valor['TipoComprobante']['id_sistema'];
             }else
                $valor['TipoComprobante']['d_tipo_comprobante'] = 'Sin Informar';
                
             // unset($valor['TipoComprobante']);          
            
            if(isset($valor['PuntoVenta']) && $valor['PuntoVenta']['id']!= null ){
                $valor['ComprobantePuntoVentaNumero']['d_punto_venta'] = $valor['PuntoVenta']['numero'];
                $valor['ComprobantePuntoVentaNumero']['factura_electronica'] = $valor['PuntoVenta']['factura_electronica'];
            }else{
                $valor['ComprobantePuntoVentaNumero']['d_punto_venta'] = 'Sin Informar';
                $valor['ComprobantePuntoVentaNumero']['factura_electronica'] = '0';
            }
                
                
              unset($valor['PuntoVenta']);
             
              if(isset($valor['TipoComprobante']) && $valor['TipoComprobante']['id']!= null )
                $valor['ComprobantePuntoVentaNumero']['d_comprobante_letra'] = $valor['TipoComprobante']['letra'];
              else
                $valor['ComprobantePuntoVentaNumero']['d_comprobante_letra'] = 'Sin Informar';
                
			if( isset($valor['TipoComprobante'])  
			 && isset($valor['TipoComprobante']['Sistema'])  
			 && isset($valor['TipoComprobante']['Sistema']['d_sistema']))
                $valor['ComprobantePuntoVentaNumero']['d_sistema'] = $valor['TipoComprobante']['Sistema']['d_sistema'];
                
              unset($valor['TipoComprobante']); 
              
              
              if(isset($valor['Impuesto']) && $valor['Impuesto']['id']!= null ){
              	$valor['ComprobantePuntoVentaNumero']['id_impuesto'] = $valor['Impuesto']['id'];
              	$valor['ComprobantePuntoVentaNumero']['d_impuesto'] = $valor['Impuesto']['d_impuesto'];
              }else{
              		$valor['TipoComprobante']['d_impuesto'] = '';
              		$valor['ComprobantePuntoVentaNumero']['id_impuesto'] = '';
              	}
              		
  }
    
}
?>