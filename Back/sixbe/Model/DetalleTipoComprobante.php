<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class DetalleTipoComprobante extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'detalle_tipo_comprobante';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 public $belongsTo = array(
        'TipoComprobante' => array(
            'className' => 'TipoComprobante',
            'foreignKey' => 'id_tipo_comprobante',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Impuesto' => array(
            'className' => 'Impuesto',
            'foreignKey' => 'id_impuesto',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ), 
        'CuentaContable' => array(
            'className' => 'CuentaContable',
            'foreignKey' => 'id_cuenta_contable',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
        );
	
 
 
 
 public function getDias($id_detalle_tipo_comprobante){
 	
 	$detalle_tipo_comprobante = $this->find('first', array(
 			'conditions' => array('DetalleTipoComprobante.id' => $id_detalle_tipo_comprobante),
 			'contain' => false
 	));
 	
 	return $detalle_tipo_comprobante["DetalleTipoComprobante"]["dias"];
 	
 }
 
 public function getDias2($id_detalle_tipo_comprobante){
 	
 	$detalle_tipo_comprobante = $this->find('first', array(
 			'conditions' => array('DetalleTipoComprobante.id' => $id_detalle_tipo_comprobante),
 			'contain' => false
 	));
 	
 	return $detalle_tipo_comprobante["DetalleTipoComprobante"]["dias2"];
 	
 }
 

}