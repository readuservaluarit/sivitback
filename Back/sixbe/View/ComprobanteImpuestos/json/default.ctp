<?php
        

        include(APP.'View'.DS.'ComprobanteImpuestos'.DS.'json'.DS.'comprobante_impuesto.ctp');  
         
         
         
        $model["fecha_contable"]["show_grid"] = 1;
        $model["fecha_contable"]["header_display"] = "Fecha";
        $model["fecha_contable"]["order"] = "0";
        $model["fecha_contable"]["width"] = "10";
        
         $model["d_tipo_comprobante"]["show_grid"] = 1;
        $model["d_tipo_comprobante"]["header_display"] = "Tipo Comprobante";
        $model["d_tipo_comprobante"]["order"] = "1";
        $model["d_tipo_comprobante"]["width"] = "10";
        
        $model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Comprobante";
        $model["pto_nro_comprobante"]["order"] = "2";
        $model["pto_nro_comprobante"]["width"] = "10";
        
        
        $model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["header_display"] = "Persona";
        $model["razon_social"]["order"] = "3";
        $model["razon_social"]["width"] = "10";
        
        $model["d_provincia"]["show_grid"] = 1;
        $model["d_provincia"]["header_display"] = "Provincia";
        $model["d_provincia"]["order"] = "4";
        $model["d_provincia"]["width"] = "10";
        
        $model["d_impuesto"]["show_grid"] = 1;
        $model["d_impuesto"]["header_display"] = "Impuesto";
        $model["d_impuesto"]["order"] = "5";
        $model["d_impuesto"]["width"] = "10";
        
      
        $model["base_imponible"]["show_grid"] = 1;
        $model["base_imponible"]["header_display"] = "Base Imponible";
        $model["base_imponible"]["order"] = "6";
        $model["base_imponible"]["width"] = "10";
        
        
        $model["tasa_impuesto"]["show_grid"] = 1;
        $model["tasa_impuesto"]["header_display"] = "Alic.(%)";
        $model["tasa_impuesto"]["order"] = "7";
        $model["tasa_impuesto"]["width"] = "10";
        
        $model["importe_impuesto"]["show_grid"] = 1;
        $model["importe_impuesto"]["header_display"] = "Importe";
        $model["importe_impuesto"]["order"] = "8";
        $model["importe_impuesto"]["width"] = "20";
        
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteImpuesto",
                    "content" => $model
                );
        echo json_encode($output);
?>