
SELECT 
  comprobante.`nro_comprobante`,
  comprobante.`razon_social`,
  persona_categoria.`d_persona_categoria`,
  comprobante.`fecha_generacion` AS fecha_gen_pedido,
  comprobante_item.`n_item`,
  producto.`codigo`,
  comprobante_item.`dias`,
  
  estado_comprobante.`d_estado_comprobante`,

  CAST(
    (
      `comprobante`.`fecha_generacion` + INTERVAL IF(
        ISNULL(`comprobante_item`.`dias`),
        0,
        `comprobante_item`.`dias`
      ) DAY
    ) AS DATE
  ) AS fecha_entrega_item_pi,
  comprobante_item.`cantidad`,
  moneda.`simbolo_internacional`,
  REPLACE(comprobante_item.`precio_unitario`,".",",") AS precio_unitario,
  factura.`fecha_contable` AS fecha_factura,
  @pedido_item_id := comprobante_item.id,
  @fact_item_id := fact_item.id,
  (SELECT 
    comprobante.`fecha_generacion`

  FROM
    comprobante_item 
    JOIN comprobante 
      ON comprobante.id = comprobante_item.`id_comprobante` 
  WHERE id_comprobante_item_origen = @fact_item_id 
    AND id_tipo_comprobante = 203 
    AND comprobante_item.`activo` = 1 
  LIMIT 1) 
  /*Chequea y te da si se hizo un remito o sea la fecha*/
  AS fecha_remitido,
  (SELECT 
    comprobante.`nro_comprobante` 
  FROM
    comprobante_item 
    JOIN comprobante 
      ON comprobante.id = comprobante_item.`id_comprobante` 
  WHERE id_comprobante_item_origen = @pedido_item_id 
    AND id_tipo_comprobante = 104 
    AND comprobante_item.`activo` = 1 
  LIMIT 1) 
  
  
  /*Chequea y te da si se hizo un remito o sea la fecha*/
  AS nro_orden_armado ,
  
    (SELECT 
    comprobante.`fecha_cierre` 
  FROM
    comprobante_item 
    JOIN comprobante 
      ON comprobante.id = comprobante_item.`id_comprobante` 
  WHERE id_comprobante_item_origen = @pedido_item_id 
    AND id_tipo_comprobante = 104 
    AND comprobante_item.`activo` = 1 
  LIMIT 1) 
  
  
  /*Chequea y te da si se hizo un remito o sea la fecha*/
  AS fecha_cierre_nro_orden_armado,
  
  comprobante.`inspeccion`
FROM
  comprobante_item 
  JOIN comprobante 
    ON comprobante.`id` = comprobante_item.`id_comprobante` 
  JOIN moneda 
    ON moneda.id = comprobante.`id_moneda` 
  JOIN persona 
    ON persona.id = comprobante.id_persona 
  LEFT JOIN persona_categoria 
    ON persona_categoria.id = persona.`id_persona_categoria` 
  LEFT JOIN comprobante_item fact_item 
    ON fact_item.`id_comprobante_item_origen` = comprobante_item.`id` 
  LEFT JOIN comprobante factura 
    ON factura.id = fact_item.`id_comprobante`  AND factura.`id_tipo_comprobante` IN( 801,817)

  AND IF(factura.`id_tipo_comprobante` = 801 ,factura.`cae` > 0,TRUE)
  JOIN producto 
    ON producto.id = comprobante_item.`id_producto` 
    LEFT JOIN estado_comprobante ON
    
    comprobante.`id_estado_comprobante` = estado_comprobante.`id`
    
WHERE comprobante.id_tipo_comprobante = 202 
  AND comprobante_item.`activo` = 1 
  AND YEAR(comprobante.`fecha_generacion`) = 2018 
  AND MONTH(comprobante.`fecha_generacion`) IN (10,11,12)


LIMIT 100000 