SELECT pedido.id,pedido.`nro_comprobante` nro_pedido,pedido.`razon_social`,remito.`nro_comprobante` AS numero_remito,item_pedido.`n_item` n_item_pedido
,producto.`codigo`,producto.`d_producto`,pedido.`fecha_generacion` fecha_carga_pedido,remito.`fecha_generacion` AS fecha_remito,pedido.`fecha_entrega` fecha_entrega_global_pedido,
item_pedido.`dias` dias_item_particular, pedido.plazo_entrega_dias dias_entrega_global,

  CAST((`pedido`.`fecha_generacion` + INTERVAL IF(ISNULL(`item_pedido`.`dias`),0,`item_pedido`.`dias`) DAY) AS DATE) AS fecha_entrega_pedido_item,
 factura.`fecha_generacion` AS fecha_factura,



item_pedido.`cantidad` cantidad_pedido, comprobante_item.`cantidad` cantidad_remito,
ROUND(item_pedido.`precio_unitario`) AS precio_item



 FROM comprobante_item

JOIN comprobante AS remito ON remito.id = comprobante_item.`id_comprobante`

JOIN comprobante_item AS item_factura ON item_factura.id = comprobante_item.`id_comprobante_item_origen`

JOIN comprobante_item AS item_pedido ON item_factura.`id_comprobante_item_origen` = item_pedido.`id`

JOIN comprobante AS pedido ON item_pedido.`id_comprobante` = pedido.`id`


JOIN comprobante AS factura ON factura.id = item_pedido.`id_comprobante`



JOIN producto ON comprobante_item.`id_producto` = producto.`id`
WHERE remito.id_tipo_comprobante = 203

AND pedido.`id_tipo_comprobante`=202

AND YEAR(remito.fecha_generacion)IN(2016,2017,2018,2019)


AND YEAR(CAST((`pedido`.`fecha_generacion` + INTERVAL IF(ISNULL(`item_pedido`.`dias`),0,`item_pedido`.`dias`) DAY) AS DATE))=2018


AND pedido.`id_estado_comprobante` <>2



ORDER BY pedido.`nro_comprobante` ASC



LIMIT 1000000