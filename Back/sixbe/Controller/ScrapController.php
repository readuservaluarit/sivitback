<?php

App::uses('ArticuloController', 'Controller');

class ScrapController extends ArticuloController {
    
    
    public $name = 'Scrap';
    public $model = 'Scrap';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $tipo_producto = EnumProductoTipo::SCRAP;
    
    
    
    /**
    * @secured(CONSULTA_SCRAP)
    */
    public function index() {
        
       
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = array();
        $this->RecuperoFiltros($conditions);
        
        
        array_push($conditions, array($this->model.'.id_producto_tipo =' => $this->tipo_producto)); 
        
            
        /*Recupero el filtro de deposito, si me lo pasa quiere decir que quiere ver el stock*/
        $id_deposito = $this->getFromRequestOrSession($this->model.'.id_deposito');
        if($id_deposito!= "" && $id_deposito>0){
        	
        	$array_consulta = array('Color','Talle','FamiliaProducto','Origen','Marca','Iva','Categoria', 'ListaPrecioProducto'=>array('conditions'=>array('ListaPrecioProducto.id_lista_precio'=>$this->id_lista_precio),"ListaPrecio","Moneda"),'Unidad','CuentaContableVenta','CuentaContableCompra','ProductoTipo'=>array('DestinoProducto'),'StockProducto'=>array('conditions'=>array('StockProducto.id_deposito'=>$id_deposito)));
        	
        }else{
        	
        	$array_consulta = array('Color','Talle','FamiliaProducto','Origen','Marca','Iva','Categoria', 'ListaPrecioProducto'=>array('conditions'=>array('ListaPrecioProducto.id_lista_precio'=>$this->id_lista_precio),"ListaPrecio","Moneda"),'Unidad','CuentaContableVenta','CuentaContableCompra','ProductoTipo'=>array('DestinoProducto'));
        	
        }
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
        		'contain' =>$array_consulta,
			// 'contain' =>array('ArticuloRelacion'/*=>array('Componente'=>array('Categoria')*/), 'FamiliaProducto','StockProducto','Origen'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.codigo desc'
        );
          //$this->Security->unlockedFields = array('costo','precio'); 
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
        
        ////////////////////
        //Filters
        ///////////////////
        
          //CUE
        $formBuilder->addFilterInput('codigo', 'C&oacute;digo', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $codigo));
        $formBuilder->addFilterInput('d_SCRAP', 'Descripci&oacute;n', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $descripcion));
        $formBuilder->setDataListado($this, 'Listado de Productos', 'Datos de los Productos', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'producto.id', "10%");
        $formBuilder->addHeader('C&oacute;digo', 'producto.codigo', "20%");
        $formBuilder->addHeader('Precio', 'producto.precio', "20%");
        $formBuilder->addHeader('Descripci&oacute;n', 'producto.d_SCRAP', "30%");
        
      

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'codigo');
        $formBuilder->addField($this->model, 'precio');
        $formBuilder->addField($this->model, 'd_SCRAP');
 
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
       //parseo el data para que los models queden dentro del objeto de respuesta
         foreach($data as &$valor){
               
             $this->cleanforOutput($valor,2);
         }
         
         
        $this->data = $data;
         
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }

    //fin vista json
        
    }
    
   
    
     /**
    * @secured(CONSULTA_SCRAP)
    */
  public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    } 
    
   private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
      
      $this->set('model',$model);
      $this->set('model_name',$this->model);
      Configure::write('debug',0);
      $this->render($vista);
   
    }  
    
    
  
  
  
  function addProductoEnTodasLasListasDePrecios($id_SCRAP){
      
      //utilizar la utilidad de la lista y basarse en el costo
      
  }
  
  
 
   
     /**
    * @secured(ADD_SCRAP)
    */
    public function add() {
        
        parent::add();
        return;
        
    }
    
    
     /**
    * @secured(ADD_SCRAP)
    */
    public function edit($id) {
        
        parent::edit($id);
        return;
        
    }
    
    /**
    * @secured(BAJA_SCRAP)
    */
    function delete($id) {
        parent::delete($id);
        return;
    }
    
    
    
    /**
    * @secured(CONSULTA_SCRAP)
    */
   public function existe_codigo(){
       
       
       parent::existe_codigo();
       return;
   }
   
   
   
    /**
    * @secured(CONSULTA_SCRAP)
    */
  public function getCosto($id_articulo){
    parent::getCosto($id_articulo);
  }
   
  
  /**
   * @secured(CONSULTA_SCRAP)
   */
  public function existe_codigo_barra(){
  	
  	
  	parent::existe_codigo_barra();
  	return;
  }
    
    
}
?>