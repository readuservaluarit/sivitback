<?php

    //PERCEPCIONES DE IIBB BS AS (SALEN DE LAS VENTAS)
    //La fecha con la que corta es la fecha_contable de la factura/nota/etc
    
    
    if($nombre_archivo  == "")
        $nombre_archivo = "ArbaPercepciones";
        
    header("Content-Disposition: attachment; filename=".$nombre_archivo.".txt");
    
    
  
    
    
    header("Pragma: no-cache"); 
    header("Expires: 0"); 
    
    $comprobante_obj  = new Comprobante();
    $persona_obj  = new Persona();
    
    $id_moneda_base  = $dato_empresa["DatoEmpresa"]["id_moneda"];
    
    foreach($datos as $comprobante){
        
      $nro_doc_final_01 =  str_pad($comprobante["Comprobante"]["Persona"]["cuit"], 11, '0', STR_PAD_LEFT);
      $nro_doc_final_01 = $persona_obj->getCuitConGuiones($nro_doc_final_01);
      
      $fecha = new DateTime($comprobante["Comprobante"]['fecha_contable']);
      $fecha_02 =$fecha->format('d/m/Y');
      
      
      
      $tipo_comprobante_03 = $comprobante["Comprobante"]["TipoComprobante"]["letra_arba"];
      
     
      $letra_tipo_comprobante_04 = trim($comprobante["Comprobante"]["TipoComprobante"]["letra"]);
      $punto_venta_05 = str_pad($comprobante["Comprobante"]["PuntoVenta"]["numero"], 4, '0', STR_PAD_LEFT);
      $nro_comprobante_06 = str_pad($comprobante["Comprobante"]["nro_comprobante"], 8, '0', STR_PAD_LEFT);
   
     
      $base_imponible = $comprobante["ComprobanteImpuesto"]["base_imponible"];
      
  
      $base_imponible = $comprobante_obj->ExpresarEnTipoMonedaMejorado($base_imponible,"valor_moneda2", $comprobante["Comprobante"]);
      
      
     if($comprobante["Comprobante"]["TipoComprobante"]["signo_comercial"] < 0){
      	$cantidad_digitos_enteros = 8;
      	$primer_caracter = '-';
     }else{
     	$cantidad_digitos_enteros = 9;
     	$primer_caracter = '';
     }
     
     
     
      $base_imponible = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($base_imponible,$cantidad_digitos_enteros,2,"0");
      
      
      $parte_entera = substr($base_imponible,0,$cantidad_digitos_enteros);
      $parte_decimal = substr($base_imponible,$cantidad_digitos_enteros,2);
      
      
      $base_imponible_07 = $primer_caracter.$parte_entera.','.$parte_decimal;//las notas de credito son negativas entonces llevan el primer caracter como un menos (-)
      
      
      
      
      $importe_impuesto = $comprobante["ComprobanteImpuesto"]["importe_impuesto"];
       
      $importe_impuesto = $comprobante_obj->ExpresarEnTipoMonedaMejorado($importe_impuesto,"valor_moneda2", $comprobante["Comprobante"]);
      
      
       
      if($comprobante["Comprobante"]["TipoComprobante"]["signo_comercial"] < 0){
      	$cantidad_digitos_enteros = 7;
      	$primer_caracter = '-';
      }else{
     	$cantidad_digitos_enteros = 8;
     	$primer_caracter = '';
       } 
       
       $importe_impuesto = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($importe_impuesto,$cantidad_digitos_enteros,2,"0");
       
       
       
       $parte_entera = substr($importe_impuesto,0,$cantidad_digitos_enteros);
       $parte_decimal = substr($importe_impuesto,$cantidad_digitos_enteros,2);
       $importe_impuesto_08 = $primer_caracter.$parte_entera.','.$parte_decimal;

       $tipo_operacion_a_09 = 'A';
     
       
       
     
       
       
       
       $linea_texto = $nro_doc_final_01.$fecha_02.$tipo_comprobante_03.$letra_tipo_comprobante_04.$punto_venta_05.$nro_comprobante_06.$base_imponible_07.$importe_impuesto_08.$tipo_operacion_a_09; 
       
       
       
       echo $linea_texto.  "\r\n";       
    }
        


?>