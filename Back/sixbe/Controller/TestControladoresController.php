<?php



    class TestControladoresController extends AppController {
    
        public $helpers = array ('Session', 'Paginator', 'Js');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

        public function index() {
            
         $controllers = App::objects('controller');
         
         
         foreach($controllers as $controller){
           
           $num_chars= strlen($controller);  
           $controller_name = substr($controller,0,$num_chars-10);
           $controller_excluir = array("TestControladores","Auditorias","PersonasCategorias","App");
           
           echo $controller_name;
           echo "<br>";
           
           if(!in_array($controller_name,$controller_excluir)){
               
               
                    
                   //var_dump($annotations);
                try{
                $resultado = $this->requestAction(
                        array('controller' => $controller_name.'.json' /*, 'action' => '' */),
                        array('data' => null)
                );
                }catch(Exception $e){
                    
                    echo $e->getMessage();
                }
            
            }
         }   
            
        }
        
       
       
       public function indexModels() {
       	
       	$models= App::objects('model');
       	
       	echo json_encode($models);
       	die();
       }
       
       
       public function indexControladores() {
       	
       	
       	$controllers = App::objects('controller');
       	
       	
       	$models = array();
       	$model_controller = array();
       	
       	$this->loadModel("Entidad");
       	
       	
       	
       	foreach($controllers as $controller){
       		
       		
       		if($controller !="TestControladoresController"){
       		
       			
       			try{
       		App::uses($controller, 'Controller');
       		
       		$reflectionClass = new ReflectionClass($controller);
	       	$obj = $reflectionClass->newInstanceArgs();
	       	
	       	
	       	if( isset($obj->model) ){
	       	
	       		
	       		App::uses($obj->model, 'Model');
	       		
	       		$reflectionClassModel = new ReflectionClass($obj->model);
	       		$objModel = $reflectionClassModel->newInstanceArgs();
	       		
	       		
	       		
	      
	       		//echo $obj->model.'  '.$controller.'</br>';
	       		array_push($models, $obj->model);
	       		
	       		$mc["Entidad"]["d_entidad"] = $objModel->useTable;
	       		$mc["Entidad"]["model"] = $obj->model;
	       		$mc["Entidad"]["controller"] = $controller;
	       		
	       		
	       		
	       		
	       
	       		
	       		
	       		$entidad = $this->Entidad->find('first', array(
	       				'conditions' => array(
	       						'Entidad.d_entidad' =>$mc["Entidad"]["d_entidad"],
	       						'Entidad.controller' =>$mc["Entidad"]["controller"]
	       				) ,
	       				'contain' => false
	       		));
	       		
	       		
	       		
	       		if(count($mc["Entidad"]["controller"])>0){
	       			
			       		if(!$entidad){//OJO no esta en la tabla
			       			
			       			array_push($model_controller, $mc);
			       		
			       			
			       			
			       		}else{//si esta actualizo el controller
			       			
			       			
			       			
			       			$this->Entidad->query("UPDATE entidad set controller='".$mc["Entidad"]["controller"]."', model='".$mc["Entidad"]["model"]."' where d_entidad='".$mc["Entidad"]["d_entidad"]."';");
			       			/*$this->Entidad->update(
			       					array('Entidad.controller' => "'".$mc["Entidad"]["controller"]."'",'Entidad.model' => "'".$mc["Entidad"]["model"]."'"),
			       					array('Entidad.d_entidad' =>   $mc["Entidad"]["d_entidad"]) );*/
			       		}
	       		
	       		}
	       		
	       	}
	       	
       	}catch (Exception $e){
       		
       		echo $e->getMessage();
       	}
       	
       		
       	
       	}
       	
       	}
       	
       	
       	if(isset($model_controller) && count($model_controller)>0)
       		$this->Entidad->saveAll($model_controller);
       	
       	echo json_encode($model_controller);
   
       	die();
       }
       
     
        
        
  
    public function beforeFilter(){
        
     App::uses('ConnectionManager', 'Model');
        
           
        
          
      
        
        //$this->Auth->allow('index','actualizaBDConControllers');
        
        parent::beforeFilter();
        
 
        
        
        
    }
    
     
    public function actualizaBDConControllers(){
        
       $this->loadModel("Controlador");
       
       $this->Controlador->query('TRUNCATE controlador;');
       
        $controllers = App::objects('controller');
         
        $controller_name_array = array();
         
         foreach($controllers as $controller){
           
           $num_chars= strlen($controller);  
           $controller_name = substr($controller,0,$num_chars-10);
           $array["Controlador"]["d_controlador"] = $controller_name ;
           array_push($controller_name_array,$array);
           
         }
        
        $this->Controlador->SaveAll($controller_name_array);
        die();
    }
    
    
    
    public function creaCacheLogin($id_usuario=1) {
    	
    
    	ob_start();
    	
    	$this->loadModel("Entidad");
    	
    	
    	$entidades = $this->Entidad->query("SELECT DISTINCT REPLACE(entidad.`controller`,'Controller','') AS controller,entidad.id,entidad.`d_entidad`,entidad.`model`,entidad.`estatica_sistema`,entidad.`tiene_cache_cliente` FROM entidad");
    	
    	
    	
    	try{
    		
    		
    	$_SESSION["creando_cache"] = 1;
    	
    		
    	$array_model = array();
    	foreach($entidades as &$entidad){
    		
    		

    			
    		$controller_name = $entidad[0]["controller"];
    		$model_name = $entidad["entidad"]["model"];
    		
    		
    		
    		
    		
		
    		$controller_excluir = array("TestControladores","Auditorias","App","Funciones","Roles","QaConformidades","Reclamos","MisDatos","ConfiguracionAplicaciones","MediaFiles","NotasUsuario","ComprobanteItemComprobantes","ComprobanteItems","Comprobantes","CuentasCorriente","EstadosPersona","Personas","PersonasCategorias","OrdenesProduccion","TurCotizaciones","TurCotizacionItems","Reparaciones","CotizacionesCompra","TurCotizacionConceptos");
    		
    		$controllers = array("OrdenesTrabajoItems");
    	 
    		if(!in_array($controller_name,$controller_excluir)){
    			
    			
    		
    		
    	/* va a haber tantas cache como rol alla. El archivo */
    			
    			try{
    				
    				$nombres_vistas = $this->getNombresFromView($controller_name,$model_name);
    				
    				/*
    				if(count($nombres_vistas) == 0){
    					
    					echo $controller_name;
    					die();
    					
    				}
    				*/
    				
    				
    				foreach($nombres_vistas as $view){
    			
    					
    					//$this->log($controller_name);
					    		$resultado = $this->requestAction(
					    				array('controller' => $controller_name.'/getModel/'.$view.'.json' /*, 'action' => '' */),
					    				array('data' => null)
					    				);
					    		
					    		
					    		
					    		$resultado2 = json_decode($resultado);
					    		
					    	
					
					    		
					    		$entidad2 =  array();
					    		
					    		
					    		if($resultado2->status == EnumError::SUCCESS){
					    			
					    			$entidad2["entidad"]["controllerName"] = $controller_name;
					    			$entidad2["entidad"]["getmodel"] = $resultado2->content;
					    			$entidad2["entidad"]["modelName"] = $model_name;
					    			$entidad2["entidad"]["viewName"] = $view;
					    		
					    			
					    			array_push($array_model, $entidad2);
					    			
					    			
					    			
					    		}else{
					    			
					    			if($resultado != null){
							    			//echo var_dump($controller_name);
							    			//echo var_dump($resultados2);
					    			}else{
					    				//var_dump($controller_name);
					    				//die();
					    			}
					    		
					    		}
		    		
    				}
		    		

    			}catch(Exception $e){
    				
    				$e;
    			}
    			
    			
    			
		   
		    		if($entidad["entidad"]["estatica_sistema"] == 1){
		    			
			    	
		    			
		    			/*
			    			try{
			    				
			    			
			    				
			    				$resultado = $this->requestAction(array('controller' => $controller_name, 'action'=>'index.json'));
			    				
			 
			    				$resultado_index = json_decode($resultado);
			    				
			    				
			    				
			    				//$entidad2["entidad"]["getmodelIndex"] = json_encode($resultado_index->content);
			    				
			    				
			    				
			    				$entidad2["entidad"]["index"] = json_encode($resultado_index->content);
			    				
			    			}catch(Exception $e){
			    				
			    		
			    			}
			    			
			    			*/
		    			
		    		}	
		    		
    
		    		
    		}
    		
    	}//fin for
    	
    	
    	}catch(Exception $e){
    		
    		$e;
    		
    	}
    	
   
    	ob_clean();
    	
    	
    	
    	try{
    		//Cache::write(EnumCacheName::getModel,  serialize($array_model));
    		
    		file_put_contents(EnumCacheName::getModel,serialize($array_model));
    		
    	}catch(Exception $e){
    		
    		/*
    		Cache::config(EnumCacheName::getModel, array(
    				'engine' => "File",
    				'path' => CACHE . 'index' . DS
    		));
    		Cache::write(EnumCacheName::getModel,  serialize($array_model));
    		*/
    		
    		
    	}
    	
    	
    	
    	
    	
    	
    	echo json_encode($array_model);
    	die();
    	
   
    	
    }
    
    
    
    
    public function creaCacheIndex($id_usuario=1) {
    	
    	
    	ob_start();
    	
    	$this->loadModel("Entidad");
    	$this->loadModel(EnumModel::DatoEmpresa);
    	
    	
    	$entidades = $this->Entidad->query("SELECT DISTINCT REPLACE(entidad.`controller`,'Controller','') AS controller,entidad.id,entidad.`d_entidad`,entidad.`model`,entidad.`estatica_sistema`,entidad.`tiene_cache_cliente` FROM entidad where estatica_sistema=1");
    	
    	
    	
    	$datoEmpressa = $this->DatoEmpresa->find('first', array(
    			'conditions' => array(
    					'DatoEmpresa.id' =>1
    					
    			) ,
    			'contain' => false
    	));
    	
    	
    	$url_web_service = $datoEmpressa["DatoEmpresa"]["app_path"];
    	
    	try{
    		
    		
    		$array_index = array();
    		
    		
    		foreach($entidades as &$entidad){
    			
    			
    			
    			
    			$controller_name = $entidad[0]["controller"];
    			$model_name = $entidad["entidad"]["model"];
    			
    			
    			
    			
    			
    			
    			$controller_excluir = array("TestControladores","Auditorias","App","Funciones","Roles","QaConformidades","Reclamos","MisDatos","ConfiguracionAplicaciones","MediaFiles","NotasUsuario","ComprobanteItemComprobantes","ComprobanteItems","Comprobantes","CuentasCorriente","EstadosPersona","Personas","PersonasCategorias","OrdenesProduccion","TurCotizaciones","TurCotizacionItems","Reparaciones","CotizacionesCompra","TurCotizacionConceptos");
    			
    			$controllers = array("OrdenesTrabajoItems");
    			
    			if(!in_array($controller_name,$controller_excluir)){
    				
    				
    				
    				
    				/* va a haber tantas cache como rol alla. El archivo */
    				
    				try{
    					
    				
    					
    					
    					
    					
    						if($entidad["entidad"]["estatica_sistema"] == 1){
    							
    							
    							$resultado = $this->requestAction(array('controller' => $controller_name, 'action'=>'getCache.json'));
    							
    							
    					
    							
    					/*
    							$entidad2 =  array();
    						
    						
	    						if($resultado2->status == EnumError::SUCCESS){
	    							
	    							$entidad2["entidad"]["controllerName"] = $controller_name;
	    							$entidad2["entidad"]["index"] = $curl_json->content;
	    							$entidad2["entidad"]["modelName"] = $model_name;
	    							$entidad2["entidad"]["viewName"] = $view;
	    							
	    							
	    							array_push($array_index, $entidad2);
	    							
	    							
	    							
	    						}else{
	    							
	    							if($resultado != null){
	    								//echo var_dump($controller_name);
	    								//echo var_dump($resultados2);
	    							}else{
	    								//var_dump($controller_name);
	    								//die();
	    							}
	    							
	    						}
    						
    						
    						
    						*/
    						
    						}
    					
    					
    				}catch(Exception $e){
    					
    					$e;
    				}
    				
    				
    			
    				
    				
    				
    			}
    			
    		}//fin for
    		
    		
    		
    		
    	}catch(Exception $e){
    		
    		$e;
    		
    	}
    	
    	
    	ob_clean();
    	
    	
    	
    	try{
    		//Cache::write(EnumCacheName::getModel,  serialize($array_model));
    		
    		//file_put_contents(APP."webroot/media_file/cache_get_index.txt",serialize($array_index));
    		
    	}catch(Exception $e){
    		
    		/*
    		 Cache::config(EnumCacheName::getModel, array(
    		 'engine' => "File",
    		 'path' => CACHE . 'index' . DS
    		 ));
    		 Cache::write(EnumCacheName::getModel,  serialize($array_model));
    		 */
    		
    		
    	}
    	
    	
    	
    	
    	
    	
    	echo json_encode($array_index);
    	die();
    	
    	
    	
    }
    
    
    public function creaStaticCacheIndex($id_usuario=1) {
    	
    	
    	ob_start();
    	
    	$this->loadModel("Entidad");
    	$this->loadModel(EnumModel::DatoEmpresa);
    	
    	
    	
    	$entidades = $this->Entidad->query("SELECT DISTINCT REPLACE(entidad.`controller`,'Controller','') AS controller,entidad.id,entidad.`d_entidad`,entidad.`model`,entidad.`estatica_sistema`,entidad.`tiene_cache_cliente` FROM entidad WHERE (tiene_cache_cliente=1)
AND (estatica_sistema IS NULL OR estatica_sistema=0)");
    	
    	
    	
    	$datoEmpresa = $this->DatoEmpresa->find('first', array(
    			'conditions' => array(
    					'DatoEmpresa.id' =>1
    					
    			) ,
    			'contain' => false
    	));
    	
    	
    	$url_web_service = $datoEmpressa["DatoEmpresa"]["app_path"];
    	
    	try{
    		
    		
    		$array_index = array();
    		
    		
    		foreach($entidades as &$entidad){
    			
    			
    			
    			
    			$controller_name = $entidad[0]["controller"];
    			$model_name = $entidad["entidad"]["model"];
    			
    			
    			
    			
    			
    			
    			$controller_excluir = array("TestControladores","Auditorias","App","Funciones","Roles","QaConformidades","Reclamos","MisDatos","ConfiguracionAplicaciones","MediaFiles","NotasUsuario","ComprobanteItemComprobantes","ComprobanteItems","Comprobantes","CuentasCorriente","EstadosPersona","Personas","PersonasCategorias","OrdenesProduccion","TurCotizaciones","TurCotizacionItems","Reparaciones","CotizacionesCompra","TurCotizacionConceptos");
    			
    			$controllers = array("OrdenesTrabajoItems");
    			
    			if(!in_array($controller_name,$controller_excluir)){
    				
    				
    				
    				
    				/* va a haber tantas cache como rol alla. El archivo */
    				
    				try{
    					
    					
    					
    					
    					
    					
    					if($entidad["entidad"]["estatica_sistema"] == 1){
    						
    						
    						$resultado = $this->requestAction(array('controller' => $controller_name, 'action'=>'getCache.json'));
    						
    						
    						
    						
    						/*
    						 $entidad2 =  array();
    						 
    						 
    						 if($resultado2->status == EnumError::SUCCESS){
    						 
    						 $entidad2["entidad"]["controllerName"] = $controller_name;
    						 $entidad2["entidad"]["index"] = $curl_json->content;
    						 $entidad2["entidad"]["modelName"] = $model_name;
    						 $entidad2["entidad"]["viewName"] = $view;
    						 
    						 
    						 array_push($array_index, $entidad2);
    						 
    						 
    						 
    						 }else{
    						 
    						 if($resultado != null){
    						 //echo var_dump($controller_name);
    						 //echo var_dump($resultados2);
    						 }else{
    						 //var_dump($controller_name);
    						 //die();
    						 }
    						 
    						 }
    						 
    						 
    						 
    						 */
    						
    					}
    					
    					
    				}catch(Exception $e){
    					
    					$e;
    				}
    				
    				
    				
    				
    				
    				
    			}
    			
    		}//fin for
    		
    		
    		
    		
    	}catch(Exception $e){
    		
    		$e;
    		
    	}
    	
    	
    	ob_clean();
    	
    	
    	
    	try{
    		//Cache::write(EnumCacheName::getModel,  serialize($array_model));
    		
    		//file_put_contents(APP."webroot/media_file/cache_get_index.txt",serialize($array_index));
    		
    	}catch(Exception $e){
    		
    		/*
    		 Cache::config(EnumCacheName::getModel, array(
    		 'engine' => "File",
    		 'path' => CACHE . 'index' . DS
    		 ));
    		 Cache::write(EnumCacheName::getModel,  serialize($array_model));
    		 */
    		
    		
    	}
    	
    	
    	
    	
    	
    	
    	echo json_encode($array_index);
    	die();
    	
    	
    	
    }
    
    
    
    public function getNombresFromView($controllerName,$model_name){
    	
    	
    	$model_name = Inflector::underscore($model_name);
    	//debo camilar el Nombre del model y excluir esa view si es que existe
    	
    	$dir_name = APP.'View/'.$controllerName.'/json/';
    	$directorio_view = scandir($dir_name);
    	//busco los nombres de los archivos dentro de la carpeta View/<Controller>/json/
    	
    	$array_view = array();
    	//array_push($array_view, "default");
    	
    	foreach ($directorio_view as $file){
    	
    		if(!in_array($file,array(".","..")) && basename($file,".ctp")!= $model_name){
    			array_push($array_view, basename($file,".ctp"));
    		}
    	}
    	


    	if(count($array_view) == 0)
    		array_push($array_view, "default");
    	return $array_view;
    	
    }
    

    }
    
   
?>