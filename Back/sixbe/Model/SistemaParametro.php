<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class SistemaParametro extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'sistema_parametro';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
       
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
        'Sistema' => array(
            'className' => 'Sistema',
            'foreignKey' => 'id_sistema',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
			
        'CuentaContableCuentaCorriente' => array(
            'className' => 'CuentaContable',
            'foreignKey' => 'id_cuenta_contable_cuenta_corriente',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        
        'CuentaContableCuentaContado' => array(
            'className' => 'CuentaContable',
            'foreignKey' => 'id_cuenta_contable_cuenta_contado',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'CuentaContableIva' => array(
            'className' => 'CuentaContable',
            'foreignKey' => 'id_cuenta_contable_iva',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CuentaContablePrincipal' => array(
            'className' => 'CuentaContable',
            'foreignKey' => 'id_cuenta_contable',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'CuentaContableDescuentoCondicionado' => array(
            'className' => 'CuentaContable',
            'foreignKey' => 'id_cuenta_contable_descuento_condicionado',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'TipoComercializacion' => array(
            'className' => 'TipoComercializacion',
            'foreignKey' => 'id_tipo_comercializacion',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
		'CuentaContableCompensacion' => array(
					'className' => 'CuentaContable',
					'foreignKey' => 'id_cuenta_contable_compensacion',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			)
		
   );
   
   
   public function getCuentaCorriente($id_sistema){
       
     $sistema_parametro = $this->find('first', array(
                                                'conditions' => array('SistemaParametro.id_sistema' => $id_sistema),
                                                'contain' =>false
                                                ));
                                                
     return $sistema_parametro["SistemaParametro"]["id_cuenta_contable_cuenta_corriente"];                                              
   }     

}
