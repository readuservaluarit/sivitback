<?php   



App::import('Vendor','mytcpdf/mytcpdf'); 
App::import('Vendor','tcpdf/tcpdf_barcodes_1d'); 
App::import('Model','Comprobante');

$comprobante = new Comprobante();

 $datos_empresa = $this->Session->read('Empresa'); //Leo los datos de datoEmpresa en session cargados en Usuarios
  


 
$pdf = new MyTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


$html = $comprobante->getHeaderPDF($datos_pdf,$datos_empresa,'REMITO INTERNO');
$pdf->set_datos_header($html);



 


$observaciones = $datos_pdf["Comprobante"]["observacion"];


//$pdf->set_observaciones($observaciones);

$footer='
  

 <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:9px;font-family:\'Courier New\', Courier, monospace;">
 
   <tr >
   
    <th width="100%"  colspan="2" scope="col" style="font-size:12px;">Destino: '.$datos_pdf["Comprobante"]["lugar_entrega"].'  </th>
  </tr>
  
  
  
 
 
 
  
  <tr >
   
    <th width="100%"  colspan="2" scope="col">Observaciones: '.nl2br($datos_pdf["Comprobante"]["observacion"]).'  </th>
  </tr>
  <tr >
  
 
  
 

  
  <tr>
   <td width="50%" scope="col">Importe declarado: '.$datos_pdf["Comprobante"]["Moneda"]["d_moneda"].' '.$datos_pdf["Comprobante"]["importe_declarado"].' </td>
    <td width="50%" scope="col">Cantidad de Bultos:  '.$datos_pdf["Comprobante"]["cantidad_bultos"].'</td>
  </tr>
  
<tr>
   
    <td style="font-size:8px;" width="50%" scope="col"></td>
    <td style="font-size:8px;" width="50%" scope="col"></td>
   
  </tr >
  <tr>
   
    <td style="font-size:8px;" width="50%" scope="col"></td>
    <td style="font-size:8px;" width="50%" scope="col"></td>
   
  </tr >

  
 
   
   
  
   
 

</table>';
   
   $html .= "<HR>";
   
   
   if( $datos_pdf["Remito"]["cae"] != '' ){
   
      $html.=$pdf->SetBarcode($codigo_barras,I25);
     
   
   }
   
   
   $html .= '
   
   <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody>
</table>
   
  ';
  
  $pdf->set_datos_footer($footer);
  
  
ob_clean();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Valuarit');
$pdf->SetTitle('Factura');
$pdf->SetSubject('Factura');
//$pdf->SetKeywords('');



// set default header data
$logo = 'img/header_factura.jpg';
$title = "";
$subtitle = ""; 
//$pdf->SetHeaderData($logo, 190, $title, $subtitle, array(0,0,0), array(0,104,128));
//$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(4, 50, 4);
//$pdf->SetMargins(PDF_MARGIN_LEFT, 50, 20);
$pdf->SetHeaderMargin(10);
$pdf->SetFooterMargin(50);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
/*$lg = Array();
$lg['a_meta_charset'] = 'ISO-8859-1';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';
*/
$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set default font subsetting mode
//$pdf->setFontSubsetting(true);

$pdf->SetFont('helvetica', '', 12, '', true);

$pdf->setCellHeightRatio(2.0);
// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));


$tbody = "";

$renglones = $datos_pdf["ComprobanteItem"];
$comprobante_item = new ComprobanteItem();

$data = array();

foreach($renglones as $renglon){

  
   
   $tbody .= "<tr style=\"font-size:9px;font-family:'Courier New', Courier, monospace\">";
   $tbody .= "<td>" .$renglon['n_item'] . "</td>";
   $tbody .= "<td>" .$renglon['cantidad']. "</td>";
   $tbody .= "<td>" .$comprobante_item->getCodigoFromProducto($renglon) . "</td>";
   $tbody .= "<td>" .$renglon['Producto']['d_producto'] . "</td>";       
   $tbody .= "</tr>";
}
 
//agrego las observaciones

   

$html='';


if($datos_pdf["Persona"]["id_tipo_persona"] == EnumTipoPersona::Cliente)//sino es cliente es proveedor en Remito Interno
	$d_persona = "Cliente";
else
	$d_persona = "Proveedor";

$html .='
 <table width="100%"   style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
  <tr >
		<td colspan="4"></td>
		</tr>
  <tr>  
  
  
 
 </table>
 

<table width="100%" border="1" style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace">
 				<tr  style="background-color:rgb(224, 225, 229);">
                    <td colspan="4"><div align="center"><strong>Cliente</strong></div></td>
                  </tr>
                  
                  
                  <tr>                                     
                    <td width="10%"><strong>Codigo:</strong> '.$datos_pdf["Persona"]["codigo"].'</td>
                    <td width="60%"><strong>Raz&oacute;n Social:</strong> '.$datos_pdf["Persona"]["razon_social"].'</td>
                    <td width="15%"><strong>'.$datos_pdf["Persona"]["TipoDocumento"]["d_tipo_documento"].':</strong> '.$datos_pdf["Persona"]["cuit"].' </td>
                    <td width="15%"><strong>'.$datos_pdf["Persona"]["TipoDocumento"]["d_tipo_documento"].':</strong> '.$datos_pdf["Persona"]["cuit"].' </td>
                 </tr>
                              <tr>
                                <td colspan="3" align="left" ><strong>Direccion:</strong> '.$datos_pdf["Persona"]["calle"].' - '.$datos_pdf["Persona"]["numero_calle"].' - '.$datos_pdf["Persona"]["localidad"].' - '.$datos_pdf["Persona"]["ciudad"].' - '.$datos_pdf["Persona"]["Provincia"]["d_provincia"].' - '.$datos_pdf["Persona"]["Pais"]["d_pais"].'</td>
                                <td rowspan="1"><strong>Tel.:</strong> '.$datos_pdf["Persona"]["tel"].'</td>
                              </tr>
                              
                           
                              
                              
                    
                    
	  <tr style="background-color:rgb(224, 225, 229); text-align:center">
		<td colspan="4"><strong>Referencias</strong></td>
	  </tr>
	  <tr>
		<td width="50%" style="text-align:left;height:20px"><strong>Dep&oacute;sito Origen: </strong>'.$datos_pdf["DepositoOrigen"]["d_deposito"].'</td>
		<td width="50%" style="text-align:left;height:20px"><strong>Dep&oacute;sito Destino:</strong>'.$datos_pdf["DepositoDestino"]["d_deposito"].' </td>
	  </tr>
</table>
                

<table width="100%" border="0px" cellspacing="0.5" cellpadding="0.5" style="width:100%;font-size:6px;font-family:\'Courier New\', Courier, monospace">
     <tbody>
        <tr >
			<td colspan="2"> </td>
			 <td colspan="2">                           </td>
			 <td colspan="2">                           </td>
        </tr>
        <!-- <tr >
			<td  colspan="6" ><HR></td>
         </tr> -->
     </tbody>
</table>


<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">

    <tr style="background-color:rgb(224, 225, 229);text-align:center;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:15%;"><strong>Item</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:15%;"><strong>Cant</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong>C&oacute;digo</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:50%;"><strong>Descripci&oacute;n</strong></th>
    </tr>

  <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
    '.$tbody.'
  </tbody>  
</table>';
  
  
   
   
   
$pdf->writeHTML($html, true, false, true, false, true);


// QRCODE,L : QR-CODE Low error correction
// set style for barcode
$style = array(
    'border' => 2,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

//$pdf->Text(20, 25, 'QRCODE L');

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->SetBarcode(date("Y-m-d H:i:s", time()));
ob_clean();
$pdf->Output(str_replace(".","",$datos_pdf["TipoComprobante"]["abreviado_tipo_comprobante"].'_'.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'-'.$datos_pdf["Persona"]["razon_social"]).'.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
