SELECT 
  comprobante.`nro_comprobante`,
  comprobante.`fecha_generacion`,
  comprobante_item.`producto_codigo`,
  comprobante.`razon_social`,
  comprobante_item.`n_item`,
  REPLACE(comprobante.`total_comprobante`,".",",") total_comprobante,
   REPLACE(comprobante_item.`precio_unitario`,".",",")precio_unitario,
  comprobante_item.`cantidad`,
  comprobante.validez_dias AS vigencia_dias,
  comprobante.`fecha_entrega` fecha_entrega_global,
  comprobante_item.dias dias_entrega_prometida,
  CAST(
    (
      `comprobante`.`fecha_generacion` + INTERVAL IF(
        ISNULL(`comprobante_item`.`dias`),
        0,
        `comprobante_item`.`dias`
      ) DAY
    ) AS DATE
  ) fecha_entraga_item 
FROM
  comprobante_item 
  JOIN comprobante 
    ON comprobante.id = comprobante_item.`id_comprobante` 
WHERE comprobante.`id_tipo_comprobante` = 201 
  AND comprobante.fecha_generacion >= '2019-04-01' 
LIMIT 10000000 