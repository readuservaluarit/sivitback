<?php
        // CONFIG  CON MODE DISPLAYED CELLS
        include(APP.'View'.DS.'Movimientos'.DS.'json'.DS.'movimiento.ctp');
        


        $propiedades = array("show_grid"=>"1","header_display" => "Nro","width"=> "10","order" =>"1","text_colour"=>"#000000" );
        $movimiento_obj->setFieldView("id",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Cantidad","width"=> "10","order" =>"2","text_colour"=>"#BB0000" );
        $movimiento_obj->setFieldView("cantidad",$propiedades,$model);
        
        
        $propiedades = array("show_grid"=>"1","header_display" => "Movimiento","width"=> "10","order" =>"3","text_colour"=>"#0000BB" );
        $movimiento_obj->setFieldView("id_movimiento_tipo",$propiedades,$model);
        
        
        $propiedades = array("show_grid"=>"1","header_display" => "Tipo Mov.","width"=> "40","order" =>"4","text_colour"=>"#0000BB" );
        $movimiento_obj->setFieldView("tipo_movimiento",$propiedades,$model);
        
        
         $propiedades = array("show_grid"=>"1","header_display" => "C&oacute;digo Artic","width"=> "15","order" =>"5","text_colour"=>"#0000BB" );
        $movimiento_obj->setFieldView("producto_codigo",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Fecha","width"=> "20","order" =>"6","text_colour"=>"#000000" );
        $movimiento_obj->setFieldView("fecha",$propiedades,$model);
        
        
        $propiedades = array("show_grid"=>"1","header_display" => "Tipo Comp.","width"=> "10","order" =>"7","text_colour"=>"#000000" );
        $movimiento_obj->setFieldView("codigo_tipo_comprobante",$propiedades,$model);
        
        
        $propiedades = array("show_grid"=>"1","header_display" => "Nro Comprobante.","width"=> "20","order" =>"8","text_colour"=>"#000000" );
        $movimiento_obj->setFieldView("pto_nro_comprobante",$propiedades,$model);
        
        
        $propiedades = array("show_grid"=>"1","header_display" => "Lote","width"=> "20","order" =>"9","text_colour"=>"#000000" );
        $movimiento_obj->setFieldView("lote_codigo",$propiedades,$model);
        
      
        $propiedades = array("show_grid"=>"1","header_display" => "Orden Prod.","width"=> "10","order" =>"10","text_colour"=>"#000000" );
        $movimiento_obj->setFieldView("id_orden_produccion",$propiedades,$model);
        
        
        $propiedades = array("show_grid"=>"1","header_display" => "Mov Padre.","width"=> "11","order" =>"11","text_colour"=>"#000000" );
        $movimiento_obj->setFieldView("id_movimiento_padre",$propiedades,$model);
        
        
        $propiedades = array("show_grid"=>"1","header_display" => "Desc.","width"=> "10","order" =>"12","text_colour"=>"#990000" );
        $movimiento_obj->setFieldView("observacion",$propiedades,$model);
        
        
        $propiedades = array("show_grid"=>"1","header_display" => "Dep. Origen","width"=> "10","order" =>"13","text_colour"=>"#990000" );
        $movimiento_obj->setFieldView("d_deposito_origen",$propiedades,$model);
        
        
        $propiedades = array("show_grid"=>"1","header_display" => "Dep. Destino","width"=> "10","order" =>"14","text_colour"=>"#990000" );
        $movimiento_obj->setFieldView("d_deposito_destino",$propiedades,$model);
 
    
    
        $propiedades = array("show_grid"=>"1","header_display" => "Usuario","width"=> "10","order" =>"15","text_colour"=>"#990000" );
        $movimiento_obj->setFieldView("d_usuario_nombre",$propiedades,$model);
 
 
 
 
 
        
         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Movimiento",
                    "content" => $model
                );
        
          echo json_encode($output);
?>