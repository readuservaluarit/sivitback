<?php
        
         include(APP.'View'.DS.'Comprobantes'.DS.'json'.DS.'default.ctp');
        // CONFIG  CON MODE DISPLAYED CELLS
        
      
        
        $model["codigo_tipo_comprobante"]["show_grid"] = 1;
        $model["codigo_tipo_comprobante"]["header_display"] = "Tipo";
        $model["codigo_tipo_comprobante"]["width"] = "7"; //REstriccion mas de esto no se puede achicar
        $model["codigo_tipo_comprobante"]["order"] = "1";
        $model["codigo_tipo_comprobante"]["text_colour"] = "#000000";
        
        $model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
        $model["pto_nro_comprobante"]["width"] = "15";
        //$model["pto_nro_comprobante"]["minimun_width"] = "90"; //REstriccion mas de esto no se puede achicar
        $model["pto_nro_comprobante"]["order"] = "2";
        $model["pto_nro_comprobante"]["text_colour"] = "#000000";
        
    
        
        $model["fecha_contable"]["show_grid"] = 1;
        $model["fecha_contable"]["header_display"] = "Fecha Contable";
        $model["fecha_contable"]["width"] = "10";
        $model["fecha_contable"]["order"] = "4";
        $model["fecha_contable"]["text_colour"] = "#0000FF";
        
        
        
        $model["codigo_persona"]["show_grid"] = 1;
        $model["codigo_persona"]["header_display"] = "Cod. Proveedor";
        $model["codigo_persona"]["width"] = "7";
        $model["codigo_persona"]["order"] = "5";
        $model["codigo_persona"]["text_colour"] = "#000000";
        
        $model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["header_display"] = "Raz&oacute;n social";
        $model["razon_social"]["width"] = "15";
        $model["razon_social"]["order"] = "6";
        $model["razon_social"]["text_colour"] = "#000000";
        

     
		
	
        $model["d_moneda_simbolo"]["show_grid"] = 1;
        $model["d_moneda_simbolo"]["header_display"] = "Moneda";
        $model["d_moneda_simbolo"]["width"] = "5";
        $model["d_moneda_simbolo"]["order"] = "7";
        $model["d_moneda_simbolo"]["text_colour"] = "#F0000F";
        
        $model["total_comprobante"]["show_grid"] = 1;
        $model["total_comprobante"]["header_display"] = "Monto";
        $model["total_comprobante"]["width"] = "10";
        $model["total_comprobante"]["order"] = "8";
        $model["total_comprobante"]["text_colour"] = "#F0000F";
        
        $model["d_estado_comprobante"]["show_grid"] = 1;
        $model["d_estado_comprobante"]["header_display"] = "Estado";
        $model["d_estado_comprobante"]["width"] = "8";
        $model["d_estado_comprobante"]["order"] = "9";
        $model["d_estado_comprobante"]["text_colour"] = "#004400";
        
        
        
        $model["d_usuario"]["show_grid"] = 1;
        $model["d_usuario"]["header_display"] = "Usuario";
        $model["d_usuario"]["width"] = "8";
        $model["d_usuario"]["order"] = "10";
        $model["d_usuario"]["text_colour"] = "#004400";
        
        
        
        
        
         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Comprobante",
                    "content" => $model
                );
        
          echo json_encode($output);
?>