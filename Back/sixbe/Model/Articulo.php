<?php
App::uses('AppModel', 'Model');
App::uses('ArticuloRelacion', 'Model');
App::uses('DatoEmpresa', 'Model');


class Articulo extends AppModel {


	public $useTable = 'producto';

    public $actsAs = array('Containable');
    
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		/*'id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar un id v&aacute;lido',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
		),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
    );
    

    /*Definicion de los campos que necesitan restriccion decimal*/
    public function setDecimalPlaces(){//configura los decimal places para este Model
    
     App::uses('Modulo', 'Model');
     $modulo_obj = new Modulo();
     $cantidad_decimales = $modulo_obj->getCantidadDecimalesFormateo(EnumModulo::STOCK); 
     
     $this->decimal_places = array(
    "stock_minimo"=>$cantidad_decimales,
    "stock_maximo"=>$cantidad_decimales,
    "stock_optimo"=>$cantidad_decimales
    
    );
    
    }
//The Associations below have been created with all possible keys, those that are not needed can be removed
/**
 * belongsTo associations
 *
 * @var array
 */
 public $hasMany = array(
         'ArticuloRelacion' => array(
            'className' => 'ArticuloRelacion',
            'foreignKey' => 'id_producto_padre',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        
         'ListaPrecioProducto' => array(
            'className' => 'ListaPrecioProducto',
            'foreignKey' => 'id_producto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'StockProducto' => array(
            'className' => 'StockProducto',
            'foreignKey' => 'id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
 		'ProductoCodigo' => array(
 				'className' => 'ProductoCodigo',
 				'foreignKey' => 'id_producto',
 				'dependent' => false,
 				'conditions' => '',
 				'fields' => '',
 				'order' => '',
 				'limit' => '',
 				'offset' => '',
 				'exclusive' => '',
 				'finderQuery' => '',
 				'counterQuery' => ''
 		),
 		
);


public $belongsTo = array(
        'Unidad' => array(
            'className' => 'Unidad',
            'foreignKey' => 'id_unidad',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Iva' => array(
            'className' => 'Iva',
            'foreignKey' => 'id_iva',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'FamiliaProducto' => array(
            'className' => 'FamiliaProducto',
            'foreignKey' => 'id_familia_producto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Origen' => array(
            'className' => 'Origen',
            'foreignKey' => 'id_origen',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'Categoria' => array(
            'className' => 'Categoria',
            'foreignKey' => 'id_categoria',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'MediaFile' => array(
            'className' => 'MediaFile',
            'foreignKey' => 'id_media_file',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'Marca' => array(
            'className' => 'Marca',
            'foreignKey' => 'id_marca',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'ProductoTipo' => array(
            'className' => 'ProductoTipo',
            'foreignKey' => 'id_producto_tipo',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'CuentaContableVenta' => array(
            'className' => 'CuentaContableVenta',
            'foreignKey' => 'id_cuenta_contable_venta',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'CuentaContableCompra' => array(
            'className' => 'CuentaContableCompra',
            'foreignKey' => 'id_cuenta_contable_compra',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'ProductoClasificacion' => array(
            'className' => 'ProductoClasificacion',
            'foreignKey' => 'id_producto_clasificacion',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
		'Talle' => array(
				'className' => 'Talle',
				'foreignKey' => 'id_talle',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
				
		),
		'Color' => array(
				'className' => 'Color',
				'foreignKey' => 'id_color',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
		),
		'Area' => array(
				'className' => 'Area',
				'foreignKey' => 'id_area',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
		)
  );

  public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     "precio"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     "precio_minimo_producto"=>array("type"=>EnumTipoDato::decimal,"show_grid"=>0,"default"=>null,"length"=>null),
     "ultimo_precio_compra"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     "ultimo_precio_venta"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_moneda_precio"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "id_moneda_precio"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_familia"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_producto_tipo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_origen"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_unidad"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_marca"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_iva"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_categoria"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_destino_producto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_componente"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_materia_prima"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "id_lista_precio_producto"=>array("type"=>EnumTipoDato::biginteger,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_cuenta_contable_compra"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_cuenta_contable_venta"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
	 "requiere_conformidad"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_area"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "fecha_vencimiento"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
	 "item_observacion"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_fecha_vencimiento"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "stock"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_talle"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_color"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null)
     );
     
}
                         
  
   function afterSave($created, $options = array())  {
       
       /////////////////////////////////////INSERT//////////////////////////////////////////////////////////////////////
     /*   if($created){ //solo en el insert
        
            if(isset($this->data["Producto"]["id_otro_sistema"])){
                
                $id_a_actualizar =  $this->data["Producto"]["id_otro_sistema"];
            
                $id_de_insercion = $this->getLastInsertId();
            
           // $this->query("SET FOREIGN_KEY_CHECKS=0;");
           
            
            $this->updateAll(
                                                                                array('Producto.id' =>$id_a_actualizar   ),
                                                                                array('Producto.id' => $id_de_insercion) ); 
            //$this->query("SET FOREIGN_KEY_CHECKS=1;");
            $this->id = $id_a_actualizar;
        }
        }*/
       ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

       ////////////////////////////////UPDATE/////////////////////////////////////////////////////////////////////////////
     
       if(!$created){ //solo en update
        
                App::uses('CakeSession', 'Model/Datasource');
                $empresa = CakeSession::read('Empresa');
                
                if($empresa["DatoEmpresa"]["actualiza_costo_en_cadena"] == 1){
                    set_time_limit(0);
                    $id_articulo= $this->id; //recupero el ultimo ID
                    
                    $costo = $this->getandSetCosto($id_articulo);
                    $this->calculoCosto($costo,$id_articulo);
                }
          }
          
          
          
         
       /////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
   }
   
   
   public function calculoCosto($costo,$id_articulo){
       
       
       
       
       $producto_relacion = new ArticuloRelacion();
       $articulo_obj = new Articulo();         
        
                
                
       $articulos_padre = $producto_relacion->find('all', array(
        'conditions' => array('ArticuloRelacion.id_producto_hijo' => $id_articulo),
        'contain' =>false
        )); //busco si este articulo pertenece a alguna composicion
                
       
       if(count($articulos_padre)>0){// pertenece a una composicion
       
        $costo_aux = 0;
        foreach($articulos_padre as $articulo){
            $this->getandSetCosto($articulo["ArticuloRelacion"]["id_producto_padre"]);
            $this->calculoCosto($costo_aux,$articulo["ArticuloRelacion"]["id_producto_padre"]);
        
        }
        
        /*
        $articulo_obj->updateAll(
                                                          array('Articulo.costo' => $costo_aux ),
                                                          array('Articulo.id' => $id_articulo) );   
        
        $costo = $costo + $costo_aux;
         */
           
       }else{
           
           $this->getandSetCosto($id_articulo);//si no tiene componentes devuelve si tiene entonces lo calcula y lo devuelve                                    
       }
       
   }
   
   
  
   public function getandSetCosto($id_articulo){
       
       $articulo_obj = new Articulo();  
       $producto_relacion = new ArticuloRelacion();
       
       $articulos_final = $producto_relacion->find('all', array(
                'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_articulo),
                'contain' =>array('ArticuloHijo')
                ));//busco los articulos integrantes de este Articulo 
                
                
            $costo_articulo = 0;
            
            if($articulos_final){//si tiene composicion
                foreach($articulos_final as $articulo_individual){  //por cada componente le saco el costo y lo acumulo
                    $costo_articulo = $costo_articulo + $articulo_individual["ArticuloHijo"]["costo"]*$articulo_individual["ArticuloRelacion"]["cantidad_hijo"];
                }
                //actualizo el costo ya que llegue al final del arbol y no tiene hijos
                $articulo_obj->updateAll(
                                                          array('Articulo.costo' => $costo_articulo ),
                                                          array('Articulo.id' => $id_articulo) );   
                //$costo = $costo + $costo_articulo;       
            }else{
                
                
               $articulo = $articulo_obj->find('first', array(
                'conditions' => array('Articulo.id' => $id_articulo),
                'contain' =>false
                ));//busco los articulos
                
                $costo_articulo = $articulo["Articulo"]["costo"];
                
                
            }
            
            
            return $costo_articulo; 
       
   }
   
   public function AceptaStock($id_producto){
       
       
        $producto = $this->find('first', array(
                                                'conditions' => array('Producto.id' => $id_producto),
                                                'contain' => array("ProductoTipo")
                                                ));
                                                
       return $producto["ProductoTipo"]["tiene_stock"];
   }
   
   
   
   public function getIva($id_producto){
   	
   	
   	$producto = $this->find('first', array(
   			'conditions' => array('Producto.id' => $id_producto),
   			'contain' => false
   	));
   	
   	return $producto["Producto"]["id_iva"];
   	
   }
   
   
   
   public function getProductoMeli($id_meli_producto){
   	
   	
   	$producto = $this->find('first', array(
   			'conditions' => array('Articulo.meli_id' => $id_meli_producto),
   			'contain' => array('Iva')
   	));
   	
   	
   	return $producto;
   	
   }
   
   
   
      public function EsRemitible($id_producto){
       
       
        $producto = $this->find('first', array(
                                                'conditions' => array('Producto.id' => $id_producto),
                                                'contain' => array("ProductoTipo")
                                                ));
                                                
       return $producto["ProductoTipo"]["remitible"];
   }
   
   
   public function buscoBucle(&$tiene_bucle,$id_articulo_padre,$id_articulo_hijo){
        
       $articulo_obj = new Articulo();  
       $producto_relacion = new ArticuloRelacion();
       
       
       $articulos_final = $producto_relacion->find('all', array(
                'conditions' => array('ArticuloRelacion.id_producto_padre' => $id_articulo_hijo),
                'contain' =>array('ArticuloHijo')
                ));//busco los articulos integrantes de este Articulo 
                
                
       if($articulos_final){//si tiene hijos el hijo
           
           foreach($articulos_final as $articulo){
               
               if($articulo["ArticuloHijo"]["id"] == $id_articulo_padre){
                    $tiene_bucle = true;
                    break;
                    return;
                    
               }
               
               $this->buscoBucle($tiene_bucle,$id_articulo_padre,$articulo["ArticuloHijo"]["id"]);
       
                
           }
           return;
       }
                
    }
  
  
  
  public function afterFind($results, $primary = false) {
    
		 App::uses('Modulo', 'Model');
		 $modulo_obj = new Modulo();
		 $cantidad_decimales = $modulo_obj->getCantidadDecimalesFormateo(EnumModulo::STOCK); 
		
		foreach ($results as $key => $val) {
			
			
			foreach($this->decimal_places as $key_decimal_places=>$cantidad_decimales){ //busco en el array decimal places la cantidad de decimales
				
				if( isset($results[$key][$this->name]) && isset($results[$key][$this->name][$key_decimal_places]) )
					$results[$key][$this->name][$key_decimal_places]  = (string) round($results[$key][$this->name][$key_decimal_places],$cantidad_decimales);
				
			}
		}
		return $results;
	}    

}
