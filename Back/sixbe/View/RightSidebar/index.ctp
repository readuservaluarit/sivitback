
          <!-- Dashboard Header Section    -->
          <section class="dashboard-header">
            <div class="container-fluid">
              <div class="row">
                <!-- Statistics -->
                <div class="statistics col-sm-3">
                  <?php echo $data_sidebar_widgets["Widget"]["contenido_html"];?>
                </div>
                
                
                   <div class="statistics col-sm-4">
                   </div>
                <!-- Line Chart            -->
               <div class="col-sm-5">
<!-- Accordion -->
            <div id="accordion">
                <h3>Alertas Programadas(<?php echo count($data_listado_alarmas)?>)</h3>
                <div>
                    <table class="table table-striped">
                        <tbody>
                        
                        <?php foreach($data_listado_alarmas as $valor){?>
                        
                            <tr class="<?php echo $valor["AlarmaUsuario"]["color"]?>">
                                <td><i class="fa fa-exclamation-triangle"></i></td>
                                <td><?php echo $valor["AlarmaUsuario"]["d_alarma"]?></td>
                         
                                <td><a onclick="$('html,body').css('cursor','wait');$(document).attr('title','<?php echo $valor["AlarmaUsuario"]["d_alarma"]?>');"  href="AlarmasUsuario/getDetalleAlarma/<?php echo $valor["AlarmaUsuario"]["id_alarma"]?>"><i class="fa fa-search"></i></a></td>
                            </tr>
                            
                          <?php }  ?>
                          
                           
                        </tbody>
                    </table>
                </div>
                
                  <h3>Reportes del usuario</h3>
                <div>
                    <table class="table table-striped">
                        <tbody>
                        
                        <?php foreach($data_listado_reporte_tablero as $valor){?>
                        
                            <tr class="<?php echo $valor["ReporteTableroUsuario"]["color"]?>">
                                <td><i class="fa fa-exclamation-triangle"></i></td>
                                <td><?php echo $valor["ReporteTableroUsuario"]["d_reporte_tablero_usuario"]?></td>
                         
                                <td><a  onclick="$('html,body').css('cursor','wait');$(document).attr('title','<?php echo $valor["ReporteTableroUsuario"]["d_reporte_tablero_usuario"]?>');" href="ReportesTableroUsuario/getDetalleReporteTablero/<?php echo $valor["ReporteTableroUsuario"]["id_reporte_tablero"]?>"><i class="fa fa-search"></i></a></td>
                            </tr>
                            
                          <?php }  ?>
                          
                           
                        </tbody>
                    </table>
                </div>
                <h3>Notas de Usuario</h3>
                <div>Pr&oacute;ximamente</div>
                <h3>Tareas</h3>
                <div>Pr&oacute;ximamente</div>
                
                 <h3>Mensajes SIVIT (<?php echo count($mensajes_sistema)?>)</h3>
                <div>
                    <table class="table table-striped">
                        <tbody>
                        
                        <?php if(isset($mensajes_sistema)){ foreach($mensajes_sistema as $valor){?>
                        
                            <tr class="<?php echo $valor["AlarmaUsuario"]["color"]?>">
                                <td><i class="fa fa-exclamation-triangle"></i></td>
                                <td><?php echo $valor["AlarmaUsuario"]["d_alarma"]?></td>
                         
                                <td><a onclick="$('html,body').css('cursor','wait');$(document).attr('title','<?php echo $valor["AlarmaUsuario"]["d_alarma"]?>');"  href="AlarmasUsuario/getDetalleAlarma/<?php echo $valor["AlarmaUsuario"]["id_alarma"]?>"><i class="fa fa-search"></i></a></td>
                            </tr>
                            
                          <?php }}  ?>
                          
                           
                        </tbody>
                    </table>
                </div>
            </div>

                
              </div>
            </div>
          </section>
          
         
         
  


<script>
$("#accordion").accordion({ autoHeight: false, navigation: true });

$(window).load(function() {
 right_sidebar.ready();
});


 
    
    
    
 var url = "<?php echo Router::url(array('controller' => 'Monedas', 'action' => 'ConsultarCotizacionMoneda')); ?>.json";
 var url2 = "<?php echo Router::url(array('controller' => 'Monedas', 'action' => 'getCotizacionMonedaSiv')); ?>.json";
 
 <?php

echo $data_sidebar_widgets["Widget"]["contenido_js"];



?>





</script>