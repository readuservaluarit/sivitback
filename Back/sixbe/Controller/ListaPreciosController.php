<?php

class ListaPreciosController extends AppController
{
    public $name = 'ListaPrecios';
    public $model = 'ListaPrecio';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    
    /**
     *
     * @secured(CONSULTA_LISTA_PRECIO)
     */
    public function index(){

        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = array();
		$this->RecuperoFiltros($conditions);
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' =>array('TipoListaPrecio','ListaPrecioPadre'=>array('TipoListaPrecio'),'Moneda'),
			//'contain' =>array('ArticuloRelacion'/*=>array('Componente'=>array('Categoria')*/), 'FamiliaProducto','StockProducto','Origen'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.id desc'
        );
	  //$this->Security->unlockedFields = array('costo','precio'); 
      if($this->RequestHandler->ext != 'json'){  
    }
          
    else{ // vista json
    
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
		//parseo el data para que los models queden dentro del objeto de respuesta
		foreach($data as &$valor)
		{
			$this->cleanforOutput($valor);
		}
       //parseo el data para que los models queden dentro del objeto de respuesta
        /* foreach($data as &$valor){
             //$this->cleanforOutput($valor,2);
         }
        */
		$this->data = $data;
        $output = array(
             "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
     }

    //fin vista json
		//[id_producto_tipo]", this.id_producto_tipo.GetStringValue()); 
       }
    
    
    /**
    * 
    * @secured(MODIFICACION_LISTA_PRECIO)
    */
   public function edit($id) 
   {
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->loadModel("ListaPrecioProducto");
            $this->loadModel(EnumModel::Melibre);
            $this->loadModel(EnumModel::DatoEmpresa);
           
           $ds = $this->{$this->model}->getdatasource();
           
           $items_lista_precio = array();
               
            try{ 
                $ds->begin();
           
                $retorno = $this->SaveItems($this->request->data,$message,0);//guarda los ListaPrecioProducto borra
               
                $retorno =0;
                
                
                if($retorno == 0){//no hay errores
                    if ($this->{$this->model}->saveAll($this->request->data,array('deep' => true  ) ) )
					{
                      
                             $error = EnumError::SUCCESS;
                             $message = "Ha sido modificada exitosamente";
                            
                             
                             if(isset($this->request->data["NuevosProductos"])){//busco los items para pasarlos al front asi el front no tiene que refrescar toda la grilla.
                             	
                             	$items_lista_precio = $this->ListaPrecioProducto->find('all', array(
                             			'conditions' => array('ListaPrecioProducto.id_producto' => $this->request->data["NuevosProductos"],"ListaPrecioProducto.id_lista_precio"=>$id),
                             			'contain' => false
                             	));
                             	
 
                             	
                             }
                             
                             
                             $dato_empresa  = $this->{EnumModel::DatoEmpresa}->find('first', array(
                             		'conditions' => array(
                             				'DatoEmpresa.id' => 1
                             		) ,
                             		'contain' => false
                             ));
                             
                             if($dato_empresa["DatoEmpresa"]["mercadolibre_id_lista_precio"]>0){//definio la lista de MercadoLibre entonces actualizo los precios de MercadoLibre
                             	
                             	
                             	$this->{EnumModel::Melibre}->actualizarPrecios($id);
                             	
                             	
                             }
                            
                            $ds->commit();
                    }else{
                        $ds->rollback();
                        $errores = $this->{$this->model}->validationErrors;
                        $errores_string = "";
                        foreach ($errores as $error){
                            $errores_string.= "&bull; ".$error[0]."\n";
                            
                        }
                        $mensaje = $errores_string;
                        $error = EnumError::ERROR;
                        $message = "Ha ocurrido un error,  No ha podido modificarse".$mensaje;
                        
                    }
                }else{
                    $error = EnumError::ERROR;
                    
                    
                }
            }catch(Exception $e){
                $ds->rollback(); 
                $error = EnumError::ERROR;
                 $message = "Ha ocurrido un error, No ha podido modificarse. ".$e->getMessage();
			}
            if($this->RequestHandler->ext == 'json')
			{  
				$output = array
				(
					"status" => $error,
					"message" => $message,
					"content" => $items_lista_precio,
				); 
				$this->set($output);
				$this->set("_serialize", array("status", "message", "content"));
            }
        } 
    }
    
    
    
    /**
     * @secured(ADD_LISTA_PRECIO)
     */
    public function add() {
    	if ($this->request->is('post')){
    		$this->loadModel($this->model);
    		
    		$id_lista_precio = "";
    		try{
    			
    			
    			$this->request->data["ListaPrecio"]["activo"] = 1; //por default es activada
    			
    			if ($this->ListaPrecio->saveAll($this->request->data, array('deep' => true))){
    				$mensaje = "La ListaPrecio ha sido creada exitosamente";
    				$id_lista_precio = $this->ListaPrecio->id;
    				
    				$retorno = $this->SaveItems($this->request->data,$mensaje,$id_lista_precio);//guarda los ListaPrecioProducto 
    				
    				
    				if($retorno == 0){
    					$tipo = EnumError::SUCCESS;
    				}else{
    					
    					$tipo = EnumError::ERROR;
    				}
    				/*Se deben agregar los articulos en LPP correspondientes al sistema de la la lista de precio*/
    				
    			}else{
    				$errores = $this->{$this->model}->validationErrors;
    				$errores_string = "";
    				foreach ($errores as $error){
    					$errores_string.= "&bull; ".$error[0]."\n";
    					
    				}
    				$mensaje = $errores_string;
    				$tipo = EnumError::ERROR;
    				
    			}
    		}catch(Exception $e){
    			
    			$mensaje = "Ha ocurrido un error,la ListaPrecio no ha podido ser creada.".$e->getMessage();
    			$tipo = EnumError::ERROR;
    		}
    		$output = array(
    				"status" => $tipo,
    				"message" => $mensaje,
    				"content" => "",
    				"id_add" => $id_lista_precio
    		);
    		//si es json muestro esto
    		if($this->RequestHandler->ext == 'json'){
    			$this->set($output);
    			$this->set("_serialize", array("status", "message", "content","id_add"));
    		}else{
    			
    			$this->Session->setFlash($mensaje, $tipo);
    			$this->redirect(array('action' => 'index'));
    		}
    	}
    	
    	//si no es un post y no es json
    	if($this->RequestHandler->ext != 'json')
    		$this->redirect(array('action' => 'abm', 'A'));

    }
    
    /**
    * 
    * @secured(BAJA_LISTA_PRECIO)
    */
    function delete($id) {
        
        $this->loadModel($this->model);
        $this->loadModel("ListaPrecioProducto");
        $mensaje = "";
        $status = "";
        $this->{$this->model}->id = $id;
       try{  
           
            if ($this->{$this->model}->delete()) {
            
             
            	$this->ListaPrecioProducto->deleteAll( array("id_lista_precio" => $id) );     
           
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "Ha sido eliminada exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }  
          }
          catch(Exception $ex)
          { 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
    }
  
	    
	 /**
	* 
	* @secured(CONSULTA_LISTA_PRECIO)
	*/
  public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    } 
    
   private function editforView($model,$vista)
   {  //esta funcion recibe el model y pone los campos que se van a ver en la grilla   
      $this->set('model',$model);
      $this->set('model_name',$this->model);
      Configure::write('debug',0);
      $this->render($vista);
    }  
    
    protected function cleanforOutput(&$valor)
	{    
	   $valor["ListaPrecio"]["d_tipo_lista_precio"] = $valor["TipoListaPrecio"]["d_tipo_lista_precio"];
	   
	   $valor["ListaPrecio"]["d_lista_padre"] = $valor["ListaPrecioPadre"]["d_lista_precio"];
	   
	   $valor["ListaPrecio"]["id_tipo_lista_precio"] = $valor["ListaPrecio"]["id_tipo_lista_precio"];
	   
	   $valor["ListaPrecio"]["moneda_simbolo"] = $valor["Moneda"]["simbolo"];
	   
	   
	   if($valor["ListaPrecio"]["por_defecto"] == 1 )
	   		$valor["ListaPrecio"]["d_por_defecto"]  = "Si";
	   else 
	   		$valor["ListaPrecio"]["d_por_defecto"]  = "No";
	   
	   		
	   // if(isset($valor["ListaPrecioPadre"]["TipoListaPrecio"]["id_tipo_lista_precio"]))
	   // $valor["ListaPrecio"]["id_tipo_lista_precio_padre"] = $valor["ListaPrecioPadre"]["TipoListaPrecio"]["id_tipo_lista_precio"];
	
	   if(isset($valor["ListaPrecioPadre"]["TipoListaPrecio"]["d_tipo_lista_precio"]))
	   {
			$valor["ListaPrecio"]["d_tipo_lista_precio_padre"] = $valor["ListaPrecioPadre"]["TipoListaPrecio"]["d_tipo_lista_precio"];
			$valor["ListaPrecio"]["id_tipo_lista_precio_padre"] = $valor["ListaPrecioPadre"]["TipoListaPrecio"]["id"];
	   }
	   
	   unset($valor["TipoListaPrecio"]);
	   unset($valor["ListaPrecioPadre"]); 
	}
  
  protected function RecuperoFiltros(&$conditions)
  {
		//Recuperacion de Filtros
        $id                    =  $this->getFromRequestOrSession($this->model.'.id');
		$d_lista_precio        =  $this->getFromRequestOrSession($this->model.'.d_lista_precio');
        $id_origen             =  $this->getFromRequestOrSession($this->model.'.id_origen');
		
		$id_sistema            =  $this->getFromRequestOrSession($this->model.'.id_sistema');
        $id_lista_precio_padre =  $this->getFromRequestOrSession($this->model.'.id_lista_precio_padre');
        $id_tipo_lista_precio   = $this->getFromRequestOrSession('ListaPrecio.id_tipo_lista_precio');
        
        $solo_vigentes = $this->getFromRequestOrSession('ListaPrecio.solo_vigentes');
        
        
        $conditions = array(); 
        
        array_push($conditions, array($this->model.'.activo =' => 1)); 
        
        if($id!="") 			array_push($conditions, array($this->model.'.id =' => $id )); 
        
        if($d_lista_precio!="") array_push($conditions, array($this->model.'.d_lista_precio LIKE' => '%' . $d_lista_precio  . '%'));
       
        if($id_origen!="") 		array_push($conditions, array($this->model.'.id_origen =' => $id_origen ));
		
        if($id_sistema!="") 		array_push($conditions, array($this->model.'.id_sistema =' => $id_sistema ));		
            
        if($id_lista_precio_padre!="") array_push($conditions, array($this->model.'.id_lista_precio_padre =' => $id_lista_precio_padre ));     
       
        if($id_tipo_lista_precio!=""){
        	$tipos_excluir = explode(",", $id_tipo_lista_precio);
        	array_push($conditions, array($this->model.'.id_tipo_lista_precio' => $tipos_excluir));
        }
		
            // if($solo_vigentes == 1){
        	
        	// $hoy = date('Y-m-d');
        	// array_push($conditions, array($this->model.'.fecha_vigencia <=' => $hoy));
        	// array_push($conditions, array($this->model.'.fecha_vencimiento >=' => $hoy));
            // }
        
		return $conditions;
   }
  
   
   
  private function SaveItems(&$data,&$mensaje,$id_lista_precio=0)
  {
	  $this->loadModel("ListaPrecioProducto");
      /*
      
	  if(isset($data["ListaPrecioProducto"]) && isset($data["ListaPrecio"]["id"]) && $data["ListaPrecio"]["id"]>0 && $data["ListaPrecio"]["id_tipo_lista_precio"] == EnumTipoListaPrecio::Base){//si es un edit
      $mensaje = "";    
      */
      
      $items_nuevos = array();
      
      if(isset($data["ListaPrecioProducto"]) && count($data["ListaPrecioProducto"])>0 ){
	      foreach($data["ListaPrecioProducto"] as $item){
	          if(!isset($item["id"])){ //valido solo para nuevos items
	             
	          	array_push($items_nuevos, $item["id_producto"]);
	         
	          }
	      }
	      
	      if(count($items_nuevos)>0)
	      	$data["NuevosProductos"] = $items_nuevos;
	      
	      	
      }
   
      	
	  if($id_lista_precio >0 && ($data["ListaPrecio"]["id_tipo_lista_precio"] == EnumTipoListaPrecio::Base || $data["ListaPrecio"]["id_tipo_lista_precio"] == EnumTipoListaPrecio::BaseCosto) ){
      		
      		
      		if($data["ListaPrecio"]["id_sistema"] == EnumSistema::VENTAS){
      			
      			$id_destino_producto = array(2,3);
      		}elseif($data["ListaPrecio"]["id_sistema"] == EnumSistema::COMPRAS){
      			
      			$id_destino_producto = array(1,3);
      		}
      		
      		$id_destino_producto = implode(",", $id_destino_producto);
      		$datoEmpresa = $this->Session->read('Empresa');
      		$id_moneda_lista_precio = $data["ListaPrecio"]["id_moneda"];
      		
      		
      		
      		try{
      			
      		
      			
      		if(isset($this->request->data["ListaPrecio"]["chk_asociar_con_todos"]) && $this->request->data["ListaPrecio"]["chk_asociar_con_todos"] == 1){
	      		$this->ListaPrecioProducto->query("
													INSERT INTO lista_precio_producto (id_producto,id_lista_precio,precio,id_moneda)
													SELECT producto.id,".$id_lista_precio.",0,".$id_moneda_lista_precio." FROM producto 
													JOIN producto_tipo ON producto_tipo.id = producto.`id_producto_tipo`
													
													WHERE id_destino_producto IN (".$id_destino_producto.")");
      		}
      			return 0;
      		
      		}catch(Exception $e){
      			
      			$message = $e->getMessage();
      			return 1;
      	
      			
      		}
      		
	  }elseif($id_lista_precio >0 && $data["ListaPrecio"]["id_tipo_lista_precio"] == EnumTipoListaPrecio::Enlazada){
      		
      		
	  	if(isset($data["ListaPrecioProducto"]))	
	  		unset($data["ListaPrecioProducto"]);//si es enlazada no puede enviar productos
      		
      	}
  } 
  
  
  /**
   * @secured(BTN_EXCEL_LISTAPRECIOS)
   */
  public function excelExport($vista="default",$metodo="index",$titulo=""){
  	
  	parent::excelExport($vista,$metodo,"Listado de Listas de Precios");
  	
  }
}
?>