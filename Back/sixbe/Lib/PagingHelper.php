<?php
/**
 * Paging Helper
 * Implementa las funcionalidades basicas para realizar la paginación de un listado
 */


class PagingHelper 
{
    
    public static function getRowClass($rownum){
        
        if ($rownum%2==0) 
            return "gradeX even";
        else
            return "gradeX odd";
    
    }
    
    public static function SortHeader($model, $key, $title = null, $width = 0, $visible = true, $options = array()){
        
        
        if ($model->Paginator->sortKey()==$key){
            if ($model->Paginator->sortDir()=='asc')
                $class = "sorting_asc";
             else 
                $class = "sorting_desc"; 
        }else{
           
           $class = "sorting"; 
        }
        
        $display = "";
        if(!$visible)
            $display = "display:none;";
        
            
        if($width!="" && $width!=0 )    
        	echo "<th class='$class' rowspan='1' colspan='1' style='width:" . $width .";".$display . "'>";
        else 
        	echo "<th class='$class' rowspan='1' colspan='1' ".$display . ">";
        
        echo $model->Paginator->sort($key, $title, $options);
        
        echo "</th>";
    }
    
    public static function SortedHeader($model, $key, $title = null, $options = array()){
        
        
        if ($model->Paginator->sortKey()==$key){
            if ($model->Paginator->sortDir()=='asc')
                $class = "sorting_asc";
             else 
                $class = "sorting_desc"; 
        }else{
           
           $class = "sorting"; 
        }
        
            
        echo "<th class='$class' rowspan='1' colspan='1'>";
        
        echo $model->Paginator->sort($key, $title, $options);
        
        echo "</th>";
    }
    
    public static function NonSortedHeader($model, $title = null, $visible = false){
           
        echo "<th class='sorting' rowspan='1' colspan='1'>";
        
        echo $title;
        
        echo "</th>";
    }

}
