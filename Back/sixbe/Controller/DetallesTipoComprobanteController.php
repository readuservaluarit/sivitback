<?php



class DetallesTipoComprobanteController extends AppController {
    public $name = 'DetallesTipoComprobante';
    public $model = 'DetalleTipoComprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    
    /**
     * @secured(CONSULTA_DETALLE_TIPO_COMPROBANTE)
     */
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        $conditions = $this->RecuperoFiltros($this->model);
    
        $this->paginado = 0;
        
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => 10000,
            'page' => $this->getPageNum(),
            'order'=> 'DetalleTipoComprobante.d_detalle_tipo_comprobante ASC'
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de DetallesTipoComprobante', 'Datos de los DetallesTipoComprobante', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_DETALLE_TIPO_COMPROBANTE', 'Nombre de DetalleTipoComprobante', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'DetalleTipoComprobante.id', "10%");
                $formBuilder->addHeader('Nombre', 'DetalleTipoComprobante.d_moneda', "50%");
             

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_DETALLE_TIPO_COMPROBANTE');
        
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json                
        	
        	
        $this->PaginatorModificado->settings = $this->paginate; 
        
        $data = $this->PaginatorModificado->paginate($this->model);
        
        foreach($data as &$dato){
            
            if(isset($dato["Impuesto"]))
                $dato["DetalleTipoComprobante"]["d_impuesto"] = $dato["Impuesto"]["d_impuesto"];
                
           if(isset($dato["TipoComprobante"])){
                $dato["DetalleTipoComprobante"]["d_tipo_comprobante"] = $dato["TipoComprobante"]["d_tipo_comprobante"];
                
                
                	
                	if( $dato["DetalleTipoComprobante"]["genera_nd"] == 1)
                		$dato["DetalleTipoComprobante"]["d_detalle_tipo_comprobante"] = $dato["DetalleTipoComprobante"]["d_detalle_tipo_comprobante"]." - Genera ND";
                	
                	
                
           }
            if(isset($dato["CuentaContable"])){
                $dato["DetalleTipoComprobante"]["d_cuenta_contable"] = $dato["CuentaContable"]["d_cuenta_contable"];   
                $dato["DetalleTipoComprobante"]["codigo_cuenta_contable"] = $dato["CuentaContable"]["codigo"];   
           } 
           
           unset($dato["TipoComprobante"]);
           unset($dato["Impuesto"]);
           unset($dato["CuentaContable"]);
        }
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->DetalleTipoComprobante->id = $id;
            $this->request->data = $this->DetalleTipoComprobante->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('DetalleTipoComprobante');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de DetalleTipoComprobante');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de DetalleTipoComprobante');
        else
            $formBuilder->setTituloForm('Consulta de DetalleTipoComprobante');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_DETALLE_TIPO_COMPROBANTE', 'Nombre de DetalleTipoComprobante', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
     
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'EstadoAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('EstadoDEstado', 'Debe ingresar un nombre');
        


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_DETALLE_TIPO_COMPROBANTE,READONLY_PROTECTED)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_add = '';
            
            try{
                if ($this->DetalleTipoComprobante->saveAll($this->request->data)){
                    $id_add = $this->DetalleTipoComprobante->id;
                    $mensaje = "El Item ha sido creado exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el Item no ha podido ser creado.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" => $id_add
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
                          
    }

    /**
    * @secured(MODIFICACION_DETALLE_TIPO_COMPROBANTE,READONLY_PROTECTED)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "El Item ha sido modificado exitosamente";
                     $status = EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
               }
               
               if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, el Item no ha podido modificarse.', 'error');
                
            }
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            //$this->{$this->model}->contain('Provincia','Pais');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    

    /**
    * @secured(BAJA_DETALLE_TIPO_COMPROBANTE,READONLY_PROTECTED)
    */
    function delete($id) { 
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->{$this->model}->id = $id;
     
       try{
            if ($this->{$this->model}->delete() ) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Item ha sido eliminado exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
    }
    
    
    function RecuperoFiltros($model){
        
          //Recuperacion de Filtros
        $nombre = strtolower($this->getFromRequestOrSession('DetalleTipoComprobante.d_detalle_tipo_comprobante'));
        $id_impuesto = strtolower($this->getFromRequestOrSession('DetalleTipoComprobante.id_impuesto'));
        $id_tipo_comprobante = strtolower($this->getFromRequestOrSession('DetalleTipoComprobante.id_tipo_comprobante'));
        
        $conditions = array(); 
        if ($nombre != "") {
            array_push($conditions, array('LOWER(DetalleTipoComprobante.d_detalle_tipo_comprobante) LIKE' => '%' . $nombre . '%'));
        }
        if ($id_impuesto != "") {
              array_push($conditions,array('DetalleTipoComprobante.id_impuesto ' => $id_impuesto ));
        }
        
         if ($id_tipo_comprobante != "") {
             $id_tipo_comprobante = explode(',', $id_tipo_comprobante);
              array_push($conditions,array('DetalleTipoComprobante.id_tipo_comprobante ' => $id_tipo_comprobante ));
        }
        
        return $conditions;
        
        
        
    }
    
     /**
    * @secured(CONSULTA_DETALLE_TIPO_COMPROBANTE)
    */ 
     public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
      
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
        
        
         $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);          
    }



}
?>