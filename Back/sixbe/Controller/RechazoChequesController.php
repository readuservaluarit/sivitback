<?php
App::uses('ComprobantesController', 'Controller');


  
  
class RechazoChequesController extends ComprobantesController {
    
    public $name = EnumController::RechazoCheques;
    public $model = 'Comprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $requiere_impuestos = 0;//Flag para impuestos calculados por el sistema
    
    
    
 
    /**
    * @secured(CONSULTA_RECHAZO_CHEQUE)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros($this->model);
            
            
                       
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>array('EstadoComprobante','TipoComprobante','ComprobanteValor', 'PuntoVenta','DetalleTipoComprobante','Moneda'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.id desc'
        ); //el contacto es la sucursal
        
      if($this->RequestHandler->ext != 'json'){  
        
    }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
            
             //$valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
             //unset($valor['ComprobanteItem']);
             
             if(isset($valor['TipoComprobante'])){
                 $valor[$this->model]['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                 $valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
                 unset($valor['TipoComprobante']);
             }
             
             $valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
             $valor[$this->model]['nro_comprobante'] = (string) str_pad($valor[$this->model]['nro_comprobante'], 8, "0", STR_PAD_LEFT);
             
             $valor[$this->model]['d_detalle_tipo_comprobante'] = $valor['DetalleTipoComprobante']['d_detalle_tipo_comprobante'];
          
             
             if(isset($valor['EstadoComprobante'])){
                $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
                unset($valor['EstadoComprobante']);
                
                
             }
             
             
             if(isset($valor['Moneda'])){
             	$valor[$this->model]['d_moneda_simbolo'] = $valor['Moneda']['simbolo_internacional'];
             	unset($valor['Moneda']);
             	
             	
             }
             
             $this->{$this->model}->formatearFechas($valor); 
          
          
           
            
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    
    /**
    * @secured(ADD_RECHAZO_CHEQUE)
    */
    public function add(){

            parent::add();    
            return;

     }
           
   
    
    
    /**
    * @secured(MODIFICACION_RECHAZO_CHEQUE)
    */
    public function edit($id){
  
    $this->loadModel("Cheque");
    $this->loadModel("Comprobante");
    $cheques_error = '';
    $hay_error = 0;
    $id_nota_debito = "";
    
    
     
              $id_comprobante_rechazo = $this->request->data["Comprobante"]["id"]; 
              //si es definitivo le seteo el id_comprobante a los cheques
              if($this->Comprobante->getDefinitivoGrabado($id) != 1 && $this->Comprobante->getDefinitivo($this->request->data) == 1  && $this->Cheque->tieneCheques($this->request->data)== 1 ){
                  
                  
                   //$id_comprobante = $this->request->data["Comprobante"]["id"];
                   $array_cheques = $this->Cheque->getArrayCheques($this->request->data);
                   $ds = $this->Cheque->getdatasource();
                    try{ 
                        $ds->begin();
                       
                        
                        
                      
                        if( $this->Cheque->validarRechazoCheque($this->request->data,$mensaje) == 1 ){
                           //$id_comprobante_rechazo = $this->request->data["Comprobante"]["id"]; 
                           $this->Cheque->actualizaComprobanteCheques($id_comprobante_rechazo,$array_cheques,"id_comprobante_rechazo");
                           
                           $array_cheque_terceros = array();
                           $array_cheque_propios = array();
                           $this->Cheque->SeparaCheques($array_cheques,$array_cheque_terceros,$array_cheque_propios);//esta funcion le paso un array con cheques y me devuelve 2 arrays uno con los cheques de terceros y otro con los cheques propios
                        
                           $this->Cheque->actualizaEstadoCheques(EnumEstadoCheque::Rechazado,$array_cheque_propios);//actualizo el estado de los cheques  
                           $this->Cheque->actualizaEstadoCheques(EnumEstadoCheque::Rechazado,$array_cheque_terceros);//actualizo el estado de los cheques  
                           $ds->commit();
                           
                 	
                        }else{
                        	
                        	$tipo = EnumError::ERROR;
                        	$ds->rollback();
                        	$hay_error = 1;
                        	
                        
                        }
                      
                          
                       
                   
                   }catch(Exception $e){
                     
                     $tipo = EnumError::ERROR;
                     $mensaje = 'Hubo una falla cuando se actulizaron los comprobantes de salida en los cheques'.$e->getMessage();
                     $ds->rollback();
                     $hay_error = 1;
                   }
              }
              
        
              if($hay_error == 0){ /*Sino hubo falla al actualizar los cheques entonces si edito*/
                parent::edit($id);
                if($this->error == 1)  /*Si hay error devuelvo el error sino creo la ND*/
                    return;
                else{
                    $id_nota_debito = $this->generaNota($id_comprobante_rechazo,$tipo,$mensaje);//genera la Nota de Debito si corresponde
                    $this->id_comprobante_auto_generado = $id_nota_debito;
                }
                
              
               }   
   
      /*Aca solo entra si hubo error si entra por el edit sale por flujo de mensaje*/
      $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                "id_comprobante_auto_generado"=>$id_nota_debito,
                "id_asiento" => $this->id_asiento
                );      
            $this->set($output);
      
            $this->set("_serialize", array("status", "message", "content","id_comprobante_auto_generado","id_asiento"));
            //return;         
                
        
    }
    
    
    /**
    * @secured(BAJA_RECHAZO_CHEQUE)
    */
    public function delete($id){
        
     parent::delete($id);    
        
    }
    
   /**
    * @secured(BAJA_RECHAZO_CHEQUE)
    */
    public function deleteItem($id,$externo=1){
        
        parent::deleteItem($id,$externo);    
        
    }
    
    
    /**
    * @secured(CONSULTA_RECHAZO_CHEQUE)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
    
    
  
    /**
    * @secured(CONSULTA_RECHAZO_CHEQUE)
    */
    public function pdfExport($id,$vista=''){
        
        
     $this->loadModel("Comprobante");
	parent::pdfExport($id,"rechazo_cheque");  
        

    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
        
        
      
 
    }
    
    
    public function Anular($id_RECHAZO_CHEQUE){
        
        $this->loadModel("Comprobante");
        $this->loadModel("Asiento");
        $this->loadModel("Cheque");
        
        
          $comprobante_cheque_rechazado = $this->Comprobante->find('first', array(
                    'conditions' => array('Comprobante.id'=>$id_RECHAZO_CHEQUE),
                    'contain' => array('ComprobanteValor','Asiento','ChequeRechazo')
                ));
                
                 $ds = $this->Comprobante->getdatasource(); 
                 // $ds->rollback();
          $aborto = 0;
          
          $listado_cheques_terceros_anulados = array();
          $listado_cheques_propios_anulados = array();//se anulan porque son propios
          
                   
            if( $comprobante_cheque_rechazado && $comprobante_cheque_rechazado["Comprobante"]["id_estado_comprobante"]!= EnumEstadoComprobante::Anulado ){
                
                try{
                    $ds->begin();
                 
                       
                               
                            foreach($comprobante_cheque_rechazado["ChequeRechazo"] as $cheque){
                           
                                    
                                    if($cheque["id_tipo_cheque"] == EnumTipoCheque::Terceros){ 
                                    
                                     
                                        $this->Cheque->updateAll(
                                                            array('Cheque.id_comprobante_rechazo' => NULL,'Cheque.id_estado_cheque'=>EnumEstadoCheque::Cartera  ),
                                                            array('Cheque.id' => $cheque["id"]) );   
                                
                                         array_push($listado_cheques_terceros_anulados,$cheque["nro_cheque"]);
                                    }else{ //es propio
                                        
                                         $this->Cheque->updateAll(
                                                            array('Cheque.id_comprobante_rechazo' => NULL,'Cheque.id_estado_cheque'=>EnumEstadoCheque::Aplicado  ),
                                                            array('Cheque.id' => $cheque["id"]) );   
                                
                                         array_push($listado_cheques_propios_anulados,$cheque["nro_cheque"]);
                                    }
                              
                            }        
                        $monto_cheques = 0;
                        
                        /*
                        if($comprobante_cheque_rechazado["Comprobante"]["comprobante_genera_asiento"] == 1)         
                            $this->Asiento->revertir($comprobante_cheque_rechazado["Asiento"]["id"],$monto_cheques);   /*Se debe revertir el asiento del comprobante*/  
                            
                        
                          $output_asiento_revertir = array("id_asiento"=>0);
                        if($comprobante_cheque_rechazado["Comprobante"]["comprobante_genera_asiento"] == 1)
                            $output_asiento_revertir = $this->Asiento->revertir($comprobante_cheque_rechazado["Asiento"]["id"],$monto_cheques);      
                        
                        
                        $this->Comprobante->updateAll(
                                                        array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
                                                        array('Comprobante.id' => $id_RECHAZO_CHEQUE) );        
                                                        
                        $ds->commit(); 
                        $tipo = EnumError::SUCCESS;
                      
                       $mensaje = '';
                        if(count($listado_cheques_terceros_anulados)>0)
                            $mensaje = ". Se Re-Ingresaron en la cartera los siguientes cheques de Terceros: Cheque Nro:".implode(" Cheque Nro: ",$listado_cheques_terceros_anulados);
                        
                        if(count($listado_cheques_propios_anulados)>0)
                            $mensaje .= ". Se Volvieron a Aplicar los siguientes cheques Propios: Cheque Nro:".implode(" Cheque Nro: ",$listado_cheques_propios_anulados);
                        
                        $mensaje = "El Comprobante se anulo correctamente ".$mensaje." Recuerde tomar alguna acci&oacute;n si el comprobante genero alguna nota de d&eacute;bito";
                          
                        
                        
                             
                     
               }catch(Exception $e){ 
                
                $ds->rollback();
                $tipo = EnumError::ERROR;
                $mensaje = "No es posible anular el Comprobante ERROR:".$e->getMessage();
                
              }  
                            
            }else{
                
               $tipo = EnumError::ERROR;
               $mensaje = "No es posible anular el Comprobante ya se encuenta ANULADO";
            } 
            
            
            
        $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                "id_asiento" => $output_asiento_revertir["id_asiento"],
                );      
        echo json_encode($output);
        die();
        
         
        
    }
    
    public function CalcularImpuestos($id_comprobante,$id_tipo_impuesto='',$error=0,$message=''){
    
        return 0;
    }
    
    
   
    
    
    
    
    
    public function generaAsiento($id_comprobante,&$message){
    
         $this->loadModel("Comprobante");
         $this->loadModel("PeriodoContable");
         $this->loadModel("EjercicioContable");
         $this->loadModel("Modulo");
         $this->loadModel("CentroCosto");
         $habilitado  = $this->Modulo->estaHabilitado(EnumModulo::CONTABILIDAD);
         
         if($habilitado == 1) {
             
            $factura = $this->Comprobante->find('first', array(
                    'conditions' => array('Comprobante.id' => $id_comprobante),
                    'contain' =>array('TipoComprobante'=>array('Sistema'=>
                                                                            array('SistemaParametro')
                                                              )
                                      ,

                                      'ComprobanteValor'=>array('Valor',"CuentaBancaria"=>
                                                                            array('CuentaContable'),
                                                                "Cheque"=>
                                                                            array('BancoSucursal','TipoCheque','Chequera'=>array("CuentaBancaria"=>array('CuentaContable')))
                                                              ),'Asiento','DetalleTipoComprobante'
                                      )
                ));
                
                
          if($this->PermiteAsiento($factura) == 1 ){
            
            
                     if($factura && $factura["Comprobante"]["total_comprobante"]>0 && !$factura["Asiento"]["id"]>0 ){/*El asiento solo se hace si el comprobante es mayor a 0*/
                     
                     $id_sistema = $factura["TipoComprobante"]["id_sistema"];
                     $cuentas_detalle = array();
                     
                     
                     $cheque_obj = new Cheque();   
                     
                     
                     foreach($factura["ComprobanteValor"] as $item_valor){
                              
                                $monto = $item_valor["monto"];
                                
                               // $id_cuenta_contable = $this->getCuentaContableValor($item_valor); //devuelve la cuenta contable de un determinado valor (cheque,etc)
                              
                                  //https://docs.google.com/document/d/1Hu5Lt_QTl4_lERIqIwJNpS7y7ErXhgfjsuGXdYgyDeo/edit
                                  
                                  
                                  $id_cheque = $item_valor["id_cheque"];
                                  
                                  switch($factura["Comprobante"]["id_detalle_tipo_comprobante"]){
                                      
                                      case  EnumDetalleTipoComprobante::DetercerosRechazadoporBanco:  //CASO 2
                                      case  EnumDetalleTipoComprobante::ChequePropioRechazado:  //CASO 1
                                      case  EnumDetalleTipoComprobante::Tercerossalidaporegreso:  //CASO 4
                                      case  EnumDetalleTipoComprobante::Propiossalidaporegreso:  //CASO 5
                                            
                                            $id_cuenta_contable_debe = $cheque_obj->getCuentaContableChequePorRechazo($id_cheque,$factura["Comprobante"]["id_detalle_tipo_comprobante"]); //Cuenta Bancaria del cheque
                                            $id_cuenta_contable_haber = $cheque_obj->getCuentaContablePrincipalPorAsiento($id_cheque,$factura["Comprobante"]["id_detalle_tipo_comprobante"]); //Cuenta corriente del proveedor
                                            $monto = $item_valor["monto"];
                                            
                                      break;
 
                                     case  EnumDetalleTipoComprobante::TercerosEntregadoaProveedorconND:  //CASO 3   LA ND DEBE estar previamente cargada. VER DOCUMENTACION SOBRE COMO CARGARLA
                                      
                                            
                                            $id_cuenta_contable_debe = $cheque_obj->getCuentaContableChequePorRechazo($id_cheque,EnumDetalleTipoComprobante::TercerosEntregadoaProveedorconND,EnumTipoPersona::Cliente);
                                            $id_cuenta_contable_haber = $cheque_obj->getCuentaContableChequePorRechazo($id_cheque,EnumDetalleTipoComprobante::TercerosEntregadoaProveedorconND,EnumTipoPersona::Proveedor);
                                            $monto = $item_valor["monto"];
                                      break;
                                      
                                      
                                       case  EnumDetalleTipoComprobante::TercerosEntregadoaProveedorSinND:  //CASO 6 el proveedor NO manda la ND
                                      
                                            
                                            $id_cuenta_contable_debe = $cheque_obj->getCuentaContableChequePorRechazo($id_cheque,EnumDetalleTipoComprobante::TercerosEntregadoaProveedorSinND,'',EnumSistema::VENTAS);//cuenta contable del cliente
                                            $id_cuenta_contable_haber = $cheque_obj->getCuentaContableChequePorRechazo($id_cheque,EnumDetalleTipoComprobante::TercerosEntregadoaProveedorSinND,'',EnumSistema::COMPRAS);
                                            $monto = $item_valor["monto"];
                                      break;
                                      
                                     
                                      
                                    
                                      
                                  }
                                 
                                 
                                 
                                 
                                 
                                  if($id_cuenta_contable_debe>0 && $id_cuenta_contable_haber>0){
                                    $item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] =$id_cuenta_contable_debe;  
                                    $item_asiento["AsientoCuentaContable"]["monto"] = $monto ;
                                    $item_asiento["AsientoCuentaContable"]["es_debe"] = 1;
                                    array_push($cuentas_detalle,$item_asiento);
                                    
                                    $item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] =$id_cuenta_contable_haber;  
                                    $item_asiento["AsientoCuentaContable"]["monto"] = $monto ;
                                    $item_asiento["AsientoCuentaContable"]["es_debe"] = 0;
                                    array_push($cuentas_detalle,$item_asiento); 
                                  }
                                  else  { 
                                    $error = EnumError::ERROR;
                                    $message = "Alguno de los valores utilizados no tiene parametrizada la cuenta contable";
                                    return -1;    
                                  }  
                     }
                     
                     
                  
                     
                     
                     
                     
                     
                     $cabecera_asiento = $this->getCabeceraAsientoGeneradoPorComprobante($id_sistema,$factura);
                     $this->loadModel("Asiento");
                     $id_asiento = $this->Asiento->InsertaAsiento($cabecera_asiento,$cuentas_detalle,$detalle_error);
                     if($id_asiento > 0){
                        
                         $this->CentroCosto->ApropiacionPorAsiento($id_asiento);//https://docs.google.com/document/d/1-4uDVXbyn2OKmtamNxXRACuYElK2-ZWJCbybORqdlJI/edit
                        $error = EnumError::SUCCESS;
                        
                        $codigo_asiento = $this->Asiento->getCodigo($id_asiento);
                        $message = "El asiento fue creado correctamente. Nro: ".$codigo_asiento.". ID(".$id_asiento.")"; 
                        
                         
                        return $id_asiento;

                     }else{

                        $error = EnumError::ERROR;  
                        $message = "Ocurrio un error, el asiento no pudo ser creado".$detalle_error;
                        $this->log("Error en Asiento :".$this->name." ".$message);
                        return -1;
                     }
                         
                     }else{
                         
                         $error = EnumError::ERROR;
                          $message = "El total del comprobante es cero o ya tiene un asiento ingresado";
                          return -1;
                     }       
          }else{
                 
                 return 0;//no requiere asiento
          } 
                   
                  
         }else{
                //$error = EnumError::ERROR;
                //$message = "No tiene habilitado el m&oacute;dulo de Contabilidad";
                return 0;
        }
    }
    
    
    protected function actualizoMontos($id_c,$nro_c,$total_iva,$total_impuestos){
        
        return;
    }
    protected function borraImpuestos($id_comprobante){
        return;
    }  
    
    
   
   
   
   
   private function generaNota($id_comprobante,&$error,&$message){
       
        $this->loadModel("Comprobante");
        $this->loadModel("Cheque");
        $this->loadModel("Moneda");
        $this->loadModel("Persona");
        $rechazo_comprobante = $this->Comprobante->find('first', array(
                                                        'conditions' => array('Comprobante.id' => $id_comprobante,'Comprobante.id_tipo_comprobante'=>EnumTipoComprobante::ChequeRechazado),
                                                        'contain' =>array('DetalleTipoComprobante','PuntoVenta')
                                                        ));
        
        
        
        
        
        if($rechazo_comprobante["DetalleTipoComprobante"]["genera_nd"] == 1){
            
            $moneda = new Moneda();
            
            //debo generar la ND al cliente, el cliente lo determino con el comprobante de entrada del cheque
            $id_cheque =  $this->getChequeFromComprobanteRechazado($id_comprobante);
            $nro_cheque = $this->Cheque->getNumeroCheque($id_cheque);
            $id_cliente = $this->Cheque->getClientePorComprobanteEntrada($id_cheque);
            
            if($id_cliente == 0)//no se puede determinar el id_cliente entonces NO necesita ND cliente
                return 0;
            
            $persona = $this->Persona->find('first', array(
                                                        'conditions' => array('Persona.id' => $id_cliente),
                                                        'contain' =>false
                                                        ));
                                                        
            $cabecera_comprobante["Comprobante"]["total_comprobante"] = $rechazo_comprobante["Comprobante"]["total_comprobante"];
            $cabecera_comprobante["Comprobante"]["subtotal_neto"] = $rechazo_comprobante["Comprobante"]["total_comprobante"];
            $cabecera_comprobante["Comprobante"]["subtotal_bruto"] = $rechazo_comprobante["Comprobante"]["total_comprobante"];
            $cabecera_comprobante["Comprobante"]["id_tipo_comprobante"] = $this->GetTipoNotaDebitoABCME($persona["Persona"]["id_tipo_iva"], $persona["Persona"]["id_provincia"]);//ver que tipo necesita
            $cabecera_comprobante["Comprobante"]["fecha_generacion"] = date("Y-m-d H:i:s");
            $cabecera_comprobante["Comprobante"]["id_estado_comprobante"] = EnumEstadoComprobante::Abierto;
            $cabecera_comprobante["Comprobante"]["id_detalle_tipo_comprobante"] = EnumDetalleTipoComprobante::ChequesRechazados;
            $cabecera_comprobante["Comprobante"]["id_moneda"] = EnumMoneda::Peso;
            $cabecera_comprobante["Comprobante"]["valor_moneda"] = $moneda->getCotizacion(EnumMoneda::Peso);
            $cabecera_comprobante["Comprobante"]["valor_moneda2"] = 1;
            $cabecera_comprobante["Comprobante"]["valor_moneda3"] = $moneda->getCotizacion(EnumMoneda::Peso)/$moneda->getCotizacion(EnumMoneda::Euro);
            $cabecera_comprobante["Comprobante"]["observacion"] = "Nota de D&eacute;bito por rechazo al cheque :".$nro_cheque;
            $cabecera_comprobante["Comprobante"]["nro_comprobante"] = $this->obetener_ultimo_numero_comprobante($cabecera_comprobante["Comprobante"]["id_tipo_comprobante"],$rechazo_comprobante["Comprobante"]["id_punto_venta"],5,"+");
            $cabecera_comprobante["Comprobante"]["id_persona"] = $id_cliente;
            $cabecera_comprobante["Comprobante"]["id_usuario"] = $this->Auth->user('id');
            $cabecera_comprobante["Comprobante"]["activo"] = 1;
            $cabecera_comprobante["Comprobante"]["comprobante_genera_asiento"] = 0;//esta ND no debe generar asiento
            $cabecera_comprobante["Comprobante"]["definitivo"] = 0;//esta ND no debe generar asiento
            $cabecera_comprobante["Comprobante"]["id_punto_venta"] = $rechazo_comprobante["Comprobante"]["id_punto_venta"];//esta ND no debe generar asiento
            
            
            ///////////////////CAMPOS DEL ITEM/////////////////////////////
           // $items_comprobante["ComprobanteItem"]["id_comprobante"] = $id_comprobante;//la Nota de debito va a tener como id_comprobante_origen al RechazoCheque
            $items_comprobante["ComprobanteItem"]["id_comprobante_origen"] = $id_comprobante;//la Nota de debito va a tener como id_comprobante_origen al RechazoCheque
            $items_comprobante["ComprobanteItem"]["cantidad"] = 1;//la Nota de debito va a tener como id_comprobante_origen al RechazoCheque
            $items_comprobante["ComprobanteItem"]["precio_unitario_bruto"] = $rechazo_comprobante["Comprobante"]["total_comprobante"];//la Nota de debito va a tener como id_comprobante_origen al RechazoCheque
            $items_comprobante["ComprobanteItem"]["precio_unitario"] = $rechazo_comprobante["Comprobante"]["total_comprobante"];//la Nota de debito va a tener como id_comprobante_origen al RechazoCheque
            $items_comprobante["ComprobanteItem"]["orden"] = 1;
            $items_comprobante["ComprobanteItem"]["n_item"] = 1;
            $items_comprobante["ComprobanteItem"]["id_iva"] = EnumIva::exento;
            $items_comprobante["ComprobanteItem"]["item_observacion"] = "CHEQUE NRO:".$nro_cheque;
            $items_comprobante["ComprobanteItem"]["activo"] = 1;
           
            
            /////////////////////////////////////////////////////////////
            $contador = $this->chequeoContador($this->request->data[$this->model]["id_tipo_comprobante"],$rechazo_comprobante["Comprobante"]["id_punto_venta"]);
            
            if($contador){
                
                
                $id_nota = $this->Comprobante->InsertaComprobante($cabecera_comprobante,$items_comprobante);
                if($id_nota > 0){
                                      $error = EnumError::SUCCESS;
                                      $message = "La Nota fue creada correctamente. Nro: ".$this->Comprobante->GetNumberComprobante($rechazo_comprobante["PuntoVenta"]["numero"],$cabecera_comprobante["Comprobante"]["nro_comprobante"]);  
                                      return $id_nota;
                                      
                                  }else{
                                      
                                       $error = EnumError::ERROR;  
                                       $message = "Ocurrio un error, la Nota no pudo ser creada"; 
                                       return -1;
                                  }
                                  
                                  
                                  
                                  
                                  
            }else{
                                      
                $error = EnumError::ERROR;  
                $message= "Se debe definir el numero inicial del contador para este comprobante (Nota de Debito).Anule este Rechazo y vuelva a generarlo"; 
                return -1;
            }                      
    }else{
    	
    		$error = EnumError::SUCCESS;
    		$message= "Se ha modificado correctamente";
            
            return 0;//no necesita
        } 
   }
   
   
       
   private function getChequeFromComprobanteRechazado($id_comprobante){
       
        $this->loadModel("Comprobante");
   
        $rechazo_comprobante = $this->Comprobante->find('first', array(
                                                        'conditions' => array('Comprobante.id' => $id_comprobante,'Comprobante.id_tipo_comprobante'=>EnumTipoComprobante::ChequeRechazado),
                                                        'contain' =>array('ComprobanteValor')
                                                        ));  
      
      
      if($rechazo_comprobante){
          /*se concidera que el comprobante de rechazo tiene solo 1 CHEQUE*/
          foreach($rechazo_comprobante["ComprobanteValor"] as $valor){
              
              return $valor["id_cheque"];
          }
          
      } 
   }    
   
   
   /**
    * @secured(BTN_EXCEL_RECHAZOCHEQUES)
    */
   public function excelExport($vista="default",$metodo="index",$titulo=""){
   	
   	parent::excelExport($vista,$metodo,"Listado de Cheques Rechazados");
   	
   	
   	
   }
    
   
   /**
    * @secured(CONSULTA_RECHAZO_CHEQUE)
    */
   public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
   {
   	parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
   }
}
    
    
   
    
    
    
    
    
    
    

?>