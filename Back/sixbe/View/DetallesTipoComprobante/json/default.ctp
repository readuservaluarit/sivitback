<?php
        // CONFIG  CON MODE DISPLAYED CELLS

        
        $model["d_detalle_tipo_comprobante"]["show_grid"] = 1;
        $model["d_detalle_tipo_comprobante"]["header_display"] = "Descripci&oacute;n";
        $model["d_detalle_tipo_comprobante"]["width"] = "10"; //REstriccion mas de esto no se puede achicar
        $model["d_detalle_tipo_comprobante"]["order"] = "1";
        $model["d_detalle_tipo_comprobante"]["text_colour"] = "#000000";
		
		
		//RENOMBRAR EL DEFAULTA DE LA TABLA DETALLE TIPO COMPROBANTE
		$model["default"]["show_grid"] = 1;
        $model["default"]["header_display"] = "Por Defecto";
        $model["default"]["width"] = "3"; //REstriccion mas de esto no se puede achicar
        $model["default"]["order"] = "2";
        $model["default"]["text_colour"] = "#000000";
        
        
        $model["d_tipo_comprobante"]["show_grid"] = 1;
        $model["d_tipo_comprobante"]["type"] = EnumTipoDato::cadena;
        $model["d_tipo_comprobante"]["header_display"] = "Tipo";
        $model["d_tipo_comprobante"]["width"] = "10"; //REstriccion mas de esto no se puede achicar
        $model["d_tipo_comprobante"]["order"] = "3";
        $model["d_tipo_comprobante"]["text_colour"] = "#660000";
        
        
         $model["codigo_cuenta_contable"]["show_grid"] = 1;
          $model["codigo_cuenta_contable"]["type"] = EnumTipoDato::cadena;
        $model["codigo_cuenta_contable"]["header_display"] = "Cuenta Contable";
        $model["codigo_cuenta_contable"]["width"] = "6"; //REstriccion mas de esto no se puede achicar
        $model["codigo_cuenta_contable"]["order"] = "4";
        $model["codigo_cuenta_contable"]["text_colour"] = "#000088";
        
        
        $model["d_cuenta_contable"]["show_grid"] = 1;
        $model["d_cuenta_contable"]["type"] = EnumTipoDato::cadena;
        $model["d_cuenta_contable"]["header_display"] = "Desc. Cuenta Contable";
          // $model["d_tipo_comprobante"]["width"] = "10";
        $model["d_cuenta_contable"]["width"] = "8"; //REstriccion mas de esto no se puede achicar
        $model["d_cuenta_contable"]["order"] = "5";
        $model["d_cuenta_contable"]["text_colour"] = "#000000";
		
		 $model["d_impuesto"]["show_grid"] = 1;
		 $model["d_impuesto"]["header_display"] = "Desc. Impuesto";
         $model["d_impuesto"]["type"] = EnumTipoDato::cadena;
		 $model["d_impuesto"]["width"] = "8"; 
		 $model["d_impuesto"]["order"] = "6";
        $model["d_impuesto"]["text_colour"] = "#440077";

        
   
         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "DetalleTipoComprobante",
                    "content" => $model
                );
        
          echo json_encode($output);
?>