<?php

class AberturasLocalDescripcionPicker extends EntityPicker{
    
    public $model = "AberturasLocalDescripcion";
    
    public $pickerTitle = "Selección de Abertura";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Descripcion", "field" => "descripcion", "width" => "80%", "keyOrder" => "descripcion")
                                );

    public function getPickerConditions($busqueda){
        
        return array('AberturasLocalDescripcion.descripcion LIKE' => '%' . $busqueda['bus_unico'] . '%');
    }

}