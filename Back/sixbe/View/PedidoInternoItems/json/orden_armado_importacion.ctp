<?php include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');
 
 
$model["id_comprobante"]["show_grid"] = 1;
$model["id_comprobante"]["header_display"] = "Id. Comprob.";
$model["id_comprobante"]["order"] = "0";
$model["id_comprobante"]["width"] = "4";

$model["pto_nro_comprobante"]["show_grid"] = 1;
$model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
$model["pto_nro_comprobante"]["order"] = "1";
$model["pto_nro_comprobante"]["width"] = "9";

$model["n_item"]["show_grid"] = 1;
$model["n_item"]["header_display"] = "Nro Item";
$model["n_item"]["order"] = "2";
$model["n_item"]["width"] = "3";

$model["d_estado_comprobante"]["show_grid"] = 1;
$model["d_estado_comprobante"]["header_display"] = "Estado";
$model["d_estado_comprobante"]["order"] = "3";
$model["d_estado_comprobante"]["width"] = "7";
$model["d_estado_comprobante"]["text_colour"] = "#770011";

// $model["n_item"]["show_grid"] = 1;
// $model["n_item"]["header_display"] = "N? Item";
// $model["n_item"]["order"] = "4";
// $model["n_item"]["width"] = "4";

$model["codigo"]["show_grid"] = 1;
$model["codigo"]["header_display"] = "[C&oacute;digo]";
$model["codigo"]["order"] = "4";            
$model["codigo"]["width"] = "8";

$model["item_observacion"]["show_grid"] = 1;
$model["item_observacion"]["header_display"] = "Item Observ.";
$model["item_observacion"]["order"] = "5";            
$model["item_observacion"]["width"] = "20";

$model["cantidad"]["show_grid"] = 1;
$model["cantidad"]["header_display"] = "Cant. Pedido";
$model["cantidad"]["order"] = "6";            
$model["cantidad"]["width"] = "5";
$model["cantidad"]["min_value"] = "0"; 
$model["cantidad"]["text_colour"] = "#0000FF";//BLUE

$model["cantidad_pendiente_a_armar"]["show_grid"] = 1;
$model["cantidad_pendiente_a_armar"]["header_display"] = "Cant. Pend OA.";
$model["cantidad_pendiente_a_armar"]["order"] = "7";            
$model["cantidad_pendiente_a_armar"]["width"] = "5";
$model["cantidad_pendiente_a_armar"]["text_colour"] = "#0022DD";//BLUE

$model["total_armado"]["show_grid"] = 1;
$model["total_armado"]["header_display"] = "Cant. OA";
$model["total_armado"]["order"] = "8";
$model["total_armado"]["width"] = "5";
$model["total_armado"]["text_colour"] = "#660099";//RED VIOLET


$model["fecha_entrega"]["show_grid"] = 1;
$model["fecha_entrega"]["header_display"] = "Fch Entrega Cli";
$model["fecha_entrega"]["order"] = "9";
$model["fecha_entrega"]["width"] = "9";
$model["fecha_entrega"]["text_colour"] = "#880000";//RED 



$output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "ComprobanteItem",  /*Importante: Nombre del Model*/
				"content" => $model
			);
        
          echo json_encode($output);
?>