<?php   

function digitoVerificadorCaeAfip($txt){
  $i = $pares = $impares = 0;

  for( $i = 0; $i < strlen($txt); $i++ ) {
    // OJO: Los caracteres impares esta en la posiciones pares del array y viceversa, porque el array arranca desde la posicion 0
    if( $i % 2 ) {
      $pares += $txt[$i];
    } else {
      $impares += $txt[$i];
    }
  }
  $impares = $impares * 3;
  
  $total = $pares + $impares;
  
  $dv = $total % 10;

  return $dv;
}

/**
 * PDF exportación de Orden Compra
 */

App::import('Vendor','tcpdf/tcpdf'); 
App::import('Vendor','tcpdf/tcpdf_barcodes_1d'); 


// create new PDF document


$html = '<table width="100%" border="0" cellspacing="0" style="font-size:10px;border:2px solid #000000; font-family:\'Courier New\', Courier, monospace">
          <tr>
            <td style="padding: 5px 5px 5px 5px;" width="37%" rowspan="4" align="center" valign="center"  ><img src="'.$datos_empresa["DatoEmpresa"]["app_path"].'/img/'.$datos_empresa["DatoEmpresa"]["id"].'.png" width="476" height="129" />  </td>
            <td width="8%" height="3%" rowspan="2"  style="border:2px solid #000000"><div align="center" style="font-size:40px">FA</div></td>
            <td width="55%" style="padding-left:5px; font-size:17px;font-weight:bold;"> FICHA DE '.$datos_pdf["ProductoTipo"]["d_producto_tipo"].': </td>
            
          </tr>
          <tr>
            <td style="padding-left:5px; font-size:15px;">  '.strtoupper($datos_pdf["Articulo"]["codigo"]).'</td>
          </tr> 
          
          <tr>
            <td width="8%" height="2%" align="center" valign="middle" >&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr >
            <td  width="8%" height="5%" align="center" valign="middle" >&nbsp;</td>
            <td style="padding-left:5px;">  </td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["calle"].' '.$datos_empresa["DatoEmpresa"]["numero"].' '.$datos_empresa["DatoEmpresa"]["codigo_postal"].' '.$datos_empresa["Provincia"]["d_provincia"].'</td>
            <td rowspan="5">&nbsp;</td>
            <td style="padding-left:5px;"> Reporte</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> E-mail: '.$datos_empresa["DatoEmpresa"]["email"].'</td>
            <td style="padding-left:5px;"> CUIT: '.$datos_empresa["DatoEmpresa"]["cuit"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> Tel: '.$datos_empresa["DatoEmpresa"]["tel"].'</td>
            <td style="padding-left:5px;"> Ingresos Brutos: '.$datos_empresa["DatoEmpresa"]["iibb"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["website"].'</td>
            <td style="padding-left:5px;"> Inicio de actividades: '.$datos_empresa["DatoEmpresa"]["fecha_inicio_actividad"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.strtoupper($datos_empresa["TipoIva"]["d_tipo_iva"]).'</td>
            <td style="padding-left:5px;">&nbsp;</td>
          </tr>
        </table>';

$html;



class MyTCPDF extends TCPDF{

    public $datos_header;
    public $datos_footer;
    public $bar_code;
    public $observaciones;
    
    public function set_datos_header($datos_empresa){
        $this->datos_header = $datos_empresa; 

    }
    public function set_datos_footer($datos_empresa_footer){
        $this->datos_footer = $datos_empresa_footer; 

    }
     public function set_barcode($bar_code){
        $this->bar_code = $bar_code; 

    }
    
    public function set_observaciones($observaciones){
        $this->observaciones = $observaciones; 

    }
  public function Header(){
     
        
        
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_header, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
  }
  
  public function Footer(){
  
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_footer, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->observaciones, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
     
     $style['text'] = true;
     $style['border'] = true;
     $style['align'] = 'C';
     $this->write1DBarcode($this->bar_code, 'I25', '40', '', '', 18, 0.4, $style, 'N');
      
      //$this->writeHTMLCell($w = 0, $h = 0, $x = '200', $y = '', $this->bar_code, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
      //$this->Cell(0, 20, $this->bar_code , 0, false, 'C', 0, '', 0, false, 'T', 'M');
      $this->Cell(0, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
      
  }
}

$pdf = new MyTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->set_datos_header($html);



$observaciones = '';
$pdf->set_observaciones($observaciones);

$footer='
  <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>                     
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody> 
</table>

 ';
   
  $html .= "<HR>";
  
  $pdf->set_datos_footer($footer);
  
  
ob_clean();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sivit by Valuarit');
$pdf->SetTitle('Factura');
$pdf->SetSubject('Factura');
//$pdf->SetKeywords('');


//set default header data
$logo = 'img/header_factura.jpg';
$title = "";
$subtitle = ""; 
//$pdf->SetHeaderData($logo, 190, $title, $subtitle, array(0,0,0), array(0,104,128));
$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 50, 20);
$pdf->SetHeaderMargin(10);
$pdf->SetFooterMargin(20);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
/*$lg = Array();
$lg['a_meta_charset'] = 'ISO-8859-1';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';
*/
$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set default font subsetting mode
//$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
/*
$html = <<<EOD
<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
EOD;
*/

$tbody = "";

$renglones = $datos_pdf["ArticuloRelacion"];

$data = array();

foreach($renglones as $renglon){

  
   $tbody.='<tr style="border:2px solid #000000;font-size:9px;">
    <td>'.$renglon["ArticuloHijo"]["codigo"].'</td>
    <td>'.$renglon["ArticuloHijo"]["d_producto"].'</td>
    <td>'.$renglon["ArticuloHijo"]["stock_minimo"].'</td>
    <td>'.$renglon["cantidad_hijo"].'</td>
    <td>'.$renglon["ArticuloHijo"]["costo"].'</td>
    <td>'.$renglon["cantidad_hijo"]*$renglon["ArticuloHijo"]["costo"].'</td>

  </tr>';
}
 
//agrego las observaciones

   

$html='';

$html .=' 
<table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;font-family:\'Courier New\', Courier, monospace">

   
 
     <tbody>
     

      
     </tbody>
</table>
                  <table width="100%" border="1"  style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
                  <tr  style="background-color:rgb(224, 225, 229);">
                    <td colspan="4"><div align="center"><strong>Art&iacute;culo</strong></div></td>
                  </tr>
                  <tr>                                     
                    <td width="15%"><strong>Codigo:</strong> '.$datos_pdf["Articulo"]["codigo"].'</td>
                    <td width="55%"><strong>Descripci&oacute;n:</strong> '.$datos_pdf["Articulo"]["d_producto"].'</td>
                    <td width="10%"><strong>Costo:</strong> '.$datos_pdf["Articulo"]["costo"].' </td>
                    <td width="10%"><strong>Precio Vta:</strong> '.$datos_pdf["Articulo"]["precio"].' </td>
                  </tr>
                  <tr>
                    <td colspan="5"><strong>Stock M&iacute;nimo:</strong> '.$datos_pdf["Articulo"]["stock_minimo"].' </td>
                  </tr>
                </table>
<table width="100%" border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;font-family:\'Courier New\', Courier, monospace">

   
     <tbody>
        <tr>
         <td colspan="2"></td>
         <td colspan="2"></td>
         <td colspan="2"></td>
        </tr>
        <tr>
        <td  colspan="6" ><HR></td>
		</tr>
     </tbody>
</table>

<table width="100%"   cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">
   <tr style="background-color:rgb(224, 225, 229);">
   <th width="20%" scope="col">Codigo</th>
   <th width="40%" scope="col">Descripci&oacute;n</th>
   <th width="10%" scope="col">Stock Min</th>
    <th width="10%" scope="col">Cant</th>
    <th width="10%" scope="col">Costo U</th>
    <th width="10%" scope="col">TOTAL</th>
  </tr>
  <tbody style="font-size:10px;font-family:\'Courier New\', Courier, monospace">
    '.$tbody.'
  </tbody>  
</table>';

$pdf->writeHTML($html, true, false, true, false, true);


// QRCODE,L : QR-CODE Low error correction
// set style for barcode
$style = array(
    'border' => 2,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);


//$pdf->Text(20, 25, 'QRCODE L');
// ---------------------------------------------------------
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->SetBarcode(date("Y-m-d H:i:s", time()));
ob_clean();
$pdf->Output('Articulo_'.$id.'.pdf', 'D');
//============================================================+
// END OF FILE
//============================================================+
