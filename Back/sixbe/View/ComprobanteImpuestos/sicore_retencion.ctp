<?php

/*Retenciones de Ganancias*/
   
    
    
    if($nombre_archivo  == "")
        $nombre_archivo = "SicoreRetenciones";
        
    header("Content-Disposition: attachment; filename=".$nombre_archivo.".txt");
    
    
    
    header("Pragma: no-cache"); 
    header("Expires: 0"); 
    
    $comprobante_obj  = new Comprobante();
    $persona_obj  = new Persona();
    
    foreach($datos as $comprobante){
        
        
        
       $tipo_comprobante_afip_01 = str_pad($comprobante["Comprobante"]["TipoComprobante"]["codigo_afip"], 2, '0', STR_PAD_LEFT); 
       $fecha = new DateTime($comprobante["Comprobante"]['fecha_contable']);//va la fecha de la retencion
       $fecha_02 =$fecha->format('d/m/Y'); 
       $nro_comprobante_03 = str_pad($comprobante["Comprobante"]['nro_comprobante'], 16, '0', STR_PAD_LEFT);//numero de la retencion
       
       $importe_comprobante = $comprobante_obj->totalOrdenPago($comprobante);//calcula el total del pago
       $importe_comprobante_04 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($importe_comprobante,13,2,"0");
       $parte_entera = substr($importe_comprobante_04,0,13);
       $parte_decimal = substr($importe_comprobante_04,13,2);
       $importe_comprobante_04 = $parte_entera.','.$parte_decimal;
       
       
       $codigo_impuesto_05 = str_pad($comprobante["Impuesto"]["codigo_impuesto"], 3, '0', STR_PAD_LEFT);//codigo del impuesto
       $codigo_regimen_06 = str_pad($comprobante["Impuesto"]["codigo_regimen"], 3, '0', STR_PAD_LEFT);//codigo del Regimen
       
       
       if($comprobante["Impuesto"]["id_tipo_impuesto"] == EnumTipoImpuesto::RETENCIONES)
       	$codigo_operacion_07 = "1";
       else
       	$codigo_operacion_07 = "2";
       	
       	
       	
       	$base_calculo_08 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($comprobante["ComprobanteImpuesto"]["base_imponible"],12,2,"0");
       	$fecha_retencion_09 = $fecha_02;
       	$codigo_condicion_10 = "01";//responsable inscripto
       	$retencion_practicada_sujeto_suspendido_11 = "0";//ninguno
       	$importe_retencion_12 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($comprobante["ComprobanteImpuesto"]["importe_impuesto"],11,2,"0");
       	$parte_entera = substr($importe_retencion_12,0,11);
        $parte_decimal = substr($importe_retencion_12,11,2);
        $importe_retencion_12 = $parte_entera.','.$parte_decimal;
       	
       	$porcentaje_exclusion_13 = "000,00";
       	$fecha_exclusion_boletin_14 = "00/01/1900";
       	$codigo_doc_afip_15 = str_pad($comprobante["Comprobante"]["Persona"]["TipoDocumento"]["abreviacion_afip"], 2, '0', STR_PAD_LEFT);
       	$cuit_final_16 =  str_pad($comprobante["Comprobante"]["Persona"]["cuit"], 20, '0', STR_PAD_LEFT);
       	$nro_certificado_original_17 = str_pad($comprobante["ComprobanteReferencia"]['nro_comprobante'], 14, '0', STR_PAD_LEFT);//numero de la retencion
       	
       	$resto_18 = str_pad("", 53, '0', STR_PAD_LEFT);//completo con ceros NO IRIA
       	
       
       
       
       
       
       
       $linea_texto = $tipo_comprobante_afip_01.$fecha_02.$nro_comprobante_03.$importe_comprobante_04.$codigo_impuesto_05.$codigo_regimen_06.$codigo_operacion_07.$base_calculo_08.$fecha_retencion_09.$codigo_condicion_10.$retencion_practicada_sujeto_suspendido_11.$importe_retencion_12.$porcentaje_exclusion_13.$fecha_exclusion_boletin_14.$codigo_doc_afip_15.$cuit_final_16.$nro_certificado_original_17; 
       echo $linea_texto.  "\r\n";       
    }
        


?>