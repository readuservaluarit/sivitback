SELECT 
  comprobante_item.id_producto,
SUM(comprobante_item.cantidad) -  IFNULL( remitidos.cantidad,0) AS pendiente
FROM
  comprobante_item 
  JOIN comprobante 
    ON comprobante.id = comprobante_item.`id_comprobante` 
  LEFT JOIN 
    (SELECT 
      id_producto,
      SUM(cantidad) cantidad 
    FROM
      (
        (SELECT 
          /* SUMO LAS CANTIDADES DE ESE PRODUCTO QUE SE REMITIERON DEL CIRCUITO FACURA-REMITO*/
          comprobante_item.`id_producto`,
          SUM(cantidad) cantidad 
        FROM
          comprobante_item 
          JOIN comprobante 
            ON comprobante.id = comprobante_item.`id_comprobante` 
        WHERE id_tipo_comprobante = 203 
          AND comprobante_item.`activo` = 1 
          AND id_estado_comprobante = 118 
          /*  Remitido */
          AND id_comprobante_item_origen IN 
          (SELECT 
            comprobante_item.id 
          FROM
            comprobante_item 
            JOIN comprobante 
              ON comprobante.id = comprobante_item.`id_comprobante` 
            JOIN comprobante_item pedido_item 
              ON pedido_item.id = comprobante_item.id_comprobante_item_origen 
            JOIN comprobante pedido 
              ON pedido_item.`id_comprobante` = pedido.id 
          WHERE comprobante.id_tipo_comprobante IN (
              801,
              806,
              811,
              817,
              833,
              804,
              809,
              815,
              836,
              890,
              893,
              896
            ) 
            AND pedido.`id_tipo_comprobante` = 202 
            AND pedido_item.`activo` = 1 
            AND pedido.`id_estado_comprobante` = 1) 
        GROUP BY comprobante_item.`id_producto`) 
        UNION
        ALL 
        (SELECT 
          ci.id_producto,
          SUM(remito_item.cantidad) AS cantidad 
        FROM
          comprobante_item ci 
          JOIN comprobante 
            ON comprobante.id = ci.id_comprobante 
          LEFT JOIN comprobante_item remito_item 
            ON remito_item.`id_comprobante_item_origen` = ci.id 
          JOIN comprobante remito 
            ON remito.id = remito_item.`id_comprobante` 
            AND remito.`id_tipo_comprobante` = 203 
        WHERE comprobante.`id_estado_comprobante` = 1 
          AND comprobante.`id_tipo_comprobante` = 202 
          AND remito_item.`activo` = 1 
          AND remito.`id_estado_comprobante` <> 2 
          AND ci.activo = 1 
        GROUP BY ci.id_producto 
        ORDER BY id_producto ASC)
      ) t 
    GROUP BY id_producto) remitidos 
    ON remitidos.id_producto = comprobante_item.`id_producto` 
WHERE id_tipo_comprobante = 202 
  AND id_estado_comprobante = 1 
  AND comprobante_item.`activo` = 1 
GROUP BY comprobante_item.id_producto 