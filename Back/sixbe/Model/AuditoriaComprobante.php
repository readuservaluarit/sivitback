<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class AuditoriaComprobante extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'auditoria_comprobante';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
    
       /*
		'd_categoria' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar un Nombre.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
            'unico' => array(
            'rule' => array('isUnique'),
            'message' => 'La Categoria ya existe debe escribir una que no exista'
        )
		),
        
        */
        
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
		
		
		
		$this->virtual_fields_view = array(
				
				"pto_nro_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_estado"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_usuario"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				
				
		);
		
	}
	
	
	
	public $belongsTo = array(
			'Comprobante' => array(
					'className' => 'Comprobante',
					'foreignKey' => 'id_comprobante',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'Usuario' => array(
					'className' => 'Usuario',
					'foreignKey' => 'id_usuario',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'EstadoComprobanteNuevo' => array(
					'className' => 'EstadoComprobante',
					'foreignKey' => 'id_estado_comprobante_nuevo',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'PuntoVenta' => array(
					'className' => 'PuntoVenta',
					'foreignKey' => 'id_punto_venta',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			)
			
	);
/**
 * hasMany associations
 *
 * @var array
 */
	/*public $hasMany = array(
		'Region' => array(
			'className' => 'Region',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public $childRecordMessage = "Existe por lo menos una regi&oacute;n, predio, establecimiento o usuario relacionado/a con la jurisdicci&oacute;n.";
    
    */
}
