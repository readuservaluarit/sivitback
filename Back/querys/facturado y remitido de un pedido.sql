SELECT @select:=comprobante_item.id,cantidad,
IFNULL((SELECT SUM(cantidad) FROM comprobante_item 

JOIN comprobante ON comprobante_item.`id_comprobante` = comprobante.`id`
WHERE id_comprobante_item_origen=@select

AND comprobante.`id_tipo_comprobante` IN (801,802,817)
AND comprobante_item.`activo` = 1
),0) AS facturado,


IFNULL((SELECT SUM(cantidad) FROM comprobante_item 

JOIN comprobante ON comprobante_item.`id_comprobante` = comprobante.`id`
WHERE id_comprobante_item_origen=@select

AND comprobante.`id_tipo_comprobante` IN (203)
AND comprobante_item.`activo` = 1
),0) AS remitido

FROM comprobante


JOIN tipo_comprobante ON tipo_comprobante.id = comprobante.`id_tipo_comprobante`
LEFT JOIN comprobante_item ON comprobante_item.`id_comprobante` = comprobante.`id`


WHERE YEAR(comprobante.fecha_generacion) IN (2018) AND
comprobante_item.activo=1

AND id_estado_comprobante=1
AND
id_tipo_comprobante IN (202)

LIMIT 100000







