<?php

    
    class TiposCarteraRendicionController extends AppController {
        public $name = 'TiposCarteraRendicion';
        public $model = 'TipoCarteraRendicion';
        public $helpers = array ('Session', 'Paginator', 'Js');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

        
        
        /**
    * @secured(CONSULTA_TIPO_CARTERA_RENDICION)
    */
        public function index() {

            if($this->request->is('ajax'))
                $this->layout = 'ajax';

            $this->loadModel($this->model);
            $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);

            $conditions = array(); 

            $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
                'conditions' => $conditions,
                'limit' => 10,
                'page' => $this->getPageNum()
            );

            if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de TipoCarteraRendicionCausas', 'Datos de los Tipo de Impuesto', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));

                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_TipoCarteraRendicion', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();

                //Headers
                $formBuilder->addHeader('Id', 'TipoCarteraRendicion.id', "10%");
                $formBuilder->addHeader('Nombre', 'TipoCarteraRendicion.d_TipoCarteraRendicion_causa', "50%");
                $formBuilder->addHeader('Nombre', 'TipoCarteraRendicion.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_TipoCarteraRendicionCausa');
                $formBuilder->addField($this->model, 'cotizacion');

                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
            }
            else
            { // vista json
                $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
                $page_count = $this->params['paging'][$this->model]['pageCount'];

               
                $this->data = $data;
                $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "list",
                    "content" => $data,
                    "page_count" =>$page_count
                );
                $this->set($output);
                $this->set("_serialize", array("status", "message","page_count", "content"));

            }
        }


        public function abm($mode, $id = null) {


            if ($mode != "A" && $mode != "M" && $mode != "C")
                throw new MethodNotAllowedException();

            $this->layout = 'ajax';
            $this->loadModel($this->model);
            if ($mode != "A"){
                $this->TipoCarteraRendicion->id = $id;
                $this->request->data = $this->TipoCarteraRendicion->read();
            }


            App::import('Lib', 'FormBuilder');
            $formBuilder = new FormBuilder();

            //Configuracion del Formulario
            $formBuilder->setController($this);
            $formBuilder->setModelName($this->model);
            $formBuilder->setControllerName($this->name);
            $formBuilder->setTitulo('TipoCarteraRendicionCausa');

            if ($mode == "A")
                $formBuilder->setTituloForm('Alta de TipoCarteraRendicionCausa');
            elseif ($mode == "M")
                $formBuilder->setTituloForm('Modificaci&oacute;n de TipoCarteraRendicionCausa');
            else
                $formBuilder->setTituloForm('Consulta de TipoCarteraRendicionCausa');

            //Form
            $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

            //Fields





            $formBuilder->addFormInput('d_TipoCarteraRendicionCausa', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
            $formBuilder->addFormInput('cotizacion', 'Cotizaci&oacute;n', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));    


            $script = "
            function validateForm(){

            Validator.clearValidationMsgs('validationMsg_');

            var form = 'TipoCarteraRendicionCausaAbmForm';

            var validator = new Validator(form);

            validator.validateRequired('TipoCarteraRendicionCausaDTipoCarteraRendicionCausa', 'Debe ingresar un nombre');
            validator.validateRequired('TipoCarteraRendicionCausaCotizacion', 'Debe ingresar una cotizaci&oacute;n');


            if(!validator.isAllValid()){
            validator.showValidations('', 'validationMsg_');
            return false;   
            }

            return true;

            } 


            ";

            $formBuilder->addFormCustomScript($script);

            $formBuilder->setFormValidationFunction('validateForm()');

            $this->set('abm',$formBuilder);
            $this->set('mode', $mode);
            $this->set('id', $id);
            $this->render('/FormBuilder/abm');    
        }



        /**
        * @secured(ADD_TIPO_CARTERA_RENDICION)
        */
        public function add() {
            if ($this->request->is('post')){
                $this->loadModel($this->model);
                $id_TIPO_CARTERA_RENDICION = '';

                try{
                    if ($this->TipoCarteraRendicion->saveAll($this->request->data, array('deep' => true))){
                        $mensaje = "La cartera de rendicion ha sido creada exitosamente";
                        $tipo = EnumError::SUCCESS;
                        $id_TIPO_CARTERA_RENDICION = $this->{$this->model}->id;

                    }else{
                        $errores = $this->{$this->model}->validationErrors;
                        $errores_string = "";
                        foreach ($errores as $error){
                            $errores_string.= "&bull; ".$error[0]."\n";

                        }
                        $mensaje = $errores_string;
                        $tipo = EnumError::ERROR; 

                    }
                }catch(Exception $e){

                    $mensaje = "Ha ocurrido un error,el Tipo de Asiento no ha podido ser creada.".$e->getMessage();
                    $tipo = EnumError::ERROR;
                }
                $output = array(
                    "status" => $tipo,
                    "message" => $mensaje,
                    "content" => "",
                    "id_add"=>$id_TIPO_CARTERA_RENDICION
                );
                //si es json muestro esto
                if($this->RequestHandler->ext == 'json'){ 
                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content","id_add"));
                }else{

                    $this->Session->setFlash($mensaje, $tipo);
                    $this->redirect(array('action' => 'index'));
                }     
            }

            //si no es un post y no es json
            if($this->RequestHandler->ext != 'json')
                $this->redirect(array('action' => 'abm', 'A'));   
        }

        /**
        * @secured(MODIFICACION_TIPO_CARTERA_RENDICION)
        */
        public function edit($id) {


            if (!$this->request->is('get')){

                $this->loadModel($this->model);
                $this->{$this->model}->id = $id;

                try{ 
                    if ($this->{$this->model}->saveAll($this->request->data)){
                        $mensaje =  "El Tipo de Asiento ha sido modificado exitosamente";
                        $status = EnumError::SUCCESS;


                    }else{   //si hubo error recupero los errores de los models

                        $errores = $this->{$this->model}->validationErrors;
                        $errores_string = "";
                        foreach ($errores as $error){ //recorro los errores y armo el mensaje
                            $errores_string.= "&bull; ".$error[0]."\n";

                        }
                        $mensaje = $errores_string;
                        $status = EnumError::ERROR; 
                    }

                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 

                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));

                    } 
                }catch(Exception $e){
                    $this->Session->setFlash('Ha ocurrido un error, el Tipo Asiento no ha podido modificarse.', 'error');

                }

            }else{ //si me pide algun dato me debe mandar el id
                if($this->RequestHandler->ext == 'json'){ 

                    $this->{$this->model}->id = $id;
                    //$this->{$this->model}->contain('Provincia','Pais');
                    $this->request->data = $this->{$this->model}->read();          

                    $output = array(
                        "status" =>EnumError::SUCCESS,
                        "message" => "list",
                        "content" => $this->request->data
                    );   

                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content")); 
                }else{

                    $this->redirect(array('action' => 'abm', 'M', $id));

                }


            } 

            if($this->RequestHandler->ext != 'json')
                $this->redirect(array('action' => 'abm', 'M', $id));
        }

        /**
        * @secured(BAJA_TIPO_CARTERA_RENDICION)
        */
        function delete($id) {
            $this->loadModel($this->model);
            $mensaje = "";
            $status = "";
            $this->TipoCarteraRendicion->id = $id;

            try{
                if ($this->TipoCarteraRendicion->delete() ) {

                    //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');

                    $status = EnumError::SUCCESS;
                    $mensaje = "El Tipo de Asiento ha sido eliminado exitosamente.";
                    $output = array(
                        "status" => $status,
                        "message" => $mensaje,
                        "content" => ""
                    ); 

                }

                else
                    throw new Exception();

            }catch(Exception $ex){ 
                //$this->Session->setFlash($ex->getMessage(), 'error');

                $status = EnumError::ERROR;
                $mensaje = $ex->getMessage();
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
            }

            if($this->RequestHandler->ext == 'json'){
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));

            }else{
                $this->Session->setFlash($mensaje, $status);
                $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            }
        }

        /**
        * @secured(CONSULTA_TIPO_CARTERA_RENDICION)
        */
        public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }

        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);

        }

    }
?>