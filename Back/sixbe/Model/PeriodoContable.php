<?php
App::uses('AppModel', 'Model');

class PeriodoContable extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'periodo_contable';
    
    
    public $actsAs = array('Containable');
    
    
    
    public $validate = array(
        
        
    );
    
    


	//The Associations below have been created with all possible keys, those that are not needed can be removed

    
    public $belongsTo = array(
        'EjercicioContable' => array(
            'className' => 'EjercicioContable',
            'foreignKey' => 'id_ejercicio_contable',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'AsientoUltimo' => array(
            'className' => 'Asiento',
            'foreignKey' => 'id_asiento_ultimo',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
        
      
       
      
     
     
       
       
       
       
      
 );
 
 
 
 public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     "es_actual"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_ejercicio_contable"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "actual"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "desc_periodo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "id_ultimo_asiento"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     "cerrado"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_disponible"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_cerrado"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
  
     
     );
     
     }   
 
 
 public function getEstadoGrabado($id){
    
    $periodo = $this->find('first', array(
                                                'conditions' => array('PeriodoContable.id' => $id),
                                                'contain' =>false
                                                )); 
        
    
   return $periodo["PeriodoContable"]["cerrado"]; 
}



public function getPeriodoActual(){
    
    $ejercicio_contable_class = ClassRegistry::init('EjercicioContable');
    
    
    $fecha = date("Y-m-d");
    
    $ejercicio_contable = $ejercicio_contable_class->find('first', array(
                                                'conditions' => array('EjercicioContable.activo' => 1,
                                                					'EjercicioContable.fecha_apertura <='=>$fecha,
                                                					'EjercicioContable.fecha_cierre >='=>$fecha
                                                
                                                ),
                                                'contain' =>false
                                                )); //selecciono el Ejercicio Activo
                                                
    
    if($ejercicio_contable){ 
    $id_ejercicio_contable = $ejercicio_contable["EjercicioContable"]["id"];                                            
                                                
     
    
                                                    
    
    $periodo = $this->find('first', array(
    		'conditions' => array('PeriodoContable.fecha_desde <=' => $fecha,'PeriodoContable.fecha_hasta >=' => $fecha,"PeriodoContable.id_ejercicio_contable"=>$id_ejercicio_contable,"PeriodoContable.cerrado"=>0,'PeriodoContable.disponible'=>1),
                                                'contain' =>false,
                                                'order'=>'PeriodoContable.id DESC'
                                                )); 
        
    
   if($periodo && count($periodo)>0)
    return $periodo["PeriodoContable"];
   else
    return 0; 
    
    }else{
        
        return 0;
    }
}


public function getFechaInicio($id_periodo_contable){
      
        $periodo_contable = $this->find('first', array(
                                                'conditions' => array('PeriodoContable.id' =>$id_periodo_contable )
                                                ));
                                                
       return $periodo_contable["PeriodoContable"]["fecha_desde"];
    
} 


public function getFechaFin($id_periodo_contable){
      
        $periodo_contable = $this->find('first', array(
                                                'conditions' => array('PeriodoContable.id' =>$id_periodo_contable )
                                                ));
                                                
       return $periodo_contable["PeriodoContable"]["fecha_hasta"];
    
} 


public function FechaEstaEnPeriodoAbierto($fecha,$id=0,$id_ejercicio=0){
    //TODO: refactor - evaluart impacto y Renombrar como el metodo de abajo FechaEstaEnPeriodoAbiertoYDisponible
    $conditions = array();
    
    array_push($conditions,array("PeriodoContable.fecha_desde <=" => $fecha));
    array_push($conditions,array("PeriodoContable.fecha_hasta >=" => $fecha));
    array_push($conditions,array("PeriodoContable.disponible >=" => 1));
    array_push($conditions,array("PeriodoContable.cerrado >=" => 0));
    
    if($id_ejercicio !=0)
    array_push($conditions,array("PeriodoContable.id_ejercicio_contable" => $id_ejercicio));
    
    if($id !=0)
         array_push($conditions,array("PeriodoContable.id <>" => $id));
    
    
      $periodo_contable = $this->find('count', array(
                                                'conditions' => $conditions
                                                ));
                                                
      return $periodo_contable;
    
    
    
    
} 

public function FechaEstaEnPeriodoAbiertoYDisponible($fecha,$id=0,$id_ejercicio=0){
    
    $conditions = array();
    
    array_push($conditions,array("PeriodoContable.fecha_desde <=" => $fecha));
    array_push($conditions,array("PeriodoContable.fecha_hasta >=" => $fecha)); //MP: ANTES ERA MAYOR e igual. Se superponian las fechas del periodo.  un comprobante sde una fecha puede ser del periodo 1  y otro comprobante de la misma fecha puede ser del periodo 2 con esto bloquemos el periodo 1
    array_push($conditions,array("PeriodoContable.disponible >=" => 1)); //DISPONIBLE
    array_push($conditions,array("PeriodoContable.cerrado >=" => 0)); //ABIERTO
    
    if($id_ejercicio !=0)
    array_push($conditions,array("PeriodoContable.id_ejercicio_contable" => $id_ejercicio));
    
    if($id !=0)
         array_push($conditions,array("PeriodoContable.id <>" => $id));
    
    
      $periodo_contable = $this->find('count', array(
                                                'conditions' => $conditions
                                                ));
                                                
      return $periodo_contable;
    
    
    
    
} 



public function GetPeriodoPorFecha($fecha,$id=0,$id_ejercicio){
    
    $conditions = array();
    
    array_push($conditions,array("PeriodoContable.fecha_desde <=" => $fecha));
    array_push($conditions,array("PeriodoContable.fecha_hasta >=" => $fecha));
    array_push($conditions,array("PeriodoContable.id_ejercicio_contable" => $id_ejercicio));
    
    if($id !=0)
         array_push($conditions,array("PeriodoContable.id <>" => $id));
    
    
      $periodo_contable = $this->find('first', array(
                                                'conditions' => $conditions
                                            
                                                ));
                                                
      if($periodo_contable)
        return $periodo_contable["PeriodoContable"];
      else
        return 0; 
    
    
    
    
}  


public function getPeriodo($id_periodo_contable){
      
        $periodo_contable = $this->find('first', array(
                                                'conditions' => array('PeriodoContable.id' =>$id_periodo_contable )
                                                ));
                                                
       return $periodo_contable["PeriodoContable"];
    
}    



public function getEjercicioContableFromPeriodo($id_periodo_contable){
	
	$periodo_contable = $this->find('first', array(
			'conditions' => array('PeriodoContable.id' =>$id_periodo_contable )
	));
	
	return $periodo_contable["EjercicioContable"];
	
} 
      
      
    

    
    
 

  
 
  




}
