/*PASO 1*/

DELETE FROM Hoja1;


/*PASO 2*/
INSERT INTO Hoja1(cantidad,id)
SELECT 
  SUM(ci.cantidad)-  IFNULL(
    (SELECT /* SUMO LAS CANTIDADES DE ESE PRODUCTO QUE SE FACTURARON*/
      SUM(cantidad) AS cantidad 
    FROM
      comprobante_item 
		WHERE id_comprobante IN 
				      (SELECT 
					id 
				      FROM
					comprobante 
				      WHERE id_tipo_comprobante IN (801, 806) 
					AND (
					  id_estado_comprobante = 115 
					  OR id_estado_comprobante = 116
					)) 
		AND id_producto = ci.`id_producto`
		AND id_comprobante_item_origen IS NOT NULL
      
      
      ),
    0
  ) AS pendiente,
  ci.id_producto 
FROM
  comprobante_item ci 
WHERE ci.id_comprobante IN 
  (SELECT 
    id 
  FROM
    comprobante 
  WHERE id_tipo_comprobante = 202 
    AND id_estado_comprobante<>2) 
  AND ci.activo = 1 
GROUP BY ci.id_producto 
ORDER BY id_producto ASC 

/*PASO 3*/
UPDATE  stock_producto
SET stock = 0
WHERE stock_producto.id_deposito=12


/*PASO 4*/
UPDATE  stock_producto

JOIN Hoja1 ON Hoja1.`id` = stock_producto.`id`
SET stock = Hoja1.cantidad
WHERE stock_producto.id_deposito=12 AND Hoja1.cantidad>=0



