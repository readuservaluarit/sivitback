<?php   // CONFIG  CON MODE DISPLAYED CELLS
         App::uses('Cheque', 'Model');  
        $cheque_obj = new Cheque();
        $cheque_obj->setVirtualFieldsForView();
        $cheque_obj->SetFieldsToView($cheque_obj->virtual_fields_view,$model);
        
        
        
        $model["id"]["show_grid"] = 1;
        $model["id"]["header_display"] = "Id";
        $model["id"]["width"] = "18"; //Restriccion mas de esto no se puede achicar
        $model["id"]["order"] = "0";
        $model["id"]["text_colour"] = "#000000";
        
        
        
        $model["d_tipo_cheque"]["show_grid"] = 1;
        $model["d_tipo_cheque"]["header_display"] = "Tipo";
        $model["d_tipo_cheque"]["width"] = "10"; //REstriccion mas de esto no se puede achicar
        $model["d_tipo_cheque"]["order"] = "1";
        $model["d_tipo_cheque"]["text_colour"] = "#BB0000";
        
        $model["d_banco"]["show_grid"] = 1;
        $model["d_banco"]["header_display"] = "Banco";
        $model["d_banco"]["width"] = "7"; //REstriccion mas de esto no se puede achicar
        $model["d_banco"]["order"] = "2";
        $model["d_banco"]["text_colour"] = "#0000EE";
        
        $model["d_banco_sucursal"]["show_grid"] = 1;
        $model["d_banco_sucursal"]["header_display"] = "Sucursal";
        $model["d_banco_sucursal"]["width"] = "13"; //REstriccion mas de esto no se puede achicar
        $model["d_banco_sucursal"]["order"] = "3";
        $model["d_banco_sucursal"]["text_colour"] = "#0000EE";
        
        $model["nro_cheque"]["show_grid"] = 1;
        $model["nro_cheque"]["header_display"] = "Nro. Cheque";
        $model["nro_cheque"]["width"] = "15"; //REstriccion mas de esto no se puede achicar
        $model["nro_cheque"]["order"] = "4";
        $model["nro_cheque"]["text_colour"] = "#EE0000";
		
        $model["monto"]["show_grid"] = 1;
        $model["monto"]["header_display"] = "Monto";                         
        $model["monto"]["width"] = "12"; //REstriccion mas de esto no se puede achicar
        $model["monto"]["order"] = "5";
        $model["monto"]["text_colour"] = "#006600";
		
		$model["d_moneda"]["show_grid"] = 1;
        $model["d_moneda"]["header_display"] = "Moneda";                         
        $model["d_moneda"]["width"] = "5"; //REstriccion mas de esto no se puede achicar
        $model["d_moneda"]["order"] = "6";
        $model["d_moneda"]["text_colour"] = "#004400";
        
        $model["cruzado"]["show_grid"] = 1;
        $model["cruzado"]["header_display"] = "Cruzado";                         
        $model["cruzado"]["width"] = "8"; //REstriccion mas de esto no se puede achicar
        $model["cruzado"]["order"] = "7";
        $model["cruzado"]["text_colour"] = "#000000";
        
        $model["no_a_la_orden"]["show_grid"] = 1;
        $model["no_a_la_orden"]["header_display"] = "No a la Orden";                         
        $model["no_a_la_orden"]["width"] = "8"; //REstriccion mas de esto no se puede achicar
        $model["no_a_la_orden"]["order"] = "8";
        $model["no_a_la_orden"]["text_colour"] = "#000000";

        $model["fecha_cheque"]["show_grid"] = 1;
        $model["fecha_cheque"]["header_display"] = "Fcha. Cheque";                         
        $model["fecha_cheque"]["width"] = "10"; //REstriccion mas de esto no se puede achicar
        $model["fecha_cheque"]["order"] = "9";
        $model["fecha_cheque"]["text_colour"] = "#330066";
        
        $model["fecha_emision"]["show_grid"] = 1;
        $model["fecha_emision"]["header_display"] = "Fcha. Emisi&oacute;n";                         
        $model["fecha_emision"]["width"] = "10"; //REstriccion mas de esto no se puede achicar
        $model["fecha_emision"]["order"] = "10";
        $model["fecha_emision"]["text_colour"] = "#330066";
		
		$model["d_estado_cheque"]["show_grid"] = 1;
        $model["d_estado_cheque"]["header_display"] = "Estado";
        $model["d_estado_cheque"]["width"] = "8"; //REstriccion mas de esto no se puede achicar
        $model["d_estado_cheque"]["order"] = "11";
        $model["d_estado_cheque"]["text_colour"] = "#BB0022";
   
        $model["d_comprobante_entrada"]["show_grid"] = 1;
        $model["d_comprobante_entrada"]["header_display"] = "Comprobante Entrada";                         
        $model["d_comprobante_entrada"]["width"] = "11"; //REstriccion mas de esto no se puede achicar
        $model["d_comprobante_entrada"]["order"] = "12";
        $model["d_comprobante_entrada"]["text_colour"] = "#000000";
        
        
        $model["razon_social_comprobante_entrada"]["show_grid"] = 1;
        $model["razon_social_comprobante_entrada"]["header_display"] = "Rzon Social Entrada/Desc.";                         
        $model["razon_social_comprobante_entrada"]["width"] = "12"; //REstriccion mas de esto no se puede achicar
        $model["razon_social_comprobante_entrada"]["order"] = "13";
        $model["razon_social_comprobante_entrada"]["text_colour"] = "#000000";
        
        
        $model["titular_razon_social"]["show_grid"] = 1;
        $model["titular_razon_social"]["header_display"] = "Titular.";                         
        $model["titular_razon_social"]["width"] = "12"; //REstriccion mas de esto no se puede achicar
        $model["titular_razon_social"]["order"] = "14";
        $model["titular_razon_social"]["text_colour"] = "#000000";
        
        
        $model["titular_cuit"]["show_grid"] = 1;
        $model["titular_cuit"]["header_display"] = "Cuit Titular.";                         
        $model["titular_cuit"]["width"] = "12"; //REstriccion mas de esto no se puede achicar
        $model["titular_cuit"]["order"] = "15";
        $model["titular_cuit"]["text_colour"] = "#000000";
        
        $model["d_comprobante_salida"]["show_grid"] = 1;
        $model["d_comprobante_salida"]["header_display"] = "Comprobante Salida";                         
        $model["d_comprobante_salida"]["width"] = "12"; //REstriccion mas de esto no se puede achicar
        $model["d_comprobante_salida"]["order"] = "16";
        $model["d_comprobante_salida"]["text_colour"] = "#000000";
        
        
        $model["razon_social_comprobante_salida"]["show_grid"] = 1;
        $model["razon_social_comprobante_salida"]["header_display"] = "Rzon Social Salida/Desc.";                         
        $model["razon_social_comprobante_salida"]["width"] = "12"; //REstriccion mas de esto no se puede achicar
        $model["razon_social_comprobante_salida"]["order"] = "17";
        $model["razon_social_comprobante_salida"]["text_colour"] = "#000000";
        
        $model["d_comprobante_rechazo"]["show_grid"] = 1;
        $model["d_comprobante_rechazo"]["header_display"] = "Comprobante Rechazo";                         
        $model["d_comprobante_rechazo"]["width"] = "12"; //REstriccion mas de esto no se puede achicar
        $model["d_comprobante_rechazo"]["order"] = "18";
        $model["d_comprobante_rechazo"]["text_colour"] = "#000000";
        
        $model["conciliado"]["show_grid"] = 1;
        $model["conciliado"]["header_display"] = "Conciliado";
        $model["conciliado"]["width"] = "8"; //REstriccion mas de esto no se puede achicar
        $model["conciliado"]["order"] = "19";
        $model["conciliado"]["text_colour"] = "#000000";
?>