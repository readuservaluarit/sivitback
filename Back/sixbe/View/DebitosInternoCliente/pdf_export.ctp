<?php
/**
 * PDF exportación de Cotizacion
 */
App::import('Vendor','tcpdf/tcpdf'); 

// create new PDF document
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
/*

------------------------------------------------------JOSE ESTA ES LA VERSION QUE ESTABA TRABAJANDO
         public $id;
     public $name;
	 public $datosPDF
     
     public function setid($id){
        $this->id = $id;
     }
     
     public function getid(){
        return $this->id;
     }
     
     public function setName($name){
        $this->name = $name;
     }
     
     public function getName(){
        return $this->name;
     }
	 
	 public function setDatosPDF($DatosPDF){
        $this->datosPDF = $DatosPDF;
     }
	 
	 public function getDatosPDF(){
        return $this->datosPDF;
     }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
		//$odato = $this->getDatosPDF();
        //$this->Cell(0, 10, $this->getName().' Nro '.$odato["TipoComprobante"]["impresion_footer_left"].$odato["TipoComprobante"]["impresion_footer_right"]'                                                    Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		//$this->Cell(0, 10, 'F23- '.$this->getName().' Nro '.$this->getid().'                     Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages() .'           Rev. 07- Abril 2016 ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->setid($datos_pdf["Comprobante"]["nro_comprobante_completo"]);
$pdf->setName($datos_pdf["TipoComprobante"]["d_tipo_comprobante"]);
$pdf->setDatosPDF($datos_pdf);



*/
// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF 
{
     public $id;
     public $name;
     
     public function setid($id){
        $this->id = $id;
     }
     
     public function getid(){
        return $this->id;
     }
     
     public function setName($name){
        $this->name = $name;
     }
     
     public function getName(){
        return $this->name;
     }


    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number

		
		$this->Cell(0, 10, 'F23 - '.$this->getName().' Nro '.$this->getid().'                                           Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages() .'                                               Rev. 07- Abril 2016 ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->setid($datos_pdf["Comprobante"]["nro_comprobante_completo"]);
$pdf->setName($datos_pdf["TipoComprobante"]["d_tipo_comprobante"]);

$fecha_entrega = new DateTime($datos_pdf['Comprobante']['fecha_entrega']);
$fecha_generacion = new DateTime($datos_pdf['Comprobante']['fecha_generacion']);

ob_clean();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sivit by Valuarit');
$pdf->SetTitle('Comprobante');
$pdf->SetSubject('Comprobante');
//$pdf->SetKeywords('');



// set default header data
$logo = 'header_factura.png';
$title = "";
$subtitle = ""; 
$pdf->SetHeaderData($logo, 57, $title, $subtitle, array(0,0,0), array(0,104,128));
$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 20, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
/*$lg = Array();
$lg['a_meta_charset'] = 'ISO-8859-1';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';
*/
$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set default font subsetting mode
//$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
/*
$html = <<<EOD
<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
EOD;
*/

$tbody = "";

$renglones = $datos_pdf["ComprobanteItem"];

$data = array();


$comprobante  =  new Comprobante;
$comprobante_item  =  new ComprobanteItem;

$suma_descuentos_individuales = $comprobante->getSumaDescuentosIndividuales($renglones,0);

foreach($renglones as $renglon){

  
   
    $descuento = ($renglon['precio_unitario_bruto']-$renglon['precio_unitario'])* $renglon['cantidad'];
    $precio_subtotal = ($renglon['precio_unitario']) * $renglon['cantidad']*$datos_pdf["Comprobante"]["valor_moneda"];
   
   
   $tbody .= "<tr>";
   $tbody .= "<td>" .$renglon['n_item'] . "</td>";
   $tbody .= "<td>" .$comprobante_item->getCodigoFromProducto($renglon). "</td>";
   $tbody .= "<td>" .$renglon['item_observacion'] . "</td>";    
   $tbody .= "<td>" .$comprobante_item->getFechaEntrega(date("Y-m-d", strtotime($datos_pdf["Comprobante"]["fecha_generacion"])), $renglon['dias'],$datos_pdf["Comprobante"]["fecha_entrega"]). "</td>";    
   $tbody .= "<td>" .money_format('%!n',round($renglon['cantidad'])). "</td>";
   if($renglon['precio_unitario'] == $renglon['precio_unitario_bruto'])
        $tbody .= "<td>" .$renglon['precio_unitario']. "</td>";
   else
        $tbody .= "<td>" .money_format('%!n', $renglon['precio_unitario_bruto']). "</td>";
   $tbody .= "<td>" .money_format('%!n',$renglon['descuento_unitario']). "</td>";
   $tbody .= "<td>" .money_format('%!n', $precio_subtotal)."</td>";
   $tbody .= "</tr>";
}
 
//agrego las observaciones
$aviso_anulado = '';
if($datos_pdf['Comprobante']['id_estado_comprobante'] == EnumEstadoComprobante::Anulado)
    $aviso_anulado = ' ANULADO';
    
    
$html='';


$html .=' 
<table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">
 <tr>
        <th class="sorting" rowspan="1" colspan="1" style="width:50%;font-size:14px;"> '.$datos_pdf["TipoComprobante"]["codigo_tipo_comprobante"].$aviso_anulado.' NRO '.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'</th>
        <th  class="sorting" rowspan="1" colspan="1"  style="width:50%;font-size:14px;"> Fecha Emisi&oacute;n: '.$fecha_generacion->format('d-m-Y').' </th>
 </tr>
     <tbody>
     </tbody>
</table>

                  <table width="100%" border="1" style="width:100%;font-size:10px;">
                  <tr  style="background-color:rgb(224, 225, 229);">
                    <td colspan="4"><div align="center">Cliente</div></td>
                  </tr>
                  <tr>
                    <td width="17%">Codigo: '.$datos_pdf["Persona"]["id"].'</td>
                    <td width="35%">Raz&oacute;n Social: '.$datos_pdf["Persona"]["razon_social"].'</td>
                    <td width="26%">Cuit: '.$datos_pdf["Persona"]["cuit"].' </td>
                    <td width="22%">O/C: '.$datos_pdf["Comprobante"]["orden_compra_externa"].'</td>
                  </tr>
                  <tr>
                    <td colspan="3">Direccion: '.$datos_pdf["Persona"]["calle"].' - '.$datos_pdf["Persona"]["numero_calle"].' - '.$datos_pdf["Persona"]["localidad"].' - '.$datos_pdf["Persona"]["ciudad"].' - '.$datos_pdf["Persona"]["Provincia"]["d_provincia"].' - '.$datos_pdf["Persona"]["Pais"]["d_pais"].'  </td>
                    <td>Telefono: '.$datos_pdf["Persona"]["tel"].'</td>
                  </tr>
                </table>
                
                
                
                
                
                <table width="100%" border="1" style="width:100%;font-size:10px;">
                  <tr  style="background-color:rgb(224, 225, 229);">
                    <td colspan="3"><div align="center">Entrega</div></td>
                  </tr>
                  <tr>
                    <td width="50%">Fecha Entrega global: '.$fecha_entrega->format('d-m-Y').'</td>
                
                  
                    <td width="50%">Lugar: '.$datos_pdf["Comprobante"]["lugar_entrega"].'</td>
              
                  </tr>
                </table>
                

                <table width="100%" border="1" style="width:100%;font-size:10px;">
                  <tr  style="background-color:rgb(224, 225, 229);">
                    <td colspan="2"><div align="center">Calidad</div></td>
                  </tr>
                  <tr>
                    <td width="50%">Inspeccion: '.$datos_pdf["Comprobante"]["inspeccion"].'</td>
                  
                    <td width="50%">Certificados: '.$datos_pdf["Comprobante"]["certificados"].'</td>
              
                  </tr>
                </table>


<table width="100%" border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
     
        <tr >
        <td colspan="2"> </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
 
        <tr >
        <td  colspan="6" ><HR></td>
      
         </tr>
     </tbody>
</table>




<table width="100%" border="1px" cellspacing="1" cellpadding="2" style="width:100%;font-size:11px;">

    <tr style="background-color:rgb(224, 225, 229);">
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;">Item</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:15%;">C&oacute;digo</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:40%;">Descripci&oacute;n</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">Entrega</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;">Cant.</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">P.U</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;">Desc(%)</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">Subt</th>
    </tr>

  <tbody>
    '.$tbody.'
  </tbody>  
</table>';

   $html .= '<table border="0px" cellspacing="1" cellpadding="20" style="width:100%;font-size:12px;">';
   $html .= "<tr>";
   $html .= "<td colspan='8'>Observaciones:</td>";
   $html .= "<td colspan='8'>".nl2br($datos_pdf["Comprobante"]["observacion"]). " ".nl2br($datos_pdf["TipoComprobante"]["impresion_observacion_extra"])."</td>";
   $html .= "</tr>";
   $html .= "</tr>";
   $html .= "<tr>";
   $html .= "<td colspan='8'>CONDICIONES COMERCIALES</td>";
   $html .= "</tr>";
   $html .= "</table>";
   
   $html .= "<HR>";
   
   $html .= '<table border="0px" cellspacing="1" cellpadding="1" style="width:100%;font-size:12px;">';
   $html .= "<tr>";
   $html .= "<td >Moneda: ".$datos_pdf['Moneda']['d_moneda']."</td>";
   $html .= "<td >Forma de Pago: ".$datos_pdf['CondicionPago']['d_condicion_pago']."</td>";
   $html .= "<td >Plazo de Entrega Global: ".$fecha_entrega->format('d-m-Y')."</td>";
   $html .= "</tr>";
   $html .= "<tr >";
   $html .= "<td colspan='3' >Lugar de Entrega: ".$datos_pdf['Comprobante']['lugar_entrega']."</td>";
   $html .= "</tr>";
   
   
     if($suma_descuentos_individuales>0){
          $html .= "<tr>";
          $html .= "<td colspan='3' >Suma Desc. Unitarios: ".$datos_pdf['Moneda']['simbolo']." ".money_format('%!n',round($suma_descuentos_individuales,2))." </td>"; 
          $html .= "</tr>";
   }
    
    
    
   $html .= "<tr>";
   $html .= "<td >Monto: ".$datos_pdf['Moneda']['simbolo']." ".money_format('%!n',$datos_pdf['Comprobante']['subtotal_bruto'])."</td>";
  
  
       $html .= "<td >Descuento Global: ".money_format('%!n',round($datos_pdf['Comprobante']['descuento'],2))."%</td>";
       $html .= "<td >Monto Descuento Global: ".$datos_pdf['Moneda']['simbolo']." ".money_format('%!n',round($datos_pdf['Comprobante']['importe_descuento'],2))." </td>";
      

    $html .= "</tr>";

   //$html .= "<tr>";
   //$html .= "<td >Bonificacion: ".$datos_pdf['Moneda']['simbolo']."  ".$datos_pdf['Comprobante']['importe_bonificacion_gravado']."</td>";
   //$html .= "</tr>";
   $html .= "<tr>";
   $html .= "<td >Total: ".$datos_pdf['Moneda']['simbolo']." ".money_format('%!n',$datos_pdf['Comprobante']['subtotal_neto'])."</td>";
   $html .= "</tr>";
   $html .= "</table>";
   $altura_fila = '20px;';
   $html .= "<HR>";
   $html .= '
   <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody>
</table>
   
   <table  border="1" style="width:100%;font-size:10px">
  <tr>
    <td width="3%">It</td>
    <td width="7%">Factura</td>
    <td width="10%">Fecha</td>
    <td width="10%">Cantidad</td>
    <td width="10%">Certificado</td>
    <td width="10%">Ensayo</td>
    
    
    <td width="3%">It</td>
    <td width="7%">Factura</td>
    <td width="10%">Fecha</td>
    <td width="10%">Cantidad</td>
    <td width="10%">Certificado</td>
    <td width="10%">Ensayo</td>
  </tr>

  <tr>
    <td style="height:px;"></td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
  </tr>
  <tr>
    <td style="height:px;"></td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
  </tr>
  <tr>
    <td style="height:px;"></td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
  </tr>
 <tr>
    <td style="height:px;"></td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
  </tr>
 <tr>
    <td style="height:px;"></td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
  </tr>
  <tr>
    <td style="height:px;"></td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
  </tr>
  <tr>
    <td style="height:px;"></td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
  </tr>
  <tr>
    <td style="height:px;"></td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
  </tr>

  <tr>
    <td style="height:px;"></td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
    <td style="height:'.$altura_fila.'">&nbsp;</td>
  </tr>
</table>';

$pdf->writeHTML($html, true, false, true, false, '');


// QRCODE,L : QR-CODE Low error correction
// set style for barcode
$style = array(
    'border' => 2,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

//$pdf->Text(20, 25, 'QRCODE L');

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->SetBarcode(date("Y-m-d H:i:s", time()));
ob_clean();
$pdf->Output($datos_pdf["TipoComprobante"]["abreviado_tipo_comprobante"].'_'.$datos_pdf['Comprobante']['nro_comprobante_completo'].'.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
