<?php

  /**
    * @secured(CONSULTA_QACONFORMIDAD)
  */
  
class QaConformidadesController extends AppController {
    public $name = 'QaConformidades';
    public $model = 'QaConformidad';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
  
  
    /**
    * @secured(CONSULTA_QaConformidad)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $fecha = $this->getFromRequestOrSession('QaConformidad.fecha');
        $id_QaConformidad = $this->getFromRequestOrSession('QaConformidad.id');
        $razon_social = $this->getFromRequestOrSession('QaConformidad.proveedor_razon_social');
        $id_estado = $this->getFromRequestOrSession('QaConformidad.id_estado');
         
        
        $conditions = array(); 

        if($fecha!="")
            array_push($conditions, array('QaConformidad.fecha LIKE' => '%' . $fecha  . '%')); 
              
        if($id_QaConformidad!="")
            array_push($conditions, array('QaConformidad.id =' => $id_QaConformidad));
            
         if($razon_social!="")
            array_push($conditions, array('Proveedor.razon_social LIKE' => '%' . $razon_social  . '%')); 
         
         
         if($id_estado!="")
            array_push($conditions, array('QaConformidad.id_estado =' => $id_estado)); 
               
                           
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>array('Area','Estado','Producto','Componente','MateriaPrima','Proveedor','QaOrigenFalla'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => 'QaConformidad.id desc'
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
    
    
        
        $formBuilder->setDataListado($this, 'Listado de Conformidades', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'QaConformidad.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'QaConformidad.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
    
        
        
        
        
        
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
             $valor['QaConformidad']['QaConformidadItem'] = $valor['QaConformidadItem'];
             unset($valor['QaConformidadItem']);
             
             
        if(isset($valor['QaConformidad']['QaConformidadItem'])){
             foreach($valor['QaConformidad']['QaConformidadItem'] as &$componente ){
                 
                 $componente["d_componente"] = $componente["Componente"]["descripcion"];
                 //$producto["d_producto"] = $this->setFieldProperties("d_producto","Producto",$producto["Producto"]["d_producto"],"1","3");
                 //$producto["codigo_producto"] = $this->setFieldProperties("codigo_producto","Codigo",$producto["Producto"]["codigo"],"1","4");
                 
                 $componente["codigo_componente"] = $componente["Componente"]["codigo"];
                 
                 if(isset($componente["Categoria"]))
                    $componente["d_categoria"] = $componente["Categoria"]["d_categoria"];
                 
                 
                 unset($componente["Componente"]);
             }
        }
             
             
             $valor['QaConformidad']['estado'] = $valor['Estado'];
             unset($valor['Estado']);
             $valor['QaConformidad']['Cliente'] = $valor['Cliente'];
             $valor['QaConformidad']['razon_social'] = $valor['Cliente']['razon_social'];
             $valor['QaConformidad']['d_producto'] = $valor['Producto']['d_producto'];
             unset($valor['Cliente']);
             $valor['QaConformidad']['Comprobante'] = $valor['Comprobante'];
             unset($valor['Comprobante']);
             $valor['QaConformidad']['Remito'] = $valor['Remito'];
             unset($valor['Remito']);
             $valor['QaConformidad']['OrdenArmado'] = $valor['OrdenArmado'];
             unset($valor['OrdenArmado']);
            
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    /**
    * @secured(ABM_USUARIOS)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->QaConformidad->id = $id;
            $this->QaConformidad->contain();
            $this->request->data = $this->QaConformidad->read();
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('QaConformidad');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de QaConformidad');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de QaConformidad');
        else
            $formBuilder->setTituloForm('Consulta de QaConformidad');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('cuit', 'Cuit', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('calle', 'Calle', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  
        $formBuilder->addFormInput('tel', 'Tel&eacute;fono', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('ciudad', 'Ciudad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descuento', 'Descuento', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descripcion', 'Descripcion', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('localidad', 'Localidad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
         
        
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'ClienteAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('ClienteRazonSocial', 'Debe ingresar una Raz&oacute;n Social');
                validator.validateRequired('ClienteCuit', 'Debe ingresar un CUIT');
                validator.validateNumeric('ClienteDescuento', 'El Descuento ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_cuit',
                        'data': 'cuit=' + $('#ClienteCuit').val()+'&id='+$('#ClienteId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('ClienteCuit','El Cuit que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

    /**
    * @secured(CONSULTA_QaConformidad)
    */
    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

     /**
    * @secured(ADD_QaConformidad)
    */
   public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_c = "";
           $this->request->data["QaConformidad"]["fecha"] = date("Y-m-d H:i:s");
            try{
                
                if(!isset($this->request->data['QaConformidad']['id'])){
              
                    $QaConformidad = $this->QaConformidad->save($this->request->data);
                    
                   
                        
                     
                          
                          $mensaje = "La No Conformidad ha sido creada exitosamente";
                          $tipo = EnumError::SUCCESS;
                          $id_c = $this->QaConformidad->id;
                          
                          
                    
                    
                }else{
                    
                
                    if ($this->QaConformidad->saveAll($this->request->data, array('deep' => true))){
                        $mensaje = "La No Conformidad ha sido creado exitosamente";
                        $tipo = EnumError::SUCCESS;
                       
                    }else{
                        $mensaje = "Ha ocurrido un error,la No Conformidad no ha podido ser creada.";
                        $tipo = EnumError::ERROR; 
                        
                    }
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error, la No Conformidad no ha podido ser creada.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" => $id_c
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
       /**
    * @secured(MODIFICACION_QaConformidad)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->QaConformidad->id = $id;
            
            try{ 
                if ($this->QaConformidad->saveAll($this->request->data,array('deep' => true))){
                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "La No Conformidad ha sido modificada exitosamente",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('La No Conformidad ha sido modificada exitosamente.', 'success');
                        $this->redirect(array('controller' => 'QaConformidad', 'action' => 'index'));
                        
                    } 
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, la No Conformidad no ha podido modificarse.', 'error');
                
            }
           
        }else{
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->QaConformidad->id = $id;
            $this->QaConformidad->contain();
            $this->request->data = $this->QaConformidad->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
            }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
     /**
    * @secured(BAJA_QaConformidad)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->QaConformidad->id = $id;
       try{
            if ($this->QaConformidad->saveField('activo', "0")) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "La No Conformidad ha sido eliminada exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }

  

   
    
    
    
    
    public function home(){
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
    
    
    
    
    /**
    * @secured(REPORTE_QaConformidad)
    */
    public function pdfExport($id){
        
        
        $this->loadModel("QaConformidad");
        $this->QaConformidad->id = $id;
        $this->QaConformidad->contain(array('Producto','OrdenArmado','Comprobante','QaConformidadItem' => array('Componente'=>array('Categoria','ArticuloRelacion'=>array('MateriaPrima'))),'Cliente'=> array('Pais','Provincia')));
        //$this->QaConformidad->order(array('QaConformidadItem.orden asc'));
        $this->request->data = $this->QaConformidad->read(); 
        Configure::write('debug',0);
        if( $this->request->data["Cliente"]["id"] == null ){
            $this->request->data["Cliente"]["razon_social"] = $this->request->data["QaConformidad"]["razon_social"];
            $this->request->data["Cliente"]["cuit"] = $this->request->data["QaConformidad"]["cuit"]; 
        
        }
            
        $this->set('datos_pdf',$this->request->data); 
        $this->set('id',$id);
        Configure::write('debug',0);
        $this->layout = 'pdf'; //esto usara el layout pdf.ctp
        $this->render();
        
  
     
        
    }
    
    public function initialize(){ //Seteo los ordenes del Listado
        
        $this->addCalculatedField($this->setFieldProperties("QaConformidadItem","Items de las QaConformidad","","0","0"));
        $this->addCalculatedField($this->setFieldProperties("moneda","Moneda","","0","0"));;
        $this->addCalculatedField($this->setFieldProperties("estado","Estado","","0","0"));;
        $this->addCalculatedField($this->setFieldProperties("Cliente","Cliente","","0","0"));;
        
        
        //seteo las propiedades de los Items Agregados en runtime
        $this->addCalculatedFieldItem($this->setFieldProperties("d_producto","Producto","","1","3"));
        $this->addCalculatedFieldItem($this->setFieldProperties("codigo_producto","Codigo","","1","4"));
        
    }
    
    
    /**
     * @secured(CONSULTA_QACERTIFICADOCALIDAD)
     */
    public function getModel($vista="default"){
    	
    	$model = parent::getModelCamposDefault();//esta en APPCONTROLLER
    	$model = $this->editforView($model);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    	$output = array(
    			"status" =>EnumError::SUCCESS,
    			"message" => $this->model,
    			"content" => $model
    	);
    	
    	
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message", "content"));
    	
    	
    }
    
    
    
    private function editforView($model){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    	
    	$model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
    	
    	
    	
    	$propiedades = array("show_grid"=>"1","header_display" => "Nro. QA","minimun_width"=> "10","order" =>"1","text_colour"=>"#000000" );
    	$this->setFieldView("id",$propiedades,$model);
    	
    	$propiedades = array("show_grid"=>"1","header_display" => "Fecha","minimun_width"=> "10","order" =>"2","text_colour"=>"#000000" );
    	$this->setFieldView("fecha",$propiedades,$model);
    	
    	$propiedades = array("show_grid"=>"1","header_display" => "Raz&oacute;n Social","minimun_width"=> "10","order" =>"3","text_colour"=>"#000000","type" => EnumTipoDato::cadena );
    	$this->setFieldView("razon_social",$propiedades,$model);
    	
    	$propiedades = array("show_grid"=>"1","header_display" => "Cantidad","minimun_width"=> "10","order" =>"4","text_colour"=>"#000000" );
    	$this->setFieldView("cantidad",$propiedades,$model);
    	
    	$propiedades = array("show_grid"=>"1","header_display" => "Pedido Interno","minimun_width"=> "10","order" =>"5","text_colour"=>"#000000","type" => EnumTipoDato::biginteger );
    	$this->setFieldView("nro_pedido_interno",$propiedades,$model);
    	
    	
    	$propiedades = array("show_grid"=>"1","header_display" => "Producto","minimun_width"=> "10","order" =>"6","text_colour"=>"#000000","type" => EnumTipoDato::cadena );
    	$this->setFieldView("d_producto",$propiedades,$model);
    	
    	$propiedades = array("show_grid"=>"1","header_display" => "Orden Armado","minimun_width"=> "10","order" =>"7","text_colour"=>"#000000","type" => EnumTipoDato::biginteger );
    	$this->setFieldView("nro_orden_armado",$propiedades,$model);
    	
    	
    	$propiedades = array("show_grid"=>"1","header_display" => "Estado","minimun_width"=> "10","order" =>"8","text_colour"=>"#000000","type" => EnumTipoDato::cadena );
    	$this->setFieldView("d_estado",$propiedades,$model);
    	
    	$propiedades = array("show_grid"=>"0","header_display" => "Estado","minimun_width"=> "10","order" =>"8","text_colour"=>"#000000","type" => EnumTipoDato::cadena );
    	$this->setFieldView("QaCertificadoCalidadItem",$propiedades,$model);
    	
    	$propiedades = array("show_grid"=>"0","header_display" => "Comprobante","minimun_width"=> "10","order" =>"8","text_colour"=>"#000000","type" => EnumTipoDato::json );
    	$this->setFieldView("Comprobante",$propiedades,$model);
    	
    	$propiedades = array("show_grid"=>"0","header_display" => "OA","minimun_width"=> "10","order" =>"8","text_colour"=>"#000000","type" => EnumTipoDato::json );
    	$this->setFieldView("OrdenArmado",$propiedades,$model);
    	
    	return $model;
    }
   
    
}
?>