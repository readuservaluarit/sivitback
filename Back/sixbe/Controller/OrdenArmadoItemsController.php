<?php

App::uses('ComprobanteItemsController', 'Controller');

class OrdenArmadoItemsController extends ComprobanteItemsController {
    
	
    public $name = EnumController::OrdenArmadoItems;
    public $model = 'ComprobanteItem';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    /**
    * @secured(CONSULTA_ORDEN_ARMADO)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        $this->loadModel("DetalleTipoComprobante");
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model);
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain'=>array('Producto','Comprobante'=>array('TipoComprobante','PuntoVenta','EstadoComprobante'),'ComprobanteItemOrigen'=>array('Comprobante'=>array('PuntoVenta'))),
             'joins' => array(
                array(
                    'table' => 'producto_tipo',
                    'alias' => 'ProductoTipo',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'ProductoTipo.id = Producto.id_producto_tipo'
                    )),
             		
             		array(
             				'table' => 'tipo_comprobante',
             				'alias' => 'TipoComprobante',
             				'type' => 'LEFT',
             				'conditions' => array(
             						'TipoComprobante.id = Comprobante.id_tipo_comprobante'
             				))
             
             
             
             ),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order'=>$this->ComprobanteItem->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_comprobante'))
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
		//Headers
        $formBuilder->addHeader('id', 'Remito.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'Remito.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as $key=>&$producto ){
			
        	
        	/*IMPORTANTE: SObreescribo el campo fecha_entrega de la view*/
        	$producto["ComprobanteItem"]["fecha_entrega"] = 	$producto["Comprobante"]["fecha_entrega"]; 
        	$producto["ComprobanteItem"]["fecha_generacion"] = 	$producto["Comprobante"]["fecha_generacion"]; 
        	
        	
        	if(isset($producto["Comprobante"]["PuntoVenta"]["numero"]))
        		$producto["ComprobanteItem"]["pto_nro_comprobante"] = $this->ComprobanteItem->GetNumberComprobante($producto["Comprobante"]["PuntoVenta"]["numero"],$producto["Comprobante"]["nro_comprobante"]);
        		
        		
        	if(isset( $producto["Comprobante"]["EstadoComprobante"]))
        		$producto["ComprobanteItem"]["d_estado_comprobante"] = $producto["Comprobante"]["EstadoComprobante"]["d_estado_comprobante"];
        	
        		
        		if(isset($producto["ComprobanteItemOrigen"]["id"]) && $producto["ComprobanteItemOrigen"]["id"]>0){
        			
        			
        			$producto["ComprobanteItem"]["pto_nro_pedido_interno"] = $this->ComprobanteItem->GetNumberComprobante($producto["ComprobanteItemOrigen"]["Comprobante"]["PuntoVenta"]["numero"],$producto["ComprobanteItemOrigen"]["Comprobante"]["nro_comprobante"]);
        		}
        	
        		
				
			 $producto["ComprobanteItem"]["d_producto"] = $producto["Producto"]["d_producto"];
			 $producto["ComprobanteItem"]["codigo"] = $this->ComprobanteItem->getCodigoFromProducto($producto);
			// $producto["ComprobanteItem"]["id_unidad"] = $producto["ComprobanteItem"]["id_unidad"];
			 
			 $producto["ComprobanteItem"]["cantidad_desvio"] = (string) ($producto["ComprobanteItem"]["cantidad_cierre"] 
			                                                             - $producto["ComprobanteItem"]["cantidad"]);
			 
			 
			 if(isset($producto["ComprobanteItem"]["id_comprobante_item_origen"]) && $producto["ComprobanteItem"]["id_comprobante_item_origen"]>0 && isset($producto["ComprobanteItemOrigen"]["fecha_entrega"])){
			 
			 $dias_fecha_limite_oa = $this->DetalleTipoComprobante->getDias2(EnumDetalleTipoComprobante::OrdenArmadoPorPedidoInterno);
			 
			 if($dias_fecha_limite_oa>0 && isset($producto["ComprobanteItemOrigen"]["fecha_entrega"])){
			 	
			 	$fecha_entrega_pi = new DateTime($producto["ComprobanteItemOrigen"]["fecha_entrega"]);
			 	$fecha_entrega_pi->sub(new DateInterval('P'.$dias_fecha_limite_oa.'D'));
			 	$producto[$this->model]['fecha_limite_armado'] =   $fecha_entrega_pi->format('d-m-Y');
			 	
			 	$producto[$this->model]['fecha_entrega_origen'] = $producto["ComprobanteItemOrigen"]["fecha_entrega"];
			 	
			 }
			 	
			 	
			 	
			 }
			 
			 $this->FiltrarCamposOnTheFly($data,$producto,$key);
			 
			 unset($producto["Producto"]);
			 unset($producto["Comprobante"]);
			 unset($producto["ComprobanteItemOrigen"]);
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
	}
	//fin vista json
    }
    
    /**
    * @secured(CONSULTA_ORDEN_ARMADO)
    */
    public function getModel($vista='default'){
     
        $model = parent::getModelCamposDefault();
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
     }
    
    private function editforView($model,$vista)
	{  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
		$this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
     }
     
     
     
     
     
     /**
      * @secured(CONSULTA_ORDEN_ARMADO)
      */
     public function excelExport($vista="default",$metodo="index",$titulo=""){
     	
     	$this->paginado =0;
     	
     	$filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');
     	
     	
     	
     	switch($filtro_agrupado){
     		
     		case EnumTipoFiltroComprobanteItem::individual:
     		case "":
     			$view = "resumen_cabecera";
     			break;
     		case EnumTipoFiltroComprobanteItem::agrupadaporComprobante:
     			$view="agrupadaporcomprobante";
     			break;
     		case EnumTipoFiltroComprobanteItem::agrupadaporPersona:
     			$view="agrupadaporpersona";
     			break;
     			
     	}
     	
     	$this->ExportarExcel($view,$metodo,"Listado de Items de Ordenes de Montaje");
     	
     	
     	
     }
    }
    
    
?>