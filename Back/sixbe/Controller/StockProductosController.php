<?php

 
class StockProductosController extends AppController {
    public $name = 'StockProductos';
    public $model = 'StockProducto';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $tipo_producto = EnumProductoTipo::Producto;
    public $padre = "Producto";
    public $d_padre = "d_producto";
    public $title_form = "Listado de Stock";

  
    /**
    * @secured(CONSULTA_STOCK)
    */
    public function index()  
	{    
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $this->deposito_fijo_principal = $this->getDepositoFijoPrincipal();
        $this->deposito_fijo_complementario= $this->getDepositoFijoComplementario();
        
        //Recuperacion de Filtros
        $codigo_StockProducto = $this->getFromRequestOrSession($this->model.'.codigo');
        $id_StockProducto = $this->getFromRequestOrSession($this->model.'.id');
		
        $descripcion = $this->getFromRequestOrSession($this->model.'.d_producto');
        $deposito_fijo = $this->getFromRequestOrSession($this->model.'.deposito_fijo');
        $id_deposito = $this->getFromRequestOrSession($this->model.'.id_deposito');
        $id_producto_padre = $this->getFromRequestOrSession($this->model.'.id_producto_padre');
        $id_categoria = $this->getFromRequestOrSession($this->model.'.id_categoria');
        $id_producto_clasificacion = $this->getFromRequestOrSession($this->model.'.id_producto_clasificacion');
        $codigo_barra = $this->getFromRequestOrSession($this->model.'.codigo_barra');
        

        
        $joins = "";
        
      
        
          $conditions = array(); 
        
        if($id_producto_padre !=""){
            $joins = array(
            array(
                'table' => 'producto_relacion',
                'alias' => 'ArticuloRelacion',
                //'type' => 'LEFT',
                'conditions' => array(
                    'ArticuloRelacion.id_producto_padre ='.$id_producto_padre
                    ,'Componente.id = ArticuloRelacion.`id_producto_hijo`'
                    
                )
            		
            		
            		)
            );
	                  
        }else{
        	
        	
        	$joins = array(
        			
        			
        			array(
        					'table' => 'producto_codigo',
        					'alias' => 'ProductoCodigo',
        					'type' => 'LEFT',
        					'conditions' => array(
        							
        							$this->padre.'.id = ProductoCodigo.id_producto'
        							
        					)
        					
        			)
        	);
        }
        
        
        
        
        
		
		
		
		
      

        array_push($conditions, array($this->padre.'.activo =' => 1));
              
        if($codigo_StockProducto!="" && $id_StockProducto==""){
           //array_push($conditions, array($this->padre.'.codigo LIKE' => '%'.$codigo_StockProducto.'%'));
           //array_push($conditions, array('ProductoCodigo.codigo LIKE' => '%'.$codigo_StockProducto.'%'));
           
           array_push($conditions,array('OR' => array(
           		'ProductoCodigo.codigo LIKE' =>  '%'.$codigo_StockProducto.'%',
           		$this->padre.'.codigo LIKE'=> '%'.$codigo_StockProducto.'%'))
           );
        }
        
        if($id_StockProducto!="")
            array_push($conditions, array($this->padre.'.id =' => $id_StockProducto));
        
		 if($descripcion !="")
            array_push($conditions, array($this->padre.'.d_producto LIKE ' => '%'. $descripcion. '%')); 
            
        if($deposito_fijo !=1 && $id_deposito !='')
            array_push($conditions, array($this->model.'.id_deposito' =>  $id_deposito));
        
            
        if($deposito_fijo == 0){
                
            array_push($conditions,array("NOT" => array(Deposito::class.'.parent_id ' => NULL)));
          }

        if($id_categoria!="")
            array_push($conditions, array($this->padre.'.id_categoria =' => $id_categoria));
            
            
        
        if($id_producto_clasificacion!="")
            array_push($conditions, array($this->padre.'.id_producto_clasificacion =' => $id_producto_clasificacion));
            
        
       if($codigo_barra!=""){
       	
       	
       	array_push($conditions, array('OR' => array(
       			array($this->model.'.codigo_barra'=> $codigo_barra),
       			array($this->model.'.codigo_barra2'=> $codigo_barra),
       			array($this->model.'.codigo_barra3'=> $codigo_barra),
       			array($this->model.'.codigo_barra4'=> $codigo_barra),
       			array($this->model.'.codigo_barra5'=> $codigo_barra),
       	)));
       	
       	
       }
            
        
        if($id_producto_padre =="") //esto es un fix para que si selecciona desde un listado productos relacionados, pueden estar relacionados con todos
        //un puente puede tener valvulas adentro y la valvula no es del tipo_producto componente, entonces si paso el filtro que no haha diferencia
         {  
         array_push($conditions, array($this->padre.'.id_producto_tipo' => $this->tipo_producto)); 
         }
        ///$deposito_fijo =1; 
        //Elijo un paginate diferente si manda el agrupar                   
        if($deposito_fijo == 1)
            $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
                 'contain' =>array($this->padre => array('Unidad','ProductoCodigo')),
            		'fields'=>array('SUM('.$this->model.'.stock) as cantidad',$this->padre.'.stock_minimo',$this->padre.'.codigo',$this->padre.'.d_producto','ProductoCodigo.codigo','ProductoCodigo.id','ProductoCodigo.d_producto_codigo'),
                'conditions' => $conditions,
                'joins'=>$joins,
                'limit' => $this->numrecords,
                'page' => $this->getPageNum(),
                'order' => $this->model.'.id desc',
                'group' => array($this->model.'.id')
            );
        else
            $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
                     'contain' =>array($this->padre=>array('Unidad','ProductoCodigo'),'Deposito'),
                     //'fields'=>array('SUM(StockProducto.stock) as cantidad','Producto.stock_minimo','Producto.codigo','Deposito.d_deposito'),
                    'conditions' => $conditions,
                     'joins'=>$joins,
                    'limit' => $this->numrecords,
                    'page' => $this->getPageNum(),
                    'order' => $this->model.'.id desc',
                    //'group' => array('StockProducto.id')
                );
  
        
        
        
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        $data_aux = array();
        
        if($deposito_fijo == 1){//consulto productos comprometidos para tenerlos en cache
        	
        	$articulos_comprometidos = $this->getArticulosComprometidos();
        	
        	
        }
        
        foreach($data as &$valor){
           
         
             
        	if(isset($valor[$this->padre]['Unidad']['d_unidad']) && !is_null($valor[$this->padre]['Unidad']['d_unidad']) ){
                $valor[$this->model]['d_unidad'] = $valor[$this->padre]['Unidad']['d_unidad'];
                unset($valor[$this->padre]['Unidad']);
             }else{
             	
             	$valor[$this->model]['d_unidad'] = '';
             }
             
             
             //esto es el SUM que agrupa
           if(isset($valor[0]['cantidad']))
             	$valor[$this->model]['stock'] = $valor[0]['cantidad'];
             
           if(isset($valor[$this->model]))  
            $valor[$this->model]['diferencia_stock'] = (string)  ($valor[$this->model]['stock'] - $valor[$this->padre]['stock_minimo']);
             
             
             if(isset($valor[$this->model])){
                     if(! ($valor[$this->padre]['stock_minimo'] <= 0) && !($valor[$this->padre]['stock_minimo']<= 0) ){
                         
                         
                         $valor[$this->model]['distancia_porcentual'] = (string)  ((($valor[$this->model]['stock'] - $valor[$this->padre]['stock_minimo'])*100)/$valor[$this->padre]['stock_minimo']); 
                     }else{
                         $valor[$this->model]['distancia_porcentual'] = "M&iacute;nimo sin definir"; 
                     }
                     
              
                          
                                  //14974
             }
             
             
             
             if(isset($this->deposito_fijo_principal) && $deposito_fijo == 1 )
             	$valor[$this->model]['stock_deposito_fijo_principal'] = (string) $this->{$this->model}->getTotalAgrupadoPorDeposito($valor[$this->padre]['id'],array($this->deposito_fijo_principal),$this->model,$deposito_fijo);  
                 
                 
                 if($deposito_fijo == 1 && $this->model == EnumModel::StockProducto && count($articulos_comprometidos)>0){
             	
                 	$valor[$this->model]['stock_deposito_fijo_secundario'] = $this->getStockFromComprometidos($articulos_comprometidos, $valor[$this->padre]['id']);
                 	
		             }else{  
		             if(isset($this->deposito_fijo_principal) && $deposito_fijo == 1)
		             	$valor[$this->model]['stock_deposito_fijo_secundario'] = (string) $this->{$this->model}->getTotalAgrupadoPorDeposito($valor[$this->padre]['id'],array($this->deposito_fijo_complementario),$this->model,$deposito_fijo);  
		             }
             
             
                if(isset($this->traer_cantidad_orden_produccion))
                    $valor[$this->model]['cantidad_componente_orden_produccion'] = (string) $this->{$this->model}->getTotalEnOrdenesDeProduccion($valor[$this->padre]['id']);  
             
             //bajar un nivel
             
            if(isset($valor[$this->padre]))
            $valor[$this->model][$this->d_padre] =  $valor[$this->padre]['d_producto'];
            
            if(isset($valor[$this->padre]))
            $valor[$this->model]['codigo'] =  $valor[$this->padre]['codigo'];
            
            if(isset($valor[$this->padre]))
            $valor[$this->model]['stock_minimo'] =  $valor[$this->padre]['stock_minimo'];
            
            
            if(isset($valor['Deposito']))
                $valor[$this->model]['d_deposito'] =  $valor['Deposito']['d_deposito']; 
            
                
            if(isset($valor['ProductoCodigo']['codigo']) && strlen($valor['ProductoCodigo']['codigo'])>0 )
                $valor[$this->model]['codigo']  = $valor['ProductoCodigo']['codigo'];
                
        	
            if(isset($valor['ProductoCodigo']['codigo']) && strlen($valor['ProductoCodigo']['d_producto_codigo'])>0 )
            	$valor[$this->model][$this->d_padre]  = $valor['ProductoCodigo']['d_producto_codigo'];
             
             unset($valor[0]['cantidad']);
                
             //$valor['StockProducto']['Producto'] = $valor['Producto'];
             unset($valor[$this->padre]);
             unset($valor['Deposito']);
             unset($valor[0]);
             
          
            
        }
    
        

        $this->data = $data;
        
        $this->viewPath = "Layouts";
        $this->set("data",$data);
        
  
        $this->set("page_count",$page_count);
        

        $seteo_form_builder = array("showActionColumn" =>true,"showNewButton" =>false,"showFooter"=>true,"showHeaderListado"=>true);
        
        
        $this->form_title = "Listado de Stock";
        $this->preparaHtmLFormBuilder($this,$seteo_form_builder,"default",true);
        
        
     
        
    //fin vista json
        
    }
    
    
    /**
    * @secured(CONSULTA_STOCK)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
        
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        
            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);
        
               
        
        
      
    }
    
    
    
    /**
     * @secured(CONSULTA_STOCK)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	$deposito_fijo = $this->getFromRequestOrSession($this->model.'.deposito_fijo');
    	
    	if($deposito_fijo == 1)//esta vista agrupa el stock de todos los depositos
    		$vista = "agrupada";
    	
    	parent::excelExport($vista,$metodo,"Listado de ".$this->name);
    	
    	
    	
    }
    
    
    public function indexHtml(){
    	
    	$a;
    }
    
    public function getDepositoFijoPrincipal(){
    	
    	$id_producto_tipo = $this->tipo_producto;
    	//hago un find de la parametrizacion
		$this->loadModel("ProductoTipo");    
		
		$producto_tipo = $this->ProductoTipo->find('first', array(
				'conditions' => array('ProductoTipo.id' => $id_producto_tipo),
				'contain' =>false
				
				
		));
		
		
		return $producto_tipo["ProductoTipo"]["id_deposito_fijo_principal"];
  
    	
    	
    	
    }
    
    public function getDepositoFijoComplementario(){
    	
    	$id_producto_tipo = $this->tipo_producto;
    	//hago un find de la parametrizacion
    	$this->loadModel("ProductoTipo");
    	
    	$producto_tipo = $this->ProductoTipo->find('first', array(
    			'conditions' => array('ProductoTipo.id' => $id_producto_tipo),
    			'contain' =>false
    			
    			
    	));
    	
    	
    	return $producto_tipo["ProductoTipo"]["id_deposito_fijo_complementario"];
    	
    	
    	
    	
    }
    
    public function getArticulosComprometidos(){
    	
    $this->loadModel(Comprobante::class);
    	
    $array_tipo_factura = 	$this->Comprobante->getTiposComprobanteController(EnumController::Facturas);
    
    $tipos_factura = implode (", ", $array_tipo_factura);
    	
    	
    	$query= "SELECT 
  IF(
    (
      SUM(ci.cantidad) - IFNULL(
        (SELECT 
          /* SUMO LAS CANTIDADES DE ESE PRODUCTO QUE SE REMITIERON DEL CIRCUITO PEDIDO-REMITO*/
          SUM(cantidad) 
        FROM
          comprobante_item 
          JOIN comprobante 
            ON comprobante.id = comprobante_item.`id_comprobante` 
        WHERE id_tipo_comprobante = ".EnumTipoComprobante::Remito."
          AND (
            id_estado_comprobante = ".EnumEstadoComprobante::CerradoReimpresion." 
            /*  CERRADO*/
            OR id_estado_comprobante = ".EnumEstadoComprobante::CerradoConCae."  
            /*  CERRADO CAE */
            OR id_estado_comprobante = ".EnumEstadoComprobante::Remitido."  
            /*  Remitido */
          ) 
          AND id_producto = ci.`id_producto` 
          AND id_comprobante_item_origen = ci.`id`),
        0
      ) - IFNULL(
        (SELECT 
          /* SUMO LAS CANTIDADES DE ESE PRODUCTO QUE SE REMITIERON DEL CIRCUITO FACURA-REMITO*/
          SUM(cantidad) 
        FROM
          comprobante_item 
          JOIN comprobante 
            ON comprobante.id = comprobante_item.`id_comprobante` 
        WHERE id_tipo_comprobante = 203 
          AND (
            id_estado_comprobante = 115 
            /*  CERRADO*/
            OR id_estado_comprobante = 116 
            /*  CERRADO CAE */
            OR id_estado_comprobante = 118 
            /*  Remitido */
          ) 
          AND id_producto = ci.`id_producto` 
          AND comprobante_item.`activo` = 1
          AND id_comprobante_item_origen IN 
          (SELECT 
            comprobante_item.id 
          FROM
            comprobante_item 
            JOIN comprobante 
              ON comprobante.id = comprobante_item.`id_comprobante` 
          WHERE id_comprobante_item_origen = ci.`id` 
            AND id_tipo_comprobante IN (".$tipos_factura.")
			AND comprobante.`id_estado_comprobante` IN (9,115)

)),
        0
      )
    ) < 0,
    0,
    SUM(ci.cantidad) - IFNULL(
      (SELECT 
        /* SUMO LAS CANTIDADES DE ESE PRODUCTO QUE SE REMITIERON*/
        SUM(cantidad) 
      FROM
        comprobante_item 
        JOIN comprobante 
          ON comprobante.id = comprobante_item.`id_comprobante` 
      WHERE id_tipo_comprobante = 203 
        AND (
          id_estado_comprobante = 115 
          /*  CERRADO*/
          OR id_estado_comprobante = 116 
          /*  CERRADO CAE */
          OR id_estado_comprobante = 118 
          /*  Remitido */
        ) 
        AND id_producto = ci.`id_producto` 
        AND comprobante_item.`activo` = 1
        AND id_comprobante_item_origen IN 
        (SELECT 
          comprobante_item.id 
        FROM
          comprobante_item 
          JOIN comprobante 
            ON comprobante.id = comprobante_item.`id_comprobante` 
        WHERE id_comprobante_item_origen = ci.`id` 
          AND id_tipo_comprobante IN (".$tipos_factura.")
		  AND comprobante.`id_estado_comprobante` IN (9,115)


)),
      0
    )
  ) AS pendiente,
  ci.id_producto 
FROM
  comprobante_item ci 
  JOIN comprobante 
    ON comprobante.id = ci.id_comprobante 
WHERE comprobante.`id_estado_comprobante` = 1 
  AND comprobante.`id_tipo_comprobante` = 202 
  AND ci.activo = 1 
GROUP BY ci.id_producto 
ORDER BY id_producto ASC
LIMIT 100000000
 ;";//viejo no va mas
    	
    	
    	
    	$query = "SELECT 
  comprobante_item.id_producto,
SUM(comprobante_item.cantidad) -  IFNULL( remitidos.cantidad,0) AS pendiente
FROM
  comprobante_item 
  JOIN comprobante 
    ON comprobante.id = comprobante_item.`id_comprobante` 
  LEFT JOIN 
    (SELECT 
      id_producto,
      SUM(cantidad) cantidad 
    FROM
      (
        (SELECT 
          /* SUMO LAS CANTIDADES DE ESE PRODUCTO QUE SE REMITIERON DEL CIRCUITO FACURA-REMITO*/
          comprobante_item.`id_producto`,
          SUM(cantidad) cantidad 
        FROM
          comprobante_item 
          JOIN comprobante 
            ON comprobante.id = comprobante_item.`id_comprobante` 
        WHERE id_tipo_comprobante = 203 
          AND comprobante_item.`activo` = 1 
          AND id_estado_comprobante = 118 
          /*  Remitido */
          AND id_comprobante_item_origen IN 
          (SELECT 
            comprobante_item.id 
          FROM
            comprobante_item 
            JOIN comprobante 
              ON comprobante.id = comprobante_item.`id_comprobante` 
            JOIN comprobante_item pedido_item 
              ON pedido_item.id = comprobante_item.id_comprobante_item_origen 
            JOIN comprobante pedido 
              ON pedido_item.`id_comprobante` = pedido.id 
          WHERE comprobante.id_tipo_comprobante IN (".$tipos_factura.")
            AND pedido.`id_tipo_comprobante` = 202 
            AND pedido_item.`activo` = 1 
            AND pedido.`id_estado_comprobante` = 1) 
        GROUP BY comprobante_item.`id_producto`) 
        UNION
        ALL 
        (SELECT 
          ci.id_producto,
          SUM(remito_item.cantidad) AS cantidad 
        FROM
          comprobante_item ci 
          JOIN comprobante 
            ON comprobante.id = ci.id_comprobante 
          LEFT JOIN comprobante_item remito_item 
            ON remito_item.`id_comprobante_item_origen` = ci.id 
          JOIN comprobante remito 
            ON remito.id = remito_item.`id_comprobante` 
            AND remito.`id_tipo_comprobante` = 203 
        WHERE comprobante.`id_estado_comprobante` = 1 
          AND comprobante.`id_tipo_comprobante` = 202 
          AND remito_item.`activo` = 1 
          AND remito.`id_estado_comprobante` <> 2 
          AND ci.activo = 1 
        GROUP BY ci.id_producto 
        ORDER BY id_producto ASC)
      ) t 
    GROUP BY id_producto) remitidos 
    ON remitidos.id_producto = comprobante_item.`id_producto` 
WHERE id_tipo_comprobante = 202 
  AND id_estado_comprobante = 1 
  AND comprobante_item.`activo` = 1 
GROUP BY comprobante_item.id_producto  ";
    	
    	
    	
    	///$this->log($query);
    	
    	$productos_comprometidos_por_pedidos_internos = $this->{$this->model}->query($query);
    	
    	
    	$aux = array();
    	
    	
    	foreach($productos_comprometidos_por_pedidos_internos as $producto){
    		
    		$value1["id_producto"] = $producto["comprobante_item"]["id_producto"];
    		$value1["cantidad"] = $producto[0]["pendiente"];
    		array_push($aux, $value1);
    		
    	}
    	
    	return $aux;
    	
    	
    	
    	
    }
    
    
    public function getStockFromComprometidos($array_productos_comprometidos,$id_producto){
    	
    	
    	
    	
    	$key = array_search($id_producto, array_column($array_productos_comprometidos, 'id_producto'));
    	
    	if($key && $key>=0){
    		//unset($array_productos_comprometidos[$key]);
    		return $array_productos_comprometidos[$key]["cantidad"];
    	}else 
    		return 0;
    	
    	/*foreach($array_productos_comprometidos as $producto){
    		
    		
    		if($producto["ci"]["id_producto"] == $id_producto){
    			return $producto[0]["pendiente"];
    		}
    	}*/
    	
    	
    }
    
    
    
    
    
    
   
   
    
   
    
}
?>