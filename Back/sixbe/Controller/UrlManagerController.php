<?php


class UrlManagerController extends AppController {
    public $name = 'urlManager';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
  
  
  
    public function getUrl() { 
        
 
        
    	$key = $this->getFromRequestOrSession('UrlManager.key');
    	$cuit = str_replace("-","",$this->getFromRequestOrSession('UrlManager.cuit'));
    	
    	$url = "";
    
    	$array_cuit_url = array("00000000000"=>"http://192.168.2.202/sivitbackend/sivitback/Back/sixbe/");
    	$array_cuit_url["00000000001"] = "http://192.168.56.2/sivitbackend/sivitback/Back/sixbe/";//valmec
    	$array_cuit_url["30587645471"] = "http://192.168.2.123/siv/sixbe/";//valmec
    	$array_cuit_url["33558510809"] = "http://192.168.2.100/siv/sixbe/";//
    	$array_cuit_url["30715876546"] = "https://metrocasasyedificios.sivit.com.ar/";//
    	$array_cuit_url["30716061376"] = "https://pristine.sivit.com.ar/";//
    	$array_cuit_url["20335245637"] = "https://josemuratorio.sivit.com.ar/";//
    	$array_cuit_url["20351532638"] = "https://mauroportelli.sivit.com.ar/";//
    	
    	
    	
    	if($key == "5252c430b1656cd08ddda03b5f7b539e8cb855bf627ff1830071ae2c4ce488d2e4f70d4867ced7c93b23860549e819c74a6ff2a084dfadb6a8511278d9afd4d"){
    		
    		$error = EnumError::SUCCESS;
    		
    		if(isset($array_cuit_url[$cuit])){
    			
    			$url = $array_cuit_url[$cuit];
    			$message = "Cuit valido";
    		}else{
    			
    			$error = EnumError::ERROR;
    			$message = "No se encuenta el CUIT";
    		}
    		
    	}else{
    		
    		
    		$error = EnumError::ERROR;
    		$message = "Error en el parametro de seguridad";
    	}
            
            
        $output = array(
            "status" =>$error,
        		"message" => $message,
        	"content" => $url,
            "page_count" =>0
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
             
             
}


public function beforeFilter(){
	
	App::uses('ConnectionManager', 'Model');
	
	$this->Auth->allow('getUrl');
	
}
       
        
 
    
   
   
    
    
 
    
 
    
  
}
?>