<?php
        
        
        include(APP.'View'.DS.'ComprobanteItemComprobantes'.DS.'json'.DS.'default.ctp');
        
      
        
        $model["d_tipo_comprobante"]["show_grid"] = 1;
        $model["d_tipo_comprobante"]["header_display"] = "Tipo. Comp.";
        $model["d_tipo_comprobante"]["order"] = "0";
        $model["d_tipo_comprobante"]["width"] = "10";
        
        
        
        $model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Nro. Comp.";
        $model["pto_nro_comprobante"]["order"] = "1";
        $model["pto_nro_comprobante"]["width"] = "20";
        
        $model["fecha_contable"]["show_grid"] = 1;
        $model["fecha_contable"]["header_display"] = "Fcha. Cont.";
        $model["fecha_contable"]["order"] = "2";
        $model["fecha_contable"]["width"] = "10";
        
        
        $model["d_estado_comprobante"]["show_grid"] = 1;
        $model["d_estado_comprobante"]["header_display"] = "Estado. Comp.";
        $model["d_estado_comprobante"]["order"] = "3";
        $model["d_estado_comprobante"]["width"] = "15";
        
        
      
        $model["d_tipo_comprobante_origen"]["show_grid"] = 1;
        $model["d_tipo_comprobante_origen"]["header_display"] = "Tipo Comp. Imputado";
        $model["d_tipo_comprobante_origen"]["order"] = "4";
        $model["d_tipo_comprobante_origen"]["width"] = "10";
        
        
        
        
        
        $model["pto_nro_comprobante_origen"]["show_grid"] = 1;
        $model["pto_nro_comprobante_origen"]["header_display"] = "Nro. Comp Imputado";
        $model["pto_nro_comprobante_origen"]["order"] = "5";
        $model["pto_nro_comprobante_origen"]["width"] = "20";
        
        $model["fecha_contable_origen"]["show_grid"] = 1;
        $model["fecha_contable_origen"]["header_display"] = "Fcha. Cont Origen.";
        $model["fecha_contable_origen"]["order"] = "6";
        $model["fecha_contable_origen"]["width"] = "10";
        
        $model["fecha_vencimiento_origen"]["show_grid"] = 1;
        $model["fecha_vencimiento_origen"]["header_display"] = "Fcha. Venc.";
        $model["fecha_vencimiento_origen"]["order"] = "7";
        $model["fecha_vencimiento_origen"]["width"] = "10";
        
        
        $model["precio_unitario"]["show_grid"] = 1;
        $model["precio_unitario"]["header_display"] = "Monto Imputado.";
        $model["precio_unitario"]["order"] = "8";
        $model["precio_unitario"]["width"] = "20";
        $model["precio_unitario"]["read_only"] = "1";
        
        $model["subtotal_comprobante_origen"]["show_grid"] = 1;
        $model["subtotal_comprobante_origen"]["header_display"] = "SubTotal Origen.";
        $model["subtotal_comprobante_origen"]["order"] = "9";
        $model["subtotal_comprobante_origen"]["width"] = "20";
        
        $model["total_comprobante_origen"]["show_grid"] = 1;
        $model["total_comprobante_origen"]["header_display"] = "Total Origen.";
        $model["total_comprobante_origen"]["order"] = "10";
        $model["total_comprobante_origen"]["width"] = "20";
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItemComprobante",
                    "content" => $model
                );
        echo json_encode($output);
?>