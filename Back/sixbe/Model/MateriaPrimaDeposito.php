<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class MateriaPrimaDeposito extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'materia_prima_deposito';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
       
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
        'MateriaPrima' => array(
            'className' => 'MateriaPrima',
            'foreignKey' => 'id_materia_prima',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Deposito' => array(
            'className' => 'Deposito',
            'foreignKey' => 'id_deposito',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ), 
        
       
   );     

}
