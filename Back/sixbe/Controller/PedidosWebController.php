<?php
App::uses('ComprobantesController', 'Controller');

class PedidosWebController extends ComprobantesController {
	public $name = EnumController::PedidosWeb;
    public $model = 'Comprobante';
    public $helpers = array ( 'Paginator', 'Js');
    public $components = array('PaginatorModificado', 'RequestHandler');
    public $traigo_items=0;
    public $requiere_impuestos = 0;
    public $title_form  = "Listado de Pedidos Web";
  
    /**
    * @secured(CONSULTA_PEDIDO_INTERNO)
    */
    public function index() {
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros($this->model);
        
        array_push($conditions, array("Comprobante.id_detalle_tipo_comprobante"=>EnumDetalleTipoComprobante::PedidosWeb));

        $traigo_items_filtro = $this->getFromRequestOrSession('Comprobante.item');
        
        if( $traigo_items_filtro!='' && $traigo_items_filtro == 1){
            $array_conditions = array('ComprobanteItem'=>array('Producto'=>array('Iva')),'Persona','PersonaSecundaria','CondicionPago','Moneda','EstadoComprobante','PuntoVenta','DepositoOrigen','DepositoDestino','Usuario','Sucursal');
            $this->traigo_items = 1;
       } else
       	$array_conditions = array('Persona','PersonaSecundaria','CondicionPago','Moneda','EstadoComprobante','PuntoVenta','DepositoOrigen','DepositoDestino','Usuario','Sucursal');

        $orden = ''; 
        if($this->getFromRequestOrSession('Comprobante.fecha_entrega_desde') != "" || $this->getFromRequestOrSession('Comprobante.fecha_entrega_hasta') != "")
           $orden .='Comprobante.fecha_entrega asc';
        else
           $orden .='Comprobante.nro_comprobante desc';
                                             
        $this->paginate = array(
            'paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            
            'conditions' => $conditions,
            'contain' => $array_conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $orden
        );
   

        $this->PaginatorModificado->settings = $this->paginate;
         $data = $this->PaginatorModificado->paginate($this->model);
         $page_count = $this->params['paging'][$this->model]['pageCount'];
      
        $this->loadModel("ComprobanteItem");
         
        //parseo el data para que los models queden dentro del objeto de respuesta
		foreach($data as &$valor)
		{
             $this->cleanforOutput($valor);
		}
        
        
		

        
        
      
        
 
        
        
     
     $this->data = $data;
     
     $this->viewPath = "Layouts";
     $this->set("data",$this->data);
     
     
     $this->set("page_count",$page_count);
     
     $this->data = $data;
     
     
     $seteo_form_builder = array("showActionColumn" =>true,"showNewButton" =>false,"showActionColumn"=>false,"showFooter"=>true,"showHeaderListado"=>true,"showXlsButton"=>false);
     
     $this->title_form = "Listado de Facturas";
     $this->preparaHtmLFormBuilder($this,$seteo_form_builder,"");
     
        
    //fin vista json
        
    }


    
	/*
	CONTROLLER METODO    ORDEN_COMPRA ITEM
	PedidosInternos /add        NUEVA     NULO  
	PedidosInternos /add        NUEVA     NUEVO  
	PedidosInternos /edit       EXISTE    NUEVO ITEM  
	PedidosInternos /edit       EXISTE    NULO  
	PedidosInternos /edit       EXISTE    NULO  
	PedidosInternos /DELETE     BORRAR     NULO  
	PedidosInternos /DELETE     BORRAR     EXISTE  
	PedidosInternos /DELETE_ITEM EXISTE    BORRAR
	
	data[PedidosInternos][plazo_entrega]="30"
	data[PedidosInternosItem][0][cantidad] = 2
	edit()
	data[PedidosInternos][plazo_entrega]="30"
	data[PedidosInternosItem][0][id] = 2
	data[PedidosInternosItem][0][cantidad] = 2
	
	
	add()
	data[PedidosInternos][plazo_entrega]="30"
	data[PedidosInternosItem][0][cantidad] = 2
	*/
	
	
  
    
    
    
  
  
    /**
    * @secured(REPORTE_PEDIDO_INTERNO)
    */
    public function pdfExport($id,$vista=''){
        
      parent::pdfExport($id);
     
        
    }
    
    
     /**
    * @secured(REPORTE_PEDIDO_INTERNO)
    */
    public function pdfExportSinPrecio($id)
	{ 
        //$precio == 1 //imprime PDF con precios    
        parent::pdfExport($id);
    }

     private function cleanforOutput(&$valor)
	 {   
         $valor['Comprobante']['razon_social'] = $valor['Persona']['razon_social'];
       
         $valor['Comprobante']['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
         $valor['Comprobante']['nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante(0,$valor['Comprobante']['nro_comprobante']);
         
         $valor['Comprobante']['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor['Comprobante']['nro_comprobante']);
         
         if(isset($valor['DepositoOrigen'])){
          $valor['Comprobante']['d_deposito_origen'] =$valor['DepositoOrigen']['d_deposito_origen'];
         }
         
         if(isset($valor['DepositoDestino'])){
            $valor['Comprobante']['d_deposito_destino'] =$valor['DepositoDestino']['d_deposito_destino'];
         }
         
          if(isset($valor['Usuario'])){
            $valor['Comprobante']['d_usuario'] =$valor['Usuario']['nombre'];
         }
         
          if(isset($valor['Moneda'])){
            $valor['Comprobante']['d_moneda'] =$valor['Moneda']['simbolo'];
         }
         
         if(isset($valor['Sucursal'])){
          $valor['Comprobante']['d_sucursal'] =$valor["Sucursal"]["nombre"]." -  ".$valor["Sucursal"]["razon_social"];
         }
         
         if(isset($valor['Persona'])){
            $valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
            $valor[$this->model]['id_lista_precio_defecto_persona'] = $valor['Persona']['id_lista_precio'];
         }else{
        	$valor[$this->model]['codigo_persona'] = "";
        	
         }
         
         
         if(isset($valor['PersonaSecundaria'])){
         	$valor[$this->model]['codigo_persona_secundaria'] = $valor['PersonaSecundaria']['codigo'];
         	$valor[$this->model]['razon_social_secundaria'] = $valor['PersonaSecundaria']['razon_social'];

         }else{
         	$valor[$this->model]['codigo_persona'] = "";
         	
         }
        
        	
         
         $this->{$this->model}->formatearFechas($valor);
         
         unset($valor['Persona']);
         unset($valor['DepositoOrigen']);
         unset($valor['DepositoDestino']);
         unset($valor['EstadoComprobante']);
         
         
         
         //esto es el flag para traer los items del PI dentro del controller
         if( $this->traigo_items == 1){
             
         if(isset($valor['ComprobanteItem'])){   
         foreach($valor['ComprobanteItem'] as &$item){
             
             $item["codigo"] = $item["Producto"]["codigo"];
             $item["d_producto"] = $item["Producto"]["d_producto"];
             unset($item["Producto"]);
              // calculo los totales facturados y remitidos
                   $remitidos = $this->Comprobante->ComprobanteItem->GetItemprocesadosEnImportacion($item,   $this->Comprobante->getTiposComprobanteController(EnumController::Remitos));
                   $facturados =$this->Comprobante->ComprobanteItem->GetItemprocesadosEnImportacion($item,   $this->Comprobante->getTiposComprobanteController(EnumController::Facturas));
                // }
                 $item["total_facturado"] = (string) $this->Comprobante->ComprobanteItem->GetCantidadItemProcesados($facturados);
                 $item["total_remitido"] = (string) $this->Comprobante->ComprobanteItem->GetCantidadItemProcesados($remitidos);
         }
         } 
         $valor['Comprobante']['ComprobanteItem'] = $valor['ComprobanteItem'];
         unset($valor['ComprobanteItem']);  
         }
          
               unset($valor['Persona']);  
               unset($valor['Moneda']);  
               unset($valor['CondicionPago']);  
               unset($valor['Sucursal']);  
               unset($valor['PuntoVenta']);  
               unset($valor['Usuario']);  
             
     }
     
     
      /**
    * @secured(CONSULTA_PEDIDO_INTERNO)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
      
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
        
        
         $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);          
    }
    
    
    /**
    * @secured(CONSULTA_PEDIDO_INTERNO)
    */
    
    public function chequeoValorNoRepetido($nombre_campo,$valor,$id_comprobante,$id_persona,$id_tipo_comprobante=EnumTipoComprobante::PedidoInterno){
        
        
        parent::chequeoValorNoRepetido($nombre_campo,$valor,$id_comprobante,$id_persona,$id_tipo_comprobante);
    }
    
   
    /**
     * @secured(MODIFICACION_PEDIDO_INTERNO)
     */
   	public function Anular($id_comprobante){
   		
   		$this->loadModel("Comprobante");
   		$this->loadModel("ComprobanteItem");
   		$this->loadModel("Movimiento");
   		$this->loadModel("Modulo");
   		
   		$this->anulo = 1;
   		$puede_anular = 1;
   		
   		$pi = $this->Comprobante->find('first', array(
   				'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
   				'contain' => array('ComprobanteItem')
   		));
   		
   		
   		
   		
   		
   		if( $pi["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::PedidoInterno  && $this->Comprobante->getEstadoGrabado($id_comprobante) != EnumEstadoComprobante::Anulado
   				
   			
   				){
   					
   					/*
   					 $this->request->data["Comprobante"]["id_estado_comprobante"] = EnumEstadoComprobante::Anulado;
   					 $this->request->data["Comprobante"]["fecha_anulacion"] = date("Y-m-d");
   					 $this->request->data["Comprobante"]["definitivo"] = 1;
   					 
   					 
   					 
   					 /*
   					 if( isset($this->request->data["ComprobanteItem"]) && count($this->request->data["ComprobanteItem"])>0 &&  $this->request->data["Comprobante"]["genera_movimiento_stock"] == 1 ){
   					 
   					 
   					 foreach($this->request->data["ComprobanteItem"] as &$valor){
   					 
   					 $valor["cantidad_cierre"] = "0";
   					 
   					 }
   					 }*/
   					
   					
   					$ds = $this->Comprobante->getdatasource();
   					
   					$status = EnumError::SUCCESS;
   					
   					try{
   						$ds->begin();
   						
   						$modulo_produccion  = $this->Modulo->estaHabilitado(EnumModulo::PRODUCCION);
   						
   						
   						if($modulo_produccion == 1){
   							
   							/*Cheque que no haya ninguna OA que no este anulada que tenga como item a algun item del PI*/
   							
   							$pi_item = $this->ComprobanteItem->find('all', array(
   									'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
   									'fields' => array('ComprobanteItem.id'),
   									'contain'=>array("Comprobante")
   							));
   							
   							
   							if(count($pi_item)>0){//si hay items obtengo los ID
		   							
   								$ids_items_pi = array();
		   						foreach($pi_item as $item){
		   								
		   								array_push($ids_items_pi, $item["ComprobanteItem"]["id"]);
		   						}
		   					
		   						
   							
		   						
		   						$oa_item = $this->ComprobanteItem->find('all', array(
		   								'conditions' => array('ComprobanteItem.id_comprobante_item_origen'=>$ids_items_pi,'Comprobante.id_tipo_comprobante'=>EnumTipoComprobante::OrdenArmado,"Comprobante.id_estado_comprobante <>"=>EnumEstadoComprobante::Anulado),
		   								'fields' => array('ComprobanteItem.id'),
		   								'contain'=>array("Comprobante")
		   						));
		   						
		   						
		   						if(count($oa_item)>0){//si tiene OA asociada
		   							
		   							$status = EnumError::ERROR;
		   							$messagge = "ERROR: El Pedido Interno no es posible anularlo ya que tiene ordenes de armado asociadas.";
		   						}
		   						
		   						
   							}
   						
   				
   							
   							
   							
   						}
   						
   						
   						if($status == EnumError::SUCCESS){
   						
   						$this->Comprobante->updateAll(
   								array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
   								array('Comprobante.id' => $id_comprobante) );
   						
   						
   						
   						
   						$status = EnumError::SUCCESS;
   						$messagge = "El Pedido Interno ha sido anulado";
   						
   						$pedido_interno = $this->Comprobante->find('first', array(
   								'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
   								'contain' => false
   						));
   						
   						$this->Auditar($id_comprobante,$pedido_interno);
   						
   						}
   						
   						
   						
   						$ds->commit();
   						
   					}catch(Exception $e){
   						
   						$ds->rollback();
   						$tipo = EnumError::ERROR;
   						$mensaje = "No es posible anular el Comprobante ERROR:".$e->getMessage();
   						
   						
   					}
   					
   					/*
   					
   					$this->Comprobante->id = $this->request->data["Comprobante"]["id"];
   					$this->Comprobante->saveField('definitivo', 0);
   					$this->Comprobante->saveField('id_estado_comprobante', EnumEstadoComprobante::Abierto);
   					
   					//$this->edit($this->request->data["Comprobante"]["id"]);
   					//return;
   					
   					*/
   		}else{
   			
   			$status = EnumError::ERROR;
   			$messagge = "ERROR: El Pedido Interno no es posible anularlo.";
   			
   			
   		}
   		
   		$output = array(
   				"status" => $status,
   				"message" => $messagge,
   				"content" => ""
   		);
   		echo json_encode($output);
   		die();
   		
   		
   	}
   
   
   /**
    * @secured(BTN_EXCEL_PEDIDOSINTERNOS)
    */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
        
        parent::excelExport($vista,$metodo,"Listado de Pedidos Internos");
      
        
        
    }
    
     /**
    * @secured(CONSULTA_PEDIDO_INTERNO)
    */
    public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
    {
        parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
    }
    
    
    
    /**
     * @secured(CONSULTA_PEDIDO_INTERNO)
     */
    public function existe_comprobante()
    {
    	parent::existe_comprobante();
    }
   
}
?>