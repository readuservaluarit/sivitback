<?php
/**
* @secured(CONSULTA_BANCO)
*/
class ConciliacionBancariaController extends AppController {
    public $name = 'ConciliacionBancaria';
    public $model = 'CsvImportConciliacion';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
     private function RecuperaFiltros()
        {
            $conditions = array(); 

            $id_asiento = strtolower($this->getFromRequestOrSession('AsientoCuentaContable.id_asiento'));
            
            $id_cuenta_contable = strtolower($this->getFromRequestOrSession('AsientoCuentaContable.id_cuenta_contable'));
            
            $fecha_desde = strtolower($this->getFromRequestOrSession('ConciliacionBancaria.fecha_desde'));
            $fecha_hasta = strtolower($this->getFromRequestOrSession('ConciliacionBancaria.fecha_hasta'));
            
            if ($id_asiento != "") 
                array_push($conditions, array('AsientoCuentaContable.id_asiento =' => $id_asiento));
            
            if ($id_cuenta_contable != "") 
                array_push($conditions, array('AsientoCuentaContable.id_cuenta_contable =' => $id_cuenta_contable));
                
                
            if ($fecha_desde != "") 
                array_push($conditions, array('DATE(CsvImportConciliacion.fecha) >= ' => $fecha_desde));

            if ($fecha_hasta != "") 
                array_push($conditions, array('DATE(CsvImportConciliacion.fecha) <= ' => $fecha_hasta));       
                

            return $conditions; 
        } 
    
/**
* @secured(CONSULTA_BANCO)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        $conditions = array();
        
        $conditions = $this->RecuperaFiltros();
        
        

        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' => false,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  
           
        }else{ // vista json
        
        $parameters = $this->paginate;
        unset($parameters["limit"]);
        $registros_csv_importacion = $this->{$this->model}->find('all',$parameters); //traigo los registros del csv
        
        $id_cuenta_contable = strtolower($this->getFromRequestOrSession('AsientoCuentaContable.id_cuenta_contable'));
        $id_cuenta_contable = 21;
        $array_conciliados = $this->getConciliados($id_cuenta_contable);//devuelve un array con los id_asiento_cuenta_contable
        $id_conciliados_csv = $this->getIdCsvImportacionConciliados($array_conciliados);//devuelve un array con los id_asiento_cuenta_contable
        $id_conciliados_acc = $this->getIdAsientoCuentaContablemportacionConciliados($array_conciliados);//devuelve un array con los id_asiento_cuenta_contable
        
        
        foreach($registros_csv_importacion as &$valor){
            
            if(in_array($valor["CsvImportConciliacion"]["id"],$id_conciliados_csv)){
                
                $valor["CsvImportConciliacion"]["conciliado"] =  1;
            }else{
                
               $valor["CsvImportConciliacion"]["conciliado"] =  0; 
            }
            
                $this->{$this->model}->formatearFechas($valor);
            
            
            
            
        }
        
        
        $page_count = 0;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $registros_csv_importacion,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


   

   

   
    
    
	
	public function getModel($vista='default')
	{    
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    }
    
    
    
    private function editforView($model,$vista)
	{  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);    
        
    }
    
    
    public function importaCsvResumenBancario(){
       ob_start(); 
       $this->loadModel($this->model);
    
         $error = '';
         $message = '';
         $this->ImportarArchivoCsv($this->model,$error,$message);
         if($error == "success"){
          //aca debo procesar la tabla  
          try{
              
          //le doy formato a los campos del archivo    
          $sql = '  UPDATE csv_import_conciliacion SET es_debe=1;

                    UPDATE csv_import_conciliacion SET es_debe=0 WHERE monto LIKE \'%(%\' OR monto LIKE \'%-%\'

                    UPDATE csv_import_conciliacion SET monto = TRIM(REPLACE(REPLACE(REPLACE(monto,"-",""),"(",""),")",""))

                    UPDATE csv_import_conciliacion SET fecha = CONCAT(SUBSTRING(fecha, 7, 4),"-",SUBSTRING(fecha, 4, 2),"-",SUBSTRING(fecha, 1, 2))';
                    
                    
                    
          $this->{$this->model}->query($sql);
          $error = EnumError::SUCCESS;
          $message = "El resumen bancario han sido importado exitosamente";
          
         }catch(Exception $e){
             
            $error = EnumError::ERROR ;
            $message = "No pudo importarse el resumen. Intente Nuevamente, revise el formato del csv.".$e->getMessage();
             
         }  
         }
         
        $this->autoRender = false;
        
        ob_clean();
        $output = array(
            "status" => $error,
            "message" => $message,
            "content" => "CONTENT"
        );
        //$this->set($output);
        //$this->set("_serialize", array("status", "message","page_count", "content"));
        
        echo json_encode($output);  
         
         
         
   }
   
   
   
   
   
   

}
?>