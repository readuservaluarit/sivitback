<?php


    App::import('Lib', 'ReportHelper');
    App::import('Lib', 'FormBuilder');

    class ReportesController extends AppController {
        public $name = 'Reportes';
        public $helpers = array ('Session', 'Paginator', 'Js', 'Html');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
        public $xlsReport =0;
        public $tiene_permisos = 0;
        

        public function index(){
           
            if($this->request->is('ajax'))
            $this->layout = 'ajax';

            $this->loadModel("Reporte");
            
            
            $id_sub_tipo_impuesto = $this->getFromRequestOrSession('Reporte.id_sub_tipo_impuesto');
            
            $conditions = array();
            
            if($id_sub_tipo_impuesto !="")
                 array_push($conditions, array('Reporte.id_sub_tipo_impuesto ' => $id_sub_tipo_impuesto));
             
            $reportes = $this->Reporte->find('all', array(
                                                'conditions' => $conditions,
                                                'contain' => array("MimeType")
                                                ));
                                                  
           if($this->RequestHandler->ext == 'json'){
               
            
            foreach($reportes as &$reporte){
                
                if(isset($reporte["MimeType"])){
                    $reporte["Reporte"]["d_mime_type"] = $reporte["MimeType"]["d_mime_type"];
                    $reporte["Reporte"]["d_extension"] = $reporte["MimeType"]["extension"];
                    unset($reporte["MimeType"]);
                }
                
            }   
               
           
              
            $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "list",
                    "content" => $reportes,
                    "page_count" =>0
                );
            echo json_encode($output, JSON_UNESCAPED_SLASHES);
            die();
                
           }
        }

        public function listadoClientes($mode=null){

            $rh = new ReportHelper();
            $rh->setController($this); // Controlador origen
            $rh->setShowReportMethod("listadoClientes"); // Nombre del metodo a llamar para mostrar el reorte
            $rh->setReportName("listado_clientes.jrxml");

            if (isset($_REQUEST[$rh->const_show_report])){

                // Muestro el reporte
                $rh->showReport();

            }
            else{

                // Formulario de Filtros

                $model = "Cliente";
                $formBuilder = new FormBuilder();

                //Configuracion del Formulario
                $formBuilder->setController($this);
                $formBuilder->setModelName($model);
                $formBuilder->setControllerName($this->name);
                $formBuilder->setTitulo('Listado de Clientes');

                $formBuilder->setTituloForm('Listado de Clientes');

                //Form
                $formBuilder->setForm2($model,  array('class' => 'form-horizontal multicolumn well span9'));

                //Fields
                $formBuilder->addFormBeginRow();

                //Filtros
                $formBuilder->addFormHtml('<div></div>');			   
                $formBuilder->addFormInput('ClienteDesde', 'Desde Cliente', array('class'=>"control-group span5"), array('label' => false));
                $formBuilder->addFormInput('ClienteHasta', 'Hasta Cliente', array('class'=>"control-group span5"), array('label' => false));

                $formBuilder->addFormEndRow();


                $script = "

                //Valida el Formulario
                function validateForm(){

                // cuando se presiona consultar se ejecuta este js
                Validator.clearValidationMsgs('validationMsg_');

                var form = '{$model}ListadoClientesForm';

                var validator = new Validator(form);

                validator.validateRequired('{$model}ClienteDesde', 'Debe especificar un Cliente desde');
                validator.validateRequired('{$model}ClienteHasta', 'Debe especificar un Cliente hasta');

                if(!validator.isAllValid()){
                validator.showValidations('', 'validationMsg_');
                return false;   
                }

                return true;
                } 

                //Pasa los filtros indicados a la ejecucion del reporte
                function getReportFilters(){

                var filters = new Array();

                if( jQuery('#{$model}ClienteDesde').val() != '' )
                filters['p_cliente_desde'] = \$('#{$model}ClienteDesde').val();

                if( jQuery('#{$model}ClienteHasta').val() != '' )
                filters['p_cliente_hasta'] = \$('#{$model}ClienteHasta').val();

                return filters;
                }

                ";


                $formBuilder->addFormCustomScript($script);
                $formBuilder->setFormAcceptButtonParameters(array('controller'=>$this->name, 'action'=>'listadoProductos', $this->const_show_report));
                $formBuilder->addFormReadyScript($rh->getReportScripts());

                $formBuilder->setFormValidationFunction('validateForm()');
                $formBuilder->setFormAcceptButtonText("Ver Reporte");
                $formBuilder->setFormCancelButtonParameters(array('controller' =>'Usuarios', 'action' => 'home'));
                $this->set('abm',$formBuilder);
                $this->set('mode', 'X');
                $this->render('/FormBuilder/reporte');  

            }



        }

        public function factura($mode=null){

            $rh = new ReportHelper();
            $rh->setController($this); // Controlador origen
            $rh->setShowReportMethod("factura"); // Nombre del metodo a llamar para mostrar el reorte
            $rh->setReportName("factura.jrxml");

            if (isset($_REQUEST[$rh->const_show_report])){

                // Muestro el reporte
                $rh->showReport();

            }
            else{

                // Formulario de Filtros

                $model = "Factura";
                $formBuilder = new FormBuilder();

                //Configuracion del Formulario
                $formBuilder->setController($this);
                $formBuilder->setModelName($model);
                $formBuilder->setControllerName($this->name);
                $formBuilder->setTitulo('Listado de Facturas');

                $formBuilder->setTituloForm('Listado de Facturas');

                //Form
                $formBuilder->setForm2($model,  array('class' => 'form-horizontal multicolumn well span9'));

                //Fields
                $formBuilder->addFormBeginRow();

                //Filtros
                $formBuilder->addFormHtml('<div></div>');               
                $formBuilder->addFormInput('id_factura', 'Id Factura', array('class'=>"control-group span5"), array('label' => false));


                $formBuilder->addFormEndRow();


                $script = "

                //Valida el Formulario
                function validateForm(){

                // cuando se presiona consultar se ejecuta este js
                Validator.clearValidationMsgs('validationMsg_');

                var form = '{$model}FacturaForm';

                var validator = new Validator(form);

                validator.validateRequired('{$model}IdFactura', 'Debe especificar una Factura');


                if(!validator.isAllValid()){
                validator.showValidations('', 'validationMsg_');
                return false;   
                }

                return true;
                } 

                //Pasa los filtros indicados a la ejecucion del reporte
                function getReportFilters(){

                var filters = new Array();

                if( jQuery('#{$model}IdFactura').val() != '' )
                filters['p_id_factura'] = \$('#{$model}IdFactura').val();



                return filters;
                }

                ";


                $formBuilder->addFormCustomScript($script);
                $formBuilder->setFormAcceptButtonParameters(array('controller'=>$this->name, 'action'=>'Facturas', $this->const_show_report));
                $formBuilder->addFormReadyScript($rh->getReportScripts());

                $formBuilder->setFormValidationFunction('validateForm()');
                $formBuilder->setFormAcceptButtonText("Ver Reporte");
                $formBuilder->setFormCancelButtonParameters(array('controller' =>'Usuarios', 'action' => 'home'));
                $this->set('abm',$formBuilder);
                $this->set('mode', 'X');
                $this->render('/FormBuilder/reporte');  

            }



        }
        public function pedidoInternoArmado($mode=null){

            $rh = new ReportHelper();
            $rh->setController($this); // Controlador origen
            $rh->setShowReportMethod("pedidoInternoArmado"); // Nombre del metodo a llamar para mostrar el reorte
            $rh->setReportName("pedido_interno.jrxml");

            if (isset($_REQUEST[$rh->const_show_report])){

                // Muestro el reporte
                $rh->showReport();

            }
            else{

                // Formulario de Filtros

                $model = "Comprobante";
                $formBuilder = new FormBuilder();

                //Configuracion del Formulario
                $formBuilder->setController($this);
                $formBuilder->setModelName($model);
                $formBuilder->setControllerName($this->name);
                $formBuilder->setTitulo('Pedido Interno');

                $formBuilder->setTituloForm('Pedido Interno');

                //Form
                $formBuilder->setForm2($model,  array('class' => 'form-horizontal multicolumn well span9'));

                //Fields
                $formBuilder->addFormBeginRow();

                //Filtros
                $formBuilder->addFormHtml('<div></div>');               
                $formBuilder->addFormInput('IdPedidoInterno', 'Numero de Pedido Interno', array('class'=>"control-group span5"), array('label' => false));


                $formBuilder->addFormEndRow();


                $script = "

                //Valida el Formulario
                function validateForm(){

                // cuando se presiona consultar se ejecuta este js
                Validator.clearValidationMsgs('validationMsg_');

                var form = '{$model}PedidoInternoArmadoForm';

                var validator = new Validator(form);

                validator.validateRequired('{$model}IdPedidoInterno', 'Debe especificar un Numero de Pedido Interno');


                if(!validator.isAllValid()){
                validator.showValidations('', 'validationMsg_');
                return false;   
                }

                return true;
                } 

                //Pasa los filtros indicados a la ejecucion del reporte
                function getReportFilters(){

                var filters = new Array();

                //if( jQuery('#{$model}IdPedidoInterno').val() != '' )

                filters['p_id_pedido_interno'] = \$('#{$model}IdPedidoInterno').val();
                filters['p_fecha_emision'] = '22/10/2014'; 
                filters['p_cliente_cuit'] =   '20-30528275';
                filters['p_razon_social'] =    'ATAKAYAMA S.A';



                return filters;
                }

                ";


                $formBuilder->addFormCustomScript($script);
                $formBuilder->setFormAcceptButtonParameters(array('controller'=>$this->name, 'action'=>'pedidoInternoArmado', $this->const_show_report));
                $formBuilder->addFormReadyScript($rh->getReportScripts());

                $formBuilder->setFormValidationFunction('validateForm()');
                $formBuilder->setFormAcceptButtonText("Ver Reporte");
                $formBuilder->setFormCancelButtonParameters(array('controller' =>'Usuarios', 'action' => 'home'));
                $this->set('abm',$formBuilder);
                $this->set('mode', 'X');
                $this->render('/FormBuilder/reporte');  
            }
        }

        /**
        * @secured(REPORTE_MATRIZNECESIDADES)
        */
        public function MatrizNecesidades(){

            $this->loadModel("Componente");

            $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

            //Recuperacion de Filtros
            $id = $this->getFromRequestOrSession('R.id');
            $codigo = $this->getFromRequestOrSession('R.codigo');
            $descripcion = $this->getFromRequestOrSession('R.descripcion');
            $id_origen= $this->getFromRequestOrSession('R.id_origen');

            $conditions = array(); 

            if($id!="")
                array_push($conditions, array('Componente.id =' => $id )); 

            if($codigo!="")
                array_push($conditions, array('Componente.codigo LIKE' => '%' . $codigo  . '%')); 

            if($descripcion!="")
                array_push($conditions, array('Componente.d_producto LIKE' => '%' . $descripcion  . '%'));

            if($id_origen!="")
                array_push($conditions, array('Componente.id_origen =' => $id_origen ));


            $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
                'contain' =>array('ArticuloRelacion'=>array('MateriaPrima'=>array('StockMateriaPrima')),'StockComponente'),
                'conditions' => $conditions,
                'limit' => $this->numrecords,
                'page' => $this->getPageNum(),
                'order' => 'Componente.codigo desc'
            );
            if($this->RequestHandler->ext == 'json'){

                $data = $this->PaginatorModificado->paginate("Componente");
                $page_count = $this->params['paging']["Componente"]['pageCount'];

                foreach($data as &$valor){

                    $stock_mp =  0;
                    $stock_mpc =  0;
                    if(isset($valor["ArticuloRelacion"])){
                        foreach ($valor["ArticuloRelacion"] as $mpc){
                            $stock_mp +=$mpc["MateriaPrima"]["StockMateriaPrima"]["stock"];
                            $stock_mpc +=$mpc["MateriaPrima"]["StockMateriaPrima"]["stock_comprometido"];

                        }
                    }    


                    $valor['R']['id'] = $valor['Componente']['id']; //La vamos a agregar un id
                    $valor['R']['Descripcion'] = $valor['Componente']['descripcion'];
                    $valor['R']['Codigo'] = $valor['Componente']['codigo'];
                    $valor['R']['C_Comprometido_PI'] = $valor['StockComponente']['stock_comprometido'];//ESTA MAL TIENE Que ser hacer un count de los componentes q esta asociandos con PI interno q no esten en estado cerrado
                    $valor['R']['Materia_Prima'] = $stock_mp; //MAL  Materia Prima: el stock de MP Disponible para fabricar,

                    $valor['R']['Materia_Prima_Comprometida'] = $stock_mpc;// MAL En Proceso, Materia Prima Comprometida. Es decir MP en proceso de Fabricación.

                    $valor['R']['Terminado'] = $valor['StockComponente']['stock']; 
                    $valor['R']['Stock_Minimo'] = $valor['Componente']['stock_minimo']; 

                    unset($valor['Componente']);
                    unset($valor['StockComponente']);


                }
                $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "list",
                    "content" => $data,
                    "page_count" =>$page_count
                );
                $this->set($output);
                $this->set("_serialize", array("status", "message","page_count", "content"));

            } 


        }



      
        private function LibroIva($id_sistema){ //le paso el id_sistema para saber si es compra o ventas

            //App::import('Lib', array('file' => 'ExcelExport.php')); 
            // App::uses('ExcelExport', 'Lib');
            //App::import('Lib', 'ExcelExport', array('file' => 'ExcelExport.php'));//importo

            $this->loadModel("Comprobante");
            $this->loadModel("DatoEmpresa");
            $this->model = "Comprobante";
            
            set_time_limit(600); 
            
            $percepciones_dato_empresa = $this->DatoEmpresa->getImpuestos($id_sistema,EnumTipoImpuesto::PERCEPCIONES);

            /*
            $excel = new ExcelExport();

            $excel->setCamposColumnas(array( "fechaEmision"=>"A","Tipo"=>"B","Razon Social"=>"B",
            "Nro Comprobante"=>"C","Neto Gravado"=>"D","Exento"=>"D",
            "IVA"=>"D","IIBB"=>"D","TOTAL"=>"C"
            ));

            */                                

            $this->PaginatorModificado->settings = array('limit' => 200, 'update' => 'main-content', 'evalScripts' => true);

            //Recuperacion de Filtros
            $fecha_desde = $this->getFromRequestOrSession('R.fecha_contable'); //Hay q usar R para mandar datos para el controller Reporting
            $fecha_hasta = $this->getFromRequestOrSession('R.fecha_contable_hasta'); //A lo sumo se puede usar "R.factura.fecha_hasta"
            $con_cae = $this->getFromRequestOrSession('R.conCAE'); //A lo sumo se puede usar "R.factura.fecha_hasta"
            $id_punto_venta = $this->getFromRequestOrSession('R.id_punto_venta'); //A lo sumo se puede usar "R.factura.fecha_hasta"
            $anio_mes = $this->getFromRequestOrSession('R.anio_mes'); //A lo sumo se puede usar "R.factura.fecha_hasta"
            $id_tipo_comprobante = $this->getFromRequestOrSession('R.id_tipo_comprobante'); //A lo sumo se puede usar "R.factura.fecha_hasta"
            $chk_enviar_titulo = $this->getFromRequestOrSession('R.chk_enviar_titulo'); //A lo sumo se puede usar "R.factura.fecha_hasta"
            $titulo = $this->getFromRequestOrSession('R.titulo'); //A lo sumo se puede usar "R.factura.fecha_hasta"
            $subtitulo1 = $this->getFromRequestOrSession('R.subtitulo1'); //A lo sumo se puede usar "R.factura.fecha_hasta"
            $subtitulo2 = $this->getFromRequestOrSession('R.subtitulo2'); //A lo sumo se puede usar "R.factura.fecha_hasta"
            $pdf = $this->getFromRequestOrSession('R.pdf'); //A lo sumo se puede usar "R.factura.fecha_hasta"
            $tipo_reporte = $this->getFromRequestOrSession('R.tipo_reporte'); //A lo sumo se puede usar "R.factura.fecha_hasta"
            $id_moneda = $this->getFromRequestOrSession('R.id_moneda'); //Nombre del campo por el cual va a convertir
            $id_periodo_impositivo = $this->getFromRequestOrSession('R.id_periodo_impositivo'); //
            $id_periodo_contable = $this->getFromRequestOrSession('R.id_periodo_contable'); //
            
            //$key_tipo_moneda = EnumTipoMoneda::Moneda2;
            $comprobante  = new Comprobante();
            
            
            
        
            $id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda);
            $field_valor_moneda = $comprobante->getFieldByTipoMoneda($id_tipo_moneda);
            //https://docs.google.com/spreadsheets/d/1oEhBYvp0eF3ejzymPeX3LRfaY4wL99ElbN2pivhAJSc/edit#gid=0


            $conditions = array(); 

            //$anio_mes = '2016-08';
            //$tipo_reporte = EnumReporte::IVAVENTAS;

            //estos son los filtros de los reportes
            if($fecha_desde!="")
                array_push($conditions, array('Comprobante.fecha_contable >=' => $fecha_desde )); 

                
            if($fecha_hasta!="")
                array_push($conditions, array('Comprobante.fecha_contable <=' => $fecha_hasta));

            if($id_punto_venta!="")
                array_push($conditions, array('Comprobante.id_punto_venta =' => $id_punto_venta));      

            if($anio_mes!=""){

            	$array_fecha = explode("-",$anio_mes);
                array_push($conditions, array('MONTH(Comprobante.fecha_contable) ='=> $array_fecha[1] ));
                array_push($conditions, array('YEAR(Comprobante.fecha_contable) ='=>  $array_fecha[0] ));

            }  

            
           
            

            array_push($conditions, array('Comprobante.id =' => 137898 ));   
            
            if($id_tipo_comprobante!="")
                array_push($conditions, array('Comprobante.id_tipo_comprobante =' => $id_tipo_comprobante ));    

           
            
            
            if($id_periodo_impositivo!="") {
      
             
               array_push($conditions, array('Comprobante.id_periodo_impositivo ' => $id_periodo_impositivo));    
             
             }
             
             
             if($id_periodo_contable !=""){
                 
                 $this->loadModel("PeriodoContable");
                
                $fecha_desde = $this->PeriodoContable->getFechaInicio($id_periodo_contable);
                $fecha_hasta = $this->PeriodoContable->getFechaFin($id_periodo_contable);
                array_push($conditions, array('Comprobante.fecha_contable >=' => $fecha_desde ));
                array_push($conditions, array('Comprobante.fecha_contable <=' => $fecha_hasta));
             }    


            //if($con_cae == 1)
                

            //array_push($conditions, array('Comprobante.definitivo' => 1)); 

          if($tipo_reporte == EnumReporte::IVAVENTAS){      
                array_push($conditions, array('Comprobante.id_tipo_comprobante' => array_merge($this->Comprobante->getTiposComprobanteController(EnumController::Facturas),$this->Comprobante->getTiposComprobanteController(EnumController::Notas))
                    ));
                    
                array_push($conditions, array('Comprobante.id_estado_comprobante' => array(EnumEstadoComprobante::CerradoConCae,EnumEstadoComprobante::Cumplida)));
        
          }else{ //Es Compras
          	
          	//si se agrega algun tipo de comprobante recordar que los reportes de impuestos deben coincidir, por ejemplo percepciones sufridas sifere, debe coincidir con el IVA compras
                array_push($conditions, array('Comprobante.id_tipo_comprobante' => 
                
                
                		array_merge($this->Comprobante->getTiposComprobanteController(EnumController::FacturasCompra),$this->Comprobante->getTiposComprobanteController(EnumController::NotasCompra),array(EnumTipoComprobante::GASTOA,EnumTipoComprobante::GASTOB,EnumTipoComprobante::GASTOC) )
                
             
                    )); 

              
                array_push($conditions, array('Comprobante.id_estado_comprobante' => array(EnumEstadoComprobante::CerradoConCae,EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::Cumplida)));
              
          }
          
          
          
          if($con_cae == 1)
          	array_push($conditions, array('Comprobante.cae >' => 0));//solo los de emitidos por mi valido que sean electronicos
          	

            /* 
            $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' =>array('Persona','ComprobanteImpuesto'=>array('Impuesto'),'TipoComprobante','PuntoVenta'),
            'conditions' => $conditions,
            'limit' => 100000000000000000000,
            //'page' => $this->getPageNum(),
            'order' => 'Comprobante.fecha_generacion asc'
            );
            */

            // $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);

          	
          	if ( $id_sistema == EnumSistema::VENTAS)
            	$data = $this->Comprobante->find('all', array(                                                                                                                              //'ComprobanteImpuesto.id_impuesto','ComprobanteImpuesto.tasa_impuesto','ComprobanteImpuesto.importe_impuesto'
                    'fields'=>array('TipoComprobante.codigo_tipo_comprobante','TipoComprobante.id','TipoComprobante.signo_comercial','Comprobante.nro_comprobante','Comprobante.subtotal_neto','Comprobante.fecha_contable','Comprobante.total_comprobante','Comprobante.cae','Persona.razon_social','Persona.cuit','PuntoVenta.numero','Comprobante.valor_moneda','Comprobante.valor_moneda2','Comprobante.d_punto_venta','Persona.id_tipo_iva'),
                    'conditions' =>$conditions,
                    'contain' =>array('Persona'=>array('TipoIva'),'ComprobanteImpuesto','TipoComprobante','PuntoVenta','TipoDocumento'),
                    'order' => array('Comprobante.fecha_contable asc','Comprobante.nro_comprobante asc')
                ));
            else
            	$data = $this->Comprobante->find('all', array(                                                                                                                              //'ComprobanteImpuesto.id_impuesto','ComprobanteImpuesto.tasa_impuesto','ComprobanteImpuesto.importe_impuesto'
            			'fields'=>array('TipoComprobante.codigo_tipo_comprobante','TipoComprobante.id','TipoComprobante.signo_comercial','Comprobante.nro_comprobante','Comprobante.subtotal_neto','Comprobante.fecha_contable','Comprobante.fecha_generacion','Comprobante.total_comprobante','Comprobante.cae','Persona.razon_social','Persona.cuit','PuntoVenta.numero','Comprobante.valor_moneda','Comprobante.valor_moneda2','Comprobante.d_punto_venta','Persona.id_tipo_iva'),
            			'conditions' =>$conditions,
            			'contain' =>array('Persona'=>array('TipoIva'),'ComprobanteImpuesto','TipoComprobante','PuntoVenta','TipoDocumento'),
            			'order' => array('Comprobante.fecha_generacion asc','Comprobante.nro_comprobante asc')
            	));
            	
            //$page_count = $this->params['paging'][$this->model]['pageCount'];


            


            foreach($data as &$valor){

            	
            	
            	
         
            	
	                $fecha_contable = new DateTime($valor['Comprobante']['fecha_contable']);
	
	                
	                if ( $id_sistema == EnumSistema::VENTAS)
	                	$valor["Comprobante"]["nro_comprobante"] = $comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
	                else 
	                	$valor["Comprobante"]["nro_comprobante"] = $comprobante->GetNumberComprobante($valor['Comprobante']['d_punto_venta'],$valor[$this->model]['nro_comprobante']);
	                
	                
	                $valor["Comprobante"]["tipo_comprobante"] = $valor["TipoComprobante"]["codigo_tipo_comprobante"];
	                $valor["Comprobante"]["id_tipo_comprobante"] = $valor["TipoComprobante"]["id"];
	                
	                
	                
	                
	                if( 	
	                		isset($valor["Comprobante"]["id_detalle_tipo_comprobante"]) 
	                		&& $valor["Comprobante"]["id_detalle_tipo_comprobante"] != EnumDetalleTipoComprobante::ConsumidorFinalFacturaB
	                		|| 	$valor["Comprobante"]["id_detalle_tipo_comprobante"] != EnumDetalleTipoComprobante::ConsumidorFinalFacturaC
	                	){
		                $valor["Comprobante"]["razon_social"] = $valor["Persona"]["razon_social"];
		                $valor["Comprobante"]["cuit"] = $valor["Persona"]["cuit"];
		                $valor["Comprobante"]["persona_d_tipo_iva"]  =  $valor["Persona"]["TipoIva"]["d_tipo_iva"];
		                
	                }else{
	                	
	                	$valor["Comprobante"]["razon_social"] = $valor["Comprobante"]["razon_social"];
	                	$valor["Comprobante"]["cuit"] = $valor["Comprobante"]["cuit"];
	                	$valor["Comprobante"]["persona_d_tipo_iva"]  =  "Consumidor Final";
	                	
	                }
	                
	                
	                
	                
	                
	                
	                
	                $valor["Comprobante"]["fecha_contable"] = $fecha_contable->format('d-m-Y');
	
	                $iibb_total = 0;
	                $iva_27 = 0;
	                $iva_21 = 0;
	                $iva_105 = 0;
	                $iva_5 = 0;
	                $iva_2coma5 = 0;
	                
	                $total_ib = array();
	                $simbolo = $valor["TipoComprobante"]["signo_comercial"];
	               
	                $redondeo_decimal = 4;
	                foreach($valor["ComprobanteImpuesto"] as $impuesto){
	                    
	               if ( $id_sistema == EnumSistema::VENTAS)
	                	$monto = round ( ($impuesto["base_imponible"]*$impuesto["tasa_impuesto"])/100,2);
	              else 
	              		$monto = $impuesto["importe_impuesto"];
	
	                    switch($impuesto["id_impuesto"]){
	
	                        case EnumImpuesto::IVANORMAL:
	                        case EnumImpuesto::IVACOMPRAS:
	                        switch($impuesto["tipo_alicuota_afip"]){
	                        	
	                        	
	                        	
	
	                            
	                        	case EnumTipoAlicuotaAfip::veintisiete:
	                        		$iva_27 += $comprobante->ExpresarEnTipoMonedaMejorado($monto,$field_valor_moneda, $valor["Comprobante"],0,0,0,$redondeo_decimal);
	                        		break;
	                        	case EnumTipoAlicuotaAfip::veintiuno:
	                        		$iva_21 += $comprobante->ExpresarEnTipoMonedaMejorado($monto,$field_valor_moneda, $valor["Comprobante"],0,0,0,$redondeo_decimal);
	                                break;
	                        	case EnumTipoAlicuotaAfip::diezcomacinco:
	                            	$iva_105 += $comprobante->ExpresarEnTipoMonedaMejorado($monto,$field_valor_moneda, $valor["Comprobante"],0,0,0,$redondeo_decimal);
	                                break;
	                        	case EnumTipoAlicuotaAfip::cinco:
	                            	$iva_5 += $comprobante->ExpresarEnTipoMonedaMejorado($monto,$field_valor_moneda, $valor["Comprobante"],0,0,0,$redondeo_decimal);
	                            	break;
	                        	case EnumTipoAlicuotaAfip::doscomacinco:
	                            	$iva_2coma5 += $comprobante->ExpresarEnTipoMonedaMejorado($monto,$field_valor_moneda, $valor["Comprobante"],0,0,0,$redondeo_decimal);
	                            	break;
	                            	
	                        }
	                        break;
	
	                        default://cualquier impuesto que NO sea IVA y que este en la columna agregada
	                        /*case EnumImpuesto::PERCEPIIBBBUENOSAIRES;
	                        case EnumImpuesto::PERCEPIIBBCOMPRA;*/
	                        
	                        	$total_ib[$impuesto["id_impuesto"]] = $comprobante->ExpresarEnTipoMonedaMejorado($monto,$field_valor_moneda, $valor["Comprobante"],0,0,0,$redondeo_decimal)*$simbolo;
	                            break;
	
	
	                    }     
	                }
	
	
	
	                
	               
	                    
	                     
	                $valor["Comprobante"]["iva_27"] = $iva_27 * $simbolo;
	                $valor["Comprobante"]["iva_21"] = $iva_21 * $simbolo;
	                $valor["Comprobante"]["iva_105"] = $iva_105 * $simbolo;
	                $valor["Comprobante"]["iva_5"] = $iva_5* $simbolo;
	                $valor["Comprobante"]["iva_2coma5"] = $iva_2coma5* $simbolo;
	                $valor["Comprobante"]["array_impuestos"] =  $total_ib;
	
	                $valor["Comprobante"]["subtotal_neto"] = $comprobante->ExpresarEnTipoMonedaMejorado($valor["Comprobante"]["subtotal_neto"],$field_valor_moneda, $valor["Comprobante"],0,0,0,$redondeo_decimal)*$simbolo;
	
	
	                $valor["Comprobante"]["no_gravado"] = 0;
	                
	                $total_no_gravado = $this->Comprobante->getNoGravado($valor["Comprobante"]["id"])*$simbolo;
	                
	               
	                $valor["Comprobante"]["no_gravado"] =  $comprobante->ExpresarEnTipoMonedaMejorado($total_no_gravado,$field_valor_moneda, $valor["Comprobante"],0,0,0,$redondeo_decimal);
	               	$valor["Comprobante"]["subtotal_neto"] = 0;
	                
	
	                
	                $valor["Comprobante"]["exento"] = 0;
	                $total_exento = $this->Comprobante->getExento($valor["Comprobante"]["id"]);
	                $total_exento = $comprobante->ExpresarEnTipoMonedaMejorado($total_exento,$field_valor_moneda,$valor["Comprobante"],0,0,0,$redondeo_decimal);
	                
	                if($total_exento >0)
	                    $valor["Comprobante"]["exento"]  = $total_exento*$simbolo;
	
	
	
	
	                //$valor["Comprobante"]["total_ib"] = $iibb_total * $simbolo;
	                    $valor["Comprobante"]["total_comprobante"] =  $comprobante->ExpresarEnTipoMonedaMejorado($valor["Comprobante"]["total_comprobante"],$field_valor_moneda, $valor["Comprobante"],0,0,0,$redondeo_decimal)*$simbolo;
	                
	                
	                $valor["Comprobante"]["gravado"] = $this->Comprobante->getGravado($valor['Comprobante']['id'])*$simbolo;
	                
	                $valor["Comprobante"]["gravado"] = $comprobante->ExpresarEnTipoMonedaMejorado($valor["Comprobante"]["gravado"],$field_valor_moneda,$valor["Comprobante"],0,0,0,$redondeo_decimal);
	                
	                
	                
	                
	
	                unset( $valor["Persona"]);
	                unset( $valor["TipoComprobante"]);
	                unset( $valor["PuntoVenta"]);
	                unset( $valor["ComprobanteImpuesto"]);
	                //unset( $valor["Comprobante"]["Persona"]);
	
	                /*
	                $valor["Comprobante"]["fecha_generacion"];//Fecha comprobante
	                $valor["TipoComprobante"]["codigo_tipo_comprobante"];//Fecha comprobante
	                $valor["Persona"]["cuit"];//Fecha comprobante
	                $valor["Persona"]["razon_social"];//Fecha comprobante
	                */
         

            }

            $pdf=0;
            $datos_extra["titulo"] = $titulo;
            $datos_extra["subtitulo1"] = $subtitulo1;
            $datos_extra["subtitulo2"] = $subtitulo2;
            $datos_extra["percepciones"] = $percepciones_dato_empresa;
         
            
            
            if ( $id_sistema == EnumSistema::VENTAS)
            	$this->xlsExport($data,"LibroIvaXls",$datos_extra);
            else
            	$this->xlsExport($data,"LibroIvaCompraXls",$datos_extra);
                             
        


            /*
            $this->set('html',$html); 
            $this->set('id',$id);
            Configure::write('debug',0);
            $this->layout = 'pdf'; //esto usara el layout pdf.ctp
            $this->render();

            */

        }
        
          /**
        * @secured(REPORTE_IVA_VENTAS)
        */
        public function LibroIvaVentas(){
            
           
           $id_reporte = EnumReporte::IVAVENTAS;
            
           $id_sistema = EnumSistema::VENTAS; 
           $this->LibroIva($id_sistema); 
            
        }
        
        
        /**
        * @secured(REPORTE_IVA_COMPRAS)
        */
        public function LibroIvaCompras(){
           
            $id_reporte = EnumReporte::IVACOMPRAS; 
           $id_sistema = EnumSistema::COMPRAS; 
           $this->LibroIva($id_sistema); 
            
        }
        
        
        
        public function BalanceGeneral($nombre_funcion="CalculaSaldo",$id_ejercicio="",$id_estado_asiento = "",$id_moneda = ""){
            
            parent::BalanceGeneral($nombre_funcion,$id_ejercicio,$id_estado_asiento,$id_moneda);
        }
       
       
       
       
       
       public function xls_prueba($nombres_columnas='',$resultset=''){
           
           
           $valor1["d_nombre"] = "hola";
           $valor1["key_nombre"] = "id_moneda";
           
           $valor2["d_nombre"] = "chau";
           $valor2["key_nombre"] = "id_moneda2";
           
           
           $nombres_columnas = array($valor1,$valor2);
           
           $resultset = array();
           
           
           
           $resultset_aux["id_moneda"] = "PESOS";
           $resultset_aux["id_moneda2"] = "Dolares";
           
           
           array_push($resultset,$resultset_aux);
           
           
           $resultset_aux["id_moneda"] = "Maconia";
           $resultset_aux["id_moneda2"] = "Maconia2";
           
           
           array_push($resultset,$resultset_aux);
           
           
           
           $file_ending = "xls";
            //header info for browser
            header("Content-Type: application/xls");    
            header("Content-Disposition: attachment; filename=prueba.".$file_ending);  
            header("Pragma: no-cache"); 
            header("Expires: 0");
            
            
            $sep = "\t"; //tabbed character
            //start of printing column names as names of MySQL fields
          foreach($nombres_columnas as $nombre){
            echo $nombre["d_nombre"] . "\t";
            }
            print("\n"); 
         // http://stackoverflow.com/questions/15699301/export-mysql-data-to-excel-in-php  
         
          foreach($resultset as $fila){ 
            
              foreach($nombres_columnas as $columna){
                  
                echo $fila[$columna["key_nombre"]] ."\t";
              }
                echo  "\r\n";  
              
          }
        die();

       } 
        
        
        
        
        
        
        
         



        public function VentasPorJurisdiccion(){ 



     

            $this->loadModel("Comprobante");
            $this->loadModel("DatoEmpresa");
            $this->model = EnumModel::Comprobante;





            $conditions = array(); 

            $conditions = $this->RecuperoFiltros($this->model);

            
		    /*Excluyo las notas de credito por cheque rechazado*/
           	array_push($conditions, array(' IFNULL(Comprobante.`id_detalle_tipo_comprobante`,0) !=' => EnumDetalleTipoComprobante::ChequesRechazados )); 

         
            array_push($conditions, array('Comprobante.cae >' => 0)); 
            array_push($conditions, array('Comprobante.id_tipo_comprobante !=' => EnumTipoComprobante::FacturaE)); 
            
                
           array_push($conditions, array('Comprobante.id_tipo_comprobante' => array_merge($this->Comprobante->getTiposComprobanteController(EnumController::Facturas),$this->Comprobante->getTiposComprobanteController(EnumController::Notas))
                ));
                
           array_push($conditions, array('Comprobante.id_estado_comprobante' => array(EnumEstadoComprobante::CerradoConCae,EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::Cumplida)));
             
           App::uses('CakeSession', 'Model/Datasource');
           $datoEmpresa = CakeSession::read('Empresa');
           $id_pais  = $datoEmpresa["DatoEmpresa"]["id_pais"];
           
           
           array_push($conditions, array('Persona.id_pais' => $id_pais)); 
           

           $id_moneda = $this->getFromRequestOrSession('Comprobante.id_moneda_expresion');
           $id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda);
           $campo_valor = $this->Comprobante->getFieldByTipoMoneda($id_tipo_moneda);//obtengo el campo en la cual expresar el reporte
                

            /*array_push($conditions, array('Comprobante.id_tipo_comprobante' =>    $this->Comprobante->getTiposComprobanteController(EnumController::Facturas)
                )); */



         
            $facturas = $this->Comprobante->find('all', array(                                                                                                                              //'ComprobanteImpuesto.id_impuesto','ComprobanteImpuesto.tasa_impuesto','ComprobanteImpuesto.importe_impuesto'
                    'fields'=>array('Provincia.id','Provincia.d_provincia',' SUM((subtotal_neto*TipoComprobante.signo_comercial)/Comprobante.'.$campo_valor.') AS monto '),
                    'conditions' =>$conditions,
                    'joins'=>array(   
                    		
                    		
                    		array(
                            'table' => 'provincia',
                            'alias' => 'Provincia',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Persona.id_provincia = Provincia.id'
                            ))
                    		
                    		
                    		
                    ),
                    'contain' =>array('TipoComprobante','Persona'=>array('Provincia')),
                    'order' => 'Provincia.d_provincia asc',
                    'group'=> 'Provincia.id'
                ));
            //$page_count = $this->params['paging'][$this->model]['pageCount'];






            foreach($facturas as &$valor){



                $valor["Comprobante"]["total_comprobante"] = $valor[0]["monto"];    
                $valor["Comprobante"]["d_provincia"] = $valor["Provincia"]["d_provincia"];    



            }
            
            
            
            $this->data = $facturas;
            
            $output = array(
            		"status" =>EnumError::SUCCESS,
            		"message" => "list",
            		"content" => $this->data,
            		"page_count" =>0
            );
            $this->set($output);
            $this->set("_serialize", array("status", "message","page_count", "content"));
            

       

        } 
        
        
        
        private function getQueryNecesidadMecanizado($array_origen,$stock_menor_a_cantidad,$n_column_order)
        {
        	return "SELECT producto.id,
                producto.codigo,
                producto.d_producto,
                stock,
               (SELECT s.stock  FROM stock_producto s WHERE s.id_deposito=6 AND s.id= stock_producto.id ) AS comprometido ,
                DATE_FORMAT(orden_armado.`fecha_entrega`,'%d-%m-%Y') AS fecha_entrega_componentes_orden_armado,
        			
               (SELECT nro_comprobante FROM auditoria_producto
                    JOIN comprobante as orden_armado ON orden_armado.id = id_orden_armado
                    WHERE auditoria_producto.`id_producto` = producto.id AND orden_armado.`id_estado_comprobante`<>".EnumEstadoComprobante::Anulado."
						AND orden_armado.id_estado_comprobante <>".EnumEstadoComprobante::CerradoReimpresion."
AND orden_armado.id_estado_comprobante <>".EnumEstadoComprobante::Cumplida." 
                     ORDER  BY orden_armado.`fecha_entrega` ASC LIMIT 1
        			
               ) AS nro_orden_armado
                FROM stock_producto
                JOIN producto ON producto.id=stock_producto.`id`
                 JOIN auditoria_producto ON auditoria_producto.`id_producto`= producto.`id`
                 JOIN comprobante as orden_armado ON orden_armado.id = auditoria_producto.`id_orden_armado`
        			
                WHERE stock_producto.id_deposito=4 AND id_producto_tipo=2 AND stock<".$stock_menor_a_cantidad." AND id_origen
                IN (".$array_origen.")
               AND orden_armado.id_estado_comprobante <>".EnumEstadoComprobante::Anulado." AND orden_armado.id_estado_comprobante <>".EnumEstadoComprobante::CerradoReimpresion." 

AND orden_armado.id_estado_comprobante <>".EnumEstadoComprobante::Cumplida." 
               GROUP BY producto.id
               ORDER BY ".$n_column_order." ASC";
        	
        }
        

public function NecesidadDeMecanizado($pdf=true, $n_column_order=5 )
        {
        	$this->loadModel("StockComponente");
        	
        	$stock_menor_a_cantidad = $this->getFromRequestOrSession('R.stock_menor_a_cantidad'); //Hay q usar R para mandar datos para el controller Reporting
        	
        	if($stock_menor_a_cantidad == '')
        		$stock_menor_a_cantidad = 0;
        		
        		/* // MP: Esta query trabaja con un trigger.
        		 //El triggere registra la AO que hace al Stock de X componente negativo.
        		 //Nota: Los que estan en negativo antes de la existencia de este trigger no se puede saber la OA que los convirtio
        		 SELECT producto.id,stock,(SELECT s.stock  FROM stock_producto s WHERE s.id_deposito=6 AND s.id= stock_producto.id ) AS comprometido
        		 , orden_armado.`fecha_entrega` AS fecha_entrega_componentes_orden_armado, orden_armado.`id` AS nro_orden_armado
        		 FROM stock_producto
        		 JOIN producto ON producto.id=stock_producto.`id`
        		 LEFT JOIN auditoria_producto ON auditoria_producto.`id_producto`= producto.`id`
        		 LEFT JOIN orden_armado ON orden_armado.id = auditoria_producto.`id_orden_armado`
        		 
        		 WHERE stock_producto.id_deposito=4   //ALMACEN
        		 AND id_producto_tipo=2             //COMPONENTE
        		 AND stock<".$stock_menor_a_cantidad.
        		 "AND id_categoria IN (319,320,321,323,324,325,326,327,328,329,330,331,332,333,334,
        		 335,336,337,338,339,340,341,344,345,346,349,350,351,352,353,354,355,356,357,358,
        		 360,372,373,374,375,378,379,380,381,382,383,384,385,386,389,390,394,395,396,399,
        		 400,402,403,404,405,406,412,414,415,416,417,418,419,420,421,422,423,427,428,429,
        		 430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,468,469,470,
        		 471,472,473,474,475,476,477,478,479,480,481,482) //CATEGORIAS DE COMPONENTES QUE LEAN QUIERE INCLUIR  EN EL LISTADO dDE FABRICACION
        		 */
        		/* // MP: ESTA ES LA QUERY Q ANTES ESTABA PRODUCTIVA
        		 SELECT producto.id,producto.codigo,stock,(SELECT s.stock  FROM stock_producto s WHERE s.id_deposito=6 AND s.id= stock_producto.id ) AS comprometido
        		 , orden_armado.`fecha_entrega` AS fecha_entrega_componentes_orden_armado, orden_armado.`id` AS nro_orden_armado
        		 FROM stock_producto
        		 JOIN producto ON producto.id=stock_producto.`id`
        		 JOIN auditoria_producto ON auditoria_producto.`id_producto`= producto.`id`
        		 JOIN orden_armado ON orden_armado.id = auditoria_producto.`id_orden_armado`
        		 
        		 WHERE stock_producto.id_deposito=4 AND id_producto_tipo=2 AND stock<".$stock_menor_a_cantidad." AND id_categoria
        		 IN (
        		 24,14,468,469,479,478,481,480,476,471,473,475,470,472,474,494,494,368,364,389,5,463,462,477,482,455,461,460,459,454,458,457,456,
        		 403,406,405,402,404,448,449,430,428,348,390,424,391,423,426,36,429,437,440,439,438,443,433,432,442,441,431,427,491,419,420,421,422,490,
        		 435,434,436,484,483,444,445,446,486,487,488,489,415,416,412,413,418,417,414,401,400,387,377,376,399,373,372,375,319,371,359,361,360,
        		 323,355,324,327,326,325,335,333,331,329,396,353,374,351,337,336,354,493,341,352,350,328,330,349,356,357,378,379,384,383,382,334,385,332,
        		 9,492,386,339,381,380,395,343,338,347,394,340,346,345,344,3,321,32)
        		 AND orden_armado.id_estado <>5
        		 ORDER BY fecha_entrega_componentes_orden_armado ASC
        		 
        		 //DATE_FORMAT(orden_armado.`fecha_entrega`,'%m-%d-%Y') AS fecha_entrega_componentes_orden_armado,
        		 //DATE(orden_armado.`fecha_entrega`) AS fecha_entrega_componentes_orden_armado,
        		 //orden_armado.`fecha_entrega` AS fecha_entrega_componentes_orden_armado,
        		 //ORDER BY fecha_entrega_componentes_orden_armado ASC
        		 */
        		
        		$registros = $this->StockComponente->query("CALL sp_componente_comprometido;");
        		
        		/*-------------------------------------------------- REPORTES  - NECESIDADES DE PRODUCCION -------------------------------------------------------------*/
        		
        		/*$array_origen= "1,3,5,9,24,36,44,57,319,320,321,323,324,325,326,327,328,329,330,331,
                332,333,334,335,336,337,339,341,343,344,345,346,347,348,349,350,351,352,
                353,354,355,356,357,359,360,361,372,373,374,377,378,379,380,381,382,383,
                384,385,386,387,388,389,390,391,395,396,399,400,402,403,404,405,406,411,412,
                413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,
                432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,448,449,454,455,456,457,
                458,459,460,461,462,466,467,463,468,469,470,471,472,474,477,478,479,480,481,482,483,484,490,
                491,492,493,494";*/
        		$array_origen = "1";
        	
        		
        		$query = $this->getQueryNecesidadMecanizado($array_origen,$stock_menor_a_cantidad,$n_column_order);
        		
        		$this->log($query);
        		
        		/*$registros = $this->StockComponente->query($query);*/
        		
        		
        		foreach($registros as &$valor)
        		{
        			$valor["NecesidadMecanizado"]["id_producto"] =  $valor["producto"]["id"];
        			$valor["NecesidadMecanizado"]["codigo"] =  		$valor["producto"]["codigo"];
        			$valor["NecesidadMecanizado"]["d_producto"] =   $valor["producto"]["d_producto"];
        			$valor["NecesidadMecanizado"]["stock"] =        $valor["stock_producto"]["stock"];
        			$valor["NecesidadMecanizado"]["comprometido"] = $valor[0]["comprometido"];
        			
        			$fecha = $valor[0]["fecha_entrega_componentes_orden_armado"];
        			$nuevafecha = strtotime ( '-2 day' , strtotime ( $fecha ) ) ;
        			$nuevafecha = date ( 'j-m-Y' , $nuevafecha );
        			
        			
        			$valor["NecesidadMecanizado"]["fecha_entrega_componentes_orden_armado"]
        			= $nuevafecha;
        			
        			if(isset($valor[0]["nro_orden_armado"]))
        				$valor["NecesidadMecanizado"]["nro_orden_armado"]
        				= $valor[0]["nro_orden_armado"];
        				else
        					$valor["NecesidadMecanizado"]["nro_orden_armado"] = "";
        		}
        		
        		/*-------------------------------------------------- REPORTES  - NECESIDADES DE PRODUCCION GRANEL  -------------------------------------------------------------*/
        		
        		$array_origen = "4";
        		
        		$query = $this->getQueryNecesidadMecanizado($array_origen,$stock_menor_a_cantidad,$n_column_order);
        		$registros2 = $this->StockComponente->query($query);
        		
        		foreach($registros2 as &$valor)
        		{
        			$valor["NecesidadMecanizadoGranel"]["id_producto"] =  	$valor["producto"]["id"];
        			$valor["NecesidadMecanizadoGranel"]["codigo"] =  		$valor["producto"]["codigo"];
        			$valor["NecesidadMecanizadoGranel"]["d_producto"] =  	$valor["producto"]["d_producto"];
        			$valor["NecesidadMecanizadoGranel"]["stock"] =        	$valor["stock_producto"]["stock"];
        			$valor["NecesidadMecanizadoGranel"]["comprometido"] = 	$valor[0]["comprometido"];
        			
        			$fecha = $valor[0]["fecha_entrega_componentes_orden_armado"];
        			$nuevafecha = strtotime ( '-2 day' , strtotime ( $fecha ) ) ;
        			$nuevafecha = date ( 'j-m-Y' , $nuevafecha );
        			
        			$valor["NecesidadMecanizadoGranel"]["fecha_entrega_componentes_orden_armado"]
        			= $nuevafecha;
        			
        			if(isset($valor[0]["nro_orden_armado"]))
        				$valor["NecesidadMecanizadoGranel"]["nro_orden_armado"]
        				= $valor[0]["nro_orden_armado"];
        				else
        					$valor["NecesidadMecanizadoGranel"]["nro_orden_armado"]  = "";
        					
        		}
        		
        		/*-------------------------------------------------- REPORTES  - COMPONENTES PARA COMPRA  -------------------------------------------------------------*/
        		
        		/*$array_categorias = "42,43,49,315,316,317,318,322,342,362,363,365,366,367,368,369,370,371,
                    392,393,397,398,401,407,408,409,410,447,450,451,452,453,464,465,485,496";*/
        		
        		$array_origen = "2,3"; 
        		
        		$query = $this->getQueryNecesidadMecanizado($array_origen,$stock_menor_a_cantidad,$n_column_order);
        		$registros3 = $this->StockComponente->query($query);
        		
        		foreach($registros3 as &$valor)
        		{
        			$valor["NecesidadCompra"]["id_producto"] =  		$valor["producto"]["id"];
        			$valor["NecesidadCompra"]["codigo"] =  				$valor["producto"]["codigo"];
        			$valor["NecesidadCompra"]["d_producto"] =  			$valor["producto"]["d_producto"];
        			$valor["NecesidadCompra"]["stock"] =        		$valor["stock_producto"]["stock"];
        			$valor["NecesidadCompra"]["comprometido"] = 		$valor[0]["comprometido"];
        			
        			$fecha = $valor[0]["fecha_entrega_componentes_orden_armado"];
        			$nuevafecha = strtotime ( '-2 day' , strtotime ( $fecha ) ) ;
        			$nuevafecha = date ( 'j-m-Y' , $nuevafecha );
        			
        			
        			$valor["NecesidadCompra"]["fecha_entrega_componentes_orden_armado"]
        			= $nuevafecha;
        			
        			if(isset($valor[0]["nro_orden_armado"]))
        				$valor["NecesidadCompra"]["nro_orden_armado"]
        				= $valor[0]["nro_orden_armado"];
        				else
        					$valor["NecesidadCompra"]["nro_orden_armado"]  = "";
        					
        		}
        		
        		$datos_envio = array();
        		
        		array_push($datos_envio,$registros);
        		array_push($datos_envio,$registros2);
        		array_push($datos_envio,$registros3);
        		
        		if($pdf)
        			$this->pdfExport($datos_envio,"","");
        		else{//aca debe ser en excel
        			
        			
        			
        			
        			
        		}
        			
        }
   

        public function VentasPorPedidoInterno($pdf=true){ 

            $this->loadModel("Comprobante");
            $this->loadModel("DatoEmpresa");

            $fecha_desde = $this->getFromRequestOrSession('R.fecha_desde');
            $fecha_hasta = $this->getFromRequestOrSession('R.fecha_hasta'); 
            $id_moneda = $this->getFromRequestOrSession('R.id_moneda'); //lo expreso en esta moneda (id_moneda,id_moneda2,id_moneda3)

            $conditions = array();
            
    

            if($fecha_desde!='') 
                array_push($conditions, array('DATE(Comprobante.fecha_generacion) >=' => substr($fecha_desde,0,10) ));      //TODO: SACARLO cuando se updatee la version 10/08/2016

            if($fecha_hasta!='') 
                array_push($conditions, array('DATE(Comprobante.fecha_generacion) <=' => substr($fecha_hasta,0,10) ));       

            array_push($conditions, array('Comprobante.id_tipo_comprobante' =>EnumTipoComprobante::PedidoInterno ));
            array_push($conditions, array('Comprobante.id_estado_comprobante <>' =>EnumEstadoComprobante::Anulado ));

            
            
          
                
            $id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda);    
            $campo_valor = $this->Comprobante->getFieldByTipoMoneda($id_tipo_moneda);//obtengo el campo en la cual expresar el reporte
            
            $registros = $this->Comprobante->find('all', array(
                    'conditions' => array($conditions),
                    'fields' => array('SUM(ROUND(Comprobante.total_comprobante/Comprobante.'.$campo_valor.',2)) as total','Persona.razon_social','Persona.cuit','Comprobante.fecha_generacion'),
                    'joins'=>array(   array(
                            'table' => 'persona',
                            'alias' => 'Persona',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Persona.id = Comprobante.id_persona'
                            )
                        )),
                    'group' => array('Comprobante.id_persona'),
                    'order' => array('SUM(ROUND(Comprobante.total_comprobante/Comprobante.'.$campo_valor.',2)) desc'),

                    'contain' =>false
                ));

            $total = 0;

            foreach($registros as $valor){

                $total = $total +  $valor[0]["total"];
            }
            
            $a;

            if($pdf)
                $this->pdfExport($registros,"",$total);
        } 



        

        


        
        
        
        



        public function pdfExport($data,$vista,$datos_extra='')
        {
            $this->set('datos_pdf',$data);
            $this->set('datos_extra',$datos_extra);


            $impuestos = '';
            Configure::write('debug',0);
            $this->layout = 'pdf'; //esto usara el layout pdf.ctp
            $this->response->type('pdf');

            if($vista=='')
            {
                $this->render();
            }
            else
            {
                $this->render($vista);
            }
        }
        
        
        
        public function BalanceSumasYSaldos($externo = 0,$incluye_mayores="",$id_ejercicio="",$id_estado_asiento = "",$id_moneda = ""){
            
            parent::BalanceSumasYSaldos($externo,$incluye_mayores,$id_ejercicio,$id_estado_asiento,$id_moneda);
        }
        
        
public function ReporteCentroCostoPorCuentaContable($nombre_funcion="CalculaTotalCentro"){
           
           set_error_handler(array(&$this, 'myFunctionErrorHandler'), E_WARNING); 
           set_error_handler(array(&$this, 'myFunctionErrorHandler'), E_NOTICE); 
            
           $this->loadModel("CuentaContable");
           $this->loadModel("DatoEmpresa");
            
           $fecha_desde = $this->getFromRequestOrSession('R.fecha_desde'); 
           $fecha_hasta = $this->getFromRequestOrSession('R.fecha_hasta'); 
           $id_ejercicio = $this->getFromRequestOrSession('R.id_ejercicio_contable'); 
           $id_periodo = $this->getFromRequestOrSession('R.id_periodo_contable');
           $get_tree = $this->getFromRequestOrSession('R.get_tree');
           $id_estado_asiento = $this->getFromRequestOrSession('R.id_estado_asiento');
           $id_moneda = $this->getFromRequestOrSession('R.id_moneda');
           $solo_imputables = $this->getFromRequestOrSession('R.solo_imputables');
           $id_cuenta_contable = $this->getFromRequestOrSession('R.id_cuenta_contable');
           
        
           
           if($solo_imputables == "")
            $solo_imputables = 0;
            
          
           
          
          
          //$id_ejercicio = 20;
          //$fecha_hasta = '2016-08-03';
          $id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda);
          $campo_valor = $this->CuentaContable->getFieldByTipoMoneda($id_tipo_moneda);//obtengo el campo en la cual expresar el reporte
          
          $conditions = array();
          
          
        
           
           if($id_ejercicio!=""){
             $this->loadModel("EjercicioContable");
             $fecha_inicio = $this->EjercicioContable->getFechaInicio($id_ejercicio);
             $fecha_fin = $this->EjercicioContable->getFechaFin($id_ejercicio);;
               
           }elseif($id_periodo !=""){
              $this->loadModel("PeriodoContable"); 
             // $id_periodo = 145;
              $fecha_inicio = $this->PeriodoContable->getFechaInicio($id_periodo);
              $fecha_fin = $this->PeriodoContable->getFechaFin($id_periodo);
               
           }else{
               
              $fecha_inicio = $fecha_desde; 
              $fecha_fin = $fecha_hasta; 
               
           }
           
           if($id_cuenta_contable!=""){
               
              array_push($conditions,array("CuentaContable.id"=>$id_cuenta_contable)); 
           }
           
           //$fecha_inicio = '2016-11-29';
           //$fecha_hasta = '2016-12-29';
           
           $data = $this->CuentaContable->find('threaded', array(
                        'conditions'=>$conditions,   
                        'contain' => false,                        // 'fields' => array('id', 'CuentaContable.parent_id'),
                        'order' => array('CuentaContable.codigo ASC'), // or array('id ASC')
                         // or array('id ASC')
                    ));
                    
           $this->CuentaContable->InicializoCentros($data);
           
           //$fecha_inicio = '2016-06-30';    
          // $fecha_hasta = '2016-07-30';

                                                   
           $this->CuentaContable->{$nombre_funcion}($data,$a,$fecha_inicio,$fecha_hasta,$id_estado_asiento,$campo_valor);    
           //$this->CuentaContable->ModificaDescripcion($data);
           
           $this->data = $data;
           
           $array_final = array();
           $this->ConviertoArbolEnRecordsetParaCentrosCosto($this->data,$array_final);        
           
           $this->data = $array_final;
           $page_count = 0;
            $output = array(
                "status" =>EnumError::SUCCESS,
                "message" => "list",
                "content" => $array_final,
                "page_count" =>$page_count
            );
            $this->set($output);
            $this->set("_serialize", array("status", "message","page_count", "content")); 
            
            
            
            
        }
        
         public function ReporteCentroCostoPorCuentaContableXls(){
             
             $this->xlsReport =1;
             $this->model = "CuentaContable";
             $this->loadModel("Moneda");
             $id_moneda = $this->getFromRequestOrSession('R.id_moneda');
             $simbolo_internacional_moneda = $this->Moneda->getSimboloInternacional($id_moneda);
             
             
             
             $this->ExportarExcel("reporte_centro_costo_por_cuenta_contable","ReporteCentroCostoPorCuentaContable","Planilla de Centros de Costo en ".$simbolo_internacional_moneda);
             
             
         }
        
        
         public function BalanceSumasYSaldosXls(){
             
             $this->xlsReport =1;
             $this->model = "CuentaContable";
             $this->ExportarExcel("balance_sumas_y_saldos","BalanceSumasYSaldos","Balance de Sumas Y Saldos");
             
             
         }
         
          public function BalanceGeneralXls(){
             
             $this->xlsReport =1;
             $this->model = "CuentaContable";
             $this->ExportarExcel("balance_sumas_y_saldos","BalanceGeneral","Balance General");
             
             
         }
       
       
        


public function ConviertoArbolEnRecordsetParaCentrosCosto(&$datos,&$array_final){
   
   
    $this->loadModel("CentroCosto");
    $centro_costo = $this->CentroCosto->find('all', array('conditions'=>array('CentroCosto.activo'=>1)));
    
    
            
                foreach($datos as $key=>$value){ 
                    
                    
                    
                    
                       if($this->xlsReport == 1){ //esto lo hago xq la grilla de .net castea todo, corre las comas, sobreinterpreta que es un numero y no un string
                          
                        
                            foreach($centro_costo as $centro){
                               
                             $auxiliar["CuentaContable"][$centro["CentroCosto"]["id"]] = (string)   str_replace(".",",",$datos[$key]["CuentaContable"][$centro["CentroCosto"]["id"]]);
                           }
                           
                           
                       }else{
                          
                           
                           foreach($centro_costo as $centro){
                               
                             $auxiliar["CuentaContable"][$centro["CentroCosto"]["id"]] = (string)  $datos[$key]["CuentaContable"][$centro["CentroCosto"]["id"]];
                           }
                           
                       }
                       
                       
                       $auxiliar["CuentaContable"]["codigo"] = (string)  $datos[$key]["CuentaContable"]["codigo"]; 
                       $auxiliar["CuentaContable"]["d_cuenta_contable"] = $datos[$key]["CuentaContable"]["d_cuenta_contable"]; 
                       $auxiliar["CuentaContable"]["es_mayor"] = $datos[$key]["CuentaContable"]["es_mayor"]; 
                       $auxiliar["CuentaContable"]["d_es_mayor"] = $datos[$key]["CuentaContable"]["d_es_mayor"]; 
                       
                       
                       array_push($array_final,$auxiliar);
                       
                    if ( isset($datos[$key]["children"]) && is_array($datos[$key]["children"])){
                        //si es un array sigo recorriendo
                     
                     $this->ConviertoArbolEnRecordsetParaCentrosCosto($datos[$key]["children"],$array_final);
                     
                     
                   
                  }
         
               }
} 


 
        public function getModel($vista='default'){
        
        $model = '';
        
        
     
        
        switch ($vista){
        	
        	case "ventas_por_pedido_interno":
        	case "compras_por_orden_compra":
        		$model = EnumModel::ComprobanteItem;
        		break;
        		
        	case "balance_sumas_y_saldos":
        		$model = "CuentaContable";
        		break;
        		
        		
        	case "ventas_por_jurisdiccion":
        	case "total_comprobante_por_persona":
        		$model = EnumModel::Comprobante;
        		break;
        		
        	case "default":
        		$model = EnumModel::Comprobante;
        		break;
        	case "reporte_pedido_orden_armado":
        		$model = EnumModel::Comprobante;
        		break;
        	
        	
        }
        
        
        if(strlen($model)>0){
        
	        $this->model = $model;
	            
	        
	        $model = parent::getModelCamposDefault();
	        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
	        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
        }  
      
    }
    
    
        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
            
              
     
              $this->set('model',$model);
              Configure::write('debug',0);
              $this->render($vista);
                
               
           
                

                
        }
        
        
         public function getReporte($id_reporte){
             
             $this->loadModel("Reporte");
        
             $model = '';
             $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
            
            
            
          
        }
        
        
        public function tablero(){
            
            $this->layout = 'tablero';
        }
        
        
        
        public function VentasVsCotizacionMensual($pdf=true){
        	
        	$this->loadModel("Comprobante");
        	$this->loadModel("DatoEmpresa");
        	
        	
        	$id_moneda = $this->getFromRequestOrSession('R.id_moneda'); //lo expreso en esta moneda (id_moneda,id_moneda2,id_moneda3)
        	
        	$conditions = array();
        
        		array_push($conditions, array('YEAR(Comprobante.fecha_generacion)'=> date('Y') ));      //TODO: SACARLO cuando se updatee la version 10/08/2016
        		

        			
        			array_push($conditions, array('Comprobante.id_tipo_comprobante' =>EnumTipoComprobante::PedidoInterno ));
        			array_push($conditions, array('Comprobante.id_estado_comprobante <>' =>EnumEstadoComprobante::Anulado ));
        			
        			
        			
        			
        			
        			$id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda(EnumMoneda::Dolar);
        			$campo_valor = $this->Comprobante->getFieldByTipoMoneda($id_tipo_moneda);//obtengo el campo en la cual expresar el reporte
        			
        			$pedidos_internos_por_mes = $this->Comprobante->find('all', array(
        					'conditions' => array($conditions),
        					'fields' => array('SUM(ROUND(Comprobante.total_comprobante/Comprobante.'.$campo_valor.',2)) as total','MONTH(fecha_generacion) as mes'),
        				
        					'group' => array('MONTH(Comprobante.fecha_generacion)'),
        			
        					'contain' =>false
        			));
        			
        			
        			$conditions = array();
        			array_push($conditions, array('YEAR(Comprobante.fecha_generacion)'=> date('Y') )); 
        			array_push($conditions, array('Comprobante.id_tipo_comprobante' =>EnumTipoComprobante::Cotizacion ));
        			array_push($conditions, array('Comprobante.id_estado_comprobante <>' =>EnumEstadoComprobante::Anulado ));
        			
        			$cotizaciones_por_mes = $this->Comprobante->find('all', array(
        					'conditions' => array($conditions),
        					'fields' => array('SUM(ROUND(Comprobante.total_comprobante/Comprobante.'.$campo_valor.',2)) as total','MONTH(fecha_generacion) as mes'),
        					
        					'group' => array('MONTH(Comprobante.fecha_generacion)'),
        					
        					'contain' =>false
        			));
        			
        			
        			$this->set('pedidos_internos',$pedidos_internos_por_mes);
        			$this->set('cotizaciones',$cotizaciones_por_mes);
        			
        			App::import('Lib', 'FormBuilder');
        			$formBuilder = new FormBuilder();
        			$formBuilder->setDataListado($this, 'Pedidos Vs Cotizaciones', '', $this->model, $this->name,null);
        			
        			$this->set('abm',$formBuilder);
        			$this->render('/FormBuilder/chart');
        	
        } 
        
        
        
        
        
        public function UnidadesPorArticulo($pdf=true){
        	
        
        	$this->loadModel("Comprobante");
        	$this->loadModel("DatoEmpresa");
        	$this->loadModel("ComprobanteItem");
        	$this->loadModel("Reporte");
        	$this->title = "Unidades Por Articulo";
        
        	
        	
        	
        	
        	
        	$conditions = array();
        	$conditions = $this->RecuperoFiltros(EnumModel::Comprobante);
        	
        	
        	
        
        	
        	
        	
        	
        	array_push($conditions, array('Comprobante.id_estado_comprobante <>' =>EnumEstadoComprobante::Anulado ));
        	
        	
        	
        	
        	$id_moneda = $this->getFromRequestOrSession('Comprobante.id_moneda_expresion');
        	
        	
        	$id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda);
  
        	
        	
        	
    
        	

        	$campo_valor = $this->Comprobante->getFieldByTipoMoneda($id_tipo_moneda);//obtengo el campo en la cual expresar el reporte
        	
        	
        	$this->ComprobanteItem->virtualFields = array('total_por_producto' => 'SUM(ROUND((ComprobanteItem.precio_unitario*ComprobanteItem.cantidad)/Comprobante.'.$campo_valor.',2))','cantidad_calculada'=>'SUM(ComprobanteItem.cantidad)','codigo'=>'Producto.codigo');
        	
        	
        	
        	$pedidos_internos_por_producto = $this->ComprobanteItem->find('all', array(
        			'conditions' => array($conditions),
        			//'fields' => array('SUM(ROUND((ComprobanteItem.precio_unitario*ComprobanteItem.cantidad)/Comprobante.'.$campo_valor.',2)) as total','SUM(cantidad) as cantidad','Producto.codigo'),
        			'fields' => array('total_por_producto','codigo','cantidad_calculada'),
        			
        			'group' => array('ComprobanteItem.id_producto'),
        			'order'=>'cantidad_calculada DESC'
        			
        		
        	));
        	
        	
        	
        	
        	$this->data = $pedidos_internos_por_producto;
        	$this->model = EnumModel::ComprobanteItem;
        	
        	$output = array(
        			"status" =>EnumError::SUCCESS,
        			"message" => "list",
        			"content" => $this->data,
        			"page_count" =>0
        	);
        	$this->set($output);
        	$this->set("_serialize", array("status", "message","page_count", "content"));
        	
      
        
        	
        }
        
        
        public function TotalComprobantePorPersona(){
        	
        	
        	$this->loadModel("Comprobante");
        	$this->loadModel("DatoEmpresa");
        	$this->loadModel("Reporte");
        	$this->loadModel("Moneda");
        	$this->title = "Total Comprobante por persona";
        	
        	
        	
        	
        	
        	
        	$conditions = array();
        	$conditions = $this->RecuperoFiltros(EnumModel::Comprobante);
        	
        	
        	
        	
        	
        	
        	

        	array_push($conditions, array('Comprobante.id_estado_comprobante <>' =>EnumEstadoComprobante::Anulado ));
        	
        	
        	
        	
        	$id_moneda = $this->getFromRequestOrSession('Comprobante.id_moneda_expresion');
        	
        	
        	$id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda);
        	
        	
        	array_push($this->array_filter_names,array("Moneda.:"=>$this->Moneda->getSimboloInternacional($id_moneda)));
        	
        	
        	
        	
        	
        	
        	
        	$campo_valor = $this->Comprobante->getFieldByTipoMoneda($id_tipo_moneda);//obtengo el campo en la cual expresar el reporte
        	
        	
        	$this->Comprobante->virtualFields = array('total_por_comprobante' => 'SUM(ROUND((Comprobante.total_comprobante)/Comprobante.'.$campo_valor.',2))','razon_social'=>'Persona.razon_social','codigo_persona'=>'Persona.codigo');
        	
        	$total_comprobante_por_persona = $this->Comprobante->find('all', array(
        			'conditions' => array($conditions),
        			//'fields' => array('SUM(ROUND((ComprobanteItem.precio_unitario*ComprobanteItem.cantidad)/Comprobante.'.$campo_valor.',2)) as total','SUM(cantidad) as cantidad','Producto.codigo'),
        			'fields' => array('total_por_comprobante','razon_social','codigo_persona'),
        			'contain'=>array('Persona'),
        			'group' => array('Comprobante.id_persona'),
        			'order'=>'total_por_comprobante DESC'
        			
        			
        	));
        	
        	
        	
        	
        	$this->data = $total_comprobante_por_persona;
        	$this->model = EnumModel::Comprobante;
        	
        	$output = array(
        			"status" =>EnumError::SUCCESS,
        			"message" => "list",
        			"content" => $this->data,
        			"page_count" =>0
        	);
        	$this->set($output);
        	$this->set("_serialize", array("status", "message","page_count", "content"));
        	
        	
        	
        	
        }
        
        
        public function ComprasPorArticulo($pdf=true){
        	
        	
        	$this->loadModel("Comprobante");
        	$this->loadModel("DatoEmpresa");
        	$this->loadModel("ComprobanteItem");
        	$this->loadModel("Reporte");
        	
        	$id_moneda = $this->getFromRequestOrSession('R.id_moneda'); //lo expreso en esta moneda (id_moneda,id_moneda2,id_moneda3)
        	
        	
        	$id_reporte = $this->getFromRequestOrSession('R.id_reporte'); //lo expreso en esta moneda (id_moneda,id_moneda2,id_moneda3)
        	
        	$conditions = array();
        	
        	$conditions = $this->RecuperoFiltros(EnumModel::Comprobante);
        	
        	
        	
        	
        	
        	
        	array_push($conditions, array('Comprobante.id_tipo_comprobante' =>EnumTipoComprobante::OrdenCompra ));
        	
        	array_push($conditions, array('Comprobante.id_estado_comprobante <>' =>EnumEstadoComprobante::Anulado ));
        	
        	
        	
        	
        	
        	
        	$id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda);
        	$campo_valor = $this->Comprobante->getFieldByTipoMoneda($id_tipo_moneda);//obtengo el campo en la cual expresar el reporte
        	
        	
        	$this->ComprobanteItem->virtualFields = array('total_por_producto' => 'SUM(ROUND((ComprobanteItem.precio_unitario*ComprobanteItem.cantidad)/Comprobante.'.$campo_valor.',2))','cantidad_calculada'=>'SUM(ComprobanteItem.cantidad)','codigo_producto'=>'Producto.codigo');
        	$pedidos_internos_por_producto = $this->ComprobanteItem->find('all', array(
        			'conditions' => array($conditions),
        			//'fields' => array('SUM(ROUND((ComprobanteItem.precio_unitario*ComprobanteItem.cantidad)/Comprobante.'.$campo_valor.',2)) as total','SUM(cantidad) as cantidad','Producto.codigo'),
        			'fields' => array('total_por_producto','codigo_producto','cantidad_calculada'),
        			
        			'group' => array('ComprobanteItem.id_producto'),
        			
        			
        	));
        	
        	
        	
        	
        	$this->data = $pedidos_internos_por_producto;
        	
        	$output = array(
        			"status" =>EnumError::SUCCESS,
        			"message" => "list",
        			"content" => $this->data,
        			"page_count" =>0
        	);
        	$this->set($output);
        	$this->set("_serialize", array("status", "message","page_count", "content"));
        	
        	
        	
        	
        }
        
        
        public function VentasPorArticulo($pdf=true){
        	
        	
        	$this->loadModel("Comprobante");
        	$this->loadModel("DatoEmpresa");
        	$this->loadModel("ComprobanteItem");
        	$this->loadModel("Reporte");
        	
        	$id_moneda = $this->getFromRequestOrSession('R.id_moneda'); //lo expreso en esta moneda (id_moneda,id_moneda2,id_moneda3)
        	
        	
        	$id_reporte = $this->getFromRequestOrSession('R.id_reporte'); //lo expreso en esta moneda (id_moneda,id_moneda2,id_moneda3)
        	
        	$conditions = array();
        	
        	$conditions = $this->RecuperoFiltros(EnumModel::Comprobante);
        	
        	
        	
        	
        	
        	
        	array_push($conditions, array('Comprobante.id_tipo_comprobante' =>EnumTipoComprobante::PedidoInterno ));
        	
        	array_push($conditions, array('Comprobante.id_estado_comprobante <>' =>EnumEstadoComprobante::Anulado ));
        	
        	
        	
        	
        	
        	
        	$id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda);
        	$campo_valor = $this->Comprobante->getFieldByTipoMoneda($id_tipo_moneda);//obtengo el campo en la cual expresar el reporte
        	
        	
        	$this->ComprobanteItem->virtualFields = array('total_por_producto' => 'SUM(ROUND((ComprobanteItem.precio_unitario*ComprobanteItem.cantidad)/Comprobante.'.$campo_valor.',2))','cantidad_calculada'=>'SUM(ComprobanteItem.cantidad)','codigo_producto'=>'Producto.codigo');
        	$pedidos_internos_por_producto = $this->ComprobanteItem->find('all', array(
        			'conditions' => array($conditions),
        			//'fields' => array('SUM(ROUND((ComprobanteItem.precio_unitario*ComprobanteItem.cantidad)/Comprobante.'.$campo_valor.',2)) as total','SUM(cantidad) as cantidad','Producto.codigo'),
        			'fields' => array('total_por_producto','codigo_producto','cantidad_calculada'),
        			
        			'group' => array('ComprobanteItem.id_producto'),
        			
        			
        	));
        	
        	
        	
        	
        	$this->data = $pedidos_internos_por_producto;
        	
        	$output = array(
        			"status" =>EnumError::SUCCESS,
        			"message" => "list",
        			"content" => $this->data,
        			"page_count" =>0
        	);
        	$this->set($output);
        	$this->set("_serialize", array("status", "message","page_count", "content"));
        	
        	
        	
        	
        }
        
     
        public function excelExport($vista="default",$metodo="index",$titulo=""){
        	
        	$vista = $this->getFromRequestOrSession('R.vista');
        	$metodo = $this->getFromRequestOrSession('R.method');
        	
        	parent::excelExport($vista,$metodo,"Reporte");
        	
        	
        	
        }
        
        
        public  function RecuperoFiltros($model)
        {
        	//Recuperacion de Filtros
        	$id_comprobante = $this->getFromRequestOrSession($model.'.id');
        	$fecha_generacion = $this->getFromRequestOrSession($model.'.fecha_generacion');
        	$fecha_generacion_hasta = $this->getFromRequestOrSession($model.'.fecha_generacion_hasta');
        	$nro_comprobante = trim(ltrim($this->getFromRequestOrSession($model.'.nro_comprobante'),'0'));//le quito los ceros de adelante
        	$id_persona = $this->getFromRequestOrSession($model.'.id_persona');
        	$razon_social = $this->getFromRequestOrSession($model.'.persona_razon_social');
        	$id_estado = $this->getFromRequestOrSession($model.'.id_estado_comprobante');
        	$id_estado_comprobante_excluir = $this->getFromRequestOrSession($model.'.id_estado_comprobante_excluir');
        	$fecha_contable = $this->getFromRequestOrSession($model.'.fecha_contable');
        	$fecha_contable_hasta = $this->getFromRequestOrSession($model.'.fecha_contable_hasta');
        	$id_tipo_comprobante = $this->getFromRequestOrSession($model.'.id_tipo_comprobante');
        	$aprobado = $this->getFromRequestOrSession($model.'.aprobado');
        	$id_transportista = $this->getFromRequestOrSession($model.'.id_transportista');
        	$fecha_vencimiento = $this->getFromRequestOrSession($model.'.fecha_vencimiento');
        	$fecha_vencimiento_hasta = $this->getFromRequestOrSession($model.'.fecha_vencimiento_hasta');
        	$fecha_entrega_desde = $this->getFromRequestOrSession($model.'.fecha_entrega');
        	$fecha_entrega_hasta = $this->getFromRequestOrSession($model.'.fecha_entrega_hasta');
        	$id_punto_venta = $this->getFromRequestOrSession($model.'.id_punto_venta');
        	$d_punto_venta = trim(ltrim($this->getFromRequestOrSession($model.'.d_punto_venta'),'0'));//le quito los ceros y los espacios de adelante
        	$orden_compra_externa = $this->getFromRequestOrSession($model.'.orden_compra_externa');
        	$id_detalle_tipo_comprobante = $this->getFromRequestOrSession($model.'.id_detalle_tipo_comprobante');
        	$id_cartera_rendicion = $this->getFromRequestOrSession($model.'.id_cartera_rendicion');
        	
        	$fecha_aprobacion = $this->getFromRequestOrSession($model.'.fecha_aprobacion');
        	$subtotal_neto_desde = $this->getFromRequestOrSession($model.'.subtotal_neto_desde');
        	$subtotal_neto_hasta = $this->getFromRequestOrSession($model.'.subtotal_neto_hasta');
        	$persona_codigo = $this->getFromRequestOrSession('Persona.persona_codigo');
        	
        	
        	$conditions = array();
        	
        	
        	if($id_tipo_comprobante!=""){
        		$tipos_comp = explode(',', $id_tipo_comprobante);
        		$this->id_tipo_comprobante = $tipos_comp;
        	}
        	
        	
        	array_push($conditions, array($model.'.activo =' => 1)); //solo trae las visibles
        	
        	if($fecha_generacion!=""){
        		array_push($conditions, array($model.'.fecha_generacion >=' => $fecha_generacion)); //MP: Si la fecha de bd tiene hh:mm:ss distintos de 00:00:00 la compoaracion no anda
        		array_push($this->array_filter_names,array("Fecha Generaci&oacute;n Dsd.:"=>$this->Comprobante->formatDate($fecha_generacion)));
        		
        	}
        	
        	
        	if($fecha_generacion_hasta!=""){
        		array_push($conditions, array($model.'.fecha_generacion <=' => $fecha_generacion_hasta)); //MP: Si la fecha de bd tiene hh:mm:ss distintos de 00:00:00 la compoaracion no anda
        		array_push($this->array_filter_names,array("Fecha Generaci&oacute;n Hta.:"=>$this->Comprobante->formatDate($fecha_generacion_hasta)));
        	}
        	
        	if($aprobado!="")
        		array_push($conditions, array($model.'.aprobado' => $aprobado));
        		
        		if($nro_comprobante!=""){
        			$nros_comprobante = explode(',', $nro_comprobante);
        			array_push($conditions, array($model.'.nro_comprobante' => $nros_comprobante));
        			array_push($this->array_filter_names,array("Nro Comprobante/s:"=>$nros_comprobante));
        		}
        		if($id_persona!=""){
        			array_push($conditions, array($model.'.id_persona' => $id_persona));
        			array_push($this->array_filter_names,array("Id Persona:"=>$id_persona));
        		}
        		/*
        		 $razon_social="";
        		 if($razon_social!=""){
        		 // array_push($conditions, array('LOWER(Persona.razon_social) LIKE' => '%' . $razon_social  . '%'));
        		 // array_push($conditions, array('LOWER(Comprobante.razon_social) LIKE' => '%' . $razon_social  . '%'));
        		 }
        		 */
        		if($id_estado!=""){
        			$this->loadModel("EstadoComprobante");
        			array_push($conditions, array($model.'.id_estado_comprobante' => $id_estado));
        			array_push($this->array_filter_names,array("Estado.:"=>$this->EstadoComprobante->getDescripcion($id_estado)));
        		}
        		
        		if( $id_estado_comprobante_excluir !="" ){
        			
        			$estados_excluir = explode(",", $id_estado_comprobante_excluir);
        			array_push($conditions,array( "NOT" => (array( 'Comprobante.id_estado_comprobante' => $estados_excluir  )) ));
        		}
        		
        		
        		
        		if($id_transportista!="")
        			array_push($conditions, array($model.'.id_transportista' => $id_transportista));
        			
        			if($fecha_contable!=""){
        				array_push($conditions, array($model.'.fecha_contable >= ' =>  $fecha_contable));
        				array_push($this->array_filter_names,array("Fecha Contable Dsd:"=>$this->Comprobante->formatDate($fecha_contable)));
        			}
        			
        			if($fecha_contable_hasta!=""){
        				array_push($conditions, array($model.'.fecha_contable <= ' => $fecha_contable_hasta));
        				array_push($this->array_filter_names,array("Fecha Contable Hta.:"=>$this->Comprobante->formatDate($fecha_contable_hasta)));
        			}
        			
        			
        			if($id_comprobante!=""){
        				array_push($conditions, array($model.'.id' => $id_comprobante));
        				array_push($this->array_filter_names,array("Id Comp.:"=>$id_comprobante));
        			}
        			
        			
        			
        			if($razon_social!="" && $id_persona == ""){
        				array_push($conditions, array('Comprobante.razon_social LIKE ' => '%' .$razon_social.'%'));
        				array_push($this->array_filter_names,array("Raz&oacute;n Social:"=>$razon_social));
        			}
        			
        			if($orden_compra_externa !=""){
        				array_push($conditions, array('Comprobante.orden_compra_externa LIKE ' => '%' .$orden_compra_externa.'%'));
        				array_push($this->array_filter_names,array("OC Externa:"=>$orden_compra_externa));
        			}
        			
        			
        			if($fecha_entrega_desde!=""){
        				array_push($conditions, array($model.'.fecha_entrega >= ' =>  $fecha_entrega_desde ));
        				array_push($this->array_filter_names,array("Fcha Entrega Dsd.:"=>$this->Comprobante->formatDate($fecha_entrega_desde)));
        			}
        			
        			if($fecha_entrega_hasta!=""){
        				array_push($conditions, array($model.'.fecha_entrega <= ' =>  $fecha_entrega_hasta ));
        				array_push($this->array_filter_names,array("Fcha Entrega Hta.:"=>$this->Comprobante->formatDate($fecha_entrega_hasta)));
        			}
        			
        			
        			if($subtotal_neto_desde!=""){
        				array_push($conditions, array($model.'.subtotal_neto >= ' =>  $subtotal_neto_desde ));
        				array_push($this->array_filter_names,array("Sbtotal Neto Dsd.:"=>$subtotal_neto_desde));
        			}
        			
        			if($subtotal_neto_hasta!=""){
        				array_push($conditions, array($model.'.subtotal_neto <= ' =>  $subtotal_neto_hasta ));
        				array_push($this->array_filter_names,array("Sbtotal Neto Hta.:"=>$subtotal_neto_hasta));
        			}
        			
        			
        			
        			if($fecha_vencimiento!=""){
        				array_push($conditions, array($model.'.fecha_vencimiento >= ' =>  $fecha_vencimiento ));
        				array_push($this->array_filter_names,array("Fecha Vto. Dsd.:"=>$this->Comprobante->formatDate($fecha_vencimiento)));
        			}
        			
        			if($fecha_vencimiento_hasta!=""){
        				array_push($conditions, array($model.'.fecha_vencimiento <= ' =>  $fecha_vencimiento_hasta));
        				array_push($this->array_filter_names,array("Fecha Vto. Hta.:"=>$this->Comprobante->formatDate($fecha_vencimiento_hasta)));
        			}
        			
        			
        			if($fecha_aprobacion!=""){
        				array_push($conditions, array($model.'.fecha_aprobacion LIKE' =>  '%' .$fecha_aprobacion.'%' ));
        				array_push($this->array_filter_names,array("Fecha Aprob.:"=>$this->Comprobante->formatDate($fecha_aprobacion)));
        			}
        			
        			if($id_punto_venta!=""){
        				array_push($conditions, array('Comprobante.id_punto_venta' =>  $id_punto_venta ));
        				array_push($this->array_filter_names,array("ID Pto Vta.:"=>$id_punto_venta));
        				
        			}
        			
        			if($d_punto_venta!=""){
        				array_push($conditions, array('Comprobante.d_punto_venta LIKE' =>   '%' .$d_punto_venta.'%' ));
        				array_push($this->array_filter_names,array("Desc Pto Vta.:"=>$d_punto_venta));
        			}
        			
        			if($id_detalle_tipo_comprobante!=""){
        				array_push($conditions, array('Comprobante.id_detalle_tipo_comprobante' =>  $id_detalle_tipo_comprobante ));
        				array_push($this->array_filter_names,array("ID Detall. Tpo Comp.:"=>$id_detalle_tipo_comprobante));
        			}
        			
        			if($id_cartera_rendicion!="")
        				array_push($conditions, array('Comprobante.id_cartera_rendicion' =>  $id_cartera_rendicion ));
        				
        			if($persona_codigo !=""){
        					array_push($conditions, array('Persona.codigo ' => $persona_codigo));
        					array_push($this->array_filter_names,array("Cod Pers.:"=>$persona_codigo));
        				}
        				
        				
        				
        			if(count($this->id_tipo_comprobante) > 0 || $this->id_tipo_comprobante != "")	
        			array_push($conditions, array($model.'.id_tipo_comprobante ' => $this->id_tipo_comprobante));
        				
        				
        				return $conditions;
        }
       
        
        
        
        public function PedidosConOrdenEnsamble(){
        	
        	
        	$this->loadModel("Comprobante");
        	
        	$pedidos_internos_items = $this->Comprobante->query('select * from pedidos_con_orden_ensamble_view');
        	
        	$output = array(
        			"status" =>EnumError::SUCCESS,
        			"message" => "list",
        			"content" => $pedidos_internos_items,
        			"page_count" =>0
        	);
        	$this->set($output);
        	$this->set("_serialize", array("status", "message","page_count", "content"));
        	
        	
        	
        	
        	
        	
        }
        
        
        
        public function usuarios() {
        	
        	
        	App::import('Lib', 'FormBuilderNew');
        	
        	if($this->request->is('ajax'))
        		$this->layout = 'ajax';
        		
        		$this->model = EnumModel::Comprobante;
        
        		$this->loadModel($this->model);
        		$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        		
        		//Recuperacion de Filtros
        		$fecha_contable = $this->getFromRequestOrSession('Comprobante.fecha_contable');
        		$fecha_contable_hasta = $this->getFromRequestOrSession('Comprobante.fecha_contable_hasta');
        		
        		
        		
        		$conditions = array();
        		
        	
        		if($fecha_contable!=""){
        			
        			$exp = explode('/', $fecha_contable);
        			$fecha_c = $exp[2].'-'.$exp[1].'-'.$exp[0];
        			
        			
        			array_push($conditions, array('DATE('.$this->model.'.fecha_contable) >= ' =>  $fecha_c));
        			
        			array_push($this->array_filter_names,array("Fecha Contable Dsd:"=>$this->{$this->model}->formatDate($fecha_contable)));
        		}
        		
        		if($fecha_contable_hasta!=""){
        			
        			$exp = explode('/', $fecha_contable_hasta);
        			$fecha_contable_h = $exp[2].'-'.$exp[1].'-'.$exp[0];
        			array_push($conditions, array('DATE('.$this->model.'.fecha_contable) <= ' => $fecha_contable_h));
        			
        			array_push($this->array_filter_names,array("Fecha Contable Hta.:"=>$this->{$this->model}->formatDate($fecha_contable_hasta)));
        		}
        		
        		
        		

        			
        				
        					
        							
        							
        	   $this->PaginatorModificado->settings = array(
							        									'paginado'=>1,
							        									'conditions' => $conditions,
							        									'limit' => 10,
							        									'page' => $this->getPageNum()
							      		);
							        							
        				
        					
        							
        								$formBuilder = new FormBuilderNew();
        								
        								$seteo_form_builder = array("showActionColumn" =>false,"showNewButton"=>false,"showFooter"=>true,"showHeaderListado"=>true,"showXlsButton"=>false);
        								
        								foreach($seteo_form_builder as $key=>$propiedad_form_builder){
        									
        									
        									
        									
        									
        									$formBuilder->{$key}($propiedad_form_builder);
        									
        									
        								}
        								
        								$formBuilder->setDataListado($this, 'Listado de Usuarios', 'Datos de los Usuarios', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        								
        								//Filters
        								//$formBuilder->addFilterBeginRow();
        								//$formBuilder->addFilterInput('codigo', 'C&oacute;digo', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $codigo));
        								$formBuilder->addFilterInput('fecha_contable', 'Fecha Cont. Desde', array('class'=>"form-group"), array('type' => 'text', 'class' => 'datepicker form-control mb-2 mr-sm-2', 'label' => false, 'value' => $fecha_contable));
        								$formBuilder->addFilterInput('fecha_contable_hasta', 'Fecha Cont. Hasta', array('class'=>"form-group"), array('type' => 'text', 'class' => 'datepicker ', 'label' => false, 'value' => $fecha_contable_hasta));
        								//$formBuilder->addFilterEndRow();
        								
        								//Headers
        			
        								$formBuilder->addHeader('Codigo', 'Comprobante.id',"10%");
        								$formBuilder->addHeader('Numero', 'nro_comprobante',"40%" );
        			
        								$formBuilder->addHeader('Persona', 'razon_social',"50%" );
        			
        								
        								//Fields
        
        								$formBuilder->addField($this->model, 'id');
        								$formBuilder->addField($this->model, 'nro_comprobante');
        								$formBuilder->addField("Persona", 'razon_social');
        								
        								
        						
        								
        								$this->layout = "desktop_panel";
        								$this->set('abm',$formBuilder);
        								$this->render('/FormBuilder/listado_new');
        					
        								
        							
        }
        
        
        
        
        public function MontosAgrupadosdeValores() {/* Te indica por comprobante el total de valores maximo horizonte 1 mes*/
        	
        	
        	App::import('Lib', 'FormBuilderNew');
        	
        	if($this->request->is('ajax'))
        		$this->layout = 'ajax';
        		
        		$this->model = EnumModel::Comprobante;
        		
        		$this->loadModel($this->model);
        		$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        		
        		//Recuperacion de Filtros
        		$fecha_contable = $this->getFromRequestOrSession('Comprobante.fecha_contable');
        		$fecha_contable_hasta = $this->getFromRequestOrSession('Comprobante.fecha_contable_hasta');
        		$pagos = $this->getFromRequestOrSession('Comprobante.pagos');
        		$recibos = $this->getFromRequestOrSession('Comprobante.recibos');

        		$conditions = array();

        		
        		
        		$array_tipo_comprobante_pagos = array();
        		$array_tipo_comprobante_recibos = array();
        		
        		
        		if($pagos == 1){
        			
        			array_push($array_tipo_comprobante_pagos, array(EnumTipoComprobante::OrdenPagoAutomatica,EnumTipoComprobante::OrdenPagoManual,EnumTipoComprobante::OrdenPagoRenGasto,EnumTipoComprobante::AnticipoCompra,EnumTipoComprobante::EgresoCaja) );	
        			
        		}
        		
        		if($recibos == 1){
        			
        			array_push($array_tipo_comprobante_recibos, array(EnumTipoComprobante::ReciboManual,EnumTipoComprobante::ReciboAutomatico,EnumTipoComprobante::IngresoCaja) );
        			
        		}
        		
        		
        		
        		if($pagos == 0 && $recibos == 0){/*sino da opcion traigo compras y ventas*/
        			
        			array_push($array_tipo_comprobante_pagos, array(EnumTipoComprobante::OrdenPagoAutomatica,EnumTipoComprobante::OrdenPagoManual,EnumTipoComprobante::OrdenPagoRenGasto,EnumTipoComprobante::AnticipoCompra,EnumTipoComprobante::EgresoCaja) );
        			array_push($array_tipo_comprobante_recibos, array(EnumTipoComprobante::ReciboManual,EnumTipoComprobante::ReciboAutomatico,EnumTipoComprobante::IngresoCaja) );
        		}
        		
        		
        		
        		$tipo_comprobantes_string = implode(",",$array_tipo_comprobante_pagos[0]);
        		
        		
        		
        		if(count($array_tipo_comprobante_pagos)>0 && count($array_tipo_comprobante_recibos)>0)
        			$tipo_comprobantes_string .= ",".implode(",",$array_tipo_comprobante_recibos[0]);
        		elseif(count($array_tipo_comprobante_recibos)>0)
        			$tipo_comprobantes_string = implode(",",$array_tipo_comprobante_recibos[0]);
        		
        		
        		
        		
        		
        		
        		
        		
        		
        		
        		
        		
        		
        		
        		
        		
        		
   
        		
        		
        		
        		if( $fecha_contable ==""){
        			
        			$fecha_contable= date('d/m/Y');
        			
        			
        		}
        		
        		
        		
        		if( $fecha_contable_hasta ==""){
        			
        			$fecha_contable_hasta = date('d/m/Y');
        			
        			
        		}
        		
        		
        		
        		
        		if($fecha_contable!=""){
        			
        			$exp = explode('/', $fecha_contable);
        			$fecha_c = $exp[2].'-'.$exp[1].'-'.$exp[0];
        			
        			
     
        			
        			array_push($this->array_filter_names,array("Fecha Contable Dsd:"=>$this->{$this->model}->formatDate($fecha_contable)));
        		}
        		
        		
        		
        		if($fecha_contable_hasta!=""){
        			
        			$exp = explode('/', $fecha_contable_hasta);
        			$fecha_contable_h = $exp[2].'-'.$exp[1].'-'.$exp[0];
        	
        			
        			array_push($this->array_filter_names,array("Fecha Contable Hta.:"=>$this->{$this->model}->formatDate($fecha_contable_hasta)));
        		}
        		
        		
        		
        	
        		
        		$sql = 'SELECT tipo_comprobante.`codigo_tipo_comprobante` as tipo,comprobante.`nro_comprobante` as Numero , persona.`razon_social`, 

  						DATE_FORMAT(comprobante.`fecha_contable`,"%d-%m-%Y") AS `fecha_contable`,

						IFNULL( (SELECT SUM(monto) FROM comprobante_valor WHERE id_comprobante=comprobante.id AND id_valor=1),0) AS efectivo,
						IFNULL( (SELECT SUM(comprobante_valor.monto) FROM comprobante_valor  JOIN cheque ON cheque.id = comprobante_valor.`id_cheque`
						WHERE  id_comprobante=comprobante.id AND id_valor=2 AND cheque.`id_tipo_cheque` = '.EnumTipoCheque::Terceros.'
						),0)  AS cheque_tercero,
						IFNULL( (SELECT SUM(comprobante_valor.monto) FROM comprobante_valor  JOIN cheque ON cheque.id = comprobante_valor.`id_cheque`
						WHERE  id_comprobante=comprobante.id AND id_valor=2 AND cheque.`id_tipo_cheque` = '.EnumTipoCheque::Propio.'
						),0)  AS cheque_propio,
						
						IFNULL( (SELECT SUM(monto) FROM comprobante_valor WHERE id_comprobante=comprobante.id AND id_valor=4),0)  AS transferencia,
						IFNULL( (SELECT SUM(importe_impuesto) FROM comprobante_impuesto WHERE comprobante_impuesto.`id_comprobante`= comprobante.id),0) AS retenciones,
						IFNULL( (SELECT SUM(monto) FROM comprobante_valor WHERE id_comprobante=comprobante.id AND id_valor=11),0)  AS tarjeta,
						IFNULL( (SELECT SUM(monto) FROM comprobante_valor WHERE id_comprobante=comprobante.id AND id_valor=13),0) AS compensacion,
						ROUND(comprobante.total_comprobante,2) as total
						  FROM comprobante
						JOIN persona ON persona.id = comprobante.`id_persona`
						JOIN tipo_comprobante ON tipo_comprobante.id = comprobante.`id_tipo_comprobante`
						WHERE id_tipo_comprobante IN('.$tipo_comprobantes_string.') AND comprobante.`id_estado_comprobante` IN (116) AND

						comprobante.fecha_contable>="'.$fecha_c.'" AND comprobante.fecha_contable<="'.$fecha_contable_h.'"
						
						 ORDER BY nro_comprobante DESC LIMIT 100000';
        		
        		
        		
        		$data = $this->{$this->model}->query($sql);
        		
        		
        		/*
        		$this->PaginatorModificado->settings = array(
        				'paginado'=>1,
        				'conditions' => $conditions,
        				'limit' => 10,
        				'page' => $this->getPageNum()
        		);
        		*/
        		
        		
        		
        		$formBuilder = new FormBuilderNew();
        		$seteo_form_builder = array("showActionColumn" =>false,"showNewButton"=>false,"showFooter"=>false,"showHeaderListado"=>true,"showXlsButton"=>false);
        		$formBuilder->setearPropiedades($seteo_form_builder);
        		$formBuilder->setDataListado($this, 'Listado de Usuarios', 'Datos de los Usuarios', $this->model, $this->name, $data);
        		$formBuilder->addFilterInput('fecha_contable', 'Fecha Cont. Desde', array('class'=>"form-group"), array('type' => 'text', 'class' => 'datepicker form-control mb-2 mr-sm-2', 'label' => false, 'value' => $fecha_contable));
        		$formBuilder->addFilterInput('fecha_contable_hasta', 'Fecha Cont. Hasta', array('class'=>"form-group"), array('type' => 'text', 'class' => 'datepicker  form-control mb-2 mr-sm-2', 'label' => false, 'value' => $fecha_contable_hasta));
        		//$formBuilder->addFilterInput('pagos', 'Pagos', array('class'=>"form-group"), array('type' => 'checkbox', 'class' => 'form-control mb-2 mr-sm-2', 'label' => false, 'value' => $pagos));
        		//$formBuilder->addFilterInput('recibos', 'Recibos', array('class'=>"form-group"), array('type' => 'checkbox', 'class' => 'form-control mb-2 mr-sm-2', 'label' => false, 'value' => $recibos));
        		
        		$formBuilder->addFilterInput('pagos', 'Pagos', array('class'=>'form-group'), array('class' => 'form-control mb-2 mr-sm-2', 'label' => false, 'type' => 'checkbox'));  
        		$formBuilder->addFilterInput('recibos', 'Recibos', array('class'=>'form-group'), array('class' => 'form-control mb-2 mr-sm-2', 'label' => false, 'type' => 'checkbox'));  
        		
        		
        		$formBuilder->setHeaderAndFieldFromRow($data);
        		
        		
        	
        		
        		
        		
        		$valid = "
                    function validateFilter(){

				
                        Validator.clearValidationMsgs('validationMsg_');
        				
                        var form = 'ComprobanteMontosAgrupadosdeValoresForm';
                        var validator = new Validator(form);
        				
                        validator.validateDate('ComprobanteFechaContable', 'Fecha Desde');
                        validator.validateDate('ComprobanteFechaContableHasta', 'Fecha Hasta');
  						validator.validateDateSmallerOrEqual('ComprobanteFechaContable', 'Fecha Desde','ComprobanteFechaContableHasta','Fecha Hasta');
  						validator.validateDateInterval('ComprobanteFechaContable', 'Fecha Desde','ComprobanteFechaContableHasta','Fecha Hasta',30);
                        
        				
        				
                        if(!validator.isAllValid()){
						
                            validator.showValidations('', 'validationMsg_');
                            return false;
                        }
        				
                        return true;
        				
                    }
                 ";
        		
        		$formBuilder->addCommonScript($valid);
        		
        		//Validaciones
        		$formBuilder->setFilterValidationFunction('validateFilter()');
        		
        		
        		//$this->data = $sql_detalle;
        		$this->layout = "desktop_panel";
        		
        		
        		$formBuilder->showBackDatatableButton(false);
        		$formBuilder->showCopyDatatableButton(true);
        		$formBuilder->showExcelDatatableButton(true);
        		
        		$this->set('abm',$formBuilder);
        		$this->render('/FormBuilder/listado_datatable');
        		
        		/*
        		$this->layout = "desktop_panel";
        		$this->set('abm',$formBuilder);
        		$this->render('/FormBuilder/listado_new');
        		*/
        		
        		
        }
        
        
        
      
    
    
    
        
        
        
        
         
    }
?>