///recordar cambiar los detalle_tipo_comprobante de la tabla sino es grave. Estos son los creados por el USUARIO.


SET FOREIGN_KEY_CHECKS=0;

UPDATE comprobante SET id_detalle_tipo_comprobante = id_detalle_tipo_comprobante+1000 WHERE id_detalle_tipo_comprobante>57;
UPDATE detalle_tipo_comprobante SET id = id+1000 WHERE id>57;

SET FOREIGN_KEY_CHECKS=1;


SELECT * FROM detalle_tipo_comprobante

SELECT MAX(id) FROM detalle_tipo_comprobante  ///obtengo valor

ALTER TABLE detalle_tipo_comprobante AUTO_INCREMENT=<mayor id tabla detalle_tipo_comprobante>; 




INSERT INTO `detalle_tipo_comprobante` (`id`, `d_detalle_tipo_comprobante`) VALUES ('64', 'Mantenimiento Correctivo');
UPDATE `detalle_tipo_comprobante` SET `id_tipo_comprobante` = '1000' , `descripcion_accion` = 'Mantenimiento Correctivo' WHERE `id` = '64'; 
INSERT INTO `detalle_tipo_comprobante` (`id`, `d_detalle_tipo_comprobante`, `id_tipo_comprobante`, `descripcion_accion`) VALUES ('65', 'Mantenimiento Preventivo', '1000', 'Mantenimiento Preventivo'); 

INSERT INTO `detalle_tipo_comprobante` (`d_detalle_tipo_comprobante`) VALUES ('Mantenimiento Predictivo'); 

UPDATE `detalle_tipo_comprobante` SET `id_tipo_comprobante` = '1000' WHERE `id` = '66'; 
 INSERT INTO `detalle_tipo_comprobante` (`d_detalle_tipo_comprobante`) VALUES ('Mantenimiento Cero Horas (Overhaul):'); 
UPDATE `detalle_tipo_comprobante` SET `d_detalle_tipo_comprobante` = 'Mantenimiento Cero Horas (Overhaul)' , `id_tipo_comprobante` = '1000' , `descripcion_accion` = 'Mantenimiento Cero Horas (Overhaul):' WHERE `id` = '67'; 
UPDATE `detalle_tipo_comprobante` SET `descripcion_accion` = 'Mantenimiento Predictivo' WHERE `id` = '66'; 
INSERT INTO `detalle_tipo_comprobante` (`id`, `d_detalle_tipo_comprobante`, `id_tipo_comprobante`, `descripcion_accion`) VALUES ('68', 'Mantenimiento En Uso:', '1000', 'Mantenimiento En Uso:'); 
 UPDATE `detalle_tipo_comprobante` SET `descripcion_accion` = 'Mantenimiento En Uso' WHERE `id` = '68'; 
 UPDATE `detalle_tipo_comprobante` SET `descripcion_accion` = 'Mantenimiento Cero Horas (Overhaul)' WHERE `id` = '67'; 
UPDATE `detalle_tipo_comprobante` SET `d_detalle_tipo_comprobante` = 'Mantenimiento En Uso' WHERE `id` = '68'; 


INSERT INTO `detalle_tipo_comprobante` VALUES('73', 'Instrumental', '103', 'OT - Operacion - Instrumental', NULL, '0', '0', '0', '0', NULL, NULL, '0', '0', '0');

INSERT INTO `detalle_tipo_comprobante` VALUES('72', 'Operacion', '103', 'OT - Operacion - Servicio de Produccion Interna', NULL, '0', '0', '0', '0', NULL, NULL, '0', '0', '0');

INSERT INTO `detalle_tipo_comprobante` VALUES('71', 'Reserva de Bien Uso', '103', 'OT - Reserva - Bien Uso', NULL, '0', '0', '0', '0', NULL, NULL, '0', '0', '0');

INSERT INTO `detalle_tipo_comprobante` VALUES('70', 'Etapa', '103', 'OT - Etapa - Planificacion', NULL, '0', '0', '0', '0', NULL, NULL, '0', '0', '0');

UPDATE `detalle_tipo_comprobante` SET `id` = '69', `d_detalle_tipo_comprobante` = 'OT - Cabecera', `id_tipo_comprobante` = '103', `descripcion_accion` = 'OT - Cabecera - Articulo a producir', `default` = NULL, `afecta_stock_origen` = '0', `tiene_iva` = '0', `tiene_impuestos` = '0', `puede_editar_precio_unitario` = '0', `id_impuesto` = NULL, `id_cuenta_contable` = NULL, `genera_nd` = '0', `dias` = '0', `dias2` = '0' WHERE `id` = '69';


 INSERT INTO `reporte` (`id`, `d_reporte`, `controller`, `method`, `id_mime_type`, `leyenda`) VALUES ('19', 'Ventas Por Articulo', 'Reportes', 'VentasPorArticulo', '519', 'Ventas Por Articulo'); 
 INSERT INTO `reporte` (`id`, `d_reporte`, `controller`, `method`, `id_mime_type`, `leyenda`) VALUES ('20', 'Compras Por Articulo', 'Reportes', 'ComprasPorArticulo', '519', 'Compras Por Articulo'); 

