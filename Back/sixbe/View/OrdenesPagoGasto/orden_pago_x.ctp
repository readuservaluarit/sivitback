<?php   



/**
 * PDF exportación de Orden Compra
 */

App::import('Vendor','tcpdf/tcpdf'); 
App::import('Vendor','tcpdf/tcpdf_barcodes_1d'); 


// create new PDF document
$fecha_entrega = new DateTime($datos_pdf['Comprobante']['fecha_contable']);
$fecha_inicio_actividad = new DateTime($datos_empresa["DatoEmpresa"]["fecha_inicio_actividad"]);


$aviso_anulado = '';

        

if($datos_pdf['Comprobante']['id_estado_comprobante']!= EnumEstadoComprobante::CerradoReimpresion)
    $aviso_anulado = " PROFORMA ";
    
if($datos_pdf['Comprobante']['id_estado_comprobante'] == EnumEstadoComprobante::Anulado)
        $aviso_anulado = ' ANULADO';    
        
        
        

$html = '<table width="100%" border="0" cellspacing="0" style="font-size:10px;border:2px solid #000000; font-family:\'Courier New\', Courier, monospace">
          <tr>
            <td style="padding: 5px 5px 5px 5px;" width="37%" rowspan="4" align="center" valign="center"  ><img src="'.$datos_empresa["DatoEmpresa"]["app_path"].'/img/'.$datos_empresa["DatoEmpresa"]["id"].'.png" width="476" height="129" />  </td>
            <td width="8%" height="3%" rowspan="2"  style="border:2px solid #000000">
                <div align="center" style="font-size:30px">'.strtoupper($datos_pdf["TipoComprobante"]["letra"]).'</div>
                 <span align="center" style="font-size:10px;"></span>
            </td>
            <td width="55%" style="padding-left:5px; font-size:17px;font-weight:bold;"> Orden de Pago de Gasto '.$aviso_anulado.': </td>
            
          </tr>
          <tr>';
          
          
          if(isset($datos_pdf["PuntoVenta"]["numero"]) && $datos_pdf["Comprobante"]["id_punto_venta"]>0)
          	$html .='  <td style="padding-left:5px; font-size:15px;"> Nro '.str_pad($datos_pdf["PuntoVenta"]["numero"],4,"0",STR_PAD_LEFT).'-'.str_pad($datos_pdf["Comprobante"]["nro_comprobante"],8,"0",STR_PAD_LEFT).'</td>';
          else	
          	$html .='  <td style="padding-left:5px; font-size:15px;"> Nro '.str_pad($datos_pdf["Comprobante"]["d_punto_venta"],4,"0",STR_PAD_LEFT).'-'.str_pad($datos_pdf["Comprobante"]["nro_comprobante"],8,"0",STR_PAD_LEFT).'</td>';
          
          
          $html .='</tr> 
          
          <tr>
            <td width="8%" height="2%" align="center" valign="middle" >&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr >
            <td  width="8%" height="5%" align="center" valign="middle" >&nbsp;</td>
            <td style="padding-left:10px;"></td>
    
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["calle"].' '.$datos_empresa["DatoEmpresa"]["numero"].' '.$datos_empresa["DatoEmpresa"]["codigo_postal"].' '.$datos_empresa["Provincia"]["d_provincia"].'</td>
            <td rowspan="5">&nbsp;</td>
            <td style="padding-left:5px;"> Fecha: '.$fecha_entrega->format('d-m-Y').'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> E-mail: '.$datos_empresa["DatoEmpresa"]["email"].'</td>
            <td style="padding-left:5px;"> CUIT: '.$datos_empresa["DatoEmpresa"]["cuit"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> Tel: '.$datos_empresa["DatoEmpresa"]["tel"].'</td>
            <td style="padding-left:5px;"> Ingresos Brutos: '.$datos_empresa["DatoEmpresa"]["iibb"].'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.$datos_empresa["DatoEmpresa"]["website"].'</td>
            <td style="padding-left:5px;"> Inicio de actividades: '.$fecha_inicio_actividad->format('d-m-Y').'</td>
          </tr>
          <tr>
            <td style="padding-left:5px;"> '.strtoupper($datos_empresa["TipoIva"]["d_tipo_iva"]).'</td>
            <td style="padding-left:5px;">&nbsp;</td>
          </tr>
        </table>';

$html;



class MyTCPDF extends TCPDF{

    public $datos_header;
    public $datos_footer;
    public $bar_code;
    public $observaciones;
    
    public function set_datos_header($datos_empresa){
        $this->datos_header = $datos_empresa; 

    }
    public function set_datos_footer($datos_empresa_footer){
        $this->datos_footer = $datos_empresa_footer; 

    }
     public function set_barcode($bar_code){
        $this->bar_code = $bar_code; 

    }
    
    public function set_observaciones($observaciones){
        $this->observaciones = $observaciones; 

    }
  public function Header(){
     
       if ($this->page == 1) {      
        
    		 $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_header, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);

  	 }
     
     //$this->setCellPaddings( $left = '', $top = '', $right = '', $bottom = 50);
  }
  
  
   protected $last_page_flag = false;

    public function Close() {
    $this->last_page_flag = true;
    parent::Close();
  }
  
  
  public function Footer(){
     
        
         if ($this->last_page_flag) {    
				     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_footer, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
				     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->observaciones, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
				     
				     $style['text'] = true;
				     $style['border'] = true;
				     $style['align'] = 'C';
				     $this->write1DBarcode($this->bar_code, 'I25', '40', '', '', 9, 0.4, $style, 'N');
				      
				      //$this->writeHTMLCell($w = 0, $h = 0, $x = '200', $y = '', $this->bar_code, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
				      //$this->Cell(0, 20, $this->bar_code , 0, false, 'C', 0, '', 0, false, 'T', 'M');
				      $this->Cell(0, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
      
      }
      
  }
  
}

$pdf = new MyTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
     
$pdf->set_datos_header($html);

 
$descuento = (($datos_pdf["Comprobante"]["total_comprobante"]*$datos_pdf["Comprobante"]["descuento"])/100) ;



//genero el codigo de barras para ver las especificaciones http://www.formularioscontinuos.com/nota1.html
if( $datos_pdf["Comprobante"]["cae"] != '' ){

    //genero el string para el codigo de barras
    $cuit =  str_replace("-","",$datos_empresa["DatoEmpresa"]["cuit"]);
    $tipo_doc_afip = str_pad($datos_pdf["TipoComprobante"]["codigo_afip"],2,"0",STR_PAD_LEFT);
    $cae = $datos_pdf["Comprobante"]["cae"];
    $fecha_vencimiento = str_replace("-","",$datos_pdf["Comprobante"]["vencimiento_cae"]);
    
    $codigo_barras = $cuit.$tipo_doc_afip.$cae.$fecha_vencimiento;
    $codigo_barras = $codigo_barras.digitoVerificadorCaeAfip($codigo_barras);
    $pdf->set_barcode($codigo_barras);  
    
   
}  

$suma_impuestos = 0;
$impuestos = '';
if(isset($datos_pdf["ComprobanteImpuesto"])){

        foreach($datos_pdf["ComprobanteImpuesto"] as $impuesto){


			if(isset($impuesto["ComprobanteReferencia"]["id"]) && $impuesto["ComprobanteReferencia"]["id"]>0)
				$nro_retencion = "(Nro: ".$impuesto["ComprobanteReferencia"]["nro_comprobante"].")";
			else
				$nro_retencion = "";
				
					
            $impuestos .= ' <tr>
                 <td>'.$impuesto["Impuesto"]["d_impuesto"].$nro_retencion.'</td>
                 <td>'.money_format('%!n',$impuesto["tasa_impuesto"]).'%</td>
                <td>'.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n',$impuesto["importe_impuesto"]).'</td>
               </tr>';
               
               $suma_impuestos = $suma_impuestos + $impuesto["importe_impuesto"];
               
        }
} 



  
  
ob_clean();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sivit by Valuarit');
$pdf->SetTitle('Factura');
$pdf->SetSubject('Factura');
//$pdf->SetKeywords('');

                              

// set default header data
$logo = 'img/header_factura.jpg';
$title = "";
$subtitle = ""; 
//$pdf->SetHeaderData($logo, 190, $title, $subtitle, array(0,0,0), array(0,104,128));
$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 50, 20);
$pdf->SetHeaderMargin(10);
$pdf->SetFooterMargin(25);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 60);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
/*$lg = Array();
$lg = Array();
$lg['a_meta_charset'] = 'ISO-8859-1';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';
*/
$pdf->setLanguageArray($lg);
    
// ---------------------------------------------------------

// set default font subsetting mode
//$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
/*
$html = <<<EOD
<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
EOD;
*/

$tbody = "";

$renglones = $datos_pdf["ComprobanteItemComprobante"];

$data = array();
$comprobante = new Comprobante();
$datoEmpresa = $this->Session->read('Empresa');

$suma_diferencia_cambio = 0;
$suma_comprobantes = 0;

foreach($renglones as $renglon){

		$fecha_vencimiento = new DateTime($renglon['ComprobanteOrigen']['fecha_vencimiento']);
	
  	
   		if($renglon['ComprobanteOrigen']['id_moneda_enlazada'] == $datoEmpresa["DatoEmpresa"]["id_moneda"])
   			$cotizacion = $renglon['ComprobanteOrigen']['valor_moneda'];
   		elseif ($renglon['ComprobanteOrigen']['id_moneda_enlazada'] == $datoEmpresa["DatoEmpresa"]["id_moneda2"])
   			$cotizacion = 1/$renglon['ComprobanteOrigen']['valor_moneda2'];
   		elseif ($renglon['ComprobanteOrigen']['id_moneda_enlazada'] == $datoEmpresa["DatoEmpresa"]["id_moneda3"])
   			$cotizacion = 1/$renglon['ComprobanteOrigen']['valor_moneda3'];
 
   
   $tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
   $tbody .= "<td>" .$renglon['ComprobanteOrigen']['TipoComprobante']['codigo_tipo_comprobante'] . "</td>";
   
   if(isset($renglon['ComprobanteOrigen']['id_punto_venta']))
   	$tbody .= "<td>" .$comprobante->GetNumberComprobante($renglon['ComprobanteOrigen']['PuntoVenta']['numero'],$renglon['ComprobanteOrigen']['nro_comprobante']) . "</td>";       
   else
   	$tbody .= "<td>" .$comprobante->GetNumberComprobante($renglon['ComprobanteOrigen']['d_punto_venta'],$renglon['ComprobanteOrigen']['nro_comprobante']) . "</td>";
   
   $tbody .= "<td>" .$fecha_vencimiento->format('d-m-Y')."</td>";
   
   if($renglon['ComprobanteOrigen']["id_moneda"]!= EnumMoneda::Peso)
   $tbody .= "<td>" .$renglon['ComprobanteOrigen']["Moneda"]["simbolo_internacional"]." ".money_format('%!n',round($renglon['precio_unitario']/$cotizacion,4))."| ARS ".money_format('%!n',$renglon['precio_unitario'])."</td>";
   else
   	$tbody .= "<td>" .$renglon['ComprobanteOrigen']["Moneda"]["simbolo_internacional"]." ".money_format('%!n',$renglon['precio_unitario'])."</td>";
   
   if($renglon['ComprobanteOrigen']['enlazar_con_moneda'] == 1 && $renglon['ajusta_diferencia_cambio'] == 1 ){
   
   		$tbody .= "<td>Si ".$renglon['ComprobanteOrigen']['MonedaEnlazada']["simbolo_internacional"]."</td>";
   	
   		$tbody .= "<td>".money_format('%!n',round($cotizacion,4))."</td>";
   		
   		if($renglon['monto_diferencia_cambio'] > 0)
			$tbody .= "<td>ND por el valor de " .money_format('%!n',$renglon['monto_diferencia_cambio'])."</td>";
		else
		   $tbody .= "<td>NC por el valor de " .money_format('%!n',$renglon['monto_diferencia_cambio'])."</td>";
		   
		$suma_diferencia_cambio += $renglon['monto_diferencia_cambio'];
		
			   
   }else{
   
   	   $tbody .= "<td>No</td>";
   	   $tbody .= "<td></td>";
   	   $tbody .= "<td></td>";
   }
   
   $suma_comprobantes  += $renglon['precio_unitario']*$renglon['ComprobanteOrigen']['TipoComprobante']['signo_comercial'];
   
   
   
   $tbody .= "</tr>";
   
   
   if($renglon["importe_descuento_unitario"]>0){//agrego una fila para mostrarle que le hice un descuento
   
   
   	 
   $tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
   $tbody .= "<td>Descuento</td>";
   $tbody .= "<td></td>";       
   $tbody .= "<td></td>";
   $tbody .= "<td>" .$renglon['importe_descuento_unitario']."</td>";
   $tbody .= "<td></td>";
   $tbody .= "<td></td>";
   $tbody .= "<td></td>";
   $tbody .= "</tr>";
   
   }
}

	 

   $tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
   $tbody .= "<td><strong>Total</strong></td>";
   $tbody .= "<td></td>";       
   $tbody .= "<td></td>";
   $tbody .= "<td><strong>".$datos_pdf["Moneda"]["simbolo_internacional"]." ".money_format('%!n',$suma_comprobantes)."</strong></td>";
   $tbody .= "<td></td>";
   $tbody .= "<td></td>";
   $tbody .= "<td></td>";
   $tbody .= "</tr>";







$renglones_credito_interno = $credito_interno_relacionados;

foreach($renglones_credito_interno as $renglon){

 
   
   $tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
   $tbody .= "<td>" .$renglon['Comprobante']['codigo_tipo_comprobante'] . " (Gen.)</td>";
   $tbody .= "<td>" .$comprobante->GetNumberComprobante($renglon['PuntoVenta']['numero'],$renglon['Comprobante']['nro_comprobante']) . "</td>";       
   $tbody .= "<td>" .$comprobante->formatDate($renglon["Comprobante"]["fecha_vencimiento"])."</td>";
   $tbody .= "<td>" .money_format('%!n',$renglon['Comprobante']["total_comprobante"])."</td>";
   $tbody .= "<td></td>";
   $tbody .= "<td></td>";
   $tbody .= "<td></td>";
   $tbody .= "</tr>";
   

}



$renglones_debito_interno = $debito_interno_relacionados;

$sumatoria_comprobantes = 0;

foreach($renglones_debito_interno as $renglon){

 
   
   $tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
   $tbody .= "<td>" .$renglon['Comprobante']['codigo_tipo_comprobante'] . " (Gen.)</td>";
   $tbody .= "<td>" .$comprobante->GetNumberComprobante($renglon['PuntoVenta']['numero'],$renglon['Comprobante']['nro_comprobante']) . "</td>";       
   $tbody .= "<td>" .$comprobante->formatDate($renglon["Comprobante"]["fecha_vencimiento"])."</td>";
   $tbody .= "<td>" .money_format('%!n',$renglon['Comprobante']["total_comprobante"])."</td>";
   $tbody .= "<td></td>";
   $tbody .= "<td></td>";
   $tbody .= "<td></td>";
   $tbody .= "</tr>";
}



 $tbody2 = '';
 
 $renglones2 = $datos_pdf["ComprobanteValor"];

 $suma_valores = 0;
 
foreach($renglones2 as $renglon){
    
    
    
    switch($renglon['Valor']['id']) {
        
        case EnumValor::TRANSFERENCIAS:
           $tbody2 .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
           $tbody2 .= "<td>" .$renglon['Valor']['d_valor'] . "</td>";
           $tbody2 .= "<td>" .$renglon['Valor']['id'] . "</td>";
           $tbody2 .= "<td>" .$renglon['CuentaBancaria']['nro_cuenta']. "</td>";       
           $tbody2 .= "<td>" .money_format('%!n',round($renglon['monto'],2))."</td>";
           $tbody2 .= "</tr>";
           $suma_valores += round($renglon['monto'],2);
        break;
        
        case EnumValor::EFECTIVO:
        
           $tbody2 .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
           $tbody2 .= "<td>" .$renglon['Valor']['d_valor']. "</td>";
           $tbody2 .= "<td></td>";
           $tbody2 .= "<td>" .$renglon['d_comprobante_valor']. "</td>";       
           $tbody2 .= "<td>" .money_format('%!n',round($renglon['monto'],2))."</td>";
           $tbody2 .= "</tr>";
           $suma_valores += round($renglon['monto'],2);
        break;
        
         case EnumValor::COMPENSACION:
        
           $tbody2 .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
           $tbody2 .= "<td>" .$renglon['Valor']['d_valor']. "</td>";
           $tbody2 .= "<td></td>";
           $tbody2 .= "<td>" .$renglon['d_comprobante_valor']. "</td>";       
           $tbody2 .= "<td>" .money_format('%!n',round($renglon['monto'],2))."</td>";
           $tbody2 .= "</tr>";
           $suma_valores += round($renglon['monto'],2);
        break;
        
        
      
        
    }
 
}


 

$tbody3 = '';
 
   $renglones3 = $datos_pdf["ChequeSalida"];

foreach($renglones3 as $renglon){


	$fecha_cheque = new DateTime($renglon['fecha_cheque']);

   $tbody3 .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
   
   
 
   $tbody3 .= "<td>" .$renglon['id'] . "</td>";
   
   if($renglon['id_tipo_cheque'] == EnumTipoCheque::Terceros){
   		  $tbody3 .= "<td>" .$renglon['nro_cheque'] . "</td>";
   		$tbody3 .= "<td>" .$renglon['Banco']['d_banco']. "</td>";   
   	}else{
   		  $tbody3 .= "<td>Propio: " .$renglon['nro_cheque'] . "</td>";
   		$tbody3 .= "<td>" .$renglon['Chequera']['CuentaBancaria']['BancoSucursal']['Banco']['d_banco']. "</td>"; 
   	
   	}  
   
     
   $tbody3 .= "<td>" .$fecha_cheque->format('d-m-Y'). "</td>";       
   $tbody3 .= "<td>" .money_format('%!n',round($renglon['monto'],2))."</td>";
   $tbody3 .= "</tr>";
   $suma_valores += round($renglon['monto'],2);
}
 
 
 
 
 
 
//agrego las observaciones

$time = strtotime($datos_pdf["Comprobante"]["fecha_vencimiento"]);
$month_venc = date("m",$time);
$year_venc =  date("y",$time);
$day_venc =   date("d",$time);






$html='';


$html .=' 
 <table width="100%"   style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
  <tr >
                    <td colspan="4"></td>
                  </tr>
  <tr>  
  <tr >
                    <td colspan="4"></td>
                  </tr>
  <tr> 
 
 </table>';
 



if(isset($datos_pdf["CarteraRendicion"]["d_cartera_rendicion"]) && strlen($datos_pdf["CarteraRendicion"]["d_cartera_rendicion"])>0){
                  
                $html.=' <table width="100%" border="1"  style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
                  <tr  style="background-color:rgb(224, 225, 229);">
                    <td width="100%"><div align="center"><strong>Cartera / Fondo:</strong> '.$datos_pdf["CarteraRendicion"]["d_cartera_rendicion"].'</div></td>
                  </tr>
                    
                    
                                 </table>';

}else{

  $html.='<table width="100%" border="1"  style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
                  <tr  style="background-color:rgb(224, 225, 229);">
                    <td colspan="4"><div align="center"><strong>Proveedor</strong></div></td>
                  </tr>
                  <tr>                                     
                    <td width="17%"><strong>Codigo:</strong> '.$datos_pdf["Persona"]["codigo"].'</td>
                    <td width="35%"><strong>Raz&oacute;n Social:</strong> '.$datos_pdf["Persona"]["razon_social"].'</td>
                    <td width="26%"><strong>Cuit:</strong> '.$datos_pdf["Persona"]["cuit"].' </td>
              
                    <td width="22%"></td>
                    </tr>
                    
                  <tr>
                    <td colspan="3"><strong>Direcci&oacute;on:</strong> '.$datos_pdf["Persona"]["calle"].' - '.$datos_pdf["Persona"]["numero_calle"].' - '.$datos_pdf["Persona"]["localidad"].' - '.$datos_pdf["Persona"]["ciudad"].' - '.$datos_pdf["Persona"]["Provincia"]["d_provincia"].' - '.$datos_pdf["Persona"]["Pais"]["d_pais"].'  </td>
                    <td><strong>Tel&eacute;fono:</strong> '.$datos_pdf["Persona"]["tel"].'</td>
                  </tr>
                    
                    
                                 </table>';
                    
                    

               

}
                  
              
$html.='
<table width="100%" border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;font-family:\'Courier New\', Courier, monospace">

   
     <tbody>
     
        <tr >
        <td colspan="2"> </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
 
        <tr >
        <td  colspan="6" ><HR></td>
      
         </tr>
     </tbody>
</table>


<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">
    <tr>
    <td class="sorting"  rowspan="1" colspan="7" style="width:100%;text-decoration:underline;text-align:left"><strong>Comprobantes imputados</strong></td>
    </tr>
    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th style="width:10%;"><strong>Tipo Comp.</strong></th>
        <th style="width:20%;"><strong>Nro. Comp.</strong></th>
        <th style="width:15%;"> <strong>Fcha Vto</strong></th>
        <th style="width:15%;"><strong>Importe</strong></th>
        <th style="width:10%;"><strong>Enlazado Tipo Cambio</strong></th>
        <th style="width:15%;"><strong>Cotizaci&oacute;n</strong></th>
        <th style="width:15%;"><strong>NC/ND</strong></th>
    </tr>

  <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
    '.$tbody.'
  </tbody> 
  
  
  
   
</table>


<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">
      <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
     <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
    <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;text-decoration:underline;"><strong>Valores</strong></td>
    </tr>

    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong>Valor</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>Id.</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:25%;"><strong>Desc.</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:50%;"><strong>Total.</strong></th>
      
    </tr>

  <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
  
    '.$tbody2.'
  </tbody> 
  
   
</table>';


  
if(strlen($tbody3)>0){

$html.='<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">
      <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
     <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
    <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;text-decoration:underline;"><strong>Cheques</strong></td>
    </tr>
    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>Id.</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:25%;"><strong>Nro.Cheque</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:25%;"><strong>Banco</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:15%;"><strong>F. Vencimiento</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:30%;"><strong>Monto</strong></th>
      
    </tr>.';

 
  
  $html.='<tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
        '.$tbody3.'
      </tbody></table>'; 
  
}   


 $html.='
     <table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">  
 <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
       <tr>
        <td class="sorting"  rowspan="1" colspan="1" style="width:100%;"><HR></td>
       </tr>
      </tbody></table>'; 
      
      
 $html.='
     <table width="100%"  cellspacing="1" cellpadding="2" style="font-size:10px;font-family:\'Courier New\', Courier, monospace">  
 <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
       <tr>
        <td class="sorting"  rowspan="1" colspan="1" style="width:50%;text-align:left;"><strong>Total</strong></td>
        <td class="sorting"  rowspan="1" colspan="1" style="width:50%;text-align:left;"><strong>'.money_format('%!n',$suma_valores).'</strong></td>
       </tr>
      </tbody></table>'; 


if($datos_pdf["Comprobante"]["descuento"]>0){

//total_comprobante - subtotal_bruto 

$html.='<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">
    
     <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
    <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;text-decoration:underline;"><strong>Descuento</strong></td>
    </tr>
    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:50%;"><strong>Descuento (%)</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:50%;"><strong>Monto</strong></th>
    
      
    </tr>';

$html.='<tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
      <tr>
        <td>
        '.round($datos_pdf["Comprobante"]["descuento"],2).'%
        </td>
        
         <td>
        '.round($datos_pdf["Comprobante"]["importe_descuento"],2).'
        </td>
        
      </tr>
      </tbody></table>'; 

}


if(strlen($impuestos)>0){

$html.='<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
      <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
     <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
    <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;text-decoration:underline;"><strong>Retenciones</strong></td>
    </tr>
    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:60%;"><strong>Desc</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>Tasa</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:30%;"><strong>Monto</strong></th>
      
    </tr>.';

 
  
  $html.='<tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
        '.$impuestos.'
      </tbody></table>'; 
  
}  





if($datos_pdf["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::OrdenPagoAnticipo)
	$total = $suma_valores;
else
	$total = ($suma_comprobantes + $suma_diferencia_cambio - $suma_impuestos - $datos_pdf["Comprobante"]["importe_descuento"]);

    	


$footer='
  <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>                     
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody> 
</table>


<table width="100%" border="1" style="width:100%;">
   <tr> 
     <td width="60%">Observaciones: Ref 1 USD= '.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n',round($datos_pdf["Comprobante"]["valor_moneda"],4)).' Ref Interna nro ='.$datos_pdf["Comprobante"]["id"].' '.$datos_pdf["Comprobante"]["observacion"].'</td>
     <td width="40%">
     <table width="100%" border="0">
      
      
       <tr>
         <td><strong>TOTAL</strong></td>
         <td>&nbsp;</td>
         <td><strong>'.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n',$total).'</strong></td>
       </tr>
     </table></td>
   </tr>';
   
   
  
  
  
   $footer .= '<tr>
     <td > Original blanco / copia color</td>

   </tr>';
  
  
  
   
   
   

$footer .='</table>';


   
   $html .= "<HR>";
   
   
   if( $datos_pdf["Comprobante"]["cae"] != '' ){
   
      $html.=$pdf->SetBarcode($codigo_barras,I25);
     
   
   }
   
   
   $html .= '
   
   <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody>
</table>
   
  ';
  
  $pdf->set_datos_footer($footer);
  
   
   
   
$pdf->writeHTML($html, true, false, true, false, true);


// QRCODE,L : QR-CODE Low error correction
// set style for barcode
$style = array(
    'border' => 2,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

//$pdf->Text(20, 25, 'QRCODE L');

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->SetBarcode(date("Y-m-d H:i:s", time()));
ob_clean();
$pdf->Output(str_replace(".","",$datos_pdf["TipoComprobante"]["abreviado_tipo_comprobante"].'_'.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'-'.$datos_pdf["Persona"]["razon_social"]).'.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
