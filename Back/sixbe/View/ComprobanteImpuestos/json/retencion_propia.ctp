<?php
 /* Registro de ejemplo
id   
id_comprobante   = NRO_RECIBO_relacionado
id_moneda =
id_impuesto
id_categoria_impuesto
id_item_asiento_contable
base_imponible
importe_impuesto
tasa_impuesto
porcentaje_reduccion
id_provincia
tipo_alicuota_afip
numero = NRO_RETENCION
fecha = fecha _ cargada por usuario 
d_comprobante_impuesto =  Observacion de la retencion
*/


         include(APP.'View'.DS.'ComprobanteImpuestos'.DS.'json'.DS.'comprobante_impuesto.ctp');    
        
        $model["id_impuesto"]["show_grid"] = 1;
        $model["id_impuesto"]["header_display"] = "Impuesto";
        $model["id_impuesto"]["order"] = "0";
        $model["id_impuesto"]["width"] = "32";
		// $model["id_impuesto"]["read_only"] = "0";
		$model["id_impuesto"]["text_colour"] = "#0000AA";
		
		$model["d_comprobante_impuesto"]["show_grid"] = 1;
        $model["d_comprobante_impuesto"]["header_display"] = "Observaci&oacute;n";
        $model["d_comprobante_impuesto"]["order"] = "1";
        $model["d_comprobante_impuesto"]["width"] = "15";
		$model["d_comprobante_impuesto"]["read_only"] = "0";
		$model["d_comprobante_impuesto"]["text_colour"] = "#110022";
		
		$model["numero"]["show_grid"] = 1;
        $model["numero"]["header_display"] = "Nro. Comprobante";
        $model["numero"]["order"] = "2";
        $model["numero"]["width"] = "20";
		$model["numero"]["read_only"] = "1"; //Tiene q ser condicional para el casao de  las retenciones manuales pero las automaticas pueden ser rean onlu
		$model["numero"]["text_colour"] = "#000000";
        
        $model["fecha"]["show_grid"] = 1;
        $model["fecha"]["header_display"] = "Fecha";
        $model["fecha"]["order"] = "3";
        $model["fecha"]["width"] = "15";
		$model["fecha"]["read_only"] = "0"; //Tiene q ser condicional para el casao de  las retenciones manuales pero las automaticas pueden ser rean onlu
		$model["fecha"]["text_colour"] = "#153FE8";
		
		$model["base_imponible"]["show_grid"] = 1;
        $model["base_imponible"]["header_display"] = "Base Imponible";
        $model["base_imponible"]["order"] = "4";
        $model["base_imponible"]["width"] = "15";
		$model["base_imponible"]["read_only"] = "0";
        $model["base_imponible"]["min_value"] = "0";
		$model["base_imponible"]["text_colour"] = "#CC0022";
        
		$model["importe_impuesto"]["show_grid"] = 1;
        $model["importe_impuesto"]["header_display"] = "Importe";
        $model["importe_impuesto"]["order"] = "5";
        $model["importe_impuesto"]["width"] = "15";
		$model["importe_impuesto"]["read_only"] = "0";
		$model["importe_impuesto"]["text_colour"] = "#004400";	
		
		$model["tasa_impuesto"]["show_grid"] = 1; ///*AHora es un campo calculado en principio por el back pero luego por .net*/
        $model["tasa_impuesto"]["header_display"] = "Alic.(%)";
        $model["tasa_impuesto"]["order"] = "6";
        $model["tasa_impuesto"]["width"] = "5";
		$model["tasa_impuesto"]["read_only"] = "0";
        $model["tasa_impuesto"]["min_value"] = "0";
        $model["tasa_impuesto"]["max_value"] = "100";
		$model["tasa_impuesto"]["text_colour"] = "#0000AA";
		
		//INVISIBLE
		$model["id_moneda"]["show_grid"] = 0;
        $model["id_moneda"]["header_display"] = "Cod.Moneda";
        $model["id_moneda"]["order"] = "7";
		$model["id_moneda"]["type"] = EnumTipoDato::entero;
        $model["id_moneda"]["width"] = "10";
		$model["id_moneda"]["read_only"] = "1";
		
		//DEBERIA ESTAR VISIBLE
		$model["d_moneda"]["show_grid"] = 0;
        $model["d_moneda"]["header_display"] = "Moneda";
        $model["d_moneda"]["order"] = "8";
        $model["d_moneda"]["width"] = "10";
		$model["d_moneda"]["read_only"] = "1";
		$model["d_moneda"]["text_colour"] = "#006600";	
        
  
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteImpuesto",
                    "content" => $model
                );
        echo json_encode($output);
?>


