<?php
    App::uses('FacturasController', 'Controller');

    /**
    * @secured(CONSULTA_FACTURA_CREDITO)
    */
    class FacturasCreditoController extends FacturasController {

        public $name = EnumController::FacturasCredito;
        public $model = 'Comprobante';
        public $helpers = array ('Session', 'Js');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
        public $requiere_impuestos = 1; //si el comprobante acepta impuesto, esto llama a las funciones de IVA E IMPUESTOS
        public $id_sistema_comprobante = EnumSistema::VENTAS;
        public $type_message = array();

        
        
        
        /**
         * @secured(CONSULTA_FACTURA_CREDITO)
         */
        public function index()
        {
        	
        	parent::index();
        }
        
        
        /**
         * @secured(CONSULTA_FACTURA_CREDITO)
         */
        public function existe_comprobante()
        {
        	parent::existe_comprobante();
        }
        
        
        /**
         * @secured(ADD_FACTURA_CREDITO)
         */
        public function add(){
        	
        	parent::add();
        	
        }
        
        
        /**
         * @secured(MODIFICACION_FACTURA_CREDITO)
         */
        public function edit($id){
        	
        	
        	parent::edit($id);
        	return;
        	
        }
        
        
        
        
        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
        	
        	$model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        	
        	
        	$this->set('model',$model);
        	Configure::write('debug',0);
        	$this->render($vista);
        }
        
        
        
        /**
         * @secured(BAJA_FACTURA_CREDITO)
         */
        public function delete($id){
        	
        	parent::delete($id);
        	
        }
        
        /**
         * @secured(BAJA_FACTURA_CREDITO)
         */
        public function deleteItem($id,$externo=1){
        	
        	parent::deleteItem($id,$externo);
        	
        }
        
        
        /**
         * @secured(CONSULTA_FACTURA_CREDITO)
         */
        public function getModel($vista='default'){
        	
        	$model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
        	
        }
        
        
        /**
         * @secured(BTN_PDF_FACTURASCREDITO)
         */
        public function pdfExport($id,$vista=''){
        	
        	
        	parent::pdfExport($id,$vista);
        	
        	
        	
        }
      
        
        /**
         * @secured(CONSULTA_FACTURA_CREDITO)
         */
        public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
        {
        	parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
        }
        
        
        
        public function CalcularImpuestos($id_comprobante,$id_tipo_impuesto='',$error=0,$message=''){
        	
        	
        	$id_tipo_impuesto = EnumTipoImpuesto::PERCEPCIONES;
        	return parent::CalcularImpuestos($id_comprobante,$id_tipo_impuesto);
        	
        }
        
        
        /**
         * @secured(MODIFICACION_FACTURA_CREDITO)
         */
        public function Anular($id_comprobante){
        
        	parent::Anular($id_comprobante);
        }
        
        
        /**
         * @secured(MODIFICACION_FACTURA_CREDITO)
         */
        public function generaAsiento($id_comprobante,&$message){
        	
        	parent::generaAsiento($id_comprobante, $message);
        	
        }
        
        
        
        public function getMovimientoFondosFormaPago(){
        	
        	parent::getMovimientoFondosFormaPago();
        }
   
        /**
         * @secured(CONSULTA_FACTURA_CREDITO)
         */
        
        public function chequeoValorNoRepetido($nombre_campo,$valor,$id_comprobante,$id_persona,$id_tipo_comprobante=""){
        	
        	if($id_tipo_comprobante == "")
        		$id_tipos_comprobante = $this->{$this->model}->getTiposComprobanteController(EnumController::FacturasCredito);
        	
        	
        	parent::chequeoValorNoRepetido($nombre_campo,$valor,$id_comprobante,$id_persona,$id_tipos_comprobante);
        }
        


    }
?>