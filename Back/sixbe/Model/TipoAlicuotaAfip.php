<?php
App::uses('AppModel', 'Model');

class TipoAlicuotaAfip extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'iva';
	
	public $primaryKey = 'tipo_alicuota_afip';
	

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed


	public function getTasaIva($tipo_alicuota_afip){
		
		
		$iva = $this->find('first', array(
				'conditions' => array('TipoAlicuotaAfip.tipo_alicuota_afip' => $tipo_alicuota_afip),
				'contain' =>false
		));
		
		return $iva["TipoAlicuotaAfip"]["d_iva"];
	}

}
