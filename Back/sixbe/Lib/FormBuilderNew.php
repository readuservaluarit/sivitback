<?php
    /**
    * Form Builder
    * Constructor único de formularios.
	* Implementa las funcionalidades abstraídas para la generación de abms.
    */

    App::import('Lib', 'PagingHelper');
    App::uses('Sanitize', 'Utility');
    App::import('Lib', 'SecurityManager}');

    class FormBuilderNew
    {
        private $id;
        private $controller;
        private $titulo;
        private $subtitulo;
        private $tituloForm;
        private $modelName;
        private $controllerName;
        private $headers;
        private $fields;
        private $data;
        private $formFields;
        private $formTabs;
        private $form;
        private $formDivWidth;
        private $formCustomScripts;
        private $commonScripts;
        private $formScriptReferences;
        private $formJsVariables;
        private $listadoScriptReferences;
        private $formValidationFunction;
        private $displayAcceptButton;
        private $formAction;
        private $executeFormThemeScripts;
        private $formAcceptButtonText;
        private $formReadyScripts;
        private $hasTabs = false; 
        private $formCancelButtonRefreshPage = false;
        private $useSubformDatatables = true;
        private $readOnly = false;
        
    
        

        //Listado
        private $showEditButton;
        private $showFooter=true;
        private $showHeaderListado=true;
        private $showViewButton;
        private $addButtonParameters;
        private $xlsButtonParameters;
        private $pdfButtonParameters;
        private $editButtonParameters;
        private $formCancelButtonParameters;
        private $formAcceptButtonParameters;
        private $editButtonTooltip;
        private $viewButtonTooltip;
        private $showDeleteButton;
        private $deleteButtonTooltip;
        private $showNewButton;
        private $showXlsButton;
        private $showPdfButton;
        private $showBackButton = false;
        private $showImpactarAfipButton = false;
        private $showCancelFormButton = true;
        private $newButtonTooltip;
        private $newButtonText;
        private $xlsButtonText = "Xls";
        private $pdfButtonText = "Pdf";
        private $showFormAcceptButton;
        private $showActionColumn;
        private $filterFindButton;
        private $showPrintButton;
        private $filtroUnicoLabel = 'Nombre';
        private $filterValidationFunction;
        private $actionColumnTitle = 'Acciones';
        private $editButtonAction = 'edit';
        private $indexAction = 'index';
        private $buscarButtonParameters = array();
        private $backButtonParameters = array();
        private $listadoFooterHtml = "";
        private $listadoSubtitleHtml = "";
        private $editIconClass="icon-edit";
        private $showBackDatatableButton;
        private $showCopyDatatableButton;
        private $showExcelDatatableButton;
        

        private $filterFields;


        function FormBuilderNew(){


            $this->headers = array();
            $this->fields = array();
            $this->formFields = array();
            $this->formCustomScripts = array();
            $this->commonScripts = array();
            $this->formTabs = array();
            $this->formScriptReferences = array();
            $this->formJsVariables = array();
            $this->listadoScriptReferences = array();
            $this->newButtonTooltip = 'Nuevo';
            $this->newButtonText = 'Nuevo';
            $this->editButtonTooltip = 'Editar';
            $this->viewButtonTooltip = 'Ver';
            $this->deleteButtonTooltip = 'Eliminar';
            $this->showNewButton = true;
            $this->showXlsButton = false;
            $this->showPdfButton = false;
            $this->showEditButton = true;
            $this->showViewButton = false;
            $this->showDeleteButton = true;
            $this->showFormAcceptButton = true;
            $this->showActionColumn = true;
            $this->filterFields = array();
            $this->showPrintButton = false;
            $this->formAction = null;
            $this->editButtonParameters = array();
            $this->formCancelButtonParameters = array();
            $this->formAcceptButtonParameters = array();
            $this->executeFormThemeScripts = true;
            $this->formAcceptButtonText = 'Guardar';
            $this->formReadyScripts = array();
            //$this->addButtonParameters = array();

        }

        function setId($id) {
            $this->id = $id;
        }

        function setAddButtonParameters($parameters){
            $this->addButtonParameters = $parameters;
        }
        
        function setXlsButtonParameters($parameters){
            $this->xlsButtonParameters = $parameters;
        }
        
        function setPdfButtonParameters($parameters){
            $this->pdfButtonParameters = $parameters;
        }

        function setEditButtonParameters($parameters){
            $this->editButtonParameters = $parameters;
        }
        
        function setFormCancelButtonParameters($parameters){
            $this->formCancelButtonParameters = $parameters;
        }
        
        function setFormAcceptButtonParameters($parameters){
            $this->formAcceptButtonParameters = $parameters;
        }

        function setForm($form){
            $this->form = $form;
        }

        function setFormDivWidth($formDivWidth){
            $this->formDivWidth = $formDivWidth;
        }

        function setModelName($modelName){
            $this->modelName = $modelName;
        }

        function setControllerName($controllerName){
            $this->controllerName = $controllerName;
            
        }

        function setTitulo($titulo){
            $this->titulo = $titulo;
        }
        
        
        function getTitulo(){
        	return $this->titulo;
        }

        function setTituloForm($tituloForm){
            $this->tituloForm = $tituloForm;
        }

        function setController($controller){
            $this->controller = $controller;
            
            $this->setReadOnly($controller);
            
        }
        
        function setEditIconClass($class){
            
            $this->editIconClass = $class;
            
        }
        
        function setReadOnly($controller){
            /*
            $this->readOnly = SecurityManager::isReadOnlyRole($controller);
            
            if($this->readOnly){
                
                $this->editButtonTooltip = "Ver Detalle";
                $this->showNewButton = false;
                $this->showDeleteButton = false;
                $this->setEditIconClass('icon-search');
                
                
            }    
            */        
            
        }

        function showEditButton($show) {
            $this->showEditButton = $show;
        }
        
        
        function showBackDatatableButton($show) {
        	$this->showBackDatatableButton = $show;
        }
        
        
        function showCopyDatatableButton($show) {
        	$this->showCopyDatatableButton= $show;
        }
        
        
        function showExcelDatatableButton($show) {
        	$this->showExcelDatatableButton = $show;
        }
        
        
        
        function showFooter($show) {
        	$this->showFooter= $show;
        }
        
        function showHeaderListado($show) {
        	$this->showHeaderListado= $show;
        }
        
        function showViewButton($show) {
            $this->showViewButton = $show;
        }

        function showDeleteButton($show) {
            $this->showDeleteButton = $show;
        }

        function showNewButton($show) {
            $this->showNewButton = $show;
        }
        
        function showXlsButton($show) {
            $this->showXlsButton = $show;
        }
        
        function showPdfButton($show) {
            $this->showPdfButton = $show;
        }
        
        function showBackButton($show) {
            $this->showBackButton = $show;
        }
        function showImpactarAfipButton($show) {
            $this->showImpactarAfipButton = $show;
        }
         function showCancelFormButton($show) {
            $this->showCancelFormButton = $show;
        }

        function showFormAcceptButton($show){
            $this->showFormAcceptButton = $show;
        }

        function showActionColumn($show){
            $this->showActionColumn = $show;
        }

        function showPrintButton($show){
            $this->showPrintButton = $show;
        }

        function setEditButtonTooltip($tooltip) {
            $this->editButtonTooltip = $tooltip;
        }
        
        function setViewButtonTooltip($tooltip) {
            $this->viewButtonTooltip = $tooltip;
        }

        function setDeleteButtonTooltip($tooltip) {
            $this->deleteButtonTooltip = $tooltip;
        }

        function setNewButtonTooltip($tooltip) {
            $this->newButtonTooltip = $tooltip;
        }
        
        function setNewButtonText($text) {
            $this->newButtonText = $text;
        }
        
        function setXlsButtonText($text) {
            $this->xlsButtonText = $text;
        }
        
        function setPdfButtonText($text) {
            $this->pdfButtonText = $text;
        }
        
        function setFormAction($formAction){
            $this->formAction = $formAction;
        }
        
        function setFormAcceptButtonText($text){
            $this->formAcceptButtonText = $text;
        }

        function setFormValidationFunction($formValidationFunction){
            $this->formValidationFunction = $formValidationFunction;
        }
        
        function setActionColumnTitle($title){
            $this->actionColumnTitle = $title;
        }
        
        function setEditButtonAction($action){
            $this->editButtonAction = $action;
        }
        
        function setIndexAction($action){
            $this->indexAction = $action;
        }
        
        function setBuscarButtonParameters($parameters){
            $this->buscarButtonParameters = $parameters;
        }
        
        function setBackButtonParameters($parameters){
            $this->backButtonParameters = $parameters;
        }
        
        function setListadoFooterHtml($html){
            $this->listadoFooterHtml = $html;
        }
        
        function setListadoSubtitleHtml($html){
            $this->listadoSubtitleHtml = $html;
        }

        function addFormCustomScript($formCustomScript){
            array_push($this->formCustomScripts, $formCustomScript);
        }
        
        function addFormReadyScript($script){
            array_push($this->formReadyScripts, $script);
        }
        
        function addFormJsVariable($formJsVariable){
            array_push($this->formJsVariables, $formJsVariable);
        }

        function addCommonScript($commonScript){
            array_push($this->commonScripts, $commonScript);
        }

        function addFormScriptReference($formScriptReference){
            array_push($this->formScriptReferences, $formScriptReference);
        }

        function addListadoScriptReference($listadoScriptReference){
            array_push($this->listadoScriptReferences, $listadoScriptReference);
        }

        function addFormTab($id, $caption){
            array_push($this->formTabs, array('id' => $id, 'caption' => __($caption)));
        }

        function addFilterField($filterField){
            array_push($this->filterFields, $filterField);
        }
        
        function executeFormThemeScripts($execute){
            
            $this->executeFormThemeScripts = $execute;
            
        }
        
        function setFormCancelButtonRefreshPage($refresh){
            
            $this->formCancelButtonRefreshPage = $refresh;
            
        }
        
        function setFiltroUnicoLabel($label){
            $this->filtroUnicoLabel = $label;
        }
        
        function setFilterValidationFunction($filterValidationFunction){
            $this->filterValidationFunction = $filterValidationFunction;
        }
        
         function getshowImpactarAfipButton() {
            return $this->showImpactarAfipButton;
        }
        
        public function useSubformDatatables(){
            return $this->useSubformDatatables;
        }


        function setDataListado($controller, $titulo, $subtitulo, $modelName, $controllerName, $data){

            $this->controller = $controller;
            $this->titulo = $titulo;
            $this->subtitulo = $subtitulo;
            $this->modelName = $modelName;
            $this->controllerName = $controllerName;
            $this->data = $data;
            $this->headers = array();
            $this->fields = array();
            
            //Setea el modo ReadOnly si corresponde
            $this->setReadOnly($controller);
            
        }

        public function paginatorOptions(){

            $this->controller->Paginator->options(array(
                    'paginado' =>'1',
                    'update' => '#main-content',
                    'evalScripts' => true,
                    'before' => $this->controller->Js->get('#zk_proc')->effect('show', array('buffer' => false)),
                    'complete' => $this->controller->Js->get('#zk_proc')->effect('hide', array('buffer' => false)),
                ));

        }

        public function headListado(){

        	
        	if($this->showHeaderListado){
           /* echo
          
                "<legend><h4>".$this->titulo."</h4></legend>";*/

            
        	}

        }   

        public function filtroUnico(){
            
            $this->addFilterBeginRow();
            
            $this->addFilterInput('bus_unico', $this->filtroUnicoLabel, array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'style' => 'width:506px;'));
            
            $this->addFilterEndRow();
            
            /*
            echo  "<div class='dataTables_filter' id='datatable_filter' style='padding-top:10px;'>";

            echo $this->controller->Form->create($this->modelName);

            echo "  <label class='col-lg-2 control-label'>Buscar: ";
            echo '<div class="input-append" style="padding-left:10px;">';
            echo $this->controller->Form->input('bus_unico', array('class' => 'form-horizontal', 'label' => false, 'div' => false, 'class' => 'bus_unico'));
            echo '<span class="add-on"><i id="botonBuscar" class="icon-search">';     
            echo '</i></span></div>';
            $this->controller->Js->get('#botonBuscar')->event('click',
                $this->controller->Js->request(
                    array('controller' => $this->controllerName, 'action' => 'index'),
                    array('update' => '#main-content', 'async' => true,
                        'dataExpression' => true, 'method' => 'post',
                        'data' => $this->controller->Js->serializeForm(array('isPost' => true, 'inline' => true)),
                        'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                    )
                )                                   
            );
            echo "             </label>
            </div>";    
            */
            
            $this->filtroCustom();
        }

        public function filtroCustom(){

            echo  "<div class='' id='datatable_filter'>";

            echo $this->controller->Form->create($this->modelName, array('class' => 'form-inline', 'style'=>'width:90%;'));
            
            echo "";

            $this->processFormFields($this->filterFields); 

            $this->filtroCustomBotonBuscar();       

            echo "            
            </div>
            </div>";

        }

        public function filtroCustomBotonBuscar(){

            if (isset($this->filterFindButton))
                $button =  $this->filterFindButton;
            else
                $button = 'Buscar';

           // echo "<div class='span12' style='width:90%;' align='right'>";
            
            if($this->showBackButton){
                $options = array('div'=>false,'name' => 'botonVolver', 'id'=>'botonVolver', 'title' => 'Volver', 'class' => 'btn btn-primary');
                echo $this->controller->Form->Button('<i class="icon-arrow-left icon-white"></i>&nbsp;Volver', $options);

                echo "<span>&nbsp;<span>";
                
                $backParameters = array('controller' => $this->controllerName, 'action' => $this->indexAction);
            
                if(isset($this->backButtonParameters))
                       $backParameters = $this->backButtonParameters;
                
                $volverClick = $this->controller->Js->request(
                    $backParameters,
                    array('update' => '#main-content', 'async' => false,
                        'dataExpression' => true, 'method' => 'get',
                        'data' => $this->controller->Js->serializeForm(array('isPost' => true, 'inline' => true)),
                        'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                    )
                );
                
                
                $this->controller->Js->get('#botonVolver')->event('click', $volverClick);
            }
            
            
            echo "<span>&nbsp;<span>";
            
            $options = array('div'=>false,'name' => 'botonLimpiar', 'id'=>'botonLimpiar', 'title' => 'Limpiar', 'class' => 'btn btn-primary mb-2');
            echo $this->controller->Form->Button('<i class="icon-repeat icon-white"></i>&nbsp;Limpiar', $options);

            echo "<span>&nbsp;<span>";
            
            $options = array('div'=>false,'name' => 'botonBuscar', 'id'=>'botonBuscar', 'title' => 'Buscar', 'class' => 'btn btn-primary mb-2');
            echo $this->controller->Form->Button('<i class="icon-search icon-white"></i>&nbsp;'.$button, $options);

            //echo "</div>";                  
            
            $this->controller->Js->get('#botonLimpiar')->event('click', "clearInputs('".$this->modelName.Inflector::camelize($this->indexAction)."Form');");
            
            $buscarParameters = array('controller' => $this->controllerName, 'action' => $this->indexAction);
            
            $buscarParameters = array_merge($buscarParameters, $this->buscarButtonParameters);   
            
            $buscarClick = $this->controller->Js->request(
                    $buscarParameters,
                    array('update' => '#main-content', 'async' => false,
                        'dataExpression' => true, 'method' => 'post',
                        'data' => $this->controller->Js->serializeForm(array('isPost' => true, 'inline' => true)),
                        'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                    )
                );
                
            $validate = "";
            if(isset($this->filterValidationFunction) && $this->filterValidationFunction!="")
                $validate = "if(!".$this->filterValidationFunction.") return false; ";
            
               // $this->controller->Js->get('#botonBuscar')->event('click', $validate . $buscarClick);
           
    

            

        }

        public function addHeader($titulo, $keyOrder, $width){
            array_push($this->headers, array('titulo' => __($titulo), 'keyOrder' => $keyOrder, 'width' => $width));

        }

        public function addField($modelName, $fieldName){
            array_push($this->fields, array('modelName' => $modelName, 'fieldName' => $fieldName));

        }

        public function headersListado(){
            foreach($this->headers as $clave=>$valor){
                
                $visible = true;
                if(isset($valor['visible']) && !$valor['visible'])
                    $visible = false;
                

                if(trim($valor['keyOrder'])!="")
                    PagingHelper::SortHeader($this->controller, $valor['keyOrder'], __($valor['titulo']), $valor['width'], $visible,  array('model' => $this->modelName, 'escape' => false));
                else
                    PagingHelper::NonSortedHeader($this->controller, __($valor['titulo'], $visible));
            }


        }

        public function rows(){

            $i =0; 
            foreach($this->data as $data){ 

                $i++;
                $class = PagingHelper::getRowClass($i);

                echo "<tr>";

                foreach($this->fields as $clave=>$valor){



                    //echo "<td>".__($data[$valor['modelName']][$valor['fieldName']])."</td>";

                    eval("\$valorDato = \$data['". str_replace('.', "']['", $valor['modelName']) ."'][\$valor['fieldName']];");

                    echo "<td>".__($valorDato)."</td>";

                }

                if($this->showActionColumn){

                    echo "<td>";

                    $idVer = "btnVer" . $i;
                    $idEditar = "btnEditar" . $i;
                    $idEliminar = "btnEliminar" . $i;
                    $idImprimir = "btnImprimir" . $i;

                    $id =  $data[$this->modelName]['id'];

                    if($this->showViewButton){
                        
                        echo '<a href="#" id="'.$idVer.'"><span class="add-on"><i class="icon-eye-open">';
                        echo '</i></span></a>';

                       // echo $this->controller->Html->link($this->controller->Html->image('icons/packs/silk/16x16/table_edit.png', array('plugin' => false)), 
                       //     array('controller' => $this->controllerName, 'action' => 'view', $id), array('escape' => false, 'class' => '', 
                       //         'title' => $this->viewButtonTooltip, 'data-gravity' => 's', 'id' => $idVer));

                        $viewButtonParameters = array('controller' => $this->controllerName, 'action' => 'view', $id);
                        if(isset($this->viewButtonParameters)){
                            $viewButtonParameters = array_merge($viewButtonParameters, $this->viewButtonParameters);   
                        }

                        $this->controller->Js->get('#'.$idVer)->event('click',
                            $this->controller->Js->request(
                                $viewButtonParameters,
                                array('update' => '#main-content', 'async' => false,
                                    'dataExpression' => true, 'method' => 'get',
                                    'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                                )
                            )
                        );
                    }

                    echo "&nbsp;";
                    
                    if($this->showEditButton){

                        echo '<a href="#" id="'.$idEditar.'" title="'.$this->editButtonTooltip.'"><span class="add-on"><i class="'.$this->editIconClass.'">';
                        echo '</i></span></a>';
                        
                        //echo $this->controller->Html->link($this->controller->Html->image('icons/packs/silk/16x16/table_edit.png', array('plugin' => false)), 
                        //    array('controller' => $this->controllerName, 'action' => 'edit', $id), array('escape' => false, 'class' => '', 
                        //        'title' => $this->editButtonTooltip, 'data-gravity' => 's', 'id' => $idEditar));

                        $editButtonParameters = array('controller' => $this->controllerName, 'action' => $this->editButtonAction, $id);
                        if(isset($this->editButtonParameters)){
                            $editButtonParameters = array_merge($editButtonParameters, $this->editButtonParameters);   
                        }

                        $this->controller->Js->get('#'.$idEditar)->event('click',
                            $this->controller->Js->request(
                                $editButtonParameters,
                                array('update' => '#main-content', 'async' => true,
                                    'dataExpression' => true, 'method' => 'get',
                                    'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                                )
                            )
                        );
                    }

                    echo "&nbsp;";

                    if($this->showDeleteButton){

                        echo '<a href="#" id="'.$idEliminar.'" title="'.$this->deleteButtonTooltip.'"><span class="add-on"><i class="icon-remove-sign">';
                        echo '</i></span></a>';
                        
                        //echo $this->controller->Html->link($this->controller->Html->image('icons/packs/silk/16x16/delete.png', array('plugin' => false)) , 
                        //    array('controller' => $this->controllerName, 'action' => 'delete'), 
                        //    array('escape' => false, 'title' => $this->deleteButtonTooltip, 'id' => $idEliminar));


                        $deleteRequest = $this->controller->Js->request(
                                array('controller' => $this->controllerName, 'action' => 'delete', $id),
                                array('update' => '#main-content', 'async' => false,
                                    'dataExpression' => true, 'method' => 'post',
                                    'data' => $this->controller->Js->serializeForm(array('isPost' => true, 'inline' => true)),
                                    'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                                )
                            );
                        

                        $this->controller->Js->get('#'.$idEliminar)->event('click','if(confirm("Confirma que desea eliminar el registro?")) ' . $deleteRequest);
                    }

                    if($this->showPrintButton){

                        echo "&nbsp;";

                        echo '<a href="#" id="'.$idImprimir.'" title="Imprimir"><span class="add-on"><i class="icon-print">';
                        echo '</i></span></a>';
                        
                        //echo $this->controller->Html->link($this->controller->Html->image('icons/packs/silk/16x16/printer.png', array('plugin' => false)) , 
                        //    array('controller' => $this->controllerName, 'action' => 'printPdf'), 
                        //    array('escape' => false, 'title' => 'Imprimir', 'id' => $idImprimir));

                        $this->controller->Js->get('#'.$idImprimir)->event('click','location.href= "'.Router::url(array('controller' => $this->controllerName, 'action' => 'printPdf',$id)).'";');

                    }


                    echo "</td>";

                }
                echo "</tr>";



            }


        } 


        
        
        public function tableListado(){
        	
        	
        	


            
            //Boton Nuevo
            if($this->showNewButton){
                
                echo '<a href="#" id="btnNuevo"><span class="add-on"><i id="btnNuevo" class="icon-plus-sign">';
                echo '</i></span>&nbsp;' . $this->newButtonText . '</a>&nbsp;&nbsp;';
                
                if(isset($this->addButtonParameters))
                    $addButtonParameters = $this->addButtonParameters;
                else
                    $addButtonParameters = array('controller' => $this->controllerName, 'action' => 'add');

                 
                    
                $this->controller->Js->get('#btnNuevo')->event('click',
                    $this->controller->Js->request(
                        $addButtonParameters,
                        array('update' => '#main-content', 'async' => false,
                            'dataExpression' => true, 'method' => 'get',
                            'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                        )
                    )
                );
            }
            
            //Boton Xls
            if($this->showXlsButton){
                
                echo '<a href="#" id="btnXls"><span class="add-on"><i id="btnXls" class="icon-file">';
                echo '</i></span>&nbsp;' . $this->xlsButtonText . '</a>&nbsp;&nbsp;';
                
                if(isset($this->xlsButtonParameters))
                    $xlsButtonParameters = $this->xlsButtonParameters;
                else
                    $xlsButtonParameters = array('controller' => $this->controllerName, 'action' => 'excelExport');

                 
                $this->controller->Js->get('#btnXls')->event('click','location.href= "'.Router::url($xlsButtonParameters).'";'); 
                
                /*    
                $this->controller->Js->get('#btnXls')->event('click',
                    $this->controller->Js->request(
                        $xlsButtonParameters,
                        array('update' => '#main-content', 'async' => false,
                            'dataExpression' => true, 'method' => 'get',
                            'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                        )
                    )
                );
                */
            }
            
            //Boton Pdf
            if($this->showXlsButton){
                
                echo '<a href="#" id="btnPdf"><span class="add-on"><i id="btnPdf" class="icon-file">';
                echo '</i></span>&nbsp;' . $this->pdfButtonText . '</a>';
                
                if(isset($this->pdfButtonParameters))
                    $pdfButtonParameters = $this->pdfButtonParameters;
                else
                    $pdfButtonParameters = array('controller' => $this->controllerName, 'action' => 'pdfExport');

                $this->controller->Js->get('#btnPdf')->event('click','location.href= "'.Router::url($pdfButtonParameters).'";'); 
                
                /*    
                $this->controller->Js->get('#btnPdf')->event('click',
                    $this->controller->Js->request(
                        $pdfButtonParameters,
                        array('update' => '#main-content', 'async' => false,
                            'dataExpression' => true, 'method' => 'get',
                            'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                        )
                    )
                );
                */
            }
            

            
   
            
            echo "<table id='myTable' class='table table-striped table-light'>";
            
            
           if($this->showHeaderListado){
           	
           	
           
            echo "<thead class='thead-dark'>
            <tr scope='col'>";

            $this->headersListado();

            if($this->showActionColumn)
                echo "          <th style='text-align:center;'>{$this->actionColumnTitle}</th> ";

            echo "      
            </tr>
            </thead>";
           }
           
           
           echo "<tbody>";
            $this->rows();

            echo "      </tbody>
            </table>";
            

        }

        function footerListado(){
        	
        	
        	
        	if($this->showFooter){
        		
        		?>
			
			<div class='block-actions' style='text-align:center;'>
		        <div class='picker_info' id='picker_info'>
		            <?php echo$this->controller->Paginator->counter('Registros {:start} a {:end} de {:count}') ?>
		        </div>
        
     
				<nav aria-label="Page navigation example">
				  <ul class="pagination">
				    <li class="page-item"><?php echo$this->controller->Paginator->first('<<') ?></li>
				    <li class="page-item"><?php echo$this->controller->Paginator->prev('<', null, null, array('class' => 'page-link')) ?></li>
				     <?php echo$this->controller->Paginator->numbers(array('tag'=>'li','currentTag' => 'a', 'class' =>'page-item', 'separator' => '', 'currentClass' => 'active')) ?>
				     
				    <li class="page-item"><?php echo$this->controller->Paginator->next('>', null, null, array('class' => 'page-link')) ?></li>
				    <li class="page-item"><?php echo$this->controller->Paginator->last('>>') ?></li>
				  </ul>
				</nav>



    		</div>
			
			
		<?php echo "
                    </div>
                    </div>
                    </div>
                    </div>"; 
                    
            

   
		}
        	
        	
		/*if($this->showFooter){
            echo " <nav aria-label='Page navigation example'>" .
                    $this->controller->Paginator->counter('Registros {:start} a {:end} de {:count}') .
                    "
                   
                        <ul  class='pagination'>
                            <li class='page-item'>" .
                            $this->controller->Paginator->first('<<', array( 'tag' => 'li', 'escape' => false,'class' => 'page-link'), null, array('class' => 'page-link')) .
                            "</li>
                            <li class='page-item'>" .
                            $this->controller->Paginator->prev('<', array( 'tag' => 'li', 'escape' => false,'class' => 'page-link'), null, array('class' => 'page-link')) .
                            "</li>".
                  
                            $this->controller->Paginator->numbers(array('separator' => '', 'tag' => 'li','class' => 'page-link','currentClass' => 'active', 'currentTag' => 'a' , 'escape' => false))."".
                           
                           
                            $this->controller->Paginator->next('>', array( 'tag' => 'li', 'escape' => false,'class' => 'page-link'), null, array('class' => 'page-link' ,'tag' => 'li', 'escape' => false))."".
                           
                            $this->controller->Paginator->last('>>', array( 'tag' => 'li', 'escape' => false,'class' => 'page-link'), null, array('class' => 'page-link')) .
                            "</ul>
                 </nav>
                    </div>
                    </div>
                    </div>
                    </div>"; 
                    
            

   
		}*/
		
		echo $this->controller->Js->writeBuffer(); 
        }

        private function listadoReadyScripts(){

            echo    
            "<script type='text/javascript'>
            $().ready(function() {

            $('.bus_unico:first').focus();

            $('.datepicker').datepicker({
  dateFormat: 'dd/mm/yy',
dayNames: [ 'Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado' ]
});
           // $('.datetimepicker').datetimepicker();
            
            //Seleccionado
             $('input:text:visible:first').focus(function(){
                    this.select();
                });
                
            // Fix para Google Chrome
                $('input:text:visible:first').mouseup(function(e){
                    e.preventDefault();
                });
                
            //Foco
            $('input:text:visible:first').focus();
            
            //Para que en el enter dispare la busqueda
            $('input').keypress(function(event) {
                if (event.which == 13) {
                    event.preventDefault();
                    //$('form').submit();
                    $('#botonBuscar').click();
                }
            });
            ".           
            $this->numericInputScripts()
            ."
            });    

            </script>";

        }

        public function printCommonScripts(){

        	
        	
        	if(isset($this->commonScripts) && sizeof($this->commonScripts) > 0){
	            echo "<script type='text/javascript'>";
	
	
	                foreach($this->commonScripts as $commonScript)
	                {
	                    echo $commonScript;
	                } 
                
                echo "</script>";
            }
        
     

        }

        public function getListado(){
	
        	?>
        	<div class="container-fluid">
              <div class="row">
                <div class="statistics col-md-12">
        	
        	<?php 
        	
            $this->paginatorOptions();
            $this->headListado();
            //$this->filtroUnico();
            
            $this->getFiltro();
            
            ?>
            
           
               
            <?php 
            
            $this->tableListado();
            echo $this->listadoFooterHtml;
            $this->footerListado();
            $this->printCommonScripts();
           $this->ListadoReadyScripts();
            //flash javascripts
            echo $this->controller->Session->flash();
            
            
            ?>
        
        		</div>
        	</div>
 		</div>	           
            <?php 
        
        }
        
        public function getChart(){
        	
        	
        	
        	//$this->paginatorOptions();
        	$this->headListado();
        	//$this->filtroUnico();
        	//$this->getFiltro();
        	$this->printChart();
        	echo $this->listadoFooterHtml;
        	//$this->footerListado();
        	//$this->printCommonScripts();
        	//$this->ListadoReadyScripts();
        	//flash javascripts
        	echo $this->controller->Session->flash();
        	
        
        }
        
        public function printChart(){
        	
        	
        	echo '<div class="row-fluid">
        			
        			
        			
        			
  <div class="control-group span10">
        			
        			
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Bar graph <small>Sessions</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <canvas id="mybarChart"></canvas>
                  </div>
                </div>
        			
            </div>
        			
          </div>
        			
         ';
        	
        	
        	echo " <!-- Custom Theme Scripts -->
        	<script>
        	if ($('#mybarChart').length ){
        			
        		var ctx = document.getElementById(\"mybarChart\");
        		var mybarChart = new Chart(ctx, {
        			type: 'bar',
        			data: {
        				labels: [\"January", "February", "March", "April", "May", "June", "July\"],
        				datasets: [{
        					label: '# of Votes',
        					backgroundColor: \"#26B99A\",
        					data: [51, 30, 40, 28, 92, 50, 45]
        				}, {
        					label: '# of Votes',
        					backgroundColor: \"#03586A\",
        					data: [41, 56, 25, 48, 72, 34, 12]
        					}]
        			},
        						
        			options: {
        				scales: {
        					yAxes: [{
        						ticks: {
        							beginAtZero: true
        						}
        					}]
        				}
        			}
        		});
        						
        	}
        						
        	</script>";
        		
        
        }

        private function getFiltro(){

            if(!sizeof($this->filterFields) == 0)
                $this->filtroCustom();

        }

        public function addFormField($field, $label, $class){

            array_push($this->formFields, array('field' => $field, 'label' => $label, 'class' => $class));

        }


        private function getFormDivWidthStyle(){

            if(isset($this->formDivWidth) && $this->formDivWidth != null)
                return "style='width:". $this->formDivWidth .";'";

            return "";

        }

        public function getForm($editing=false){

            echo"  
            <div class='container_12'>            
            <div class='grid_12'>
            <h1>".__($this->titulo)."</h1>
            <div id='validationMsg_".$this->id."' class='alert error' style='display:none;'>
            </div>
            <p></p>
            </div>

            <div class='grid_6' ".$this->getFormDivWidthStyle().">
            <div class='block-border'>
            <div class='block-header'>
            <h1>".__($this->tituloForm)."</h1><span></span>
            </div>";

            echo $this->form;

            if($editing)
                echo $this->controller->Form->input('id', array('type' => 'hidden'));


            foreach($this->formFields as $clave=>$valor){          

                echo    "<div class='".$valor['class']."'>
                <p>
                <label for='textfield'>".$valor['label']."</label>".
                $valor['field'] .
                "</p>
                </div>";

            }
            
            if(isset($this->formCancelButtonParameters) && $this->formCancelButtonParameters != null)
                    $cancelButtonparameters = $this->formCancelButtonParameters;
                else
                    $cancelButtonparameters = array('controller' => $this->controllerName, 'action' => 'index');

            echo
            "<div class='clear'></div>
            <div class='block-actions'>
            <ul class='actions-left'>
            <li>
            " . $this->controller->Html->link('Cancelar' , $cancelButtonparameters, array('escape' => false, 'class' => 'button red', 'id' => 'btnCancelar'.$this->id)) .
            "</li>
            </ul>
            <ul class='actions-right'>
            <li>" .
            $this->controller->Form->end(array('label' => $this->formAcceptButtonText, 'class' => 'button', 'div' => false, 'id' => 'btnGuardar'.$this->id)) .
            "</li>
            </ul>
            </div>
            </div>
            </div>
            </div>

            <div class='clear height-fix'></div>";


            $this->controller->Js->get('#btnCancelar'.$this->id)->event('click',
                $this->controller->Js->request(
                    $cancelButtonparameters,
                    array('update' => '#main-content', 'async' => true,
                        'dataExpression' => true, 'method' => 'get',
                        'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                    )
                )
            );

            if($editing)
                $action = 'edit';
            else
                $action = 'add';
                
            if($this->formAction != null)
                $action = $this->formAction;

            if($this->showFormAcceptButton){

                $ajaxSubmit = $this->controller->Js->request(array('controller' => $this->controllerName, 'action' => $action),
                    array('update' => '#main-content', 'async' => false,
                        'dataExpression' => true, 'method' => 'post',
                        'data' => $this->controller->Js->serializeForm(array('isPost' => true, 'inline' => true)),
                        'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                    )
                );

                $validate = "";
                if(isset($this->formValidationFunction) && $this->formValidationFunction!="")
                    $validate = "if(!".$this->formValidationFunction.") return false; ";

                $this->controller->Js->get('#btnGuardar'.$this->id)->event('click', $validate . $ajaxSubmit);

            }

            echo $this->controller->Js->writeBuffer(); 

        }



        private $form2;
        function setForm2($label, $options){
            $this->form2 = array("label" => $label, "options" => $options);
        }

        private $formFields2 = array();
        private $formButtons2 = array();
        public function addFormInput($id, $label, $fieldOptions, $inputOptions) {
            array_push($this->formFields2, array("id" => $id, "label" => $label, "type" => 'input', "fieldOptions" => $fieldOptions, "inputOptions" => $inputOptions));
        }
        public function addFormPicker(
            $pickerClassName, 
            $selectionList,  
            $hiddenId, 
            $hiddenOptions, $id, $label, 
            $fieldOptions, 
            $inputOptions, 
            $callbacks=array(), 
            $filterQueryFields=array()) {
            array_push($this->formFields2, array("pickerClassName"=>$pickerClassName, "selectionList" => $selectionList, "hiddenId" => $hiddenId, "hiddenOptions" => $hiddenOptions, "id" => $id, "label" => $label, "linkId" => "lnk".$id, "type" => 'picker', "fieldOptions" => $fieldOptions, "inputOptions" => $inputOptions, "callbacks" => $callbacks, "filterQueryFields" => $filterQueryFields));    
        }
        public function addFormRadio($id, $label, $fieldOptions, $inputOptions, $data) {
            array_push($this->formFields2, array("id" => $id, "label" => $label, "type" => 'radio', "fieldOptions" => $fieldOptions, "inputOptions" => $inputOptions, "data" => $data));
        }
        public function addFormCheckbox($id, $label, $fieldOptions, $inputOptions) {
            array_push($this->formFields2, array("id" => $id, "label" => $label, "type" => 'check', "fieldOptions" => $fieldOptions, "inputOptions" => $inputOptions));
        }
        public function addFormSeparator($id, $label, $fieldOptions) {
            array_push($this->formFields2, array("id" => $id, "label" => $label, "type" => 'separator', "fieldOptions" => $fieldOptions));
        }
        public function addFormHidden($id, $inputOptions = array()) {
            array_push($this->formFields2, array("id" => $id, "type" => 'hidden', "inputOptions" => $inputOptions));
        }
        public function addFormButton($id, $title, $fieldOptions = array(), $inputOptions = array()) {
            array_push($this->formFields2, array("id" => $id, "title" => $title, "type" => 'button', "fieldOptions" => $fieldOptions, "inputOptions" => $inputOptions));
        }
        public function addFormLabel($id, $text, $fieldOptions = array()) {
            array_push($this->formFields2, array("id" => $id, "type" => 'label', 'text' => $text, "fieldOptions" => $fieldOptions));
        }
        public function addFormLink($title, $url = null, $options = array(), $confirmMessage = false) {
            array_push($this->formFields2, array("title" => $title, "type" => 'link', 'url' => $url, "options" => $options, "confirmMessage" => $confirmMessage));
        }
        public function addFormSubform($id, $label, $model, $options, $listado, $subform, $datos = array()) {
            array_push($this->formFields2, array("id" => $id, "label" => $label, "type" => 'subform', "model" => $model, "options" => $options, "datos" => $datos, "listado" => $listado, "subform" => $subform));
        }
        public function addFormGeoButton($id, $class, $departamento, $localidad, $calle, $numero, $idX, $idY){
            array_push($this->formFields2, array('type' => 'geoButton', 'id' => $id, 'class' => $class,
                    'departamento' => $departamento, 'localidad' => $localidad,
                    'calle' => $calle, 'numero' => $numero,
                    'idX' => $idX, 'idY' => $idY
                )
            );
        }
        public function addFormQuestionsNew($text, $options, $preguntas) {
            array_push($this->formFields2, array("text" => $text, "type" => 'questions', "options" => $options, "preguntas" => $preguntas));
        }
        public function addFormQuestions($id, $text, $options, $preguntas) {
            array_push($this->formFields2, array("id" => $id, "text" => $text, "type" => 'questions2', "options" => $options, "preguntas" => $preguntas));
        }
        public function addFormHtml($html){
            array_push($this->formFields2, array("type" => 'html', 'html' => $html));
        }
        public function addFormLineSeparator(){
            $this->addFormHtml('<div class="clear"></div>');
        }
        public function addFormFooterSeparator(){
            $this->addFormHtml(' <div class="_100">&nbsp;</div>');
        }
        public function addFormBeginTab($id){
            
            if(!$this->hasTabs)
                $class = "tab-pane active in";
            else
                $class = "tab-pane fade"; 
            
            $this->hasTabs = true;
            
            $this->addFormHtml(' <div class="'.$class.'" id="'.$id.'" style="padding-bottom:50px;"> ');
        }
        
        public function addFormEndTab(){
            $this->addFormHtml(' </div> ');
        }
        
        
        public function addFormBeginFieldset($id, $caption=''){
            $this->addFormHtml(' <fieldset id="'.$id.'"> ');
            if(trim($caption) != '')
                $this->addFormHtml('<legend id="'.$id.'-legend">'.__($caption).'</legend> ');
        }
        public function addFormEndFieldset(){
            $this->addFormHtml(' </fieldset> ');
        }
        
        public function addFormBeginRow(){
            $this->addFormHtml('<div>');
        }
        
        public function addFormEndRow(){
            $this->addFormHtml('</div>');
        }
        
        public function addButton($options) {
            array_push($this->formButtons2, $options);
        }

        /*-----------------------------------------------------*/
        /*---Metodos para agregar filtros al listado----------*/

        public function addFilterInput($id, $label, $fieldOptions, $inputOptions) {
            array_push($this->filterFields, array("id" => $id, "label" => $label, "type" => 'input', "fieldOptions" => $fieldOptions, "inputOptions" => $inputOptions));
        }
        public function addFilterPicker($pickerClassName, $selectionList,  $hiddenId, $hiddenOptions, $id, $label, $fieldOptions, $inputOptions, $callbacks=array(), $filterQueryFields=array()) {
            array_push($this->filterFields, array("pickerClassName"=>$pickerClassName, "selectionList" => $selectionList, "hiddenId" => $hiddenId, "hiddenOptions" => $hiddenOptions, "id" => $id, "label" => $label, "linkId" => "lnk".$id, "type" => 'picker', "fieldOptions" => $fieldOptions, "inputOptions" => $inputOptions, "callbacks" => $callbacks, "filterQueryFields" => $filterQueryFields));    
        }
        public function addFilterRadio($id, $label, $fieldOptions, $inputOptions, $data) {
            array_push($this->filterFields, array("id" => $id, "label" => $label, "type" => 'radio', "fieldOptions" => $fieldOptions, "inputOptions" => $inputOptions, "data" => $data));
        }
        public function addFilterSeparator($id, $label, $fieldOptions) {
            array_push($this->filterFields, array("id" => $id, "label" => $label, "type" => 'separator', "fieldOptions" => $fieldOptions));
        }
        public function addFilterHidden($id, $inputOptions = array()) {
            array_push($this->filterFields, array("id" => $id, "type" => 'hidden', "inputOptions" => $inputOptions));
        }
        public function addFilterButton($id, $title, $fieldOptions = array(), $inputOptions = array()) {
            array_push($this->filterFields, array("id" => $id, "title" => $title, "type" => 'button', "fieldOptions" => $fieldOptions, "inputOptions" => $inputOptions));
        }
        public function addFilterLabel($id, $text, $fieldOptions = array()) {
            array_push($this->filterFields, array("id" => $id, "type" => 'label', 'text' => $text, "fieldOptions" => $fieldOptions));
        }
        public function addFilterLink($title, $url = null, $options = array(), $confirmMessage = false) {
            array_push($this->filterFields, array("title" => $title, "type" => 'link', 'url' => $url, "options" => $options, "confirmMessage" => $confirmMessage));
        }
        public function addFilterHtml($html){
            array_push($this->filterFields, array("type" => 'html', 'html' => $html));
        }
        public function addFilterLineSeparator(){
            $this->addFilterHtml('<div class="clear"></div>');
        }
        public function addFilterFooterSeparator(){
            $this->addFilterHtml(' <div class="_100">&nbsp;</div>');
        }
        public function addFilterBeginTab($id){
            $this->addFilterHtml(' <div class="tab-content" id="'.$id.'"> ');
        }
        public function addFilterEndTab(){
            $this->addFilterHtml(' </div> ');
        }
        public function addFilterBeginFieldset($id, $caption){
            $this->addFilterHtml(' <fieldset id="'.$id.'"> <legend>'.__($caption).'</legend> ');
        }
        public function addFilterEndFieldset(){
            $this->addFilterHtml(' </fieldset> ');
        }
        
        public function addFilterBeginRow(){
            $this->addFilterHtml('<div class="row-fluid">');
        }
        
        public function addFilterEndRow(){
            $this->addFilterHtml('</div>');
        }


        /*---Fin metodos filtro listado-------------*/
        /*-----------------------------------------*/

        private $formObservers2 = array();
        public function addFormObserver($observe, $update, $url) {
            array_push($this->formObservers2, array("observe" => $observe, "update" => $update, "url" => $url));
        }

        private function setOffset($options){
            
            if(!isset($options["class"]))
                return $options;
                
            if(strpos($options["class"], "span9"))
                $options["class"] .=" offset1";
            elseif (strpos($options["class"], "span6"))
                $options["class"] .=" offset2";
            
            return $options;
        }
        
        public function getForm2($mode, $id = null){
        	
        	echo "<div class='container' style='margin-top:50px'>";
        	echo "<div class='row justify-content-center align-items-center'>";
        	echo "<div class='col-md-12' style='background-color:white;max-width:600px'>";

            $this->mode = $mode;
            
            $this->printFormJsVariables();
            
            $this->printFormScriptReferences();

            //Si tiene un class spanx seteado, le pongo el offset que corresponde para centrarlo
            if(isset($this->form2["options"]))
                $this->form2["options"] = $this->setOffset($this->form2["options"]);
            
                
                
             
              
               
            echo $this->controller->Form->create($this->form2["label"], $this->form2["options"]);
            echo "<div  id='".$this->titulo."-title-link' class='card-header'><h5>".$this->tituloForm."</h5></div>";
            
            if($this->formTabs!=null){
                
                $i = 0;
                echo "<ul class='nav nav-tabs'>";
                foreach($this->formTabs as $tab){
                    $i++;
                    if($i==1)
                        $class = "nav-link active";
                    else
                        $class = "nav-link"; 
                        
                    echo "<li class='nav-item'  id='".$tab['id']."-link'><a class='{$class}' href='#" . $tab['id'] ."' data-toggle='tab'>". $tab['caption'] ."</a></li>"; 

                }
                echo "</ul>";
            }  
            
            echo "<div id='validationMsg_".$this->id."' style='display:none;'></div>";
            if($mode == 'M')
                echo $this->controller->Form->input('id', array('type' => 'hidden'));


            if($this->formTabs!=null)
                echo "<div id='myTabContent' class='tab-content'>";
                
            $this->processFormFields($this->formFields2);
            
            if($this->formTabs!=null)
                echo "</div>";

            echo "
<div class='row'>
            <div class='col-md-12'>";
            
            
            if($this->showBackButton){
            echo "
            <div style='width:33%;float:left' class='centered'>
            " . 
            $this->controller->Form->Button('<i class="icon-arrow-left icon-white"></i>&nbsp;Volver', array('type' => 'button', 'class' => 'btn btn-primary pull-left', 'div' => false, 'id' => 'btnCancelar'.$this->id)) .
            "</div>";
            
            }
           
            
            echo "<div style='width:33%;float:left' class='centered' align='center'>";
             
             if($this->showImpactarAfipButton){//muestro boton AFIP
             
             
               
              echo $this->controller->Form->Button('<i class="icon-arrow-right icon-white"></i>&nbsp; Guardar y Validar Electronicamente', array('type' => 'button', 'class' => 'btn btn-primary pull-left', 'div' => false, 'id' => 'btnValidar'.$id)); 
              
               $ajaxSubmit = $this->controller->Js->request(array('controller' => $this->controllerName, 'action' => 'edit'),
                    array('update' => '#main-content', 'async' => false,
                        'dataExpression' => true, 'method' => 'post',
                        'data' => $this->controller->Js->serializeForm(array('isPost' => true, 'inline' => true)),
                        'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                    )
                );

                $validate = "";
                if(isset($this->formValidationFunction) && $this->formValidationFunction!="")
                    $validate = "if(!".$this->formValidationFunction.") return false; ";

                $this->controller->Js->get('#btnValidar'.$id)->event('click', $validate . $ajaxSubmit);
             
             /*
                 echo $this->controller->Form->Button('<i class="icon-arrow-right icon-white"></i>&nbsp;Validar Electronicamente', array('type' => 'button', 'class' => 'btn btn-primary pull-left', 'div' => false, 'id' => 'btnValidar'.$this->id));
             
             $params = array('controller' => $this->controllerName, 'action' => "impactarAfip", $id);

                        $this->controller->Js->get('#btnValidar'.$this->id)->event('click',
                            $this->controller->Js->request(
                                $params,
                                array('update' => '#main-content', 'async' => false,
                                    'dataExpression' => true, 'method' => 'get',
                                    'before' => 'ValidarFacturaAntesImpactar();showProcessing(true);', 'complete' => 'showProcessing(false);'
                                )
                            )
                        );
             */
             }
            if(($mode == "A" || $mode == "M") && !$this->readOnly){
                
                if($this->showCancelFormButton)
                     echo $this->controller->Form->Button('<i class=" icon-repeat icon-white"></i>&nbsp;Cancelar', array('type' => 'button', 'class' => 'btn btn-primary pull-center', 'div' => false, 'id' => 'btnLimpiar'.$this->id));
                     

                if($this->formCancelButtonRefreshPage){
                    //Directamente recarga la pagina  (para casos en donde el reset no alcanza)
                    if($mode == 'A')
                        $action = 'add';
                    else
                        $action = 'edit';
                        
                     $params = array('controller' => $this->controllerName, 'action' => $action, $id);

                        $this->controller->Js->get('#btnLimpiar'.$this->id)->event('click',
                            $this->controller->Js->request(
                                $params,
                                array('update' => '#main-content', 'async' => false,
                                    'dataExpression' => true, 'method' => 'get',
                                    'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                                )
                            )
                        );
                }
                else
                    $this->controller->Js->get('#btnLimpiar'.$this->id)->event('click', "clearInputs('".$this->modelName."IndexForm');");
            } elseif ($mode == "X") {
                $options = array('div'=>false,'name' => 'botonLimpiar', 'id'=>'botonLimpiar', 'title' => 'Limpiar', 'class' => 'btn btn-primary');
                echo $this->controller->Form->Button('<i class="icon-repeat icon-white"></i>&nbsp;Limpiar', $options);
                
                $this->controller->Js->get('#botonLimpiar')->event('click', "clearInputs($('form')[0].id);");
            }
            
            echo "</div>";
            
            echo "
                  <div style='width:33%;float:right'>";
            
            /*
            if($mode == "M"){
             
                    echo $this->controller->Form->Button('Eliminar', array('type' => 'button', 'class' => 'btn btn-primary pull-left', 'div' => false, 'id' => 'btnEliminar'.$this->id));
                
                   $deleteRequest = $this->controller->Js->request(
                                array('controller' => $this->controllerName, 'action' => 'delete', $id),
                                array('update' => '#main-content', 'async' => false,
                                    'dataExpression' => true, 'method' => 'post',
                                    'data' => $this->controller->Js->serializeForm(array('isPost' => true, 'inline' => true)),
                                    'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                                )
                            );
                        

                   $this->controller->Js->get('#'.'btnEliminar'.$this->id)->event('click','if(confirm("Confirma que desea eliminar el registro?")) ' . $deleteRequest);
                
            }
            */
            
            if ($mode == "A" || $mode == "M" || $mode == "X" ) {
                if($this->showFormAcceptButton && !$this->readOnly){
                    echo "".
                    $this->controller->Form->Button('<i class=" icon-ok icon-white"></i>&nbsp;'.$this->formAcceptButtonText, array('class' => 'btn btn-primary pull-right', 'div' => false, 'id' => 'btnGuardar'.$this->id)).
                    "";
                }
            } elseif ($mode == "S") {
                if(!$this->readOnly){
                    echo "".
                    $this->controller->Html->link('Guardar' , array('controller' => $this->controllerName, 'action' => 'index'), array('escape' => false, 'class' => 'btn btn-primary pull-right', 'id' => 'btnGuardar'.$this->id)) .
                    //$this->controller->Form->end(array('label' => 'Aceptar', 'class' => 'button', 'div' => false, 'id' => 'btnGuardar'.$this->id)).
                    "";
                }
            }
            
            
            foreach($this->formButtons2 as $button)
            {
                $style="";
                if(isset($button['style']))
                    $style = $button['style']; 
                echo $this->controller->Form->Button('<i class="'.$button['icon'].'"></i>&nbsp;'.$button['text'], array('class' => 'btn btn-primary pull-right', 'div' => false, 'id' => $button['id'].$this->id, 'style' => $style));
            }
            
            echo "</div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>

           ";

            // Observers
            foreach($this->formObservers2 as $observer)
            {
                $this->controller->Js->get('#'.$observer['observe'])->event(
                    'change',
                    $this->controller->Js->request(
                        $observer['url'],
                        array('update' => '#'.$observer['update'], 'dataExpression' => true, 'data' => '$("#'.$observer['observe'].'").serialize()', 
                        'before' => 'showProcessing(true)',
                        'complete' => 'showProcessing(false)'
                        )
                        
                    )."$('#".$observer['update']."').trigger('change');"
                );
            }

            if ($mode == "A" || $mode == "M" || $mode == "C" || $mode == "X") {

                if(isset($this->formCancelButtonParameters) && $this->formCancelButtonParameters != null)
                    $parameters = $this->formCancelButtonParameters;
                else
                    $parameters = array('controller' => $this->controllerName, 'action' => 'index');
                
                $this->controller->Js->get('#btnCancelar'.$this->id)->event('click',
                    $this->controller->Js->request(
                        $parameters,
                        array('update' => '#main-content', 'async' => true,
                            'dataExpression' => true, 'method' => 'get',
                            'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                        )
                    )
                );

                if ($mode == "A" || $mode == "M" || $mode == "X")
                {
                    if($mode == "M")
                        $action = 'edit';
                    else
                        $action = 'add';
                        
                    if($this->formAction != null)
                        $action = $this->formAction;

                    /*
                    $this->controller->Js->get('#btnGuardar'.$this->id)->event('click',
                    $this->controller->Js->request(
                    array('controller' => $this->controllerName, 'action' => $action, $id),
                    array('update' => '#main-content', 'async' => true,
                    'dataExpression' => true, 'method' => 'post',
                    'data' => $this->controller->Js->serializeForm(array('isPost' => true, 'inline' => true)),
                    'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                    )
                    )
                    );
                    */


                    if($mode != 'X'){
                        
                        $ajaxSubmit = $this->controller->Js->request(
                            array('controller' => $this->controllerName, 'action' => $action, $id),
                            array('update' => '#main-content', 'async' => false,
                                'dataExpression' => true, 'method' => 'post',
                                'data' => $this->controller->Js->serializeForm(array('isPost' => true, 'inline' => true)),
                                'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                            )
                        );

                        $validate = "";
                        if(isset($this->formValidationFunction) && $this->formValidationFunction!="")
                            $validate = "if(!".$this->formValidationFunction.") return false; ";

                        $this->controller->Js->get('#btnGuardar'.$this->id)->event('click', $validate . $ajaxSubmit);
                        
                        
   
                    }else{
                        
                        
                        if(isset($this->formAcceptButtonParameters))
                            $parms = $this->formAcceptButtonParameters;
                        else
                            $parms = array('controller' => $this->controllerName, 'action' => $action, $id);
                            
                        $ajaxSubmit = $this->controller->Js->request(
                            $parms,
                            array('update' => '#main-content', 'async' => false,
                                'dataExpression' => true, 'method' => 'post',
                                'data' => $this->controller->Js->serializeForm(array('isPost' => true, 'inline' => true)),
                                'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                            )
                        );
                        
                        $validate = "";
                        if(isset($this->formValidationFunction) && $this->formValidationFunction!="")
                            $validate = "if(!".$this->formValidationFunction.") return false; ";

                        $this->controller->Js->get('#btnGuardar'.$this->id)->event('click', $validate . $ajaxSubmit);
                    }
                }
            }
            
            foreach($this->formButtons2 as $button)
            {
                $ajaxSubmit = $this->controller->Js->request(
                    array('controller' => $this->controllerName, 'action' => $action, $id),
                    array('update' => '#main-content', 'async' => false,
                        'dataExpression' => true, 'method' => 'post',
                        'data' => $this->controller->Js->serializeForm(array('isPost' => true, 'inline' => true)),
                        'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                    )
                );
                
                $this->controller->Js->get('#'.$button['id'].$this->id)->event('click', $button['onClick'] . $ajaxSubmit);
            }

            
        
            echo "<script type='text/javascript'>";
            
            if($this->formTabs != null){
                //echo "$('#tab-panel').createTabs();";  
            }

            foreach($this->formCustomScripts as $formCustomScript)
            {
                echo $formCustomScript;
            }

            if(isset($this->commonScripts) && sizeof($this->commonScripts) > 0){
                foreach($this->commonScripts as $commonScript)
                {
                    echo $commonScript;
                } 
            }

            if($this->executeFormThemeScripts){
                
                /*if($mode!='S') //Solo si no es subform, sino lo ejecuta 2 veces y no andan los combos
                    echo "execThemeScripts();";*/
                
            } 
            
            echo " $('.datepicker').datepicker();";
            echo "$('.datetimepicker').datetimepicker();";

            
            
            //Seleccionado 
            $this->addFormReadyScript("\$('input:text:visible:first').focus(function(){this.select();});");
            
            // Fix para Google Chrome
            $this->addFormReadyScript("\$('input:text:visible:first').mouseup(function(e){e.preventDefault();});");
            
            //Foco en el primer field
            $this->addFormReadyScript("\$('input:text:visible:first').focus();");
            
            //Campos Numericos
            $this->addFormReadyScript($this->numericInputScripts()); 
            
            //SubformDatatables
            $this->addFormReadyScript($this->subFormDatatablesScript($mode));   
            
            if(isset($this->formReadyScripts) && sizeof($this->formReadyScripts) > 0){
                
                echo "$(document).ready(function() {";
                foreach($this->formReadyScripts as $script)
                {
                    
                    echo $script;
                    
                }
                echo "});"; 
            }
            

            echo "</script>";

            echo $this->controller->Js->writeBuffer(); 
            
      
echo "</div>";
echo "</div>";
echo "</div>";
        }


        private function printGeoButtonScripts($id, $departamento, $localidad, $calle, $numero, $idX, $idY){

            $scriptGeo = "
            $('#".$id."').click(
            function(){

            if($('#".$departamento."').val().trim() == ''){
            alert('Debe ingresar un Departamento.');
            return;
            }
            if($('#".$localidad."').val().trim() == ''){
            alert('Debe ingresar una Localidad.');
            return;
            }
            if($('#".$calle."').val().trim() == ''){
            alert('Debe ingresar una Calle.');
            return;
            } 

            var nro = $('#".$numero."').val();
            
            if(nro==undefined)
                nro='-1';

            var url =  '" . Router::url(array('controller' => 'Georeferencia', 'action' => 'index',$idX,$idY)) . "' 
            + '/' + $('#".$departamento."').val()
            + '/' + $('#".$localidad."').val()
            + '/' + $('#".$calle."').val()
            + '/' + nro
            + '/' + '';

            //alert(encodeURI(url));
            $.fancybox.open({
            href :encodeURI(url),
            type : 'ajax',
            padding : 5,
            closeBtn: false
            });

            /*$.colorbox({href : encodeURI(url), width: '80%', height: '80%', speed:0});*/
            }
            );
            ";


            echo "<script type='text/javascript'>";
            echo $scriptGeo;
            echo "</script>";
        }

        private function printFormScriptReferences(){

            foreach($this->formScriptReferences as $scriptReference){

                echo $this->controller->Html->script($scriptReference);

            }

        }
        
        private function printFormJsVariables(){
            
            echo "<script type='text/javascript'>";
            
            //Seteo en una variable javascript el modo del formulario
            echo "var mode = '" . $this->mode . "';";
            
            if($this->mode != "S"){
                
                echo "var subFormTables = new Array();"; //Variable global que guarda los datatables
                
                $usedt = ($this->useSubformDatatables()?'true':'false');
                echo "var useSubFormDatatables = " . $usedt . ";"; //uso Datatables para subform o no
                
            }
            
            
             foreach($this->formJsVariables as $jsVariable){

                echo $jsVariable;

            }
            
            echo "</script>";
            
        } 

        private function processFormFields($fields){
            App::import('Lib/Enums', 'EnumRespuesta');

            $subforms = array();

            foreach($fields as $clave=>$valor){      


                if ($valor["type"] == 'input') 
                {

                    $valor['inputOptions']['div'] = false;
                    
                    echo    "<div class='".$valor['fieldOptions']['class']."'>
                    <label class='col-sm-4 col-form-label' for='".$valor['id']."'>".$valor['label']."</label>";
             
                    
                    
                    echo "<div class='col-sm-8'>";
                    echo $this->controller->Form->input($valor['id'], $valor['inputOptions']);
                    echo "</div>";
                    
                    
                    echo "</div>";

                }
                elseif ($valor["type"] == 'radio') 
                {
                    echo    "<div class='".$valor['fieldOptions']['class']."' ".(isset($valor['fieldOptions']['style']) ? "style='".$valor['fieldOptions']['style'] : "").">
                    <p>
                    <label class='mr-sm-2' for='".$valor['id']."'>".$valor['label']."</label>";    
           
                    echo $this->controller->Form->radio($valor['id'], $valor['data'], $valor['inputOptions']);
                   
                    echo "</div>";
                }
                elseif ($valor["type"] == 'check') 
                {
                    /*
                    echo    "<div class='".$valor['fieldOptions']['class']."' ".(isset($valor['fieldOptions']['style']) ? "style='".$valor['fieldOptions']['style'] : "").">
                    <p>
                    <label for='".$valor['id']."'>".$valor['label']."</label>";    
                    echo $this->controller->Form->checkbox($valor['id'], $valor['inputOptions']);
                    echo    "</p>
                    </div>";
                    */
                    
                    $style = "color: #666666 !important;
                            font-size: 12px;
                            font-weight: 700;
                            text-shadow: 0 1px 0 #FFFFFF;";
                    
                    echo    "<div class='".$valor['fieldOptions']['class']."' ".(isset($valor['fieldOptions']['style']) ? "style='".$valor['fieldOptions']['style'] : "").">
                    
                    <label class='mr-sm-2' style=" . $style . "for='".$valor['id']."'><div class='checker' id='uniform-undefined'><span>";
                    echo $this->controller->Form->checkbox($valor['id'], $valor['inputOptions']);
                    echo "</span></div>".$valor['label']."</label>";
                    echo    "
                    </div>";
                    
                    
                    
                }
                elseif ($valor["type"] == 'picker') 
                {
                    App::import('Lib/EntityPickers', 'EntityPickerHelper');

                    $eph = new EntityPickerHelper($valor['id']);
                    $eph->setPickerConfigurationClass($valor['pickerClassName']);
                    $eph->addSelectionList($valor['selectionList']);
                    $eph->setCallbacksJS(isset($valor['callbacks'])?$valor['callbacks']:"array()");
                    $eph->setfilterQueryFields(isset($valor['filterQueryFields'])?$valor['filterQueryFields']:"array()");

                    $eph->makeEntityPicker($this->controller, $valor['label'], $valor['id'], $valor['hiddenId'], $valor['inputOptions'], $valor['hiddenOptions'], $valor['fieldOptions']);


                }
                elseif ($valor["type"] == 'hidden') 
                {
                    $valor['inputOptions']['type'] = 'hidden';
                    echo $this->controller->Form->input($valor['id'], $valor['inputOptions']);
                }
                elseif ($valor["type"] == 'button')
                {
                    echo    "<div class='".$valor['fieldOptions']['class']."'>
                    <p>";
                    echo $this->controller->Form->button($valor['title'], $valor['inputOptions']);
                    echo    "</p>
                    </div>";
                }
                elseif ($valor["type"] == 'label') 
                {
                    echo $this->controller->Form->label(false, $valor['text'], $valor['inputOptions']);
                }
                elseif ($valor["type"] == 'link') 
                {
                    echo $this->controller->Html->link($valor['title'] , $valor['url'], $valor['options'], $valor['confirmMessage']);
                }
                elseif ($valor["type"] == 'html') 
                {
                    echo $valor['html'];
                }
                elseif ($valor["type"] == 'geoButton') 
                {
                    //$img = $this->controller->Html->image('map.png', array('plugin' => false, 'alt' => 'Georeferenciar', 'class' => 'img-left framed', 'id' => $valor['id']));
                    //echo "<div class='".$valor['class']."'><a id='' href='javascript:void(0);' title='Georeferenciar'>".$img."</a></div>";

                    echo "<div class='".$valor['class']."'>
                          <label class='mr-sm-2' for='textfield'>"."Georeferencia"."</label>
                      
                                <a id='".$valor['id']."' class='btn btn-small' href='javascript:void(0);'>
                                    <i  style='cursor: pointer;' class='icon-map-marker'></i>
                                </a>
                     
                        </div>";
                    
                    $this->printGeoButtonScripts($valor['id'], $valor['departamento'], $valor['localidad'], $valor['calle'], $valor['numero'], $valor['idX'], $valor['idY']);
                }
                elseif ($valor["type"] == 'subform')
                {
                    $valor["subform"]->setController($this->controller);
                    $valor["subform"]->setId($valor["id"]);

                    // Genero el subform en una variable
                    echo $this->controller->Js->writeBuffer();
                    ob_start();
                    $valor["subform"]->getForm2("S");

                    $this->controller->Js->get('#btnCancelar'.$valor["id"])->event('click', 'cancelar_'.$valor["id"].'();');

                    $validate = "";
                    if(isset($valor["subform"]->formValidationFunction) && $valor["subform"]->formValidationFunction!="")
                        $validate = "if(!".$valor["subform"]->formValidationFunction."){ $.colorbox.resize(); return false; }";

                    $this->controller->Js->get('#btnGuardar'.$valor["id"])->event('click', $validate . 'guardar_'.$valor["id"].'();');
                    echo $this->controller->Js->writeBuffer(); 

                    $subform = ob_get_contents();
                    ob_end_clean();
                    
                    array_push($subforms, $valor["id"]);

                    // Html del listado          
                    echo   
                    "<div id='".$valor["id"]."_listado'>
                    <div class='grid_12'>";

                    echo "<div class='block-border'>";
                    if ($valor["label"] != "" && $valor["label"] != null) {
                        echo "<div class='block-header'>
                                <legend><h5>".$valor["label"]."</h5></legend>
                              </div>";
                    }
                    echo "<div class='block-content'>
                    <div id='datatable_wrapper' class='dataTables_wrapper'>
                    <div class='top'>";
                    
                    
                        
                    if ((!isset($valor["options"]["alta"]) || $valor["options"]["alta"])){
                      
                        echo "<div>";
                        if($valor["subform"]->showNewButton){
                            echo $this->controller->Html->link('<span class="add-on"><i class="icon-plus-sign"></i></span>Nuevo' , array('action' => 'add'), array('escape' => false, 'class' => '', 'title' => 'Nuevo', 'data-gravity' => 's', 'id' => 'btnNuevo'.$valor["id"], 'class' => 'subformNuevo'));
                        }
                        echo "</div>";
                    }
                    
                    $subFormTableId = $valor["subform"]->id;
                    echo "<table id='subformtable_{$subFormTableId}' class='subformtable table table-striped table-bordered table-hover' style=''>
                    <thead>
                    <tr>";

                    foreach($valor["listado"] as $col){
                        if ($col["visible"]) {
                            $class = (array_key_exists("headclass", $col) ? $col["headclass"] : "");
                            echo "<th style='text-align:center;' class='".$class."'>".$col["nombre"]."</th>";
                        }
                    }

                    if($valor["subform"]->showActionColumn){
                        echo "                              <th style='text-align:center;'>Acciones</th>   ";
                    }
                    
                    echo "
                    </tr>
                    </thead>
                    <tbody>";

                    $i=0; 
                    $hiddens = "";
                    $submodels =  explode(".", $valor["model"]);
                    
                    //Variable que me dise el display del delete button
                    $displayDeleteButton = ($valor["subform"]->showDeleteButton?"":"display:none;");
                    
                    if ($this->controller->request->data /*&& array_key_exists($valor["model"], $this->controller->request->data) && count($this->controller->request->data[$valor["model"]]) > 0*/)
                    {
                        
                        $ix = implode($submodels, "']['");
                        eval("\$datos = isset(\$this->controller->request->data['". $ix ."'])?\$this->controller->request->data['". $ix ."']:array();");
                        foreach($datos as $dato){
                            $class = PagingHelper::getRowClass($i);
                            echo "<tr class='".$class."'>";

                            foreach($valor["listado"] as $col){
                                $col["dataclass"] = (array_key_exists("dataclass", $col) ? $col["dataclass"] : "");
                            
                                $aux = explode(".", $col["id"]);
                                array_shift($aux);

                                $fieldName = implode($aux, "']['");
                                eval("\$valorDato = isset(\$dato['".$fieldName."'])?\$dato['".$fieldName."']:null;");

                                if ($col["visible"])
                                    echo "<td class='".$col["dataclass"]."'>".$valorDato."</td>";

                                $hiddens .= "<input type=\"hidden\" name=\"data[".implode($submodels, "][")."][".$i."][".implode($aux, "][")."]\" value=\"".$valorDato."\" />";
                            }
                            
                            //Celda de acciones
                            if($valor["subform"]->showActionColumn){
                            
                                echo "<td class='center' style='width:10%;text-align:center;'>";

                                if (!isset($valor["options"]["modificacion"]) || $valor["options"]["modificacion"]){
                                    
                                    $title = "Editar";
                                    if($this->readOnly) $title = "Ver Detalle";
                                    
                                     echo "<a href=\"javascript:open_editRow_".$valor["id"]."(".$i.");\" title=\"".$title."\" id=\"btnEditar".$i."\">".
                                    "<span class='add-on'><i class='".$this->editIconClass."'></i></span></a>&nbsp;";

                                }

                                if (!isset($valor["options"]["baja"]) || $valor["options"]["baja"]){
                                     echo "<a href=\"javascript:deleteRow_".$valor["id"]."(".$i.");\" title=\"Eliminar\" id=\"btnEliminar".$i."\">".
                                    "<span style=".$displayDeleteButton."  class='add-on'><i class='icon-remove-sign'></i></span></a>";
                                }

                                echo "</td>";
                                
                            }

                            $i++;     
                        }
                    }


                    echo "                      </tbody>
                    </table>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    <input type='hidden' id='".$valor["id"]."pos' />
                    </div>
                    <div id='".$valor["id"]."_hiddens'>".$hiddens."</div>";



                    // Funciones Javascript
                    $js = 'var cant_'.$valor["id"].' = '.$i.';
                    var html_'.$valor["id"].' = decodeURIComponent("'.rawurlencode($subform).'");
                    
                    function open_addRow_'.$valor["id"].'() {
                    //alert(arguments.callee.name);
                    $("#'.$valor["id"].'").html(html_'.$valor["id"].');
                    $("#'.$valor["id"].'pos").val(null);
                    //execThemeScriptsSubForm("'.$valor["id"].'");
                    ';
                    
                    if (isset($valor['options']['onadd']))
                        $js .= $valor['options']['onadd'];
                    
                    $js .= '$.colorbox({inline:true, href:"#'.$valor["id"].'", maxWidth:"90%", maxHeight:"90%"});
                    }

                    function addRow_'.$valor["id"].'() {
                    $.colorbox.close();';
                    
                    if($this->useSubformDatatables){
                        
                        $js .= '//Freezo el estado del datatable y lo destruyo
                                var freeze = subFormTables["subformtable_'.$valor["id"].'"].fnGetSubformTableFreezeState();
                                subFormTables["subformtable_'.$valor["id"].'"].fnDestroy();';
                        
                    }
                        
                    $js .= '

                    var clase = "gradeX " + (cant_'.$valor["id"].'%2 == 1 ? "odd" : "even");
                    var row = $("<tr class=\'"+clase+"\'></tr>");
                    ';
                    foreach($valor["listado"] as $col){
                        $col["dataclass"] = (array_key_exists("dataclass", $col) ? $col["dataclass"] : "");
                        
                        $aux = explode(".", $col["id"]);
                        $name = "data[".implode($aux, "][")."]";
                        array_shift($aux);
                        $nameHidden = "data[".implode($submodels, "][")."][\"+cant_".$valor["id"]."+\"][".implode($aux, "][")."]";

                        $js .= 'var hid = $("<input type=\"hidden\" name=\"'.$nameHidden.'\" />");';
                        $js .= '
                        if ($("#'.$valor["id"].' [name=\''.$name.'\']").attr("type") == "checkbox")
                            hid.val($("#'.$valor["id"].' [name=\''.$name.'\']").is(":checked") ? 1 : 0);
                        else
                            hid.val($("#'.$valor["id"].' [name=\''.$name.'\']").val());
                        ';
                        $js .= '$("#'.$valor["id"].'_hiddens").append(hid);';
                        $js .= '$("#'.$valor["id"].' [name=\''.$name.'\']").val(null);';

                        if ($col["visible"])
                            $js .= 'row.append("<td class=\''.$col["dataclass"].'\'>"+hid.val()+"</td>");';
                    }       


                    $js .= 'row.append("<td class=\"center\" style=\"width:10%;text-align:center;\">';
                    if (!isset($valor["options"]["modificacion"]) || $valor["options"]["modificacion"])
                        $js .= '<a href=\"javascript:open_editRow_'.$valor["id"].'("+cant_'.$valor["id"].'+");\" title=\"Editar\" id=\"btnEditar"+cant_'.$valor["id"].'+"\">'."<span class='add-on'><i class='".$this->editIconClass."'></i></span></a>&nbsp;";
                    if (!isset($valor["options"]["baja"]) || $valor["options"]["baja"])
                        $js .= '<a href=\"javascript:deleteRow_'.$valor["id"].'("+cant_'.$valor["id"].'+")\" title=\"Eliminar\" id=\"btnEliminar"+cant_'.$valor["id"].'+"\">'. "<span  style=".$displayDeleteButton."  class='add-on'><i class='icon-remove-sign'></i></span>";
                    $js .= '</td>");

                    $("#'.$valor["id"].'_listado #subformtable_'.$valor["id"].' tbody").append(row);
                    cant_'.$valor["id"].'++;';
                    
                     if($this->useSubformDatatables){
                            
                            $js .= '//Reconstruyo el datatable
                                    $("#'.$valor["id"].'_listado #subformtable_'.$valor["id"].'").width("100%");
                                    subFormTables["subformtable_'.$valor["id"].'"] = $("#'.$valor["id"].'_listado #subformtable_'.$valor["id"].'").dataTable('.$this->getSubFormDataTableParameters().');
                                    subFormTables["subformtable_'.$valor["id"].'"].fnAdjustColumnSizing();
                                    subFormTables["subformtable_'.$valor["id"].'"].fnSetSubformTableFreezeState(freeze);
                                ';
                            
                        }
                    
                    if (isset($valor['options']['onchange']))
                        $js .= $valor['options']['onchange'];
                        
                    $js .= '}

                    function open_editRow_'.$valor["id"].'(pos) {
                    $("#'.$valor["id"].'").html(html_'.$valor["id"].');
                    $("#'.$valor["id"].'pos").val(pos);';
                    foreach($valor["listado"] as $col){
                        $aux = explode(".", $col["id"]);
                        $name = "data[".implode($aux, "][")."]";
                        array_shift($aux);
                        $nameHidden = "data[".implode($submodels, "][")."][\"+pos+\"][".implode($aux, "][")."]";

                        $js .= '
                        if ($("#'.$valor["id"].' [name=\''.$name.'\']").attr("type") == "checkbox")
                            $("#'.$valor["id"].' [name=\''.$name.'\']").attr("checked", $("[name=\''.$nameHidden.'\']").val()==1);
                        else
                            $("#'.$valor["id"].' [name=\''.$name.'\']").val($("[name=\''.$nameHidden.'\']").val());
                        ';
                    }
                    
                    if (isset($valor['options']['onedit']))
                        $js .= $valor['options']['onedit'];
                        
                    $js .= ';$.colorbox({inline:true, href:"#'.$valor["id"].'", maxWidth:"90%", maxHeight:"90%"});
                    }

                    function editRow_'.$valor["id"].'(pos) {
                    $.colorbox.close();';
                    
                    if($this->useSubformDatatables){
                        
                        $js .= '//Freezo el estado del datatable y lo destruyo
                                var freeze = subFormTables["subformtable_'.$valor["id"].'"].fnGetSubformTableFreezeState();
                                subFormTables["subformtable_'.$valor["id"].'"].fnDestroy();';
                        
                    }
                        
                    $js .= '

                    var row = $($("#'.$valor["id"].'_listado #subformtable_'.$valor["id"].' tbody tr")[pos]);
                    row.html("");';
                    foreach($valor["listado"] as $col){
                        $col["dataclass"] = (array_key_exists("dataclass", $col) ? $col["dataclass"] : "");
                        
                        $aux = explode(".", $col["id"]);
                        $name = "data[".implode($aux, "][")."]";
                        array_shift($aux);
                        $nameHidden = "data[".implode($submodels, "][")."][\"+pos+\"][".implode($aux, "][")."]";

                        $js .= '
                        if ($("#'.$valor["id"].' [name=\''.$name.'\']").attr("type") == "checkbox")
                            $("[name=\''.$nameHidden.'\']").val($("#'.$valor["id"].' [name=\''.$name.'\']").is(":checked") ? 1 : 0);
                        else
                            $("[name=\''.$nameHidden.'\']").val($("#'.$valor["id"].' [name=\''.$name.'\']").val());
                        ';
                        $js .= '$("#'.$valor["id"].' [name=\''.$name.'\']").val(null);';

                        if ($col["visible"])
                            $js .= 'row.append("<td class=\''.$col["dataclass"].'\'>"+$("[name=\''.$nameHidden.'\']").val()+"</td>");';
                    }       
                    $js .= 'row.append("<td class=\"center\" style=\"width:10%;text-align:center;\">';
                    if (!isset($valor["options"]["modificacion"]) || $valor["options"]["modificacion"])
                        $js .= '<a href=\"javascript:open_editRow_'.$valor["id"].'("+pos+");\" title=\"Editar\" id=\"btnEditar"+pos+"\">'."<span class='add-on'><i class='".$this->editIconClass."'></i></span></a>&nbsp;";
                    if (!isset($valor["options"]["baja"]) || $valor["options"]["baja"])
                        $js .= '<a href=\"javascript:deleteRow_'.$valor["id"].'("+pos+")\" title=\"Eliminar\" id=\"btnEliminar"+pos+"\">'."<span style=".$displayDeleteButton."  class='add-on'><i class='icon-remove-sign'></i></span>";
                    $js .= '</td>");';
                    
                     if($this->useSubformDatatables){
                            
                            $js .= '//Reconstruyo el datatable
                                    $("#'.$valor["id"].'_listado #subformtable_'.$valor["id"].'").width("100%");
                                    subFormTables["subformtable_'.$valor["id"].'"] = $("#'.$valor["id"].'_listado #subformtable_'.$valor["id"].'").dataTable('.$this->getSubFormDataTableParameters().');
                                    subFormTables["subformtable_'.$valor["id"].'"].fnAdjustColumnSizing();
                                    subFormTables["subformtable_'.$valor["id"].'"].fnSetSubformTableFreezeState(freeze);
                                ';
                            
                        }
                    
                    if (isset($valor['options']['onchange']))
                        $js .= $valor['options']['onchange'];
                        
                    $js .= '}

                    function deleteRow_'.$valor["id"].'(pos) {
                        '.(array_key_exists("bajaValidacion", $valor["options"]) ? 'if (!'.$valor["options"]["bajaValidacion"].'(pos)) return;' : "");
                    
                    if($this->useSubformDatatables){
                        
                        $js .= '//Freezo el estado del datatable y lo destruyo
                                var freeze = subFormTables["subformtable_'.$valor["id"].'"].fnGetSubformTableFreezeState();
                                subFormTables["subformtable_'.$valor["id"].'"].fnDestroy();';
                        
                    }
                        
                        $js .= '
                        
                        var visibility = [];
                        for (var i=pos; i<cant_'.$valor["id"].'; i++) {
                            visibility[i] = $($("#'.$valor["id"].'_listado #subformtable_'.$valor["id"].' tbody tr")[pos]).is(":visible");
                            $($("#'.$valor["id"].'_listado #subformtable_'.$valor["id"].' tbody tr")[pos]).remove();
                        }

                        for (var i=pos+1; i<cant_'.$valor["id"].'; i++) {
                            var clase = "gradeX " + ((i-1)%2 == 1 ? "odd" : "even");
                            var row = $("<tr class=\'"+clase+"\'></tr>");
                            if (!visibility[i]) row.hide();
                    ';
                    foreach($valor["listado"] as $col){
                        $col["dataclass"] = (array_key_exists("dataclass", $col) ? $col["dataclass"] : "");
                        
                        $aux = explode(".", $col["id"]);
                        array_shift($aux);
                        $nameHiddenOri = "data[".implode($submodels, "][")."][\"+i+\"][".implode($aux, "][")."]";
                        $nameHiddenDes = "data[".implode($submodels, "][")."][\"+(i-1)+\"][".implode($aux, "][")."]";

                        $js .= '$("[name=\''.$nameHiddenDes.'\']").val($("[name=\''.$nameHiddenOri.'\']").val());';

                        if ($col["visible"])
                            $js .= 'row.append("<td class=\''.$col["dataclass"].'\'>"+$("[name=\''.$nameHiddenDes.'\']").val()+"</td>");';
                    }       

                    $js .= 'row.append("<td class=\"center\" style=\"width:10%;text-align:center;\">';
                    if (!isset($valor["options"]["modificacion"]) || $valor["options"]["modificacion"])
                        $js .= '<a href=\"javascript:open_editRow_'.$valor["id"].'("+(i-1)+");\" title=\"Editar\" id=\"btnEditar"+(i-1)+"\">'."<span class='add-on'><i class='".$this->editIconClass."'></i></span></a>&nbsp;";
                    if (!isset($valor["options"]["baja"]) || $valor["options"]["baja"])
                        $js .= '<a href=\"javascript:deleteRow_'.$valor["id"].'("+(i-1)+")\" title=\"Eliminar\" id=\"btnEliminar"+(i-1)+"\">'."<span style=".$displayDeleteButton."  class='add-on'><i class='icon-remove-sign'></i></span></a>";
                    $js .= '</td>");

                            $("#'.$valor["id"].'_listado #subformtable_'.$valor["id"].' tbody").append(row);
                        }

                        cant_'.$valor["id"].'--;';
                        
                        if($this->useSubformDatatables){
                            
                            $js .= '//Reconstruyo el datatable
                                    $("#'.$valor["id"].'_listado #subformtable_'.$valor["id"].'").width("100%");
                                    subFormTables["subformtable_'.$valor["id"].'"] = $("#'.$valor["id"].'_listado #subformtable_'.$valor["id"].'").dataTable('.$this->getSubFormDataTableParameters().');
                                    subFormTables["subformtable_'.$valor["id"].'"].fnAdjustColumnSizing();
                                    subFormTables["subformtable_'.$valor["id"].'"].fnSetSubformTableFreezeState(freeze);
                                ';
                            
                        }
                        
                        

                    foreach($valor["listado"] as $col){
                        $aux = explode(".", $col["id"]);
                        array_shift($aux);
                        $nameHidden = "data[".implode($submodels, "][")."][\"+cant_".$valor["id"]."+\"][".implode($aux, "][")."]";

                        $js .= '$("[name=\''.$nameHidden.'\']").remove();';
                    }      
                    
                    if (isset($valor['options']['onchange']))
                        $js .= $valor['options']['onchange'];
                        
                    $js .= '
                    }


                    function guardar_'.$valor["id"].'() {
                        var pos = $("#'.$valor["id"].'pos").val();
                        if (pos == "")
                            addRow_'.$valor["id"].'();
                        else
                            editRow_'.$valor["id"].'(pos);
                    }

                    function cancelar_'.$valor["id"].'() {
                    ';
                    foreach($valor["listado"] as $col){
                        $aux = explode(".", $col["id"]);
                        $name = "data[".implode($aux, "][")."]";

                        $js .= '$("#'.$valor["id"].' [name=\''.$name.'\']").val(null);';
                    }       
                    $js .= '
                    $.colorbox.close();
                    }';

                    $this->controller->Js->buffer($js);

                    $this->controller->Js->get('#btnNuevo'.$valor["id"])->event('click','open_addRow_'.$valor["id"].'();');
                }
                elseif($valor["type"] == 'questions')
                {
                    $model = $valor['options']['model'];
                    $modelPreguntas = $valor['options']['modelPreguntas'];
                    
                    // Cabecera
                    echo "
                        <table id='questions".$valor['options']['model']."' class='table table-striped table-bordered table-hover table-questions'>
                            <thead>
                            <tr>
                                <th>Nro</th>
                                <th>".$valor['text']."</th>";
                    foreach($valor['options']['extras'] as $extra) {
                        echo "
                                <th>".$extra['text']."</th>";
                    }
                    echo "            
                            </tr>
                            </thead>
                            <tbody>";
                    
                    // Preguntas
                    foreach($valor["preguntas"] as $key => $tipo) {
                        echo "
                            <tr>
                                <td>".(isset($tipo[$modelPreguntas]['orden']) ? $tipo[$modelPreguntas]['orden'] : $tipo[$modelPreguntas]['id'])."</td>
                                <td>".$tipo[$modelPreguntas]['descripcion']."</td>
                                ";
                                
                        // Columnas respuesta
                        foreach($valor['options']['extras'] as $extra) {
                            //Si no hay columna requerida, le paso el campo id asi genera, cabeceada, cambiarlo
                            if(!isset($extra['columna_req'])) $extra['columna_req'] = 'id';
                            
                            $class = '';
                            if(isset($extra['class']))
                                $class = $extra['class'];
                            
                            if ($tipo[$modelPreguntas][$extra['columna_req']]) {
                                $opciones = array('type' => $extra['type'], 'label' => false, 'disabled' => false);
                                
                                if ($extra['type'] == "select") {
                                    $opciones['empty'] = true;
                                    if(isset($extra['valores'])){
                                        //Si pasa valores se los asigno
                                        $opciones['options'] = $extra['valores'];
                                    }elseif (isset($extra['opcionesRespuesta'])){
                                        //Sino, tomo del model de respuestas
                                        // Respuesta Custom
                                        $opcionesRespuesta = $extra['opcionesRespuesta'];
                                        $modelRtas = $opcionesRespuesta['modelOpciones'];
                                        $idRtaField = $opcionesRespuesta['campoId'];
                                        $detalleRtaField = $opcionesRespuesta['campoDetalle'];
                                        
                                        App::import('Model', $modelRtas);
                                        eval("\$mod = new ".$modelRtas."();");
                                        $opcionesRtaExtra = $mod->find('all');
                                        $optionsExt = array('recursive' => 0/*, 'fields' => array($idRtaField, $detalleRtaField, $extra['changeBehaviour']['campoControl'])*/);
                                        if(isset($extra['opcionesRespuesta']['filtroOpciones']))
                                            $optionsExt['conditions'] = array($extra['opcionesRespuesta']['filtroOpciones'] . ' = ' => $tipo[$modelPreguntas]['id']);
                                        $opcionesRtaExtra = $mod->find('all', $optionsExt);
                                        $opcionesRtaList = $this->getListFromOpcionesRespuesta($opcionesRtaExtra, $idRtaField, $detalleRtaField);
                                        $opciones['options'] = $opcionesRtaList;    
                                    } else {
                                        $opciones['options'] = array(EnumRespuesta::Si=>'Si',EnumRespuesta::No=>'No'); // Usa los valores de la tabla respuesta
                                    }
                                }
                            
                                $idCampoExtra = $model.'.'.$tipo[$modelPreguntas]['id'].'.'.$extra['columna']; 
                                echo "<td class='".$class."'>".$this->controller->Form->input($idCampoExtra, $opciones)."</td>";
                                    
                                //Javascript para el campo extra (analizo changeBehaviour)
                                if(isset($extra['changeBehaviour'])){
                                    
                                    // permite mandar arrays de changeBehaviour
                                    if (count(array_filter(array_keys($extra['changeBehaviour']), 'is_string')))
                                        $extra['changeBehaviour'] = array($extra['changeBehaviour']);
                                    
                                    foreach($extra['changeBehaviour'] as $changeB) {
                                        //Lo transformo al id generado en html
                                        $idFieldExtra = Inflector::camelize(str_replace('.', '_', $idCampoExtra));
                                        
                                        //Tomo los id de los que habilitan
                                        $options_control = "";
                                        if(isset($changeB['campoControl'])){
                                            $auxControl =  explode(".", $changeB['campoControl']);
                                            $ixControl = implode($auxControl, "']['");
                                            
                                            $auxControl =  explode(".", $idRtaField);
                                            $idControl = implode($auxControl, "']['");
                                            
                                            foreach($opcionesRtaExtra as $opcionRta){
                                                eval("\$options_control .= (isset(\$opcionRta['".$ixControl."']) && \$opcionRta['".$ixControl."']!='1')?\$opcionRta['".$idControl."'] . ',':'';");
                                            }
                                        } elseif (isset($changeB['disableValor'])) {
                                            $options_control = $changeB['disableValor'];
                                        }
                                        
                                         echo "
                                                <script type='text/javascript'>
                                                    $(function() {
                                                        $('#".$idFieldExtra."').change(function() {";
                                                        
                                                        foreach($changeB['camposHabilitar'] as $campoHabilitar){
                                                            $idCampoHabilitar = Inflector::camelize(str_replace('.', '_', $model.'.'.$tipo[$modelPreguntas]['id'].'.'.$campoHabilitar));;                                                          
                                                            //Con indefOf hago que habilite solo si el id esta en los que habilitan
                                                            echo    "var disable = ('{$options_control}'.indexOf($('#".$idFieldExtra."').val()) != -1);
                                                                    $('#".$idCampoHabilitar."').prop('disabled',disable);
                                                                    if (disable) $('#".$idCampoHabilitar."').val('');
                                                                    
                                                                    //Si es un combo y está vacío, lo dejo siempre deshabilitado
                                                                    if ($('#".$idCampoHabilitar."').prop('type') != undefined && $('#".$idCampoHabilitar."').prop('type').indexOf('select') >= 0 && $('#".$idCampoHabilitar." option').length <= 1){
                                                                        $('#".$idCampoHabilitar."').prop('disabled',true);
                                                                    }
                                                                    ";
                                                        }
                                                        
                                                        echo "});
                                                        
                                                        $('#".$idFieldExtra."').change();
                                                    });
                                                 </script>";
                                    }
                                }
                                //Fin analisis changeBehaviour
                                    
                            } else {
                                echo "<td class='".$class."'>No corresponde</td>";
                            }
                        }
                    }
                    
                    echo "
                                <script type='text/javascript'>
                                    function Completo".$model."() {
                                        var ok = true;
                                        $('#questions".$valor['options']['model']." input, #questions".$valor['options']['model']." select').each(function(i,elem) { 
                                            ok = ok && ($(elem).attr('disabled') || $(elem).val() != ''); 
                                        });
                                        
                                        return ok;
                                    }
                                </script>
                            </tbody>
                        </table>";   
                }
                elseif($valor["type"] == 'questions2')
                {
                    $model = $valor['options']['model'];
                    $modelPreguntas = $valor['options']['modelPreguntas'];
                    
                    // Cabecera
                    echo "
                        <table id='datatables' class='table table-striped table-bordered table-hover table-questions'>
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>".$valor['text']."</th>
                                <th></th>";
                    foreach($valor['options']['extras'] as $extra) {
                        echo "
                                <th>".$extra['text']."</th>";
                    }
                    echo "            
                            </tr>
                            </thead>
                            <tbody>";
                    
                    // Preguntas
                    foreach($valor["preguntas"] as $key => $tipo) {
                        echo "
                            <tr>
                                <td>".$tipo[$modelPreguntas]['id']."</td>
                                <td>".$tipo[$modelPreguntas]['descripcion']."</td>
                                ";
                            
                            
                            if(!isset($valor['options']['opcionesRespuesta'])){ 
                                //Respuesta Default: si / no
                                $opcionesRta = array('NO','SI');
                                $opcionesRtaList = $opcionesRta;
                            } else {
                                // Respuesta Custom
                                $opcionesRespuesta = $valor['options']['opcionesRespuesta'];
                                $modelRtas = $opcionesRespuesta['modelOpciones'];
                                $idRtaField = $opcionesRespuesta['campoId'];
                                $detalleRtaField = $opcionesRespuesta['campoDetalle'];
                                
                                App::import('Model', $modelRtas);
                                eval("\$mod = new ".$modelRtas."();");
                                $opcionesRta = $mod->find('all');
                                $optionss = array('fields' => array($idRtaField, $detalleRtaField));
                                if(isset($valor['options']['opcionesRespuesta']['filtroOpciones']))
                                    $optionss['conditions'] = array($valor['options']['opcionesRespuesta']['filtroOpciones'] . ' = ' => $tipo[$modelPreguntas]['id']);
                                if(isset($opcionesRespuesta['campoReq'])) 
                                    array_push($optionss['fields'], $opcionesRespuesta['campoReq']);
                                $opcionesRta = $mod->find('all', $optionss);
                                $opcionesRtaList = $this->getListFromOpcionesRespuesta($opcionesRta, $idRtaField, $detalleRtaField);
                            }   
                            
                            if(isset($opcionesRtaList)){
                              
                                echo  "<td>
                                    ".$this->controller->Form->input($model.'.'.$tipo[$modelPreguntas]['id'].'.tiene', array('label' => false, 'type' => 'select', 'options' => $opcionesRtaList));".
                                </td>
                                ";
                            }
                            
                            
                        // Columnas extras
                        foreach($valor['options']['extras'] as $extra) {
                            //Si no hay columna requerida, le paso el campo id asi genera, cabeceada, cambiarlo
                            if(!isset($extra['columna_req'])) $extra['columna_req'] = 'id';
                            
                            if ($tipo[$modelPreguntas][$extra['columna_req']]) {
                                
                                $opciones = array('label' => false, 'type' => $extra['type'], 'disabled' => false);
                                
                                if ($extra['type'] == "select") {
                                    $opciones['empty'] = true;
                                    if(isset($extra['valores'])){
                                        //Si pasa valores se los asigno
                                        $opciones['options'] = $extra['valores'];
                                    }else{
                                        //Sino, tomo del model de respuestas
                                        // Respuesta Custom
                                        $opcionesRespuesta =$extra['opcionesRespuesta'];
                                        $modelRtas = $opcionesRespuesta['modelOpciones'];
                                        $idRtaField = $opcionesRespuesta['campoId'];
                                        $detalleRtaField = $opcionesRespuesta['campoDetalle'];
                                        
                                        App::import('Model', $modelRtas);
                                        eval("\$mod = new ".$modelRtas."();");
                                        $opcionesRtaExtra = $mod->find('all');
                                        $optionsExt = array();
                                        if(isset($extra['opcionesRespuesta']['filtroOpciones']))
                                            $optionsExt['conditions'] = array($extra['opcionesRespuesta']['filtroOpciones'] . ' = ' => $tipo[$modelPreguntas]['id']);
                                        $opcionesRtaExtra = $mod->find('all', $optionsExt);
                                        $opcionesRtaList = $this->getListFromOpcionesRespuesta($opcionesRtaExtra, $idRtaField, $detalleRtaField);
                                        $opciones['options'] = $opcionesRtaList;
                                    }
                                }
                                
                                $idCampoExtra = $model.'.'.$tipo[$modelPreguntas]['id'].'.'.$extra['columna']; 
                                echo "<td>".$this->controller->Form->input($idCampoExtra, $opciones);".</td>";
                                    
                                //Javascript para el campo extra (analizo changeBehaviour)
                                if(isset($extra['opcionesRespuesta']['changeBehaviour'])){
                                    
                                    $changeB = $extra['opcionesRespuesta']['changeBehaviour'];
                                    //Lo transformo al id generado en html
                                    $idFieldExtra = Inflector::camelize(str_replace('.', '_', $idCampoExtra));
                                    
                                    //Tomo los id de los que habilitan
                                    $options_control = "";
                                     if(isset($changeB['campoControl'])){
                                      
                                         foreach($opcionesRtaExtra as $opcionRta){
                                            $auxControl =  explode(".", $changeB['campoControl']);
                                            $ixControl = implode($auxControl, "']['");
                                          
                                            eval("\$options_control .= (isset(\$opcionRta['".$ixControl."']) && \$opcionRta['".$ixControl."']=='1')?\$opcionRta['".$ixControl."'] . ',':'';");
                                         }
                                     } 
                                    
                                     echo "
                                            <script type='text/javascript'>
                                                $(function() {
                                                    $('#".$idFieldExtra."').change(function() {";
                                                    
                                                    foreach($changeB['camposHabilitar'] as $campoHabilitar){
                                                        $idCampoHabilitar = Inflector::camelize(str_replace('.', '_', $model.'.'.$tipo[$modelPreguntas]['id'].'.'.$campoHabilitar));;                                                          
                                                        //Con indefOf hago que habilite solo si el id esta en los que habilitan
                                                        echo    "var disable = ('{$options_control}'.indexOf($('#".$idFieldExtra."').val()) != -1);
                                                                $('#".$idCampoHabilitar."').prop('disabled',disable);
                                                                if (disable) $('#".$idCampoHabilitar."').val('');
                                                                ";
                                                    }
                                                    
                                                    echo "});
                                                    
                                                    $('#".$idFieldExtra."').change();
                                                });
                                             </script>";
                                }
                                //Fin analisis changeBehaviour
                                    
                            } else {
                                echo "<td>No corresponde</td>";
                            }
                        }
                        if(!isset($valor['options']['opcionesRespuesta'])){
                            //Javascript original Respuestas
                            echo "
                                </tr>
                                <script type='text/javascript'>
                                    $(function() {
                                        $('[name=\"data[".$model."][".$tipo[$modelPreguntas]['id']."][tiene]\"]').change(function() {
                                            //alert('change');
                                            var disable = !($('[name=\"data[".$model."][".$tipo[$modelPreguntas]['id']."][tiene]\"]').val() == '1');
                                                ";
                            foreach($valor['options']['extras'] as $extra) {
                                echo            "$('[name=\"data[".$model."][".$tipo[$modelPreguntas]['id']."][".$extra['columna']."]\"]').prop('disabled',disable);
                                                if (disable) $('[name=\"data[".$model."][".$tipo[$modelPreguntas]['id']."][".$extra['columna']."]\"]').val('');";
                            }
                            echo "
                                        });
                                        $('[name=\"data[".$model."][".$tipo[$modelPreguntas]['id']."][tiene]\"]').change();
                                    });
                                </script>
                                ";
                        }else{
                          //Javascript custom para Respuestas custom  
                          $options_requiere = "";
                          foreach($opcionesRta as $opcionRta){
                              
                              if(isset($valor['options']['opcionesRespuesta']['campoReq']) && isset($opcionRta[$valor['options']['opcionesRespuesta']['modelOpciones']][$valor['options']['opcionesRespuesta']['campoReq']])
                                && $opcionRta[$valor['options']['opcionesRespuesta']['modelOpciones']][$valor['options']['opcionesRespuesta']['campoReq']] == '1'){
                               
                                    
                               //$options_requiere = $opcionRta[$valor['options']['opcionesRespuesta']['modelOpciones']][$valor['options']['opcionesRespuesta']['campoId']] . ',';
                               $fields =  explode(".", $valor['options']['opcionesRespuesta']['campoId']);
                               $ixId = implode($fields, "']['");
                               eval("\$options_requiere .= \$opcionRta['".$ixId."'] . ',';");
                                }
                              
                          }
                          
                          echo "
                                </tr>
                                <script type='text/javascript'>
                                    $(function() {
                                        $('[name=\"data[".$model."][".$tipo[$modelPreguntas]['id']."][tiene]\"]').change(function() {
                                            var disable = ('{$options_requiere}'.indexOf($('[name=\"data[".$model."][".$tipo[$modelPreguntas]['id']."][tiene]\"]').val()) == -1);
                                ";
                            //Habilito / deshabilito las columnas que correspondan
                            foreach($valor['options']['extras'] as $extra) {
                                if(array_key_exists('columnasHabilitar', $valor['options']['opcionesRespuesta']) && in_array($extra['columna'], $valor['options']['opcionesRespuesta']['columnasHabilitar'])){
                                    //echo "alert('[name=\"data[".$model."][".$tipo[$modelPreguntas]['id']."][".$extra['columna']."]\"]');";
                                    //echo "alert('{$options_requiere}');";
                                    echo    "$('[name=\"data[".$model."][".$tipo[$modelPreguntas]['id']."][".$extra['columna']."]\"]').prop('disabled',disable);
                                            if (disable) $('[name=\"data[".$model."][".$tipo[$modelPreguntas]['id']."][".$extra['columna']."]\"]').val('');";
                                }
                            }
                            echo "
                                        });
                                        $('[name=\"data[".$model."][".$tipo[$modelPreguntas]['id']."][tiene]\"]').change();
                                    });
                                </script>
                                ";  
                            
                        }
                    }
                    echo "
                                <script type='text/javascript'>
                                    function Completo".$model."() {";
                    foreach($valor["preguntas"] as $key => $tipo) {
                        echo "
                                        if ($('[name=\"data[".$model."][".$tipo[$modelPreguntas]['id']."][tiene]\"]').val() == '1') {";
                        foreach($valor['options']['extras'] as $extra) {
                            echo "
                                            if ($('[name=\"data[".$model."][".$tipo[$modelPreguntas]['id']."][".$extra['columna']."]\"]').val() == '') return false;";
                        }
                        echo "
                                        }";
                    }
                    echo "
                                        return true;
                                    }
                                </script>
                            </tbody>
                        </table>";   
                }
                else
                {
                    echo "<div id='".$valor['id']."' class='".$valor['fieldOptions']['class']."'><h1>".$valor['label']."</h1></div>";
                }

            }

            foreach($subforms as $subform) {
                echo "<div style='display:none;'><div id='".$subform."'>";
                //echo $subform->getForm2("S");
                echo "</div></div>";
            }   

        } 
        
        private function getListFromOpcionesRespuesta($opcionesRta, $campoId, $campoDetalle){
            
            $list = array();
            
            $fields =  explode(".", $campoId);
            $ixId = implode($fields, "']['");
            $fields =  explode(".", $campoDetalle);
            $ixDetail = implode($fields, "']['");
            
            foreach($opcionesRta as $opc){
                
                
                eval("\$list[\$opc['".$ixId."']] = \$opc['".$ixDetail."'];");
                
                //$list[$opc[$model][$campoId]] = $opc[$model][$campoDetalle];
                
            }
            
            return $list;
            
            
        }
        
        /**
        * Javascripts para campos solo numericos
        * 
        * @param mixed $echo si imprime el script o solo devuele el string
        */
        private function numericInputScripts($echo=false){
            
            $scripts = "";
            
            $scripts.= '$(".integer").numericInput();';
            $scripts.= '$(".float").numericInput({ allowFloat: true });';
            //$("#positiveNumber").numericInput({ allowFloat: true });
            //$("#anyInt").numericInput({ allowNegative: true });
            //$("#anyNumber").numericInput({ allowFloat: true, allowNegative: true });
            
            if($echo)
                echo $scripts;
                
            return $scripts;
            
        }
        
        private function subFormDatatablesScript($mode){

            
            //Solo si no es subform.
            if($mode == "S" or !$this->useSubformDatatables)
                return;
            
            return  "

            
            $( '.subformtable' ).each(function( index ) {
                
                var dt = $( this ).dataTable(".$this->getSubFormDataTableParameters().");
                
                subFormTables[this.id] = dt;

                
            });
            ";

        }
        
        private function getSubFormDataTableParameters(){
            
            
            return "{
                'sDom': 'frtip',
                'oLanguage': {
                              'sEmptyTable': '',
                              'sSearch': 'Buscar:',
                              'sInfoEmpty': 'No se encontraron registros.',
                              'sZeroRecords': '',
                              'sInfo': 'Registros _START_ a _END_ de _TOTAL_',
                              'sInfoFiltered': '',
                              'oPaginate': {
                                'sPrevious': 'Anterior',
                                'sNext': 'Siguiente',
                              }
                            }
                        } ";
            
        }
        
        public function getReporte($mode, $id = null){

            $this->mode = $mode;
            
            $this->printFormJsVariables();
            
            $this->printFormScriptReferences();

            //Si tiene un class spanx seteado, le pongo el offset que corresponde para centrarlo
            if(isset($this->form2["options"]))
                $this->form2["options"] = $this->setOffset($this->form2["options"]);
            
            echo $this->controller->Form->create($this->form2["label"], $this->form2["options"]);
            echo "<legend><h4 id='".$this->titulo."-title-link'>".$this->tituloForm."</h4></legend>";
            
            if($this->formTabs!=null){
                
                $i = 0;
                echo "<ul class='nav nav-tabs'>";
                foreach($this->formTabs as $tab){
                    $i++;
                    if($i==1)
                        $class = "active";
                    else
                        $class = ""; 
                        
                    echo "<li class='{$class}' id='".$tab['id']."-link'><a href='#" . $tab['id'] ."' data-toggle='tab'>". $tab['caption'] ."</a></li>"; 

                }
                echo "</ul>";
            }  
            
            echo "<div id='validationMsg_".$this->id."' style='display:none;'></div>";


            if($this->formTabs!=null)
                echo "<div id='myTabContent' class='tab-content'>";
                  
                
            $this->processFormFields($this->formFields2);
            
            if($this->formTabs!=null)
                echo "</div>";

            // pintar boton volver
            echo "<div class='clear'></div>
            <div class='control-group'>
            <div style='width:33%;float:left' class='centered'>
            " . 
            $this->controller->Form->Button('<i class="icon-arrow-left icon-white"></i>&nbsp;Volver', array('type' => 'button', 'class' => 'btn btn-primary pull-left', 'div' => false, 'id' => 'btnCancelar'.$this->id)) .
            "</div>";
            
            //pintar boton consultar
            echo "
                  <div style='width:33%;float:right'>";
            
            echo "".
            $this->controller->Form->Button('<i class=" icon-ok icon-white"></i>&nbsp;'.$this->formAcceptButtonText, array('class' => 'btn btn-primary pull-right', 'div' => false, 'id' => 'btnGuardar'.$this->id)).
            "";

            //pintar boton limpiar             
            $options = array('div'=>false,'name' => 'botonLimpiar', 
                'id'=>'botonLimpiar', 
                'title' => 'Limpiar', 
                'class' => 'btn btn-primary pull-right', 
                'style' => 'margin-right:20px');                        
            echo "".
            $this->controller->Form->Button('<i class="icon-repeat icon-white"></i>&nbsp;Limpiar', $options);
            "";
                                                    

            
            foreach($this->formButtons2 as $button)
            {
                $style="";
                if(isset($button['style']))
                    $style = $button['style']; 
                echo $this->controller->Form->Button('<i class="'.$button['icon'].'"></i>&nbsp;'.$button['text'], array('class' => 'btn btn-primary pull-right', 'div' => false, 'id' => $button['id'].$this->id, 'style' => $style));
            }
            
            echo "</div>
            </div>
            </div>
            </div>
            </div>
            </div>

            <div class='clear height-fix'></div>";

            // Observers
            foreach($this->formObservers2 as $observer)
            {
                $this->controller->Js->get('#'.$observer['observe'])->event(
                    'change',

                    $this->controller->Js->request(
                        $observer['url'],
                        array('update' => '#'.$observer['update'], 'dataExpression' => true, 'data' => '$("#'.$observer['observe'].'").serialize()', 
                        'before' => 'showProcessing(true)',
                        'complete' => 'showProcessing(false)'
                        )
                        
                    )."$('#".$observer['update']."').trigger('change');"
                );
            }



            if(isset($this->formCancelButtonParameters) && $this->formCancelButtonParameters != null)
                $parameters = $this->formCancelButtonParameters;
            else
                $parameters = array('controller' => $this->controllerName, 'action' => 'index');

            $this->controller->Js->get('#btnCancelar'.$this->id)->event('click',
                $this->controller->Js->request(
                    $parameters,
                    array('update' => '#main-content', 'async' => true,
                        'dataExpression' => true, 'method' => 'get',
                        'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                    )
                )
            );


            if($this->formAction != null)
                $action = $this->formAction;

            if(isset($this->formAcceptButtonParameters))
                $parms = $this->formAcceptButtonParameters;
            else
                $parms = array('controller' => $this->controllerName, 'action' => $action, $id);
                
            $ajaxSubmit = $this->controller->Js->request(
                $parms,
                array('update' => '#main-content', 'async' => false,
                    'dataExpression' => true, 'method' => 'post',
                    'data' => $this->controller->Js->serializeForm(array('isPost' => true, 'inline' => true)),
                    'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                )
            );

            //asignar js validacion a boton guardar
            $validate = "";
            if(isset($this->formValidationFunction) && $this->formValidationFunction!="")
                $validate = "if(!".$this->formValidationFunction.") return false; ";

            $this->controller->Js->get('#btnGuardar'.$this->id)->event('click', $validate . $ajaxSubmit);
            
            foreach($this->formButtons2 as $button)
            {
                $ajaxSubmit = $this->controller->Js->request(
                    array('controller' => $this->controllerName, 'action' => $action, $id),
                    array('update' => '#main-content', 'async' => false,
                        'dataExpression' => true, 'method' => 'post',
                        'data' => $this->controller->Js->serializeForm(array('isPost' => true, 'inline' => true)),
                        'before' => 'showProcessing(true);', 'complete' => 'showProcessing(false);'
                    )
                );
                
                $this->controller->Js->get('#'.$button['id'].$this->id)->event('click', $button['onClick'] . $ajaxSubmit);
            }

            echo "<script type='text/javascript'>";
            
            if($this->formTabs != null){
                //echo "$('#tab-panel').createTabs();";  
            }

            foreach($this->formCustomScripts as $formCustomScript)
            {
                echo $formCustomScript;
            }

            if(isset($this->commonScripts) && sizeof($this->commonScripts) > 0){
                foreach($this->commonScripts as $commonScript)
                {
                    echo $commonScript;
                } 
            }

            if($this->executeFormThemeScripts){
                
                /*if($mode!='S') //Solo si no es subform, sino lo ejecuta 2 veces y no andan los combos
                    echo "execThemeScripts();";*/
                
            } 
            
            echo " $('.datepicker').datepicker();";
            echo "$('.datetimepicker').datetimepicker();";

            
            
            //Seleccionado 
            $this->addFormReadyScript("\$('input:text:visible:first').focus(function(){this.select();});");
            
            // Fix para Google Chrome
            $this->addFormReadyScript("\$('input:text:visible:first').mouseup(function(e){e.preventDefault();});");
            
            //Foco en el primer field
            $this->addFormReadyScript("\$('input:text:visible:first').focus();");
            
            //Campos Numericos
            $this->addFormReadyScript($this->numericInputScripts()); 
            
            //SubformDatatables
            $this->addFormReadyScript($this->subFormDatatablesScript($mode));   
            
            if(isset($this->formReadyScripts) && sizeof($this->formReadyScripts) > 0){
                
                echo "$(document).ready(function() {";
                foreach($this->formReadyScripts as $script)
                {
                    
                    echo $script;
                    
                }
                echo "});"; 
            }
            

            echo "</script>";

            echo $this->controller->Js->writeBuffer(); 

        }
        
        
        public function setHeaderAndFieldFromRow($data){
        	
        	
        	
        	foreach($data[0] as $key=>$columna){
        		
        		
        		
        		foreach($columna as $key2=>$valor){
        			$this->addHeader(Inflector::humanize($key2), 'tipo_comprobante.codigo_tipo_comprobante');
        			$this->addField($key, $key2);
        		}
        		
        		
        		
        		
        	}
        	
        	
        	
        }
        
        
        public function setearPropiedades($propiedades){
        	
        	foreach($propiedades as $key=>$propiedad_form_builder){
        		
        		
        		
        		
        		
        		$this->{$key}($propiedad_form_builder);
        		
        		
        	}
        	
        }
        
        
        public function getButtonsDatatable(){
        	
        	
        	$botones = array();
        	
        	
        	if($this->showBackDatatableButton){
        		
        		array_push($botones,"{
        		text: 'Volver',
        		action: function ( e, dt, node, config ) {
        			window.history.back();
        		}
        	}");
        	}
        	
        	
        	if($this->showCopyDatatableButton){
        		
        		
        		array_push($botones,"{
        		extend: 'copy',
        		text: 'Copiar'
        	}");
        		
        	}
        	
        	if($this->showExcelDatatableButton){
        		
        		array_push($botones,"'excel'");
        	}
        	
        	
        	return  implode(', ', $botones);
        	
        	 
        	
        	
        }

        
}
