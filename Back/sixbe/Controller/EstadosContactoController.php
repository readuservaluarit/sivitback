<?php
App::uses('EstadosPersonaController', 'Controller');


  /**
    * @secured(CONSULTA_CONTACTO)
  */
class EstadosContactoController extends EstadosPersonaController {
    
    public $name = 'EstadosContacto';
    public $model = 'EstadoPersona';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $id_tipo_persona = EnumTipoPersona::Contacto;
    
    
    /**
    * @secured(CONSULTA_CONTACTO)
    */
    public function index() {
        
         parent::index();    
        
    }
    
    

    /**
    * @secured(ADD_CONTACTO)
    */
    public function add(){
        
     parent::add();    
        
    }
    
    
    /**
    * @secured(MODIFICAR_CONTACTO)
    */
    public function edit($id){
        
     parent::edit($id);    
        
    }
    
    
     /**
    * @secured(BORRAR_CONTACTO)
    */
    public function delete($id){
        
     parent::delete($id);    
        
    }

     
   /**
    * @secured(CONSULTA_CONTACTO)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
      
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
        
        
         $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);          
    }
    
    
    
    
}
?>