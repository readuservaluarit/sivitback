/* REPOERTE LISTADO DE VENTAS POR JURISDICCION*/



SELECT p.`id_provincia` AS id_provincia , prov.d_provincia,SUM(fc.`subtotal`) AS subtotal_prov 
FROM factura fc
JOIN cliente c ON c.numero = fc.id_cliente
JOIN valuarit_valmec.`persona` p ON p.`cuit` = REPLACE(c.cuit,'-','') AND p.id_tipo_persona = 1
JOIN valuarit_valmec.`provincia` prov ON prov.id = p.`id_provincia`
WHERE fc.cae > 0 AND fc.fecha_emision >= '2015-07-01' AND fc.fecha_emision <= '2015-12-31' 


UNION

SELECT p.`id_provincia` AS id_provincia, prov.d_provincia, SUM(cr.`subtotal`)*-1 AS subtotal_prov 
FROM nota cr
JOIN cliente c ON c.numero = cr.id_cliente AND cr.tipo = 'CR' 
JOIN valuarit_valmec.`persona` p ON p.`cuit` = REPLACE(c.cuit,'-','') AND p.id_tipo_persona = 1
JOIN valuarit_valmec.`provincia` prov ON prov.id = p.`id_provincia`
WHERE cr.cae > 0  AND cr.fecha_emision >= '2015-07-01' AND cr.fecha_emision <= '2015-12-31' 

UNION

SELECT p.`id_provincia` AS id_provincia , prov.d_provincia, SUM(DB.`subtotal`) AS subtotal_prov 
FROM nota DB
JOIN cliente c ON c.numero = DB.id_cliente AND DB.tipo = 'DB'
JOIN valuarit_valmec.`persona` p ON p.`cuit` = REPLACE(c.cuit,'-','') AND p.id_tipo_persona = 1
JOIN valuarit_valmec.`provincia` prov ON prov.id = p.`id_provincia`
WHERE DB.cae > 0  AND DB.fecha_emision >= '2015-07-01' AND DB.fecha_emision <= '2015-12-31' 


/**/

select fecha_emision, tipo, id , id_cliente as CLIENTE, nro_factura as fc_fox 
,mask(nro_factura_afip,'0007-########') as factura, subtotal ,ROUND(subtotal*iva/100,2) as IVA,
perc_ib AS IIBB, total 
from factura where procesado = 1 and fecha_emision >= '{0}' and fecha_emision <= '{1}'
UNION 
select fecha_emision, tipo, id , id_cliente as CLIENTE, nro_factura as fc_fox 
,mask(nro_factura_afip,'0007-########') as factura, subtotal ,ROUND(subtotal*iva/100,2) as IVA,
perc_ib AS IIBB, total 
from nota where procesado = 1 and fecha_emision >= '{0}' and fecha_emision <= '{1}'

ORDER BY fecha_emision, desde.ToString("yyyy-MM-dd"), hasta.ToString("yyyy-MM-dd"))

