<?php
App::uses('ComprobantesController', 'Controller');



class NotasController extends ComprobantesController

    {
    public $name = EnumController::Notas;

    public $model = 'Comprobante';

    public $helpers = array(

        'Session',
        'Paginator',
        'Js'
    );
    public $components = array(

        'Session',
        'PaginatorModificado',
        'RequestHandler'
    );
   
    public $requiere_impuestos = 1;

 // si el comprobante acepta impuesto, esto llama a las funciones de IVA E IMPUESTOS

    public $id_sistema_comprobante = EnumSistema::VENTAS;

    /**
     * @secured(CONSULTA_NOTA)
     */
    public

    function index()
        {
        if ($this->request->is('ajax')) 
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        
        
        $this->PaginatorModificado->settings = array(
            'limit' => $this->numrecords,
            'update' => 'main-content',
            'evalScripts' => true
        );
        
        $conditions = $this->RecuperoFiltros($this->model);
        $array_conditions = array(
        	'Usuario',
            'ComprobanteItem',
            'Persona',
            'EstadoComprobante',
            'Moneda',
            'CondicionPago',
            'TipoComprobante',
            'ComprobanteImpuesto' => array(
                'Impuesto' => array(
                    'Sistema',
                    'ImpuestoGeografia'
                )
            ) ,
            'PuntoVenta',
            'DetalleTipoComprobante',
            'Transportista',
            'Sucursal'
        );

        // Si pasa el ID traigo diferentes models relacionados,sino lo basico

        /*
        if(in_array('Comprobante.id',$conditions))//elijo los models a traer dependiendo si es consulta o index
        $array_conditions = array('Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante','ComprobanteImpuesto'=>array('Impuesto'=>array('Sistema','ImpuestoGeografia')),'PuntoVenta');
          else
        $array_conditions = array('Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante','ComprobanteImpuesto','PuntoVenta');
        */
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' => $array_conditions,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum() ,
            'order' => $this->model . '.id desc'
        );
      
        
        
            $this->PaginatorModificado->settings = $this->paginate; 
            $data = $this->PaginatorModificado->paginate($this->model);
            $page_count = $this->params['paging'][$this->model]['pageCount'];
            foreach($data as & $valor)
                {

                // $valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
                // unset($valor['ComprobanteItem']);

                if (isset($valor['TipoComprobante']))
                    {
                    $valor[$this->model]['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                    $valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
                    unset($valor['TipoComprobante']);
                    }

                if (isset($valor['PuntoVenta']))
                    {
                    $valor[$this->model]['pto_nro_comprobante'] = (string)$this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'], $valor[$this->model]['nro_comprobante']);
                    unset($valor['PuntoVenta']);
                    }

                if (isset($valor['Transportista']))
                    {
                    $valor[$this->model]['t_razon_social'] = $valor['Transportista']['razon_social'];
                    unset($valor['Transportista']);
                    }

                /*
                foreach($valor[$this->model]['ComprobanteItem'] as &$producto ){
                $producto["d_producto"] = $producto["Producto"]["d_producto"];
                $producto["codigo_producto"] = $this->getProductoCodigo($producto);
                unset($producto["Producto"]);
                }

                */
                    
                if(isset($valor['Usuario'])){
                    	$valor[$this->model]['d_usuario'] = $valor['Usuario']['username'];
                    	unset($valor['Usuario']);
                }
                    
                $valor[$this->model]['cotizacion'] =  	$this->getCotizacionConMonedaCorriente($valor);
                
                
                if (isset($valor['Moneda']))
                    {
                    $valor[$this->model]['d_moneda'] = $valor['Moneda']['d_moneda'];
                    $valor[$this->model]['simbolo_internacional'] = $valor['Moneda']['simbolo_internacional'];
                    unset($valor['Moneda']);
                    }

                if (isset($valor['ComprobanteImpuesto']))
                    {
                    $valor[$this->model]['ComprobanteImpuestoDefault'] = $this->getImpuestos($valor['ComprobanteImpuesto'], $valor["Comprobante"]["id"], array(
                        EnumSistema::VENTAS
                    ) , array() , EnumTipoImpuesto::PERCEPCIONES);
                    
                    $valor[$this->model]['ComprobanteImpuestoTotal'] = (string)$this->TotalImpuestos($valor[$this->model]['ComprobanteImpuestoDefault']);
                    $valor[$this->model]['ComprobanteImpuestoIva'] = $this->getImpuestos($valor['ComprobanteImpuesto'], $valor["Comprobante"]["id"], array(
                        EnumSistema::VENTAS
                    ) , array(
                        EnumImpuesto::IVANORMAL
                    ) , EnumTipoImpuesto::IVA_IMPUESTO_VALOR_AGREGADO);
                    $valor[$this->model]['ComprobanteIvaTotal'] = (string)$this->TotalImpuestos($valor[$this->model]['ComprobanteImpuestoIva']);
                    }

                if (isset($valor['EstadoComprobante']))
                    {
                    $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
                    unset($valor['EstadoComprobante']);
                    }

                if (isset($valor['CondicionPago']))
                    {
                    $valor[$this->model]['d_condicion_pago'] = $valor['CondicionPago']['d_condicion_pago'];
                    unset($valor['CondicionPago']);
                    }

                if(isset($valor['Persona'])){
                    	
                    	if($valor['Persona']['id']== null || $valor['Persona']['id'] == EnumPersonaBase::ClienteNoRegistrado)
                    	{
                    		$valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social'];
                    		$valor[$this->model]['id_tipo_iva'] = $valor['Persona']['id_tipo_iva'];
                    		$valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
                    		$valor[$this->model]['id_lista_precio_defecto_persona'] = $valor['Persona']['id_lista_precio'];
                    	}
                    	else{
                    		$valor[$this->model]['razon_social'] = $valor['Persona']['razon_social'];
                    		$valor[$this->model]['id_tipo_iva'] = $valor['Persona']['id_tipo_iva'];
                    		$valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
                    		$valor[$this->model]['id_lista_precio_defecto_persona'] = $valor['Persona']['id_lista_precio'];
                    	}
                    	unset($valor['Persona']);
                    }

                if (isset($valor['DepositoOrigen']))
                    {
                    $valor['Comprobante']['d_deposito_origen'] = $valor['DepositoOrigen']['d_deposito_origen'];
                    }

                if (isset($valor['DepositoDestino']))
                    {
                    $valor['Comprobante']['d_deposito_destino'] = $valor['DepositoDestino']['d_deposito_destino'];
                    }
            
                if(isset($valor['Sucursal'])){
                      $valor['Comprobante']['d_sucursal'] =$valor["Sucursal"]["nombre"]." -  ".$valor["Sucursal"]["razon_social"];
                }    
                    

                if (isset($valor['DetalleTipoComprobante']))
                    {
                    $valor['Comprobante']['d_detalle_tipo_comprobante'] = $valor['DetalleTipoComprobante']['d_detalle_tipo_comprobante'];
                    }

                $valor["Comprobante"]["es_importado"] = (string)$this->Comprobante->esImportado($valor['ComprobanteItem']);
                unset($valor['ComprobanteItem']);
                $this->{$this->model}->formatearFechas($valor);
                unset($valor['ComprobanteImpuesto']);
                unset($valor['DetalleTipoComprobante']);
               }
                    
                
                $this->data = $data;                
                
   
                
                
                $seteo_form_builder = array("showActionColumn" =>true,"showNewButton" =>false,"showDeleteButton"=>false,"showEditButton"=>false,"showPrintButton"=>true,"showFooter"=>true,"showHeaderListado"=>true,"showXlsButton"=>false);
                
                $this->title_form = "Listado de Notas";
                $this->preparaHtmLFormBuilder($this,$seteo_form_builder,"");
                
                
                $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "list",
                    "content" => $data,
                    "page_count" => $page_count
                );
                $this->set($output);
                
                
                
                $this->set("_serialize", array(
                    "status",
                    "message",
                    "page_count",
                    "content"
                ));
                

            // fin vista json

            }
			
					        /**
         * @secured(CONSULTA_NOTA)
         */
        public function existe_comprobante()
            {
            parent::existe_comprobante();
            }


       

        /**
         * @secured(ADD_NOTA)
         */
        public

        function add()
            {
            parent::add();
            }

        /**
         * @secured(MODIFICACION_NOTA)
         */
        public

        function edit($id)
            {
            parent::edit($id);
            return;
            }

        /**
         * @secured(BAJA_NOTA)
         */
        public

        function delete($id)
            {
            parent::delete($id);
            }

        /**
         * @secured(BAJA_NOTA)
         */
        public

        function deleteItem($id,$externo=1)
            {
            parent::deleteItem($id,$externo);
            }

        /**
         * @secured(CONSULTA_NOTA)
         */
        public

        function getModel($vista = 'default')
            {
            $model = parent::getModelCamposDefault(); //esta en APPCONTROLLER
            $model = $this->editforView($model, $vista); //esta funcion edita y agrega campos para la vista, debe estar LOCAL
            }

        /**
         * @secured(REPORTE_NOTA)
         */
        public

        function pdfExport($id, $vista = '')
            {
            $this->loadModel("Comprobante");
            $this->loadModel("ComprobantePuntoVentaNumero");
            
            $nota = $this->Comprobante->find('first', array(
                'conditions' => array(
                    'Comprobante.id' => $id
                ) ,
                'contain' => array(
                    'TipoComprobante'
                )
            ));
            
            $comprobante_punto_venta_numero = $this->ComprobantePuntoVentaNumero->find('first', array(
            		'conditions' => array(
            				'ComprobantePuntoVentaNumero.id_punto_venta' => $nota["Comprobante"]["id_punto_venta"],
            				'ComprobantePuntoVentaNumero.id_tipo_comprobante' => $nota["Comprobante"]["id_tipo_comprobante"]
            		) ,
            		'contain' => false
            ));
            
            $comprobantes_relacionados = $this->getComprobantesRelacionados($id);
            
            if($nota && $comprobante_punto_venta_numero["ComprobantePuntoVentaNumero"]["id_tipo_comprobante_punto_venta_numero"]!=EnumTipoComprobantePuntoVentaNumero::PropioOfflinePreimpreso){
            	
                $facturas_relacionados = array();
                foreach($comprobantes_relacionados as $comprobante)
                    {
                    if (isset($comprobante["Comprobante"]) && in_array($comprobante["Comprobante"]["id_tipo_comprobante"],
                    
                    $this->Comprobante->getTiposComprobanteController(EnumController::Facturas)
                    
                    )) array_push($facturas_relacionados, $this->Comprobante->GetNumberComprobante($comprobante["PuntoVenta"]["numero"], $comprobante["Comprobante"]["nro_comprobante"]));
                    }

                $this->set('facturas_relacionados', $facturas_relacionados);
                $letra = $nota["TipoComprobante"]["letra"];
                //$this->view = 'Notas';
                $this->viewPath = 'Notas';// Para cambiarle la ruta a la view, esta en otra carpeta, le fuerzo
                $vista = "nota" . $letra;
                parent::pdfExport($id, $vista); //envio la vista con la letra que corresponde A o B
                }
              else
                {
                	
                	$output = array(
                			"status" =>EnumError::ERROR,
                			"message" => "Error: No puede generar el PDF de la Nota ya que es un comprobante preimpreso.",
                			"content" => ""
                	);
                	echo json_encode($output);
                	die();
                }
            }

        private
        function editforView($model, $vista)
            { //esta funcion recibe el model y pone los campos que se van a ver en la grilla
            $model = parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER

            // $model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);

            $this->set('model', $model);
            Configure::write('debug', 0);
            $this->render($vista);
            }

        public

        function ObtenerCae($id_comprobante, $id_punto_venta, $recursivo = 0)
            {
            	$resultado = parent::ObtenerCae($id_comprobante,$id_punto_venta,$recursivo);
            	return $resultado;
            }

        public

        function CalcularImpuestos($id_comprobante, $id_tipo_impuesto = '', $error = 0, $message = '')
            {
            $id_tipo_impuesto = EnumTipoImpuesto::PERCEPCIONES;
            return parent::CalcularImpuestos($id_comprobante, $id_tipo_impuesto);
            }

        /**
         * @secured(MODIFICACION_NOTA)
         */
        public

        function Anular($id_comprobante)
            {
            $this->loadModel("Comprobante");
            $this->loadModel("Asiento");
            $output_asiento_revertir = array("id_asiento"=>0);
            $factura = $this->Comprobante->find('first', array(
                'conditions' => array(
                    'Comprobante.id' => $id_comprobante,
                    'Comprobante.id_tipo_comprobante' => $this->id_tipo_comprobante
                ) ,
                'contain' => array(
                    'ComprobanteValor',
                    'Asiento',
                    'ChequeEntrada'
                )
            ));
            $ds = $this->Comprobante->getdatasource();
            if ($this->Comprobante->getEstadoGrabado($id_comprobante) != EnumEstadoComprobante::Anulado && count($this->getRecibosRelacionados($id_comprobante)) == 0 && $factura && !$factura["Comprobante"]["cae"] > 0)
                { //si la nota se puede anular

                // TODO: si tiene stock lo revierto

                try
                    {
                    $ds->begin();

                    // le clavo el estado ANULADO

                    $this->Comprobante->updateAll(array(
                        'Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,
                        'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'" ,
                        'Comprobante.definitivo' => 1
                    ) , array(
                        'Comprobante.id' => $id_comprobante
                    ));
                    
                    
                  
                    
                    $output_asiento_revertir = array("id_asiento"=>0);
                    if($factura["Comprobante"]["comprobante_genera_asiento"] == 1)
                        $output_asiento_revertir = $this->Asiento->revertir($factura["Asiento"]["id"]); /*Se debe revertir el asiento del comprobante*/
                    
                        
                        
                    $factura = $this->Comprobante->find('first', array(
                        		'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                        		'contain' => false
                        ));
                        
                    $this->Auditar($id_comprobante,$factura);
                        
                        
                    
                    $ds->commit();
                    $tipo = EnumError::SUCCESS;
                    $mensaje = "El Comprobante se anulo correctamente ";
                    
                    
                    
                    
                    }

                catch(Exception $e)
                    {
                    $ds->rollback();
                    $tipo = EnumError::ERROR;
                    $mensaje = "No es posible anular el Comprobante ERROR:" . $e->getMessage();
                    }
                }
              else
                {
                $tipo = EnumError::ERROR;
                $mensaje = "No es posible anular el Comprobante ya que NO se encuentra ABIERTO o ya ha sido ANULADO o esta presente en algun RECIBO" . "\n";
                $recibos_relacionados = $this->getRecibosRelacionados($id_comprobante);
                if (count($recibos_relacionados) > 0)
                    {
                    foreach($recibos_relacionados as $recibo)
                        {
                        $mensaje.= "&bull; Recibo Nro: " . $recibo["Comprobante"]["nro_comprobante"] . "\n";
                        }
                    }
                }

            $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                "id_asiento" => $output_asiento_revertir["id_asiento"],
            );
            echo json_encode($output);
            die();
            }

        /**
         * @secured(CONSULTA_NOTA)
         */
        public

        function getComprobantesRelacionadosExterno($id_comprobante, $id_tipo_comprobante = 0, $generados = 1)
            {
            parent::getComprobantesRelacionadosExterno($id_comprobante, $id_tipo_comprobante, $generados);
            }
            
            
            /**
             * @secured(BTN_EXCEL_NOTAS)
             */
            public function excelExport($vista="default",$metodo="index",$titulo=""){
            	
            	parent::excelExport($vista,$metodo,"Listado de Notas de Venta");
            	
            	
            	
            }
            
            
            
            public function generaAsiento($id_comprobante,&$message){
            	
            	$this->loadModel("ComprobantePuntoVentaNumero");
            	
            	$comprobante_punto_venta_numero = $this->ComprobantePuntoVentaNumero->find('first', array(
            			'conditions' => array(
            					'ComprobantePuntoVentaNumero.id_punto_venta' => $this->request->data["Comprobante"]["id_punto_venta"],
            					'ComprobantePuntoVentaNumero.id_tipo_comprobante' => $this->request->data["Comprobante"]["id_tipo_comprobante"]
            			) ,
            			'contain' => false
            	));
            	
            	
            	if($comprobante_punto_venta_numero && $comprobante_punto_venta_numero["ComprobantePuntoVentaNumero"]["id_tipo_comprobante_punto_venta_numero"] == EnumTipoComprobantePuntoVentaNumero::PropioNumeradorSistemaEDITABLE||
            			$comprobante_punto_venta_numero["ComprobantePuntoVentaNumero"]["id_tipo_comprobante_punto_venta_numero"] == EnumTipoComprobantePuntoVentaNumero::PropioElectronicoOfflineExportacion
            			){
            				parent::generaAsiento($id_comprobante, $message);
            				return;
            	}//solo genero asiento en estos casos, sino se genera cuando se obtiene el CAE
            	
            }
            
          
            
        }

?>