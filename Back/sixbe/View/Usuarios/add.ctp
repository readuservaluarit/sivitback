<?php

     App::import('Lib', 'FormBuilder');
     App::import('Lib/EntityPickers', 'EntityPickerHelper');
     App::import('Lib/EntityPickers', 'FuncionPicker');
     
     $formBuilder = new FormBuilder();
     
     //Configuracion del Formulario
     $formBuilder->setController($this);
     $formBuilder->setModelName('Usuario');
     $formBuilder->setControllerName('Usuarios');
     $formBuilder->setTitulo('Usuarios');
     $formBuilder->setTituloForm('Alta de Usuario');
     
     //Form
     $form = $this->Form->create('Usuario',  array('class' => 'block-content form'));
     $formBuilder->setForm($form);
     
     //Fields
     $field = $this->Form->input('username', array('class' => 'required', 'label' => false));
     $formBuilder->addFormField($field, 'Código de Usuario', '_100');
     
     $field = $this->Form->input('password', array('class' => 'required', 'label' => false));
     $formBuilder->addFormField($field, 'Contraseña', '_100');
     
     $field = $this->Form->input('apellido_nombres', array('class' => 'required', 'label' => false));
     $formBuilder->addFormField($field, 'Apellido y Nombres', '_100');
     
     $field = $this->Form->input('email', array('class' => 'required', 'label' => false));
     $formBuilder->addFormField($field, 'e-mail', '_100');
     
     $field = $this->Form->input('id_rol', array('options' => $roles, 'label' => false));
     $formBuilder->addFormField($field, 'Rol', '_100');
     
     //Obtengo el formulario en modo alta
     $formBuilder->getForm();
     
?>