<?php

  
        App::uses('OrdenProduccion', 'Model');  
        $comprobante_obj = new OrdenProduccion();

        
       $propiedades = array("show_grid"=>"1","header_display" => "Nro. Op","minimun_width"=> "10","order" =>"1","text_colour"=>"#000000" );
        $comprobante_obj->setFieldView("id",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Cod.Comp","minimun_width"=> "10","order" =>"2","text_colour"=>"#000000","type" => EnumTipoDato::cadena );
        $comprobante_obj->setFieldView("codigo_componente",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Desc.Comp","minimun_width"=> "10","order" =>"3","text_colour"=>"#000000","type" => EnumTipoDato::cadena );
        $comprobante_obj->setFieldView("descripcion_componente",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Fecha Emisi&oacute;n","minimun_width"=> "10","order" =>"4","text_colour"=>"#000000" );
        $comprobante_obj->setFieldView("fecha_emision",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Fecha Cierre","minimun_width"=> "10","order" =>"5","text_colour"=>"#000000" );
        $comprobante_obj->setFieldView("fecha_cierre",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Cant.Comp.Retirado","minimun_width"=> "10","order" =>"6","text_colour"=>"#000000","type" => EnumTipoDato::decimal_flotante );
        $comprobante_obj->setFieldView("c_retirado",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Cant.Comp.Terminado","minimun_width"=> "10","order" =>"7","text_colour"=>"#000000","type" => EnumTipoDato::decimal_flotante );
        $comprobante_obj->setFieldView("c_terminado",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Cant.Mat.Prima","minimun_width"=> "10","order" =>"8","text_colour"=>"#000000","type" => EnumTipoDato::decimal_flotante );
        $comprobante_obj->setFieldView("mp_cantidad",$propiedades,$model);
        
        
        $propiedades = array("show_grid"=>"1","header_display" => "Cant.Mat.Prima.Retirado","minimun_width"=> "10","order" =>"9","text_colour"=>"#000000","type" => EnumTipoDato::decimal_flotante );
        $comprobante_obj->setFieldView("mp_retirado",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Cant.Mat.Prima.Terminado","minimun_width"=> "10","order" =>"10","text_colour"=>"#000000","type" => EnumTipoDato::decimal_flotante );
        $comprobante_obj->setFieldView("mp_terminado",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Estado","minimun_width"=> "10","order" =>"11","text_colour"=>"#000000","type" => EnumTipoDato::cadena );
        $comprobante_obj->setFieldView("d_estado",$propiedades,$model);
        
        
        
        
        
         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "OrdenProduccion",
                    "content" => $model
                );
        
          echo json_encode($output);
?>