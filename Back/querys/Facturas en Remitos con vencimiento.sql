SELECT  comprobante.`nro_comprobante` AS recibo,item_recibo.`nro_comprobante` AS nro_factura,item_recibo.`fecha_contable` AS fecha_factura,item_recibo.`fecha_vencimiento` AS vencimiento_factura,

item_recibo.`fecha_contable` fecha_recibo,
item_recibo.`razon_social`,item_recibo.`total_comprobante`,

condicion_pago.`d_condicion_pago`,persona_categoria.`d_persona_categoria`


 FROM comprobante_item


JOIN comprobante ON comprobante_item.`id_comprobante` = comprobante.id 
JOIN comprobante  item_recibo ON item_recibo.id = comprobante_item.`id_comprobante_origen`

LEFT JOIN persona ON persona.id = item_recibo.`id_persona`
LEFT JOIN condicion_pago  ON condicion_pago.id = persona.`id_condicion_pago`
LEFT JOIN persona_categoria ON persona_categoria.id = persona.`id_persona_categoria`


WHERE comprobante.id_tipo_comprobante = 208

AND item_recibo.id_tipo_comprobante = 801

AND YEAR(item_recibo.`fecha_contable`) >2018


LIMIT 1000000000
