<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class TarjetaCredito extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'tarjeta_credito';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
        /*'d_cuenta_contable' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar una descripci&oacute;n v&aacute;lida',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 public $belongsTo = array(

        'CuentaBancaria' => array(
            'className' => 'CuentaBancaria',
            'foreignKey' => 'id_cuenta_bancaria',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'TipoTarjetaCredito' => array(
            'className' => 'TipoTarjetaCredito',
            'foreignKey' => 'id_tipo_tarjeta_credito',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
 		'CuentaContable' => array(
 				'className' => 'CuentaContable',
 				'foreignKey' => 'id_cuenta_contable',
 				'dependent' => false,
 				'conditions' => '',
 				'fields' => '',
 				
 		),
 		'Moneda' => array(
 				'className' => 'Moneda',
 				'foreignKey' => 'id_moneda',
 				'dependent' => false,
 				'conditions' => '',
 				'fields' => '',
 				
 		),
        
);



public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     "d_banco_sucursal"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_tipo_cuenta_bancaria"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_moneda"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_propia"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_tipo_tarjeta_credito"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "codigo_cuenta_contable"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_cuenta_contable"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_moneda"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "valor_moneda"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_simbolo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_abreviacion_afip"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_simbolo_internacional"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_cuenta_bancaria"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),

    
     
    
    );
    
}


}
