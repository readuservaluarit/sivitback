<?php

App::uses('PersonasController', 'Controller');

class ClientesController extends PersonasController { 

    public $name = 'Clientes';
    public $model = 'Persona';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $id_tipo_persona = EnumTipoPersona::Cliente;
  
  
    /**
    * @secured(CONSULTA_CLIENTE)
    */
    public function index() {
      
        parent::index(); 
        return;

    }
    
    
    
    /**
    * @secured(MODIFICACION_CLIENTE)
    */
    public function abm($mode, $id = null) {
        
        parent::abm($mode,$id);
        
    }

    /**
    * @secured(ABM_CLIENTE)
    */
    public function view($id) {        
        parent::view($id);
        return;
    }

    /**
    * @secured(ADD_CLIENTE)
    */
    public function add() {
        
        
        
       $this->request->data["Persona"]["id_tipo_persona"] = $this->id_tipo_persona;
        parent::add(); 
        return;
        
      
    }
   
   
   /**
    * @secured(MODIFICACION_CLIENTE)
    */ 
   public function edit($id) {
   	
   	
   	
   	try{
   		
       $this->loadModel("Persona");
       $this->loadModel("Comprobante");
       
       
       $cliente = $this->Persona->find('first', array(
       		'conditions' => array('Persona.id' => $id),
       		'contain' =>false
       )); 
   	    
       
       
   	    $cuenta_corriente_grabado = $cliente["Persona"]["cuenta_corriente"];
   	    
   	    
   	    if(isset($this->request->data["Persona"]["cuenta_corriente"]))
   	    	$cuenta_corriente_nuevo_valor =  $this->request->data["Persona"]["cuenta_corriente"];
   	    
   	    
   	    	if( isset($cuenta_corriente_nuevo_valor) && $cuenta_corriente_grabado!=0 && $cuenta_corriente_grabado != $cuenta_corriente_nuevo_valor ){
   	    	
   	    	
   	    	
   	    	$datoEmpresa = $this->Session->read('Empresa');
   	    	
   	    	
   	    	$moneda_base = $datoEmpresa["DatoEmpresa"]["id_moneda"];
   	    	
   	    	$saldo = $this->Comprobante->getComprobantesImpagos($this->request->data["Persona"]["id"], 1,0,1,EnumSistema::VENTAS,0,$moneda_base,0,9999999999999999999999,0,0,0);
   	    	
   	    	if($saldo >0){
   	    		
   	    		
   	    		$mensaje = "Si usted quiere cancelarle la cuenta corriente a el Cliente primero debe cancelar los comprobantes impagos.";
   	    		$status = EnumError::ERROR;   
   	    		
   	    		$output = array(
   	    				"status" => $status,
   	    				"message" => $mensaje,
   	    				"content" => ""
   	    		);
   	    		
   	    		$this->set($output);
   	    		$this->set("_serialize", array("status", "message", "content"));
   	    		return;
   	    		
   	    	}
   	    	
   	    	
   	    }else{
   	    
   	    	$this->request->data["Persona"]["id_tipo_persona"] = $this->id_tipo_persona;
        
        	parent::edit($id);  
        
   	    }
   	    
   	}catch(Exception $e){
   		
   		
   		$output = array(
   				"status" =>EnumError::ERROR,
   				"message" => "ERROR: ".$e->getMessage(),
   				"content" => ""
   		);
   		
   		$this->set($output);
   		$this->set("_serialize", array("status", "message", "content"));

   	}
        
        return;    
  }
    
  
    /**
    * @secured(BAJA_CLIENTE)
    */
    function delete($id) {
        
        parent::delete($id);
        return;
        
    }
    
    
         /**
        * @secured(CONSULTA_CLIENTE)
        */
        public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
            //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
            $this->set('model',$model);
            
            Configure::write('debug',0);
            $this->render($vista);
            
            
                
    
     
        }
        
    
    public function home(){
        parent::home();
    }
    
  
    /**
    * @secured(CONSULTA_CLIENTE)
    */
    public function existe_cuit(){
        
       parent::existe_cuit();
       return;
    }
    
    
     /**
    * @secured(CONSULTA_CLIENTE)
    */
    public function existe_codigo($codigo='',$id_persona='',$llamada_interna=0){
        
       
       $dato = parent::existe_codigo($codigo,$id_persona,$llamada_interna);
       if($llamada_interna == 1)
        return $dato;
       else
        return;
    }
    
    /**
     * @secured(BTN_EXCEL_CLIENTES)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Clientes");
    	
    	
    	
    }
    
    
    /**
     * @secured(CONSULTA_CLIENTE)
     */
    public function getDomicilio(){
    	
    	
     parent::getDomicilio();
     return ;
    	
    }
    
    /**
     * @secured(CONSULTA_CLIENTE)
     */
    public function getDataCuitFromAfip(){
    	
    	
    	parent::getDataCuitFromAfip();
    	return ;
    	
    }
    
    public function formula(){
    
    App::import('Vendor', 'EvalMath', array('file' => 'Classes/evalmath.class.php'));
    $m = new EvalMath();
    $m->suppress_errors = true;
    if ($m->evaluate('y(x) = ((1+900)/3)*5')) {
    	
    	echo $m->e("y(0);");
    	die();
    
    
    
    }
    
    }
    
    
    
    
}
?>