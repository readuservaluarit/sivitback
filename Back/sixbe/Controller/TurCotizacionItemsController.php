<?php

App::uses('ComprobanteItemsController', 'Controller');

class TurCotizacionItemsController extends ComprobanteItemsController {
    
    public $name = 'TurCotizacionItems';
    public $model = 'ComprobanteItem';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $id_tipo_comprobante = EnumTipoComprobante::TurCotizacion; //NOTA DE PEDIDO / ORDEN DE VENT
    
  
  
     /**
    * @secured(CONSULTA_TUR_COTIZACION)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model);
        
        
         if($this->getFromRequestOrSession('Comprobante.id_tipo_comprobante') == '')
                array_push($conditions, array('Comprobante.id_tipo_comprobante ' => $this->id_tipo_comprobante
                 ));  
        
        
     
        /*
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        */
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
    
    
        
        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'cotizacion.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'cotizacion.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
    
    
        //$this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        
        $data = $this->ComprobanteItem->find('all', array(
                            'conditions' => $conditions,
                            'contain' =>array("Producto","Comprobante"),
                            'order' => array("ComprobanteItem.n_item asc")
                           
                        ));
                        
                        
        //$page_count = $this->params['paging'][$this->model]['pageCount'];
        $page_count = 0;
        
        foreach($data as &$producto ){
                 
                 $producto["ComprobanteItem"]["d_producto"] = $producto["Producto"]["d_producto"];
                 $producto["ComprobanteItem"]["codigo"] = $this->ComprobanteItem->getCodigoFromProducto($producto);
                 
                 //$producto["ComprobanteItem"]["total"] = (string) $this->{$this->model}->getTotalItem($producto);
                 
                 unset($producto["Producto"]);
                 unset($producto["Comprobante"]);
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    
     /**
    * @secured(CONSULTA_TUR_COTIZACION)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);
        
        
        
        
    }
    

    
   
    
}
?>