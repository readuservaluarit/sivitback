<?php




class CuentasCorrienteController extends AppController {
    public $name = 'CuentasCorriente';
    public $model = 'Comprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
  
  
    protected function index($id_tipo_persona) {
      
    	App::import('Model', 'Persona');
    	$this->loadModel("Comprobante");
    	$this->loadModel("Moneda");
    	$this->loadModel("DatoEmpresa");
       
       if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
       //Recuperacion de Filtros
        $id_persona = strtolower($this->getFromRequestOrSession('Comprobante.id_persona'));
        
        if($this->Auth->user('id_persona')>0){
        	
        	$id_persona = $this->Auth->user('id_persona');
        	
        }

        $id_moneda = strtolower($this->getFromRequestOrSession('Comprobante.id_moneda_expresion'));
        //$id_moneda = 2;
        
        $id_punto_venta = strtolower($this->getFromRequestOrSession('Comprobante.id_punto_venta'));
        
        $d_punto_venta= strtolower($this->getFromRequestOrSession('Comprobante.d_punto_venta'));
        
        $dias_vencidos = strtolower($this->getFromRequestOrSession('Comprobante.dias_vencidos'));
        
        $nro_comprobante = strtolower($this->getFromRequestOrSession('Comprobante.nro_comprobante'));
        
        $id_tipo_comprobante = strtolower($this->getFromRequestOrSession('Comprobante.id_tipo_comprobante'));
        
        
        
        $chk_solo_con_saldo = strtolower($this->getFromRequestOrSession('Comprobante.chk_solo_con_saldo'));
        
        
       
        $fecha_vencimiento = strtolower($this->getFromRequestOrSession('Comprobante.fecha_vencimiento'));
        $fecha_vencimiento_hasta = strtolower($this->getFromRequestOrSession('Comprobante.fecha_vencimiento_hasta'));
        
        
        
        $fecha_contable = strtolower($this->getFromRequestOrSession('Comprobante.fecha_contable'));
        $fecha_contable_hasta = strtolower($this->getFromRequestOrSession('Comprobante.fecha_contable_hasta'));
        
        $array_id_tipo_comprobante = $this->getFromRequestOrSession('TipoComprobante.listado_id_tipo_comprobante');//recibo e
        
  
        
       
        $conditions = array();
         
        if($id_punto_venta!="")
            array_push($conditions, array('Comprobante.id_punto_venta' => $id_punto_venta));
        
        if($nro_comprobante!=""){
        	$nros_comprobante = explode(',', $nro_comprobante);
        	array_push($conditions, array('Comprobante.nro_comprobante' => $nro_comprobante));
        }
        
        if($id_tipo_comprobante!="")
            array_push($conditions, array('Comprobante.id_tipo_comprobante ' => $id_tipo_comprobante));
            
            
 
            
        if($fecha_vencimiento!=""){
            	array_push($conditions, array('Comprobante.fecha_vencimiento >=' => $fecha_vencimiento)); //MP: Si la fecha de bd tiene hh:mm:ss distintos de 00:00:00 la compoaracion no anda
            	array_push($this->array_filter_names,array("Fecha Generaci&oacute;n Dsd.:"=>$this->{$this->model}->formatDate($fecha_vencimiento)));
            	
        }
            
            
       if($fecha_vencimiento_hasta!=""){
            	array_push($conditions, array('Comprobante.fecha_vencimiento <=' => $fecha_vencimiento_hasta)); //MP: Si la fecha de bd tiene hh:mm:ss distintos de 00:00:00 la compoaracion no anda
            	array_push($this->array_filter_names,array("Fecha Generaci&oacute;n Hta.:"=>$this->{$this->model}->formatDate($fecha_vencimiento_hasta)));
       }	
            
        
            
        if($id_persona!="")
            array_push($conditions, array('Comprobante.id_persona'=> $id_persona));    
            
            
        if($d_punto_venta!="")
        	array_push($conditions, array('Comprobante.d_punto_venta' => $d_punto_venta));
            
        	
        if($array_id_tipo_comprobante !='')
        	array_push($conditions, array('Comprobante.id_tipo_comprobante' => explode(",", $array_id_tipo_comprobante)));
        
        	
        if($fecha_contable!=""){
        		array_push($conditions, array('Comprobante.fecha_contable >= ' =>  $fecha_contable));
        		array_push($this->array_filter_names,array("Fecha Contable Dsd:"=>$this->{$this->model}->formatDate($fecha_contable)));
        }
        	
        if($fecha_contable_hasta!=""){
        		array_push($conditions, array('Comprobante.fecha_contable <= ' => $fecha_contable_hasta));
        		array_push($this->array_filter_names,array("Fecha Contable Hta.:"=>$this->{$this->model}->formatDate($fecha_contable_hasta)));
        }
        		
 
         array_push($conditions,array("NOT"=> array('Comprobante.id_persona' => array(EnumPersonaBase::ClienteNoRegistrado,EnumPersonaBase::ProveedorNoRegistrado ))));
        
        


         
         
         $id_moneda= $this->getFromRequestOrSession('Comprobante.id_moneda_expresion');
         
         $id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda);
         //$id_tipo_moneda =  EnumTipoMoneda::Corriente;
         
         $persona = New Persona();
         $resultados = $persona->getListadoCuentaCorriente($id_persona,$this->id_tipo_persona,$conditions);
         $saldo_total = 0;
         $persona = '';
         $datoEmpresa = $this->Session->read('Empresa');
         $id_moneda_base  = $datoEmpresa["DatoEmpresa"]["id_moneda"];
         $id_moneda_secundaria  = $datoEmpresa["DatoEmpresa"]["id_moneda2"];
         $campo_moneda = $this->Moneda->getFieldByTipoMoneda($id_tipo_moneda);
         $limite_cuenta_corriente = 0;
         
         $comprobante_obj = New Comprobante();
         
         $recibos = $comprobante_obj->getTiposComprobanteController(EnumController::Recibos);
         $ordenes_pago = $comprobante_obj->getTiposComprobanteController(EnumController::OrdenesPago);
         
         foreach($resultados as $key=>&$comprobante){
           
           
         	$limite_credito = $comprobante["Persona"]["limite_credito"];
           
           $comprobante["Comprobante"]["d_tipo_comprobante"]= $comprobante["TipoComprobante"]["codigo_tipo_comprobante"];
           
           if($comprobante["PuntoVenta"]["numero"]>0) 
            $comprobante["Comprobante"]["pto_nro_comprobante"] = $this->Comprobante->GetNumberComprobante($comprobante["PuntoVenta"]["numero"],$comprobante["Comprobante"]["nro_comprobante"]); 
           else
            $comprobante["Comprobante"]["pto_nro_comprobante"] = $this->Comprobante->GetNumberComprobante($comprobante["Comprobante"]["d_punto_venta"],$comprobante["Comprobante"]["nro_comprobante"]);           
                 
                        
            $comprobante["Comprobante"]["d_moneda"] = $comprobante["Moneda"]["simbolo_internacional"];
            
            
            
            if( $comprobante["TipoComprobante"]["signo_comercial"]<>0)
           		$saldo = $this->Comprobante->getComprobantesImpagos($comprobante["Comprobante"]["id_persona"],0,$comprobante["Comprobante"]["id"],1,$this->id_sistema,0,$id_moneda);
            else
            	$saldo = 0;
          
          
           //$saldo = $this->Comprobante->ExpresarEnTipoMonedaMejorado($saldo,$this->Moneda->getFieldByTipoMoneda($id_tipo_moneda),$comprobante["Comprobante"]); 
           $comprobante["Comprobante"]["saldo"] = $saldo;
           
        

           
           $total_comprobante = $this->Comprobante->ExpresarEnTipoMonedaMejorado($comprobante["Comprobante"]["total_comprobante"],$campo_moneda,$comprobante["Comprobante"]);
           
           $comprobante["Comprobante"]["valor_moneda2"] = (string) round(1/$comprobante["Comprobante"]["valor_moneda2"],2);
             
           if($comprobante["TipoComprobante"]["signo_comercial"] == -1 || $comprobante["Comprobante"]["total_comprobante"]<0  ) {  //significa que Resta dinero Ejemplo NC
             $comprobante["Comprobante"]["d_total_comprobante"] =   " ".$comprobante["TipoComprobante"]["signo_comercial"]*$total_comprobante."";
             $comprobante["Comprobante"]["d_saldo"] = " ".$saldo." ";
             $saldo_aux = $saldo;
           }else{
              $comprobante["Comprobante"]["d_total_comprobante"] = " ".$total_comprobante."";
              $comprobante["Comprobante"]["d_saldo"] = " ".$saldo."";
              $saldo_aux = $saldo;
           }
           
           
           
       

           if($chk_solo_con_saldo == 1 && $saldo == 0){
          		unset($resultados[$key]); 
            }else{
                
                $saldo_total = $saldo_total + $saldo_aux;
            } 
           
            /*
           if($saldo !=0)
           		$saldo_total = $saldo_total + $saldo_aux;
           
           */

           unset($comprobante["TipoComprobante"]);
           
     
        
          unset($comprobante["PuntoVenta"]);
          unset($comprobante["Persona"]);
          $this->{$this->model}->formatearFechas($comprobante);
          
         }
         
         
         /*si tengo resultados agrego el result content*/
         if(count($resultados)>0){
         	
         	
         	$moneda_base["id_moneda"] = $id_moneda_base; //simula un comprobante que tiene solo el campo id_moneda como el limite_credito esta siempre en moneda base
         	
         	$this->loadModel("Moneda");
         	$moneda = New Moneda();
         	$moneda_base["valor_moneda2"] = $moneda->getCotizacion($id_moneda_secundaria); //simula un comprobante que tiene solo el campo id_moneda como el limite_credito esta siempre en moneda base
         	unset($moneda);
         	
         	$limite_credito = $this->Comprobante->ExpresarEnTipoMoneda($id_tipo_moneda,$limite_credito,$moneda_base,$id_moneda_base);
         	$persona["ResultadosComprobante"]["total_saldo"] =(string) $saldo_total;//total de los comprobantes
         	$persona["ResultadosComprobante"]["limite_credito_actual"] = (string) ($limite_credito - $saldo_total);//total de los comprobantes
         	$persona["ResultadosComprobante"]["limite_cuenta_corriente"] = (string) $limite_credito;//total de los comprobantes
         	
         }
         
         //$resultados["ResultadosComprobante"] = $persona;
         
        
         
         $array_comprobantes = array();
         
         
         foreach($resultados as $key=>&$comprobante){
           array_push($array_comprobantes,$comprobante);
         }
          
         $this->data =  $array_comprobantes;
         
         

         
         
         $seteo_form_builder = array("showActionColumn" =>true,"showNewButton" =>false,"showActionColumn"=>false,"showFooter"=>true,"showHeaderListado"=>true,"showXlsButton"=>false);
         $this->title_form = "Cuenta Corriente";
         $this->sub_title_form = "Saldo: ARS ".$persona["ResultadosComprobante"]["total_saldo"];
         $this->preparaHtmLFormBuilder($this,$seteo_form_builder,"");
         
         
          $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $array_comprobantes,
            "resultset_content"=>$persona,
            "page_count" =>0
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content","resultset_content"));
    
        
    }
    
    

    
    
    protected function getTituloReporte(){
        
        $this->loadModel("Persona");
        $id_persona = strtolower($this->getFromRequestOrSession('Comprobante.id_persona'));
        $razon_social = $this->Persona->getRazonSocial($id_persona);
        return "Cuenta Corriente - ".$razon_social;
         
        
        
    }
    
    
    
 

   
   
   
    
   
    
    
    

    
    
    



   
    
    
    
    
    
    
  
   
    
}
?>