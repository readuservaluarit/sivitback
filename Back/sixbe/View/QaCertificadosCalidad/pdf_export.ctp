<?php

/**
 * PDF exportación de Orden Compra
 */

App::import('Vendor','tcpdf/tcpdf'); 

// create new PDF document
class MYPDF extends TCPDF 
{
     public $id;
     public $name;
     
     public function setid($id){
        $this->id = $id;
     }
     
     public function getid(){
        return $this->id;
     }
     
     public function setName($name){
        $this->name = $name;
     }
     
     public function getName(){
        return $this->name;
     }


    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number

        
        $this->Cell(0, 10, 'F69- Certificado de calidad'.$this->getName().' Nro '.$this->getid().'                                           Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages() .'                                               Rev. 02- Abril 2016 ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

ob_clean();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistema Siv');
$pdf->SetTitle('Certificado de Calidad');
$pdf->SetSubject('Certificado de Calidad');
//$pdf->SetKeywords('');



// set default header data
$logo = '/img/header_calidad.jpg';
$title = "";
$subtitle = ""; 
$pdf->SetHeaderData($logo, 190, $title, $subtitle, array(0,0,0), array(0,104,128));
$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 50, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(10);
$pdf->SetFooterMargin(20);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
/*$lg = Array();
$lg['a_meta_charset'] = 'ISO-8859-1';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';
*/
$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set default font subsetting mode
//$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
/*
$html = <<<EOD
<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
EOD;
*/

$tbody = "";

$renglones = $datos_pdf["QaCertificadoCalidadItem"];
$pdf->setid( $datos_pdf["QaCertificadoCalidad"]["id"]);

$data = array();



   $tbody.='<table width="100%" border="1" cellspacing="1" cellpadding="1" style="font-size:11px">
      <tr>
        <th  width="20%"  colspan="2" scope="col">Ensayos Finales</th>
        <th width="10%"  scope="col">Fluido</th>
        <th width="10%"  scope="col">Presion (Kg/cm2)</th>
        <th width="20%" scope="col">Temperatura (Gr C)</th>
        <th width="20%" scope="col">Duracion (minutos)</th>
        <th width="20%" scope="col">Cantidad Ensayada</th>
      </tr>
  <tr style="font-size:10px">
    <td width="10%"><strong>Cuerpo</strong></td>
    <td width="10%"><strong>Hidrostático</strong></td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["f1"].'</td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["p1"].'</td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["t1"].'</td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["tmin1"].'</td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["cantidad1"].'</td>
  </tr>
  <tr style="font-size:10px">
    <td width="10%" rowspan="2"><strong>Cierre</strong></td>
    <td width="10%"><strong>Hidrostático</strong></td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["f2"].'</td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["p2"].'</td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["t2"].'</td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["tmin2"].'</td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["cantidad2"].'</td>
  </tr>
  <tr style="font-size:10px">
    <td width="10%"><strong>Neumatico</strong></td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["f3"].'</td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["p3"].'</td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["t3"].'</td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["tmin3"].'</td>
     <td>'.$datos_pdf["QaCertificadoCalidad"]["cantidad3"].'</td>
  </tr>
  
  <tr style="font-size:10px">
    <td width="10%" rowspan="2"><strong>Cuerpo</strong></td>
    <td><strong>Nitr&oacute;geno</strong></td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["f4"].'</td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["p4"].'</td>
    <td>'.$datos_pdf["QaCertificadoCalidad"]["t4"].'</td>
     <td>'.$datos_pdf["QaCertificadoCalidad"]["tmin4"].'</td>
     <td>'.$datos_pdf["QaCertificadoCalidad"]["cantidad4"].'</td>
  </tr>
  
 
</table>

<table width="100%" border="1" border="1" cellspacing="1" cellpadding="1" style="font-size:10px">
  <tr>
    <td width="25%"><strong>Doble bloqueo y venteo</strong></td>
    <td width="75%" align="left" valign="middle">'.$datos_pdf["QaCertificadoCalidad"]["f5"].'</td>
  </tr>
</table>


';

//agrego las observaciones

   

$html='';

$fecha = new DateTime($datos_pdf["QaCertificadoCalidad"]["fecha"]);
$html .=' 
    <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

            <tr>
                 <th class="sorting" rowspan="1" colspan="1" style="width:50%;font-size:15px;"><strong> CERTIFICADO DE CALIDAD NRO '.$id.'</strong></th>
                <th  class="sorting" rowspan="1" colspan="1"  style="width:50%;"  align="right"> Fecha: '.$fecha->format('d-m-Y').' </th>
            </tr>
     <tbody>
        <tr >
        
        <td width="33%"  align="left"><strong> Numero de Ensayo:</strong> '.$datos_pdf["OrdenArmado"]["nro_comprobante"].'</td>
        <td width="33%" align="center" ><strong> Numero de Serie:</strong> '.$datos_pdf["OrdenArmado"]["nro_comprobante"].' </td>
        <td width="34%" align="right" ><strong>Numero de Remito:</strong>'.$datos_pdf["Persona"]["nro_comprobante"].'</td>
         </tr>
         <tr >
        <td  colspan="3" ><HR> </td>
      
         </tr>
        <tr >
        <td  colspan="2"  align="left"><strong> Sres:</strong> '.$datos_pdf["Persona"]["razon_social"].' </td>
        <td  colspan="1" align="right" ><strong> Cuit:</strong> '.$datos_pdf["Persona"]["cuit"].' </td>
         </tr>
         
           <tr >
        <td  colspan="3" ><HR> </td>
      
         </tr>
         <tr >
        <td  colspan="1" ><strong> Código:</strong> '.$datos_pdf["Producto"]["codigo"].'</td>
        <td  colspan="2" ><strong> Descripcion:</strong>'.$datos_pdf["Producto"]["d_producto"].'</td>
         </tr>                                                                                                                                                                                     
         <tr >
        <td  colspan="3" ><strong>Cantidad:</strong>'.$datos_pdf["QaCertificadoCalidad"]["cantidad"].'</td>
     
         </tr>
        <tr >
        <td  colspan="3" ><HR></td>
      
         </tr>
     </tbody>
</table>
<table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody>
</table>'.$tbody.'';

   $html .= '<table border="0px" cellspacing="1" cellpadding="20" style="width:100%;font-size:11px;">';
   $html .= "<tr>";
   $html .= "<td colspan='20' align='left'>Observaciones:".$datos_pdf['QaCertificadoCalidad']['observaciones']. "</td>";
   $html .= "</tr>";
   $html .= "</tr>";
   $html .= "<tr>";
   $html .= "<td colspan='20'>Los ensayos se efectúan conforme a las normas API 6D, API 598, ASME B16.34 según corresponda y Plan de inspección y ensayos.</td>";
   $html .= "</tr>";
 
   $html .= "</table>";
   
   
   $html .="
   
            
             <br> <br>";
   
   $html .= "<HR>";
  
  $cuerpo_tabla_comp = ''; 
  
  
  foreach($datos_pdf['QaCertificadoCalidadItem'] as $dato){
  

        $cuerpo_tabla_comp .= ' <tr>
                        <td>'.$dato["Componente"]["Categoria"]["d_categoria"].'</td>
                        <td>'.$dato["Componente"]["ArticuloRelacion"][0]["ArticuloHijo"]["d_costo"].'</td>
                        <td>'.$dato["lote"].'</td>
                      </tr>';
  } 
   
   $html.='<table width="100%" border="1" cellspacing="1" cellpadding="1" style="width:100%;font-size:10px;" >
          <tr>
            <th scope="col"><strong>Componente</strong></th>
            <th scope="col"><strong>Material del Componente</strong></th>
            <th scope="col"><strong>Certificados, lotes y/o coladas</strong></th>
          </tr>
           '.$cuerpo_tabla_comp.'
        </table>
        </br>
        </br>
        </br>
        </br>
        <table width="100%" border="0">
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td width="50%">&nbsp;</td>
    <td width="50%" align="center" valign="middle">Control de Calidad </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="middle">Valmec S.A</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="middle"><hr width="100%" align="center" /></td>
  </tr>
</table>

        ';
   
     
   
$pdf->writeHTML($html, true, false, true, false, '');


// QRCODE,L : QR-CODE Low error correction
// set style for barcode
$style = array(
    'border' => 2,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

//$pdf->Text(20, 25, 'QRCODE L');

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->SetBarcode(date("Y-m-d H:i:s", time()));
ob_clean();
$pdf->Output('CertificadoCalidad_'.$id.'.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
