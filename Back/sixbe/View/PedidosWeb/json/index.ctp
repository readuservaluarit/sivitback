<?php
        // CONFIG  CON MODE DISPLAYED CELLS
        include(APP.'View'.DS.'Comprobantes'.DS.'json'.DS.'default.ctp');
        
        $model["codigo_tipo_comprobante"]["show_grid"] = 1;
        $model["codigo_tipo_comprobante"]["header_display"] = "Tipo";
        $model["codigo_tipo_comprobante"]["width"] = "6";
        $model["codigo_tipo_comprobante"]["order"] = "0";
        $model["codigo_tipo_comprobante"]["text_colour"] = "#000000";
        
        $model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
        $model["pto_nro_comprobante"]["width"] = "8";
        $model["pto_nro_comprobante"]["web_width"] = "12";
        $model["pto_nro_comprobante"]["order"] = "1";
        $model["pto_nro_comprobante"]["text_colour"] = "#000000";

       
 
        
        $model["fecha_generacion"]["show_grid"] = 1;
        $model["fecha_generacion"]["header_display"] = "Fecha Gen.";
        $model["fecha_generacion"]["width"] = "8";
        $model["fecha_generacion"]["web_width"] = "8";
        $model["fecha_generacion"]["order"] = "4";
        $model["fecha_generacion"]["text_colour"] = "#0000FF";
		
		$model["fecha_vencimiento"]["show_grid"] = 1;
        $model["fecha_vencimiento"]["header_display"] = "Fecha Vencimiento";
        $model["fecha_vencimiento"]["width"] = "8";
        $model["fecha_vencimiento"]["order"] = "5";
        $model["fecha_vencimiento"]["text_colour"] = "#CC0000";
        
        $model["d_moneda_simbolo"]["show_grid"] = 1;
        $model["d_moneda_simbolo"]["header_display"] = "Moneda";
        $model["d_moneda_simbolo"]["width"] = "4";
        $model["d_moneda_simbolo"]["order"] = "6";
        $model["d_moneda_simbolo"]["text_colour"] = "#AA0011";
        
        $model["subtotal_neto"]["show_grid"] = 1;
        $model["subtotal_neto"]["header_display"] = "Monto Neto";
        $model["subtotal_neto"]["width"] = "6";
        $model["subtotal_neto"]["order"] = "7";
        $model["subtotal_neto"]["text_colour"] = "#550000";
        
		$model["total_comprobante"]["show_grid"] = 1;
        $model["total_comprobante"]["header_display"] = "Total Comprobate";
        $model["total_comprobante"]["width"] = "7";
        $model["total_comprobante"]["order"] = "8";
        $model["total_comprobante"]["text_colour"] = "#AA0011";
        
     
    
        $model["d_estado_comprobante"]["show_grid"] = 1;
        $model["d_estado_comprobante"]["header_display"] = "Estado";
        $model["d_estado_comprobante"]["width"] = "6";
        $model["d_estado_comprobante"]["order"] = "10";
        $model["d_estado_comprobante"]["text_colour"] = "#004400";
        
        
       
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Comprobante",
                    "content" => $model
                );
        
        echo json_encode($output);
?>