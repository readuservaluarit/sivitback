<?php


App::uses('StockProductosController', 'Controller');

  /**
    * @secured(CONSULTA_STOCK)
  */
class StockMateriaPrimasController extends StockProductosController {
    public $name = 'StockMateriaPrimas';
    public $model = 'StockMateriaPrima';
    public $tipo_producto = EnumProductoTipo::MateriaPrima;
    public $padre = "MateriaPrima";
    public $d_padre = "d_materia_prima";
    public $deposito_fijo_principal = 4;
    public $deposito_fijo_complementario = 5;
   
   
    /**
    * @secured(CONSULTA_STOCK)
    */
    public function index(){
        
        parent::index();
    }
    
    /**
    * @secured(CONSULTA_STOCK)
    */
    public function getModel($vista='default'){
        
        parent::getModel($vista);
    }
    
}
?>