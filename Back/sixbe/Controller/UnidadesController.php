<?php

/**
* @secured(CONSULTA_UNIDAD)
*/
class UnidadesController extends AppController {
    public $name = 'Unidades';
    public $model = 'Unidad';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
/**
* @secured(CONSULTA_UNIDAD)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
        $nombre = strtolower($this->getFromRequestOrSession('Unidad.d_unidad'));
        
        $conditions = array(); 
        if ($nombre != "") {
            $conditions = array('LOWER(Unidad.d_unidad) LIKE' => '%' . $nombre . '%');
        }

        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => 10,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de Unidades', 'Datos de las Unidades', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('Unidad.d_unidad', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'id', "10%");
                $formBuilder->addHeader('Nombre', 'd_unidad', "90%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_unidad');

                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        foreach($data as &$valor){
            
            $valor["Unidad"]["d_unidad_afip"] = $valor["UnidadAfip"]["d_unidad_afip"];
            $valor["Unidad"]["d_unidad_calculo"] = $valor["UnidadCalculo"]["d_unidad_calculo"];
            unset( $valor["UnidadAfip"]);
            unset( $valor["UnidadCalculo"]);
            
            
        }
        
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->Unidad->id = $id;
            $this->request->data = $this->Unidad->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Unidades');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Unidades');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Unidades');
        else
            $formBuilder->setTituloForm('Consulta de Unidades');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_unidad', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));  
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'UnidadAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('UnidadDUnidad', 'Debe ingresar un nombre');


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_UNIDAD)
    */
    public function add() {
    	if ($this->request->is('post')){
    		$this->loadModel($this->model);
    		
    		try{
    			if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
    				$mensaje = "La Unidad ha sido creada exitosamente";
    				$tipo = EnumError::SUCCESS;
    				
    			}else{
    				$errores = $this->{$this->model}->validationErrors;
    				$errores_string = "";
    				foreach ($errores as $error){
    					$errores_string.= "&bull; ".$error[0]."\n";
    					
    				}
    				$mensaje = $errores_string;
    				$tipo = EnumError::ERROR;
    				
    			}
    		}catch(Exception $e){
    			
    			$mensaje = "Ha ocurrido un error,la Unidad no ha podido ser creada.";
    			$tipo = EnumError::ERROR;
    		}
    		$output = array(
    				"status" => $tipo,
    				"message" => $mensaje,
    				"content" => ""
    		);
    		//si es json muestro esto
    		if($this->RequestHandler->ext == 'json'){
    			$this->set($output);
    			$this->set("_serialize", array("status", "message", "content"));
    		}else{
    			
    			$this->Session->setFlash($mensaje, $tipo);
    			$this->redirect(array('action' => 'index'));
    		}
    		
    	}
    	
    	//si no es un post y no es json
    	if($this->RequestHandler->ext != 'json')
    		$this->redirect(array('action' => 'abm', 'A'));   
    }

    /**
    * @secured(BAJA_UNIDAD)
    */
    function delete($id) {
        
    	$this->loadModel($this->model);
    	$mensaje = "";
    	$status = "";
    	$this->{$this->model}->id = $id;
    	
    	try{
    		if ($this->{$this->model}->delete() ) {
    			
    			//$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
    			
    			$status = EnumError::SUCCESS;
    			$mensaje = "La Unidad ha sido eliminada exitosamente.";
    		
    			
    		}
    		
    		else
    			throw new Exception("No puede Borrarse la Unidad");
    			
    	}catch(Exception $ex){
    		//$this->Session->setFlash($ex->getMessage(), 'error');
    		
    		$status = EnumError::ERROR;
    		$mensaje = $ex->getMessage();
    	
    	}
    	

    	
    	$output = array(
    			"status" => $status,
    			"message" => $mensaje,
    			"content" => ""
    	);
    		$this->set($output);
    		$this->set("_serialize", array("status", "message", "content"));
    		
    	
    	
    }

    
    
    /**
     * @secured(MODIFICACION_UNIDAD)
     */
    public function edit($id) {
    	
    	
    	if (!$this->request->is('get')){
    		
    		$this->loadModel($this->model);
    		$this->{$this->model}->id = $id;
    		
    		try{
    			if ($this->{$this->model}->saveAll($this->request->data)){
    				
    				$error = EnumError::SUCCESS;
    				$mensaje = "La Unidad ha sido modificado exitosamente";
    				
    				
    			}else{
    				$error = EnumError::ERROR;
    				$mensaje = "Ha ocurrido un error, La Unidad no ha podido modificarse.";
    				
    			}
    		}catch(Exception $e){
    			$this->Session->setFlash('Ha ocurrido un error, La Unidad no ha podido modificarse.', 'error');
    			$error = EnumError::ERROR;
    			$mensaje = "Ha ocurrido un error, La Unidad no ha podido modificarse.";
    			
    		}
    		
    	}
    	
    	if($this->RequestHandler->ext != 'json'){
    		$this->Session->setFlash($mensaje, $error);
    		$this->redirect(array('controller' => 'Provincias', 'action' => 'index'));
    	} else{
    		
    		$output = array(
    				"status" => $error,
    				"message" => $mensaje,
    				"content" => ""
    		);
    		$this->set($output);
    		$this->set("_serialize", array("status", "message", "content"));
    	}
    }
    
     /**
    * @secured(CONSULTA_UNIDAD)
    */
    public function getModel($vista="default"){
        
		$model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
        
    }
    
	
	private function editforView($model,$vista)
	{  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);    
        
    }
	
	/*
    private function editforView($model){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER

        $propiedades = array("show_grid"=>"1","header_display" => "Desc.","minimun_width"=> "10","order" =>"1","text_colour"=>"#000000" );
        $this->setFieldView("d_unidad",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Unidad Afip","minimun_width"=> "10","order" =>"2","text_colour"=>"#000000" );
        $this->setFieldView("d_unidad_afip",$propiedades,$model);
	
        $propiedades = array("show_grid"=>"1","header_display" => "Unidad Calculo","minimun_width"=> "10","order" =>"3","text_colour"=>"#000000" );
        $this->setFieldView("d_unidad_calculo",$propiedades,$model);
		

		$propiedades = array("show_grid"=>"1","header_display" => "U01","minimun_width"=> "10","order" =>"2","text_colour"=>"#000000" );
        $this->setFieldView("d_unidad_afip",$propiedades,$model);
		
		
		$propiedades = array("show_grid"=>"1","header_display" => "U02" ,"minimun_width"=> "10","order" =>"2","text_colour"=>"#000000" );
        $this->setFieldView("d_unidad_afip",$propiedades,$model);
        
        return $model;
    }*/


}
?>