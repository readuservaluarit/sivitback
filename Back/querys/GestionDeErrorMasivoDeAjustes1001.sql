/*
INSERT INTO `valuarit_siv_produccion`.`movimiento` (
    `cantidad`, `id_movimiento_tipo`,  `id_producto`,  `id_componente`,  
    `id_materia_prima`, `fecha`,  `observacion`,  `id_lote`,
  `id_usuario`, `id_deposito_origen`
) 
SELECT 
    cantidad,  1002,    id_producto,    id_componente,
    id_materia_prima , '2019-04-24' AS fecha, 'FIX Masivo - Primer carga al sistema.', id_lote, 
   '1' AS id_usuario , 4 as id_deposito_origen
FROM movimiento
WHERE `observacion` LIKE '%Primer carga al sistema%'
AND fecha >= '2019-04-17'
AND id_usuario IN(15,20) /*112*/
*/

SELECT * FROM movimiento
WHERE `observacion` LIKE '%Primer carga al sistema%'
AND id_usuario IN(15,20) /*112*/
AND fecha >= '2019-04-17'



CREATE TEMPORARY TABLE IF NOT EXISTS tmp_stock_fix AS (
		SELECT id_producto AS id, SUM(cantidad) AS cantidad 
		FROM movimiento  
		WHERE  id_movimiento_tipo = 1001
		AND `observacion` LIKE '%Primer carga al sistema%'
		AND fecha >= '2019-04-17'
		AND id_usuario IN(15,20)
		GROUP BY id_producto )
		
		
		
SELECT * FROM stock_producto T1
INNER JOIN tmp_stock_fix  T2 ON T1.id = T2.id AND T1.id_deposito = 4





UPDATE stock_producto T1
INNER JOIN tmp_stock_fix T2 ON T1.id = T2.id AND T1.id_deposito = 4
SET T1.stock = T1.stock - T2.cantidad

