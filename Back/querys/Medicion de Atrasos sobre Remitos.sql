﻿
SELECT * FROM (




/*PEDIDO-REMITO*/

SELECT DISTINCT comp_abajo.`nro_comprobante` AS nro_pedido,persona.`razon_social`, comp_abajo.`fecha_generacion` AS fecha_carga_pi,comp_abajo.`fecha_entrega` AS fecha_entrega_prometida_pi,comp_abajo.`inspeccion`,moneda.`simbolo_internacional`,
persona_categoria.`d_persona_categoria`,
REPLACE(comp_item_abajo_pi.`precio_unitario`,".",",") AS precio_unitario,comp_item_abajo_pi.`cantidad` cantidad_del_pedido,comprobante_item.`cantidad` cantidad_remitida,comp_item_abajo_pi.`n_item`,producto.`codigo`,comp_item_abajo_pi.`dias` AS dias_prometidos,

 CAST(
    (
      `comp_abajo`.`fecha_generacion` + INTERVAL IF(
        ISNULL(`comp_item_abajo_pi`.`dias`),
        0,
        `comp_item_abajo_pi`.`dias`
      ) DAY
    ) AS DATE
  ) fecha_entrega_item_prometida,
  
  1,/*esta calumna hay que borrarla para el reporte en el excel*/
  2,/*esta calumna hay que borrarla para el reporte en el excel*/
  
  comprobante.`fecha_add` AS fecha_generacion_remito,
  comprobante.`nro_comprobante` AS nro_remito,
  
   
  
   (SELECT 
    comprobante.`nro_comprobante` 
  FROM
    comprobante_item 
    JOIN comprobante 
      ON comprobante.id = comprobante_item.`id_comprobante` 
  WHERE id_comprobante_item_origen = comp_item_abajo_pi.`id` 
    AND id_tipo_comprobante = 104 
    AND comprobante_item.`activo` = 1 
  LIMIT 1) AS nro_orden_armado,
  
  (SELECT 
    comprobante.`fecha_cierre` 
  FROM
    comprobante_item 
    JOIN comprobante 
      ON comprobante.id = comprobante_item.`id_comprobante` 
  WHERE id_comprobante_item_origen = comp_item_abajo_pi.id 
    AND id_tipo_comprobante = 104 
    AND comprobante_item.`activo` = 1 
  LIMIT 1) AS fecha_cierre_nro_orden_armado,
   

  
  (SELECT 
    comprobante.`fecha_entrega` 
  FROM
    comprobante_item 
    JOIN comprobante 
      ON comprobante.id = comprobante_item.`id_comprobante` 
  WHERE id_comprobante_item_origen = comp_item_abajo_pi.id 
    AND id_tipo_comprobante = 104 
    AND comprobante_item.`activo` = 1 
  LIMIT 1) 
   AS fecha_entrega_nro_orden_armado
  
 
   
 
  




 FROM comprobante_item
LEFT JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`

LEFT JOIN comprobante_item comp_item_abajo_pi ON comp_item_abajo_pi.id = comprobante_item.id_comprobante_item_origen

 LEFT JOIN comprobante comp_abajo ON comp_abajo.id = comp_item_abajo_pi.`id_comprobante`
 
LEFT JOIN producto ON comp_item_abajo_pi.`id_producto` = producto.id

LEFT JOIN moneda ON  moneda.id=comp_abajo.`id_moneda`

JOIN persona ON comp_abajo.`id_persona` = persona.id

LEFT JOIN persona_categoria ON persona_categoria.id = persona.`id_persona_categoria`

WHERE comprobante.id_tipo_comprobante = 203 AND comprobante.id_estado_comprobante<>2 AND YEAR(comprobante.`fecha_generacion`) =YEAR(NOW())

AND comprobante.`id_estado_comprobante`<>2

AND comp_abajo.`id_tipo_comprobante` IN (202)


AND comp_item_abajo_pi.`activo` = 1

AND comprobante_item.`activo`=1




UNION /*PEDIDO FACTURA REMITO*/




SELECT 
  comprobante.`nro_comprobante` AS nro_pedido,
  comprobante.`razon_social`,
  comprobante.`fecha_generacion` AS fecha_carga_pedido,
  comprobante.`fecha_entrega` AS fecha_entrega_prometida_pi,
  comprobante.`inspeccion`,
  moneda.`simbolo_internacional`,
  persona_categoria.`d_persona_categoria`,
  REPLACE(comprobante_item.`precio_unitario`,".",",") AS precio_unitario,
  comprobante_item.`cantidad` cantidad_pedido,
  remito_item.cantidad AS cantidad_remitida,
 
  comprobante_item.`n_item`,
  producto.`codigo`,
  comprobante_item.`dias`,
 
  
  

  CAST(
    (
      `comprobante`.`fecha_generacion` + INTERVAL IF(
        ISNULL(`comprobante_item`.`dias`),
        0,
        `comprobante_item`.`dias`
      ) DAY
    ) AS DATE
  ) AS fecha_entrega_item_prometida_pi,
  
  

  @pedido_item_id := comprobante_item.id,
  @fact_item_id := fact_item.id,
    remito.fecha_add AS fecha_generacion_remito, 
  remito.nro_comprobante AS nro_remito,


  (SELECT 
    comprobante.`nro_comprobante` 
  FROM
    comprobante_item 
    JOIN comprobante 
      ON comprobante.id = comprobante_item.`id_comprobante` 
  WHERE id_comprobante_item_origen = @pedido_item_id 
    AND id_tipo_comprobante = 104 
    AND comprobante_item.`activo` = 1 
  LIMIT 1) 
  
  
  /*Chequea y te da si se hizo un remito o sea la fecha*/
  AS nro_orden_armado ,
  
    (SELECT 
    comprobante.`fecha_cierre` 
  FROM
    comprobante_item 
    JOIN comprobante 
      ON comprobante.id = comprobante_item.`id_comprobante` 
  WHERE id_comprobante_item_origen = @pedido_item_id 
    AND id_tipo_comprobante = 104 
    AND comprobante_item.`activo` = 1 
  LIMIT 1) 
  
  
  /*Chequea y te da si se hizo un remito o sea la fecha*/
  AS fecha_cierre_nro_orden_armado,
  
  (SELECT 
    comprobante.`fecha_entrega` 
  FROM
    comprobante_item 
    JOIN comprobante 
      ON comprobante.id = comprobante_item.`id_comprobante` 
  WHERE id_comprobante_item_origen = @pedido_item_id 
    AND id_tipo_comprobante = 104 
    AND comprobante_item.`activo` = 1 
  LIMIT 1) 
   AS fecha_entrega_nro_orden_armado
FROM
  comprobante_item 
  JOIN comprobante 
    ON comprobante.`id` = comprobante_item.`id_comprobante` 
  JOIN moneda 
    ON moneda.id = comprobante.`id_moneda` 
  JOIN persona 
    ON persona.id = comprobante.id_persona 
  LEFT JOIN persona_categoria 
    ON persona_categoria.id = persona.`id_persona_categoria` 
  LEFT JOIN comprobante_item fact_item 
    ON fact_item.`id_comprobante_item_origen` = comprobante_item.`id` 
  LEFT JOIN comprobante factura 
    ON factura.id = fact_item.`id_comprobante` 
 JOIN comprobante_item remito_item
    ON remito_item.id_comprobante_item_origen = fact_item.id 
    
  JOIN comprobante remito
  ON remito.id = remito_item.id_comprobante
 
  JOIN producto 
    ON producto.id = comprobante_item.`id_producto` 
    LEFT JOIN estado_comprobante ON


comprobante.`id_estado_comprobante` = estado_comprobante.`id`
    
    
WHERE comprobante.id_tipo_comprobante = 202 
AND factura.id_tipo_comprobante>=800
AND comprobante_item.`activo` = 1 
AND remito_item.activo=1
AND fact_item.activo = 1
AND factura.cae>0
AND YEAR(factura.fecha_contable)= YEAR(NOW())
AND remito.id_tipo_comprobante=203

) AS tabla LIMIT 10000000000000000