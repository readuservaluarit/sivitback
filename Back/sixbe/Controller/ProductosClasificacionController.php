<?php


class ProductosClasificacionController extends AppController {
    public $name = 'ProductosClasificacion';
    public $model = 'ProductoClasificacion';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

/**
* @secured(CONSULTA_PRODUCTO_CLASIFICACION)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
                                                                            //data[ProductoClasificacion][id_destino_PRODUCTO_excluir]=2
        $codigo = strtolower($this->getFromRequestOrSession('ProductoClasificacion.codigo'));
        
        
                    //$id_destino_PRODUCTO_excluir = 2;
        $conditions = array();
    
        if ($codigo != "") {
            $conditions = array('ProductoClasificacion.$codigo LIKE' =>'%'. $codigo .'%' );
        }
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'contain'=>false,
            'limit' => 10,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de TipoPersonaCausas', 'Datos de los Tipo de Documento', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_CLASIFICACIONPersona', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'TipoPersona.id', "10%");
                $formBuilder->addHeader('Nombre', 'TipoPersona.d_CLASIFICACIONPersona_causa', "50%");
                $formBuilder->addHeader('Nombre', 'TipoPersona.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_CLASIFICACIONPersonaCausa');
                $formBuilder->addField($this->model, 'cotizacion');
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }
    
       /**
        * @secured(CONSULTA_PRODUCTO_CLASIFICACION)
        */
        public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }

        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);

        }


   



    
    
    
    
   
	

}
?>