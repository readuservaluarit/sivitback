<?php

/**
* @secured(CONSULTA_MONEDA)
*/
class MonedasController extends AppController {
    public $name = 'Monedas';
    public $model = 'Moneda';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
/**
* @secured(CONSULTA_MONEDA)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
        $nombre = strtolower($this->getFromRequestOrSession('Moneda.d_moneda'));
		$permite_factura_electronica = strtolower($this->getFromRequestOrSession('Moneda.permite_factura_electronica'));
        
		//$permite_factura_electronica = 1; // TESTING POR BROWSER. TODO si le pongo 0 no funciona
		
        $conditions = array(); 
        if ($nombre != "") {
            $conditions = array('LOWER(Moneda.d_moneda) LIKE' => '%' . $nombre . '%');
        }
		
		if ($permite_factura_electronica != "") {
            $conditions = array('Moneda.permite_factura_electronica ' => $permite_factura_electronica );
        }

        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de Monedas', 'Datos de las Monedas', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_moneda', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'Moneda.id', "10%");
                $formBuilder->addHeader('Nombre', 'Moneda.d_moneda', "50%");
                $formBuilder->addHeader('Nombre', 'Moneda.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_moneda');
                $formBuilder->addField($this->model, 'cotizacion');
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->Moneda->id = $id;
            $this->request->data = $this->Moneda->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Moneda');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Moneda');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Moneda');
        else
            $formBuilder->setTituloForm('Consulta de Moneda');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_moneda', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('cotizacion', 'Cotizaci&oacute;n', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));    
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'MonedaAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('MonedaDMoneda', 'Debe ingresar un nombre');
        validator.validateRequired('MonedaCotizacion', 'Debe ingresar una cotizaci&oacute;n');


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_MONEDA)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            
            try{
                if ($this->Moneda->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "La Moneda ha sido creada exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,la Moneda no ha podido ser creada.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => ""
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }

    /**
    * @secured(MODIFICACION_MONEDA)
    */
     
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                
                $error_cheque = $this->ChequeoCotizacionBase($id);
                
                if($error_cheque == 0){
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "La Moneda ha sido modificada exitosamente";
                     $status = EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
               }
               
               }else{
                   
                    $mensaje = "La cotizaci&oacute;n de la Moneda base deber ser = 1.";
                    $status = EnumError::ERROR;
                   
               }
               
               if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, la Moneda no ha podido modificarse.', 'error');
                
            }
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            //$this->{$this->model}->contain('Provincia','Pais');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }

    
    
     /**
    * @secured(CONSULTA_MONEDA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
        
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        
            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);
        
               
        
        
      
    }
    
    
    public function ConsultarCotizacionMoneda(){
    	
    	
    	App::import('Vendor', 'config', array('file' => 'fesix/config.php'));//importo el archivo de configuracion de la carpeta Vendor/fesix
    	App::import('Vendor', 'AfipWsaa', array('file' => 'fesix/afip/AfipWsaa.php'));//importo el archivo de configuracion de la carpeta Vendor/fesix
    	App::import('Vendor', 'AfipWsfev1', array('file' => 'fesix/afip/AfipWsfev1.php'));//imp
    	
    	
    	
    	$id_moneda   = $this->getFromRequestOrSession('Moneda.id_moneda');
    	
    	
    	
    	$this->loadModel(EnumModel::Moneda);
  
    	
    	
    	$cae = 0;
    	$webService   = 'wsfe';//Creando el objeto WSAA (Web Service de Autenticación y Autorización)
    	$datoEmpresa = $this->Session->read('Empresa');
    	$empresaCuit  = str_replace("-","",$datoEmpresa["DatoEmpresa"]["cuit"]);
    	$empresaAlias  = $datoEmpresa["DatoEmpresa"]["alias"];
    	$wsaa = new AfipWsaa($webService,$empresaAlias);
    	
    	$this->Moneda->id = $id_moneda;
    	$this->request->data = $this->Moneda->read();
    	$content = "";
    	$messagge = "";
    	
    	
    	
    	
    	
    	
    	$MonId = $this->request->data["Moneda"]["abreviacion_afip"];
    	
    	
    	if($datoEmpresa["DatoEmpresa"]["id_moneda"] == EnumMoneda::Dolar && $id_moneda == EnumMoneda::Peso)
    		$MonId = 'DOL';
    	
    	
    	
    	$array_comprobantes = array();
    	
    	//$a = file_get_contents("https://www.google.com");
    	
    	
    	if ($ta = $wsaa->loginCms())
    	{
    		$token      = $ta['token'];
    		$sign       = $ta['sign'];
    		$expiration = $ta['expiration'];
    		$uniqueid   = $ta['uniqueid'];
    		
 
    		
    		
    		$wsfe = new AfipWsfev1($empresaCuit,$token,$sign);
    		
    		
    		
    		$moneda_cotizacion = $wsfe->FEParamGetCotizacion($MonId);
    		
    		
    		
    		if(!$moneda_cotizacion){
    			$error = EnumError::ERROR;
    			$messagge = "Error no es posible obtener la cotizaci&oacute;n";
    			
    		}else{
    			$error = EnumError::SUCCESS;
    			$content = $moneda_cotizacion['MonCotiz'];
    			$messagge = "list";
    		}
    			
    			
    			
    			
    			

    		
    		
    		
    		
    	}else{
    		$error = EnumError::ERROR;
    		$messagge = "Error no es posible obtener la cotizaci&oacute;n";
    	
    	}
    	
    	
    	
    	$output = array(
    			"status" => $error,
    			"message" => $messagge,
    			"content" => $content,
    			"page_count" =>0
    	);
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message","page_count", "content"));
    	
    	
    }  
    
    
    public function getCotizacionMonedaSiv(){
    	
    	$this->loadModel(EnumModel::Moneda);
    	
    	$id_moneda = $this->getFromRequestOrSession('Moneda.id_moneda');
    	
    	$cotizacion = $this->Moneda->getCotizacion($id_moneda);
    	
    	if($cotizacion == 0)
    		$error = EnumError::ERROR;
    	else 
    		$error = EnumError::SUCCESS;
    	
    		
    	
    	$output = array(
    			"status" => $error,
    			"message" => "",
    			"content" => round($cotizacion,2),
    			"page_count" =>0
    	);
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message","page_count", "content"));
    	
    	
    	
    }
    
    
    



}
?>