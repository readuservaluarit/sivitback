<?php


            include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');
            
            
            $model["codigo"]["show_grid"] = 1;
            $model["codigo"]["header_display"] = "C&oacute;digo";
            $model["codigo"]["order"] = "1";
            $model["codigo"]["width"] = "15";

            $model["cantidad"]["show_grid"] = 1;
            $model["cantidad"]["header_display"] = "Cantidad";
            $model["cantidad"]["order"] = "2";            
            $model["cantidad"]["width"] = "10";
			$model["cantidad"]["text_colour"] = "#0000FF";//BLUE

            $model["total_facturado"]["show_grid"] = 1;
            $model["total_facturado"]["header_display"] = "Cant. Facturada";
            $model["total_facturado"]["order"] = "3";
            $model["total_facturado"]["width"] = "5";
			$model["total_facturado"]["text_colour"] = "#00AA00";//GREEN
            
            
            $model["cantidad_pendiente"]["show_grid"] = 1;
            $model["cantidad_pendiente"]["header_display"] = "Cant. Pendiente";
            $model["cantidad_pendiente"]["order"] = "4";            
            $model["cantidad_pendiente"]["width"] = "5";
			$model["cantidad_pendiente"]["text_colour"] = "#FF0000";

 $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItem",
                    "content" => $model
                );
        
          echo json_encode($output);
?>