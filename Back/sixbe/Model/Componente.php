<?php
App::uses('AppModel', 'Model');
App::uses('ArticuloRelacion', 'Model');
App::uses('Producto', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class Componente extends Producto {


/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
        'codigo' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'El codigo no debe ser vacio',
            ),
            
            'minimodecaracteres' => array(
            'rule' => array('minLength', 2),
            'message' => 'El c&oacute;digo debe tener como m&iacute;nimo 2 caract&eacute;res'
        ),
        /*'unico' => array(
            'rule' => array('isUnique'),
            'message' => 'El c&oacute;digo ya existe'
        )*/
        ),
        'd_componente' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'La descripci&oacute;n no debe ser vacio',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        
       
       
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 
 
 
public $hasMany = array(
        'ArticuloRelacion' => array(
            'className' => 'ArticuloRelacion',
            'foreignKey' => 'id_producto_padre',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        
         'ListaPrecioProducto' => array(
            'className' => 'ListaPrecioProducto',
            'foreignKey' => 'id_producto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'StockComponente' => array(
            'className' => 'StockComponente',
            'foreignKey' => 'id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
		'ProductoCodigo' => array(
				'className' => 'ProductoCodigo',
				'foreignKey' => 'id_producto',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
				'order' => '',
				'limit' => '',
				'offset' => '',
				'exclusive' => '',
				'finderQuery' => '',
				'counterQuery' => ''
		),
        
);

public $belongsTo = array(
        'Unidad' => array(
            'className' => 'Unidad',
            'foreignKey' => 'id_unidad',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Iva' => array(
            'className' => 'Iva',
            'foreignKey' => 'id_iva',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'FamiliaProducto' => array(
            'className' => 'FamiliaProducto',
            'foreignKey' => 'id_familia_producto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Origen' => array(
            'className' => 'Origen',
            'foreignKey' => 'id_origen',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'Categoria' => array(
            'className' => 'Categoria',
            'foreignKey' => 'id_categoria',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'MediaFile' => array(
            'className' => 'MediaFile',
            'foreignKey' => 'id_media_file',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'ProductoTipo' => array(
            'className' => 'ProductoTipo',
            'foreignKey' => 'id_producto_tipo',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'Marca' => array(
            'className' => 'Marca',
            'foreignKey' => 'id_marca',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'CuentaContableVenta' => array(
            'className' => 'CuentaContableVenta',
            'foreignKey' => 'id_cuenta_contable_venta',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'CuentaContableCompra' => array(
            'className' => 'CuentaContableCompra',
            'foreignKey' => 'id_cuenta_contable_compra',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        )
        
        
  );

/*
public function afterSave($model,$created,$options) {
   
   
   //solo en el update    
    if(!$created){
        
       
        $componente_p = new ArticuloRelacion();
        
        
        //obtengo todos los productos que estan formados por ese componente (puede haber mas de 1)
        $cp = $componente_p->find('all', array(
        'conditions' => array('ArticuloRelacion.id_componente' => $this->data["Componente"]["id"]),
        'contain' =>false
       
    ));
    
    
        if($cp){ //si la Materia Prima Tiene componentes le actualizo el costo
        
        foreach($cp as $componente_producto){ //por cada componente actualizo el producto
            
          
            $id_producto = $componente_producto["ArticuloRelacion"]["id_producto"];
            
            $componentes = $componente_p->find('all', array(       //obtengo los componentes del producto
            'conditions' => array('ArticuloRelacion.id_producto' => $id_producto),
            
            ));
            
            $costo_producto = 0;
            foreach($componentes as $componente_individual){  //por cada componente le saco el costo y lo acumulo
                $costo_producto = $costo_producto + $componente_individual["Componente"]["costo"]*$componente_individual["ArticuloRelacion"]["cantidad"];
            }
            
            
            $producto = new Producto();
            
            $producto->updateAll(
                                  array('Producto.costo' => $costo_producto ),
                                  array('Producto.id' => $id_producto) );
        }
        
        }
       
        
  }
}
	*/
    
    
    
}


