<?php
App::uses('AppModel', 'Model');

/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class StockArticulo extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'stock_producto';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		/*'id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar un id v&aacute;lido',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
		),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	
    
    );

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 
 public function setDecimalPlaces(){//configura los decimal places para este Model
    
     App::uses('Modulo', 'Model');
     $modulo_obj = new Modulo();
     $cantidad_decimales = $modulo_obj->getCantidadDecimalesFormateo(EnumModulo::STOCK); 
     
     $this->decimal_places = array(
    "stock"=>$cantidad_decimales
    
    );
    
    }
 

public $belongsTo = array(
        'Articulo' => array(
            'className' => 'Articulo',
            'foreignKey' => 'id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Deposito' => array(
            'className' => 'Deposito',
            'foreignKey' => 'id_deposito',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        
  );
  
  
  
 public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     "codigo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_producto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_componente"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_materia_prima"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "stock_deposito_fijo_principal"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     "stock_deposito_fijo_secundario"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     "stock_minimo"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_unidad"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_deposito"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "cantidad_componente_orden_produccion"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
	 "diferencia_stock"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     "distancia_porcentual"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     );
     
     }
  
  
     public function getTotalAgrupadoPorDeposito($id_producto,$array_depositos,$model,$desposito_fijo = 0){
    
    
    
	
	
    $stock = $this->find('all', array(
                                    'fields'=>array("SUM(".$model.".stock) as stock"),
                                    'conditions' => array($model.'.id' => $id_producto,$model.'.id_deposito ' => $array_depositos),
                                    'contain' =>false,
                                    'group' => array($model.'.id')
                                    ));
    
    
    
    if($stock)
        return $stock[0][0]["stock"];
    else
        return 0;
        
        
        
        
        
}


public function getTotalEnOrdenesDeProduccion($id_componente,$model="OrdenProduccion"){
    

	
    $orden_produccion = ClassRegistry::init("ComprobanteItem");
    
    $stock = $orden_produccion->find('all', array(
    		'fields'=>array("SUM(IFNULL(ComprobanteItem.cantidad,0)) as cantidad"),
    		'conditions' => array('ComprobanteItem.id_producto' => $id_componente,"Comprobante.id_estado_comprobante " =>EnumEstadoComprobante::Abierto,"ComprobanteItem.id_detalle_tipo_comprobante"=>EnumDetalleTipoComprobante::OrdenDeTrabajoCabecera),
    		'contain' =>array("Comprobante")
    		//   'group' => array($model.'.id')
    ));
    
    
   /* 
    $stock = $orden_produccion->find('all', array(
                                    'fields'=>array("SUM(".$model.".cantidad) as cantidad"),
                                    'conditions' => array($model.'.id_componente' => $id_componente,$model.".id_estado" =>EnumEstado::Abierta),
                                    'contain' =>false
                                 //   'group' => array($model.'.id')
                                    ));
    
    */
    
    if($stock)
        return $stock[0][0]["cantidad"];
    else
        return 0;
        
        
        
}
	
    
  public function afterFind($results, $primary = false) {
    
     App::uses('Modulo', 'Model');
     $modulo_obj = new Modulo();
     $cantidad_decimales = $modulo_obj->getCantidadDecimalesFormateo(EnumModulo::STOCK); 
    
    foreach ($results as $key => $val) {
        
        if (isset($val['StockArticulo']['stock'])) {
            $results[$key]['StockArticulo']['stock'] = (string) round($results[$key]['StockArticulo']['stock'],$cantidad_decimales);
    }
    
  
    }
    

    return $results;
}    

}




