<?php
        $model["cantidad_irm"]["show_grid"] = 0;
        $model["cantidad_irm"]["type"] = EnumTipoDato::biginteger;
        $model["signo_comercial"]["type"] = EnumTipoDato::biginteger;
        $model["simbolo_signo_comercial"]["type"] = EnumTipoDato::biginteger;
        $model["signo_contable"]["type"] = EnumTipoDato::biginteger;
        $model["total_comprobante_original"]["type"] = EnumTipoDato::decimal_flotante;
        
        // CONFIG  CON MODE DISPLAYED CELLS
      
		
		$model["d_tipo_comprobante"]["show_grid"] = 1;
        $model["d_tipo_comprobante"]["type"] = EnumTipoDato::cadena;
        $model["d_tipo_comprobante"]["header_display"] = "Tipo";
        $model["d_tipo_comprobante"]["width"] = "7"; //REstriccion mas de esto no se puede achicar
        $model["d_tipo_comprobante"]["order"] = "1";
        $model["d_tipo_comprobante"]["text_colour"] = "#000000";
		$model["d_tipo_comprobante"]["read_only"] = 1;
        
		$model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["type"] = EnumTipoDato::cadena;
        $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
        $model["pto_nro_comprobante"]["width"] = "11"; //REstriccion mas de esto no se puede achicar
        $model["pto_nro_comprobante"]["order"] = "2";
        $model["pto_nro_comprobante"]["text_colour"] = "#000000";
		$model["pto_nro_comprobante"]["read_only"] = 1;
		
		
		$model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["type"] = EnumTipoDato::cadena;
        $model["razon_social"]["header_display"] = "Raz&oacute;n Social";
        $model["razon_social"]["width"] = "20"; //REstriccion mas de esto no se puede achicar
        $model["razon_social"]["order"] = "3";
        $model["razon_social"]["text_colour"] = "#000000";
		$model["razon_social"]["read_only"] = 1;
        
        $model["total_comprobante"]["show_grid"] = 1;
        $model["total_comprobante"]["type"] = EnumTipoDato::decimal_flotante;
        $model["total_comprobante"]["header_display"] = "Total Comprobante";
        $model["total_comprobante"]["width"] = "10";
        $model["total_comprobante"]["order"] = "4";
        $model["total_comprobante"]["text_colour"] = "#F0000F";
		$model["total_comprobante"]["read_only"] = 1;
		
		 $model["total_a_imputar"]["show_grid"] = 0;
         $model["total_a_imputar"]["type"] = enumtipodato::decimal_flotante;
         $model["total_a_imputar"]["header_display"] = "total a imputar";
         $model["total_a_imputar"]["width"] = "10";
         $model["total_a_imputar"]["order"] = "5";
         $model["total_a_imputar"]["text_colour"] = "#0000cc";
        
        $model["saldo"]["show_grid"] = 1;
        $model["saldo"]["type"] = EnumTipoDato::decimal_flotante;
        $model["saldo"]["header_display"] = "Saldo";
        $model["saldo"]["width"] = "10";
        $model["saldo"]["order"] = "6";
        $model["saldo"]["text_colour"] = "#004400";
		$model["saldo"]["read_only"] = 1;
		
		$model["fecha_generacion"]["show_grid"] = 1;
        $model["fecha_generacion"]["type"] = EnumTipoDato::datetime;
        $model["fecha_generacion"]["header_display"] = "Fecha generación";
        $model["fecha_generacion"]["width"] = "10";
        $model["fecha_generacion"]["order"] = "7";
        $model["fecha_generacion"]["text_colour"] = "#0000FF";
		$model["fecha_generacion"]["read_only"] = 1;
        
        $model["fecha_vencimiento"]["show_grid"] = 1;
        $model["fecha_vencimiento"]["type"] = EnumTipoDato::datetime;
        $model["fecha_vencimiento"]["header_display"] = "Fecha Vencimiento";
        $model["fecha_vencimiento"]["width"] = "10";
        $model["fecha_vencimiento"]["order"] = "8";
        $model["fecha_vencimiento"]["text_colour"] = "#F0000F";
		$model["fecha_vencimiento"]["read_only"] = 1;
		
		//$model["id_moneda_origen"]["show_grid"] = 1;
        //$model["id_moneda_origen"]["type"] = EnumTipoDato::decimal_flotante;
        //$model["id_moneda_origen"]["header_display"] = "id moneda";
        //$model["id_moneda_origen"]["width"] = "10";
        //$model["id_moneda_origen"]["order"] = "9";
        //$model["id_moneda_origen"]["text_colour"] = "#000000";
		//$model["id_moneda_origen"]["read_only"] = 1;
		
		$model["valor_moneda"]["show_grid"] = 1;
        $model["valor_moneda"]["type"] = EnumTipoDato::decimal_flotante;
        $model["valor_moneda"]["header_display"] = "Valor Moneda";
        $model["valor_moneda"]["width"] = "10";
        $model["valor_moneda"]["order"] = "10";
        $model["valor_moneda"]["text_colour"] = "#000000";
		$model["valor_moneda"]["read_only"] = 1;
		
		
		
		$model["total_comprobante_origen"]["show_grid"] = 1;
        $model["total_comprobante_origen"]["type"] = EnumTipoDato::decimal_flotante;
        $model["total_comprobante_origen"]["header_display"] = "Total Cte. Origen";
        $model["total_comprobante_origen"]["width"] = "10";
        $model["total_comprobante_origen"]["order"] = "11";
        $model["total_comprobante_origen"]["text_colour"] = "#000000";
		$model["total_comprobante_origen"]["read_only"] = 1;
		
		
		// $model["valor_moneda2"]["show_grid"] = 1;
        // $model["valor_moneda2"]["type"] = EnumTipoDato::decimal_flotante;
        // $model["valor_moneda2"]["header_display"] = "Valor Moneda2";
        // $model["valor_moneda2"]["width"] = "10";
        // $model["valor_moneda2"]["order"] = "12";
        // $model["valor_moneda2"]["text_colour"] = "#000000";
		// $model["valor_moneda2"]["read_only"] = 1;
		
        
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Comprobante",
                    "content" => $model
                );
        
          echo json_encode($output);
?>