
select 'FAC',
f.id_punto_venta,
f.nro_factura_afip,
DATE_FORMAT(f.fecha,'%d/%m/%Y'),
tf.d_tipo_factura,
c.cuit,
ifnull(f.iva21,f.subtotal) as Exento,
CASE WHEN f.iva21 IS NOT NULL 
       THEN f.subtotal
       ELSE 0
END AS Gravado,
f.iva21,
f.perc_ib

from factura f
inner join tipo_factura tf  on tf.id = f.id_tipo_factura
inner join cliente c on c.id = f.id_cliente
WHERE f.id_dato_empresa = 6 AND NOT ISNULL(f.`cae`  ) AND f.nro_factura > 608


perc_ib
procesado	vencimiento_cae	cae	DESC Cliente	id_cliente	id_moneda	subtotal		total	id_punto_venta	id_estado	observaciones	valor_moneda	iva21	multiple_iva	porc_percepcion_ib	importe	access_token	id_dato_empresa




Tipo de documento     1-3         FAC   NCR  NDB
Punto de Venta        4-5         (Siempre seria 3 en caso de CEMI)
Nro,Comprobante       9-8
Fecha                 17-8        DDMMAAAA
CUIT
Letra                 25-1 
Exento                26-15       Numerico dos decimales  (los decimales los temas con punto o con Coma?)
Gravado               41-15           “
Iva                   56-15           “
Perc.I.B.             71-15           “


FAC[00003][00000001][13042016][A][30-53735085-3]
[000000000000.00]
[000000000156.00]
[000000000032.76]
[000000000000.00]

FAC[00003][00000015][15042016][B][33-66213495-9]
000000004634.30
000000000000.00
000000000000.00
000000000000.00

