<?php

class EstablecimientoPicker extends EntityPicker{
    
    public $model = "Establecimiento";
    
    public $pickerTitle = "Selección de Establecimiento (CUE)";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Cue", "field" => "cue_anexo", "width" => "60%", "keyOrder" => "cue_anexo"),
                                  array("title" => "Denominacion", "field" => "denominacion", "width" => "60%", "keyOrder" => "denominacion")
                                );

    public function getPickerConditions($busqueda){
        
        return array('OR' => array('cue_anexo LIKE'  => '%' . $busqueda['bus_unico'] . '%', 'Establecimiento.denominacion LIKE' => '%' . $busqueda['bus_unico'] . '%'));
    }

}