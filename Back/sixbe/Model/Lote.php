<?php
App::uses('AppModel', 'Model');

/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class Lote extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'lote';

    public $actsAs = array('Containable');

	public $source = "lote";

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'codigo' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar una cantidad',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
        
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);
	
	


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
   /*
   public $belongsTo = array(
        'OrdenProduccion' => array(
            'className' => 'OrdenProduccion',
            'foreignKey' => 'id_orden_produccion',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
   );     
	*/

	public $belongsTo = array(
			'TipoLote' => array(
					'className' => 'TipoLote',
					'foreignKey' => 'id_tipo_lote',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
			
	);
	
	
	
	    public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
		$this->virtual_fields_view = array(
		 
		"d_tipo_lote"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		"TipoLote"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null),
		"id_comprobante_item_lote"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 
		 
		 // //MP: objetos json particulares para  OrdenesTrabajo /indexMD/id.json
     	 // "ComprobanteItem"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null),
		 // "Detalle"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null), 
		 // "Lotes"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null),

		 
		 );
	}
	

}
