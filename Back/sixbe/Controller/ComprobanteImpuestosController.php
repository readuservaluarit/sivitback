<?php



class ComprobanteImpuestosController extends AppController {
    public $name = 'ComprobanteImpuestos';
    public $model = 'ComprobanteImpuesto';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    
    
    
	protected function RecuperoFiltros(){
      
       //Recuperacion de Filtros
        $id = $this->getFromRequestOrSession($this->model.'.id');
        $id_tipo_comprobante = $this->getFromRequestOrSession('Comprobante.id_tipo_comprobante');
        $id_tipo_impuesto = $this->getFromRequestOrSession('Impuesto.id_tipo_impuesto');
        $id_impuesto = $this->getFromRequestOrSession('ComprobanteImpuesto.id_impuesto');
        $id_persona = $this->getFromRequestOrSession('Comprobante.id_persona');
        $id_punto_venta = $this->getFromRequestOrSession('Comprobante.id_punto_venta');
        $id_sistema = $this->getFromRequestOrSession('TipoComprobante.id_sistema');
        $solo_definitivos = $this->getFromRequestOrSession('Comprobante.solo_definitivos');
        $id_comprobante = $this->getFromRequestOrSession($this->model.'.id_comprobante');
        $fecha_desde = $this->getFromRequestOrSession('Comprobante.fecha_contable'); //Hay q usar R para mandar datos para el controller Reporting
        $fecha_hasta = $this->getFromRequestOrSession('Comprobante.fecha_contable_hasta'); //
        $id_periodo_impositivo = $this->getFromRequestOrSession($this->model.'.id_periodo_impositivo'); //
        $ck_agrupado_jurisdiccion = $this->getFromRequestOrSession('Comprobante.ck_agrupado_jurisdiccion'); //para saber si el reporte agrupa o no por provincia
        $id_periodo_contable = $this->getFromRequestOrSession('Comprobante.id_periodo_contable'); //
        $anio_mes = $this->getFromRequestOrSession('Comprobante.anio_mes'); //A lo sumo se puede usar "R.factura.fecha_hasta"
        $excluir_anulados = $this->getFromRequestOrSession('Comprobante.excluir_anulados');
        $nro_comprobante = $this->getFromRequestOrSession('Comprobante.nro_comprobante');
        $d_punto_venta = $this->getFromRequestOrSession('Comprobante.d_punto_venta');
   
        $conditions = array(); 
        
        //$fecha_desde = '2016-12-01';        
         if($id_tipo_comprobante!=""){
                $id_tipos_de_comprobantes = explode(",", $id_tipo_comprobante);
             array_push($conditions, array('Comprobante.id_tipo_comprobante ' => $id_tipos_de_comprobantes )); 
         }
         if($id_tipo_impuesto!="")
            array_push($conditions, array('Impuesto.id_tipo_impuesto' => $id_tipo_impuesto ));
         
        if($id_impuesto!="")
            array_push($conditions, array($this->model.'.id_impuesto =' => $id_impuesto ));
         
         if($id_persona!="")
            array_push($conditions, array('Comprobante.id_persona ' => $id_persona ));
                  
         if($id_punto_venta!="")
            array_push($conditions, array('Comprobante.id_punto_venta ' => $id_punto_venta )); 
                 
         if($id_comprobante!="")
            array_push($conditions, array($this->model.'.id_comprobante ' => $id_comprobante )); 
            
         if($fecha_desde!="")
                array_push($conditions, array('Comprobante.fecha_contable >=' => $fecha_desde )); 
         
                
                
         if($nro_comprobante!="")
         	array_push($conditions, array('Comprobante.nro_comprobante' => $nro_comprobante)); 
         
         
         if($d_punto_venta!="")
         	array_push($conditions, array('Comprobante.d_punto_venta' => $d_punto_venta));
         	
         if($solo_definitivos==1)
                array_push($conditions, array('Comprobante.definitivo' => 1 ));                 

         if($fecha_hasta!="")
            array_push($conditions, array('Comprobante.fecha_contable <=' => $fecha_hasta));   
            
         if($id_periodo_impositivo!="") {
             array_push($conditions, array('Comprobante.id_periodo_impositivo ' => $id_periodo_impositivo));    
             } 
         
         if($anio_mes!=""){

         	$array_fecha = explode("-",$anio_mes);
            array_push($conditions, array('MONTH(Comprobante.fecha_contable)'=> $array_fecha[1] ));
            array_push($conditions, array('YEAR(Comprobante.fecha_contable)'=>  $array_fecha[0] ));
		}  
        
        if($id_periodo_contable !=""){
             
             $this->loadModel("PeriodoContable");
            
            $fecha_desde = $this->PeriodoContable->getFechaInicio($id_periodo_contable);
            $fecha_hasta = $this->PeriodoContable->getFechaFin($id_periodo_contable);
            array_push($conditions, array('Comprobante.fecha_contable >=' => $fecha_desde ));
            array_push($conditions, array('Comprobante.fecha_contable <=' => $fecha_hasta));
         }      
         
         
         if($this->Auth->user('id_persona')>0){
         	
         	$id_persona = $this->Auth->user('id_persona');
         	array_push($conditions, array('Comprobante.id_usuario' => $id_persona));
         	
         }
         
         if($excluir_anulados ==1)
         	array_push($conditions, array("NOT"=>array('Comprobante.id_estado_comprobante' => array(EnumEstadoComprobante::Anulado,EnumEstadoComprobante::Cancelado)) ));
         	
        
         return $conditions;
	}
    
    
    private function getArrayQuery($conditions,$id_sistema=0){
       
        if($id_sistema == 0){
            $id_sistema = $this->getFromRequestOrSession('TipoComprobante.id_sistema');
            if($id_sistema !="")
                array_push($conditions, array('TipoComprobante.id_sistema'=>$id_sistema ) );  
        }else{
            array_push($conditions, array('TipoComprobante.id_sistema'=>$id_sistema ) );  
        }
      
       if($id_sistema !="" || $id_sistema > 0){
    
		$joins = array(
                            "table"=>'tipo_comprobante',
                            //"type"=>'JOIN',
                            "alias"=>'TipoComprobante',
                            "conditions"=>array("TipoComprobante.id = Comprobante.id_tipo_comprobante")
                            );
       }else{
           
           $joins = "";
       
       } 
       
     
          return  array(
                'joins'=>array($joins
                            
                ),
                'contain' =>array('Impuesto','Comprobante'=>array('TipoComprobante','PuntoVenta','Persona'=>array('Provincia')),'ComprobanteReferencia','TipoAlicuotaAfip'),
                'conditions' => $conditions,
                'limit' => $this->numrecords,
                'page' => $this->getPageNum(),
                'order' => 'Comprobante.fecha_contable desc'
            );
            
      
    }
    
    
    private function getArrayQueryAgrupadoPorJurisdiccion($conditions){ /*este reporte va en moneda corriente*/
       
       $id_sistema = $this->getFromRequestOrSession('TipoComprobante.id_sistema');
       
    
      
       if($id_sistema !=""){
             
           array_push($conditions, array('TipoComprobante.id_sistema'=>$id_sistema ) );   
         
               $joins = array(  array(
                                "table"=>'tipo_comprobante',
                                //"type"=>'JOIN',
                                "alias"=>'TipoComprobante',
                                "conditions"=>array("TipoComprobante.id = Comprobante.id_tipo_comprobante")
                                ),
                                
                                array(
                                    'table' => 'persona',
                                    'alias' => 'Persona',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Persona.id = Comprobante.id_persona'
                                    )
                                 ),
                                 array(
                                    'table' => 'provincia',
                                    'alias' => 'Provincia',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Persona.id_provincia = Provincia.id'
                                    )
                                 )
                                 );

                        
       }else{
           
           $joins = "";
       
       } 
       
     
          return  array(
                'joins'=>$joins,
                 'fields'=>array('Provincia.id','Provincia.d_provincia','SUM(ROUND(ComprobanteImpuesto.importe_impuesto/Comprobante.valor_moneda2,2)) as monto'),
                'contain' =>array('Impuesto','Comprobante'=>array('TipoComprobante','PuntoVenta','Persona'=>array('Provincia')),'TipoAlicuotaAfip'),
                'conditions' => $conditions,
                'limit' => $this->numrecords,
                'page' => $this->getPageNum(),
                'group'=>'Provincia.id'
            );
            
      
    }
    
    
    /**
    * @secured(CONSULTA_COMPROBANTE_IMPUESTO)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);  
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros();
        
        
      
        $this->paginate = $this->getArrayQuery($conditions);
            
       
	  //$this->Security->unlockedFields = array('costo','precio'); 
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
        
        ////////////////////
        //Filters
        ///////////////////
        
	    //CUE
        $formBuilder->addFilterInput('codigo', 'C&oacute;digo', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $codigo));
        $formBuilder->addFilterInput('d_COMPROBANTE_IMPUESTO', 'Descripci&oacute;n', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $descripcion));
        $formBuilder->setDataListado($this, 'Listado de COPROBANTE_PUNTO_VENTA_NUMEROs', 'Datos de los COPROBANTE_PUNTO_VENTA_NUMEROs', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        //Headers
        $formBuilder->addHeader('id', 'COPROBANTE_PUNTO_VENTA_NUMERO.id', "10%");
        $formBuilder->addHeader('C&oacute;digo', 'COPROBANTE_PUNTO_VENTA_NUMERO.codigo', "20%");
        $formBuilder->addHeader('Precio', 'COPROBANTE_PUNTO_VENTA_NUMERO.precio', "20%");
        $formBuilder->addHeader('Descripci&oacute;n', 'COPROBANTE_PUNTO_VENTA_NUMERO.d_COMPROBANTE_IMPUESTO', "30%");
        
		//Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'codigo');
        $formBuilder->addField($this->model, 'precio');
        $formBuilder->addField($this->model, 'd_COMPROBANTE_IMPUESTO');
 
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }else{ // vista json 
        
        if($this->paginado == 1){
            $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
            $page_count = $this->params['paging'][$this->model]['pageCount'];
        }else{
            $parameters = $this->paginate;
            unset($parameters["limit"]);
            $data = $this->{$this->model}->find('all',$parameters);
            $page_count = 0;
           }
        
       //parseo el data para que los models queden dentro del objeto de respuesta
         foreach($data as &$valor){
             $this->cleanforOutput($valor);
         }
         
         $this->data = $data; 
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));  
     }
    //fin vista json
    }
    
    public function indexAgrupadoPorJurisdiccion() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);  
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros();
        
        
      
        $this->paginate = $this->getArrayQueryAgrupadoPorJurisdiccion($conditions);
            
       
          //$this->Security->unlockedFields = array('costo','precio'); 
      if($this->RequestHandler->ext != 'json'){  
       
        //vista formBuilder
    }else{ // vista json 
        
		$parameters = $this->paginate;
		unset($parameters["limit"]);
		$data = $this->{$this->model}->find('all',$parameters);
		$page_count = 0;
           
      
       //parseo el data para que los models queden dentro del objeto de respuesta
		if($data){
			foreach($data as &$valor){
             $this->cleanforOutputAgrupadoPorJurisdiccion($valor);
			}
		}
         
        $this->data = $data; 
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));  
     }
    //fin vista json
    }
	
	//MP:
	private function cleanforOutput(&$valor)
	  {
			$this->ComprobanteImpuesto->Comprobante->formatearFechas($valor);
			
            $valor['ComprobanteImpuesto']['d_impuesto'] = $valor['Impuesto']['d_impuesto'];
            $valor['ComprobanteImpuesto']['abreviado_impuesto'] = $valor['Impuesto']['abreviado_impuesto'];
            
            $valor['ComprobanteImpuesto']['autogenerado'] = $valor['Impuesto']['autogenerado'];
            
            $valor['ComprobanteImpuesto']['razon_social'] = $valor['Comprobante']['Persona']['razon_social'];
            
            $valor['ComprobanteImpuesto']['id_moneda'] = $valor['Comprobante']['id_moneda'];
            
            $valor['ComprobanteImpuesto']['d_provincia'] = $valor['Comprobante']['Persona']['Provincia']['d_provincia'];
            
             if(isset($valor['ComprobanteReferencia']) &&
			    $valor['ComprobanteReferencia']['nro_comprobante']>0 &&
			    isset($valor['ComprobanteReferencia']['id'])){
            	 $valor['ComprobanteImpuesto']['numero'] =  
					 (string) $this->ComprobanteImpuesto->Comprobante->GetNumberComprobante($valor['Comprobante']['PuntoVenta']['numero'],
																						    $valor['ComprobanteReferencia']['nro_comprobante']);
            	 $valor['ComprobanteImpuesto']['fecha'] = $valor['Comprobante']['fecha_contable'];
             }else{
			 
            	 unset($valor['ComprobanteReferencia']);
			 
			 }
			
            
            if(isset($valor['TipoAlicuotaAfip'])){
            	$valor[$this->model]['d_iva_detalle'] = $valor['TipoAlicuotaAfip']['d_iva_detalle'];
            	unset($valor['TipoAlicuotaAfip']);
            }else
            	$valor[$this->model]['d_iva_detalle']= '';
            
			
			
            $valor['ComprobanteImpuesto']['d_tipo_comprobante'] = $valor['Comprobante']['TipoComprobante']['d_tipo_comprobante'];
			
            if(isset($valor['Comprobante']['PuntoVenta']['numero']) && $valor['Comprobante']['PuntoVenta']['numero']>0)
                $valor[$this->model]['pto_nro_comprobante'] = (string) $this->ComprobanteImpuesto->Comprobante->GetNumberComprobante($valor['Comprobante']['PuntoVenta']['numero'],$valor['Comprobante']['nro_comprobante']);
            else
				$valor[$this->model]['pto_nro_comprobante'] = (string) $this->ComprobanteImpuesto->Comprobante->GetNumberComprobante($valor['Comprobante']['d_punto_venta'],$valor['Comprobante']['nro_comprobante']);
            
            
            $valor['ComprobanteImpuesto']['fecha_contable'] = $valor['Comprobante']['fecha_contable'];
             
            unset($valor['TipoComprobante']); 
			unset($valor['Comprobante']); 
			unset($valor['Impuesto']); 

		}
        
        
        
    private function cleanforOutputAgrupadoPorJurisdiccion(&$valor)
      {
			$valor['ComprobanteImpuesto']['d_provincia'] = $valor['Provincia']['d_provincia'];
            $valor['ComprobanteImpuesto']['importe_impuesto'] = $valor[0]["monto"];
            unset($valor["Provincia"]);
            unset($valor["Comprobante"]);
            unset($valor["Impuesto"]);
            unset($valor[0]);
	  }   
    
  
    /**
    * @secured(ADD_COMPROBANTE_IMPUESTO)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            
            $id_p = "";
            try{
                    if ($this->ComprobanteImpuesto->saveAll($this->request->data, array('deep' => true)))
                    {
                        $mensaje = "El Numero ha sido creado exitosamente";
                        $tipo = EnumError::SUCCESS;
                        $id_p = $this->ComprobanteImpuesto->id;
                       
                    }else
                    {
                        $mensaje = "Ha ocurrido un error,el Numero no ha podido ser creado.";
                        $tipo = EnumError::ERROR; 
                    }    
            }
            catch(Exception $e)
            {
                
                $mensaje = "Ha ocurrido un error,el Numero no ha podido ser creado .";
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" => $id_p
            );
                //si es json muestro esto
                if($this->RequestHandler->ext == 'json'){ 
                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content","id_add"));
                }else{
                    
                    $this->Session->setFlash($mensaje, $tipo);
                    $this->redirect(array('action' => 'index'));
                }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    
    
    /**
    * @secured(MODIFICACION_COMPROBANTE_IMPUESTO)
    */
    function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
          
            $this->ComprobanteImpuesto->id = $id;
            $this->request->data["ComprobanteImpuesto"]["fecha_ultima_modificacion"] = date("Y-m-d H:i:s");
            
          
          
            
            try{ 
                if ($this->ComprobanteImpuesto->saveAll($this->request->data["ComprobanteImpuesto"]) ){
                    
                    
                  $mensaje = "El Numero ha sido modificado exitosamente";
                  $error = EnumError::SUCCESS;
                                  
                                 
                    
                  
                }else{
                    
                     $mensaje = "Ha ocurrrido un error el Numero NO ha sido modificado exitosamente";
                     $error = EnumError::ERROR;
                }
            }catch(Exception $e){
                $mensaje = "Ha ocurrrido un error el Numero NO ha sido modificado exitosamente".$e->getMessage();
                $error = EnumError::ERROR;
                
            }
            
              if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $error,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $error);
                        $this->redirect(array('controller' => 'COPROBANTE_PUNTO_VENTA_NUMEROs', 'action' => 'index'));
                        
                    } 
            
            
           
        }else{
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->COPROBANTE_PUNTO_VENTA_NUMERO->id = $id;
            $this->COPROBANTE_PUNTO_VENTA_NUMERO->contain();
            $this->request->data = $this->COPROBANTE_PUNTO_VENTA_NUMERO->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
            }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
  
    
    /**
    * @secured(BAJA_COMPROBANTE_IMPUESTO)
    */
    function delete($id) {
        
        $this->loadModel($this->model);
     
        $mensaje = "";
        $status = "";
        $this->ComprobanteImpuesto->id = $id;
       try{  
           
            if ($this->ComprobanteImpuesto->delete()) {
            
             
        
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Impuesto ha sido eliminado exitosamente.";
               
                
            }
                
            else{
                 //throw new Exception();
                  $status = EnumError::ERROR;
                  $mensaje = "El Impuesto ha sido eliminado exitosamente.";
            }
             $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                 
          }
		  catch(Exception $ex)
		  { 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
    }
  
    
  
    
    
   
    
    
  
    
    
    
    
    
  
  
 
    
    
   
      /**
    * @secured(CONSULTA_COMPROBANTE_IMPUESTO)
    */
    public function getModel($vista = 'default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        
       
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
        
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $this->set('model',$model);
       // $this->set('this',$this);
        Configure::write('debug',0);
        $this->render($vista);
    
        
    }
    
    
 public function ReporteComprobanteImpuesto($vista='default'){
     

     $this->ExportarExcel($vista,"index",$this->getTituloReporte(""));
           
           
           
           
 }
 
 public function ReporteComprobanteImpuestoAgrupadoPorJurisdiccion($vista='impuesto_agrupado_por_jurisdiccion'){
     

     $this->ExportarExcel($vista,"indexAgrupadoPorJurisdiccion",$this->getTituloReporte("agrupado_jurisdiccion"));
           
           
           
           
 }
 
 public function getTituloReporte($texto_extra){
     
    $id_tipo_impuesto = $this->getFromRequestOrSession('Impuesto.id_tipo_impuesto'); 
    $id_impuesto = $this->getFromRequestOrSession('Comprobante.id_impuesto'); 
    $fecha_desde = $this->getFromRequestOrSession('Comprobante.fecha_contable'); //Hay q usar R para mandar datos para el controller Reporting
    $fecha_hasta = $this->getFromRequestOrSession('Comprobante.fecha_contable_hasta');
    
    

    $this->loadModel("TipoImpuesto");
    $this->loadModel("Impuesto");
    $d_tipo_impuesto = "Sin informar";
    if($id_tipo_impuesto!="")
        $d_tipo_impuesto = $this->TipoImpuesto->getDescripcion($id_tipo_impuesto);
     
    $d_impuesto = "Sin informar";   
    if($id_impuesto!="")    
        $d_impuesto = $this->Impuesto->getAbreviado($id_impuesto);
    
    return "Reporte del tipo impuesto (".$d_tipo_impuesto.") impuesto (".$d_impuesto.") Desde Fecha ".$fecha_desde." Hasta Fecha ".$fecha_hasta." ".$texto_extra;
    
     
 }
 
 
 public function getSiferePercepcionesTxt(){ /*recibe fecha_contable desde y hasta el id_impuesto*/
 
 /*Esto es para lo sufrido o sea compras percepciones en las facturas de compras*/
 		set_time_limit(180); 
     
 		$this->loadModel("ComprobanteImpuesto");
        //$conditions = $this->RecuperoFiltros(); no lo uso en este metodo
        
        $fecha_desde = $this->getFromRequestOrSession('Comprobante.fecha_contable'); //Hay q usar R para mandar datos para el controller Reporting
        $fecha_hasta = $this->getFromRequestOrSession('Comprobante.fecha_contable_hasta'); //
        $id_periodo_impositivo = $this->getFromRequestOrSession($this->model.'.id_periodo_impositivo'); //
        $ck_agrupado_jurisdiccion = $this->getFromRequestOrSession('Comprobante.ck_agrupado_jurisdiccion'); //para saber si el reporte agrupa o no por provincia
        $id_periodo_contable = $this->getFromRequestOrSession('Comprobante.id_periodo_contable'); //
        $anio_mes = $this->getFromRequestOrSession('Comprobante.anio_mes'); //A lo sumo se puede usar "R.factura.fecha_hasta"
        
        
        $conditions = array();
        /*En las Facturas para las PERCEPCIONES SUFRIDAS se toma la fecha de la factura (fecha_generacion), no importa la fecha contable*/
        
        if($anio_mes!=""){
        	
        	$array_fecha = explode("-",$anio_mes);
        	array_push($conditions, array('MONTH(Comprobante.fecha_contable)'=> $array_fecha[1] ));
        	array_push($conditions, array('YEAR(Comprobante.fecha_contable)'=>  $array_fecha[0] ));
        }
        
        if($id_periodo_contable !=""){
        	
        	$this->loadModel("PeriodoContable");
        	
        	$fecha_desde = $this->PeriodoContable->getFechaInicio($id_periodo_contable);
        	$fecha_hasta = $this->PeriodoContable->getFechaFin($id_periodo_contable);
        	array_push($conditions, array('Comprobante.fecha_contable >=' => $fecha_desde ));
        	array_push($conditions, array('Comprobante.fecha_contable <=' => $fecha_hasta));
        }      
        
        
        $nombre_archivo = $this->getFromRequestOrSession('Reporte.nombre_archivo');//1 global 2 por comprobante
       
        array_push($conditions, array('Impuesto.id_sub_tipo_impuesto' => EnumSubTipoImpuesto::PERCEPCION_IIBB ));
        array_push($conditions, array('Comprobante.id_estado_comprobante' => array(EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::CerradoConCae,EnumEstadoComprobante::Cumplida) ));
        
        
        array_push($conditions, array('ComprobanteImpuesto.importe_impuesto >' => 0));  

       
      array_push($conditions, array('TipoComprobante.id_sistema' => array(EnumSistema::COMPRAS,EnumSistema::CAJA)));
      
      
      
      
        
       
        $joins = array(  array(
        		"table"=>'tipo_comprobante',
        		//"type"=>'JOIN',
        		"alias"=>'TipoComprobante',
        		"conditions"=>array("TipoComprobante.id = Comprobante.id_tipo_comprobante")
        ),
        		
        	
        		
        );
        
        $resultados = $this->ComprobanteImpuesto->find('all',array(
        		'joins'=>$joins,
        		'conditions' =>$conditions,
        		'contain'=>array('Impuesto'=>array('Provincia'),'Comprobante'=>array('TipoComprobante','Persona'=>array("TipoDocumento","Provincia"),'PuntoVenta','Moneda'))
        ));
        
         
        
        
        $impuestos = '';
        $datoEmpresa = $this->Session->read('Empresa');
        $this->set('dato_empresa',$datoEmpresa);
        $this->set('datos',$resultados);
        $this->set('nombre_archivo',$nombre_archivo);
        Configure::write('debug',0);
        $this->layout = 'txt'; //esto usara el layout txt.ctp
        $this->response->type('txt');
        $this->render("SiferePercepcion");
            
     
     
 }
 
  public function getSifereRetencionesTxt(){ /*recibe fecha_contable desde y hasta el id_impuesto*/
     
      /*Esto es para lo sufrido, o sea las reteciones que me hicieron y se cargan en un recibo*/
  	 /*La fecha a tener en cuenta para los filtros es la fecha en que cargo el Recibo*/
  		$this->loadModel("ComprobanteImpuesto");
        $conditions = $this->RecuperoFiltros();
        set_time_limit(180); 
        
        
        $nombre_archivo = $this->getFromRequestOrSession('Reporte.nombre_archivo');//1 global 2 por comprobante
        $anio_mes = $this->getFromRequestOrSession('Comprobante.anio_mes'); //A lo sumo se puede usar "R.factura.fecha_hasta"
        $fecha_desde = $this->getFromRequestOrSession('ComprobanteImpuesto.fecha_contable'); //Hay q usar R para mandar datos para el controller Reporting
        $fecha_hasta = $this->getFromRequestOrSession('ComprobanteImpuesto.fecha_contable_hasta'); 
        $id_periodo_contable = $this->getFromRequestOrSession('Comprobante.id_periodo_contable'); 
        
        
        if($anio_mes!=""){
        	
        	$array_fecha = explode("-",$anio_mes);
        	array_push($conditions, array('MONTH(ComprobanteImpuesto.fecha)'=> $array_fecha[1] ));
        	array_push($conditions, array('YEAR(ComprobanteImpuesto.fecha)'=>  $array_fecha[0] ));
        }
        
        
        
        if($id_periodo_contable !=""){
        	
        	$this->loadModel("PeriodoContable");
        	
        	$fecha_desde = $this->PeriodoContable->getFechaInicio($id_periodo_contable);
        	$fecha_hasta = $this->PeriodoContable->getFechaFin($id_periodo_contable);
        	array_push($conditions, array('ComprobanteImpuesto.fecha >=' => $fecha_desde ));
        	array_push($conditions, array('ComprobanteImpuesto.fecha <=' => $fecha_hasta));
        }   
        
        if($fecha_desde!="")
        	array_push($conditions, array('ComprobanteImpuesto.fecha >=' => $fecha_desde ));
        
        		
        if($fecha_hasta!="")
        	array_push($conditions, array('ComprobanteImpuesto.fecha <=' => $fecha_hasta));   
       
        array_push($conditions, array('Comprobante.id_estado_comprobante' => array(EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::CerradoConCae) ));
        array_push($conditions, array('TipoComprobante.id' => array(EnumTipoComprobante::ReciboManual,EnumTipoComprobante::ReciboAutomatico) ));
        array_push($conditions, array('Impuesto.id_sub_tipo_impuesto' => EnumSubTipoImpuesto::RETENCION_IIBB ));
       
        //$this->filtros = $this->getArrayQuery($conditions,EnumSistema::COMPRAS);
        
        //array_push($conditions, array('TipoComprobante.id_sistema' => EnumSistema::VENTAS));  
        
        array_push($conditions, array('ComprobanteImpuesto.importe_impuesto >' => 0));  
        
        $joins = array(  array(
        		"table"=>'tipo_comprobante',
        		//"type"=>'JOIN',
        		"alias"=>'TipoComprobante',
        		"conditions"=>array("TipoComprobante.id = Comprobante.id_tipo_comprobante")
        ),
        		
        		array(
        				'table' => 'persona',
        				'alias' => 'Persona',
        				'type' => 'LEFT',
        				'conditions' => array(
        						'Persona.id = Comprobante.id_persona'
        				)
        		),
        		array(
        				'table' => 'provincia',
        				'alias' => 'Provincia',
        				'type' => 'LEFT',
        				'conditions' => array(
        						'Persona.id_provincia = Provincia.id'
        				)
        		)
        ); 

        $resultados = $this->ComprobanteImpuesto->find('all',array(
        		'joins'=>$joins,
        		'conditions' =>$conditions,
        		'contain'=>array('Impuesto'=>array('Provincia'),'Comprobante'=>array('TipoComprobante','Persona'=>array("TipoDocumento","Provincia"),'PuntoVenta','Moneda'))
        ));
         
        
        
        $impuestos = '';
        $this->set('datos',$resultados);
        $datoEmpresa = $this->Session->read('Empresa');
        $this->set('dato_empresa',$datoEmpresa);
        $this->set('nombre_archivo',$nombre_archivo);
        Configure::write('debug',0);
        $this->layout = 'txt'; //esto usara el layout txt.ctp
        $this->response->type('txt');
        $this->render("SifereRetencion");
            
     
     
 }
 
 
  public function getCitiTxt($id_sistema=""){ /*recibe fecha_contable desde y hasta el id_impuesto*/
  
        set_time_limit (120);
     
        $conditions = $this->RecuperoFiltros();
        
        $prorratear_credito_fiscal_computable = $this->getFromRequestOrSession('R.prorratear_credito_fiscal_computable');
        $tipo_prorrateo = $this->getFromRequestOrSession('R.tipo_prorrateo');//1 global 2 por comprobante
        
        $id_sistema_filtro = $this->getFromRequestOrSession('TipoComprobante.id_sistema');//1 global 2 por comprobante
        $nombre_archivo = $this->getFromRequestOrSession('Reporte.nombre_archivo');//1 global 2 por comprobante
        
        $array_id_tipo_comprobante = $this->getFromRequestOrSession('TipoComprobante.listado_id_tipo_comprobante');//recibo el string separado por coma con los id_tipo_comprobante
       
        /*Sino me lo mandas como parametro toma el del filtro*/ 
        if ( $id_sistema == "" )
                $id_sistema =  $id_sistema_filtro;
        
        
        $id_sistema_array = explode(",", $id_sistema); //soporta mas de un sistema ya que citi compras incluye gastos que es como si fuera una factura compra
        array_push($conditions, array('TipoComprobante.id_sistema' => $id_sistema_array ));
          
        if($id_sistema == EnumSistema::VENTAS)
            array_push($conditions, array('Comprobante.cae >' => 0 ));
        else
            array_push($conditions, array('Comprobante.id_estado_comprobante =' => array(EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::Cumplida,EnumEstadoComprobante::CerradoConCae) ));
        
            
            
         if($array_id_tipo_comprobante !='')
            array_push($conditions, array('Comprobante.id_tipo_comprobante' => explode(",", $array_id_tipo_comprobante)));
          
       
            	
        $this->loadModel("Comprobante");
        $resultados = $this->Comprobante->find( 'all',array(
                                                        'conditions' =>$conditions,
                                                        'contain'=>array('TipoComprobante','Persona'=>array("TipoDocumento"),'PuntoVenta','Moneda')
                                                        )
        );
         
   
        $this->set('datos',$resultados);
        $this->set('nombre_archivo',$nombre_archivo);
        $datoEmpresa = $this->Session->read('Empresa');
        $this->set('dato_empresa',$datoEmpresa);
        //$this->set('datos_extra',$datos_extra);
        Configure::write('debug',0);
        $this->layout = 'txt'; //esto usara el layout txt.ctp
        
        $this->response->type(array('txt' => 'text/plain'));
        $this->response->type('txt');
        
                
        if($id_sistema == EnumSistema::VENTAS)
            $this->render("CitiVentas");
        else
            $this->render("CitiCompras");
            
 }
 
  public function getCitiAlicuotasTxt($id_sistema=""){ /*recibe fecha_contable desde y hasta el id_impuesto*/
     
     
  	set_time_limit(180); 
  	$conditions = $this->RecuperoFiltros();
  	
  	$prorratear_credito_fiscal_computable = $this->getFromRequestOrSession('R.prorratear_credito_fiscal_computable');
  	$tipo_prorrateo = $this->getFromRequestOrSession('R.tipo_prorrateo');//1 global 2 por comprobante
  	
  	$id_sistema_filtro = $this->getFromRequestOrSession('TipoComprobante.id_sistema');//1 global 2 por comprobante
  	$nombre_archivo = $this->getFromRequestOrSession('Reporte.nombre_archivo');//1 global 2 por comprobante
  	
  	$array_id_tipo_comprobante = $this->getFromRequestOrSession('TipoComprobante.listado_id_tipo_comprobante');//recibo el string separado por coma con los id_tipo_comprobante
  	
  	/*Sino me lo mandas como parametro toma el del filtro*/
  	if ( $id_sistema == "" )
  		$id_sistema =  $id_sistema_filtro;
  		
  		
  		$id_sistema_array = explode(",", $id_sistema); //soporta mas de un sistema ya que citi compras incluye gastos que es como si fuera una factura compra
  		array_push($conditions, array('TipoComprobante.id_sistema' => $id_sistema_array ));
  		
  	if($id_sistema == EnumSistema::VENTAS)
  		array_push($conditions, array('Comprobante.cae >' => 0 ));
  	else
  	    array_push($conditions, array('Comprobante.id_estado_comprobante =' => array(EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::Cumplida,EnumEstadoComprobante::CerradoConCae) ));
  				
  				
  				
    if($array_id_tipo_comprobante !='')
  		array_push($conditions, array('Comprobante.id_tipo_comprobante' => explode(",", $array_id_tipo_comprobante)));
    
  		
        
       
       /*Se fuerza el impuesto ya que las alicuotas que se exportan son las de IVA*/ 
       if($id_sistema == EnumSistema::VENTAS){
        $id_impuesto = EnumImpuesto::IVANORMAL;
       }else
        $id_impuesto = EnumImpuesto::IVACOMPRAS; 

        
       array_push($conditions, array('ComprobanteImpuesto.id_impuesto' => $id_impuesto ));  
        
        $this->loadModel("Comprobante");

       $joins = array(  array(
                                "table"=>'tipo_comprobante',
                                //"type"=>'JOIN',
                                "alias"=>'TipoComprobante',
                                "conditions"=>array("TipoComprobante.id = Comprobante.id_tipo_comprobante")
                                ),
                                
                                array(
                                    'table' => 'persona',
                                    'alias' => 'Persona',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Persona.id = Comprobante.id_persona'
                                    )
                                 ),
                                 array(
                                    'table' => 'provincia',
                                    'alias' => 'Provincia',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Persona.id_provincia = Provincia.id'
                                    )
                                 )
                                 ); 
        
       $resultados = $this->ComprobanteImpuesto->find( 'all',array(
                                                         'joins'=>$joins,
                                                        'conditions' =>$conditions,
                                                        'contain'=>array("Comprobante"=>array("TipoComprobante","PuntoVenta","Persona"=>array("TipoDocumento")),"Impuesto")
       							
                                                        ));
       
       $conditions2 = $conditions;
       
       array_push($conditions2, array('ComprobanteImpuesto.tasa_impuesto' => 0 )); 
         
       $resultados2 = $this->ComprobanteImpuesto->find( 'all',array(
       		'fields'=>'SUM(ComprobanteImpuesto.importe_impuesto) as total_impuesto',
       		'joins'=>$joins,
       		'conditions' =>$conditions2,
       		'contain'=>array("Comprobante"=>array("TipoComprobante","PuntoVenta","Persona"=>array("TipoDocumento")),"Impuesto"),
       		'group'=>'ComprobanteImpuesto.id_comprobante'
       		
       ));
        
   
        $this->set('datos',$resultados);
        $this->set('impuestos_tasa_cero',$resultados2);
        $this->set('nombre_archivo',$nombre_archivo);
        $datoEmpresa = $this->Session->read('Empresa');
        $this->set('dato_empresa',$datoEmpresa);
        Configure::write('debug',0);
        $this->layout = 'txt'; //esto usara el layout txt.ctp
        //$id_sistema = EnumSistema::VENTAS; 
        
        $this->response->type(array('txt' => 'text/plain'));
        $this->response->type('txt');
        
        
        
        
        if($id_sistema == EnumSistema::VENTAS)
            $this->render("CitiVentasAlicuotas");
        else
           $this->render("CitiComprasAlicuotas"); 

     
 }
 
 
 public function getPercepcionesRetencionesCabaTxt($array_comprobantes = array(),$vista = "CabaPercepcionRetencion" ){
     
    /*son pertcepciones y retenciones hechas por la empresa*/
 	/*Este archivo va todo junto sino lo rechaza*/
 	/*Debe ir ordenado por fecha*/
 	/*Las notas de credito van en otro archivo*/
 	
 	set_time_limit(180); 
 	
 	
     $conditions = $this->RecuperoFiltros();
     $nombre_archivo = $this->getFromRequestOrSession('Reporte.nombre_archivo');//1 global 2 por comprobante
     
     

         
     array_push($conditions, array('ComprobanteImpuesto.id_impuesto' => array(EnumImpuesto::PERCEPIIBBCABA,EnumImpuesto::RETIIBBCABA) ));  
     array_push($conditions, array('ComprobanteImpuesto.importe_impuesto >' => 0));  
     array_push($conditions, array('IF(ComprobanteImpuesto.id_impuesto='.EnumImpuesto::PERCEPIIBBCABA.',Comprobante.cae >0,1)='=>1));//filtrar un campo por condicion en el where  
     
     
     //array_push($conditions, array('IF(ComprobanteImpuesto.id_impuesto ='.EnumImpuesto::PERCEPIIBBCABA.',Comprobante.cae>0)' ));  

     
     array_push($conditions, array('Comprobante.id_estado_comprobante' => array(EnumEstadoComprobante::CerradoConCae,EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::Cumplida) ));  
     
     
     if(count($array_comprobantes) == 0)
     	array_push($conditions, array('TipoComprobante.id' => array(EnumTipoComprobante::NotaDebitoAFacturaCredito,EnumTipoComprobante::NotaDebitoBFacturaCredito,EnumTipoComprobante::NotaDebitoCFacturaCredito,EnumTipoComprobante::FacturaCreditoA,EnumTipoComprobante::FacturaCreditoB,EnumTipoComprobante::FacturaCreditoC,EnumTipoComprobante::FacturaA,EnumTipoComprobante::FacturaB,EnumTipoComprobante::FacturaC,EnumTipoComprobante::FacturaM,EnumTipoComprobante::NotaDebitoA,EnumTipoComprobante::NotaDebitoB,EnumTipoComprobante::NotaDebitoC,EnumTipoComprobante::NotaDebitoM,EnumTipoComprobante::OrdenPagoManual,EnumTipoComprobante::OrdenPagoAutomatica) ));  
     else
     	array_push($conditions, array('TipoComprobante.id' => $array_comprobantes));
     
     	
     	
     	
     	
     	
     
     	
     	
     	
     $this->loadModel("Comprobante");

     $joins = array(  		array(
                                "table"=>'tipo_comprobante',
                                //"type"=>'JOIN',
                                "alias"=>'TipoComprobante',
                                "conditions"=>array("TipoComprobante.id = Comprobante.id_tipo_comprobante")
                                ), 
								
                            
							/*MP: FIX 06/03/2020 No va parece que no es necesario para hacer la 
							array( //MP: NUEVO JOIN
									'table' => 'comprobante_item',
									'alias' => 'ComprobanteItem',
									'type' => 'LEFT',
									'conditions' => array(
											'ComprobanteItem.id_comprobante = Comprobante.id'
									)),*/
		
		
							array(
								'table' => 'persona',
								'alias' => 'Persona',
								'type' => 'LEFT',
								'conditions' => array(
									'Persona.id = Comprobante.id_persona'
								)
							 ),
							 array(
								'table' => 'provincia',
								'alias' => 'Provincia',
								'type' => 'LEFT',
								'conditions' => array(
									'Persona.id_provincia = Provincia.id'
								)
							 )
                                 ); 
        
		
		
		
		
		/*DESDE EL ARCHIVO LO QUIERO TRAER DE ESTA FORMA*/
		
		/*$comprobante["Comprobante"]['ComprobanteItem'][0]["ComprobanteItemOrigen"]["Comprobante"]["nro_comprobante"] */
		/*comprobante["Comprobante"]['ComprobanteItem'][0]["ComprobanteItemOrigen"]["id"]*/
       $resultados = $this->ComprobanteImpuesto->find( 'all',array(
                                                         'joins'=>$joins,
                                                        'conditions' =>$conditions,
														//mp: PARA TRAER EL coMPROBANTE DE ORIGEN*/,
                                                        'contain'=>array("Comprobante" =>array( 'ComprobanteItem'=> array("ComprobanteItemOrigen"=>array("Comprobante")) ,
																								"TipoComprobante",
																								"PuntoVenta",
																								"Persona"=>array("TipoDocumento","SituacionIb")),
																		 "Impuesto",
																		 "ComprobanteReferencia"),
                                                        'order'=>array("Comprobante.fecha_contable ASC")
                                                        ));
         
        
        
   
        $this->set('datos',$resultados);
        $this->set('nombre_archivo',$nombre_archivo);
        $datoEmpresa = $this->Session->read('Empresa');
        $this->set('dato_empresa',$datoEmpresa);
        Configure::write('debug',0);
        $this->layout = 'txt'; //esto usara el layout txt.ctp
        //$id_sistema = EnumSistema::VENTAS; 
        
        $this->response->type(array('txt' => 'text/plain'));
        $this->response->type('txt'); 
         
         
         
        $this->render($vista);
   
 }
 
 
  public function getPercepcionesRetencionesCabaSoloNCTxt($id_reporte=""){
  	
  	
  	 set_time_limit(180); 
      
      if($id_reporte == "")
            $id_reporte = $this->getFromRequestOrSession('Reporte.id_reporte');
            
      $this->getPercepcionesRetencionesCabaTxt(
		array(EnumTipoComprobante::NotaCreditoA,
			  EnumTipoComprobante::NotaCreditoB,
			  EnumTipoComprobante::NotaCreditoC,
			  EnumTipoComprobante::NotaCreditoM),"CabaPercepcionRetencionNotaCredito" );
      return;
      
      
  }
  
  
  
  
  
  public function getPercepcionesSicoreIvaTxt(){
  	
  	
  		set_time_limit(180); 
  	
        $this->loadModel("Comprobante");
        $conditions = $this->RecuperoFiltros();
        $nombre_archivo = $this->getFromRequestOrSession('Reporte.nombre_archivo');//1 global 2 por comprobante  
        array_push($conditions, array('ComprobanteImpuesto.importe_impuesto >' => 0));  
      
      
  }
  
  
   public function getRetencionesSicoreIvaTxt(){
        
   	
   		set_time_limit(180); 
        $this->loadModel("Comprobante");
        $conditions = $this->RecuperoFiltros();
        $nombre_archivo = $this->getFromRequestOrSession('Reporte.nombre_archivo');//1 global 2 por comprobante  
        array_push($conditions, array('ComprobanteImpuesto.importe_impuesto >' => 0));  
        
        
        
        
      
      
  }
  
  
  public function getRetencionesSicoreGananciasTxt(){
  	
  	set_time_limit(180); 
  	$this->loadModel("Comprobante");
  	$conditions = $this->RecuperoFiltros();
  	$nombre_archivo = $this->getFromRequestOrSession('Reporte.nombre_archivo');//1 global 2 por comprobante
  	array_push($conditions, array('ComprobanteImpuesto.importe_impuesto >' => 0));
  	
  	$nombre_archivo = $this->getFromRequestOrSession('Reporte.nombre_archivo');//1 global 2 por comprobante
  	
  	
  	
  	
  	array_push($conditions, array('Impuesto.id_tipo_impuesto' => array(EnumTipoImpuesto::RETENCIONES) ));
  	array_push($conditions, array('Impuesto.id_sub_tipo_impuesto' => array(EnumSubTipoImpuesto::RETENCION_GANANCIA) ));
  	array_push($conditions, array('ComprobanteImpuesto.importe_impuesto >' => 0));
  	
  	array_push($conditions, array('Comprobante.id_estado_comprobante' => array(EnumEstadoComprobante::CerradoReimpresion) ));
  	array_push($conditions, array('TipoComprobante.id' => array(EnumTipoComprobante::OrdenPagoManual,EnumTipoComprobante::OrdenPagoAutomatica) ));
  		
  			$this->loadModel("Comprobante");
  			
  			$joins = array(  array(
  					"table"=>'tipo_comprobante',
  					//"type"=>'JOIN',
  					"alias"=>'TipoComprobante',
  					"conditions"=>array("TipoComprobante.id = Comprobante.id_tipo_comprobante")
  			),
  					
  					array(
  							'table' => 'persona',
  							'alias' => 'Persona',
  							'type' => 'LEFT',
  							'conditions' => array(
  									'Persona.id = Comprobante.id_persona'
  							)
  					),
  					array(
  							'table' => 'provincia',
  							'alias' => 'Provincia',
  							'type' => 'LEFT',
  							'conditions' => array(
  									'Persona.id_provincia = Provincia.id'
  							)
  					)
  			);
  			
  			$resultados = $this->ComprobanteImpuesto->find( 'all',array(
  					'joins'=>$joins,
  					'conditions' =>$conditions,
  					'contain'=>array("Comprobante"=>array("TipoComprobante","PuntoVenta","Persona"=>array("TipoDocumento","SituacionIb"),"ComprobanteItemComprobante"),"Impuesto","ComprobanteReferencia"),
  					'order'=>array("Comprobante.fecha_contable ASC")
  			));
  			
  			
  			
  			
  			$this->set('datos',$resultados);
  			$this->set('nombre_archivo',$nombre_archivo);
  			$datoEmpresa = $this->Session->read('Empresa');
  			$this->set('dato_empresa',$datoEmpresa);
  			Configure::write('debug',0);
  			$this->layout = 'txt'; //esto usara el layout txt.ctp
  			//$id_sistema = EnumSistema::VENTAS;
  			
  			$this->response->type(array('txt' => 'text/plain'));
  			$this->response->type('txt');
  			
  			
  			
  			$this->render("SicoreRetencion");
  	
  	
  	
  	
  	
  	
  }
  
  public function getPercepcionesRetencionesArbaTxt($id_reporte=""){
  	
  	//La fecha con la que corta es la fecha_contable de la factura/nota/etc Comprobante.fecha_contable
  		set_time_limit(180); 
       $this->loadModel("Comprobante");
       $conditions = $this->RecuperoFiltros();
        $nombre_archivo = $this->getFromRequestOrSession('Reporte.nombre_archivo');//1 global 2 por comprobante
        
       if($id_reporte == "")
            $id_reporte = $this->getFromRequestOrSession('Reporte.id_reporte');
       
       if($id_reporte == EnumReporte::ARBAPercepcionesIIBBVentaClientes){
            array_push($conditions, array('ComprobanteImpuesto.id_impuesto' => array(EnumImpuesto::PERCEPIIBBBUENOSAIRES) ));  
            array_push($conditions, array('TipoComprobante.id_sistema' => EnumSistema::VENTAS ));  
            array_push($conditions, array('Comprobante.id_estado_comprobante' =>array(EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::CerradoConCae,EnumEstadoComprobante::Cumplida)));
            array_push($conditions, array('Comprobante.cae  >' => 0));  
            
            $vista = "ArbaPercepcion";
       }     
       elseif($id_reporte == EnumReporte::ARBARetencionesCompraIIBBProveedores){
       	    array_push($conditions, array('ComprobanteImpuesto.id_impuesto' => array(EnumImpuesto::RETENCIONIIBBBSAS) )); 
            array_push($conditions, array('TipoComprobante.id_sistema' => array(EnumSistema::COMPRAS,EnumSistema::CAJA) )); 
            array_push($conditions, array('Comprobante.id_estado_comprobante' =>array(EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::CerradoConCae,EnumEstadoComprobante::Cumplida)));  
            $vista = "ArbaRetencion";  
       }     
            
       
       array_push($conditions, array('ComprobanteImpuesto.importe_impuesto >' => 0));  
       
       
       $joins = array(  array(
                                "table"=>'tipo_comprobante',
                                //"type"=>'JOIN',
                                "alias"=>'TipoComprobante',
                                "conditions"=>array("TipoComprobante.id = Comprobante.id_tipo_comprobante")
                                ),
                                
                                array(
                                    'table' => 'persona',
                                    'alias' => 'Persona',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Persona.id = Comprobante.id_persona'
                                    )
                                 ),
                                 array(
                                    'table' => 'provincia',
                                    'alias' => 'Provincia',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Persona.id_provincia = Provincia.id'
                                    )
                                 )
                                 ); 
        
       $resultados = $this->ComprobanteImpuesto->find( 'all',array(
                                                         'joins'=>$joins,
                                                        'conditions' =>$conditions,
                                                        'contain'=>array("Comprobante"=>
																			array("TipoComprobante",
																				  "PuntoVenta",
																				  "Persona"=>array("TipoDocumento","SituacionIb")),
																				  "Impuesto",
																				  "ComprobanteReferencia"=>array("PuntoVenta")),
                                                        'order'=>array("Comprobante.fecha_contable ASC")
                                                        ));
         
        
        
   
        $this->set('datos',$resultados);
        $this->set('nombre_archivo',$nombre_archivo);
        $datoEmpresa = $this->Session->read('Empresa');
        $this->set('dato_empresa',$datoEmpresa);
        Configure::write('debug',0);
        $this->layout = 'txt'; //esto usara el layout txt.ctp
        //$id_sistema = EnumSistema::VENTAS; 
        
        $this->response->type(array('txt' => 'text/plain'));
        $this->response->type('txt'); 
         
         
         
        $this->render($vista);
      
      
  }
  
  
 
 
 
 
 
     
    

    
}
?>