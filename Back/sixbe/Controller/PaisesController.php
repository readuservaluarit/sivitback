<?php

/**
* @secured(CONSULTA_PAIS)
*/
class PaisesController extends AppController {
    public $name = 'Paises';
    public $model = 'Pais';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

 /**
* @secured(CONSULTA_PAIS)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 50, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
        $d_pais = strtolower($this->getFromRequestOrSession('Pais.d_pais'));
		$id = strtolower($this->getFromRequestOrSession('Pais.id'));
        
        $conditions = array(); 
        
		if ($id != "") {
            $conditions = array('LOWER(Pais.id) LIKE' => '%' . $id . '%');
        }
		if ($d_pais != "") {
            $conditions = array('LOWER(Pais.d_pais) LIKE' => '%' . $d_pais . '%');
        }
		

        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
        	'maxLimit'=>$this->maxLimitRows,
            'page' => $this->getPageNum(),
        	'order'=> 'Pais.d_pais asc'
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de Pais', 'Datos de las Pais', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('nombre', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'Pais.id', "10%");
                $formBuilder->addHeader('Nombre', 'Pais.d_pais', "90%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_pais');

                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->Pais->id = $id;
            $this->request->data = $this->Pais->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Pais');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Pais');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Pais');
        else
            $formBuilder->setTituloForm('Consulta de Pais');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_pais', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));  
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'PaisAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('PaisDPais', 'Debe ingresar un nombre');


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

   /**
    * @secured(ADD_PAIS)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            
            try{
                if ($this->Paises->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "El Pais ha sido creada exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el Pais no ha podido ser creada.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => ""
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }

    
    /**
* @secured(MODIFICACION_PAIS)
*/ 
     public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->Pais->id = $id;
            
            try{ 
                if ($this->Pais->saveAll($this->request->data)){
                    
                    $error = EnumError::SUCCESS;
                    $mensaje = "El Pais ha sido modificado exitosamente";
                    
                  
                }else{
                    $error = EnumError::ERROR;
                    $mensaje = "Ha ocurrido un error, El Pais no ha podido modificarse."; 
                    
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, El Pais no ha podido modificarse.', 'error');
                $error = EnumError::ERROR;
                $mensaje = "Ha ocurrido un error, El Pais no ha podido modificarse.".$e->getMessage();
                
            }
            
        } 
        
        if($this->RequestHandler->ext != 'json'){
            $this->Session->setFlash($mensaje, $error);
            $this->redirect(array('controller' => 'Pais', 'action' => 'index'));
       } else{
            
             $output = array(
                            "status" => $error,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
              $this->set($output);
              $this->set("_serialize", array("status", "message", "content"));
        }
    }

    /**
    * @secured(BAJA_PAIS)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->Pais->id = $id;
     
       try{
            if ($this->Pais->delete() ) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Pais ha sido eliminada exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }

    
    
     /**
    * @secured(CONSULTA_PAIS)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    } 
    
   private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
      
      $this->set('model',$model);
      $this->set('model_name',$this->model);
      Configure::write('debug',0);
      $this->render($vista);
   
    }  

}
?>