<?php

App::uses('ComprobanteItemComprobantesController', 'Controller');

class OrdenPagoAnticipoItemsController extends ComprobanteItemComprobantesController {
    
	public $name = EnumController::OrdenPagoAnticipoItems;
    public $model = 'ComprobanteItemComprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    

     /**
    * @secured(CONSULTA_ANTICIPO_COMPRA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 30, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model); 
        
        $saldo = $this->getFromRequestOrSession('ComprobanteItemComprobante.saldos');
        $id_moneda = $this->getFromRequestOrSession('Comprobante.id_moneda');
         
       

        
         $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain'=>array('ComprobanteOrigen'=>array('PuntoVenta','TipoComprobante'),'Comprobante'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order'=>$this->{$this->model}->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_comprobante'))
        );
        
        
      if($this->RequestHandler->ext != 'json'){  
    
    
        //vista formBuilder
    } else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
                $comprobante = New Comprobante();
                
                foreach($data as &$producto ){    
                         //roducto["ComprobanteItemComprobante"]["d_producto"] = $producto["Producto"]["d_producto"];
                         $producto["ComprobanteItemComprobante"]["precio_unitario"] = $producto["ComprobanteItemComprobante"]["precio_unitario"];
						 // $producto["ComprobanteItemComprobante"]["nro_comprobante_origen"] = $producto["ComprobanteOrigen"]["nro_comprobante"];
                         $producto["ComprobanteItemComprobante"]["nro_comprobante_origen"] = $producto["ComprobanteOrigen"]["nro_comprobante"];
                         
                         if(isset($producto["ComprobanteOrigen"]["TipoComprobante"])){
                            $producto["ComprobanteItemComprobante"]["d_tipo_comprobante_origen"] = $producto["ComprobanteOrigen"]["TipoComprobante"]["codigo_tipo_comprobante"];
                            $producto["ComprobanteItemComprobante"]["simbolo_signo_comercial"] =  "+";
                             if($producto["ComprobanteOrigen"]["TipoComprobante"]["signo_comercial"] ==  -1)
                                 $producto["ComprobanteItemComprobante"]["simbolo_signo_comercial"] = "-";
                         }
                            
                         $producto["ComprobanteItemComprobante"]["fecha_generacion_origen"] = $producto["ComprobanteOrigen"]["fecha_generacion"];
                         $producto["ComprobanteItemComprobante"]["fecha_vencimiento_origen"] = $producto["ComprobanteOrigen"]["fecha_vencimiento"];
                         $producto["ComprobanteItemComprobante"]["total_comprobante_origen"] = $producto["ComprobanteOrigen"]["total_comprobante"];
                
                        
                         
                         if(isset($producto["ComprobanteOrigen"]["PuntoVenta"]["numero"]))
                            $id_punto_venta = $producto["ComprobanteOrigen"]["PuntoVenta"]["numero"];
                         else
                            $id_punto_venta = 0;
                            
                         $producto["ComprobanteItemComprobante"]["pto_nro_comprobante_origen"] = $this->ComprobanteItemComprobante->GetNumberComprobante($id_punto_venta,$producto["ComprobanteOrigen"]["nro_comprobante"]);

                         unset($producto["ComprobanteOrigen"]);
                         unset($producto["Comprobante"]);
            }
        }
        
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
     }
    //fin vista json
        
    
    
     /**
    * @secured(CONSULTA_ANTICIPO_COMPRA)
    */
    public function getModel($vista='default')
    {    
        $model = parent::getModelCamposDefault();
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
    }
    
    
    private function editforView($model,$vista)
    {  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
      $this->set('model',$model);
      Configure::write('debug',0);
      $this->render($vista);
    }
}
?>