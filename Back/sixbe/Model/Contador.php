<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class Contador extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'contador';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	/*public $hasMany = array(
		'Region' => array(
			'className' => 'Region',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public $childRecordMessage = "Existe por lo menos una regi&oacute;n, predio, establecimiento o usuario relacionado/a con la jurisdicci&oacute;n.";
    
    */
    
    
 public function getProximoNumero($id_contador,$suma ="+"){
     /*Obtiene el codigo a traves de un contador definido en la tabla TipoPersona*/
     
     try{
         
        
        
        if($id_contador > 0){
        $this->query("UPDATE contador SET numero_actual = LAST_INSERT_ID(numero_actual ".$suma." 1)  WHERE id=".$id_contador.";");
    
        $result = $this->query("SELECT LAST_INSERT_ID() as numero;");
        $numero = $result[0][0]["numero"];
        
        }else{
            return 0;
            
        }
        
    }catch(Exception $ex){
      // $numero_comprobante = 0;
      return 0;
   }
  
  return $numero; 
   
     
     
 }
 
 
 public function getNumeroActual($id_contador){
     
     
      $contador = $this->find('first', array('conditions'=>array('Contador.id'=>$id_contador)));      
      if( isset($contador["Contador"]["numero_actual"]) && !is_null($contador["Contador"]["numero_actual"]))
        return $contador["Contador"]["numero_actual"];
      else
        return  0;
 }
 
 
}
