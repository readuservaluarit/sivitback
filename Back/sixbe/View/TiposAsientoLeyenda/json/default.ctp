<?php
        // CONFIG  CON MODE DISPLAYED CELLS
        $model["d_tipo_asiento"]["show_grid"] = 1;
		$model["d_tipo_asiento"]["type"] = EnumTipoDato::cadena;
        $model["d_tipo_asiento"]["header_display"] = "Tipo Asiento";
        $model["d_tipo_asiento"]["minimun_width"] = "90"; //REstriccion mas de esto no se puede achicar
        $model["d_tipo_asiento"]["order"] = "0";
        $model["d_tipo_asiento"]["text_colour"] = "#0000EE";
		
       
        $model["d_tipo_asiento_leyenda"]["show_grid"] = 1;
        $model["d_tipo_asiento_leyenda"]["header_display"] = "Descripci&oacute;n";
        $model["d_tipo_asiento_leyenda"]["minimun_width"] = "90"; //REstriccion mas de esto no se puede achicar
        $model["d_tipo_asiento_leyenda"]["order"] = "2";
        $model["d_tipo_asiento_leyenda"]["text_colour"] = "#880055";
		

		$model["por_defecto"]["show_grid"] = 1;
        $model["por_defecto"]["header_display"] = "Por Defecto";
        $model["por_defecto"]["minimun_width"] = "90"; //REstriccion mas de esto no se puede achicar
        $model["por_defecto"]["order"] = "3";
        $model["por_defecto"]["text_colour"] = "#000000";
		
		


        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "TipoAsientoLeyenda",
                    "content" => $model
                );
        
        echo json_encode($output);
?>