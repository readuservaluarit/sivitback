<?php   //CONFIG  CON MODE DISPLAYED CELLS
        include(APP.'View'.DS.'ListaPrecioProductos'.DS.'json'.DS.'lista_precio_producto.ctp');

        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "C&oacute;digo";
        $model["codigo"]["order"] = "1";
		$model["codigo"]["width"] = "15";
        $model["codigo"]["text_colour"] = "#000000";
		$model["codigo"]["read_only"] = "1";
        
        $model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "Descripci&oacute;n";
        $model["d_producto"]["order"] = "2";
        $model["d_producto"]["width"] = "20";
        $model["d_producto"]["text_colour"] = "#000000";
		$model["d_producto"]["read_only"] = "1";
		
		$model["costo"]["show_grid"] = 1;
        $model["costo"]["header_display"] = "Costo";
        $model["costo"]["order"] = "3";
        $model["costo"]["width"] = "6";
        $model["costo"]["text_colour"] = "#440000";
		$model["costo"]["read_only"] = "1";
		
		$model["porcentaje_utilidad_minima"]["show_grid"] = 1;
        $model["porcentaje_utilidad_minima"]["header_display"] = "% Util M&iacute;nima";
        $model["porcentaje_utilidad_minima"]["order"] = "4";
        $model["porcentaje_utilidad_minima"]["width"] = "7";
        $model["porcentaje_utilidad_minima"]["text_colour"] = "#3300BB";
		
		$model["precio_minimo_por_utilidad"]["show_grid"] = 1;
        $model["precio_minimo_por_utilidad"]["header_display"] = "Precio M&iacute;nimo Por Util";
        $model["precio_minimo_por_utilidad"]["order"] = "5";
        $model["precio_minimo_por_utilidad"]["width"] = "10";
        $model["precio_minimo_por_utilidad"]["text_colour"] = "#2200EE";
		$model["precio_minimo_por_utilidad"]["read_only"] = "1"; //SOLO LECTURA
        
        $model["precio_minimo"]["show_grid"] = 1;
        $model["precio_minimo"]["header_display"] = "Precio M&iacute;nimo Manual";
        $model["precio_minimo"]["order"] = "6";
        $model["precio_minimo"]["width"] = "8";
        $model["precio_minimo"]["text_colour"] = "#0000FF";

        $model["precio"]["show_grid"] = 1;
        $model["precio"]["header_display"] = "Precio";
        $model["precio"]["order"] = "7";
        $model["precio"]["width"] = "8";
        $model["precio"]["text_colour"] = "#AA0011";
		
        $model["precio_final"]["show_grid"] = 1;
        $model["precio_final"]["header_display"] = "Precio Final";
        $model["precio_final"]["order"] = "8";
        $model["precio_final"]["width"] = "8";
		$model["precio_final"]["text_colour"] = "#770044";
		$model["precio_final"]["read_only"] = "1";
		
		$model["moneda_simbolo"]["show_grid"] = 0;//lo SACAMOS NO ES VISIBLE POR ES ES A NIVEL CABECERA
        $model["moneda_simbolo"]["header_display"] = "Moneda";
        $model["moneda_simbolo"]["order"] = "5";
        $model["moneda_simbolo"]["width"] = "4";
		$model["moneda_simbolo"]["text_colour"] = "#00AA00";
		$model["moneda_simbolo"]["read_only"] = "1";
        
     
       
         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ListaPrecioProducto",
                    "content" => $model
                );
        
          echo json_encode($output);
?>