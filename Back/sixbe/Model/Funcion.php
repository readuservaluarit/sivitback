<?php
class Funcion extends AppModel {
    public $name = "Funcion";
	public $useTable = "funcion";

    public $validate = array(
        'c_funcion' => array(
            'rule' => 'notEmpty',
			'message' => 'Debe ingresar un codigo.'
        ),
        'd_funcion' => array(
            'rule' => 'notEmpty',
			'message' => 'Debe ingresar un detalle.'
        )
    );
    
}