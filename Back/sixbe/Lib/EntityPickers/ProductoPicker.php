<?php
class ProductoPicker extends EntityPicker{
    
    public $model = "Producto";
    
    public $pickerTitle = "Selección de Items";
    
    public function getPickerJoins() {
        
            return array(       array(
                                'alias' => 'Unidad',
                                'table' => 'unidad',
                                'type' => 'LEFT',
                                'conditions' => '`Producto`.`id_unidad` = `Unidad`.`id`'
                                ),
                                array(
                                'alias' => 'Iva',
                                'table' => 'iva',
                                'type' => 'LEFT',
                                'conditions' => '`Producto`.`id_iva` = `Iva`.`id`'
                                )
                                
                          );
        
    }
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "5%", "keyOrder" => "id"),
                                  array("title" => "descripcion", "field" => "d_producto", "width" => "30%", "keyOrder" => "d_producto"),
                                  array("title" => "precio", "field" => "precio", "width" => "30%", "keyOrder" => "precio"),
                                  array("title" => "Unidad", "field" => "Unidad.d_unidad", "width" => "30%", "keyOrder" => "Unidad.d_unidad"),
                                  array("title" => "Iva", "field" => "Iva.d_iva", "width" => "5%", "keyOrder" => "Iva.d_iva")
                                );

    public function getPickerConditions($busqueda){
        
        return array('Producto.d_producto LIKE' => '%' . $busqueda['bus_unico'] . '%');
    }

}