<?php       include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');
             
			$model["id_comprobante"]["show_grid"] = 1;
			$model["id_comprobante"]["header_display"] = "Id.Cte.";
			$model["id_comprobante"]["order"] = "0";
			$model["id_comprobante"]["width"] = "3";

			$model["pto_nro_comprobante"]["show_grid"] = 1;
			$model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
			$model["pto_nro_comprobante"]["order"] = "1";
			$model["pto_nro_comprobante"]["width"] = "8";

			$model["codigo"]["show_grid"] = 1;
			$model["codigo"]["header_display"] = "[C&oacute;digo]";
			$model["codigo"]["order"] = "2";
			$model["codigo"]["width"] = "9";

			$model["razon_social"]["show_grid"] = 1;
			$model["razon_social"]["header_display"] = "Razon Social";
			$model["razon_social"]["order"] = "3";
			$model["razon_social"]["width"] = "8";

			$model["d_estado_comprobante"]["show_grid"] = 1;
			$model["d_estado_comprobante"]["header_display"] = "Estado";
			$model["d_estado_comprobante"]["order"] = "4";
			$model["d_estado_comprobante"]["width"] = "8";
			$model["d_estado_comprobante"]["text_colour"] = "#880022";//RED ORANGE

			$model["item_observacion"]["show_grid"] = 1;
			$model["item_observacion"]["header_display"] = "Item Observ.";
			$model["item_observacion"]["order"] = "5";            
			$model["item_observacion"]["width"] = "25";

			$model["cantidad"]["show_grid"] = 1;                      
			$model["cantidad"]["header_display"] = "Cantidad O.C";
			$model["cantidad"]["order"] = "6";            
			$model["cantidad"]["width"] = "5";
			$model["cantidad"]["text_colour"] = "#0000FF";//BLUE

			$model["cantidad_pendiente_a_remitir"]["show_grid"] = 1;
			$model["cantidad_pendiente_a_remitir"]["header_display"] = "Cant. Pend RM.";
			$model["cantidad_pendiente_a_remitir"]["order"] = "7";            
			$model["cantidad_pendiente_a_remitir"]["width"] = "5";
			$model["cantidad_pendiente_a_remitir"]["text_colour"] = "#AA0000";//RED

 $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItem",  /*Importante: Nombre del Model*/
                    "content" => $model
                );
        
          echo json_encode($output);
?>