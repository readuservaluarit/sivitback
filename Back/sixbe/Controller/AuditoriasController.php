<?php

/**
* @secured(CONSULTA_AUDITORIA)
*/
class AuditoriasController extends AppController {
    public $name = 'Auditorias';
    public $model = 'Auditoria';
    public $helpers = array ('Session', 'Paginator', 'Js', 'Time');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $defaultAuditModel = "Usuario";
  
    
    
    
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
         
        App::uses('ConnectionManager', 'Model');
        $dataSource = $this->Auditorias->getdatasource();
        
        $database = $dataSource->config['default'];
        
            
        $modelNames = $this->getModelNames($database);
        
        $modelAudit = $this->defaultAuditModel;

        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        $id_au = "";
        $au_c_operacion = "";
        $au_id_usuario = "";
        $au_fecha_desde = "";
        $au_fecha_hasta = "";
        
        $conditions = array(); 
        if ($this->request->is('post')) {

            $modelAudit =$this->request->data[$this->model]['model'];
            
            /*if($this->request->data[$this->model]['id_au']!=""){
                $id_au = $this->request->data[$this->model]['id_au'];
                array_push($conditions, array("{$this->model}.id_au = " => $id_au)); 
            }*/
            
            if($this->request->data[$this->model]['au_c_operacion']!=""){
                $au_c_operacion = $this->request->data[$this->model]['au_c_operacion'];
                array_push($conditions, array("{$this->model}.au_c_operacion = " => $au_c_operacion)); 
            }
            
            if($this->request->data[$this->model]['au_id_usuario']!=""){
                $au_id_usuario = $this->request->data[$this->model]['au_id_usuario'];
                array_push($conditions, array("{$this->model}.au_id_usuario = " => $au_id_usuario)); 
            }
            
            if($this->request->data[$this->model]['au_fecha_desde']!=""){
                $au_fecha_desde = $this->request->data[$this->model]['au_fecha_desde'];
                $exp = explode('/', $au_fecha_desde);
                $fecha = $exp[2].'-'.$exp[1].'-'.$exp[0];
                $fecha = date('Y-m-d', strtotime ( $fecha ));
                array_push($conditions, array("{$this->model}.au_fecha_hora >= " => $fecha)); 
            }
            
            if($this->request->data[$this->model]['au_fecha_hasta']!=""){
                $au_fecha_hasta = $this->request->data[$this->model]['au_fecha_hasta'];
                $exp = explode('/', $au_fecha_hasta);
                $fecha = $exp[2].'-'.$exp[1].'-'.$exp[0];
                 $fecha = date('Y-m-d', strtotime('+1 day', strtotime ( $fecha )));
                array_push($conditions, array("{$this->model}.au_fecha_hora <= " => $fecha)); 
            }

        }
        
        
        App::import('Model',$modelAudit);
        
        $objModelAudit = new $modelAudit();
        
        $model = new Model(1, $objModelAudit->useTable);
         
        $newModel = new Auditoria(1, $objModelAudit->useTable);
                
        unset($this->Auditoria);
        $this->Auditoria = $newModel;
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'fields' => '*',
            'conditions' => $conditions,
            'limit' => 10,
            'order' => array(
                                'Auditoria.id_au' => 'desc'
                            ),
            'page' => $this->getPageNum()
        );
      
      
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        
        $data = $this->prepareData($data);
      
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
        $formBuilder->setDataListado($this, 'Panel de Auditoría', 'Panel de Auditoría', $this->model, $this->name, $data);

        //Filters
        $formBuilder->addFilterBeginRow();
        
        $formBuilder->addFilterHtml("<div style='width:100%'></div>");
        
        $span = "span5";
       
        //Entidad
                 $formBuilder->addFilterInput('model', 'Entidad', array('class'=>"control-group {$span}"), array('style' => 'width:300px;z-index:999999999;','class' => '', 'label' => false, 'multiple' => false, 'options' => $modelNames, 'empty' => false, 'default' => $this->defaultAuditModel));
        
        $script = "
            $(function() {
            
                 var options = {multiple: false,header: '',noneSelectedText: false,selectedList: 1, width: '350px;', minWidth:'350px;', show:['slide', 500]};
                 var optionsfilter = {label: 'Buscar', width:'200px;', placeholder: ''};
                 sWidget = $('#{$this->model}Model').multiselect(options).multiselectfilter(optionsfilter);
            });
        ";
        
        $formBuilder->addCommonScript($script);
        
        $formBuilder->addFilterHtml("<div style='visibility:hidden;' class='control-group {$span}'></div>");
        
        //Operacion
        $operacionOpt = array('I'=>'Inserción', 'U' =>'Modificación', 'D'=>'Eliminación');
        $formBuilder->addFilterInput('au_c_operacion', 'Operación', array('class'=>"control-group {$span}"), array('class' => '', 'label' => false, 'options' => $operacionOpt, 'default' => $au_c_operacion, 'empty' => 'Todas'));
        

        //Usuario
        $au_id_usuario='';
        $au_d_usuario='';
        if ($this->request->is('post')) {
            $au_id_usuario=$this->request->data[$this->model]['au_id_usuario'];
            $au_d_usuario=$this->request->data[$this->model]['au_d_usuario'];
        }
        $selectionList = array($this->model."AuIdUsuario" => "id", $this->model."AuDUsuario" => "username");
        $formBuilder->addFilterPicker('UsuarioPicker', $selectionList, 'au_id_usuario', 
                                   array('value' => $au_id_usuario), 'au_d_usuario', 'Usuario', 
                                   array('class'=>"control-group {$span}"), array('class' => '', 'label' => false, 'value' => $au_d_usuario)
                                   ); 
                                   
        //Id
        //$formBuilder->addFilterInput('id_au', 'Id', array('class'=>"control-group {$span}"), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $id_au));
                                   
        //FechaDesde
        $formBuilder->addFilterInput('au_fecha_desde', 'Fecha Desde', array('class'=>"control-group {$span}"), array('type' => 'text', 'class' => 'datepicker', 'label' => false, 'value' => $au_fecha_desde));
        
        //FechaHasta
        $formBuilder->addFilterInput('au_fecha_hasta', 'Fecha Hasta', array('class'=>"control-group {$span}"), array('type' => 'text', 'class' => 'datepicker', 'label' => false, 'value' => $au_fecha_hasta));
         

        $formBuilder->addFilterEndRow();
        
        $valid = "
                    function validateFilter(){
                    
                        Validator.clearValidationMsgs('validationMsg_');
                
                        var form = 'AuditoriaIndexForm';
                        var validator = new Validator(form);
                        
                        validator.validateDate('AuditoriaAuFechaDesde', 'Fecha Desde');
                        validator.validateDate('AuditoriaAuFechaHasta', 'Fecha Hasta');
                        validator.validateDateSmaller('AuditoriaAuFechaDesde', '','AuditoriaAuFechaHasta','');
                        
                        
                        if(!validator.isAllValid()){
                            validator.showValidations('', 'validationMsg_');
                            return false;   
                        }
                        
                        return true;
                    
                    }
                 ";
                 
        $formBuilder->addCommonScript($valid);
        
        //Validaciones
        $formBuilder->setFilterValidationFunction('validateFilter()');
        
        //Headers
        $formBuilder->addHeader('Id Aud.', '', "10px");
        $formBuilder->addHeader('Operación', '', "200px");
        $formBuilder->addHeader('Fecha / Hora', '', "1800px");
        $formBuilder->addHeader('Usuario', '', "1800px");
        
        $fields = $newModel->schema() ;
        
        foreach($fields as $clave => $valor){
            
            if(!$this->esCampoAuditoria($clave)){
                
               $formBuilder->addHeader(Inflector::camelize($clave), '', "1800px"); 
                
            }
            
            
        }
        

        //Fields
        $formBuilder->addField($this->model, 'id_au');
        $formBuilder->addField($this->model, 'au_c_operacion');
        $formBuilder->addField($this->model, 'au_fecha_hora');
        $formBuilder->addField($this->model, 'au_usuario');
        
        
        foreach($fields as $clave => $valor){
            
            if(!$this->esCampoAuditoria($clave)){
               $formBuilder->addField($this->model, $clave);
                
            }
            
            
        }
        
        
        $formBuilder->showActionColumn(false);
        $formBuilder->showNewButton(false);
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    }
    
    
    private function esCampoAuditoria($campo){
        
        if($campo == "id_au" || $campo =="au_c_operacion" || $campo == "au_fecha_hora" || $campo == "au_usuario" || $campo == "au_id_usuario")
            return true;
        return false;
        
    }
    
    private function getModelNames($database){
        
         //Trae solamente los models que tienen creada en la base tabla de auditoria
        
         $models = App::objects('Model');
         
         $aux = array();
         
         foreach($models as $model){
             
             
             App::import('Model',$model);
             $m = new $model();

            if($m->useDbConfig == 'default'){ //Solo los que usan la conexion principal
				$aux[$m->useTable] = $model;
             
				unset($m);
			}
             
         }
         
         App::import('Model','Usuario');
         $mu = new Usuario();
         $tables =$mu->query("SELECT DISTINCT table_name
                                    FROM information_schema.columns
                                    WHERE table_schema = '{$database}' AND SUBSTRING(table_name, 1,3) = 'au_'
                                    ORDER BY table_name");
                                    
         $aux2 = array();
         foreach($tables as $table){
             
             $tableName = $table['columns']['table_name'];
             
             if(isset($aux[substr($tableName, 3, strlen($tableName))])){
                
                 $mod = $aux[substr($tableName, 3, strlen($tableName))];
             $aux2[$mod]=$mod; 
                 
             }
             
            
         }
         
         return $aux2;
         
         
    }
    
    private function prepareData($data){
        
        $aux = array();
        foreach($data as $dato){
            
            $reg = $dato;
            
            if($reg['Auditoria']['au_c_operacion'] == 'U'){
                $reg['Auditoria']['au_c_operacion'] = 'Modificación';
            }
            else  if($reg['Auditoria']['au_c_operacion'] == 'D'){
                $reg['Auditoria']['au_c_operacion'] = 'Eliminación';
            }
            else if($reg['Auditoria']['au_c_operacion'] == 'I'){
                $reg['Auditoria']['au_c_operacion'] = 'Inserción';
            }
            
            array_push($aux, $reg);
            
        }
        
        return $aux;
        
        
    }
    
   
}
?>