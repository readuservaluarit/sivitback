<?php
   
    /**
    * Hereda de IdDetalleAbmController y le agrega el comportamiento para el campo orden
    */
    
    App::uses('IdDetalleAbmController', 'Controller');
   
    class IdDetalleOrdenAbmController extends IdDetalleAbmController {
        
        
        public $orderField = 'orden';
        public $orderLabel = 'Orden';
        public $orderValidationMessage = "Debe ingresar un orden";
        public $orderNumericValidationMessage = "El campo orden debe ser numérico";
        
        public function addIndexHeaders($formBuilder){
            
            parent::addIndexHeaders($formBuilder);
            
            $formBuilder->addHeader($this->orderLabel, $this->model . '.' . $this->orderField, "10%");
            
        }
        
        public function addIndexFields($formBuilder){
            
            parent::addIndexFields($formBuilder);
         
            $formBuilder->addField($this->model, $this->orderField);
            
        }
        
        public function addFormFields($formBuilder){
            
            parent::addFormFields($formBuilder);
            
            $formBuilder->addFormInput($this->orderField, $this->orderLabel, array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));  

            
        }
        
        public function getFormValidations(){
            
            return parent::getFormValidations() . 
                    " validator.validateRequired('" . Inflector::camelize($this->model) . Inflector::camelize($this->orderField) ."', '". $this->orderValidationMessage ."');" .
                    " validator.validateNumeric('" . Inflector::camelize($this->model) . Inflector::camelize($this->orderField) ."', '". $this->orderNumericValidationMessage ."');";
            
        }


    }
?>