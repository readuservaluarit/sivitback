<?php       


        App::uses('ArticuloRelacion', 'Model');  
        $articulo_relacion_obj = new ArticuloRelacion();
        $articulo_relacion_obj->setVirtualFieldsForView();
     
     	App::import('Model','Comprobante');
  		App::import('Lib','SecurityManager');
		$comprobante = new Comprobante();
		$stock = $comprobante->getActivoModulo(EnumModulo::STOCK);
     
      
        $articulo_relacion_obj->SetFieldsToView($articulo_relacion_obj->virtual_fields_view,$model);

 
    
        
        
        $model["codigo_producto"]["show_grid"] = 1;
        $model["codigo_producto"]["header_display"] = "C&oacute;digo";
        $model["codigo_producto"]["order"] = "1";
        $model["codigo_producto"]["width"] = "15";
        $model["codigo_producto"]["text_colour"] = "#000000";
        

        $model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "Descripci&oacute;n";
        $model["d_producto"]["order"] = "3";
        $model["d_producto"]["width"] = "20";
        $model["d_producto"]["text_colour"] = "#0000FF";
        
        
     
        
        if($stock == 1){
        
        	$model["stock"]["show_grid"] = 1;
        	$model["stock"]["header_display"] = "Stock";
        	$model["stock"]["width"] = "10";
        	$model["stock"]["order"] = "4";
        	$model["stock"]["text_colour"] = "#0000FF";
        }
       
        
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => $model_name,
                    "content" => $model
                );
        echo json_encode($output);
?>