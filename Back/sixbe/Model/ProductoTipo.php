<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class ProductoTipo extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'producto_tipo';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'd_comprobante' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar una descripci&oacute;n.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	
	
	public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
		
		
		
		$this->virtual_fields_view = array(
				
				"d_deposito_fijo_principal"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_deposito_fijo_complementario"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null)
			
		);
		
	}     

	//The Associations below have been created with all possible keys, those that are not needed can be removed
 /*
 public $belongsTo = array(
        'TipoElemento' => array(
            'className' => 'TipoElemento',
            'foreignKey' => 'id_tipo_elemento',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'Agente' => array(
            'className' => 'Agente',
            'foreignKey' => 'id_agente',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
 );
 
 */
 
/**
 * hasMany associations
 *
 * @var array
 */
	/*public $hasMany = array(
		'Region' => array(
			'className' => 'Region',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public $childRecordMessage = "Existe por lo menos una regi&oacute;n, predio, establecimiento o usuario relacionado/a con la jurisdicci&oacute;n.";
    
    */
    
    
    public $belongsTo = array(
        'DestinoProducto' => array(
            'className' => 'DestinoProducto',
            'foreignKey' => 'id_destino_producto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
    		'DepositoFijoPrincipal' => array(
    				'className' => 'Deposito',
    				'foreignKey' => 'id_deposito_fijo_principal',
    				'dependent' => false,
    				'conditions' => '',
    				'fields' => '',
    				
    		),
    		'DepositoFijoComplementario' => array(
    				'className' => 'Deposito',
    				'foreignKey' => 'id_deposito_fijo_complementario',
    				'dependent' => false,
    				'conditions' => '',
    				'fields' => '',
    				
    		)
       
        );
    
}
