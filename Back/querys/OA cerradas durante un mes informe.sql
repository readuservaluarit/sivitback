SELECT comprobante.nro_comprobante AS nro_oa, persona.`razon_social`,producto.`codigo`, comprobante_item.`cantidad`, ROUND(comprobante_item.`cantidad_cierre`,0) AS cant_cierre,
comprobante.`fecha_cierre` fecha_cierre_oa,comprobante.fecha_entrega AS fecha_entrega_comp_armado,auditoria_comprobante.`fecha` AS fecha_completo


 FROM comprobante_item

LEFT JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`
LEFT JOIN comprobante_item piitem ON piitem.id = comprobante_item.`id_comprobante_item_origen`
LEFT JOIN comprobante pedido ON pedido.id = piitem.`id_comprobante`
LEFT JOIN persona ON pedido.`id_persona` = persona.`id`
LEFT JOIN producto ON producto.id = comprobante_item.`id_producto`
LEFT JOIN auditoria_comprobante ON auditoria_comprobante.`id_comprobante` = comprobante.`id`


WHERE comprobante.id_tipo_comprobante = 104
AND comprobante.fecha_cierre>='2018-09-01'

AND auditoria_comprobante.`id_estado_comprobante_nuevo` = 5
