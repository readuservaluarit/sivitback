<?php

App::uses('WordpressAppModel', 'Wordpress.Model');

class WordpressPost extends WordpressAppModel {

    public $useDbConfig = 'wordpress_database';
    public $name = 'WordpressPost';
    public $primaryKey = 'ID';
    public $useTable = 'six_wp_posts';

    //The Associations below have been created with all possible keys, those that are not needed can be removed
    public $belongsTo = array(
        'PostParent' => array(
            'className' => 'WordpressPost',
            'foreignKey' => 'post_parent',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ), 
        'User' => array(
            'className' => 'WordpressUser',
            'foreignKey' => 'post_author',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public $hasMany = array(
        'Postmetum' => array(
            'className' => 'WordpressPostmetum',
            'foreignKey' => 'post_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'TermRelationship' => array(
            'className' => 'WordpressTermRelationship',
            'foreignKey' => 'object_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Comment' => array(
            'className' => 'WordpressComment',
            'foreignKey' => 'comment_post_ID',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
?>