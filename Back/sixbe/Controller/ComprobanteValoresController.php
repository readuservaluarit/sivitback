<?php



class ComprobanteValoresController extends AppController {
    public $name = 'ComprobanteValores';
    public $model = 'ComprobanteValor';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
 

protected  function RecuperoFiltros($model)
{
        //Recuperacion de Filtros
        $id_comprobante = $this->getFromRequestOrSession('ComprobanteValor.id_comprobante');
        $id_sistema = $this->getFromRequestOrSession('TipoComprobante.id_sistema');
        $id_valor = $this->getFromRequestOrSession('ComprobanteValor.id_valor');  
        $fecha_generacion = $this->getFromRequestOrSession('Comprobante.fecha_generacion');
        $fecha_generacion_hasta = $this->getFromRequestOrSession('Comprobante.fecha_generacion_hasta');
        $paginado = $this->getFromRequestOrSession('ComprobanteValor.paginado');
        $nro_comprobante = $this->getFromRequestOrSession('Comprobante.nro_comprobante');
        $id_persona = $this->getFromRequestOrSession('Comprobante.id_persona');
        $razon_social = $this->getFromRequestOrSession('Comprobante.persona_razon_social');
        $persona_codigo= $this->getFromRequestOrSession('Comprobante.persona_codigo');
        $id_estado_comprobante = $this->getFromRequestOrSession('Comprobante.id_estado_comprobante');
        $orden_compra_externa = $this->getFromRequestOrSession('Comprobante.orden_compra_externa');
        $id_punto_venta = $this->getFromRequestOrSession('Comprobante.id_punto_venta');
        $d_punto_venta = $this->getFromRequestOrSession('Comprobante.d_punto_venta');
        $fecha_contable = $this->getFromRequestOrSession('Comprobante.fecha_contable');
        $fecha_contable_hasta = $this->getFromRequestOrSession('Comprobante.fecha_contable_hasta');
        
         if($paginado!="")
            $this->paginado = $paginado;
      
        $conditions = array(); 
        
         if($id_comprobante!="")
            array_push($conditions, array('ComprobanteValor.id_comprobante ' => $id_comprobante ));
            
          if($id_valor!="")
            array_push($conditions, array('ComprobanteValor.id_valor ' => $id_valor ));
          
            
         if($fecha_generacion!=""){
            array_push($conditions, array('Comprobante.fecha_generacion >=' => $fecha_generacion)); //MP: Si la fecha de bd tiene hh:mm:ss distintos de 00:00:00 la compoaracion no anda
            array_push($this->array_filter_names,array("Fecha Generaci&oacute;n Dsd.:"=>$this->{$this->model}->formatDate($fecha_generacion)));
         }
         if($fecha_generacion_hasta!=""){
            	array_push($conditions, array('Comprobante.fecha_generacion <=' => $fecha_generacion_hasta)); //MP: Si la fecha de bd tiene hh:mm:ss distintos de 00:00:00 la compoaracion no anda
            	array_push($this->array_filter_names,array("Fecha Generaci&oacute;n Hta.:"=>$this->{$this->model}->formatDate($fecha_generacion)));
         }
         
         if($nro_comprobante!=""){
            	$nros_comprobante = explode(',', $nro_comprobante);
            	array_push($conditions, array('Comprobante.nro_comprobante' => $nros_comprobante));
            	array_push($this->array_filter_names,array("Nro. Comprobante:"=>$nro_comprobante));
          }
         
         if($id_persona!="")
            	array_push($conditions, array('Comprobante.id_persona' => $id_persona));
         
         if($razon_social!=""){
            		array_push($conditions, array('Comprobante.razon_social LIKE ' => '%' .$razon_social.'%'));
            		array_push($this->array_filter_names,array("Rzn. Social:"=>$razon_social));
         } 
         
         
         if($orden_compra_externa !=""){
         	array_push($conditions, array('Comprobante.orden_compra_externa LIKE ' => '%' .$orden_compra_externa.'%'));
         	array_push($this->array_filter_names,array("OC Externa:"=>$orden_compra_externa));
         } 
         
         if($id_punto_venta!="")
         	array_push($conditions, array('Comprobante.id_punto_venta' =>  $id_punto_venta ));
         	
         	
         if($d_punto_venta!="")
         	array_push($conditions, array('Comprobante.d_punto_venta LIKE' =>   '%' .$d_punto_venta.'%' ));  
            		
          if($persona_codigo !=""){
         		array_push($conditions, array('Persona.codigo  LIKE ' => '%' .$persona_codigo.'%'));
         		array_push($this->array_filter_names,array("Cod Pers.:"=>$persona_codigo));
          }
         		
         if($id_sistema !="")
         		array_push($conditions, array('TipoComprobante.id_sistema' => $id_sistema));
         
         		
         		
        if($fecha_contable !=""){
        	array_push($conditions, array('Comprobante.fecha_contable >=' => $fecha_contable)); //MP: Si la fecha de bd tiene hh:mm:ss distintos de 00:00:00 la compoaracion no anda
        	array_push($this->array_filter_names,array("Fecha Contable Dsd:"=>$this->{$this->model}->formatDate($fecha_contable)));
        }
         			
        
        if($fecha_contable_hasta!=""){
        	array_push($conditions, array('Comprobante.fecha_contable <=' => $fecha_contable_hasta)); //MP: Si la fecha de bd tiene hh:mm:ss distintos de 00:00:00 la compoaracion no anda
        	array_push($this->array_filter_names,array("Fecha Contable Hta:"=>$this->{$this->model}->formatDate($fecha_contable_hasta)));
        }				
         			
       if($id_estado_comprobante!="")
       		array_push($conditions, array('Comprobante.id_estado_comprobante' =>  $id_estado_comprobante));
       
       		
        return $conditions;
}    

   /**
    * @secured(CONSULTA_COMPROBANTE_VALOR)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->loadModel("Comprobante");
        
        $this->PaginatorModificado->settings = array('limit' => 100000, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model); 
        
        
        $chkRecibidos  = $this->getFromRequestOrSession('ComprobanteValor.chkRecibidos');
        $chkEntregados = $this->getFromRequestOrSession('ComprobanteValor.chkEntregados');
        
        $comprobantes_ingreso = array(EnumTipoComprobante::ReciboManual,EnumTipoComprobante::ReciboAutomatico,EnumTipoComprobante::IngresoCaja);
        $comprobantes_egreso = array(EnumTipoComprobante::OrdenPagoManual,EnumTipoComprobante::OrdenPagoAutomatica,EnumTipoComprobante::EgresoCaja,EnumTipoComprobante::OrdenPagoAnticipo,EnumTipoComprobante::OrdenPagoRenGasto);
        
        
        $comprobantes_filtro = array();
        
        if($chkRecibidos == 1 && $chkEntregados == 1)
        	$comprobantes_filtro = array_merge($comprobantes_ingreso,$comprobantes_egreso);
        elseif ($chkRecibidos == 1)
        	$comprobantes_filtro = $comprobantes_ingreso;
        elseif( $chkEntregados == 1)
        	$comprobantes_filtro = $comprobantes_egreso;
        
        	
        
        if($chkRecibidos == 1  || $chkEntregados == 1)
        	array_push($conditions, array('Comprobante.id_tipo_comprobante'=> $comprobantes_filtro));
        
        if($chkRecibidos == 1)//agrego los comprobantes de Ventas en los cuales se ingresan Valores
        {
        	array_push($conditions, array('NOT'=>array('Comprobante.id_estado_comprobante' =>array(EnumEstadoComprobante::Anulado,EnumEstadoComprobante::Cancelado)) ));
        	array_push($conditions, array('Comprobante.definitivo'=> 1 ));
        	
        }
        
        	
        if($chkEntregados== 1)//agrego los comprobantes de Ventas en los cuales se ingresan Valores
        {
        	array_push($conditions, array('NOT'=>array('Comprobante.id_estado_comprobante' =>array(EnumEstadoComprobante::Anulado,EnumEstadoComprobante::Cancelado)) ));
        	array_push($conditions, array('Comprobante.definitivo'=> 1 ));
        
        	
        }
        	
        
        $joins =array(
        		array(
        				'table' => 'tipo_comprobante',
        				'alias' => 'TipoComprobante',
        				'type' => 'LEFT',
        				'conditions' => array(
        						'TipoComprobante.id = Comprobante.id_tipo_comprobante'
        				)
        				
        		));
    
        $this->paginate = array(
        		'paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
        		'joins'=>$joins,
        		'contain'=> array('Comprobante'=>array('PuntoVenta','Persona','EstadoComprobante','TipoComprobante','DetalleTipoComprobante','CarteraRendicion'),'Cheque'=>array('Banco','BancoSucursal'=>array('Banco'),'Chequera'=>array('CuentaBancaria'=>array('BancoSucursal'=>array("Banco")))),'Valor','Moneda','CuentaBancaria'=>array('BancoSucursal'=>array('Banco')),'TarjetaCredito'=>array('CuentaBancaria') ),
            	'conditions' => $conditions,
            	'limit' => $this->numrecords,
            	'page' => $this->getPageNum()
        );
        
     
    

    	$this->PaginatorModificado->settings = $this->paginate;
    	$data = $this->PaginatorModificado->paginate($this->model);
    	$page_count = $this->params['paging'][$this->model]['pageCount'];
      
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$producto )
        {
                 $producto["ComprobanteValor"]["d_valor"] = $producto["Valor"]["d_valor"];
                 
                 
                 
                 if( isset($producto["Comprobante"]["PuntoVenta"]["id"] ))
                 	$producto[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($producto['Comprobante']['PuntoVenta']['numero'], $producto['Comprobante']['nro_comprobante']);
                 else
                 	$producto[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($producto['Comprobante']['d_punto_venta'], $producto['Comprobante']['nro_comprobante']);
                 
                 	
                
                 	if(isset($producto['Comprobante']['fecha_generacion']))
                 		$producto[$this->model]['fecha_generacion'] = (string) $producto['Comprobante']['fecha_generacion'];
                 		
                 		if(isset($producto['Comprobante']['fecha_contable']))
                 			$producto[$this->model]['fecha_contable'] = (string) $producto['Comprobante']['fecha_contable'];
                 		
                 			
                 			
                 	$producto[$this->model]['codigo_tipo_comprobante'] = (string) $producto['Comprobante']['TipoComprobante']['codigo_tipo_comprobante'];
                 	
                 	if(isset($producto['Comprobante']['Persona']['razon_social']))
                 		$producto[$this->model]['razon_social'] = (string) $producto['Comprobante']['Persona']['razon_social'];
                 	
                 	if(isset($producto['Comprobante']['DetalleTipoComprobante']['id']) && $producto['Comprobante']['DetalleTipoComprobante']['id']>0)
                 			$producto[$this->model]['razon_social'] = (string) $producto['Comprobante']['DetalleTipoComprobante']['d_detalle_tipo_comprobante'];
                 			
                 	if(isset($producto['Comprobante']['CarteraRendicion']['id']) && $producto['Comprobante']['CarteraRendicion']['id']>0)
                 			$producto[$this->model]['razon_social'] = (string) $producto['Comprobante']['CarteraRendicion']['d_cartera_rendicion'];
                 		
                 		
                 	if(isset($producto['Comprobante']['EstadoComprobante']))
                 		$producto[$this->model]['d_estado_comprobante'] = (string) $producto['Comprobante']['EstadoComprobante']['d_estado_comprobante'];
                    /*
                 if(isset($producto["Moneda"])
                  && isset($producto["Moneda"]["id"])) //MP: chequeo extra de cheque
                 {*/
                    $producto["ComprobanteValor"]["d_moneda"] = $producto["Moneda"]["d_moneda"]; 
                 /*}    */
                 if( $producto["ComprobanteValor"]["id_valor"] == EnumValor::CHEQUE && isset($producto["Cheque"])
                  && isset($producto["Cheque"]["id"])) //MP: chequeo extra de cheque
                 {
                    
                     if($producto['Cheque']['id_tipo_cheque'] ==  EnumTipoCheque::Terceros){
                         
                     	
                     	$producto["ComprobanteValor"]["d_valor"] =  $producto["ComprobanteValor"]["d_valor"].' '. ' TERCEROS';
                        
                     	if( isset($producto["Cheque"]["Banco"]["id"]) && $producto["Cheque"]["Banco"]["id"]>0)
                     		$producto["ComprobanteValor"]["d_banco"] =          $producto["Cheque"]["Banco"]["d_banco"];
                         
                     	if( isset($producto["Cheque"]["BancoSucursal"]["d_banco_sucursal"]) && $producto["Cheque"]["BancoSucursal"]["id"]>0)
                         	$producto["ComprobanteValor"]["d_banco_sucursal"] = $producto["Cheque"]["BancoSucursal"]["d_banco_sucursal"];
                     		
                         
                         	$producto["ComprobanteValor"]["nro_cheque"] =       $producto["Cheque"]["nro_cheque"];   
                         	$producto["ComprobanteValor"]["fecha_valor"] =       $producto["Cheque"]["fecha_cheque"];   
                         
                     }else{
                         
                     	$producto["ComprobanteValor"]["d_valor"] =  $producto["ComprobanteValor"]["d_valor"].' '. ' PROPIO';
                     	if(isset($producto["Cheque"]["Chequera"]["CuentaBancaria"]) && $producto["Cheque"]["Chequera"]["CuentaBancaria"]["id"]>0){
                     	 
                     		$producto["ComprobanteValor"]["d_banco"] =          $producto["Cheque"]["Chequera"]["CuentaBancaria"]["BancoSucursal"]["Banco"]["d_banco"];
                         	$producto["ComprobanteValor"]["d_banco_sucursal"] = $producto["Cheque"]["Chequera"]["CuentaBancaria"]["BancoSucursal"]["d_banco_sucursal"];
                     	} 
                         
                         
                         $producto["ComprobanteValor"]["nro_cheque"] =       $producto["Cheque"]["nro_cheque"];  
                         $producto["ComprobanteValor"]["fecha_valor"] =       $producto["Cheque"]["fecha_cheque"];  
                         
                         
                     }
                     
                     
                 } 
                 
                 
                 if( in_array($producto["ComprobanteValor"]["id_valor"],
					   array(EnumValor::TRANSFERENCIAS,EnumValor::EXTRACCION,
							 EnumValor::DEPOSITOS)) && isset($producto["CuentaBancaria"])
                  && isset($producto["CuentaBancaria"]["id"])) //MP: chequeo extra de cheque
                 {
                 	
                 	if(isset($producto["CuentaBancaria"]["BancoSucursal"]["Banco"]["id"]) && $producto["CuentaBancaria"]["BancoSucursal"]["Banco"]["id"] >0){
                     $producto["ComprobanteValor"]["d_banco"] =          $producto["CuentaBancaria"]["BancoSucursal"]["Banco"]["d_banco"];
                     $producto["ComprobanteValor"]["d_banco_sucursal"] = $producto["CuentaBancaria"]["BancoSucursal"]["d_banco_sucursal"];
                     $producto["ComprobanteValor"]["nro_cuenta"] = 		 $producto["CuentaBancaria"]["nro_cuenta"];
                     
                 	}else{
                    	$producto["ComprobanteValor"]["d_banco"] = "";
                    	$producto["ComprobanteValor"]["d_banco_sucursal"] = "";
                 	}
                 	
                 	
                 
                 } 
                 
                 
                 
                 if( in_array($producto["ComprobanteValor"]["id_valor"],
                 		array(EnumValor::TARJETA))
                 		&& isset($producto["TarjetaCredito"]["id"])) //MP: chequeo extra de cheque
                 {
                 	
                 
                 	if(isset($producto["TarjetaCredito"]["id"]) && $producto["TarjetaCredito"]["id"] >0){
                 		$producto["ComprobanteValor"]["nro_tarjeta"] =          $producto["TarjetaCredito"]["nro_tarjeta"];
                 		$producto["ComprobanteValor"]["d_banco_sucursal"] = $producto["TarjetaCredito"]["CuentaBancaria"]["nro_cuenta"];
                 		
                 		
                 	}
                 } 
                 unset($producto["Valor"]);
                 unset($producto["Cheque"]);
				 unset($producto["CuentaBancaria"]);
				 unset($producto["TarjetaCredito"]);
				 unset($producto["Comprobante"]);
				 unset($producto["Moneda"]);
				 //unset($producto["Moneda"]);
        }
        
        
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     
    //fin vista json
    }
    
    protected function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            
            try{
                if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "El Item ha sido creado exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.";
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => ""
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    protected function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                    
                    $mensaje = "El Item Ha sido modificado correctamente.";
                    $tipo = EnumError::SUCCESS;
                    
                }else{
                    $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.";
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
              
                $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.".$e->getMessage();
                $tipo = EnumError::ERROR; 
                
            }
          
        } 
        if($this->RequestHandler->ext == 'json'){  
            $output = array(
                "status" => $tipo,
                "message" =>$mensaje,
                "content" => ""
            ); 
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content"));
        }else{
            $this->Session->setFlash($mensaje, $tipo);
             $this->redirect(array('controller' => 'CotizacionItems', 'action' => 'index'));
        
        } 
      
    }
    
  
    
    

   
    
    
      /**
    * @secured(CONSULTA_ITEM_COMPROBANTE)
    */
    public function getModel($vista = 'default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        
       
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
        
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $this->set('model',$model);
       // $this->set('this',$this);
        Configure::write('debug',0);
        $this->render($vista);
    
        
    }
    
    
    /**
    * @secured(BAJA_COMPROBANTE_VALOR)
    */
    function delete($id) {
        
        $this->loadModel($this->model);
     
        $mensaje = "";
        $status = "";
        $this->ComprobanteValor->id = $id;
       try{  
       	
       	
      $comprobante_valor = $this->{EnumModel::ComprobanteValor}->find('first', array(
       			'conditions' => array(
       					'ComprobanteValor.id' => $id
       			) ,
       			'contain' => array("Comprobante")));
       			
       			
       			
       			
       			
       	if($comprobante_valor["Comprobante"]["definitivo"] == 0){
            if ($this->ComprobanteValor->delete()) {
            
             
        
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Valor ha sido eliminado exitosamente.";
               
                
            }
                
            else{
                 //throw new Exception();
                  $status = EnumError::ERROR;
                  $mensaje = "El Valor no ha podido ser eliminado.";
            }
            
            
       	}else{
       		
       		
       		$status = EnumError::ERROR;
       		$mensaje = "El Valor no ha podido ser eliminado ya que el comprobante es definitivo";
       		
       		
       	}
             $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                 
          }
          catch(Exception $ex)
          { 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
    }
    
    /**
     * @secured(BTN_EXCEL_COMPROBANTEVALORES)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Valores");
    	
    	
    	
    }
    
    
    
    public function getConciliacionBancariaOLD(){
    	
    	$this->loadModel("Comprobante");
    	$this->loadModel("ComprobanteValor");
    	
    	
    	
    	$fecha_inicio = strtolower($this->getFromRequestOrSession('Comprobante.fecha_contable'));
    	$fecha_fin = strtolower($this->getFromRequestOrSession('Comprobante.fecha_contable_hasta'));
    	$conciliado = strtolower($this->getFromRequestOrSession('Cheque.conciliado'));
    	$id_cuenta_bancaria = strtolower($this->getFromRequestOrSession('ComprobanteValor.id_cuenta_bancaria'));
    	
    	$conditions = array();
    	array_push($conditions, array('Comprobante.definitivo'=> 1 ));
    	array_push($conditions, array('NOT'=>array('Comprobante.id_estado_comprobante' =>array(EnumEstadoComprobante::Anulado,EnumEstadoComprobante::Cancelado)) ));

    	
    	array_push($conditions, array('ComprobanteValor.id_valor'=> array(EnumValor::DEPOSITOS,EnumValor::TRANSFERENCIAS,EnumValor::TARJETA) ));

    	
    	
    	if($id_cuenta_bancaria != ""){/*Me envia la cuenta bancaria entonces es para conciliar*/
    		
    		/*En un ingreso solo tengo cheques de Terceros*/
    		array_push($conditions, array('IF(ComprobanteValor.id_valor='.EnumValor::TARJETA.' ,TarjetaCredito.id_cuenta_bancaria='.$id_cuenta_bancaria.',1)'=>1));//filtrar un campo por condicion en el where
    		
    		array_push($conditions, array('IF(ComprobanteValor.id_valor IN('.EnumValor::DEPOSITOS.','.EnumValor::TRANSFERENCIAS.') ,ComprobanteValor.id_cuenta_bancaria='.$id_cuenta_bancaria.',1)='=>1));//filtrar un campo por condicion en el where
    		
    		
    		array_push($conditions, array('IF(Comprobante.id_tipo_comprobante NOT IN('.EnumTipoComprobante::ReciboManual.','.EnumTipoComprobante::ReciboAutomatico.') ,ComprobanteValor.origen=0,1)='=>1));//filtrar un campo por condicion en el where
    		
    		
    		array_push($conditions, array('IF(Comprobante.id_tipo_comprobante IN('.EnumTipoComprobante::ReciboManual.','.EnumTipoComprobante::ReciboAutomatico.') ,ComprobanteValor.origen IS NULL,1)='=>1));//filtrar un campo por condicion en el where
    		
    	}		
    	
    	
    	
    	if($fecha_inicio!=""){
    		array_push($conditions, array('Comprobante.fecha_contable >='=>$fecha_inicio));
    		array_push($this->array_filter_names,array("Fecha Contable Dsd:"=>$this->{$this->model}->formatDate($fecha_inicio)));
    	}
    	
    	
    	if($fecha_fin!=""){
    		array_push($conditions, array('Comprobante.fecha_contable <='=>$fecha_fin));
    		array_push($this->array_filter_names,array("Fecha Contable Hta:"=>$this->{$this->model}->formatDate($fecha_fin)));
    	}
    	
    	
    	

    	
    	$this->ComprobanteValor->virtualFields = array('total_por_comprobante' => 'ROUND(ComprobanteValor.monto,2)','codigo_tipo_comprobante'=>'TipoComprobante.codigo_tipo_comprobante','pto_nro_comprobante'=>'Comprobante.nro_comprobante','d_ingreso'=>'CONCAT("Ingreso")','fecha_contable'=>'DATE_FORMAT(fecha_generacion,"%d-%m%-%Y")');
    	$comprobante_ingreso = $this->ComprobanteValor->find('all', array(
    			
    			'joins'=>array(   array(
    					'table' => 'tipo_comprobante',
    					'alias' => 'TipoComprobante',
    					'type' => 'LEFT',
    					'conditions' => array(
    							'TipoComprobante.id = Comprobante.id_tipo_comprobante'
    					)),
    					array(
    							'table' => 'sistema',
    							'alias' => 'Sistema',
    							'type' => 'LEFT',
    							'conditions' => array(
    									'TipoComprobante.id_sistema = Sistema.id'
    							)
    							
    							
    							
    					),
    					array(
    							'table' => 'tarjeta_credito',
    							'alias' => 'TarjetaCredito',
    							'type' => 'LEFT',
    							'conditions' => array(
    									'TarjetaCredito.id = ComprobanteValor.id_tarjeta_credito'
    							)
    							
    							
    							
    					)
    					
    					
    					
    			),
    			'conditions' => array($conditions),
    			
    			
    			'fields'=>array('pto_nro_comprobante','total_por_comprobante','codigo_tipo_comprobante','d_ingreso','fecha_contable'),
    			
    			'contain' =>array('Comprobante')
    	));
    	
    	
    	
    	$this->ComprobanteValor->virtualFields = array('total' => 'SUM(ROUND(ComprobanteValor.monto,2))');
    	$total_comprobante_ingreso = $this->ComprobanteValor->find('all', array(
    			
    			'joins'=>array(   array(
    					'table' => 'tipo_comprobante',
    					'alias' => 'TipoComprobante',
    					'type' => 'LEFT',
    					'conditions' => array(
    							'TipoComprobante.id = Comprobante.id_tipo_comprobante'
    					)),
    					array(
    							'table' => 'sistema',
    							'alias' => 'Sistema',
    							'type' => 'LEFT',
    							'conditions' => array(
    									'TipoComprobante.id_sistema = Sistema.id'
    							)
    							
    							
    							
    					),
    					array(
    							'table' => 'tarjeta_credito',
    							'alias' => 'TarjetaCredito',
    							'type' => 'LEFT',
    							'conditions' => array(
    									'TarjetaCredito.id = ComprobanteValor.id_tarjeta_credito'
    							)
    							
    							
    							
    					)
    					
    					
    					
    			),
    			'conditions' => array($conditions),
    			
    			
    			'fields'=>array('total'),
    			
    			'contain' =>array('Comprobante')
    	));
    	
    	
 
    	$this->data = $comprobante_ingreso;
    	
    	/*---------------------------------------COMPROBANTES EGRESO------------------------------------------------------------------------*/
    	
    	$conditions = array();
    	array_push($conditions, array('Comprobante.definitivo'=> 1 ));
    	array_push($conditions, array('NOT'=>array('Comprobante.id_estado_comprobante' =>array(EnumEstadoComprobante::Anulado,EnumEstadoComprobante::Cancelado)) ));
    	
    	array_push($conditions, array('ComprobanteValor.id_valor'=> array(EnumValor::EXTRACCION,EnumValor::TRANSFERENCIAS,EnumValor::TARJETA) ));
    	
    	
    	$array_comprobantes_egreso = '('.EnumTipoComprobante::OrdenPagoAnticipo.','.EnumTipoComprobante::OrdenPagoAutomatica.','.EnumTipoComprobante::OrdenPagoManual.','.EnumTipoComprobante::OrdenPagoRenGasto.')';
    	
    	
    	if($id_cuenta_bancaria != ""){/*Me envia la cuenta bancaria entonces es para conciliar*/
    		
    		/*En un ingreso solo tengo cheques de Terceros*/
    		array_push($conditions, array('IF(ComprobanteValor.id_valor='.EnumValor::TARJETA.' ,TarjetaCredito.id_cuenta_bancaria='.$id_cuenta_bancaria.',1)'=>1));//filtrar un campo por condicion en el where
    		
    		array_push($conditions, array('IF(ComprobanteValor.id_valor IN('.EnumValor::EXTRACCION.','.EnumValor::TRANSFERENCIAS.') ,ComprobanteValor.id_cuenta_bancaria='.$id_cuenta_bancaria.',1)='=>1));//filtrar un campo por condicion en el where
    		
    		
    		array_push($conditions, array('IF(Comprobante.id_tipo_comprobante NOT IN '.$array_comprobantes_egreso.',ComprobanteValor.origen=0,1)='=>1));//filtrar un campo por condicion en el where
    		
    		
    		array_push($conditions, array('IF(Comprobante.id_tipo_comprobante IN '.$array_comprobantes_egreso.' ,ComprobanteValor.origen IS NULL,1)='=>1));//filtrar un campo por condicion en el where
    		
    	}
    	
    	
    	
    	if($fecha_inicio!=""){
    		array_push($conditions, array('Comprobante.fecha_contable >='=>$fecha_inicio));
    		array_push($this->array_filter_names,array("Fecha Contable Dsd:"=>$this->{$this->model}->formatDate($fecha_inicio)));
    	}
    	
    	
    	if($fecha_fin!=""){
    		array_push($conditions, array('Comprobante.fecha_contable <='=>$fecha_fin));
    		array_push($this->array_filter_names,array("Fecha Contable Hta:"=>$this->{$this->model}->formatDate($fecha_fin)));
    	}
    	
    	
    	

    	
    	$this->ComprobanteValor->virtualFields = array('total_por_comprobante' => 'ROUND(ComprobanteValor.monto,2)','codigo_tipo_comprobante'=>'TipoComprobante.codigo_tipo_comprobante','pto_nro_comprobante'=>'Comprobante.nro_comprobante','d_ingreso'=>'CONCAT("Egreso")','fecha_contable'=>'DATE_FORMAT(fecha_generacion,"%d-%m%-%Y")');
    	
    	$comprobante_egreso = $this->ComprobanteValor->find('all', array(
    			
    			'joins'=>array(   array(
    					'table' => 'tipo_comprobante',
    					'alias' => 'TipoComprobante',
    					'type' => 'LEFT',
    					'conditions' => array(
    							'TipoComprobante.id = Comprobante.id_tipo_comprobante'
    					)),
    					array(
    							'table' => 'sistema',
    							'alias' => 'Sistema',
    							'type' => 'LEFT',
    							'conditions' => array(
    									'TipoComprobante.id_sistema = Sistema.id'
    							)
    							
    							
    							
    					),
    					array(
    							'table' => 'tarjeta_credito',
    							'alias' => 'TarjetaCredito',
    							'type' => 'LEFT',
    							'conditions' => array(
    									'TarjetaCredito.id = ComprobanteValor.id_tarjeta_credito'
    							)
    							
    							
    							
    					)
    					
    					
    					
    			),
    			'conditions' => array($conditions),
    			
    			
    			'fields'=>array('pto_nro_comprobante','total_por_comprobante','codigo_tipo_comprobante','d_ingreso','fecha_contable'),
    			
    			'contain' =>array('Comprobante')
    	));
    	
    	
    	$this->ComprobanteValor->virtualFields = array('total' => 'SUM(ROUND(ComprobanteValor.monto,2))');
    	
    	$total_comprobante_egreso = $this->ComprobanteValor->find('all', array(
    			
    			'joins'=>array(   array(
    					'table' => 'tipo_comprobante',
    					'alias' => 'TipoComprobante',
    					'type' => 'LEFT',
    					'conditions' => array(
    							'TipoComprobante.id = Comprobante.id_tipo_comprobante'
    					)),
    					array(
    							'table' => 'sistema',
    							'alias' => 'Sistema',
    							'type' => 'LEFT',
    							'conditions' => array(
    									'TipoComprobante.id_sistema = Sistema.id'
    							)
    							
    							
    							
    					),
    					array(
    							'table' => 'tarjeta_credito',
    							'alias' => 'TarjetaCredito',
    							'type' => 'LEFT',
    							'conditions' => array(
    									'TarjetaCredito.id = ComprobanteValor.id_tarjeta_credito'
    							)
    							
    							
    							
    					)
    					
    					
    					
    			),
    			'conditions' => array($conditions),
    			
    			
    			'fields'=>array('total'),
    			
    			'contain' =>array('Comprobante')
    	));
    	
    	
    	
    	
    	/*----------------------------------------------------------*/
    	
    	
    	$this->data  = array_merge($comprobante_ingreso,$comprobante_egreso);
    	
    	
    	
    	$resultado["ResultadosComprobante"]["total_debe"] =(string) $total_comprobante_ingreso[0]["ComprobanteValor"]["total"];//total de los comprobantes
    	$resultado["ResultadosComprobante"]["total_haber"] =(string) ($total_comprobante_egreso[0]["ComprobanteValor"]["total"]*-1);//total de los comprobantes
    	$resultado["ResultadosComprobante"]["saldo"] =(string) ($resultado["ResultadosComprobante"]["total_debe"]+ $resultado["ResultadosComprobante"]["total_haber"]);//total de los comprobantes  
    	
    	
    	$output = array(
    			"status" =>EnumError::SUCCESS,
    			"message" => "list",
    			"content" => $this->data ,
    			"resultset_content"=>$resultado,
    			"page_count" =>0
    	);
    	
    	
    	
    	
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message","page_count", "content","resultset_content"));
    	

    }
    
    
    public function getMovimientoFondos(){
    	
    	$this->loadModel("Comprobante");
    	$this->loadModel("ComprobanteValor");
    	
    	
    	
    	$fecha_inicio = strtolower($this->getFromRequestOrSession('Comprobante.fecha_contable'));
    	$fecha_fin = strtolower($this->getFromRequestOrSession('Comprobante.fecha_contable_hasta'));
    	$conciliado = strtolower($this->getFromRequestOrSession('Cheque.conciliado'));
    	$id_cuenta_bancaria = strtolower($this->getFromRequestOrSession('ComprobanteValor.id_cuenta_bancaria'));
    	
    	
    	
    	$conditions = array();
    	array_push($conditions, array('NOT'=>array('Comprobante.id_estado_comprobante' =>array(EnumEstadoComprobante::Anulado,EnumEstadoComprobante::Cancelado)) ));
    	array_push($conditions, array('Comprobante.definitivo'=> 1 ));
    	array_push($conditions, array('Comprobante.id_tipo_comprobante'=> array(EnumTipoComprobante::ReciboManual,EnumTipoComprobante::ReciboAutomatico,EnumTipoComprobante::IngresoCaja) ));
    	
    	if($conciliado == 1)
    		array_push($conditions, array('IF(ComprobanteValor.id_valor='.EnumValor::CHEQUE.',Cheque.conciliado=1,1)'=>1));//filtrar un campo por condicion en el where
    	
    	
    	if($id_cuenta_bancaria != ""){/*Me envia la cuenta bancaria entonces es para conciliar*/
    		
    			/*En un ingreso solo tengo cheques de Terceros*/
    		array_push($conditions, array('IF(ComprobanteValor.id_valor='.EnumValor::CHEQUE.' AND ComprobanteValor.id_cheque IS NOT NULL AND Cheque.id_tipo_cheque = '.EnumTipoCheque::Terceros.' ,Chequera.id_cuenta_bancaria='.$id_cuenta_bancaria.',1)'=>1));//filtrar un campo por condicion en el where
    			
    	}		
    		
    	
    	if($fecha_inicio!=""){
	    	array_push($conditions, array('Comprobante.fecha_contable >='=>$fecha_inicio));
	    	array_push($this->array_filter_names,array("Fecha Contable Dsd:"=>$this->{$this->model}->formatDate($fecha_inicio)));
    	}
    	
    	
    	if($fecha_fin!=""){
    		array_push($conditions, array('Comprobante.fecha_contable <='=>$fecha_fin));
    		array_push($this->array_filter_names,array("Fecha Contable Hta:"=>$this->{$this->model}->formatDate($fecha_fin)));
    	}
    
    	
    	$comprobante_ventas = $this->ComprobanteValor->find('all', array(
    			
    			'joins'=>array(   array(
    					'table' => 'tipo_comprobante',
    					'alias' => 'TipoComprobante',
    					'type' => 'LEFT',
    					'conditions' => array(
    							'TipoComprobante.id = Comprobante.id_tipo_comprobante'
    					)),
    					array(
    							'table' => 'sistema',
    							'alias' => 'Sistema',
    							'type' => 'LEFT',
    							'conditions' => array(
    									'TipoComprobante.id_sistema = Sistema.id'
    							)
    				
    							
    					
    			)
    					
    					
    					
    			),
    			'conditions' => array($conditions),
    			'fields' => array('SUM(ROUND(ComprobanteValor.monto,2)) as total','Valor.id','TipoComprobante.id_sistema','Valor.d_valor','Sistema.d_sistema','Cheque.conciliado','Cheque.nro_cheque'),
    			
    			'group' => array('ComprobanteValor.id_valor','TipoComprobante.id_sistema','ComprobanteValor.id_cuenta_bancaria'),
    			
    			'contain' =>array('Valor','Comprobante'=>array('TipoComprobante'),'Cheque'=>array('Chequera'))
    	));
    	
    	
    	
    	$conditions = array();
    	
    	
    	if($conciliado == 1)
    		array_push($conditions, array('IF(ComprobanteValor.id_valor='.EnumValor::CHEQUE.',Cheque.conciliado=1,1)'=>1));//filtrar un campo por condicion en el where
    		
    	
    	if($fecha_inicio!=""){
	    	array_push($conditions, array('Comprobante.fecha_contable >='=>$fecha_inicio));
	    	array_push($this->array_filter_names,array("Fecha Contable Dsd:"=>$this->{$this->model}->formatDate($fecha_inicio)));
    	}
    	
    	
    	if($fecha_fin!=""){
	    	array_push($conditions, array('Comprobante.fecha_contable <='=>$fecha_fin));
	    	array_push($this->array_filter_names,array("Fecha Contable Hta:"=>$this->{$this->model}->formatDate($fecha_fin)));
    	}
    	
    	
    	array_push($conditions, array('NOT'=>array('Comprobante.id_estado_comprobante' =>array(EnumEstadoComprobante::Anulado,EnumEstadoComprobante::Cancelado)) ));
    	array_push($conditions, array('Comprobante.definitivo'=> 1 ));
    	array_push($conditions, array('Comprobante.id_tipo_comprobante'=> array(EnumTipoComprobante::OrdenPagoManual,EnumTipoComprobante::OrdenPagoAutomatica,EnumTipoComprobante::EgresoCaja,EnumTipoComprobante::OrdenPagoAnticipo,EnumTipoComprobante::OrdenPagoRenGasto) ));
    	
    	$comprobante_compras = $this->ComprobanteValor->find('all', array(
    			
    			'joins'=>array(   array(
    					'table' => 'tipo_comprobante',
    					'alias' => 'TipoComprobante',
    					'type' => 'LEFT',
    					'conditions' => array(
    							'TipoComprobante.id = Comprobante.id_tipo_comprobante'
    					)
    					
    			),array(
    					'table' => 'sistema',
    					'alias' => 'Sistema',
    					'type' => 'LEFT',
    					'conditions' => array(
    							'TipoComprobante.id_sistema = Sistema.id'
    					)
    					
    					
    			)
    					
    					
    					
    					
    					
    			),
    			'conditions' => array($conditions),
    			'fields' => array('SUM(ROUND(ComprobanteValor.monto,2))*-1 as total','Valor.id','TipoComprobante.id_sistema','Valor.d_valor','Sistema.d_sistema','Cheque.conciliado','Cheque.nro_cheque'),
    			
    			'group' => array('ComprobanteValor.id_valor','TipoComprobante.id_sistema'),
    			
    			'contain' =>array('Cheque', 'Valor','Comprobante'=>array('TipoComprobante'=>array("Sistema")))
    	));
    	
    	
    	$array_final = array_merge($comprobante_ventas,$comprobante_compras);
    	
    	
    	$total_haber = 0;
    	$total_haber_conciliado = 0;
    	$total_debe = 0;
    	$total_debe_conciliado = 0;
    	
    	$array_data = array();
    	
    	foreach($array_final as &$valor){
    		
    		
    		
    		
    		$valor["ComprobanteValor"]["monto"] = $valor[0]["total"];
    		$valor["ComprobanteValor"]["d_valor"] = $valor["Valor"]["d_valor"];
    		$valor["ComprobanteValor"]["d_sistema"] = $valor["Sistema"]["d_sistema"];
    		
    		
    		if($valor["ComprobanteValor"]["monto"]<0){
    			$total_haber  = $total_haber + $valor["ComprobanteValor"]["monto"];
    			$valor["ComprobanteValor"]["descripcion"] = "Egreso";
    		}else{
    			$total_debe = $total_debe + $valor["ComprobanteValor"]["monto"];
    			$valor["ComprobanteValor"]["descripcion"] = "Ingreso";
    		}
    		
    			
    			
    		
    		unset($valor[0]);
    		unset($valor["Valor"]);
    		unset($valor["Sistema"]);
    		unset($valor["Comprobante"]);
    		unset($valor["TipoComprobante"]);
    		array_push($array_data, $valor);
    		
    	}
    	
    	
    	
    	
    	
    	$resultado["ResultadosComprobante"]["total_debe"] =(string) $total_debe;//total de los comprobantes
    	$resultado["ResultadosComprobante"]["total_haber"] =(string) $total_haber;//total de los comprobantes
    	$resultado["ResultadosComprobante"]["saldo"] =(string) ($total_debe + $total_haber);//total de los comprobantes  
    	
    	
    	$status = EnumError::SUCCESS;
    	$this->data = $array_data;
    	
    	$output = array(
    			"status" =>EnumError::SUCCESS,
    			"message" => "list",
    			"content" => $array_data,
    			"resultset_content"=>$resultado,
    			"page_count" =>0
    	);
    	
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message","page_count", "content","resultset_content"));
    	
    	
    	
    	
    	
    	
    	
    	
    	
    }
    
    
    
    
    
    
    
    public function getConciliacionBancaria(){
    	
    	$this->loadModel("Comprobante");
    	$this->loadModel("ComprobanteValor");
    	
    	
    	
    	$fecha_inicio = strtolower($this->getFromRequestOrSession('Comprobante.fecha_contable'));
    	$fecha_fin = strtolower($this->getFromRequestOrSession('Comprobante.fecha_contable_hasta'));
    	$conciliado = strtolower($this->getFromRequestOrSession('Cheque.conciliado'));
    	$id_cuenta_bancaria = strtolower($this->getFromRequestOrSession('ComprobanteValor.id_cuenta_bancaria'));
    	$conciliado = strtolower($this->getFromRequestOrSession('ComprobanteValor.conciliado'));
    	$saldo_inicial = strtolower($this->getFromRequestOrSession('ComprobanteValor.saldo_inicial'));
    	
    	
    	$filtro_conciliado = "";
    	if($conciliado != "")
    		$filtro_conciliado = " AND comprobante_valor.conciliado=".$conciliado;
    		
    	
    	
    		$query_saldo_inicial = "SELECT SUM(monto) as monto FROM (SELECT
											 
												  IFNULL(SUM(comprobante_valor.monto),0) AS monto
											 
											FROM
											  comprobante_valor
											  JOIN comprobante
											    ON comprobante.id = comprobante_valor.`id_comprobante`
											  JOIN tipo_comprobante
											    ON comprobante.id_tipo_comprobante = tipo_comprobante.`id`
											  LEFT JOIN detalle_tipo_comprobante
											    ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id`
											  LEFT JOIN punto_venta
											    ON punto_venta.id = comprobante.`id_punto_venta`
											  LEFT JOIN persona
											    ON persona.id = comprobante.id_persona
											  LEFT JOIN cheque
											    ON cheque.id = comprobante_valor.`id_cheque`
											  LEFT JOIN valor
											    ON valor.id = comprobante_valor.id_valor
											WHERE origen = 0
											  AND id_cuenta_bancaria = ".$id_cuenta_bancaria."
											  AND comprobante.id_tipo_comprobante = 501
											 AND comprobante.fecha_contable <'".$fecha_inicio."'".$filtro_conciliado."
											 		
											UNION
											ALL
											SELECT
												  IFNULL(SUM(comprobante_valor.monto),0) AS monto
											FROM
											  comprobante_valor
											  JOIN comprobante
											    ON comprobante.id = comprobante_valor.`id_comprobante`
											  JOIN tipo_comprobante
											    ON comprobante.id_tipo_comprobante = tipo_comprobante.`id`
											  LEFT JOIN detalle_tipo_comprobante
											    ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id`
											  LEFT JOIN punto_venta
											    ON punto_venta.id = comprobante.`id_punto_venta`
											  LEFT JOIN persona
											    ON persona.id = comprobante.id_persona
											  LEFT JOIN valor
											    ON valor.id = comprobante_valor.id_valor
											  LEFT JOIN cheque
											    ON cheque.id = comprobante_valor.`id_cheque`
											WHERE id_cuenta_bancaria = ".$id_cuenta_bancaria."
											  AND comprobante.`id_tipo_comprobante` IN (207, 208)
											  AND comprobante.fecha_contable <'".$fecha_inicio."'
											".$filtro_conciliado."
											UNION
											ALL
											SELECT
											  IFNULL(SUM(comprobante_valor.monto),0) AS monto
											FROM
											  comprobante_valor
											  JOIN comprobante
											    ON comprobante.id = comprobante_valor.`id_comprobante`
											  JOIN tipo_comprobante
											    ON comprobante.id_tipo_comprobante = tipo_comprobante.`id`
											  LEFT JOIN detalle_tipo_comprobante
											    ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id`
											  LEFT JOIN punto_venta
											    ON punto_venta.id = comprobante.`id_punto_venta`
											  LEFT JOIN persona
											    ON persona.id = comprobante.id_persona
											  LEFT JOIN valor
											    ON valor.id = comprobante_valor.id_valor
											  LEFT JOIN cheque
											    ON cheque.id = comprobante_valor.`id_cheque`
											WHERE origen = 1
											  AND id_comprobante IN
											  (SELECT
											    comprobante_valor.id_comprobante
											  FROM
											    comprobante_valor
											    JOIN comprobante
											      ON comprobante.id = comprobante_valor.`id_comprobante`
											    JOIN detalle_tipo_comprobante
											      ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id`
											  WHERE origen = 0
											    AND id_cuenta_bancaria = ".$id_cuenta_bancaria."
											    AND comprobante.`id_tipo_comprobante` = 604
											    AND comprobante.`id_detalle_tipo_comprobante` IN (46, 49))
															 AND comprobante.fecha_contable >='".$fecha_inicio."'
											 AND comprobante.fecha_contable <='".$fecha_fin."' ".$filtro_conciliado."
											 	".$filtro_conciliado.") as tabla";
    		
    	
    	$query_ingreso = "SELECT 
											  comprobante.`nro_comprobante`,
											  punto_venta.`numero` AS pto_venta,
											  CONCAT(LPAD(punto_venta.`numero`,4,'0') ,'-',LPAD(comprobante.`nro_comprobante`,8,'0')) AS pto_nro_comprobante,
											  tipo_comprobante.`codigo_tipo_comprobante`,
											  detalle_tipo_comprobante.`d_detalle_tipo_comprobante`,
											  persona.`razon_social`,
											  comprobante_valor.monto,
											  comprobante.`fecha_contable`,
											  valor.`d_valor`,
											  comprobante_valor.`d_comprobante_valor`,
											  comprobante_valor.`referencia`,
											  cheque.`nro_cheque`,
											  cheque.`fecha_cheque`,
											  cheque.`fecha_clearing`,
											  comprobante_valor.`id`,
											  comprobante_valor.conciliado
											FROM
											  comprobante_valor 
											  JOIN comprobante 
											    ON comprobante.id = comprobante_valor.`id_comprobante` 
											  JOIN tipo_comprobante 
											    ON comprobante.id_tipo_comprobante = tipo_comprobante.`id` 
											  LEFT JOIN detalle_tipo_comprobante 
											    ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id` 
											  LEFT JOIN punto_venta 
											    ON punto_venta.id = comprobante.`id_punto_venta` 
											  LEFT JOIN persona 
											    ON persona.id = comprobante.id_persona 
											  LEFT JOIN cheque 
											    ON cheque.id = comprobante_valor.`id_cheque` 
											  LEFT JOIN valor 
											    ON valor.id = comprobante_valor.id_valor 
											WHERE origen = 0 
											  AND id_cuenta_bancaria = ".$id_cuenta_bancaria." 
											  AND comprobante.`id_tipo_comprobante` = 501 
											 AND comprobante.fecha_contable >='".$fecha_inicio."'
											 AND comprobante.fecha_contable <='".$fecha_fin."' ".$filtro_conciliado."
								
											UNION
											ALL 
											SELECT 
											  comprobante.`nro_comprobante`,
											  punto_venta.`numero` AS pto_venta,
											    CONCAT(LPAD(punto_venta.`numero`,4,'0') ,'-',LPAD(comprobante.`nro_comprobante`,8,'0')) AS pto_nro_comprobante,
											  tipo_comprobante.`codigo_tipo_comprobante`,
											  detalle_tipo_comprobante.`d_detalle_tipo_comprobante`,
											  persona.`razon_social`,
											  comprobante_valor.monto,
											  comprobante.`fecha_contable`,
											  valor.`d_valor`,
											  comprobante_valor.`d_comprobante_valor`,
											  comprobante_valor.`referencia`,
											  cheque.`nro_cheque`,
											  cheque.`fecha_cheque`,
											  cheque.`fecha_clearing`,
											  comprobante_valor.`id`,
											  comprobante_valor.conciliado 
											FROM
											  comprobante_valor 
											  JOIN comprobante 
											    ON comprobante.id = comprobante_valor.`id_comprobante` 
											  JOIN tipo_comprobante 
											    ON comprobante.id_tipo_comprobante = tipo_comprobante.`id` 
											  LEFT JOIN detalle_tipo_comprobante 
											    ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id` 
											  LEFT JOIN punto_venta 
											    ON punto_venta.id = comprobante.`id_punto_venta` 
											  LEFT JOIN persona 
											    ON persona.id = comprobante.id_persona 
											  LEFT JOIN valor 
											    ON valor.id = comprobante_valor.id_valor 
											  LEFT JOIN cheque 
											    ON cheque.id = comprobante_valor.`id_cheque` 
											WHERE id_cuenta_bancaria = ".$id_cuenta_bancaria." 
											  AND comprobante.`id_tipo_comprobante` IN (207, 208) 
											  AND comprobante.fecha_contable >='".$fecha_inicio."'
											 AND comprobante.fecha_contable <='".$fecha_fin."'".$filtro_conciliado."
											UNION
											ALL 
											SELECT 
											  comprobante.`nro_comprobante`,
											  punto_venta.`numero` AS pto_venta,
											  CONCAT(LPAD(punto_venta.`numero`,4,'0') ,'-',LPAD(comprobante.`nro_comprobante`,8,'0')) AS pto_nro_comprobante,
											  tipo_comprobante.`codigo_tipo_comprobante`,
											  detalle_tipo_comprobante.`d_detalle_tipo_comprobante`,
											  persona.`razon_social`,
											  comprobante_valor.monto,
											  comprobante.`fecha_contable`,
											  valor.`d_valor`,
											  comprobante_valor.`d_comprobante_valor`,
											  comprobante_valor.`referencia`,
											  cheque.`nro_cheque`,
											  cheque.`fecha_cheque`,
											  cheque.`fecha_clearing`,
											  comprobante_valor.`id`,
											  comprobante_valor.conciliado 
											FROM
											  comprobante_valor 
											  JOIN comprobante 
											    ON comprobante.id = comprobante_valor.`id_comprobante` 
											  JOIN tipo_comprobante 
											    ON comprobante.id_tipo_comprobante = tipo_comprobante.`id` 
											  LEFT JOIN detalle_tipo_comprobante 
											    ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id` 
											  LEFT JOIN punto_venta 
											    ON punto_venta.id = comprobante.`id_punto_venta` 
											  LEFT JOIN persona 
											    ON persona.id = comprobante.id_persona 
											  LEFT JOIN valor 
											    ON valor.id = comprobante_valor.id_valor 
											  LEFT JOIN cheque 
											    ON cheque.id = comprobante_valor.`id_cheque` 
											WHERE origen = 1 
											  AND id_comprobante IN 
											  (SELECT 
											    comprobante_valor.id_comprobante 
											  FROM
											    comprobante_valor 
											    JOIN comprobante 
											      ON comprobante.id = comprobante_valor.`id_comprobante` 
											    JOIN detalle_tipo_comprobante 
											      ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id` 
											  WHERE origen = 0 
											    AND id_cuenta_bancaria = ".$id_cuenta_bancaria." 
											    AND comprobante.`id_tipo_comprobante` = 604 
											    AND comprobante.`id_detalle_tipo_comprobante` IN (46, 49))
												AND comprobante.fecha_contable >='".$fecha_inicio."'
											 	AND comprobante.fecha_contable <='".$fecha_fin."'".$filtro_conciliado."
												    ORDER BY fecha_contable ASC";
    	
    	
    	
    	$query_egreso_saldo_inicial = "SELECT SUM(monto) as monto FROM (SELECT
											  		   IFNULL(SUM(comprobante_valor.monto),0) AS monto
											FROM
											  comprobante_valor
											  JOIN comprobante
											    ON comprobante.id = comprobante_valor.`id_comprobante`
											  JOIN tipo_comprobante
											    ON comprobante.id_tipo_comprobante = tipo_comprobante.`id`
											  LEFT JOIN detalle_tipo_comprobante
											    ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id`
											  LEFT JOIN punto_venta
											    ON punto_venta.id = comprobante.`id_punto_venta`
											  LEFT JOIN persona
											    ON persona.id = comprobante.id_persona
											  LEFT JOIN cheque
											    ON cheque.id = comprobante_valor.`id_cheque`
											  LEFT JOIN valor
											    ON valor.id = comprobante_valor.id_valor
											WHERE origen = 1
											  AND id_cuenta_bancaria = ".$id_cuenta_bancaria."
											  AND comprobante.`id_tipo_comprobante` = 502
											 AND comprobante.fecha_contable >='".$fecha_inicio."'
											 AND comprobante.fecha_contable <='".$fecha_fin."'".$filtro_conciliado."
											UNION
											ALL
											SELECT
											 		  IFNULL(SUM(comprobante_valor.monto),0) AS monto
											FROM
											  comprobante_valor
											  JOIN comprobante
											    ON comprobante.id = comprobante_valor.`id_comprobante`
											  JOIN tipo_comprobante
											    ON comprobante.id_tipo_comprobante = tipo_comprobante.`id`
											  LEFT JOIN detalle_tipo_comprobante
											    ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id`
											  LEFT JOIN punto_venta
											    ON punto_venta.id = comprobante.`id_punto_venta`
											  LEFT JOIN persona
											    ON persona.id = comprobante.id_persona
											  LEFT JOIN valor
											    ON valor.id = comprobante_valor.id_valor
											  LEFT JOIN cheque
											    ON cheque.id = comprobante_valor.`id_cheque`
											WHERE id_cuenta_bancaria = ".$id_cuenta_bancaria."
											  AND comprobante.`id_tipo_comprobante` IN (306,307,308, 504)
											  AND comprobante.fecha_contable >='".$fecha_inicio."'
											 AND comprobante.fecha_contable <='".$fecha_fin."'".$filtro_conciliado."
											UNION
											ALL
											SELECT
													   IFNULL(SUM(comprobante_valor.monto),0) AS monto
											FROM
											  comprobante_valor
											  JOIN comprobante
											    ON comprobante.id = comprobante_valor.`id_comprobante`
											  JOIN tipo_comprobante
											    ON comprobante.id_tipo_comprobante = tipo_comprobante.`id`
											  LEFT JOIN detalle_tipo_comprobante
											    ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id`
											  LEFT JOIN punto_venta
											    ON punto_venta.id = comprobante.`id_punto_venta`
											  LEFT JOIN persona
											    ON persona.id = comprobante.id_persona
											  LEFT JOIN valor
											    ON valor.id = comprobante_valor.id_valor
											  LEFT JOIN cheque
											    ON cheque.id = comprobante_valor.`id_cheque`
											WHERE origen = 0
											  AND id_comprobante IN
											  (SELECT
											    comprobante_valor.id_comprobante
											  FROM
											    comprobante_valor
											    JOIN comprobante
											      ON comprobante.id = comprobante_valor.`id_comprobante`
											    JOIN detalle_tipo_comprobante
											      ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id`
											  WHERE origen = 1
											    AND id_cuenta_bancaria = ".$id_cuenta_bancaria."
											    AND comprobante.`id_tipo_comprobante` = 604
											    AND comprobante.`id_detalle_tipo_comprobante` IN (47, 49))
											 AND comprobante.fecha_contable >='".$fecha_inicio."'
											 AND comprobante.fecha_contable <='".$fecha_fin."' ".$filtro_conciliado."

												".$filtro_conciliado."
												   ) as tabla";
    	
    	
    	$query_egreso = "SELECT
											  comprobante.`nro_comprobante`,
											  punto_venta.`numero` AS pto_venta,
											  CONCAT(LPAD(punto_venta.`numero`,4,'0') ,'-',LPAD(comprobante.`nro_comprobante`,8,'0')) AS pto_nro_comprobante,
											  tipo_comprobante.`codigo_tipo_comprobante`,
											  detalle_tipo_comprobante.`d_detalle_tipo_comprobante`,
											  persona.`razon_social`,
											  comprobante_valor.monto,
											  comprobante.`fecha_contable`,
											  valor.`d_valor`,
											  comprobante_valor.`d_comprobante_valor`,
											  comprobante_valor.`referencia`,
											  cheque.`nro_cheque`,
											  cheque.`fecha_cheque`,
											  cheque.`fecha_clearing`,
											  comprobante_valor.`id`,
											  comprobante_valor.conciliado
											FROM
											  comprobante_valor
											  JOIN comprobante
											    ON comprobante.id = comprobante_valor.`id_comprobante`
											  JOIN tipo_comprobante
											    ON comprobante.id_tipo_comprobante = tipo_comprobante.`id`
											  LEFT JOIN detalle_tipo_comprobante
											    ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id`
											  LEFT JOIN punto_venta
											    ON punto_venta.id = comprobante.`id_punto_venta`
											  LEFT JOIN persona
											    ON persona.id = comprobante.id_persona
											  LEFT JOIN cheque
											    ON cheque.id = comprobante_valor.`id_cheque`
											  LEFT JOIN valor
											    ON valor.id = comprobante_valor.id_valor
											WHERE origen = 1
											  AND id_cuenta_bancaria = ".$id_cuenta_bancaria."
											  AND comprobante.`id_tipo_comprobante` = 502
											 AND comprobante.fecha_contable >='".$fecha_inicio."'
											 AND comprobante.fecha_contable <='".$fecha_fin."'".$filtro_conciliado."
											UNION
											ALL
											SELECT
											  comprobante.`nro_comprobante`,
											  punto_venta.`numero` AS pto_venta,
											    CONCAT(LPAD(punto_venta.`numero`,4,'0') ,'-',LPAD(comprobante.`nro_comprobante`,8,'0')) AS pto_nro_comprobante,
											  tipo_comprobante.`codigo_tipo_comprobante`,
											  detalle_tipo_comprobante.`d_detalle_tipo_comprobante`,
											  persona.`razon_social`,
											  comprobante_valor.monto,
											  comprobante.`fecha_contable`,
											  valor.`d_valor`,
											  comprobante_valor.`d_comprobante_valor`,
											  comprobante_valor.`referencia`,
											  cheque.`nro_cheque`,
											  cheque.`fecha_cheque`,
											  cheque.`fecha_clearing`,
											  comprobante_valor.`id`,
											  comprobante_valor.conciliado
											FROM
											  comprobante_valor
											  JOIN comprobante
											    ON comprobante.id = comprobante_valor.`id_comprobante`
											  JOIN tipo_comprobante
											    ON comprobante.id_tipo_comprobante = tipo_comprobante.`id`
											  LEFT JOIN detalle_tipo_comprobante
											    ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id`
											  LEFT JOIN punto_venta
											    ON punto_venta.id = comprobante.`id_punto_venta`
											  LEFT JOIN persona
											    ON persona.id = comprobante.id_persona
											  LEFT JOIN valor
											    ON valor.id = comprobante_valor.id_valor
											  LEFT JOIN cheque
											    ON cheque.id = comprobante_valor.`id_cheque`
											WHERE id_cuenta_bancaria = ".$id_cuenta_bancaria."
											  AND comprobante.`id_tipo_comprobante` IN (306,307,308, 504)
											  AND comprobante.fecha_contable >='".$fecha_inicio."'
											 AND comprobante.fecha_contable <='".$fecha_fin."'".$filtro_conciliado."
											UNION
											ALL
											SELECT
											  comprobante.`nro_comprobante`,
											  punto_venta.`numero` AS pto_venta,
											  CONCAT(LPAD(punto_venta.`numero`,4,'0') ,'-',LPAD(comprobante.`nro_comprobante`,8,'0')) AS pto_nro_comprobante,
											  tipo_comprobante.`codigo_tipo_comprobante`,
											  detalle_tipo_comprobante.`d_detalle_tipo_comprobante`,
											  persona.`razon_social`,
											  comprobante_valor.monto,
											  comprobante.`fecha_contable`,
											  valor.`d_valor`,
											  comprobante_valor.`d_comprobante_valor`,
											  comprobante_valor.`referencia`,
											  cheque.`nro_cheque`,
											  cheque.`fecha_cheque`,
											  cheque.`fecha_clearing`,
											  comprobante_valor.`id`,
											  comprobante_valor.conciliado
											FROM
											  comprobante_valor
											  JOIN comprobante
											    ON comprobante.id = comprobante_valor.`id_comprobante`
											  JOIN tipo_comprobante
											    ON comprobante.id_tipo_comprobante = tipo_comprobante.`id`
											  LEFT JOIN detalle_tipo_comprobante
											    ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id`
											  LEFT JOIN punto_venta
											    ON punto_venta.id = comprobante.`id_punto_venta`
											  LEFT JOIN persona
											    ON persona.id = comprobante.id_persona
											  LEFT JOIN valor
											    ON valor.id = comprobante_valor.id_valor
											  LEFT JOIN cheque
											    ON cheque.id = comprobante_valor.`id_cheque`
											WHERE origen = 0
											  AND id_comprobante IN
											  (SELECT
											    comprobante_valor.id_comprobante
											  FROM
											    comprobante_valor
											    JOIN comprobante
											      ON comprobante.id = comprobante_valor.`id_comprobante`
											    JOIN detalle_tipo_comprobante
											      ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id`
											  WHERE origen = 1
											    AND id_cuenta_bancaria = ".$id_cuenta_bancaria."
											    AND comprobante.`id_tipo_comprobante` = 604
											    AND comprobante.`id_detalle_tipo_comprobante` IN (47, 49))
											AND comprobante.fecha_contable >='".$fecha_inicio."'
											 AND comprobante.fecha_contable <='".$fecha_fin."' ".$filtro_conciliado."

												".$filtro_conciliado."
												    ORDER BY fecha_contable ASC";
    	
    	
    	
    	
    	
    	$ingresos = $this->ComprobanteValor->query($query_ingreso);
    	
    	if($saldo_inicial !="")
    		$ingresos_saldo_inicial = $this->ComprobanteValor->query($query_saldo_inicial);
    	$egresos = $this->ComprobanteValor->query($query_egreso);
    	
    	if($saldo_inicial !="")
    		$egresos_saldo_inicial = $this->ComprobanteValor->query($query_egreso_saldo_inicial);
    	
    	
    	
    	
    	$array_data_ingreso = array();
    	
    	
    	if($saldo_inicial!=""){
    		
    		
    		
    		$valor2[0]["monto"] = $egresos_saldo_inicial[0][0]["monto"];
    		$valor2[0]["conciliado"] = 1;
    		$valor2[0]["d_valor"] = "Saldo Inicial Egreso";
    		
    		array_unshift($egresos,$valor2);//agrego el saldo inicial como primer elemento
    		
    		
    		$valor2[0]["monto"] = $ingresos_saldo_inicial[0][0]["monto"];
    		$valor2[0]["conciliado"] = 1;
    		$valor2[0]["d_valor"] = "Saldo Inicial Ingreso";
    		
    		array_unshift($ingresos,$valor2);//agrego el saldo inicial como primer elemento
    	}
    	

    	

    	$total_debe = 0;
    	$total_debe_conciliado = 0;
    	
    
    	foreach($ingresos as &$valor){
    		
    		
    		if($valor[0]["conciliado"] == 1)
    			$total_debe_conciliado += $valor[0]["monto"];
    		
    		$total_debe  += $valor[0]["monto"];
    		
    		$valor[0]["d_ingreso"] = "Ingreso";
    		$valor2["ComprobanteValor"] = $valor[0];
    		
    		array_push($array_data_ingreso, $valor2);
    		
    		
    	}
    	
    	
    	$array_data_egreso = array();
    	
    	$total_haber = 0;
    	$total_haber_conciliado = 0;
    	
    	foreach($egresos as & $valor){
    		
    		if($valor[0]["conciliado"] == 1)
    			$total_haber_conciliado+= $valor[0]["monto"];
    		
    			
    		$total_haber += $valor[0]["monto"];
    		$valor[0]["d_ingreso"] = "Egreso";
    		$valor2["ComprobanteValor"] = $valor[0];
    		
    		array_push($array_data_egreso, $valor2);
    		
    		
    	}
    	
    	
    	$array_final = array_merge($array_data_ingreso,$array_data_egreso);
    	

    	
    	
    	


    	
    	$resultado["ResultadosComprobante"]["total_debe"] =(string) $total_debe;//total de los comprobantes
    	$resultado["ResultadosComprobante"]["total_debe_conciliado"] =(string) $total_debe_conciliado;//total de los comprobantes
    	$resultado["ResultadosComprobante"]["total_haber"] =(string) $total_haber;//total de los comprobantes
    	$resultado["ResultadosComprobante"]["total_haber_conciliado"] =(string) $total_haber_conciliado;//total de los comprobantes
    	$resultado["ResultadosComprobante"]["saldo"] =(string) ($total_debe - $total_haber);//total de los comprobantes
    	$resultado["ResultadosComprobante"]["total_saldo_conciliado"] =(string) ($total_debe_conciliado - $total_haber_conciliado);//total de los comprobantes
    	
    	
    	$this->data = $array_final;
    	$output = array(
    			"status" =>EnumError::SUCCESS,
    			"message" => "list",
    			"content" => $array_final,
    			"resultset_content"=>$resultado,
    			"page_count" =>0
    	);
    	
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message","page_count", "content","resultset_content"));
    	
    	
    	
    	
    	
    }
    
    
    
    
    
    /**
     * @secured(BAJA_COMPROBANTE_VALOR)
     */
    public function Conciliar($id_comprobante_valor){
    	
    	$this->loadModel($this->model);
    	
    	$comprobante_valor = $this->ComprobanteValor->find('first',array('conditions'=>array('ComprobanteValor.id'=>$id_comprobante_valor)));
    	
    	
    	
    	$error = EnumError::SUCCESS;
    	$message = "";
    	
    	if($comprobante_valor && $comprobante_valor["ComprobanteValor"]["conciliado"] == 1  ){
    		
    		
    	
    		
    		$error = EnumError::ERROR;
    		$message = "El Registro ya se encuentra conciliado";
    	}
    	
    	
    	if($error == EnumError::SUCCESS){/*actualizo el cheque ya que se puede conciliar*/
    		
    		$this->ComprobanteValor->updateAll(
    				array('ComprobanteValor.conciliado' => 1),
    				array('ComprobanteValor.id' => $id_comprobante_valor)
    				) ;
    		
    		$error = EnumError::SUCCESS;
    		$message = "El Registro se concili&oacute; correctamente";
    	}
    	
    	
    	$output = array(
    			"status" => $error,
    			"message" => $message,
    			"content" => "",
    			"page_count"=>0
    	);
    	
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message","page_count", "content"));
    	
    	
    	
    }
    
    

    
    
    
   
    
}
?>