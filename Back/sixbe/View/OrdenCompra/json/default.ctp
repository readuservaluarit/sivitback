<?php
      include(APP.'View'.DS.'Comprobantes'.DS.'json'.DS.'default.ctp');
 // CONFIG  CON MODE DISPLAYED CELLS
        $model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Nro. OC";
        $model["pto_nro_comprobante"]["order"] = "1";
        $model["pto_nro_comprobante"]["width"] = "15"; //REstriccion mas de esto no se puede achicar
        $model["pto_nro_comprobante"]["text_colour"] = "#000000";

        $model["fecha_generacion"]["show_grid"] = 1;
        $model["fecha_generacion"]["header_display"] = "Fecha Creaci&oacute;n";
        $model["fecha_generacion"]["order"] = "2";
        $model["fecha_generacion"]["width"] = "8";
        $model["fecha_generacion"]["text_colour"] = "#0000FF";
        
        $model["fecha_entrega"]["show_grid"] = 1;
        $model["fecha_entrega"]["header_display"] = "Fecha de Entrega";
        $model["fecha_entrega"]["order"] = "3";
        $model["fecha_entrega"]["width"] = "8";
        $model["fecha_entrega"]["text_colour"] = "#990011";
        
        $model["codigo_persona"]["show_grid"] = 1;
        $model["codigo_persona"]["header_display"] = "Cod. Proveedor";
        $model["codigo_persona"]["order"] = "4";
        $model["codigo_persona"]["width"] = "5";
        $model["codigo_persona"]["text_colour"] = "#000000";
        
        $model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["header_display"] = "Raz&oacute;n social";
        $model["razon_social"]["order"] = "5";
        $model["razon_social"]["width"] = "10";
        $model["razon_social"]["text_colour"] = "#000000";
       
        $model["d_moneda"]["show_grid"] = 1;
        $model["d_moneda"]["header_display"] = "Moneda";
        $model["d_moneda"]["order"] = "6";
        $model["d_moneda"]["width"] = "5";
        $model["d_moneda"]["text_colour"] = "#F0000F";
        
        $model["subtotal_neto"]["show_grid"] = 1;
        $model["subtotal_neto"]["header_display"] = "Sub.Total";
        $model["subtotal_neto"]["order"] = "7";
        $model["subtotal_neto"]["width"] = "10";
        $model["subtotal_neto"]["text_colour"] = "#F0000F";
        
        $model["d_estado_comprobante"]["show_grid"] = 1;
        $model["d_estado_comprobante"]["header_display"] = "Estado";
        $model["d_estado_comprobante"]["order"] = "8";
        $model["d_estado_comprobante"]["width"] = "8";
        $model["d_estado_comprobante"]["text_colour"] = "#004400";
        
        
        $model["d_usuario"]["show_grid"] = 1;
        $model["d_usuario"]["header_display"] = "Usuario";
        $model["d_usuario"]["web_width"] = "5";
        $model["d_usuario"]["order"] = "10";
        $model["d_usuario"]["text_colour"] = "#770077";
        
        
           $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Comprobante",
                    "content" => $model
                );
        
          echo json_encode($output);
?>