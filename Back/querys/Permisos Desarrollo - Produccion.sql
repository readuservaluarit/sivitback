SET FOREIGN_KEY_CHECKS = 0;
-- DELETE FROM usuario;
DELETE FROM funcion;
DELETE FROM funcion_rol;
DELETE FROM rol;

SET FOREIGN_KEY_CHECKS = 1;


SET FOREIGN_KEY_CHECKS = 0;

/* MP: La tabla USUARIO NO SE SINCRONIZA MAS entre six_test2 y VALUARIT_VALMEC
 por que six_test2 tiene todos los clientes LUIS Pristine , Mariana Enerbom etc 
 No pasar mas la tabla clientes Chequear SIEMPRE el campo activo.
 */
-- INSERT INTO usuario
-- SELECT * FROM six_test2.usuario;

INSERT INTO funcion
SELECT * FROM six_test2.funcion;

INSERT INTO funcion_rol
SELECT * FROM six_test2.funcion_rol;

INSERT INTO rol
SELECT * FROM six_test2.rol;

SET FOREIGN_KEY_CHECKS = 1;


/* Esto es un conjunto de funciones de algunos roles para asignarselos a DIRECTOR*/
INSERT INTO funcion_rol (id_funcion,id_rol)
SELECT DISTINCT id_funcion,6 FROM funcion_rol WHERE id_rol IN (3,4,7,8,10,14,15)


INSERT INTO movimiento_tipo
SELECT * FROM six_test2.movimiento_tipo WHERE id NOT IN (SELECT id FROM  valuarit_valmec_q.movimiento_tipo)