<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class Impuesto extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'impuesto';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id_tipo_impuesto' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar un tipo de impuesto',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);
    
      public $hasOne = array(
        'TipoComprobante' => array(
            'className' => 'TipoComprobante',
            'foreignKey' => 'id_impuesto',
        ),
        );

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 public $belongsTo = array(
        'Estado' => array(
            'className' => 'Estado',
            'foreignKey' => 'id_estado',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Sistema' => array(
            'className' => 'Sistema',
            'foreignKey' => 'id_sistema',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'TipoImpuesto' => array(
            'className' => 'TipoImpuesto',
            'foreignKey' => 'id_tipo_impuesto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'ImpuestoGeografia' => array(
            'className' => 'ImpuestoGeografia',
            'foreignKey' => 'id_impuesto_geografia',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'CuentaContable' => array(
            'className' => 'CuentaContable',
            'foreignKey' => 'id_cuenta_contable',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'SubTipoImpuesto' => array(
            'className' => 'SubTipoImpuesto',
            'foreignKey' => 'id_sub_tipo_impuesto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
 		'TipoComercializacion' => array( //hace referencia a la sucursal del comprobante
 				'className' => 'TipoComercializacion',
 				'foreignKey' => 'id_tipo_comercializacion',
 				'conditions' => '',
 				'fields' => '',
 				'order' => ''
 		),
 		'Provincia' => array( //hace referencia a la sucursal del comprobante
 				'className' => 'Provincia',
 				'foreignKey' => 'id_provincia',
 				'conditions' => '',
 				'fields' => '',
 				'order' => ''
 		)
        
        
);


public function getMinimoAplicacion($id_impuesto){
    
    $impuesto = $this->find('first', array(
                                                'conditions' => array('Impuesto.id' => $id_impuesto),
                                                'contain' =>false
                                                )); 
        
    
   if($impuesto)
    return $impuesto["Impuesto"]["base_imponible_desde"]; 
   else
    return 0;
}


public function getMaximoAplicacion($id_impuesto){
    
    $impuesto = $this->find('first', array(
                                                'conditions' => array('Impuesto.id' => $id_impuesto),
                                                'contain' =>false
                                                )); 
        
    
   if($impuesto)
    return $impuesto["Impuesto"]["base_imponible_hasta"]; 
   else
    return 0;
}


public function esAutogenerado($id_impuesto){
    
    $impuesto = $this->find('first', array(
                                                'conditions' => array('Impuesto.id' => $id_impuesto),
                                                'contain' =>false
                                                )); 
        
    
   if($impuesto)
    return $impuesto["Impuesto"]["autogenerado"]; 
   else
    return 0;   //por default es manual, no deberia devolver esto nunca, es por algun error que pudiera ocurrir
}



public function getDescripcion($id){
    
    $impuesto = $this->find('first', array(
                                                'conditions' => array('Impuesto.id' => $id),
                                                'contain' =>false
                                                )); 
        
    
   return $impuesto["Impuesto"]["d_impuesto"]; 
}

public function getAbreviado($id){
    
    $impuesto = $this->find('first', array(
                                                'conditions' => array('Impuesto.id' => $id),
                                                'contain' =>false
                                                )); 
        
    
   return $impuesto["Impuesto"]["abreviado_impuesto"]; 
}


public function getTablaTemporalName($id_impuesto){
    
    switch($id_impuesto){
            
            case EnumImpuesto::PERCEPIIBBBUENOSAIRES:
            case EnumImpuesto::RETENCIONIIBBBSAS:
              $nombre_tabla  = "persona_impuesto_alicuota_temporal";
            
            
            break;
            
   
            
            case EnumImpuesto::PERCEPIIBBCABA: 
            case EnumImpuesto::RETIIBBCABA:
            case EnumImpuesto::ExencionPercepCABA:    
            case EnumImpuesto::ExencionRetenCABA:    
                $nombre_tabla  = "persona_impuesto_alicuota_temporal_caba"; 
            break;
           
             }
    return $nombre_tabla;  
    
    
}

public function getTipo($id){
    
      $impuesto = $this->find('first', array(
                                                'conditions' => array('Impuesto.id' => $id),
                                                'contain' =>false
                                                )); 
        
    
   return $impuesto["Impuesto"]["id_tipo_impuesto"]; 
    
}



public function getSistema($id){
    
      $impuesto = $this->find('first', array(
                                                'conditions' => array('Impuesto.id' => $id),
                                                'contain' =>false
                                                )); 
        
    
   return $impuesto["Impuesto"]["id_sistema"]; 
    
}



public function getImpuestosArray($id_tipo_impuesto,$id_subtipo_impuesto){
    
      $impuestos = $this->find('all', array(
                                                'conditions' => array('Impuesto.id_tipo_impuesto' => $id_tipo_impuesto,'Impuesto.id_sub_tipo_impuesto' => $id_subtipo_impuesto),
                                                'contain' =>false
                                                )); 
                                                
                                                
      $array_id_impuesto = array();                                          
      if( $impuestos ){
          
          
          
          foreach($impuestos as $impuesto ){
              
              array_push($array_id_impuesto,$impuesto["Impuesto"]["id"]);
              
              
          }
          
          
          
          
          
      }
      
      return $array_id_impuesto;
}


public function EsGlobal($id_impuesto){
    
    
    $impuesto = $this->find('first', array(
                                                'conditions' => array('Impuesto.id' => $id_impuesto),
                                                'contain' =>false
                                                )); 
                                                
                                                
    if($impuesto){
        
        if($impuesto["Impuesto"]["global_proveedor"] == 1 || $impuesto["Impuesto"]["global_cliente"] == 1)
            return 1;
        else
            return 0;
    }else{
        
        return 0;
    }                                            
    
}


public function chequeoIntervaloBase($id_sistema,$id_impuesto,$id_tipo_impuesto,$id_sub_tipo_impuesto,$base_imponible_desde,$base_imponible_hasta,$id_tipo_comercializacion){
	
	/* Chequeo si hay cruzamiento de intervalos para la base desde y hasta de una retencion de ganancias*/
	$conditions = array(); 
	array_push($conditions,array("Impuesto.id <>" => $id_impuesto));
	array_push($conditions,array("Impuesto.base_imponible_desde <=" => $base_imponible_desde));
	array_push($conditions,array("OR" =>array ("Impuesto.base_imponible_hasta >=" => $base_imponible_desde)));
	array_push($conditions,array("Impuesto.id_sistema" => $id_sistema));
	array_push($conditions,array("Impuesto.id_sub_tipo_impuesto" => $id_sub_tipo_impuesto));
	array_push($conditions,array("Impuesto.id_tipo_impuesto" => $id_tipo_impuesto));
	array_push($conditions,array("Impuesto.id_tipo_comercializacion" => $id_tipo_comercializacion));
	
	
	
	
	
	$conditions2 = array();
	
	array_push($conditions2,array("Impuesto.id <>" => $id_impuesto));
	array_push($conditions2,array("Impuesto.base_imponible_desde <=" => $base_imponible_hasta));
	array_push($conditions2,array("OR" =>array ("Impuesto.base_imponible_hasta >=" => $base_imponible_hasta)));
	array_push($conditions2,array("Impuesto.id_sistema" => $id_sistema));
	array_push($conditions2,array("Impuesto.id_sub_tipo_impuesto" => $id_sub_tipo_impuesto));
	array_push($conditions2,array("Impuesto.id_tipo_impuesto" => $id_tipo_impuesto));
	array_push($conditions2,array("Impuesto.id_tipo_comercializacion" => $id_tipo_comercializacion));
	
	
	
	
	
	
	if($id_tipo_impuesto == EnumTipoImpuesto::RETENCIONES && $id_sub_tipo_impuesto == EnumSubTipoImpuesto::RETENCION_GANANCIA ){
	
		$impuesto_desde = $this->find('first', array(
			'conditions' => $conditions
			
	));
		
		
		$impuesto_hasta= $this->find('first', array(
				'conditions' => $conditions2
				
		));
	
		if($impuesto_desde || $impuesto_hasta)
			return 1;
		else
			return 0; 
		
	}else {
		
		return 0;
	}

}

	

}
