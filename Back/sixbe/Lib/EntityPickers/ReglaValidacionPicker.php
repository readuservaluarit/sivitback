<?php

class ReglaValidacionPicker extends EntityPicker{
    
    public $model = "ReglaValidacion";
    
    public $pickerTitle = "Selección de Codigo";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "C&oacute;digo", "field" => "codigo", "width" => "10%", "keyOrder" => "codigo"),
                                  array("title" => "Descripci&oacute;n", "field" => "mensaje", "width" => "80%", "keyOrder" => "mensaje")
                                );

    public function getPickerConditions($busqueda){
        
        return array('ReglaValidacion.codigo LIKE' => '%' . $busqueda['bus_unico'] . '%');
    }

}