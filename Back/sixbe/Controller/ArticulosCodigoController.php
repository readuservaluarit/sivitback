<?php


class ArticulosCodigoController extends AppController {
    
    
    
    public $name = 'ArticulosCodigo';
    public $model = 'ProductoCodigo';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    
  
  
    
    public function index($id_producto = null) {
        
        
        
        $this->loadModel($this->model);
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $id_producto = $this->getFromRequestOrSession($this->model.'.id_producto');

        

        $codigo = $this->getFromRequestOrSession($this->model.'.codigo');
       
       
            
        
        $conditions = array(); 
        
        if($id_producto!='')
            array_push($conditions, array($this->model.'.id_producto' => $id_producto)); 
            
        if($codigo!='')
            	array_push($conditions, array('Articulo.codigo' => $id_producto)); 
       
            $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
                'contain' => false,
                'conditions' => $conditions,
                'limit' => 1000, //estoo es solo para traer todos los componentes
                'page' => $this->getPageNum()
        );
            
            
        
        // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
  
          
    
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     
        
        
        
        
    //fin vista json
        
    }
   
    /**
    * @secured(ABM_USUARIOS)
    */
    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

     
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            
            try{
                if ($this->ProductoCodigo->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "El Item ha sido creado exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el item no ha podido ser asociado.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => ""
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    
     
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->ProductoCodigo->id = $id;
            
            try{ 
            	if ($this->ProductoCodigo->saveAll($this->request->data)){
                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "La asociacion de item ha sido modificado exitosamente",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('La asociacion item ha sido modificado exitosamente.', 'success');
                        $this->redirect(array('controller' => 'Componentes', 'action' => 'index'));
                        
                    } 
                }else{
                     
                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::ERROR,
                            "message" => "Ha ocurrido un error, el item no ha podido ser asociado.",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('Ha ocurrido un error, el Componente no ha podido modificarse.', 'error');
                        $this->redirect(array('controller' => 'Componentes', 'action' => 'index'));
                        
                    } 
                    
                    
                    
                    
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, el item no ha podido modificarse.', 'error');
                if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::ERROR,
                            "message" => "Ha ocurrido un error, el item no ha podido ser asociado.".$e->getMessage(),
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('Ha ocurrido un error, el item no ha podido ser asociado.', 'error');
                        $this->redirect(array('controller' => 'Componentes', 'action' => 'index'));
                        
                    } 
                
            }
            $this->redirect(array('action' => 'index'));
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
  

    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
       try{
       	if ($this->ProductoCodigo->delete($id)) {
                
                //$this->Session->setFlash('El Codigo altenerviso ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "La asociacion del item ha sido eliminada exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
    }
    

    public function home(){
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
    
    
    
    
   public function existe_codigo(){
        
        $codigo = $this->request->query['codigo'];
        $id_componente = $this->request->query['id'];
        $this->loadModel("Producto");
      
        $data= $this->Producto->find('count',array('conditions' => array(
                                                                            'codigo' => $codigo,
                                                                            'id !=' =>$id_componente
                                                                            )
                                                                            ));
        
        if($data>0)
        	$error = EnumError::ERROR;
        else
        	$error = EnumError::SUCCESS;
        		
       	$output = array(
        				"status" => $error,
        				"message" => "",
        				"content" => $data
        		);
        $this->set($output);
       	$this->set("_serialize", array("status", "message", "content"));
        		
    }
    
    
  
  public function getModel($vista='default')
  {      
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model,$vista); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    } 
    
    
   
    
    
  public function editforView($model,$vista)
  { 
  
      $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
        
      $this->set('model',$model);
      $this->set('model_name',$this->model);
      Configure::write('debug',0);
      $this->render($vista);
     
  }

    
    
}
?>