<?php
class GeoreferenciaController extends AppController {

    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $theme = 'Grape';
    public $name = 'Georeferencia';
    
    public function index($x_id, $y_id, $departamento, $localidad, $calle, $nro='') {
        
        $this->layout = 'ajax';
        
        $numero = ($nro=='-1'?'':$nro);
         
        $this->set('departamento', $departamento);
        $this->set('localidad', $localidad);
        $this->set('calle', $calle);
        $this->set('nro', $numero);
        $this->set('x_id', $x_id);
        $this->set('y_id', $y_id);
        $this->set('address', trim($calle) . ' ' . trim($numero) . ', ' . trim($localidad) . ', ' . trim($departamento) . ', ' . trim('ARGENTINA'));
        
    }
    
}
?>