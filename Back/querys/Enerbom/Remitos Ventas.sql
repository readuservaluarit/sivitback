SELECT 
  matcompr_id.compr_id AS id_otro_sistema,
  remito.remnum AS nro_comprobante,
  1 AS id_persona, /* MAL*/
  remito.remptven AS id_punto_venta,
  detrem.pedoc AS orden_compra_externa,
  1 AS id_estado_comprobante,  /* MAL DETERMINAR CON CUAL MANDAN*/
  remito.remfec AS fecha_entrega,
  remito.remmon AS id_moneda,
  planpag.plcodalf AS id_forma_pago,
  '1' AS aprobado /* NO VA MAS, VA EL DEFINITIVO*/,
  remito.remfec AS fecha_aprobacion,
  SUM(detrem.dervalor) AS subtotal_bruto,
  0 AS descuento,
  0 AS importe_descuento,
  SUM(detrem.dervalor) AS subtotal_neto,
  SUM(detrem.dervalor) AS total_comprobante,
  '' AS observacion 
FROM
  matcompr_id,
  remito,
  matpers_id,
  planpag,
  detrem 
WHERE matcompr_id.compr_tipo = 'C' 
  AND matcompr_id.compr_pvta = remito.remptven 
  AND matcompr_id.compr_nro = remito.remnum 
  AND matpers_id.persona_tipo = 'C' 
  AND matpers_id.persona_cod = remito.clcod 
  AND matpers_id.persona_suc = remito.sucnum 
  AND planpag.plpcod = remito.remplp 
  AND remito.remnum = detrem.remnum 
  AND remito.remptven = detrem.remptven 
GROUP BY remito.remptven,
  remito.remnum 
