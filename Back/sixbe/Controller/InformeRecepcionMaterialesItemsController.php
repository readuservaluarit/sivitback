<?php

App::uses('ComprobanteItemsController', 'Controller');

class InformeRecepcionMaterialesItemsController extends ComprobanteItemsController {
    
    public $name = EnumController::InformeRecepcionMaterialesItems;
    public $model = 'ComprobanteItem';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
  
  
     /**
    * @secured(CONSULTA_INFORME_RECEPCION_MATERIAL)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model);
        
        
        $filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');
        $id_moneda_expresion = $this->getFromRequestOrSession('Comprobante.id_moneda_expresion');
        
        
        //$filtro_agrupado = 3; //MP antes tenia uno 1 si manda un /index plano me agrupaba los items y no me traie los campos q necesito para el form PIMasterDetail0
        $this->ComprobanteItem->paginateAgrupado($this,$filtro_agrupado,$conditions,
        		$this->numrecords,$this->getPageNum(),
        		array('Unidad','Comprobante'=>array('EstadoComprobante','Moneda','PuntoVenta','Persona','TipoComprobante'),
					  'Producto'=>array('ProductoTipo'),
					  'ComprobanteItemOrigen',
					  'ComprobanteItemLote'=>array("Lote"=>array("TipoLote"))),
			  $this->ComprobanteItem->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_comprobante')),
			  $id_moneda_expresion); // AGREGADO PARA EL ORDEN DE ISCO
        		
        		
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
    
    
        
        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'Remito.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'Remito.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$producto ){
			
        	
        	
        	
        	switch($filtro_agrupado){
        		
        		
        		
        		case "":
        		case EnumTipoFiltroComprobanteItem::individual:
        		case EnumTipoFiltroComprobanteItem::agrupadaporComprobante:
        			
        			if(isset($producto["Comprobante"]["PuntoVenta"]["numero"]))
        				$producto["ComprobanteItem"]["pto_nro_comprobante"] = $this->ComprobanteItem->GetNumberComprobante($producto["Comprobante"]["PuntoVenta"]["numero"],$producto["Comprobante"]["nro_comprobante"]);
        				
        				
			
        				if(isset($producto["Comprobante"])&& isset($producto["Comprobante"]["Persona"]) ){
        					$producto["ComprobanteItem"]["razon_social"] = $producto["Comprobante"]["Persona"]["razon_social"];
        					$producto["ComprobanteItem"]["persona_tel"]  =  $producto["Comprobante"]["Persona"]["tel"];
        				}
        				
				if(isset( $producto["ComprobanteItemOrigen"]) ){
        	     
        		 $cantidad_remito_origen = $producto["ComprobanteItemOrigen"]["cantidad"];
        		 
        		 $total_irm = $this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItemOrigen"],$this->Comprobante->getTiposComprobanteController(EnumController::InformeRecepcionMaterialesItems));
                 
        		 $producto["ComprobanteItem"]["total_remito_origen"] = (string) $cantidad_remito_origen;
        		 $producto["ComprobanteItem"]["total_pendiente_irm"] = 
        		 (string)  number_format((float) ($cantidad_remito_origen/*10*/
        		 		- $this->ComprobanteItem->GetCantidadItemProcesados($total_irm))/*2*/, $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');;
        		 
        		 
        	}else 
        	{
        		
        		$producto["ComprobanteItem"]["total_remito_origen"]  = 0;
        		$producto["ComprobanteItem"]["total_pendiente_irm"]  = 0;
        	}
			  
			$producto["ComprobanteItem"]["d_producto"] = $producto["Producto"]["d_producto"];
			$producto["ComprobanteItem"]["d_estado_comprobante"] = $producto["Comprobante"]["EstadoComprobante"]["d_estado_comprobante"];
			
			
			
			$producto["ComprobanteItem"]["codigo"] = $this->ComprobanteItem->getCodigoFromProducto($producto);
			
			
			$producto["ComprobanteItem"]["id_unidad"] = $producto["ComprobanteItemOrigen"]["id_unidad"];//la unidad viene dada en el Remito de Compra
			$producto["ComprobanteItem"]["cantidad_rechazada"] = (string) ($producto["ComprobanteItem"]["cantidad"] 
																		 - $producto["ComprobanteItem"]["cantidad_cierre"]);

			$producto["ComprobanteItem"]["Lotes"]	 = array();														 
			foreach ($producto["ComprobanteItemLote"] as &$lote2){
				
				
				
				 $lote2["codigo"] = $lote2["Lote"]["codigo"];//Subo un nivel
				 $lote2["id_tipo_lote"] = $lote2["Lote"]["id_tipo_lote"];//Subo un nivel
				 $lote2["fecha_emision"] = $lote2["Lote"]["fecha_emision"];//Subo un nivel
				 $lote2["id_lote"] = $lote2["Lote"]["id"];//Subo un nivel
				 
				 
				 
				 if($lote2["Lote"]["tiene_certificado"] == 1)
				 	$lote2["d_tiene_certificado"] = "Si";
				 else
				 	$lote2["d_tiene_certificado"] = "No";
				 
				 	
				 if($lote2["Lote"]["validado"] == 1)
				 	$lote2["d_validado"] = "Si";
				 else
				 	$lote2["d_validado"] = "No";
				 			
				 
				 	
				 	
				 
				 	
				 //$lote2["id_lote"] = $lote2["Lote"]["d_lote"];//NO SE USA MAS [deprecate con la descripcion esta sobrevista]
				  $lote2["d_tipo_lote"] = $lote2["Lote"]["TipoLote"]["d_tipo_lote"];
				  
				  unset($lote2["Lote"]["TipoLote"]);
				  // unset($lote2["Lote"]); 
				  $lote_aux['ComprobanteItemLote'] = $lote2;
				  array_push($producto["ComprobanteItem"]["Lotes"], $lote_aux);
				  
				  
				  
			}
			
			
			// $producto["ComprobanteItem"]["Lotes"] = $producto["ComprobanteItemLote"];
			
			unset($producto["ComprobanteItemLote"]);
			unset($producto["Producto"]);
			unset($producto["Comprobante"]);
			unset($producto["ComprobanteItemOrigen"]);
			
			break;
			
        		case EnumTipoFiltroComprobanteItem::agrupadaporPersona:
        			
        			$producto["ComprobanteItem"]["total"] = $producto[0]["total"] ;
        			$producto["ComprobanteItem"]["razon_social"] = $producto["Persona"]["razon_social"];
        			
        		break;
			
        	}
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    
      /**
    * @secured(CONSULTA_INFORME_RECEPCION_MATERIAL)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
     
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
        
        
      
 
    }
    

    
   
    
}
?>