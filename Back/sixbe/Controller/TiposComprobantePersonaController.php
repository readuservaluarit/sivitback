<?php


class TiposComprobantePersonaController extends AppController {
    public $name = 'TiposComprobantePersona';
    public $model = 'TipoComprobantePersona';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    
    
     /**
    * @secured(CONSULTA_TIPO_COMPROBANTE_PERSONA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $id_tipo_comprobante = $this->getFromRequestOrSession('TipoComprobantePersona.id_tipo_comprobante');
        $id_persona = $this->getFromRequestOrSession('TipoComprobantePersona.id_persona');
        $codigo_persona = $this->getFromRequestOrSession('TipoComprobantePersona.codigo_persona');
      
		
        $conditions = array(); 

        if($id_tipo_comprobante!="")
        	array_push($conditions, array('TipoComprobantePersona.id_tipo_comprobante =' =>  $id_tipo_comprobante)); 
         
        if($id_persona!="")
        	array_push($conditions, array('TipoComprobantePersona.id_persona =' =>  $id_persona));
        
        if($codigo_persona!="")
        	array_push($conditions, array('TipoComprobantePersona.codigo_persona =' =>  $codigo_persona));
			
			
        $this->paginado = 0;
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' => array("Persona","TipoComprobante"),
            'conditions' => $conditions,
            'limit' => 10,
            'page' => $this->getPageNum(),
        
        );
        
     
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];

        
        
        if($data){
	        foreach($data as &$dato){
	        	
	        	
	            
	            $dato["TipoComprobantePersona"]["codigo_tipo_comprobante"] =  $dato["TipoComprobante"]["codigo_tipo_comprobante"];
	            $dato["TipoComprobantePersona"]["razon_social"] =  $dato["Persona"]["razon_social"];
	            $dato["TipoComprobantePersona"]["codigo_persona"] =  $dato["Persona"]["codigo"];
			
	        
	
	            unset($dato["TipoComprobante"]);
	            unset($dato["Persona"]);
	          
	            
	        }
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
        		"content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
    
        
        
        
        
    //fin vista json
        
    }
    
   

    /**
    * @secured(CONSULTA_TIPO_COMPROBANTE_PERSONA)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_add = '';
            
            try{
            	if ($this->TipoComprobantePersona->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "El Tipo  ha sido creado exitosamente";
                    $tipo = EnumError::SUCCESS;
                    $id_add = $this->TipoComprobantePersona->id;
                   
                }else{
                    $mensaje = "Ha ocurrido un error, el Tipo no ha podido ser creado.";
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error, el Tipo NO ha podido ser creado. ".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
            
            
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" =>$id_add
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->TipoComprobantePersona->id = $id;
            
            try{ 
            	if ($this->TipoComprobantePersona->saveAll($this->request->data)){
                    
                    $error = EnumError::SUCCESS;
                    $message = "El Tipo ha sido modificado exitosamente";
                   
                }else{
                    $error = EnumError::ERROR;
                    $message = "El Tipo No sido modificado";   
                    
                }
            }catch(Exception $e){
                    $error = EnumError::ERROR;
                    $message = "El Tipo No sido modificado".$e->getMessage();   
                
            }
            
             if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "El Tipo ha sido modificado exitosamente",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
            }else{
                $this->Session->setFlash('El Tipo ha sido modificado exitosamente.', 'success');
                $this->redirect(array('controller' => 'OrdenCompra', 'action' => 'index'));
                
            } 
           
        }
        
      
    }
    
  
    /**
    * @secured(ABM_USUARIOS, READONLY_PROTECTED)
    */
    function delete($id) {
    	
    	$this->loadModel($this->model);
    	
    	$mensaje = "";
    	$status = "";
    	$this->TipoComprobantePersona->id = $id;
    	try{
    		
    		if ($this->TipoComprobantePersona->delete()) {
    			
    			
    			
    			
    			$status = EnumError::SUCCESS;
    			$mensaje = "El Tipo ha sido eliminado exitosamente.";
    			
    			
    		}
    		
    		else{
    			//throw new Exception();
    			$status = EnumError::ERROR;
    			$mensaje = "El Tipo ha sido eliminado exitosamente.";
    		}
    		$output = array(
    				"status" => $status,
    				"message" => $mensaje,
    				"content" => ""
    		);
    		
    	}
    	catch(Exception $ex)
    	{
    		//$this->Session->setFlash($ex->getMessage(), 'error');
    		
    		$status = EnumError::ERROR;
    		$mensaje = $ex->getMessage();
    		$output = array(
    				"status" => $status,
    				"message" => $mensaje,
    				"content" => ""
    		);
    	}
    	
    	if($this->RequestHandler->ext == 'json'){
    		$this->set($output);
    		$this->set("_serialize", array("status", "message", "content"));
    		
    	}else{
    		$this->Session->setFlash($mensaje, $status);
    		$this->redirect(array('controller' => $this->name, 'action' => 'index'));
    		
    	}
    	
    	
    }
    

	 // public function getModel( $vista='normal')
	 // {    
         // $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
         // $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
         // $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
     // }
    
    
     // private function editforView($model,$vista)
	 // {  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
       // $this->set('model',$model);
       // Configure::write('debug',0);
       // $this->render($vista);    
     // }
	 
     
     /**
    * @secured(CONSULTA_TIPO_COMPROBANTE_PERSONA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
     private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
 
    }
    
    
     
    
   
    
}
?>