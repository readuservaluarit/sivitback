<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <title><?php echo Configure::read("loginTitle") ?></title>
  <meta name="description" content="">
  <meta name="author" content="">
  

  <?php

    //Favicon
    echo $this->Html->meta('favicon',$this->webroot . '/favicon.ico',array('type' => 'icon'));

	echo $this->Html->css('bootstrap/css/bootstrap.css');
	echo $this->Html->css('bootstrap/css/bootstrap-custom.css');
	echo $this->Html->css('bootstrap/css/bootstrap-responsive.css');
	//echo $this->Html->css('bootstrap/css/variables.less');
	//echo $this->Html->css('bootstrap/css/bootswatch.less');
	echo $this->Html->script('jquery/jquery-1.8.3.min.js');
	echo $this->Html->script('bootstrap/js/bootstrap.js');
    echo $this->Html->script('validator.js');
	
  
  ?>
	
</head>

<body>
  
  <!--Begin bootstrap login -->
  
  <a id="btnIngresar" style="display:none;" class="btn btn-primary btn-large" data-toggle="modal" href="#betaModal">Ingresar</a> 
	<div id="betaModal" class="modal fade" data-keyboard="false" data-backdrop="static">
		<div class="modal-header" style="margin-bottom:10px;">
				<!--<button class="close" data-dismiss="modal">×</button>-->
				<h4>
                    <?php echo Configure::read("loginHeader") ?>
                </h4>
		</div>
        <?php
            echo $this->Session->flash();
            echo $this->Session->flash('auth');
        ?>
	<div class="modal-body">
		<div class="row-fluid">
			<div class="span12">
				<div class="span6">
				<div class="logowrapper" style="text-align:center;">
                    <?php 
                            echo $this->Html->image(Configure::read("loginLogo"), array('plugin' => false, 'alt' => 'App Logo', 'class' => 'logoicon'));
                         
                    ?>
				</div>
				</div>
				<div class="span6">
					<?php
					
					
						echo $this->Form->create('Usuario', array('id' => 'login-form', 'class' => 'form-horizontal')); 
						 
						

					?>
						<p class="help-block">Usuario</p>
						<div class="input-prepend">
							<span class="add-on">*</span> 
							<?php echo $this->Form->input('username',  array('label' => false, 'div' => false, 'class' => 'prependedInput', 'size' => '16')); ?>
						</div>
						<p class="help-block">Contrase&ntilde;a</p>
						<div class="input-prepend">
							<span class="add-on">*</span>
							<?php echo $this->Form->input('password',  array('label' => false, 'div' => false, 'class' => 'prependedInput', 'size' => '16', 'type' => 'password')); ?>
                           
                           <?php echo $this->Form->input('navegador',  array('label' => false, 'div' => false, 'class' => 'prependedInput', 'size' => '16', 'type' => 'hidden')); ?>
                           
						</div>
                        
                        <p class="help-block">Modo Inicio</p>
                        <div class="input-prepend">
                            <span class="add-on">*</span>
                            <select id="data[Test][activo]" name="data[Test][activo]">
                            <option value="0">Productivo</option>
                            <option value="1">Test</option>
                            </select>
                           
                        </div>
                        
                        <div class="pull-right" style='padding-right:10px;'>
                            <a data-toggle="tab" id="forgotpassword" style="cursor:pointer;cursor:hand" >Olvid&eacute; mi contrase&ntilde;a</a>
                        </div>
                        
                     	
                        <div class="pull-right" style='padding-right:10px;'>
                            <a data-toggle="tab" id="closeallsessions" style="cursor:pointer;cursor:hand">Cerrar sesion en todos los dispositivos</a>
                        </div>
                    
                         <hr/>
						<div class="help-block" style="text-align:center;">
							<button type="submit" class="btn btn-large btn-primary"><i class="icon-share icon-white"></i>&nbsp;Ingresar</button>
						</div>
						<div class="help-block" style="text-align:center;">

						</div>
					</form>
                    
    				<?php
						
						
						echo $this->Form->create('Usuario', array('id' => 'remind-form',
					
					'url'   => array(
								               'controller' => 'Usuarios','action' => 'remind'
								           ),
					
					 'class' => 'form-horizontal oculto'));
					?>
                        <div id='validationMsg_remind' style='display:none;'></div>
						<p class="help-block">Usuario</p>
						<div class="input-prepend">
							<span class="add-on">*</span> 
							<?php echo $this->Form->input('usernamer',  array('label' => false, 'div' => false, 'class' => 'prependedInput', 'size' => '16')); ?>
						</div>
                        <div class="pull-right" style='padding-right:10px;'>
                            <a data-toggle="tab" id="volver" style="cursor:pointer;cursor:hand">Volver</a>
                        </div>
                         <hr/>
						<div class="help-block" style="text-align:center;">
							<button id="btn_remind" type="submit" class="btn btn-large btn-primary"><i class="icon-share icon-white"></i>&nbsp;Recuperar</button>
						</div>
                        <script type="text/javascript">
                            $('#btn_remind').click(function() {
                                Validator.clearValidationMsgs('validationMsg_remind');
                                
                                var form = 'remind-form';
                                var validator = new Validator(form);
                                validator.validateRequired('UsuarioUsernamer','Debe ingresar un Usuario');
                                
                                if(!validator.isAllValid()){
                                    validator.showValidations('', 'validationMsg_remind');
                                    return false;   
                                }
                                
                                return true;
                            })
                        </script>
					</form>
				</div>
			</div>
		</div>
	</div>
    <div class="modal-footer">
        <p><i><?php echo Configure::read("loginFooterText") ?></i></p>
    </div>
</div>
  
  <!-- End bootstrap login -->
  
  <script type="text/javascript">
  
    $('#login-form').attr('action', '<?php echo Router::url(array('controller' => 'Usuarios', 'action' => 'login'))?>');
  
    $("#closeallsessions").click(function(){
  
  
  $('#login-form').attr('action', '<?php echo Router::url(array('controller' => 'Usuarios', 'action' => 'logoutAllSessions'))?>');
  

   $('#login-form').submit();
   
    
});


function inicioparameter(username,password){


$('#UsuarioUsername').val(username);
$('#UsuarioPassword').val(password);
$("#login-form").submit();

}
  

	$(document).ready(function() {
		$('#btnIngresar').click();
        
        $('#forgotpassword').click(function() {
            $('form').hide();
            $('#remind-form').show();
        });
        $('#volver').click(function() {
            $('form').hide();
            $('#login-form').show();
        });
        
        var nombre_navegador = "";
        var userAgent = navigator.userAgent;
        if (userAgent.indexOf("MSIE") != -1) {
            nombre_navegador = "MSIE";
        }
        else if (userAgent.indexOf("Firefox") != -1) {
            nombre_navegador = "firefox";
        }
        else if (userAgent.indexOf("Opera") != -1) {
            nombre_navegador = "opera";
        }
        else if (userAgent.indexOf("iPad") != -1) {
            nombre_navegador = "iPad";
        }
        else if (userAgent.indexOf("iPhone") != -1) {
            nombre_navegador = "iPhone";
        }
        else if (userAgent.indexOf("Android") != -1) {
            nombre_navegador = "Android";
        }
        else if (userAgent.indexOf("Chrome") != -1) {
            nombre_navegador = "Chrome";
        }
        else if (userAgent.indexOf("Safari") != -1) {
            nombre_navegador = "Safari";
        }
                
        nombre_navegador = nombre_navegador + "-ver: " + navigator.appVersion;

        $("#UsuarioNavegador").attr("value", nombre_navegador);
                
	});
		
  </script>

</body>
</html>
