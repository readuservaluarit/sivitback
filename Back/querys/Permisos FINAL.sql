/*1 ) Agregar todas las funcione a  ROL - ADMINISTRADOR - MENOS LOS Q YA TIENE*/

	INSERT INTO funcion_rol (id_rol,id_funcion) 
	SELECT 1, FB.id FROM funcion AS FB 
	WHERE  NOT EXISTS(SELECT FR.id FROM funcion_rol AS FR
										WHERE FB.id = FR.id_funcion  AND FR.id_rol = 1)


/*2 ) agregar permisos  al CONTADOR en funcion del ADMINITRADOR */
DELETE  FROM funcion_rol WHERE id_rol = 18;


INSERT INTO funcion_rol (id_funcion, id_rol) 
SELECT 
  id_funcion,
  18 
FROM
  funcion_rol 
WHERE id_rol = 1;
  DELETE 
  FROM
    funcion_rol 
  WHERE id_rol = 18 
    AND id_funcion IN 
    (SELECT 
      id 
    FROM
      funcion 
    WHERE c_funcion LIKE '%operativa%' 
      OR c_funcion LIKE '%stock%' 
      OR c_funcion LIKE '%calidad%' 
      OR c_funcion LIKE '%conciliacion%' 
      OR c_funcion LIKE '%RAIZ_USUARIO%' 
      OR c_funcion LIKE '%MENU_USUARIO%' 
      OR c_funcion LIKE '%REPORTE_PEDIDO_INTERNO_AGRUPADO_PRODUCTO%' 
      OR c_funcion LIKE '%MENU_REMITO%' 
      OR c_funcion LIKE '%MENU_COTIZACION%' 
      OR c_funcion LIKE '%MENU_PI_CALENDARIO_VTO%' 
      OR c_funcion LIKE '%MENU_SISTEMA%' 
      OR c_funcion LIKE '%MENU_PACKAGING%'
      OR c_funcion LIKE '%MENU_MATERIA_PRIMA%'
      OR c_funcion LIKE '%MENU_COMPONENTE%'
      OR c_funcion LIKE '%MENU_BIEN_USO%'
      OR c_funcion LIKE '%MENU_INSUMO_INTERNO%'
      OR c_funcion LIKE '%MENU_PRODUCTO'
      OR c_funcion LIKE '%MENU_PERIODO_IMPOSITIVO'
      OR c_funcion LIKE '%MENU_TIPO_IVA'
      OR c_funcion LIKE '%MENU_IVA'
      OR c_funcion LIKE '%MENU_ESTADO'
      OR c_funcion LIKE '%MENU_TIPO_COMPROBANTE'
      OR c_funcion LIKE '%MENU_MOVIMIENTO_CAJA%'
      
      );
	  
	  
	  /*AAGREGAR PERMISOS ESPECIFICOS A GESTOR OPERATICO */ 
	  /*DUPLICAR ARTICULOS*/
	  	INSERT INTO funcion_rol (id_rol,id_funcion) 
	SELECT 7, FB.id FROM funcion AS FB 
	WHERE  id IN(
1952 ,
1953,
1843,
1915,
1909,
1984,
1939,
1821) AND
	
	NOT EXISTS(SELECT FR.id FROM funcion_rol AS FR
			  WHERE FB.id = FR.id_funcion  AND FR.id_rol = 7)