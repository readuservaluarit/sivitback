SELECT 


IF(
  (SUM(ci.cantidad)-  IFNULL(
    (SELECT /* SUMO LAS CANTIDADES DE ESE PRODUCTO QUE SE REMITIERON DEL CIRCUITO PEDIDO-REMITO*/
      SUM(cantidad) 
    FROM
      comprobante_item 
      
	JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`
	WHERE id_tipo_comprobante = 203 AND (
					  id_estado_comprobante = 115    /*  CERRADO*/
					  OR id_estado_comprobante = 116 /*  CERRADO CAE */
					  OR id_estado_comprobante = 118 /*  Remitido */
					)
		AND id_producto = ci.`id_producto`
		AND id_comprobante_item_origen = ci.`id`
      
      
      ),
    0
  ) -
  IFNULL(
    (SELECT /* SUMO LAS CANTIDADES DE ESE PRODUCTO QUE SE REMITIERON DEL CIRCUITO FACURA-REMITO*/
      SUM(cantidad) 
    FROM
      comprobante_item 
      
	JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`
	WHERE id_tipo_comprobante = 203 AND (
					  id_estado_comprobante = 115    /*  CERRADO*/
					  OR id_estado_comprobante = 116 /*  CERRADO CAE */
					  OR id_estado_comprobante = 118 /*  Remitido */
					)
		AND id_producto = ci.`id_producto`
		
		AND id_comprobante_item_origen IN (
		
		
		SELECT comprobante_item.id FROM comprobante_item
		
		JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`
		
		 WHERE id_comprobante_item_origen = ci.`id`
	         AND id_tipo_comprobante IN (801,802)
		 
		  )
      
      
      ),
    0
  )
  
  
  
  )<0,0,SUM(ci.cantidad)-  IFNULL(
    (SELECT /* SUMO LAS CANTIDADES DE ESE PRODUCTO QUE SE REMITIERON*/
      SUM(cantidad) 
    FROM
      comprobante_item 
      
	JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`
	WHERE id_tipo_comprobante = 203 AND (
					  id_estado_comprobante = 115    /*  CERRADO*/
					  OR id_estado_comprobante = 116 /*  CERRADO CAE */
					  OR id_estado_comprobante = 118 /*  Remitido */
					)
		AND id_producto = ci.`id_producto`
			AND id_comprobante_item_origen IN (
		
		
		SELECT comprobante_item.id FROM comprobante_item
		
		JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`
		
		 WHERE id_comprobante_item_origen = ci.`id`
	         AND id_tipo_comprobante IN (801,802)
		 
		  )
      
      
      ),
    0
  ) )
  
  
  
  
  AS pendiente,
  ci.id_producto 
FROM
  comprobante_item ci 
  JOIN comprobante ON comprobante.id = ci.id_comprobante
WHERE
    
    comprobante.`id_estado_comprobante` = 1
    AND comprobante.`id_tipo_comprobante` = 202
  AND ci.activo = 1 
GROUP BY ci.id_producto 
ORDER BY id_producto ASC;