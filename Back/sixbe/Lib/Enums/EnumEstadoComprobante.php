<?php

class EnumEstadoComprobante
{

    const Abierto = 1;
    const Anulado = 2;
    const Cancelado = 3; 
    const Migrado = 4; 
    const EnCustodia = 5; 
    const Acreditado = 6; 
    const AEnviarInformacion = 7; 
    const Cumplida = 9; 
    const CerradoConCae = 115; 
    const CerradoReimpresion = 116; 
    const Autorizado = 117; 
    const Remitido = 118; 
/*
"1"    "ABIERTO"    "0"
"2"    "ANULADO"    "1"
"3"    "CANCELADO"    "1"
"4"    "MIGRADO"    "1"
"5"    "EN CUSTODIA"    "0"
"6"    "ACREDITADO"    "1"
"7"    "A ENVIAR INFORMACION"    "0"
"8"    "INFORMACION ENVIADA"    "0"
"71"    "A ENVIAR INFORMACION (A)"    "0"
"73"    "A ENVIAR INFORMACION (C)"    "0"
"81"    "INFORMACION ENVIADA (A)"    "0"
"83"    "INFORMACION ENVIADA (C)"    "0"
"100"    "RECHAZADA POR AFIP"    "1"
"101"    "RECHAZADA POR AFIP MOTIVO 01"    "1"
"102"    "RECHAZADA POR AFIP MOTIVO 02"    "1"
"103"    "RECHAZADA POR AFIP MOTIVO 03"    "1"
"104"    "RECHAZADA POR AFIP MOTIVO 04"    "1"
"105"    "RECHAZADA POR AFIP MOTIVO 05"    "1"
"106"    "RECHAZADA POR AFIP MOTIVO 06"    "1"
"107"    "RECHAZADA POR AFIP MOTIVO 07"    "1"
"108"    "RECHAZADA POR AFIP MOTIVO 08"    "1"
"109"    "RECHAZADA POR AFIP MOTIVO 09"    "1"
"110"    "RECHAZADA POR AFIP MOTIVO 10"    "1"
"111"    "RECHAZADA POR AFIP MOTIVO 11"    "1"
"112"    "RECHAZADA POR AFIP MOTIVO 12"    "1"
\N    ""    "0"

*/
}

  
?>