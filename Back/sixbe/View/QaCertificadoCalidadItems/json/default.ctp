<?php 


   		$model["d_categoria"]["show_grid"] = 1;
		$model["d_categoria"]["type"] = EnumTipoDato::cadena;
        $model["d_categoria"]["header_display"] = "C&adegoria";
        
        
		$model["n_item"]["show_grid"] = 1;
        $model["n_item"]["header_display"] = "Nro. Item";
        $model["n_item"]["order"] = "1";
        $model["n_item"]["width"] = "7";
		
        $model["codigo_hijo"]["show_grid"] = 1;
		$model["codigo_hijo"]["type"] = EnumTipoDato::cadena;
        $model["codigo_hijo"]["header_display"] = "C&oacute;digo";
        $model["codigo_hijo"]["order"] = "2";
        $model["codigo_hijo"]["width"] = "20";
		
		// $model["componente_codigo"]["show_grid"] = 1;
        // $model["componente_codigo"]["header_display"] = "C&oacute;digo";
        // $model["componente_codigo"]["order"] = "2";
        // $model["componente_codigo"]["width"] = "20";
        
        $model["d_producto_hijo"]["show_grid"] = 1;
        $model["d_producto_hijo"]["type"] = EnumTipoDato::cadena;
        $model["d_producto_hijo"]["header_display"] = "Art&iacute;culo Parte";
        $model["d_producto_hijo"]["order"] = "3";
        $model["d_producto_hijo"]["width"] = "35";
        
        $model["lote"]["show_grid"] = 1;
        $model["lote"]["header_display"] = "Lote/s";
        $model["lote"]["order"] = "4";
        $model["lote"]["width"] = "75";
		$model["lote"]["text_colour"] = "#0000FF";
        

 $output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "QaCertificadoCalidadItem",
				"content" => $model
                );
        echo json_encode($output);
?>