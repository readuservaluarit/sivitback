<?php
/**
 * Security Manager
 *
 */

App::import('Lib', 'AnnotationsManager');
App::uses('SessionHelper', 'View/Helper');


class SecurityManager 
{
    
    
	
	public static function checkSecurity($appController, $controller, $action){
        
        $excludedControllers = array('ErrorController');
        
        if(!in_array($controller,$excludedControllers)){
            
            //Obtengo las annotations de la clase
            $annotations = AnnotationsManager::getClassAnnotations($controller);
            $securedAnnotations = SecurityManager::getSecuredAnnotations($annotations);

            
		    //Obtengo las annotations del metodo de la accion
		    $annotations = AnnotationsManager::getMethodAnnotations($controller, $action);
		    $securedAnnotations = array_merge($securedAnnotations, SecurityManager::getSecuredAnnotations($annotations));
		    
            
		    if(!self::userHasPermission($appController, $securedAnnotations)){
             
              if(array_key_exists("ext",$appController->request->params))
                $ext = $appController->request->params["ext"];
              else
                $ext = ""; 
             
              /* if($ext != 'json')          
                    $appController->redirect(array('controller' => 'Security', 'action' => 'permisos_insuficientes'));
               else {*/
               
             
              	/*Ahora sino tiene permisos siempre devuelve json, esto hay que mejorarlo con el user_agent*/
              	header('Content-Type: application/json');
              	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
              	header("Cache-Control: no-cache");
              	header("Pragma: no-cache");
              	
              
             
                         $output = array(
                            "status" => "no_permission",
                         		"message" => "Usted no posee los permisos suficientes para ejecutar esta acci&oacute;n.".json_encode($appController->request->params),
                            "content" => "",
                            
                        );
                        //$appController->response->type('json');
                        echo json_encode($output);
                        
                        if(!isset($_SESSION["creando_cache"]))    
                     		die();
                       
          
            
                    
                   
               //}
                   
            }    
        }
	
	}
	
	public static function getSecuredAnnotations($annotations){
	
		$annotationsArray = explode("@", $annotations);
		
		$annotationsString = "";
		
		foreach($annotationsArray as $annotation){
		
			if ( substr_count(strtoupper ($annotation), 'SECURED') > 0){
				$annotationsString = strtoupper($annotation);
				break;
			}
		}
		
		$annotationsString = str_replace('SECURED','',$annotationsString);
        $annotationsString = str_replace(':','',$annotationsString);
		$annotationsString = str_replace('(','',$annotationsString);
        $annotationsString = str_replace(')','',$annotationsString);
        $annotationsString = trim($annotationsString);
		
		$securedAnnotations = explode(",", $annotationsString);
        
        return $securedAnnotations;
        
	
	}
    
    public static function userHasPermission($controller, $functions, $functionsOperator = 'AND'){
         
         
         if($functions == null)
            return true;
         
         $username = $controller->Session->read('Auth.User.username');
         
         $procesadas = array();
         $in = "";
         
         foreach($functions as $function){
     
             if(trim($function) != '' and !in_array($function, $procesadas)){
             
                 //Si la funcion esta protegida para los roles solo lectura, y el rol del usuario es de solo lectura entonces no tiene permiso.
                 if (trim($function) == "READONLY_PROTECTED"){
                    if(self::isReadOnlyRole($controller))
                        return false;
                 }
                 else //Agrego la funcion a la pila
                    array_push($procesadas, $function);
                 
                 if(trim($in) != '')
                    $in .= ",";
                
                    $in .=  "'" . $function . "'"; 
                 
                
             }    
             
         }
         
         //Si no habia ninguna funcion, devuelvo true
         if(trim($in) == '')
            return true;
         
         $in = "(" . $in .")";
         
         //TODO: Quitar el query y acceder por model
             
         $sql = "select count(1) as cant 
                from usuario u
                join rol r on (r.id = u.id_rol)
                join funcion_rol fr on (r.id = fr.id_rol)
                join funcion f on (fr.id_funcion = f.id)
				join tipo_funcion on tipo_funcion.id = f.id_tipo_funcion
				join modulo on modulo.id = tipo_funcion.id_modulo
                where modulo.habilitado=1 and u.username = '"  . $username . "' and f.c_funcion in " . $in; 
          
         
         App::uses('Usuario', 'Model');
         $model = new Usuario();
         $res = $model->query($sql);
         
         $cant = $res[0][0]['cant'];
         
         if (($functionsOperator == 'AND' && $cant < sizeof($procesadas)) || ($functionsOperator == 'OR' && $cant == 0 ))
            return false;
         else
            return true;
         
     }

    public static function isReadOnlyRole($controller)
    {
        $rol = $controller->Session->read('Auth.User.Rol');
        
        return $rol['solo_lectura']==1;
    }
    
    
    public function permiso_particular($permiso,$username){
        
         $sql = "select count(1) as cant 
                from usuario u
                join rol r on (r.id = u.id_rol)
                join funcion_rol fr on (r.id = fr.id_rol)
                join funcion f on (fr.id_funcion = f.id)
				join tipo_funcion on tipo_funcion.id = f.id_tipo_funcion
				join modulo on modulo.id = tipo_funcion.id_modulo
                where u.username = '"  . $username . "' and f.c_funcion in ('" . $permiso."') and modulo.habilitado=1"; 
          
         
         App::uses('Usuario', 'Model');
         $model = new Usuario();
         $res = $model->query($sql);
         
         $cant = $res[0][0]['cant'];
         return $cant;
        
    }
    
    public function getallpermision($username){
        
          $sql = "select f.c_funcion
                from usuario u
                join rol r on (r.id = u.id_rol)
                join funcion_rol fr on (r.id = fr.id_rol)
                join funcion f on (fr.id_funcion = f.id)
				join tipo_funcion on tipo_funcion.id = f.id_tipo_funcion
				join modulo on modulo.id = tipo_funcion.id_modulo
                where u.username = '"  . $username . "' and modulo.habilitado=1"; 
          
         
         App::uses('Usuario', 'Model');
         $model = new Usuario();
         $res = $model->query($sql);
         $array_permisos = array();
         if(count($res > 0)){
             foreach($res as $permiso){
                 
                 array_push($array_permisos,$permiso["f"]["c_funcion"]);
                 
             }
             
         }
         unset($model);
         return $array_permisos;
        
    }
    
    
    function encryptdata($data){
        
        
        
        return $data;
        
        
        
    }
     
}
