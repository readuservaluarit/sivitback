/*SACADO DIRECTO DEL METODO PRODUCTIVO*/
SELECT producto.id,producto.codigo,stock,(SELECT s.stock  FROM stock_producto s WHERE s.id_deposito=6 AND s.id= stock_producto.id ) AS comprometido 
                                                    , orden_armado.fecha_entrega AS fecha_entrega_componentes_orden_armado, orden_armado.id AS nro_orden_armado
FROM stock_producto
JOIN producto ON producto.id=stock_producto.id
LEFT JOIN auditoria_producto ON auditoria_producto.id_producto= producto.id
LEFT JOIN orden_armado ON orden_armado.id = auditoria_producto.id_orden_armado

WHERE stock_producto.id_deposito=4 AND id_producto_tipo=2 AND stock< 0  AND id_categoria 
IN (
319,320,321,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,344,345,346,349,350,351,352,353,
354,355,356,357,358,360,372,373,374,375,378,379,380,381,382,383,384,385,386,389,390,394,395,396,399,400,402,403,404,405,
406,412,414,415,416,417,418,419,420,421,422,423,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,
445,446,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482)





SELECT codigo, d_producto, stock, 
	  (SELECT SUM(cantidad) FROM orden_produccion WHERE id_estado=1 AND id_componente=producto.id) AS cantidad_en_ordenes_prod_abiertas 
FROM stock_producto
JOIN producto ON producto.id = stock_producto.`id`
WHERE id_deposito = 4 AND id_producto_tipo = 2 AND stock<0
ORDER BY producto.codigo
DESC LIMIT 100000
