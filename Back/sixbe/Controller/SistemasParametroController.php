<?php

/**
* @secured(CONSULTA_DATOSEMPRESA)
*/
class SistemasParametroController extends AppController {
    public $name = 'SistemasParametro';
    public $model = 'SistemaParametro';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
        $id_sistema = strtolower($this->getFromRequestOrSession('SistemaParametro.id_sistema'));
        
       
      
        $conditions = array(); 
        if ($id_sistema!= "") {
           array_push($conditions,array('SistemaParametro.id_sistema' => $nombre ));
        }

       // array_push($conditions,array('EstadoTipoCheque.id_tipo_cheque' =>  $id_tipo_cheque));
  
      /*'joins' =>array (array(
                'table' => 'estado_tipo_cheque',
                'alias' => 'EstadoTipoCheque',
                'type' => 'LEFT',
                'conditions' => array(
                    'EstadoTipoCheque.id_tipo_cheque ='.$id_tipo_cheque
                )
                
            )),*/  
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            
            //'contain'=>array('EstadoTipoCheque'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de EstadosCheque', 'Datos de los EstadosCheque', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_ESTADO_CHEQUE', 'Nombre de EstadoCheque', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'EstadoCheque.id', "10%");
                $formBuilder->addHeader('Nombre', 'EstadoCheque.d_moneda', "50%");
             

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_ESTADO_CHEQUE');
        
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        	
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        
       
        foreach($data as &$sistema_parametro){
        	
        	$sistema_parametro['SistemaParametro']['d_cuenta_contable_cuenta_corriente'] = $sistema_parametro['CuentaContableCuentaCorriente']['d_codigo_cuenta_contable'];
        	$sistema_parametro['SistemaParametro']['d_cuenta_contable_iva'] = $sistema_parametro['CuentaContableIva']['d_codigo_cuenta_contable'];
        	$sistema_parametro['SistemaParametro']['d_cuenta_contable'] = $sistema_parametro['CuentaContablePrincipal']['d_codigo_cuenta_contable'];
        	$sistema_parametro['SistemaParametro']['d_cuenta_contable_cuenta_contado'] = $sistema_parametro['CuentaContableCuentaContado']['d_codigo_cuenta_contable'];
        	$sistema_parametro['SistemaParametro']['d_cuenta_contable_descuento_condicionado'] = $sistema_parametro['CuentaContableDescuentoCondicionado']['d_codigo_cuenta_contable'];
        	$sistema_parametro['SistemaParametro']['d_cuenta_contable_compensacion'] = $sistema_parametro['CuentaContableCompensacion']['d_codigo_cuenta_contable'];
        	
        	
        	unset($sistema_parametro['CuentaContableCuentaCorriente']);
        	unset($sistema_parametro['CuentaContableIva']);
        	unset($sistema_parametro['CuentaContablePrincipal']);
        	unset($sistema_parametro['CuentaContableCuentaContado']);
        	unset($sistema_parametro['CuentaContableDescuentoCondicionado']);
        	unset($sistema_parametro['CuentaContableCompensacion']);
        	unset($sistema_parametro['Sistema']);
        	unset($sistema_parametro['TipoComercializacion']);
        }
        
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->EstadoCheque->id = $id;
            $this->request->data = $this->EstadoCheque->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('EstadoCheque');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de EstadoCheque');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de EstadoCheque');
        else
            $formBuilder->setTituloForm('Consulta de EstadoCheque');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_ESTADO_CHEQUE', 'Nombre de EstadoCheque', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
     
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'EstadoChequeAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('EstadoChequeDEstadoCheque', 'Debe ingresar un nombre');
        


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    
    /**
    * @secured(MODIFICACION_DATOEMPRESA)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->SistemaParametro->id = $id;
            
            try{ 
            	if ($this->SistemaParametro->saveAll($this->request->data)){
                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "El Sistema Parametro ha sido modificado exitosamente",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('El Sistema Parametro ha sido modificado exitosamente.', 'success');
                        $this->redirect(array('controller' => 'Monedas', 'action' => 'index'));
                        
                    } 
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, el Sistema Parametro no ha podido modificarse.', 'error');
                
            }
            $this->redirect(array('action' => 'index'));
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }

   


}
?>