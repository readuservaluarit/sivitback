<?php

App::uses('ComprobanteItemComprobantesController', 'Controller');

class ReciboItemsController extends ComprobanteItemComprobantesController {
    
    public $name = EnumController::ReciboItems;
    public $model = 'ComprobanteItemComprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    

     /**
    * @secured(CONSULTA_RECIBO)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->loadModel("DatoEmpresa");
        $this->PaginatorModificado->settings = array('limit' => 30, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model); 
        
        $saldo = $this->getFromRequestOrSession('ComprobanteItemComprobante.saldos');
        $id_moneda = $this->getFromRequestOrSession('Comprobante.id_moneda');
        $id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda);
        
        $comprobante = New Comprobante();
        $field_valor_moneda = $comprobante->getFieldByTipoMoneda($id_tipo_moneda); 
        
       

        
         $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
  
            'contain'=>array('ComprobanteOrigen'=>array('PuntoVenta','TipoComprobante'),'Comprobante'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order'=>$this->{$this->model}->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_comprobante'))
        );
        
        
      if($this->RequestHandler->ext != 'json'){  
    
    
        //vista formBuilder
    } else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
               
                
                foreach($data as &$producto ){    
                         //roducto["ComprobanteItemComprobante"]["d_producto"] = $producto["Producto"]["d_producto"];
                	$producto["ComprobanteItemComprobante"]["precio_unitario"] = $producto["ComprobanteItemComprobante"]["precio_unitario"]*$producto["ComprobanteOrigen"]["TipoComprobante"]["signo_comercial"];
						 // $producto["ComprobanteItemComprobante"]["nro_comprobante_origen"] = $producto["ComprobanteOrigen"]["nro_comprobante"];
                         $producto["ComprobanteItemComprobante"]["nro_comprobante_origen"] = $producto["ComprobanteOrigen"]["nro_comprobante"];
						 
                         
                         if(isset($producto["ComprobanteOrigen"]["TipoComprobante"])){
                            $producto["ComprobanteItemComprobante"]["d_tipo_comprobante_origen"] = $producto["ComprobanteOrigen"]["TipoComprobante"]["codigo_tipo_comprobante"];
                            $producto["ComprobanteItemComprobante"]["signo_comercial"] = (string) $producto["ComprobanteOrigen"]["TipoComprobante"]["signo_comercial"];
							$producto["ComprobanteItemComprobante"]["simbolo_signo_comercial"] =  "+";
							 if($producto["ComprobanteOrigen"]["TipoComprobante"]["signo_comercial"] ==  -1)
								 $producto["ComprobanteItemComprobante"]["simbolo_signo_comercial"] = "-";
                         }else{
                            $producto["ComprobanteItemComprobante"]["d_tipo_comprobante_origen"] = '';
                         }   
                               
                         $producto["ComprobanteItemComprobante"]["fecha_generacion_origen"] = $producto["ComprobanteOrigen"]["fecha_generacion"];
                         
                         $producto["ComprobanteItemComprobante"]["fecha_contable_origen"] = $producto["ComprobanteOrigen"]["fecha_contable"];
                         
                         $producto["ComprobanteItemComprobante"]["fecha_vencimiento_origen"] = $producto["ComprobanteOrigen"]["fecha_vencimiento"];
                        
                         $producto["ComprobanteItemComprobante"]["enlazar_con_moneda"] = $producto["ComprobanteOrigen"]["enlazar_con_moneda"];
                         
                         
                         //$producto["ComprobanteItemComprobante"]["total_comprobante_origen"] = $producto["ComprobanteOrigen"]["total_comprobante"];
                         $producto["ComprobanteItemComprobante"]["total_comprobante_origen"] = (string) ($comprobante->ExpresarEnTipoMonedaMejorado($producto["ComprobanteOrigen"]["total_comprobante"],$field_valor_moneda, $producto["ComprobanteOrigen"])*$producto["ComprobanteOrigen"]["TipoComprobante"]["signo_comercial"]);
                         //$saldo = 1;
                         if($saldo!='')
                            $producto["ComprobanteItemComprobante"]["saldo"] = (string) $comprobante->getComprobantesImpagos($producto["ComprobanteOrigen"]["id_persona"],1,$producto["ComprobanteOrigen"]["id"],1,EnumSistema::VENTAS,0,$id_moneda);
                         else
                            $producto["ComprobanteItemComprobante"]["saldo"] = '';
                         
                         //$producto["ComprobanteItemComprobante"]["subtotal_comprobante_origen"] = $producto["ComprobanteOrigen"]["subtotal_comprobante"];
                         
                         
                         if(isset($producto["ComprobanteOrigen"]["PuntoVenta"]["numero"]))
                            $id_punto_venta = $producto["ComprobanteOrigen"]["PuntoVenta"]["numero"];
                         else
                            $id_punto_venta = 0;
                            
                            
                         $producto["ComprobanteItemComprobante"]["pto_nro_comprobante_origen"] = $this->ComprobanteItemComprobante->GetNumberComprobante($id_punto_venta,$producto["ComprobanteOrigen"]["nro_comprobante"]);

                         unset($producto["ComprobanteOrigen"]);
                         unset($producto["Comprobante"]);
                         
            }
        }
        
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    
    
     /**
    * @secured(CONSULTA_RECIBO)
    */
    public function getModel($vista='default')
    {    
        $model = parent::getModelCamposDefault();
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
    }
    
    
    private function editforView($model,$vista)
    {  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
      $this->set('model',$model);
      Configure::write('debug',0);
      $this->render($vista);
    }
}
?>