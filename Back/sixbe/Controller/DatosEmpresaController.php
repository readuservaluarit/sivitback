<?php


class DatosEmpresaController extends AppController {
    public $name = 'DatosEmpresa';
    public $model = 'DatoEmpresa';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
  
  
    /**
    * @secured(CONSULTA_DATOSEMPRESA)
    */
    public function index() {
        App::import('Model', 'Modulo');
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->loadModel("ComprobantePuntoVentaNumero");
        
        
        $this->loadModel("EjercicioContable");
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $cuit = $this->getFromRequestOrSession('DatoEmpresa.cuit');

       
        
        $conditions = array(); 
        
        
        $datoEmpresa = $this->Session->read('Empresa');
        
        
        $id_dato_empresa  = $datoEmpresa["DatoEmpresa"]["id"];
        
        
        if($id_dato_empresa != "")
            array_push($conditions,array("DatoEmpresa.id" =>$id_dato_empresa));
        
        if($cuit != "")
            array_push($conditions,array("DatoEmpresa.Cuit" =>$cuit));
        
            
            
            
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain'=>array('PuntoVenta'=>array('ComprobantePuntoVentaNumero'),'Pais','Provincia','TipoIva','TipoDocumento',
							  'Moneda','ListaPrecio','TipoEmpresa','DatoEmpresaImpuesto','Moneda2','Moneda3','CuentaContableResultadoEjercicio','CuentaContableChequeRechazado','CuentaContableChequeCartera','CuentaContableCaja','CuentaContableResultadoNoAsignado'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
        ////////////////////
        //Filters
        ///////////////////
        
        $formBuilder->setDataListado($this, 'Datos de su Empresa', 'Datos de su Empresa', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        $formBuilder->showNewButton(false);
        $formBuilder->showDeleteButton(false);
       
        
        //Headers
        $formBuilder->addHeader('Numero', 'DatoEmpresa.id', "5%");
        $formBuilder->addHeader('Raz&oacute;n Social', 'DatoEmpresa.razon_social', "30%");
        $formBuilder->addHeader('Cuit', 'DatoEmpresa.cuit', "20%");
        $formBuilder->addHeader('Email', 'DatoEmpresa.email', "25%");
        $formBuilder->addHeader('IIBB', 'DatoEmpresa.iibb', "20%");
        
      //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'iibb');
 
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
    else{ // vista json
    	
    	
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
       
        
 


        
        foreach($data as $key=>&$dato){
			
        	$this->DatoEmpresa->index($dato);
				
				
        }
        
        $comprobante_pto_venta_numero_aux = array();
        
    
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    /**
    * @secured(MODIFICACION_DATOSEMPRESA)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        $this->loadModel(EnumModel::Modulo);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        $id_moneda ='';
        $d_moneda ='';
        
        
        if ($mode != "A"){
            $this->DatoEmpresa->id = 1;
            $this->DatoEmpresa->contain("Provincia","TipoIva","PuntoVenta","Moneda","PuntoVentaMeli");
            $this->request->data = $this->DatoEmpresa->read();
              
            
            $id_moneda = $this->request->data['Moneda']['id'];
            $d_moneda = $this->request->data['Moneda']['d_moneda'];
            
            
            $mercadolibre_id_punto_venta_defecto_pedido = $this->request->data['PuntoVentaMeli']['id'];
            $numero_mercadolibre_id_punto_venta_defecto_pedido = $this->request->data['PuntoVentaMeli']['numero'];
        
        } 
        
        
        App::import('Lib', 'FormBuilderNew');
        $formBuilder = new FormBuilderNew();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Configuracion de Empresa');
        
        $formBuilder->showCancelFormButton(false);
        $formBuilder->showBackButton(false);
        
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de DatosEmpresa');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Configuraci&oacute;n de Modulos Web');
        else
            $formBuilder->setTituloForm('Consulta de DatoEmpresa');
        
            
        $modulo_sivit_clientes = $this->Modulo->estaHabilitado(EnumModulo::SIVITCLIENTES);
        $modulo_sivit_proveedores = $this->Modulo->estaHabilitado(EnumModulo::SIVITPROVEEDORES);
        $modulo_meli = $this->Modulo->estaHabilitado(EnumModulo::MELI);
            
        
       
       //$formBuilder->setFormTabDefault('tab-general'); 
     //  $formBuilder->addFormTab('tabs-general', 'Datos Generales');
       //$formBuilder->addFormTab('tabs-impositivo', 'Datos Impositivos');
       $formBuilder->addFormTab('tabs-meli', 'Mercadolibre');
       
       if($modulo_sivit_clientes == 1 || $modulo_sivit_proveedores == 1)
       		$formBuilder->addFormTab('tabs-sivit', 'Sivit Portales');
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        /*
        $formBuilder->addFormBeginTab('tabs-general');
        $formBuilder->addFormBeginFieldset('fs-general', 'General');
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('razon_social', 'Raz&oacute;n Social', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm required', 'label' => false));
        
        $formBuilder->addFormInput('cuit', 'CUIT (con guiones)', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm required required', 'label' => false)); 
        
        $formBuilder->addFormInput('email', 'Email', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm required required', 'label' => false));  
        
        
        $formBuilder->addFormInput('domicilio', 'Domicilio', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm required required', 'label' => false));  
        
        $formBuilder->addFormInput('localidad', 'Localidad', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm required required', 'label' => false));  
        
        $formBuilder->addFormInput('codigo_postal', 'Codigo Postal', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm required required', 'label' => false));  
        
        */
        
        //PROVINCIA
        
        $id_provincia='';
        $d_provincia='';
        if ($mode == "M") {
            $id_provincia=$this->request->data['Provincia']['id'];
            
            $prov = $this->DatoEmpresa->Provincia->find('first', array('conditions' => array('Provincia.id =' => $id_provincia)));
            $d_provincia = $prov['Provincia']['d_provincia'];
            
            
            $fecha_inicio_actividad = strtotime($this->request->data['DatoEmpresa']['fecha_inicio_actividad']);
                
                $this->request->data['DatoEmpresa']['fecha_inicio_actividad'] = date('d', $fecha_inicio_actividad).'/'.date('m', $fecha_inicio_actividad).'/'.date('Y', $fecha_inicio_actividad);
               
        }
        
        
        /*
        $selectionList = array($this->model."IdProvincia" => "id", $this->model."DProvincia" => "d_provincia");
        
        
        
        
        
        $formBuilder->addFormPicker('ProvinciaPicker', $selectionList, 'id_provincia', 
                                   array('value' => $id_provincia), 'd_provincia', 'Provincia', 
                                   array('class'=>'form-group row'), array('class' => 'form-control form-control-sm', 'label' => false, 'value' => $d_provincia)
                                   );  
        
        $formBuilder->addFormInput('tel', 'Tel&eacute;fono', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm required', 'label' => false));
        
        $formBuilder->addFormInput('iibb', 'Numero de ingresos Brutos', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm required', 'label' => false));
        

        
          $formBuilder->addFormInput('fecha_inicio_actividad', 'Fecha Inicio Actividad', array('class'=>'form-group row'), array('class' => 'datepicker','type'=>'text', 'label' => false));
        
        $formBuilder->addFormInput('website', 'Website', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm required', 'label' => false));
        
       
       
         
        
        $formBuilder->addFormEndRow(); 
        
        
        $formBuilder->addFormEndTab();
        */
        
        /*
        
        //TAB IMPOSITIVO
        $formBuilder->addFormBeginTab('tabs-impositivo');
        $formBuilder->addFormBeginFieldset('fs-impositivo', 'Datos Impositivos');
        */

        
        /*
        $id_tipo_iva='';
        $d_TipoIva='';
      
        if ($mode == "M") {
            $id_tipo_iva=$this->request->data['TipoIva']['id'];
            
            $prov = $this->DatoEmpresa->TipoIva->find('first', array('conditions' => array('TipoIva.id =' => $id_tipo_iva)));
            $d_TipoIva = $prov['TipoIva']['d_tipo_iva'];
          
        }
        
        
        
        $selectionList = array($this->model."IdTipoIva" => "id", $this->model."DTipoIva" => "d_tipo_iva");
        $formBuilder->addFormPicker('TipoIvaPicker', $selectionList, 'id_tipo_iva', 
                                   array('value' => $id_tipo_iva), 'd_tipo_iva', 'Condici&oacute;n de Iva', 
                                   array('class'=>'form-group row'), array('class' => 'form-control form-control-sm', 'label' => false, 'value' => $d_TipoIva)
                                   );  
        
        
         $formBuilder->addFormInput('nro_certificado_suss', 'Nro Cert SUSS', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm required', 'label' => false));
        
        $formBuilder->addFormInput('agente_percepcion_iva', 'Es agente de percepci&oacute;n IVA?', array('class'=>'form-group row'), array('class' => 'form-check-input', 'label' => false,'type' => 'checkbox'));
        
        $formBuilder->addFormInput('agente_percepcion_iibb', 'Es agente de percepci&oacute;n IIBB?', array('class'=>'form-group row'), array('class' => 'form-check-input', 'label' => false,'type' => 'checkbox'));
        
        $formBuilder->addFormInput('PuntoVenta.numero', 'Punto de Venta', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm required', 'label' => false));
        
        $formBuilder->addFormHidden('PuntoVenta.id');
        */
        
        
        /*
          //Moneda
        $selectionList = array($this->model."MonedaId" => "id", $this->model."DMoneda" => "d_moneda");
        $formBuilder->addFormPicker('MonedaPicker', $selectionList, 'id_cliente', 
                               array('value' => $id_moneda), 'd_moneda', 'Moneda con la que trabajara el sistema', 
                               array('class'=>'form-group row'), array('class' => 'form-control form-control-sm', 'label' => false, 'value' => $d_moneda));
                               
        $formBuilder->addFormHidden('DatoEmpresa.Moneda.id');
   
         $formBuilder->addFormInput('DatoEmpresa.file_upload', 'Logo de su Empresa', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm required', 'label' => false,'type'=>'file'));
        $formBuilder->addFormHtml('<div id="queue"></div>');
        
       
        $formBuilder->addFormHidden('DatoEmpresa.id');
    

        
        $formBuilder->addFormEndTab();
          */
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'DatoEmpresaAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('DatoEmpresaCuit', 'Debe ingresar un Cuit');
                validator.validateRequired('DatoEmpresaRazonSocial', 'Debe ingresar una Raz&oacute;n Social');
                validator.validateRequired('DatoEmpresaDProvincia', 'Debe ingresar una Provincia');
                validator.validateRequired('DatoEmpresaLocalidad', 'Debe ingresar una Localidad');
                validator.validateRequired('DatoEmpresaDomicilio', 'Debe ingresar un Domicilio');
                validator.validateRequired('DatoEmpresaEmail', 'Debe ingresar un Email');
                validator.validateRequired('PuntoVentaNumero', 'Debe ingresar un punto de Venta');
            
               
                //No va comentado pero sirve el codigo
                       /*
                       $.ajax({
                        'url': './".$this->name."/existe_codigo',
                        'data': 'codigo=' + $('#DatoEmpresaCodigo').val()+'&id='+$('#DatoEmpresaId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('DatoEmpresaCodigo','El C&oacute;digo que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                */
              
              
              
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 



    $('#DatoEmpresaMercadolibreConexionActiva').click(function(){

if(this.checked){
    window.open('".Router::url(array(
    		'controller' => EnumController::MercadoLibre,
    		'action' => 'index',
    		
    ))."', '_blank', 'width=300,height=400');

return true;
}
   


    });



            ";
         
    
    
    

        $formBuilder->addFormCustomScript($script);
        /*
        
        
        $formBuilder->setFormValidationFunction('validateForm()');
        
        */
        
        
        if($modulo_meli == 1){
	        $formBuilder->addFormBeginTab('tabs-meli');
	        $formBuilder->addFormBeginFieldset('fs-meli', '</br>');
	        
	        
	        $formBuilder->addFormHtml("<strong>Desde aqui podr&aacute; configurar la conexi&oacute;n de SIVIT con Mercadolibre</strong>");
	        $formBuilder->addFormInput('mercadolibre_conexion_activa', 'Habilitar conexi&oacute;n con MercadoLibre', array('class'=>'form-group row'), array('class' => 'form-check-input', 'label' => false,'type' => 'checkbox'));
	        
	        $formBuilder->addFormInput('mercadolibre_username', 'Usuario', array('class'=>'form-group row'), array('class' => 'form-control-plaintext', 'label' => false,array('type' => 'text','readonly')));
	        
	        
	        
	        $selectionList = array($this->model."MercadolibreIdPuntoVentaDefectoPedido" => "id", $this->model."NumeroMercadolibreIdPuntoVentaDefectoPedido" => "mercadolibre_id_punto_venta_defecto_pedido");
	        $formBuilder->addFormPicker('PuntoVentaPicker', $selectionList, 'MercadolibreIdPuntoVentaDefectoPedidos',
	        		array('value' => $mercadolibre_id_punto_venta_defecto_pedido), 'mercadolibre_id_punto_venta_defecto_pedido', 'Punto de Venta por defecto de Mercadolibre',
	        		array('class'=>'form-group row'), array('class' => 'form-control form-control-sm', 'label' => false, 'value' => $numero_mercadolibre_id_punto_venta_defecto_pedido));
	        
	        
	        
	        $formBuilder->addFormInput('mercadolibre_enviar_mensaje_post_venta', 'Enviar Mensaje Post Venta', array('class'=>'form-group row'), array('class' => 'form-check-input', 'label' => false,'type' => 'checkbox'));
	        
	        //$formBuilder->addFormInput('mercadolibre_enviar_mensaje_post_venta', 'Mensaje Post Venta', array('class'=>'form-group row'), array('class' => 'form-check-input', 'label' => false,'type' => 'text'));
	        
	        
	        $formBuilder->addFormInput('mercadolibre_mensaje_post_venta', 'Mensaje Post Venta', array('class'=>'form-group row'), array('class' => 'form-control form-control-sm required', 'label' => false,'type' => 'textarea'));
	        $formBuilder->addFormEndFieldset();
	        $formBuilder->addFormEndTab();
    	}
        
        if($modulo_sivit_clientes == 1 || $modulo_sivit_proveedores == 1){
        $formBuilder->addFormBeginTab('tabs-sivit');
        $formBuilder->addFormBeginFieldset('fs-sivit', 'General');
        
        if($modulo_sivit_clientes == 1)
        	$formBuilder->addFormInput('sivit_clientes_conexion_activa', 'Habilitar SIVIT Clientes', array('class'=>'form-group row'), array('class' => 'form-check-input', 'label' => false,'type' => 'checkbox'));
        
       	if($modulo_sivit_proveedores == 1)
        	$formBuilder->addFormInput('sivit_proveedores_conexion_activa', 'Habilitar SIVIT Proveedores', array('class'=>'form-group row'), array('class' => 'form-check-input', 'label' => false,'type' => 'checkbox'));
        
        	
        	
        $this->request->data["DatoEmpresa"]["url_sivit2"] = 'URL: <a target="_blank" href="'.$this->request->data["DatoEmpresa"]["url_sivit"].'">'.$this->request->data["DatoEmpresa"]["url_sivit"].'</a>';
        
        $formBuilder->addFormHtml($this->request->data["DatoEmpresa"]["url_sivit2"]);
        $formBuilder->addFormEndFieldset();
        $formBuilder->addFormEndTab();
        }
        
    
        
        $this->layout = "desktop_panel";
        /////Fin TAB General
        $this->set('abm',$formBuilder);
        $this->set('mode', "M");
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');
        
        
    }

    /**
    * @secured(ABM_USUARIOS)
    */
    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

     /**
    * @secured(ADD_DATOSEMPRESA)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            
            try{
                if ($this->DatoEmpresa->saveAll($this->request->data, array('deep' => true))){
                   
                    if(isset($this->request->params['form']['Filedata']['tmp_name'])) 
                        $this->uploadFile();
                    
                    $mensaje = "Los Datos de la Empresa han sido creados exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $mensaje = "Ha ocurrido un error,los Datos de la Empresa no han podido ser creados.";
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,los Datos de la Empresa no han podido ser creados.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => ""
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    
     /**
    * @secured(MODIFICACION_DATOSEMPRESA)
    */
    function edit($id,$Ws = 0) { //id de la empresa // si es dsd WS que se esta ejecutando
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->DatoEmpresa->id = $id;
            
            if($this->RequestHandler->ext != 'json')//solo si es web
                $this->request->data['DatoEmpresa']['fecha_inicio_actividad'] = preg_replace('#(\d{2})/(\d{2})/(\d{4})\s(.*)#', '$3-$2-$1', $this->request->data['DatoEmpresa']['fecha_inicio_actividad']);// en la bd aaaa-mm-dd y web se lo paso como dd/mm/aaaa
            
            
             if($Ws == 1)
              $this->request->data = $this->request->input('json_decode',true);
              
              //por default siempre es error
              $error = EnumError::ERROR;
              $message = 'Ha ocurrido un error, los Datos de la Empresa no han podido modificarse.';
              
              
              
             
              
            try{ 
              
            	if(isset($this->request->data["DatoEmpresa"]["id_moneda"]) && $this->ChequeoCotizacionBase($this->request->data["DatoEmpresa"]["id_moneda"]) != 0)
                	throw new Exception("La cotizaci&oacute;n de la Moneda base deber ser = 1. Debe correjirlo dirijiendose a la Pantalla de Monedas y cambiando la cotizaci&oacute;n.");
                
                
                    if ($this->DatoEmpresa->saveAll($this->request->data, array('deep' => true))){
                        
                        
                        
                        if(isset($this->request->params['form']['Filedata']['tmp_name']))
                            $this->uploadFile();
                            
                            
                        $datoEmpresa = $this->DatoEmpresa->find('first', array('conditions' => array('DatoEmpresa.id' => 1), 'contain' => array('TipoIva')));
                    
                   
                        $this->Session->write("Empresa", $datoEmpresa);   /*sobreescribo la session*/
                        
                        $error = EnumError::SUCCESS;
                        $message = 'Los Datos de la Empresa han sido modificados exitosamente';
                    
                       
                    }
                
               
            }catch(Exception $e){
                
                
                $error = EnumError::ERROR;
                $message = 'Ha ocurrido un error, los Datos de la Empresa no han podido modificarse.'.$e->getMessage();
                //$this->Session->setFlash('Ha ocurrido un error, los Datos de la Empresa no han podido modificarse.', 'error');
                
                
            }
           
        } 
        
         if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $error,
                            "message" => $message,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
         }else{
                        $this->Session->setFlash($message, $error);
                        $this->redirect(array('controller' => EnumController::DatosEmpresa, 'action' => 'abm/M/',$id));
                        
         } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
  
    /**
    * @secured(BAJA_DATOSEMPRESA)
    */
    function delete($id) {
        
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->DatoEmpresa->id = $id;
       try{
            if ($this->DatoEmpresa->delete()) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "Los Datos de la Empresa han sido eliminado exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }
    
    
     function uploadFile(){
        
        $dir_temp = new Folder(APP.'DatoEmpresaTemp');
        
        $random=$this->request->data['random'];
      
        if (!empty($_FILES)) {
            $tempFile =$this->request->params['form']['Filedata']['tmp_name'];
            
            $destino=$dir_temp->path .'/'.$random.'-'.$this->request->params['form']['Filedata']['name'];
            // Validate the file type
            $fileTypes = array('jpg','JPG','jpeg','JPEG','dwg','DWG','pdf','PDF','bmp','BMP','tif','TIF','tiff','TIFF','mcd','MCD'); // File extensions
            $fileParts = pathinfo($this->request->params['form']['Filedata']['name']);
            
            if (in_array( strtoupper( $fileParts['extension'] ) , $fileTypes)) {
                move_uploaded_file($tempFile,$destino);
                echo '1';
            } else {
                echo 'Invalid file type.';
            }
            
            
            
        }
        
      die();  
    }
    
      public function getModel($vista = 'default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER

            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }

        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
            $this->set('model',$model);
            // $this->set('this',$this);
            Configure::write('debug',0);
            $this->render($vista);
        }
    
    public function product_version(){
        
        return "1";
    }
    
    
    public function importa_logo($id_dato_empresa){
      
        
         App::uses('Folder', 'Utility');
         
         $dir_temp = new Folder(APP.'webroot/img');
         $this->autoRender = false;
         $nombre_archivo_importacion  = $id_dato_empresa;
        
        
        
        if($nombre_archivo_importacion != ''){
        if (!empty($_FILES)) {
                    $tempFile = $this->request->params['form']['file']['tmp_name'];
                    $size = $this->request->params['form']['file']['size'];
                    $size_mb = ($size/1024)/1024;
                    $tamanio_maximo_mb = 1;
                    
                    $destino = $dir_temp->path .'//'.$id_dato_empresa.'.png';
                    
                    if(move_uploaded_file($tempFile,$destino)){
                    
                
                  
                      
                              
                              
                              if(file_exists($destino)){
                       
                             
                     
                                    $error = EnumError::SUCCESS;
                                    $message = 'El archivo fue importado exitosamente';  
                                      
                                
                                  
                              }else{
                                  
                                 $error = EnumError::ERROR;
                                 $message = 'El archivo NO puede ser abierto'; 
                              }
                            
                            } else {
                                
                                 $error = EnumError::ERROR;
                                 $message = 'El archivo NO pudo ser subido';
                             
                            }
                
                }else{
                    
                     $error = EnumError::ERROR;
                     $message = 'El archivo no subio correctamente cheque permisos en el servidor';
                    
                }
        }else{
            
            $error = EnumError::ERROR;
            $message = 'El archivo no subio correctamente cheque la configuracion del servidor';
            
            
        }
        
       
         $this->autoRender = false;
        $output = array(
            "status" => $error,
            "message" => $message,
            "content" => "CONTENT"
        );
        //$this->set($output);
        //$this->set("_serialize", array("status", "message","page_count", "content"));
        echo json_encode($output);
        die();
        
        
    }
    
    
    
    /**
     * @secured(MODIFICACION_DATOSEMPRESA)
     */
    public function DatoEmpresaPanel(){
    	
    	
    	
    	
    }
    
     
    


  
  
   
    
}
?>