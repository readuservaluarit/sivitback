<?php

 include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');
 
 
$model["id_comprobante"]["show_grid"] = 1;
$model["id_comprobante"]["header_display"] = "Id. Comprob.";
$model["id_comprobante"]["order"] = "0";
$model["id_comprobante"]["width"] = "8";

$model["pto_nro_comprobante"]["show_grid"] = 1;
$model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
$model["pto_nro_comprobante"]["order"] = "1";
$model["pto_nro_comprobante"]["width"] = "25";

$model["n_item"]["show_grid"] = 1;
$model["n_item"]["header_display"] = "Nro Item";
$model["n_item"]["order"] = "2";
$model["n_item"]["width"] = "5";

$model["d_estado_comprobante"]["show_grid"] = 1;
$model["d_estado_comprobante"]["header_display"] = "Estado";
$model["d_estado_comprobante"]["order"] = "3";
$model["d_estado_comprobante"]["width"] = "13";
$model["d_estado_comprobante"]["text_colour"] = "#770011";

$model["n_item"]["show_grid"] = 1;
$model["n_item"]["header_display"] = "N? Item";
$model["n_item"]["order"] = "4";
$model["n_item"]["width"] = "4";

$model["codigo"]["show_grid"] = 1;
$model["codigo"]["header_display"] = "[C&oacute;digo]";
$model["codigo"]["order"] = "5";            
$model["codigo"]["width"] = "20";

$model["item_observacion"]["show_grid"] = 1;
$model["item_observacion"]["header_display"] = "Item Observ.";
$model["item_observacion"]["order"] = "6";            
$model["item_observacion"]["width"] = "20";

$model["cantidad"]["show_grid"] = 1;
$model["cantidad"]["header_display"] = "Cant. Pedido";
$model["cantidad"]["order"] = "7";            
$model["cantidad"]["width"] = "5";
$model["cantidad"]["min_value"] = "0"; 
$model["cantidad"]["text_colour"] = "#0000FF";//BLUE


$model["cantidad_pendiente_a_facturar"]["show_grid"] = 1;
$model["cantidad_pendiente_a_facturar"]["header_display"] = "Cant. Pend FC.";
$model["cantidad_pendiente_a_facturar"]["order"] = "8";            
$model["cantidad_pendiente_a_facturar"]["width"] = "5";
$model["cantidad_pendiente_a_facturar"]["text_colour"] = "#0000FF";//BLUE


$model["total_facturado"]["show_grid"] = 1;
$model["total_facturado"]["header_display"] = "Cant. FC";
$model["total_facturado"]["order"] = "9";
$model["total_facturado"]["width"] = "5";
$model["total_facturado"]["text_colour"] = "#008822";//GREEN

$model["total_remitido"]["show_grid"] = 1;
$model["total_remitido"]["header_display"] = "Cant. RM";
$model["total_remitido"]["order"] = "10";
$model["total_remitido"]["width"] = "5";
$model["total_remitido"]["text_colour"] = "#660099";//RED VIOLET



$model["moneda_simbolo"]["show_grid"] = 1;
$model["moneda_simbolo"]["header_display"] = "Moneda";
$model["moneda_simbolo"]["order"] = "11";          
$model["moneda_simbolo"]["width"] = "5";
$model["moneda_simbolo"]["text_colour"] = "#004400";


$model["precio_unitario"]["show_grid"] = 1;
$model["precio_unitario"]["header_display"] = "Precio Unitario";
$model["precio_unitario"]["order"] = "12";           
$model["precio_unitario"]["width"] = "11";
$model["precio_unitario"]["min_value"] = "0"; 
$model["precio_unitario"]["text_colour"] = "#F0220F";

$model["descuento_unitario"]["show_grid"] = 1;
$model["descuento_unitario"]["header_display"] = "Desc. Unitario %";
$model["descuento_unitario"]["order"] = "13";
$model["descuento_unitario"]["min_value"] = "0"; 
$model["descuento_unitario"]["max_value"] = "100"; 
$model["descuento_unitario"]["width"] = "5"; 

$model["d_moneda"]["show_grid"] = 1;
$model["d_moneda"]["header_display"] = "Moneda";
$model["d_moneda"]["order"] = "14";
$model["d_moneda"]["width"] = "5"; 
$model["d_moneda"]["text_colour"] = "#004400";

$model["total"]["show_grid"] = 1;
$model["total"]["header_display"] = "Total";
$model["total"]["order"] = "15";
$model["total"]["width"] = "12";
$model["total"]["text_colour"] = "#F0220F";

$output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "ComprobanteItem",  /*Importante: Nombre del Model*/
				"content" => $model
			);
        
          echo json_encode($output);
?>