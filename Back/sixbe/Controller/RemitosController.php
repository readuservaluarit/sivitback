<?php
App::uses('ComprobantesController', 'Controller');


  /**
    * @secured(CONSULTA_REMITO)
  */
class RemitosController extends ComprobantesController {
    
    public $name = EnumController::Remitos;
    public $model = 'Comprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $requiere_impuestos = 0;
    
    /**
    * @secured(CONSULTA_REMITO)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros($this->model);
            
            
                       
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>array('Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante','DepositoOrigen','DepositoDestino','Transportista','PuntoVenta'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.id desc'
        ); //el contacto es la sucursal
        
      
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
            
             //$valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
             //unset($valor['ComprobanteItem']);
             
             if(isset($valor['TipoComprobante'])){
                 $valor[$this->model]['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                 $valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
                 unset($valor['TipoComprobante']);
             }
             
             
             
          
           
             if(isset($valor['Moneda'])){
                 $valor[$this->model]['d_moneda'] = $valor['Moneda']['d_moneda'];
                 unset($valor['Moneda']);
             }
             
             if(isset($valor['EstadoComprobante'])){
                $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
                unset($valor['EstadoComprobante']);
             }
             if(isset($valor['CondicionPago'])){
                $valor[$this->model]['d_condicion_pago'] = $valor['CondicionPago']['d_condicion_pago'];
                unset($valor['CondicionPago']);
             }
             
             if(isset($valor['DepositoOrigen'])){
                $valor[$this->model]['id_deposito_origen'] = $valor['DepositoOrigen']['id'];
                $valor[$this->model]['d_deposito_origen'] = $valor['DepositoOrigen']['d_deposito'];
                unset($valor['DepositoOrigen']);
             }
             
              if(isset($valor['DepositoDestino'])){
                $valor[$this->model]['id_deposito_destino'] = $valor['DepositoDestino']['id'];
                $valor[$this->model]['d_deposito_destino'] = $valor['DepositoDestino']['d_deposito'];
                unset($valor['DepositoDestino']);
             }
             if(isset($valor['Transportista'])){
                $valor[$this->model]['t_razon_social'] = $valor['Transportista']['razon_social'];
                $valor[$this->model]['t_direccion'] = $valor['Transportista']['calle'].' '.$valor['Transportista']['numero_calle'];
                unset($valor['Transportista']);
             }
             
             if(isset($valor['Persona'])){
            
            
                 if($valor['Persona']['id']== null){
                     
                    $valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social']; 
                 }else{
                     
                     $valor[$this->model]['razon_social'] = $valor['Persona']['razon_social'];    
                     $valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
                 }
                 
                 unset($valor['Persona']);
             }
             
                if(isset($valor['DepositoOrigen'])){
                  $valor['Comprobante']['d_deposito_origen'] =$valor['DepositoOrigen']['d_deposito_origen'];
              }
                 
              if(isset($valor['DepositoDestino'])){
                    $valor['Comprobante']['d_deposito_destino'] =$valor['DepositoDestino']['d_deposito_destino'];
               }
               
               
                 if(isset($valor['PuntoVenta'])){
                    $valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
                    unset($valor['PuntoVenta']);
             }
            $this->{$this->model}->formatearFechas($valor);
      
        }
        
        
        $this->data = $data;
        
        
        $seteo_form_builder = array("showActionColumn" =>true,"showNewButton" =>false,"showDeleteButton"=>false,"showEditButton"=>false,"showPrintButton"=>true,"showFooter"=>true,"showHeaderListado"=>true,"showXlsButton"=>false);
        
        $this->title_form = "Listado de Remitos";
        $this->preparaHtmLFormBuilder($this,$seteo_form_builder,"");
        
        
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     
        
        
        

        
    }
    
    /**
    * @secured(ABM_USUARIOS)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->REMITO->id = $id;
            $this->REMITO->contain();
            $this->request->data = $this->REMITO->read();
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('REMITO');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de REMITOes');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de REMITOes');
        else
            $formBuilder->setTituloForm('Consulta de REMITOes');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('cuit', 'Cuit', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('calle', 'Calle', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  
        $formBuilder->addFormInput('tel', 'Tel&eacute;fono', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('ciudad', 'Ciudad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descuento', 'Descuento', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descripcion', 'Descripcion', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('localidad', 'Localidad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
         
        
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'ClienteAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('ClienteRazonSocial', 'Debe ingresar una Raz&oacute;n Social');
                validator.validateRequired('ClienteCuit', 'Debe ingresar un CUIT');
                validator.validateNumeric('ClienteDescuento', 'El Descuento ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_cuit',
                        'data': 'cuit=' + $('#ClienteCuit').val()+'&id='+$('#ClienteId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('ClienteCuit','El Cuit que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

    /**
    * @secured(ADD_REMITO)
    */
    public function add(){
 
     parent::add();    
        
    }
    
    
    /**
    * @secured(MODIFICACION_REMITO)
    */
    public function edit($id){ 
     parent::edit($id); 
     return;   
        
    }
    
    
    /**
    * @secured(BAJA_REMITO)
    */
    public function delete($id){
        
     parent::delete($id);    
        
    }
    
   /**
    * @secured(BAJA_REMITO)
    */
    public function deleteItem($id,$externo=1){
        
        parent::deleteItem($id,$externo);    
        
    }
    
    
      /**
    * @secured(CONSULTA_REMITO)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
  
    
    
    
    /**
    * @secured(REPORTE_REMITO)
    */
    public function pdfExport($id,$vista=''){
        
        
        
        
        
    $datoEmpresa = $this->Session->read('Empresa');
    $chequea_remito_imprimir_solo_con_remitido  = $datoEmpresa["DatoEmpresa"]["chequea_remito_imprimir_solo_con_remitido"];//este flag si esta en 1 debe chequear que si el estado del Remito es Remitido entonces ahi deja imprimir    
        
        
        
        
        
    $this->loadModel("Comprobante");
    $remito = $this->Comprobante->find('first', array(
                                                        'conditions' => array('Comprobante.id' => $id),
                                                        'contain' =>array('TipoComprobante')
                                                        ));
                                                        
                                                        
     $imprimir  = 1;
     
     
     if( $chequea_remito_imprimir_solo_con_remitido == 1  && $remito && $remito["Comprobante"]["id_estado_comprobante"] != EnumEstadoComprobante::Remitido  ){
         
      $imprimir = 0;   
         
         
     }
     
     
    
     
        if($imprimir == 0){   //Debo devolver un Json porque no deberia imprimir
        
        
          $output = array(
            "status" =>EnumError::ERROR,
            "message" => "El Remito no puede imprimirse. Primero debe realizar el movimiento de Despacho de mercader&iacute;a.",
            "content" => "",
        );
        
        
        $this->autoRender = false;
        $this->response->type('json');
        echo json_encode($output);
       
        
        
        
        }else{                                                
                                                        
        
        
        
        if($remito){
            $comprobantes_relacionados = $this->getComprobantesRelacionados($id);
         
                                           
          
            
            
              
            $facturas_relacionados = array();
            $pedidos_internos_relacionados = array();


            foreach($comprobantes_relacionados as $comprobante){


             if(  isset($comprobante["Comprobante"]["id_tipo_comprobante"]) && in_array($comprobante["Comprobante"]["id_tipo_comprobante"],   $this->Comprobante->getTiposComprobanteController(EnumController::Facturas) ))
                array_push($facturas_relacionados,$this->Comprobante->GetNumberComprobante($comprobante["PuntoVenta"]["numero"],$comprobante["Comprobante"]["nro_comprobante"]));
            
            if(isset($comprobante["Comprobante"]["id_tipo_comprobante"]) && $comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::PedidoInterno)
                        array_push($pedidos_internos_relacionados,$this->Comprobante->GetNumberComprobante($comprobante["PuntoVenta"]["numero"],$comprobante["Comprobante"]["nro_comprobante"]));


            }
           
            
            
            
            $this->set('facturas_relacionados',$facturas_relacionados);  
            $this->set('pedidos_internos_relacionados',$pedidos_internos_relacionados);     
            parent::pdfExport($id);  
         }
            
       }
     
        
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
        
        
      
 
    }
    
    
    /**
    * @secured(MODIFICACION_REMITO)
    */
    public function Anular($id_comprobante){
     
     $this->loadModel("Comprobante");
     $this->loadModel("Asiento");
     $this->loadModel("Cheque");
     $this->loadModel("Modulo");
        
        
          $oc = $this->Comprobante->find('first', array(
                    'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                    'contain'=>false
                ));
                
          $ds = $this->Comprobante->getdatasource(); 
      
          
          
          
                   
            if( $oc && $oc["Comprobante"]["id_estado_comprobante"]!= EnumEstadoComprobante::Anulado ){
                  
                try{
                    $ds->begin();
                        
                        $this->Comprobante->updateAll(
                                                        array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
                                                        array('Comprobante.id' => $id_comprobante) );  
                                                        
                        
                        
                         $habilitado  = $this->Modulo->estaHabilitado(EnumModulo::STOCK);
                                                         
                        
                        if($habilitado == 1){ //TODO: Si tiene mov stock anular
                        	
                        	
                        	$id_tipo_movimiento_defecto = $this->Comprobante->getTipoMovimientoDefecto($id_comprobante);
                        	$id_movimiento = $this->Comprobante->HizoMovimientoStock($id_comprobante,$id_tipo_movimiento_defecto);                 
                            
                                        
                            if( $id_movimiento > 0 ){//si hizo movimiento
                                $this->loadModel("Movimiento");
                                $this->Movimiento->Anular($id_movimiento);
                            }
                        }                                
                       
                        
                    $remito = $this->Comprobante->find('first', array(
                        		'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                        		'contain' => false
                        ));
                        
                    $this->Auditar($id_comprobante,$remito);
                                                        
                    $ds->commit(); 
                    $tipo = EnumError::SUCCESS;
                    $mensaje = "Se Anulo correctamente el comprobante";
                       
               }catch(Exception $e){ 
                
                $ds->rollback();
                $tipo = EnumError::ERROR;
                $mensaje = "No es posible anular el Comprobante ERROR:".$e->getMessage();
                
                
              }  
            
            }else{
                
               $tipo = EnumError::ERROR;
               $mensaje = "No es posible anular el Comprobante ya se encuenta ANULADO";
            }
            
              $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                );      
              echo json_encode($output);
              die(); 
    }
    
    
        /**
        * @secured(CONSULTA_REMITO)
        */
        public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
        {
            parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
        }
        
        
        
        /**
         * @secured(BTN_EXCEL_REMITOS)
         */
        public function excelExport($vista="default",$metodo="index",$titulo=""){
        	
        	parent::excelExport($vista,$metodo,"Listado de Remitos de venta");
        	
        	
        	
        }
         
    
}
?>