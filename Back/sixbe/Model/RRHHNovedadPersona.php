<?php
App::uses('AppModel', 'Model');
App::uses('RRHHConcepto', 'Model');

class RRHHNovedadPersona extends AppModel {
	
/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'rrhh_novedad_persona';

    public $actsAs = array('Containable');
    
    public  $id_liquidacion;
    public  $id_persona;

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
       
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
        'RRHHLiquidacion' => array(
            'className' => 'RRHHLiquidacion',
            'foreignKey' => 'id_rrhh_liquidacion',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Legajo' => array(
            'className' => 'Legajo',
            'foreignKey' => 'id_persona',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
	'RRHHConcepto' => array(
					'className' => 'RRHHConcepto',
					'foreignKey' => 'id_rrhh_concepto',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
   );
   
   
	
	public function generarNovedadPeriodo($id_rrhh_liquidacion){
		/*inserta por cada persona perteneciente al periodo los registros que coinciden */
		/*Selecciono conceptos de empleados que pertenecen a esta liquidacion que coincida con el tipo de liquidacion*/
		
		
		
	}
	
	
	public function getValorConcepto($codigo_concepto,$tipo_campo_concepto){
		
		
		$novedad_persona = $this->find('first', array(
				'conditions' => array('RRHHNovedadPersona.id_rrhh_liquidacion' =>$this->id_liquidacion,'RRHHConcepto.codigo'=>$codigo_concepto,'RRHHNovedadPersona.id_persona'=>$this->id_persona),
				'contain' =>array("RRHHConcepto")
		));//sino lo encuentro significa que esta mal seteada la agenda de la persona, si se usa en la liquidacion, tiene que estar cargado en la parte de Novedades, manualmente al menos.
		
		
		if($novedad_persona){
		
		$rrhh_concepto_obj = new RRHHConcepto();
		
		$campo = $rrhh_concepto_obj->getTipoCampoConcepto($tipo_campo_concepto);
		
		if($tipo_campo_concepto != EnumRRHHTipoCampoConcepto::ValorGenerico)
			return $novedad_persona["RRHHNovedadPersona"][$campo];
		else 
			return $novedad_persona["RRHHConcepto"][$campo];
		
		}else{
			
			return 0;//TODO: ver que sucede sino esta en agenda y no lo carga
		}
		
		
	}
	
	
	
	public function actualizaNovedad($concepto){
		
		$fecha_modificacion = date("Y-m-d");
	
		$this->updateAll(
				array('RRHHNovedadPersona.formula' => "'". $concepto["RRHHNovedadPersona"]["formula"]."'",'RRHHNovedadPersona.importe_calculado' => $concepto["RRHHNovedadPersona"]["importe_calculado"],'RRHHNovedadPersona.fecha_modificacion'=>  "'".$fecha_modificacion."'"),
				array('RRHHNovedadPersona.id' => $concepto["RRHHNovedadPersona"]["id"]) );   
		
		 
		
		
		
		
	}
	
   
   

}
