<?php

App::uses('WordpressAppModel', 'Wordpress.Model');

class WordpressPostmetum extends AppModel {
    
    public $useDbConfig = 'wordpress_database';
    public $name = 'WordpressPostmetum';
    public $primaryKey = 'meta_id';
    public $useTable = 'six_wp_postmeta';

    //The Associations below have been created with all possible keys, those that are not needed can be removed
    public $belongsTo = array(
        'Post' => array(
            'className' => 'WordpressPost',
            'foreignKey' => 'post_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
?>