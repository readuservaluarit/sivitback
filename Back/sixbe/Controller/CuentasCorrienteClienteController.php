<?php


App::uses('CuentasCorrienteController', 'Controller');

class CuentasCorrienteClienteController extends CuentasCorrienteController {
    
    
    public  $id_tipo_persona = EnumTipoPersona::Cliente;
    public  $id_sistema = EnumSistema::VENTAS;
	public $name = 'CuentasCorrienteCliente';

    
    /**
    * @secured(CONSULTA_CUENTA_CORRIENTE_CLIENTE)
    */
  public function index($id_tipo_persona=''){
      
     $id_tipo_persona = $this->id_tipo_persona;
     
     parent::index($id_tipo_persona);
  } 
  
    
    /**
    * @secured(CONSULTA_CUENTA_CORRIENTE_CLIENTE)
    */
   public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
      
    }
    
    public function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
        
        
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);          
    }
    
    /**
    * @secured(CONSULTA_CUENTA_CORRIENTE_CLIENTE)
    */ 
    public function ReporteCuentaCorriente(){
    
        parent::ReporteCuentaCorriente(); 
        
        
    }
    
    
    /**
     * @secured(BTN_EXCEL_CUENTASCORRIENTECLIENTE)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	
    	parent::excelExport($vista,$metodo,$this->getTituloReporte());
    	return;
    	
    	
    	
    }
    
    
    
    
}
?>