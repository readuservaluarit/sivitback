SELECT 
  persona.codigo,
  persona.`id_migracion`,
  persona.`cuit`,
  razon_social,
  fecha_creacion,
  pais.d_pais,
  provincia.`d_provincia`,
  REPLACE((SELECT ROUND(SUM(total_comprobante/valor_moneda),0) FROM comprobante WHERE id_persona = persona.id AND id_tipo_comprobante=202 AND YEAR(fecha_generacion)=2019 AND id_estado_comprobante<>2),".",",") AS total_ventas,
  persona_categoria.`d_persona_categoria`
  
FROM
  persona 
  JOIN pais ON pais.id = persona.`id_pais`
  JOIN provincia ON provincia.id = persona.`id_provincia`
  JOIN persona_categoria ON persona_categoria.id = persona.`id_persona_categoria`
WHERE YEAR(fecha_creacion) = 2019
  AND persona.id_tipo_persona = 1  
ORDER BY persona.id DESC 