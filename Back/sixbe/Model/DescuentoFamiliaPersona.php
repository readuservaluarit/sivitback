<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class DescuentoFamiliaPersona extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'descuento_familia_persona';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
       
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
        'Persona' => array(
            'className' => 'Persona',
            'foreignKey' => 'id_persona',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'FamiliaProducto' => array(
            'className' => 'FamiliaProducto',
            'foreignKey' => 'id_familia_producto',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
   );     

}
