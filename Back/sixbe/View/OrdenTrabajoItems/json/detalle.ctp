<?php 
         include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');
         
		$model["n_item"]["show_grid"] = 1;
        $model["n_item"]["header_display"] = "Nro.";
        $model["n_item"]["order"] = "0";
		$model["n_item"]["read_only"] = "0";
        
		$model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "Operacion Interna";
        $model["d_producto"]["order"] = "4";
		$model["d_producto"]["read_only"] = "1";
		$model["d_producto"]["picker"] = EnumMenu::mnuArticulosGeneral;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
	
		$model["fecha_generacion"]["show_grid"] = 1;
		$model["fecha_generacion"]["type"] = "datetime";
		$model["fecha_generacion"]["mask"] = "00/00/0000 00:00"; //esto convierte la grilla 
        $model["fecha_generacion"]["header_display"] = "F. y Hora de Inicio";
        $model["fecha_generacion"]["order"] = "6";
		$model["fecha_generacion"]["read_only"] = "0";
		$model["fecha_generacion"]["with_px"] = "110";//AGREGAR un modo q agregue esto en pixeles
		$model["fecha_generacion"]["text_colour"] = "#000000";
		
		$model["fecha_cierre"]["show_grid"] = 1;
		$model["fecha_cierre"]["type"] = "datetime";
		$model["fecha_cierre"]["mask"] = "00/00/0000 00:00"; //esto convierte la grilla 
		$model["fecha_cierre"]["header_display"] = "F. y Hora Finalizacion";
        $model["fecha_cierre"]["order"] = "7";
		$model["fecha_cierre"]["read_only"] = "0";
		$model["fecha_cierre"]["text_colour"] = "#000000";
		$model["fecha_cierre"]["with_px"] = "110";//AGREGAR un modo q agregue esto en pixeles esto ahora esta harcodeade en el tipo de maskara "00/00/0000 00:00"
		
		$model["cantidad_cierre"]["show_grid"] = 1;
        $model["cantidad_cierre"]["header_display"] = "CNF";
        $model["cantidad_cierre"]["order"] = "8";
		$model["cantidad_cierre"]["read_only"] = "0";
        $model["cantidad_cierre"]["min_value"] = "0"; 
		$model["cantidad_cierre"]["text_colour"] = "#008800"; //GREEN
		
		
		$model["item_observacion"]["show_grid"] = 1;
		$model["item_observacion"]["type"] = "string";
        $model["item_observacion"]["header_display"] = "Observacion";
        $model["item_observacion"]["order"] = "9";
		$model["item_observacion"]["read_only"] = "0";
		$model["item_observacion"]["text_colour"] = "#000000";
	
		
		$model["d_persona"]["show_grid"] = 1;
        $model["d_persona"]["header_display"] = "Ejecucion";
        $model["d_persona"]["order"] = "10";
        // $model["d_persona"]["width"] = "8";
		$model["d_persona"]["read_only"] = "1";
		$model["d_persona"]["text_colour"] = "#C0000C";
		$model["d_persona"]["picker"] = EnumMenu::mnuEmpleadoGeneral;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
		
		
		$model["cantidad"]["show_grid"] = 1;
        $model["cantidad"]["header_display"] = "NC";
        $model["cantidad"]["order"] = "11";
        // $model["cantidad"]["width"] = "4";
		$model["cantidad"]["read_only"] = "0";
        $model["cantidad"]["min_value"] = "0"; 
		$model["cantidad"]["text_colour"] = "#CC00CC"; //VIOLETA
		
		$model["cantidad2"]["show_grid"] = 1;
        $model["cantidad2"]["header_display"] = "RP";
        $model["cantidad2"]["order"] = "12";
        // $model["cantidad2"]["width"] = "4";
		$model["cantidad2"]["read_only"] = "0";
        $model["cantidad2"]["min_value"] = "0"; 
		$model["cantidad2"]["text_colour"] = "#0000CC"; //BLUE
		
		$model["cantidad3"]["show_grid"] = 1;
        $model["cantidad3"]["header_display"] = "SCRP";
        $model["cantidad3"]["order"] = "13";
        // $model["cantidad3"]["width"] = "4";
		$model["cantidad3"]["read_only"] = "0";
        $model["cantidad3"]["min_value"] = "0"; 
		$model["cantidad3"]["text_colour"] = "#CC0000"; //VIOL
		
		
		$model["orden"]["show_grid"] = 0; //INVISIBLE PARA NO PERDER EL ORDEN DE LOS Q NO TIENE IDS
         $model["orden"]["header_display"] = "Orden";
         $model["orden"]["order"] = "15";
		 // $model["orden"]["width"] = "3";
		 $model["orden"]["read_only"] = "0";
		
		
 $output = array(
			"status" =>EnumError::SUCCESS,
			"message" => "ComprobanteItem",
			"content" => $model
                );
        echo json_encode($output);
         
        

?>