<?php
class UsuarioPicker extends EntityPicker{
    
    public $model = "Usuario";
    
    public $pickerTitle = "Selección de Usuario";
    
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Usuario", "field" => "username", "width" => "30%", "keyOrder" => "username"),
                                  array("title" => "Nombre", "field" => "nombre", "width" => "60%", "keyOrder" => "nombre")
                                );

    public function getPickerConditions($busqueda){
        
        return array('Usuario.username LIKE' => '%' . $busqueda['bus_unico'] . '%');
    }

}