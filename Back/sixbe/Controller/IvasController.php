<?php


class IvasController extends AppController {
    public $name = 'Ivas';
    public $model = 'Iva';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
/**
* @secured(CONSULTA_IVA)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
        $nombre = strtolower($this->getFromRequestOrSession('Iva.d_iva'));
        
        $conditions = array(); 
        if ($nombre != "") {
            $conditions = array('LOWER(Iva.d_iva) LIKE' => '%' . $nombre . '%');
        }

        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de Ivas', 'Datos de los Ivas', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_iva', 'Nombre de Iva', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'Iva.id', "10%");
                $formBuilder->addHeader('Nombre', 'Iva.d_moneda', "50%");
             

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_iva');
        
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->Iva->id = $id;
            $this->request->data = $this->Iva->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Iva');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Iva');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Iva');
        else
            $formBuilder->setTituloForm('Consulta de Iva');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_iva', 'Nombre de Iva', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
     
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'IvaAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('IvaDIva', 'Debe ingresar un nombre');
        


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ABM_IVA,READONLY_PROTECTED)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            
            try{
                if ($this->Iva->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "El Iva ha sido creado exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el Iva no ha podido ser creado.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => ""
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }

     /**
    * @secured(MODIFICACION_IVA,READONLY_PROTECTED)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->Iva->id = $id;
            
            try{ 
                if ($this->Iva->saveAll($this->request->data)){
                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "El Iva ha sido modificado exitosamente",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('El Iva ha sido modificado exitosamente.', 'success');
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, el Iva no ha podido modificarse.', 'error');
                
            }
            $this->redirect(array('action' => 'index'));
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }

    /**
    * @secured(BAJA_IVA,READONLY_PROTECTED)
    */
    function delete($id) {
        
        $this->loadModel($this->model);

        try{

            if ($this->Iva->delete($id))
                 $this->Session->setFlash('El Iva ha sido eliminado exitosamente.', 'success');
            else
                throw new Exception();

        }catch(ChildRecordException $ex){ 
            $this->Session->setFlash($ex->getMessage(), 'error');
        }
        
        
        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
    }
	

	
	/**
    * @secured(CONSULTA_IVA)
    */
  public function getModel($vista="default"){
        
  	
  	$model = parent::getModelCamposDefault();//esta en APPCONTROLLER
  	$model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
  	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

    }
	
	
	 private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
	 	$this->set('model',$model);
	 	Configure::write('debug',0);
	 	$this->render($vista);
	 	
    }



}
?>