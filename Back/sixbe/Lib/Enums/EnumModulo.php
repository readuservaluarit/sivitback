<?php

class EnumModulo
{

	const  STOCK 			= 100;
	const  VENTAS 			= 200;
	const  B2C 				= 201;
	const  B2B 				= 202;
	const  SIVITCLIENTES 	= 203;
	const  MELI			 	= 204;
	const  COMPRAS 			= 300;
	const  SIVITPROVEEDORES = 303;
	const  CAJA   			= 400;
	const  GASTOS   		= 401;
	const  IVA   			= 500;
	const  CONTABILIDAD 	= 600;
	const  CENTRO_COSTO 	= 601;
	const  RRHH      		= 700;
	const  IMPORTACIONES	= 800;
	const  CASHFLOW    		= 900;
	const  ACTIVOFIJO   	= 1000;
    const  TESORERIA   		= 1100;
	const  CHEQUES   		= 1101;
	const  DATOEMPRESA   	= 1200;
	const  VALUARITWEB   	= 1300;
	const  TABLERODECONTROL = 1400;
	const  EXPORTACIONES   	= 1500;
    const  PROVEEDORES    	= 1600;
	const  PRODUCCION    	= 1700;  
	const  CALIDAD    		= 1800;  
	const  AUDITORIA    	= 2000;  
	const  PARAMETRICAS     = 2100;  
	const  LISTAPRECIO    	= 2200;  
	const  LOGISTICA    	= 2300;  
	const  ENVIOMAIL    	= 2400;  

}

  
?>