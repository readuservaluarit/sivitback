<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class EjercicioContable extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'ejercicio_contable';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
  public $belongsTo = array(
        'DatoEmpresa' => array(
            'className' => 'DatoEmpresa',
            'foreignKey' => 'id_dato_empresa',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Estado' => array(
            'className' => 'Estado',
            'foreignKey' => 'id_estado',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        
        'Contador' => array(
            'className' => 'Contador',
            'foreignKey' => 'id_contador',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'EjercicioContableNuevo' => array(
            'className' => 'EjercicioContable',
            'foreignKey' => 'id_ejercicio_contable_nuevo',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        )
        
        );
        
  public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     "d_contador"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_ejercicio"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "ultimo_asiento"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_estado"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_activo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
   
     );

}


      
  public function getFechaInicio($id_ejercicio_contable){
      
        $ejercicio_contable = $this->find('first', array(
                                                'conditions' => array('EjercicioContable.id' =>$id_ejercicio_contable )
                                                ));
                                                
       return $ejercicio_contable["EjercicioContable"]["fecha_apertura"];
    
      
      
      
  }
  
  
  
  
  public function getFechaFin($id_ejercicio_contable){
      
        $ejercicio_contable = $this->find('first', array(
                                                'conditions' => array('EjercicioContable.id' =>$id_ejercicio_contable )
                                                ));
                                                
       return $ejercicio_contable["EjercicioContable"]["fecha_cierre"];
    
      
      
      
  } 
  
  public function GetEjercicioPorFecha($fecha,$id=0,$activo=1){
    
    $conditions = array();
    
    array_push($conditions,array("EjercicioContable.fecha_apertura <=" => $fecha));
    array_push($conditions,array("EjercicioContable.fecha_cierre >=" => $fecha));
    array_push($conditions,array("EjercicioContable.activo" => $activo));
    
    if($id !=0)
         array_push($conditions,array("EjercicioContable.id <>" => $id));
    
    
      $ejercicio_contable = $this->find('first', array(
                                                'conditions' => $conditions,
                                                'contain'=>array("Contador")
                                            
                                                ));
                                                
      if($ejercicio_contable)
        return $ejercicio_contable;
      else
        return 0; 
    
    
    
    
}        


public function getEstado($id_ejercicio_contable){
      
        $ejercicio_contable = $this->find('first', array(
                                                'conditions' => array('EjercicioContable.id' =>$id_ejercicio_contable )
                                                ));
                                                
       return $ejercicio_contable["EjercicioContable"]["id_estado"];
    
      
      
      
  }
  
public function getDescripcion($id_ejercicio_contable){
      
        $ejercicio_contable = $this->find('first', array(
                                                'conditions' => array('EjercicioContable.id' =>$id_ejercicio_contable )
                                                ));
                                                
       return $ejercicio_contable["EjercicioContable"]["d_ejercicio"];
    
      
      
      
  }  
  
  
  public function getContador($id_ejercicio_contable){
      
        $ejercicio_contable = $this->find('first', array(
                                                'conditions' => array('EjercicioContable.id' =>$id_ejercicio_contable )
                                                ));
                                                
       return $ejercicio_contable["EjercicioContable"]["id_contador"];
    
      
      
      
  }
  
  
  
  public function getPrimerPeriodo($id_ejercicio){
      
      
        $periodo_contable = ClassRegistry::init('PeriodoContable');
      
        $periodo_contable = $periodo_contable->find('first', array(
                                                'conditions' => array('PeriodoContable.id_ejercicio_contable' =>$id_ejercicio),
                                                'order' =>'PeriodoContable.fecha_desde ASC'
                                                 )
                                                );
                                                
        return $periodo_contable["PeriodoContable"]["id"];
      
  }
  
  
  public function getEjercicioContable($id_ejercicio){
  	
  	

  	
  	$ejercicio_contable = $this->find('first', array(
  			'conditions' => array('EjercicioContable.id' =>$id_ejercicio)
  	)
  			);
  	
  	return $ejercicio_contable["EjercicioContable"];
  	
  }
        
        
        
 
	

}
