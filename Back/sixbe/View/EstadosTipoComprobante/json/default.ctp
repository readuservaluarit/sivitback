<?php
	// CONFIG  CON MODE DISPLAYED CELLS
	$model["id_estado_comprobante"]["show_grid"] = 1;
	$model["id_estado_comprobante"]["header_display"] = "Descripci&oacute;n";
	$model["id_estado_comprobante"]["minimun_width"] = "90"; //REstriccion mas de esto no se puede achicar
	$model["id_estado_comprobante"]["order"] = "1";
	$model["id_estado_comprobante"]["text_colour"] = "#000077";
	
	$model["id"]["show_grid"] = 1;
	$model["id"]["header_display"] = "Activo";
	$model["id"]["minimun_width"] = "90"; //Restriccion mas de esto no se puede achicar
	$model["id"]["order"] = "2";
	$model["id"]["text_colour"] = "#FF0000";
	
	$output = array(
			"status" =>EnumError::SUCCESS,
			"message" => "EstadoTipoComprobante",
			"content" => $model
		);

	echo json_encode($output);
?>