<?php
App::uses('AppModel', 'Model');

class QaCertificadoCalidad extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'qa_certificado_calidad';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id_producto' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar un producto',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
        
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 public $belongsTo = array(
        'Persona' => array(
            'className' => 'Persona',
            'foreignKey' => 'id_persona',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'PedidoInterno' => array( //es remito
            'className' => 'Comprobante',
            'foreignKey' => 'id_pedido_interno',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Producto' => array(
            'className' => 'Producto',
            'foreignKey' => 'id_producto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'OrdenArmado' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_orden_armado',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Estado' => array(
            'className' => 'Estado',
            'foreignKey' => 'id_estado',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        
);

public $hasMany = array(
        'QaCertificadoCalidadItem' => array(
            'className' => 'QaCertificadoCalidadItem',
            'foreignKey' => 'id_qa_certificado_calidad',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        
);
	

}
