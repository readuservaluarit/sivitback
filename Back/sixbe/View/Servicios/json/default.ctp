<?php
		include_once(APP.'View'.DS.'Articulo'.DS.'json'.DS.'articulo.ctp');
         
  		App::import('Model','Comprobante');
  		App::import('Lib','SecurityManager');
		$comprobante = new Comprobante();
		$stock = $comprobante->getActivoModulo(EnumModulo::STOCK);
		
		
		 $sm = new SecurityManager(); 
  
         
         
		$talle = $sm->permiso_particular("CONSULTA_TALLE",AuthComponent::user('username'))>0;
		$color = $sm->permiso_particular("CONSULTA_COLOR",AuthComponent::user('username'))>0;

        
        
        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "C&oacute;digo";
        $model["codigo"]["width"] = "12";
        $model["codigo"]["order"] = "2";
        $model["codigo"]["text_colour"] = "#000000";
        
        $model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "Descripci&oacute;n";
        $model["d_producto"]["width"] = "35";
        $model["d_producto"]["order"] = "3";
        $model["d_producto"]["text_colour"] = "#0000FF";
        
        $model["d_unidad"]["show_grid"] = 1;
        $model["d_unidad"]["header_display"] = "Unidad";
        $model["d_unidad"]["width"] = "5";
        $model["d_unidad"]["order"] = "4";
        $model["d_unidad"]["text_colour"] = "#0000DD";
        
        $model["d_categoria"]["show_grid"] = 1;
        $model["d_categoria"]["header_display"] = "Categor&iacute;a";
        $model["d_categoria"]["order"] = "4";
        $model["d_categoria"]["width"] = "10";
        $model["d_categoria"]["text_colour"] = "#0000DD";
        
        $model["d_familia"]["show_grid"] = 1;
        $model["d_familia"]["header_display"] = "Familia";
        $model["d_familia"]["width"] = "10";
        $model["d_familia"]["order"] = "4";
        $model["d_familia"]["text_colour"] = "#0000DD";
       
        $model["costo"]["show_grid"] = 1;
        $model["costo"]["header_display"] = "Costo";
        $model["costo"]["width"] = "5";
        $model["costo"]["order"] = "5";
        $model["costo"]["text_colour"] = "#DD0000";
        
      
        
        
     
		$model["revision"]["show_grid"] = 1;
        $model["revision"]["header_display"] = "RV";
        $model["revision"]["width"] = "5";
        $model["revision"]["order"] = "12";
        $model["revision"]["text_colour"] = "#0000FF";
		
	
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Producto",
                    "content" => $model
                );
        
        echo json_encode($output);
?>