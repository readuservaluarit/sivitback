
 DECLARE done INT DEFAULT 0;
  DECLARE products_id INT;
  DECLARE result VARCHAR(4000);
  DECLARE cur1 CURSOR FOR SELECT nro_comprobante FROM comprobante; /*query para traer los NRO PI) aca debe ir una tabla temporal con los nro_comprobante*/
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

  OPEN cur1;

  REPEAT
    FETCH cur1 INTO products_id;
    IF NOT done THEN
      
         INSERT INTO comprobante (id_tipo_comprobante,nro_comprobante,fecha_generacion,id_usuario,id_estado_comprobante,activo,id_punto_venta)

VALUES (202,@nro_comprobante,NOW(),1,1,1,1);

          SET @id_comprobante =  LAST_INSERT_ID();
          INSERT INTO movimiento (id_movimiento_tipopo,id_comprobante,id_deposito_destino,fecha) VALUES  (800,@id_comprobante,12,NOW());
    END IF;
  UNTIL done END REPEAT;

  CLOSE cur1;