<?php

class RegionDWPicker extends EntityPicker{
    public $model = "RegionDW";
    
    public $pickerTitle = "Selección de Región";
    
    
     public function getPickerJoins() {
        return array(
            array(
                'alias' => 'JurisdiccionDW',
                'table' => 'jurisdiccion',
                'type' => 'LEFT',
                'conditions' => '`JurisdiccionDW`.`id` = `RegionDW`.`id_jurisdiccion`'
                ));
    }
    
    public $pickerFields = array(
                                    array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),    
                                    array("title" => "Nombre", "field" => "nombre", "width" => "30%", "keyOrder" => "nombre") ,    
                                    array("title" => "Jurisdicci&oacute;n", "field" => "JurisdiccionDW.nombre", "width" => "70%", "keyOrder" => "JurisdiccionDW.nombre")
                                );

     public function getPickerOrder() {
        //return 'JurisdiccionDW.nombre, RegionDW.nombre';
        return 'JurisdiccionDW.nombre, LPAD(RegionDW.nombre, 2, "0")';
    }   
                                    
    public function getPickerConditions($busqueda){
        return array('RegionDW.nombre LIKE' => '%' . $busqueda['bus_unico'] . '%'   );

    }
    
    public function getPickerInitialFilter(){
        //Filtra siempre por la Region del usuario si corresponde
        return DataFilter::getRegionPickerFilter();
    }

}