<?php   

function digitoVerificadorCaeAfip($txt){
  $i = $pares = $impares = 0;

  for( $i = 0; $i < strlen($txt); $i++ ) {
    // OJO: Los caracteres impares esta en la posiciones pares del array y viceversa, porque el array arranca desde la posicion 0
    if( $i % 2 ) {
      $pares += $txt[$i];
    } else {
      $impares += $txt[$i];
    }
  }
  $impares = $impares * 3;
  
  $total = $pares + $impares;
  
  $dv = $total % 10;

  return $dv;
}

/**
 * PDF exportación de Orden Compra
 */


$logo = '/img/'.$datos_empresa["DatoEmpresa"]["id"].'.png';
App::import('Vendor','mytcpdf/mytcpdf'); 
App::import('Vendor','tcpdf/tcpdf_barcodes_1d'); 
App::import('Model','Comprobante');

	


$comprobante = new Comprobante();

if($comprobante->esImportado($datos_pdf['ComprobanteItem']) == 1)
    $orden_compra_externa = implode(",",$comprobante->getOrdenCompraExterna($datos_pdf['ComprobanteItem']));
else
    $orden_compra_externa = $datos_pdf['Comprobante']['orden_compra_externa'];


$html = $comprobante->getHeaderPDF($datos_pdf,$datos_empresa,'FACTURA');



$pdf = new MyTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  


$html.= '<table width="100%"   style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
  <tr >
                    <td colspan="4"></td>
                  </tr>
  <tr>  
  

 
 </table>


                  
                    
<table width="100%" border="1"  style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
                  <tr  style="background-color:rgb(224, 225, 229);">
                    <td colspan="4"><div align="center"><strong> Cliente </strong></div></td>
                  </tr>
                  <tr>                                     
                     <td width="10%" align="center"><strong> Codigo:</strong> '.$datos_pdf["Persona"]["codigo"].'</td>
                    <td width="42%"><strong> Raz&oacute;n Social:</strong> '.$datos_pdf["Persona"]["razon_social"].'</td>
                    <td width="26%"><strong> '.$datos_pdf["Persona"]["TipoDocumento"]["d_tipo_documento"].':</strong> '.$datos_pdf["Persona"]["cuit"].' </td>
                    
              
                       <td width="22%"><strong> O/C:</strong> '.$orden_compra_externa.'</td>
                    
                    

                 </tr>
                              <tr>
                                <td colspan="3" align="left" ><strong> Direccion:</strong> '.$datos_pdf["Persona"]["calle"].' - '.$datos_pdf["Persona"]["numero_calle"].' - '.$datos_pdf["Persona"]["localidad"].' - '.$datos_pdf["Persona"]["ciudad"].' - '.$datos_pdf["Persona"]["Provincia"]["d_provincia"].' - '.$datos_pdf["Persona"]["Pais"]["d_pais"].'</td>
                                <td rowspan="2"><strong> Tel.:</strong> '.$datos_pdf["Persona"]["tel"].'</td>
                              </tr>
                              
                              <tr>
                                
                                <td colspan="3" align="left"><strong> Condicion de Iva:</strong> '.$datos_pdf["Persona"]["TipoIva"]["d_tipo_iva"].'</td>
                              </tr>
                              
                              
                    <tr  style="background-color:rgb(224, 225, 229);">
                                    <td colspan="4"><div align="center"><strong> Condiciones de Venta</strong></div></td>
                    </tr>
                    <tr>
                    <td colspan="2"><div align="center"><strong> '.$datos_pdf["CondicionPago"]["d_condicion_pago"].'</strong></div></td>
                    <td colspan="2"><div align="center"><strong> Vencimiento: </strong>'.date("d-m-Y", strtotime($datos_pdf["Comprobante"]["fecha_vencimiento"])).'</div></td>
                    
                    </tr>
                     <tr  style="background-color:rgb(224, 225, 229);">
                                    <td colspan="4"><div align="center"><strong> Referencias</strong></div></td>
                    </tr>
                    <tr>
                    <td colspan="2"><div align="center"><strong> Remitos: </strong>'.implode(",", $remitos_relacionados).'</div></td>
                    <td colspan="2"><div align="center"><strong> Pedidos de Venta: </strong>'.implode(",", $pedidos_internos_relacionados).'</div></td>
                    
                    
                    </tr>
                    
                    
                                 </table>
 
 <table width="100%"   style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
  <tr >
                    <td colspan="4"></td>
                  </tr>
  <tr>  
  

 
 </table>
                                 
  

<table width="99%" border="1"  style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">

    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <td class="sorting"  rowspan="1" colspan="1" style="width:3%;"><strong>Itm</strong></td>
        <td class="sorting"  rowspan="1" colspan="1" style="width:7%;"><strong>O/C</strong></td>
        <td class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>Un.</strong></td>
        <td class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>Cant</strong></td>

        <td class="sorting"  rowspan="1" colspan="1" style="width:13%;"><strong>C&oacute;digo</strong></td>
        <td class="sorting"  rowspan="1" colspan="1" style="width:24%;"><strong>Descripci&oacute;n</strong></td>
        <td class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>P.Un.</strong></td>

        <td class="sorting"  rowspan="1" colspan="1" style="width:9%;"><strong>Iva(%)</strong></td>
        <td class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>Dto.Un(%)</strong></td>
        <td class="sorting"  rowspan="1" colspan="1" style="width:15%;"><strong>Subtotal</strong></td>
    </tr>

</table>';


     
$pdf->set_datos_header($html);

 
$descuento = (($datos_pdf["Comprobante"]["total_comprobante"]*$datos_pdf["Comprobante"]["descuento"])/100) ;



//genero el codigo de barras para ver las especificaciones http://www.formularioscontinuos.com/nota1.html
if( $datos_pdf["Comprobante"]["cae"] != '' ){

    //genero el string para el codigo de barras
    $cuit =  str_replace("-","",$datos_empresa["DatoEmpresa"]["cuit"]);
    $tipo_doc_afip = str_pad($datos_pdf["TipoComprobante"]["codigo_afip"],2,"0",STR_PAD_LEFT);
    $cae = $datos_pdf["Comprobante"]["cae"];
    $fecha_vencimiento = str_replace("-","",$datos_pdf["Comprobante"]["vencimiento_cae"]);
    
    $codigo_barras = $cuit.$tipo_doc_afip.$cae.$fecha_vencimiento;
    $codigo_barras = $codigo_barras.digitoVerificadorCaeAfip($codigo_barras);
    $pdf->set_barcode($codigo_barras);  
    
   
}  


$impuestos = '';
if(isset($datos_pdf["ComprobanteImpuesto"])){
        $importe_iva = 0;
        foreach($datos_pdf["ComprobanteImpuesto"] as $impuesto){

            if($impuesto['id_impuesto']!= EnumImpuesto::IVANORMAL){ //no imprimo el iva
                $impuestos .= ' <tr>
                     <td style="width:30%;">'.$impuesto["Impuesto"]["abreviado_impuesto"].'</td>
                      <td style="width:40%;">'.money_format("%!n",$impuesto["tasa_impuesto"]).'% (B.Imp. '.money_format("%!n",$impuesto["base_imponible"]).')</td>
                     <td style="width:30%;">'.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format("%!n",$impuesto["importe_impuesto"]).'</td>
                   </tr>';
             }else{
                $importe_iva = $importe_iva + $impuesto["importe_impuesto"];
             }
        }
        
        
} 





App::import('Vendor', 'NumberToLetterConverter', array('file' => 'Classes/NumberToLetterConverter.class.php'));   
$numero_conversion = new NumberToLetterConverter();
$letras_total =  "Importe en letras: ".$numero_conversion->to_word(str_replace(".",",",$datos_pdf["Comprobante"]["total_comprobante"]),$datos_pdf["Moneda"]["simbolo_internacional"]);


$footer='
  <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>                     
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody> 
</table>


<table width="100%" border="1" style="width:100%;">
   <tr>';
   
   
  if($datos_pdf["Comprobante"]["imprimir_obs_cuerpo"] != 1)
  	$observacion =  nl2br($datos_pdf["Comprobante"]["observacion"]);
  else
  	$observacion = "";
   
   
$time = strtotime($datos_pdf["Comprobante"]["vencimiento_cae"]);
$month_venc = date("m",$time);
$year_venc =  date("y",$time);
$day_venc =   date("d",$time);
      
$footer.='<td width="50%">Observaciones: '.$observacion;
 	
 	
   
     if($datos_pdf["Comprobante"]["id_moneda"] == EnumMoneda::Peso && $datos_pdf["Comprobante"]["valor_moneda"]!= $datos_pdf["Comprobante"]["valor_moneda2"])
        $footer.=' <br/>  Esta factura por un monto de '.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n',$datos_pdf["Comprobante"]["total_comprobante"]).' equivale a USD '.money_format('%!n',round($datos_pdf["Comprobante"]["total_comprobante"]/$datos_pdf["Comprobante"]["valor_moneda"],4)).'  Ref 1 USD= '.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!.5n',bcdiv($datos_pdf["Comprobante"]["valor_moneda"],1,5)).'. Ref Interna Factura nro ='.$datos_pdf["Comprobante"]["id"];
     elseif($datos_pdf["Comprobante"]["id_moneda"] == EnumMoneda::Dolar  && $datos_pdf["Comprobante"]["valor_moneda"]!= $datos_pdf["Comprobante"]["valor_moneda2"])
        $footer.=' <br/>  Esta factura por un monto de '.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n',$datos_pdf["Comprobante"]["total_comprobante"]).' equivale a PES '.money_format('%!n',round($datos_pdf["Comprobante"]["total_comprobante"]/$datos_pdf["Comprobante"]["valor_moneda2"],4)).'  Ref 1 USD= PES '.money_format('%!.5n',bcdiv($datos_pdf["Comprobante"]["valor_moneda"]/$datos_pdf["Comprobante"]["valor_moneda2"],1,5)).'. Ref Interna Factura nro ='.$datos_pdf["Comprobante"]["id"];
     elseif($datos_pdf["Comprobante"]["id_moneda"] == EnumMoneda::Euro  && $datos_pdf["Comprobante"]["valor_moneda"]!= $datos_pdf["Comprobante"]["valor_moneda3"])
        $footer.=' <br/>  Esta factura por un monto de '.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n',$datos_pdf["Comprobante"]["total_comprobante"]).' equivale a PES '.money_format('%!n',round($datos_pdf["Comprobante"]["total_comprobante"]/$datos_pdf["Comprobante"]["valor_moneda3"],4)).'  Ref 1 USD=  EU'.money_format('%!.5n',bcdiv($datos_pdf["Comprobante"]["valor_moneda"]/$datos_pdf["Comprobante"]["valor_moneda3"],1,5)).'. Ref Interna Factura nro ='.$datos_pdf["Comprobante"]["id"];
       
       
    $footer.='<br>'.$letras_total.'</td>'; 

   
     $footer.='<td width="50%">
     <table width="100%" border="0">
      <tr>
         <td style="width:30%;">DESC. GLOBAL</td>
         <td style="width:40%;">&nbsp;</td>
         <td style="width:30%;">'.money_format('%!n',round($datos_pdf["Comprobante"]["descuento"],2)).'%</td>
       </tr>
       <tr>
         <td style="width:30%;">SUBTOTAL</td>
         <td style="width:40%;">&nbsp;</td>
         <td style="width:30%;">'.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n', $datos_pdf["Comprobante"]["subtotal_neto"]).'</td>
       </tr>
       '.$impuestos.'
       <tr>
         <td><strong>TOTAL</strong></td>
         <td>&nbsp;</td>
         <td><strong>'.$datos_pdf["Moneda"]["simbolo_internacional"].' '. money_format("%!n", $datos_pdf["Comprobante"]["total_comprobante"]).'</strong></td>
       </tr>
     </table></td>
   </tr>
   <tr>
     <td > CAE: '.$datos_pdf["Comprobante"]["cae"].'</td>
     <td > Fecha Vto.: '.$day_venc.'-'.$month_venc.'-'.$year_venc.'</td>
   </tr>
   <tr>
     <td  colspan="2" style="font-size:6px;">'.$datos_pdf["TipoComprobante"]["impresion_observacion_extra"].'</td>
   
   </tr>
</table>
';


  $pdf->set_datos_footer($footer);
  
  
   
   $html .= "<HR>";
   
   
   if( $datos_pdf["Comprobante"]["cae"] != '' ){
   
      $html.=$pdf->SetBarcode($codigo_barras,I25);
     
   
   }
   
   
   
   
   $html .= '
   
   <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody>
</table>
   
  ';
  

  
  
ob_clean();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sivit by Valuarit');
$pdf->SetTitle('Factura');
$pdf->SetSubject('Factura');
//$pdf->SetKeywords('');

                              

// set default header data

$title = "";
$subtitle = ""; 
$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 100, 20);
$pdf->SetHeaderMargin(10);
$pdf->SetFooterMargin(55);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 70);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


    


// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
/*
$html = <<<EOD
<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
EOD;
*/

$tbody = "";

$renglones = $datos_pdf["ComprobanteItem"];

$data = array();

$comprobante_item = new ComprobanteItem();



foreach($renglones as $key=>&$renglon){

   $descuento = ($renglon['descuento_unitario']);
    $aux["ComprobanteItem"]["precio_unitario"] = $renglon['precio_unitario'];
    $aux["ComprobanteItem"]["cantidad"] = $renglon['cantidad'];
    $precio_subtotal = $comprobante_item->getTotalItem($aux);//hago esto del $aux porque la funcion toma con ComprobanteItem
    
    
    if(isset($renglon["ComprobanteItemOrigen"]) && $renglon["ComprobanteItemOrigen"]>0){//ES IMPORTADA DE UN COMPROBANTE

        $renglon["orden_compra_externa"]  = $renglon["ComprobanteItemOrigen"]["Comprobante"]["orden_compra_externa"];

    }else{

        $renglon["orden_compra_externa"] = '';
    }
                  
    
   $aux = $renglon;
   $comprobante_item->calculaUnidad($aux);

   
   $tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
   $tbody .= "<td   width=\"3%\" >" .$renglon['n_item'] . "</td>";
   $tbody .= "<td   width=\"7%\" >" .$renglon['orden_compra_externa'] . "</td>";
   $tbody .= "<td   width=\"5%\"  >" .$aux["ComprobanteItem"]["d_unidad"]. "</td>";
   $tbody .= "<td   width=\"5%\" >" .$renglon['cantidad']. "</td>";
   $tbody .= "<td   width=\"13%\"   align=\"left\">" .$comprobante_item->getCodigoFromProducto($renglon). "</td>";
   $tbody .= "<td   width=\"24%\"  align=\"left\">" .$renglon['item_observacion'] . "</td>";       
   $tbody .= "<td   width=\"10%\"  >" .money_format('%!n', $renglon['precio_unitario_bruto']). "</td>";
   $tbody .= "<td   width=\"9%\" >" .$renglon['Iva']['d_iva_detalle'] . "</td>"; 
   $tbody .= "<td   width=\"10%\"  >" .$descuento. "</td>";
   $tbody .= "<td   width=\"15%\"  >" .money_format('%!n', $precio_subtotal)."</td>";
   $tbody .= "</tr>";
}
 
//agrego las observaciones

$time = strtotime($datos_pdf["Comprobante"]["fecha_vencimiento"]);
$month_venc = date("m",$time);
$year_venc =  date("y",$time);
$day_venc =   date("d",$time);






$html='';


$html .=' 
 

    
<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">

  <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
   <tr style=\"font-size:10px;font-family:\'Courier New\', Courier, monospace\">
     <td colspan="10">
    </td>
  </tr>
    '.$tbody.'
  </tbody>  
</table>';



$obs_en_cuerpo = ''; 

if($datos_pdf["Comprobante"]["imprimir_obs_cuerpo"] == 1){


$obs_en_cuerpo = '

<table width="100%" border="0"  align="left" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">


<td>


Observaciones:'.nl2br($datos_pdf["Comprobante"]["observacion"]).'
</td>
</table>';


$html.= $obs_en_cuerpo;



}
  
  
   
   
   
$pdf->writeHTML($html, true, false, true, false, true);


// QRCODE,L : QR-CODE Low error correction
// set style for barcode
$style = array(
    'border' => 2,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

//$pdf->Text(20, 25, 'QRCODE L');

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->SetBarcode(date("Y-m-d H:i:s", time()));
ob_clean();




$pdf_name = $comprobante->getNamePDF($datos_pdf);
$root_pdf = $comprobante->getRootPDF();


if($datos_pdf["TipoComprobante"]["adjunta_pdf_mail"] == 1) {

	$path_para_pdf = $datos_empresa["DatoEmpresa"]["local_path"].$root_pdf;
          	
	$pdf->Output($path_para_pdf.$pdf_name.'.pdf', 'F');//Esta linea guarda un archivo en path_para_pdf
}



$pdf->Output($pdf_name.'.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
