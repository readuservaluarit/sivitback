
UPDATE orden_armado
JOIN comprobante ON comprobante.id = orden_armado.`id_pedido_interno`
JOIN comprobante_item ON comprobante_item.`id_comprobante` = comprobante.id 
AND comprobante_item.`id_producto` = orden_armado.`id_producto`

SET fecha_entrega_maxima =  DATE_ADD(comprobante.fecha_generacion,INTERVAL (comprobante_item.`dias`-1) DAY),
	fecha_entrega_minima =  DATE_ADD(comprobante.fecha_generacion,INTERVAL (comprobante_item.`dias` -15) DAY)
	
	
	where id = 10000