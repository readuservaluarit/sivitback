<?php
App::uses('ComprobantesController', 'Controller');

 
  
class RetencionesController extends ComprobantesController {
    
    public $name = EnumController::Retenciones;
    public $model = 'Comprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $requiere_impuestos = 0;
    public $id_impuesto = 0;//esta variable la uso para el chequeoContador, como es un comprobante en base a un impuesto tengo que buscar por la columna id_impuesto en la tabla comprobante_punto_venta_numero
    //public $id_sistema_comprobante = EnumSistema::COMPRAS;
    
    /*
    public function beforeFilter() {
     
     $this->loadModel("TipoComprobante");
     
     $this->id_tipo_comprobante =  $this->TipoComprobante->getRetencionesPropias();
     AppController::beforeFilter();
        
    }
    
    /**
    * @secured(CONSULTA_RETENCION_PROPIA)
    */
    public function index() {
    	
  
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        
        $conditions = array();
       
      	$conditions = $this->RecuperoFiltros($this->model);
     
        
        $id_impuesto = $this->getFromRequestOrSession($this->model.'.id_impuesto');
        $id_sistema = $this->getFromRequestOrSession($this->model.'.id_sistema');
        
       
        if($id_impuesto !=''){
        	$this->loadModel("Impuesto");
        	array_push($conditions, array('ComprobanteReferenciaImpuesto.id_impuesto' => $id_impuesto)); 
        	
        	array_push($this->array_filter_names,array("Impuesto: "=>$this->Impuesto->getDescripcion($id_impuesto)));
        	
        	
        }
            
        if($id_sistema !=""){
        	array_push($conditions, array('TipoComprobante.id_sistema ' => $id_sistema));
        }
        
        
     
        
                   
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
        		'limit' => $this->numrecords,
             'contain' =>array('ComprobanteReferenciaImpuesto'=>array('Impuesto','Comprobante'=>array('EstadoComprobante','PuntoVenta','TipoComprobante')),'Persona','Moneda'),
            'conditions' => $conditions,
           // 'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.nro_comprobante desc'
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
    
    
        
        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'cotizacion.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'cotizacion.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        $definitivos = array();
        
        foreach($data as &$valor){
            
             //$valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
             //unset($valor['ComprobanteItem']);
             
          // if(isset($valor['ComprobanteReferenciaImpuesto']['Impuesto'])){
                 $valor[$this->model]['d_impuesto'] = $valor['ComprobanteReferenciaImpuesto']['Impuesto']['d_impuesto'];
                 $valor[$this->model]['id_comprobante_impuesto'] = $valor['ComprobanteReferenciaImpuesto']['id'];
                 $valor[$this->model]['numero'] = $valor['Comprobante']['nro_comprobante'];
                 $valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
                 $valor[$this->model]['razon_social'] = $valor['Persona']['razon_social'];
                 $valor[$this->model]['fecha_contable'] = $valor['Comprobante']['fecha_contable'];
                 $valor[$this->model]['d_moneda'] = $valor['Moneda']['d_moneda'];
                 $valor[$this->model]['base_imponible'] = $valor['ComprobanteReferenciaImpuesto']['base_imponible'];
                 $valor[$this->model]['tasa_impuesto'] = $valor['ComprobanteReferenciaImpuesto']['tasa_impuesto'];
                 $valor[$this->model]['importe_impuesto'] = $valor['ComprobanteReferenciaImpuesto']['importe_impuesto'];
                 
              
                 
                 $this->{$this->model}->formatearFechas($valor); 
                 
                 if(isset($valor['EstadoComprobante']))
                    $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
             
                 unset($valor['ComprobanteReferenciaImpuesto']);
                 unset($valor['Persona']);
                 unset($valor['Moneda']);
                 unset($valor['PuntoVenta']);
                 unset($valor['EstadoComprobante']);
                 array_push($definitivos,$valor);
                 
         /*  }else{
               
               unset($data[$key]);
           }  
             */
            
            
        }
        $this->data = $definitivos;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $definitivos,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    
    /**
    * @secured(ADD_RETENCION_PROPIA)
    */
    public function generarRetencionesPorOrdenPago($id_comprobante_impuesto){
        
     $this->loadModel("ComprobanteImpuesto");
     $this->loadModel($this->model);
     $this->RequestHandler->ext = 'json';   
     

     /*busco si el impuesto del comprobante tiene retencion generada*/
     
     $cpvn = $this->ComprobanteImpuesto->find('first', array(
                                                'conditions' => array('ComprobanteImpuesto.id' => $id_comprobante_impuesto),
                                                'contain' =>array("Comprobante",'Impuesto'=>array("TipoComprobante"))
                                                ));
                                                
                                              
     
     if(!$cpvn["ComprobanteImpuesto"]["id_comprobante_referencia"]>0){ //quiere decir que este impuesto no genero ningun comprobante.
         
      
     	$this->id_impuesto = 							$cpvn["ComprobanteImpuesto"]["id_impuesto"];
        $id_tipo_comprobante =  $this->getTipoRetencion($cpvn["ComprobanteImpuesto"]["id_impuesto"]);
        $comprobante["Comprobante"]["id_tipo_comprobante"] = $id_tipo_comprobante;
        $comprobante["Comprobante"]["id_punto_venta"] = 	 $cpvn["Comprobante"]["id_punto_venta"];
        $comprobante["Comprobante"]["total_comprobante"] = 	 $cpvn["ComprobanteImpuesto"]["importe_impuesto"];
        $comprobante["Comprobante"]["id_moneda"] = 			 $cpvn["Comprobante"]["id_moneda"];
        $comprobante["Comprobante"]["valor_moneda"] = $cpvn["Comprobante"]["valor_moneda"];
        $comprobante["Comprobante"]["id_estado_comprobante"] = EnumEstadoComprobante::CerradoReimpresion;
        $comprobante["Comprobante"]["definitivo"] = 1;
        $comprobante["Comprobante"]["fecha_add"] = date("Y-m-d H:i:s");
        $comprobante["Comprobante"]["fecha_generacion"] = date("Y-m-d H:i:s");
        $comprobante["Comprobante"]["fecha_contable"] = $cpvn["Comprobante"]["fecha_contable"];//la retencion tiene la fecha_contable del comprobante
        $comprobante["Comprobante"]["id_persona"] = $cpvn["Comprobante"]['id_persona'];
        
        $this->request->data = $comprobante;
        //$this->reques->data
         $ds = $this->{$this->model}->getdatasource();
         $this->llamada_interna = 1;//para que entre al add  
        try{
			$ds->begin(); 
			parent::add();
			
			if($this->id>0){
				$id_comprobante_generado = $this->id;
				
				/*actualizo el comprobante impuesto con el id de la retencion*/
				$this->ComprobanteImpuesto->updateAll(
												  array('ComprobanteImpuesto.id_comprobante_referencia' => $id_comprobante_generado ),
												  array('ComprobanteImpuesto.id' => $id_comprobante_impuesto) );
				$ds->commit();
				$this->pdfExport($id_comprobante_impuesto); 
			}else{
				
				$ds->rollback();
				$this->set($this->output);
				
				$this->layout = 'json'; 
				$this->set("salida", $this->output);//uso el output ya que se carga en el metodo add de comprobantes porque se utiliza para agregar la retencion
				return;//el return con vacio para que vuelva del flujo
			}                                                 
       }catch(Exception $e){
          $ds->rollback();
          $mensaje = "Ha ocurrido un error y no puede crearse la Retenci&oacute;n".$e->getMessage();
          $tipo = EnumError::ERROR;
          $this->response->type('json');
          
          $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" => 0,
            "nro_add" => 0,
            "id_asiento" => -1
            );
            
          $this->set("_serialize", array("status", "message", "content","id_add","nro_add","id_asiento"));
          die();
           
       }      
     }else
        $this->pdfExport($id_comprobante_impuesto);//devuelvo el pdf     
    }
    
    
    /**
    * @secured(MODIFICACION_RETENCION_PROPIA)
    */
    public function edit($id){
     $this->request->data[$this->model]["id_tipo_comprobante"] = $this->id_tipo_comprobante;     
     parent::edit($id);
     return;  
    }
    
    
   /**
    * @secured(CONSULTA_RETENCION_PROPIA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
      
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
        
        
         $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);          
    }
    
    
  
    /**
    * @secured(REPORTE_RETENCION_PROPIA)
    */
    public function pdfExport($id,$vista='pdf_export'){
        
      $this->loadModel("ComprobanteImpuesto");
      $this->loadModel("Comprobante");
        //$this->Comprobante->id = $id;
         /*$this->Comprobante->contain(array('ComprobanteItem' => array('Producto'=>array('Iva'),'Iva','Unidad'),'EstadoComprobante','Moneda',
                                    'Persona'=> array('Pais','Provincia'),'CondicionPago','PuntoVenta','TipoComprobante','CondicionPago',
                                    'ComprobanteImpuesto'=>array('Impuesto'),'Transportista','Sucursal'=>array('Provincia','Pais')));
                                    */
                                    
       $this->request->data = $this->ComprobanteImpuesto->find('first',array('contain'=>array(
       
                                                                                       "Comprobante"=>array("ComprobanteItem"=>array("ComprobanteOrigen"=>array("TipoComprobante",'ComprobanteItem','ComprobanteImpuesto'=>array('Impuesto'=>array("Sistema","ImpuestoGeografia","SubTipoImpuesto")),'Moneda')),
                                                                                       'EstadoComprobante','Moneda','Persona'=> array('Pais','Provincia'),
                                                                                       'CondicionPago','PuntoVenta','TipoComprobante'=>array(),
       
                                                                                       'Sucursal'=>array('Provincia','Pais')
                                                                                        )
                                                                                       
                                                                                       ,
                                                                                       'ComprobanteReferencia'=>array('PuntoVenta','TipoComprobante','Persona','Moneda')),'Impuesto',
                                                                                'conditions' =>array('ComprobanteImpuesto.id'=>$id),
                                                                                 
                                                                                ));
       
        //$this->request->data = $this->Comprobante->read();
        
        Configure::write('debug',0);
        
      
        if($this->request->data["Impuesto"]["SubTipoImpuesto"]["id"] == EnumSubTipoImpuesto::RETENCION_GANANCIA)
            $vista = "retencion_ganancia_pdf";
        
        
        foreach($this->request->data['Comprobante']["ComprobanteItem"] as $key=>&$valor){ 
          
          $impuestos_iva =  $this->getImpuestos($valor['ComprobanteOrigen']['ComprobanteImpuesto'],$valor['ComprobanteOrigen']["id"],array(EnumSistema::COMPRAS),array(EnumImpuesto::IVACOMPRAS),EnumTipoImpuesto::IVA_IMPUESTO_VALOR_AGREGADO);
      
          $valor['ComprobanteOrigen']['total_iva']= $this->TotalImpuestos($impuestos_iva);   
        }
        
       
       
        
        
        
        
        $this->request->data["ComprobanteReferencia"]["nro_comprobante_completo"] = $this->Comprobante->GetNumberComprobante($this->request->data["ComprobanteReferencia"]["PuntoVenta"]["numero"],$this->request->data["ComprobanteReferencia"]["nro_comprobante"]);    
        $this->request->data["Comprobante"]["nro_comprobante_completo"] = $this->Comprobante->GetNumberComprobante($this->request->data["Comprobante"]["PuntoVenta"]["numero"],$this->request->data["Comprobante"]["nro_comprobante"]);    
        
        $this->set('datos_pdf',$this->request->data);
        $this->set('datos_empresa',$this->Session->read('Empresa'));
            
        $impuestos = '';
        $this->set('id',$id);
        Configure::write('debug',0);
        $this->layout = 'pdf'; //esto usara el layout pdf.ctp
        $this->response->type('pdf');
        
        if($vista==''){
            
            
          
            $this->render("pdf_export");
            
        }else{
           
           
            $this->render($vista);
        } 
     
        
    }
    
    
   
    
    
    
    
    
    
    public function CalcularImpuestos($id_comprobante,$id_tipo_impuesto='',$error=0,$message=''){
        
        return;
    }
    
    public function actualizoMontos($id_c,$nro_comprobante,$total_iva,$total_impuestos){
        
        return;
    }
    
    
    
    public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
    {
        parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
    }
    
    
    /**
     * @secured(BTN_EXCEL_RETENCIONES)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){

    	parent::excelExport($vista,$metodo,"Listado de Retenciones");
    	
    	
    	
    }
    
    
    
    protected function chequeoContador($id_tipo_comprobante,$id_punto_venta='',$id_impuesto=''){
    	
    	
    	
    	
    	return parent::chequeoContador($id_tipo_comprobante,$id_punto_venta,$this->id_impuesto);
    
    }
    
    
    
}
?>