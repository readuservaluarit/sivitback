<?php

App::uses('ProductosController', 'Controller');

class MateriasPrimasController extends ProductosController {
    
    
    public $name = 'MateriasPrimas';
    public $model = 'MateriaPrima';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $tipo_producto = EnumProductoTipo::MateriaPrima;
    public $nombre_producto = "La Materia Prima";
    public $model_stock = "StockMateriaPrima";
    public $d_padre = "d_materia_prima";
    
  
  
    /**
    * @secured(CONSULTA_MATERIA_PRIMA)
    */
    public function index() {
        
        parent::index();
    }
    
    
    
    /**
    * @secured(ABM_USUARIOS)
    */
    
    /**
    * @secured(ADD_MATERIA_PRIMA)
    */
    public function add() {
        
        parent::add();
      
    }
    
    
    /**
    * @secured(MODIFICACION_MATERIA_PRIMA)
    */
    function edit($id) {
        
        parent::edit($id);
    }
    
  
    /**
    * @secured(BAJA_MATERIA_PRIMA)
    */
    function delete($id) {
        
        parent::delete($id);
     
        
        
    }
    

    
    
  
    /**
    * @secured(CONSULTA_MATERIA_PRIMA)
    */
    public function existe_codigo(){
        
        parent::existe_codigo();
    }
    
    
    
    
   
    
    /**
    * @secured(CONSULTA_MATERIA_PRIMA)
    */
  public function getModel($vista='default'){
       parent::getModel($vista);
  }
    
    
  
   /**
    * @secured(BAJA_STOCK_MATERIA_PRIMA)
    */
  public function deleteDepositoAsociado($id,$id_deposito){
        
        $this->loadModel($this->model);
        
         $stockMateriaPrima = $this->MateriaPrima->StockMateriaPrima->find('first', array(
                                'conditions' => array('StockMateriaPrima.id' => $id,"StockMateriaPrima.id_deposito"=>$id_deposito),
                                'contain' =>false
                            ));
        
        $total = 0;
        if($stockMateriaPrima){
            $total = $stockMateriaPrima[0]["stock"];
            
        }
        
        if($total == 0 && $stockMateriaPrima){
        
        
        try{
            if ($this->MateriaPrima->StockMateriaPrima->delete(array("StockMateriaPrima.id"=>$id,"StockMateriaPrima.id_deposito"=>$id_deposito))) {
        
                $status = EnumError::SUCCESS;
                $mensaje = "El Deposito de Materia Prima ha sido eliminado exitosamente.";
              
            }else{
              
                
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
                
                
            }
        }
        catch(Exception $ex){ 
              $mensaje = "Ha ocurrido un error, el Deposito de la Materia Prima NO ha podido ser borrado";
               $status = EnumError::ERROR; 
        }
        
        
  }elseif ($total >0 ){
      
      $mensaje = "Ha ocurrido un error, el Deposito de la Materia Prima NO puede ser borrado ya que tiene stock.";
      $status = EnumError::ERROR; 
      
  }elseif(!$stockMateriaPrima){
      $mensaje = "El deposito NO fue encontrado verifique los parametros.";
      $status = EnumError::ERROR; 
      
  }
        
        if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
        }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
        }
        
        
        
        
    }
    
    
    
  /**
    * @secured(CONSULTA_MATERIA_PRIMA)
    */
  public function getCosto($id_articulo){
    parent::getCosto($id_articulo);
  }
    
  
  /**
   * @secured(CONSULTA_MATERIA_PRIMA)
   */
  public function existe_codigo_barra(){
  	
  	
  	parent::existe_codigo_barra();
  	return;
  }
    
    
}
?>