<?php 


   		$model["fecha"]["show_grid"] = 1;
		$model["fecha"]["type"] = EnumTipoDato::cadena;
        $model["fecha"]["header_display"] = "Fecha";
        $model["fecha"]["order"] = "0";
        
        
		$model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["header_display"] =  "Raz&oacute;n Social";
        $model["razon_social"]["type"] = EnumTipoDato::cadena;
        $model["razon_social"]["order"] = "1";
        $model["razon_social"]["width"] = "30";
		
        $model["cantidad"]["show_grid"] = 1;
		$model["cantidad"]["type"] = EnumTipoDato::entero;
        $model["cantidad"]["header_display"] = "Cantidad";
        $model["cantidad"]["order"] = "2";
        $model["cantidad"]["width"] = "20";
		
	
        $model["nro_pedido_interno"]["show_grid"] = 1;
        $model["nro_pedido_interno"]["type"] = EnumTipoDato::cadena;
        $model["nro_pedido_interno"]["header_display"] = "Pedido";
        $model["nro_pedido_interno"]["order"] = "3";
        $model["nro_pedido_interno"]["width"] = "35";
        
        $model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "Producto";
        $model["d_producto"]["type"] = EnumTipoDato::cadena;
        $model["d_producto"]["order"] = "4";
        $model["d_producto"]["width"] = "15";
		$model["d_producto"]["text_colour"] = "#0000FF";
		
		
		
		 
   
		
		$model["nro_orden_armado"]["show_grid"] = 1;
        $model["nro_orden_armado"]["header_display"] = "Orden Armado";
        $model["nro_orden_armado"]["type"] = EnumTipoDato::cadena;
        $model["nro_orden_armado"]["order"] = "6";
        $model["nro_orden_armado"]["width"] = "10";
		$model["nro_orden_armado"]["text_colour"] = "#0000FF";
		
		
		
		
		
		$model["d_estado"]["show_grid"] = 1;
        $model["d_estado"]["header_display"] = "Estado";
        $model["d_estado"]["type"] = EnumTipoDato::cadena;
        $model["d_estado"]["order"] = "7";
        $model["d_estado"]["width"] = "5";
		$model["d_estado"]["text_colour"] = "#0000FF";
        
        
        $model["QaCertificadoCalidadItem"]["show_grid"] = 0;
        $model["QaCertificadoCalidadItem"]["type"] = EnumTipoDato::json;
        $model["QaCertificadoCalidadItem"]["order"] = "8";
        $model["QaCertificadoCalidadItem"]["width"] = "5";
		$model["QaCertificadoCalidadItem"]["text_colour"] = "#0000FF";
		
		
		
		$model["Comprobante"]["show_grid"] = 0;
        $model["Comprobante"]["type"] = EnumTipoDato::json;
        $model["Comprobante"]["order"] = "8";
        $model["Comprobante"]["width"] = "5";
		$model["Comprobante"]["text_colour"] = "#0000FF";
		
		
		$model["OrdenArmado"]["show_grid"] = 0;
        $model["OrdenArmado"]["type"] = EnumTipoDato::json;
        $model["OrdenArmado"]["order"] = "8";
        $model["OrdenArmado"]["width"] = "5";
		$model["OrdenArmado"]["text_colour"] = "#0000FF";
        

 $output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "QaCertificadoCalidad",
				"content" => $model
                );
        echo json_encode($output);
?>