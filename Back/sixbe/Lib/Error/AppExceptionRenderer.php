<?

App::uses('ExceptionRenderer', 'Error');

class AppExceptionRenderer extends ExceptionRenderer {
    public function missingController($error) {
        $this->controller->render('/Errors/error404', 'layout');
        $this->controller->response->send();
    }

    public function missingAction($error) {
        $this->missingController($error);
    }
    
    public static function handleError($code, $description, $file = null,
    		$line = null, $context = null) {
    			echo 'There has been an error!';
    }
}