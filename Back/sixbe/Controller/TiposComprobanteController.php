<?php


class TiposComprobanteController extends AppController {
    public $name = 'TiposComprobante';
    public $model = 'TipoComprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');  

    
    /**
     * @secured(CONSULTA_TIPO_COMPROBANTE)
     */
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);

        $conditions = array(); 
        
        $id_cuenta_contable = strtolower($this->getFromRequestOrSession($this->model.'.id_cuenta_contable'));
        $id_tipo_comprobante = strtolower($this->getFromRequestOrSession($this->model.'.id'));
        
        $d_tipo_comprobante = strtolower($this->getFromRequestOrSession($this->model.'.d_tipo_comprobante'));
        $id_impuesto = strtolower($this->getFromRequestOrSession($this->model.'.id_impuesto'));
        $id_tipo_impuesto = strtolower($this->getFromRequestOrSession('Impuesto.id_tipo_impuesto'));
		//TODO: propuesta de flag para saber los comprobantes q se llaman impagos desde algun sistema
		// $es_comprobante_impago = strtolower($this->getFromRequestOrSession($this->model.'es_comprobante_impago'));
        $id_sistema = strtolower($this->getFromRequestOrSession($this->model.'.id_sistema'));
        $codigo_afip_desde = strtolower($this->getFromRequestOrSession($this->model.'.codigo_afip_desde'));
        $codigo_afip_hasta = strtolower($this->getFromRequestOrSession($this->model.'.codigo_afip_hasta'));
        $signo_comercial_no_cero = strtolower($this->getFromRequestOrSession($this->model.'.signo_comercial_no_cero'));
        
        if ($id_tipo_comprobante!= "")
        	array_push($conditions, array($this->model.'.id ' => $id_tipo_comprobante));
        	
        if ($id_cuenta_contable != "") 
                array_push($conditions, array($this->model.'.id_cuenta_contable ' => $id_cuenta_contable));
                 
        if ($d_tipo_comprobante != "") 
                array_push($conditions, array($this->model.'.d_tipo_comprobante LIKE ' => '%'.$d_tipo_comprobante.'%'));
                
        if ($id_impuesto != "") 
                array_push($conditions, array($this->model.'.id_impuesto ' => $id_impuesto));        
                
        if ($id_tipo_impuesto != "") 
                array_push($conditions, array('Impuesto.id_tipo_impuesto ' => $id_tipo_impuesto));  
       
        if ($id_sistema != ""){
                $id_sistema = explode(",", $id_sistema);
                array_push($conditions, array($this->model.'.id_sistema ' => $id_sistema)); 
        }
        
        if ($codigo_afip_desde != "") 
                array_push($conditions, array($this->model.'.codigo_afip >= ' => $codigo_afip_desde));         
        
        if ($codigo_afip_hasta != "") 
                array_push($conditions, array($this->model.'.codigo_afip <= ' => $codigo_afip_hasta));
                
        if ($signo_comercial_no_cero!= "")
        	array_push($conditions, array($this->model.'.signo_comercial <>' =>0 ));
        	
       
        
        
   
        
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de TipoComprobanteCausas', 'Datos de los Tipo de Documento', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_TipoComprobante', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'TipoComprobante.id', "10%");
                $formBuilder->addHeader('Nombre', 'TipoComprobante.d_TipoComprobante_causa', "50%");
                $formBuilder->addHeader('Nombre', 'TipoComprobante.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_TipoComprobanteCausa');
                $formBuilder->addField($this->model, 'cotizacion');
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else  // vista json
		{
			
			
			$data = $this->TipoComprobante->find('all', array(
					'conditions' =>$conditions,
					'contain' => array('DepositoOrigen','DepositoDestino', 'Producto','Impuesto','Persona'=>array('Provincia'),'MovimientoTipo'),
			));  

			
			
			$data = $this->cleanforOutput($data);
			$this->data = $data;
			$output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "list",
				"content" => $data,
				"page_count" =>0
			);
			$this->set($output);
			$this->set("_serialize", array("status", "message","page_count", "content"));
         }
    }
	
	
	private function cleanforOutput($data){
	
        
		$this->loadModel("ComprobanteItem");
		
        foreach($data as &$valor){
            
            $valor["TipoComprobante"]["d_deposito_origen"] =  $valor["DepositoOrigen"]["d_deposito_origen"];
            $valor["TipoComprobante"]["d_deposito_destino"] =  $valor["DepositoDestino"]["d_deposito_destino"];
            $valor['TipoComprobante']['d_producto_default'] = "";
            $valor['TipoComprobante']['codigo_producto_default'] = "";
            $valor['TipoComprobante']['costo_default'] = "";
            $valor['TipoComprobante']['id_moneda_costo_default'] = "";
            $valor['TipoComprobante']['id_unidad_default'] = "";
            $valor['TipoComprobante']['id_iva_default'] = "";
			
			if( isset($valor['Producto']) &&  $valor['TipoComprobante']['id_producto_default']!= null )
			{
                $valor['TipoComprobante']['d_producto_default'] = $valor['Producto']['d_producto'];
                $valor['TipoComprobante']['codigo_producto_default'] = $this->ComprobanteItem->getCodigoFromProducto($valor);
				$valor['TipoComprobante']['costo_default'] = $valor['Producto']['costo'];
				$valor['TipoComprobante']['id_moneda_costo_default'] = $valor['Producto']['id_moneda_costo'];
				
				$valor['TipoComprobante']['id_unidad_default'] = $valor['Producto']['id_unidad'];
				$valor['TipoComprobante']['id_iva_default'] = $valor['Producto']['id_iva'];
			}
			
			if( isset($valor['Persona']) &&  $valor['TipoComprobante']['id_persona_default']!= null )
			{
				$valor['TipoComprobante']['codigo_persona_default'] = $valor['Persona']['codigo']; //EL NO REGISTRADO NO TIENE CODIGO APROPOSITO PARA modificar mas rapido la cotizacion desde el picker de busqueda de ususarios
				
				
                $valor['TipoComprobante']['razon_social_persona_default'] = $valor['Persona']['razon_social'];
				
				$valor['TipoComprobante']['cuit_persona_default'] =   		$valor['Persona']['cuit'];
				$valor['TipoComprobante']['tel_persona_default'] = 	  		$valor['Persona']['tel'];
				$valor['TipoComprobante']['id_tipo_iva_default'] = 	  		$valor['Persona']['id_tipo_iva'];
				$valor['TipoComprobante']['id_moneda_default'] = 	  		$valor['Persona']['id_moneda'];
				
				
				$valor['TipoComprobante']['id_condicion_pago_default'] = 	  	 $valor['Persona']['id_condicion_pago'];
				$valor['TipoComprobante']['id_forma_pago_default'] = 	  	 $valor['Persona']['id_forma_pago'];
				$valor['TipoComprobante']['tiene_cuenta_corriente_default'] =$valor['Persona']['cuenta_corriente'];
				
				$valor['TipoComprobante']['calle_default'] =$valor['Persona']['calle'];
				$valor['TipoComprobante']['numero_calle_default'] =$valor['Persona']['numero_calle'];
				$valor['TipoComprobante']['localidad_default'] =$valor['Persona']['localidad'];
				$valor['TipoComprobante']['ciudad_default'] =$valor['Persona']['ciudad'];
				$valor['TipoComprobante']['d_provincia_default'] =$valor['Persona']['Provincia']['d_provincia'];
			}
			
			if(isset($valor['MovimientoTipo']) && $valor['MovimientoTipo']['id']!= null )
			{
				$valor['TipoComprobante']['d_movimiento_tipo'] = $valor['MovimientoTipo']['d_movimiento_tipo'];
			}
			else
				unset($valor["MovimientoTipo"]);
			
			unset($valor["DepositoOrigen"]);
            unset($valor["DepositoDestino"]);
			unset($valor["Impuesto"]);
			unset($valor["TipoComprobantePersona"]);
			
            
			if(  $valor['TipoComprobante']['id_producto_default']== null )
			{	unset($valor['Producto']);  //BORRO NULOS
			}
			if(  $valor['TipoComprobante']['id_persona_default']== null )
			{	unset($valor['Persona']);  //BORRO NULOS
			}
        }
        
        return $data;
        
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->TipoComprobante->id = $id;
            $this->request->data = $this->TipoComprobante->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('TipoComprobanteCausa');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de TipoComprobanteCausa');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de TipoComprobanteCausa');
        else
            $formBuilder->setTituloForm('Consulta de TipoComprobanteCausa');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_TipoComprobanteCausa', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('cotizacion', 'Cotizaci&oacute;n', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));    
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'TipoComprobanteCausaAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('TipoComprobanteCausaDTipoComprobanteCausa', 'Debe ingresar un nombre');
        validator.validateRequired('TipoComprobanteCausaCotizacion', 'Debe ingresar una cotizaci&oacute;n');


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_TIPO_COMPROBANTE)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            
            try{
                if ($this->TipoComprobante->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "El TipoComprobante ha sido creada exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el Tipo Documento no ha podido ser creada.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => ""
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }

    
    
    
    /**
    * @secured(MODIFICACION_TIPO_COMPROBANTE)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "El Tipo Comprobante ha sido modificado exitosamente";
                     $status = EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
               }
               
               if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, el Tipo Documento no ha podido modificarse.', 'error');
                
            }
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            //$this->{$this->model}->contain('Provincia','Pais');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }

    /**
    * @secured(BAJA_TIPO_COMPROBANTE)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->TipoComprobante->id = $id;
     
       try{
            if ($this->TipoComprobante->delete() ) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Tipo de Documento ha sido eliminado exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
    }
    
    

    
    
    
        /**
        * @secured(CONSULTA_TIPO_COMPROBANTE)
        */
        public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }

        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);

        }




}
?>