<?php

App::uses('PersonasController', 'Controller');


class EmpleadosController extends PersonasController {
    public $name = 'Empleados';
    public $model = 'Persona';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $id_tipo_persona = EnumTipoPersona::Empleado;
  
  
    /**
    * @secured(CONSULTA_EMPLEADO)
    */
    public function index() {
      
           
       
        parent::index();

        
    }
    
    /**
    * @secured(MODIFICACION_EMPLEADO)
    */
    public function abm($mode, $id = null) {
        
        parent::abm($mode,$id);
        
    }

    /**
    * @secured(ABM_EMPLEADO)
    */
    public function view($id) {        
        parent::view($id);
    }

    /**
    * @secured(ADD_EMPLEADO)
    */
    public function add() {
        
        
        
       $this->request->data["Persona"]["id_tipo_persona"] = $this->id_tipo_persona;
        parent::add(); 
        
      
    }
   
   
   /**
    * @secured(MODIFICACION_EMPLEADO)
    */ 
   public function edit($id) {
       
        $this->request->data["Persona"]["id_tipo_persona"] = $this->id_tipo_persona;
        parent::edit($id);      
  }
    
  
    /**
    * @secured(BAJA_EMPLEADO)
    */
    function delete($id) {
        
        parent::delete($id);
        
    }
    
    
         /**
        * @secured(CONSULTA_EMPLEADO)
        */
        public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
            //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);

            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);
        }
        
        
        /**
         * @secured(CONSULTA_EMPLEADO)
         */
        public function existe_codigo($codigo='',$id_persona='',$llamada_interna=0){
        	
        	
        	$dato = parent::existe_codigo($codigo,$id_persona,$llamada_interna);
        	if($llamada_interna == 1)
        		return $dato;
        		else
        			return;
        }
    



   
    
                
                
                
                
    
    
    public function home(){
        parent::home();
    }
    
  
    /**
    * @secured(CONSULTA_EMPLEADO)
    */
    public function existe_cuit(){
        
       parent::existe_cuit();
    }
    
    /**
     * @secured(BTN_EXCEL_EMPLEADOS)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Empleados");
    	
    	
    	
    }
    
}
?>