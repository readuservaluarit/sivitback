<?php

/**
* @secured(CONSULTA_TIPO_COMPROBANTE)
*/
class TiposComprobantePuntoVentaNumeroController extends AppController {
    public $name = 'TiposComprobantePuntoVentaNumero';
    public $model = 'TipoComprobantePuntoVentaNumero';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

 /**
* @secured(CONSULTA_TIPO_COMPROBANTE)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);

        $conditions = array();
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => 10,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

			App::import('Lib', 'FormBuilder');
			$formBuilder = new FormBuilder();
			$formBuilder->setDataListado($this, 'Listado de TIPO_COMPROBANTE', 'Datos de las TIPO_COMPROBANTE', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
			
			//Filters
			$formBuilder->addFilterBeginRow();
			$formBuilder->addFilterInput('nombre', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
			$formBuilder->addFilterEndRow();
			
			//Headers
			$formBuilder->addHeader('Id', 'TIPO_COMPROBANTE.id', "10%");
			$formBuilder->addHeader('Nombre', 'TIPO_COMPROBANTE.d_TIPO_COMPROBANTE', "90%");

			//Fields
			$formBuilder->addField($this->model, 'id');
			$formBuilder->addField($this->model, 'd_TIPO_COMPROBANTE');

			$this->set('abm',$formBuilder);
			$this->render('/FormBuilder/index');
        }else{ // vista json
			$this->PaginatorModificado->settings = $this->paginate; 
			$data = $this->PaginatorModificado->paginate($this->model);
			$page_count = $this->params['paging'][$this->model]['pageCount'];
			
			$output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "list",
				"content" => $data,
				"page_count" =>$page_count
			);
			$this->set($output);
			$this->set("_serialize", array("status", "message","page_count", "content"));
		}
    }

    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->TIPO_COMPROBANTE->id = $id;
            $this->request->data = $this->TIPO_COMPROBANTE->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('TIPO_COMPROBANTE');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de TIPO_COMPROBANTE');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de TIPO_COMPROBANTE');
        else
            $formBuilder->setTituloForm('Consulta de TIPO_COMPROBANTE');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_TIPO_COMPROBANTE', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));  
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'TIPO_COMPROBANTEAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('TIPO_COMPROBANTEDTIPO_COMPROBANTE', 'Debe ingresar un nombre');


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


   

   

    
   
    
     /**
    * @secured(CONSULTA_TIPO_COMPROBANTE)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    } 
    
   private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
      
      $this->set('model',$model);
      $this->set('model_name',$this->model);
      Configure::write('debug',0);
      $this->render($vista);
   
    }  

}
?>