<?php
App::uses('AppModel', 'Model');
App::uses('StockProducto', 'Model');


/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class StockMateriaPrima extends StockProducto {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'stock_producto';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		/*'id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar un id v&aacute;lido',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
		),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	
    
    );

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 

public $belongsTo = array(
        'MateriaPrima' => array(
            'className' => 'MateriaPrima',
            'foreignKey' => 'id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'Deposito' => array(
            'className' => 'Deposito',
            'foreignKey' => 'id_deposito',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        
  );
  
  
  
  
  
	

}
