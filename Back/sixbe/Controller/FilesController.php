<?php
  class FilesController extends AppController {
      
      
      
      
      public function getMaxSizeUpload(){
          
          
           $this->autoRender = false;
           $max_size = ini_get('post_max_size');
           return $max_size;
           
            $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "post_max_size",
            "content" => $max_size
        );
        
        echo json_encode($output);
      }
      
      
      
      
      private function parse_size($size) {
          $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
          $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
          if ($unit) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
          }
          else {
            return round($size);
          }
  }
      
      
  }

?>