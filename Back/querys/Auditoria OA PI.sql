 SELECT auditoria_comprobante.id,comprobante.`nro_comprobante`,comprobante.`fecha_generacion` AS fecha_gene_pi ,usuario.`username`,auditoria_comprobante.`fecha` AS fecha_modific
,comprobante.`fecha_entrega`, orden_armado.`fecha_emision` AS fecha_emision_oa, DATEDIFF(orden_armado.`fecha_emision`,comprobante.`fecha_generacion`) AS demora_en_hacer_oa
 
 ,DATEDIFF(comprobante.`fecha_entrega`,comprobante.`fecha_generacion`) AS dias_gus
  FROM comprobante 
 
 JOIN auditoria_comprobante ON auditoria_comprobante.id_comprobante = comprobante.id
 
 JOIN usuario ON usuario.id = auditoria_comprobante.`id_usuario`
 
 JOIN orden_armado ON orden_armado.`id_pedido_interno` = comprobante.id
 
 WHERE id_tipo_comprobante=202
 
 AND YEAR(fecha)=2017
  AND MONTH(fecha)>7

  
  

 ORDER BY nro_comprobante DESC, fecha DESC
 
 LIMIT 100000000