<?php

/**
 * XLS exportación de consistencia censal
 */



ob_clean();

$tbody = "";

$renglones = $datos_xls;

$subtitulo = "Listado de Consistencia Censal - ". utf8_decode($subtitulo_predio);

$data = array();

foreach($renglones as $renglon){
    
   $tbody .= "<tr>";
   $tbody .= "<td>" .$renglon['ReglaValidacion']['codigo'] . "</td>";
   $tbody .= "<td>" .utf8_decode($renglon['TipoReglaValidacion']['descripcion']) . "</td>";
   $tbody .= "<td>" .utf8_decode($renglon['ReglaValidacion']['mensaje']) . "</td>";
   $tbody .= "<td>" .$renglon['Construccion']['nro_construccion'] . "</td>";
   $tbody .= "<td>" .$renglon['Local']['nro_planta'] . "</td>";
   $tbody .= "<td>" .$renglon['Local']['ident_plano'] . "</td>";
   $tbody .= "<td>" .utf8_decode($renglon['TipoLocal']['descripcion']) . "</td>";
   $tbody .= "</tr>";
}


$html = '

<a style="font-size:18px; font-weight:bold;">'.$subtitulo.'</a>

<table border="0px"  style="width:100%;border:none; table-layout:auto;">

</table> 
 
<table border="1px" cellspacing="1" cellpadding="2" style="width:100%;">
    <tr style="background-color:rgb(224, 225, 229);">
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;">C&oacute;digo</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;">Tipo</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:40%;">Descripci&oacute;n</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">Nro Construcci&oacute;n</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">Nro Planta</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:20%;">Ident Plano</th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;">Tipo Local</th>
    </tr>
    '.$tbody.'
</table>';

echo $html;