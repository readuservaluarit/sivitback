
/*CAMBIOS DE ESTRUCTURAS ENTRE TABLE 
OA -> COMPROBANTE
*/
SELECT TABLE_NAME, COLUMN_NAME , DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_SCHEMA = 'six_test2'  
AND (TABLE_NAME = 'orden_armado' OR TABLE_NAME = 'comprobante' OR TABLE_NAME = 'comprobante_item')
ORDER BY TABLE_NAME DESC



TABLE_NAME        COLUMN_NAME                       DATA_TYPE  
----------------  --------------------------------  -----------
orden_armado      [item.id_producto]id_producto                       int        
orden_armado      [id]id                                int   
     
orden_armado      [fecha_generacion]fecha_emision                     date     
orden_armado      [fecha_cierre]fecha_cierre                      date    


orden_armado      chk_pedido_interno                tinyint   ( )

orden_armado      [VER CALCULADO]fecha_entrega_maxima              datetime  CALCULADO NO VA
orden_armado      [fecha_entrega]fecha_entrega                     datetime   
orden_armado      [VER CALCULADO]fecha_entrega_minima              datetime  CALCULADO NO VA

				  
orden_armado      [item.cantidad]cantidad_armar_planificado        int        
orden_armado      [item.cantidad_cierre]cantidad                          int        
orden_armado      [id_usuario] id_usuario                        int        
orden_armado      [id_estado_comprobante]id_estado                         int        


orden_armado      [id_otro_sistema]id_pedido_interno_fox             bigint     
orden_armado      [observacion]observacion                       varchar    

orden_armado      [genera_movimiento_stock]genera_movimiento_stock           tinyint    
orden_armado      [inspeccion]presion_ensayo                    varchar    

orden_armado      [detalle_tipo_comprobante]id_tipo_orden_armado              tinyint    (DETALLE TIPO COMPROBANTE)
orden_armado      [item.id_comprobante_item_origen]id_pedido_interno                 bigint    (COMPROBANTE_ITEM->ORIGEN)