<?php

/**
* @secured(CONSULTA_PROVINCIA)
*/
class ProvinciasController extends AppController {
    public $name = 'Provincias';
    public $model = 'Provincia';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

/**
* @secured(CONSULTA_PROVINCIA)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
        $d_provincia = strtolower($this->getFromRequestOrSession('Provincia.d_provincia'));
        $id = strtolower($this->getFromRequestOrSession('Pais.id'));
		
        $conditions = array(); 
        if ($d_provincia != "") {
            $conditions = array('LOWER(Provincia.d_provincia) LIKE' => '%' . $d_provincia . '%');
        }
		
		if ($id != "") {
            $conditions = array('LOWER(Pais.id) LIKE' => '%' . $id . '%');
        }
		

        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => 10,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de Provincias', 'Datos de las Provincias', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('nombre', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $d_provincia));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'provincia.id', "10%");
                $formBuilder->addHeader('Nombre', 'provincia.d_provincia', "90%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_provincia');

                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        
        
        
        
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        foreach($data as &$prov){
            
            $prov['Provincia']['d_pais'] = $prov['Pais']['d_pais'];
            
            
        }
        
        
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->Provincia->id = $id;
            $this->request->data = $this->Provincia->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Provincias');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Provincias');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Provincias');
        else
            $formBuilder->setTituloForm('Consulta de Provincias');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_provincia', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));  
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'ProvinciaAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('ProvinciaDProvincia', 'Debe ingresar un nombre');


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_PROVINCIA)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            
            try{
                if ($this->Provincia->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "La Provincia ha sido creada exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,la Provincia no ha podido ser creada.";
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => ""
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }

   
/**
* @secured(MODIFICACION_PROVINCIA)
*/ 
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->Provincia->id = $id;
            
            try{ 
                if ($this->Provincia->saveAll($this->request->data)){
                    
                    $error = EnumError::SUCCESS;
                    $mensaje = "La Provincia ha sido modificado exitosamente";
                    
                  
                }else{
                    $error = EnumError::ERROR;
                    $mensaje = "Ha ocurrido un error, La Provincia no ha podido modificarse."; 
                    
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, La Provincia no ha podido modificarse.', 'error');
                $error = EnumError::ERROR;
                $mensaje = "Ha ocurrido un error, La Provincia no ha podido modificarse.";
                
            }
            
        } 
        
        if($this->RequestHandler->ext != 'json'){
            $this->Session->setFlash($mensaje, $error);
            $this->redirect(array('controller' => 'Provincias', 'action' => 'index'));
       } else{
            
             $output = array(
                            "status" => $error,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
              $this->set($output);
              $this->set("_serialize", array("status", "message", "content"));
        }
    }

    /**
    * @secured(BAJA_PROVINCIA)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->Provincia->id = $id;
     
       try{
            if ($this->Provincia->delete() ) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "La Provincia ha sido eliminada exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }
    
    
     /**
    * @secured(CONSULTA_PROVINCIA)
    */
     public function getModel($vista='default'){
            
            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
           
        } 
        
       private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
          
          $this->set('model',$model);
          $this->set('model_name',$this->model);
          Configure::write('debug',0);
          $this->render($vista);
       
        }  



}
?>