<?php
   



    App::uses('ComprobantesController', 'Controller');
    
 


/**
    * @secured(CONSULTA_FACTURA)
    */
    class FacturasController extends ComprobantesController {

        public $name = EnumController::Facturas;
        public $model = 'Comprobante';
        public $helpers = array ('Session', 'Js');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
        public $requiere_impuestos = 1; //si el comprobante acepta impuesto, esto llama a las funciones de IVA E IMPUESTOS
        public $id_sistema_comprobante = EnumSistema::VENTAS;
        public $type_message = array();

        /**
        * @secured(CONSULTA_FACTURA)
        */
        public function index() 
        {
            
           
            if($this->request->is('ajax'))
                $this->layout = 'ajax';/*sin ";" throw exception ERROR 505*/

            $this->loadModel($this->model);
            //$this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

            $conditions = $this->RecuperoFiltros($this->model);
			
            $array_conditions = array('Usuario','ComprobanteItem'=>array('ComprobanteItemOrigen'=>
								array('Comprobante')),'Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante',
									'ComprobanteImpuesto'=>array('Impuesto'=>array('Sistema','ImpuestoGeografia')),'PuntoVenta','Sucursal'=>array('Provincia','Pais'),'Transportista'); //sucursal

            $this->paginate = array(
         //       'fields'=>array('Comprobante.*'),
                'paginado' =>$this->paginado,
                'contain' =>$array_conditions,
                'conditions' => $conditions,
                'limit' => $this->numrecords,
            	'maxLimit'=>$this->maxLimitRows,
                'page' => $this->getPageNum(),
                'order' => $this->model.'.id desc'
            );  
            
            $this->PaginatorModificado->settings = $this->paginate;
             
                $data = $this->PaginatorModificado->paginate($this->model);
                $page_count = $this->params['paging'][$this->model]['pageCount'];

                foreach($data as &$valor){
					
                    if(isset($valor['TipoComprobante'])){
                        $valor[$this->model]['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                        $valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
                        unset($valor['TipoComprobante']);
                    }
                    
					if(isset($valor['Usuario'])){
                    	$valor[$this->model]['d_usuario'] = $valor['Usuario']['username'];
                    	unset($valor['Usuario']);
                    }
                    
                    if(isset($valor['PuntoVenta'])){
                        $valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
                        unset($valor['PuntoVenta']);
                    }
                    
                    $valor[$this->model]['cotizacion'] =  $this->getCotizacionConMonedaCorriente($valor);
                    
                    if(isset($valor['Moneda'])){
                        $valor[$this->model]['d_moneda'] = 		   $valor['Moneda']['d_moneda'];
                        $valor[$this->model]['d_moneda_simbolo'] = $valor['Moneda']['simbolo'];
                        unset($valor['Moneda']);
                    }

                    if(isset($valor['ComprobanteImpuesto'])){

                        $valor[$this->model]['ComprobanteImpuestoDefault'] =  $this->getImpuestos($valor['ComprobanteImpuesto'],$valor["Comprobante"]["id"],array(EnumSistema::VENTAS),array(),EnumTipoImpuesto::PERCEPCIONES);
                        $valor[$this->model]['ComprobanteImpuestoTotal'] = (string) $this->TotalImpuestos($valor[$this->model]['ComprobanteImpuestoDefault']);

                        $valor[$this->model]['ComprobanteImpuestoIva'] =  $this->getImpuestos($valor['ComprobanteImpuesto'],$valor["Comprobante"]["id"],array(EnumSistema::VENTAS),array(EnumImpuesto::IVANORMAL),EnumTipoImpuesto::IVA_IMPUESTO_VALOR_AGREGADO);
                        $valor[$this->model]['ComprobanteIvaTotal'] = (string) $this->TotalImpuestos($valor[$this->model]['ComprobanteImpuestoIva']);
					}

                    if(isset($valor['EstadoComprobante'])){
                        $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
                        unset($valor['EstadoComprobante']);
                    }
                    
                    if(isset($valor['CondicionPago'])){
                        $valor[$this->model]['d_condicion_pago'] = $valor['CondicionPago']['d_condicion_pago'];
                        unset($valor['CondicionPago']);
                    }

                    if(isset($valor['Transportista'])){
                        $valor[$this->model]['t_razon_social'] = $valor['Transportista']['razon_social'];
                        $valor[$this->model]['t_direccion'] = $valor['Transportista']['calle'].'-'.$valor['Transportista']['numero_calle'].'-'.$valor['Transportista']['localidad'].$valor['Transportista']['ciudad'];
                        unset($valor['Transportista']);
                    }

                    if(isset($valor['Persona'])){
						
                    	if($valor['Persona']['id']== null || $valor['Persona']['id'] == EnumPersonaBase::ClienteNoRegistrado)
                        {    
                            $valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social']; 
                            $valor[$this->model]['id_tipo_iva'] = $valor['Persona']['id_tipo_iva'];
                            $valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
                            $valor[$this->model]['id_lista_precio_defecto_persona'] = $valor['Persona']['id_lista_precio'];
                        }
                        else{
                            $valor[$this->model]['razon_social'] = $valor['Persona']['razon_social'];
                            $valor[$this->model]['id_tipo_iva'] = $valor['Persona']['id_tipo_iva'];
                            $valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
                            $valor[$this->model]['id_lista_precio_defecto_persona'] = $valor['Persona']['id_lista_precio'];
                        }
                        unset($valor['Persona']);
                    }

                    if(isset($valor['DepositoOrigen'])){
                        $valor['Comprobante']['d_deposito_origen'] =$valor['DepositoOrigen']['d_deposito_origen'];
                    }

                    if(isset($valor['DepositoDestino'])){
                        $valor['Comprobante']['d_deposito_destino'] =$valor['DepositoDestino']['d_deposito_destino'];
                    }


                    if(isset($valor['Sucursal'])){
                          $valor['Comprobante']['d_sucursal'] =$valor["Sucursal"]["nombre"]." -  ".$valor["Sucursal"]["razon_social"];
                    }
                    
                    $valor["Comprobante"]["es_importado"] = (string) $this->Comprobante->esImportado($valor['ComprobanteItem']);

                    
                    
                    
                    if($valor["Comprobante"]["es_importado"] == 1)
                        $valor["Comprobante"]["orden_compra_externa"] = implode(",",$this->Comprobante->getOrdenCompraExterna($valor['ComprobanteItem']));
                    else
                        $valor["Comprobante"]["orden_compra_externa"] ="";
                        
                    $this->{$this->model}->formatearFechas($valor);
                    $this->setRowStyle($valor["Comprobante"]);
                    
                    unset($valor['ComprobanteItem']);
                    unset($valor['Sucursal']);
                    unset($valor['ComprobanteImpuesto']);
                    unset($valor['ComprobanteItemOrigen']);
                }
                
                $this->data = $data;

                
                $seteo_form_builder = array("showActionColumn" =>true,"showNewButton" =>false,"showDeleteButton"=>false,"showEditButton"=>false,"showPrintButton"=>true,"showFooter"=>true,"showHeaderListado"=>true,"showXlsButton"=>false);
                
                $this->title_form = "Listado de Facturas";
                $this->preparaHtmLFormBuilder($this,$seteo_form_builder,"");
               
                //Cache::add('index', $data);
                
                $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "list",
                    "content" => $data,
                    "page_count" =>$page_count
                );
                $this->set($output);
                $this->set("_serialize", array("status", "message","page_count", "content"));

        }
		
		
		        /**
         * @secured(CONSULTA_FACTURA)
         */
        public function existe_comprobante()
            {
            parent::existe_comprobante();
            }

       
        /**
    * @secured(CONSULTA_FACTURA)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->Comprobante->id = $id;
            $this->Comprobante->contain();
            $this->request->data = $this->Comprobante->read();
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Factura');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Facturas');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Facturas');
        else
            $formBuilder->setTituloForm('Consulta de Facturas');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('cuit', 'Cuit', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('calle', 'Calle', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  
        $formBuilder->addFormInput('tel', 'Tel&eacute;fono', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('ciudad', 'Ciudad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descuento', 'Descuento', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descripcion', 'Descripcion', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('localidad', 'Localidad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
         
        
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'ClienteAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('ClienteRazonSocial', 'Debe ingresar una Raz&oacute;n Social');
                validator.validateRequired('ClienteCuit', 'Debe ingresar un CUIT');
                validator.validateNumeric('ClienteDescuento', 'El Descuento ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_cuit',
                        'data': 'cuit=' + $('#ClienteCuit').val()+'&id='+$('#ClienteId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('ClienteCuit','El Cuit que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

        /**
        * @secured(ADD_FACTURA)
        */
        public function add(){

            parent::add();    

        }


        /**
        * @secured(MODIFICACION_FACTURA)
        */
        public function edit($id){

           
            parent::edit($id);
            return;    

        }


        /**
        * @secured(BAJA_FACTURA)
        */
        public function delete($id){

            parent::delete($id);    

        }

        /**
        * @secured(BAJA_FACTURA)
        */
        public function deleteItem($id,$externo=1){

            parent::deleteItem($id,$externo);    

        }


        /**
        * @secured(CONSULTA_FACTURA)
        */
        public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }


        /**
        * @secured(REPORTE_FACTURA)
        */
        public function pdfExport($id,$vista=''){


            $this->loadModel("Comprobante");
            $this->loadModel("ComprobantePuntoVentaNumero");
            
            
            
           
            
          try{  
            
            $factura = $this->Comprobante->find('first', array(
                    'conditions' => array('Comprobante.id' => $id),
                    'contain' =>array('TipoComprobante','PuntoVenta')
                ));
            
            
            
            
            $comprobante_punto_venta_numero = $this->ComprobantePuntoVentaNumero->find('first', array(
            		'conditions' => array(
            				'ComprobantePuntoVentaNumero.id_punto_venta' => $factura["Comprobante"]["id_punto_venta"],
            				'ComprobantePuntoVentaNumero.id_tipo_comprobante' => $factura["Comprobante"]["id_tipo_comprobante"]
            		) ,
            		'contain' => false
            ));
            
            

            $comprobantes_relacionados = $this->getComprobantesRelacionados($id);


            if($factura && $comprobante_punto_venta_numero["ComprobantePuntoVentaNumero"]["id_tipo_comprobante_punto_venta_numero"]!=EnumTipoComprobantePuntoVentaNumero::PropioOfflinePreimpreso){



                $remitos_relacionados = array();
                $pedidos_internos_relacionados = array();


                foreach($comprobantes_relacionados as $comprobante){


                    if( isset($comprobante["Comprobante"]) && $comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::Remito)
                        array_push($remitos_relacionados,$comprobante["Comprobante"]["nro_comprobante"]);

                    if( isset($comprobante["Comprobante"]) && $comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::PedidoInterno)
                        array_push($pedidos_internos_relacionados,$comprobante["Comprobante"]["nro_comprobante"]);


                }

                
                
                $this->set('remitos_relacionados',$remitos_relacionados);  
                $this->set('pedidos_internos_relacionados',$pedidos_internos_relacionados);  
                $letra = $factura["TipoComprobante"]["letra"];
                $vista = "factura".$letra;  


                parent::pdfExport($id,$vista);//envio la vista con la letra que corresponde A o B
            }else
            {    
            	
            	//$this->RequestHandler->ext = 'json';
            
            	$salida= array(
            			"status" =>EnumError::ERROR,
            			"message" => "Error: No puede generar el PDF de la Factura ya que es un comprobante preimpreso.",
            			"content" => ""
            	);

            	$this->set('salida',$salida);
            
            	$this->layout = 'json';
            	$this->autoRender = false;
            	$this->render(false);
            	

            }
            
          }catch (Exception $e){
          	
          	$salida= array(
          			"status" =>EnumError::ERROR,
          			"message" => "Error: No puede generar el PDF".$e->getMessage(),
          			"content" => ""
          	);
          	
          	
          	
          	
          	
          	$this->set('salida',$salida);
          	
          	$this->layout = 'json';
          	$this->autoRender = false;
          	$this->render(false);
          	
          	
          }


        }


        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER


            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);
        }


        /*
        public function ObtenerCae($id_comprobante,$id_punto_venta,$recursivo=0){

           $resultado = parent::ObtenerCae($id_comprobante,$id_punto_venta,$recursivo);
            return $resultado;

        }*/



        /**
        * @secured(CONSULTA_FACTURA)
        */
        public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
        {
            parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
        }
        
        public function CalcularImpuestos($id_comprobante,$id_tipo_impuesto='',$error=0,$message=''){
            
            
           $id_tipo_impuesto = EnumTipoImpuesto::PERCEPCIONES; 
          return parent::CalcularImpuestos($id_comprobante,$id_tipo_impuesto);
          
        }
        
        
        
        /**
        * @secured(MODIFICACION_FACTURA)
        */
        public function Anular($id_comprobante){
            
          $this->loadModel("Comprobante");
          $this->loadModel("Asiento");
          $this->loadModel("Modulo");
          
          $output_asiento_revertir = array("id_asiento"=>0);
          $factura = $this->Comprobante->find('first', array(
                    'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                    'contain' => array('ComprobanteValor','Asiento','ChequeEntrada','ComprobanteItem')
						));
                
		  $ds = $this->Comprobante->getdatasource(); 
		  $output_asiento_revertir = array("id_asiento"=>0);

            if(
                $this->Comprobante->getEstadoGrabado($id_comprobante)!= EnumEstadoComprobante::Anulado &&
                count($this->getRecibosRelacionados($id_comprobante))==0    &&
                $factura &&
                !$factura["Comprobante"]["cae"] >0
            ){ //si la factura cumple esto entonces puedo anularla
              
            //TODO: si tiene stock lo revierto
             try{
                 $ds->begin();
                //le clavo el estado ANULADO
                $this->Comprobante->updateAll(
											  array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,
											  'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
											  array('Comprobante.id' => $id_comprobante) );  
                                                            
             
                if($factura["Comprobante"]["comprobante_genera_asiento"] == 1)
                            $output_asiento_revertir = $this->Asiento->revertir($factura["Asiento"]["id"]); 
                              /*Se debe revertir el asiento del comprobante*/                                
                            
                            
                            
                            
                  $habilitado  = $this->Modulo->estaHabilitado(EnumModulo::STOCK);
                            
                  
                  if($habilitado == 1){
                  	
                  	$id_tipo_movimiento_defecto = $this->Comprobante->getTipoMovimientoDefecto($id_comprobante);
                  	
                  	$id_movimiento = $this->Comprobante->HizoMovimientoStock($id_comprobante,$id_tipo_movimiento_defecto);
                  	
                  	
                  	if( $id_movimiento > 0 ){//si hizo movimiento
                  		$this->loadModel("Movimiento");
                  		$this->Movimiento->Anular($id_movimiento);
                  	}
                  }
                  
                          
                            
                    $ds->commit(); 
                    
                    
                    $this->abrirComprobantesAsociados($factura);//abro los Pedidos si es que los cerre
                    
                    $tipo = EnumError::SUCCESS; 
                    $mensaje = "El Comprobante se anulo correctamente ";       
                    
                    $factura = $this->Comprobante->find('first', array(
                    		'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                    		'contain' => false
                    ));
                    
                    $this->Auditar($id_comprobante,$factura);
                    
             }catch(Exception $e){ 
                
                $ds->rollback();
                $tipo = EnumError::ERROR;
                $mensaje = "No es posible anular el Comprobante ERROR:".$e->getMessage();
                
              }     
            }else{
                $tipo = EnumError::ERROR;
                $mensaje = "No es posible anular el Comprobante ya que o ya ha sido ANULADO o esta presente en algun RECIBO o ya posee CAE"."\n";
                
                $recibos_relacionados = $this->getRecibosRelacionados($id_comprobante);
                if(count($recibos_relacionados)>0){
                   foreach($recibos_relacionados as $recibo){
                    $mensaje.= "&bull; Recibo Nro: ".$recibo["Comprobante"]["nro_comprobante"]."\n";
                   } 
                }
            }
            $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                "id_asiento"=>$output_asiento_revertir["id_asiento"]
                );      
            echo json_encode($output);
            die();
        }
        
        
    public function ProcesarLote($array_lote_comprobantes){
        
        
        
        
        
        
    }   
    
    
    private function setRowStyle(&$row){
            
    
    $row["forecolor"] = EnumRowStyleOk::forecolor;        
    $row["backcolor"] = EnumRowStyleOk::backcolor;        
            
            
            
    }    

	
    
    
    /**
     * @secured(BTN_EXCEL_FACTURAS)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Facturas de Venta");
    	
    	
    	
    }
    
    public function generaAsiento($id_comprobante,&$message){
    	
    	$this->loadModel("ComprobantePuntoVentaNumero");
    	
    	
    	if(isset($this->request->data["Comprobante"]["id_punto_venta"]) && $this->request->data["Comprobante"]["id_punto_venta"] !="")
    		$id_punto_venta = $this->request->data["Comprobante"]["id_punto_venta"];
    	else 
    		$id_punto_venta = $this->request->data["Comprobante"]["id_punto_venta_fiscal"];
    	
    		
    	
    	$comprobante_punto_venta_numero = $this->ComprobantePuntoVentaNumero->find('first', array(
    			'conditions' => array(
    					'ComprobantePuntoVentaNumero.id_punto_venta' => $id_punto_venta,
    					'ComprobantePuntoVentaNumero.id_tipo_comprobante' => $this->request->data["Comprobante"]["id_tipo_comprobante"]
    			) ,
    			'contain' => false
    	));
    	
    	
    	if($comprobante_punto_venta_numero && 
    			$comprobante_punto_venta_numero["ComprobantePuntoVentaNumero"]["id_tipo_comprobante_punto_venta_numero"] == EnumTipoComprobantePuntoVentaNumero::PropioElectronicoOfflineExportacion
    			
    			||  $comprobante_punto_venta_numero["ComprobantePuntoVentaNumero"]["id_tipo_comprobante_punto_venta_numero"] == EnumTipoComprobantePuntoVentaNumero::PropioNumeradorSistemaEDITABLE
    			){
    				parent::generaAsiento($id_comprobante, $message);
    				return;
    	}//solo genero asiento en estos casos, sino se genera cuando se obtiene el CAE
    	
    }
    
    
    public function getMovimientoFondosFormaPago(){
    	
    	$this->loadModel("Comprobante");
    	
    	$fecha_inicio = strtolower($this->getFromRequestOrSession('Comprobante.fecha_contable'));
    	$fecha_fin = strtolower($this->getFromRequestOrSession('Comprobante.fecha_contable_hasta'));
    	$conciliado = strtolower($this->getFromRequestOrSession('Cheque.conciliado'));
    	$id_cuenta_bancaria = strtolower($this->getFromRequestOrSession('ComprobanteValor.id_cuenta_bancaria'));
    	
    	$conditions = array();
    	array_push($conditions, array('NOT'=>array('Comprobante.id_estado_comprobante' =>array(EnumEstadoComprobante::Anulado,EnumEstadoComprobante::Cancelado)) ));
    	array_push($conditions, array('Comprobante.definitivo'=> 1 ));
    	array_push($conditions, array('Comprobante.id_tipo_comprobante'=> array(EnumTipoComprobante::FacturaCreditoA,EnumTipoComprobante::FacturaCreditoB,EnumTipoComprobante::FacturaCreditoC,EnumTipoComprobante::FacturaA,EnumTipoComprobante::FacturaB,EnumTipoComprobante::FacturaC) ));
    	
    	if($fecha_inicio!=""){
    		array_push($conditions, array('Comprobante.fecha_contable >='=>$fecha_inicio));
    		array_push($this->array_filter_names,array("Fecha Contable Dsd:"=>$this->Comprobante->formatDate($fecha_inicio)));
    	}
    	
    	
    	if($fecha_fin!=""){
    		array_push($conditions, array('Comprobante.fecha_contable <='=>$fecha_fin));
    		array_push($this->array_filter_names,array("Fecha Contable Hta:"=>$this->Comprobante->formatDate($fecha_fin)));
    	}
    	
    	$comprobante_ventas = $this->Comprobante->find('all', array(

    			'conditions' => array($conditions),
    			'fields' => array('SUM(ROUND(Comprobante.total_comprobante,2)) as total','FormaPago.id','CONCAT(FormaPago.codigo,"-",IFNULL(FormaPago.interes,"0"),"%") as d_forma_pago'),
    			'group' => array('Comprobante.id_forma_pago'),
    			'contain' =>array('FormaPago','TipoComprobante')
    	));
    	
    	$array_final = $comprobante_ventas;
    	
    	$total_haber = 0;
    	$total_haber_conciliado = 0;
    	$total_debe = 0;
    	$total_debe_conciliado = 0;
    	
    	$array_data = array();
    	
    	foreach($array_final as &$valor){
    		
    		$valor["Comprobante"]["total_comprobante"] = $valor[0]["total"];
    		$valor["Comprobante"]["d_forma_pago"] = $valor[0]["d_forma_pago"];
    		$total_debe = $total_debe + $valor["Comprobante"]["total_comprobante"];
    		$valor["Comprobante"]["descripcion"] = "Ingreso";

    		unset($valor["FormaPago"]);
    		unset($valor["TipoComprobante"]);
    		unset($valor[0]);
    		
    		array_push($array_data, $valor);	
    	}

    	$resultado["ResultadosComprobante"]["total_debe"] =(string) $total_debe;//total de los comprobantes
    	$resultado["ResultadosComprobante"]["total_haber"] =(string) 0;//total de los comprobantes
    	$resultado["ResultadosComprobante"]["saldo"] =(string) ($total_debe + 0);//total de los comprobantes
    	
    	$status = EnumError::SUCCESS;
    	$this->data = $array_data;
    	
    	$output = array(
    			"status" =>EnumError::SUCCESS,
    			"message" => "list",
    			"content" => $array_data,
    			"resultset_content"=>$resultado,
    			"page_count" =>0
    	);

    	$this->set($output);
    	$this->set("_serialize", array("status", "message","page_count", "content","resultset_content"));

    }
    
    
   


    }
?>