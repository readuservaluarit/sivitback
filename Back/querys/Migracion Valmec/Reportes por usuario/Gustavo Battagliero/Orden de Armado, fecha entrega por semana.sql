
SELECT  comprobante.`fecha_entrega` ,producto.`codigo`,producto.`d_producto`,SUM(comprobante_item.`cantidad`) total,WEEK(comprobante.fecha_entrega) semana_marzo,
GROUP_CONCAT(comprobante.`nro_comprobante`) nros_oa,

GROUP_CONCAT(pedido.`nro_comprobante`) nros_pedido



 FROM comprobante_item

JOIN comprobante ON comprobante.id = comprobante_item.`id_comprobante`

JOIN producto ON producto.id = comprobante_item.`id_producto`

JOIN comprobante_item pedido_item ON pedido_item.id = comprobante_item.`id_comprobante_item_origen`
JOIN comprobante pedido ON pedido.id = pedido_item.`id_comprobante`

WHERE comprobante.id_tipo_comprobante = 104
AND comprobante.`id_estado_comprobante` NOT IN(116,2)


AND comprobante.`fecha_entrega` >='2019-01-01'
GROUP BY comprobante.`fecha_entrega`,producto.id


ORDER BY fecha_entrega ASC 

LIMIT 100000000