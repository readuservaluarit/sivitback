

/*VISTA*/
SELECT * FROM facturas_proceso_tristan

/*ver query*/

SELECT 'FAC',
LPAD(f.id_punto_venta, 5, '0') AS punto_venta,
LPAD(f.nro_factura_afip, 8, '0') AS nro_comprobante,
DATE_FORMAT(f.fecha,'%d%m%Y') AS fecha,
tf.d_tipo_factura AS Letra,
c.cuit,
CASE WHEN f.iva21 IS NULL 
       THEN LPAD(f.subtotal, 15, '0') 
       ELSE LPAD(0.00, 15, '0')
END AS Exento,
CASE WHEN f.iva21 IS  NULL 
       THEN LPAD(0.00, 15, '0')
       ELSE LPAD(f.subtotal, 15, '0') 
END AS Gravado,
LPAD(f.iva21, 15, '0') AS iva,
LPAD(f.perc_ib, 15, '0') AS perc_iibb
FROM factura f
INNER JOIN tipo_factura tf  ON tf.id = f.id_tipo_factura
INNER JOIN cliente c ON c.id = f.id_cliente
WHERE f.id_dato_empresa = 6 AND NOT ISNULL(f.`cae`  ) AND f.nro_factura > 608
ORDER BY f.nro_factura_afip