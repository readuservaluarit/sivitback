<?php
/**
* @secured(CONSULTA_ASIENTO)
*/
class AsientosModeloController extends AppController {
    public $name = 'AsientosModelo';
    public $model = 'AsientoModelo';
    public $sub_model = 'AsientoModeloCuentaContable';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    
    private function RecuperaFiltros(){

            $conditions = array();
		   
            $id = strtolower($this->getFromRequestOrSession('AsientoModelo.id'));
            $codigo = strtolower($this->getFromRequestOrSession('AsientoModelo.codigo'));
			$d_asiento_modelo = strtolower($this->getFromRequestOrSession('AsientoModelo.d_asiento_modelo'));
            $id_cuenta_contable = strtolower($this->getFromRequestOrSession('AsientoModelo.id_cuenta_contable'));
            $codigo_cuenta_contable_desde = strtolower($this->getFromRequestOrSession('AsientoModelo.codigo_cuenta_contable_desde'));
            $codigo_cuenta_contable_hasta = strtolower($this->getFromRequestOrSession('AsientoModelo.codigo_cuenta_contable_hasta'));
            $id_tipo_asiento = strtolower($this->getFromRequestOrSession('AsientoModelo.id_tipo_asiento'));
            
            if ($id != "") 
                array_push($conditions, array('AsientoModelo.id ' => $id ));
            
            if ($id_tipo_asiento != "") 
                array_push($conditions, array('AsientoModelo.id_tipo_asiento ' => $id_tipo_asiento));

			 if($d_asiento_modelo!="")
				 array_push($conditions, array('LOWER(AsientoModelo.d_asiento_modelo) LIKE' => '%' . $d_asiento_modelo  . '%')); 
			
            if ($id_cuenta_contable != "") 
                array_push($conditions, array('AsientoModelCuentaContable.id_cuenta_contable ' => $id_cuenta_contable));

            if ($codigo_cuenta_contable_desde != "") 
                array_push($conditions, array('CuentaContable.codigo >=' => $codigo_cuenta_contable_desde));

            if ($codigo_cuenta_contable_hasta != "") 
                array_push($conditions, array('CuentaContable.codigo <=' => $codigo_cuenta_contable_hasta));                    


			
			return $conditions;
        }
    
/**
* @secured(CONSULTA_ASIENTO)
*/
    public function index() 
    {    
         if($this->request->is('ajax'))
                $this->layout = 'ajax';

            $this->loadModel($this->model);
            $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);


            //Recuperacion de Filtros
            $conditions = $this->RecuperaFiltros();


            $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
                //'contain' => array('Asientosucursal'),
                'conditions' => $conditions,
                'limit' => $this->numrecords,
                'page' => $this->getPageNum()
               // ,'order'=>array("AsientoModelo.id asc")
            );

            if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de Asientos', 'Datos de las Asientos', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));

                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_ASIENTO', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();

                //Headers
                $formBuilder->addHeader('Id', 'AsientoModelo.id', "10%");
                $formBuilder->addHeader('Nombre', 'AsientoModelo.d_ASIENTO', "50%");
                $formBuilder->addHeader('Nombre', 'AsientoModelo.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_ASIENTO');
                $formBuilder->addField($this->model, 'cotizacion');

                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
            }else
            { // vista json
                $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
                $page_count = $this->params['paging'][$this->model]['pageCount'];


                foreach($data as &$record)
                {
					$record["AsientoModelo"]["d_tipo_asiento"] =       $record["TipoAsiento"]["d_tipo_asiento"];
                    unset($record["Moneda"]);
                    unset($record["TipoAsiento"]);
                    unset($record["AsientoEfecto"]);
                    unset($record["ClaseAsiento"]);
                    //unset($record["EstadoAsiento"]);
                }       

                $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "list",
                    "content" => $data,
                    "page_count" =>$page_count
                );
                $this->set($output);
                $this->set("_serialize", array("status", "message","page_count", "content"));
            }
    }

      /**
        * @secured(ADD_ASIENTO)
        */
        public function add() 
        {
            if (!$this->request->is('get'))
            {
                $this->loadModel($this->model);
                $id_cat = '';

                //$dataSource = ConnectionManager::getDataSource('default');
                /* Lo hago transaccional */
                //$dataSource = $this->getDataSource();
                //$dataSource->begin();

                try{
                    //if ($this->model($this->request->data, array('deep' => true))){
                    if ($this->{$this->model}->saveAll($this->request->data)){
                        //if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
                        //$dataSource->commit();
                        $mensaje = "El Asiento Modelo ha sido creado exitosamente";
                        $status = EnumError::SUCCESS;
                        $id_cat = $this->{$this->model}->id;

                    }
                    else{
                        //$dataSource->rollback();
                        $errores = $this->{$this->model}->validationErrors;
                        $errores_string = "";
                        foreach ($errores as $error)
                        {    //recorro los errores y armo el mensaje
                            $errores_string.= "&bull; ".$error[0]."\n";
                        }
                        $mensaje = $errores_string;
                        $status = EnumError::ERROR;     
                    }
                }
                catch(Exception $e)
                {
                    //$dataSource->rollback();
                    $mensaje = "Ha ocurrido un error,el Asiento Modelo no ha podido ser creado.";
                    $status = EnumError::ERROR;
                }

                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => "",
                    "id_add" =>$id_cat
                );
                //si es json muestro esto
                if($this->RequestHandler->ext == 'json')
                { 
                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content" ,"id_add"));
                }
                else
                {
                    $this->Session->setFlash($mensaje, $status);
                    $this->redirect(array('action' => 'index'));
                }     
            }
        }

        /**
        * @secured(MODIFICACION_ASIENTO)
        */  
        public function edit($id) {


            if (!$this->request->is('get')){

                $this->loadModel($this->model);
                $this->{$this->model}->id = $id;

                try{ 
                    if ($this->{$this->model}->saveAll($this->request->data)){
                        $mensaje =  "El Asiento Modelo ha sido modificado exitosamente";
                        $status = EnumError::SUCCESS;


                    }else{   //si hubo error recupero los errores de los models

                        $errores = $this->{$this->model}->validationErrors;
                        $errores_string = "";
                        foreach ($errores as $error){ //recorro los errores y armo el mensaje
                            $errores_string.= "&bull; ".$error[0]."\n";

                        }
                        $mensaje = $errores_string;
                        $status = EnumError::ERROR; 
                    }

                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 

                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));

                    } 
                }catch(Exception $e){
                    $this->Session->setFlash('Ha ocurrido un error, el Asiento Modelo no ha podido modificarse.', 'error');

                }

            }else{ //si me pide algun dato me debe mandar el id
                if($this->RequestHandler->ext == 'json'){ 

                    $this->{$this->model}->id = $id;
                    //$this->{$this->model}->contain('Provincia','Pais');
                    $this->request->data = $this->{$this->model}->read();          

                    $output = array(
                        "status" =>EnumError::SUCCESS,
                        "message" => "list",
                        "content" => $this->request->data
                    );   

                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content")); 
                }else{
                    $this->redirect(array('action' => 'abm', 'M', $id));
                }


            } 

            if($this->RequestHandler->ext != 'json')
                $this->redirect(array('action' => 'abm', 'M', $id));
        }
    
	/**
    * @secured(CONSULTA_ASIENTO)
    */ 
	public function getModel($vista='default')
	{    
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    }

    private function editforView($model,$vista)
	{  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);    
    }
}
?>