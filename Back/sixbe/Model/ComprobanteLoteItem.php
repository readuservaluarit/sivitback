<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class ComprobanteLoteItem extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'comprobante_lote_item';
    
    
    

    public $actsAs = array('Containable');
    
   
    
    
  
   
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'El id del item no puede ser vacio',
            
            ),
        ),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);
    
    
  
    
    public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     
    
    );
    
    }

	//The Associations below have been created with all possible keys, those that are not needed can be removed

public $belongsTo = array(
        'Comprobante' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ComprobanteLote' => array(
            'className' => 'ComprobanteLote',
            'foreignKey' => 'id_comprobante_lote',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ComprobanteLoteEjecucion' => array(
            'className' => 'ComprobanteLoteEjecucion',
            'foreignKey' => 'id_comprobante_lote_ejecucion',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        
        
       
      
 );
 
 
 
 
 
 
 
   
  
    
 
    
  
  

}
