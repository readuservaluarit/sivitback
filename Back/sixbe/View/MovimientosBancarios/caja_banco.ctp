<?php   


/**
 * PDF exportación de Orden Compra
 */

App::import('Vendor','tcpdf/tcpdf'); 
App::import('Vendor','tcpdf/tcpdf_barcodes_1d'); 



foreach($datos_pdf["ComprobanteValor"] as $valor){
        
        if($valor["id_valor"] == EnumValor::DEPOSITOS){
            
            $nro_cuenta = $valor["CuentaBancaria"]["nro_cuenta"];
            $d_banco_sucursal = $valor["CuentaBancaria"]["BancoSucursal"]["d_banco_sucursal"];
            $d_banco = $valor["CuentaBancaria"]["BancoSucursal"]["Banco"]["d_banco"];
        }
           
}


// create new PDF document

$aviso_anulado = '';
    if($datos_pdf['Comprobante']['id_estado_comprobante'] == EnumEstadoComprobante::Anulado)
        $aviso_anulado = ' ANULADO';
    if($datos_pdf['Comprobante']['id_estado_comprobante'] == EnumEstadoComprobante::Abierto)
        $aviso_anulado = ' (PROFORMA)';     

$fecha_contable = new DateTime($datos_pdf['Comprobante']['fecha_contable']);


$html = '<table width="100%" border="0">
  <tr>
    <td colspan="3" align="center" valign="middle"><strong> MOVIMIENTO BANCARIO '.$aviso_anulado.' ('.$datos_pdf["DetalleTipoComprobante"]["d_detalle_tipo_comprobante"].')</strong></td>
  </tr>
  <tr align="left" valign="middle">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td ><strong>Fecha:</strong>'.$fecha_contable->format('d-m-Y').'</td>
  </tr>
  <tr align="left" valign="middle">
    <td colspan="3"><strong>Nro Comprobante:</strong> '.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'</td>
    
  </tr>
  <tr align="left" valign="middle">
    <td colspan="3"><strong>Banco:</strong> '.$d_banco.'</td>
    
  </tr>
  <tr align="left" valign="middle">
    <td colspan="3"><strong>Sucursal:</strong> '.$d_banco_sucursal.'</td>
   
  </tr>
  <tr align="left" valign="middle">
    <td colspan="3"><strong>Cuenta:</strong> '.$nro_cuenta.'</td>
   
  </tr>
   <tr align="left" valign="middle">
   <td align="left"><strong>Observaci&oacute;n: </strong>'.$datos_pdf["Comprobante"]["observacion"].'</td>
   
    </tr>
</table>';





$html;


$tabla_cheques ='<table width="100%" border="0">
                      <tr>
                        <td colspan="2">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td colspan="2">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="5" align="left" valign="middle" style="border-bottom:2px solid black;"><strong>CHEQUES EN CARTERA</strong></td>
                      </tr>
                      <tr >
                        <td ><strong>Numero</strong></td>
                        <td ><strong>Fecha</strong></td>
                        <td ><strong>Banco</strong></td>
                        <td ><strong>Sucursal</strong></td>
                        <td ><strong>Monto</strong></td>
                      </tr>';
                      

$tabla_efectivo ='<table width="50%" border="0">
                      <tr>
                        <td colspan="2">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td colspan="2">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="5" align="left" valign="middle" style="border-bottom:2px solid black;"><strong>EFECTIVO</strong></td>
                      </tr>
                      <tr >
                        <td colspan="1" width="50" height="23"><strong>Desc.</strong></td>
                        <td colspan="4" width="50"><strong>Monto</strong></td>
                        
                      </tr>';

                      
$total_cheques = 0;
$total_efectivo = 0;
$hay_cheques = 0;
$hay_efectivo = 0;
$comprobante =  new Comprobante();
foreach($datos_pdf["ComprobanteValor"] as $valor){
    
       if($valor["id_valor"] == EnumValor::CHEQUE){
            $hay_cheques = 1;
            $nro_cheque = $valor["Cheque"]["nro_cheque"];
            $fecha_cheque = $valor["Cheque"]["fecha_cheque"];
            $d_banco = $valor["Cheque"]["Banco"]["d_banco"];
            $d_banco_sucursal = $valor["Cheque"]["BancoSucursal"]["d_banco_sucursal"];
            $monto = $valor["Cheque"]["monto"];
            $total_cheques += $monto;
            
            $tabla_cheques.='
                      <tr>
                        <td style="border-top:2px solid black;">'.$nro_cheque.'</td>
                        <td style="border-top:2px solid black;">'.$comprobante->formatDate($fecha_cheque).'</td>
                        <td style="border-top:2px solid black;">'.$d_banco.'</td>
                        <td style="border-top:2px solid black;">'.$d_banco_sucursal.'</td>
                        <td style="border-top:2px solid black;">'.money_format('%!n',round($monto,2)).'</td>
                      </tr>
                      
                      ';
            
        }elseif($valor["id_valor"]==EnumValor::EFECTIVO){
               $hay_efectivo = 1;
               $total_efectivo +=$valor["monto"];
               
               $tabla_efectivo.='
                      <tr>
                        <td  colspan="1"  style="border-top:2px solid black;">$</td>
                        <td colspan="4" style="border-top:2px solid black;">'.round($valor["monto"],2).'</td>
                        
                      </tr>
                      
                      ';
            
        }
    
}

$tabla_cheques.='<tr>
                        <td height="23"><strong>Total:</strong></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td> '.money_format('%!n',$total_cheques).'</td>
                      </tr>
                    </table>';
                    
$tabla_efectivo.='<tr>
                        <td colspan="1" ><strong>Total:</strong></td>
                        
                        <td colspan="4"> '.money_format('%!n',$total_efectivo).'</td>
                      </tr>
                    </table>';
                    
                    

$footer = '<table width="100%" border="1">
  <tr>
    <td><strong>Total del Movimiento: </strong>'.money_format('%!n',$datos_pdf["Comprobante"]["total_comprobante"]).'</td>
  </tr>
</table>
';

if($hay_cheques == 1)
$html =$html.$tabla_cheques;

if($hay_efectivo == 1)
$html =$html.$tabla_efectivo;

$html =$html.$footer;

class MyTCPDF extends TCPDF{

    public $datos_header;
    public $datos_footer;
    public $bar_code;
    public $observaciones;
    
    public function set_datos_header($datos_empresa){
        $this->datos_header = $datos_empresa; 

    }
    public function set_datos_footer($datos_empresa_footer){
        $this->datos_footer = $datos_empresa_footer; 

    }
     public function set_barcode($bar_code){
        $this->bar_code = $bar_code; 

    }
    
    public function set_observaciones($observaciones){
        $this->observaciones = $observaciones; 

    }
  public function Header(){
     
        
        
     //$this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_header, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
  }
  
  public function Footer(){
     
        
        
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_footer, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->observaciones, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
     
     $style['text'] = true;
     $style['border'] = true;
     $style['align'] = 'C';
     $this->write1DBarcode($this->bar_code, 'I25', '40', '', '', 9, 0.4, $style, 'N');
      
      //$this->writeHTMLCell($w = 0, $h = 0, $x = '200', $y = '', $this->bar_code, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
      //$this->Cell(0, 20, $this->bar_code , 0, false, 'C', 0, '', 0, false, 'T', 'M');
      $this->Cell(0, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
      
  }
  
}

$pdf = new MyTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->set_datos_header($html);


ob_clean();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistema SIV');
$pdf->SetTitle('Movimiento Bancario');
$pdf->SetSubject('Movimiento Bancario');
//$pdf->SetKeywords('');



// set default header data
$logo = 'img/header_factura.jpg';
$title = "";
$subtitle = ""; 
//$pdf->SetHeaderData($logo, 190, $title, $subtitle, array(0,0,0), array(0,104,128));
$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 10, 20);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
/*$lg = Array();
$lg['a_meta_charset'] = 'ISO-8859-1';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';
*/
$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set default font subsetting mode
//$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

  
  
   
$pdf->writeHTML($html, true, false, true, false, true);


// QRCODE,L : QR-CODE Low error correction
// set style for barcode
$style = array(
    'border' => 2,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

//$pdf->Text(20, 25, 'QRCODE L');

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->SetBarcode(date("Y-m-d H:i:s", time()));
ob_clean();
$pdf->Output($datos_pdf["TipoComprobante"]["abreviado_tipo_comprobante"].'_'.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
