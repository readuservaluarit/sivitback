/*
SQLyog Ultimate v9.63 
MySQL - 5.5.38-0+wheezy1 : Database - dbarcangel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbarcangel` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dbarcangel`;

/*Table structure for table `comprobantes_impuesto` */

CREATE TABLE `comprobantes_impuesto` (
  `Id_Comprobante` int(11) NOT NULL DEFAULT '0',
  `Id_Moneda` int(11) NOT NULL DEFAULT '0',
  `Id_Impuesto` int(11) NOT NULL DEFAULT '0',
  `Id_Categoria_Impuesto` int(11) NOT NULL DEFAULT '0',
  `Id_Tasa_Impuesto` int(11) NOT NULL DEFAULT '0',
  `Id_Jurisdiccion` int(11) NOT NULL DEFAULT '0',
  `Id_Item_Asiento_Contable` int(11) NOT NULL DEFAULT '0',
  `Base_Imponible` decimal(15,2) NOT NULL DEFAULT '0.00',
  `Importe_Impuesto` decimal(15,2) NOT NULL DEFAULT '0.00',
  `Tasa_Impuesto` decimal(6,2) DEFAULT NULL,
  `Porcentaje_Reduccion` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`Id_Comprobante`,`Id_Impuesto`,`Id_Categoria_Impuesto`,`Id_Tasa_Impuesto`,`Id_Jurisdiccion`,`Id_Moneda`),
  KEY `XIF1045Comprobantes_Impuesto` (`Id_Jurisdiccion`),
  KEY `XIF912Comprobantes_Impuesto` (`Id_Comprobante`),
  KEY `XIF909Comprobantes_Impuesto` (`Id_Moneda`),
  KEY `XIF903Comprobantes_Impuesto` (`Id_Impuesto`),
  KEY `XIF468Comprobantes_Impuesto` (`Id_Tasa_Impuesto`),
  KEY `XIF6Comprobantes_Impuesto` (`Id_Categoria_Impuesto`),
  KEY `XIF8Comprobantes_Impuesto` (`Id_Item_Asiento_Contable`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
