<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

 ?>

<style>
  .center {text-align: center; margin-left: auto; margin-right: auto; margin-bottom: auto; margin-top: auto;}
</style>
<title><?php echo $name; ?></title>
<body>
  <div class="hero-unit center">
    <h2>Ha ocurrido un error <small><font face="Tahoma" color="red">Error 404</font></small></h2>
    <br />
    <p>La página a la que intenta acceder no se encuentra en el servidor.</p>
    <strong><?php echo $url;?></strong>
    <p><b>Por favor verifique la dirección a la que intenta ingresar.</b></p>
    <?php 
        echo $this->Html->link("<i class='icon-home icon-white'></i> Ir al Inicio" , array('controller' => 'Usuarios', 'action' => 'home'), array('class' => 'btn btn-large btn-primary', 'escape' => false)); 
    ?>
  </div>
  <br />
  
<?php
if (Configure::read('debug') > 0 ):
    echo $this->element('exception_stack_trace');
endif;
?>



