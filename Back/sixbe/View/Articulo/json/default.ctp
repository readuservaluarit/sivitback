<?php
		include(APP.'View'.DS.'Articulo'.DS.'json'.DS.'articulo.ctp');
        
        App::import('Model','Comprobante');
  		App::import('Lib','SecurityManager');
		$comprobante = new Comprobante();
		$stock = $comprobante->getActivoModulo(EnumModulo::STOCK);
		
		 $sm = new SecurityManager(); 
  
		$talle = $sm->permiso_particular("CONSULTA_TALLE",AuthComponent::user('username'))>0;
		$color = $sm->permiso_particular("CONSULTA_COLOR",AuthComponent::user('username'))>0;
		
		
        $model["d_producto_tipo"]["show_grid"] = 1;
        $model["d_producto_tipo"]["header_display"] = "Tipo";
        $model["d_producto_tipo"]["order"] = "2";
        $model["d_producto_tipo"]["width"] = "10";
        $model["d_producto_tipo"]["text_colour"] = "#000000";
        
        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "C&oacute;digo";
        $model["codigo"]["order"] = "3";
        $model["codigo"]["width"] = "12";
        $model["codigo"]["text_colour"] = "#000000";
        
        $model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "Descripci&oacute;n";
        $model["d_producto"]["order"] = "4";
        $model["d_producto"]["width"] = "35";
        $model["d_producto"]["text_colour"] = "#0000FF";
        
        $model["d_unidad"]["show_grid"] = 1;
        $model["d_unidad"]["header_display"] = "Unidad";
        $model["d_unidad"]["order"] = "5";
        $model["d_unidad"]["width"] = "5";
        $model["d_unidad"]["text_colour"] = "#0000DD";
        
   
     		
		$model["precio_minimo_producto"]["show_grid"] = 1;
        $model["precio_minimo_producto"]["header_display"] = "Precio m&iacute;nimo";
        $model["precio_minimo_producto"]["order"] = "9";
        $model["precio_minimo_producto"]["width"] = "8";
        $model["precio_minimo_producto"]["text_colour"] = "#CC0033";
        
        $model["peso"]["show_grid"] = 1;
        $model["peso"]["header_display"] = "Peso";
        $model["peso"]["order"] = "10";
        $model["peso"]["width"] = "5";
        $model["peso"]["text_colour"] = "#AA0077";
        
            if($stock == 1){
        
        	$model["stock"]["show_grid"] = 1;
        	$model["stock"]["header_display"] = "Stock";
        	$model["stock"]["width"] = "10";
        	$model["stock"]["order"] = "11";
        	$model["stock"]["text_colour"] = "#0000FF";
        }
        
        
        if($talle){
        
        	$model["d_talle"]["show_grid"] = 1;
        	$model["d_talle"]["header_display"] = "Talle";
        	$model["d_talle"]["width"] = "10";
        	$model["d_talle"]["order"] = "12";
        	$model["d_talle"]["text_colour"] = "#0000FF";
        }
        
        
        if($color){
        
        	$model["d_color"]["show_grid"] = 1;
        	$model["d_color"]["header_display"] = "Color";
        	$model["d_color"]["width"] = "10";
        	$model["d_color"]["order"] = "13";
        	$model["d_color"]["text_colour"] = "#0000FF";
        }
        
		
	
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Articulo",
                    "content" => $model
                );
        
          echo json_encode($output);
?>