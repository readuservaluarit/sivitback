<?php


class MateriasPrimaDepositosController extends AppController {
    public $name = 'MateriasPrimaDepositos';
    public $model = 'MateriaPrimaDeposito';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
  
  
    /**
    * @secured(CONSULTA_COMPONENTE_PRODUCTO)
    */
    public function index($id_producto = null) {
        
        
        
        $this->loadModel($this->model);
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $id_componente = $this->getFromRequestOrSession('MateriaPrimaDeposito.id_componente');
    
        $limite_registros_pagina =  $this->getFromRequestOrSession('MateriaPrimaDeposito.limite_registros_pagina');
       
        if($limite_registros_pagina !='' && is_numeric($limite_registros_pagina))
            $this->numrecords = (int) $limite_registros_pagina;
            
        
        $conditions = array(); 
        
        if($id_componente!='')
            array_push($conditions, array('MateriaPrimaDeposito.id_componente' => $id_componente)); 
    
    
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' => array('Deposito','MateriaPrima'=>array('Moneda')),
            'conditions' => $conditions,
            'limit' => 1000, //estoo es solo para traer todos las materias primas
            'page' => $this->getPageNum()
        );
        
  
          
    // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$prodcom){
            
   
            $prodcom['MateriaPrimaDeposito']['d_materia_prima'] = $prodcom['MateriaPrima']['d_materia_prima'];
            $prodcom['MateriaPrimaDeposito']['materia_prima_codigo'] = $prodcom['MateriaPrima']['codigo'];
           
            unset($prodcom['MateriaPrima']);
            
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     
        
        
        
        
    //fin vista json
        
    }
    
    /**
    * @secured(ABM_USUARIOS)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->Componente->id = $id;
            $this->Componente->contain();
            $this->request->data = $this->Componente->read();
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Componente');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Componentes');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Componentes');
        else
            $formBuilder->setTituloForm('Consulta de Componente');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('codigo', 'C&oacute;digo', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('descripcion', 'Descripci&oacute;n', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('costo', 'Costo', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('stock', 'Stock', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('stock_minimo', 'Stock M&iacute;nimo', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        
       
         
        
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'ComponenteAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('ComponenteCodigo', 'Debe ingresar un C&oacute;digo');
                validator.validateRequired('ComponenteDescripcion', 'Debe ingresar una descripcion');
                validator.validateNumeric('ComponenteStock', 'El stock ingresado debe ser num&eacute;rico');
                validator.validateNumeric('ComponenteStockMinimo', 'El stock ingresado debe ser num&eacute;rico');
                validator.validateRequired('ComponenteCosto', 'El costo ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_codigo',
                        'data': 'codigo=' + $('#ComponenteCodigo').val()+'&id='+$('#ComponenteId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('ComponenteCodigo','El C&oacute;digo que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

    /**
    * @secured(ABM_USUARIOS)
    */
    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

     /**
    * @secured(ALTA_COMPONENTE_PRODUCTO)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            
            try{
                if ($this->MateriaPrimaDeposito->saveAll($this->request->data, array('deep' => false))){
                    $mensaje = "La Materia Prima ha sido asignada exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $mensaje = "Ha ocurrido un error,la Materia Prima no ha podido ser asignada.";
                  
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,la Materia Prima no ha podido ser asiganda.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => ""
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    
      /**
    * @secured(MODIFICAR_COMPONENTE_PRODUCTO)
    */
   function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "La Materia Prima ha sido modificado exitosamente";
                     $status = EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
               }
               
               if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, la Materia Prima no ha podido modificarse.', 'error');
                
            }
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            //$this->{$this->model}->contain('Unidad');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
  
  /**
    * @secured(BORRAR_COMPONENTE_PRODUCTO)
  */
   function delete($id) {
        
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->MateriaPrimaDeposito->id = $id;
       try{
            if ($this->MateriaPrimaDeposito->delete()) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Deposito ha sido eliminado exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }


    
    
    
    public function home(){
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
    
  
    
  
    
     /**
    * @secured(CONSULTA_COMPONENTE)
    */
  public function getModel($vista="default"){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model = $this->editforView($model);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => $this->model,
                    "content" => $model
                );
        
          echo json_encode($output);
          die();
    }
    
    
    private function editforView($model){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        
        
        //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
        
        
        // CONFIG  CON MODE DISPLAYED CELLS
        $model["id"]["show_grid"] = 1;
        $model["id"]["header_display"] = "Nro. Mat. Prima";
          // $model["nro_comprobante"]["width"] = "10";
        $model["id"]["minimun_width"] = "10"; //REstriccion mas de esto no se puede achicar
        $model["id"]["order"] = "1";
        $model["id"]["text_colour"] = "#000000";
        
        $model["codigo"]["show_grid"] = 1;
        $model[""][""]["header_display"] = "C&oacute;digo";
        // $model["razon_social"]["width"] = "50";
        $model[""][""]["order"] = "2";
        $model[""][""]["text_colour"] = "#000000";
        

        $model["d_materia_prima"]["show_grid"] = 1;
        $model["d_materia_prima"]["header_display"] = "Descripci&oacute;n";
          // $model["fecha_generacion"]["width"] = "10";
        $model["d_materia_prima"]["order"] = "3";
        $model["d_materia_prima"]["text_colour"] = "#0000FF";
        
        
         
       
        return $model;
    }  
    
}
?>