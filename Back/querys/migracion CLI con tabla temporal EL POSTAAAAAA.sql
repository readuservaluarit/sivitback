SET FOREIGN_KEY_CHECKS = 0;
DELETE FROM usuario;
DELETE FROM funcion;
DELETE FROM funcion_rol;
DELETE FROM rol;

SET FOREIGN_KEY_CHECKS = 1;


SET FOREIGN_KEY_CHECKS = 0;

INSERT INTO usuario
SELECT * FROM valuarit_valmec.usuario;

INSERT INTO funcion
SELECT * FROM valuarit_valmec.funcion;


INSERT INTO funcion_rol
SELECT * FROM valuarit_valmec.funcion_rol;


INSERT INTO rol
SELECT * FROM valuarit_valmec.rol;

SET FOREIGN_KEY_CHECKS = 1;