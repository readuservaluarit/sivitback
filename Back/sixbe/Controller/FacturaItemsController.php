<?php

App::uses('ComprobanteItemsController', 'Controller');

class FacturaItemsController extends ComprobanteItemsController {
    
    public $name = EnumController::FacturaItems;
    public $model = 'ComprobanteItem';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
   /**
    * @secured(CONSULTA_FACTURA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->loadModel("Comprobante");
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model); 
        $id_comprobante_filtro = $this->getFromRequestOrSession($this->model.'.id_comprobante');
        $filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');
        
        
        /*
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
         'joins' => array(
            array(
                'table' => 'producto_tipo',
                'alias' => 'ProductoTipo',
                'type' => 'LEFT',
                'conditions' => array(
                    'ProductoTipo.id = Producto.id_producto_tipo'
                )
                
            )),
            'contain' =>array('Comprobante'=>array('PuntoVenta','EstadoComprobante','Moneda'),'Producto'=>array('Unidad'),'ComprobanteItemOrigen'=>array("Comprobante")),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order'=>$this->ComprobanteItem->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_comprobante'))
        );
        */
        
        $this->ComprobanteItem->paginateAgrupado($this,$filtro_agrupado,$conditions,
        		$this->numrecords,$this->getPageNum(),
        		array('Unidad','Comprobante'=>array('EstadoComprobante','Moneda','PuntoVenta','Persona','TipoComprobante'),
        				'Producto'=>array('ProductoTipo','Unidad'),'ComprobanteItemOrigen'=>array("Comprobante")),$this->ComprobanteItem->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_comprobante'))); // AGREGADO PARA EL ORDEN DE ISCO
        		
        		
        
        
        
      if($this->RequestHandler->ext != 'json'){  
      
        //vista formBuilder
    }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        
        
        foreach($data as $key=>&$producto)
		{         
			
			
			
			switch($filtro_agrupado){
				
				
				
				case "":
				case EnumTipoFiltroComprobanteItem::individual:
				case EnumTipoFiltroComprobanteItem::agrupadaporComprobante:
					
		                 $producto["ComprobanteItem"]["d_producto"] = $producto["Producto"]["d_producto"];
		                 //$producto["ComprobanteItem"]["id_lista_precio"] = $producto["ComprobanteItem"]["id_lista_precio"];
		                 $producto["ComprobanteItem"]["codigo"] = $this->ComprobanteItem->getCodigoFromProducto($producto);
		                 
		                 $this->ComprobanteItem->CalculaUnidad($producto);
		                 
						 //$producto["ComprobanteItem"]["total"] = (string) $this->{$this->model}->getTotalItem($producto);
		                 $producto["ComprobanteItem"]["pto_nro_comprobante"] = $this->ComprobanteItem->GetNumberComprobante($producto["Comprobante"]["PuntoVenta"]["numero"],$producto["Comprobante"]["nro_comprobante"]);
		                 $producto["ComprobanteItem"]["nro_comprobante"] = $producto["Comprobante"]["nro_comprobante"];
		                 $producto["ComprobanteItem"]["d_estado_comprobante"] = $producto["Comprobante"]["EstadoComprobante"]["d_estado_comprobante"];
		                 $producto["ComprobanteItem"]["moneda_simbolo"] = $producto["Comprobante"]["Moneda"]["simbolo"];
		                 $producto["ComprobanteItem"]["id_moneda"] = $producto["Comprobante"]["Moneda"]["id"];
		                 $producto["ComprobanteItem"]["valor_moneda"] = $producto["Comprobante"]["valor_moneda"];
		                 
		                 //a terminar
						 $array["id"] = $producto["ComprobanteItem"]["id_comprobante_item_origen"];
						 $producto["ComprobanteItem"]["total_facturado_origen"] = "0";
						 $producto["ComprobanteItem"]["total_notas_credito_origen"] = "0";
						 $producto["ComprobanteItem"]["total_pendiente_a_facturar_origen"] = "0";
						 $producto["ComprobanteItem"]["total_pendiente_a_remitir_origen"] = "0";
						 $producto["ComprobanteItem"]["total_remitido_origen"] = "0";
		                         
		                 if($id_comprobante_filtro !="" && $producto["ComprobanteItemOrigen"]["id"]>0){//si se manda el filtro de COmprobante y ademas tiene un origen calculo cuanto le falta
		                     $total_item_comprobante_origen = $producto["ComprobanteItemOrigen"]["cantidad"];
		                     $total_facturas_origen = $this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItemOrigen"],$this->Comprobante->getTiposComprobanteController(EnumController::Facturas));
		                     $total_remitidos_origen = $this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItemOrigen"],$this->Comprobante->getTiposComprobanteController(EnumController::Remitos));
		                     
		                     /*ej: PI 10 -> sin id FC 2  total_pendiente_a_facturar_origen = 10
								   PI 10 -> con id FC 2(ABIERTO) total_pendiente_a_facturar_origen = 8
								   PI 10 -> con id FC 2(CERRADO) total_pendiente_a_facturar_origen = 8 */
		                     $producto["ComprobanteItem"]["total_pendiente_a_facturar_origen"] =  
											(string)  number_format((float) ($total_item_comprobante_origen  /*10*/ 
											- $this->ComprobanteItem->GetCantidadItemProcesados($total_facturas_origen))/*2*/, $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');;
		                     $producto["ComprobanteItem"]["total_pendiente_a_remitir_origen"] =  
											(string)  number_format((float) ($total_item_comprobante_origen 
											- $this->ComprobanteItem->GetCantidadItemProcesados($total_remitidos_origen)), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		                     
		                     $producto["ComprobanteItem"]["orden_compra_externa"] = $producto["ComprobanteItemOrigen"]["Comprobante"]["orden_compra_externa"];
		                     
		                                    
		                 }else{
		                     
		                     
		                     $producto["ComprobanteItem"]["orden_compra_externa"] = "";
		                 }
		                  
		                  
		                 $notas_credito = $this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItem"],$this->Comprobante->getTiposComprobanteController(EnumController::Notas));
		                 
		                 $remitidos = $this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItem"],$this->Comprobante->getTiposComprobanteController(EnumController::Remitos));
		                 
		                 $producto["ComprobanteItem"]["total_remitido"] = (string)  number_format((float) ($this->ComprobanteItem->GetCantidadItemProcesados($remitidos)), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		
		                 $producto["ComprobanteItem"]["total_nota_credito"] = (string)  number_format((float)$this->ComprobanteItem->GetCantidadItemProcesados($notas_credito),$producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		                
		                 //$producto["ComprobanteItem"]["total_posible_nota_credito"] = (string)  number_format((float) ($producto["ComprobanteItem"]["cantidad"]- ( $producto["ComprobanteItem"]["total_notas_credito_origen"])), 2, '.', '');
		                 
		                 
		                 $producto["ComprobanteItem"]["cantidad_pendiente_nota_credito"] = (string)  number_format((float) ($producto["ComprobanteItem"]["cantidad"]
																						- ( $producto["ComprobanteItem"]["total_nota_credito"])), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		                 
		                // $producto["ComprobanteItem"]["total_nota_credito_posibles"] =  (string) $producto["ComprobanteItem"]["total_posible_nota_credito"];
		                 
		                 $producto["ComprobanteItem"]["cantidad_pendiente_a_remitir"] = (string) number_format((float)( $producto["ComprobanteItem"]["cantidad"] 
																						- ( $producto["ComprobanteItem"]["total_remitido"])), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		                 //}
		                 
		                 $this->FiltrarCamposOnTheFly($data,$producto,$key);
		                 unset($producto["Producto"]);
		                 unset($producto["Comprobante"]);
		                 unset($producto["ComprobanteItemOrigen"]);
                 
                break;
                
				case EnumTipoFiltroComprobanteItem::agrupadaporPersona:
					
					$producto["ComprobanteItem"]["total"] = $producto[0]["total"] ;
					$producto["ComprobanteItem"]["razon_social"] = $producto["Persona"]["razon_social"];
					
				break;
				
			}
				
				
        }
        
       $this->data = $this->prepararRespuesta($data,$page_count);
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $this->data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    //fin vista json 
    }
    
    
     /**
    * @secured(CONSULTA_FACTURA)
    */
    public function getModel($vista='default')
	{    
        $modelx = parent::getModelCamposDefault();
        $modelx=  parent::setDefaultFieldsForView($modelx);//deja todo en 0 y no mostrar
        $this->editforView($modelx,$vista);//esta funcion edita y agrega campos para la vista
    }
    
    private function editforView($modelx,$vista)
	{  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
		$this->set('model',$modelx);
      Configure::write('debug',0);
      $this->render($vista);
    }
    
    
    /**
     * @secured(CONSULTA_FACTURA)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	$this->paginado =0;
    	
    	$filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');
    	
    	
    	
    	switch($filtro_agrupado){
    		
    		case EnumTipoFiltroComprobanteItem::individual:
    		case "":
    			$view = "resumen_cabecera";
    			break;
    		case EnumTipoFiltroComprobanteItem::agrupadaporComprobante:
    			$view="agrupadaporcomprobante";
    			break;
    		case EnumTipoFiltroComprobanteItem::agrupadaporPersona:
    			$view="agrupadaporpersona";
    			break;
    			
    	}
    	
    	$this->ExportarExcel($view,$metodo,"Listado de Facturas");
    	
    	
    	
    }
    
    
   
}
?>