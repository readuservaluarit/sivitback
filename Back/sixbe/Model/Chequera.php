<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class Chequera extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'chequera';

    public $actsAs = array('Containable');
    
    

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'cheque_desde' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar el comienzo de la cartera',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 public $belongsTo = array(
        'CuentaBancaria' => array(
            'className' => 'CuentaBancaria',
            'foreignKey' => 'id_cuenta_bancaria',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        'EstadoChequera' => array(
            'className' => 'EstadoChequera',
            'foreignKey' => 'id_estado_chequera',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
         'CuentaContable' => array(
            'className' => 'CuentaContable',
            'foreignKey' => 'id_cuenta_contable',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        )
        
);

 public $hasMany = array(
        'Cheque' => array(
            'className' => 'Cheque',
            'foreignKey' => 'id_chequera',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
        );
        
        
 public function ExisteChequeraDisponible(){
            
     $chequera = $this->find(
        'count', array(
        'conditions' => array('Chequera.numero_actual < Chequera.cheque_hasta',
                              "Chequera.id_estado_chequera"=>EnumEstadoChequera::Activa),
        'contain' =>false
                    ));
       return $chequera;
}


}
