<?php
/**
 * Annotations Manager
 *
 */
 App::import('Controller');
                            
class AnnotationsManager 
{

    static function getClassAnnotations($class)
    {        
        
        //Reflection de la Clase
        $r = new ReflectionClass($class);
        //Obtengo las annotations de la clase
        $annotations = $r->getDocComment();  
        $annotations = str_replace('/', '', $annotations);
        $annotations = str_replace('*', '', $annotations);
        return $annotations;
    }
    
	static function getMethodAnnotations($class, $function)
	{		
		
		//Reflection de la Clase
		$r = new ReflectionClass($class);
		//Obtengo el metodo
		$m = $r->getMethod($function);
		//Obtengo las annotations del metodo
		$annotations = $m->getDocComment();  
		$annotations = str_replace('/', '', $annotations);
		$annotations = str_replace('*', '', $annotations);
		return $annotations;
	}
}
