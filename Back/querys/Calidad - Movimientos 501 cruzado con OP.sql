SELECT 
  movimiento.id AS id_mov,movimiento.cantidad,fecha AS fecha_movimiento
  ,
  producto.`codigo`,
  producto.`d_producto`,
  producto.`peso`
  
FROM
  movimiento 

LEFT JOIN comprobante ON comprobante.id = movimiento.`id_comprobante`
LEFT JOIN comprobante_item ON comprobante.id = comprobante_item.`id_comprobante` 
JOIN producto ON producto.id =   comprobante_item.`id_producto`
  
WHERE id_movimiento_tipo = 501 
  AND YEAR(fecha) = 2019 
   AND
 comprobante_item.`id_detalle_tipo_comprobante` = 69
 
 LIMIT 1000000000