<?php        
        
        App::uses('ProductoCodigo', 'Model');  
        $producto_codigo_obj = new ProductoCodigo();
        $producto_codigo_obj->setVirtualFieldsForView();
     
      
        $producto_codigo_obj->SetFieldsToView($producto_codigo_obj->virtual_fields_view,$model);
        
        
        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "C&oacute;digo";
        $model["codigo"]["order"] = "1";
		$model["codigo"]["width"] = "20"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["codigo"]["text_colour"] = "#000000";
		$model["codigo"]["read_only"] = "0";
       
        $model["d_producto_codigo"]["show_grid"] = 1;
        $model["d_producto_codigo"]["header_display"] = "Descripci&oacute;n";
        $model["d_producto_codigo"]["order"] = "2";
		$model["d_producto_codigo"]["width"] = "40"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["d_producto_codigo"]["text_colour"] = "#0000FF";
		$model["d_producto_codigo"]["read_only"] = "0";
         
         
         
        $model["codigo_barra"]["show_grid"] = 1;
        $model["codigo_barra"]["header_display"] = "Cod. Barra";
        $model["codigo_barra"]["order"] = "3";
		$model["codigo_barra"]["width"] = "40"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["codigo_barra"]["text_colour"] = "#0000FF";
		$model["codigo_barra"]["read_only"] = "0";
         
    
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => $model_name,
                    "content" => $model
                );
        echo json_encode($output);
?>