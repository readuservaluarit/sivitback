<?php

App::uses('ComprobanteItemsController', 'Controller');

class ReclamosVentaItemsController extends ComprobanteItemsController {
    
    public $name = EnumController::ReclamosVentaItems;
    public $model = 'ComprobanteItem';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

     /**
    * @secured(CONSULTA_RECLAMO_VENTA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model);
        
        /*
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        */
        
        $filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');
        $id_moneda_expresion = $this->getFromRequestOrSession('Comprobante.id_moneda_expresion');
        
        
        //$filtro_agrupado = 3; //MP antes tenia uno 1 si manda un /index plano me agrupaba los items y no me traie los campos q necesito para el form PIMasterDetail0
        $this->ComprobanteItem->paginateAgrupado($this,$filtro_agrupado,$conditions,
        		$this->numrecords,$this->getPageNum(),
        		array('Unidad','Comprobante'=>array('EstadoComprobante','Moneda','PuntoVenta','Persona','TipoComprobante'),
        				'Producto'=>array('ProductoTipo'),'ComprobanteItemOrigen'=>array("Comprobante"=>array("PuntoVenta")),'ComprobanteOrigen'=>array("PuntoVenta")),$this->ComprobanteItem->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_comprobante')),$id_moneda_expresion); // AGREGADO PARA EL ORDEN DE ISCO
        		
        		
        		
    

                        
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        
        foreach($data as $key=>&$producto ){
        	
        	
        	
        	
        	switch($filtro_agrupado){
        		
        		
        		
        		case "":
        		case EnumTipoFiltroComprobanteItem::individual:
        		case EnumTipoFiltroComprobanteItem::agrupadaporComprobante:
			
						if(isset($producto["Comprobante"]["PuntoVenta"]["numero"]))
							$producto["ComprobanteItem"]["pto_nro_comprobante"] = $this->ComprobanteItem->GetNumberComprobante($producto["Comprobante"]["PuntoVenta"]["numero"],$producto["Comprobante"]["nro_comprobante"]);
						
							
							
						if(isset($producto["ComprobanteItemOrigen"]["id"]) && $producto["ComprobanteItemOrigen"]["id"]>0)
							$producto["ComprobanteItem"]["pto_nro_comprobante_origen"] = $this->ComprobanteItem->GetNumberComprobante($producto["ComprobanteItemOrigen"]["Comprobante"]["PuntoVenta"]["numero"],$producto["ComprobanteItemOrigen"]["Comprobante"]["nro_comprobante"]);
							
								
							
							if(isset($producto["ComprobanteOrigen"]["id"]) && $producto["ComprobanteOrigen"]["id"]>0)
									$producto["ComprobanteItem"]["pto_nro_comprobante_origen"] = $this->ComprobanteItem->GetNumberComprobante($producto["ComprobanteOrigen"]["PuntoVenta"]["numero"],$producto["ComprobanteOrigen"]["nro_comprobante"]);
									
									
								
							
							if(isset($producto["Comprobante"])&& isset($producto["Comprobante"]["Moneda"]) )
						{
						    $producto["ComprobanteItem"]["id_moneda"] = $producto["Comprobante"]["Moneda"]["id"];
							$producto["ComprobanteItem"]["moneda_simbolo"] = $producto["Comprobante"]["Moneda"]["simbolo"];
							$producto["ComprobanteItem"]["valor_moneda"] = $producto["Comprobante"]["valor_moneda"];
						}
			
						$producto["ComprobanteItem"]["referencia_comprobante_externo"] = $producto["Comprobante"]["referencia_comprobante_externo"];
						
						$producto["ComprobanteItem"]["codigo"] = $this->ComprobanteItem->getCodigoFromProducto($producto);
						$producto["ComprobanteItem"]["d_producto"] = $producto["Producto"]["d_producto"];
						
						
						
						if(isset($producto["Comprobante"])&& isset($producto["Comprobante"]["Persona"]) ){
							$producto["ComprobanteItem"]["razon_social"] = $producto["Comprobante"]["Persona"]["razon_social"];
							$producto["ComprobanteItem"]["persona_tel"]  =  $producto["Comprobante"]["Persona"]["tel"];
						}
						
						
						//$producto["ComprobanteItem"]["total"] = (string) $this->{$this->model}->getTotalItem($producto);
						
						
						$fecha = new DateTime($producto["Comprobante"]["fecha_generacion"]);
						$producto["ComprobanteItem"]["fecha_generacion"] = $fecha->format('d-m-Y');;
						
						
						$pedidos =$this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItem"], $this->Comprobante->getTiposComprobanteController(EnumController::PedidosInternos) );
						
						
						$producto["ComprobanteItem"]["total_pedido"] = (string) number_format((float)($this->ComprobanteItem->GetCantidadItemProcesados($pedidos)), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
						
						$producto["ComprobanteItem"]["cantidad_pendiente_a_pedido"] = (string) number_format( (float)($producto["ComprobanteItem"]["cantidad"] -
								$producto["ComprobanteItem"]["total_pedido"]), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
						
						
						$this->ComprobanteItem->CalculaUnidad($producto);
			
						unset($producto["Producto"]);
						unset($producto["Comprobante"]);
						unset($producto["ComprobanteItemOrigen"]);
						$this->FiltrarCamposOnTheFly($data,$producto,$key);
						
			    break;
			    
        		case EnumTipoFiltroComprobanteItem::agrupadaporPersona:
        			
        			$producto["ComprobanteItem"]["total"] = $producto[0]["total"] ;
        			$producto["ComprobanteItem"]["razon_social"] = $producto["Persona"]["razon_social"];
        			
        		break;
        		
        	}
        }
        
        $this->data = $this->prepararRespuesta($data,$page_count);
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $this->data,
            "page_count" =>$page_count
        );
        
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
    //fin vista json
    }
    
    
     /**
    * @secured(CONSULTA_RECLAMO_VENTA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);
    }
    
    
    public function prepararRespuesta($data,&$page_count){
    	
    	
    	
    	$filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');//dice como mostrar los items
    	$array_id_comprobantes = array();
    	
    	
    	
    	if($filtro_agrupado == EnumTipoFiltroComprobanteItem::agrupadaporComprobante){ //si es la vista agrupada tomo los filtros de los items y agrupo en memoria por id_comprobante
    		
    		
    		
    		foreach($data as $key=>$valor){
    			if(in_array($valor["ComprobanteItem"]["id_comprobante"],$array_id_comprobantes))
    				unset($data[$key]);
    				else
    					array_push($array_id_comprobantes,$valor["ComprobanteItem"]["id_comprobante"]);
    					
    		}
    		
    		
    		
    		
    		
    		
    	}
    	
    	return parent::prepararRespuesta($data,$page_count);
    }
    
    
    /**
     * @secured(CONSULTA_RECLAMO_VENTA)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	$this->paginado =0;
    	
    	$filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');
    	
    	
    	
    	switch($filtro_agrupado){
    		
    		case EnumTipoFiltroComprobanteItem::individual:
    		case "":
    			$view = "resumen_cabecera";
    			break;
    		case EnumTipoFiltroComprobanteItem::agrupadaporComprobante:
    			$view="agrupadaporcomprobante";
    			break;
    		case EnumTipoFiltroComprobanteItem::agrupadaporPersona:
    			$view="agrupadaporpersona";
    			break;
    			
    	}
    	
    	$this->ExportarExcel($view,$metodo,"Listado de Reclamos");
    	
    	
    	
    }
    

    
   
    
}
?>