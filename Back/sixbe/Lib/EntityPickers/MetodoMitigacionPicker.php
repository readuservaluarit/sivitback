<?php

class MetodoMitigacionPicker extends EntityPicker{
    
    public $model = "MetodoMitigacion";
    
    public $pickerTitle = "Selección de M&eacute;todo de Mitigaci&oacute;n";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "descripcion", "field" => "descripcion", "width" => "80%", "keyOrder" => "descripcion")
                                );

    public function getPickerConditions($busqueda){
        
        return array('MetodoMitigacion.descripcion LIKE' => '%' . $busqueda['bus_unico'] . '%');
    }

}