<?php

/**
* @secured(CONSULTA_TARJETA_CREDITO)
*/
class TarjetasCreditoController extends AppController {
    public $name = 'TarjetasCredito';
    public $model = 'TarjetaCredito';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
/**
* @secured(CONSULTA_TARJETA_CREDITO)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        
        
        $propia = strtolower($this->getFromRequestOrSession('TarjetaCredito.propia'));
        $activo = $this->getFromRequestOrSession('TarjetaCredito.activo');
        $id_tipo_tarjeta_credito = $this->getFromRequestOrSession('TarjetaCredito.id_tipo_tarjeta_credito');
        
        $conditions = array(); 
      
        
        
        
        if ($activo!= "") {
        	array_push($conditions,array('TarjetaCredito.activo =' =>  $activo));
        }
        
   
        
        if ($propia != "") { 
            array_push($conditions, array('TarjetaCredito.propia =' =>  $propia ));
        }
        
        
        if ($id_tipo_tarjeta_credito!= "") {
        	array_push($conditions, array('TarjetaCredito.id_tipo_tarjeta_credito' =>  $id_tipo_tarjeta_credito));
        }
        
        

        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
        	'contain'=>array('CuentaBancaria'=>array('BancoSucursal','TipoCuentaBancaria'),'TipoTarjetaCredito','CuentaContable','Moneda'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        

			$this->PaginatorModificado->settings = $this->paginate; 
			$data = $this->PaginatorModificado->paginate($this->model);
			$page_count = $this->params['paging'][$this->model]['pageCount'];
			
			
			foreach($data as &$dato){
				
				if(isset($dato['Pais']))
					$dato['TarjetaCredito']['d_pais']=$dato['Pais']['d_pais'];
				
					
			    if(isset($dato['TipoTarjetaCredito']))
						$dato['TarjetaCredito']['d_tipo_tarjeta_credito']=$dato['TipoTarjetaCredito']['d_tipo_tarjeta_credito'];
			    
			
					
				if(isset($dato['CuentaBancaria']['BancoSucursal']) && isset($dato["CuentaBancaria"]['BancoSucursal']['d_banco_sucursal'])){
					$dato['TarjetaCredito']['d_banco_sucursal'] = $dato["CuentaBancaria"]['BancoSucursal']['d_banco_sucursal'];
					$dato['TarjetaCredito']['d_cuenta_bancaria'] = $dato["CuentaBancaria"]['nro_cuenta'].' '.$dato['TarjetaCredito']['d_banco_sucursal'];

				}
				
					if(isset($dato['CuentaBancaria']['TipoCuentaBancaria']) && isset($dato['CuentaBancaria']['TipoCuentaBancaria']['d_tipo_cuenta_bancaria']))
						$dato['TarjetaCredito']['d_tipo_cuenta_bancaria']=$dato['CuentaBancaria']['TipoCuentaBancaria']['d_tipo_cuenta_bancaria'];       
				
				
				if(isset($dato['Moneda']))
				{
					//MP: Trae todo de la tabla Moneda (Cotizacion del Dia)
					$dato['TarjetaCredito']['d_moneda']     		  =$dato['Moneda']['d_moneda'];       
					$dato['TarjetaCredito']['valor_moneda']  		  =$dato['Moneda']['cotizacion'];     
					$dato['TarjetaCredito']['d_simbolo']     		  =$dato['Moneda']['simbolo'];       
					$dato['TarjetaCredito']['d_abreviacion_afip']     =$dato['Moneda']['abreviacion_afip'];     
					$dato['TarjetaCredito']['d_simbolo_internacional']=$dato['Moneda']['simbolo_internacional'];       
				}
				
				
				if(isset($dato['CuentaContable'])){
                    $dato['TarjetaCredito']['d_cuenta_contable'] = $dato['CuentaContable']['codigo'].' '.$dato['CuentaContable']['d_cuenta_contable'];    
					$dato['TarjetaCredito']['codigo_cuenta_contable'] = $dato['CuentaContable']['codigo'];    
				}else{
                    
                    $dato['TarjetaCredito']['d_cuenta_contable'] = '';
                    $dato['TarjetaCredito']['codigo_cuenta_contable'] = '';
                }	
                
                if($dato["TarjetaCredito"]["propia"] == 1)
                    $dato["TarjetaCredito"]["d_propia"] = "Si";
                else    
                    $dato["TarjetaCredito"]["d_propia"] = "No";
                    
				unset($dato['Pais']);
				unset($dato['Provincia']);
				unset($dato['BancoSucursal']);
				unset($dato['TipoCuentaBancaria']);
				unset($dato['Persona']);
				//unset($dato['Moneda']);
				unset($dato['CuentaContable']);
	   
			}
			
			$output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "list",
				"content" => $data,
				"page_count" =>$page_count
			);
			$this->set($output);
			$this->set("_serialize", array("status", "message","page_count", "content"));
			
		
    }


   

    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_TARJETA_CREDITO)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_add = '';
            
            try{
                if ($this->TarjetaCredito->saveAll($this->request->data, array('deep' => true))){
                	$id_add = $this->TarjetaCredito->id;
                    $mensaje = "La Tarjeta de Credito ha sido creada exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,la Tarjeta de Credito no ha podido ser creada.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" =>$id_add
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }

    /**
    * @secured(MODIFICACION_TARJETA_CREDITO)
    */
     
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "La Tarjeta de Credito ha sido modificada exitosamente";
                     $status = EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
               }
               
               if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, la Cuenta Bancaria no ha podido modificarse.', 'error');
                
            }
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            //$this->{$this->model}->contain('Provincia','Pais');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
    
        /**
    * @secured(CONSULTA_TARJETA_CREDITO)
    */
   public function getModel($vista='default')
    {    
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    }
    
    
    
    private function editforView($model,$vista)
    {  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);    
        
    }
    
    /**
    * @secured(BAJA_TARJETA_CREDITO,READONLY_PROTECTED)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->TarjetaCredito->id = $id;
     
       try{
       	
       	if ($this->TarjetaCredito->delete() ) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "La Tarjeta de Credito ha sido eliminada exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }
    
    
    
    
  



}
?>