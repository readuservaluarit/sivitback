<?php
    //echo $this->Html->script('gmaps_js/gmaps.js');
?>

<div class='well' style="width:600px">
    <div class="block-border" style="margin-top: 0px;">
        <div class="block-header">
            <legend><h4><?php echo __("Georeferenciación"); ?></h4></legend>
        </div>
        <label><?php echo __($address); ?></label>
        <div class="block-content">
           <div id="map" class="well" style=" height: 500px;"></div>
        </div>
    </div> 
</div>
<div style="width:100%;">
    <button id="btnCancelar" class="btn btn-primary pull-left" type="button" title="Cancelar" name="btnCancelar" onclick='cancelar();'>Cancelar</button>
    <button id="btnAceptar" class="btn btn-primary pull-right" type="button" title="Confirmar" name="btnAceptar" onclick='aceptar();'>Confirmar</button>
</div>


<script type="text/javascript">

var geoPosicionar = false;

if($('#<?php echo $x_id;?>').val().trim()=='')
    geoPosicionar = true;
    
    
var x = $('#<?php echo $x_id;?>').val();
var y = $('#<?php echo $y_id;?>').val();

function cancelar(){
    
     $('#<?php echo $x_id; ?>').val('');
     $('#<?php echo $y_id; ?>').val('');
     $.fancybox.close(); return false;
}

function aceptar(){
    
     $('#<?php echo $x_id; ?>').val(x);
     $('#<?php echo $y_id; ?>').val(y);
     $.fancybox.close(); return false;
}



$(document).ready(function(){
      
      //alert(x);
      //alert(y);
      
      if(x==undefined || x.trim()=='')
        x = -34.601422;
        
      if(y==undefined || y.trim()=='')
        y = -58.382034;
    
      map = new GMaps({
        div: '#map',
        lat: x,
        lng: y,
        click: function(e) {
                                 if(confirm('¿Desea establecer el punto escogido como destino de referencia?')){
                                        e.stop();
                                        definirGeo(e.latLng.lat(), e.latLng.lng());
                                }
                          }
      });
      
      
      if(geoPosicionar){
          
          GMaps.geocode({
          address: '<?php echo $address; ?>',
          callback: function(results, status){
            if(status=='OK'){
              var latlng = results[0].geometry.location;
              definirGeo(latlng.lat(), latlng.lng());
            }
          }
        });
          
      }else{
          
          definirGeo(x,y);
          
      }     
      
    });
    
    
    function definirGeo(x_geo, y_geo){
        
        map.removeMarkers();
        x=x_geo;
        y=y_geo;
        
        map.setCenter(x, y);
        map.addMarker({lat: x,lng: y});
        
        <?php 
              
        if(trim($x_id!="")){
           echo "$('#".$x_id."').val(x_geo); "; 
        }
            
            
        if(trim($y_id!=""))
            echo "$('#".$y_id."').val(y_geo); ";
              
        ?>
        
    }

</script>

<?php

    echo $this->Js->writeBuffer();

?>
