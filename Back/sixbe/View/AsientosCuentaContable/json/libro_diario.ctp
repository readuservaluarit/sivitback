<?php		

include(APP.'View'.DS.'AsientosCuentaContable'.DS.'json'.DS.'asiento_cuenta_contable.ctp');
//LIBRO DIARIO
// 0)id = Nro Asiento  
// 1)d_asiento
// 2)fecha

// 3)debe_cuenta
// 4)debe_concepto 
// 5)debe_monto

// 6)haber_cuenta 
// 7)haber_concepto  
// 8)haber_monto
         // CONFIG  CON MODE DISPLAYED CELLS   
        $model["nro_asiento"]["show_grid"] = 1;
        $model["nro_asiento"]["header_display"] = "nºAsiento";
        $model["nro_asiento"]["minimun_width"] = "5"; //REstriccion mas de esto no se puede achicar
        $model["nro_asiento"]["order"] = "0";
        $model["nro_asiento"]["text_colour"] = "#000000";
        
        $model["d_asiento"]["show_grid"] = 1;
        $model["d_asiento"]["header_display"] = "Descripci&oacute;n";
        $model["d_asiento"]["minimun_width"] = "25"; //REstriccion mas de esto no se puede achicar
        $model["d_asiento"]["order"] = "1";
        $model["d_asiento"]["text_colour"] = "#000000";
        
        $model["fecha"]["show_grid"] = 1;
        $model["fecha"]["header_display"] = "Fecha";
        $model["fecha"]["minimun_width"] = "25"; //Restriccion mas de esto no se puede achicar
        $model["fecha"]["order"] = "2";
        $model["fecha"]["text_colour"] = "#000000";
		
		$model["debe_cuenta"]["show_grid"] = 1;
        $model["debe_cuenta"]["header_display"] = "Debe Cta";
        $model["debe_cuenta"]["minimun_width"] = "15"; //Restriccion mas de esto no se puede achicar
        $model["debe_cuenta"]["order"] = "3";
        $model["debe_cuenta"]["text_colour"] = "#0000FF";
		
		$model["debe_concepto"]["show_grid"] = 1;
        $model["debe_concepto"]["header_display"] = "Debe Cto";
        $model["debe_concepto"]["minimun_width"] = "30"; //Restriccion mas de esto no se puede achicar
        $model["debe_concepto"]["order"] = "4";
        $model["debe_concepto"]["text_colour"] = "#0000FF";
		
		$model["debe_monto"]["show_grid"] = 1;
        $model["debe_monto"]["header_display"] = "D. Monto";
        $model["debe_monto"]["minimun_width"] = "10"; //Restriccion mas de esto no se puede achicar
        $model["debe_monto"]["order"] = "5";
        $model["debe_monto"]["text_colour"] = "#0000FF";
		
		$model["haber_cuenta"]["show_grid"] = 1;
        $model["haber_cuenta"]["header_display"] = "Haber Cta";
        $model["haber_cuenta"]["minimun_width"] = "15"; //Restriccion mas de esto no se puede achicar
        $model["haber_cuenta"]["order"] = "6";
        $model["haber_cuenta"]["text_colour"] = "#008800";//dark green 
		        
		$model["haber_concepto"]["show_grid"] = 1;
        $model["haber_concepto"]["header_display"] = "Haber Cto";
        $model["haber_concepto"]["minimun_width"] = "30"; //Restriccion mas de esto no se puede achicar
        $model["haber_concepto"]["order"] = "7";
        $model["haber_concepto"]["text_colour"] = "#008800"; //dark green 
		        
		$model["haber_monto"]["show_grid"] = 1;
        $model["haber_monto"]["header_display"] = "H. Monto";
        $model["haber_monto"]["minimun_width"] = "10"; //Restriccion mas de esto no se puede achicar
        $model["haber_monto"]["order"] = "8";
        $model["haber_monto"]["text_colour"] = "#008800"; //dark green 
		
		$model["d_estado_asiento"]["show_grid"] = 1;
        $model["d_estado_asiento"]["header_display"] = "Estado";
        $model["d_estado_asiento"]["minimun_width"] = "15"; //Restriccion mas de esto no se puede achicar
        $model["d_estado_asiento"]["order"] = "9";
        $model["d_estado_asiento"]["text_colour"] = "#880033"; 

         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "AsientoCuentaContable",
                    "content" => $model
                );
        
          echo json_encode($output);
?>