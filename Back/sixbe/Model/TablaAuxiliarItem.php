<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class TablaAuxiliarItem extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'tabla_auxiliar_item';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	
	


}
