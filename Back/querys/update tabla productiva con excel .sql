

/*primero pasar la tabal EXCEL a con nombre hoja "hcosto"  six_test2 */

/*-----------------------------------------------------------------------------*/
/*1)PRUEBA EN TESTING- apuntanbdo a six_test2*/
/*opcional chequear difirencia entre las tablas*/
SELECT h.id, h.d_costo, c.costo AS cviejo, h.costo,   h.costo - c.costo AS dif
FROM costo c
INNER JOIN `hcosto` h ON h.id = c.id
WHERE   h.costo <> c.costo 
ORDER BY dif  DESC

/*ACTUALIZAR TABLA*/
UPDATE costo AS c
INNER JOIN hcosto AS h ON h.id = c.id
SET  c.costo = h.costo,
c.`d_costo`= h.d_costo,
c.d_costo_interna = h.d_costo_interna,
c.`id_unidad` = h.`id_unidad`

/*-----------------------------------------------------------------------------*/


/**2) APUNTAR A PRODUCCION valuarit_valmec_produccion */
/**2) chequear difirencia entre las tablas*/
SELECT h.id, h.d_costo, c.costo AS cviejo, h.costo,   h.costo - c.costo AS dif
FROM costo c
INNER JOIN `six_test2`.`hcosto` h ON h.id = c.id
WHERE   h.costo <> c.costo 
ORDER BY dif  DESC


/*ACTUALIZAR TABLA*/
UPDATE costo AS c
INNER JOIN six_test2.hcosto AS h ON h.id = c.id
SET  c.costo = h.costo,
c.`d_costo`= h.d_costo,
c.d_costo_interna = h.d_costo_interna,
c.`id_unidad` = h.`id_unidad`


/*-----------------------------------------------------------------------------*/




10  AISI 316           6.60    7.50   0.90   
20  AISI 304           4.90    5.75   0.85   
30  SAE 1026           1.15    2.00   0.85   
5   ASTM A216 Gr WCB  6.25    6.96   0.71   
16  ASTM A216 Gr WCB   5.80    6.44   0.64   
17  ASTM A216 Gr WCB   5.80    6.44   0.64   
8   SAE 1026           0.83    1.25   0.42   
24  SAE 1026           1.53    1.87   0.34   
15  ASTM A216 Gr WCB   5.95    5.70   -0.25  