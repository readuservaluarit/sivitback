<?php   
        include(APP.'View'.DS.'ComprobanteItemComprobantes'.DS.'json'.DS.'default.ctp');
        
     
        $model["d_tipo_comprobante_origen"]["show_grid"] = 1;
        $model["d_tipo_comprobante_origen"]["header_display"] = "Tipo";
        $model["d_tipo_comprobante_origen"]["order"] = "2";
		$model["d_tipo_comprobante_origen"]["text_colour"] = "#000000";
		$model["d_tipo_comprobante_origen"]["width"] = "3";
		$model["d_tipo_comprobante_origen"]["read_only"] = "1";
		
		$model["simbolo_signo_comercial"]["show_grid"] = 0;
        $model["simbolo_signo_comercial"]["header_display"] = "Signo";
        $model["simbolo_signo_comercial"]["order"] = "3";
		$model["simbolo_signo_comercial"]["width"] = "2";
		$model["simbolo_signo_comercial"]["read_only"] = "1";
		$model["simbolo_signo_comercial"]["text_colour"] = "#007700";
		$model["simbolo_signo_comercial"]["type"] = "string"; //Con estro lo centra en el medio de la comulna //esta definicion deberia ir en COMprobanteItemsComprobante MODEL
		$model["simbolo_signo_comercial"]["length"] = "1";//Con estro lo centra en el medio de la comulna
        
        $model["pto_nro_comprobante_origen"]["show_grid"] = 1;
        $model["pto_nro_comprobante_origen"]["header_display"] = "Nro.";
        $model["pto_nro_comprobante_origen"]["order"] = "4";
		$model["pto_nro_comprobante_origen"]["text_colour"] = "#000000";
		$model["pto_nro_comprobante_origen"]["width"] = "12";
		$model["pto_nro_comprobante_origen"]["read_only"] = "1";
		
		$model["nro_comprobante_origen"]["show_grid"] = 0;
        $model["nro_comprobante_origen"]["header_display"] = "Nro.";
        $model["nro_comprobante_origen"]["order"] = "5";
		$model["nro_comprobante_origen"]["text_colour"] = "#000000";
		$model["nro_comprobante_origen"]["width"] = "5";
		$model["nro_comprobante_origen"]["read_only"] = "1";
        
        $model["fecha_generacion_origen"]["show_grid"] = 1;
        $model["fecha_generacion_origen"]["header_display"] = "Fcha. Gen.";
        $model["fecha_generacion_origen"]["order"] = "6";
		$model["fecha_generacion_origen"]["read_only"] = "1";
		$model["fecha_generacion_origen"]["width"] = "5";
		$model["fecha_generacion_origen"]["text_colour"] = "#0000FF";
        
        $model["fecha_vencimiento_origen"]["show_grid"] = 1;
        $model["fecha_vencimiento_origen"]["header_display"] = "Fcha. Venc.";
        $model["fecha_vencimiento_origen"]["order"] = "7";
		$model["fecha_vencimiento_origen"]["read_only"] = "1";
		$model["fecha_vencimiento_origen"]["width"] = "5";
		$model["fecha_vencimiento_origen"]["text_colour"] = "#F0000F";
        
        $model["item_observacion"]["show_grid"] = 1;
        $model["item_observacion"]["header_display"] = "Observ.";
        $model["item_observacion"]["order"] = "8";
        $model["item_observacion"]["read_only"] = "0";
		$model["item_observacion"]["width"] = "5";
		$model["item_observacion"]["text_colour"] = "#0000AA";
		
		$model["total_comprobante_origen"]["show_grid"] = 1;
        $model["total_comprobante_origen"]["header_display"] = "Total Origen.";
        $model["total_comprobante_origen"]["order"] = "9";
        $model["total_comprobante_origen"]["read_only"] = "1";
		$model["total_comprobante_origen"]["width"] = "5";
		$model["total_comprobante_origen"]["text_colour"] = "#F0000F";
				
		$model["saldo"]["show_grid"] = 1;
        $model["saldo"]["header_display"] = "Saldo"; //SALDO COMERCIAL?
        $model["saldo"]["order"] = "10";
		$model["saldo"]["width"] = "5";
		$model["saldo"]["text_colour"] = "#007700";
		$model["saldo"]["read_only"] = "1";
		

		$model["importe_descuento_unitario"]["show_grid"] = 1;
        $model["importe_descuento_unitario"]["header_display"] = "Descuento Unit.";
        $model["importe_descuento_unitario"]["order"] = "11";
        $model["importe_descuento_unitario"]["read_only"] = "0";
		$model["importe_descuento_unitario"]["width"] = "5";
		$model["importe_descuento_unitario"]["text_colour"] = "#5500AA";
		
		$model["precio_unitario"]["show_grid"] = 1;
        $model["precio_unitario"]["header_display"] = "Monto Imputado.";
        $model["precio_unitario"]["order"] = "12";
        $model["precio_unitario"]["read_only"] = "0";
        $model["precio_unitario"]["width"] = "6";
        $model["precio_unitario"]["text_colour"] = "#0000CC";
		

		/*OPCINALES SEGUN CONFIGURACION DE COMPROBANTE*/
		/*READ ONLY*/
		$model["diferencia_cambio_porcentaje"]["show_grid"] = 1;
        $model["diferencia_cambio_porcentaje"]["header_display"] = "%Dif. Cambio";
        $model["diferencia_cambio_porcentaje"]["order"] = "13";
        $model["diferencia_cambio_porcentaje"]["read_only"] = "1";
		$model["diferencia_cambio_porcentaje"]["width"] = "4";
		$model["diferencia_cambio_porcentaje"]["text_colour"] = "#FF0000";
		
		/*READ ONLY*/
		$model["monto_diferencia_cambio"]["show_grid"] = 1;
        $model["monto_diferencia_cambio"]["header_display"] = "Monto Dif. Cambio";
        $model["monto_diferencia_cambio"]["order"] = "14";
        $model["monto_diferencia_cambio"]["read_only"] = "1";
		$model["monto_diferencia_cambio"]["width"] = "5";
		$model["monto_diferencia_cambio"]["text_colour"] = "#CC0000";
		
		/*DEBERIA SER UN CHECK BOX*/
		$model["ajusta_diferencia_cambio"]["show_grid"] = 1;
        $model["ajusta_diferencia_cambio"]["header_display"] = "Ajusta Dif.";
        $model["ajusta_diferencia_cambio"]["order"] = "15";
        // $model["ajusta_diferencia_cambio"]["read_only"] = "1";
		$model["ajusta_diferencia_cambio"]["width"] = "3";
		$model["ajusta_diferencia_cambio"]["text_colour"] = "#CC0000";
		
		$output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItemComprobante",
                    "content" => $model
                );
        echo json_encode($output);
?>