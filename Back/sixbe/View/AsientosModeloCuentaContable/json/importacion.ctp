<?php
        // IMPORTACION DESDE ASIENTOS - Es la misma vista q asiento_cuenta_contable default
        
        include(APP.'View'.DS.'AsientosCuentaContable'.DS.'json'.DS.'asiento_cuenta_contable.ctp');
        // CONFIG  CON MODE DISPLAYED CELLS
     
		
		//esto se presetea agrega en codigo como un CBO
		$model["es_debe"]["show_grid"] = 1;
		$model["es_debe"]["read_only"] = 0;
		$model["es_debe"]["type"] = EnumTipoDato::entero;
		$model["es_debe"]["header_display"] = "Debe / Haber";
		$model["es_debe"]["minimun_width"] = "65"; //REstriccion mas de esto no se puede achicar
		$model["es_debe"]["order"] = "1";
		$model["es_debe"]["text_colour"] = "#000000";
        
		$model["id_cuenta_contable"]["show_grid"] = 0;//INVISIBLE
        $model["id_cuenta_contable"]["header_display"] = "id_cuenta_contable";
        $model["id_cuenta_contable"]["minimun_width"] = "20"; //Restriccion mas de esto no se puede achicar
        $model["id_cuenta_contable"]["order"] = "2";
        $model["id_cuenta_contable"]["text_colour"] = "#0000EE";
		
		// $model["btnClickMe"]["show_grid"] = 1;//VISIBLE
        // $model["btnClickMe"]["header_display"] = "Click.";
        // $model["btnClickMe"]["minimun_width"] = "3"; //Restriccion mas de esto no se puede achicar
        // $model["btnClickMe"]["order"] = "3";
        // $model["btnClickMe"]["text_colour"] = "#FF0000";
		
		$model["codigo"]["show_grid"] = 1;//VISIBLE
		$model["codigo"]["type"] = EnumTipoDato::cadena;
        $model["codigo"]["header_display"] = "[Código]";
		$model["codigo"]["width"] = "90"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["codigo"]["minimun_width"] = "60"; //Restriccion mas de esto no se puede achicar
        $model["codigo"]["order"] = "3";
        $model["codigo"]["text_colour"] = "#660044";
		
        $model["d_cuenta_contable"]["show_grid"] = 1;//VISIBLE
		
		$model["d_cuenta_contable"]["type"] = EnumTipoDato::cadena;
        $model["d_cuenta_contable"]["header_display"] = "[Desc. Cuenta Contable]";
		$model["d_cuenta_contable"]["width"] = "200"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["d_cuenta_contable"]["minimun_width"] = "80"; //Restriccion mas de esto no se puede achicar
        $model["d_cuenta_contable"]["order"] = "4";
        $model["d_cuenta_contable"]["text_colour"] = "#550055";
		
		
		//En medio de estos campos se agrega un BotonPicker en grilla
		$model["monto"]["show_grid"] = 0;//INVISIBLE
        $model["monto"]["header_display"] = "Monto";
        $model["monto"]["minimun_width"] = "80"; //REstriccion mas de esto no se puede achicar
        $model["monto"]["order"] = "5";
        $model["monto"]["text_colour"] = "#000000";
		
		$model["debe_monto"]["show_grid"] = 1;
        $model["debe_monto"]["header_display"] = "D. Monto";
		$model["debe_monto"]["width"] = "70"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["debe_monto"]["minimun_width"] = "40"; //Restriccion mas de esto no se puede achicar
        $model["debe_monto"]["order"] = "6";
        $model["debe_monto"]["text_colour"] = "#0000FF";
		
		$model["haber_monto"]["show_grid"] = 1;
        $model["haber_monto"]["header_display"] = "H. Monto";
		$model["haber_monto"]["width"] = "70";
        $model["haber_monto"]["minimun_width"] = "40"; //Restriccion mas de esto no se puede achicar
        $model["haber_monto"]["order"] = "7";
        $model["haber_monto"]["text_colour"] = "#008800"; //dark green 
		
		//SE USA COMO VARIABLE PARA GUARDAR Las modificaiones 
		$model["AsientoCuentaContableCentroCostoItem"]["show_grid"] = 0;
		$model["AsientoCuentaContableCentroCostoItem"]["type"] = EnumTipoDato::cadena;
        $model["AsientoCuentaContableCentroCostoItem"]["header_display"] = "ITEMS CENTRO COSTO";
        $model["AsientoCuentaContableCentroCostoItem"]["minimun_width"] = "90"; //Restriccion mas de esto no se puede achicar
        $model["AsientoCuentaContableCentroCostoItem"]["order"] = "9";
        $model["AsientoCuentaContableCentroCostoItem"]["text_colour"] = "#440000";
		

        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "AsientoModeloCuentaContable",
                    "content" => $model
                );
        echo json_encode($output);
?>