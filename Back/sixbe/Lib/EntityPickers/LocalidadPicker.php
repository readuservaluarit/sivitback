<?php

class LocalidadPicker extends EntityPicker{
    
    public $model = "Localidad";
    
    public $pickerTitle = "Selección de Localidad";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Nombre", "field" => "d_localidad", "width" => "90%", "keyOrder" => "d_localidad")
                                );

    public function getPickerConditions($busqueda){
        
        $pickerConditions = array('Localidad.d_localidad LIKE' => '%' . $busqueda['bus_unico'] . '%');
        
        return $pickerConditions;
        
        
    }
  /*
    public function getPickerInitialFilter(){
        
        //Filtra siempre por la Jurisdiccion del usuario si corresponde
        return DataFilter::getJurisdiccionPickerFilter();
        
    }
  */
  
}