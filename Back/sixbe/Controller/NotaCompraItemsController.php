<?php

App::uses('ComprobanteItemsController', 'Controller');

class NotaCompraItemsController extends ComprobanteItemsController {
    
    public $name = EnumController::NotaCompraItems;
    public $model = 'ComprobanteItem';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    

     /**
    * @secured(CONSULTA_NOTA_COMPRA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
     
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model); 
        $id_comprobante_filtro = $this->getFromRequestOrSession($this->model.'.id_comprobante');
        
     
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
         'joins' => array(
            array(
                'table' => 'producto_tipo',
                'alias' => 'ProductoTipo',
                'type' => 'LEFT',
                'conditions' => array(
                    'ProductoTipo.id = Producto.id_producto_tipo'
                )
                
            )),
            'contain' =>array('Comprobante'=>array('EstadoComprobante','Moneda'),'Producto','ComprobanteItemOrigen'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order'=>$this->ComprobanteItem->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_comprobante'))
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));

        //Headers
        $formBuilder->addHeader('id', 'Nota.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'Nota.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$producto ){
                 
                 $producto["ComprobanteItem"]["d_producto"] = $producto["Producto"]["d_producto"];
                 $producto["ComprobanteItem"]["codigo"] = $this->ComprobanteItem->getCodigoFromProducto($producto);
              
                 //$producto["ComprobanteItem"]["total"] = (string) $this->{$this->model}->getTotalItem($producto);
                 $producto["ComprobanteItem"]["nro_comprobante"] = $this->ComprobanteItem->GetNumberComprobante($producto["Comprobante"]["d_punto_venta"],$producto["Comprobante"]["nro_comprobante"]);
                 $producto["ComprobanteItem"]["d_estado_comprobante"] = $producto["Comprobante"]["EstadoComprobante"]["d_estado_comprobante"];
                 $producto["ComprobanteItem"]["moneda_simbolo"] = $producto["Comprobante"]["Moneda"]["simbolo"];
                 $producto["ComprobanteItem"]["id_moneda"] = $producto["Comprobante"]["Moneda"]["id"];
                 
                 
                  $array["id"] = $producto["ComprobanteItem"]["id_comprobante_item_origen"];
                  $producto["ComprobanteItem"]["total_facturado_origen"] = 0;
                  $producto["ComprobanteItem"]["total_notas_credito_origen"] = 0;
       
      
        
                  $id_comprobante_filtro = 211;
                                
               
	
                 
                 
                  if( $id_comprobante_filtro !="" && $producto["ComprobanteItemOrigen"]["id"]>0 && $producto["ComprobanteItem"]["id_comprobante_item_origen"]!= null){
                        
                  	
                  	$notas_credito_origen = $this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItemOrigen"],$this->Comprobante->getTiposComprobanteController(EnumController::NotasCreditoCompra));
                         
                  
                         
                        
                         
                         
                 
                  	$producto["ComprobanteItem"]["total_facturado_origen"] = (string) number_format((float)$producto["ComprobanteItemOrigen"]["cantidad"], $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
                         
                         
                   $producto["ComprobanteItem"]["total_notas_credito_origen"] = (string)  number_format((float)$this->ComprobanteItem->GetCantidadItemProcesados($notas_credito_origen), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
                        
                   
                   
                   $producto["ComprobanteItem"]["total_pendiente_notas_credito_origen"] = (string)  number_format((float) (
                                                                                                     ( $producto["ComprobanteItem"]["total_facturado_origen"] - $producto["ComprobanteItem"]["total_notas_credito_origen"])
                                                                                                       ), 2, '.', '');
                         
                         
                         
                        
        
                         
                 }
                 unset($producto["Producto"]);
                 unset($producto["Comprobante"]);
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    
     /**
    * @secured(CONSULTA_NOTA_COMPRA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
      
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        
     
      $this->set('model',$model);
      Configure::write('debug',0);
      $this->render($vista);
        
       
   
        

        
    }
    

    
   
    
}
?>