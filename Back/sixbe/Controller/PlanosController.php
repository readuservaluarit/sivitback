<?php

/**
* @secured(ABM_PLANOS)
*/
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class PlanosController extends AppController {
    public $name = 'Planos';
    public $model = 'Predio';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    
   
  
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true, 'contain' => array('Lote' =>array('Region' => array('Jurisdiccion'))));
        
        //Recuperacion de Filtros
        $cui = $this->getFromRequestOrSession('Predio.cui');
        $id_jurisdiccion = $this->getFromRequestOrSession('Predio.id_jurisdiccion');
        $d_jurisdiccion = $this->getFromRequestOrSession('Predio.d_jurisdiccion');
        $id_region = $this->getFromRequestOrSession('Predio.id_region');
        $d_region = $this->getFromRequestOrSession('Predio.d_region');
        $id_lote = $this->getFromRequestOrSession('Predio.id_lote');
        $d_lote = $this->getFromRequestOrSession('Predio.d_lote');
        $id_segmento = $this->getFromRequestOrSession('Predio.id_segmento');
        $d_segmento = $this->getFromRequestOrSession('Predio.d_segmento');

        
        $conditions = array(); 

        if($cui!="")
            array_push($conditions, array('Predio.cui LIKE' => '%' . trim($cui)  . '%'));  
        
        if($id_jurisdiccion!="")
            array_push($conditions, array('Region.id_jurisdiccion = ' => $id_jurisdiccion));
            
        if($id_region!="")
            array_push($conditions, array('Lote.id_region = ' => $id_region));
            
        if($id_lote!="")
            array_push($conditions, array('Lote.id = ' => $id_lote));
        
        if($id_segmento!="")
            array_push($conditions, array('Segmento.id = ' => $id_segmento));    
        
        
        //obtengo el rol del usuario logueado
        $id_usuario_logueado=$this->Session->read("Auth.User.id");    
        //Le agrego condicion para que filtre por usuario logueado        
        //array_push($conditions, array('Asignacion.id_usuario = ' => $id_usuario_logueado));
        
        $joins = array(
                             array(
                                'alias' => 'Segmento',
                                'table' => 'segmento',
                                'type' => 'LEFT',
                                'conditions' => '`Segmento`.`id` = `Predio`.`id_segmento`'
                                ),
                           
                            array(
                                'alias' => 'Lote',
                                'table' => 'lote',
                                'type' => 'LEFT',
                                'conditions' => '`Lote`.`id` = `Segmento`.`id_lote`'
                                ),
                            array('alias' => 'Region',
                                'table' => 'region',
                                'type' => 'LEFT',
                                'conditions' => '`Region`.`id` = `Lote`.`id_region`'
                                ),
                            array('alias' => 'Jurisdiccion',
                                                'table' => 'jurisdiccion',
                                                'type' => 'LEFT',
                                                'conditions' => '`Region`.`id_jurisdiccion` = `Jurisdiccion`.`id`'
                                                ),
                                       
        );
        
        //DataFilter
        $joins = array_merge($joins, DataFilter::getPlanosJoinFilter());
        $conditions = array_merge($conditions, DataFilter::getPlanosFilter($this->model));
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'fields' => '*',
            'joins' => $joins, 
            'conditions' => $conditions,
            'limit' => 10, 
            //'contain' =>array('Segmento'=> array('Lote' =>array('Region' => array('Jurisdiccion'))))
            'contain' => array(),
            'page' => $this->getPageNum()
        );
      
        App::import('Lib', 'FormBuilder');
        
        $formBuilder = new FormBuilder();
        
        $data =   $this->PaginatorModificado->paginate($this->model);
        $formBuilder->setDataListado($this, 'Archivos de Planos', 'Datos de los Predios', $this->model, $this->name, $data);
        
        ////////////////////
        //Filters
        ///////////////////
        
        $formBuilder->addFilterBeginRow();
        
        //Jurisdiccion
        $selectionList = array($this->model."IdJurisdiccion" => "id", $this->model."DJurisdiccion" => "nombre");
        $formBuilder->addFilterPicker('JurisdiccionPicker', $selectionList, 'id_jurisdiccion', 
                                   array('value' => $id_jurisdiccion), 'd_jurisdiccion', 'Jurisdicción', 
                                   array('class'=>'control-group span5'), array('class' => '', 'label' => false, 'value' => $d_jurisdiccion),
                                   array("\$('#PredioIdRegion').val('');\$('#PredioDRegion').val('');\$('#PredioIdLote').val('');\$('#PredioDLote').val('');\$('#PredioIdSegmento').val('');\$('#PredioDSegmento').val('');")
                                   );  
                                   
        //Region
        $selectionList = array($this->model."IdRegion" => "id", $this->model."DRegion" => "nombre");
        $formBuilder->addFilterPicker('RegionPicker', $selectionList, 'id_region', 
                                   array('value' => $id_region), 'd_region', 'Región', 
                                   array('class'=>'control-group span5'), array('class' => '', 'label' => false, 'value' => $d_region),
                                   array("\$('#PredioIdLote').val('');\$('#PredioDLote').val('');\$('#PredioIdSegmento').val('');\$('#PredioDSegmento').val('');"),
                                   array('id_jurisdiccion' => 'PredioIdJurisdiccion')
                                   ); 
                                   
        //Lote
        $selectionList = array($this->model."IdLote" => "id", $this->model."DLote" => "nombre");
        $formBuilder->addFilterPicker('LotePicker', $selectionList, 'id_lote', 
                                   array('value' => $id_lote), 'd_lote', 'Lote', 
                                   array('class'=>'control-group span5'), array('class' => '', 'label' => false, 'value' => $d_lote),
                                  array("\$('#PredioIdSegmento').val('');\$('#PredioDSegmento').val('');"),
                                   array('id_region' => 'PredioIdRegion')
                                   ); 
         //Segmento
        $selectionList = array($this->model."IdSegmento" => "id", $this->model."DSegmento" => "nombre");
        $formBuilder->addFilterPicker('SegmentoPicker', $selectionList, 'id_segmento', 
                                   array('value' => $id_segmento), 'd_segmento', 'Segmento', 
                                   array('class'=>'control-group span5'), array('class' => '', 'label' => false, 'value' => $d_segmento),
                                   array(),
                                   array('id_lote' => 'PredioIdLote')
                                   );                            
                                
        //CUI
        $formBuilder->addFilterInput('cui', 'CUI', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $cui));
         
         
        $formBuilder->addFilterEndRow(); 
      
       
        //Headers
        $formBuilder->addHeader('Id', 'id', "5%");
        $formBuilder->addHeader('CUI', 'cui', "10%");
        $formBuilder->addHeader('Jurisdicci&oacute;n', 'Jurisdiccion.nombre', "20%");
        $formBuilder->addHeader('Regi&oacute;n', 'Segmento.Lote.Region.nombre', "10%");
        $formBuilder->addHeader('Lote', 'Segmento.Lote.nombre', "10%");
        $formBuilder->addHeader('Segmento', 'Segmento.nombre', "10%");
        $formBuilder->addHeader('Direcci&oacute;n', 'direccion', "20%");
        //$formBuilder->addHeader('Localidad', 'localidad', "20%");
        //$formBuilder->addHeader('Departamento', 'departamento', "20%");
        

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'cui');
        $formBuilder->addField("Jurisdiccion", 'nombre');
        $formBuilder->addField("Region", 'nombre');
        $formBuilder->addField("Lote", 'nombre');
        $formBuilder->addField("Segmento", 'nombre');
        $formBuilder->addField($this->model, 'direccion');
        //$formBuilder->addField($this->model, 'localidad');
        //$formBuilder->addField($this->model, 'departamento');
        
        
        $formBuilder->showNewButton(false);
        $formBuilder->showDeleteButton(false);
      
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    }
    
    
    public function abm($mode, $id = null) {
        
        if (($mode != "M" && $mode != "C") || !strpos(Controller::referer(), $this->name))
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
       
        if ($mode != "A"){
            $this->Predio->id = $id;
            $this->Predio->contain(array('PlanoPrevio'=>array('TipoArchivo','TipoPlano'),'PlanoCenie'=>array('TipoArchivo','TipoPlano'),'ResumenPlano'=>array('PlantaResumenPlano' => array('PlantaArquitectura','OpcionesRtaPlantaArquitectura') ) ));
            $this->request->data = $this->Predio->read();
            
            
            foreach($this->request->data['PlanoPrevio'] as &$planoPrevio){
                
                
                $link_descarga=$this->directorioDescarga(EnumTipoPlano::PlanoPrevio).'\\'.$planoPrevio['nombre_descarga'];
                
                if(file_exists($link_descarga))
                    $planoPrevio['link_descarga'] ='<a href =\''.Router::url('/', true).'Planos/download/'.$planoPrevio['id'].'\'>Descargar '.$planoPrevio['nombre_original'].'</a>';
                else
                    $planoPrevio['link_descarga'] ='El archivo no se encuentra disponible';
            }
            
            
              foreach($this->request->data['PlanoCenie'] as &$planoCenie){
                
                
               
                $link_descarga=$this->directorioDescarga(EnumTipoPlano::PlanoCenie).'\\'.$planoCenie['nombre_descarga'];
                
                if(file_exists($link_descarga))
                    $planoCenie['link_descarga'] ='<a href =\''.Router::url('/', true).'Planos/download/'.$planoCenie['id'].'\'>Descargar '.$planoCenie['nombre_original'].'</a>';
                else
                    $planoCenie['link_descarga'] ='El archivo no se encuentra disponible';
 
            }
             
        }
        
        
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Archivos de Plano - Cui:'.$this->request->data['Predio']['cui'].' '.$this->request->data['Predio']['direccion']);
        $formBuilder->setTituloForm('Archivos de Plano - Cui:'.$this->request->data['Predio']['cui'].' '.$this->request->data['Predio']['direccion']);

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal multicolumn well span9'));

        //$formBuilder->addFormLineSeparator();
        
        //Links de pestañas
        $formBuilder->addFormTab('tab-planos_predios', 'Archivo Planos Previos');
        $formBuilder->addFormTab('tab-planos_cenie', 'Archivo Planos Cenie');
        $formBuilder->addFormTab('tab-resumen_planos', 'Resumen de Planos');


        
        
        $formBuilder->addFormBeginTab('tab-planos_predios');
        $formBuilder->addFormBeginFieldset('fs-planos_predios', 'Archivo Planos Previos');
        
        
        
        /////////////Inicio Pesta?a Planos Previos//////////////////////
        
        $abmPlanosPrevios = new FormBuilder();
        $abmPlanosPrevios->setController($this);
        $abmPlanosPrevios->setModelName($this->model);
        $abmPlanosPrevios->setControllerName($this->name);
        $abmPlanosPrevios->setTitulo('Archivos Planos Previos');
        $abmPlanosPrevios->setTituloForm('Archivos Planos Previos');
        $abmPlanosPrevios->setFormDivWidth("98%");
        $abmPlanosPrevios->setForm2('PlanoPrevio',  array('class' => 'form-horizontal well span6 subform'));

        $abmPlanosPrevios->addFormBeginRow();
        $tipoArchivoOpt = $this->Predio->PlanoPrevio->TipoArchivo->find('list', array('fields' => array('TipoArchivo.id', 'TipoArchivo.nombreextension')));
        $abmPlanosPrevios->addFormInput('PlanoPrevio.id_tipo_archivo', 'Tipo de Archivo', array('class'=>"control-group span"), array('class' => '', 'label' => false, 'disabled' => false, 'options' => $tipoArchivoOpt));
        $abmPlanosPrevios->addFormInput('PlanoPrevio.nombre_fsystem', 'Nombre en el Sistema', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        
        $abmPlanosPrevios->addFormInput('PlanoPrevio.file_upload', 'Archivo', array('class'=>'control-group'), array('class' => 'required', 'label' => false,'type'=>'file'));
        $abmPlanosPrevios->addFormHtml('<div id="queue"></div>');
        
        
        $abmPlanosPrevios->addFormHidden('PlanoPrevio.TipoArchivo.descripcion');
        $abmPlanosPrevios->addFormHidden('PlanoPrevio.TipoArchivo.extension');
        $abmPlanosPrevios->addFormHidden('PlanoPrevio.id');
        $abmPlanosPrevios->addFormHidden('PlanoPrevio.codigo_archivo');
        $abmPlanosPrevios->addFormHidden('PlanoPrevio.nombre_archivo');
        $abmPlanosPrevios->addFormHidden('PlanoPrevio.nombre_original');
        $abmPlanosPrevios->addFormHidden('PlanoPrevio.hay_error');
        $abmPlanosPrevios->addFormHidden('Predio.cui');
        $abmPlanosPrevios->addFormHidden('PlanoPrevio.tipo_plano');
        $codigo=rand(0,10);
        //$abmPlanosPrevios->addFormHidden('PlanoPrevio.link_descarga');    
        
        
        $random = rand(0,100000000000);
        $tipo_plano = EnumTipoPlano::PlanoPrevio;
        $script="
        var aleatorio=".$random.";
        
         function ValidarPlanoPrevio() {
                var tipo_plano=".$tipo_plano.";
                Validator.clearValidationMsgs('validationMsg_planop');
                
                var form = 'PlanoPrevioAbmForm';
                var validator = new Validator(form);
                jQuery('#PlanoPrevioTipoPlano').val(tipo_plano);
                
             
                
                validator.validateRequired('PlanoPrevioNombreFsystem', 'Debe ingresar un Nombre del Sistema');
                
                if($('#PlanoPrevioId').val()=='')
                      validator.validateRequired('PlanoPrevioCodigoArchivo', 'Debe seleccionar un archivo');
               
                
                
                var valor_tipo=jQuery('#PlanoPrevioIdTipoArchivo option:selected').html();
                                          
                jQuery('#PlanoPrevioTipoArchivoDescripcion').val(valor_tipo);
                
                
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_planop');
                    return false;   
                }
                
                return true;
            }
          
         
         
         
         function Numeroaleatorio(){
          
               
              var aleatorio_local=Math.floor((Math.random()*1000000)+1);
              jQuery('#PlanoPrevioCodigoArchivo').val(aleatorio_local);
              
          return     jQuery('#PlanoPrevioCodigoArchivo').val();
         
         }
         
         
         
        function HandlerError (errorType) {
         
               Validator.clearValidationMsgs('validationMsg_planop');
                
                var form = 'PlanoPrevioAbmForm';
                var validator = new Validator(form);
                
                switch(errorType) {
                            case '404_FILE_NOT_FOUND':
                                errorMsg = '404 Error';
                                break;
                            case '403_FORBIDDEN':
                                errorMsg = '403 Forbidden';
                                break;
                            case 'FORBIDDEN_FILE_TYPE':
                                errorMsg = 'El formato del archivo es incorrecto </br> Los archivos aceptados son jpg ,.jpeg ,.pdf , .dwg , .tiff ,.tif y .mcd';
                                break;
                            case 'FILE_SIZE_LIMIT_EXCEEDED':
                                errorMsg = 'El archivo es demasiado grande';
                                break;
                            default:
                                errorMsg = 'Unknown Error';
                                break;
                        }
             
                
                
                validator.addErrorMessage('PlanoPrevioCodigoArchivo',errorMsg); 
                validator.showValidations('', 'validationMsg_planop');
                $.colorbox.resize();
                return true;
                
         }
    
        $(function() {
            $('#PlanoPrevioFileUpload').uploadifive({
                'auto'             : true,
                'multi'            : false,
                'formData'         : {
                 
                    'DBGSESSID' : '415487275833100001@127.0.0.1',
                    'd'     : '1',
                    'p'     : '0',
                    'c'     : '1'
                },
                'uploadScript'     :'" . Router::url(array('controller' => 'Planos', 'action' => 'uploadFile')) . "',
                'buttonText' : 'EXAMINAR...' ,
                'fileType'   : ['image\/gif','image\/jpeg','image\/png','image\/tiff','image\/tif','application\/vnd.mcd','application\/vnd.mcd','application\/pdf','application\/x-pdf','image\/vnd.dwg','image\/x-dwg'],
                'onUploadComplete' : function(file, data) { 
                
                    alert('El archivo ' + file.name + ' ha sido subido exitosamente.');
                    $.colorbox.resize();
                    jQuery('#PlanoPrevioNombreOriginal').val(file.name); 
                    
                
                
                },
                 'onAddQueueItem' : function(file) {
                    $('#PlanoPrevioHayError').val('0');
                     Validator.clearValidationMsgs('validationMsg_planop');
                     var form = 'PlanoPrevioAbmForm';
                     var validator = new Validator(form);
                     //$('#PlanoPrevioFileUpload').uploadifive('settings', 'formData', {'random': Numeroaleatorio()});
                     this.data('uploadifive').settings.formData = { 'random' : Numeroaleatorio() };
                     jQuery('#PlanoPrevioNombreFsystem').val(file.name);
                     jQuery('#PlanoPrevioNombreFsystem').val( jQuery('#PlanoPrevioNombreFsystem').val().replace(' ','_') );
                     
                     //si la extension cargada no coincide con la seleccionada error
                     var archivo = file.name;
                     if(archivo.substr( (archivo.lastIndexOf('.') +1) ) != jQuery('#PlanoPrevioIdTipoArchivo').find(':selected').text().slice(-4).trim()){
                      jQuery('#PlanoPrevioHayError').val('1');
                     validator.addErrorMessage('PlanoPrevioCodigoArchivo','La extensi&oacute;n del archivo que seleccion&oacute; no coincide con el Tipo de Archivo seleccionado');
                     //cancelo la subida
                     jQuery('#PlanoPrevioFileUpload').uploadifive('cancel', jQuery('.uploadifive-queue-item').first().data('file'));
                     //limpio el form
                     //jQuery('#PlanoPrevioFileUpload').uploadifive('clearQueue');
                     $.colorbox.resize();
                     jQuery('#PlanoPrevioNombreFsystem').val('');
                     jQuery('#PlanoPrevioId').val('');
                     jQuery('#PlanoPrevioCodigoArchivo').val('');
                     validator.showValidations('', 'validationMsg_planop');
                     }
                     
                     
                    }, 
                     
                'onError'         : function(errorType) {
                                        HandlerError(errorType);
                                
                } 
            });
        });";
        
        $abmPlanosPrevios->addFormCustomScript($script);
        $abmPlanosPrevios->setFormValidationFunction('ValidarPlanoPrevio()');
        
        $columnas = array(
            array('id'=>'PlanoPrevio.id','visible'=>false),
            array('id'=>'PlanoPrevio.TipoArchivo.descripcion','nombre'=>'Tipo Archivo','visible'=>true),
            array('id'=>'PlanoPrevio.id_tipo_archivo','nombre'=>'Id de tipo de archivo','visible'=>false),
            array('id'=>'PlanoPrevio.nombre_original','nombre'=>'Archivo','visible'=>true),
            array('id'=>'PlanoPrevio.nombre_fsystem','nombre'=>'Nombre en el Sistema','visible'=>true),
            array('id'=>'PlanoPrevio.codigo_archivo','nombre'=>'Codigo oculto de Archivo','visible'=>false),
            array('id'=>'PlanoPrevio.nombre_archivo','nombre'=>'Nombre Original Archivo','visible'=>false),
            array('id'=>'PlanoPrevio.id_tipo_plano','nombre'=>'Tipo Plano','visible'=>false),
            array('id'=>'PlanoPrevio.link_descarga','nombre'=>'Link de Descarga','visible'=>true),
            array('id'=>'PlanoPrevio.TipoPlano.path_fsystem','nombre'=>'path','visible'=>false), 
            array('id'=>'Predio.cui','visible'=>false),
             
            
        );
        $formBuilder->addFormSubform("planop", "", "PlanoPrevio", array('bajaValidacion'=>'ValidarBajaPlano'), $columnas,$abmPlanosPrevios);
                             
        
        
        
        
        $formBuilder->addFormEndFieldset();
        $formBuilder->addFormEndTab();
        
        /////////////Fin Pesta?a Planos Previos//////////////////////
        
       
       
       $formBuilder->addFormBeginTab('tab-planos_cenie');
        $formBuilder->addFormBeginFieldset('fs-planos_cenie', 'Archivo Planos Cenie');
        
        
        
        
        
        
        /////////////Inicio Pesta?a Planos Cenie//////////////////////
        
        $abmPlanosCenie = new FormBuilder();
        $abmPlanosCenie->setController($this);
        $abmPlanosCenie->setModelName($this->model);
        $abmPlanosCenie->setControllerName($this->name);
        $abmPlanosCenie->setTitulo('Archivos Planos Cenie');
        $abmPlanosCenie->setTituloForm('Archivos Planos Cenie');
        $abmPlanosCenie->setFormDivWidth("98%");
        $abmPlanosCenie->setForm2('PlanoCenie',  array('class' => 'form-horizontal well span6 subform'));

        $abmPlanosCenie->addFormBeginRow();
        $tipoArchivoOpt = $this->Predio->PlanoCenie->TipoArchivo->find('list', array('fields' => array('TipoArchivo.id', 'TipoArchivo.nombreextension')));
        $abmPlanosCenie->addFormInput('PlanoCenie.id_tipo_archivo', 'Tipo de Archivo', array('class'=>"control-group"), array('class' => '', 'label' => false, 'disabled' => false, 'options' => $tipoArchivoOpt));
        $abmPlanosCenie->addFormInput('PlanoCenie.nombre_fsystem', 'Nombre en el Sistema', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $abmPlanosCenie->addFormHtml('<div id="queue"></div>');
        $abmPlanosCenie->addFormInput('PlanoCenie.file_upload', 'Archivo', array('class'=>'control-group'), array('class' => 'required', 'label' => false,'type'=>'file'));
        $abmPlanosCenie->addFormHidden('PlanoCenie.TipoArchivo.descripcion');
        $abmPlanosCenie->addFormHidden('PlanoCenie.codigo_archivo');
        $abmPlanosCenie->addFormHidden('PlanoCenie.nombre_archivo');
        $abmPlanosCenie->addFormHidden('PlanoCenie.nombre_original');
        $abmPlanosPrevios->addFormHidden('PlanoCenie.hay_error');
        $abmPlanosCenie->addFormHidden('Predio.cui');
        $abmPlanosCenie->addFormHidden('PlanoCenie.tipo_plano');
        $codigo=rand(0,10);
        //$abmPlanosPrevios->addFormHidden('PlanoCenie.link_descarga');    
        
        
        $random = rand(0,100000000000);
        $tipo_plano = EnumTipoPlano::PlanoCenie;
        $script="
        var aleatorio=".$random.";
        
          function ValidarPlanoCenie() {
                var tipo_plano=".$tipo_plano.";    
                Validator.clearValidationMsgs('validationMsg_planoc');
                
                var form = 'PlanoCenieAbmForm';
                var validator = new Validator(form);
                jQuery('#PlanoCenieTipoPlano').val(tipo_plano);
                
             
                
                validator.validateRequired('PlanoCenieNombreFsystem', 'Debe ingresar un Nombre del Sistema');
                if($('#PlanoCenieId').val()=='')
                    validator.validateRequired('PlanoCenieCodigoArchivo', 'Debe seleccionar un archivo');
               
                                           
                var valor_tipo=jQuery('#PlanoCenieIdTipoArchivo option:selected').html();
                                          
                jQuery('#PlanoCenieTipoArchivoDescripcion').val(valor_tipo);
                
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_planoc');
                    return false;   
                }
                
                return true;
            }
         
         
         
         function Numeroaleatorio(){
          
               
              var aleatorio_local=Math.floor((Math.random()*1000000)+1);
              jQuery('#PlanoCenieCodigoArchivo').val(aleatorio_local);
              
          return     jQuery('#PlanoCenieCodigoArchivo').val();
         
         }
         
         
         function HandlerError (errorType) {
         
               Validator.clearValidationMsgs('validationMsg_planoc');
                
                var form = 'PlanoCenieAbmForm';
                var validator = new Validator(form);
                
                switch(errorType) {
                            case '404_FILE_NOT_FOUND':
                                errorMsg = '404 Error';
                                break;
                            case '403_FORBIDDEN':
                                errorMsg = '403 Forbidden';
                                break;
                            case 'FORBIDDEN_FILE_TYPE':
                                errorMsg = 'El formato del archivo es incorrecto </br> Los archivos aceptados son jpg ,.jpeg ,.pdf , .dwg , .tiff ,.tif y .mcd';
                                break;
                            case 'FILE_SIZE_LIMIT_EXCEEDED':
                                errorMsg = 'El archivo es demasiado grande';
                                break;
                            default:
                                errorMsg = 'Unknown Error';
                                break;
                        }
             
                
                
                validator.addErrorMessage('PlanoCenieCodigoArchivo',errorMsg); 
                validator.showValidations('', 'validationMsg_planoc');
                $.colorbox.resize();
                return true;
                
         }
        
        $(function() {
            $('#PlanoCenieFileUpload').uploadifive({
                'auto'             : true,
                'multi'            : false,
                'formData'         : {
                 
                    'DBGSESSID' : '415487275833100001@127.0.0.1',
                    'd'     : '1',
                    'p'     : '0',
                    'c'     : '1'
                },
                'uploadScript'     :'" . Router::url(array('controller' => 'Planos', 'action' => 'uploadFile')) . "',
                'buttonText' : 'EXAMINAR...' ,
                'fileType'   : ['image\/gif','image\/jpeg','image\/png','image\/tiff','image\/tif','application\/vnd.mcd','application\/vnd.mcd','application\/pdf','application\/x-pdf','image\/vnd.dwg','image\/x-dwg'],
                'onUploadComplete' : function(file, data) { 
                
                    alert('El archivo ' + file.name + ' ha sido subido exitosamente.');
                    $.colorbox.resize();
                    jQuery('#PlanoCenieNombreOriginal').val(file.name); 
                
                
                
                },
                 'onAddQueueItem' : function(file) {
                     Validator.clearValidationMsgs('validationMsg_planoc');
                     var form = 'PlanoCenieAbmForm';
                     var validator = new Validator(form);
                     //$('#PlanoCenieFileUpload').uploadifive('settings', 'formData', {'random': Numeroaleatorio()});
                     this.data('uploadifive').settings.formData = { 'random' : Numeroaleatorio() };
                     jQuery('#PlanoCenieNombreFsystem').val(file.name);
                     jQuery('#PlanoCenieNombreFsystem').val( jQuery('#PlanoCenieNombreFsystem').val().replace(' ','_') );
                     //si la extension cargada no coincide con la seleccionada error
                     var archivo = file.name;
                     
                     if(archivo.substr( (archivo.lastIndexOf('.') +1) ) != jQuery('#PlanoCenieIdTipoArchivo').find(':selected').text().slice(-4).trim()){
                      jQuery('#PlanoCenieHayError').val('1');
                     validator.addErrorMessage('PlanoCenieCodigoArchivo','La extensi&oacute;n del archivo que seleccion&oacute; no coincide con el Tipo de Archivo seleccionado');
                     //cancelo la subida
                     jQuery('#PlanoCenieFileUpload').uploadifive('cancel', jQuery('.uploadifive-queue-item').first().data('file'));
                     //limpio el form
                     //jQuery('#PlanoCenieFileUpload').uploadifive('clearQueue');
                     $.colorbox.resize();
                     jQuery('#PlanoCenieNombreFsystem').val('');
                     jQuery('#PlanoCenieId').val('');
                     jQuery('#PlanoCenieCodigoArchivo').val('');
                     validator.showValidations('', 'validationMsg_planoc');
                     }
                },
                'onError'         : function(errorType) {
                                        HandlerError(errorType);
                                
                } 
            });
        });
        ";
        
        $abmPlanosCenie->addFormCustomScript($script);
        $abmPlanosCenie->setFormValidationFunction('ValidarPlanoCenie()');
        
         $columnas = array(
            array('id'=>'PlanoCenie.id','visible'=>false),
            array('id'=>'PlanoCenie.TipoArchivo.descripcion','nombre'=>'Tipo Archivo','visible'=>true),
            array('id'=>'PlanoCenie.id_tipo_archivo','nombre'=>'Id de tipo de archivo','visible'=>false),
            array('id'=>'PlanoCenie.nombre_original','nombre'=>'Archivo','visible'=>true),
            array('id'=>'PlanoCenie.nombre_fsystem','nombre'=>'Nombre en el Sistema','visible'=>true),
            array('id'=>'PlanoCenie.codigo_archivo','nombre'=>'Codigo oculto de Archivo','visible'=>false),
            array('id'=>'PlanoCenie.nombre_archivo','nombre'=>'Nombre Original Archivo','visible'=>false),
            array('id'=>'PlanoCenie.id_tipo_plano','nombre'=>'Tipo Plano','visible'=>false),
            array('id'=>'PlanoCenie.link_descarga','nombre'=>'Link de Descarga','visible'=>true),
            array('id'=>'Predio.cui','visible'=>false),
            array('id'=>'PlanoCenie.TipoPlano.path_fsystem','nombre'=>'path','visible'=>false), 
            
        );
        $formBuilder->addFormSubform("planoc", "", "PlanoCenie", array('bajaValidacion'=>'ValidarBajaPlano'), $columnas,$abmPlanosCenie);
                             
        
        
        
        
        $formBuilder->addFormEndFieldset();
        $formBuilder->addFormEndTab();
        
        /////////////Fin Pesta?a Planos Cenie//////////////////////                                                                                 
       
       
         
       $formBuilder->addFormBeginTab('tab-resumen_planos');
       $formBuilder->addFormBeginFieldset('fs-resumen_planos', 'Resumen de Planos');
       
        /////////////Inicio Pesta?a Resumen Planos//////////////////////
        
        $abmResumenPlanos = new FormBuilder();
        $abmResumenPlanos->setController($this);
        $abmResumenPlanos->setModelName($this->model);
        $abmResumenPlanos->setControllerName($this->name);
        $abmResumenPlanos->setTitulo('Resumen de Planos');
        $abmResumenPlanos->setTituloForm('Resumen de Planos');
        $abmResumenPlanos->setFormDivWidth("98%");
        $abmResumenPlanos->setForm2('ResumenPlano',  array('class' => 'form-horizontal well span6 subform'));

        $abmResumenPlanos->addFormBeginRow();
       
       
         $script="function ValidarResumenPlano(){
         
                      var valor_tipo=jQuery('#PlantaResumenPlanoIdPlanta option:selected').html();
                                              
                      jQuery('#PlantaResumenPlanoPlantaArquitecturaDescripcion').val(valor_tipo);
                      
                      var valor_tipo=jQuery('#PlantaResumenPlanoIdRtaPlanta option:selected').html();
                                              
                      jQuery('#PlantaResumenPlanoOpcionesRtaPlantaArquitecturaDescripcion').val(valor_tipo);
                      
                      Validator.clearValidationMsgs('validationMsg_planor');
                    
                      var form = 'ResumenPlanoAbmForm';
                      var validator = new Validator(form);
                    
                    
                        var existe = $('input').filter(function() { return this.name.match(/(data\[ResumenPlano\]\[PlantaResumenPlano\]\[)[0-9]*(\]\[id_rta_planta\])/) && this.name != 'data[ResumenPlano][PlantaResumenPlano]['+$('#otrosppos').val()+'][id_rta_planta]' && this.value == $('#PlantaResumenPlanoIdRtaPlanta').val(); }).length;
                          if (existe >= 1) {
                            validator.addErrorMessage('PlantaResumenPlanoIdRtaPlanta', 'Ya existe un Resumen de Plano con este Tipo de Plano.');
                            } 
                    
                      //validator.validateRequired('ResumenPlanoObservaciones', 'Debe ingresar una Observaci&oacute;n');
                    
                   
                                               
                     
                    
                        if(!validator.isAllValid()){
                            validator.showValidations('', 'validationMsg_planor');
                            return false;   
                        }
                    
                        return true;
         
                }";
       
       
        $abmResumenPlanos->addFormCustomScript($script);
        
        $opcionesPlantaArq = $this->Predio->ResumenPlano->PlantaResumenPlano->PlantaArquitectura->find('list', array('fields' => array('PlantaArquitectura.id', 'PlantaArquitectura.descripcion')));
       
        $abmResumenPlanos->addFormInput('PlantaResumenPlano.id_planta', 'Planta Arquitectura', array('class'=>"control-group"), array('class' => '', 'label' => false, 'disabled' => false, 'options' => $opcionesPlantaArq));
        
         $opcionesrtaArq = $this->Predio->ResumenPlano->PlantaResumenPlano->OpcionesRtaPlantaArquitectura->find('list', array('fields' => array('OpcionesRtaPlantaArquitectura.id', 'OpcionesRtaPlantaArquitectura.descripcion')));
       
        $abmResumenPlanos->addFormInput('PlantaResumenPlano.id_rta_planta', 'Tipo de Plano', array('class'=>"control-group"), array('class' => '', 'label' => false, 'disabled' => false, 'options' => $opcionesrtaArq));
        
        //$abmResumenPlanos->addFormInput('ResumenPlano.Observaciones', 'Observaciones', array('class'=>'control-group span'), array('class' => 'required', 'label' => false,'type'=>'textarea','style'=>'whidth:350px;'));
        
        $abmResumenPlanos->addFormHidden('PlantaResumenPlano.PlantaArquitectura.descripcion');
        $abmResumenPlanos->addFormHidden('PlantaResumenPlano.OpcionesRtaPlantaArquitectura.descripcion');
      
        $formBuilder->addFormInput('ResumenPlano.observaciones', 'Observaciones', array('class'=>'required span5'), array('style' => 'width: 500px;','class' => 'required','type' => 'textarea', 'label' => false, 'value' => $this->request->data['ResumenPlano']['observaciones']));
       
        //$abmResumenPlanos->addFormHidden('ResumenPlano.observaciones');
           
        $abmResumenPlanos->setFormValidationFunction('ValidarResumenPlano()');
         $columnas = array(
            //array('id'=>'ResumenPlano.id','nombre'=>'Id','visible'=>false),
            array('id'=>'PlantaResumenPlano.id','nombre'=>'Id','visible'=>false),
            array('id'=>'PlantaResumenPlano.id_planta','nombre'=>'Id planta','visible'=>false),
            array('id'=>'PlantaResumenPlano.id_rta_planta','nombre'=>'Id Rta Planta','visible'=>false),
            array('id'=>'PlantaResumenPlano.PlantaArquitectura.descripcion','nombre'=>'Tipo de Planta','visible'=>true),
            array('id'=>'PlantaResumenPlano.OpcionesRtaPlantaArquitectura.descripcion','nombre'=>'Tipo de Plano','visible'=>true),
          
             
            
        );
        $formBuilder->addFormSubform("planor", "", "ResumenPlano.PlantaResumenPlano", array('bajaValidacion'=>'ValidarBajaResumen'), $columnas,$abmResumenPlanos);
                             
        
        
        
        $formBuilder->addFormHidden('ResumenPlano.id');
        $formBuilder->addFormEndFieldset();
        $formBuilder->addFormEndTab();
        
        /////////////Fin Pesta?a Resumen Planos//////////////////////   
        
        
         
      /*   
        //////////////fin PESTA?A  observaciones/////////////////////////////////////////////////
        
            $formBuilder->addFormBeginTab('tab-area_exteriror');
            $formBuilder->addFormBeginFieldset('fs-area_exteriror', 'Areas Exteriores');
            
            $abmAreaExterior = new FormBuilder();
            $abmAreaExterior->setController($this);
            $abmAreaExterior->setModelName($this->model);
            $abmAreaExterior->setControllerName($this->name);
            $abmAreaExterior->setTitulo('Areas Exteriores');
            $abmAreaExterior->setTituloForm('Areas Exteriores');
            $abmAreaExterior->setFormDivWidth("98%");
            $abmAreaExterior->setForm2('AreaExterior',  array('class' => 'form-horizontal well span5 subform'));
            
            $abmAreaExterior->addFormBeginRow();   //Alta de Asociacion
            
            //Lote
            $id_lote='';
            $d_lote='';
            
            
           
            /*$selectionList = array("UsuarioSegmentoIdLote" => "id", "UsuarioSegmentoDLote" => "nombre");
            $abmAreaExterior->addFormPicker('LotePicker', $selectionList, 'id_lote', 
                                   array('value' => $id_region), 'd_lote', 'Lote', 
                                   array('class'=>'control-group span10'), array('class' => '', 'label' => false, 'value' => $d_region),
                                   array("\$('#UsuarioSegmentoIdSegmento').val('');\$('#UsuarioSegmentoDLote').val('');"),
                                   array('id_region' => 'UsuarioIdRegion')
                                   );
        
            */
            
           /* //Segmento
            $id_segmento='';
            $d_segmento='';
            $selectionList = array("UsuarioSegmentoSegmentoId" => "id", "UsuarioSegmentoSegmentoNombre" => "nombre");
            $abmAreaExterior->addFormPicker('SegmentoPicker', $selectionList, 'UsuarioSegmento.Segmento.id', 
                                   array('value' => $id_segmento), 'UsuarioSegmento.Segmento.nombre', 'Segmento', 
                                   array('class'=>'control-group span10'), array('class' => '', 'label' => false, 'value' => $d_lote),
                                   array(),
                                   array('id_lote' => 'UsuarioIdLote')
                                   );
           $abmAreaExterior->addFormHidden('UsuarioSegmento.id_temporal');
           $abmAreaExterior->addFormHidden('UsuarioSegmento.id_seleccion_temporal');   
                                
           $columnas = array(
            array('id'=>'UsuarioSegmento.id','nombre'=>'id','visible'=>false),                              
            array('id'=>'UsuarioSegmento.Segmento.nombre','nombre'=>'Nombre Segmento','visible'=>true),
            array('id'=>'UsuarioSegmento.Segmento.id','nombre'=>'id_segmento','visible'=>false),
            array('id'=>'UsuarioSegmento.id_temporal','visible'=>false)
           
            );
                                    
            //cambia el titulo del Subform 
            $script="
          
                    
                    
                    function ValidarAsociacionSegmento() {
                    
          
                        
                        Validator.clearValidationMsgs('validationMsg_aext');
                        var form = 'UsuarioSegmentoAbmForm';
                        var validator_aext = new Validator(form);
                        var segmento_id_temp = ".$id_segmento_temporal.";
                        
                        
                        
                        validator_aext.validateRequired('UsuarioSegmentoSegmentoNombre', 'Debe seleccionar un Segmento');
                        
                      
                                
                      // valida si ya agrego este segmento
                       var existe = $('input').filter(function() { return this.name.match(/(data\[UsuarioSegmento\]\[)[0-9]*(\]\[Lote\]\[id\])/) && this.name != 'data[UsuarioSegmento]['+$('#aextpos').val()+'][Segmento][id]' && this.value == $('#UsuarioSegmentoSegmentoId').val(); }).length;
                        if (existe >= 1) {
                            validator_aext.addErrorMessage('UsuarioSegmentoSegmentoNombre', 'Esta Asociaci&oacute;n de Lote ya existe, verif&iacute;quelo.');
                        }else{
                            
                        
                        //Valido si el lote ya esta asociado
                       $.ajax({
                        'url': './".$this->name."/ajax_asociacion_segmento',
                        'data': $('#UsuarioSegmentoAbmForm').serialize(),
                        'async': false,
                        'success': function(data) {
                              
                            if(data !=0){
                              
                               validator_aext.addErrorMessage('UsuarioSegmentoSegmentoNombre','El Segmento que elijio ya se encuenta asignado, verif&iacute;quelo.');
                               validator_aext.showValidations('', 'validationMsg_aext');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                     }     
                    if(!validator_aext.isAllValid()){
                            validator_aext.showValidations('', 'validationMsg_aext');
                            return false;   
                    }
                    
                    
                   // agrego un id_temporal y actualizo el combo del tab cuis
                   var id_segmento_nuevo_form=$('#UsuarioSegmentoSegmentoId').val();
                    if ($('#UsuarioSegmentoIdTemporal').val()=='') {
                        
                        var id_segmento_nuevo=segmento_id_temp++;
                        $('#UsuarioSegmentoIdTemporal').val(id_segmento_nuevo); 
                        $('#NoSaveUsuarioSegmentoId').append('<option value=\"'+$('#UsuarioSegmentoIdTemporal').val()+'\">'+id_segmento_nuevo_form+ ' - ' +$('#UsuarioSegmentoSegmentoNombre').val()+'</option>');
                        $('#NoSaveUsuarioSegmentoId').multiselect('refresh');
                    } else {
                        $('#UsuarioSegmentoIdSeleccionTemporal').val($('#UsuarioSegmentoIdTemporal').val());
                        $('#NoSaveUsuarioSegmentoId option[value='+$('#UsuarioSegmentoIdTemporal').val()+']').html(id_segmento_nuevo_form + ' - ' + $('#UsuarioSegmentoSegmentoNombre').val());
                    }
                      $('#UsuarioHidCodigoSegmento').val(id_segmento_nuevo_form); 
                                                 
                        return true;  
                           
                        
                      
                       
                        
                        
                     }";
            
            
             
            $abmAreaExterior->addFormCustomScript($script);
            $abmAreaExterior->setFormValidationFunction('ValidarAsociacionSegmento()');                       
                                   
            $abmAreaExterior->addFormEndRow();
            
            
            
          
        $formBuilder->addFormSubform("aext", "", "UsuarioSegmento", array('bajaValidacion'=>'ValidarBajaAsignacionSegmento'), $columnas, $abmAreaExterior);                       
       
    
       ///////////////////////FIN PESTA?A ASIGNACIONSEGMENTOS/////////////////////////////////////// 
         $formBuilder->addFormEndFieldset();
         $formBuilder->addFormEndTab();
       */
        
        /*
        $formBuilder->addFormBeginTab('tab-observaciones');
        $formBuilder->addFormBeginFieldset('fs-observaciones', 'Observaciones');
        
        
        $formBuilder->addFormHidden('FormularioPredio.id', array('value' => $formularioPredioData['id']));
        $formBuilder->addFormHidden('FormularioPredio.id_tipo_formulario', array('value' => $formularioPredioData['id_tipo_formulario']));
        $formBuilder->addFormHidden('FormularioPredio.completo', array('value' => $formularioPredioData['completo']));
        $formBuilder->addFormInput('FormularioPredio.observaciones', 'Observaciones', array('class'=>'control-group span6'), array('class' => 'required', 'style' => 'width:506px;', 'label' => false, 'type' => 'textarea', 'value' => $formularioPredioData['observaciones']));
        
        
        $formBuilder->addFormEndFieldset();
        $formBuilder->addFormEndTab();
        
        
        
        $formBuilder->addButton(array(
            'id'=>'btnFinalizar', 
            'text'=>'Finalizar Carga', 
            'style' => 'margin-right:20px', 
            'icon'=>'icon-download-alt icon-white', 
            'onClick'=>"if(!validateForm(true)) 
                            return false;
                        //$('#PredioIdEstadoPredio').val(3);
                        $('#FormularioPredioCompleto').val(1);
                        "
        )); */
        
        $formBuilder->addButton(array(
            'id'=>'btnFinalizar', 
            'text'=>'Finalizar Carga', 
            'style' => 'margin-right:20px', 
            'icon'=>'icon-download-alt icon-white', 
            'onClick'=>"if(!validateForm(true)) 
                            return false;
                        //$('#PredioIdEstadoPredio').val(3);
                        $('#FormularioPredioCompleto').val(1);
                        "
        )); 
        
        $script = "
             
        
            function validateForm(finalizar){
                Validator.clearValidationMsgs('validationMsg_');
               
                var form = 'PredioAbmForm';
                var validator = new Validator(form);
                
               
                if(tipo_no_escolar_detalle_otro==1){
                 validator.validateRequired('PredioDetalleOtroInstNoEscolar', 'Debe ingresar Otro En B- Cue Anexo en edificios No Escolares');
                }
                if(tipo_situacion_dominio_detalle_otro==1){
                   validator.validateRequired('PredioDetalleOtraSituacion', 'Debe ingresar Otra en 1 - Situaci&oacute;n de Dominio');
                  }
                
                
                
                
               
                
                
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                
              
                
                return true;
            }
            
            
            function ValidarBajaPlano(pos) {
                return confirm('¿Esta seguro que desea eliminar el Plano?');
            }
             function ValidarBajaResumen(pos) {
                return confirm('¿Esta seguro que desea eliminar el Resumen del Plano?');
            }
             ";
           /* function ValidarBajaConstruccion(pos) {
                id_temp = $('[name=\"data[Construccion]['+pos+'][id_temporal]\"]').val();
                if ($('.temp_id:contains('+id_temp+')').length > 0) {
                    alert('La construccion tiene locales asignados.');
                    return false;
                }
                
                if (!confirm('¿Esta seguro que desea eliminar la Construccion?'))
                    return false;
                
                // saco la opcion del tab locales
                $('#NoSaveConstruccionId option[value='+id_temp+']').remove()
                $('#NoSaveConstruccionId').multiselect('refresh');
                NoSaveConstruccionIdChange();
                
                return true;
            }
            function ValidarBajaLocal(pos) {
                return confirm('¿Esta seguro que desea eliminar el Local?');
            }
            ";*/
        $formBuilder->addFormCustomScript($script);

        //$formBuilder->setFormValidationFunction('validateForm(false)');
      
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }

    function edit($id) {
        if (!$this->request->is('get')){
            $this->loadModel($this->model);
            $this->loadModel('PlanoPrevio');
            $this->loadModel('PlanoCenie');
            $this->loadModel('ResumenPlano');
            $this->loadModel('PlantaResumenPlano');
            
          
            
            $this->Predio->id = $id;
            
            
            
             //cargo los PlanosPrevios que me envia
            $planosPreviosId = array();
            if (array_key_exists('PlanoPrevio', $this->request->data)) {
                foreach($this->request->data['PlanoPrevio'] as $planoprevio) {
                    
                    if ($planoprevio['id'])
                        array_push($planosPreviosId, $planoprevio['id']);
                }
            }
            
               //cargo los PlanosPrevios que me envia
            $planosCenieId = array();
            if (array_key_exists('PlanoCenie', $this->request->data)) {
                foreach($this->request->data['PlanoCenie'] as $planocenie) {
                   
                    if ($planocenie['id'])
                        array_push($planosCenieId, $planocenie['id']);
                }
            }
            
            
             $presumenPlanosId = array();
            if (array_key_exists('PlantaResumenPlano', $this->request->data['ResumenPlano'])) {
                foreach($this->request->data['ResumenPlano']['PlantaResumenPlano'] as &$presumenplano) {
                    
                    $presumenplano['id_resumen_plano']  =  $this->request->data['ResumenPlano']['id'];
                    unset($presumenplano['PlantaArquitectura']);
                    unset($presumenplano['OpcionesRtaPlantaArquitectura']);

                    if ($presumenplano['id'])
                        array_push($presumenPlanosId, $presumenplano['id']);
                }
                
                
            }
            
            $this->request->data['ResumenPlano']['id_predio'] =  $id;
            $this->request->data['Predio']['ResumenPlano'] =    $this->request->data['ResumenPlano'];
            unset($this->request->data['ResumenPlano']);
            
            $id_predio=$id;
            if (array_key_exists('PlanoPrevio', $this->request->data)){
            
                foreach($this->request->data['PlanoPrevio'] as $key=> &$planoPrevio ){
                    
                     
                        
                        unset($planoPrevio['TipoArchivo']);
                        unset($planoPrevio['TipoPlano']);
                        //muevo el archivo nuevo 
                       if($planoPrevio['codigo_archivo']){
                           
                           $codigo_temporal=$planoPrevio['codigo_archivo'];
                           $nombre_archivo=$planoPrevio['nombre_original'];
                           $cui=$planoPrevio['cui'];
                           $partes = explode(".", $nombre_archivo); 
                           $extencion = end( $partes ); 
                           $nombre_descarga=$cui."_".mb_strtoupper($extencion)."_".$codigo_temporal.".".$extencion;
                           $dir_temp = new Folder(APP.'FilesPlanosTemp');
                           $dir_definitivo= $this->directorioDescarga(EnumTipoPlano::PlanoPrevio);
                           $origen=$dir_temp->path .'\\'.$codigo_temporal.'-'.$nombre_archivo;
                           $destino=$dir_definitivo.'\\'.$nombre_descarga;
                           $file_size=filesize($origen);
                           
                           //si NO movio bien el archivo borro toda la subida de ese plano
                           if(!rename($origen,$destino)){
                              unset($planoPrevio);
                           }else{
                               $planoPrevio['id_tipo_plano']   = EnumTipoPlano::PlanoPrevio;
                               $planoPrevio['tamanio_kb']      = $file_size;
                               $planoPrevio['fecha_carga']     = date('Y-m-d H:i:s');
                               $planoPrevio['id_predio']       = $id_predio;
                               $planoPrevio['nombre_original'] = $nombre_archivo;
                               $planoPrevio['nombre_descarga'] = $nombre_descarga;
                               
                           }
                           
                           
                       }else{
                           if($planoPrevio['id']){
                             $planoPrevio['id_tipo_plano']   = EnumTipoPlano::PlanoPrevio;
                             $planoPrevio['id_predio']       = $id_predio;    
                           }else{
                             unset($planoPrevio); //se salteo el control JS por algun motivo lo borro 
                           }
                          
                           
                           
                           
                       }
                       
                        
                        
                    }
            
            }
            if (array_key_exists('PlanoCenie', $this->request->data)){  
                foreach($this->request->data['PlanoCenie'] as  &$planoCenie ){
                    
                     
                    
                    unset($planoCenie['TipoArchivo']);
                    unset($planoCenie['TipoPlano']);
                    //muevo el archivo nuevo 
                   if($planoCenie['codigo_archivo']){
                       
                       //borro el que ya tiene porque me esta mandando otro
                       if($planoCenie['id'])
                        $this->borrarArchivoPlano($planoCenie['id']);
                       
                       $codigo_temporal=$planoCenie['codigo_archivo'];
                       $nombre_archivo=$planoCenie['nombre_original'];
                       $cui=$planoCenie['cui'];
                       $partes = explode(".", $nombre_archivo); 
                       $extencion = end( $partes ); 
                       $nombre_descarga=$cui."_".mb_strtoupper($extencion)."_".$codigo_temporal.".".$extencion;
                       $id_predio=$id;
                       $dir_temp = new Folder(APP.'FilesPlanosTemp');
                       $dir_definitivo= $this->directorioDescarga(EnumTipoPlano::PlanoCenie);
                       $origen=$dir_temp->path .'\\'.$codigo_temporal.'-'.$nombre_archivo;
                       $destino=$dir_definitivo.'\\'.$nombre_descarga;
                       $file_size=filesize($origen);
                       
                       //si NO movio bien el archivo borro toda la subida de ese plano
                       if(!rename($origen,$destino)){
                          unset($planoCenie);
                       }else{
                           $planoCenie['id_tipo_plano']   = EnumTipoPlano::PlanoCenie;
                           $planoCenie['tamanio_kb']      = $file_size;
                           $planoCenie['fecha_carga']     = date('Y-m-d H:i:s');
                           $planoCenie['id_predio']       = $id_predio;
                           $planoCenie['nombre_original'] = $nombre_archivo;
                           $planoCenie['nombre_descarga'] = $nombre_descarga;
                           
                       }
                       
                       
                   }
                   
                    
                    
                }
           } 
      
           
           
                
            //$this->procesarDatos();
            
           
            
            
           
            
           
            
            
            
            try {
            
                    $borrar_planos_previos = $this->PlanoPrevio->find('list', array('conditions'=>array('NOT' => array('PlanoPrevio.id' => $planosPreviosId),'PlanoPrevio.id_predio' => $id,'PlanoPrevio.id_tipo_plano' => 1), 'contain'=>array()));
                    $borrar = array();
                    foreach($borrar_planos_previos as $plano_previo){
                        $this->borrarArchivoPlano($plano_previo);
                        array_push($borrar, array('PlanoPrevio.id'=>$plano_previo));
                        
                    }    
                    $this->PlanoPrevio->deleteAll($borrar);
                
                    $borrar_planos_cenie = $this->PlanoCenie->find('list', array('conditions'=>array('NOT' => array('PlanoCenie.id' => $planosCenieId),'PlanoCenie.id_predio' => $id,'PlanoCenie.id_tipo_plano' => 2), 'contain'=>array()));
                    $borrar = array();
                    foreach($borrar_planos_cenie as $plano_cenie){
                        $this->borrarArchivoPlano($plano_cenie);
                        array_push($borrar, array('PlanoCenie.id'=>$plano_cenie));
              
                    }
                     $this->PlanoCenie->deleteAll($borrar);
                     
                    // borro las PlantaResumenPlano relacionadas con el ResumenPlano 
                    $borrar_presumen_planos = $this->ResumenPlano->PlantaResumenPlano->find('all', array('conditions'=>array('NOT' => array('PlantaResumenPlano.id' => $presumenPlanosId),'PlantaResumenPlano.id_resumen_plano' => $this->request->data['Predio']['ResumenPlano']['id']), 'contain'=>array()));
                    
                    $borrar = array();
                    foreach($borrar_presumen_planos as $presumen_plano)
                        array_push($borrar, array('PlantaResumenPlano.id'=>$presumen_plano['PlantaResumenPlano']['id']));
                    $this->ResumenPlano->PlantaResumenPlano->deleteAll($borrar);
                     
                    
                if ($this->Predio->saveAll($this->request->data, array('deep' => true))){
                    $this->Session->setFlash('El Archivo de Planos ha sido guardado exitosamente.', 'success');
                    $this->redirect(array('action' => 'index'));
                }
                else {
                    $this->Session->setFlash('Ha ocurrido un error, el Archivo de Planos no se ha guardado.', 'error');
                    $this->redirect(array('action' => 'index'));
                    //print_r($this->Predio->validationErrors);
                }
            } catch(Exception $ex) {
                echo $ex->getMessage();
                $this->Session->setFlash($ex->getMessage(), 'error');
                $this->redirect(array('action' => 'index'));
            }
            //$this->redirect(array('action' => 'index'));
        } 
        
        $this->redirect(array('action' => 'abm', 'M', $id));
    }

    function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allow('uploadFile');
    }
    
    /**
    * @secured(READONLY_PROTECTED)
    */    
    function borrarArchivoPlano($id){
       
        $this->loadModel("Plano");
        $PlanoPredio = $this->Plano->find('all',array('conditions' => array('Plano.id' => $id),'fields' => array('Plano.id','Plano.nombre_descarga','Plano.id_predio','Plano.id_tipo_plano')));      
        if(file_exists($this->directorioDescarga($PlanoPredio[0]['Plano']['id_tipo_plano']).'\\'.$PlanoPredio[0]['Plano']['nombre_descarga']))
            unlink($this->directorioDescarga($PlanoPredio[0]['Plano']['id_tipo_plano']).'\\'.$PlanoPredio[0]['Plano']['nombre_descarga']);
        
        return true;
        
    }

    /**
    * @secured(READONLY_PROTECTED)
    */
    function uploadFile(){
        
    
        
        
        $dir_temp = new Folder(APP.'FilesPlanosTemp');
        
        $random=$this->request->data['random'];
      
        if (!empty($_FILES)) {
            $tempFile =$this->request->params['form']['Filedata']['tmp_name'];
            
            $destino=$dir_temp->path .'\\'.$random.'-'.$this->request->params['form']['Filedata']['name'];
            // Validate the file type
            $fileTypes = array('jpg','JPG','jpeg','JPEG','dwg','DWG','pdf','PDF','bmp','BMP','tif','TIF','tiff','TIFF','mcd','MCD'); // File extensions
            $fileParts = pathinfo($this->request->params['form']['Filedata']['name']);
            
            if (in_array($fileParts['extension'],$fileTypes)) {
                move_uploaded_file($tempFile,$destino);
                echo '1';
            } else {
                echo 'Invalid file type.';
            }
            
            
            
        }
        
      die();  
    }
    
   function directorioDescarga($id_tipo_plano){
        $this->loadModel('TipoPlano');
        $TipoPlano = $this->TipoPlano->find('all',array('conditions' => array('TipoPlano.id' => $id_tipo_plano),'fields' => array('TipoPlano.path_fsystem')));     
        
       $path = $TipoPlano[0]['TipoPlano']['path_fsystem'];
       $dir_definitivo = Folder::addPathElement(APP.'FilesPlanos'.'\\'.$path, $this->request->data['Predio']['id']);
       //if(EnumTipoPlano::PlanoCenie)
        
       if (!file_exists($dir_definitivo)) {
            mkdir($dir_definitivo, 0777, true);
       }
       
       return $dir_definitivo;
       
   }
   
    
    public function download($id_plano) {
        
        $this->loadModel("Plano");
        $PlanoPredio = $this->Plano->find('all',array('conditions' => array('Plano.id' => $id_plano),'fields' => array('Plano.id','Plano.nombre_descarga','Plano.id_predio','Plano.id_tipo_plano')));      
        
        $this->viewClass = 'Media';
        // Download app/outside_webroot_dir/example.zip
        
        if($PlanoPredio) {
            $this->request->data['Predio']['id']=$PlanoPredio[0]['Plano']['id_predio'];
            $link_descarga = $this->directorioDescarga($PlanoPredio[0]['Plano']['id_tipo_plano']);
            $file=array();
            $extension=pathinfo($PlanoPredio[0]['Plano']['nombre_descarga']);
            $file['extension']=$extension['extension'];
            $file['nombre']=$PlanoPredio[0]['Plano']['nombre_descarga'];
            $file['path']=$link_descarga;
            $partes=explode(".",$PlanoPredio[0]['Plano']['nombre_descarga']);
            
         
            
            if(file_exists($this->directorioDescarga($PlanoPredio[0]['Plano']['id_tipo_plano']).'\\'.$PlanoPredio[0]['Plano']['nombre_descarga'])){
                
            
                $params = array(
                    'id'        => $file['nombre'],
                    'name'      => $partes[0],
                    'download'  => true,
                    'extension' => $file['extension'],
                    'path'      => $file['path'] . DS
                );
                return $this->set($params);
            }
        }
        
    }
    
    
    

}
?>