<?php

class EnumEstadoCheque
{

	const 	Cartera     = 1;
	const	Aplicado    = 2;
	const	Emitido     = 3;
	const	Diferido    = 4;
	const   Rechazado   = 5;
	const   Anulado     = 6;
	const   PendienteAcreditacion    = 7;
	const	Acreditado    = 8;
}

  
?>