<?php
App::uses('AppModel', 'Model');

class RRHHConceptoClaseLegajo extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'rrhh_concepto_clase_legajo';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
       
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
        'RRHHConcepto' => array(
            'className' => 'RRHHConcepto',
            'foreignKey' => 'id_rrhh_concepto',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'RRHHClaseLegajo' => array(
            'className' => 'RRHHClaseLegajo',
            'foreignKey' => 'id_rrhh_clase_legajo',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
   );
   
   
   
   

}
