<?php

App::uses('ArticuloController', 'Controller');

class OperacionesInternaController extends ArticuloController {
    public $name = 'OperacionesInterna';
    public $model = 'OperacionInterna';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $tipo_producto = EnumProductoTipo::OPERACIONINTERNA;
    public $nombre_OPERACION_INTERNA = "Operaciones Interna";
    
    
    
    /**
    * @secured(CONSULTA_OPERACION_INTERNA)
    */
    public function index() {
        
       
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = array();
        $this->RecuperoFiltros($conditions);
        
        
        array_push($conditions, array($this->model.'.id_producto_tipo =' => $this->tipo_producto)); 
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' =>array('FamiliaProducto','Origen','Marca','Iva','Categoria', 'ListaPrecioProducto'=>array('conditions'=>array('ListaPrecioProducto.id_lista_precio'=>$this->id_lista_precio),"ListaPrecio","Moneda"),'Unidad','CuentaContableVenta','CuentaContableCompra','ProductoTipo'=>array('DestinoProducto'),'Area'),
			// 'contain' =>array('ArticuloRelacion'/*=>array('Componente'=>array('Categoria')*/), 'FamiliaProducto','StockProducto','Origen'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.codigo desc'
        );
      
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
       //parseo el data para que los models queden dentro del objeto de respuesta
         foreach($data as &$valor){
               
             $this->cleanforOutput($valor,2);
         }
         
         $this->data = $data;
         
         
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
    //fin vista json
        
    }
    
   
    
     /**
    * @secured(CONSULTA_OPERACION_INTERNA)
    */
  public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    } 
    
   private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
      
      $this->set('model',$model);
      $this->set('model_name',$this->model);
      Configure::write('debug',0);
      $this->render($vista);
   
    }  
    
    
  
  
  
  function addProductoEnTodasLasListasDePrecios($id_OPERACION_INTERNA){
      
      //utilizar la utilidad de la lista y basarse en el costo
      
  }
  
  
 
   
     /**
    * @secured(ADD_OPERACION_INTERNA)
    */
    public function add() {
        
        parent::add();
        return;
        
    }
    
    
     /**
    * @secured(ADD_OPERACION_INTERNA)
    */
    public function edit($id) {
        
        parent::edit($id);
        return;
        
    }
    
    /**
    * @secured(BAJA_OPERACION_INTERNA)
    */
    function delete($id) {
        parent::delete($id);
        return;
    }
    
    
    
    
    /**
    * @secured(CONSULTA_OPERACION_INTERNA)
    */
   public function existe_codigo(){
       
       
       parent::existe_codigo();
       return;
   }
   
   
   
    /**
    * @secured(CONSULTA_OPERACION_INTERNA)
    */
  public function getCosto($id_articulo){
    parent::getCosto($id_articulo);
  }
   
  
    
    
}
?>