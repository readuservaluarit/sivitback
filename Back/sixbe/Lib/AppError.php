<?php 


App::uses('CakeLog', 'Log');



/* Para lo que el front reciba
 si estas cargando un combo y recibis el 100 entonces tenes que hacer el algoritmo del paginado
si recibis 101 es un excel y mostras el mensaje
si recibis 0 no es memoria es otra cosa
 */

class AppError {
	public static function handleError($code, $description, $file = null,
			$line = null, $context = null) {
		
				
				$codigo_error = 0;
				list(, $level) = ErrorHandler::mapErrorCode($code);
				
				//(LOG_EMERG, LOG_ALERT, LOG_CRIT, LOG_ERR)
				
				
				//$_SERVER["REQUEST_URI"] ANALIZAR si contiene excelExport
				
				$pos = strpos($_SERVER["REQUEST_URI"], 'excelExport');
				
				
				$logMessage = 'Fatal Error (' . $code . '): ' . $description . ' in [' . $file . ', line ' . $line . ']'.json_encode($context);
				
				if (strpos($description, 'memory size') !== false) {
					
					
					$logMessage ="Supera la cantidad maxima de registros disponibles para exportar.Soluci&oacute;n: Disminuya la cantidad de resultados y exportelos nuevamente";
					
					if($pos == false)
						$codigo_error = 100 ;
					else
						$codigo_error = 101;//Excel Error
					
				}else{
					
					$codigo_error = 0;//sin determinar
				}
				CakeLog::write(LOG_ERR, $logMessage);
				
				
				if ($level === LOG_ERR) {
					// Ignore fatal error. It will keep the PHP error message only
					
				
					
					
					
					$output = array(
							"status" => EnumError::ERROR,
							"message" => $logMessage,
							"content" => "",
							"page_count" =>0,
							"context"=>$context,
							"codigo_error"=>$codigo_error,
							"error_level"=>$level
					);
					
					header('Content-Type: application/json');
					header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
					header("Cache-Control: no-cache");
					header("Pragma: no-cache");
					
					echo json_encode($output);
					//die();
					
					
					
					return false;
				}else{
					
					$debug = Configure::read('debug');
					if($debug >0){
						
						$output = array(
								"status" => EnumError::ERROR,
								"message" => $logMessage,
								"content" => "",
								"page_count" =>0,
								"context"=>$context,
								"codigo_error"=>$codigo_error,
								"error_level"=>$level
						);
						
		
						
						header('Content-Type: application/json');
						header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
						header("Cache-Control: no-cache");
						header("Pragma: no-cache");
						echo json_encode($output);
						//die();
						
					}
					
				}
				
				
				
	}
}


?>