Cuando puedan por favor
p�senme la cantidad de v�lvulas producidas por Orden Armado
a partir de abril de 2016. 
Entiendo que esta informaci�n la pueden obtener a trav�s de las OA cerradas.

SELECT SUM(cantidad) FROM orden_armado
WHERE fecha_cierre  >'2016-05-01'
ORDER BY  fecha_cierre ASC

SELECT * FROM orden_armado OA
WHERE OA.fecha_cierre  > '2016-05-01' /*3094*/
AND OA.id_pedido_interno > 0     /*2889*/
AND OA.id_producto> 0     /*2889*/
ORDER BY  OA.fecha_cierre ASC

SELECT * FROM orden_armado OA
JOIN comprobante C ON C.id = OA.`id_pedido_interno`
JOIN comprobante_item CI ON CI.`id_comprobante` = C.id 
			AND CI.`id_producto` 	= OA.`id_producto`
WHERE OA.fecha_cierre  > '2016-05-01' /*3094*/
AND OA.id_pedido_interno > 0     /*2889*/
ORDER BY  OA.fecha_cierre ASC




/*FINAL */
SELECT OA.id, OA.fecha_emision, OA.fecha_entrega, OA.fecha_cierre, C.codigo, C.d_producto, OA.cantidad, OA.observacion  
FROM orden_armado OA
JOIN producto C ON C.id = OA.id_producto
WHERE OA.fecha_cierre  > '2016-05-01' /*3164*/
AND OA.id_estado = 9 /*CERRADA*/
ORDER BY  OA.fecha_cierre ASC