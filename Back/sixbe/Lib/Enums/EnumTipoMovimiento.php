<?php

class EnumTipoMovimiento
{
    const IngresoMateriaPrima = 100;
    const EgresoMateriaPrima = 101;
    const IngresoMateriaPrimaNoConforme = 102;
    const DevolucionMateriaPrimaAlProveedor = 103;
    
    const RecepcionComponentes = 200;
    const EgresoComponente = 201;
    const DeteccionComponenteNc = 203;
    const ReprocesoComponente = 204;
    const ComponenteScrap = 205;
    const EgresoComponenteScrap = 206;
    
    const RecepcionProducto = 300;
    const ComprometerseACumplirPedido = 301;
    const DespachoMercaderia = 302; //SACAR
    const DeteccionProductoNc = 303;
    const ReprocesoProducto = 304;
    const ProductoScrap = 305;
    const EgresoProductoScrap = 306;
    
    const PedidoInterno = 400; //SACAR
    const CanceloPedidoInterno = 401;//SACAR
    const CanceloItemPedidoInterno = 402;//SACAR
    const AgregoItemPedidoInterno = 403;//SACAR
    const AjustePositivoPedidoInternoItem = 404;//SACAR
    const AjusteNegativoPedidoInternoItem = 405;//SACAR
    
    const RetirodeMateriaPrimaPorOrdenProduccion = 500;
    const RetirodeMateriaPrimaPorOrdenProduccionViejo = 5000;//MODELO VIEJO al 27-06-2018
    const IngresoDeComponenteParcialPorOrdenProduccion = 501; // NUEVO
    const IngresoDeComponenteParcialPorOrdenProduccionViejo = 5010; // NUEVO
    const IngresoMateriaPrimaSobrantePorOrdenProduccion = 502;
    const IngresoMateriaPrimaSobrantePorOrdenProduccionViejo = 5020;
    const IngresoDeComponentePorOrdenProduccion = 503; //VIEJO 501
    
    
    
    const OrdenArmadoAlta = 600;
    
    /* CHEQUEO - OA* RECORDAR EN OA DEFINIR EL MISMO DEPOSITO ORIGEN Y DESTINO PARA EL CIERRE Y EL AJUSTE COMO ESTA EN SIX_TEST2/*/

    const OrdenArmadoCierre = 601;
    const IngresoComponenteSobrantexOrdenArmado = 602;
    const OrdenArmadoAjuste = 603;
    
    
    const Remito = 700;//SACAR
    const CanceloRemito = 701;//SACAR
    const CanceloItemRemito = 702;//SACAR
    const AgregoItemRemito = 703; //SACAR
    const AjustePositivoRemitoItem = 704;//SACAR
    const AjusteNegativoRemitoItem = 705;//SACAR
    
    
    
    //generales para los comprobantes
    const Comprobante = 800;
    const CanceloComprobante = 801;
    const CanceloItemComprobante = 802;
    const AgregoItemComprobante = 803;
    const AjustePositivoComprobanteItem = 804;
    const AjusteNegativoComprobanteItem = 805;
    const RevertirLoteItems = 807;
    

    const AjusteStock = 1001;
    const AjusteStockNegativo = 1002;
    const AjusteStockComprometidoPositivo = 1003;
    const AjusteStockComprometidoNegativo = 1004;
    
    //MOVIMIENTOS MANUALES
    const Ajuste_MP_ComprometidoPositivo1551 = 1551;
    const Ajuste_MP_ComprometidoNegativo1552 = 1552;
    const Ajuste_C_ComprometidoPositivo1601 = 1601;
    const Ajuste_C_ComprometidoNegativo1602 = 1602;
    const Ajuste_P_ComprometidoPositivo1301 = 1301;
    const Ajuste_P_ComprometidoNegativo1302 = 1302;
    
     //REVERTIR
    const Inversodel500 = 1500;
    const Inversodel501 = 1501;
    const Inversodel600 = 1600;

    //const ProductoComprometido = 1002; //MP: Antes era el 1002 -> podria ser el 301 q pertenecea aproducto
}

  
?>
