<?php

    class CacheController extends AppController {
    
        public $helpers = array ('Session', 'Paginator', 'Js');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

        public function index() {
         /*   
         $controllers = App::objects('controller');
         
         
         foreach($controllers as $controller){
           
           $num_chars= strlen($controller);  
           $controller_name = substr($controller,0,$num_chars-10);
           $controller_excluir = array("TestControladores","Auditorias","PersonasCategorias","App");
           
           echo $controller_name;
           echo "<br>";
           
           if(!in_array($controller_name,$controller_excluir)){
               
               
                    
                   //var_dump($annotations);
                try{
                $resultado = $this->requestAction(
                        array('controller' => $controller_name.'.json'),
                        array('data' => null)
                );
                }catch(Exception $e){
                    
                    echo $e->getMessage();
                }
            
            }
         }   */

        }
        
       
       
       protected function indexModels() {
       	
	       	$models= App::objects('model');
	       	
	       	echo json_encode($models);
	       	die();
       }
       
       
       protected function indexControladores() {
       	
       	
			$controllers = App::objects('controller');
			
			
			$models = array();
			$model_controller = array();
			
			$this->loadModel("Entidad");
			
			
			
			foreach($controllers as $controller){
				
				
				if($controller !="TestControladoresController"){
				
					
					try{
						App::uses($controller, 'Controller');
						
						$reflectionClass = new ReflectionClass($controller);
						$obj = $reflectionClass->newInstanceArgs();
						
						
						if( isset($obj->model) ){
						
							
							App::uses($obj->model, 'Model');
							
							$reflectionClassModel = new ReflectionClass($obj->model);
							$objModel = $reflectionClassModel->newInstanceArgs();
							

							//echo $obj->model.'  '.$controller.'</br>';
							array_push($models, $obj->model);
							$mc["Entidad"]["d_entidad"] = $objModel->useTable;
							$mc["Entidad"]["model"] = $obj->model;
							$mc["Entidad"]["controller"] = $controller;
							
							
							
							$entidad = $this->Entidad->find('first', array(
									'conditions' => array(
											'Entidad.d_entidad' =>$mc["Entidad"]["d_entidad"],
											'Entidad.controller' =>$mc["Entidad"]["controller"]
									) ,
									'contain' => false
							));
							

							if(count($mc["Entidad"]["controller"])>0){
								
									if(!$entidad){//OJO no esta en la tabla
										
										array_push($model_controller, $mc);
									
									}else{//si esta actualizo el controller
										

										$this->Entidad->query("UPDATE entidad set controller='".$mc["Entidad"]["controller"]."', model='".$mc["Entidad"]["model"]."' where d_entidad='".$mc["Entidad"]["d_entidad"]."';");
										/*$this->Entidad->update(
												array('Entidad.controller' => "'".$mc["Entidad"]["controller"]."'",'Entidad.model' => "'".$mc["Entidad"]["model"]."'"),
												array('Entidad.d_entidad' =>   $mc["Entidad"]["d_entidad"]) );*/
									}
							
							}
							
						}
				
						}catch (Exception $e){
							echo $e->getMessage();
						}
				}
			
			}

			if(isset($model_controller) && count($model_controller)>0)
				$this->Entidad->saveAll($model_controller);
			
			echo json_encode($model_controller);
	   
			die();
       }
       
     
    public function beforeFilter(){
        
		App::uses('ConnectionManager', 'Model');

		$this->Auth->allow('index','actualizaBDConControllers');

		parent::beforeFilter();
        
    }
    
     
    protected function actualizaBDConControllers(){
        
       $this->loadModel("Controlador");
       
       $this->Controlador->query('TRUNCATE controlador;');
       
        $controllers = App::objects('controller');
         
        $controller_name_array = array();
         
         foreach($controllers as $controller){
           
           $num_chars= strlen($controller);  
           $controller_name = substr($controller,0,$num_chars-10);
           $array["Controlador"]["d_controlador"] = $controller_name ;
           array_push($controller_name_array,$array);
           
         }
        
        $this->Controlador->SaveAll($controller_name_array);
        die();
    }
    

    public function creaCacheModel($id_usuario=1) {
    	
		/* set_time_limit(60000);*/
		
    	unlink (EnumCacheName::getModel);
    	
    	ob_start();
    	
    	$this->loadModel("Entidad");
    	$this->loadModel(EnumModel::DatoEmpresa);
    	$this->loadModel(EnumModel::Modulo);
    	
    	$modulos_habilitados = $this->{EnumModel::Modulo}->find('all', array(
    			'conditions' => array("Modulo.habilitado"=>1),
    			'contain' =>false
    	));
    	
    	
    	
    	$array_id_modulo_habilitado = array();
    	
    	foreach ($modulos_habilitados as $modulo_h){
    		
    		
    		
    		array_push($array_id_modulo_habilitado, $modulo_h["Modulo"]["id"]);
    		
    	}
    	
    	/*Habilito todos para generar cache*/
    	$this->{EnumModel::Modulo}->updateAll(
    			array('Modulo.habilitado' => 1));
    	
    	
    	
    	
    	
    	
    	
    	
    	$entidades = $this->Entidad->query(
	"SELECT DISTINCT TRIM(REPLACE(entidad.`controller`,'Controller','')) AS controller,entidad.id,entidad.`d_entidad`,entidad.`model`,entidad.`estatica_sistema`,entidad.`tiene_cache_cliente` FROM entidad");
    	
    	try{
    			
    	$_SESSION["creando_cache"] = 1;
    	    		
    	$array_model = array();
    	foreach($entidades as &$entidad){
    			
    		$controller_name = $entidad[0]["controller"];
    		$model_name = $entidad["entidad"]["model"];
    		
    		$controller_excluir = array("TestControladores","Auditorias","App","Funciones","Roles","QaConformidades","Reclamos","MisDatos",
			"ConfiguracionAplicaciones","MediaFiles","NotasUsuario","ComprobanteItemComprobantes","ComprobanteItems","Comprobantes","CuentasCorriente",
			"EstadosPersona","Personas","OrdenesProduccion","TurCotizaciones","TurCotizacionItems","CotizacionesCompra","TurCotizacionConceptos");
    		
    		$controllers = array("OrdenesTrabajoItems");
    	 
    		if(!in_array($controller_name,$controller_excluir)){
    		
			/* va a haber tantas cache como rol alla. El archivo */
    			
    			try{
					$nombres_vistas = $this->getNombresFromView($controller_name,$model_name);
    				/*
    				if(count($nombres_vistas) == 0){
    					echo $controller_name;
    					die();
    				}
    				*/
    				
    				foreach($nombres_vistas as $view){
    			
    					try{
    					//$this->log($controller_name);
					    		$resultado = $this->requestAction(
					    				array('controller' => $controller_name.'/getModel/'.$view.'.json' /*, 'action' => '' */),
					    				array('data' => null)
					    				);
					    		
    					}catch (Exception $e){
    						$aaa;
    					}
						
						$resultado2 = json_decode($resultado);

						$entidad2 =  array();
						
						if($resultado2->status == EnumError::SUCCESS ){
							
							$entidad2["entidad"]["controllerName"] = $controller_name;
							$entidad2["entidad"]["getmodel"] = $resultado2->content;
							$entidad2["entidad"]["modelName"] = $model_name;
							$entidad2["entidad"]["viewName"] = $view;
						
							array_push($array_model, $entidad2);

						}else{
							
							if($resultado != null){
									//echo var_dump($controller_name);
									//echo var_dump($resultados2);
							}else{
								//var_dump($controller_name);
								//die();
							}
						
						}
		    		
    				}
		    		

    			}catch(Exception $e){
    				
    				$e;
    			}
					if($entidad["entidad"]["estatica_sistema"] == 1){
						/*
						try{
							$resultado = $this->requestAction(array('controller' => $controller_name, 'action'=>'index.json'));
							$resultado_index = json_decode($resultado);
							//$entidad2["entidad"]["getmodelIndex"] = json_encode($resultado_index->content);
							$entidad2["entidad"]["index"] = json_encode($resultado_index->content);
							}catch(Exception $e)
							{
							}
						*/
		    		}	
    		}
    	}//fin for
    	}catch(Exception $e){
    		$e;
    	}
    	ob_clean();
    	try{
    		//Cache::write(EnumCacheName::getModel,  serialize($array_model));
    		
    		file_put_contents(EnumCacheName::getModel,serialize($array_model));
    		
    	}catch(Exception $e){
    		
    		/*
    		Cache::config(EnumCacheName::getModel, array(
    				'engine' => "File",
    				'path' => CACHE . 'index' . DS
    		));
    		Cache::write(EnumCacheName::getModel,  serialize($array_model));
    		*/
    	}
    	
    	$datoEmpresa = $this->DatoEmpresa->find('first', array('conditions' => array('DatoEmpresa.id' => 1), 'contain' => false));
    	
    	
    	$output = array(
    			"status" =>EnumError::SUCCESS,
    			"message" => "",
    			"content" => "",
    			"product_version" => $datoEmpresa["DatoEmpresa"]["product_version"],
    	);
    	
    	
    	
    	/*Pongo a todos en cero para luego poner en 1 solo los que le corresponden al cliente*/
    	$this->{EnumModel::Modulo}->updateAll(
    			array('Modulo.habilitado' => 0));
    	
    	
    	$this->{EnumModel::Modulo}->updateAll(
    			array('Modulo.habilitado' => 1),array("Modulo.id IN"=>$array_id_modulo_habilitado));
    	
    	
    	
    	echo json_encode($output);
    	die();
   
    }
    

    protected function creaCacheIndex($id_usuario=1) {
    	
    	ob_start();
    	
    	$this->loadModel("Entidad");
    	
    	$entidades = $this->Entidad->query(
		"SELECT DISTINCT REPLACE(entidad.`controller`,'Controller','') AS controller,entidad.id,
		entidad.`d_entidad`,entidad.`model`,entidad.`estatica_sistema`,entidad.`tiene_cache_cliente` FROM entidad");

    	try{
    		$array_index = array();
    		foreach($entidades as &$entidad){

    			$controller_name = $entidad[0]["controller"];
    			$model_name = $entidad["entidad"]["model"];
    			
    			$controller_excluir = array("TestControladores","Auditorias","App","Funciones","Roles","QaConformidades",
				"Reclamos","MisDatos","ConfiguracionAplicaciones","MediaFiles","NotasUsuario","ComprobanteItemComprobantes",
				"ComprobanteItems","Comprobantes","CuentasCorriente","EstadosPersona","Personas","PersonasCategorias","OrdenesProduccion",
				"TurCotizaciones","TurCotizacionItems","Reparaciones","CotizacionesCompra","TurCotizacionConceptos");
    			
    			$controllers = array("OrdenesTrabajoItems");
    			
    			if(!in_array($controller_name,$controller_excluir)){
    				
    				/* va a haber tantas cache como rol alla. El archivo */
    				try{
						if($entidad["entidad"]["estatica_sistema"] == 1){
							$resultado = $this->requestAction(array('controller' => $controller_name, 'action'=>'getCache.json'));
							$resultado_index = json_decode($resultado);
					
							$entidad2 =  array();
						
							if($resultado2->status == EnumError::SUCCESS){
								
								$entidad2["entidad"]["controllerName"] = $controller_name;
								$entidad2["entidad"]["index"] = $resultado2->content;
								$entidad2["entidad"]["modelName"] = $model_name;
								$entidad2["entidad"]["viewName"] = $view;
								
								array_push($array_index, $entidad2);

							}else{
								
								if($resultado != null){
									//echo var_dump($controller_name);
									//echo var_dump($resultados2);
								}else{
									//var_dump($controller_name);
									//die();
								}
							}
						}
    				}catch(Exception $e){
    					
    					$e;
    				}
    			}
    		}//fin for

    	}catch(Exception $e){
		$e;
    	}
    	ob_clean();

    	try{
    		//Cache::write(EnumCacheName::getModel,  serialize($array_model));
    		//file_put_contents(APP."webroot/media_file/cache_get_index.txt",serialize($array_index));
    	}catch(Exception $e){
    		/*
    		 Cache::config(EnumCacheName::getModel, array(
    		 'engine' => "File",
    		 'path' => CACHE . 'index' . DS
    		 ));
    		 Cache::write(EnumCacheName::getModel,  serialize($array_model));
    		 */
    	}
    	echo json_encode($array_index);
    	die();
    }
    
    public function creaStaticCacheIndex($id_usuario=1) {
    	
    	
    	ob_start();
    	
    	$this->loadModel("Entidad");
    	$this->loadModel(EnumModel::DatoEmpresa);
    	

    	$entidades = $this->Entidad->query("SELECT DISTINCT REPLACE(entidad.`controller`,'Controller','') AS controller,entidad.id,entidad.`d_entidad`,entidad.`model`,entidad.`estatica_sistema`,entidad.`tiene_cache_cliente` FROM entidad WHERE estatica_sistema=1");
    	
    	
    	
    	$datoEmpresa = $this->DatoEmpresa->find('first', array(
    			'conditions' => array(
    					'DatoEmpresa.id' =>1
    					
    			) ,
    			'contain' => false
    	));
    	
    	
    	$url_web_service = $datoEmpressa["DatoEmpresa"]["app_path"];
    	
    	try{
    		
    		
    		$array_index = array();
    		
    		
    		foreach($entidades as &$entidad){
    			
    			
    			
    			
    			$controller_name = $entidad[0]["controller"];
    			$model_name = $entidad["entidad"]["model"];
    			
    			
    			
    			
    			
    			
    			$controller_excluir = array("TestControladores","Auditorias","App","Funciones","Roles","QaConformidades","Reclamos","MisDatos","ConfiguracionAplicaciones","MediaFiles","NotasUsuario","ComprobanteItemComprobantes","ComprobanteItems","Comprobantes","CuentasCorriente","EstadosPersona","Personas","PersonasCategorias","OrdenesProduccion","TurCotizaciones","TurCotizacionItems","Reparaciones","CotizacionesCompra","TurCotizacionConceptos");
    			
    			$controllers = array("OrdenesTrabajoItems");
    			
    			if(!in_array($controller_name,$controller_excluir)){
    				
    				
    				
    				
    				/* va a haber tantas cache como rol alla. El archivo */
    				
    				try{
    					
    					
    					
    					
    					
    					
    					if($entidad["entidad"]["estatica_sistema"] == 1){
    						
    						
    						$resultado = $this->requestAction(array('controller' => $controller_name, 'action'=>'getCache.json'));
    						
    						
    						
    						
    						/*
    						 $entidad2 =  array();
    						 
    						 
    						 if($resultado2->status == EnumError::SUCCESS){
    						 
    						 $entidad2["entidad"]["controllerName"] = $controller_name;
    						 $entidad2["entidad"]["index"] = $curl_json->content;
    						 $entidad2["entidad"]["modelName"] = $model_name;
    						 $entidad2["entidad"]["viewName"] = $view;
    						 
    						 
    						 array_push($array_index, $entidad2);
    						 
    						 
    						 
    						 }else{
    						 
    						 if($resultado != null){
    						 //echo var_dump($controller_name);
    						 //echo var_dump($resultados2);
    						 }else{
    						 //var_dump($controller_name);
    						 //die();
    						 }
    						 
    						 }
    						 
    						 
    						 
    						 */
    						
    					}
    					
    					
    				}catch(Exception $e){
    					
    					$e;
    				}
    				
    				
    				
    				
    				
    				
    			}
    			
    		}//fin for
    		
    		
    		
    		
    	}catch(Exception $e){
    		
    		$e;
    		
    	}
    	
    	
    	ob_clean();
    	
    	
    	
    	try{
    		//Cache::write(EnumCacheName::getModel,  serialize($array_model));
    		
    		//file_put_contents(APP."webroot/media_file/cache_get_index.txt",serialize($array_index));
    		
    	}catch(Exception $e){
    		
    		/*
    		 Cache::config(EnumCacheName::getModel, array(
    		 'engine' => "File",
    		 'path' => CACHE . 'index' . DS
    		 ));
    		 Cache::write(EnumCacheName::getModel,  serialize($array_model));
    		 */
    		
    		
    	}
    	
    	
    	
    	
    	
    	
    	echo json_encode($array_index);
    	die();
    	
    	
    	
    }
    
    protected function getNombresFromView($controllerName,$model_name){
    	
    	
    	$model_name = Inflector::underscore($model_name);
    	//debo camilar el Nombre del model y excluir esa view si es que existe
    	
    	$dir_name = APP.'View/'.$controllerName.'/json/';
    	$directorio_view = scandir($dir_name);
    	//busco los nombres de los archivos dentro de la carpeta View/<Controller>/json/
    	
    	$array_view = array();
    	//array_push($array_view, "default");
    	
    	foreach ($directorio_view as $file){
    	
    		if(!in_array($file,array(".","..")) && basename($file,".ctp")!= $model_name){
    			array_push($array_view, basename($file,".ctp"));
    		}
    	}
    	


    	if(count($array_view) == 0)
    		array_push($array_view, "default");
    	return $array_view;
    	
    }
    
    
    public function getStaticCacheIndex(){
    	
    	
    	
    	
    	$getIndexCache = unserialize(file_get_contents(EnumCacheName::getCacheIndexTablasEstaticaSistema));
    	
    	
    	
    	$output = array(
    			"status" =>EnumError::SUCCESS,
    			"message" => "list",
    			"content" => $getIndexCache,
    			"page_count" =>0
    	);
    	
    	
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message", "page_count", "content")); 
    	
    	
    	
    	
    	
    }
    
    
    public function  ClearCache() {
    	
    	
    	parent::ClearCache();
    }
    
    
    public function getCacheModel(){
    	
    	$this->loadModel(EnumModel::Usuario);
    	
    	$mis_datos_cache = $this->Usuario->getCacheModel();//esta en APPModel
    	
    	
    	$output = array(
    			"status" =>EnumError::SUCCESS,
    			"message" => "GetModel",
    			"content" => $mis_datos_cache
    		
    	);
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message", "content"));
    	
    }
    
    
    public function getCacheStaticIndex(){
    	
    	$this->loadModel(EnumModel::Usuario);
    	
    	$mis_datos_cache = $this->Usuario->getCacheStaticIndex();//esta en APPModel
    	
    	
    	$output = array(
    			"status" =>EnumError::SUCCESS,
    			"message" => "GetModel",
    			"content" => $mis_datos_cache
    			
    	);
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message", "content"));
    	
    }
    
    
    
    public function getCacheMenu(){
    	
    	$this->loadModel(EnumModel::Usuario);
    	
    	$menu_cache = $this->Usuario->getMenu($this);//estos metodos de getCache estan definidos en el AppModel
    	
    	$data["GetCacheMenu"] = $menu_cache;
    	
    	
    	$output = array(
    			"status" =>EnumError::SUCCESS,
    			"message" => "GetCacheMenu",
    			"content" => $data
    			
    	);
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message", "content"));
    	
    }
    
    
    
    public function getCachePermisos(){
    	
    	$this->loadModel(EnumModel::Usuario);
    	
    	$menu_permisos = $this->Usuario->allpermision();//estos metodos de getCache estan definidos en el AppModel
    	
    	$data = $menu_permisos;
    	
    	
    	
    	
    	
    	$output = array(
    			"status" =>EnumError::SUCCESS,
    			"message" => "GetCachePermisos",
    			"content" => $data,
    			"id_rol"=>$id_persona = $this->Auth->user('id_rol'),
    			"username"=>$id_persona = $this->Auth->user('username')
    			
    			
    			
    	);
    	
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message", "content","id_rol","username"));
    	
    }
    
    
    
    public function GetCacheEstadoTipoComprobante(){
    	
    	$this->loadModel(EnumModel::EstadoTipoComprobante);
    	
    	$index_etc_cache = $this->EstadoTipoComprobante->getindex();
    	
    	$data["GetCacheEstadoTipoComprobante"] = $index_etc_cache;
    	
    	
    	
    	$output = array(
    			"status" =>EnumError::SUCCESS,
    			"message" => "GetCacheEstadoTipoComprobante",
    			"content" => $data
    			
    	);
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message", "content"));
    	
    }
    

    }
    
   
?>