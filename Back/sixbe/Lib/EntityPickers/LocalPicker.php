<?php
class LocalPicker extends EntityPicker{
    
    public $model = "Local";
    
    public $pickerTitle = "Selección de Local";
    
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Nro. Planta", "field" => "nro_planta", "width" => "30%", "keyOrder" => "nro_planta"),
                                  array("title" => "Ident. Plano", "field" => "ident_plano", "width" => "30%", "keyOrder" => "ident_plano"),
                                );

    public function getPickerConditions($busqueda){
        
        return array('OR' => array('Local.nro_planta LIKE' => '%' . $busqueda['bus_unico'] . '%', 'Local.ident_plano LIKE' => '%' . $busqueda['bus_unico'] . '%'));
    }
    
    public function getPickerJoins() {
        
        return DataFilter::getLocalPickerJoinFilter();
        
    }

}