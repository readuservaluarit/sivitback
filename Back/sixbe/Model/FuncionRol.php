<?php
App::uses('AppModel', 'Model');
/**
 * Rol Model
 *
 * @property NivelAutorizadoInformacion $NivelAutorizadoInformacion
 * @property Usuario $Usuario
 */
class FuncionRol extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'funcion_rol';


    public $actsAs = array('Containable');


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Funcion' => array(
			'className' => 'Funcion',
			'foreignKey' => 'id_funcion',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
    	'Rol' => array(
			'className' => 'Rol',
			'foreignKey' => 'id_rol',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}