<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class TablaAuxiliar extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'tabla_auxiliar';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	
	
    public $hasMany = array(
    		'TablaAuxiliarItem' => array(
    				'className' => 'TablaAuxiliarItem',
    				'foreignKey' => 'id_tabla_auxiliar',
    				'dependent' => false,
    				'conditions' => '',
    				'fields' => '',
    				'order' => '',
    				'limit' => '',
    				'offset' => '',
    				'exclusive' => '',
    				'finderQuery' => '',
    				'counterQuery' => ''
    		)
    		
    		
    		
    );

}
