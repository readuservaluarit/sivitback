SELECT  tipo_comprobante.`codigo_tipo_comprobante`,comprobante.`nro_comprobante`, 

persona.`razon_social`,
cartera_rendicion.`d_cartera_rendicion` AS cartera_rendicion,
detalle_tipo_comprobante.`d_detalle_tipo_comprobante` AS detalle

, comprobante.`fecha_contable`, 

REPLACE(IFNULL( (SELECT SUM(monto) FROM comprobante_valor WHERE id_comprobante=comprobante.id AND id_valor=1),0),".",",") AS efectivo,
REPLACE(IFNULL( (SELECT SUM(comprobante_valor.monto) FROM comprobante_valor  JOIN cheque ON cheque.id = comprobante_valor.`id_cheque`
WHERE  id_comprobante=comprobante.id AND id_valor=2 AND cheque.`id_tipo_cheque` = 1
),0),".",",")  AS cheque_tercero,
REPLACE(IFNULL( (SELECT SUM(comprobante_valor.monto) FROM comprobante_valor  JOIN cheque ON cheque.id = comprobante_valor.`id_cheque`
WHERE  id_comprobante=comprobante.id AND id_valor=2 AND cheque.`id_tipo_cheque` = 2
),0),".",",")  AS cheque_propio,

REPLACE(IFNULL( (SELECT SUM(monto) FROM comprobante_valor WHERE id_comprobante=comprobante.id AND id_valor IN (4,12)),0),".",",")  AS transferencia,
REPLACE(IFNULL( (SELECT SUM(importe_impuesto) FROM comprobante_impuesto WHERE comprobante_impuesto.`id_comprobante`= comprobante.id),0),".",",")  AS retenciones,
REPLACE(IFNULL( (SELECT SUM(monto) FROM comprobante_valor WHERE id_comprobante=comprobante.id AND id_valor=11),0),".",",")  AS tarjeta,
REPLACE(IFNULL( (SELECT SUM(monto) FROM comprobante_valor WHERE id_comprobante=comprobante.id AND id_valor=13),0),".",",")  AS compensacion,
REPLACE(total_comprobante,".",",") AS total
  FROM comprobante
LEFT JOIN persona ON persona.id = comprobante.`id_persona`
JOIN tipo_comprobante ON tipo_comprobante.id = comprobante.`id_tipo_comprobante`
LEFT JOIN cartera_rendicion ON comprobante.`id_cartera_rendicion` = cartera_rendicion.`id`
LEFT JOIN detalle_tipo_comprobante ON detalle_tipo_comprobante.id = comprobante.`id_detalle_tipo_comprobante`
WHERE comprobante.id_tipo_comprobante IN(306,307,308,502,504,602) AND comprobante.`id_estado_comprobante` IN (116)
AND YEAR(comprobante.`fecha_contable`)>=YEAR(NOW())

 ORDER BY nro_comprobante DESC LIMIT 10000