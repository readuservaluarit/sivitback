// Migrar todos los movimientos de la estrucutra  SIX a  VALUARIT_VALMEC

INSERT valuarit_valmec_q.movimiento_temp
(id,cantidad,remito_recibido,factura_recibida,lotes,id_movimiento_tipo,
id_producto,
id_componente,
id_materia_prima,
fecha, id_orden_armado,id_orden_produccion,id_factura,observacion,id_lote,id_orden_compra,id_comprobante,id_usuario,nro_informe_recepcion
id_movimiento_padre,id_deposito_origen,id_deposito_destino,id_comprobante_item,id_migracion)

SELECT M.id,M.cantidad,M.remito_recibido,M.factura_recibida,M.lotes,M.id_movimiento_tipo,
M.id_producto, /* el producto mantiene ID*/
M.id_componente, /* traer el  ID del componente de la tabla producto */
M.id_materia_prima, /* traer el  ID del materia prima de la tabla producto */
M.fecha,
M.id_orden_armado,
M.id_orden_produccion,
M.id_factura,
M.observacion,M.id_lote,
M.id_orden_compra,
M.id_pedido_interno as id_comprobante,
M.id_usuario,M.nro_informe_recepcion,
M.id_movimiento_padre , O.id_deposito as id_deposito_origen, D.id_deposito as id_deposito_destino, null as id_comprobante_item ,  M.id as id_migracion
FROM 		  six.movimiento AS M
left  JOIN valuarit_valmec.deposito_movimiento_tipo as O ON M.id_movimiento_tipo = O.id_movimiento_tipo and O.es_origen = 1 and O.default = 1
left JOIN valuarit_valmec.deposito_movimiento_tipo as D ON M.id_movimiento_tipo = D.id_movimiento_tipo and D.es_origen = 0 and D.default = 1






