<?php
App::uses('ComprobantesController', 'Controller');



class MantenimientosController extends ComprobantesController {
    
    public $name = EnumController::Mantenimientos;
    public $model = 'Comprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $requiere_impuestos = 0;
    
    /**
    * @secured(CONSULTA_MANTENIMIENTO)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros($this->model);
            
            
                       
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>array('Persona','EstadoComprobante','CondicionPago','TipoComprobante','PuntoVenta','DetalleTipoComprobante'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.id desc'
        ); //el contacto es la sucursal
        
      
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
            
             //$valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
             //unset($valor['ComprobanteItem']);
             
             if(isset($valor['TipoComprobante'])){
                 $valor[$this->model]['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                 $valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
                 unset($valor['TipoComprobante']);
             }
             
             
             
          
          
             
             if(isset($valor['EstadoComprobante'])){
                $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
                unset($valor['EstadoComprobante']);
             }
            
           
             
             if(isset($valor['Persona'])){
            
            
                 if($valor['Persona']['id']== null){
                     
                    $valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social']; 
                 }else{
                     
                 	if($valor['Persona']['id_tipo_persona'] == EnumTipoPersona::Proveedor){
                     $valor[$this->model]['razon_social'] = $valor['Persona']['razon_social'];    
                    
                 	}else{//es empleado
                 		
                 		$valor[$this->model]['razon_social'] = $valor['Persona']['nombre']."  ". $valor['Persona']['apellido'];
                 		
                 	}
                 	
                 	$valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
                     
                     
                 }
                 
                 unset($valor['Persona']);
             }
             
             
             
             
             if(isset($valor['DetalleTipoComprobante'])){
             	$valor[$this->model]['d_detalle_tipo_comprobante'] = $valor['DetalleTipoComprobante']["d_detalle_tipo_comprobante"];
             }
             
             
               
               
                 if(isset($valor['PuntoVenta'])){
                    $valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
                    unset($valor['PuntoVenta']);
             }
            $this->{$this->model}->formatearFechas($valor);
      
        }
        
        
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     
        
        
        

        
    }
    
   

    /**
    * @secured(ADD_MANTENIMIENTO)
    */
    public function add(){
 
     parent::add();    
        
    }
    
    
    /**
    * @secured(MODIFICACION_MANTENIMIENTO)
    */
    public function edit($id){ 
     parent::edit($id); 
     return;   
        
    }
    
    
    /**
    * @secured(BAJA_MANTENIMIENTO)
    */
    public function delete($id){
        
     parent::delete($id);    
        
    }
    
   /**
    * @secured(BAJA_MANTENIMIENTO)
    */
    public function deleteItem($id,$externo=1){
        
        parent::deleteItem($id,$externo);    
        
    }
    
    
      /**
    * @secured(CONSULTA_MANTENIMIENTO)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
  
    /**
    * @secured(BTN_PDF_MANTENIMIENTOS)
    */
    public function pdfExport($id,$vista=''){
        
        
        
        
        
    $datoEmpresa = $this->Session->read('Empresa');
        
        
        
        
        
        
    $this->loadModel("Comprobante");
    $remito = $this->Comprobante->find('first', array(
                                                        'conditions' => array('Comprobante.id' => $id),
                                                        'contain' =>array('TipoComprobante')
                                                        ));
       
    
     
                                   
                                                        
        
        
        
        if($remito){
            $comprobantes_relacionados = $this->getComprobantesRelacionados($id);
         
                                           
          
            
            
              
            $facturas_relacionados = array();
            $pedidos_internos_relacionados = array();


            foreach($comprobantes_relacionados as $comprobante){


             if(  isset($comprobante["Comprobante"]["id_tipo_comprobante"]) && in_array($comprobante["Comprobante"]["id_tipo_comprobante"],   $this->Comprobante->getTiposComprobanteController(EnumController::Facturas) ))
                array_push($facturas_relacionados,$this->Comprobante->GetNumberComprobante($comprobante["PuntoVenta"]["numero"],$comprobante["Comprobante"]["nro_comprobante"]));
            
            if(isset($comprobante["Comprobante"]["id_tipo_comprobante"]) && $comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::PedidoInterno)
                        array_push($pedidos_internos_relacionados,$this->Comprobante->GetNumberComprobante($comprobante["PuntoVenta"]["numero"],$comprobante["Comprobante"]["nro_comprobante"]));


            }
           
            
            
            
            $this->set('facturas_relacionados',$facturas_relacionados);  
            $this->set('pedidos_internos_relacionados',$pedidos_internos_relacionados);     
            parent::pdfExport($id);  
         }
            
       
     
        
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
        
        
      
 
    }
    
    
    /**
    * @secured(MODIFICACION_MANTENIMIENTO)
    */
    public function Anular($id_comprobante){
     
     $this->loadModel("Comprobante");
     $this->loadModel("Asiento");
     $this->loadModel("Cheque");
     $this->loadModel("Modulo");
        
        
          $oc = $this->Comprobante->find('first', array(
                    'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                    'contain'=>false
                ));
                
          $ds = $this->Comprobante->getdatasource(); 
      
          
          
          
                   
            if( $oc && $oc["Comprobante"]["id_estado_comprobante"]!= EnumEstadoComprobante::Anulado ){
                  
                try{
                    $ds->begin();
                        
                        $this->Comprobante->updateAll(
                                                        array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
                                                        array('Comprobante.id' => $id_comprobante) );  
                                                        
                        
                        
                         $habilitado  = $this->Modulo->estaHabilitado(EnumModulo::STOCK);
                                                         
                        
                        if($habilitado == 1){ //TODO: Si tiene mov stock anular
                        	
                        	
                        	$id_tipo_movimiento_defecto = $this->Comprobante->getTipoMovimientoDefecto($id_comprobante);
                        	$id_movimiento = $this->Comprobante->HizoMovimientoStock($id_comprobante,$id_tipo_movimiento_defecto);                 
                            
                                        
                            if( $id_movimiento > 0 ){//si hizo movimiento
                                $this->loadModel("Movimiento");
                                $this->Movimiento->Anular($id_movimiento);
                            }
                        }                                
                       
                                                        
                        
                    $mantenimiento = $this->Comprobante->find('first', array(
                        		'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                        		'contain' => false
                        ));
                        
                    $this->Auditar($id_comprobante,$mantenimiento);
                        
                    $ds->commit(); 
                    $tipo = EnumError::SUCCESS;
                    $mensaje = "Se Anulo correctamente el comprobante";
                       
               }catch(Exception $e){ 
                
                $ds->rollback();
                $tipo = EnumError::ERROR;
                $mensaje = "No es posible anular el Comprobante ERROR:".$e->getMessage();
                
                
              }  
            
            }else{
                
               $tipo = EnumError::ERROR;
               $mensaje = "No es posible anular el Comprobante ya se encuenta ANULADO";
            }
            
              $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                );      
              echo json_encode($output);
              die(); 
    }
    
    
        /**
        * @secured(CONSULTA_MANTENIMIENTO)
        */
        public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
        {
            parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
        }
        
        
        
        /**
         * @secured(BTN_EXCEL_MANTENIMIENTOS)
         */
        public function excelExport($vista="default",$metodo="index",$titulo=""){
        	
        	parent::excelExport($vista,$metodo,"Listado de Mantenimientos");
        	
        	
        	
        }
         
    
}
?>