<?php


App::uses('Usuario', 'Model');


class cake_sessions extends AppModel{
	
	
	public function __construct() {

		parent::__construct();
	}
	
	
	
	
    
  
    
    
    function afterSave($created, $options = array())  {
    	
    	$a;
    }
    

    public function getActiveUsers(){
       
    	$minutes = 10; // Conditions for the interval of an active session
    	$sessionData = $this->find('all',array(
    			'conditions' => array(
    					'expires >=' => time() - ($minutes * 60) // Making sure we only get recent user sessions
    			)
    	));
    	
    	$activeUsers = array();
    	foreach($sessionData as $session){
    		$data = $session['cake_sessions']['data'];
    		// Clean the string from unwanted characters
    		$data = str_replace('Config','',$data);
    		$data = str_replace('Message','',$data);
    		$data = str_replace('Auth','',$data);
    		$data = substr($data, 1); // Removes the first pipe, don't need it
    		
    		// Explode the string so we get an array of data
    		$data = explode('|',$data);
    		
    		// Unserialize all the data so we can use it
    		$auth = unserialize($data[1]);
    		
    		// Check if we are dealing with a signed-in user
    		if(!isset($auth['Usuario']) || is_null($auth['Usuario']['id'])) continue;
    		
    		if(!in_array($auth['User']['id'],$activeUsers))
    			$esa = "bieen";
    		
    		/* Because a user session contains all the data of a user
    		 * (except the password), I will only return the User id
    		 * and the first and last name of the user */
    		
    		/* First check if a user id hasn't already been saved
    		 * (can happen because of multiple sign-ins on different
    		 * browsers / computers!) */
    		
    		
    			
    			/* Keep in mind, your User table needs to contain
    			 * a first- and lastname to return them. If not,
    			 * you could use the email address or username
    			 * instead of this data. */
    			
    		
    	}
    	return $activeUsers;
    }
    
   public function IsUserActive2($id,$cantidad_maxima_sesiones_multiples,$id_rol_usuario){
       
       
       if($id_rol_usuario != EnumRol::Webservice){
           if($id!=0){
               
               $usuarios_activos = $this->getActiveUsers();
               
               if(isset($usuarios_activos[$id])){ //esta logueado, chequeo la maxima cantidad
                 
                 if($usuarios_activos[$id]["cantidad_maxima_sesiones_multiples"] >= $cantidad_maxima_sesiones_multiples){
                     //no dejo loguearse y borra anteriores
                     
                     return 0; //$id de usuario
                     
                 }else{
                    return 1;//puede loguearse 
                 }  
                   
               }else{
                   
                   return 1;//puede loguearse
               } 
               
               
           }else{
               
               return 0;
           }
   }else{
       
      return 1;   // si es Webservice no pasa por el control de sessiones abiertas puede N
   }
   }
   
   private function deleteSession($id){
       
       
         $minutes = 10; // Conditions for the interval of an active session
        $sessionData = $this->find('all',array(
            'conditions' => array(
                'expires >=' => time() - ($minutes * 60) // Making sure we only get recent user sessions
            )
    ));

    $activeUsers = array();
    
    foreach($sessionData as $session){
        $data = $session['cake_sessions']['data'];
        // Clean the string from unwanted characters
        $data = str_replace('Config','',$data);
        $data = str_replace('Message','',$data);
        $data = str_replace('Auth','',$data);
            $data = substr($data, 1); // Removes the first pipe, don't need it

        // Explode the string so we get an array of data
        $data = explode('|',$data);

        // Unserialize all the data so we can use it
        $auth = unserialize($data[2]);

        // Check if we are dealing with a signed-in user
        if(isset($auth['User']) && $auth['User']['id'] == $id){
            
           $this->id = $session['cake_sessions']["id"];
           $this->delete(); 
        }
       
       
   } 
    
}


public function whoisonline(){
	
	/*
	$sessiondata = $this->query('SELECT `data` FROM
`cake_sessions`');*/
	
	
	
	$sessiondata= $this->find('all',array(
			'fields'=>'cake_sessions.data',
			'conditions' => array(
					'expires >=' => time() - (6 * 60) // Making sure we only get recent user sessions
			)
	));
	
	//pr($sessiondata);
	foreach($sessiondata as $sess){
		$sessdata =
		preg_replace('/[0-9]{1,3}:/','',$sess['cake_sessions']['data']);
		$sessarray = explode(';s:',$sessdata);
		
		//pr($sessarray);
		$usuarios_online = array();
		
		foreach ($sessarray as $key =>$wert){
			if ($wert == '"username"') {//la  proxima corrida del for es el username
				
				$username = $sessarray[$key+1];
				$username= preg_replace('/"/','',$username);
				if(!in_array($username, $usuarios_online)){
					
					array_push($usuarios_online, $username);
					
				}
			}
		}
		//$key++;
		//$name = $sessarray[$key];
		
		//$name = preg_replace('/"/','',$name);
		//$onlineusers[]=$name;
	}
	//pr($onlineuser);
	//$this->set('onlineusers',$onlineusers);
	return $usuarios_online;
}


public function userOnlineManyTimes($username_login){
	
	/*
	 $sessiondata = $this->query('SELECT `data` FROM
	 `cake_sessions`');*/
	
	
	
	$sessiondata= $this->find('all',array(
			'fields'=>'cake_sessions.data',
			'conditions' => array(
					'expires >=' => time() - (6 * 60) // Making sure we only get recent user sessions
			)
	));
	
	$cantidad_sessiones_abiertas = 0;
	

	
	
	//pr($sessiondata);
	foreach($sessiondata as $sess){
		
		//$pepe = unserialize($sess['cake_sessions']['data']);
		
		
		
		$sessdata =
		preg_replace('/[0-9]{1,3}:/','',$sess['cake_sessions']['data']);
		$sessarray = explode(';s:',$sessdata);
		
		//pr($sessarray);
	
		
		foreach ($sessarray as $key =>$wert){
			if ($wert == '"username"') {//la  proxima corrida del for es el username
				
				$username = $sessarray[$key+1];
				$username= preg_replace('/"/','',$username);
				if(strtolower($username) == strtolower($username_login))
					$cantidad_sessiones_abiertas++;
				
				break;
				
				
			}
		}
		//$key++;
		//$name = $sessarray[$key];
		
		//$name = preg_replace('/"/','',$name);
		//$onlineusers[]=$name;
	}
	//pr($onlineuser);
	//$this->set('onlineusers',$onlineusers);
	return $cantidad_sessiones_abiertas;
}



public function deleteSessionByUserName($username_login){
	
	$sessiondata= $this->find('all',array(
			//'fields'=>'cake_sessions.data','cake_sessions.id',
			'conditions' => array(
					'expires >=' => time() - (6 * 60) // Making sure we only get recent user sessions
			)
	));
	

	$array_session_id = array();
	//pr($sessiondata);
	foreach($sessiondata as $sess){
		//$pepe = unserialize($sess['cake_sessions']['data']);
		$sessdata = preg_replace('/[0-9]{1,3}:/','',$sess['cake_sessions']['data']);
		$sessarray = explode(';s:',$sessdata);
		
		//pr($sessarray);
		
	
		foreach ($sessarray as $key =>$wert){
			if ($wert == '"username"') {//la  proxima corrida del for es el username
				
				$username = $sessarray[$key+1];
				$username= preg_replace('/"/','',$username);
				if(strtolower($username) == strtolower($username_login) ){
					$sessionid = $sess['cake_sessions']['id'];
					
					array_push($array_session_id, $sessionid);
				}	
			
					
					
			}
		}
		
	
	}
	
	if(count($array_session_id)>0){
		
		$this->deleteAll(array('cake_sessions.id'=>$array_session_id));
	}
	
	
}



public function IsUserActive($id,$cantidad_maxima_sesiones_multiples,$id_rol_usuario,$username,$hash_sesion=""){
    
	//si envia el hash significa que lo envia el front, es el id de session de php
	
	$cantidad_sesiones_iniciadas = $this->userOnlineManyTimes($username);
	
	if(( $cantidad_sesiones_iniciadas == 0) && $hash_sesion == "")
		return 1;
	
	
		if((strlen($hash_sesion)>0 && $this->TieneHashValido($id,$hash_sesion)) && $cantidad_sesiones_iniciadas>=1)
    	return 1;
	else 
		return 0;
	
		
	
		
}



public function TieneHashValido($id_usuario,$hash){
	
	
	$usuario_class = ClassRegistry::init('Usuario'); 
	
	$tiene_hash_valido = $usuario_class->find('count',array(
			'conditions' => array(
					'Usuario.id' =>$id_usuario,'Usuario.hash_sesion'=>trim($hash) // Making sure we only get recent user sessions
			)
	));
	
	if($tiene_hash_valido >0)
		return true;
	else
		return false;
	
	
}

}