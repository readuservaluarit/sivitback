<?php

App::uses('ComprobantesController', 'Controller');

class OrdenArmadoController extends ComprobantesController {

	
	public $name = EnumController::OrdenArmado;
	public $model = 'Comprobante';
	public $helpers = array ( 'Paginator', 'Js');
	public $components = array('PaginatorModificado', 'RequestHandler');
	public $requiere_impuestos = 0;
	public $title_form  = "Listado de Ordenes de Armado";
	
	
	//Se sobre escribio esta clase para que  el pdf export llama este metodo antes llamaba al index comun. mucho mas pesado no se por q(30/01/2020). ver de unificar ambos
	/**
	 * @secured(CONSULTA_ORDEN_ARMADO)
	 */
	public function index2() {
	}
	
	
	//Unificacion de index2 e index (30/01/2020). 
	public function index() {
		
		if($this->request->is('ajax'))
			$this->layout = 'ajax';
			
			$this->loadModel($this->model);
			$this->loadModel("DetalleTipoComprobante");
			
			$this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
			
			$conditions = $this->RecuperoFiltros($this->model);
			
			$id_pedido_interno = $this->getFromRequestOrSession('Comprobante.id_pedido_interno');
			
			$this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
					'contain' =>array('Usuario','Persona','EstadoComprobante','TipoComprobante','DepositoOrigen','DepositoDestino','PuntoVenta','ComprobanteItem'=>array("ComprobanteItemOrigen"=>array("Producto"=>array('ArticuloRelacion'=>array('ArticuloHijo'=>array('Categoria'))),"Comprobante"=>array("PuntoVenta","Persona"
																																																							
					)),"Producto"),'DetalleTipoComprobante'),
					'joins'=>array(
								array(
									'table' => 'comprobante_item',
									'alias' => 'ComprobanteItem',
									'type' => 'LEFT',
									'conditions' => array(
											'ComprobanteItem.id_comprobante = Comprobante.id'
									))
					),
					'conditions' => $conditions,
					'limit' => $this->numrecords,
					'page' => $this->getPageNum(),
					'order' => $this->model.'.id desc'
			); //el contacto es la sucursal
			
			
			$this->PaginatorModificado->settings = $this->paginate; 
			$data = $this->PaginatorModificado->paginate($this->model);
			$page_count = $this->params['paging'][$this->model]['pageCount'];
			
			$dias_oa_pi = $this->DetalleTipoComprobante->getDias(EnumDetalleTipoComprobante::OrdenArmadoPorPedidoInterno);
			$dias_fecha_limite_oa = $this->DetalleTipoComprobante->getDias2(EnumDetalleTipoComprobante::OrdenArmadoPorPedidoInterno);
			
			
			
			foreach($data as &$valor){
				
				//$valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
				//unset($valor['ComprobanteItem']);
				
				if(isset($valor['TipoComprobante'])){
					$valor[$this->model]['d_tipo_comprobante'] = 	 $valor['TipoComprobante']['d_tipo_comprobante'];
					$valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
					unset($valor['TipoComprobante']);
				}
				
				if(isset($valor['DetalleTipoComprobante'])){
					$valor[$this->model]['d_detalle_tipo_comprobante'] = 	 $valor['DetalleTipoComprobante']['d_detalle_tipo_comprobante'];
					
					unset($valor['DetalleTipoComprobante']);
				}
				

				if(isset($valor['ComprobanteItem'][0]['id_comprobante_item_origen'])  && $valor[$this->model]["id_detalle_tipo_comprobante"] == EnumDetalleTipoComprobante::OrdenArmadoPorPedidoInterno){//es 0 el array porque la OA solo tiene 1 item
					
				
					$valor[$this->model]['pto_nro_comprobante_origen'] = (string) $this->Comprobante->GetNumberComprobante($valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["Comprobante"]['PuntoVenta']['numero'],$valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["Comprobante"]["nro_comprobante"]);
					$valor[$this->model]['nro_comprobante_origen'] = (string) $valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["Comprobante"]["nro_comprobante"];
					$valor[$this->model]['item_observacion_origen'] = (string) $valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["item_observacion"];

					$valor[$this->model]['razon_social'] = 		$valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["Comprobante"]['Persona']['razon_social'];
					$valor[$this->model]['codigo_persona'] = 	$valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["Comprobante"]['Persona']['codigo'];
					$valor[$this->model]['id_persona'] = 		$valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["Comprobante"]['Persona']['id'];
					$valor[$this->model]['id_pedido_interno'] = $valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["Comprobante"]["id"];

						
					$fecha = new DateTime($valor["ComprobanteItemOrigen"]["fecha_entrega"] );
					
					$fecha->sub(new DateInterval('P'.$dias_fecha_limite_oa.'D'));
		
					
					$valor[$this->model]['fecha_entrega_maxima'] =  $fecha->format('d-m-Y');
					
					if(isset($valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["fecha_entrega"])){
						$fecha_min = new DateTime($valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["fecha_entrega"] );;
					
						if($dias_oa_pi >0)//cuantos dias antes de la fecha de vencimiento del PI debe entregar almacen los componentes
							$fecha_min->sub(new DateInterval('P'.$dias_oa_pi.'D'));
						else
							$fecha_min = new DateTime($valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["fecha_entrega"]);
						
						
						$valor[$this->model]['fecha_entrega_minima'] =   $fecha_min->format('d-m-Y');
						
						if($dias_oa_pi>0 && isset($valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["fecha_entrega"])){
							
							$fecha_entrega_pi = new DateTime($valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["fecha_entrega"]);
							$fecha_entrega_pi->sub(new DateInterval('P'.$dias_oa_pi.'D'));
							$valor[$this->model]['fecha_vencimiento'] =   $fecha_entrega_pi->format('d-m-Y');//fecha limite de armado de la OA son XX dias antes de la entrega del item del PI
							
							$valor[$this->model]['fecha_entrega_origen'] = $valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["fecha_entrega"];
						}
						
						$valor[$this->model]['fecha_entrega_minima'] =   $fecha_min->format('d-m-Y');

						$valor[$this->model]['codigo_producto'] = (string) $valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["Producto"]["codigo"];
						$valor[$this->model]['id_producto'] = (string) $valor['ComprobanteItem'][0]["ComprobanteItemOrigen"]["Producto"]["id"];
						$valor[$this->model]['cantidad'] = $valor['ComprobanteItem'][0]["cantidad_cierre"];
					}
				}else{
						$valor[$this->model]['razon_social'] = "";
						$valor[$this->model]['pto_nro_comprobante_origen'] = "";
						$valor[$this->model]['id_pedido_interno'] = "";
						$valor[$this->model]['nro_comprobante_origen'] = "";
						$valor[$this->model]['codigo_producto'] = (string) $valor['ComprobanteItem'][0]["Producto"]["codigo"];
						$valor[$this->model]['id_producto'] = (string) $valor['ComprobanteItem'][0]["Producto"]["id"];

						$valor[$this->model]['cantidad'] = $valor['ComprobanteItem'][0]["cantidad_cierre"];
				}
				
				
				/*$valor["Comprobante"]['cantidad'] = '2';TESTING*/
				
				$valor[$this->model]['Producto'] = $valor['ComprobanteItem'][0]["Producto"];//esto es por el certificado de Calidad
				$valor[$this->model]['d_usuario'] = $valor['Usuario']["nombre"];//esto es por el certificado de Calidad
				
				unset($valor['Usuario']);

				if(isset($valor['EstadoComprobante'])){
					$valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
					unset($valor['EstadoComprobante']);
				}
				
				if(isset($valor['DepositoOrigen'])){
					$valor[$this->model]['id_deposito_origen'] = $valor['DepositoOrigen']['id'];
					$valor[$this->model]['d_deposito_origen'] = $valor['DepositoOrigen']['d_deposito'];
					unset($valor['DepositoOrigen']);
				}
				
				if(isset($valor['DepositoDestino'])){
					$valor[$this->model]['id_deposito_destino'] = $valor['DepositoDestino']['id'];
					$valor[$this->model]['d_deposito_destino'] = $valor['DepositoDestino']['d_deposito'];
					unset($valor['DepositoDestino']);
				}
				
				if(isset($valor['Persona'])){
					
					if($valor['Persona']['id']== null){
						
						$valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social'];
					}else{
						
						$valor[$this->model]['razon_social'] = $valor['Persona']['razon_social'];
						$valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
					}
					unset($valor['Persona']);
				}
				
				if(isset($valor['PuntoVenta'])){
					$valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
					unset($valor['PuntoVenta']);
				}
				$this->{$this->model}->formatearFechas($valor);
				unset($valor['ComprobanteItem']);
				
				unset($valor[$this->model]['Producto']);
				//$valor["Comprobante"]['cantidad'] = '222';//TESTING*/
			}
			
			$this->data = $data;
			$output = array(
					"status" =>EnumError::SUCCESS,
					"message" => "list",
					"content" => $data,
					"page_count" =>$page_count
			);
			$this->set($output);
			$this->set("_serialize", array("status", "message","page_count", "content"));
	}
	
	/*Para el formulario - INdex a secas viejo*/
	public function indexOLD() {
		
		if($this->request->is('ajax'))
			$this->layout = 'ajax';
			
			$this->loadModel($this->model);
			$this->loadModel(EnumModel::ComprobanteItem);

			$this->loadModel("DetalleTipoComprobante");
			
			$this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
			
			$conditions = $this->RecuperoFiltros($this->model);
			
			//array_push($conditions, array('ComprobanteItem.id_detalle_tipo_comprobante' => EnumDetalleTipoComprobante::Ord)); 
			
			$this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
					'contain' =>array("Comprobante"=>array('Usuario','Persona','EstadoComprobante','TipoComprobante','DepositoOrigen','DepositoDestino','PuntoVenta'						
							,'DetalleTipoComprobante'),"Producto"=>array("ArticuloRelacion"=>array('ArticuloHijo'=>array('Categoria'))),"ComprobanteItemOrigen"=>array("Comprobante"=>array("Persona","PuntoVenta"))),
					/*
					'joins'=>array(
							array(
									'table' => 'comprobante',
									'alias' => 'Comprobante',
									'type' => 'LEFT',
									'conditions' => array(
											'ComprobanteItem.id_comprobante = Comprobante.id'
									))
					),*/
					'conditions' => $conditions,
					'limit' => $this->numrecords,
					'page' => $this->getPageNum(),
					'order' => $this->model.'.id desc'
			); //el contacto es la sucursal
			
			$this->PaginatorModificado->settings = $this->paginate;
			$data = $this->PaginatorModificado->paginate("ComprobanteItem");
			$page_count = $this->params['paging']["ComprobanteItem"]['pageCount'];
			
			$dias_oa_pi = $this->DetalleTipoComprobante->getDias(EnumDetalleTipoComprobante::OrdenArmadoPorPedidoInterno);
			$dias_fecha_limite_oa = $this->DetalleTipoComprobante->getDias2(EnumDetalleTipoComprobante::OrdenArmadoPorPedidoInterno);
			
			foreach($data as &$valor){
			
				if(isset($valor['Comprobante']['TipoComprobante'])){
					$valor['Comprobante']['d_tipo_comprobante'] = 	 $valor['Comprobante']['TipoComprobante']['d_tipo_comprobante'];
					$valor['Comprobante']['codigo_tipo_comprobante'] = $valor['Comprobante']['TipoComprobante']['codigo_tipo_comprobante'];
					unset($valor['Comprobante']['TipoComprobante']);
				}
				
				if(isset($valor['Comprobante']['DetalleTipoComprobante'])){
					$valor['Comprobante']['d_detalle_tipo_comprobante'] = 	 $valor['Comprobante']['DetalleTipoComprobante']['d_detalle_tipo_comprobante'];
					unset($valor['Comprobante']['DetalleTipoComprobante']);
				}
				
				if(isset($valor['ComprobanteItem']['id_comprobante_item_origen'])  
					  && $valor["Comprobante"]["id_detalle_tipo_comprobante"] == EnumDetalleTipoComprobante::OrdenArmadoPorPedidoInterno){//es 0 el array porque la OA solo tiene 1 item
					
					
					$valor['Comprobante']['pto_nro_comprobante_origen'] = (string) $this->Comprobante->GetNumberComprobante($valor["ComprobanteItemOrigen"]["Comprobante"]['PuntoVenta']['numero'],$valor["ComprobanteItemOrigen"]["Comprobante"]["nro_comprobante"]);
					$valor['Comprobante']['nro_comprobante_origen'] = (string) $valor["ComprobanteItemOrigen"]["Comprobante"]["nro_comprobante"];
					$valor[$this->model]['item_observacion_origen'] = (string) $valor["ComprobanteItemOrigen"]["item_observacion"];
					
					$valor['Comprobante']['razon_social'] = $valor["Comprobante"]['Persona']['razon_social'];
					$valor['Comprobante']['codigo_persona'] = $valor["Comprobante"]['Persona']['codigo'];
					$valor['Comprobante']['id_persona'] = $valor["Comprobante"]['Persona']['id'];
					$valor['Comprobante']['id_pedido_interno'] = $valor["ComprobanteItemOrigen"]["Comprobante"]["id"];
					
					$valor['Comprobante']['inspeccion_origen'] = $valor["ComprobanteItemOrigen"]["Comprobante"]["inspeccion"];
					
					
					$fecha = new DateTime($valor['ComprobanteItemOrigen']["fecha_entrega"] );
					
					$fecha->sub(new DateInterval('P'.$dias_fecha_limite_oa.'D'));
					
					$valor["Comprobante"]['fecha_entrega_maxima'] =  $fecha->format('d-m-Y');
					
					
					if(isset($valor['ComprobanteItemOrigen']["fecha_entrega"])){
						$fecha_min = new DateTime($valor['ComprobanteItemOrigen']["fecha_entrega"] );;
						
						
						if($dias_oa_pi >0)//cuantos dias antes de la fecha de vencimiento del PI debe entregar almacen los componentes
							$fecha_min->sub(new DateInterval('P'.$dias_oa_pi.'D'));
							else
								$fecha_min = new DateTime($valor["fecha_entrega"]);
								
								$valor["Comprobante"]['fecha_entrega_minima'] =   $fecha_min->format('d-m-Y');
								
															
								if($dias_oa_pi>0 && isset($valor['ComprobanteItemOrigen']["fecha_entrega"])){
									/*$fecha_entrega_pi = new DateTime($valor['ComprobanteItem']["fecha_entrega"]);
									$fecha_entrega_pi->sub(new DateInterval('P'.$dias_oa_pi.'D'));
									$valor["Comprobante"]['fecha_vencimiento'] =   $fecha_entrega_pi->format('d-m-Y');//fecha limite de armado de la OA son XX dias antes de la entrega del item del PI
									*/
									$valor["Comprobante"]['fecha_entrega_origen'] = $valor['ComprobanteItemOrigen']["fecha_entrega"];
									
								}
								$valor["Comprobante"]['fecha_entrega_minima'] =   $fecha_min->format('d-m-Y');
								
								$valor["Comprobante"]['codigo_producto'] = (string) $valor["Producto"]["codigo"];
								$valor["Comprobante"]['id_producto'] = (string) $valor["Producto"]["id"];
								$valor["Comprobante"]['cantidad'] = $valor["cantidad_cierre"];
					}
				}else{
					$valor["Comprobante"]['razon_social'] = "";
					$valor["Comprobante"]['pto_nro_comprobante_origen'] = "";
					$valor["Comprobante"]['id_pedido_interno'] = "";
					$valor["Comprobante"]['nro_comprobante_origen'] = "";
					$valor["Comprobante"]['codigo_producto'] = (string) $valor["Producto"]["codigo"];
					$valor["Comprobante"]['id_producto'] = (string) $valor["Producto"]["id"];
					
					$valor["Comprobante"]['cantidad'] = $valor['ComprobanteItem']["cantidad_cierre"];
				}
				
				
				
				
				if($this->getFromRequestOrSession('Comprobante.traer_item') == "1")
					$valor["Comprobante"]['Producto'] = $valor["Producto"];//esto es por el certificado de Calidad
				
				
				
				
				$valor["Comprobante"]['d_usuario'] = $valor["Comprobante"]['Usuario']["nombre"];//esto es por el certificado de Calidad
				unset($valor["Comprobante"]['Usuario']);
				
				if(isset($valor["Comprobante"]['EstadoComprobante'])){
					$valor["Comprobante"]['d_estado_comprobante'] = $valor["Comprobante"]['EstadoComprobante']['d_estado_comprobante'];
					unset($valor["Comprobante"]['EstadoComprobante']);
				}
				
				if(isset($valor["Comprobante"]['DepositoOrigen'])){
					$valor["Comprobante"]['id_deposito_origen'] = $valor["Comprobante"]['DepositoOrigen']['id'];
					$valor["Comprobante"]['d_deposito_origen'] = $valor["Comprobante"]['DepositoOrigen']['d_deposito'];
					unset($valor["Comprobante"]['DepositoOrigen']);
				}
				
				if(isset($valor["Comprobante"]['DepositoDestino'])){
					$valor["Comprobante"]['id_deposito_destino'] = $valor["Comprobante"]['DepositoDestino']['id'];
					$valor["Comprobante"]['d_deposito_destino'] = $valor["Comprobante"]['DepositoDestino']['d_deposito'];
					unset($valor["Comprobante"]['DepositoDestino']);
				}

				if(isset($valor["ComprobanteItemOrigen"]["Comprobante"]['Persona'])){

					if($valor["ComprobanteItemOrigen"]["Comprobante"]['Persona']['id']== null){
						
						$valor["Comprobante"]['razon_social'] = $valor['Comprobante']['razon_social'];
					}else{
						
						$valor["Comprobante"]['razon_social'] = $valor["ComprobanteItemOrigen"]["Comprobante"]['Persona']['razon_social'];
						$valor["Comprobante"]['codigo_persona'] = $valor["ComprobanteItemOrigen"]["Comprobante"]['Persona']['codigo'];
					}
					unset($valor["ComprobanteItemOrigen"]);
				}
				
				if(isset($valor["Comprobante"]['PuntoVenta'])){
					$valor["Comprobante"]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor["Comprobante"]['PuntoVenta']['numero'],$valor["Comprobante"]['nro_comprobante']);
					unset($valor["Comprobante"]['PuntoVenta']);
				}
				$this->{$this->model}->formatearFechas($valor);
				unset($valor['ComprobanteItem']);
				
				//$valor["Comprobante"]['cantidad'] = '1';//TESTING*/
				
				//unset($valor[$this->model]['Producto']);
				unset($valor[$this->model]['Persona']);
			}
			
			
			$this->data = $data;
			$output = array(
					"status" =>EnumError::SUCCESS,
					"message" => "list",
					"content" => $data,
					"page_count" =>$page_count
			);
			$this->set($output);
			$this->set("_serialize", array("status", "message","page_count", "content"));
	}
	
	
	
	
	
	
	/**
	 * @secured(ADD_ORDEN_ARMADO)
	 */
	public function add() {
		
		
		
		/*
		if($this->request->data["Comprobante"]["id_detalle_tipo_comprobante"] == EnumDetalleTipoComprobante::OrdenArmadoPorPedidoInterno){
			
			$this->loadModel("ComprobanteItem");
			$this->loadModel("DetalleTipoComprobante");
			
			
			$dias_fecha_limite_oa = $this->DetalleTipoComprobante->getDias2(EnumDetalleTipoComprobante::OrdenArmadoPorPedidoInterno);
			$id_comprobante_item_pedido_interno = $this->request->data['ComprobanteItem'][0]['id_comprobante_item_origen'];
		
			
			$pi_item= $this->ComprobanteItem->find('first', array(
					'conditions' => array('ComprobanteItem.id' =>$id_comprobante_item_pedido_interno),
					'contain' =>false
			));
	

			    $fecha_entrega_pedido = $pi_item['ComprobanteItem']["fecha_entrega"];
				
			
				
			    $nuevafecha = strtotime ( '-'.$dias_fecha_limite_oa.'day' , strtotime ( $fecha_entrega_pedido) ) ;
				$nuevafecha = date ( 'Y-m-j' , $nuevafecha );
				$this->request->data["Comprobante"]["fecha_vencimiento"]= $nuevafecha;
				
			
		
		}*/
		parent::add();
		
		
		
	}
	
	
	/*
	 public function add() {
	 if ($this->request->is('post')){
	 
	 
	 $this->loadModel($this->model);
	 $this->request->data["OrdenArmado"]["id_estado"] = EnumEstado::Abierta;
	 $id_oa = '' ;
	 $this->request->data["OrdenArmado"]["fecha_emision"] = date("Y-m-d");
	 
	 
	 
	 if( $this->request->data["OrdenArmado"]["chk_pedido_interno"] == 0){//chk_pedido_interno
	 $this->request->data["OrdenArmado"]["id_pedido_interno_fox"] =  $this->request->data["OrdenArmado"]["id_pedido_interno"];
	 unset($this->request->data["OrdenArmado"]["id_pedido_interno"]);
	 
	 }
	 try{
	 if ($this->OrdenArmado->saveAll($this->request->data)){
	 $id_oa = $this->OrdenArmado->id;
	 
	 if($this->AceptaStock()){
	 
	 //$this->genera_movimiento_stock($id_oa,EnumTipoMovimiento::OrdenArmadoAlta)
	 $resultado = $this->ActualizoStock($id_oa,EnumTipoMovimiento::OrdenArmadoAlta);
	 //$resultado =  false;
	 if($resultado){
	 $mensaje = "La Orden de Armado ha sido creada exitosamente y se ha actualizado el stock";
	 $tipo = EnumError::SUCCESS;
	 }else{
	 $mensaje = "La Orden de Armado ha sido creada, pero no estan definidos los depositos por default";
	 $tipo = EnumError::SUCCESS;
	 
	 }
	 }else{
	 
	 $mensaje = "La Orden de Armado ha sido creada exitosamente";
	 $tipo = EnumError::SUCCESS;
	 
	 }
	 }else{
	 $mensaje = "Ha ocurrido un error,la Orden de Armado NO ha podido ser creada.";
	 $tipo = EnumError::ERROR;
	 
	 }
	 }catch(Exception $e){
	 
	 $mensaje = "Ha ocurrido un error, la Orden Armado NO ha podido ser creada.".$e->getMessage();
	 $tipo = EnumError::ERROR;
	 }
	 $output = array(
	 "status" => $tipo,
	 "message" => $mensaje,
	 "content" => "",
	 "id_add" =>$id_oa
	 );
	 //si es json muestro esto
	 if($this->RequestHandler->ext == 'json'){
	 $this->set($output);
	 $this->set("_serialize", array("status", "message", "content","id_add"));
	 }else{
	 
	 $this->Session->setFlash($mensaje, $tipo);
	 $this->redirect(array('action' => 'index'));
	 }
	 
	 }
	 
	 //si no es un post y no es json
	 if($this->RequestHandler->ext != 'json')
	 $this->redirect(array('action' => 'abm', 'A'));
	 
	 
	 }
	 */
	 
	 
	 
	 /**
	  * @secured(MODIFICACION_ORDEN_ARMADO)
	  */
	 public function edit($id) {
	 	$this->is_edit = 1;
	 	
	 	//$this->request->data[$this->model]["id_tipo_comprobante"] = $this->id_tipo_comprobante;
	 	
	 	$id_estado_comprobante_grabado = $this->{$this->model}->getEstadoGrabado($id);
	 	$cantidad_anterior = $this->getCantidadGuardada($id);//obtiene la cantidad guardada del unico item de la Orden de Armado
	 	
	 	$this->cantidad_anterior = $cantidad_anterior;

	 	unset($this->request->data["Comprobante"]["fecha_aprobacion"]);
	 	
	 	//fecha en que se cumple la OA
	 	if($id_estado_comprobante_grabado == EnumEstadoComprobante::Abierto && $this->request->data["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::Cumplida)
	 		$this->request->data["Comprobante"]["fecha_aprobacion"] =  date("Y-m-d");
	 	
	 	
	 	if($id_estado_comprobante_grabado != EnumEstadoComprobante::Anulado && $this->request->data["Comprobante"]["id_estado_comprobante"] != EnumEstadoComprobante::Anulado){
	 		parent::edit($id);
	 		return;
	 	}else{
	 		
	 		$this->Anular($id);
	 	}
	 	
	 }
	 
	 
	 /*
	 
	 function edit($id) {
	 
	 
	 if (!$this->request->is('get')){
	 
	 $this->loadModel($this->model);
	 $this->{$this->model}->id = $id;
	 
	 $cantidad_anterior = $this->getCantidadGuardada($id);
	 $estado_anterior = $this->getEstadoAnterior($id);
	 
	 
	 
	 if( $this->request->data["OrdenArmado"]["chk_pedido_interno"] == 0){//chk_pedido_interno
	 $this->request->data["OrdenArmado"]["id_pedido_interno_fox"] =  $this->request->data["OrdenArmado"]["id_pedido_interno"];
	 unset($this->request->data["OrdenArmado"]["id_pedido_interno"]);
	 
	 }
	 
	 if($this->request->data["OrdenArmado"]["id_estado"]== EnumEstado::Cerrada && $estado_anterior!=EnumEstado::Cerrada)
	 $this->request->data["OrdenArmado"]["fecha_cierre"] = date("Y-m-d");
	 
	 
	 if( $this->request->data["OrdenArmado"]["id_estado"] == EnumEstado::Completa && $estado_anterior!=EnumEstado::Completa)
	 $this->request->data["OrdenArmado"]["fecha_cumplida"] = date("Y-m-d");
	 
	 
	 unset($this->request->data["OrdenArmado"]["fecha_emision"]);
	 
	 try{
	 if ($this->{$this->model}->saveAll($this->request->data)){
	 
	 
	 
	 $this->AjusteStock($id,$cantidad_anterior);//realizo movimientos y ajuste si corresponde
	 
	 $mensaje =  "La Orden de Armado ha sido modificada exitosamente";
	 $status = EnumError::SUCCESS;
	 
	 $retorno = true;
	 if( $this->AceptaStock() && $this->request->data["OrdenArmado"]["id_estado"]== EnumEstado::Cerrada && $estado_anterior!=EnumEstado::Cerrada )
	 $retorno = $this->ActualizoStock($id,EnumTipoMovimiento::OrdenArmadoCierre);
	 
	 
	 
	 
	 
	 if(!$retorno){
	 
	 $mensaje =  "La Orden de Armado se cerro pero se debe definir los depositos por default para los movimientos de Cierre";
	 $status =    "error";
	 }
	 
	 //$this->genera_movimiento_stock($id_oa,EnumTipoMovimiento::OrdenArmadoCierre);
	 
	 
	 }else{   //si hubo error recupero los errores de los models
	 
	 $errores = $this->{$this->model}->validationErrors;
	 $errores_string = "";
	 foreach ($errores as $error){ //recorro los errores y armo el mensaje
	 $errores_string.= "&bull; ".$error[0]."\n";
	 
	 }
	 $mensaje = $errores_string;
	 $status = EnumError::ERROR;
	 }
	 
	 if($this->RequestHandler->ext == 'json'){
	 $output = array(
	 "status" => $status,
	 "message" => $mensaje,
	 "content" => ""
	 );
	 
	 $this->set($output);
	 $this->set("_serialize", array("status", "message", "content"));
	 }else{
	 $this->Session->setFlash($mensaje, $status);
	 $this->redirect(array('controller' => $this->name, 'action' => 'index'));
	 
	 }
	 }catch(Exception $e){
	 $this->Session->setFlash('Ha ocurrido un error, la Orden de Armado no ha podido modificarse.', 'error');
	 
	 }
	 
	 }else{ //si me pide algun dato me debe mandar el id
	 if($this->RequestHandler->ext == 'json'){
	 
	 $this->{$this->model}->id = $id;
	 $this->{$this->model}->contain('Unidad');
	 $this->request->data = $this->{$this->model}->read();
	 
	 $output = array(
	 "status" =>EnumError::SUCCESS,
	 "message" => "list",
	 "content" => $this->request->data
	 );
	 
	 $this->set($output);
	 $this->set("_serialize", array("status", "message", "content"));
	 }else{
	 
	 $this->redirect(array('action' => 'abm', 'M', $id));
	 
	 }
	 
	 
	 }
	 
	 if($this->RequestHandler->ext != 'json')
	 $this->redirect(array('action' => 'abm', 'M', $id));
	 }
	 
	 */
	 /**
	  * @secured(BAJA_ORDEN_ARMADO)
	  */
	 function delete($id) {
	 	$this->loadModel($this->model);
	 	$mensaje = "";
	 	$status = "";
	 	$this->OrdenArmado->id = $id;
	 	try{
	 		if ($this->OrdenArmado->saveField('activo', "0")) {
	 			
	 			//$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
	 			
	 			$status = EnumError::SUCCESS;
	 			$mensaje = "La Orden de Armado ha sido eliminada exitosamente.";
	 			$output = array(
	 					"status" => $status,
	 					"message" => $mensaje,
	 					"content" => ""
	 			);
	 			
	 		}
	 		
	 		else
	 			throw new Exception();
	 			
	 	}catch(Exception $ex){
	 		//$this->Session->setFlash($ex->getMessage(), 'error');
	 		
	 		$status = EnumError::ERROR;
	 		$mensaje = $ex->getMessage();
	 		$output = array(
	 				"status" => $status,
	 				"message" => $mensaje,
	 				"content" => ""
	 		);
	 	}
	 	
	 	if($this->RequestHandler->ext == 'json'){
	 		$this->set($output);
	 		$this->set("_serialize", array("status", "message", "content"));
	 		
	 	}else{
	 		$this->Session->setFlash($mensaje, $status);
	 		$this->redirect(array('controller' => $this->name, 'action' => 'index'));
	 		
	 	}
	 	
	 	
	 	
	 	
	 	
	 }
	 
	 
	 /**
	  * @secured(REPORTE_ORDEN_ARMADO)
	  */
	 public function pdfExport($id,$vista=''){
	 	
	 	$this->loadModel("Comprobante");
	 	$this->loadModel("DetalleTipoComprobante");
	 	
	 	$this->Comprobante->id = $id;
	 	$this->Comprobante->contain(array('ComprobanteItem'=>array('Producto' => array('ArticuloRelacion' => array('ArticuloHijo')),'ComprobanteItemOrigen'=>array("Comprobante"=>array("Persona"))),'Persona','TipoComprobante','PuntoVenta' ));
	 	//$this->Cotizacion->order(array('CotizacionItem.orden asc'));
	 	$this->request->data = $this->Comprobante->read();
	 	Configure::write('debug',0);
	 	
	 	$fecha_entrega_item = "";
	 	$comprobante_item  =  new ComprobanteItem;
	 	
	 	/*
	 	if(isset($this->request->data["ComprobanteItem"][0]["id_producto"]) && $this->request->data["ComprobanteItem"][0]["id_producto"] > 0 ){
	 		
	 		foreach($this->request->data["Comprobante"]["ComprobanteItem"] as $item){
	 			
	 			if( $item["id_producto"] == $this->request->data["Comprobante"]["id_producto"] ){
	 				$fecha_entrega_item = $comprobante_item->getFechaEntrega(date("Y-m-d", strtotime($this->request->data["Comprobante"]["fecha_generacion"])), $item['dias'],$this->request->data["Comprobante"]["fecha_entrega"]);
	 				$fecha = $fecha_entrega_item;
	 				$nuevafecha = strtotime ( '-2 day' , strtotime ( $fecha ) ) ;
	 				$nuevafecha = date ( 'j-m-Y' , $nuevafecha );
	 			}
	 		}
	 	}

	 	$this->request->data["Comprobante"]["fecha_limite_armado"]= $nuevafecha;
	 		 	*/
	 	$this->request->data["Comprobante"]["nro_comprobante_completo"] =
	 	$this->Comprobante->GetNumberComprobante($this->request->data["PuntoVenta"]["numero"],$this->request->data["Comprobante"]["nro_comprobante"]);    
	 	
	 	$this->set('datos_pdf',$this->request->data);
	 	$this->set('id',$id);
	 	Configure::write('debug',0);
	 	$this->response->type('pdf');
	 	$this->layout = 'pdf'; //esto usara el layout pdf.ctp
	 	$this->render();	 	
	 }
	 
	  /**
    * @secured(CONSULTA_REMITO)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    	
    	$this->set('model',$model);
    	Configure::write('debug',0);
    	$this->render($vista);
    	
    	
    	
    	
    }
	 
	 
	 protected function genera_movimiento_stock($id_pi,$action,$cantidad_calculada=0,$data =null,$simbolo_operacion=1) {
	 	
	 	
	 	if(isset($this->is_edit) || $this->is_edit == 1){
	 		
	 		
	 		if($this->request->data["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion){
	 			$action = EnumTipoMovimiento::OrdenArmadoCierre;
	 		}
	 		else{
	 			$action = EnumTipoMovimiento::OrdenArmadoAjuste;
	 		}
	 		
	 		
	 		
	 		$diferencia = 0;
	 		$cantidad_anterior = $this->cantidad_anterior;
	 		
	 		if($cantidad_anterior != $this->request->data["ComprobanteItem"][0]["cantidad_cierre"]){
	 		
	 			$diferencia = $this->request->data["ComprobanteItem"][0]["cantidad_cierre"] - $cantidad_anterior;
	 			$cantidad_calculada = $diferencia;
	 		}
	 	}
	 	
	 	$id_deposito = 0;
	 	$id_deposito_destino = 0;
	 	
	 	
	 	
	 	if(isset($this->request->data["Comprobante"]["id_deposito_origen"]))
	 		$id_deposito = $this->request->data["Comprobante"]["id_deposito_origen"];
	 		
	 	if(isset($this->request->data["Comprobante"]["id_deposito_destino"]))
	 			$id_deposito_destino = $this->request->data["Comprobante"]["id_deposito_destino"];
	 			
	 	
	 	$action2 = $action;
	 	
	 	/*Esto se hace para que el deposito de ajuste sea igual al de alta*/
	 	if($action == EnumTipoMovimiento::OrdenArmadoAjuste)
	 		$action2 = EnumTipoMovimiento::OrdenArmadoAlta;
	 		
	 		
	 		
	 	//	$resultado = $this->getDepositoDefault($action2,$id_deposito,$id_deposito_destino); //devuelvo un 0 si no definio el deposito default
	 		
	 		
	 		
	 	
	 			switch($action){
	 				
	 				case EnumTipoMovimiento::OrdenArmadoAlta:
	 				case EnumTipoMovimiento::OrdenArmadoCierre:
	 					
	 					$data = array();
	 					$data["Movimiento"]["id_movimiento_tipo"] = $action;
	 					$data["Movimiento"]["id_comprobante"] = $id_pi;
	 					$data["Movimiento"]["id_deposito_origen"] = $id_deposito;
	 					$data["Movimiento"]["id_deposito_destino"] = $id_deposito_destino;
	 					$data["Movimiento"]["llamada_interna"] = 1;
	 					$data["Movimiento"]["simbolo_operacion"] = 1;//resta o suma
	 					break;
	 					
	 				case EnumTipoMovimiento::OrdenArmadoAjuste:
	 					$data = array();
	 					$data["Movimiento"]["id_movimiento_tipo"] = $action;
	 					$data["Movimiento"]["id_comprobante"] = $id_pi;
	 					$data["Movimiento"]["id_deposito_origen"] = $id_deposito;
	 					$data["Movimiento"]["id_deposito_destino"] = $id_deposito_destino;
	 					$data["Movimiento"]["llamada_interna"] = 1;
	 					$data["Movimiento"]["diferencia"] = $cantidad_calculada;//resta o suma
	 					break;
	 					
	 			}
	 			
	 			
	 			if($id_deposito_destino!=0)
	 				$data["Movimiento"]["id_deposito_destino"] = $id_deposito_destino;
	 				
	 				//realizo la llamada al controller y le envio el movimiento
	 				
	 				
	 				if(!isset($this->is_edit) || $this->is_edit== 0){
	 					$this->requestAction(
	 							array('controller' => 'Movimientos', 'action' => 'add'),
	 							array('data' => $data)
	 							);
	 				}else{//en el edit chequeo si modifico la cantidad si la modifico hago movimiento por la diferencia, que puede ser para arriba o para abajo
	 					
	 					
	 					if($action == EnumTipoMovimiento::OrdenArmadoAjuste){
	 						if($cantidad_anterior != $this->request->data["ComprobanteItem"][0]["cantidad_cierre"]  ){
	 							
	 							$this->requestAction(
	 									array('controller' => 'Movimientos', 'action' => 'add'),
	 									array('data' => $data)
	 									);
	 						}
	 					}else{
	 						
	 						$this->requestAction(
	 								array('controller' => 'Movimientos', 'action' => 'add'),
	 								array('data' => $data)
	 								);
	 					}
	 					
	 		
	 						
	 						
	 				}
	 				
	 				return true;
	 	
	 		
	 		
	 		
	 }
	 
	 
	 
	 protected function AjusteStock($id_orden_armado,$cantidad_anterior){
	 	
	 	
	 	$this->cantidad_anterior = $cantidad_anterior;
	 	
	 	if($this->request->data["Comprobante"]["id_estado"]!= EnumEstadoComprobante::CerradoReimpresion && isset($this->request->data["Comprobante"]["genera_movimiento_stock"]) && $this->request->data["Comprobante"]["genera_movimiento_stock"] == 1){ //chequeo si modifico la cantidad fue modificada
	 		
	 		if($cantidad_anterior != $this->request->data["ComprobanteItem"][0]["cantidad"]  ){ //si cambio debo hacer un movimiento
	 			
	 			$diferencia = $this->request->data["ComprobanteItem"][0]["cantidad_cierre"] - $cantidad_anterior;
	 			$this->genera_movimiento_stock($id_orden_armado,EnumTipoMovimiento::OrdenArmadoAjuste,$diferencia);
	 			
	 			
	 			
	 		}
	 	}
	 }
	 
	 
	 protected function getCantidadGuardada($id_orden_armado){
	 	
	 	$orden_armado = $this->Comprobante->ComprobanteItem->find('first',array(
	 			'conditions' => array('ComprobanteItem.id_comprobante' => $id_orden_armado),
	 			'contain' =>array('Comprobante')
	 	));
	 	
	 	return $orden_armado["ComprobanteItem"]["cantidad_cierre"];
	 	
	 	
	 	
	 }
	 
	 
	 /*
	 protected function getDepositoDefault($id_movimiento_tipo,&$id_deposito_origen,&$id_deposito_destino){
	 	
	 	$this->loadModel("DepositoMovimientoTipo");
	 	
	 	$dm = $this->DepositoMovimientoTipo->find('all',array(
	 			'conditions' => array('DepositoMovimientoTipo.id_movimiento_tipo' => $id_movimiento_tipo,
	 					'DepositoMovimientoTipo.por_defecto' => 1),
	 			'contain' =>false
	 	));
	 	
	 	
	 	$id_deposito_origen = 0;
	 	$id_deposito_destino = 0;
	 	
	 	if($dm){
	 		foreach($dm as $deposito_movimiento){
	 			
	 			if($deposito_movimiento["DepositoMovimientoTipo"]["es_origen"] == 0 )
	 				$id_deposito_destino = $deposito_movimiento["DepositoMovimientoTipo"]["id_deposito"];
	 				
	 				if($deposito_movimiento["DepositoMovimientoTipo"]["es_origen"] == 1 )
	 					$id_deposito_origen = $deposito_movimiento["DepositoMovimientoTipo"]["id_deposito"];
	 					
	 		}
	 		
	 		if($id_deposito_origen== 0 || $id_deposito_destino ==0)
	 			return false;
	 			else
	 				return true;
	 				
	 	}else{
	 		return false;
	 	}
	 }
	 
	 */
	 
	 
	 
	 protected function getEstadoAnterior($id_orden_armado){
	 	
	 	$orden_armado = $this->OrdenArmado->find('first',array(
	 			'conditions' => array('OrdenArmado.id' => $id_orden_armado),
	 			'contain' =>false
	 	));
	 	
	 	return $orden_armado["OrdenArmado"]["id_estado"];
	 	
	 	
	 	
	 }
	 
	 
	 /**
	  * @secured(CONSULTA_ORDEN_ARMADO)
	  */
	 public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
	 {
	 	parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
	 }
	 
	 
	 
	 /**
	  * @secured(MODIFICACION_ORDEN_ARMADO)
	  */
	 public function Anular($id_comprobante){
	 	
	 	$this->loadModel("Comprobante");
	 	$this->loadModel("ComprobanteItem");
	 	$this->loadModel("Movimiento");
	 	$this->loadModel("Modulo");
	 	$this->anulo = 1;
	 	$puede_anular = 1;
	 	
	 	$oa = $this->Comprobante->find('first', array(
	 			'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
	 			'contain' => array('ComprobanteItem')
	 	));
	 	
	 	
	 	/*TODO: Controlar si fue entregado algun pedido*/
	 	
	 	
	 	if( $oa["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::OrdenArmado  && $this->Comprobante->getEstadoGrabado($id_comprobante) != EnumEstadoComprobante::Anulado
	 			
	 			
	 			){
	 		
	 				$this->is_edit = 1;//es un Edit porque no puede anular sin haber creado
	 		
	 
	 				$this->request->data = $oa;
	 				$this->request->data["ComprobanteItem"][0]["cantidad_cierre"] = 0;//si anulo
	 				$this->request->data["ComprobanteItem"][0]["cantidad"] = 0;
	 		
	 				
	 				
	 				
	 				
	 				$ds = $this->Comprobante->getdatasource();
	 				
	 				
	 				try{
	 					$ds->begin();
	 					
	 					
	 					
	 					$habilitado  = $this->Modulo->estaHabilitado(EnumModulo::STOCK);
	 					
	 					
	 					$message = '';
	 					
	 					if($habilitado == 1){
	 						
	 						$cantidad_anterior = $this->getCantidadGuardada($id_comprobante);//
	 						
	 						$this->AjusteStock($id_comprobante,$cantidad_anterior);
	 							
	 							
	 					//$this->RealizarMovimientoStock($id_comprobante,$message);
	 					
	 					
	 					}
	 					
	 					$this->Comprobante->updateAll(
	 							array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
	 							array('Comprobante.id' => $id_comprobante) );
	 					
	 					
	 					
	 					
	 					
	 					
	 					$status = EnumError::SUCCESS;
	 					$messagge = "La Orden de Armado/Montaje ha sido anulada ".$message;
	 					
	 					
	 					$orden_armado = $this->Comprobante->find('first', array(
	 							'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
	 							'contain' => false
	 					));
	 					
	 					$this->Auditar($id_comprobante,$orden_armado);
	 					
	 					
	 					$ds->commit();
	 					
	 				}catch(Exception $e){
	 					
	 					$ds->rollback();
	 					$tipo = EnumError::ERROR;
	 					$mensaje = "No es posible anular el Comprobante ERROR:".$e->getMessage();
	 					
	 					
	 				}
	 				
	 				/*
	 				
	 				$this->Comprobante->id = $this->request->data["Comprobante"]["id"];
	 				$this->Comprobante->saveField('definitivo', 0);
	 				$this->Comprobante->saveField('id_estado_comprobante', EnumEstadoComprobante::Abierto);
	 				
	 				//$this->edit($this->request->data["Comprobante"]["id"]);
	 				//return;
	 				
	 				*/
	 	}else{
	 		
	 		$status = EnumError::ERROR;
	 		$messagge = "ERROR: La Orden de Armado/Montaje no es posible anularla ya que se encuenta ANULADO/A.";
	 		
	 		
	 	}
	 	
	 	$output = array(
	 			"status" => $status,
	 			"message" => $messagge,
	 			"content" => ""
	 	);
	 	echo json_encode($output);
	 	die();
	 	
	 	
	 }
	 
	 
	 /**
	  * @secured(CONSULTA_ORDEN_ARMADO)
	  */
	 public function existe_comprobante()
	 {
	 	parent::existe_comprobante();
	 }
	 
	 /**
	  * @secured(CONSULTA_ORDEN_ARMADO)
	  */
	 public function excelExport($vista="default",$metodo="index",$titulo=""){
	 	
		
		set_time_limit(1200);
		ini_set('memory_limit', '512M');



		
	 	parent::excelExport($vista,$metodo,"Listado de Ordenes de Montaje");
	 	
	 	
	 	
	 }
	 
	 
}
?>