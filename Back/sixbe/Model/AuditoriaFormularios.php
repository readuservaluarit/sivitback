<?php
App::uses('AppModel', 'Model');
/**
 * AberturasLocal Model
 *
 * @property Local $Local
 */
class AuditoriaFormularios extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'au_modificacion_formularios';

}
