<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
    
    public $childRecordMessage = "No se puede eliminar el registro ya que posee registros asociados.";
    public $uniqueKeyMessage = "Ya existe otro registro con la clave que desea ingresar";
    public $decimal_places = array();
    public $virtual_fields_view = array();
    
    
    
    
       
   
    public function schema($field = false){
    
    $this->setDecimalPlaces();
    $schema =  parent::schema($field);
    $this->getExtrainfoTabla($schema);//seteo informacion extra a los campos
    
    $this->setVirtualFields();
    
     foreach($this->decimal_places as $key_decimal_places=>$cantidad_decimales){
     
        $schema[$key_decimal_places]["decimal_place"] = $cantidad_decimales;   
        
     }
     
     
   
     
     
     
     
     
     return $schema;
     }
     
     public function SetFieldsToView($virtual_fields_view,&$model){
         
       /*seteo los virtual fields*/
         foreach($virtual_fields_view as $key_virtual_fields=>$array_configuracion){
         
            
            foreach($array_configuracion as $key_propiedad_field =>$array_field){ 
                
                $model[$key_virtual_fields][$key_propiedad_field] = $array_field;   

            }   
            
            
            
         }  
         
     }
     
     
     
     public function getUseTable(){
     	
     	if(isset($this->useTable))
     		return $this>useTable;
     	else
     		return fals;
     	
     }
     
     

    public function setDecimalPlaces(){
        
        
    }
    
     public function setVirtualFields(){
        
        
    }
    
    
    
    public function getExtrainfoTabla(&$schema){
    	
    
    /*
    	if( $this->useTable!="cake_sessions"){
    	$query = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='".$this->useTable."'";
    	
    	$fields =  $this->query($query);
    	//echo $query;
   
    	if(count($fields)>0){
    		
    		if($schema){
		    	foreach($schema  as $key=>&$campo){
		    		
		    		//var_dump($fields);
		    		
		    		foreach ($fields as $campo_secundario){
		    			
		    			if($key == $campo_secundario["COLUMNS"]["COLUMN_NAME"]){
		    				
		    				if($campo_secundario["COLUMNS"]["EXTRA"] == "auto_increment")
		    					$schema[$key]["is_auto_increment"] = "1";
		    				else 
		    					$schema[$key]["is_auto_increment"] = "0";
		    				
		    			}
		    			
		    		}
		    	}
    		}
    	
    	}
    	
    	}
    	
    	*/
    }

    public function delete($id = null, $cascade = true){
        try{
            return parent::delete($id, $cascade);
        }
        catch(Exception $e){
            if(isset($e->errorInfo) && $e->errorInfo[1] == "1451") //Error de FK
                throw new ChildRecordException($this->childRecordMessage);
            else
                throw $e;
            
        }
    }
    
     public function save($data = null, $validate = true, $fieldList = array()){
        try{
           return parent::save($data, $validate, $fieldList);
        }
        catch(Exception $e){
            if(isset($e->errorInfo) && $e->errorInfo[1] == "1062") //Error de UK
                throw new UniqueKeyException($this->uniqueKeyMessage);
            else
                throw $e;
            
        }
    }
    
    
  
    

    
    public function updateAll($fields = array(), $conditions = null){
    	
    	return parent::updateAll($fields, $conditions);
    	
    }
    
    
  
     public function setFieldView($indice,$propiedades,&$model){
        
        
        foreach($propiedades as $key=>$value){
            
            
            $model[$indice][$key] = $value;
            
            
        }
        
       
       return $model; 
        
        
        
    }  
    
    
 public function getFieldByTipoMoneda($key_tipo_moneda){
      
      
      switch($key_tipo_moneda){
          
          case EnumTipoMoneda::Base:
            return "valor_moneda";
          break;
          case EnumTipoMoneda::Corriente:
            return "valor_moneda2";
          
          break;
          
          case EnumTipoMoneda::CorrienteSecundaria:
            return "valor_moneda3";
          
          default:
            return "valor_moneda2";
          
          break;
          
      }
      
      
      
  }
  
  
  public function getValorConvertidoMoneda($valor, $campo_valor){
   /*$valor es el valor a convertir
   $campo_valor es el valor del campo va*/  
   
   if($campo_valor>0)
    return round($valor/$campo_valor,2);
   else
    return  $valor;
      
      
      
  }
  
  
  
function validateDate($date, $format = 'Y-m-d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}


function formatDate($fecha){
    
    
    if(strtotime($fecha))
        return date("d-m-Y", strtotime($fecha));
    else
        return "";
}


function formatDateTime($fecha){
	
	
	if(strtotime($fecha))
		return date("d-m-Y  H:i:s", strtotime($fecha));
		else
			return "";
}



public function NombreComprobanteItemAfectaStock(){
	
	return "cantidad";
}

/**
 * Reemplaza todos los acentos por sus equivalentes sin ellos
 *
 * @param $string
 *  string la cadena a sanear
 *
 * @return $string
 *  string saneada
 */
public   function sanear_string($text)
{
	
	
	
	$text = htmlentities($text, ENT_QUOTES, 'UTF-8');
	$text = strtolower($text);
	$patron = array (
			// Espacios, puntos y comas por guion
			//'/[\., ]+/' => ' ',
			
			// Vocales
			'/\+/' => '',
			'/&agrave;/' => 'a',
			'/&egrave;/' => 'e',
			'/&igrave;/' => 'i',
			'/&ograve;/' => 'o',
			'/&ugrave;/' => 'u',
			
			'/&aacute;/' => 'a',
			'/&eacute;/' => 'e',
			'/&iacute;/' => 'i',
			'/&oacute;/' => 'o',
			'/&uacute;/' => 'u',
			
			'/&acirc;/' => 'a',
			'/&ecirc;/' => 'e',
			'/&icirc;/' => 'i',
			'/&ocirc;/' => 'o',
			'/&ucirc;/' => 'u',
			
			'/&atilde;/' => 'a',
			'/&etilde;/' => 'e',
			'/&itilde;/' => 'i',
			'/&otilde;/' => 'o',
			'/&utilde;/' => 'u',
			
			'/&auml;/' => 'a',
			'/&euml;/' => 'e',
			'/&iuml;/' => 'i',
			'/&ouml;/' => 'o',
			'/&uuml;/' => 'u',
			
			'/&auml;/' => 'a',
			'/&euml;/' => 'e',
			'/&iuml;/' => 'i',
			'/&ouml;/' => 'o',
			'/&uuml;/' => 'u',
			
			// Otras letras y caracteres especiales
			'/&aring;/' => 'a',
			'/&ntilde;/' => 'n',
			
			// Agregar aqui mas caracteres si es necesario
			
	);
	
	$text = preg_replace(array_keys($patron),array_values($patron),$text);
	return $text;
	/*
	$string = utf8_encode(trim($string));
	
	$string = str_replace(
			array('�', '�', '�', '�', '�', '�', '�', '�', '�'),
			array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
			$string
			);
	
	$string = str_replace(
			array('�', '�', '�', '�', '�', '�', '�', '�'),
			array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
			$string
			);
	
	$string = str_replace(
			array('�', '�', '�', '�', '�', '�', '�', '�'),
			array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
			$string
			);
	
	$string = str_replace(
			array('�', '�', '�', '�', '�', '�', '�', '�'),
			array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
			$string
			);
	
	$string = str_replace(
			array('�', '�', '�', '�', '�', '�', '�', '�'),
			array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
			$string
			);
	
	$string = str_replace(
			array('�', '�', '�', '�'),
			array('n', 'N', 'c', 'C',),
			$string
			);
	
	
	*/
	
	//return $string;
}


public function getActivoModulo($id_modulo){
	
	
	App::uses('CakeSession', 'Model/Datasource');
	$modulos = CakeSession::read('Modulo');
	
	
	foreach($modulos as $modulo){
		
		if($modulo["id"] == $id_modulo)
			return $modulo["habilitado"];
	}
	
	
	

}


public function getCacheModel(){

	$dato_empresa_class = ClassRegistry::init(EnumModel::DatoEmpresa);
	//$getModelCache = unserialize(Cache::read(EnumCacheName::getModel));
	
	set_time_limit(1200);
	ini_set('memory_limit', '512M');//le doy memoria asi levanta el archivo que es pesado
	
	
	$getModelCache = unserialize(file_get_contents(EnumCacheName::getModel));
	
	
	if ($getModelCache == false) {
		
		
		/*
		 $resultado = $this->requestAction(
		 array('controller' => "TestControladores" , 'action' => 'creaCacheLogin'),
		 array('data' => null)
		 );
		 */
		
		file_put_contents(EnumCacheName::getModel,serialize($array_model));
		
		$getModelCache = unserialize(file_get_contents(EnumCacheName::getModel));
		
		//$getModelCache = unserialize(Cache::read(EnumCacheName::getModel));
		
		
	}
	
	
	// echo 'true';
	// die();
	
	$datoEmpresa = $dato_empresa_class->find('first', array('conditions' => array('DatoEmpresa.id' => 1), 'contain' => array()));
	
	
	
	
	$output = array(
			"status" => EnumError::SUCCESS,
			"message" => "list",
			"content" =>$getModelCache,
			"page_count" =>0,
			"product_version"=>$datoEmpresa["DatoEmpresa"]["product_version"]
	);

	return $output;
	
	
}


public function getCacheStaticIndex(){
	

	//$getModelCache = unserialize(Cache::read(EnumCacheName::getModel));
	
		$getCacheIndex = unserialize(file_get_contents(EnumCacheName::getCacheIndexTablasEstaticaSistema));
	
	
	
		
		$array_aux = array();
		
		foreach($getCacheIndex as $itemcache){
			
			
			array_push($array_aux, $itemcache);
			
		}
	
		
		
		
		
		return $array_aux;
		//$getModelCache = unserialize(Cache::read(EnumCacheName::getModel));
		
		

	
	
	
	
}

public function esBisiesto($year=NULL) {
	$year = ($year==NULL)? date('Y'):$year;
	return ( ($year%4 == 0 && $year%100 != 0) || $year%400 == 0 );
}



    


}



