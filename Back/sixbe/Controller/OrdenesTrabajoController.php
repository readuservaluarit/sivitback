<?php

App::uses('ComprobantesController', 'Controller');
class OrdenesTrabajoController extends ComprobantesController {
	
	public $name = EnumController::OrdenesTrabajo;
	public $model = 'Comprobante';
	public $helpers = array ( 'Paginator', 'Js');
	public $components = array('PaginatorModificado', 'RequestHandler');
	public $traigo_items=0;
	public $requiere_impuestos = 0;
	public $title_form  = "Listado de Ordenes de Trabajo";
	
	/**
	 * @secured(CONSULTA_ORDEN_TRABAJO)
	 */
	public function index() {
		
		if($this->request->is('ajax'))
			$this->layout = 'ajax';/*SIN PUNTO Y COMA A PROPOSITO*/
		
			$this->loadModel(EnumModel::ComprobanteItem);
			$this->loadModel(EnumModel::ArticuloRelacion);
			$conditions = $this->RecuperoFiltros($this->model);
			
			$conditions_items = array();
			
			$id_detalle_tipo_comprobante = $this->getFromRequestOrSession('ComprobanteItem.id_detalle_tipo_comprobante');
			$id_producto= $this->getFromRequestOrSession('ComprobanteItem.id_producto');
			$d_producto= $this->getFromRequestOrSession('ComprobanteItem.d_producto');
			$codigo_producto= $this->getFromRequestOrSession('ComprobanteItem.codigo_producto');
			
			 // $id_detalle_tipo_comprobante = EnumDetalleTipoComprobante::OrdenDeTrabajoCabecera;
			 // $id = 87162;
			
			 // if($id !="")
				 // array_push($conditions,array("Comprobante.id"=>$id));
			
			
			if($id_detalle_tipo_comprobante !="")
				array_push($conditions,array("ComprobanteItem.id_detalle_tipo_comprobante"=>$id_detalle_tipo_comprobante));

			if($id_producto!="")
					array_push($conditions,array("ComprobanteItem.id_producto"=>$id_producto));
			
			if($id_producto == ""){		
				if($codigo_producto !="")		
					array_push($conditions,array("Producto.codigo LIKE"=> '%'.$codigo_producto.'%'));
			
				if($d_producto!="")
					array_push($conditions,array("Producto.d_producto LIKE"=> '%'.$d_producto.'%'));
			}
			
			$this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
					'contain' =>array("Producto","Comprobante"=>array('Movimiento','Usuario','Persona','EstadoComprobante','Moneda',
								                                      'CondicionPago','TipoComprobante','PuntoVenta','Usuario')),
					'conditions' => $conditions,
					'limit' => $this->numrecords,
					'page' => $this->getPageNum(),
					'order' => $this->model.'.nro_comprobante desc'
			);
			
			if($this->RequestHandler->ext != 'json'){
				App::import('Lib', 'FormBuilder');
				$formBuilder = new FormBuilder();
				
				$formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
				
				//Headers
				$formBuilder->addHeader('id', 'cotizacion.id', "10%");
				$formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
				$formBuilder->addHeader('CUIT', 'cotizacion.cuit', "20%");
				$formBuilder->addHeader('Email', 'cliente.email', "30%");
				$formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");
				
				//Fields
				$formBuilder->addField($this->model, 'id');
				$formBuilder->addField($this->model, 'razon_social');
				$formBuilder->addField($this->model, 'cuit');
				$formBuilder->addField($this->model, 'email');
				$formBuilder->addField($this->model, 'tel');
				
				$this->set('abm',$formBuilder);
				$this->render('/FormBuilder/index');
				//vista formBuilder
			}
			
			else{ // vista json
				$this->PaginatorModificado->settings = $this->paginate; 
				$data = $this->PaginatorModificado->paginate(EnumModel::ComprobanteItem);
				$page_count = $this->params['paging'][EnumModel::ComprobanteItem]['pageCount'];
				
				foreach($data as &$valor){
					
					//$valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
					//unset($valor['ComprobanteItem']);
					if($id_detalle_tipo_comprobante == EnumDetalleTipoComprobante::OrdenDeTrabajoCabecera)
					{	
							$mpc = $this->ArticuloRelacion->find('all', array(
									'conditions' => array('ArticuloRelacion.id_producto_padre' =>  $valor["ComprobanteItem"]["id_producto"]),
									'contain' =>false ));
							
							//calcular la cantidad recibida en movimientos devolver campos cantidad_parcial cantidad_pendiente
							$cantidad_retirado = 0;
							$cantidad_terminado = 0;
							
							if( count($valor["Comprobante"]["Movimiento"]) > 0 ){//si tiene movimientos
								foreach ($valor["Comprobante"]["Movimiento"] as $movimiento){
									if($movimiento["id_movimiento_tipo"] == EnumTipoMovimiento::RetirodeMateriaPrimaPorOrdenProduccion)
										$cantidad_retirado +=  $movimiento["cantidad"];
										if($movimiento["id_movimiento_tipo"] == EnumTipoMovimiento::IngresoDeComponenteParcialPorOrdenProduccion)
											$cantidad_terminado +=  $movimiento["cantidad"];
								}
							}
							$cant_insume = 0;
							if($mpc){ //por cada materiaPrima encontrada actualizo el stock y le resto lo que insume de materia prima ese componente
								foreach($mpc as $materia_prima_componente){
									
									$cant_insume += $materia_prima_componente["ArticuloRelacion"]["cantidad_hijo"];
								}
							}
							//MP: cantidad que insume de MP cada unidad de Componente
							//$cant_insume = $valor["Componente"]["ArticuloRelacion"]["cantidad"];
							
							$valor[EnumModel::ComprobanteItem]["c_retirado"] =   $cantidad_retirado;
							$valor[EnumModel::ComprobanteItem]["c_terminado"] =  $cantidad_terminado;
							
							$valor[EnumModel::ComprobanteItem]["mp_cantidad"] = (string)$valor["ComprobanteItem"]["cantidad"]*$cant_insume;
							$valor[EnumModel::ComprobanteItem]["mp_retirado"] = (string)$valor["ComprobanteItem"]["c_retirado"] * $cant_insume;
							$valor[EnumModel::ComprobanteItem]["mp_terminado"] = (string)$valor["ComprobanteItem"]["c_terminado"] * $cant_insume;  
					}	
					
					if(isset($valor["Comprobante"]['TipoComprobante'])){
						$valor[EnumModel::ComprobanteItem]['d_tipo_comprobante'] = $valor["Comprobante"]['TipoComprobante']['d_tipo_comprobante'];
						$valor[EnumModel::ComprobanteItem]['codigo_tipo_comprobante'] = $valor["Comprobante"]['TipoComprobante']['codigo_tipo_comprobante'];
						unset($valor["Comprobante"]['TipoComprobante']);
					}

					if(isset($valor["Comprobante"]['PuntoVenta']))
						$valor[EnumModel::ComprobanteItem]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor["Comprobante"]['PuntoVenta']['numero'],$valor["Comprobante"]['nro_comprobante']);
						/*
						 foreach($valor[$this->model]['ComprobanteItem'] as &$producto ){
						 
						 $producto["d_producto"] = $producto["Producto"]["d_producto"];
						 $producto["codigo_producto"] = $this->getProductoCodigo($producto);;

						 unset($producto["Producto"]);
						 }
						 */
						
						
						if(isset($valor["Comprobante"]['EstadoComprobante'])){
							$valor[EnumModel::ComprobanteItem]['d_estado_comprobante'] = $valor["Comprobante"]['EstadoComprobante']['d_estado_comprobante'];
							unset($valor['EstadoComprobante']);
						}
					
						$valor[EnumModel::ComprobanteItem]['codigo_producto'] = (string) $valor["Producto"]["codigo"];
						$valor[EnumModel::ComprobanteItem]['id_producto'] = (string) $valor["Producto"]["id"];
						$valor[EnumModel::ComprobanteItem]['fecha_generacion'] = (string) $valor["Comprobante"]["fecha_generacion"];
						$valor[EnumModel::ComprobanteItem]['d_usuario'] = (string) $valor["Comprobante"]["Usuario"]["username"];

						
						$valor[$this->model]['d_usuario'] = $valor['Usuario']["nombre"];//esto es por el certificado de Calidad
						
						if(isset($valor['Usuario'])){
							
							$valor[EnumModel::ComprobanteItem]['d_usuario'] = $valor['Usuario']['nombre'];
						}
						
						$this->{$this->model}->formatearFechas($valor);

						$valor["ComprobanteItem"]["id"] = $valor["ComprobanteItem"]["id_comprobante"];
						unset($valor["ComprobanteItem"]["id_comprobante"]);
						
						$valor["Comprobante"] = $valor["ComprobanteItem"];
						
						unset($valor["ComprobanteItem"]);
						unset($valor["Producto"]);
				}
				$this->data = $data;
				$output = array(
						"status" =>EnumError::SUCCESS,
						"message" => "list",
						"content" => $data,
						"page_count" =>$page_count
				);
				$this->set($output);
				$this->set("_serialize", array("status", "message","page_count", "content"));
			}
	}
	
	
	
	/**
	 * @secured(ADD_ORDEN_TRABAJO)
	 */
	public function add() {
		 // echo json_encode($this->request->data);//ESTVACIO POR ESO LO COMENTO ABAJO
		 // die();
		
		$data = $this->request->data;

		parent::add();
	}
	
	private  function deleteItems($id_comprobante){
		
		$this->loadModel($this->model);
		$this->loadModel("ComprobanteItemLote");
		
		
		$ds = $this->Comprobante->getdatasource();
		
		try{ 
			$ds->begin();
			
			$conditions = array();
			array_push($conditions, array('ComprobanteItem.id_comprobante' => $id_comprobante));
			array_push($conditions, array('ComprobanteItem.id_detalle_tipo_comprobante' => EnumDetalleTipoComprobante::EtapaOrdenTrabajo));
			
			/* $conditions = array();
			array_push($conditions, array('ComprobanteItem.id_comprobante' => $id_comprobante));
			array_push($conditions, array('ComprobanteItem.id_detalle_tipo_comprobante' => EnumDetalleTipoComprobante::OperacionOrdenDeTrabajo));*/
			
			/*Busco los registros de comprobante_item que pueden estar asociados con un LOTE*/
			$comprobante_item_etapa = $this->Comprobante->ComprobanteItem->find('all', array(
					'conditions' => $conditions,
					'contain'=>array("Persona","Producto"),
					'order' => 'ComprobanteItem.id asc'
			));
			$array_id_item_etapa = array();
			
			if(count($comprobante_item_etapa)>0) //recorro y lleno el array para borrar todo
			{
				foreach($comprobante_item_etapa as $etapa ){
					array_push($array_id_item_etapa, $etapa["ComprobanteItem"]["id"]);
				}
			}
			
			if(count($array_id_item_etapa)>0){//busco todos los items de todas las etapas
				
				$conditions = array();
				
				array_push($conditions, array('ComprobanteItem.id_comprobante_item_origen' => $array_id_item_etapa));
				
				$comprobante_item_perteneciente_etapa = $this->Comprobante->ComprobanteItem->find('all', array(
						'conditions' => $conditions,
						'contain'=>array("Persona","Producto"),
						'order' => 'ComprobanteItem.id asc'
				));
				$array_id_item_perteneciente_etapa = array();
				
				foreach($comprobante_item_perteneciente_etapa as $item_etapa ){
					array_push($array_id_item_perteneciente_etapa, $item_etapa["ComprobanteItem"]["id"]);
				}
				if(count($comprobante_item_perteneciente_etapa)>0)
					$this->ComprobanteItemLote->deleteAll(array("ComprobanteItemLote.id_comprobante_item"=>$array_id_item_perteneciente_etapa));
			}
			$this->Comprobante->query("SET FOREIGN_KEY_CHECKS=0; DELETE from comprobante_item where id_comprobante=".$id_comprobante.";SET FOREIGN_KEY_CHECKS=1;");
			
			//$this->Comprobante->ComprobanteItem->deleteAll(array("ComprobanteItem.id_comprobante"=>$id_comprobante));
			//$this->Comprobante->query("SET FOREIGN_KEY_CHECKS=1");
			
			$ds->commit();
			return 0;
		}catch(Exception $e){
			
			$ds->rollback();    
			return 1;
		}
	}
	
	
	public function SaveItems(&$data,&$message=''){//este metodo se usa tanto para el add como el edit
		

		if(isset($data['Comprobante']['ComprobanteItem'])){
			
			$item_cabecera_ot = $data['Comprobante']['ComprobanteItem'];
			unset($item_cabecera_ot["Etapa"]);//le quito la etapa asi se guarda limpio
			
			
			if(!isset($data['Comprobante']["id"]))
				$id_ot = $this->{$this->model}->id;
			else 
				$id_ot = $data['Comprobante']["id"];
			
			$item_cabecera_ot['id_comprobante'] = $id_ot;//le asigno el ID de la OT
			
			//$this->Comprobante->ComprobanteItem->saveAll($item_cabecera_ot);
			
			if(!$this->Comprobante->ComprobanteItem->saveAll($item_cabecera_ot)){
				
				$errores = $this->Comprobante->ComprobanteItem->validationErrors;
				$message = $this->getErrors($errores);
				$error = 1;
				return $error;
			}else{
				$error = 0;//NO hubo errores sigue todo bien
			}
			
			
			//ya tengo el id del Detalle 69 asique el ComprobanteItem Cabecera lo recupero
			if(isset($data['Comprobante']['ComprobanteItem']["Etapa"]) && count($data['Comprobante']['ComprobanteItem']["Etapa"]) >0){
					
					//EL id_detalle_comprobante aca debe ser el 70 y se debe setear el id_comprobante OT
					foreach ($data['Comprobante']['ComprobanteItem']["Etapa"] as $etapa_item)
					{//recorro cada etapa
						
						$etapa_item_cabecera = $etapa_item["ComprobanteItem"];
						$etapa_item_cabecera["id_comprobante"] = $id_ot;
						
						
						if(isset($etapa_item["ComprobanteItem"]["n_item"])){
						if(!$this->Comprobante->ComprobanteItem->saveAll($etapa_item_cabecera)){//doy de alta la etapa

							$errores = $this->Comprobante->ComprobanteItem->validationErrors;
							$message = $this->getErrors($errores);
							$error = 1;
							return $error;
						}else{
							$id_etapa = $this->Comprobante->ComprobanteItem->id;//obtengo el id recien insertado de la etapa
							$error = 0;//NO hubo errores sigue todo bien
							
							//Las etapas pueden tener detalles y los detalles pueden tener Lotes o Instrumental
							if(isset($etapa_item["ComprobanteItem"]["Detalle"]) && count($etapa_item["ComprobanteItem"]["Detalle"]) >0){//tengo detalles los recorro
								foreach ($etapa_item["ComprobanteItem"]["Detalle"] as $detalle_item){//recorro cada detalle
									//el detalle es hijo de la etapa
									$detalle_item["ComprobanteItem"]["id_comprobante"] = $id_ot;
									$detalle_item["ComprobanteItem"]["id_comprobante_item_origen"] = $id_etapa;
									unset($detalle_item["ComprobanteItem"]["id"]);
									
									if(!$this->Comprobante->ComprobanteItem->saveAll($detalle_item)){
										
										$id_etapa = $this->Comprobante->ComprobanteItem->id;//obtengo el id recien insertado de la etapa
										
										$errores = $this->Comprobante->ComprobanteItem->validationErrors;
										$message = $this->getErrors($errores);
										$error = 1;
										return $error;
									}else{//inserto bien la "cabecera" del detalle entonces prosigo
										//puede haber Lotes e instrumental
											
										$id_cabecera_detalle = $this->Comprobante->ComprobanteItem->id;
										
										//ahora chequeo si hay Lotes e Instrumental
										
										if(isset($detalle_item["ComprobanteItem"]["Lotes"]) && count($detalle_item["ComprobanteItem"]["Lotes"]) >0){//si Tiene Lotes este Detalle Entonces los doy de alta
										
											foreach ($detalle_item["ComprobanteItem"]["Lotes"] as &$lote_item){//recorro los Lotes y le asigno el id del detalle
											
												$lote_item["ComprobanteItemLote"]["id_comprobante_item"] = $id_cabecera_detalle;
												unset($lote_item["ComprobanteItemLote"]["id"]);
												
												if(isset($lote_item["ComprobanteItemLote"]["id_lote"]) && $lote_item["ComprobanteItemLote"]["id_lote"]>0)//estructura BASE de .NET manda a veces null el id_lote a mejorar
													$this->Comprobante->ComprobanteItem->ComprobanteItemLote->saveAll($lote_item);//grabo el LoteItem
											}
										}
										if(isset($detalle_item["ComprobanteItem"]["Instrumental"]) && count($detalle_item["ComprobanteItem"]["Instrumental"]) >0){//si Tiene Lotes este Detalle Entonces los doy de alta
											
											foreach ($detalle_item["ComprobanteItem"]["Instrumental"] as &$instrumental_item){//recorro los Instrumentos y le asigno el id del detalle y el de OT
												
												$instrumental_item["ComprobanteItem"]["id_comprobante_item_origen"] = $id_cabecera_detalle;
												$instrumental_item["ComprobanteItem"]["id_comprobante"] = $id_ot;
												$instrumental_item["ComprobanteItem"]["id_detalle_tipo_comprobante"] = EnumDetalleTipoComprobante::InstrumentalDeTrabajo;
												unset($instrumental_item["ComprobanteItem"]["id"]);
												
												if(isset($instrumental_item["ComprobanteItem"]["id_producto"]) && $instrumental_item["ComprobanteItem"]["id_producto"]>0)//estructura BASE de .NET manda a veces null el id_producto a mejorar
													$this->Comprobante->ComprobanteItem->saveAll($instrumental_item);//grabo el Instrumental
											}
										}
									}
							}
						}
					}
					
					}
					
					
				}
			}
		}
	}
	 
	 /**
	  * @secured(MODIFICACION_ORDEN_TRABAJO)
	  */
	 public function edit($id) {
		 
		 // echo json_encode("topu");//POR Q ESTA VACIO?
		 // echo json_encode($this->request->data);//POR Q ESTA VACIO?
		 // die();
		 
	 	$this->is_edit = 1;
	 	$id_estado_comprobante_grabado = $this->{$this->model}->getEstadoGrabado($id);
	 	
		//echo json_encode($this->request->data);//ESTVACIO POR ESO LO COMENTO ABAJO
		//die();
		
	 	if($id_estado_comprobante_grabado != EnumEstadoComprobante::Anulado ){
		//&& $this->request->data["Comprobante"]["id_estado_comprobante"] != EnumEstadoComprobante::Anulado){ //ESTA COMENTADO POR Q ESTA VACIO VER PORQUE NO SE LLENA
		
	 		
	 		$retorno = $this->deleteItems($id);
	 		
	 		if($retorno == 0){
	 			$retorno2 = $this->SaveItems($this->request->data,$mensaje);
		 		
		 		if($retorno2 == 0){
		 			parent::edit($id);
		 			return;
		 		}else{
		 			$status = EnumError::ERROR;
		 		
		 		}
	 		
	 		}else{
	 			
	 			$status = EnumError::ERROR;
	 			$mensaje = "No es posible borrar los items en la edici&oacute;n";
	 		}
	 	}else{
	 		
	 		$this->Anular($id);
	 		return;
	 	}
	 	
	 	
	 	$output = array(
	 			"status" => $status,
	 			"message" => $mensaje,
	 			"content" => "",
	 	);
	 	$this->output = $output;
	 	$this->set($output);
	 	$this->set("_serialize", array("status", "message", "content"));        
	 }
	 
	 public function pdfExport($id,$vista=''){

	 	$this->loadModel("Comprobante");
	 	$this->loadModel("ComprobanteItem");
	 	$this->loadModel("DetalleTipoComprobante");
	 	
	 	$this->Comprobante->id = $id;
	 	$this->Comprobante->contain(
 	    array('ComprobanteItem'=>
                array('Producto' => 
                        array('ArticuloRelacion' => 
                              array('ArticuloHijo'),'ComprobanteItemLote'=>array("Lote")),
                        'ComprobanteItemOrigen'=>
                              array("Comprobante")),'Persona','TipoComprobante','PuntoVenta' ));
	 	
	 	//$this->Cotizacion->order(array('CotizacionItem.orden asc'));
	 	
	 	
	 	$ot1 = $this->Comprobante->find('first',array('contain'=>array('ComprobanteItem'=>
	 			array('Producto' =>
	 					array('ArticuloRelacion' =>
	 							array('ArticuloHijo'),'ComprobanteItemLote'=>array("Lote")),
	 					'ComprobanteItemOrigen'=>
	 					array("Comprobante"),'order'=>"ComprobanteItem.n_item ASC"),'Persona','TipoComprobante','PuntoVenta' ),
	 			'conditions' =>array('Comprobante.id'=>$id),
	 		
	 	));
	 	
	 	
	 	
	 	
	 
	 	

	 	

	 	Configure::write('debug',0);
	 	
	 	$fecha_entrega_item = "";
	 	$comprobante_item  =  new ComprobanteItem;
	 	/*
	 	 if(isset($this->request->data["ComprobanteItem"][0]["id_producto"]) && $this->request->data["ComprobanteItem"][0]["id_producto"] > 0 ){
	 	 foreach($this->request->data["Comprobante"]["ComprobanteItem"] as $item){
	 	 if( $item["id_producto"] == $this->request->data["Comprobante"]["id_producto"] ){
	 	 $fecha_entrega_item = $comprobante_item->getFechaEntrega(date("Y-m-d", strtotime($this->request->data["Comprobante"]["fecha_generacion"])), $item['dias'],$this->request->data["Comprobante"]["fecha_entrega"]);
	 	 $fecha = $fecha_entrega_item;
	 	 $nuevafecha = strtotime ( '-2 day' , strtotime ( $fecha ) ) ;
	 	 $nuevafecha = date ( 'j-m-Y' , $nuevafecha );
	 	 }
	 	 
	 	 
	 	 
	 	 }
	 	 }
	 	 
	 	 
	 	 $this->request->data["Comprobante"]["fecha_limite_armado"]= $nuevafecha;
	 	 */
	 	
	 	
	 	//$dias_fecha_limite_oa = $this->DetalleTipoComprobante->getDias2(EnumDetalleTipoComprobante::OrdenArmadoPorPedidoInterno);
	 	
	 	
	 	
	 	
	 /*
	 	if(isset($this->request->data['ComprobanteItem'][0]['ComprobanteItemOrigen']) && $this->request->data['ComprobanteItem'][0]['ComprobanteItemOrigen']['id']>0){
	 		$fecha_entrega_pedido = $this->request->data['ComprobanteItem'][0]['ComprobanteItemOrigen']["fecha_entrega"];
	 		
	 		$fecha = $this->request->data['ComprobanteItem'][0]['ComprobanteItemOrigen']["fecha_entrega"];
	 		
	 		$nuevafecha = strtotime ( '-'.$dias_fecha_limite_oa.'day' , strtotime ( $fecha ) ) ;
	 		$nuevafecha = date ( 'j-m-Y' , $nuevafecha );
	 		$this->request->data["Comprobante"]["fecha_limite_armado"]= $nuevafecha;
	 		
	 	}*/
	 	
	 	
	 	$etapas = array();
	 	foreach($ot1["ComprobanteItem"] as $item){
	 	
	 		if($item["id_detalle_tipo_comprobante"] == EnumDetalleTipoComprobante::EtapaOrdenTrabajo){
	 			
	 			
	 			//busco el registro de la maquina
	 			$maquina = $this->ComprobanteItem->find('first',array('contain'=>array('Producto'),'conditions'=>array("ComprobanteItem.id_detalle_tipo_comprobante"=>EnumDetalleTipoComprobante::ReservadeBiendeUsoOrdenDeTrabajo,"ComprobanteItem.id_comprobante_item_origen"=>$item["id"])));
	 			$item["maquina"] = $maquina;
	 			array_push($etapas, $item);
	 		}
	 			
	 	}
	 	
	 	
	
	 	
	 	$this->set('etapas_ot1',$etapas);
	 	

	 	
	 	$ot1["Comprobante"]["nro_comprobante_completo"] =
	 	$this->Comprobante->GetNumberComprobante($ot1["PuntoVenta"]["numero"],$ot1["Comprobante"]["nro_comprobante"]);
	 	
	 	
	 	$ot2["Comprobante"]["nro_comprobante_completo"] =
	 	$this->Comprobante->GetNumberComprobante($ot2["PuntoVenta"]["numero"],$ot2["Comprobante"]["nro_comprobante"]);
	 	$this->set('datos_empresa',$this->Session->read('Empresa'));
	 	$this->set('ot1',$ot1);
	
	 	$this->set('id',$id);
	 	Configure::write('debug',0);
	 	
	 	$this->response->type('pdf');
	 	$this->layout = 'pdf'; //esto usara el layout pdf.ctp
	 	$this->render();	
	 }
	 
	 /**
	  * @secured(CONSULTA_ORDEN_TRABAJO)
	  */
	 public function getModel($vista='default'){
	 	
	 	$model = parent::getModelCamposDefault();//esta en APPCONTROLLER
	 	$model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
	 	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
	 	
	 }
	 
	 
	 private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
	 	
	 	$this->set('model',$model);
	 	Configure::write('debug',0);
	 	$this->render($vista);

	 }
	 
	 
	 protected function genera_movimiento_stock($id_pi,$action,$cantidad_calculada=0,$data =null,$simbolo_operacion=1) {
	 	
	 	
	 	if(isset($this->is_edit) || $this->is_edit == 1){
	 		$action = EnumTipoMovimiento::OrdenArmadoAjuste;
	 		$cantidad_anterior = $this->getCantidadGuardada($id_pi);//obtiene la cantidad guardada del unico item de la Orden de Armado
	 		$diferencia = 0;
	 		
	 		if($cantidad_anterior != $this->request->data["ComprobanteItem"][0]["cantidad_cierre"]){
	 			$diferencia = $this->request->data["ComprobanteItem"][0]["cantidad_cierre"] - $cantidad_anterior;
	 			$cantidad_calculada = $diferencia;
	 		}
	 	}
	 	
	 	$id_deposito = 0;
	 	$id_deposito_destino = 0;
	 	
	 	if(isset($this->request->data["Comprobante"]["id_deposito_origen"]))
	 		$id_deposito = $this->request->data["Comprobante"]["id_deposito_origen"];
	 		
	 		if(isset($this->request->data["Comprobante"]["id_deposito_destino"]))
	 			$id_deposito_destino = $this->request->data["Comprobante"]["id_deposito_destino"];
	 			
	 			
	 			$action2 = $action;
	 			
	 			/*Esto se hace para que el deposito de ajuste sea igual al de alta*/
	 			if($action == EnumTipoMovimiento::OrdenArmadoAjuste)
	 				$action2 = EnumTipoMovimiento::OrdenArmadoAlta;
	 				
	 				//	$resultado = $this->getDepositoDefault($action2,$id_deposito,$id_deposito_destino); //devuelvo un 0 si no definio el deposito default
	 				
	 				switch($action){
	 					
	 					case EnumTipoMovimiento::OrdenArmadoAlta:
	 					case EnumTipoMovimiento::OrdenArmadoCierre:
	 						
	 						$data = array();
	 						$data["Movimiento"]["id_movimiento_tipo"] = $action;
	 						$data["Movimiento"]["id_comprobante"] = $id_pi;
	 						$data["Movimiento"]["id_deposito_origen"] = $id_deposito;
	 						$data["Movimiento"]["id_deposito_destino"] = $id_deposito_destino;
	 						$data["Movimiento"]["llamada_interna"] = 1;
	 						$data["Movimiento"]["simbolo_operacion"] = 1;//resta o suma
	 						break;
	 						
	 					case EnumTipoMovimiento::OrdenArmadoAjuste:
	 						$data = array();
	 						$data["Movimiento"]["id_movimiento_tipo"] = $action;
	 						$data["Movimiento"]["id_comprobante"] = $id_pi;
	 						$data["Movimiento"]["id_deposito_origen"] = $id_deposito;
	 						$data["Movimiento"]["id_deposito_destino"] = $id_deposito_destino;
	 						$data["Movimiento"]["llamada_interna"] = 1;
	 						$data["Movimiento"]["diferencia"] = $cantidad_calculada;//resta o suma
	 						break;
	 						
	 				}
	 				
	 				
	 				if($id_deposito_destino!=0)
	 					$data["Movimiento"]["id_deposito_destino"] = $id_deposito_destino;
	 					
	 					//realizo la llamada al controller y le envio el movimiento
	 					
	 					
	 					if(!isset($this->is_edit) || $this->is_edit== 0){
	 						$this->requestAction(
	 								array('controller' => 'Movimientos', 'action' => 'add'),
	 								array('data' => $data)
	 								);
	 					}else{//en el edit chequeo si modifico la cantidad si la modifico hago movimiento por la diferencia, que puede ser para arriba o para abajo
	 						
	 						
	 						
	 						if($cantidad_anterior != $this->request->data["ComprobanteItem"][0]["cantidad_cierre"]  ){
	 							
	 							$this->requestAction(
	 									array('controller' => 'Movimientos', 'action' => 'add'),
	 									array('data' => $data)
	 									);
	 						}
	 						
	 						
	 						
	 					}
	 					
	 					return true;
	 }
	 
	 
	 
	 protected function AjusteStock($id_ORDEN_TRABAJO,$cantidad_anterior){
	 	
	 	
	 	if($this->request->data["Comprobante"]["id_estado"]!= EnumEstadoComprobante::CerradoReimpresion && isset($this->request->data["Comprobante"]["genera_movimiento_stock"]) && $this->request->data["Comprobante"]["genera_movimiento_stock"] == 1){ //chequeo si modifico la cantidad fue modificada
	 		
	 		if($cantidad_anterior != $this->request->data["ComprobanteItem"][0]["cantidad"]  ){ //si cambio debo hacer un movimiento
	 			
	 			$diferencia = $this->request->data["ComprobanteItem"][0]["cantidad_cierre"] - $cantidad_anterior;
	 			$this->genera_movimiento_stock($id_ORDEN_TRABAJO,EnumTipoMovimiento::OrdenArmadoAjuste,$diferencia);
	 			
	 			
	 			
	 		}
	 	}
	 }
	 
	 
	 protected function getCantidadGuardada($id_ORDEN_TRABAJO){
	 	
	 	$orden_armado = $this->Comprobante->ComprobanteItem->find('first',array(
	 			'conditions' => array('ComprobanteItem.id_comprobante' => $id_ORDEN_TRABAJO),
	 			'contain' =>array('Comprobante')
	 	));
	 	
	 	return $orden_armado["ComprobanteItem"]["cantidad_cierre"];
	 	
	 }
	 	  
	  
	  protected function getEstadoAnterior($id_ORDEN_TRABAJO){
	  	
	  	$orden_armado = $this->OrdenArmado->find('first',array(
	  			'conditions' => array('OrdenArmado.id' => $id_ORDEN_TRABAJO),
	  			'contain' =>false
	  	));
	  	
	  	return $orden_armado["OrdenArmado"]["id_estado"];
	  }
	  
	  
	  /**
	   * @secured(CONSULTA_ORDEN_TRABAJO)
	   */
	  public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
	  {
	  	parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
	  }
	  
	  
	  
	  /**
	   * @secured(MODIFICACION_ORDEN_TRABAJO)
	   */
	  public function Anular($id_comprobante){
	  	
	  	$this->loadModel("Comprobante");
	  	$this->loadModel("ComprobanteItem");
	  	$this->loadModel("Movimiento");
	  	$this->loadModel("Modulo");
	  	$this->anulo = 1;
	  	$puede_anular = 1;
	  	
	  	$oa = $this->Comprobante->find('first', array(
	  			'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
	  			'contain' => array('ComprobanteItem')
	  	));
	  	
	  	
	  	if( $oa["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::OrdenTrabajo  && 
			$this->Comprobante->getEstadoGrabado($id_comprobante) != EnumEstadoComprobante::Anulado
	  			)
				{
					$this->is_edit = 1;//es un Edit porque no puede anular sin haber creado
					$this->request->data = $oa;
					$ds = $this->Comprobante->getdatasource();
	  			
	  				try{
	  					$ds->begin();

	  					/*
	  					$habilitado  = $this->Modulo->estaHabilitado(EnumModulo::STOCK);
	  					
	  					
	  					$message = '';
	  					
	  					if($habilitado == 1){ //TODO: Si tiene mov stock anular
	  						
	  						//$this->RealizarMovimientoStock($id_comprobante,$message);
	  						
	  						
	  					}*/
	  					
	  					$this->Comprobante->updateAll(
	  							array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
	  							array('Comprobante.id' => $id_comprobante) );
	  					
	  					
	  					
	  					
	  					
	  					
	  					$status = EnumError::SUCCESS;
	  					$messagge = "La Orden de Trabajo ha sido anulada ".$message;
	  					
	  					$ds->commit();
	  					
	  				}catch(Exception $e){
	  					$ds->rollback();
	  					$tipo = EnumError::ERROR;
	  					$mensaje = "No es posible anular el Comprobante ERROR:".$e->getMessage();
	  				}
	  				
	  				/*
	  				$this->Comprobante->id = $this->request->data["Comprobante"]["id"];
	  				$this->Comprobante->saveField('definitivo', 0);
	  				$this->Comprobante->saveField('id_estado_comprobante', EnumEstadoComprobante::Abierto);
	  				
	  				//$this->edit($this->request->data["Comprobante"]["id"]);
	  				//return;
	  				*/
	  	}else{
	  		
	  		$status = EnumError::ERROR;
	  		$messagge = "ERROR: La Orden de Trabajo no es posible anularla ya que se encuenta ANULADO/A.";
	  	}
	  	
	  	$output = array(
	  			"status" => $status,
	  			"message" => $messagge,
	  			"content" => ""
	  	);
	  	echo json_encode($output);
	  	die();
	  }
	  
	  
	  /**
	   * @secured(CONSULTA_ORDEN_TRABAJO)
	   */
	  public function existe_comprobante()
	  {
	  	parent::existe_comprobante();
	  }
	  
	  
	  public function indexMD($id_orden_trabajo) {
	  	
	  	if($this->request->is('ajax'))
	  		$this->layout = 'ajax';
	  		
	  		$this->loadModel($this->model);
	  		$this->loadModel("ComprobanteItem");
	  		$this->loadModel("ComprobanteItemLote");
	  		$this->loadModel("Comprobante");
			
			$this->loadModel(EnumModel::ArticuloRelacion);
	  		
	  		$comprobante_item = new ComprobanteItem();
	  		
	  		$this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
	  		//$conditions = $comprobante_item->RecuperoFiltros("ComprobanteItem");
	  		$comprobante_ot = $this->Comprobante->find('first', array('conditions'=>
						array("Comprobante.id"=>$id_orden_trabajo),'contain'=>false));
						
	  		$conditions = array();
	  		
	  		array_push($conditions, array('ComprobanteItem.id_comprobante' => $id_orden_trabajo));
	  		array_push($conditions, array('ComprobanteItem.id_comprobante_item_origen' => null));
	  		array_push($conditions, array('ComprobanteItem.id_detalle_tipo_comprobante' => EnumDetalleTipoComprobante::OrdenDeTrabajoCabecera));
	  		
	  		$this->paginate = array(
	  				/*'contain' =>array('Persona','EstadoComprobante','TipoComprobante','DepositoOrigen','DepositoDestino','PuntoVenta','ComprobanteItem'=>array("ComprobanteItemOrigen"=>array("Producto","Comprobante"=>array("PuntoVenta","Persona"
	  				)),"Producto"),'DetalleTipoComprobante'),*/
	  				//'contain'=>array("ComprobanteItemMany"=>array("ComprobanteItemMany"=>array("ComprobanteItemLote"))),
	  				'conditions' => $conditions,
					//'contain'=>array("DetalleTipoComprobante","Producto","ComprobanteItemMany"),
	  				//'fields' => array('ComprobanteItem.id_producto','d_detalle_tipo_comprobante','ComprobanteItem.cantidad','ComprobanteItem.cantidad_ceros_decimal','ComprobanteItem.cantidad_cierre','d_producto','ComprobanteItem.fecha_generacion','ComprobanteItem.fecha_cierre','ComprobanteItem.item_observacion',"ComprobanteItemMany"),
	  				'contain'=>array("Producto","Unidad","Comprobante"=>array("Movimiento")/*etapas*/),
	  				'limit' => $this->numrecords,
	  				'order' => 'ComprobanteItem.id asc'
	  		); 
	  		$comprobante_item = $this->ComprobanteItem->find('first', $this->paginate);
			
			/*INI - Agregado de campos de movimientos*/
			$mpc = $this->ArticuloRelacion->find('all', array(
					'conditions' => array('ArticuloRelacion.id_producto_padre' =>  $comprobante_item["Producto"]["id"]) ,
					'contain' =>false ));
			
			//Calcular la cantidad recibida en movimientos devolver campos cantidad_parcial cantidad_pendiente
			$cantidad_retirado = 0;
			$cantidad_terminado = 0;
			
			if( count( $comprobante_item["Comprobante"]["Movimiento"]) > 0 ){//si tiene movimientos
				foreach ($comprobante_item["Comprobante"]["Movimiento"] as $movimiento){
					if($movimiento["id_movimiento_tipo"] == EnumTipoMovimiento::RetirodeMateriaPrimaPorOrdenProduccion)
						$cantidad_retirado +=  $movimiento["cantidad"];
						if($movimiento["id_movimiento_tipo"] == EnumTipoMovimiento::IngresoDeComponenteParcialPorOrdenProduccion)
							$cantidad_terminado +=  $movimiento["cantidad"];
				}
			}
			
			$cant_insume = 0;
			if($mpc){ //por cada materiaPrima encontrada actualizo el stock y le resto lo que insume de materia prima ese componente
				foreach($mpc as $materia_prima_componente){
					$cant_insume += $materia_prima_componente["ArticuloRelacion"]["cantidad_hijo"];
				}
			}
			
			

			
			$comprobante_ot["Comprobante"]["c_retirado"] =   "X";
			$comprobante_ot["Comprobante"]["c_terminado"] = (string)  $cantidad_terminado;
			
			
			$comprobante_ot["Comprobante"]["mp_cantidad"] = (string) ($comprobante_item["ComprobanteItem"]["cantidad"]    *$cant_insume);
			$comprobante_ot["Comprobante"]["mp_retirado"] = (string) $cantidad_retirado;
			$comprobante_ot["Comprobante"]["mp_terminado"]= (string) ($cantidad_retirado*$cant_insume);
			$comprobante_ot["Comprobante"]["mp_cantidad"] = (string) ($comprobante_item["ComprobanteItem"]["cantidad"]    *$cant_insume);
			$comprobante_ot["Comprobante"]["mp_terminado"]= (string) ($comprobante_ot["Comprobante"]["c_terminado"] *$cant_insume);
			
			
			
		
			/*FIN - Agregado de campos de  movimientos*/
			
	  		$comprobante_item["ComprobanteItem"]["d_producto"] = $comprobante_item["Producto"]["d_producto"];
	  		$comprobante_item["ComprobanteItem"]["codigo_producto"] = $comprobante_item["Producto"]["codigo"];
	  		$comprobante_item["ComprobanteItem"]["producto_unidad"] = $comprobante_item["Unidad"]["d_unidad"];
	  		
	  		unset($comprobante_item["Producto"]);
	  		unset($comprobante_item["Unidad"]);
	  		
	  		$conditions = array();
	  		array_push($conditions, array('ComprobanteItem.id_comprobante' => $id_orden_trabajo));
	  		array_push($conditions, array('ComprobanteItem.id_detalle_tipo_comprobante' => EnumDetalleTipoComprobante::EtapaOrdenTrabajo));
	  		
	  		
	  		$comprobante_item_etapas = $this->ComprobanteItem->find('all', array(
	  				
	  				'conditions' => $conditions,
	  				
	  				'contain'=>array("Persona","Producto"),
	  				'order' => 'ComprobanteItem.id asc'
	  		));
	  		
	  		$comprobante_item["ComprobanteItem"]["Etapa"] = $comprobante_item_etapas;

	  		foreach($comprobante_item["ComprobanteItem"]["Etapa"] as &$etapa){
				
	  			$conditions = array();
	  			array_push($conditions, array('ComprobanteItem.id_comprobante_item_origen' => $etapa["ComprobanteItem"]["id"]));
	
	  			$comprobante_item_equipos = $this->ComprobanteItem->find('all', array(
	  					'conditions' => $conditions,
	  					'contain'=>array("Producto","Persona"),
	  					'order' => 'ComprobanteItem.id asc'
	  			));
	  			
	  			foreach($comprobante_item_equipos as &$detalle){
	  				
	  				if(isset($detalle["Persona"]["id"]) && $detalle["Persona"]["id"]>0){
	  					$detalle["ComprobanteItem"]["d_persona"] = $detalle["Persona"]["nombre"]." ".$detalle["Persona"]["apellido"];
	  					
	  				}else{
	  					$detalle["ComprobanteItem"]["d_persona"] = "";
	  				}
	  				
	  				unset($detalle["Persona"]);
	  				
	  				$detalle["ComprobanteItem"]["d_producto"] = $detalle["Producto"]["d_producto"];
	  				$detalle["ComprobanteItem"]["codigo_producto"] = $detalle["Producto"]["codigo"];
	  				unset($detalle["Producto"]);
	  				
	  				$conditions = array();
	  				array_push($conditions, array('ComprobanteItemLote.id_comprobante_item' => $detalle["ComprobanteItem"]["id"]));
	  				
	  				$comprobante_item_lotes = $this->ComprobanteItemLote->find('all', array(
	  						
	  						'conditions' => $conditions,
	  						'contain'=>array("Lote"=>array("TipoLote")),
	  						'order' => 'ComprobanteItemLote.id asc'
	  				));
	  				
	  				foreach ($comprobante_item_lotes as &$lote){	
						$lote["Lote"]["d_tipo_lote"] = $lote["Lote"]["TipoLote"]["d_tipo_lote"];
						// $lote["ComprobanteItemLote"]["d_tipo_lote"] = $lote["Lote"]["TipoLote"]["d_tipo_lote"];
						// $lote["ComprobanteItemLote"]["d_tipo_lote"] = $lote["Lote"]["TipoLote"]["d_tipo_lote"];
						$lote["ComprobanteItemLote"]["d_lote"] = 	  $lote["Lote"]["d_lote"];
						$lote["ComprobanteItemLote"]["codigo"] = 	  $lote["Lote"]["codigo"];
						$lote["ComprobanteItemLote"]["d_tipo_lote"] = $lote["Lote"]["TipoLote"]["d_tipo_lote"];
						//$lote["Lote"]["fecha_vencimiento"] = ""; //MP: buscar  dentro de los comprobantes de mantenimiento
					
						unset($lote["Lote"]["TipoLote"]);
					}
	  				$detalle["ComprobanteItem"]["Lotes"] = $comprobante_item_lotes;
	  				
	  				$conditions = array();
	  				array_push($conditions, array('ComprobanteItem.id_comprobante_item_origen' => $detalle["ComprobanteItem"]["id"]));
	  				array_push($conditions, array('ComprobanteItem.id_detalle_tipo_comprobante' => EnumDetalleTipoComprobante::InstrumentalDeTrabajo));
	  				
	  				$comprobante_instrumentos_medicion = $this->ComprobanteItem->find('all', array(
	  						
	  						'conditions' => $conditions,
	  						'contain'=>array("Producto","Persona"),
	  						'order' => 'ComprobanteItem.id asc'
	  				));
	  				
	  				foreach ($comprobante_instrumentos_medicion as &$equipo_medicion){
	  					
	  					$equipo_medicion["ComprobanteItem"]["d_fecha_vencimiento"] = "CORRECTO";//SE DEBE DETERMINAR QUE FECHA USAR. BUSCAR EN MANTENIMIENTO el calibre y su fecha de vencimiento 
						$equipo_medicion["ComprobanteItem"]["fecha_vencimiento"] = date('Y-m-d');//SE DEBE DETERMINAR QUE FECHA USAR. BUSCAR EN MANTENIMIENTO el calibre y su fecha de vencimiento 
	  					$equipo_medicion["ComprobanteItem"]["d_producto"] =		 $equipo_medicion["Producto"]["d_producto"];
	  					$equipo_medicion["ComprobanteItem"]["codigo_producto"] = $equipo_medicion["Producto"]["codigo"];
	  					
	  					unset($equipo_medicion["Producto"]);
						unset($equipo_medicion["Persona"]);
	  				}
	  				
	  				$detalle["ComprobanteItem"]["Instrumental"] = $comprobante_instrumentos_medicion;
	  				}
	  			
	  			$etapa["ComprobanteItem"]["Detalle"] = $comprobante_item_equipos;
	  			
	  			
	  			if(isset($etapa["Persona"]["id"]) && $etapa["Persona"]["id"]>0){
		  			$etapa["ComprobanteItem"]["d_persona"] = $etapa["Persona"]["nombre"]." ".$etapa["Persona"]["apellido"];
		  	
	  			}else{
	  				$etapa["ComprobanteItem"]["d_persona"] = "";
	  			}
	  			
	  			unset($etapa["Persona"]);

				
	  			if(isset($etapa["Producto"]["id"]) && $etapa["Producto"]["id"]>0){
	  				$etapa["ComprobanteItem"]["d_producto"] = $etapa["Producto"]["d_producto"];
	  				$etapa["ComprobanteItem"]["codigo_producto"] = $etapa["Producto"]["codigo"];
	  				
	  			}else{
	  				$etapa["ComprobanteItem"]["d_producto"] = "";
	  			}
	  			
	  			unset($etapa["Producto"]);
	  		}
	  		
	  		unset($comprobante_item["ComprobanteItemMany"]);
	  		unset($comprobante_item["Producto"]);

	  		$comprobante_ot["Comprobante"]["ComprobanteItem"] = $comprobante_item["ComprobanteItem"];
	  		$this->data=$comprobante_ot;
	  		
	  		if($this->llamada_interna == 0) 
	  		{
    	  		$output = array(
    	  				"status" =>EnumError::SUCCESS,
    	  				"message" => "",
    	  				"content" => $comprobante_ot
    	  		);
    	  		echo json_encode($output);
    	  		 die();
	  		}
	  }
	  
	  

	  
	 /**
	  * @secured(CONSULTA_ORDEN_TRABAJO)
	  */
	  public function excelExport($vista="default",$metodo="index",$titulo=""){
	  	
	  	parent::excelExport($vista,$metodo,"Listado de Ordenes de Trabajo");	
	  }
}
?>
