<?php
class ProveedorPicker extends EntityPicker{
    
    public $model = "Proveedor";
    
    public $pickerTitle = "Selección de Proveedor";
    
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "razon_social", "field" => "razon_social", "width" => "30%", "keyOrder" => "razon_social"),
                                  array("title" => "Cuit", "field" => "cuit", "width" => "60%", "keyOrder" => "cuit")
                                );

    public function getPickerConditions($busqueda){
        
        return array('Proveedor.razon_social LIKE' => '%' . $busqueda['bus_unico'] . '%');
    }

}