$.fn.dataTableExt.oApi.fnAddDataAndDisplay = function ( oSettings, aData )
{
    /* Add the data */
    alert(0);
    //var iAdded = this.oApi._fnAddData( oSettings, aData );
    var iAdded = this.oApi._fnAddData(  aData );
    alert(1);
    var nAdded = oSettings.aoData[ iAdded ].nTr;
    alert(2);
    /* Need to re-filter and re-sort the table to get positioning correct, not perfect
     * as this will actually redraw the table on screen, but the update should be so fast (and
     * possibly not alter what is already on display) that the user will not notice
     */
    this.oApi._fnReDraw( oSettings );
      
    /* Find it's position in the table */
    var iPos = -1;
    for( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
    {
        if( oSettings.aoData[ oSettings.aiDisplay[i] ].nTr == nAdded )
        {
            iPos = i;
            break;
        }
    }
      
    /* Get starting point, taking account of paging */
    if( iPos >= 0 )
    {
        oSettings._iDisplayStart = ( Math.floor(i / oSettings._iDisplayLength) ) * oSettings._iDisplayLength;
        this.oApi._fnCalculateEnd( oSettings );
    }
      
    this.oApi._fnDraw( oSettings );
    return {
        "nTr": nAdded,
        "iPos": iAdded
    };
};

$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
{
    return {
        "iStart":         oSettings._iDisplayStart,
        "iEnd":           oSettings.fnDisplayEnd(),
        "iLength":        oSettings._iDisplayLength,
        "iTotal":         oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage":          oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages":    oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
};

$.fn.dataTableExt.oApi.fnDisplayStart = function ( oSettings, iStart, bRedraw )
{
    if ( typeof bRedraw == 'undefined' )
    {
        bRedraw = true;
    }
      
    oSettings._iDisplayStart = iStart;
    oSettings.oApi._fnCalculateEnd( oSettings );
      
    if ( bRedraw )
    {
        oSettings.oApi._fnDraw( oSettings );
    }
};

$.fn.dataTableExt.oApi.fnGetSubformTableFreezeState = function ( oSettings)
{   

    freeze = new Array();
    
    freeze['iDisplayStart'] = oSettings._iDisplayStart;
    freeze['filter'] = $('input', this.fnSettings().aanFeatures.f).val();
    
    return freeze;
};

$.fn.dataTableExt.oApi.fnSetSubformTableFreezeState = function ( oSettings, freeze )
{   

    var anControl = $('input', this.fnSettings().aanFeatures.f);
    anControl.val(freeze['filter']);
    
    //Disparo la busqueda
    anControl.trigger('keyup');
    
    //Seteo la página en que estaba
    oSettings._iDisplayStart = freeze['iDisplayStart'];
    
    oSettings.oApi._fnCalculateEnd(oSettings);
    
    oSettings.oApi._fnDraw(oSettings);
    
};
