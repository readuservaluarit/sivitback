<?php

App::uses('NotasController', 'Controller');




class NotasDebitoVentaController extends NotasController

    {
    	
    	
    	public $name = EnumController::NotasDebitoVenta;
    	
    	
    	
    	
    	/**
    	 * @secured(CONSULTA_NOTA)
    	 */
    	public
    	
    	function getModel($vista = 'default')
    	{
    		
    		
    		$model = parent::getModelCamposDefault(); //esta en APPCONTROLLER
    		$model = $this->editforView($model, $vista); //esta funcion edita y agrega campos para la vista, debe estar LOCAL
    	}
    	
    	
    	private
    	function editforView($model, $vista)
    	{ //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    		$model = parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
    		
    		// $model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
    		$this->autoRender = false;
    		$this->layout = false;
    		$this->viewPath = "/Notas/";
    		$this->set('model', $model);
    		Configure::write('debug', 0);
    		$this->render($vista);
    	}
    	
    	
    	
    
    
    
     }
     
     
     

?>