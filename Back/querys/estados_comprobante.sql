/*
SQLyog Ultimate v9.63 
MySQL - 5.5.38-0+wheezy1 : Database - dbarcangel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbarcangel` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dbarcangel`;

/*Table structure for table `estados_comprobante` */

CREATE TABLE `estados_comprobante` (
  `Id_Estado_Comprobante` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Estado_Comprobante` varchar(30) NOT NULL DEFAULT '',
  `Deshabilitado` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id_Estado_Comprobante`),
  UNIQUE KEY `XAK1Estados_Comprobante` (`Nombre_Estado_Comprobante`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
