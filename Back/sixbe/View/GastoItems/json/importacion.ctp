<?php


            include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');
            $model["id_comprobante"]["show_grid"] = 1;
            $model["id_comprobante"]["header_display"] = "Id. Comprobante";
            $model["id_comprobante"]["order"] = "0";
            $model["id_comprobante"]["width"] = "8";
            
            
            $model["nro_comprobante"]["show_grid"] = 1;
            $model["nro_comprobante"]["header_display"] = "Nro. Comprobante";
            $model["nro_comprobante"]["order"] = "2";
            $model["nro_comprobante"]["width"] = "8";

            
            $model["d_estado_comprobante"]["show_grid"] = 1;
            $model["d_estado_comprobante"]["header_display"] = "Estado";
            $model["d_estado_comprobante"]["order"] = "2";
            $model["d_estado_comprobante"]["width"] = "10";
			$model["d_estado_comprobante"]["text_colour"] = "#004400";
            
            
            $model["codigo"]["show_grid"] = 1;
            $model["codigo"]["header_display"] = "[C&oacute;digo]";
            $model["codigo"]["order"] = "3";
            $model["codigo"]["width"] = "10";
                    
            $model["item_observacion"]["show_grid"] = 1;
            $model["item_observacion"]["header_display"] = "Item Observ.";
            $model["item_observacion"]["order"] = "4";            
            $model["item_observacion"]["width"] = "20";
            
            $model["cantidad"]["show_grid"] = 1;
            $model["cantidad"]["header_display"] = "Cantidad";
            $model["cantidad"]["order"] = "5";            
            $model["cantidad"]["width"] = "5"; 
            
		   $model["moneda_simbolo"]["show_grid"] = 1;
           $model["moneda_simbolo"]["header_display"] = "Moneda";
           $model["moneda_simbolo"]["order"] = "6";           
           $model["moneda_simbolo"]["width"] = "5";
		   $model["moneda_simbolo"]["text_colour"] = "#F0000F";
           
           
           $model["precio_unitario"]["show_grid"] = 1;
           $model["precio_unitario"]["header_display"] = "Precio Unitario";
           $model["precio_unitario"]["order"] = "7";           
           $model["precio_unitario"]["width"] = "5";
		   $model["precio_unitario"]["text_colour"] = "#F0000F";
           
           
           $model["descuento_unitario"]["show_grid"] = 1;
           $model["descuento_unitario"]["header_display"] = "Desc. Unitario";
           $model["descuento_unitario"]["order"] = "8";
           $model["descuento_unitario"]["width"] = "5"; 
            
            
            $model["total"]["show_grid"] = 1;
            $model["total"]["header_display"] = "Total";
            $model["total"]["order"] = "9";
            $model["total"]["width"] = "15";
			$model["total"]["text_colour"] = "#F0000F";

 $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItem",
                    "content" => $model
                );
        
          echo json_encode($output);
?>