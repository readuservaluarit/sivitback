SELECT 
  persona.`id_provincia`,
  provincia.`d_provincia`,
 SUM( IF(
    comprobante.`id_tipo_comprobante` IN(803,808),
    subtotal_neto*-1,
    subtotal_neto
  )) AS monto 
FROM
  comprobante 
  JOIN persona 
    ON persona.id = comprobante.`id_persona` 
  JOIN provincia 
    ON provincia.id = persona.`id_provincia` 
WHERE comprobante.`id_tipo_comprobante` IN (801,802,803, 806,807,808)
GROUP BY id_provincia
