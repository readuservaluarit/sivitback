<?php


class ChequerasController extends AppController {
    public $name = 'Chequeras';
    public $model = 'Chequera';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
  
    /**
    * @secured(CONSULTA_CHEQUERA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $id_cuenta_bancaria = $this->getFromRequestOrSession('Chequera.id_cuenta_bancaria');
        $numero_desde = $this->getFromRequestOrSession('Chequera.cheque_desde');
        $numero_hasta = $this->getFromRequestOrSession('Chequera.cheque_hasta');
        $activo = $this->getFromRequestOrSession('Chequera.activo');
        $cheques_disponibles = $this->getFromRequestOrSession('Chequera.cheques_disponibles');
        $id_estado_chequera = $this->getFromRequestOrSession('Chequera.id_estado_chequera');
        
       
       $conditions = array();        
        if($activo!="")
            array_push($conditions, array('Chequera.activo =' => $activo)); 
            
        if($id_cuenta_bancaria!="")
            array_push($conditions, array('Chequera.id_cuenta_bancaria =' =>  $id_cuenta_bancaria )); 
             
        if($numero_desde!="")
            array_push($conditions, array('Chequera.cheque_desde <=' =>  $numero_desde )); 
            
         if($numero_hasta!="")
            array_push($conditions, array('Chequera.cheque_hasta >=' =>  $numero_hasta ));    
            
         if($id_estado_chequera!="")
            array_push($conditions, array('Chequera.id_estado_chequera ' =>  $id_estado_chequera ));    
            
            $cheques_disponibles =1;
         
         if($cheques_disponibles== 1){
            	
         	array_push($conditions, array('Chequera.numero_actual < Chequera.cheque_hasta' ));    
            	
          }
            
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' => array('CuentaBancaria'=>array('BancoSucursal'=>array('Banco')),'EstadoChequera' ),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
        
        ////////////////////
        //Filters
        ///////////////////
        
          //CUE
        $formBuilder->addFilterInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $nombre));
        
        $formBuilder->addFilterInput('cuit', 'CUIT', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $cuit));
        
        
        $formBuilder->setDataListado($this, 'Listado de Chequeras', 'Datos de los Chequeras', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'Cliente.id', "10%");
        $formBuilder->addHeader('Razon Social', 'Cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'Cliente.cuit', "20%");
        $formBuilder->addHeader('Email', 'Cliente.email', "20%");
        $formBuilder->addHeader('Provincia', 'Provincia.d_provincia', "20%");
        $formBuilder->addHeader('Tel&eacute;fono', 'Cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField('Provincia', 'd_provincia');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
       
        foreach($data as $key=>&$valor){
             //$valor['Chequera']['numero_chequera'] = $valor['CuentaBancaria']['nro_cuenta'];
             
        	if(isset($valor['CuentaBancaria']['BancoSucursal']['Banco']["id"]) && $valor['CuentaBancaria']['BancoSucursal']['Banco']["id"]>0)
			 {
				$valor['Chequera']['id_banco'] =  $valor['CuentaBancaria']['BancoSucursal']['id_banco'];
				$valor['Chequera']['d_banco'] =   $valor['CuentaBancaria']['BancoSucursal']['Banco']['d_banco'];
			 }
			 
			 if(isset($valor['CuentaBancaria']['BancoSucursal']['id']) && $valor['CuentaBancaria']['BancoSucursal']['id']>0)
			 {
				$valor['Chequera']['id_banco_sucursal'] = $valor['CuentaBancaria']['BancoSucursal']['id'];
				$valor['Chequera']['d_banco_sucursal'] = $valor['CuentaBancaria']['BancoSucursal']['d_banco_sucursal'];
			 }
			 
			 if(isset($valor['Chequera']['numero_chequera']))
				$valor['Chequera']['d_chequera'] = $valor['Chequera']['numero_chequera'].' - '.$valor['Chequera']['d_banco_sucursal'].'- Dsd:'.$valor['Chequera']['cheque_desde'].'- Hta:'.$valor['Chequera']['cheque_hasta'];
				
			 if(isset($valor['CuentaBancaria']['nro_cuenta']))   
				$valor['Chequera']['nro_cuenta'] = $valor['CuentaBancaria']['nro_cuenta'];

			 if(isset($valor['EstadoChequera']['d_estado_chequera']))   
				$valor['Chequera']['d_estado_chequera'] = $valor['EstadoChequera']['d_estado_chequera'];
			 else
				$valor['Chequera']['d_estado_chequera'] = "";
        
             unset($valor['CuentaBancaria']);
             unset($valor['EstadoChequera']);
             
           
             
            
        }
        
        
       
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
        		"content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    /**
    * @secured(ABM_CHEQUERA)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        $tipoDocumento = '';
        $tipoIva = '';
        $Iva = '';
        $CondicionPago = '';
        
        if ($mode != "A"){
            $this->Cliente->id = $id;
            $this->Cliente->contain("Provincia","TipoDocumento","TipoIva");
            $this->request->data = $this->Cliente->read();
             if(isset($this->request->data['TipoDocumento']['id']))
                $tipoDocumento = $this->request->data['TipoDocumento']['id'];
             if(isset($this->request->data['TipoIva']['id']))
                $tipoIva = $this->request->data['TipoIva']['id'];
                
             if(isset($this->request->data['Iva']['id']))
                $Iva = $this->request->data['Iva']['id'];
            
              if(isset($this->request->data['CondicionPago']['id']))
                $CondicionPago = $this->request->data['CondicionPago']['id'];
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Cliente');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Chequeras');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Chequeras');
        else
            $formBuilder->setTituloForm('Consulta de Chequeras');
        
        $formBuilder->addFormTab('tab-general', 'Datos Generales');
        $formBuilder->addFormTab('tab-impositivo', 'Datos Impositivos'); 
        $formBuilder->addFormTab('tab-observaciones', 'Observaciones'); 
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        
        
        
        //TAB GENERAL
        $formBuilder->addFormBeginTab('tab-general');
        $formBuilder->addFormBeginFieldset('fs-general', 'General');
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('nombre_fantasia', 'Nombre Fantas&iacute;a', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('cuit', 'CUIT', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        
      /*  $TipoDocumentos = $this->Chequera->TipoDocumento->find('list', array('fields' => array('TipoDocumento.id', 'TipoDocumento.descripcion')));
        $formBuilder->addFormInput('id_tipo_documento', 'Tipo Documento', array('class'=>'control-group'), array('class' => 'required', 'label' => false, 'options' => $TipoDocumentos, 'empty' => true, 'default' => $tipoDocumento));
        */
          
        $formBuilder->addFormInput('email', 'E-Mail',                         array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('tel', 'Tel&eacute;fono',                 array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax',                            array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('calle', 'Calle', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  
        $formBuilder->addFormInput('ciudad', 'Ciudad', 						array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('numero', 'Numero',                         array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('codigo_postal', 'Codigo Postal', 		array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
		$formBuilder->addFormInput('localidad', 'Localidad',                 array('class'=>'control-group'), array('class' => 'required', 'label' => false));
		
        
        
        
         //Provincia
       
        $id_provincia='';
        $d_provincia='';
        if ($mode == "M") {
            $id_provincia=$this->request->data['Provincia']['id'];
            //$d_provincia=$this->request->data['Jurisdiccion']['nombre'];
            $prov = $this->Chequera->Provincia->find('first', array('conditions' => array('Provincia.id =' => $id_provincia)));
            $d_provincia = $prov['Provincia']['d_provincia'];
        }
        $selectionList = array($this->model."IdProvincia" => "id", $this->model."DProvincia" => "d_provincia");
        $formBuilder->addFormPicker('ProvinciaPicker', $selectionList, 'id_provincia', 
                                   array('value' => $id_provincia), 'd_provincia', 'Provincia', 
                                   array('class'=>'control-group'), array('class' => '', 'label' => false, 'value' => $d_provincia)
                                   );  

								/*`razon_social` varchar(200) NOT NULL,
								  `cuit` varchar(30) NOT NULL,
								  `calle` varchar(100) DEFAULT NULL,
								  `tel` varchar(100) DEFAULT NULL,
								  `ciudad` varchar(100) DEFAULT NULL,
								  `fax` varchar(30) DEFAULT NULL,
								  `descuento` float(8,2) DEFAULT NULL,
								  `codigo_postal` varchar(30) DEFAULT NULL,
								  `email` varchar(150) DEFAULT NULL,
								  `percepcion` varchar(20) DEFAULT NULL,
								  `numero` int(20) DEFAULT NULL,
								  `observaciones` varchar(200) DEFAULT NULL,
								  `porc_percepcion` float(8,2) DEFAULT NULL,
								  `condicion_pago` int(11) DEFAULT NULL,
								  `localidad` varchar(200) DEFAULT NULL,
								  `id_pais` int(11) DEFAULT NULL,
								  `id_provincia` int(11) DEFAULT NULL,
								  `activo` tinyint(4) DEFAULT '1' COMMENT 'no_visible',*/
        
        
        $formBuilder->addFormEndRow(); 
        $formBuilder->addFormEndTab();                          
       
                                
		//Fin Datos Generales								
         
          //TAB Impositivo
        $formBuilder->addFormBeginTab('tab-impositivo');
        $formBuilder->addFormBeginFieldset('fs-impositivo', 'Datos Impositivos');
        
        $formBuilder->addFormBeginRow();
       

        
        $TipoIvas = $this->Chequera->TipoIva->find('list', array('fields' => array('TipoIva.id', 'TipoIva.d_tipo_iva')));
        
        $formBuilder->addFormInput('id_tipo_iva', 'Condici&oacute;n Iva', array('class'=>'control-group'), array('class' => 'required', 'label' => false, 'options' => $TipoIvas, 'empty' => true, 'default' => $tipoIva));
        
        $Ivas = $this->Chequera->Iva->find('list', array('fields' => array('Iva.id', 'Iva.d_iva')));
        
        $formBuilder->addFormInput('id_iva', '% Iva', array('class'=>'control-group'), array('class' => 'required', 'label' => false, 'options' => $Ivas, 'empty' => true, 'default' => $Iva));
        
        $formBuilder->addFormInput('descuento', 'Descuento (%)',array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('porc_percepcion', 'Percepci&oacute;n (%)',     array('class'=>'control-group'), array('class' => 'required', 'label' => false));
       
       $FormasPago = $this->Chequera->CondicionPago->find('list', array('fields' => array('CondicionPago.id', 'CondicionPago.d_condicion_pago')));
        
        $formBuilder->addFormInput('id_condicion_pago', 'Condici&oacute;n de Venta', array('class'=>'control-group'), array('class' => 'required', 'label' => false, 'options' => $FormasPago, 'empty' => true, 'default' => $CondicionPago));
        
        
        $formBuilder->addFormEndRow();  
        $formBuilder->addFormEndTab();
        
        
          //TAB Impositivo
        $formBuilder->addFormBeginTab('tab-observaciones');
        $formBuilder->addFormBeginFieldset('fs-observaciones', 'Observaciones'); 
        $formBuilder->addFormBeginRow();
        $formBuilder->addFormInput('observaciones', '',         array('class'=>'control-group'), array('class' => 'required', 'label' => false,'type'=>'textarea'));
        $formBuilder->addFormEndRow();
        $formBuilder->addFormEndTab();
         
        
         $script = "
       
            
            
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'ChequeraAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('ChequeraNombreFantasia', 'Debe ingresar en Datos Generales un Nombre de Fantasia');
                validator.validateRequired('ChequeraRazonSocial', 'Debe ingresar en Datos Generales una Raz&oacute;n Social');
                validator.validateRequired('ChequeraCuit', 'Debe ingresar en Datos Generales un CUIT');
                validator.validateRequired('ChequeraEmail', 'Debe ingresar en Datos Generales un Email');
                validator.validateRequired('ChequeraIdTipoIva', 'Debe ingresar en Datos Impositivos la Condicion de Iva');
                validator.validateRequired('ChequeraIdIva', 'Debe ingresar en Datos Impositivos el Iva del Chequera');
                //validator.validateRequired('ChequeraDProvincia', 'Debe ingresar en Datos Generales una Provincia');
          
                
               // if(!ValidarCuit(jQuery('#ChequeraCuit')))
                 //  validator.addErrorMessage('ChequeraCuit','El Cuit que eligi&oacute; no es v&aacute;lido, verif&iacute;quelo.'); 
                
                validator.validateNumeric('ChequeraDescuento', 'El Descuento ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_cuit',
                        'data': 'cuit=' + $('#ChequeraCuit').val()+'&id='+$('#ChequeraId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('ChequeraCuit','El Cuit que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

    /**
    * @secured(ABM_CHEQUERA)
    */
    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_CHEQUERA, READONLY_PROTECTED)
    */
    public function add() {
    	
    	
        if ($this->request->is('post')){
            $this->loadModel($this->model);
             $this->request->data["Chequera"]["fecha"] = date("Y-m-d H:i:s"); 
             
             $this->request->data["Chequera"]["numero_actual"] = $this->request->data["Chequera"]["cheque_desde"];
             
              
             $id_add = '';
               
            try{
                if ($this->Chequera->saveAll($this->request->data, array('deep' => true))){
                  
                    $id_add = $this->Chequera->id;
                   
                    $mensaje = "La Chequera ha sido creada exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,la Chequera no ha podido ser creada.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" =>$id_add
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
	/**
    * @secured(MODIFICACION_CHEQUERA, READONLY_PROTECTED)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "La Chequera ha sido modificada exitosamente";
                     $status = EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
               }
               
               if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, la Chequera no ha podido modificarse.', 'error');
                
            }
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            $this->{$this->model}->contain('Provincia','Pais');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
  
    /**
    * @secured(BAJA_CHEQUERA, READONLY_PROTECTED)
    */
    function anular($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->Chequera->id = $id;
		$this->Chequera->saveField('cuit', "0");
       try{
            if ($this->Chequera->saveField('id_estado_chequera', EnumEstadoChequera::Inhabilitada)) {
                
                //$this->Session->setFlash('El Chequera ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "La Chequera ha sido eliminada exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }
    



   /**
    * @secured(CONSULTA_CHEQUERA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
        
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        
            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);
        
               
        
        
      
    } 
    
    
    
    
    public function home(){
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
    
    
    private function existe_chequera(){
    	
    	
    	$this->loadModel($this->model);
    	
    	$conditions = array();
    	array_push($conditions, array('Chequera.numero_chequera' =>  trim($this->request->data["Chequera"]["numero_chequera"])));
    	array_push($conditions, array('Cheque.id_cuenta_bancaria'=>$this->request->data["Cheque"]["id_chequera"]));
    	
    	
    	
    	
    }
    
   
    
    
       
}
?>