<?php


class DescuentoFamiliaPersonasController extends AppController {
    public $name = 'DescuentoFamiliaPersonas';
    public $model = 'DescuentoFamiliaPersona';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
     /**
    * @secured(CONSULTA_DESCUENTO_FAMILIA_PERSONA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
       
        //Recuperacion de Filtros
        $id_persona = $this->getFromRequestOrSession('DescuentoFamiliaPersona.id_persona');
        $razon_social = $this->getFromRequestOrSession('DescuentoFamiliaPersona.razon_social');
        $cuit = $this->getFromRequestOrSession('DescuentoFamiliaPersona.cuit');
        
        
        $conditions = array(); 

        if($id_persona!="")
            array_push($conditions, array('DescuentoFamiliaPersona.id_persona' =>  $id_persona )); 
            
        
        if($razon_social!="")
            array_push($conditions, array('Persona.razon_social LIKE' =>'%'.  $razon_social .'%' ));
            
            
        if($cuit!="")
            array_push($conditions, array('Persona.cuit LIKE' =>'%'.  $cuit .'%' ));                  
        
        
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' => array("Persona","FamiliaProducto"),
            'conditions' => $conditions,
            'limit' => 10,
            'page' => $this->getPageNum()
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'DescuentoFamiliaPersona.id', "50%");
        $formBuilder->addHeader('id_PERSONA', 'DescuentoFamiliaPersona.id_PERSONA', "20%");
        $formBuilder->addHeader('descuento', 'DescuentoFamiliaPersona.descuento', "30%");
        // $formBuilder->addHeader('Email', 'cliente.email', "30%");
        // $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'descuento');
		$formBuilder->addField($this->model, 'id_PERSONA');
        // $formBuilder->addField($this->model, 'cuit');
        // $formBuilder->addField($this->model, 'email');
        // $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$dato){
            
            $dato["DescuentoFamiliaPersona"]["razon_social"] =  $dato["Persona"]["razon_social"];
            $dato["DescuentoFamiliaPersona"]["d_familia_producto"] =  $dato["FamiliaProducto"]["d_familia"];
            
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    /**
    * @secured(ABM_USUARIOS)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->CotizacionItem->id = $id;
            $this->CotizacionItem->contain();
            $this->request->data = $this->CotizacionItem->read();
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('CotizacionItem');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Cotizaciones');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Cotizaciones');
        else
            $formBuilder->setTituloForm('Consulta de Cotizaciones');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('cuit', 'Cuit', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('calle', 'Calle', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  
        $formBuilder->addFormInput('tel', 'Tel&eacute;fono', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('ciudad', 'Ciudad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descuento', 'Descuento', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descripcion', 'Descripcion', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('localidad', 'Localidad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
         
        
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'ClienteAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('ClienteRazonSocial', 'Debe ingresar una Raz&oacute;n Social');
                validator.validateRequired('ClienteCuit', 'Debe ingresar un CUIT');
                validator.validateNumeric('ClienteDescuento', 'El Descuento ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_cuit',
                        'data': 'cuit=' + $('#ClienteCuit').val()+'&id='+$('#ClienteId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('ClienteCuit','El Cuit que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

    /**
    * @secured(ABM_USUARIOS)
    */
    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(CONSULTA_DESCUENTO_FAMILIA_PERSONA)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_nuevo = '';
            
            try{
                if ($this->DescuentoFamiliaPersona->saveAll($this->request->data)){
                    $mensaje = "El Descuento ha sido creado exitosamente";
                    $tipo = EnumError::SUCCESS;
                    $id_nuevo = $this->DescuentoFamiliaPersona->id;
                   
                }else{
                    $mensaje = "Ha ocurrido un error, el Descuento no ha podido ser creado.";
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error, el Descuento no ha podido ser creado.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" =>$id_nuevo
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->DescuentoFamiliaPersona->id = $id;
            
            try{ 
                if ($this->DescuentoFamiliaPersona->saveAll($this->request->data)){
                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "El Descuento ha sido modificada exitosamente",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('El Descuento ha sido modificada exitosamente.', 'success');
                        $this->redirect(array('controller' => 'OrdenCompra', 'action' => 'index'));
                        
                    } 
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, el Descuento no ha podido modificarse.', 'error');
                
            }
           
        }else{
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->DescuentoFamiliaPersona->id = $id;
            $this->DescuentoFamiliaPersona->contain();
            $this->request->data = $this->DescuentoFamiliaPersona->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
            }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
  
    /**
    * @secured(ABM_USUARIOS, READONLY_PROTECTED)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->DescuentoFamiliaPersona->id = $id;
       try{
            if ($this->DescuentoFamiliaPersona->delete()) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Descuento ha sido eliminado exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }
    
    
    public function home(){
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
    
     /**
    * @secured(CONSULTA_DESCUENTO_FAMILIA_PERSONA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
        
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        
            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);
        
               
        
        
      
    }
    
  
    
   
    
}
?>