<?php
		include(APP.'View'.DS.'ComprobanteValores'.DS.'json'.DS.'comprobante_valor.ctp');
   
        
        $model["d_comprobante_valor"]["show_grid"] = 1;
        $model["d_comprobante_valor"]["header_display"] = "Moneda";
        $model["d_comprobante_valor"]["order"] = "1";
        $model["d_comprobante_valor"]["width"] = "20";
		$model["d_comprobante_valor"]["read_only"] = "1";
		
		/* TODO: CAMBIAR d_comprobante_valor  POR d_moneda QUE ES REDUNDNATE
		$model["id_moneda"]["read_only"] = "1";
		$model["d_moneda"]["read_only"] = "1";*/
		
        $model["monto"]["show_grid"] = 1;
        $model["monto"]["header_display"] = "Monto";
        $model["monto"]["order"] = "2";
        $model["monto"]["width"] = "20";
		$model["monto"]["read_only"] = "0";		
		$model["monto"]["text_colour"] = "#006600";
        
        $model["valor_moneda"]["show_grid"] = 1;
        $model["valor_moneda"]["header_display"] = "Cotiz.";
        $model["valor_moneda"]["order"] = "3";
        $model["valor_moneda"]["width"] = "20"; 
		$model["valor_moneda"]["read_only"] = "1";		
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteValor",
                    "content" => $model
                );
        echo json_encode($output);
?>