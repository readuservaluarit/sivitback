<?php

class EnumProductoTipo
{
    const Producto = 1;
    const Componente = 2;
    const MateriaPrima = 3;
    const Servicio = 4;
    const BIENUSO = 5;
    const MRETAIL = 6;
    const INSUMOINTERNO = 7;
    //const MNV = 8;  //Reemplazados por Insumos
    const MPE = 9;
    const GAST = 10;
    const SCRAP = 11;
    const OPERACIONINTERNA = 12;
}
?>