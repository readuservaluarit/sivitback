<?php

class TipoIvaPicker extends EntityPicker{
    
    public $model = "TipoIva";
    
    public $pickerTitle = "Selección de Condici&oacute;n Iva";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Condici&oacute;n", "field" => "d_tipo_iva", "width" => "80%", "keyOrder" => "d_tipo_iva"),
                                 
                                );

    public function getPickerConditions($busqueda){
        
        $pickerConditions = array('TipoIva.d_tipo_iva LIKE' => '%' . $busqueda['bus_unico'] . '%');
        
        return $pickerConditions;
        
        
    }
    
    public function getPickerInitialFilter(){
        
        //Filtra siempre por la Jurisdiccion del usuario si corresponde
        return DataFilter::getJurisdiccionPickerFilter($this->model);
        
    }

}