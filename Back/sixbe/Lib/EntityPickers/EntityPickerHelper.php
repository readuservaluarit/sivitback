<?php
class EntityPickerHelper{
    
    private $id;
    private $selectionFields;
    private $pickerConfiguration;
    private $pickerConfigurationClass;
    private $view;
    private $callbacksJS;
    private $filterQueryFields;
    
    public $helpers = array ('Html');
      
    function EntityPickerHelper($id){
        $this->selectionFields = array();
        $this->callbacksJS = array();
        $this->filterQueryFields = array();
        $this->id = $id;
    }
    
    public function addSelectionField($elementId, $fieldName){
        array_push($this->selectionFields, array('elementId' => $elementId, 'fieldIndex' => $this->getFieldIndex($fieldName)));       
    }
    
    public function addSelectionList($selectionList = array()){
        
        foreach($selectionList as $elementId => $fieldName){
            
            $this->addSelectionField($elementId, $fieldName);
            
        }
        
    }
    
    public function setCallbacksJS($callbacksJS){
        
         $this->callbacksJS = $callbacksJS;
        
    }
    
    public function setfilterQueryFields($filterQueryFields){
        
         $this->filterQueryFields = $filterQueryFields;
        
    }
    
    
    public function getSelectionFields(){
        
        return $this->selectionFields;
        
    }
    
    public function setPickerConfiguration($pickerConfiguration){
        
        $this->pickerConfiguration = $pickerConfiguration;
        
    }
    
    public function setPickerConfigurationClass($pickerConfigurationClass){
        
        $this->pickerConfigurationClass = $pickerConfigurationClass;
        App::import('Lib/EntityPickers', $this->pickerConfigurationClass);
        $this->setPickerConfiguration(new $this->pickerConfigurationClass);
        
    }  
    
    public function setView($view){
        
        $this->view = $view;
        
    }
    
    public function getFieldIndex($fieldName){
        
        $i=0;
        foreach($this->pickerConfiguration->pickerFields as $field){
            
           
           if (trim(strtoupper($field["field"])) == trim(strtoupper($fieldName)))
                return $i; 
           
           $i++;
        }
        
        return 0;
         
    }
    
    public function getJSShowPicker($suffix){
           
           $fnc = "function showPicker_" . $suffix . "(){";
           
           $url = Router::url(array("controller" => "EntityPicker", "action" => "index", $this->pickerConfigurationClass, 
           		urlencode(json_encode($this->id)), urlencode(json_encode($this->selectionFields)), urlencode(json_encode($this->callbacksJS)) ));
           
           $fnc .= " var url = '{$url}/' + getPickerQueryFields_{$suffix}();"; 
           
           //$fnc .= " alert(url);"; 
           
           $fnc .= "$.fancybox.open({
                    href : url,
                    type : 'ajax',
                    padding : 5,
                    beforeClose : function() {pickerClose(); }
                });";
           
           $fnc .= "}";
           
           return $fnc;
        
    }
    
    public function makeEntityPicker($controller, $label, $id, $hiddenId, $inputOptions, $hiddenOptions, $fieldOptions){
        
        $suffix = rand();
        
        $lnkId = 'lnk_'.$suffix;
                         
        $inputOptions['label'] = false;   
        $inputOptions['div'] = false;    
        $inputOptions['readonly'] = 'readonly';
        
        if(isset($inputOptions['class']))
            $inputOptions['class'] = $inputOptions['class'] . " pickerInput";
        else
            $inputOptions['class'] = " pickerInput";
                    
        $hiddenOptions['type'] = 'hidden';
        
        echo $controller->Form->input($hiddenId, $hiddenOptions);
        
        if($fieldOptions['class'] == "_25"){
           $textWidth = '80%';
           $imgWidth = '10%'; 
        }    
        else{
            $textWidth = '80%';
            $imgWidth = '15%';
        }
            
        
        if(isset($inputOptions['style']))
            $inputOptions['style'] .= 'cursor:pointer';
        else
            $inputOptions['style'] = 'cursor:pointer';
        
        
        $input = $controller->Form->input($id, $inputOptions);    
        
        
        echo "<div class='".$fieldOptions['class']."'>
                    <label class='col-sm-4 col-form-label' for='textfield'>".$label."</label>
                        <div class='col-sm-8'>
						
                            <div class='input-append' style='cursor: pointer;'>
							<div class='row'>
							<div class='col-sm-10'>"
							
							.
                            
                                $input.
                                "</div><div class='col-sm-2'><span id='".$lnkId."' class='add-on'><i class='fa fa-search'></i></span></div>

</div>
                            </div>
						
                        </div>";
                
        echo "</div>";
        
        
        
       
       
        $customScripts = "$('#".$lnkId."').click(" . $this->getJSShowPicker($suffix) ." );";
        $customScripts .= "$('#".$lnkId."').prev().click(function(){" . "$('#".$lnkId."').trigger('click');" ." });";
        
        echo "<script type='text/javascript'>$customScripts</script>";
        
        echo "<script type='text/javascript'>";
        
        echo " function getPickerQueryFields_{$suffix}(){  ";
                
                
                    if(isset($this->filterQueryFields) && $this->filterQueryFields!=null && sizeof($this->filterQueryFields)>0){
                
                        echo "var filterQuery = {};";
                    
                       //Preparo los datos de Pickers anidados
                       foreach($this->filterQueryFields as $clave => $valor){
                        
                            echo "filterQuery." . $clave . " = $('#" . $valor ."').val();" ;    
                           
                       }
                    
                       echo "return encodeURIComponent(JSON.stringify(filterQuery));";
                       
                    }
        
        
           echo  "}";
        
            
        
        echo "</script>";
        
          
         
        
    }

}