/*Migracion de clientes de arcangel a -> valuarit*/

INSERT INTO valuarit_enerbom_test.`persona2` (id,razon_social,id_tipo_documento,id_tipo_persona,activo,id_forma_pago,id_tipo_iva,id_provincia,id_pais, calle,numero_calle,piso,dpto,cuit,fecha_creacion)


SELECT 
  personas.Id_Persona,
  Razon_Social,
  3 AS id_tipo_documento,
 1 AS id_tipo_persona,
  1 AS activo,


  
CASE personas.`Id_Condicion_Comercial` 
  
  WHEN 0 THEN 29
  
  WHEN 1 THEN 4
  WHEN 3 THEN 32
  WHEN 8 THEN 33
  WHEN 10 THEN 20
  WHEN 11 THEN 2
  WHEN 12 THEN 31
  WHEN 13 THEN 3
  WHEN 16 THEN 30
  WHEN 18 THEN 11
  WHEN 20 THEN 8
  WHEN 21 THEN 4
  
  END
  
  AS id_forma_pago
  
  
  ,
  
  CASE personas.`Id_Condicion_Iva` 
  
  WHEN 1 THEN 9
  
  WHEN 2 THEN 9
  WHEN 5 THEN 1
  WHEN 6 THEN 13
  WHEN 10 THEN 9
  
  END
  
  AS id_tipo_iva,
  
  
  CASE domicilios_persona.`Id_Provincia`
  
  WHEN 0 THEN 27
  WHEN 24 THEN 1
  ELSE domicilios_persona.`Id_Provincia`+1
  
  END
  
  AS id_provincia
  ,
  
  
  
  
  
  
  
  9 AS id_pais,
  Nombre_Calle,
  Numero_Calle,
  Piso,
  Depto,
   personas.`Numero_Documento`,
  personas.`Fecha_Alta` 
FROM
  personas 
  LEFT JOIN domicilios_persona 
    ON domicilios_persona.`Id_Persona` = personas.`Id_Persona` 
  LEFT JOIN tipos_documento 
    ON tipos_documento.`Id_Tipo_Documento` = personas.`Id_Tipo_Documento` 
  LEFT JOIN persona_documento 
    ON persona_documento.`Id_Persona` = personas.`Id_Persona` 
  LEFT JOIN telefonos_persona 
    ON telefonos_persona.`Id_Persona` = personas.`Id_Persona` 
  LEFT JOIN paises 
    ON paises.`Id_Pais` = domicilios_persona.`Id_Pais` 
WHERE id_sistema = 2 
  AND personas.Numero_Documento > 1 
  AND domicilios_persona.Id_Provincia > 0 
  AND domicilios_persona.Id_Pais > 0 
  AND domicilios_persona.Id_Provincia < 25 
  /*solo nacionales importo*/
  AND personas.`Id_Estado` = 1 
  
  /*NO ELIJO LOS REPETIDOS*/

AND personas.`Numero_Documento` NOT IN (


SELECT 
  personas.`Numero_Documento`
 
FROM
  personas 
  LEFT JOIN domicilios_persona 
    ON domicilios_persona.`Id_Persona` = personas.`Id_Persona` 
  LEFT JOIN tipos_documento 
    ON tipos_documento.`Id_Tipo_Documento` = personas.`Id_Tipo_Documento` 
  LEFT JOIN persona_documento 
    ON persona_documento.`Id_Persona` = personas.`Id_Persona` 
  LEFT JOIN telefonos_persona 
    ON telefonos_persona.`Id_Persona` = personas.`Id_Persona` 
  LEFT JOIN paises 
    ON paises.`Id_Pais` = domicilios_persona.`Id_Pais` 
WHERE id_sistema = 2 
  AND personas.Numero_Documento > 1 
  AND domicilios_persona.Id_Provincia > 0 
  AND domicilios_persona.Id_Pais > 0 
  AND domicilios_persona.Id_Provincia < 25 
  /*solo nacionales importo*/
  AND personas.`Id_Estado` = 1 
  
  GROUP BY personas.`Numero_Documento`
  
  HAVING COUNT(*)>1
  
  )