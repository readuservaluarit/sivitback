<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class Reporte extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'reporte';
    public $actsAs = array('Containable');


    
    
     public $belongsTo = array(
        'MimeType' => array(
            'className' => 'MimeType',
            'foreignKey' => 'id_mime_type',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'SubTipoImpuesto' => array(
            'className' => 'SubTipoImpuesto',
            'foreignKey' => 'id_sub_tipo_impuesto',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'TipoImpuesto' => array(
            'className' => 'TipoImpuesto',
            'foreignKey' => 'id_tipo_impuesto',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
        );
    
}
