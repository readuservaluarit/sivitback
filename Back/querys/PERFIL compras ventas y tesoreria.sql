DELETE  FROM funcion_rol WHERE id_rol = 19;


INSERT INTO funcion_rol (id_funcion, id_rol) 
SELECT 
  id_funcion,
  19 
FROM
  funcion_rol 
WHERE id_rol = 1;


  DELETE 
  FROM
    funcion_rol 
  WHERE id_rol = 19 
    AND id_funcion IN 
    (SELECT 
      id 
    FROM
      funcion 
    WHERE c_funcion LIKE '%operativa%' 
      OR c_funcion LIKE '%calidad%' 
      OR c_funcion LIKE '%conciliacion%' 
      OR c_funcion LIKE '%RAIZ_USUARIO%' 
      OR c_funcion LIKE '%MENU_USUARIO%' 
      OR c_funcion LIKE '%REPORTE_PEDIDO_INTERNO_AGRUPADO_PRODUCTO%' 
      OR c_funcion LIKE '%MENU_REMITO%' 
      OR c_funcion LIKE '%MENU_COTIZACION%' 
      OR c_funcion LIKE '%MENU_PI_CALENDARIO_VTO%' 
      OR c_funcion LIKE '%MENU_SISTEMA%' 
      OR c_funcion LIKE '%MENU_PACKAGING%'
      OR c_funcion LIKE '%MENU_MATERIA_PRIMA%'
      OR c_funcion LIKE '%MENU_COMPONENTE%'
      OR c_funcion LIKE '%MENU_BIEN_USO%'
      OR c_funcion LIKE '%MENU_INSUMO_INTERNO%'
      OR c_funcion LIKE '%MENU_PRODUCTO'
      OR c_funcion LIKE '%MENU_PERIODO_IMPOSITIVO'
      OR c_funcion LIKE '%MENU_TIPO_IVA'
      OR c_funcion LIKE '%MENU_IVA'
      OR c_funcion LIKE '%MENU_ESTADO'
      OR c_funcion LIKE '%MENU_TIPO_COMPROBANTE'
      OR c_funcion LIKE '%MENU_CONTABILIDAD%'
      OR c_funcion LIKE '%MENU_ADMINISTRACION%'
      OR c_funcion LIKE '%ADD_DATOSEMPRESA%'
      OR c_funcion LIKE '%MENU_INFORME_RECEPCION_MATERIAL%'
      );