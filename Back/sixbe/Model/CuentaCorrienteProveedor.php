<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class CuentaCorrienteProveedor extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'cuenta_corriente_proveedor';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
		
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 * 
 */
 
 public $belongsTo = array(
        'Proveedor' => array(
            'className' => 'Proveedor',
            'foreignKey' => 'id_proveedor',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'Estado' => array(
            'className' => 'Estado',
            'foreignKey' => 'id_estado',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Moneda' => array(
            'className' => 'Moneda',
            'foreignKey' => 'id_moneda',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'FacturaProveedor' => array(
            'className' => 'FacturaProveedor',
            'foreignKey' => 'id_factura_proveedor',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
        
       
 );
 


	

}
