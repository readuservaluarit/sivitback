<?php

class JurisdiccionPicker extends EntityPicker{
    
    public $model = "Jurisdiccion";
    
    public $pickerTitle = "Selección de Jurisdicción";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Nombre", "field" => "nombre", "width" => "80%", "keyOrder" => "nombre"),
                                  array("title" => "id_provincia", "field" => "id_provincia", "width" => "80%", "keyOrder" => "id_provincia","visible"=>false)
                                );

    public function getPickerConditions($busqueda){
        
        $pickerConditions = array('Jurisdiccion.nombre LIKE' => '%' . $busqueda['bus_unico'] . '%');
        
        return $pickerConditions;
        
        
    }
    
    public function getPickerInitialFilter(){
        
        //Filtra siempre por la Jurisdiccion del usuario si corresponde
        return DataFilter::getJurisdiccionPickerFilter($this->model);
        
    }

}