<?php
      
        include(APP.'View'.DS.'AsientosCuentaContable'.DS.'json'.DS.'asiento_cuenta_contable.ctp');
        
        
        $model["codigo_asiento"]["show_grid"] = 1;//VISIBLE
        $model["codigo_asiento"]["header_display"] = "Asiento";
        $model["codigo_asiento"]["width"] = "90"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["codigo_asiento"]["minimun_width"] = "60"; //Restriccion mas de esto no se puede achicar
        $model["codigo_asiento"]["order"] = "0";
        $model["codigo_asiento"]["text_colour"] = "#660044";
        
        
		$model["fecha_asiento"]["show_grid"] = 1;//VISIBLE
        $model["fecha_asiento"]["header_display"] = "Fecha";
        $model["fecha_asiento"]["width"] = "90"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["fecha_asiento"]["minimun_width"] = "60"; //Restriccion mas de esto no se puede achicar
        $model["fecha_asiento"]["order"] = "1";
        $model["fecha_asiento"]["text_colour"] = "#660044";
        
        
        

        $model["tipo_comprobante"]["show_grid"] = 1;//VISIBLE
        $model["tipo_comprobante"]["header_display"] = "Tipo";
        $model["tipo_comprobante"]["width"] = "90"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["tipo_comprobante"]["minimun_width"] = "60"; //Restriccion mas de esto no se puede achicar
        $model["tipo_comprobante"]["order"] = "2";
        $model["tipo_comprobante"]["text_colour"] = "#660044";
        
        
        
		$model["pto_nro_comprobante"]["show_grid"] = 1;//VISIBLE
        $model["pto_nro_comprobante"]["header_display"] = "Nro.Comprobante";
		$model["pto_nro_comprobante"]["width"] = "90"; //solo al tener esto ya se convierete en una grilla q toma todo el espacio posible
        $model["pto_nro_comprobante"]["minimun_width"] = "60"; //Restriccion mas de esto no se puede achicar
        $model["pto_nro_comprobante"]["order"] = "3";
        $model["pto_nro_comprobante"]["text_colour"] = "#660044";
		
   
		
		
		
		$model["debe_monto"]["show_grid"] =1;
        $model["debe_monto"]["header_display"] = "Debe";
        $model["debe_monto"]["minimun_width"] = "80"; //REstriccion mas de esto no se puede achicar
        $model["debe_monto"]["order"] = "4";
        $model["debe_monto"]["text_colour"] = "#000000";
        
        
        
        $model["haber_monto"]["show_grid"] =1;
        $model["haber_monto"]["header_display"] = "Haber";
        $model["haber_monto"]["minimun_width"] = "80"; //REstriccion mas de esto no se puede achicar
        $model["haber_monto"]["order"] = "5";
        $model["haber_monto"]["text_colour"] = "#000000";
        
        
        $model["conciliado"]["show_grid"] =1;
        $model["conciliado"]["header_display"] = "Conciliado";
        $model["conciliado"]["minimun_width"] = "80"; //REstriccion mas de esto no se puede achicar
        $model["conciliado"]["order"] = "6";
        $model["conciliado"]["text_colour"] = "#000000";
        
        
        $model["fecha_conciliacion"]["show_grid"] =1;//INVISIBLE
        $model["fecha_conciliacion"]["header_display"] = "Fecha Conciliado";
        $model["fecha_conciliacion"]["minimun_width"] = "80"; //REstriccion mas de esto no se puede achicar
        $model["fecha_conciliacion"]["order"] = "7";
        $model["fecha_conciliacion"]["text_colour"] = "#000000";
        
        
        $model["observacion"]["show_grid"] =1;//INVISIBLE
        $model["observacion"]["header_display"] = "Obs.";
        $model["observacion"]["minimun_width"] = "80"; //REstriccion mas de esto no se puede achicar
        $model["observacion"]["order"] = "8";
        $model["observacion"]["text_colour"] = "#000000";
		
	

        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "AsientoCuentaContable",
                    "content" => $model
                );
        echo json_encode($output);
?>