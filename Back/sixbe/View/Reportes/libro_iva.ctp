<?php   


class MyTCPDF extends TCPDF{

    public $datos_header;
    public $datos_footer;
    public $bar_code;
    public $observaciones;
    
    public function set_datos_header($datos_empresa){
        $this->datos_header = $datos_empresa; 

    }
    public function set_datos_footer($datos_empresa_footer){
        $this->datos_footer = $datos_empresa_footer; 

    }
     public function set_barcode($bar_code){
        $this->bar_code = $bar_code; 

    }
    
    public function set_observaciones($observaciones){
        $this->observaciones = $observaciones; 

    }
    
  public function Header(){
     
        
        
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_header, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
     
     
  }
  
  public function Footer(){
     
        
        
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->datos_footer, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
     $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->observaciones, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
     
     $style['text'] = true;
     $style['border'] = true;
     $style['align'] = 'C';
     $this->Cell(0, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
      
  }
  
}
//define('PDF_PAGE_FORMAT', 'LEGAL');       // 355.6

$custom_layout = array(215.9, 355.6);

$pdf = new MyTCPDF('L', PDF_UNIT, $custom_layout, true, 'UTF-8', false);

$html = '';
//$pdf->set_datos_header($html);




 


$footer="";


   
  
   
   $html .= '   
  ';
  
  $pdf->set_datos_footer($footer);
  
  
ob_clean();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sivit by Valuarit');
$pdf->SetTitle('Factura');
$pdf->SetSubject('Factura');
//$pdf->SetKeywords('');



// set default header data
$logo = 'img/header_factura.jpg';
$title = "";
$subtitle = ""; 
//$pdf->SetHeaderData($logo, 190, $title, $subtitle, array(0,0,0), array(0,104,128));
$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 10, 10);
//$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(20);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
/*$lg = Array();
$lg = Array();
$lg['a_meta_charset'] = 'ISO-8859-1';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';
*/
$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set default font subsetting mode
//$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
/*
$html = <<<EOD
<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
EOD;
*/

$tbody = "";

$renglones = $datos_pdf;

$total_registros = count($renglones);



$data = array();

$corte_control = 33;


$cantidad_paginas =    round($total_registros/$corte_control, 0, PHP_ROUND_HALF_UP);


$total_subtotal_neto = 0;
$total_no_gravado = 0;
$total_exento = 0;
$total_comprobante = 0;
$total_iva_21 = 0;
$total_iva_105 = 0;
$total_ib = 0;
$i=1;
$z=1;
$cant_pag=1;
$titulo = $datos_extra["titulo"].' '.$datos_extra["subtitulo1"];
$subtitulo1 = $datos_extra["subtitulo2"];
        $tbody .='
        
        <table width="100%" border="0">
  <tr>
    <td>'.$titulo.'</td>
   
  </tr>
  <tr>
    <td>'.$subtitulo1.'</td>
   
  </tr>
</table>
        
        
        <table width="100%"  cellspacing="1" cellpadding="2" style="font-family:\'Courier New\', Courier, monospace">
        
        
        ';
        
   
       
       
   
       
       

       
$tbody .='<tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:20px;">
    
        <th class="sorting"  rowspan="1" colspan="1" style="width:7%;"><strong>Fecha</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:3%;"><strong>Tipo</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:12%;"><strong>Nro</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:23%;"><strong>R. Social</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>cuit</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>Gravado</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>No Gravado</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:3%;"><strong>Exento</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:8%;"><strong>I.V.A 21%</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>I.V.A 10,5%</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>IIBB</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>Total</strong></th>
        
    </tr>
';

foreach($renglones as $renglon){

  
  
   
   
   
   if($i-1 == $corte_control ) {
   
   
   
       $tbody .= "
         
       
       
       
       <tr style=\"font-size:25px;font-family:'Courier New', Courier, monospace\">";
      
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";       
       $tbody .= "<td>Transporte</td>";
       $tbody .= "<td>" .$total_subtotal_neto. "</td>"; 
       $tbody .= "<td>" .$total_no_gravado. "</td>";
       $tbody .= "<td>" .$total_exento. "</td>";
       $tbody .= "<td>" .$total_iva_21. "</td>";
       $tbody .= "<td>" .$total_iva_105."</td>";
       $tbody .= "<td>" .$total_ib."</td>";
       $tbody .= "<td colspan='2'>" .$total_comprobante."</td>";
       $tbody .= "</tr>";
       $tbody .= "<tr style=\"font-size:25px;font-family:'Courier New', Courier, monospace\">";
      
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";       
       $tbody .= "<td></td>";
       $tbody .= "<td></td>"; 
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td colspan='2'></td>";
       $tbody .= "</tr>";
       
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";       
       $tbody .= "<td></td>";
       $tbody .= "<td></td>"; 
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td colspan='2'></td>";
       $tbody .= "</tr></table>";
       
       $tbody .= '<table width="100%" border="0">
                          <tr>
                            <td>'.$titulo.'</td>
                           
                          </tr>
                          <tr>
                            <td>'.$subtitulo1.'</td>
                           
                          </tr>
                        </table>';       
              
              
       $tbody .= '<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:30px;font-family:\'Courier New\', Courier, monospace">';
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";       
       $tbody .= "<td>Transporte</td>";
       $tbody .= "<td>" .$total_subtotal_neto. "</td>"; 
       $tbody .= "<td>" .$total_no_gravado. "</td>";
       $tbody .= "<td>" .$total_exento. "</td>";
       $tbody .= "<td>" .$total_iva_21. "</td>";
       $tbody .= "<td>" .$total_iva_105."</td>";
       $tbody .= "<td>" .$total_ib."</td>";
       $tbody .= "<td colspan='2'>" .$total_comprobante."</td>";
       $tbody .= "</tr>";
       $tbody .= "</table>";
       $i = 1;
       ++$cant_pag;
     //$pdf->AddPage();
      //$pdf->setPage($cant_pag);
      $tbody .='
       <table width="100%"  cellspacing="1" cellpadding="2" style="font-family:\'Courier New\', Courier, monospace">
      <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:20px;">
    
        <th class="sorting"  rowspan="1" colspan="1" style="width:7%;"><strong>Fecha</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:3%;"><strong>Tipo</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:12%;"><strong>Nro</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:23%;"><strong>R. Social</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>cuit</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>Gravado</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>No Gravado</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:3%;"><strong>Exento</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:8%;"><strong>I.V.A 21%</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>I.V.A 10,5%</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>IIBB</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:10%;"><strong>Total</strong></th>
        
    </tr>
    
';


       $total_subtotal_neto = $total_subtotal_neto + $renglon['Comprobante']['subtotal_neto']; 
       $total_no_gravado +=$renglon['Comprobante']['no_gravado']; 
       $total_exento +=$renglon['Comprobante']['exento']; 
       $total_iva_21 +=$renglon['Comprobante']['iva_21']; 
       $total_iva_105 +=$renglon['Comprobante']['iva_105']; 
       $total_ib +=$renglon['Comprobante']['total_ib']; 
       $total_comprobante +=$renglon['Comprobante']['total_comprobante'];
       
       
       
      $tbody .= "<tr style=\"font-size:25px;font-family:'Courier New', Courier, monospace\">";
       $tbody .= "<td>" .$renglon['Comprobante']['fecha_contable'] . "</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['tipo_comprobante']. "</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['nro_comprobante'] . "</td>";
       $tbody .= "<td>" .substr($renglon['Comprobante']['razon_social'],0,37) . "</td>";       
       $tbody .= "<td>" .$renglon['Comprobante']['cuit']. "</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['subtotal_neto']. "</td>"; 
       $tbody .= "<td>" .$renglon['Comprobante']['no_gravado']. "</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['exento']. "</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['iva_21']. "</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['iva_105']."</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['total_ib']."</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['total_comprobante']."</td>";
       $tbody .= "</tr>";
   }else{
   
       $total_subtotal_neto = $total_subtotal_neto + $renglon['Comprobante']['subtotal_neto']; 
       $total_no_gravado +=$renglon['Comprobante']['no_gravado']; 
       $total_exento +=$renglon['Comprobante']['exento']; 
       $total_iva_21 +=$renglon['Comprobante']['iva_21']; 
       $total_iva_105 +=$renglon['Comprobante']['iva_105']; 
       $total_ib +=$renglon['Comprobante']['total_ib']; 
       $total_comprobante +=$renglon['Comprobante']['total_comprobante'];
      
   
   
       $tbody .= "<tr style=\"font-size:25px;font-family:'Courier New', Courier, monospace\">";
       $tbody .= "<td>" .$renglon['Comprobante']['fecha_contable'] . "</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['tipo_comprobante']. "</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['nro_comprobante'] . "</td>";
       $tbody .= "<td>" .substr($renglon['Comprobante']['razon_social'],0,37) . "</td>";       
       $tbody .= "<td>" .$renglon['Comprobante']['cuit']. "</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['subtotal_neto']. "</td>"; 
       $tbody .= "<td>" .$renglon['Comprobante']['no_gravado']. "</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['exento']. "</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['iva_21']. "</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['iva_105']."</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['total_ib']."</td>";
       $tbody .= "<td>" .$renglon['Comprobante']['total_comprobante']."</td>";
       $tbody .= "</tr>";
   
   
   
   }
   ++$i;
   ++$z;
   
   
   if($z > $total_registros){
   
       $tbody .= "<tr style=\"font-size:25px;font-family:'Courier New', Courier, monospace\">";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";
       $tbody .= "<td></td>";       
       $tbody .= "<td>Transporte</td>";
       $tbody .= "<td>" .$total_subtotal_neto. "</td>"; 
       $tbody .= "<td>" .$total_no_gravado. "</td>";
       $tbody .= "<td>" .$total_exento. "</td>";
       $tbody .= "<td>" .$total_iva_21. "</td>";
       $tbody .= "<td>" .$total_iva_105."</td>";
       $tbody .= "<td>" .$total_ib."</td>";
       $tbody .= "<td colspan='2'>" .$total_comprobante."</td>";
       $tbody .= "</tr>";
    
   
   }
   
   
   
}





 
//agrego las observaciones







$html='';


$html .=' 
    
 
    '.$tbody.'
';
  
  
   
   
   
$pdf->writeHTML($html, true, false, true, false, true);


// QRCODE,L : QR-CODE Low error correction
// set style for barcode
$style = array(
    'border' => 2,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

//$pdf->Text(20, 25, 'QRCODE L');

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->SetBarcode(date("Y-m-d H:i:s", time()));
ob_clean();

$pdf->Output('IVA_VENTAS.pdf', 'D');


//============================================================+
// END OF FILE
//============================================================+
