<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class RRHHTipoLiquidacion extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'rrhh_tipo_liquidacion';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	
    
    
    
    public $hasMany = array(
    		'RRHHTipoLiquidacionClaseLiquidacion' => array(
    				'className' => 'RRHHTipoLiquidacionClaseLiquidacion',
    				'foreignKey' => 'id_tipo_liquidacion',
    				'dependent' => false,
    				'fields' => '',
    				'order' => '',
    				'limit' => '',
    				'offset' => '',
    				'exclusive' => '',
    				'finderQuery' => '',
    				'counterQuery' => ''
    		));


}
