<?php

class ArticuloController extends AppController
{
    public $name = 'Articulo';
    public $model = 'Articulo';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $nombre_producto = "Articulo";
    public $d_padre = "d_producto";
    public $id_lista_precio = 0;
    
    
    public function beforeFilter() {
        $id_lista_precio = $this->getFromRequestOrSession($this->model.'.id_lista_precio');
        $this->id_lista_precio = $id_lista_precio;
        
        if($id_lista_precio == "")
            $this->id_lista_precio = 2; //por default usa la 2
            
       // return;
    } 
    
    public function index(){

        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = array();
		$this->RecuperoFiltros($conditions);
		
		
		/*Recupero el filtro de deposito, si me lo pasa quiere decir que quiere ver el stock*/
		$id_deposito = $this->getFromRequestOrSession($this->model.'.id_deposito');
		if($id_deposito!= "" && $id_deposito>0){
			
			$array_contain = array('Unidad','FamiliaProducto','Origen','Marca','Iva','Categoria', 'ListaPrecioProducto'=>array('conditions'=>array('ListaPrecioProducto.id_lista_precio'=>$this->id_lista_precio),"ListaPrecio","Moneda"),'Unidad','CuentaContableVenta','CuentaContableCompra','ProductoTipo','StockProducto'=>array('conditions'=>array('StockProducto.id_deposito'=>$id_deposito)));
			
		}else{
			
			$array_contain = array('Unidad','FamiliaProducto','Origen','Marca','Iva','Categoria', 'ListaPrecioProducto'=>array('conditions'=>array('ListaPrecioProducto.id_lista_precio'=>$this->id_lista_precio),"ListaPrecio","Moneda"),'Unidad','CuentaContableVenta','CuentaContableCompra','ProductoTipo');
		}
		
		
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
        		'contain' =>$array_contain,
			// 'contain' =>array('ArticuloRelacion'/*=>array('Componente'=>array('Categoria')*/), 'FamiliaProducto','StockProducto','Origen'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.codigo desc'
        );
          //$this->Security->unlockedFields = array('costo','precio'); 
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
        
        ////////////////////
        //Filters
        ///////////////////
        
        //CUE
        $formBuilder->addFilterInput('codigo', 'C&oacute;digo', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $codigo));
        $formBuilder->addFilterInput('d_producto', 'Descripci&oacute;n', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $descripcion));
        $formBuilder->setDataListado($this, 'Listado de Productos', 'Datos de los Productos', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        //Headers
        $formBuilder->addHeader('id', 'producto.id', "10%");
        $formBuilder->addHeader('C&oacute;digo', 'producto.codigo', "20%");
        $formBuilder->addHeader('Precio', 'producto.precio', "20%");
        $formBuilder->addHeader('Descripci&oacute;n', 'producto.d_producto', "30%");
        
        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'codigo');
        $formBuilder->addField($this->model, 'precio');
        $formBuilder->addField($this->model, 'd_producto');
 
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    }
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $data_codigo_producto = $this->getProductoCodigo();//traigo la tabla producto_codigo
        if(count($data_codigo_producto)>0)
        	$data = array_merge($data,$data_codigo_producto);
        
        
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
         //parseo el data para que los models queden dentro del objeto de respuesta
         foreach($data as &$valor){
               
             $this->cleanforOutput($valor,$this->id_lista_precio);
         }
        
        
       //parseo el data para que los models queden dentro del objeto de respuesta
        /* foreach($data as &$valor){
               
             //$this->cleanforOutput($valor,2);
         }
        */
         
         $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }

    //fin vista json
		//[id_producto_tipo]", this.id_producto_tipo.GetStringValue()); 
       

    }
  private function filtro_destino_articulo($id_destino_producto,$conditions){
        
        switch($id_destino_producto){
            case EnumDestinoProducto::Compra:
                array_push($conditions, array($model.'.id_origen =' => EnumDestinoProducto::Compra )); 
            break;
            case EnumDestinoProducto::Venta:
                array_push($conditions, array($model.'.id_origen =' => EnumDestinoProducto::Venta )); 
            break;
             case EnumDestinoProducto::ComprayVenta:
                array_push($conditions,array( 'OR' => array(
                                                        $model.".id_origen" => array(EnumDestinoProducto::Compra, EnumDestinoProducto::Venta, EnumDestinoProducto::ComprayVenta)
                                                )
                                           )
									   ); 
            break;
             case EnumDestinoProducto::Produccion:
             	array_push($conditions, array($model.'.id_origen =' => EnumDestinoProducto::Produccion));
             	break;
            
            }
        return $conditions;
    } 
    
    
    protected function getPrecio($producto,$id_lista_precio=2){
        
        
        if($id_lista_precio == 0){
            
           return 0;// NO definido 
            
        }else
		{ //si el precio no es fijo lo busco en la lista de precio
			$this->loadModel("ListaPrecioProducto");
			$li_item = $this->ListaPrecioProducto->find('first', array(
										'conditions' => array('ListaPrecioProducto.id_producto' => $producto["id"]),
										'contain' =>false
										));
			if($li_item)
			{
				return $li_item["ListaPrecioProducto"]["precio"];
			}
			else
			{
			   return 0; 
			}
        }
    } 
    
    
     /**
    * @secured(ADD_PRODUCTO)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $this->loadModel("ArticuloRelacion");
            $id_p = "";
            $this->request->data['Producto']['activo'] = 1;
            $model_items = 'ArticuloRelacion';
            
            try{
                
                $this->request->data[$this->model]["fecha_alta"] = date("Y-m-d H:i:s");
                $this->request->data[$this->model]["id_usuario"] = $this->Auth->user('id');
                $this->request->data[$this->model]["id_producto_tipo"] = $this->tipo_producto;
                
               
                if(!isset($this->request->data[$this->model]['id'])){
              	
                	$stock = 0;
                	if(isset($this->request->data[$this->model]["stock"]))
                		$stock = $this->request->data[$this->model]["stock"];
                	
                    $Producto = $this->{$this->model}->save($this->request->data);
                    $id_producto = $this->{$this->model}->id;
                    
                    $id_deposito = 0;
                    if(isset($this->request->data[$this->model]["id_deposito"]) && $this->request->data[$this->model]["id_deposito"]>0)
                    	$id_deposito = $this->request->data[$this->model]["id_deposito"];
                    
                    $this->AgregarArticuloListaPrecio($id_producto);
                    
                    
                    
                     //$this->request->data["ListaPrecioProducto"]["id_producto"] = $this->{$this->model}->id;
          
          
          
                   
                   //$Lista_Precio = $this->ListaPrecioProducto->save($this->request->data["ListaPrecioProducto"]);
                    
                    if (!empty($Producto)) {
                        
                        if( isset($this->request->data[$this->model_hijos_relacionados]) ){
                            
                            foreach($this->request->data[$this->model_hijos_relacionados] as &$c_item){
                                $c_item['id_producto_padre'] = $this->{$this->model}->id;
                                
                            }
                            $this->ArticuloRelacion->saveAll($this->request->data[$this->model_hijos_relacionados]);
                        }
                        
                        $this->generaMovimiento($id_producto,$stock,$id_deposito);
                          
                          $mensaje = "".$this->nombre_producto." ha sido creado exitosamente";
                          $tipo = EnumError::SUCCESS;
                          $id_p = $this->{$this->model}->id;
                    }
                    
                }else{
                    
                    
                    
                    $this->{$this->model}->id = $this->request->data["Producto"]["id"];
                    if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
                        $mensaje = "".$this->nombre_producto." ha sido creado exitosamente";
                        $tipo = EnumError::SUCCESS;
                        $this->generaMovimiento($this->{$this->model}->id,$this->request->data["Producto"]["stock"]);
                       
                    }else{
                        $mensaje = "Ha ocurrido un error, ".$this->nombre_producto." no ha podido ser creado.";
                        $tipo = EnumError::ERROR; 
                        
                    }
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,".$this->nombre_producto." no ha podido ser creado . ".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" => $id_p
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    
    
    
    private function AgregarArticuloListaPrecio($id_articulo){
       $this->loadModel("ListaPrecio");
       $this->loadModel("ListaPrecioProducto");
       
       $lista_precio = $this->ListaPrecio->find('all',array('conditions'=>array('ListaPrecio.id_tipo_lista_precio'=>EnumTipoListaPrecio::Base),'contain'=>false
       ));
       
       
       /*Agrego el producto a todas las listas base*/
       foreach($lista_precio as $lista){
           
           $datoEmpresa = $this->Session->read('Empresa');
           
           $id_moneda_base  = $datoEmpresa["DatoEmpresa"]["id_moneda"];
           $data["ListaPrecioProducto"]["id_lista_precio"] = $lista["ListaPrecio"]["id"];
           $data["ListaPrecioProducto"]["id_producto"] = $id_articulo;
           $data["ListaPrecioProducto"]["precio"] = 0.00;
           $data["ListaPrecioProducto"]["id_moneda"] = $id_moneda_base;
           $this->ListaPrecioProducto->save($data);
           
       }
        
        
        
        
    }
    
    
    /**
    * @secured(MODIFICACION_PRODUCTO)
    */
   public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->loadModel("ListaPrecioProducto");
            $this->{$this->model}->id = $id;
            $this->request->data[$this->model]["fecha_ultima_modificacion"] = date("Y-m-d H:i:s");
            $this->request->data[$this->model]["id_producto_tipo"] = $this->tipo_producto;
            unset($this->request->data["ListaPrecioProducto"]);
            
            /*
            if(isset( $this->request->data["ListaPrecioProducto"])){
            
               
                $precio_lista = $this->request->data["ListaPrecioProducto"]["precio"];
                $id_lista_precio = $this->request->data["ListaPrecioProducto"]["id_lista_precio"];
                unset( $this->request->data["ListaPrecioProducto"]);
            }
            */
            
            $ds = $this->{$this->model}->getdatasource();
                
         
            try{ 
                $ds->begin();
           
                $retorno = $this->SaveItems($this->request->data,$message);//guarda los ArticuloRelacion para que la funcion de costo se ejecute bien ademas borra
                
                if($retorno == 0){//no hay errores
                    if ($this->{$this->model}->saveAll($this->request->data,array('deep' => true  ) ) ){
                        
                    	/*
                         if(isset($id_lista_precio)){ 
                        //actualizo el precio de la lista es solo para este caso
                            
                           //esta funcion si no existe en la BD porque por ejemplo fue importado crea el registro de la LP 
                           $this->actualiza_lista_precio($id_lista_precio,$id,$precio_lista);
                           
                           
                         }
                         
                       */
                        
                            
                             $error = EnumError::SUCCESS;
                             $message = "".$this->nombre_producto." ha sido modificado exitosamente";
                            
                            //$this->Session->setFlash('El Producto ha sido modificado exitosamente.', 'success');
                            //$this->redirect(array('controller' => 'Productos', 'action' => 'index'));
                            
                            $ds->commit();
                    }else{
                        $ds->rollback();
                        $errores = $this->{$this->model}->validationErrors;
                        $errores_string = "";
                        foreach ($errores as $error){
                            $errores_string.= "&bull; ".$error[0]."\n";
                            
                        }
                        $mensaje = $errores_string;
                        $error = EnumError::ERROR;
                        $message = "Ha ocurrido un error,  ".$this->nombre_producto." no ha podido modificarse".$mensaje;
                        
                    }
                }else{
                    $error = EnumError::ERROR;
                    
                    
                }
            }catch(Exception $e){
                $ds->rollback(); 
                $error = EnumError::ERROR;
                 $message = "Ha ocurrido un error, ".$this->nombre_producto." no ha podido modificarse".$e->getMessage();
                
            }
            
            
            if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $error,
                            "message" => $message,
                            "content" => ""
            ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
            }
           
        } 
        
   
    }
    
  
    
    /**
    * @secured(BAJA_PRODUCTO)
    */
    function delete($id) {
        
        $this->loadModel($this->model);
        $this->loadModel("Modulo");
        $this->loadModel("StockArticulo");
        $this->loadModel("ComprobanteItem");
        $this->loadModel("Comprobante");
     
        
        
   
        $mensaje = "";
        $status = "";
        $this->{$this->model}->id = $id;
       try{  
       	
       	
       	$habilitado  = $this->Modulo->estaHabilitado(EnumModulo::STOCK);
       	
       	if($habilitado == 1){
       		
       		$cantidad = $this->StockArticulo->getTotalAgrupadoPorDeposito($id,null,"StockArticulo");
       		
       		if($cantidad !=0)
       			throw new Exception("Este Articulo que desa eliminar posee registros con Stock, para poder eliminarlo el stock debe estar en 0.");
       		
       	}
           
       	
       	
       	
       	$comprobante_item = $this->ComprobanteItem->find('all', array(
       			'conditions' => array(
       					'ComprobanteItem.id_producto' => $id,
       					'Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Abierto/*TODO: deberia preguntar por definitivo*/
       			) ,
       			'contain' => array('Comprobante'=>array('TipoComprobante','PuntoVenta'))
       	));
       	
       	
       	if(count($comprobante_item) >0){
       		
       		$array_nro_comprobante = array();
       		
       		foreach ($comprobante_item as $item){
       			
       			if($item["Comprobante"]["id_punto_venta"]>0)
       				array_push($array_nro_comprobante,  $item["Comprobante"]["TipoComprobante"]["codigo_tipo_comprobante"].$this->Comprobante->GetNumberComprobante($item['Comprobante']['PuntoVenta']['numero'],$item['Comprobante']['nro_comprobante']));
       			else 
       				array_push($array_nro_comprobante,	$item["Comprobante"]["TipoComprobante"]["codigo_tipo_comprobante"].$this->Comprobante->GetNumberComprobante($item['Comprobante']['d_punto_venta'],$item['Comprobante']['nro_comprobante']));
       			
       			
       		}
       		
       		
       		throw new Exception("Este Articulo que desa eliminar posee registros con alguno de estos comprobantes que se encuentran abiertos.Los comprobantes son: ".implode(",", $array_nro_comprobante).". Debe cerrar estos comprobantes o debe eliminar este articulo de los comprobantes.");
       	}
       	
       	
       	
       	//$this->ArticuloRelacion->deleteAll( array("id_producto" => $id) );
       	//$this->ListaPrecioProducto->deleteAll( array("id_producto" => $id) );//Borro el producto de todas las listas
       	
       	
       	if ($this->{$this->model}->saveField('activo', 0)) {

               
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "".$this->nombre_producto." ha sido eliminado exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception("ERROR: No se pudo borrar el producto");
                 
          }
          catch(Exception $ex)
          { 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
    }
    
    
    /**
    * @secured(CONSULTA_PRODUCTO)
    */
    public function home(){
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
    
  
    
    

  protected function existe_codigo(){
        
  	
  	/*
        $codigo = $this->request->query['codigo'];
        $id_producto = $this->request->query['id'];
        */
        
  		$id_producto=  $this->getFromRequestOrSession($this->model.'.id');
        $codigo = $this->getFromRequestOrSession($this->model.'.codigo');
        $message = "";
        
        $this->loadModel($this->model);
      
        $existe = $this->{$this->model}->find('count',array('conditions' => array(
                                                                            $this->model.'.codigo' => $codigo,
                                                                            $this->model.'.id !=' =>$id_producto,
                                                                            $this->model.'.id_producto_tipo' =>$this->tipo_producto
                                                                            )
                                                                            ));

        $messagge = "";
        if($existe >0){
        	$error = EnumError::ERROR;
        	$message = "El c&oacute;digo que ingreso ya existe, es inv&aacute;lido" ;
        }else{
        	$error = EnumError::SUCCESS;
        	
        }
        	
        	$output = array(
        			"status" => $error,
        			"message" => $message,
        			"content" => "",
        			"message_type"=>EnumMessageType::Modal
        	);
        	$this->set($output);
        	$this->set("_serialize", array("status", "message", "content"));
        
    }
    
    
    protected function existe_codigo_barra(){
    	
    	$codigo = $this->request->data['codigo_barra'];
    	$id_producto = $this->request->data['id'];
    	$this->loadModel($this->model);
    	
    	
    	
    	$data = $this->{$this->model}->find('count',array('conditions' => array(
    			'OR' => array(
    					array($this->model.'.codigo_barra'=> $codigo),
    					array($this->model.'.codigo_barra2'=> $codigo),
    					array($this->model.'.codigo_barra3'=> $codigo),
    					array($this->model.'.codigo_barra4'=> $codigo),
    					array($this->model.'.codigo_barra5'=> $codigo),
				),
    			
    			$this->model.'.id !=' =>$id_producto
    	)
    	));
    	
    	
   
    	if($data>0)
    		$error = EnumError::ERROR;
    	else
    		$error = EnumError::SUCCESS;
    			
    	$output = array(
    					"status" => $error,
    					"message" => "",
    					"content" => $data
    			);
    	$this->set($output);
    	$this->set("_serialize", array("status", "message", "content"));
    	
    	

    }
    
    /**
    * @secured(REPORTE_PRODUCTO)
    */
  public function pdfExport($id){
        
        $error = EnumError::ERROR;
        $this->loadModel("Articulo");
        $this->loadModel("DatoEmpresa");
        $this->Articulo->id = $id;
        $this->Articulo->contain(array('ProductoTipo','ArticuloRelacion'=>'ArticuloHijo'));
        //$this->Factura->order(array('FacturaItem.orden asc'));
        $this->request->data = $this->Articulo->read(); 
        Configure::write('debug',0);
        App::uses('CakeSession', 'Model/Datasource');
             
        $datoEmpresa = CakeSession::read('Empresa');
        
        
        $datos_empresa = $this->DatoEmpresa->read();
        
       
            
        $this->set('datos_pdf',$this->request->data); 
        $this->set('datos_empresa',$datoEmpresa); 
        $this->set('id',$id);
        Configure::write('debug',0);
        $this->layout = 'pdf'; //esto usara el layout pdf.ctp
        $this->response->type('pdf');
        $this->render('/Articulo/pdf_export');
        
    } 
     
    private function actualiza_lista_precio($id_lista_precio,$id_producto,$precio_lista){
       
       
       
        $lp = $this->ListaPrecioProducto->find('first', array(
                                'conditions' =>   array('ListaPrecioProducto.id_lista_precio' => $id_lista_precio,'ListaPrecioProducto.id_producto' => $id_producto),
                                'contain' =>false
                            ));
                            
                        if($lp){
                         $this->ListaPrecioProducto->updateAll(
                                  array('ListaPrecioProducto.precio' =>  $precio_lista ),
                                  array('ListaPrecioProducto.id_lista_precio' => $id_lista_precio,'ListaPrecioProducto.id_producto' => $id_producto  ) );
                                  
                       }else{
                           //grabo la lista de precio
                           $lista_precio = array();
                           $lista_precio["ListaPrecioProducto"]["id_lista_precio"] = $id_lista_precio;
                           $lista_precio["ListaPrecioProducto"]["id_producto"] = $id_producto;
                           $lista_precio["ListaPrecioProducto"]["precio"] = $precio_lista;
                           $this->ListaPrecioProducto->save($lista_precio["ListaPrecioProducto"]);
                           
                       }           
       
   }
   
   
   
    
    
    /**
    * @secured(BAJA_PRODUCTO)
    */
  public function deleteItem($id_item){
        
        $this->loadModel($this->model);
        
        
        $item = $this->{$this->model}->ArticuloRelacion->find('first', array(
                                    'conditions' => array('ArticuloRelacion.id' => $id_item),
                                    'contain' =>false
                                    ));
        
        try{
            if ($this->{$this->model}->ArticuloRelacion->delete($id_item)) {
                
                
                
                $costo = 0;
                $this->{$this->model}->calculoCosto($costo,$item["ArticuloRelacion"]["id_producto_padre"]);
        
                $status = EnumError::SUCCESS;
                $mensaje = "El Item  ha sido eliminado exitosamente.";
              
            }else{
              
                
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
                
                
            }
        }
        catch(Exception $ex){ 
              $mensaje = "Ha ocurrido un error, el Item no ha podido ser borrado";
               $status = EnumError::ERROR; 
        }
        
        
        
        
        if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
        }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
        }
        
        
        
        
    }
	
	    
     /**
    * @secured(CONSULTA_PRODUCTO)
    */
  public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    } 
    
   private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
      
      $this->set('model',$model);
      $this->set('model_name',$this->model);
      Configure::write('debug',0);
      $this->render($vista);
   
    }  
    
    
    
    protected function cleanforOutput(&$valor,$id_lista_precio){
      
			//$valor[$this->model]["precio_minimo_producto"] ="0.00";
			
			if(isset($valor["ListaPrecioProducto"][0]) && $valor["ListaPrecioProducto"][0]["id"] >0 )
			{
                $valor[$this->model]["precio"] = (string) round($valor["ListaPrecioProducto"][0]["precio"],2);// es [0] ya que como se le pasa el id_lista_precio entonces siempre trae un registro
                if(isset($valor["ListaPrecioProducto"][0]["precio_minimo"]) && $valor["ListaPrecioProducto"][0]["precio_minimo"] >0)
					$valor[$this->model]["precio_minimo_producto"] = (string) $valor["ListaPrecioProducto"][0]["precio_minimo"];
            }else{
                $valor[$this->model]["precio"] = "0.00"; 
            } 
            //$valor[$this->model]["precio"] = (string) $this->getPrecio($valor[$this->model],$id_lista_precio);
            
            if( isset($valor["ListaPrecioProducto"][0]["porcentaje_utilidad_minima"])  && $valor["ListaPrecioProducto"][0]["porcentaje_utilidad_minima"]>0)
                $valor[$this->model]["precio_minimo_producto"] = (string)  ($valor["ListaPrecioProducto"][0]["precio_minimo_por_utilidad"]);
            
             $valor[$this->model]['ultimo_precio_compra'] = (string) 0;
             $valor[$this->model]['ultimo_precio_venta'] = (string) 0;
      
             if(isset($valor['FamiliaProducto']) && $valor['FamiliaProducto']['id']!= null)
                $valor[$this->model]['d_familia'] = $valor['FamiliaProducto']['d_familia'];
             else
                $valor[$this->model]['d_familia'] = 'Sin Informar';
                
			$datoEmpresa = $this->Session->read('Empresa');
            $id_moneda_base  = $datoEmpresa["DatoEmpresa"]["id_moneda"];
                       
            $valor[$this->model]['d_moneda_precio'] = (string)""; //Por defecto 
			$valor[$this->model]['id_moneda_precio'] = $id_moneda_base; // por defecto
             

            if(isset($valor["ListaPrecioProducto"][0]) && $valor["ListaPrecioProducto"][0]["id"] >0 )
			{
              $valor[$this->model]['id_moneda_precio'] = $valor["ListaPrecioProducto"][0]["id_moneda"];
			  $valor[$this->model]['d_moneda_precio']  = $valor["ListaPrecioProducto"][0]["Moneda"]["d_moneda"];
			}  


            
            $valor[$this->model]['d_producto_tipo'] = $valor['ProductoTipo']['d_producto_tipo']; 
            $valor[$this->model]['requiere_conformidad'] = $valor['ProductoTipo']['requiere_conformidad']; 

       
             
             if(isset($valor['Origen']) && $valor['Origen']['id']!= null )
                $valor[$this->model]['d_origen'] = $valor['Origen']['d_origen'];
              else
                $valor[$this->model]['d_origen'] = 'Sin Informar';
                
              unset($valor['Origen']);
             
             if(isset($valor['Marca']) && $valor['Marca']['id']!= null )
                $valor[$this->model]['d_marca'] = $valor['Marca']['d_marca'];   
             else
                $valor[$this->model]['d_marca'] = 'Sin Informar'; 
                
            // unset($valor['Marca']);
                
              if(isset($valor['Iva']) && $valor['Iva']['id']!= null)
                $valor[$this->model]['d_iva'] = $valor['Iva']['d_iva'];   
             else
                $valor[$this->model]['d_iva'] = 'Sin Informar';
                
             unset($valor['Iva']);
                
              if(isset($valor['Marca']) && $valor['Marca']['id']!= null )
                $valor[$this->model]['d_marca'] = $valor['Marca']['d_marca'];   
             else
                $valor[$this->model]['d_marca'] = 'Sin Informar';
                
             unset($valor['Marca']);
              
              if(isset($valor['Categoria']) && $valor['Categoria']['id']!= null)
                $valor[$this->model]['d_categoria'] = $valor['Categoria']['d_categoria'];   
             else
                $valor[$this->model]['d_categoria'] = 'Sin Informar';
                    
                
            unset($valor['Categoria']);    
                
               if(isset($valor['ProductoTipo']['DestinoProducto']['id']) && $valor['ProductoTipo']['DestinoProducto']['id']!= null )
                $valor[$this->model]['d_destino_producto'] = $valor['ProductoTipo']['DestinoProducto']['d_destino_producto'];   
             else
                $valor[$this->model]['d_destino_producto'] = 'Sin Informar'; 
             
             
             //TRAIGOO EL VALOR DE LA PRIMERA LISTA ESTO HAY QUE CAMBIARLO   
             if(isset($valor['ListaPrecioProducto']) && isset($valor['ListaPrecioProducto'][0]) && $valor['ListaPrecioProducto'][0]['id']!= null )
                $valor[$this->model]['id_lista_precio_producto'] =  $valor['ListaPrecioProducto'][0]['id_lista_precio'];   
             
                
                
                
                $valor[$this->model][$this->d_padre] =  $valor[$this->model]["d_producto"];   
                
            
                
                
                if(isset($valor['Unidad']) && $valor['Unidad']['id']!= null )
                $valor[$this->model]['d_unidad'] = $valor['Unidad']['d_unidad'];   
             else
                $valor[$this->model]['d_unidad'] = 'Sin Informar';           
             
             
             
             if(isset($valor['CuentaContableCompra']) && $valor['CuentaContableCompra']['id']!= null )
                    $valor[$this->model]['d_cuenta_contable_compra'] = $valor['CuentaContableCompra']['d_cuenta_contable'];  
                    
                     
             if(isset($valor['CuentaContableVenta']) && $valor['CuentaContableVenta']['id']!= null )
                    $valor[$this->model]['d_cuenta_contable_venta'] = $valor['CuentaContableVenta']['d_cuenta_contable'];
             
                    
                
             if(isset($valor['ProductoCodigo']['id']) && $valor['ProductoCodigo']['id']!= null ){
                    	$valor[$this->model]['codigo'] = $valor['ProductoCodigo']['codigo'];
                    	if(strlen($valor['ProductoCodigo']['d_producto_codigo'])>0)
                    		$valor[$this->model]['d_producto'] = $valor['ProductoCodigo']['d_producto_codigo'];
             }
             
             
             
             if(isset($valor['Area']) && $valor['Area']['id']!= null )
             		$valor[$this->model]['d_area'] = $valor['Area']['d_area'];
             else
             		$valor[$this->model]['d_area'] = 'Sin Informar';
 
             		
             		
             		
             if($valor[$this->model]["id_producto_tipo"] == EnumProductoTipo::BIENUSO){
             	
             	
             	$valor[$this->model]["fecha_vencimiento"] = date('y-m-d');//poner la fecha del ultimo mantenimiento sino tiene poner sin informar
             	$valor[$this->model]["d_fecha_vencimiento"] = "Vencido";
				$valor[$this->model]["item_observacion"] = 	  "Detalle del Mantenimiento";
             	
             }else{
             	$valor[$this->model]["fecha_vencimiento"] = null;
             	$valor[$this->model]["d_fecha_vencimiento"] = "";
				$valor[$this->model]["item_observacion"] = ""; 	
			 }
                    
             
			 if(isset($valor['StockProducto']) && $valor['StockProducto'][0]['id']!= null ){
			 	$valor[$this->model]['stock'] = $valor['StockProducto'][0]['stock'];
			 
			 }
			 
			 
			 
			 if(isset($valor['Talle']) && $valor['Talle']['id']!= null )
			 	$valor[$this->model]['d_talle'] = $valor['Talle']['d_talle'];
			 else
			 	$valor[$this->model]['d_talle'] = 'Sin Informar'; 
			 
			 	
			 if(isset($valor['Color']) && $valor['Color']['id']!= null )
			 	$valor[$this->model]['d_color'] = $valor['Color']['d_color'];
			 else
			 	$valor[$this->model]['d_color'] = 'Sin Informar';
			 			
			
			
			 
             unset($valor['ListaPrecioProducto']);   
             unset($valor['ProductoTipo']['DestinoProducto']);   
             unset($valor['CuentaContableVenta']);   
             unset($valor['CuentaContableCompra']);   
             unset($valor['Unidad']);   
             unset($valor['ProductoTipo']);   
             unset($valor['Area']);   
             unset($valor['StockProducto']);   
             unset($valor['Talle']);   
             unset($valor['Color']);   
             unset($valor['Marca']);   
             unset($valor['FamiliaProducto']);
         
             unset($valor[$this->model_stock]);   
            
             
               
      
  }
  
  protected function RecuperoFiltros(&$conditions){
      
      //Recuperacion de Filtros
        $id =        				 $this->getFromRequestOrSession($this->model.'.id');
        $codigo =    				 $this->getFromRequestOrSession($this->model.'.codigo');
        $descripcion = 				 $this->getFromRequestOrSession($this->model.'.d_producto');
        $id_origen= 				 $this->getFromRequestOrSession($this->model.'.id_origen');
        $id_destino_producto = 		 $this->getFromRequestOrSession($this->model.'.id_destino_producto');// si es 3 debo buscar por 1 tmb Compra y Compra y Venta
        $id_lista_precio = 			 $this->getFromRequestOrSession($this->model.'.id_lista_precio');
        $id_producto_tipo = 		 $this->getFromRequestOrSession($this->model.'.id_producto_tipo');
        $id_producto_clasificacion = $this->getFromRequestOrSession($this->model.'.id_producto_clasificacion');
        $producto_es_remitible = 	 $this->getFromRequestOrSession('Producto.remitible');
        $activo 			  = 	 $this->getFromRequestOrSession($this->model.'.activo');
		
		$id_categoria= 				 $this->getFromRequestOrSession($this->model.'.id_categoria');
		$id_familia_producto= 		 $this->getFromRequestOrSession($this->model.'.id_familia_producto');
		$id_area= 					 $this->getFromRequestOrSession($this->model.'.id_area');
		$id_talle= 					 $this->getFromRequestOrSession($this->model.'.id_talle');
		$id_color= 					 $this->getFromRequestOrSession($this->model.'.id_color');
		$codigo_barra = 		     $this->getFromRequestOrSession($this->model.'.codigo_barra');
		
        $conditions = array(); 
        //array_push($conditions, array($this->model.'.activo =' => 1)); 
        if($id!="")
            array_push($conditions, array($this->model.'.id =' => $id )); 
        
        if($codigo!=""){
            array_push($conditions, array($this->model.'.codigo LIKE' => '%' . $codigo  . '%')); 
            array_push($this->array_filter_names,array("C&oacute;digo:"=>$codigo));
        }
             
        if($descripcion!=""){
            array_push($conditions, array($this->model.'.d_producto LIKE' => '%' . $descripcion  . '%'));
            array_push($this->array_filter_names,array("Desc."=>$descripcion));
        }
		if($id_categoria!="")
            array_push($conditions, array($this->model.'.id_categoria =' => $id_categoria ));
		if($id_familia_producto!="")
            array_push($conditions, array($this->model.'.id_familia_producto =' => $id_familia_producto ));
	   if($id_area!="")
            array_push($conditions, array($this->model.'.id_area =' => $id_area ));
	   if($id_talle!="")
            array_push($conditions, array($this->model.'.id_talle =' => $id_talle ));
	   if($id_color!="")
            array_push($conditions, array($this->model.'.id_color =' => $id_color ));
       
       if($id_origen!="")
            array_push($conditions, array($this->model.'.id_origen =' => $id_origen ));
            
       if($id_producto_clasificacion!="")
            array_push($conditions, array($this->model.'.id_producto_clasificacion =' => $id_producto_clasificacion ));     
       
       if($id_producto_tipo!=""){
            $id_array_id_tipo_producto = explode(',', $id_producto_tipo);
            array_push($conditions, array($this->model.'.id_producto_tipo' => $id_array_id_tipo_producto ));
       }else{
           if(isset($this->tipo_producto))
            array_push($conditions, array($this->model.'.id_producto_tipo' => $this->tipo_producto ));
       }
       
       if($producto_es_remitible!=""){
        array_push($conditions, array('ProductoTipo.remitible' => $producto_es_remitible));  
        array_push($this->array_filter_names,array("Es Remitible."=>$producto_es_remitible));
       }
       
       if($id_destino_producto!=""){
					$conditions = $this->filtro_destino_articulo($id_destino_producto,$conditions);
       }
       
       if($activo!=""){
       	array_push($conditions, array($this->model.'.activo' => $activo));     
       	array_push($this->array_filter_names,array("Activo:"=>$activo));
       }
       
       
       if($codigo_barra!="")
       	array_push($conditions, array('OR' => array(
       			array($this->model.'.codigo_barra'=> $codigo_barra),
       			array($this->model.'.codigo_barra2'=> $codigo_barra),
       			array($this->model.'.codigo_barra3'=> $codigo_barra),
       			array($this->model.'.codigo_barra4'=> $codigo_barra),
       			array($this->model.'.codigo_barra5'=> $codigo_barra),
       			
       	)));
       	
        return $conditions;
  }
  
  
  private function SaveItems(&$data,&$mensaje){
    
      $this->loadModel("Articulo");
      if(isset($data["ArticuloRelacion"])){
      $mensaje = "";    
      foreach($data["ArticuloRelacion"] as $item){
          
          if(!isset($item["id"])){ //valido solo para nuevos items
              if($this->validar_articulo_relacion($data[$this->model]["id"],$item["id_producto_hijo"],$mensaje)==1){
                 return 1;     
              }
          }
      }
          $this->Articulo->ArticuloRelacion->SaveAll($data["ArticuloRelacion"]);
          unset($data["ArticuloRelacion"]);
          return 0;
          
      }
      
      
  }
  
  protected function getCosto($id_articulo){
  
  $this->loadModel("Articulo");
      
  $articulo = $this->Articulo->find('first', array(
                                'conditions' => array('Articulo.id' => $id_articulo),
                                'contain' =>false
                                ));
                                
                                
    if($articulo){
        
        $costo = $articulo["Articulo"]["costo"];
    }else{
       
       $costo = 0; 
        
    }
    
    $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $costo,
            "page_count" =>0
            );
     $this->set($output);
     $this->set("_serialize", array("status", "message","page_count", "content")); 
      
      
  }
    
    
    protected function validar_articulo_relacion($id_articulo_padre,$id_articulo_hijo,&$mensaje){
        
        $this->loadModel("Articulo");
        
        $mensajes = "";
        $error = 0;
        
        if($id_articulo_padre == $id_articulo_hijo){
            $mensaje .= "El art&iacute;culo no se puede contener a si mismo";
            $error    = 1;
        }
        
        //1) No puede haber BUCLES en ningun tipo de relacion
        //El articulo a agregar como hijo no debe pertenecer al arbol del producto padre
        //busco los hijos del articulo a agregar y si "llego" hasta el articulo padre entonces hay error
        $tiene_bucle = false;
        $this->Articulo->buscoBucle($tiene_bucle,$id_articulo_padre,$id_articulo_hijo,$mensaje);
        if($tiene_bucle){
            $mensaje .= "No es posible asociar este art&iacute;culo al existir una condici&oacute;n de bucle";
            $error    = 1;
            
        }
        
        return $error;
        
       
            
        
        
        
    
    }
    
    
    public function getProductoCodigo(){
    	

    	
    	
    	try{
	    	$conditions = array();
	    	$this->RecuperoFiltros($conditions);
	    	$data = array();
	    	
	    	$codigo = $this->getFromRequestOrSession($this->model.'.codigo');
	    	
	    	
	    	$indexCompleted = array_search($this->model.'.codigo LIKE', $conditions);
	    	
	    	/*borro la KEY del conditions*/
	    	foreach($conditions as $key =>$value){
	    		if( isset($value[$this->model.'.codigo LIKE']))
	    			unset($conditions[$key]);
	    	}
	    	
	    	if($codigo!="" ){
	    		array_push($conditions, array('ProductoCodigo.codigo LIKE' => '%' . $codigo  . '%')); 
	    		
	    		
	    	
	    		
	    	
	    	$this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
	    			'fields'=>'*',
	    			'joins' => array(
	    					array(
	    							'table' => 'producto_codigo',
	    							'alias' => 'ProductoCodigo',
	    							'type' => 'LEFT',
	    							'conditions' => array(
	    									'ProductoCodigo.id_producto = '.$this->model.'.id'
	    							),
	    							
	    							
	    					)
	    			),
	    			'contain' =>array('FamiliaProducto','Origen','Marca','Iva','Categoria', 'ListaPrecioProducto'=>array('conditions'=>array('ListaPrecioProducto.id_lista_precio'=>$this->id_lista_precio),"ListaPrecio","Moneda"),'Unidad','CuentaContableVenta','CuentaContableCompra','ProductoTipo','ProductoCodigo'),
	    			// 'contain' =>array('ArticuloRelacion'/*=>array('Componente'=>array('Categoria')*/), 'FamiliaProducto','StockProducto','Origen'),
	    			'conditions' => $conditions ,
	    			'limit' => $this->numrecords,
	    			'page' => $this->getPageNum(),
	    			'order' => $this->model.'.codigo desc'
	    	);
	    	
	    	$this->PaginatorModificado->settings = $this->paginate;
	    	$data = $this->PaginatorModificado->paginate($this->model);
	    	$page_count = $this->params['paging'][$this->model]['pageCount'];
	    	
	    	}
	    	return $data;
    	
    	}catch (Exception $e){
    		
    		return array();
    	}
    	
    	
    	
    }
    
    
    /*
    public function beforeFilter(){
     
     
        $id_lista_precio = $this->getFromRequestOrSession($this->model.'.id_lista_precio');
        $this->loadModel($this->model);
        $this->{$this->model}->id_lista_precio = $id_lista_precio;
        return;
        
        
    }
    */
    
    
    /**
     * @secured(BTN_EXCEL_PEDIDOSINTERNOS)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Articulos");
    	
    	
    	
    }
    
    public function generaMovimiento($id_producto,$stock,$id_deposito){
    	
    	$this->loadModel("Comprobante");
    	$modulo_stock = $this->Comprobante->getActivoModulo(EnumModulo::STOCK);
    	$this->loadModel($this->model);
    	
    	$producto = $this->{$this->model}->find('first',array('conditions'=>array($this->model.'.id'=>$id_producto),'contain'=>false
    	));
    	
    	if($stock>0 && $producto[$this->model]["id_producto_clasificacion"] == EnumProductoClasificacion::Articulo_REAL && $id_deposito >0 && $modulo_stock == 1 && $stock>0){
    		

    		
	    	$this->loadModel("Movimiento");
	    	$movimiento["id_producto"] = $id_producto;
	    	$movimiento["id_movimiento_tipo"] = EnumTipoMovimiento::AjusteStock;
	    	$movimiento["cantidad"] = $stock;
	    	$movimiento["fecha"] = date('y-m-d');
	    	$movimiento["id_deposito_destino"] = $id_deposito;
	    	$movimiento["id_usuario"] =  $this->Auth->user('id'); 
	    	$this->Movimiento->addMovimiento($movimiento);
    	
    	}
    	
    	
    }
    
    
    
}



?>