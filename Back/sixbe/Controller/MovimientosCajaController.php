<?php
App::uses('ComprobantesController', 'Controller');


  /**
    * @secured(CONSULTA_MOVIMIENTO_CAJA)
  */
class MovimientosCajaController extends ComprobantesController {
    
    public $name = EnumController::MovimientosCaja;
    public $model = 'Comprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $requiere_impuestos = 0;//Flag para impuestos calculados por el sistema
    
    /**
    * @secured(CONSULTA_MOVIMIENTO_CAJA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $conditions = $this->RecuperoFiltros($this->model);
                       
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>array('EstadoComprobante','TipoComprobante','ComprobanteValor', 'PuntoVenta','DetalleTipoComprobante','Moneda'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.id desc'
        ); //el contacto es la sucursal
        
      if($this->RequestHandler->ext != 'json'){  
        
    }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
            
             //$valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
             //unset($valor['ComprobanteItem']);
             
             if(isset($valor['TipoComprobante'])){
                 $valor[$this->model]['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                 $valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
                 unset($valor['TipoComprobante']);
             }
             
             $valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
             $valor[$this->model]['nro_comprobante'] = (string) str_pad($valor[$this->model]['nro_comprobante'], 8, "0", STR_PAD_LEFT);
             
             $valor[$this->model]['d_detalle_tipo_comprobante'] = $valor['DetalleTipoComprobante']['d_detalle_tipo_comprobante'];
          
             
             if(isset($valor['EstadoComprobante'])){
                $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
                unset($valor['EstadoComprobante']);
             }
             
             if(isset($valor['Moneda'])){
                $valor[$this->model]['d_moneda_simbolo'] = $valor['Moneda']['simbolo_internacional'];
                unset($valor['Moneda']);
             }
             $this->{$this->model}->formatearFechas($valor); 
        }
        
        
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    
    /**
    * @secured(ADD_MOVIMIENTO_CAJA)
    */
    public function add(){

            parent::add();    
            return;

     }
           
   
    
    
    /**
    * @secured(MODIFICACION_MOVIMIENTO_CAJA)
    */
    public function edit($id){
  
    $this->loadModel("Cheque");
    $this->loadModel("Comprobante");
    $cheques_error = '';
    $hay_error = 0;
    
    
    if($this->request->data["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::IngresoCaja)
    	$valido = $this->Cheque->validarIngresoCheque($this->request->data,$cheques_error);
    else 
    	$valido = $this->Cheque->validarEgresoCheque($this->request->data,$cheques_error);
     
     
    if($valido == 1){
              //si es definitivo le seteo el id_comprobante a los cheques
              if($this->Comprobante->getDefinitivoGrabado($id) != 1 && $this->Comprobante->getDefinitivo($this->request->data) == 1  && $this->Cheque->tieneCheques($this->request->data)== 1 ){
                  
                  
                   //$id_comprobante = $this->request->data["Comprobante"]["id"];
                   $array_cheques = $this->Cheque->getArrayCheques($this->request->data);
                   $ds = $this->Cheque->getdatasource();
                    try{ 
                        $ds->begin();
                       
                       
                       if($this->request->data["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::IngresoCaja){
                           
                           $id_comprobante_origen = $this->request->data["Comprobante"]["id"]; 
                           $this->Cheque->actualizaComprobanteCheques($id_comprobante_origen,$array_cheques,"id_comprobante_entrada"); 
                            
                       }else{
                           
                           $id_comprobante_destino = $this->request->data["Comprobante"]["id"]; 
                           $this->Cheque->actualizaComprobanteCheques($id_comprobante_destino,$array_cheques,"id_comprobante_salida");
                           
                           $array_cheque_terceros = array();
                           $array_cheque_propios = array();
                           $this->Cheque->SeparaCheques($array_cheques,$array_cheque_terceros,$array_cheque_propios);//esta funcion le paso un array con cheques y me devuelve 2 arrays uno con los cheques de terceros y otro con los cheques propios
                        
                           $this->Cheque->actualizaEstadoCheques(EnumEstadoCheque::Emitido,$array_cheque_propios);//actualizo el estado de los cheques  
                           $this->Cheque->actualizaEstadoCheques(EnumEstadoCheque::Aplicado,$array_cheque_terceros);//actualizo el estado de los cheques  
                 
                       }
                       
                       
                      
                        
                        
                   }catch(Exception $e){
                     
                     $tipo = EnumError::ERROR;
                     $mensaje = 'Hubo una falla cuando se actulizaron los comprobantes de salida en los cheques'.$e->getMessage();
                     $ds->rollback();
                     $hay_error = 1;
                   }
              }
    
    }else{
    	$hay_error = 1;
		$tipo = EnumError::ERROR;
		$mensaje = $cheques_error;
    	
    }
              
        
              if($hay_error == 0){ /*Sino hubo falla al actualizar los cheques entonces si edito*/
                parent::edit($id);
                if($this->Cheque->tieneCheques($this->request->data)== 1)
                	$ds->commit();
                return 0; 
              
               }   
   
      /*Aca solo entra si hubo error si entra por el edit sale por flujo de mensaje*/
      $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                );      
            $this->set($output);
      
            $this->set("_serialize", array("status", "message", "content"));
            return;         
                
        
    }
    
    
    /**
    * @secured(BAJA_MOVIMIENTO_CAJA)
    */
    public function delete($id){
        
     parent::delete($id);    
        
    }
    
   /**
    * @secured(BAJA_MOVIMIENTO_CAJA)
    */
    public function deleteItem($id,$externo=1){
        
        parent::deleteItem($id,$externo);    
        
    }
    
    
    /**
    * @secured(CONSULTA_MOVIMIENTO_CAJA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
  
   
    public function pdfExport($id,$vista=''){
        
        
     $this->loadModel("Comprobante");
    $mov_CAJA = $this->Comprobante->find('first', array(
                                                        'conditions' => array('Comprobante.id' => $id),
                                                        'contain' =>array('TipoComprobante')
                                                        ));
        
        
        
        if($mov_CAJA){
           
            
            switch($mov_CAJA["TipoComprobante"]["id"]){
             
             case EnumTipoComprobante::IngresoCaja:
             
                $vista = 'IngresoCaja';
             break;   
                
             case EnumTipoComprobante::EgresoCaja:
             
                $vista = 'EgresoCaja';
             break;
     
            }
            
           
          
            parent::pdfExport($id,$vista);  
         }
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
        
        
      
 
    }
    
    
    public function Anular($id_movimiento){
        
        $this->loadModel("Comprobante");
        $this->loadModel("Asiento");
        $this->loadModel("Cheque");
        $output_asiento_revertir = array("id_asiento"=>0);
        
        
          $recibo = $this->Comprobante->find('first', array(
                    'conditions' => array('Comprobante.id'=>$id_movimiento),
                    'contain' => array('ComprobanteValor','Asiento','ChequeSalida','ChequeEntrada')
                ));
                
                 $ds = $this->Comprobante->getdatasource(); 
                 // $ds->rollback();
          $aborto = 0;
          $id_asiento = 0;
          
          $listado_cheques_terceros_anulados = array();
          $listado_cheques_propios_anulados = array();//se anulan porque son propios
          
                   
            if( $recibo && $recibo["Comprobante"]["id_estado_comprobante"]!= EnumEstadoComprobante::Anulado ){
                
                try{
                	
                	
                	if($this->PeriodoValidoParaAnular($recibo) == 0)
                		throw new Exception("El Comprobante no es posible anularlo. Ya que el Peri&oacute;do contable en el cual se encuenta no esta disponible.");
                		
                		
                		
                    $ds->begin();
                    
                     /*Se deben hacer acciones con los cheques*/
                     
                    
                       
                       
                               $monto_cheques = 0;
                               foreach($recibo["ChequeSalida"] as $cheque){
                           
                                    
                                    if($cheque["id_tipo_cheque"] == EnumTipoCheque::Terceros){ 
                                     $monto_cheques += $cheque["monto"];
                                        $this->Cheque->updateAll(
                                                            array('Cheque.id_comprobante_salida' => NULL,'Cheque.id_estado_cheque'=>EnumEstadoCheque::Cartera  ),
                                                            array('Cheque.id' => $cheque["id"]) );   
                                
                                         array_push($listado_cheques_terceros_anulados,$cheque["nro_cheque"]);
                                    }else{ //si es un cheque propio entonces lo meto en cartera nuevamente
                                    
                                         $this->Cheque->updateAll(
                                         		array('Cheque.id_comprobante_salida' => NULL,'Cheque.id_estado_cheque'=>EnumEstadoCheque::Cartera),
                                                            array('Cheque.id' => $cheque["id"]) );   
                                        
                                         array_push($listado_cheques_propios_anulados,$cheque["nro_cheque"]);
                                    }
                              
                            }        

                            /*si es IngresoCaja va a tener cheques de entrada*/
                            foreach($recibo["ChequeEntrada"] as $cheque){
                           
                                    
                                    if($cheque["id_tipo_cheque"] == EnumTipoCheque::Terceros){ 
                                     $monto_cheques += $cheque["monto"];
                                        $this->Cheque->updateAll(
                                                            array('Cheque.id_comprobante_entrada' => NULL,'Cheque.id_estado_cheque'=>EnumEstadoCheque::Cartera  ),
                                                            array('Cheque.id' => $cheque["id"]) );   
                                
                                         array_push($listado_cheques_terceros_anulados,$cheque["nro_cheque"]);
                                    }
                            }  
                            
                            /*      
                          if($recibo["Comprobante"]["comprobante_genera_asiento"] == 1)      
                            $this->Asiento->revertir($recibo["Asiento"]["id"],$monto_cheques);   /*Se debe revertir el asiento del comprobante*/  
                          
                            
                          $output_asiento_revertir = array("id_asiento"=>0);
                           if($recibo["Comprobante"]["comprobante_genera_asiento"] == 1)
                                $output_asiento_revertir = $this->Asiento->revertir($recibo["Asiento"]["id"],$monto_cheques); 
                            
                            
                        $this->Comprobante->updateAll(
                                                        array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
                                                        array('Comprobante.id' => $id_movimiento) );        
                                                  
                        
                        $movimiento_caja = $this->Comprobante->find('first', array(
                        		'conditions' => array('Comprobante.id'=>$id_movimiento,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                        		'contain' => false
                        ));
                        
                        $this->Auditar($id_movimiento,$movimiento_caja);
                        
                        
                        $ds->commit(); 
                        $tipo = EnumError::SUCCESS;
                      
                       $mensaje = '';
                        if(count($listado_cheques_terceros_anulados)>0)
                            $mensaje = ". Se Re-Ingresaron en la cartera los siguientes cheques de Terceros: Cheque Nro:".implode(" Cheque Nro: ",$listado_cheques_terceros_anulados);
                        
                        if(count($listado_cheques_propios_anulados)>0)
                            $mensaje .= ". Se Re-Ingresaron los siguientes cheques Propios: Cheque Nro:".implode(" Cheque Nro: ",$listado_cheques_propios_anulados);
                        
                        $mensaje = "El Comprobante se anulo correctamente ".$mensaje;       

                     
               }catch(Exception $e){ 
                
                $ds->rollback();
                $tipo = EnumError::ERROR;
                $mensaje = "No es posible anular el Comprobante ERROR:".$e->getMessage();
                
              }  
                            
            }else{
                
               $tipo = EnumError::ERROR;
               $mensaje = "No es posible anular el Comprobante ya se encuenta ANULADO";
            } 
            
            
            
        $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                "id_asiento" => $output_asiento_revertir["id_asiento"]
                );      
        echo json_encode($output);
        die();
        
         
        
    }
    
    public function CalcularImpuestos($id_comprobante,$id_tipo_impuesto='',$error=0,$message=''){
    
        return 0;
    }
    
    
    
    public function generaAsiento($id_comprobante,&$message){
    
         $this->loadModel("Comprobante");
         $this->loadModel("PeriodoContable");
         $this->loadModel("EjercicioContable");
         $this->loadModel("Modulo");
         $this->loadModel("CentroCosto");
         
         
         $habilitado  = $this->Modulo->estaHabilitado(EnumModulo::CONTABILIDAD);
         
         if($habilitado == 1) {
             
            $factura = $this->Comprobante->find('first', array(
                    'conditions' => array('Comprobante.id' => $id_comprobante),
                    'contain' =>array('TipoComprobante'=>array('Sistema'=>
                                                                            array('SistemaParametro')
                                                              )
                                      ,

                                      'ComprobanteValor'=>array('Valor',"CuentaBancaria"=>
                                                                            array('CuentaContable'),
                                                                "Cheque"=>
                                                                            array('BancoSucursal','TipoCheque','Chequera'=>array("CuentaBancaria"=>array('CuentaContable')))
                                                              ),'Asiento','DetalleTipoComprobante'
                                      )
                ));
                
                
          if($this->PermiteAsiento($factura) == 1 ){
            
            
                     if($factura && $factura["Comprobante"]["total_comprobante"]>0 && !$factura["Asiento"]["id"]>0 ){/*El asiento solo se hace si el comprobante es mayor a 0*/
                     
                     $id_sistema = $factura["TipoComprobante"]["id_sistema"];
                     $cuentas_detalle = array();
                     
                     
                        
                     
                     
                     foreach($factura["ComprobanteValor"] as $item_valor){
                              
                                $monto = $item_valor["monto"];
                                
                                $id_cuenta_contable = $this->getCuentaContableValor($item_valor); //devuelve la cuenta contable de un determinado valor (cheque,etc)
                              
                                  if($factura["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::IngresoCaja)  //si es destino debe =1
                                    $es_debe = 1;
                                  else
                                    $es_debe = 0;
                                         
                                 
                                  if($id_cuenta_contable>0){
                                    $item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] =$id_cuenta_contable;  
                                    $item_asiento["AsientoCuentaContable"]["monto"] = $monto ;
                                    $item_asiento["AsientoCuentaContable"]["es_debe"] = $es_debe;
                                    array_push($cuentas_detalle,$item_asiento); 
                                  }
                                  else  { 
                                    $error = EnumError::ERROR;
                                    $message = "Alguno de los valores utilizados no tiene parametrizada la cuenta contable";
                                    return -1;    
                                  }  
                     }
                     
                     
                     $id_cuenta_contable = $factura["DetalleTipoComprobante"]["id_cuenta_contable"];
                     
                      if($id_cuenta_contable>0){
                          
                          if($es_debe == 1) //este valor viene de arriba
                            $es_debe = 0;
                          else
                            $es_debe = 1;
                            
                            $item_asiento["AsientoCuentaContable"]["id_cuenta_contable"] =$id_cuenta_contable;  
                            $item_asiento["AsientoCuentaContable"]["monto"] = $factura["Comprobante"]["total_comprobante"];
                            $item_asiento["AsientoCuentaContable"]["es_debe"] = $es_debe;
                            array_push($cuentas_detalle,$item_asiento);  
                          
                       }
                      else  { 
                        $error = EnumError::ERROR;
                        $message = "El Concepto utilizado no tiene parametrizada la cuenta contable";
                        return -1;    
                      }
                     
                     
                     
                     $cabecera_asiento = $this->getCabeceraAsientoGeneradoPorComprobante($id_sistema,$factura);
                     $this->loadModel("Asiento");
                     $id_asiento = $this->Asiento->InsertaAsiento($cabecera_asiento,$cuentas_detalle,$detalle_error);
                     if($id_asiento > 0){
                        
                         $this->CentroCosto->ApropiacionPorAsiento($id_asiento);//https://docs.google.com/document/d/1-4uDVXbyn2OKmtamNxXRACuYElK2-ZWJCbybORqdlJI/edit
                        $error = EnumError::SUCCESS;
                        
                        
                        $codigo_asiento = $this->Asiento->getCodigo($id_asiento);
                        
                        $message = "El asiento fue creado correctamente. Nro: ".$codigo_asiento.". ID(".$id_asiento.")";
                        
                          
                        return $id_asiento;

                     }else{

                        $error = EnumError::ERROR;  
                        $message = "Ocurrio un error, el asiento no pudo ser creado".$detalle_error; 
                        $this->log("Error en Asiento :".$this->name." ".$message);
                        return -1;
                     }
                         
                     }else{
                         
                         $error = EnumError::ERROR;
                          $message = "El total del comprobante es cero o ya tiene un asiento ingresado";
                          return -1;
                     }       
          }else{
                 
                 return 0;//no requiere asiento
          } 
                   
                  
         }else{
                //$error = EnumError::ERROR;
                //$message = "No tiene habilitado el m&oacute;dulo de Contabilidad";
                return 0;
        }
    }
    
    
    protected function actualizoMontos($id_c,$nro_c,$total_iva,$total_impuestos){
        
        return;
    }
    protected function borraImpuestos($id_comprobante){
        return;
    }        
    
    /**
     * @secured(BTN_EXCEL_MOVIMIENTOSCAJA)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Movimientos de Caja");
    	
    	
    	
    }
    
}
    
    
   
    
    
    
    
    
    
    

?>