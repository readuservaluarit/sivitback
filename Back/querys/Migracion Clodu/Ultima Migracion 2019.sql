/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

USE `valuarit_josemuratorio_produccion`; 

/* Foreign Keys must be dropped in the target to ensure that requires changes can be done*/


/* Alter table in target */
ALTER TABLE `area` 
	CHANGE `d_area` `d_area` VARCHAR(300)  COLLATE latin1_swedish_ci NULL AFTER `id` , 
	ADD COLUMN `abreviado_d_area` VARCHAR(30)  COLLATE latin1_swedish_ci NULL AFTER `d_area` ;

/* Alter table in target */
ALTER TABLE `comprobante` 
	CHANGE `observacion` `observacion` LONGTEXT  COLLATE latin1_swedish_ci NULL AFTER `id_forma_pago` , 
	ADD COLUMN `imprimir_obs_cuerpo` TINYINT(4)   NULL AFTER `meli_shippment_cost` ;

/* Alter table in target */
ALTER TABLE `comprobante_punto_venta_numero` 
	ADD COLUMN `id_estado_comprobante_defecto_sin_uso` INT(11)   NULL AFTER `id_impuesto` , 
	DROP COLUMN `id_estado_comprobante_defecto` , 
	DROP KEY `id_estado_comprobante_defecto`, ADD KEY `id_estado_comprobante_defecto`(`id_estado_comprobante_defecto_sin_uso`) , 
	DROP FOREIGN KEY `comprobante_punto_venta_numero_ibfk_7`  ;

/* Alter table in target */
ALTER TABLE `lista_precio` 
	CHANGE `fecha_vigencia` `fecha_vigencia` DATE   NOT NULL AFTER `porcentaje_utilidad_minima` ;

/* Alter table in target */
ALTER TABLE `persona` 
	ADD COLUMN `fecha_antiguedad` DATETIME   NULL AFTER `id_persona_origen` , 
	ADD COLUMN `id_rrhh_convenio` INT(11)   NULL AFTER `fecha_antiguedad` ;
ALTER TABLE `persona`
	ADD CONSTRAINT `persona_ibfk_19` 
	FOREIGN KEY (`id_persona_origen`) REFERENCES `persona_origen` (`id`) ;


/* Create table in target */
CREATE TABLE `request_log`(
	`controller` VARCHAR(50) COLLATE latin1_swedish_ci NULL  , 
	`id_usuario` INT(11) NULL  , 
	`action` VARCHAR(50) COLLATE latin1_swedish_ci NULL  , 
	`date` DATETIME NULL  
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_categoria`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_categoria` VARCHAR(100) COLLATE latin1_swedish_ci NULL  , 
	`id_rrhh_convenio` INT(11) NULL  , 
	`abreviado` VARCHAR(20) COLLATE latin1_swedish_ci NULL  , 
	`importe` DECIMAL(20,2) NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_concepto`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_concepto` VARCHAR(100) COLLATE latin1_swedish_ci NOT NULL  , 
	`codigo` INT(11) NOT NULL  , 
	`activo` TINYINT(4) NOT NULL  DEFAULT 1 , 
	`id_cuenta_contable` INT(11) NULL  , 
	`orden_impresion` INT(11) NULL  DEFAULT 1 , 
	`texto_mostrar` VARCHAR(50) COLLATE latin1_swedish_ci NULL  , 
	`id_rrhh_tipo_concepto` INT(11) NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_convenio`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_convenio` VARCHAR(100) COLLATE latin1_swedish_ci NOT NULL  , 
	`activo` TINYINT(4) NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_modelo_liquidacion`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_modelo_liquidacion` VARCHAR(100) COLLATE latin1_swedish_ci NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_modelo_liquidacion_concepto_item`(
	`id` BIGINT(20) NOT NULL  AUTO_INCREMENT , 
	`id_rrhh_modelo_liquidacion` INT(11) NOT NULL  , 
	`id_rrhh_concepto` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_periodo_liquidacion`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_periodo_liquidacion` VARCHAR(50) COLLATE latin1_swedish_ci NOT NULL  , 
	`id_rrhh_modelo_liquidacion` INT(11) NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_persona_concepto`(
	`id` BIGINT(20) NOT NULL  AUTO_INCREMENT , 
	`id_persona` INT(11) NOT NULL  , 
	`id_rrhh_concepto` INT(11) NOT NULL  , 
	`valor` DECIMAL(20,2) NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_tipo_concepto`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_tipo_concepto` VARCHAR(50) COLLATE latin1_swedish_ci NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `rrhh_tipo_liquidacion`(
	`id` INT(11) NOT NULL  AUTO_INCREMENT , 
	`d_rrhh_tipo_liquidacion` VARCHAR(50) COLLATE latin1_swedish_ci NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=INNODB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';
 
 
 
 SET FOREIGN_KEY_CHECKS = 0;

DELETE FROM rol;

INSERT  INTO `rol`(`id`,`id_nivel_autorizado_informacion`,`descripcion`,`asignacion_geografica`,`codigo`,`solo_lectura`) VALUES (1,1,'ADMINISTRADOR',1,'ADMINISTRADOR',0),(2,1,'SOPORTE',1,'SOPORTE',1),(3,1,'VENTAS',1,'VENTAS',0),(4,1,'COMPRAS',1,'COMPRAS',0),(5,1,'CONTABILIDAD',4,'CONTABILIDAD',0),(6,1,'GERENCIA',1,'GERENCIA',0),(7,1,'GOPERATIVA',1,'GOPERATIVA',0),(8,1,'RRHH - Agenda + Consulta F',1,'Administracion Lectura',0),(9,1,'ALMACEN',1,'ALMACEN',0),(10,1,'Armado - Almacen',1,'Armado - Almacen',0),(11,1,'MULTI ADMINISTRADOR',1,'GODADMIN',0),(12,1,'GERENTE TURISMO',1,'GENRENTE_TURISMO',0),(13,1,'Factura electronica',1,'FACTURA_ELECTRONICA_BASICO',0),(14,1,'Perfil de facturacion',1,'FACTURADOR',0),(15,1,'GESTOR DE CALIDAD',1,'CALIDAD',0),(16,1,'Usuario Goperativo con permisos de armado',1,'Goperativo-Armado',0),(17,1,'Web service enerbom',1,'WEBSERVICEENERBOM',0),(18,1,'PAGOS',1,'PAGOS',0),(19,1,'CONTADOR',1,'CONTADOR',0),(20,1,'GOPERATIVOMAYOR',1,'GOPERATIVOMAYOR',0),(21,1,'ARMADO-PRODUCCION-ALMACEN',1,'ARMADO-PRODUCCION-ALMACEN',0),(22,1,'COMERCIO-EXTERIOR',1,'COMERCIO-EXTERIOR',0);


UPDATE rol SET id = id+100 WHERE id>1;
DELETE FROM rol WHERE id= 1;
INSERT  INTO `rol`(`id`,`id_nivel_autorizado_informacion`,`descripcion`,`asignacion_geografica`,`codigo`,`solo_lectura`) VALUES (1,1,'ADMINISTRADOR',1,'ADMINISTRADOR',0),(2,1,'SivitCliente',1,'SivitCliente',1),(3,1,'SivitProveedor',1,'SivitProveedor',0),(4,1,'SivitB2B',1,'SivitB2B',0);
SET FOREIGN_KEY_CHECKS = 1;

DELETE FROM funcion_rol;
DELETE FROM funcion;

/*pasar todas las funciones*/
estado_comprobante
estado_tipo_comprobante
entidad

UPDATE usuario SET id_rol=119;



INSERT INTO funcion_rol (id_funcion,id_rol)
SELECT id,119 FROM funcion;


/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;