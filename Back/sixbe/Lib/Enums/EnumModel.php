<?php

class EnumModel
{	
	
		 const  Comprobante = "Comprobante"; //por defecto
	 	 const  ComprobanteItem = "ComprobanteItem";
	 	 const  ComprobanteItemLote = "ComprobanteItemLote";
	 	 const  Producto =  "Producto";
	 	 const  ProductoRetail =  "ProductoRetail";
	 	 const StockProducto = "StockProducto";
	 	 const DatoEmpresaImpuesto= "DatoEmpresaImpuesto";
	 	 const DatoEmpresa = "DatoEmpresa";
	 	 const Impuesto= "Impuesto";
	 	 const PersonaImpuestoAlicuota= "PersonaImpuestoAlicuota";
	 	 const TipoComprobante= "TipoComprobante";
	 	 const ArticuloRelacion= "ArticuloRelacion";
	 	 const Lote= "Lote";
	 	 const AlarmaUsuario= "AlarmaUsuario";
	 	 const Alarma = "Alarma";
	 	 const Usuario = "Usuario";
	 	 const Persona = "Persona";
	 	 const Moneda = "Moneda";
	 	 const Categoria = "Categoria";
	 	 const ListaPrecio = "ListaPrecio";
	 	 const ListaPrecioProducto = "ListaPrecioProducto";
	 	 const Melibre = "Melibre";
	 	 const ComprobanteValor = "ComprobanteValor";
	 	 const ReporteTablero = "ReporteTablero";
	 	 const ReporteTableroUsuario = "ReporteTableroUsuario";
	 	 const Marca = "Marca";
	 	 const Provincia = "Provincia";
	 	 const PersonaFigura = "PersonaFigura";
	 	 const Articulo = "Articulo";
	 	 const Modulo = "Modulo";
	 	 const DepositoMovimientoTipo= "DepositoMovimientoTipo";
	 	 const ProductoTipo= "ProductoTipo";
	 	 const EjercicioContable= "EjercicioContable";
	 	 const ComprobantePuntoVentaNumero = "ComprobantePuntoVentaNumero";
	 	 const Entidad = "Entidad";
	 	 const PersonaOrigen = "PersonaOrigen";
	 	 const EstadoTipoComprobante = "EstadoTipoComprobante";
	 	 const TipoAlicuotaAfip = "TipoAlicuotaAfip";
	 	 const Deposito = "Deposito";
	 	 const Iva = "Iva";
	 	 const FuncionRol = "FuncionRol";
	 	 const RequestLog = "RequestLog";
	 	 const FacturaItem = "FacturaItem";
	 	 const MailToSend = "MailToSend";
	 	 const RRHHActividad = "RRHHActividad";
	 	 const RRHHCategoria = "RRHHCategoria";
	 	 const RRHHClaseLiquidacion = "RRHHClaseLiquidacion";
	 	 const RRHHConcepto = "RRHHConcepto";
	 	 const RRHHCondicionSicoss = "RRHHCondicionSicoss";
	 
	 	 
		/*
		 "AgrupacionAsiento",
		 "AppModel",
		 "Area",
		 "Articulo",
		 "ArticuloRelacion",
		 "Asiento",
		 "AsientoCuentaContable",
		 "AsientoCuentaContableCentroCostoItem",
		 "AsientoEfecto",
		 "AsientoModelo",
		 "AsientoModeloCuentaContable",
		 "AudiAtributo",
		 "AudiEntidad",
		 "Auditoria",
		 "AuditoriaComprobante",
		 "AuditoriaFormularios",
		 "Banco",
		 "BancoSucursal",
		 "BienUso",
		 "CakeSessions",
		 "CarteraRendicion",
		 "Categoria",
		 "CentroCosto",
		 "Cheque",
		 "Chequera",
		 "CierreContable",
		 "ClaseAsiento",
		 "ClaseCuentaContable",
		 "Color",
		 "Componente",
		 "Comprobante",
		 "ComprobanteCarteraRendicion",
		 "ComprobanteDigital",
		 "ComprobanteImpuesto",
		 "ComprobanteItem",
		 "ComprobanteItemComprobante",
		 "ComprobanteItemOrigen",
		 "ComprobanteLote",
		 "ComprobanteLoteEjecucion",
		 "ComprobanteLoteItem",
		 "ComprobantePuntoVentaNumero",
		 "ComprobanteReferencia",
		 "ComprobanteReferenciaImpuesto",
		 "ComprobanteValor",
		 "ConfiguracionAplicacion",
		 "Contador",
		 "Controlador",
		 "CsvImportAsiento",
		 "CsvImportConciliacion",
		 "CuentaBancaria",
		 "CuentaContable",
		 "CuentaContableAdd",
		 "CuentaContableCentroCosto",
		 "CuentaContableCompra",
		 "CuentaContableVenta",
		 "CuentaCorrienteCliente",
		 "CuentaCorrienteProveedor",
		 "DatoEmpresa",
		 "DatoEmpresaImpuesto",
		 "Deposito",
		 "DepositoCategoria",
		 "DepositoDestino",
		 "DepositoMovimientoTipo",
		 "DepositoOrigen",
		 "DescuentoFamiliaPersona",
		 "DestinoProducto",
		 "DetalleTipoComprobante",
		 "EjercicioContable",
		 "Equipo",
		 "Estado",
		 "EstadoAsiento",
		 "EstadoCheque",
		 "EstadoCheque-Copia",
		 "EstadoChequera",
		 "EstadoComprobante",
		 "EstadoPersona",
		 "EstadoTipoCheque",
		 "EstadoTipoComprobante",
		 "FacturaProveedor",
		 "FamiliaProducto",
		 "Filtro",
		 "CondicionPago",
		 "Funcion",
		 "GastoArticulo",
		 "HistoricoLogin",
		 "Impuesto",
		 "ImpuestoGeografia",
		 "InsumoInterno",
		 "Iva",
		 "ListaPrecio",
		 "ListaPrecioProducto",
		 "Lote",
		 "Marca",
		 "MateriaPrima",
		 "MateriaPrimaDeposito",
		 "MediaFile",
		 "MediaFileTipo",
		 "MimeType",
		 "Modulo",
		 "Moneda",
		 "Movimiento",
		 "MovimientoItem",
		 "MovimientoTipo",
		 "NivelAutorizadoInformacion",
		 "NotaUsuario",
		 "OrdenArmado",
		 "OrdenArmadoItem",
		 "OrdenPago",
		 "OrdenPagoConcepto",
		 "OrdenPagoRecepcionItem",
		 "OrdenProduccion",
		 "Origen",
		 "Pais",
		 "PeriodoContable",
		 "PeriodoImpositivo",
		 "Persona",
		 "PersonaCategoria",
		 "PersonaFigura",
		 "PersonaImpuestoAlicuota",
		 "PersonaImpuestoAlicuotaTemporal",
		 "PersonaImpuestoAlicuotaTemporalCaba",
		 "PersonaValorTipoComprobante",
		,
		 "ProductoClasificacion",
		 "ProductoRetail",
		 "ProductoTipo",
		 "Provincia",
		 "PuntoVenta",
		 "QaAccionCorrectiva",
		 "QaCertificadoCalidad",
		 "QaCertificadoCalidadItem",
		 "QaConformidad",
		 "QaOrigenFalla",
		 "RecepcionMaterial",
		 "RecepcionMaterialItem",
		 "Reclamo",
		 "Remito",
		 "Reporte",
		 "Rol",
		 "Scrap",
		 "Servicio",
		 "Sistema",
		 "SistemaParametro",
		 "SituacionIb",
		 "StockArticulo",
		 "StockComponente",
		 "StockMateriaPrima",
		 "StockProducto",
		 "SubTipoImpuesto",
		 "Sucursal",
		 "Talle",
		 "TesoreriaClaseTransaccion",
		 "TipoActividad",
		 "TipoArticulo",
		 "TipoAsiento",
		 "TipoAsientoLeyenda",
		 "TipoCarteraRendicion",
		 "TipoCheque",
		 "TipoComercializacion",
		 "TipoComprobante",
		 "TipoComprobanteImpuesto",
		 "TipoComprobantePersona",
		 "TipoComprobantePuntoVentaNumero",
		 "TipoCuentaBancaria",
		 "TipoCuentaContable",
		 "TipoDocumento",
		 "TipoEmpresa",
		 "TipoImpuesto",
		 "TipoIva",
		 "TipoListaPrecio",
		 "TipoLote",
		 "TipoPersona",
		 "TipoReclamo",
		 "Transportista",
		 "TurCotizacion",
		 "TurCotizacionClienteAsociado",
		 "TurCotizacionConcepto",
		 "TurCotizacionItem",
		 "Unidad",
		 "UnidadAfip",
		 "UnidadCalculo",
		 "Usuario",
		 "Valor",
		 "WordpressAppModel",
		 "WordpressComment",
		 "WordpressPost",
		 "WordpressPostmetum",
		 "WordpressTermRelationship",
		 "WordpressUser",
		 "WordpressUsermetum",
		 "WsAutenticacion"*/
		
	}
  
?>