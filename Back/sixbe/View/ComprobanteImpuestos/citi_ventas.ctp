<?php


    App::import('Model','Comprobante'); 
    App::import('Model','DatoEmpresa'); 

    if($nombre_archivo  == "")
        $nombre_archivo = "CitiVentas";
        
        
    header("Content-Disposition: attachment; filename=".$nombre_archivo.".txt");
    header("Pragma: no-cache"); 
    header("Expires: 0");
    
    $comprobante_obj  = new Comprobante();
    $dato_empresa_obj  = new DatoEmpresa();
    
    $id_tipo_moneda = $dato_empresa_obj->getNombreCampoMonedaPorIdMoneda(EnumMoneda::Peso);
    $field_valor_moneda = $comprobante_obj->getFieldByTipoMoneda($id_tipo_moneda);
    
    
    foreach($datos as $comprobante){
        
        
       $fecha = new DateTime($comprobante["Comprobante"]['fecha_contable']); 
       $fecha_comprobante_01 =$fecha->format('Ymd'); 
       $tipo_comprobante_afip_02 = str_pad($comprobante["TipoComprobante"]["codigo_afip"], 3, '0', STR_PAD_LEFT);
       $punto_venta_03 = str_pad($comprobante["PuntoVenta"]["numero"], 5, '0', STR_PAD_LEFT);
       $nro_comprobante_04 = str_pad($comprobante["Comprobante"]["nro_comprobante"], 20, '0', STR_PAD_LEFT);
       $nro_comprobante_hasta_05 = str_pad($comprobante["Comprobante"]["nro_comprobante"], 20, '0', STR_PAD_LEFT);
       $codigo_doc_afip_06 = str_pad($comprobante["Persona"]["TipoDocumento"]["abreviacion_afip"], 2, '0', STR_PAD_LEFT);
       $cuit_final_07 =  str_pad($comprobante["Persona"]["cuit"], 20, '0', STR_PAD_LEFT);
       
       $razon_social = utf8_decode(trim($comprobante["Persona"]["razon_social"]));
       
       if(strlen($razon_social)>30)
        $razon_social = substr(trim($razon_social),0,30);
       
       $razon_social_08 =  str_pad(trim($razon_social), 30, ' ', STR_PAD_RIGHT);
       
       
       $total_comprobante = $comprobante_obj->ExpresarEnTipoMonedaMejorado($comprobante["Comprobante"]["total_comprobante"],$field_valor_moneda, $comprobante["Comprobante"]);
       $importe_total_operacion_09 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_comprobante,13,2,"0");
       
       
       /*Esto es los items que tienen iva 0%*/
       
       $percepcion_no_categorizados_11 = "000000000000000";
       $id_comprobante = $comprobante["Comprobante"]['id'];
       
       
       
           
       $importe_total_concepto_no_integran_neto_gravado_010 = $comprobante_obj->getNoGravado($id_comprobante);
       $no_gravado = $importe_total_concepto_no_integran_neto_gravado_010;
       $importe_total_concepto_no_integran_neto_gravado_010 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($importe_total_concepto_no_integran_neto_gravado_010,$field_valor_moneda, $comprobante["Comprobante"]);
       $importe_total_concepto_no_integran_neto_gravado_010 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($importe_total_concepto_no_integran_neto_gravado_010,13,2,"0");  
         
     
       //le mando solo los items que tenga "EXENTO" en el iva, esto es en caso de la FC Exportacion, tanto franca como internacional.    
       $exento = $comprobante_obj->getExento($id_comprobante);
       $importe_operaciones_exento_12 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($exento,$field_valor_moneda, $comprobante["Comprobante"]);   
       $importe_operaciones_exento_12 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($importe_operaciones_exento_12,13,2,"0");  
       
           
 
        
        
       $total_impuestos_nacionales_13   = $comprobante_obj->getTotalPorImpuestoGeografia($comprobante["Comprobante"]["id"],EnumImpuestoGeografia::ImpuestoNacional,0,EnumTipoImpuesto::PERCEPCIONES);
       $total_impuestos_nacionales_13 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($total_impuestos_nacionales_13,$field_valor_moneda, $comprobante["Comprobante"]);   
       $total_impuestos_nacionales_13   = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_impuestos_nacionales_13,13,2,"0");
       
       
       $total_impuestos_provinciales_14 = $comprobante_obj->getTotalPorImpuestoGeografia($comprobante["Comprobante"]["id"],EnumImpuestoGeografia::ImpuestoProvincial,0,EnumTipoImpuesto::PERCEPCIONES);
       $total_impuestos_provinciales_14 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($total_impuestos_provinciales_14,$field_valor_moneda, $comprobante["Comprobante"]);   
       $total_impuestos_provinciales_14 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_impuestos_provinciales_14,13,2,"0");
       
       $total_impuestos_municipales_15  = $comprobante_obj->getTotalPorImpuestoGeografia($comprobante["Comprobante"]["id"],EnumImpuestoGeografia::ImpuestoMunicipal,0,EnumTipoImpuesto::PERCEPCIONES);
       $total_impuestos_municipales_15 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($total_impuestos_municipales_15,$field_valor_moneda, $comprobante["Comprobante"]);   
       $total_impuestos_municipales_15  = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_impuestos_municipales_15,13,2,"0");
       
       $total_impuestos_interno_16      = $comprobante_obj->getTotalPorImpuestoGeografia($comprobante["Comprobante"]["id"],EnumImpuestoGeografia::ImpuestoInterno,0,EnumTipoImpuesto::PERCEPCIONES);  
       $total_impuestos_interno_16 = $comprobante_obj->ExpresarEnTipoMonedaMejorado($total_impuestos_interno_16,$field_valor_moneda, $comprobante["Comprobante"]);   
       $total_impuestos_interno_16      = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($total_impuestos_interno_16,13,2,"0");
       
       $codigo_moneda_17 =  str_pad('PES',3,' ',STR_PAD_RIGHT);
       $tipo_cambio = 1 ;
       $tipo_cambio_18 = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($tipo_cambio,4,6,"0"); 
       $cantidad_alicuota_iva_19 = $comprobante_obj->getCantidadIvas($comprobante["Comprobante"]["id"],$comprobante["TipoComprobante"]["id_sistema"],1,0,1,0);
       
       
       if($cantidad_alicuota_iva_19 == 0)
        $cantidad_alicuota_iva_19 = 1;//esto por regla de AFIP
        
        
        
       $codigo_operacion_20 = $comprobante_obj->getCodigoOperacionAfip($comprobante["Comprobante"]["id"],$no_gravado,$exento);
       $otros_tributos_21 = "000000000000000";
       
       $fecha_vencimiento_pago_22 = "00000000";
       
       
       
       
       $linea_texto = $fecha_comprobante_01.$tipo_comprobante_afip_02.$punto_venta_03.$nro_comprobante_04.$nro_comprobante_hasta_05.$codigo_doc_afip_06.$cuit_final_07.$razon_social_08.$importe_total_operacion_09.$importe_total_concepto_no_integran_neto_gravado_010.$percepcion_no_categorizados_11.$importe_operaciones_exento_12.$total_impuestos_nacionales_13.$total_impuestos_provinciales_14.$total_impuestos_municipales_15.$total_impuestos_interno_16.$codigo_moneda_17.$tipo_cambio_18.$cantidad_alicuota_iva_19.$codigo_operacion_20.$otros_tributos_21.$fecha_vencimiento_pago_22;
       echo $linea_texto. "\r\n";       
    }
?>