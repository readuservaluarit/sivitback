<?php


class MailSenderController extends AppController {
	public $name = 'MailSender';
	public $model = 'MailToSend';
	public $helpers = array ('Session', 'Paginator', 'Js');
	public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
	
	
	public function sendMailsComprobantes() {
		
		
		$_SERVER['REMOTE_ADDR'];//debe estar vacia. Ya que esta vacua
		
		
		$this->loadModel($this->model);
		$this->loadModel("Comprobante");
		$this->loadModel(EnumModel::DatoEmpresa);
		
		
		$mails_a_enviar = $this->MailToSend->find('all', array(
				'conditions' => array('MailToSend.enviado'=>0),
				'contain' => array("Comprobante"=>array("TipoComprobante","Persona","Usuario","PuntoVenta", 'ComprobanteImpuesto'=>array('Impuesto','ComprobanteReferencia'=>array('TipoComprobante','PuntoVenta','Persona'),'Comprobante'=>array("TipoComprobante"))))
		));
		
		
		
		
		
		if(count($mails_a_enviar)>0){
			
			//$datoEmpresa = $this->Session->read('Empresa');
			
			$datoEmpresa = $this->DatoEmpresa->find('first', array(
					'conditions' => array('DatoEmpresa.id' => 1),
					'contain' =>array()
			));
			
			
			
			$max_intentos_fallidos = $datoEmpresa["DatoEmpresa"]["email_intentos_fallidos"];
			
			
			App::import('Vendor', '', array('file' => 'PHPMailer-master/PHPMailerAutoload.php'));//imp
			
			foreach($mails_a_enviar as $mail){
				
				
				
				$mail_to_send = new PHPMailer;
				$mail_to_send->isSMTP();
				
				//	$mail_to_send->SMTPDebug = 3; //Alternative to above constant
				
				
				$comprobante = new Comprobante();
				
				
				$mail_to_send->Host = $datoEmpresa["DatoEmpresa"]["email_smtp_host"];
				$mail_to_send->Port = $datoEmpresa["DatoEmpresa"]["email_smtp_port"];
				$mail_to_send->SMTPSecure = $datoEmpresa["DatoEmpresa"]["email_smtp_secure"];
				
				
				
				if($datoEmpresa["DatoEmpresa"]["email_smtp_auth"] == 1)
					$auth = true;
					else
						$auth = false;
						
						
						$mail_to_send->SMTPAuth = $auth;
						
						
						$mail_to_send->Username = $datoEmpresa["DatoEmpresa"]["email_envio"];
						$mail_to_send->Password = $datoEmpresa["DatoEmpresa"]["email_password_envio"];
						
						try{
							
							
							
							$mail_to = $mail["Comprobante"]["Persona"]["email"];
							
							
							if($mail["Comprobante"]["TipoComprobante"]["usa_mail_usuario_envio"] == 1){//si se usa el mail del usuario que creo el comprobante hay que buscarlo en la tabla Usuario
								
								$mail_to_send->Username = $mail["Comprobante"]["Usuario"]["email"];
								$mail_to_send->Password = $mail["Comprobante"]["Usuario"]["email_password"];
								
								$mail_to_send->setFrom($mail_to_send->Username,$datoEmpresa["DatoEmpresa"]["razon_social"]." - ".$mail["Comprobante"]["Usuario"]["nombre"]);
								
								
								
							}else{
								
								$mail_to_send->setFrom($mail_to_send->Username,$datoEmpresa["DatoEmpresa"]["razon_social"]." - ".$datoEmpresa["DatoEmpresa"]["email_set_from"]);
								
							}
							
							
							$mail_to_send->addAddress($mail_to, $mail["Comprobante"]["Persona"]["razon_social"]);
							
							
							
							
							
							
							
							
							//$mail_to_send->Subject = (string) $this->Comprobante->GetNumberComprobante($mail["Comprobante"]['PuntoVenta']['numero'],$mail["Comprobante"]['nro_comprobante']);
							$mail_to_send->Subject = (string)  $mail["Comprobante"]["TipoComprobante"]["codigo_tipo_comprobante"].' '.$this->Comprobante->GetNumberComprobante(1,$mail["Comprobante"]['nro_comprobante']);
							
							
							$mail_to_send->msgHTML($mail["Comprobante"]["TipoComprobante"]["html_mail"]);
							
							$mail_to_send->AltBody = strip_tags($mail["Comprobante"]["TipoComprobante"]["html_mail"]);
							
							
							if($mail["Comprobante"]["TipoComprobante"]["adjunta_pdf_mail"] == 1){//genero el pdf y lo adjunto
								
								
								
								$mail["TipoComprobante"] = $mail["Comprobante"]["TipoComprobante"];//adapto los datos para que la funcion getNamePdf funcione
								$mail["Persona"] = $mail["Comprobante"]["Persona"];//adapto los datos para que la funcion getNamePdf funcione
								
								$mail["Comprobante"]["nro_comprobante_completo"] = $comprobante->GetNumberComprobante($mail["Comprobante"]["PuntoVenta"]["numero"],$mail["Comprobante"]["nro_comprobante"]);
								
								$pdf_name = $comprobante->getNamePDF($mail);
								$root_pdf = $comprobante->getRootPDF();
								
								
								$path_para_pdf = $datoEmpresa["DatoEmpresa"]["local_path"].$root_pdf;
								
								
								//si es una OP debe adjuntar las retenciones tambien
								$mail_to_send->addAttachment($path_para_pdf.$pdf_name.'.pdf');
								
								if(	$mail["Comprobante"]["TipoComprobante"]["id"] == EnumTipoComprobante::OrdenPagoManual ||
										
										$mail["Comprobante"]["TipoComprobante"]["id"] == EnumTipoComprobante::OrdenPagoAutomatica){
											
											foreach($mail["Comprobante"]["ComprobanteImpuesto"] as $impuesto){
												
												$dato["ComprobanteReferencia"] = $impuesto["ComprobanteReferencia"];
												$dato["ComprobanteReferencia"]["nro_comprobante_completo"] = $comprobante->GetNumberComprobante($impuesto["ComprobanteReferencia"]["PuntoVenta"]["numero"],$impuesto["ComprobanteReferencia"]["nro_comprobante"]);
												$dato["Comprobante"] = $impuesto["Comprobante"];
												$dato["Comprobante"]["nro_comprobante_completo"] = $mail["Comprobante"]["nro_comprobante_completo"];
												$dato["Impuesto"] = $impuesto["Impuesto"];
												
												$pdf_retencion_name = $comprobante->getNamePDFRetencion($dato);
												
												$mail_to_send->addAttachment($path_para_pdf.$pdf_retencion_name.'.pdf');
												
											}
											
											
											
								}
								
								
							}
							
							
							
							
							
							//send the message, check for errors
							if (!$mail_to_send->send()) {
								
								
								
								if($max_intentos_fallidos+1 == $mail["MailToSend"]["intento_fallidos"]){
									
									
									//$this->MailToSend->id = $mail["MailToSend"]["id"];
									$this->MailToSend->updateAll(array("MailToSend.enviado" => "2"),
											array('MailToSend.id' => $mail["MailToSend"]["id"]) );
								}else{
									
									
									
									$this->MailToSend->updateAll(array("MailToSend.intento_fallidos" => "MailToSend.intento_fallidos+1","MailToSend.error_log"=>"'".$mail_to_send->ErrorInfo."'"),
											array('MailToSend.id' => $mail["MailToSend"]["id"]) );
								}
								
								
								
								
								
								
							}else{
								
								
								$this->Comprobante->updateAll(array("Comprobante.mail_enviado" => 1),
										array('Comprobante.id' => $mail["Comprobante"]["id"]) );
								
								$this->MailToSend->id = $mail["MailToSend"]["id"];
								$this->MailToSend->delete();
							}
							
							
							
							
						}catch (Exception $e){//falla siempre lo borro del proceso ya que algo esta mal
							
							echo "Mailer Error: " . $mail_to_send->ErrorInfo;
							
							$this->MailToSend->updateAll(array("MailToSend.intento_fallidos" => "MailToSend.intento_fallidos+1","MailToSend.error_log"=> "'".$mail_to_send->ErrorInfo."'"),
									array('MailToSend.id' => $mail["MailToSend"]["id"]) );
							
							
							if($max_intentos_fallidos+1 == $mail["MailToSend"]["intento_fallidos"]){
								
								$this->MailToSend->id = $mail["MailToSend"]["id"];
								$this->MailToSend->delete();
							}
							
							
						}
						
						
						$mail_to_send->ClearAllRecipients();
						$mail_to_send->ClearAttachments();   //Remove all attachements
						
			}//fin for
			
			
			//echo "Mailer Error: " . $mail_to_send->ErrorInfo;
			die();
		}
		
	}
	
	
	public function beforeFilter(){
		
		App::uses('ConnectionManager', 'Model');
		
		
		
		
		
		
		$this->Auth->allow('sendMailsComprobantes');
		
		parent::beforeFilter();
		
		
		
		
		
	}
	
	
	
	
	public function SivitPersona($mail_to,$html){
		
		$this->loadModel(EnumModel::DatoEmpresa);
		$this->loadModel(EnumModel::Persona);
		$this->loadModel($this->model);
		
		
		
		$usuario = $this->getFromRequestOrSession('Usuario.username');
		$password = $this->getFromRequestOrSession('Usuario.password');
		$id_persona = $this->getFromRequestOrSession('Usuario.id_persona');
		
		$persona = $this->Persona->find('first', array(
				'conditions' => array('Persona.id' => $id_persona),
				'contain' =>array("Usuario")
		));
		
		
		
		//controlar que tenga cargada la url de sivit web url_sivit en datoempresa
		
		
		
		$datoEmpresa = $this->DatoEmpresa->find('first', array(
				'conditions' => array('DatoEmpresa.id' => 1),
				'contain' =>array()
		));
		
		
		$html = "Bienvenido a SIVIT WEB.</br>".$datoEmpresa["DatoEmpresa"]["razon_social"]." le da la bienvenida al portal en el cual podra ver su cuenta corriente, descargar comprobantes y muchas cosas mas.
				Mediante el siguiente enlace podr� acceder, recuerde su usuario y constase�a y jamas los de a conocer publicamente.</br>. Haciendo click en este link podra ingresar:<a href='".$datoEmpresa["DatoEmpresa"]["url_sivit"]."'>".$datoEmpresa["DatoEmpresa"]["url_sivit"]."</a>
				El usuario es:".$usuario." y la constase�a es:".$password.". Una vez ingresado cambie la constrase�a mediante el apartado Mis Datos.Gracias";
		
		
		
		
		
		
		
		$this->{EnumModel::MailToSend}->usa_mail_defecto = 1;
		
		$this->{EnumModel::MailToSend}->html = $html;
		
		$this->{EnumModel::MailToSend}->mail_to = $usuario;
		
		$this->{EnumModel::MailToSend}->Subject = "Bienvenido a Sivit Web Clientes";
		
		$this->{EnumModel::MailToSend}->to_person_name = $persona["Persona"]["razon_social"];
		
		$this->{EnumModel::MailToSend}->sendMail();
		
		
		
		
		
		
		$output = array(
				"status" => $mail_to_send->error,
				"message" => $mail_to_send->message_error,
				"content" => "",
				"page_count" =>0,
				
		);
		$this->set($output);
		$this->set("_serialize", array("status", "message","page_count", "content"));
		
		
		
	}
	
	public function sendMail($html){
		
		
		//$datoEmpresa = $this->Session->read('Empresa');
		$this->loadModel(EnumModel::MailToSend);
		
		$datoEmpresa = $this->DatoEmpresa->find('first', array(
				'conditions' => array('DatoEmpresa.id' => 1),
				'contain' =>array()
		));
		
		
		
		
		
		
		
		
		
		
		
		$this->{EnumModel::MailToSend}->usa_mail_defecto = 1;
		
		$this->{EnumModel::MailToSend}->html = "Envio de e-mail de prueba desde Sivit";
		
		$this->{EnumModel::MailToSend}->mail_to = $usuario;
		
		$this->{EnumModel::MailToSend}->Subject = "Envio de prueba Sivit";
		
		$this->{EnumModel::MailToSend}->to_person_name = "Prueba";
		
		$this->{EnumModel::MailToSend}->sendMail();
		
		try {
			$mail_to_send->sendMail();
			$error = EnumError::SUCCESS;
			$message = "El mensaje fue enviado correctamente";
		}catch (Exception  $e){
			
			$error = EnumError::ERROR;
			$message = "ERROR: ".$e->getMessage();
			
			
		}
		
		
		
		
		
		
		
	}
	
	
	
	public function envioPrueba(){
		
		$this->loadModel(EnumModel::DatoEmpresa);
		$this->loadModel($this->model);
		
		$email_test_destino = $this->getFromRequestOrSession('Usuario.email_test_destino');
		
		
		//$datoEmpresa = $this->Session->read('Empresa');
		
		$datoEmpresa = $this->DatoEmpresa->find('first', array(
				'conditions' => array('DatoEmpresa.id' => 1),
				'contain' =>array()
		));
		
		
		
		
		
		
		
		
		
		$this->{EnumModel::MailToSend}->usa_mail_defecto = 1;
		
		$this->{EnumModel::MailToSend}->html = "Envio de e-mail de prueba desde Sivit";
		
		$this->{EnumModel::MailToSend}->mail_to = $email_test_destino;
		
		$this->{EnumModel::MailToSend}->Subject = "Envio de prueba Sivit";
		
		$this->{EnumModel::MailToSend}->to_person_name = "Test";
		
		$this->{EnumModel::MailToSend}->sendMail();
		
		
		$output = array(
				"status" => $mail_to_send->error,
				"message" => $mail_to_send->message_error,
				"content" => "",
				"page_count" =>0,
				
		);
		echo json_encode($output);
		die();
		
		
		
		
		
		
		
	}
	
	
	public function avisoComprobanteImpago(){
		
		
		
	}
	
	
	
	
	
	
}
?>