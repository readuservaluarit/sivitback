<?php
    class AsientosModeloCuentaContableController extends AppController {
        public $name = 'AsientosModeloCuentaContable';
        public $model = 'AsientoModeloCuentaContable';
        public $helpers = array ('Session', 'Paginator', 'Js');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

        private function RecuperaFiltros()
        {
            $conditions = array(); 

             $id_asiento_modelo = strtolower($this->getFromRequestOrSession('AsientoModeloCuentaContable.id_asiento_modelo'));
            
            if ($id_asiento_modelo != "") 
                 array_push($conditions, array('AsientoModeloCuentaContable.id_asiento_modelo =' => $id_asiento_modelo));

            // if ($id_cuenta_contable != "") 
                // array_push($conditions, array('AsientoModeloCuentaContable.id_cuenta_contable =' => $id_cuenta_contable));

            return $conditions; 
        }

        /**
        * @secured(CONSULTA_ASIENTO)
        */
        public function index() 
        {
            if($this->request->is('ajax'))
                $this->layout = 'ajax';

            $this->loadModel("AsientoModeloCuentaContable");
            //$this->PaginatorModificado->settings = array('limit' => 100, 'update' => 'main-content', 'evalScripts' => true);

            //Recuperacion de Filtros
            $conditions = $this->RecuperaFiltros();

            /*
            $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
            );*/
            if($this->RequestHandler->ext != 'json'){  
                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();

                $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));

                //Headers
                $formBuilder->addHeader('id', 'cotizacion.id', "10%");
                $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
                $formBuilder->addHeader('CUIT', 'cotizacion.cuit', "20%");
                $formBuilder->addHeader('Email', 'cliente.email', "30%");
                $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'razon_social');
                $formBuilder->addField($this->model, 'cuit');
                $formBuilder->addField($this->model, 'email');
                $formBuilder->addField($this->model, 'tel');

                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');

                //vista formBuilder
            }
            else
            { // vista json

                $data = $this->AsientoModeloCuentaContable->find('all', array(
                        'conditions' => $conditions,
                        'contain' => array("AsientoModelo","CuentaContable"),
                        'order'=>array("AsientoModeloCuentaContable.id asc")
                    ));
     
                $page_count = 0; //$this->params['paging'][$this->model]['pageCount'];


                foreach($data as &$record)
                {       
                    //Toda la logica de mapeo se basan en el model. por eso se subu un nivel a [AsientoModeloCuentaContable ]
                    $record["AsientoModeloCuentaContable"]["d_asiento"] =             $record["AsientoModelo"]["d_asiento"];
                    $record["AsientoModeloCuentaContable"]["codigo_cuenta_contable"] =                $record["CuentaContable"]["codigo"];
                    
                    $record["AsientoModeloCuentaContable"]["d_cuenta_contable"] =  $record["CuentaContable"]["d_cuenta_contable"];
     
                    
                    $record["AsientoModeloCuentaContable"]["nro_asiento"] =   $record["AsientoModelo"]["codigo"];
                    $record["AsientoModeloCuentaContable"]["fecha_asiento"] = $record["AsientoModelo"]["fecha"];
                                            
                    //Subir un nivel datos propios del Asiento (cabecera);
                    $record["AsientoModeloCuentaContable"]["id_tipo_asiento"] =       $record["AsientoModelo"]["id_tipo_asiento"];
                    $record["AsientoModeloCuentaContable"]["id_clase_asiento"] =       $record["AsientoModelo"]["id_clase_asiento"];

                    $record["AsientoModeloCuentaContable"]["debe_cuenta"] =    "";
                    $record["AsientoModeloCuentaContable"]["debe_concepto"] =  "";
                    
					
				
                    $record["AsientoModeloCuentaContable"]["haber_cuenta"] =    "";
                    $record["AsientoModeloCuentaContable"]["haber_concepto"] =  "";
                    

                    if($record["AsientoModeloCuentaContable"]["es_debe"])
                    {
                        $record["AsientoModeloCuentaContable"]["debe_cuenta"] =     $record["CuentaContable"]["codigo"];
                        $record["AsientoModeloCuentaContable"]["debe_concepto"] =   $record["CuentaContable"]["d_cuenta_contable"];
                    }
                    else
                    {
                        $record["AsientoModeloCuentaContable"]["haber_cuenta"] =    $record["CuentaContable"]["codigo"];
                        $record["AsientoModeloCuentaContable"]["haber_concepto"] =  $record["CuentaContable"]["d_cuenta_contable"];
                    }

                    $record["AsientoModeloCuentaContable"]["es_debe"] =  (string)((int)($record["AsientoModeloCuentaContable"]["es_debe"]));
                    unset($record["AsientoModelo"]);
                    unset($record["CentroCosto"]);
                    //unset($record["CuentaContable"]);
                }       

                $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "list",
                    "content" => $data,
                    "page_count" =>$page_count
                );
                $this->set($output);
                $this->set("_serialize", array("status", "message","page_count", "content"));
            }
            //fin vista json
        }
        
        /*probar con el save() All de AsientosModelo y el save all de AsientoModeloCuentaContable*/
        protected function add() {
            if ($this->request->is('post')){
                $this->loadModel($this->model);

                try{
                    if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
                        $mensaje = "El Item ha sido creado exitosamente";
                        $tipo = EnumError::SUCCESS;

                    }else{
                        $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.";
                        $tipo = EnumError::ERROR; 

                    }
                }catch(Exception $e){

                    $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.".$e->getMessage();
                    $tipo = EnumError::ERROR;
                }
                $output = array(
                    "status" => $tipo,
                    "message" => $mensaje,
                    "content" => ""
                );
                //si es json muestro esto
                if($this->RequestHandler->ext == 'json'){ 
                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content"));
                }else{

                    $this->Session->setFlash($mensaje, $tipo);
                    $this->redirect(array('action' => 'index'));
                }     

            }

            //si no es un post y no es json
            if($this->RequestHandler->ext != 'json')
                $this->redirect(array('action' => 'abm', 'A'));   


        }

        protected function edit($id) {


            if (!$this->request->is('get')){

                $this->loadModel($this->model);
                $this->{$this->model}->id = $id;

                try{ 
                    if ($this->{$this->model}->saveAll($this->request->data)){

                        $mensaje = "El Item Ha sido modificado correctamente.";
                        $tipo = EnumError::ERROR;

                    }else{
                        $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.";
                        $tipo = EnumError::ERROR; 

                    }
                }catch(Exception $e){

                    $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.".$e->getMessage();
                    $tipo = EnumError::ERROR; 

                }

            } 
            if($this->RequestHandler->ext == 'json'){  
                $output = array(
                    "status" => $tipo,
                    "message" =>$mensaje,
                    "content" => ""
                ); 

                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('controller' => 'CotizacionItems', 'action' => 'index'));

            } 

        }

        /**
        * @secured(CONSULTA_ASIENTO)
        */ 
        public function getModel($vista = 'default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER

            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }

        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
            $this->set('model',$model);
            // $this->set('this',$this);
            Configure::write('debug',0);
            $this->render($vista);
        }
    }
?>