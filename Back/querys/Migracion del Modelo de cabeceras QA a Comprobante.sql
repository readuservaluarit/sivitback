INSERT INTO comprobante (nro_comprobante,fecha_generacion,id_persona,id_estado_comprobante,definitivo,
genera_movimiento_stock,id_usuario,id_punto_venta,id_tipo_comprobante, observacion)
SELECT id AS numero,fecha,
id_persona,
CASE  
   WHEN id_estado = 1  THEN 1
   WHEN id_estado = 5  THEN 2
   WHEN id_estado = 6  THEN 9
    WHEN id_estado = 9  THEN 116
END AS id_estado_comprobante,
CASE  
   WHEN id_estado = 1  THEN 0
   WHEN id_estado = 5  THEN 1
   WHEN id_estado = 6  THEN 0
    WHEN id_estado = 9  THEN 1
END AS definitivo ,
'0' AS genera_movimiento_stock ,
1 AS id_usuario,3 AS id_punto_venta, 1801 AS id_tipo_comprobante
,
CONCAT( 
'[id OA:', id_orden_armado,   --ESTAS DOS RELACIONES SE PUEDE HACER UN innerjoin con comprobante  OA y con PI para traer el NroComprobante luego incluso un buscador de observacion y listo
'][id PI:', id_pedido_interno,
'][Obs.:',  observaciones, ']') AS observacion 
 FROM qa_certificado_calidad
 WHERE id = 1