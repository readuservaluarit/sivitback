<?php		include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');
             
			$model["id_comprobante"]["show_grid"] = 1;
			$model["id_comprobante"]["header_display"] = "Id. Comprobante";
			$model["id_comprobante"]["order"] = "0";
			$model["id_comprobante"]["width"] = "6";

			$model["pto_nro_comprobante"]["show_grid"] = 1;
			$model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
			$model["pto_nro_comprobante"]["order"] = "1";
			$model["pto_nro_comprobante"]["width"] = "13";

			$model["codigo"]["show_grid"] = 1;
			$model["codigo"]["header_display"] = "[C&oacute;digo]";
			$model["codigo"]["order"] = "2";
			$model["codigo"]["width"] = "10";

			$model["razon_social"]["show_grid"] = 1;
			$model["razon_social"]["header_display"] = "Razon Social";
			$model["razon_social"]["order"] = "3";
			$model["razon_social"]["width"] = "10";

			$model["d_estado_comprobante"]["show_grid"] = 1;
			$model["d_estado_comprobante"]["header_display"] = "Estado";
			$model["d_estado_comprobante"]["order"] = "4";
			$model["d_estado_comprobante"]["width"] = "10";
			$model["d_estado_comprobante"]["text_colour"] = "#880022";//RED ORAGANGE

			$model["item_observacion"]["show_grid"] = 1;
			$model["item_observacion"]["header_display"] = "Item Observ.";
			$model["item_observacion"]["order"] = "5";            
			$model["item_observacion"]["width"] = "20";

			$model["cantidad"]["show_grid"] = 1;                      //cantidad_pendiente_a_facturar
			$model["cantidad"]["header_display"] = "Cantidad O.C";
			$model["cantidad"]["order"] = "6";            
			$model["cantidad"]["width"] = "5";
			$model["cantidad"]["text_colour"] = "#0000FF";//BLUE

			$model["cantidad_pendiente_a_facturar"]["show_grid"] = 1;
			$model["cantidad_pendiente_a_facturar"]["header_display"] = "Cant. Pend FC.";
			$model["cantidad_pendiente_a_facturar"]["order"] = "7";            
			$model["cantidad_pendiente_a_facturar"]["width"] = "5";
			$model["cantidad_pendiente_a_facturar"]["text_colour"] = "#0000FF";//BLUE

			$model["total_facturado"]["show_grid"] = 1;
			$model["total_facturado"]["header_display"] = "Cant. FC";
			$model["total_facturado"]["order"] = "8";
			$model["total_facturado"]["width"] = "5";
			$model["total_facturado"]["text_colour"] = "#00AA00";//GREEN

			$model["total_remitido"]["show_grid"] = 1;
			$model["total_remitido"]["header_display"] = "Cant. RM";
			$model["total_remitido"]["order"] = "9";
			$model["total_remitido"]["width"] = "5";
			$model["total_remitido"]["text_colour"] = "#660099";//RED VIOLET
			
			
			$model["cantidad_recibida_irm"]["show_grid"] = 1;
			$model["cantidad_recibida_irm"]["header_display"] = "Cant. Rec. IRM";
			$model["cantidad_recibida_irm"]["order"] = "10";
			$model["cantidad_recibida_irm"]["width"] = "5";
			$model["cantidad_recibida_irm"]["text_colour"] = "#660099";//RED VIOLET
			
			
			$model["cantidad_pendiente_irm"]["show_grid"] = 1;
			$model["cantidad_pendiente_irm"]["header_display"] = "Cant. Pend. IRM";
			$model["cantidad_pendiente_irm"]["order"] = "11";
			$model["cantidad_pendiente_irm"]["width"] = "5";
			$model["cantidad_pendiente_irm"]["text_colour"] = "#660099";//RED VIOLET
			

			$model["precio_unitario"]["show_grid"] = 1;
			$model["precio_unitario"]["header_display"] = "Precio Unitario";
			$model["precio_unitario"]["order"] = "12";           
			$model["precio_unitario"]["width"] = "5";

			$model["descuento_unitario"]["show_grid"] = 1;
			$model["descuento_unitario"]["header_display"] = "Desc. Unitario";
			$model["descuento_unitario"]["order"] = "13";
			$model["descuento_unitario"]["width"] = "5"; 

			$model["total"]["show_grid"] = 1;
			$model["total"]["header_display"] = "Total";
			$model["total"]["order"] = "14";
			$model["total"]["width"] = "15";
			$model["total"]["text_colour"] = "#005511";//DARK GREEN 

			$model["moneda_simbolo"]["show_grid"] = 1;
			$model["moneda_simbolo"]["header_display"] = "Moneda";
			$model["moneda_simbolo"]["order"] = "15";
			$model["moneda_simbolo"]["width"] = "4";
			$model["moneda_simbolo"]["text_colour"] = "#005511";//DARK GREEN 
			
			
			
			$model["requiere_conformidad"]["show_grid"] = 1;
			$model["requiere_conformidad"]["header_display"] = "Req. Conformidad";
			$model["requiere_conformidad"]["order"] = "16";
			$model["requiere_conformidad"]["width"] = "4";
			$model["requiere_conformidad"]["text_colour"] = "#005511";//DARK GREEN 
			

 $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItem",  /*Importante: Nombre del Model*/
                    "content" => $model
                );
        
          echo json_encode($output);
?>