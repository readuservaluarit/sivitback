
/*************************************************/
/*PRODUCTO  PASAJE DIRECTO TIENE EL MISMO ID */
/*************************************************/
/*Migrar el stock de PRODUCTO  la estructura SIX a VALUARIT_VALMEC (nueva)
Affected rows: 1052
Time: 0.115s*/
UPDATE valuarit_valmec.stock_producto v
JOIN six.stock_producto vv 
ON v.id = vv.id and v.id_deposito = '4' /*ALMACEN*/
SET v.stock = vv.stock 


/*************************************************/
/*MATERIA PRIMA - TRAER ID DE PRODUCTO*/
/*************************************************/
/*Migrar el stock de MATERIA PRIMA  la estructura SIX a VALUARIT_VALMEC(nueva)
Affected rows: 542
Time: 12.183s*/
UPDATE valuarit_valmec.stock_producto v
JOIN six.stock_materia_prima vv 
ON v.id = (select id from valuarit_valmec.producto as P  /*obtener nuevo id producto */
		   where P.id_materia_prima_migracion = vv.id)
and v.id_deposito = '4' /*ALMACEN*/
SET v.stock = vv.stock


/*Migrar el stock de MATERIA PRIMA la estructura SIX a VALUARIT_VALMEC(nueva)*/  
/*Affected rows: 176
Time: 12.082s*/
UPDATE valuarit_valmec.stock_producto v
JOIN six.stock_materia_prima vv 
ON v.id = (select id from valuarit_valmec.producto as P  /*obtener nuevo id producto */
		   where P.id_materia_prima_migracion = vv.id)
and v.id_deposito = '5' /*MECANIZADO*/
SET v.stock = vv.stock_comprometido


/*Migrar el stock de MATERIA PRIMA la estructura SIX a VALUARIT_VALMEC(nueva)
Affected rows: 4
Time: 12.100s*/
UPDATE valuarit_valmec.stock_producto v
JOIN six.stock_materia_prima vv 
ON v.id = (select id from valuarit_valmec.producto as P /*obtener nuevo id producto */
		        where P.id_materia_prima_migracion = vv.id)
and v.id_deposito = '7' /* NO CONFORME*/
SET v.stock = vv.stock_no_conforme



/*************************************************/
/*COMPONENTE - TRAER ID DE PRODUCTO*/
/*************************************************/
/*Migrar el stock de COMPONENTE  la estructura SIX a VALUARIT_VALMEC(nueva) 
Affected rows: 1905
Time: 26.863s*/
UPDATE valuarit_valmec.stock_producto v
JOIN six.stock_componente vv 
ON v.id = (select id from valuarit_valmec.producto as P  /*obtener nuevo id producto */
		   where P.id_componente_migracion = vv.id)
and v.id_deposito = '4' /*ALMACEN*/
SET v.stock = vv.stock

/*Migrar el stock de COMPONENTE la estructura SIX a VALUARIT_VALMEC(nueva)*/  
/*Affected rows: 1071
Time: 26.853s*/
UPDATE valuarit_valmec.stock_producto v
JOIN six.stock_componente vv 
ON v.id = (select id from valuarit_valmec.producto as P  /*obtener nuevo id producto */
		   where P.id_componente_migracion = vv.id)
and v.id_deposito = '6' /*ARMADO*/
SET v.stock = vv.stock_comprometido


/*Migrar el stock de MATERIA PRIMA la estructura SIX a VALUARIT_VALMEC(nueva)
Affected rows: 1
Time: 26.858s
*/
UPDATE valuarit_valmec.stock_producto v
JOIN six.stock_componente vv 
ON v.id = (select id from valuarit_valmec.producto as P /*obtener nuevo id producto */
		        where P.id_componente_migracion = vv.id)
and v.id_deposito = '7' /* NO CONFORME*/
SET v.stock = vv.stock_no_conforme


/*--------------------------------------------------------
LOG 
VIEJO CORRIDO EN EL  VALUARIT_VALMEC_Q
--------------------------------------------------------*/


/*************************************************/
/*PRODUCTO  PASAJE DIRECTO TIENE EL MISMO ID */
/*************************************************/
/*Migrar el stock de PRODUCTO  la estructura SIX a VALUARIT_VALMEC (nueva)*/
UPDATE valuarit_valmec_q.stock_producto v
JOIN six.stock_producto vv 
ON v.id = vv.id and v.id_deposito = '4' /*ALMACEN*/
SET v.stock = vv.stock 


/*************************************************/
/*MATERIA PRIMA - TRAER ID DE PRODUCTO*/
/*************************************************/
/*Migrar el stock de MATERIA PRIMA  la estructura SIX a VALUARIT_VALMEC(nueva)*/
UPDATE valuarit_valmec_q.stock_producto v
JOIN six.stock_materia_prima vv 
ON v.id = (select id from valuarit_valmec_q.producto as P  /*obtener nuevo id producto */
		   where P.id_materia_prima_migracion = vv.id)
and v.id_deposito = '4' /*ALMACEN*/
SET v.stock = vv.stock


/*Migrar el stock de MATERIA PRIMA la estructura SIX a VALUARIT_VALMEC(nueva)*/  
/*Affected rows: 176
Time: 12.130s*/
UPDATE valuarit_valmec_q.stock_producto v
JOIN six.stock_materia_prima vv 
ON v.id = (select id from valuarit_valmec_q.producto as P  /*obtener nuevo id producto */
		   where P.id_materia_prima_migracion = vv.id)
and v.id_deposito = '5' /*MECANIZADO*/
SET v.stock = vv.stock_comprometido


/*Migrar el stock de MATERIA PRIMA la estructura SIX a VALUARIT_VALMEC(nueva)*/
UPDATE valuarit_valmec_q.stock_producto v
JOIN six.stock_materia_prima vv 
ON v.id = (select id from valuarit_valmec_q.producto as P /*obtener nuevo id producto */
		        where P.id_materia_prima_migracion = vv.id)
and v.id_deposito = '7' /* NO CONFORME*/
SET v.stock = vv.stock_no_conforme



/*************************************************/
/*COMPONENTE - TRAER ID DE PRODUCTO*/
/*************************************************/
/*Migrar el stock de COMPONENTE  la estructura SIX a VALUARIT_VALMEC(nueva) 
Affected rows: 1907
Time: 26.857s*/
UPDATE valuarit_valmec_q.stock_producto v
JOIN six.stock_componente vv 
ON v.id = (select id from valuarit_valmec_q.producto as P  /*obtener nuevo id producto */
		   where P.id_componente_migracion = vv.id)
and v.id_deposito = '4' /*ALMACEN*/
SET v.stock = vv.stock

/*Migrar el stock de COMPONENTE la estructura SIX a VALUARIT_VALMEC(nueva)*/  
/*Affected rows: 1075
Time: 26.712s*/
UPDATE valuarit_valmec_q.stock_producto v
JOIN six.stock_componente vv 
ON v.id = (select id from valuarit_valmec_q.producto as P  /*obtener nuevo id producto */
		   where P.id_componente_migracion = vv.id)
and v.id_deposito = '6' /*ARMADO*/
SET v.stock = vv.stock_comprometido


/*Migrar el stock de MATERIA PRIMA la estructura SIX a VALUARIT_VALMEC(nueva)
Affected rows: 1
Time: 26.705s
*/
UPDATE valuarit_valmec_q.stock_producto v
JOIN six.stock_componente vv 
ON v.id = (select id from valuarit_valmec_q.producto as P /*obtener nuevo id producto */
		        where P.id_componente_migracion = vv.id)
and v.id_deposito = '7' /* NO CONFORME*/
SET v.stock = vv.stock_no_conforme