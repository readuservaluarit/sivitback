<?php


class DatoEmpresaImpuestosController extends AppController {
    public $name = 'DatoEmpresaImpuestos';
    public $model = 'DatoEmpresaImpuesto';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    
    
     /**
    * @secured(CONSULTA_DATOSEMPRESA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $id_DATOSEMPRESA = $this->getFromRequestOrSession('DatoEmpresaImpuesto.id_DATOSEMPRESA');
        $id_impuesto = $this->getFromRequestOrSession('DatoEmpresaImpuesto.id_impuesto');        
        $id_tipo_impuesto = $this->getFromRequestOrSession('DatoEmpresaImpuesto.id_tipo_impuesto');        
        $id_sistema_impuesto = $this->getFromRequestOrSession('DatoEmpresaImpuesto.id_sistema_impuesto');        
        
        
        $conditions = array(); 

        if($id_DATOSEMPRESA!="")
            array_push($conditions, array('DatoEmpresaImpuesto.id_DATOSEMPRESA=' =>  $id_DATOSEMPRESA )); 
         
        
        if($id_impuesto!="")
            array_push($conditions, array('DatoEmpresaImpuesto.id_impuesto =' =>  $id_impuesto));
            
        if($id_tipo_impuesto!="")
            array_push($conditions, array('Impuesto.id_tipo_impuesto =' =>  $id_tipo_impuesto));      
        
        
        if($id_sistema_impuesto!="")
            array_push($conditions, array('Impuesto.id_sistema =' =>  $id_sistema_impuesto));        
        
        $this->paginado = 0;
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' => array("Impuesto"=>array('Sistema')),
            'conditions' => $conditions,
            'limit' => 10,
            'page' => $this->getPageNum()
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'DescuentoFamiliaPersona.id', "50%");
        $formBuilder->addHeader('id_cliente', 'DescuentoFamiliaPersona.id_cliente', "20%");
        $formBuilder->addHeader('descuento', 'DescuentoFamiliaPersona.descuento', "30%");
        // $formBuilder->addHeader('Email', 'cliente.email', "30%");
        // $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'descuento');
        $formBuilder->addField($this->model, 'id_cliente');
        // $formBuilder->addField($this->model, 'cuit');
        // $formBuilder->addField($this->model, 'email');
        // $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$dato){
            
            $dato["DatoEmpresaImpuesto"]["d_impuesto"] =  $dato["Impuesto"]["d_impuesto"];
            $dato["DatoEmpresaImpuesto"]["id_sistema"] =  $dato["Impuesto"]["id_sistema"];
            $dato["DatoEmpresaImpuesto"]["d_sistema"] =  $dato["Impuesto"]["Sistema"]["d_sistema"];
            // $dato["PersonaImpuestosAlicuota"]["razon_social"] =  $dato["Persona"]["razon_social"];
            
            if($dato["DatoEmpresaImpuesto"]["sin_vencimiento"] == 1){
            	$dato["DatoEmpresaImpuesto"]["fecha_vigencia_desde"] = "";
            	$dato["DatoEmpresaImpuesto"]["fecha_vigencia_hasta"] = "";
            	$dato["DatoEmpresaImpuesto"]["d_sin_vencimiento"] = "Si";
            	
            }else{
            	
            	$dato["PersonaImpuestoAlicuota"]["d_sin_vencimiento"] = "No";
            }
            
            
            if($dato["DatoEmpresaImpuesto"]["check_mostrar_impuesto_libro_iva"] == 1)
            	$dato["DatoEmpresaImpuesto"]["d_mostrar_libro_iva"] = "Si";
            else
            	$dato["DatoEmpresaImpuesto"]["d_mostrar_libro_iva"] = "No";
            
            
            
            
            
            unset($dato["Impuesto"]);
          
            
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
   

    /**
    * @secured(CONSULTA_DATOSEMPRESA)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_add = '';
            $id_DATOSEMPRESA = $this->getFromRequestOrSession('DatoEmpresaImpuesto.id_dato_empresa');
            
            
            try{
                $this->ProcesarImpuestos($id_DATOSEMPRESA,$this->request->data);
                if ($this->DatoEmpresaImpuesto->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "El Impuesto ha sido creado exitosamente";
                    $tipo = EnumError::SUCCESS;
                    $id_add = $this->DatoEmpresaImpuesto->id;
                   
                }else{
                    $mensaje = "Ha ocurrido un error, el Impuesto no ha podido ser creado.";
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error, el Impuesto NO ha podido ser creado. ".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
            
            
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" =>$id_add
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->loadModel("Impuesto");
            $this->DatoEmpresaImpuesto->id = $id;
            $id_DATOSEMPRESA = $this->getFromRequestOrSession('DatoEmpresaImpuesto.id_dato_empresa');
            
            try{ 
                $this->ProcesarImpuestos($id_DATOSEMPRESA,$this->request->data);
                if ($this->DatoEmpresaImpuesto->saveAll($this->request->data)){
                    if($this->RequestHandler->ext == 'json'){  
                        
                    	$mensaje = "";
                    	
                    	/*
                        $es_global = $this->Impuesto->EsGlobal($this->request->data["DatoEmpresaImpuesto"]["id_impuesto"]);
                        
                     
                    
                        if($es_global == 1)
                            $mensaje = "Recuerde que modifico un impuesto global y se ven afectados los proveedores y los clientes asociados al mismo.";
                        */
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "El Impuesto ha sido modificado exitosamente.".$mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('El Impuesto ha sido modificado exitosamente.', 'success');
                        $this->redirect(array('controller' => 'OrdenCompra', 'action' => 'index'));
                        
                    } 
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, el Impuesto no ha podido modificarse.', 'error');
                
            }
           
        }else{
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->PersonaImpuestosAlicuota->id = $id;
            $this->PersonaImpuestosAlicuota->contain();
            $this->request->data = $this->PersonaImpuestosAlicuota->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
            }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
  
    /**
    * @secured(ABM_USUARIOS, READONLY_PROTECTED)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $this->loadModel("PersonaImpuestoAlicuota");
        $this->loadModel("DatoEmpresa");
        
        $mensaje = "";
        $status = "";
        $this->DatoEmpresaImpuesto->id = $id;
        $impuesto = $this->DatoEmpresa->getDatoEmpresaImpuesto($id);
       try{
           

           
           
            if ($this->DatoEmpresaImpuesto->delete()) {
            	
            	$this->PersonaImpuestoAlicuota->borrarImpuesto($impuesto["DatoEmpresaImpuesto"]["id_impuesto"]);
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Impuesto ha sido eliminado exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception("No es posible eliminar el impuesto ya que tiene registros asociados");
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
    }
    

     // public function getModel( $vista='normal')
     // {    
         // $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
         // $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
         // $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
     // }
    
    
     // private function editforView($model,$vista)
     // {  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
       // $this->set('model',$model);
       // Configure::write('debug',0);
       // $this->render($vista);    
     // }
     
     
     /**
    * @secured(CONSULTA_DATOSEMPRESA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
     private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
 
    }
    
    public function ProcesarImpuestos($id,$data){
         
         $this->loadModel("DatoEmpresa");
         $this->loadModel("PersonaImpuestoAlicuota");
         $this->loadModel("Impuesto");
        
        if(isset($data["DatoEmpresaImpuesto"])){
            
                $impuesto = $data["DatoEmpresaImpuesto"];
                
                $impuesto["id_empresa"] = $id;
             
                
                if(!isset($impuesto["id"])){//sino esta seteado el impuesto
                
                	if( $this->Impuesto->EsGlobal($impuesto["id_impuesto"])==1 ){
                		
                		
                		
                    	
                		$this->PersonaImpuestoAlicuota->agregarGlobal($impuesto["id_impuesto"],$impuesto["id_sistema"]);
                       
                         
                         
                    }
                    
                }
                
                
                
                
            
            
            
        }
        
        
    }
    
    
    
    /*
    
    if(isset($impuesto["id"])){  //si esta seteado el id tengo que ver si modifico la alicuota
    	
    	$DATOSEMPRESA_impuesto = $this->DatoEmpresa->getDatoEmpresaImpuesto($impuesto["id"]);
    	
    	$DATOSEMPRESA_impuesto = $DATOSEMPRESA_impuesto["DatoEmpresaImpuesto"];
    	
    	
    	if( $impuesto["fecha_vigencia_desde"] != $DATOSEMPRESA_impuesto["fecha_vigencia_desde"]  || $impuesto["fecha_vigencia_hasta"] != $DATOSEMPRESA_impuesto["fecha_vigencia_hasta"]  ){
    		/*cambio fecha o porcentaje debo borrar todos los impuestos de las personas si corresponde ese impuesto
    		
    		if( $this->Impuesto->EsGlobal($impuesto["id_impuesto"])==1 ){//solo en estos casos le agrego el impuesto a todas las personas de la BD
    			
    			
    			$this->PersonaImpuestoAlicuota->borrarImpuesto($impuesto["id_impuesto"]);//primero les borro a todas las personas esos impuestos y luego les agrego el global
    			
    			$this->PersonaImpuestoAlicuota->AgregarImpuesto($impuesto,EnumTipoPersona::Proveedor);
    			
    		}
    		
    		
    	}
    	
    }else{
     
    */
   
    
}
?>