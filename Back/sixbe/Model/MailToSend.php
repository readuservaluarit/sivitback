<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class MailToSend extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'mail_to_send';

    public $actsAs = array('Containable');
    
    
    public $usa_mail_defecto = 1;
    
    public $html = "";
    
    public $mail_to = "";
    
    public $Subject = "";
    
    public $to_person_name = "";
    
    public $id_usuario = 0;
    
    
    public $error = EnumError::SUCCESS;
    
    public $message_error = "";
    
    
    

    
    

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
			
     
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 
	
	public $belongsTo = array(
			'Comprobante' => array(
					'className' => 'Comprobante',
					'foreignKey' => 'id_comprobante',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			)
			);
	
	
	
	public function sendMail(){
		
		App::uses(EnumModel::DatoEmpresa, 'Model');
		//$datoEmpresa = $this->Session->read('Empresa');
		$dato_empresa_obj = new DatoEmpresa();
		
		try{
			$datoEmpresa = $dato_empresa_obj->find('first', array(
				'conditions' => array('DatoEmpresa.id' => 1),
				'contain' =>array()
		));
		
		
		App::import('Vendor', '', array('file' => 'PHPMailer-master/PHPMailerAutoload.php'));//imp
		$mail_to_send = new PHPMailer;
		$mail_to_send->isSMTP();
		$mail_to_send->Host = $datoEmpresa["DatoEmpresa"]["email_smtp_host"];
		$mail_to_send->Port = $datoEmpresa["DatoEmpresa"]["email_smtp_port"];
		$mail_to_send->SMTPSecure = $datoEmpresa["DatoEmpresa"]["email_smtp_secure"];
		
		$mail_to_send->setFrom($mail_to_send->Username,$datoEmpresa["DatoEmpresa"]["razon_social"]." - ".$datoEmpresa["DatoEmpresa"]["email_set_from"]);
		$mail_to_send->addAddress($this->mail_to, $this->to_person_name);
		$mail_to_send->Subject = (string) $this->Subject;
		$mail_to_send->msgHTML($this->html);
		
		$mail_to_send->AltBody = strip_tags($this->html);
		
		
		if($datoEmpresa["DatoEmpresa"]["email_smtp_auth"] == 1)
			$auth = true;
		else
			$auth = false;
		
		$mail_to_send->SMTPAuth = $auth;
		
		
		
		if($this->usa_mail_defecto  == 0){
			
			
			if($this->id_usuario = 0)
				throw new Exception("Se debe indicar el id_usuario");
				
			App::uses(EnumModel::Usuario, 'Model');
			$usuario_obj = new Usuario();
			$dato_usuario = $usuario_obj->getUsuario($this->id_usuario);
			$mail_to_send->Username = $dato_usuario["Usuario"]["email"];
			$mail_to_send->Password = $dato_usuario["Usuario"]["email_password"];
			
			$mail_to_send->setFrom($mail_to_send->Username,$datoEmpresa["DatoEmpresa"]["razon_social"]." - ".$dato_usuario["Usuario"]["nombre"]);
			
			
			
		}else{
			
			$mail_to_send->Username = $datoEmpresa["DatoEmpresa"]["email_envio"];
			$mail_to_send->Password = $datoEmpresa["DatoEmpresa"]["email_password_envio"];
			
			$mail_to_send->setFrom($mail_to_send->Username,$datoEmpresa["DatoEmpresa"]["razon_social"]." - ".$dato_usuario["Usuario"]["nombre"]);
			
		}
		
		

		

		
		
		
		//send the message, check for errors
		if (!$mail_to_send->send()) {
			
			$this->error = EnumError::ERROR;
			$this->message_error = "El E-Mail no puede ser enviado. Detalles de la falla: ".$mail_to_send->ErrorInfo;
		}else{
			
			$this->error = EnumError::SUCCESS;
			$this->message_error = "El E-Mail se envi&oacute; correctamente";
		}
		
		
		}catch(Exception $e){
			
			$this->message_error = $e->getMessage();
			$this->error = EnumError::ERROR;
			
			
		}
		
	}
	

}
