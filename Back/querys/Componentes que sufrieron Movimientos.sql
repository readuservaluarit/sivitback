DELETE FROM Hoja1; /*PASO 0 limpio la tabla auxiliar*/

INSERT INTO Hoja1 (id) /*selecciono los componentes que sufrieron movimientos y los inserto en HOJA1 PASO 1*/

SELECT DISTINCT id_componente FROM (

SELECT id_componente FROM movimiento WHERE id_componente IS NOT NULL AND MONTH(fecha)>=3 AND YEAR(fecha)=2016

UNION

SELECT id_componente FROM orden_produccion WHERE fecha_cierre IS NULL AND YEAR(fecha_emision)=2016 AND MONTH(fecha_emision)>=3 
AND id IN (SELECT id_orden_produccion FROM movimiento WHERE id_orden_produccion IS NOT NULL AND MONTH(fecha)>=3 AND YEAR(fecha)=2016)

UNION

SELECT DISTINCT pr.id_producto_hijo AS id_componente FROM orden_armado 
JOIN producto_relacion pr  ON pr.id_producto_padre = orden_armado.`id_producto`

WHERE YEAR(fecha_emision)=2016 AND MONTH(fecha_emision)>=3 ) AS tabla


/*PASO 2*/
SELECT producto.id, producto.`codigo`,producto.`d_producto`, (SELECT stock FROM stock_producto WHERE id = producto.id AND id_deposito=4) AS stock,(SELECT stock FROM stock_producto WHERE id = producto.id AND id_deposito=6) AS stock_comprometido FROM Hoja1
JOIN producto ON producto.id = Hoja1.`id`
WHERE id_categoria IN (319,320,321,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,344,345,346,349,350,351,352,353,354,355,356,357,358,360,372,373,374,375,378,379,380,381,382,383,384,385,386,389,390,394,395,396,399,400,402,403,404,405,406,412,414,415,416,417,418,419,420,421,422,423,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482)
ORDER BY codigo ASC


