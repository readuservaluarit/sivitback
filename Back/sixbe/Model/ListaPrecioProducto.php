<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class ListaPrecioProducto extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'lista_precio_producto_view';

    public $actsAs = array('Containable');
    
    public $source = "lista_precio_producto";
    public $view = "lista_precio_producto_view";

/**
 * Validation rules
 *
 * @var array
 */
public $validate = array(
	
);



public function getSource(){
	
	return $this->source;
}

public function getView(){
	
	return $this->view;
}

public function setVirtualFieldsForView()
{//configura los virtual fields  para este Model
	$this->virtual_fields_view = array(
	"moneda_simbolo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>1,"default"=>null,"length"=>null),
	);
}

public $belongsTo = array(
        'ListaPrecio' => array(
            'className' => 'ListaPrecio',
            'foreignKey' => 'id_lista_precio',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
        ),
         'Moneda' => array(
            'className' => 'Moneda',
            'foreignKey' => 'id_moneda',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
        ),
		'Producto' => array(
				'className' => 'Producto',
				'foreignKey' => 'id_producto',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
		)
);
        

public function beforeSave($options = array())
{
    //$this->useTable = 'people'; <= Does not work
    $this->setSource($this->source); // Works
    return true;
}

public function afterSave($created, $options = array()) {
    //$this->useTable = 'people_rel'; <= Does not work
    $this->setSource($this->view); // Works
}


public function beforeDelete($cascade = true) {
    $this->setSource($this->source);
    return true;
}

public function afterDelete() {
    $this->setSource($this->view);
    return true;
}


public function beforeFind($results, $primary = false) {
    $this->setSource($this->view);
    return true;
}

}
