<?php

/**
* @secured(CONSULTA_CENTRO_COSTO)
*/
class CuentaContableCentroCostosController extends AppController {
    public $name = 'CuentaContableCentroCostos';
    public $model = 'CuentaContableCentroCosto';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
/**
* @secured(CONSULTA_CENTRO_COSTO)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
        $d_centro_costo = strtolower($this->getFromRequestOrSession('CuentaContableCentroCosto.d_centro_costo'));
        $id_centro_costo = strtolower($this->getFromRequestOrSession('CuentaContableCentroCosto.id_centro_costo'));
		$id_cuenta_contable = strtolower($this->getFromRequestOrSession('CuentaContableCentroCosto.id_cuenta_contable'));
        $porcentaje_desde = strtolower($this->getFromRequestOrSession('CuentaContableCentroCosto.porcentaje_desde'));
        $porcentaje_hasta = strtolower($this->getFromRequestOrSession('CuentaContableCentroCosto.porcentaje_hasta'));
        
        $conditions = array();
         
        if ($d_centro_costo != "") {
            array_push($conditions, array('LOWER(CentroCosto.d_centro_costo) LIKE' => '%' . $d_centro_costo . '%'));
        }
        
        if ($id_centro_costo != "") {
           array_push($conditions, array('CuentaContableCentroCosto.id_centro_costo' => $id_centro_costo));
        }
		
		if ($id_cuenta_contable != "") {
           array_push($conditions, array('CuentaContableCentroCosto.id_cuenta_contable' => $id_cuenta_contable));
        }
        
         if ($porcentaje_desde != "") {
           array_push($conditions, array('CuentaContableCentroCosto.porcentaje >=' => $porcentaje_desde));
        }
        
          if ($porcentaje_hasta != "") {
           array_push($conditions, array('CuentaContableCentroCosto.porcentaje <=' => $porcentaje_hasta));
        }

        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain'=>array("CentroCosto"=>array("Area")),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de CuentaContableCentroCostos', 'Datos de las CuentaContableCentroCostos', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_CENTRO_COSTO', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'CuentaContableCentroCosto.id', "10%");
                $formBuilder->addHeader('Nombre', 'CuentaContableCentroCosto.d_CENTRO_COSTO', "50%");
                $formBuilder->addHeader('Nombre', 'CuentaContableCentroCosto.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_CENTRO_COSTO');
                $formBuilder->addField($this->model, 'cotizacion');
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        foreach($data as &$dato){
            
         if(!isset($dato['CentroCosto']["Area"]["id"]))
            $dato['CuentaContableCentroCosto']['d_area_centro_costo']=$dato['CentroCosto']['d_centro_costo'];    
         else
            $dato['CuentaContableCentroCosto']['d_area_centro_costo']=$dato['CentroCosto']['Area']['d_area'].'-'.$dato['CentroCosto']['d_centro_costo'];      
            
          unset($dato['CentroCosto']);
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }

    /**
    * @secured(BAJA_CENTRO_COSTO,READONLY_PROTECTED)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->CuentaContableCentroCosto->id = $id;
     
       try{
            if ($this->CuentaContableCentroCosto->delete() ) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "El centro de costo ha sido desasignado exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }
	
	
	public function getModel($vista='default')
	{    
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
    
    private function editforView($model,$vista)
	{  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);    
    }

}
?>