<?php
App::uses('AppModel', 'Model');

class ComprobanteItem extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'comprobante_item_view';
    
    public $actsAs = array('Containable');
    
    public $source = "comprobante_item";
    public $view = "comprobante_item_view";
    
   
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'El id del item no puede ser vacio',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);
    
    
    public function setDecimalPlaces(){//configura los decimal places para este Model
    
     App::uses('Modulo', 'Model');
     $modulo_obj = new Modulo();
     $cantidad_decimales = $modulo_obj->getCantidadDecimalesFormateo(EnumModulo::STOCK); 
     
     $this->decimal_places = array(
    "cantidad"=>$cantidad_decimales
    
    );
    
    }
    
    public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     $this->virtual_fields_view = array(
		 
		 "d_producto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
		 "d_unidad"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
		 "codigo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "item_codigo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_iva"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_facturado"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_remitido"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_notas_credito_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_pendiente_a_facturar_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_pendiente_a_remitir_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_pendiente_notas_credito_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_remitido_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_facturado_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_nota_credito_posibles"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_facturado"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_nota_credito"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_pendiente_nota_credito"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_pendiente_a_remitir"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_pendiente_a_facturar"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "d_moneda"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "d_moneda_simbolo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "dias_atraso_item"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "fecha_generacion"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
		 "dias_atraso_global"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "moneda_simbolo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "persona_tel"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "razon_social"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "d_estado_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "orden_compra_externa"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "id_moneda"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "nro_comprobante"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "id_iva"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "valor_moneda"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "valor_moneda2"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "valor_moneda3"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "fecha_entrega"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
		 "id_persona"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "id_deposito_origen"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "d_deposito_origen"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "id_deposito_destino"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "t_razon_social"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "t_direccion"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_recibida_irm"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_pendiente_irm"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "pto_nro_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "d_tipo_comprobante_origen"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "pto_nro_comprobante_origen"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "nro_comprobante_origen"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 "fecha_generacion_origen"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
		 "fecha_vencimiento_origen"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_comprobante_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_orden_compra_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_pendiente_a_gastar"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_remito_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_pendiente_irm"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_pedido"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_pendiente_a_pedido"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_aprobada_a_facturar_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_aprobada_a_facturar"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_recibida_irm_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_pendiente_irm_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "referencia_comprobante_externo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     	 "enlazar_con_moneda"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     	 "d_iva"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     	 "cantidad_pendiente_a_armar"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     	 "total_armado"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     	 "persona_codigo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     	 "persona_id"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     	 "d_orden_armado"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_desvio"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
		 
		 
		 "fecha_limite_armado"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
		 "fecha_entrega_origen"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
		 "fecha_entrega_orden_armado"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
		 "pto_nro_pedido_interno"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "total_por_producto"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
		 "cantidad_calculada"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     	 "d_persona"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 "codigo_producto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		 
		 //MP: objetos json particulares para  OrdenesTrabajo /indexMD/id.json
     	"ComprobanteItem"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null),
 		"Detalle"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null), 
		"Lotes"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null),
		"Instrumental"=>array("type"=>EnumTipoDato::json,"show_grid"=>0,"default"=>null,"length"=>null), //ESTA PERO PARECE Q NO ESTA
     	
     	"d_detalle_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     	"d_usuario"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
		"codigo_barra"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null)
		);	
    
    
    }

	//The Associations below have been created with all possible keys, those that are not needed can be removed

public $belongsTo = array(
        'Comprobante' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Producto' => array(
            'className' => 'Producto',
            'foreignKey' => 'id_producto',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ComprobanteOrigen' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante_origen',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        
        'ComprobanteItemOrigen' => array(
            'className' => 'ComprobanteItemOrigen',
            'foreignKey' => 'id_comprobante_item_origen',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
		
         'Iva' => array(
            'className' => 'Iva',
            'foreignKey' => 'id_iva',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Unidad' => array(
            'className' => 'Unidad',
            'foreignKey' => 'id_unidad',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
       'ListaPrecio' => array( //hace referencia a la sucursal del comprobante
            'className' => 'ListaPrecio',
            'foreignKey' => 'id_lista_precio',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'TipoLote' => array( //hace referencia a la sucursal del comprobante
            'className' => 'TipoLote',
            'foreignKey' => 'id_tipo_lote',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
		'DetalleTipoComprobante' => array( //hace referencia a la sucursal del comprobante
				'className' => 'DetalleTipoComprobante',
				'foreignKey' => 'id_detalle_tipo_comprobante',
				'conditions' => '',
				'fields' => '',
				'order' => ''
		),
		'Persona' => array( //hace referencia a la sucursal del comprobante
				'className' => 'Persona',
				'foreignKey' => 'id_persona',
				'conditions' => '',
				'fields' => '',
				'order' => ''
		)
        
       
      
 );
 

 public $hasMany = array(
      /*  'ComprobanteItemMany' => array(
            'className' => 'ComprobanteItem',
            'foreignKey' => 'id_comprobante_item_origen',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),*/
 		'ComprobanteItemLote' => array(
 				'className' => 'ComprobanteItemLote',
 				'foreignKey' => 'id_comprobante_item',
 				'dependent' => false,
 				'conditions' => '',
 				'fields' => '',
 				'order' => '',
 				'limit' => '',
 				'offset' => '',
 				'exclusive' => '',
 				'finderQuery' => '',
 				'counterQuery' => ''
 		)
        );
 
 
 public function GetItemprocesadosEnImportacion($id,$array_tipo_comprobante= array(),$array_id_estado=array(),$array_estado_omitir=array()){ //el id del item y el tipo de comprobante
        
        /*Devuelve todos los items de un comprobante que tiene como origen al id de comprobante pasado*/ 
        
        
        if(count($array_tipo_comprobante) !=0){
            
           // $short_list_array = explode(',', $id_tipo_comprobante
        if(isset($id["id"])){   
        	
        	$conditions = array();
        	 
        	 array_push($conditions, array('ComprobanteItem.id_comprobante_item_origen'=>$id["id"]));

        	 
         if(count($array_tipo_comprobante)>0)
        	 array_push($conditions, array('Comprobante.id_tipo_comprobante'=>$array_tipo_comprobante));
        	
          if(count($array_id_estado)>0)
        	 	array_push($conditions, array('Comprobante.id_estado_comprobante'=>$array_id_estado));
        	 	
        	 	
        	 	
        if(count($array_estado_omitir)>0)
        	array_push($conditions, array('NOT'=>array('Comprobante.id_estado_comprobante'=>$array_estado_omitir)));
         
        	 		
        	 		
            $procesados = $this->find('all', array(
            		'fields'=>array('ComprobanteItem.cantidad','ComprobanteItem.cantidad_cierre','Comprobante.id_tipo_comprobante','ComprobanteItem.id_comprobante_item_origen','ComprobanteItem.cantidad_ceros_decimal','Comprobante.nro_comprobante','EstadoComprobante.d_estado_comprobante','Comprobante.fecha_vencimiento','Comprobante.fecha_entrega'),
            					    'conditions' => $conditions,
                                    'contain' =>false,
                                    'joins' => array(
                                                        array(
                                                            'table' => 'comprobante',
                                                            'alias' => 'Comprobante',
                                    
                                                            'foreignKey' => 'id_comprobante',
                                                            'conditions'=> array(
                                                                'Comprobante.id = ComprobanteItem.id_comprobante', 
                                                                'ComprobanteItem.activo = 1',
                                                                'Comprobante.id_estado_comprobante <>'=>EnumEstadoComprobante::Anulado 
                                                            )
                                                            ),
                                    		array(
                                    				'table' => 'estado_comprobante',
                                    				'alias' => 'EstadoComprobante',
                                    				
                                    				'foreignKey' => 'id_estado_comprobante',
                                    				'conditions'=> array(
                                    						'Comprobante.id_estado_comprobante = EstadoComprobante.id'
                                    		
                                    				)
                                    		)
                                                            )
                                 ));
        }else{
            $procesados = null;
        }
        
        }else//devuelvo todos
            $procesados = $this->find('all', array(
            		'field'=>array("ComprobanteItem.cantidad","ComprobanteItem.cantidad_cierre",'Comprobante.id_tipo_comprobante','ComprobanteItem.id_comprobante_item_origen','ComprobanteItem.cantidad_ceros_decimal','Comprobante.nro_comprobante','EstadoComprobante.d_estado_comprobante'),
                                'conditions' => array('ComprobanteItem.id_comprobante_item_origen' => $id,'ComprobanteItem.activo'=>1, 'Comprobante.id_estado_comprobante <>'=>EnumEstadoComprobante::Anulado,'ComprobanteItemOrigen.activo'=>1 ),
                                'contain' =>array('Comprobante'=>array('EstadoComprobante','TipoComprobante','PuntoVenta'),'ComprobanteItemOrigen')
                             ));
        return $procesados;
        
    }
    
    
     public function GetCantidadItemProcesados($items,$campo_sumar="cantidad"){ //$campo_sumar es el campo de la tabla comprobante_items a sumarizar

        if( is_array($items) && count($items)>0){
			
            $cantidad = 0;
            foreach($items as $item){
                
            	$cantidad = $cantidad + $item[$this->name][$campo_sumar];
            }
            return $cantidad;
        }else
            return 0;
    }
    
    public function getFechaEntrega($fecha_inicio='', $dias=0,$fecha_entrega){
        
        if($fecha_inicio == '')
            $fecha_inicio =  date("Y-m-d");
            
            
        if(is_null($dias))
            $dias = 0; 
        
        $diferencia_fecha_emision_entrega =  abs(strtotime($fecha_entrega) - strtotime($fecha_inicio));
        
        
        $dias_calculados = floor($diferencia_fecha_emision_entrega/(60*60*24));
        
        
        $fecha = new DateTime($fecha_inicio);
        $fecha->add(new DateInterval('P'.$dias.'D'));
        return $fecha->format('d-m-Y');
    }
    
    
    public function getDiasAtraso($fecha_entrega,$dias =0){ /*Si dias esta en 1 entonces mando dias sino es un fecha*/

                if($fecha_entrega){
                $fecha_inicio = date("Y-m-d");
                $fecha_entrega = new DateTime($fecha_entrega);
                
                
                $datetime1 = new DateTime($fecha_entrega->format('Y-m-d'));
                $datetime2 = new DateTime($fecha_inicio);
                $interval = $datetime1->diff($datetime2);
                $dias = $interval->format('%R%a'); 
             
                return $dias;
               
            }else{
                
                return 0;
            }    
     
    }
    
 
   /*
 public $hasMany = array(
        'ArticuloRelacion' => array(
            'className' => 'ArticuloRelacion',
            'foreignKey' => 'id_producto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        
);
	  */
      
      
      public function paginateAgrupado(&$puntero,$id_agrupacion='',$conditions,$limite_paginas,$numero_paginas,$array_agrupacion,$orden="'Producto.codigo ASC'",$id_moneda_expresion = 2){
      	
     
     
      	App::uses('DatoEmpresa', 'Model');
      	$datoEmpresa = new DatoEmpresa();
      	
       if($id_moneda_expresion == "")
       		$id_moneda_expresion = EnumMoneda::Dolar;
      	
       		
      	
       	$id_tipo_moneda = $datoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda_expresion);
      	$campo_valor = $this->Comprobante->getFieldByTipoMoneda($id_tipo_moneda);//obtengo el campo en la cual expresar el reporte
          
          switch($id_agrupacion){
          
              case '':
              case EnumTipoFiltroComprobanteItem::individual:
              case EnumTipoFiltroComprobanteItem::agrupadaporComprobante:
 
               $puntero->paginate = array(
                 'paginado'=>$puntero->paginado,
                'joins' => array(
                    array(
                        'table' => 'producto_tipo',
                        'alias' => 'ProductoTipo',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'ProductoTipo.id = Producto.id_producto_tipo'
                        )
                        
                    ),
                    
                    array(
                    'table' => 'tipo_comprobante',
                    'alias' => 'TipoComprobante',
                    'type' => 'LEFT',
                    'conditions' => array(
                    'TipoComprobante.id = Comprobante.id_tipo_comprobante'
                    		)
                    		
                    ),
                    
                    array(
                    'table' => 'persona',
                    'alias' => 'Persona',
                    'type' => 'LEFT',
                    'conditions' => array(
                    'Persona.id = Comprobante.id_persona'
                    		)
                    		
                    		)
                    
                    
               
                    
                    ),
                'contain' => $array_agrupacion,
                'conditions' => $conditions,
                'limit' => $limite_paginas,
                'page' => $numero_paginas,
                'order' => $orden
                );
               break;
               
              case EnumTipoFiltroComprobanteItem::agrupadaporproducto: //agrupo por id_producto
              
                  $puntero->paginate = array(
                   'paginado'=>$puntero->paginado,
                   'joins' => array(
                        array(
                            'table' => 'producto_tipo',
                            'alias' => 'ProductoTipo',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'ProductoTipo.id = Producto.id_producto_tipo'
                            )
                            
                        ),
                        array(
                        'table' => 'tipo_comprobante',
                        'alias' => 'TipoComprobante',
                        'type' => 'LEFT',
                        'conditions' => array(
                        'TipoComprobante.id = Comprobante.id_tipo_comprobante'
                        		)
                        		
                        ),
                        
                        array(
                        'table' => 'persona',
                        'alias' => 'Persona',
                        'type' => 'LEFT',
                        'conditions' => array(
                        'Persona.id = Comprobante.id_persona'
                        		)
                        		
                        )
                  
                        
                        ),
                    'contain' =>array('Producto','Comprobante'),
                    'fields' => array('SUM(ComprobanteItem.cantidad) as cantidad','Producto.codigo','ComprobanteItem.id','ComprobanteItem.id_producto','Comprobante.id_tipo_comprobante'),
                    'conditions' => $conditions,
                    'limit' => $limite_paginas,
                    'page' => $numero_paginas,
                    'group' => array('ComprobanteItem.id_producto'),
                    'order' => $orden
                    );
              
              break;
              
               case EnumTipoFiltroComprobanteItem::agrupadaporfamilia://agrupo por id_familia_producto
              
                  $puntero->paginate = array(
                  'paginado'=>$puntero->paginado,
                   'joins' => array(
                        array(
                            'table' => 'producto_tipo',
                            'alias' => 'ProductoTipo',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'ProductoTipo.id = Producto.id_producto_tipo'
                            )
                            
                        ),
                        array(
                        'table' => 'tipo_comprobante',
                        'alias' => 'TipoComprobante',
                        'type' => 'LEFT',
                        'conditions' => array(
                        'TipoComprobante.id = Comprobante.id_tipo_comprobante'
                        		)
                        		
                        ),
                        array(
                        'table' => 'persona',
                        'alias' => 'Persona',
                        'type' => 'LEFT',
                        'conditions' => array(
                        'Persona.id = Comprobante.id_persona'
                        		)
                        		
                        )
                  
                        
                        
                        ),
                    'contain' => array('SUM(ComprobanteItem.cantidad) as cantidad','Producto.codigo','ComprobanteItem.id','Producto.id','Comprobante.id_tipo_comprobante'),
                    'conditions' => $conditions,
                    'limit' => $limite_paginas,
                    'page' => $numero_paginas,
                    'group' => array('Producto.id_familia_producto'),
                    'order' => $orden
                    );
              
              break;
              
              
               case EnumTipoFiltroComprobanteItem::agrupadaporPersona://agrupo por id_familia_producto
               	
               	$puntero->paginate = array(
               	'paginado'=>$puntero->paginado,
               	'joins' => array(
             
               	array(
               	'table' => 'tipo_comprobante',
               	'alias' => 'TipoComprobante',
               	'type' => 'LEFT',
               	'conditions' => array(
               	'TipoComprobante.id = Comprobante.id_tipo_comprobante'
               			)
               			
               	),
               	
               	array(
               	'table' => 'persona',
               	'alias' => 'Persona',
               	'type' => 'LEFT',
               	'conditions' => array(
               	'Comprobante.id_persona = Persona.id'
               			)
               			
               	)
               	
               	
               	
               	),
               	'fields' => array('SUM(ROUND((ComprobanteItem.precio_unitario*ComprobanteItem.cantidad)/Comprobante.'.$campo_valor.',2)) as total','Persona.razon_social'),
               	'conditions' => $conditions,
               	'limit' => $limite_paginas,
               	'page' => $numero_paginas,
               	'contain'=>array('Comprobante'=>array('Persona','TipoComprobante')),
               	'group' => array('Persona.id'),
               	'order' => $orden
               	);
               	
               	break;
              
              
              
           
              
           
            
          }
      }
      
      
       public function GetNumberComprobante($pto_venta,$nro_comprobante){
        
        
        
        if( $pto_venta != 0 )
            return str_pad($pto_venta, 3, "0", STR_PAD_LEFT).' - '.str_pad($nro_comprobante, 8, "0", STR_PAD_LEFT);
        else
            
            return str_pad($nro_comprobante, 8, "0", STR_PAD_LEFT);
        
    }
    
    
     public function setDefaultFieldsForView($model){
        
        
        foreach($model as $key=> &$dato){ //por default no se muesta ningun campo
            
           $model[$key]["show_grid"] = 0;
           $model[$key]["order"] = 1;
           $model[$key]["text_color"] = "#000000";
            
        }
        
        return $model; 
        
        
    } 
    
public function getOrderItems($model,$id_comprobante=''){
    
    if($id_comprobante != "")
        return array("".$model.".n_item"=>'ASC',"".$model.".id"=>'ASC');
    else
        return array("".$model.".id"=>'DESC'); 
    
    
}


public function afterFind($results, $primary = false) {
    
    
    
    foreach ($results as $key => $val) {
        
        if (isset($val['ComprobanteItem']['cantidad'])) {
            $results[$key]['ComprobanteItem']['cantidad'] = (string) round($results[$key]['ComprobanteItem']['cantidad'],$results[$key]['ComprobanteItem']['cantidad_ceros_decimal'] );
    }
    
  
    }
    
    $this->setSource($this->view);
    return $results;
}


public function beforeSave($options = array())
{
    //$this->useTable = 'people'; <= Does not work
    $this->setSource($this->source); // Works
    return true;
}

public function afterSave($created, $options = array()) {
    //$this->useTable = 'people_rel'; <= Does not work
    $this->setSource($this->view); // Works
}


public function beforeDelete($cascade = true) {
    $this->setSource($this->source);
    return true;
}

public function afterDelete() {
    $this->setSource($this->view);
    return true;
}


public function beforeFind($results, $primary = false) {
    $this->setSource($this->view);
    return true;
}


public function getTotalItem($item){    
    
    //$this->{$this->model}->getTotalItem($producto);
    return round($item[$this->name]["precio_unitario"] * $item[$this->name]["cantidad"],2);
    
    
    
}


public function ChildRecords($id_comprobante_item){
    
          $cantidad_items_comprobante = $this->find('count', array(
                                                'conditions' => array('ComprobanteItem.id_comprobante_item_origen' => $id_comprobante_item,'ComprobanteItemOrigen.activo'=>1),
                                                'contain' => array('ComprobanteItemOrigen')
                                                ));
                                                
          return $cantidad_items_comprobante;                                      
    
}


public function getTotalItemsGenerados2Niveles($id_comprobante_item,$tipo_comprobante_primer_nivel,$tipo_comprobante_segundo_nivel,$array_estado_segundo_nivel = array(EnumEstadoComprobante::Abierto,EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::CerradoConCae,EnumEstadoComprobante::Cumplida,EnumEstadoComprobante::Remitido)){
	
	$joins = array(
			array(
					"table"=>'comprobante',
					//"type"=>'JOIN',
					"alias"=>'FacturaVenta',
					"conditions"=>array("ComprobanteItem.id_comprobante = FacturaVenta.id")
			),
			
			array(
					"table"=>'comprobante_item',
					//"type"=>'JOIN',
					"alias"=>'remito_venta_item',
					"conditions"=>array("remito_venta_item.id_comprobante_item_origen = ComprobanteItem.id")
			),
			array(
					"table"=>'comprobante',
					//"type"=>'JOIN',
					"alias"=>'RemitoVenta',
					"conditions"=>array("RemitoVenta.id = remito_venta_item.id_comprobante")
			)
			
			
	);
	
	$conditions = array();
	
	
	array_push($conditions,array("ComprobanteItem.id_comprobante_item_origen"=>$id_comprobante_item));
	array_push($conditions,array("FacturaVenta.id_tipo_comprobante"=>$tipo_comprobante_primer_nivel));
	array_push($conditions,array("RemitoVenta.id_tipo_comprobante"=>$tipo_comprobante_segundo_nivel));
	array_push($conditions,array("ComprobanteItem.activo"=>1));
	array_push($conditions,array("RemitoVenta.id_estado_comprobante"=>$array_estado_segundo_nivel));
	
	
	$cantidad_remito = $this->find('all',
			array(
					'joins'=>$joins,
					'fields'=>array('IFNULL(SUM(remito_venta_item.cantidad),0) as cantidad'),
					'conditions' => $conditions,
					'contain'=>false
			)
			
			);
	
	 
	
	return $cantidad_remito[0][0]["cantidad"];
	
	
}


public function getIdArticulosFromComprobante($data){
	
	
	$array_id_articulo = array();
	
	if(isset($data[$this->name])){
		
		foreach($data[$this->name] as $valor){
			
			
			array_push($array_id_articulo, $valor["id_producto"]);
			
			
		}
		
		
		
		
		
	}
	
	
	return $array_id_articulo;
	
	
	
}


public function getCodigoFromProducto($data){
	
	
	if( (isset($data[$this->name]["producto_codigo"]) && strlen($data[$this->name]["producto_codigo"])>0)){
		
		return $data[$this->name]["producto_codigo"];
		
	}elseif((isset($data["producto_codigo"]) && strlen($data["producto_codigo"])>0)){
		
		return $data["producto_codigo"];
	}else{
		
		return $data["Producto"]["codigo"];
	}
	
	
}

public function calculaUnidad(&$item){
	
	if(isset($item[$this->name]["id_unidad"]) && $item[$this->name]["id_unidad"]>0){
		
		
		$item[$this->name]["d_unidad"] = $item["Unidad"]["d_unidad"];
		$item[$this->name]["id_unidad"] = $item["Unidad"]["id"];
	}elseif(isset($item["ComprobanteItemOrigen"]["Unidad"]["id_unidad"]) && $item["ComprobanteItemOrigen"]["id_unidad"]>0){
		
		$item[$this->name]["d_unidad"] = $item["ComprobanteItemOrigen"]["Unidad"]["d_unidad"];
		$item[$this->name]["id_unidad"] = $item["ComprobanteItemOrigen"]["id_unidad"];
	}
	
	
	if(is_null($item[$this->name]["id_unidad"]) && !$item[$this->name]["id_unidad"]>0 ){
		
		$item[$this->name]["d_unidad"] = $item["Producto"]['Unidad']["d_unidad"];
		$item[$this->name]["id_unidad"] = $item["Producto"]['Unidad']["id"];
	}
}



}
