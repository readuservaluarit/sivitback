<?php
App::uses('EstadosPersonaController', 'Controller');



class EstadosEmpleadosController extends EstadosPersonaController {
    
    public $name = 'EstadosEmpleados';
    public $model = 'EstadoPersona';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $id_tipo_persona = EnumTipoPersona::Empleado;
    public $tine_permisos = 0;
    
    
  
    public function index() {
        
         parent::index();    
        
    }
    
    

    
    

     
  
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
      
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
        
        
         $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);          
    }
    
    
    
    
}
?>