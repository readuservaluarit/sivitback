<?php

class RegionPicker extends EntityPicker{
    
    public $model = "Region";
    
    public $pickerTitle = "Selección de Región";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Nombre", "field" => "nombre", "width" => "80%", "keyOrder" => "nombre")
                                );

    public function getPickerConditions($busqueda){
        
        return array('Region.nombre LIKE' => '%' . $busqueda['bus_unico'] . '%');
    }
    
    public function getPickerInitialFilter(){
        
        //Filtra siempre por la Region del usuario si corresponde
        return DataFilter::getRegionPickerFilter();
        
    }

}