

  
SELECT 
  comprobante.`nro_comprobante`,
  punto_venta.`numero` AS pto_venta,
  CONCAT(LPAD(punto_venta.`numero`,4,'0') ,'-',LPAD(comprobante.`nro_comprobante`,8,'0')) AS pto_nro_comprobante,
  tipo_comprobante.`codigo_tipo_comprobante`,
  detalle_tipo_comprobante.`d_detalle_tipo_comprobante`,
  persona.`razon_social`,
  comprobante_valor.monto,
  comprobante.`fecha_contable`,
  valor.`d_valor`,
  comprobante_valor.`d_comprobante_valor`,
  comprobante_valor.`referencia`,
  cheque.`nro_cheque`,
  cheque.`fecha_cheque`,
  cheque.`fecha_clearing`,
  comprobante_valor.`id` 
FROM
  comprobante_valor 
  JOIN comprobante 
    ON comprobante.id = comprobante_valor.`id_comprobante` 
  JOIN tipo_comprobante 
    ON comprobante.id_tipo_comprobante = tipo_comprobante.`id` 
  LEFT JOIN detalle_tipo_comprobante 
    ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id` 
  LEFT JOIN punto_venta 
    ON punto_venta.id = comprobante.`id_punto_venta` 
  LEFT JOIN persona 
    ON persona.id = comprobante.id_persona 
  LEFT JOIN cheque 
    ON cheque.id = comprobante_valor.`id_cheque` 
  LEFT JOIN valor 
    ON valor.id = comprobante_valor.id_valor 
WHERE origen = 1 
  AND id_cuenta_bancaria = 2 
  AND comprobante.`id_tipo_comprobante` = 502
UNION
ALL 
SELECT 
  comprobante.`nro_comprobante`,
  punto_venta.`numero` AS pto_venta,
    CONCAT(LPAD(punto_venta.`numero`,4,'0') ,'-',LPAD(comprobante.`nro_comprobante`,8,'0')) AS pto_nro_comprobante,
  tipo_comprobante.`codigo_tipo_comprobante`,
  detalle_tipo_comprobante.`d_detalle_tipo_comprobante`,
  persona.`razon_social`,
  comprobante_valor.monto,
  comprobante.`fecha_contable`,
  valor.`d_valor`,
  comprobante_valor.`d_comprobante_valor`,
  comprobante_valor.`referencia`,
  cheque.`nro_cheque`,
  cheque.`fecha_cheque`,
  cheque.`fecha_clearing`,
  comprobante_valor.`id` 
FROM
  comprobante_valor 
  JOIN comprobante 
    ON comprobante.id = comprobante_valor.`id_comprobante` 
  JOIN tipo_comprobante 
    ON comprobante.id_tipo_comprobante = tipo_comprobante.`id` 
  LEFT JOIN detalle_tipo_comprobante 
    ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id` 
  LEFT JOIN punto_venta 
    ON punto_venta.id = comprobante.`id_punto_venta` 
  LEFT JOIN persona 
    ON persona.id = comprobante.id_persona 
  LEFT JOIN valor 
    ON valor.id = comprobante_valor.id_valor 
  LEFT JOIN cheque 
    ON cheque.id = comprobante_valor.`id_cheque` 
WHERE id_cuenta_bancaria = 2 
  AND comprobante.`id_tipo_comprobante` IN (306,307,308, 504) 
UNION
ALL 
SELECT 
  comprobante.`nro_comprobante`,
  punto_venta.`numero` AS pto_venta,
  CONCAT(LPAD(punto_venta.`numero`,4,'0') ,'-',LPAD(comprobante.`nro_comprobante`,8,'0')) AS pto_nro_comprobante,
  tipo_comprobante.`codigo_tipo_comprobante`,
  detalle_tipo_comprobante.`d_detalle_tipo_comprobante`,
  persona.`razon_social`,
  comprobante_valor.monto,
  comprobante.`fecha_contable`,
  valor.`d_valor`,
  comprobante_valor.`d_comprobante_valor`,
  comprobante_valor.`referencia`,
  cheque.`nro_cheque`,
  cheque.`fecha_cheque`,
  cheque.`fecha_clearing`,
  comprobante_valor.`id` 
FROM
  comprobante_valor 
  JOIN comprobante 
    ON comprobante.id = comprobante_valor.`id_comprobante` 
  JOIN tipo_comprobante 
    ON comprobante.id_tipo_comprobante = tipo_comprobante.`id` 
  LEFT JOIN detalle_tipo_comprobante 
    ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id` 
  LEFT JOIN punto_venta 
    ON punto_venta.id = comprobante.`id_punto_venta` 
  LEFT JOIN persona 
    ON persona.id = comprobante.id_persona 
  LEFT JOIN valor 
    ON valor.id = comprobante_valor.id_valor 
  LEFT JOIN cheque 
    ON cheque.id = comprobante_valor.`id_cheque` 
WHERE origen = 0
  AND id_comprobante IN 
  (SELECT 
    comprobante_valor.id_comprobante 
  FROM
    comprobante_valor 
    JOIN comprobante 
      ON comprobante.id = comprobante_valor.`id_comprobante` 
    JOIN detalle_tipo_comprobante 
      ON comprobante.id_detalle_tipo_comprobante = detalle_tipo_comprobante.`id` 
  WHERE origen = 1
    AND id_cuenta_bancaria = 2 
    AND comprobante.`id_tipo_comprobante` = 604 
    AND comprobante.`id_detalle_tipo_comprobante` IN (47, 49))
    AND comprobante_valor.conciliado = 1
    ORDER BY fecha_contable ASC