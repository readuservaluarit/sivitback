
//mejorar poniendo fecha de cumplido
INSERT INTO comprobante (nro_comprobante,fecha_generacion,id_estado_comprobante,definitivo,fecha_entrega,inspeccion,genera_movimiento_stock,id_detalle_tipo_comprobante,id_usuario,id_punto_venta,id_tipo_comprobante)

SELECT id AS numero,fecha_emision,
CASE  
   WHEN id_estado = 1  THEN 1
   WHEN id_estado = 5  THEN 2
   WHEN id_estado = 6  THEN 9
    WHEN id_estado = 9  THEN 116
END AS id_estado_comprobante,


CASE  
   WHEN id_estado = 1  THEN 0
   WHEN id_estado = 5  THEN 1
   WHEN id_estado = 6  THEN 0
    WHEN id_estado = 9  THEN 1
END AS definitivo



,fecha_entrega,presion_ensayo AS inspeccion ,genera_movimiento_stock,IF(id_tipo_orden_armado = 0,62,63) AS id_detalle_tipo_comprobante,
1 AS id_usuario,3 AS id_punto_venta, 104 AS id_tipo_comprobante
 FROM orden_armado
       WHERE id_pedido_interno_fox IS NULL OR id_pedido_interno_fox =0
 ;
 
 

  INSERT INTO comprobante_item (
    id_producto,
    cantidad,
    cantidad_cierre,
    id_comprobante,
    activo,
    orden,
    n_item,
    id_comprobante_item_origen
  ) 
  
  
  SELECT 
    id_producto,
    cantidad_armar_planificado,
    cantidad,
    (SELECT 
      comprobante.id 
    FROM
      comprobante 
    WHERE nro_comprobante = orden_armado.id 
      AND id_tipo_comprobante = 104) AS id_comprobante,
    1 AS activo,
    1,
    1,
    (SELECT 
      id 
    FROM
      comprobante_item 
    WHERE id_comprobante = orden_armado.`id_pedido_interno` 
      AND comprobante_item.`id_producto` = orden_armado.`id_producto` AND comprobante_item.`activo` = 1 LIMIT 1) AS id_comprobante_item_origen 

  FROM
    orden_armado
    
  WHERE id_pedido_interno_fox IS NULL OR id_pedido_interno_fox =0
   /* ORDER BY (SELECT 
      id 
    FROM
      comprobante_item 
    WHERE id_comprobante = orden_armado.`id_pedido_interno` 
      AND comprobante_item.`id_producto` = orden_armado.`id_producto` AND comprobante_item.`activo` = 1 LIMIT 1) DESC*/

ALTER TABLE `qa_certificado_calidad` DROP FOREIGN KEY `qa_certificado_calidad_ibfk_2`; 
ALTER TABLE `qa_certificado_calidad` CHANGE `id_orden_armado` `id_orden_armado` BIGINT(11) NULL; 

//agregar la FK a comprobante

ALTER TABLE `qa_certificado_calidad` ADD COLUMN `id_comprobante_aux` BIGINT NULL AFTER `id_remito`; //lo uso como auxiliar para migrar

UPDATE qa_certificado_calidad 
JOIN comprobante ON comprobante.`nro_comprobante` = qa_certificado_calidad.id_orden_armado
SET id_comprobante_aux= comprobante.`id`

WHERE comprobante.`id_tipo_comprobante` = 104;

UPDATE qa_certificado_calidad 
SET id_orden_armado= id_comprobante_aux;

//faltan revisar los triggers

/*
registra_oa_columna_negativa
sp_componente_comprometido

/*


/*selecciono los QA y el equivalente id de comprobante*/
SELECT id_orden_armado,comprobante.id  FROM qa_certificado_calidad
JOIN comprobante ON comprobante.`nro_comprobante` = qa_certificado_calidad.`id_orden_armado`

WHERE comprobante.`id_tipo_comprobante`=104
