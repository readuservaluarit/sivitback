<?php

App::uses('ComprobanteItemsController', 'Controller');

class MantenimientoItemsController extends ComprobanteItemsController {
    
    public $name = EnumController::MantenimientoItems;
    public $model = 'ComprobanteItem';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
     /**
    * @secured(CONSULTA_MANTENIMIENTO)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model);
        
      
        
        
        $filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');
        
        
        //$filtro_agrupado = 3; //MP antes tenia uno 1 si manda un /index plano me agrupaba los items y no me traie los campos q necesito para el form PIMasterDetail0
        $this->ComprobanteItem->paginateAgrupado($this,$filtro_agrupado,$conditions,
        		$this->numrecords,$this->getPageNum(),
        		array('Comprobante'=>array('Usuario','EstadoComprobante','Moneda','PuntoVenta','Persona','TipoComprobante','DetalleTipoComprobante'),
        				'Producto'=>array('ProductoTipo','Unidad'),'ComprobanteItemOrigen'),$this->ComprobanteItem->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_comprobante'))); // AGREGADO PARA EL ORDEN DE ISCO
        		
        		
        		
        		
        
      
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as $key=> &$producto ){
                 
                 $producto["ComprobanteItem"]["d_producto"] = $producto["Producto"]["d_producto"];
                 $producto["ComprobanteItem"]["codigo"] = $this->ComprobanteItem->getCodigoFromProducto($producto);
                
                 //$producto["ComprobanteItem"]["total"] = (string) $this->{$this->model}->getTotalItem($producto);
                 $producto["ComprobanteItem"]["id_unidad"] = (string) $producto["Producto"]["Unidad"]["id"];
                 
                 $producto["ComprobanteItem"]["pto_nro_comprobante"] = $this->ComprobanteItem->GetNumberComprobante($producto["Comprobante"]["PuntoVenta"]["numero"],$producto["Comprobante"]["nro_comprobante"]);
                 
                 
                 if(isset($producto["Comprobante"])&& isset($producto["Comprobante"]["Persona"]) ){
                 	$producto["ComprobanteItem"]["razon_social"] = $producto["Comprobante"]["Persona"]["razon_social"];
                 	$producto["ComprobanteItem"]["persona_tel"]  =  $producto["Comprobante"]["Persona"]["tel"];
                 }
                 
                 $fecha = new DateTime($producto["Comprobante"]["fecha_generacion"]);
                 $producto["ComprobanteItem"]["fecha_generacion"] = $fecha->format('d-m-Y');;
                 $producto["ComprobanteItem"]["d_estado_comprobante"] = $producto["Comprobante"]["EstadoComprobante"]["d_estado_comprobante"];
                 
                 
                 if(isset($producto["Comprobante"]['DetalleTipoComprobante'])){
                 	$producto[$this->model]['d_detalle_tipo_comprobante'] = $producto["Comprobante"]['DetalleTipoComprobante']["d_detalle_tipo_comprobante"];
                 }
                 
                 
                 if(isset($producto['Comprobante']['Persona'])){
                 	
                 	
                 	if($producto['Comprobante']['Persona']['id']== null){
                 		
                 		$producto[$this->model]['razon_social'] = $producto['Comprobante']['razon_social'];
                 	}else{
                 		
                 		if($producto['Comprobante']['Persona']['id_tipo_persona'] == EnumTipoPersona::Proveedor){
                 			$producto[$this->model]['razon_social'] = $producto['Comprobante']['Persona']['razon_social'];
                 			
                 		}else{//es empleado
                 			
                 			$producto[$this->model]['razon_social'] = $producto['Comprobante']['Persona']['nombre']."  ". $producto['Comprobante']['Persona']['apellido'];
                 			
                 		}
                 		
                 	
                 		
                 	}
                 	
                 	unset($producto['Comprobante']['Persona']);
                 }
                 
                 
                 
                 
                 
                 if( isset($producto['Comprobante']["Usuario"]) )
                 	$producto[$this->model]['d_usuario'] = $producto['Comprobante']['Usuario']['nombre'];
                 
                 	
                 	
                 
                 
                 $this->FiltrarCamposOnTheFly($data,$producto,$key);
                 unset($producto["Producto"]);
                 unset($producto["Comprobante"]);
                 unset($producto["ComprobanteItemOrigen"]);
                 
                 
        }
        
        
        $this->data = $this->prepararRespuesta($data,$page_count);
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
        		"content" => $this->data,
            "page_count" =>$page_count
        );
        
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        

        
        
        
        
    //fin vista json
        
    }
    
    
      /**
    * @secured(CONSULTA_MANTENIMIENTO)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
     
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  

 
    }
    
    
    public function prepararRespuesta($data,&$page_count){
    	
    	
    	
    	$filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');//dice como mostrar los items
    	$array_id_comprobantes = array();
    	
    	
    	
    	if($filtro_agrupado == EnumTipoFiltroComprobanteItem::agrupadaporComprobante){ //si es la vista agrupada tomo los filtros de los items y agrupo en memoria por id_comprobante
    		
    		
    		
    		foreach($data as $key=>$valor){
    			if(in_array($valor["ComprobanteItem"]["id_comprobante"],$array_id_comprobantes))
    				unset($data[$key]);
    				else
    					array_push($array_id_comprobantes,$valor["ComprobanteItem"]["id_comprobante"]);
    					
    		}
    		
    		
    		
    		
    		
    		
    	}
    	
    	return parent::prepararRespuesta($data,$page_count);
    }

    
   
    
}
?>