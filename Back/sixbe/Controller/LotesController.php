<?php

class LotesController extends AppController {
    public $name = 'Lotes';
    public $model = 'Lote';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
   /**
    * @secured(CONSULTA_LOTE)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
		//Recuperacion de Filtros
		$id = $this->getFromRequestOrSession('Lote.id');
		$id_tipo_lote = $this->getFromRequestOrSession('Lote.id_tipo_lote');
        $codigo = $this->getFromRequestOrSession('Lote.codigo');
        $descripcion = $this->getFromRequestOrSession('Lote.d_lote');
        $tiene_certificado= $this->getFromRequestOrSession('Lote.tiene_certificado');
		$validado= $this->getFromRequestOrSession('Lote.validado');
       
        $conditions = array(); 
       
	   if($id!="")
            array_push($conditions, array('Lote.id' => $id)); 
	   
		if($id_tipo_lote!="")
       		array_push($conditions, array('Lote.id_tipo_lote' => $id_tipo_lote)); 
	   
       	if($codigo!=""){
            array_push($conditions, array('Lote.codigo LIKE' => '%' . $codigo  . '%')); 
            array_push($this->array_filter_names,array("Cod. Lote.:"=>$codigo));
       	
       	}
             
        if($descripcion!=""){
            array_push($conditions, array('Lote.d_lote LIKE' => '%' . $descripcion  . '%'));
            array_push($this->array_filter_names,array("Desc. Lote.:"=>$descripcion));
        }
		
		if($tiene_certificado!=""){
            array_push($conditions, array('Lote.tiene_certificado' =>  $tiene_certificado));
            array_push($this->array_filter_names,array("Tiene Certificado.:"=>$tiene_certificado));
		}
        
        if($validado!=""){
            array_push($conditions, array('Lote.validado' =>  $validado));
            array_push($this->array_filter_names,array("Lote Validado.:"=>$validado));
        }
    
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain'=>array("TipoLote"),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => 'Lote.id desc'
        );
        
      // vista json
        
        $this->PaginatorModificado->settings = $this->paginate; 
		$data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        
        foreach ($data as &$lote){
        	
        	$lote["Lote"]["d_tipo_lote"] = $lote["TipoLote"]["d_tipo_lote"];
        	
        	if($lote["Lote"]["tiene_certificado"] == 1)
        		$lote["Lote"]["d_tiene_certificado"] = "Si";
        	else 
        		$lote["Lote"]["d_tiene_certificado"] = "No";
        	
        		
        	if($lote["Lote"]["validado"] == 1)
        		$lote["Lote"]["d_validado"] = "Si";
        	else
        		$lote["Lote"]["d_validado"] = "No";
        				
        	
        	
        	unset($lote["TipoLote"]);
        }
        
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     
        
        
        
        
    //fin vista json
        
    }
    
    /**
    * @secured(ABM_USUARIOS)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->Lote->id = $id;
            $this->Lote->contain();
            $this->request->data = $this->Lote->read();
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Materia Prima');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Materia Prima');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Materia Prima');
        else
            $formBuilder->setTituloForm('Consulta de Materia Prima');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('codigo', 'C&oacute;digo', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('descripcion', 'Descripci&oacute;n', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('costo', 'Costo', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  
        $formBuilder->addFormInput('stock', 'Stock', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('stock_minimo', 'Stock M&iacute;nimo', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
       
         
        
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'LoteAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('LoteCodigo', 'Debe ingresar un C&oacute;digo');
                validator.validateRequired('LoteDescripcion', 'Debe ingresar una descripcion');
                validator.validateNumeric('LoteStock', 'El stock ingresado debe ser num&eacute;rico');
                validator.validateNumeric('LoteStockMinimo', 'El stock ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_codigo',
                        'data': 'codigo=' + $('#LoteCodigo').val()+'&id='+$('#LoteId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('LoteCodigo','El C&oacute;digo que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

    /**
    * @secured(ABM_USUARIOS)
    */
    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_LOTE)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_lote = '';
            $this->request->data["Lote"]["fecha_emision"] = date("Y-m-d H:i:s");
            
            
            
            
            
            
            
            try{
                if ($this->Lote->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "El Lote ha sido creado exitosamente";
                    $tipo = EnumError::SUCCESS;
                    $id_lote = $this->Lote->id;
                   
                }else{
                    $mensaje = "Ha ocurrido un error,el Lote no ha podido ser creado.";
                  
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el Lote no ha podido ser creado.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
            
            
            
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" => $id_lote
            
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    
    /**
    * @secured(MODIFICACION_LOTE)
    */
    function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "El Lote ha sido modificado exitosamente";
                     $status = EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
               }
               
               if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, la Materia Prima no ha podido modificarse.', 'error');
                
            }
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            $this->{$this->model}->contain('Unidad');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
  
    /**
    * @secured(BAJA_LOTE)
    */
    function delete($id) {
        
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->Lote->id = $id;
       try{
            if ($this->Lote->delete()) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Lote sido eliminado exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
    }
    

    
    public function home(){
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
    
  
    /**
    * @secured(CONSULTA_LOTE)
    */
    public function existe_codigo(){
        
    	$codigo =  $this->request->data['codigo'];
        $id_Lote = $this->request->data['id'];
        
        
        $this->loadModel("Lote");
      
        $existe = $this->Lote->find('count',array('conditions' => array(
                                                                            'Lote.codigo' => $codigo,
                                                                            'Lote.id !=' =>$id_Lote
                                                                            )
                                                                            ));
        if($existe >0)
        	$error = EnumError::ERROR;
       else
        	$error = EnumError::SUCCESS;
        		
       
       $output = array(
        				"status" => $error,
        				"message" => "",
        				"content" => ""
        		);
       $this->set($output);
       $this->set("_serialize", array("status", "message", "content"));
       
       
       
    }
    
    
     /**
    * @secured(CONSULTA_LOTE)
    */
	 public function getModel($vista='default'){
	 	
	 	$model = parent::getModelCamposDefault();//esta en APPCONTROLLER
	 	$model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
	 	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
	 	
	 }

	 
	 private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
	 	
	 	$this->set('model',$model);
	 	Configure::write('debug',0);
	 	$this->render($vista);
	 	
	 }
	 
	 
	 
	 
	 
	 /**
	  * @secured(CONSULTA_LOTE)
	  */
	 public function existe_lote(){
	 	
	 	
	 	
	 	$codigo_lote  = $this->request->data['codigo_lote'];
	 	$id_persona   = $this->request->data['id_persona'];
	 	$id_tipo_lote = $this->request->data['id_tipo_lote'];
	 	$this->loadModel("Lote");
	 	
	 	
	 	
	 	
	 			
	 	$existe = $this->Lote->find('count',
	 					array('conditions' => array(
	 							'Lote.codigo' => $codigo_lote,
	 							'Persona.id !=' =>$id_Persona,
	 							'Persona.id_tipo_persona =' =>$id_tipo_persona_Persona,
	 							'Persona.id_estado_persona =' => $id_estado_persona,
	 					)
	 					));
	 			
	 			$data = $existe;
	 			
	 			
	 			if($llamada_interna == 0){
	 				
	 				if($data >0)
	 					$error = EnumError::ERROR;
	 					else
	 						$error = EnumError::SUCCESS;
	 						
	 						$output = array(
	 								"status" => $error,
	 								"message" => "",
	 								"content" => $data
	 						);
	 						$this->set($output);
	 						$this->set("_serialize", array("status", "message", "content"));
	 						
	 						
	 						
	 						
	 			}else{
	 				
	 				return  $data;
	 			}
	 }
	 
	 
	 /**
	  * @secured(BTN_EXCEL_LOTES)
	  */
	 public function excelExport($vista="default",$metodo="index",$titulo=""){
	 	
	 	parent::excelExport($vista,$metodo,"Listado de Lotes");
	 	
	 	
	 	
	 }
}
?>