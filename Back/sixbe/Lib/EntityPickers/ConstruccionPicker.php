<?php
class ConstruccionPicker extends EntityPicker{
    
    public $model = "Construccion";
    
    public $pickerTitle = "Selección de Construcción";
    
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Nro. Construccion", "field" => "nro_construccion", "width" => "30%", "keyOrder" => "nro_construccion"),
                                );

    public function getPickerConditions($busqueda){
        
        return array('Construccion.nro_construccion LIKE' => '%' . $busqueda['bus_unico'] . '%');
    }
    
    public function getPickerJoins() {
        
        return DataFilter::getConstruccionPickerJoinFilter();
        
    }

}