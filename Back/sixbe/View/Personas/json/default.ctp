<?php
        // CONFIG  CON MODE DISPLAYED CELLS
       // include(APP.'View'.DS.'Comprobantes'.DS.'json'.DS.'default.ctp');
       include(APP.'View'.DS.'Personas'.DS.'json'.DS.'persona.ctp');
       
        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "C&oacute;digo";
        $model["codigo"]["width"] = "5";
        $model["codigo"]["order"] = "0";
        $model["codigo"]["text_colour"] = "#000000";
   
        // CONFIG  CON MODE DISPLAYED CELLS
        $model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["header_display"] = "Raz&oacute;n Social";
        $model["razon_social"]["width"] = "10";
        $model["razon_social"]["order"] = "2";
        $model["razon_social"]["text_colour"] = "#000000";

        $model["cuit"]["show_grid"] = 1;
        $model["cuit"]["header_display"] = "CUIT";
        $model["cuit"]["width"] = "7";
        $model["cuit"]["order"] = "3";
        $model["cuit"]["text_colour"] = "#0000FF";
        
        
        $model["fecha_creacion"]["show_grid"] = 1;
        $model["fecha_creacion"]["header_display"] = "Fecha Creaci&oacute;n";
        $model["fecha_creacion"]["width"] = "8";
        $model["fecha_creacion"]["order"] = "4";
        $model["fecha_creacion"]["text_colour"] = "#0000FF";
        $model["fecha_creacion"]["mask"] = "00/00/0000"; //esto convierte la grilla
        
        $model["tel"]["show_grid"] = 1;
        $model["tel"]["header_display"] = "Tel&eacute;fono";
        $model["tel"]["width"] = "5";
        $model["tel"]["order"] = "5";
        $model["tel"]["text_colour"] = "#000000";
        
		$model["d_tipo_iva"]["show_grid"] = 1;
        $model["d_tipo_iva"]["header_display"] = "Tipo Iva";
        $model["d_tipo_iva"]["width"] = "5";
        $model["d_tipo_iva"]["order"] = "6";
        $model["d_tipo_iva"]["text_colour"] = "#005500";
        
        $model["d_provincia"]["show_grid"] = 1;
        $model["d_provincia"]["header_display"] = "Provincia";
        $model["d_provincia"]["width"] = "5";
        $model["d_provincia"]["order"] = "7";
        $model["d_provincia"]["text_colour"] = "#000000";
        
        $model["d_pais"]["show_grid"] = 1;
        $model["d_pais"]["header_display"] = "Pais";
        $model["d_pais"]["width"] = "5";
        $model["d_pais"]["order"] = "8";
        $model["d_pais"]["text_colour"] = "#000000";
        
        $model["d_persona_categoria"]["show_grid"] = 1;
        $model["d_persona_categoria"]["header_display"] = "Categor&iacute;a";
        $model["d_persona_categoria"]["width"] = "20";
        $model["d_persona_categoria"]["order"] = "8";
        $model["d_persona_categoria"]["text_colour"] = "#000000";
		
		$model["d_estado_persona"]["show_grid"] = 1;
        $model["d_estado_persona"]["header_display"] = "Estado";
        $model["d_estado_persona"]["width"] = "5";
        $model["d_estado_persona"]["order"] = "9";
        $model["d_estado_persona"]["text_colour"] = "#660000";
		
		$model["PersonaImpuestoAlicuota"]["show_grid"] = 0;
        $model["PersonaImpuestoAlicuota"]["header_display"] = "Impuestos";
        $model["PersonaImpuestoAlicuota"]["width"] = "2";
        $model["PersonaImpuestoAlicuota"]["order"] = "10";
        $model["PersonaImpuestoAlicuota"]["text_colour"] = "#000000";
?>