<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class Movimiento extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'movimiento';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		/*'id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Debe ingresar un Id.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
           
		),*/
        
	);

    
    
 
 
 public $belongsTo = array(
        'Producto' => array(
            'className' => 'Producto',
            'foreignKey' => 'id_producto',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'id_usuario',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'MovimientoTipo' => array(
            'className' => 'MovimientoTipo',
            'foreignKey' => 'id_movimiento_tipo',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'Componente' => array(
            'className' => 'Componente',
            'foreignKey' => 'id_componente',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'MateriaPrima' => array(
            'className' => 'MateriaPrima',
            'foreignKey' => 'id_materia_prima',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'Comprobante' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'OrdenArmado' => array(
            'className' => 'OrdenArmado',
            'foreignKey' => 'id_orden_armado',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'OrdenProduccion' => array(
            'className' => 'OrdenProduccion',
            'foreignKey' => 'id_orden_produccion',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'Lote' => array(
            'className' => 'Lote',
            'foreignKey' => 'id_lote',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ComprobanteItem' => array(
            'className' => 'ComprobanteItem',
            'foreignKey' => 'id_comprobante_item',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
 		
 		'DepositoOrigen' => array(
 				'className' => 'Deposito',
 				'foreignKey' => 'id_deposito_origen',
 				'conditions' => '',
 				'fields' => '',
 				'order' => ''
 		),
 		
 		'DepositoDestino' => array(
 				'className' => 'Deposito',
 				'foreignKey' => 'id_deposito_destino',
 				'conditions' => '',
 				'fields' => '',
 				'order' => ''
 		)
 		
        
 );
 
 
 
 public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
        
         
         
         $this->virtual_fields_view = array(
         
         "d_usuario_nombre"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "nro_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "tipo_movimiento"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_producto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_materia_prima"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_componente"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "producto_codigo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "pto_nro_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_deposito_destino"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "d_deposito_origen"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         "codigo_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
         );
         
         }    
 
 
 
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	/*public $hasMany = array(
		'Region' => array(
			'className' => 'Region',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public $childRecordMessage = "Existe por lo menos una regi&oacute;n, predio, establecimiento o usuario relacionado/a con la jurisdicci&oacute;n.";
    
    */
    
 
 /*
 public function SaveMovimiento($data){
        
        if(isset($data)){ 
                      
                      
                          $this->saveAll($data);
                          
        }   
        
        
    }
    
  */ 
  
  
  
  /*
  
PI se modifica la cantidad virtual



Factura modifica la real y la virtual

para afectar la virtual hay que pasarle a la funcion el id_item_comprobante_origen luego buscar en la tabla movimientos id_comprobante_item cone ese id
hacer la sumatoria de items y hacer el movimiento por ese total tomando el id_deposito del padre de cada item.
SOLO SE APLICA EN EL IMPORTAR. O sea en aquellos items que tengan seteado el origen
*/
  
  public function revertirItems($comprobante ='',$comprobante_item ='' ) { //id item del PI
      
      
      
      
      
    
              
              
              if(count($comprobante["ComprobanteItem"])>0){

                foreach($comprobante["ComprobanteItem"] as $item){
                    
                    
                    if(isset($item["ComprobanteItem"]))
                        $item = $item["ComprobanteItem"];
                     
                    $this->revertirItem($item,$comprobante["Comprobante"]["id_tipo_comprobante"]);
              
               }   
              }
      
     
     
         
         
} 
      
      
      

  /*
    Revierte los items del origen o sea si se manda a invertir una 
    factura que esta hecha desde un PI entonces revertiria los movimientos del PI
  */
  public function revertirItem($item,$tipo_comprobante=''){ 
  
      
      $stock_producto = ClassRegistry::init('StockProducto');
      $movimiento_insertar = ClassRegistry::init('Movimiento');
      $comprobante_item = ClassRegistry::init('ComprobanteItem');
      //$item = $item["ComprobanteItem"];
       $id_producto = $item["id_producto"];            
                  
  if( isset($item["id_comprobante_item_origen"]) && $item["id_comprobante_item_origen"]>0   ){
                  
                  
              $item_comprobante_origen = $item["id_comprobante_item_origen"]; 
              
              
             /*Busco si ya reverti al item*/ 
             /* $existe_movimiento_revertido_anterior = $this->find('all', array(
                                         'conditions' => array('Movimiento.id_movimiento_tipo' => EnumTipoMovimiento::RevertirLoteItems,'Movimiento.id_comprobante_item'=>$item_comprobante_origen),
                                         //'contain' =>array('ComprobanteItem'=>array('Producto')),
                                       
                                     ));
               */
               
                                     
              //Busco la cantidad del Item del Comprobante original para saber cuando se revirtio anteriormente. Puede haber revertido solo una parte. Como es el caso de factura con PI.
              
              $comprobante_item_original = $comprobante_item->find('all', array(
                                         'conditions' => array('ComprobanteItem.id' =>$item_comprobante_origen,'ComprobanteItem.activo'=>1),
                                         //'contain' =>array('ComprobanteItem'=>array('Producto')),
                                       
                                     ));
                                     
              /*
              if($comprobante_item_original[0]["ComprobanteItem"]["id_comprobante_item_origen"] >0 ){ 
                
                $comprobante_item_original = $comprobante_item->find('all', array(
                                         'conditions' => array('ComprobanteItem.id' =>$comprobante_item_original[0]["ComprobanteItem"]["id_comprobante_item_origen"],'ComprobanteItem.activo'=>1),
                
                                       
                                     ));  
                  
              }*/
                                     
              $cantidad_original_item = $comprobante_item_original[0]["ComprobanteItem"]["cantidad"];
              
                                     
             if($comprobante_item_original[0]["ComprobanteItem"]["id_comprobante_item_origen"])
                $id_item_origen_considerar =  $comprobante_item_original[0]["ComprobanteItem"]["id_comprobante_item_origen"];
             else
                $id_item_origen_considerar = $item_comprobante_origen;
                
             
             
             $existe_movimiento_revertido_anterior = $this->find('all', array(
                                        'fields'=>array('SUM(Movimiento.cantidad) as cant'),
                                         'conditions' => array('Movimiento.id_movimiento_tipo' => EnumTipoMovimiento::RevertirLoteItems,'Movimiento.id_comprobante_item'=>$id_item_origen_considerar),
                                         //'contain' =>array('ComprobanteItem'=>array('Producto')),
                                       
                                     ));                        
                                    
              
              if(isset($existe_movimiento_revertido_anterior[0][0]["cant"]))
                $total =  $existe_movimiento_revertido_anterior[0][0]["cant"]; //cantidad
              else
                $total = 0;
                
              //chequear el Prod Comprom
              
              if($total <  $cantidad_original_item  ){ //Si la cantidad de items que revirtio es menor a la total entonces si puedo seguir revirtiendo
                             
                              
                               $sumatoria_movimientos = $this->find('all', array(
                                                         'fields'=>array('SUM(Movimiento.cantidad) as cant','Movimiento.id_deposito_origen','Movimiento.id_deposito_destino'),
                                                         'conditions' => array('Movimiento.id_comprobante_item' => $id_item_origen_considerar),
                                                         //'contain' =>array('ComprobanteItem'=>array('Producto')),
                                                         'group' => array('Movimiento.id_comprobante_item')
                                                     ));  
                                                     
                  
                              
                             switch($tipo_comprobante){
                                 
                                 case EnumTipoComprobante::FacturaA:
                                 case EnumTipoComprobante::FacturaB:
                                 case EnumTipoComprobante::FacturaC:
                                 case EnumTipoComprobante::FacturaE:
                                 case EnumTipoComprobante::FacturaM:
                                 case EnumTipoComprobante::NotaCreditoA:
                                 case EnumTipoComprobante::NotaCreditoB:  
                                 case EnumTipoComprobante::NotaCreditoC:  
                                 case EnumTipoComprobante::NotaCreditoE:  
                                 case EnumTipoComprobante::Remito:
                                 case EnumTipoComprobante::FacturaCreditoA:
                                 case EnumTipoComprobante::FacturaCreditoB:
                                 case EnumTipoComprobante::FacturaCreditoC: 
                                 $total = $item["cantidad"]; //TOMO LA CANTIDAD DE LA FACTURA XQ ESTOY DEVOLVIENDO PRODUCTO COMPROMETIDO O EN EL CASO DE LA NOTA DE C.
                                 break;
                                 default:
                                     if($sumatoria_movimientos)
                                        $total =  $sumatoria_movimientos[0][0]["cant"]; //cantidad
                                 break;
                                 
                             }
                              
                              
                              
                              
                              if($total > 0 && count($sumatoria_movimientos) > 0){ //el count($sumatoria_movimientos)>0  es por el tema del stock solo lo hace si tuvo movimientos de stock el item por el cual estoy revirtiendo 
                                  
                                  
                                  
                                      $id_deposito_origen =  $sumatoria_movimientos[0]["Movimiento"]["id_deposito_origen"]; //cantidad a revertir en origen (le sumo)
                                      $id_deposito_destino =  $sumatoria_movimientos[0]["Movimiento"]["id_deposito_destino"]; //cantidad a revertir en destino (le resto)
                                      
                                      if(!$id_deposito_origen)
                                            $id_deposito_origen = 6; //por default saca de comprometido de producto
                                      
                                     
                                      
                                      
                                      if($id_deposito_origen>0){
                                          
                                             $stock_producto->updateAll(
                                                                                array('StockProducto.stock' => "StockProducto.stock + ".$total  ),
                                                                                array('StockProducto.id' => $id_producto,'StockProducto.id_deposito' => $id_deposito_origen) ); 
                                            
                                      }
                                      
                                      
                                      
                                      if($id_deposito_destino>0){
                                          
                                             $stock_producto->updateAll(
                                                                                array('StockProducto.stock' => "StockProducto.stock - ".$total  ),
                                                                                array('StockProducto.id' => $id_producto,'StockProducto.id_deposito' => $id_deposito_destino) ); 
                                                                             
                                      }
                                      
                                      
                                      
                                        $movimiento = array();
                                        $movimiento["id_comprobante"] = $item["id_comprobante"];
                                        $movimiento["cantidad"] =  $total;
                                        $movimiento["id_comprobante_item"] = $item_comprobante_origen;
                                        $movimiento["id_movimiento_tipo"] = EnumTipoMovimiento::RevertirLoteItems;
                                        $movimiento["fecha"] = date("Y-m-d H:i:s");

                                        if($id_deposito_origen > 0)
                                        $movimiento["id_deposito_origen"] = $id_deposito_origen;

                                        if($id_deposito_destino > 0)
                                        $movimiento["id_deposito_destino"] = $id_deposito_destino;

                                        $movimiento["id_usuario"] = CakeSession::read("Auth.User.id");


                                        //array_push($movimientos,$movimiento);

                                        $movimiento_insertar->Save($movimiento);                                        
                                                 
                                      
                                      
                                      
                                      
                                      
                                      
                                      
                                      
                            } 
              
           }   
      }
      
  }
  
  
  
  
  public function  Anular($id_movimiento){
      
      $movimiento = $this->find('first', array(
                                                'conditions' => array('Movimiento.id' => $id_movimiento),
                                                'contain' => false
                                                ));
                                                
      $stock_producto = ClassRegistry::init('StockProducto');
      $movimiento_insertar = ClassRegistry::init('Movimiento');
      $comprobante_item = ClassRegistry::init('ComprobanteItem');
      $producto_obj = ClassRegistry::init('Producto');
      $comprobante_obj= ClassRegistry::init('Comprobante');
      
      
                                                
      if($movimiento){
          
          $id_comprobante = $movimiento["Movimiento"]["id_comprobante"];
          
          
          $comprobante = $comprobante_obj->find('first', array(
          		'conditions' => array('Comprobante.id' =>$id_comprobante),
          	    'contain' =>false,
          		
          ));
          
          $nombre_campo_q_afecta_stock = $comprobante_obj->NombreComprobanteItemAfectaStock();
          
          if($comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::InformeRecepcionMateriales)
          	$nombre_campo_q_afecta_stock = "cantidad_cierre";
          
          	
          
          $comprobante_items = $comprobante_item->find('all', array(
                                         'conditions' => array('ComprobanteItem.id_comprobante' =>$id_comprobante,'ComprobanteItem.activo'=>1),
                                         //'contain' =>array('ComprobanteItem'=>array('Producto')),
                                       
                                     ));
                                     
          $id_deposito_origen =  $movimiento["Movimiento"]["id_deposito_destino"];
          $id_deposito_destino =  $movimiento["Movimiento"]["id_deposito_origen"];
          $movimientos_items = array();
           foreach($comprobante_items as $comprobante){ //por cada producto del ComprobanteItem
                  $id_producto = $comprobante["ComprobanteItem"]["id_producto"];
                  
                  
                  
                  
                 
                  	
                  	
                  	
                  $cantidad = $comprobante["ComprobanteItem"][$nombre_campo_q_afecta_stock];
                  
                  $tiene_stock_producto = $producto_obj->AceptaStock($id_producto);
                  
                  
                  if($id_deposito_origen > 0 && $tiene_stock_producto == 1)
                   $stock_producto->updateAll(
                        array('StockProducto.stock' => "StockProducto.stock -".$cantidad  ),
                        array('StockProducto.id' => $id_producto,'StockProducto.id_deposito'=>$id_deposito_origen) );
                    
                   
                   if($id_deposito_destino > 0 && $tiene_stock_producto == 1)
                    $stock_producto->updateAll(
                        array('StockProducto.stock' => "StockProducto.stock +".$cantidad  ),
                        array('StockProducto.id' => $id_producto,'StockProducto.id_deposito'=>$id_deposito_destino) );
                        
                        
                        
             
                   $movimiento = array();
                   $movimiento["id_comprobante"] = $id_comprobante;
                   $movimiento["cantidad"] = $cantidad;
                   $movimiento["id_comprobante_item"] = $comprobante["ComprobanteItem"]["id"];
                   $movimiento["id_movimiento_tipo"] = EnumTipoMovimiento::AjusteNegativoComprobanteItem;
                   $movimiento["fecha"] = date("Y-m-d H:i:s");
                   if($id_deposito_origen > 0)
                    $movimiento["id_deposito_origen"] = $id_deposito_origen;
                       if($id_deposito_destino > 0)
                        $movimiento["id_deposito_destino"] = $id_deposito_destino;
                       
                       $movimiento["id_usuario"] = CakeSession::read("Auth.User.id");
                       array_push($movimientos_items,$movimiento);
                       
                       
                 
                     
        }
        
          if(count($movimientos_items)>0)
          	$movimiento_insertar->saveAll($movimientos_items);
          
      }
  
  }
  
  
  public function afterFind($results, $primary = false) {
    
     App::uses('Modulo', 'Model');
     $modulo_obj = new Modulo();
     $cantidad_decimales = $modulo_obj->getCantidadDecimalesFormateo(EnumModulo::STOCK); 
    
    foreach ($results as $key => $val) {
        
        if (isset($val['Movimiento']['cantidad'])) {
            $results[$key]['Movimiento']['cantidad'] = (string) round($results[$key]['Movimiento']['cantidad'],$cantidad_decimales);
    }
    
  
    }
    

    return $results;
}



public function addMovimiento($movimiento){
	
	
	$stock_producto = ClassRegistry::init('StockProducto');
	$movimiento_insertar = ClassRegistry::init('Movimiento');
	
	
	switch($movimiento["id_movimiento_tipo"]){

		
		case EnumTipoMovimiento::AjusteStock:
			
			
			if(count($movimiento)>0)
				$movimiento_insertar->saveAll($movimiento);
				
				
				if(isset($movimiento["id_deposito_destino"]) && $movimiento["id_deposito_destino"]>0)
					$stock_producto->updateAll(
							array('StockProducto.stock' => "StockProducto.stock +".$movimiento["cantidad"]),
							array('StockProducto.id' => $movimiento["id_producto"],'StockProducto.id_deposito'=>$movimiento["id_deposito_destino"]) );
			break;
		
		
		
	}
	
	
}

  
}
