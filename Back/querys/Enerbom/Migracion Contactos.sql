


SET FOREIGN_KEY_CHECKS = 0;

INSERT INTO persona (
  id_tipo_persona,
  id_estado_persona,
  nombre,
  apellido,
  fecha_nacimiento,
  fecha_ingreso,
  fecha_egreso,
  id_tipo_documento,
  pasaporte,
  pasaporte_fecha_vto,
  pasaporte_fecha_creacion,
  horario_contacto,
  calle,
  numero_calle,
  localidad,
  tel,
  tel2,
  interno,
  email,
  id_persona_padre,
  d_persona
) 
SELECT 
  6,
  21,
  nombre,
  apellido,
  fecha_nacimiento,
  fecha AS fecha_ingreso,
  NULL AS fecha_egreso,
  1 AS tipo_documento,
  pasaporte,
  pasaporte_fecha_vto,
  pasaporte_fecha_creacion,
  horario_contacto ,
  calle,
  '',
  localidad,
  tel1,
  tel2,
  interno1,
  email,
  id_persona,
  d_contacto
FROM
  valuarit_pristine.contacto ;


SET FOREIGN_KEY_CHECKS = 1;




