<?php
App::uses('ComprobantesController', 'Controller');


  /**
    * @secured(CONSULTA_QAINFORME)
  */
class QAInformesController extends ComprobantesController {
    
    public $name = 'QAInformes';
    public $model = 'Comprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $requiere_impuestos = 0;
    
    /**
    * @secured(CONSULTA_QAINFORME)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros($this->model);
            
            
                       
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>array('Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante','DepositoOrigen','DepositoDestino','Transportista','PuntoVenta'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.id desc'
        ); //el contacto es la sucursal
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
    
    
        
        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'QAINFORME.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'QAINFORME.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
            
             //$valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
             //unset($valor['ComprobanteItem']);
             
             if(isset($valor['TipoComprobante'])){
                 $valor[$this->model]['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                 $valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
                 unset($valor['TipoComprobante']);
             }
             
             $valor[$this->model]['nro_comprobante'] = (string) str_pad($valor[$this->model]['nro_comprobante'], 8, "0", STR_PAD_LEFT);
             
             /*
             foreach($valor[$this->model]['ComprobanteItem'] as &$producto ){
                 
                 $producto["d_producto"] = $producto["Producto"]["d_producto"];
                 $producto["codigo_producto"] = $producto["Producto"]["codigo"];
                 
                 
                 unset($producto["Producto"]);
             }
             */
           
             if(isset($valor['Moneda'])){
                 $valor[$this->model]['d_moneda'] = $valor['Moneda']['d_moneda'];
                 unset($valor['Moneda']);
             }
             
             if(isset($valor['EstadoComprobante'])){
                $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
                unset($valor['EstadoComprobante']);
             }
             if(isset($valor['CondicionPago'])){
                $valor[$this->model]['d_condicion_pago'] = $valor['CondicionPago']['d_condicion_pago'];
                unset($valor['CondicionPago']);
             }
             
             if(isset($valor['DepositoOrigen'])){
                $valor[$this->model]['id_deposito_origen'] = $valor['DepositoOrigen']['id'];
                $valor[$this->model]['d_deposito_origen'] = $valor['DepositoOrigen']['d_deposito'];
                unset($valor['DepositoOrigen']);
             }
             
              if(isset($valor['DepositoDestino'])){
                $valor[$this->model]['id_deposito_destino'] = $valor['DepositoDestino']['id'];
                $valor[$this->model]['d_deposito_destino'] = $valor['DepositoDestino']['d_deposito'];
                unset($valor['DepositoDestino']);
             }
             if(isset($valor['Transportista'])){
                $valor[$this->model]['t_razon_social'] = $valor['Transportista']['razon_social'];
                $valor[$this->model]['t_direccion'] = $valor['Transportista']['calle'].' '.$valor['Transportista']['numero_calle'];
                unset($valor['Transportista']);
             }
             
             if(isset($valor['Persona'])){
            
            
                 if($valor['Persona']['id']== null){
                     
                    $valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social']; 
                 }else{
                     
                     $valor[$this->model]['razon_social'] = $valor['Persona']['razon_social'];
                 }
                 
                 unset($valor['Persona']);
             }
             
                if(isset($valor['DepositoOrigen'])){
                  $valor['Comprobante']['d_deposito_origen'] =$valor['DepositoOrigen']['d_deposito_origen'];
              }
                 
              if(isset($valor['DepositoDestino'])){
                    $valor['Comprobante']['d_deposito_destino'] =$valor['DepositoDestino']['d_deposito_destino'];
               }
               
               
                 if(isset($valor['PuntoVenta'])){
                    $valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
                    unset($valor['PuntoVenta']);
             }
            
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    /**
    * @secured(ABM_USUARIOS)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->QAINFORME->id = $id;
            $this->QAINFORME->contain();
            $this->request->data = $this->QAINFORME->read();
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('QAINFORME');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de QAINFORMEes');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de QAINFORMEes');
        else
            $formBuilder->setTituloForm('Consulta de QAINFORMEes');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('cuit', 'Cuit', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('calle', 'Calle', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  
        $formBuilder->addFormInput('tel', 'Tel&eacute;fono', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('ciudad', 'Ciudad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descuento', 'Descuento', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descripcion', 'Descripcion', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('localidad', 'Localidad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
         
        
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'ClienteAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('ClienteRazonSocial', 'Debe ingresar una Raz&oacute;n Social');
                validator.validateRequired('ClienteCuit', 'Debe ingresar un CUIT');
                validator.validateNumeric('ClienteDescuento', 'El Descuento ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_cuit',
                        'data': 'cuit=' + $('#ClienteCuit').val()+'&id='+$('#ClienteId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('ClienteCuit','El Cuit que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

    /**
    * @secured(ADD_QAINFORME)
    */
    public function add(){
    
     parent::add();    
        
    }
    
    
    /**
    * @secured(MODIFICACION_QAINFORME)
    */
    public function edit($id){
  
     parent::edit($id);    
        
    }
    
    
    /**
    * @secured(BAJA_QAINFORME)
    */
    public function delete($id){
        
     parent::delete($id);    
        
    }
    
   /**
    * @secured(BAJA_QAINFORME)
    */
    public function deleteItem($id,$externo=1){
        
        parent::deleteItem($id,$externo);    
        
    }
    
    
      /**
    * @secured(CONSULTA_QAINFORME)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
  
    /**
    * @secured(REPORTE_QAINFORME)
    */
    public function pdfExport($id,$vista=''){
        
        
        
        
        
    $datoEmpresa = $this->Session->read('Empresa');
    $chequea_remito_imprimir_solo_con_remitido  = $datoEmpresa["DatoEmpresa"]["chequea_remito_imprimir_solo_con_remitido"];//este flag si esta en 1 debe chequear que si el estado del Remito es Remitido entonces ahi deja imprimir    
        
        
        
        
        
    $this->loadModel("Comprobante");
    $remito = $this->Comprobante->find('first', array(
                                                        'conditions' => array('Comprobante.id' => $id),
                                                        'contain' =>array('TipoComprobante')
                                                        ));
                                                        
                                                        
     $imprimir  = 1;
     
     
     if( $chequea_remito_imprimir_solo_con_remitido == 1  && $remito && $remito["Comprobante"]["id_estado_comprobante"] != EnumEstadoComprobante::Remitido  ){
         
      $imprimir = 0;   
         
         
     }
     
     
    
     
        if($imprimir == 0){   //Debo devolver un Json porque no deberia imprimir
        
        
          $output = array(
            "status" =>EnumError::ERROR,
            "message" => "El Remito no puede imprimirse. Primero debe realizar el movimiento de Despacho de mercader&iacute;a.",
            "content" => "",
        );
        
        
        $this->autoRender = false;
        $this->response->type('json');
        echo json_encode($output);
       
        
        
        
        }else{                                                
                                                        
        
        
        
        if($remito){
            $comprobantes_relacionados = $this->getComprobantesRelacionados($id);
         
                                           
          
            
            
              
            $facturas_relacionados = array();
            $pedidos_internos_relacionados = array();


            foreach($comprobantes_relacionados as $comprobante){


             if(  isset($comprobante["Comprobante"]["id_tipo_comprobante"]) && in_array($comprobante["Comprobante"]["id_tipo_comprobante"],   $this->Comprobante->getTiposComprobanteController(EnumController::Facturas) ))
                array_push($facturas_relacionados,$this->Comprobante->GetNumberComprobante($comprobante["PuntoVenta"]["numero"],$comprobante["Comprobante"]["nro_comprobante"]));
            
            if(isset($comprobante["Comprobante"]["id_tipo_comprobante"]) && $comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::PedidoInterno)
                        array_push($pedidos_internos_relacionados,$this->Comprobante->GetNumberComprobante($comprobante["PuntoVenta"]["numero"],$comprobante["Comprobante"]["nro_comprobante"]));


            }
           
            
            
            
            $this->set('facturas_relacionados',$facturas_relacionados);  
            $this->set('pedidos_internos_relacionados',$pedidos_internos_relacionados);     
            parent::pdfExport($id);  
         }
            
       }
     
        
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
        
        
      
 
    }
    
    
    
    
}
?>