<?php

App::uses('PersonasController', 'Controller');


class SucursalesController extends PersonasController {
    public $name = 'Sucursales';
    public $model = 'Persona';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $id_tipo_persona = EnumTipoPersona::Sucursal;
  
  
    /**
    * @secured(CONSULTA_SUCURSAL)
    */
    public function index() {
      
           
       
        parent::index();

        
    }
    
    /**
    * @secured(MODIFICACION_SUCURSAL)
    */
    public function abm($mode, $id = null) {
        
        parent::abm($mode,$id);
        
    }

    /**
    * @secured(ABM_SUCURSAL)
    */
    public function view($id) {        
        parent::view($id);
    }

    /**
    * @secured(ADD_SUCURSAL)
    */
    public function add() {
        
        
        
       $this->request->data["Persona"]["id_tipo_persona"] = $this->id_tipo_persona;
        parent::add(); 
        
      
    }
   
   
   /**
    * @secured(MODIFICACION_SUCURSAL)
    */ 
   public function edit($id) {
       
        $this->request->data["Persona"]["id_tipo_persona"] = $this->id_tipo_persona;
        parent::edit($id);      
  }
    
  
    /**
    * @secured(BAJA_SUCURSAL)
    */
    function delete($id) {
        
        parent::delete($id);
        
    }
    
    
         /**
        * @secured(CONSULTA_SUCURSAL)
        */
        public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
            //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);

            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);
        }
    



   
    
                
                
                
                
    
    
    public function home(){
        parent::home();
    }
    
  
    /**
    * @secured(CONSULTA_SUCURSAL)
    */
    public function existe_cuit(){
        
       parent::existe_cuit();
    }
    
}
?>