<?php
/**
 * DataFilter
 *
 */
 
App::uses('CakeSession', 'Model/Datasource');

class DataFilter 
{
    
    /**
    * Devuelve si el filtro de datos se encuentra activo o no en la configuracion
    * 
    */
    private static function filterActive(){
        
        return Configure::read("dataFilterActive");
        
    }
    
    /**
    * Devuelve la Jurisdiccion Asignada al usuario
    * 
    */
    public static function getIdJurisdiccionUsuario(){
        
        $ret = null;
        
        if(CakeSession::check('Auth.User.id_jurisdiccion'))
            $ret = CakeSession::read('Auth.User.id_jurisdiccion');
        
        return $ret;
        
    }
    
    /**
    * Devuelve la Region Asignada al usuario
    * 
    */
    public static function getIdRegionUsuario(){
        
        $ret = null;

        if(CakeSession::check('Auth.User.id_region'))
            $ret = CakeSession::read('Auth.User.id_region');
        
        return $ret;
        
    }
    
    /**
    * Devuelve el Lote Asignado al usuario
    * 
    */
    public static function getIdLoteUsuario(){
        
        $ret = null;

        if(CakeSession::check('Auth.User.id_lote'))
            $ret = CakeSession::read('Auth.User.id_lote');
        
        return $ret;
        
    }
    
    /*===========================================================================
    ============== Métodos para filtrado de Pickers ========================
    ===========================================================================*/
    
    /**
    * Filtra el picker de Jurisdiccion con la Jurisdiccion asignada al usuario
    * 
    */
    public static function getJurisdiccionPickerFilter($model){
        
        if(!self::filterActive())
            return array();
        
        $id = self::getIdJurisdiccionUsuario();
        if(trim($id) != "" && $id != null && self::filterActive()) {
            return array("Jurisdiccion.id = " =>  $id);
        }
        
        return array();
        
    }
    
    /**
    * Filtra el picker de Region con la Jurisdiccion asignada al usuario
    * 
    */
    public static function getRegionPickerFilter(){
        
        if(!self::filterActive())
            return array();
        
        $id = self::getIdRegionUsuario();
        
        if(trim($id) != "" && $id != null && self::filterActive())
            return array("Region.id = " =>  $id);
        
        return array();
        
    }
    
    /**
    * Filtra el picker de Lote con la Jurisdiccion asignada al usuario
    * 
    */
    public static function getLotePickerFilter(){
        
        if(!self::filterActive())
            return array();
        
        $id = self::getIdLoteUsuario();
        
        $ret =  array();
        
        if(trim($id) != "" && $id != null && self::filterActive())
            $ret = array("Lote.id = " =>  $id);
        
        return $ret;
        
    }
    
    /**
    * Joinea el picker de Lote con los Lotes asignados al usuario
    * 
    */
    public static function getLotePickerJoinFilter(){
        
        if(!self::filterActive())
            return null;
        
        if(CakeSession::read('Auth.User.Rol.id') == EnumRol::Supervisor){
            
            return array(
                            'alias' => 'UsuarioLote',
                            'table' => 'usuario_lote',
                            'type' => 'INNER',
                            'conditions' => "`Lote`.`id` = `UsuarioLote`.`id_lote` and `UsuarioLote`.`id_usuario` = " . CakeSession::read('Auth.User.id')
                        );
            
        }else{
            
            return null;
            
        }
        
        
        
    }

    /**
    * Joinea el picker de Segmento con los Segmentos asignados al usuario
    * 
    */
    public static function getSegmentoPickerJoinFilter(){
        
        if(!self::filterActive())
            return null;
        
        if(CakeSession::read('Auth.User.Rol.id') == EnumRol::Censista){
            
            return array(
                            'alias' => 'UsuarioSegmento',
                            'table' => 'usuario_segmento',
                            'type' => 'INNER',
                            'conditions' => "`Segmento`.`id` = `UsuarioSegmento`.`id_segmento` and `UsuarioSegmento`.`id_usuario` = " . CakeSession::read('Auth.User.id')
                        );
            
        }else{
            
            return null;
            
        }
        
        
        
    }
    
     /**
    * Joinea con los CUis asignados al usuario si corresponde
    * 
    */
    public static function getAsignacionJoinFilter(){
        
        if(!self::filterActive())
            return null;
        
        if(CakeSession::read('Auth.User.Rol.id') == EnumRol::Censista){
            
            return array(
                            'alias' => 'Asignacion',
                            'table' => 'asignacion',
                            'type' => 'INNER',
                            'conditions' => "`Predio`.`id` = `Asignacion`.`id_predio` and `Asignacion`.`id_usuario` = " . CakeSession::read('Auth.User.id')
                        );
            
        }else{
            
            return null;
            
        }
        
        
        
    }
    
    /**
    * Joinea el picker de Predio con los filtros necesarios
    * 
    */
    public static function getPredioPickerJoinFilter(){
        
        $joins = array(
                        array(
                                'alias' => 'Segmento',
                                'table' => 'segmento',
                                'type' => 'INNER',
                                'conditions' => "`Segmento`.`id` = `Predio`.`id_segmento`"
                                ),
                        array(
                                'alias' => 'Lote',
                                'table' => 'lote',
                                'type' => 'INNER',
                                'conditions' => "`Lote`.`id` = `Segmento`.`id_lote`"
                                ),
                        array(
                                'alias' => 'Region',
                                'table' => 'region',
                                'type' => 'INNER',
                                'conditions' => "`Region`.`id` = `Lote`.`id_region`"
                                ),
                        array(
                                'alias' => 'Jurisdiccion',
                                'table' => 'jurisdiccion',
                                'type' => 'INNER',
                                'conditions' => "`Jurisdiccion`.`id` = `Region`.`id_jurisdiccion`"
                                ),
                      );
                      
        if(!self::filterActive())
            return $joins;
        
        //Join con UsuarioLote si corresponde
        $lotePickerJoinFilter = self::getLotePickerJoinFilter();
        if($lotePickerJoinFilter != null)
            array_push($joins, $lotePickerJoinFilter);
        
        //Join con UsuarioSegmento si corresponde
        $segmentoPickerJoinFilter = self::getSegmentoPickerJoinFilter();
        if($segmentoPickerJoinFilter != null)
            array_push($joins, $segmentoPickerJoinFilter);
        
        //Join con Asignacion si corresponde
        $asignacionPickerJoinFilter = self::getAsignacionJoinFilter();
        if($asignacionPickerJoinFilter != null)
            array_push($joins, $asignacionPickerJoinFilter);
            
        return $joins;
        
    }
    
    /**
    * Joinea el picker de Construccion con los filtros necesarios
    * 
    */
    public static function getConstruccionPickerJoinFilter(){
        
        
        $joins = array(
                        array(
                                'alias' => 'Predio',
                                'table' => 'predio',
                                'type' => 'INNER',
                                'conditions' => "`Predio`.`id` = `Construccion`.`id_predio`"
                                ),
                      );
                      
        $joins = array_merge($joins, $joins = self::getPredioPickerJoinFilter());
        
        return $joins;
        
    }
    
    /**
    * Joinea el picker de Local con los filtros necesarios
    * 
    */
    public static function getLocalPickerJoinFilter(){
        
        $joins = array(
                        array(
                                'alias' => 'Construccion',
                                'table' => 'construccion',
                                'type' => 'INNER',
                                'conditions' => "`Construccion`.`id` = `Local`.`id_construccion`"
                                ),
                      );
                      
        $joins = array_merge($joins, $joins = self::getConstruccionPickerJoinFilter());
        
        return $joins;
        
    }
    
    /*===========================================================================
    ============== Métodos para filtrado de Listados ==========================
    ===========================================================================*/
    
    public static function getForm1JoinFilter(){
        
        $joins = array();
                      
        if(!self::filterActive())
            return $joins;
        
        //Join con UsuarioLote si corresponde
        $lotePickerJoinFilter = self::getLotePickerJoinFilter();
        if($lotePickerJoinFilter != null)
            array_push($joins, $lotePickerJoinFilter);
        
        //Join con UsuarioSegmento si corresponde
        $segmentoPickerJoinFilter = self::getSegmentoPickerJoinFilter();
        if($segmentoPickerJoinFilter != null)
            array_push($joins, $segmentoPickerJoinFilter);
        
        //Join con Asignacion si corresponde
        $asignacionPickerJoinFilter = self::getAsignacionJoinFilter();
        if($asignacionPickerJoinFilter != null)
            array_push($joins, $asignacionPickerJoinFilter);
            
        return $joins;
        
    }
    
    public static function getForm1Filter($model){
        $filter = array();
        $filter = array_merge(self::getJurisdiccionPickerFilter($model), self::getRegionPickerFilter(), self::getLotePickerFilter());
        return $filter;
    }
    
    public static function getForm2JoinFilter(){
     
        return self::getForm1JoinFilter();
        
    }
    
    public static function getForm2Filter($model){
        
        return self::getForm1Filter($model);
        
    }
    
    public static function getForm3JoinFilter(){
     
        return self::getForm1JoinFilter();
        
    }
    
    public static function getForm3Filter($model){
        
        return self::getForm1Filter($model);
        
    }
    
    public static function getPlanosJoinFilter(){
     
        return self::getForm1JoinFilter();
        
    }
    
    public static function getPlanosFilter($model){
        
        return self::getForm1Filter($model);
        
    }
    
    public static function getPlanillaJoinFilter(){
     
        return self::getForm1JoinFilter();
        
    }
    
    public static function getPlanillaFilter($model){
        
        return self::getForm1Filter($model);
        
    }
    
    public static function getConsistenciaCensalJoinFilter(){
     
        return self::getForm1JoinFilter();
        
    }
    
    public static function getConsistenciaCensalFilter($model){
        
        return self::getForm1Filter($model);
        
    }
	
	public static function getPrediosJoinFilter(){
     
        return self::getForm1JoinFilter();
        
    }
    
    public static function getPrediosFilter($model){
        
        return self::getForm1Filter($model);
        
    }
	
	public static function getEstablecimientosJoinFilter(){
     
        return self::getForm1JoinFilter();
        
    }
    
    public static function getEstablecimientosFilter($model){
        
        return self::getForm1Filter($model);
        
    }
    
    
}
