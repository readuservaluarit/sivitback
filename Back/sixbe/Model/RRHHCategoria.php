<?php
App::uses('AppModel', 'Model');



class RRHHCategoria extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'rrhh_categoria';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	
	
    
    
    public $belongsTo = array(
    		'RRHHConvenio' => array(
    				'className' => 'RRHHConvenio',
    				'foreignKey' => 'id_rrhh_convenio',
    				'dependent' => false,
    				'conditions' => '',
    				'fields' => '',
    				
    		));


}
