<?php

class AreaExteriorPicker extends EntityPicker{
    
    public $model = "area_exterior";
    
    public $pickerTitle = "Selección de Area Exterior";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Nombre", "field" => "nombre", "width" => "80%", "keyOrder" => "nombre")
                                );

    public function getPickerConditions($busqueda){
        
        return array('Lote.nombre LIKE' => '%' . $busqueda['bus_unico'] . '%');
    }

}