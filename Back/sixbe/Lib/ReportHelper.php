<?php
  
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::import('Vendor', 'TCPDF', array('file' => 'phpjasperxml_0.9d'.DS.'class'.DS.'tcpdf'.DS.'tcpdf.php'));
App::import('Vendor', 'PHPJasperXML', array('file' => 'phpjasperxml_0.9d'.DS.'class'.DS.'PHPJasperXML.inc.php'));
  
  class ReportHelper{
      
      private $reportFolder = "Reportes"; // Carpeta de la app que contiene los reportes
      private $reportName = "";
      private $reportPath = "";
      private $showParameters = "false";
      private $parameters=array();
	  private $controller;
	  private $showReportMethod;
	  public  $const_show_report = "SHOW_REPORT";
      
	  
	  
      public function getReportUrl(){
		  
		  $url = Router::url(array('controller' => $this->controller->name, 'action' => $this->showReportMethod), true) . "?".$this->const_show_report."=true";
		  
          foreach($this->parameters as $clave=>$valor){
              $url.="&".$clave."=".$valor;
          }
          
          return $url;
          
          
      }
      
      public function setReportName($reportName){
	  
		  // Cuando setea el nombre del reporte seteo automaticamente el path
		  $dir_temp = new Folder(APP);
		  $this->reportPath = $dir_temp->path.$this->reportFolder.DS;
	  
          $this->reportName = $reportName;
		  
      }
      
      public function getReportName(){
          return $this->reportName;
      }
	  
	  public function setController($controller){
          $this->controller = $controller;
      }
      
      public function getController(){
          return $this->controller;
      }
	  
	  public function setShowReportMethod($showReportMethod){
          $this->showReportMethod = $showReportMethod;
      }
      
      public function getShowReportMethod(){
          return $this->showReportMethod;
      }
            
      public function setReportPath($reportPath){
          $this->reportPath = $reportPath;
      }
            
      public function addParameter($key, $value){
          $this->parameters[$key] = $value;
      }
      
      public function getReportScripts(){
          
          return "
            var handlerConsultar = function() {
                if( validateForm() ){
                    showReport();
                }
                    
                return false;
            };
        
            $( document ).ready(function () {
                $( '#btnGuardar').unbind( 'click' );
                $( '#btnGuardar').bind( 'click', handlerConsultar );
            });
            
            function getReportUrl(){
                var url = '".$this->getReportUrl()."';
                
				/*var filters = getReportFilters();
                for( var indice in filters ){
                    url += '&' + indice + '=' + filters[indice];
                }
				*/
                    
                return url;
            }
            
            function showReport(){
			
				
                map = window.open('', '_blank');
                if (!map) {
                    alert('Debe habilitar los popups en su navegador para visualizar correctamente los reportes.');
                    return;
                }
				
                var mapForm = map.document.createElement('form');
                mapForm.method = 'POST';
				
                mapForm.action = getReportUrl();
                map.document.body.appendChild(mapForm);
				
                //User y Pass
                var mapInput = document.createElement('input');
                mapInput.type = 'hidden';
                mapInput.name = 'userid';
                mapInput.value = 'joe';
                mapForm.appendChild(mapInput);
                var mapInput2 = document.createElement('input');
                mapInput2.type = 'hidden';
                mapInput2.name = 'password';
                mapInput2.value = 'password';
                mapForm.appendChild(mapInput2);
                
                //Filtros del Reporte
                var filters = getReportFilters();
                for( var indice in filters ){
                    var inputFiltro = document.createElement('input');
                    inputFiltro.type = 'hidden';
                    inputFiltro.name = indice;
                    inputFiltro.value = filters[indice];
                    mapForm.appendChild(inputFiltro);
                }
                
                mapForm.submit();
				
            }
			
        ";
          
          
      }
       
	  private function getReportFileFullPath(){
	  
			return $this->reportPath . $this->reportName;
	  
	  }
	   
	  public function showReport(){
	  
			$PHPJasperXML = new PHPJasperXML();
			
			$parameters = array();
			
			//Leo parametros del request
			if(isset($_REQUEST["p_cliente_desde"])){
				$parameters["id_cliente_desde"] = $_REQUEST["p_cliente_desde"];
			}
				
			if(isset($_REQUEST["p_cliente_hasta"])){
				$parameters["id_cliente_hasta"] = $_REQUEST["p_cliente_hasta"];
			}
            
            if(isset($_REQUEST["p_id_pedido_interno"])){
                $parameters["id_pedido_interno"] = $_REQUEST["p_id_pedido_interno"];
            }
            
             if(isset($_REQUEST["p_cliente_cuit"])){
                $parameters["cliente_cuit"] = $_REQUEST["p_cliente_cuit"];
            }
             if(isset($_REQUEST["p_fecha_emision"])){
                $parameters["fecha_emision"] = $_REQUEST["p_fecha_emision"];
            }
             if(isset($_REQUEST["p_razon_social"])){
                $parameters["razon_social"] = $_REQUEST["p_razon_social"];
            }
             if(isset($_REQUEST["p_id_factura"])){
                $parameters["id_factura"] = $_REQUEST["p_id_factura"];
            }
			
			//Paso parametros al reporte
			$PHPJasperXML->arrayParameter=$parameters;
			
			//Levanto el reporte
			$PHPJasperXML->load_xml_file($this->getReportFileFullPath());

			App::uses('ConnectionManager', 'Model');
			$dataSource = ConnectionManager::getDataSource('default');
			
			//Paso los datos de conexion al reporte
			$server = $dataSource->config['host'];
			$user = $dataSource->config['login'];
			$pass = $dataSource->config['password'];
			$db = $dataSource->config['database'];
			
			// Sobrescribo para que no aparezcan los resultados de debugging ya que sino daria un error al generar el pdf.
			Configure::write('debug',0);
			
			$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db);
			
			$PHPJasperXML->outpage("I");    //page output method I:standard output  D:Download file
			
			$this->render("jasper_pdf");
	  
	  
	  }
	   
  }
  
?>
