<?php
App::uses('AppModel', 'Model');

/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class RRHHVariable extends AppModel {
	
	
	private $codigos = array("SJO"=>1,"TSR"=>4,"THE"=>5,"ANT"=>6,"TAU"=>9,"TRE"=>10,"X"=>11,"UIC"=>13,"DTM"=>14,"DTS"=>15,"CDM"=>17,"CDS"=>18);

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'rrhh_variable';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	
    
    
    public function getEquivalencia($key){
    	
    	
    	
    	
    	$result = isset($this->codigos[$key]) ? $this->codigos[$key] : null;
    	
    	return $result;
    	
    }
	


}
