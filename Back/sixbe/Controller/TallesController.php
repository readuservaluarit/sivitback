<?php

/**
* @secured(CONSULTA_TALLE)
*/
class TallesController extends AppController {
    public $name = 'Talles';
    public $model = 'Talle';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
/**
* @secured(CONSULTA_TALLE)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
       
       
        $nombre = $this->getFromRequestOrSession('Talle.d_talle');
        
        $conditions = array(); 
        
        
        
        if($nombre!='')
            array_push($conditions,array('LOWER(Talle.d_talle) LIKE' => '%' . $nombre . '%'));
        

        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de Marcas', 'Datos de las Marcas', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_TALLE', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $d_TALLE));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'Marca.id', "10%");
                $formBuilder->addHeader('Nombre', 'Marca.d_TALLE', "50%");
                $formBuilder->addHeader('Nombre', 'Marca.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_TALLE');
                $formBuilder->addField($this->model, 'cotizacion');
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        	
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->Marca->id = $id;
            $this->request->data = $this->Marca->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Marca');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Marca');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Marca');
        else
            $formBuilder->setTituloForm('Consulta de Marca');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_TALLE', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('cotizacion', 'Cotizaci&oacute;n', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));    
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'MarcaAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('MarcaDMarca', 'Debe ingresar un nombre');
        validator.validateRequired('MarcaCotizacion', 'Debe ingresar una cotizaci&oacute;n');


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_TALLE)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_cat = '';
            
            try{
                if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "El Talle ha sido creada exitosamente";
                    $tipo = EnumError::SUCCESS;
                    $id_cat = $this->{$this->model}->id;
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el Talle no ha podido ser creada.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" =>$id_cat
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content" ,"id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }

    /**
    * @secured(MODIFICACION_TALLE)
    */
     
    public function edit($id) {
        
        

            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "El Talle ha sido modificada exitosamente";
                     $status = EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
               }
               
              
            }catch(Exception $e){
            	$status = EnumError::ERROR;
                $mensaje = $e->getMessage();
                
            }
            
            $output = array(
            		"status" => $status,
            		"message" => $mensaje,
            		"content" => ""
            );
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content"));
           
       
    }

    /**
    * @secured(BAJA_TALLE)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->{$this->model}->id = $id;
     
       try{
            if ($this->{$this->model}->delete() ) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "El Talle ha sido eliminado exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }
    
     /**
    * @secured(CONSULTA_TALLE)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
        
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        
            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);
        
               
        
        
      
    }



}
?>