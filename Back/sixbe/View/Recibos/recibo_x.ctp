<?php   




App::import('Vendor','tcpdf/mytcpdf');
App::import('Model','Comprobante');
$comprobante = new Comprobante();




$html = $comprobante->getHeaderPDF($datos_pdf,$datos_empresa,'RECIBO');


$pdf = new MyTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
     
$pdf->set_datos_header($html); 
$descuento = (($datos_pdf["Comprobante"]["total_comprobante"]*$datos_pdf["Comprobante"]["descuento"])/100) ;




$impuestos = '';
$suma_impuestos = 0;
if(isset($datos_pdf["ComprobanteImpuesto"])){

        foreach($datos_pdf["ComprobanteImpuesto"] as $impuesto){

            $impuestos .= ' <tr>
                 <td>'.$impuesto["Impuesto"]["d_impuesto"].'</td>
                 <td>'.money_format('%!n',$impuesto["Impuesto"]["base_imponible"]).'</td>
                 <td>'.money_format('%!n',$impuesto["tasa_impuesto"]).'%</td>
                <td>'.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n',$impuesto["importe_impuesto"]).'</td>
               </tr>';
               
            $suma_impuestos = $suma_impuestos + $impuesto["importe_impuesto"];
               
        }
} 


$total_comprobante = $datos_pdf["Comprobante"]["total_comprobante"]  + $diferencia;

App::import('Vendor', 'NumberToLetterConverter', array('file' => 'Classes/NumberToLetterConverter.class.php'));   
$numero_conversion = new NumberToLetterConverter();
$letras_total =  "Importe en letras: ".$numero_conversion->to_word(str_replace(".",",",$total_comprobante),$datos_pdf["Moneda"]["simbolo_internacional"]);






$footer='
  <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>                     
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody> 
</table>


<table width="100%" border="1" style="width:100%;">
   <tr>
     <td width="60%">Observaciones: '.$datos_pdf["Comprobante"]["observacion"].' '.$letras_total.'</td>
     <td width="40%">
     <table width="100%" border="0">
      
      
       <tr>
         <td><strong>TOTAL</strong></td>
         <td>&nbsp;</td>
         <td><strong>'.$datos_pdf["Moneda"]["simbolo_internacional"].' '.money_format('%!n',$total_comprobante).'</strong></td>
       </tr>
     </table></td>
   </tr>';
   
   
    if( $datos_pdf["Comprobante"]["cae"] != '' ){
   
  $footer .= '<tr>
     <td > CAE: '.$datos_pdf["Comprobante"]["cae"].'</td>
     <td > Fecha Vto.: '.$datos_pdf["Comprobante"]["vencimiento_cae"].'</td>
   </tr>';
   
  }
  
  
   $footer .= '<tr>
     <td > Original blanco / copia color</td>

   </tr>';
  
  
  
   
   
   

$footer .='</table>';


   
   $html .= "<HR>";
   
   
   if( $datos_pdf["Comprobante"]["cae"] != '' ){
   
      $html.=$pdf->SetBarcode($codigo_barras,I25);
     
   
   }
   
   
   $html .= '
   
   <table border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;">

   
     <tbody>
        <tr >
        <td colspan="2">
        
        </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
     </tbody>
</table>
   
  ';
  
  $pdf->set_datos_footer($footer);
  
  
ob_clean();


$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sivit by Valuarit');
$pdf->SetTitle('Factura');
$pdf->SetSubject('Factura');


                              


$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));


$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);


$pdf->SetMargins(PDF_MARGIN_LEFT, 50, 20);
$pdf->SetHeaderMargin(10);
$pdf->SetFooterMargin(65);


$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);


$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('helvetica', '', 12, '', true);

$pdf->AddPage();


$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));


$tbody = "";

$renglones = $datos_pdf["ComprobanteItemComprobante"];

$data = array();
$comprobante = new Comprobante();
$datoEmpresa = $this->Session->read('Empresa');

$suma_diferencia_cambio = 0;
$suma_comprobantes = 0;


foreach($renglones as $renglon){

		$fecha_vencimiento = new DateTime($renglon['ComprobanteOrigen']['fecha_vencimiento']);
		$fecha_contable = new DateTime($renglon['ComprobanteOrigen']['fecha_contable']);
	
	
  	
   		if($renglon['ComprobanteOrigen']['id_moneda_enlazada'] == $datoEmpresa["DatoEmpresa"]["id_moneda"])
   			$cotizacion = $renglon['ComprobanteOrigen']['valor_moneda'];
   		elseif ($renglon['ComprobanteOrigen']['id_moneda_enlazada'] == $datoEmpresa["DatoEmpresa"]["id_moneda2"])
   			$cotizacion = 1/$renglon['ComprobanteOrigen']['valor_moneda2'];
   		elseif ($renglon['ComprobanteOrigen']['id_moneda_enlazada'] == $datoEmpresa["DatoEmpresa"]["id_moneda3"])
   			$cotizacion = 1/$renglon['ComprobanteOrigen']['valor_moneda3'];
 
 
   
   $tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
   $tbody .= "<td>" .$renglon['ComprobanteOrigen']['TipoComprobante']['codigo_tipo_comprobante'] . "</td>";
   
   if(isset($renglon['ComprobanteOrigen']['id_punto_venta']))
   	$tbody .= "<td>" .$comprobante->GetNumberComprobante($renglon['ComprobanteOrigen']['PuntoVenta']['numero'],$renglon['ComprobanteOrigen']['nro_comprobante']) . "</td>";       
   else
   	$tbody .= "<td>" .$comprobante->GetNumberComprobante($renglon['ComprobanteOrigen']['d_punto_venta'],$renglon['ComprobanteOrigen']['nro_comprobante']) . "</td>";
   
   
   $tbody .= "<td>" .$fecha_contable->format('d-m-Y')."</td>";
   $tbody .= "<td>" .$fecha_vencimiento->format('d-m-Y')."</td>";
   
   if($renglon['ComprobanteOrigen']["id_moneda"]!= EnumMoneda::Peso)
   $tbody .= "<td>" .$renglon['ComprobanteOrigen']["Moneda"]["simbolo_internacional"]." ".money_format('%!n',round($renglon['precio_unitario']/$cotizacion,4))."| ARS ".money_format('%!n',$renglon['precio_unitario'])."</td>";
   else
   	$tbody .= "<td>" .$renglon['ComprobanteOrigen']["Moneda"]["simbolo_internacional"]." ".money_format('%!n',$renglon['precio_unitario'])."</td>";
   
   if($renglon['ComprobanteOrigen']['enlazar_con_moneda'] == 1){
   
   		$tbody .= "<td>Si ".$renglon['ComprobanteOrigen']['MonedaEnlazada']["simbolo_internacional"]."</td>";
   	
   		$tbody .= "<td>".money_format('%!n',round($cotizacion,4))."</td>";
   		
   		if($renglon['monto_diferencia_cambio'] !=0){
	   		if($renglon['monto_diferencia_cambio'] > 0)
				$tbody .= "<td>ND por el valor de " .money_format('%!n',$renglon['monto_diferencia_cambio'])."</td>";
			else
			   $tbody .= "<td>NC por el valor de " .money_format('%!n',$renglon['monto_diferencia_cambio'])."</td>";
			   
			$suma_diferencia_cambio += $renglon['monto_diferencia_cambio'];
			
		}	   
   }else{
   
   	   $tbody .= "<td>No</td>";
   	   $tbody .= "<td></td>";
   	   $tbody .= "<td></td>";
   }
   
   $suma_comprobantes  += $renglon['precio_unitario']*$renglon['ComprobanteOrigen']['TipoComprobante']['signo_comercial'];
   
   
   
   $tbody .= "</tr>";
   
   
   if($renglon["importe_descuento_unitario"]>0){//agrego una fila para mostrarle que le hice un descuento
   
   
   	 
   $tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
   $tbody .= "<td>Descuento</td>";
   $tbody .= "<td></td>";       
   $tbody .= "<td></td>";
   $tbody .= "<td>" .$renglon['importe_descuento_unitario']."</td>";
   $tbody .= "<td></td>";
   $tbody .= "<td></td>";
   $tbody .= "<td></td>";
   $tbody .= "</tr>";
   
   $suma_comprobantes  = $suma_comprobantes - $renglon['importe_descuento_unitario'];
   
   }
}





   $tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
   $tbody .= "<td><strong>Total</strong></td>";
   $tbody .= "<td></td>";       
   $tbody .= "<td></td>";
   $tbody .= "<td><strong>".$datos_pdf["Moneda"]["simbolo_internacional"]." ".money_format('%!n',$suma_comprobantes)."</strong></td>";
   $tbody .= "<td></td>";
   $tbody .= "<td></td>";
   $tbody .= "<td></td>";
   $tbody .= "</tr>";



$renglones_credito_interno = $credito_interno_relacionados;


$diferencia = 0;
foreach($renglones_credito_interno as $renglon){

 
   
   $tbody .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
   $tbody .= "<td>" .$renglon['Comprobante']['codigo_tipo_comprobante'] . " (Generado)</td>";
   $tbody .= "<td>" .$comprobante->GetNumberComprobante($renglon['PuntoVenta']['numero'],$renglon['Comprobante']['nro_comprobante']) . "</td>";      
   $tbody .= "<td>" .$comprobante->formatDate($renglon["Comprobante"]["fecha_vencimiento"])."</td>";
   $tbody .= "<td>" .money_format('%!n',$renglon['Comprobante']["total_comprobante"])."</td>";
   $diferencia += $renglon['Comprobante']["total_comprobante"];
   $tbody .= "</tr>";
}



 $tbody2 = '';
 
 $renglones2 = $datos_pdf["ComprobanteValor"];
 
 $suma_valores = 0;

foreach($renglones2 as $renglon){
    
    
    if($renglon['monto']>0){
    
    
	    switch($renglon['Valor']['id']) {
	        
	        case EnumValor::TRANSFERENCIAS:
	           $tbody2 .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
	           $tbody2 .= "<td>" .$renglon['Valor']['d_valor'] . "</td>";
	           $tbody2 .= "<td>" .$renglon['Valor']['id'] . "</td>";
	           $tbody2 .= "<td>" .$renglon['CuentaBancaria']['nro_cuenta']. "</td>";       
	           $tbody2 .= "<td>" .money_format('%!n',round($renglon['monto'],2))."</td>";
	           $tbody2 .= "</tr>";
	           $suma_valores += round($renglon['monto'],2);
	               
	        break;
	        
	        case EnumValor::EFECTIVO:
	        
	           $tbody2 .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
	           $tbody2 .= "<td>" .$renglon['Valor']['d_valor']. "</td>";
	           $tbody2 .= "<td></td>";
	           $tbody2 .= "<td>" .$renglon['d_comprobante_valor']. "</td>";       
	           $tbody2 .= "<td>" .money_format('%!n',round($renglon['monto'],2))."</td>";
	           $tbody2 .= "</tr>";
	            $suma_valores += round($renglon['monto'],2);
	        break;
	        
	         case EnumValor::COMPENSACION:
	        
	           $tbody2 .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
	           $tbody2 .= "<td>" .$renglon['Valor']['d_valor']. "</td>";
	            $tbody2 .= "<td></td>";
	           $tbody2 .= "<td>" .$renglon['d_comprobante_valor']. "</td>";       
	           $tbody2 .= "<td>" .money_format('%!n',round($renglon['monto'],2))."</td>";
	           $tbody2 .= "</tr>";
	            $suma_valores += round($renglon['monto'],2);
	        break;
	        
	          case EnumValor::TARJETA:
	        
	           $tbody2 .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
	           $tbody2 .= "<td>" .$renglon['Valor']['d_valor']." Nro:".$renglon['nro_tarjeta']."</td>";
	            $tbody2 .= "<td></td>";
	           $tbody2 .= "<td>" .$renglon['d_comprobante_valor']. "</td>";       
	           $tbody2 .= "<td>" .money_format('%!n',round($renglon['monto'],2))."</td>";
	           $tbody2 .= "</tr>";
	            $suma_valores += round($renglon['monto'],2);
	        break;
	        
	    }
    
    }
    
          
 
}


$tbody3 = '';
 
$renglones3 = $datos_pdf["ChequeEntrada"];




foreach($renglones3 as $renglon){
    
  


   $tbody3 .= "<tr style=\"font-size:10px;font-family:'Courier New', Courier, monospace\">";
   $tbody3 .= "<td>" .$renglon['id'] . "</td>";
   $tbody3 .= "<td>" .$renglon['nro_cheque'] . "</td>";
   $tbody3 .= "<td>" .$renglon['Banco']['d_banco']. "</td>";       
   $tbody3 .= "<td>" .$comprobante->formatDate($renglon["fecha_cheque"]). "</td>";       
   $tbody3 .= "<td>" .money_format("%!n",round($renglon['monto'],2))."</td>";
   $tbody3 .= "</tr>";
   
   $suma_valores += round($renglon['monto'],2);
}
 
//agrego las observaciones





$html='';


$html .=' 
<table width="100%"   style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
  <tr >
                    <td colspan="4"></td>
                  </tr>
  <tr>  
  <tr >
                    <td colspan="4"></td>
                  </tr>
  <tr>  

 
 </table>



                  
                    <table width="100%" border="1"  style="width:100%;font-size:10px;font-family:\'Courier New\', Courier, monospace;">
                  <tr  style="background-color:rgb(224, 225, 229);">
                    <td colspan="4"><div align="center"><strong>Cliente</strong></div></td>
                  </tr>
                  <tr>                                     
                    <td width="17%"><strong>Codigo:</strong> '.$datos_pdf["Persona"]["codigo"].'</td>
                    <td width="35%"><strong>Raz&oacute;n Social:</strong> '.$datos_pdf["Persona"]["razon_social"].'</td>
                    <td width="26%"><strong>Cuit:</strong> '.$datos_pdf["Persona"]["cuit"].' </td>';
                    
              
                        $html.='<td width="22%"></td>';
                    
                    

                    $html.='</tr>
                              <tr>
                                <td colspan="3"><strong>Direccion:</strong> '.$datos_pdf["Persona"]["calle"].' - '.$datos_pdf["Persona"]["numero_calle"].' - '.$datos_pdf["Persona"]["localidad"].' - '.$datos_pdf["Persona"]["ciudad"].' - '.$datos_pdf["Persona"]["Provincia"]["d_provincia"].' - '.$datos_pdf["Persona"]["Pais"]["d_pais"].'  </td>
                                <td><strong>Teléfono:</strong> '.$datos_pdf["Persona"]["tel"].'</td>
                              </tr>
                    
                    
                                 </table>


                  
              

<table width="100%" border="0px" cellspacing="1" cellpadding="2" style="width:100%;font-size:12px;font-family:\'Courier New\', Courier, monospace">

   
     <tbody>
     
        <tr >
        <td colspan="2"> </td>
         <td colspan="2">                           </td>
         <td colspan="2">                           </td>
        </tr>
 
        <tr >
        <td  colspan="6" ><HR></td>
      
         </tr>
     </tbody>
</table>


<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">
    <tr>
    <td class="sorting"  rowspan="1" colspan="7" style="width:100%;text-decoration:underline;text-align:left"><strong>Comprobantes imputados</strong></td>
    </tr>
    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th style="width:5%;"><strong>Tipo Comp.</strong></th>
        <th style="width:20%;"><strong>Nro. Comp.</strong></th>
        <th style="width:15%;"><strong>Fcha. Comp.</strong></th>
        <th style="width:15%;"> <strong>Fcha Vto</strong></th>
        <th style="width:15%;"><strong>Importe</strong></th>
        <th style="width:10%;"><strong>Enlazado Tipo Cambio</strong></th>
        <th style="width:10%;"><strong>Cot.</strong></th>
        <th style="width:10%;"><strong>NC/ND</strong></th>
    </tr>

  <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
    '.$tbody.'
  </tbody> 
  
  
  
   
</table>



<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">
      <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
     <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
    <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;text-decoration:underline;"><strong>Efec./Transf/Compens..</strong></td>
    </tr>

    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong>Valor</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>Id</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:25%;"><strong>Desc.</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:50%;"><strong>Total.</strong></th>
      
    </tr>

  <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
  
    '.$tbody2.'
  </tbody> 
  
   
</table>';


  
if(strlen($tbody3)>0){

$html.='<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">
      <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
     <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
    <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;text-decoration:underline;"><strong>Cheques</strong></td>
    </tr>
    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:5%;"><strong>Id.</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:25%;"><strong>Nro.Cheque</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:25%;"><strong>Banco</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:15%;"><strong>F. Vencimiento</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:30%;"><strong>Monto</strong></th>
      
    </tr>.';

 
  
  $html.='<tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
        '.$tbody3.'
      </tbody></table>'; 
      
      
      
      
   
  
}   

   $html.='
     <table width="100%"  cellspacing="1" cellpadding="2" style="font-size:10px;font-family:\'Courier New\', Courier, monospace">  
 <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
       <tr>
        <td class="sorting"  rowspan="1" colspan="1" style="width:50%;text-align:left;"><strong>Total</strong></td>
        <td class="sorting"  rowspan="1" colspan="1" style="width:50%;text-align:left;"><strong>'.money_format('%!n',$suma_valores).'</strong></td>
       </tr>
      </tbody></table>'; 


if($datos_pdf["Comprobante"]["descuento"]>0){

//total_comprobante - subtotal_bruto 

$html.='<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:11px;font-family:\'Courier New\', Courier, monospace">
    
     <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
    <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;text-decoration:underline;"><strong>Descuento</strong></td>
    </tr>
    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:50%;"><strong>Descuento (%)</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:50%;"><strong>Monto</strong></th>
    
      
    </tr>';

$html.='<tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
      <tr>
        <td>
        '.round($datos_pdf["Comprobante"]["descuento"],2).'%
        </td>
        
         <td>
        '.round($datos_pdf["Comprobante"]["importe_descuento"],2).'
        </td>
        
      </tr>
      </tbody></table>'; 

}


if(strlen($impuestos)>0){

$html.='<table width="100%"  cellspacing="1" cellpadding="2" style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
      <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
     <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong></strong></td>
    </tr>
    <tr>
    <td class="sorting"  rowspan="1" colspan="1" style="width:20%;text-decoration:underline;"><strong>Retenciones</strong></td>
    </tr>
    <tr style="background-color:rgb(224, 225, 229);text-align:center;font-size:10px;">
        <th class="sorting"  rowspan="1" colspan="1" style="width:40%;"><strong>Desc</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong>Base Imp.</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong>Tasa</strong></th>
        <th class="sorting"  rowspan="1" colspan="1" style="width:20%;"><strong>Monto</strong></th>
      
    </tr>.';

 
  
  $html.='<tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
        '.$impuestos.'
      </tbody></table>'; 
      
       $html.='
     <table width="100%"  cellspacing="1" cellpadding="2" style="font-size:10px;font-family:\'Courier New\', Courier, monospace">  
 <tbody style="font-size:9px;font-family:\'Courier New\', Courier, monospace">
       <tr>
        <td class="sorting"  rowspan="1" colspan="1" style="width:50%;text-align:left;"><strong>Total</strong></td>
        <td class="sorting"  rowspan="1" colspan="1" style="width:50%;text-align:left;"><strong>'.$suma_impuestos.'</strong></td>
       </tr>
      </tbody></table>'; 
  
}  

$pdf->writeHTML($html, true, false, true, false, true);


$pdf_name = $comprobante->getNamePDF($datos_pdf);
$root_pdf = $comprobante->getRootPDF();

if($datos_pdf["TipoComprobante"]["adjunta_pdf_mail"] == 1 && ($output == 0 || $output == 3)) {
	
	$path_para_pdf = $datos_empresa["DatoEmpresa"]["local_path"].$root_pdf;
	
	$pdf->Output($path_para_pdf.$pdf_name.'.pdf', 'F');//Esta linea guarda un archivo en path_para_pdf
}



if($output == 1 || $output == 3)
	$pdf->Output($pdf_name.'.pdf', 'D');
	


ob_clean();
$pdf->Output(str_replace(".","",$datos_pdf["TipoComprobante"]["abreviado_tipo_comprobante"].'_'.$datos_pdf["Comprobante"]["nro_comprobante_completo"].'-'.$datos_pdf["Persona"]["razon_social"]).'.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
