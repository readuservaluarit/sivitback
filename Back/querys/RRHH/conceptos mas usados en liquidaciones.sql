SELECT COUNT(*),items_comprobante_sueldo.Id_Concepto,conceptos.`Codigo_Concepto`,conceptos.`Formula`,conceptos.`Abreviado_Concepto`,conceptos.`Nombre_Concepto` FROM 
items_comprobante_sueldo

JOIN conceptos ON conceptos.`Id_Concepto` = items_comprobante_sueldo.`Id_Concepto`

WHERE LENGTH(conceptos.`Formula`)>5

GROUP BY items_comprobante_sueldo.Id_Concepto

ORDER BY COUNT(*) DESC