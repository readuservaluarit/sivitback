<?php 


		
		$model["d_provincia"]["show_grid"] = 0;
        $model["d_provincia"]["type"] = EnumTipoDato::cadena;
        
        
		$model["id"]["show_grid"] = 1;
        $model["id"]["header_display"] = "Cod.";
        $model["id"]["order"] = "0";
        $model["id"]["width"] = "4";
		$model["id"]["text_colour"] = "#000055";

        $model["d_impuesto"]["show_grid"] = 1;
        $model["d_impuesto"]["header_display"] = "Nombre";
        $model["d_impuesto"]["order"] = "1";
        $model["d_impuesto"]["width"] = "25";
		$model["d_impuesto"]["text_colour"] = "#0000FF";
        
        $model["abreviado_impuesto"]["show_grid"] = 1;
        $model["abreviado_impuesto"]["header_display"] = "Cod.Abreviado";
        $model["abreviado_impuesto"]["order"] = "2";
        $model["abreviado_impuesto"]["width"] = "10";
		$model["abreviado_impuesto"]["text_colour"] = "#0000AA";
		
		$model["d_tipo_impuesto"]["show_grid"] = 1;
        $model["d_tipo_impuesto"]["type"] = EnumTipoDato::cadena;
        $model["d_tipo_impuesto"]["header_display"] = "Tipo de Impuesto";
        $model["d_tipo_impuesto"]["order"] = "3";
        $model["d_tipo_impuesto"]["width"] = "25";
        $model["d_tipo_impuesto"]["text_colour"] = "#550055";
		
		$model["autogenerado"]["show_grid"] = 1;
        $model["autogenerado"]["type"] = EnumTipoDato::cadena;
        $model["autogenerado"]["header_display"] = "Auto";
        $model["autogenerado"]["order"] = "4";
        $model["autogenerado"]["width"] = "3";
		$model["autogenerado"]["text_colour"] = "#660066"; 
        
		$model["d_sistema"]["show_grid"] = 1;
        $model["d_sistema"]["type"] = EnumTipoDato::cadena;
        $model["d_sistema"]["header_display"] = "M&oacute;dulo";
        $model["d_sistema"]["order"] = "5";
        $model["d_sistema"]["width"] = "7";
		$model["d_sistema"]["text_colour"] = "#660000"; 
		
		
		$model["base_imponible_desde"]["show_grid"] = 1;
        $model["base_imponible_desde"]["type"] = EnumTipoDato::cadena;
        $model["base_imponible_desde"]["header_display"] = "Base Imp. Dsd";
        $model["base_imponible_desde"]["order"] = "6";
        $model["base_imponible_desde"]["width"] = "7";
		$model["base_imponible_desde"]["text_colour"] = "#660000"; 
		
		
		
		$model["base_imponible_hasta"]["show_grid"] = 1;
        $model["base_imponible_hasta"]["type"] = EnumTipoDato::cadena;
        $model["base_imponible_hasta"]["header_display"] = "Base Imp. Hasta";
        $model["base_imponible_hasta"]["order"] = "7";
        $model["base_imponible_hasta"]["width"] = "7";
		$model["base_imponible_hasta"]["text_colour"] = "#660000"; 
		
		
		$model["alicuota"]["show_grid"] = 1;
        $model["alicuota"]["type"] = EnumTipoDato::decimal;
        $model["alicuota"]["header_display"] = "Alic.(%)";
        $model["alicuota"]["order"] = "8";
        $model["alicuota"]["width"] = "7";
		$model["alicuota"]["text_colour"] = "#660000"; 
		
        
        $model["d_estado"]["show_grid"] = 0;
        $model["d_estado"]["type"] = EnumTipoDato::cadena;
        $model["d_estado"]["header_display"] = "Estado";
        $model["d_estado"]["order"] = "9";
        $model["d_estado"]["width"] = "15";
        $model["d_estado"]["text_colour"] = "#660000"; 
        
        $model["d_cuenta_contable"]["show_grid"] = 0;
        $model["d_cuenta_contable"]["type"] = EnumTipoDato::cadena;
        $model["d_cuenta_contable"]["header_display"] = "CC";
        $model["d_cuenta_contable"]["order"] = "10";
        $model["d_cuenta_contable"]["width"] = "15";
        $model["d_cuenta_contable"]["text_colour"] = "#660000"; 

 $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Impuesto",
                    "content" => $model
                );
        echo json_encode($output);
         
        

?>