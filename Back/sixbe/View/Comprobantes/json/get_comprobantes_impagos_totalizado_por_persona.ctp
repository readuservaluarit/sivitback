<?php
        // CONFIG  CON MODE DISPLAYED CELLS
        
        
        $model["tel"]["show_grid"] = 0;
        $model["tel"]["type"] = EnumTipoDato::cadena;
        
        
        $model["saldo"]["show_grid"] = 0;
        $model["saldo"]["type"] = EnumTipoDato::decimal_flotante;
        
        
        $model["codigo_persona"]["show_grid"] = 1;
        $model["codigo_persona"]["type"] = EnumTipoDato::entero;
        $model["codigo_persona"]["header_display"] = "C&oacute;digo";
        $model["codigo_persona"]["width"] = "5"; //REstriccion mas de esto no se puede achicar
        $model["codigo_persona"]["order"] = "0";
        $model["codigo_persona"]["text_colour"] = "#000000";
        $model["codigo_persona"]["read_only"] = 1;
        
        
        $model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["header_display"] = "Raz&oacute;n Social";
        $model["razon_social"]["width"] = "10"; //REstriccion mas de esto no se puede achicar
        $model["razon_social"]["order"] = "1";
        $model["razon_social"]["text_colour"] = "#000000";
		$model["razon_social"]["read_only"] = 1;
        
        
        $model["tel"]["show_grid"] = 1;
        $model["tel"]["header_display"] = "Tel.";
        $model["tel"]["width"] = "5"; //REstriccion mas de esto no se puede achicar
        $model["tel"]["order"] = "2";
        $model["tel"]["text_colour"] = "#000000";
        $model["tel"]["read_only"] = 1;
        
        
        $model["total_comprobante"]["show_grid"] = 1;
        $model["total_comprobante"]["header_display"] = "Total";
        $model["total_comprobante"]["width"] = "10"; //REstriccion mas de esto no se puede achicar
        $model["total_comprobante"]["order"] = "3";
        $model["total_comprobante"]["text_colour"] = "#000000";
        $model["total_comprobante"]["read_only"] = 1;
        
        
        $model["saldo"]["show_grid"] = 1;
        $model["saldo"]["header_display"] = "Saldo";
        $model["saldo"]["width"] = "10"; //REstriccion mas de esto no se puede achicar
        $model["saldo"]["order"] = "4";
        $model["saldo"]["text_colour"] = "#000000";
        $model["saldo"]["read_only"] = 1;
        
      
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Comprobante",
                    "content" => $model
                );
        
          echo json_encode($output);
?>