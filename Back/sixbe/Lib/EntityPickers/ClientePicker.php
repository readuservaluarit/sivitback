<?php

class ClientePicker extends EntityPicker{
    
    public $model = "Cliente";
    
    public $pickerTitle = "Selección de Cliente";
    
    public $pickerFields = array(
                                  array("title" => "Id", "field" => "id", "width" => "10%", "keyOrder" => "id"),
                                  array("title" => "Nombre", "field" => "razon_social", "width" => "90%", "keyOrder" => "razon_social")
                                );

    public function getPickerConditions($busqueda){
        
        $pickerConditions = array('Cliente.razon_social LIKE' => '%' . $busqueda['bus_unico'] . '%');
        
        return $pickerConditions;
        
        
    }
  /*
    public function getPickerInitialFilter(){
        
        //Filtra siempre por la Jurisdiccion del usuario si corresponde
        return DataFilter::getJurisdiccionPickerFilter();
        
    }
  */
  
}