<?php 
         include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');
         
         
         App::import('Model','Comprobante');
		$comprobante = new Comprobante();
		$produccion = $comprobante->getActivoModulo(EnumModulo::PRODUCCION);
		
		
		$model["n_item"]["show_grid"] = 1;
        $model["n_item"]["header_display"] = "NItem";
        $model["n_item"]["order"] = "1";
        $model["n_item"]["width"] = "3";
		$model["n_item"]["read_only"] = "0";
		$model["n_item"]["text_colour"] = "#0000AA"; //BLUE
		
       
        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "[C&oacute;digo]";
        $model["codigo"]["order"] = "3";
        $model["codigo"]["width"] = "10";
		$model["codigo"]["read_only"] = "1";//AHROA SE PUEDE HACER DOBLE CLICK
		$model["codigo"]["picker"] = EnumMenu::mnuArticulosGeneral;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
		
		$model["codigo_barra"]["show_grid"] = 1;
        $model["codigo_barra"]["header_display"] = "[C&oacute;dBar]";
        $model["codigo_barra"]["order"] = "4";
        $model["codigo_barra"]["width"] = "5";
		$model["codigo_barra"]["read_only"] = "1";//AHROA SE PUEDE HACER DOBLE CLICK
		$model["codigo_barra"]["picker"] = EnumMenu::mnuArticulosGeneral;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
		
        $model["d_producto"]["show_grid"] = 1;
        $model["d_producto"]["header_display"] = "[Descripci&oacute;n]";
        $model["d_producto"]["order"] = "5";
        $model["d_producto"]["width"] = "15";
		$model["d_producto"]["read_only"] = "1";//AHROA SE PUEDE HACER DOBLE CLICK
		$model["d_producto"]["picker"] = EnumMenu::mnuArticulosGeneral;//Esto enlaza un item del MENU.php invisible con eventos picker de la CELDA 
		
		$model["item_observacion"]["show_grid"] = 1;
		$model["item_observacion"]["type"] = "string";
        $model["item_observacion"]["header_display"] = "Item Obs.";
        $model["item_observacion"]["order"] = "6";
        $model["item_observacion"]["width"] = "12";
		$model["item_observacion"]["read_only"] = "0";
		
		
		$model["id_unidad"]["show_grid"] = 1;
        $model["id_unidad"]["header_display"] = "Un.";
        $model["id_unidad"]["order"] = "7";
        $model["id_unidad"]["width"] = "5";
		$model["id_unidad"]["read_only"] = "0";
		$model["id_unidad"]["text_colour"] = "#0000FF"; //BLUE
        
        $model["cantidad"]["show_grid"] = 1;
        $model["cantidad"]["header_display"] = "Cant.";
        $model["cantidad"]["order"] = "8";
        $model["cantidad"]["width"] = "4";
		$model["cantidad"]["read_only"] = "0";
		$model["cantidad"]["text_colour"] = "#0000FF"; //BLUE
		
		
        	
		$model["precio_unitario_bruto"]["show_grid"] = 1;
        $model["precio_unitario_bruto"]["header_display"] = "Precio U. Bruto";
        $model["precio_unitario_bruto"]["order"] = "10";
        $model["precio_unitario_bruto"]["width"] = "8";
		$model["precio_unitario_bruto"]["read_only"] = "0";
		$model["precio_unitario_bruto"]["text_colour"] = "#C0000C";
		$model["precio_unitario_bruto"]["decimal_place"] = "2";
		
		
        $model["descuento_unitario"]["show_grid"] = 1;
        $model["descuento_unitario"]["header_display"] = "Desc. Unitario %";
        $model["descuento_unitario"]["order"] = "11";
        $model["descuento_unitario"]["width"] = "5";
        $model["descuento_unitario"]["min_value"] = "0";
		$model["descuento_unitario"]["max_value"] = "100";
		$model["descuento_unitario"]["read_only"] = "0";
        
        $model["precio_unitario"]["show_grid"] = 1;
        $model["precio_unitario"]["header_display"] = "Precio Unitario";
        $model["precio_unitario"]["order"] = "12";
        $model["precio_unitario"]["width"] = "9";
		$model["precio_unitario"]["read_only"] = "1";
		$model["precio_unitario"]["min_value"] = "0";
		$model["precio_unitario"]["text_colour"] = "#C0000C";
		
		
        $model["total"]["show_grid"] = 1;
        $model["total"]["header_display"] = "Total";
        $model["total"]["order"] = "13";
        $model["total"]["width"] = "11";
		$model["total"]["read_only"] = "1";
		$model["total"]["text_colour"] = "#C0000C";
		
		
		$model["id_iva"]["show_grid"] = 1;
        $model["id_iva"]["header_display"] = "IVA";
        $model["id_iva"]["order"] = "14";
        $model["id_iva"]["width"] = "6";
		$model["id_iva"]["read_only"] = "0";


		
		
		if($produccion == 1){
			$model["precio_minimo_producto"]["show_grid"] = 1; // INVISIBLE
	        $model["precio_minimo_producto"]["header_display"] = "Prec. Min Venta";
	        $model["precio_minimo_producto"]["order"] = "15";
	        $model["precio_minimo_producto"]["width"] = "6";
			$model["precio_minimo_producto"]["read_only"] = "1";
        }
        
        $model["orden_compra_externa"]["show_grid"] = 1; // INVISIBLE
        $model["orden_compra_externa"]["header_display"] = "OC.";
        $model["orden_compra_externa"]["order"] = "16";
        $model["orden_compra_externa"]["width"] = "3";
        $model["orden_compra_externa"]["read_only"] = "1";
        
 $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItem",
                    "content" => $model
                );
        echo json_encode($output);
         
        

?>