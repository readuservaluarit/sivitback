<?php
App::uses('AppModel', 'Model');

class PeriodoImpositivo extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'periodo_impositivo';
    
    
    public $actsAs = array('Containable');
    
    
    
    public $validate = array(
        
        
    );


	//The Associations below have been created with all possible keys, those that are not needed can be removed

    
    public $belongsTo = array(
        'EjercicioContable' => array(
            'className' => 'EjercicioContable',
            'foreignKey' => 'id_ejercicio_contable',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
      
        
      
       
      
     
     
       
       
       
       
      
 );
 
 
 public function getEstadoGrabado($id){
    
    $periodo = $this->find('first', array(
                                                'conditions' => array('PeriodoContable.id' => $id),
                                                'contain' =>false
                                                )); 
        
    
   return $periodo["PeriodoContable"]["cerrado"]; 
}



public function getPeriodoActual(){
                                                    
    
	$fecha = date("Y-m-d");
    $periodo = $this->find('first', array(
    		'conditions' => array('PeriodoContable.fecha_desde <=' =>$fecha,'PeriodoContable.fecha_hasta >=' => $fecha),
                                                'contain' =>false
                                                )); 
        
    
   if($periodo)
    return $periodo["PeriodoContable"];
   else
    return 0; 
}


public function getFechaInicio($id_periodo_contable){
      
        $periodo_contable = $this->find('first', array(
                                                'conditions' => array('PeriodoContable.id' =>$id_periodo_contable )
                                                ));
                                                
       return $periodo_contable["PeriodoContable"]["fecha_desde"];
    
} 


public function getFechaFin($id_periodo_contable){
      
        $periodo_contable = $this->find('first', array(
                                                'conditions' => array('PeriodoContable.id' =>$id_periodo_contable )
                                                ));
                                                
       return $periodo_contable["PeriodoContable"]["fecha_hasta"];
    
}        
      
      
    

    
    
 

  
 
  




}
