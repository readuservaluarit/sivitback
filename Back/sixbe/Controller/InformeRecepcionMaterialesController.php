<?php
App::uses('ComprobantesController', 'Controller');


  /**
    * @secured(CONSULTA_INFORME_RECEPCION_MATERIAL)
  */
class InformeRecepcionMaterialesController extends ComprobantesController {
    
    public $name = EnumController::InformeRecepcionMateriales;
    public $model = 'Comprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $requiere_impuestos = 0;
    
    /**
    * @secured(CONSULTA_INFORME_RECEPCION_MATERIAL)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros($this->model);
  
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>array('Persona','EstadoComprobante','TipoComprobante','DepositoOrigen','DepositoDestino','Transportista','PuntoVenta','Usuario'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.id desc'
        ); //el contacto es la sucursal
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));

        //Headers
        $formBuilder->addHeader('id', 'REMITO.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'REMITO.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
        //vista formBuilder
    }     
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
            //$valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
             //unset($valor['ComprobanteItem']);
             if(isset($valor['TipoComprobante'])){
                 $valor[$this->model]['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                 $valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
                 unset($valor['TipoComprobante']);
             }
             $valor[$this->model]['nro_comprobante'] = (string) str_pad($valor[$this->model]['nro_comprobante'], 8, "0", STR_PAD_LEFT);
             
             if(isset($valor['EstadoComprobante'])){
                $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
                unset($valor['EstadoComprobante']);
             }
             if(isset($valor['DepositoOrigen'])){
                $valor[$this->model]['id_deposito_origen'] = $valor['DepositoOrigen']['id'];
                $valor[$this->model]['d_deposito_origen'] = $valor['DepositoOrigen']['d_deposito'];
                unset($valor['DepositoOrigen']);
             }
             
              if(isset($valor['DepositoDestino'])){
                $valor[$this->model]['id_deposito_destino'] = $valor['DepositoDestino']['id'];
                $valor[$this->model]['d_deposito_destino'] = $valor['DepositoDestino']['d_deposito'];
                unset($valor['DepositoDestino']);
             }
             if(isset($valor['Transportista'])){
                $valor[$this->model]['t_razon_social'] = $valor['Transportista']['razon_social'];
                $valor[$this->model]['t_direccion'] = $valor['Transportista']['calle'].' '.$valor['Transportista']['numero_calle'];
                unset($valor['Transportista']);
             }
             
             if(isset($valor['Persona'])){
                 if($valor['Persona']['id']== null){
                     
                    $valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social']; 
                 }else{
                     
                     $valor[$this->model]['razon_social'] = $valor['Persona']['razon_social'];    
                     $valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
                 }
                 unset($valor['Persona']);
             }
			if(isset($valor['PuntoVenta'])){
				$valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
				unset($valor['PuntoVenta']);
			}

			//$valor['Comprobante']['d_usuario'] = 'ADMIN';
			
			if(isset($valor['Usuario'])){
				$valor['Comprobante']['d_usuario'] = $valor['Usuario']['nombre'];
			}
            $this->{$this->model}->formatearFechas($valor);
        }
        
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
     }
    //fin vista json
        
    }
    
    
    
    /**
    * @secured(ABM_USUARIOS)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->REMITO->id = $id;
            $this->REMITO->contain();
            $this->request->data = $this->REMITO->read();
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('REMITO');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de REMITOes');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de REMITOes');
        else
            $formBuilder->setTituloForm('Consulta de REMITOes');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('cuit', 'Cuit', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('calle', 'Calle', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  
        $formBuilder->addFormInput('tel', 'Tel&eacute;fono', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('ciudad', 'Ciudad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descuento', 'Descuento', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descripcion', 'Descripcion', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('localidad', 'Localidad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
         
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'ClienteAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('ClienteRazonSocial', 'Debe ingresar una Raz&oacute;n Social');
                validator.validateRequired('ClienteCuit', 'Debe ingresar un CUIT');
                validator.validateNumeric('ClienteDescuento', 'El Descuento ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_cuit',
                        'data': 'cuit=' + $('#ClienteCuit').val()+'&id='+$('#ClienteId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('ClienteCuit','El Cuit que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";
        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

    /**
    * @secured(ADD_INFORME_RECEPCION_MATERIAL)
    */
    public function add(){

     if($this->ChequeoIrmYaGenerado() == 0){
     	parent::add();    
     }else{
     	$output = array(
     			"status" =>EnumError::ERROR,
     			"message" => "ERROR:Existe un IRM creado desde el mismo Remito de Compra",
     			"content" => ""
     		
     	);
     	
     	$this->set($output);
     	$this->set("_serialize", array("status", "message", "content","id_add","nro_add","id_asiento"));
     }
    }
    
    
    /**
    * @secured(MODIFICACION_INFORME_RECEPCION_MATERIAL)
    */
    public function edit($id){ 
    	
    	
     $this->borroLotes($id);
     $this->SaveItems($this->request->data);
     parent::edit($id); 
     
     return;   
        
    }
    
    
    
    protected  function borroLotes($id_comprobante){
    	
    	$this->loadModel(EnumModel::ComprobanteItemLote);
    	$id_items = array();
    	
    	foreach ($this->request->data["ComprobanteItem"] as $item){
    		
    		
    		if(isset($item["id"]) && $item["id"]>0)
    			array_push($id_items, $item["id"]);
    	}
    	
    	if(count($id_items)>0)
    		$this->ComprobanteItemLote->deleteAll(array("ComprobanteItemLote.id_comprobante_item"=>$id_items),false);
    	    
    	
    }
    
    /**
    * @secured(BAJA_INFORME_RECEPCION_MATERIAL)
    */
    public function delete($id){
        
     parent::delete($id);    
        
    }
    
   /**
    * @secured(BAJA_INFORME_RECEPCION_MATERIAL)
    */
    public function deleteItem($id,$externo=1){
        
        parent::deleteItem($id,$externo);    
        
    }
    
    
      /**
    * @secured(CONSULTA_INFORME_RECEPCION_MATERIAL)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
  
    /**
    * @secured(CONSULTA_INFORME_RECEPCION_MATERIAL)
    */
    public function pdfExport($id,$vista=''){

    $this->loadModel("Comprobante");
  
     
    $imprimir = 1;
    
     
        if($imprimir == 0){   //Debo devolver un Json porque no deberia imprimir
        
        
          $output = array(
            "status" =>EnumError::ERROR,
            "message" => "El Remito no puede imprimirse. Primero debe realizar el movimiento de Despacho de mercader&iacute;a.",
            "content" => "",
        );
        
        
        $this->autoRender = false;
        $this->response->type('json');
        echo json_encode($output);
       
        
        
        
        }else{                                                
                                                        
        
        
        
      
            $comprobantes_relacionados = $this->getComprobantesRelacionados($id);
         
                                           
          
            
            
              
            $remito_compra_relacionados = array();



            foreach($comprobantes_relacionados as $comprobante){


             if(  isset($comprobante["Comprobante"]["id_tipo_comprobante"]) && in_array($comprobante["Comprobante"]["id_tipo_comprobante"],   $this->Comprobante->getTiposComprobanteController(EnumController::RemitosCompra) ))
             	array_push($remito_compra_relacionados,$this->Comprobante->GetNumberComprobante($comprobante["Comprobante"]["d_punto_venta"],$comprobante["Comprobante"]["nro_comprobante"]));
          


            }
           
            
            
            
            $this->set('remito_compra_relacionados',$remito_compra_relacionados);  
     
            parent::pdfExport($id,"pdf_export_sin_precio");  
            
            
      
            
       }
     
        
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
        
        
      
 
    }
    
    
    /**
    * @secured(MODIFICACION_INFORME_RECEPCION_MATERIAL)
    */
    public function Anular($id_comprobante){
     
    	$this->loadModel("Comprobante");
    	$this->loadModel("ComprobanteItem");
    	$this->loadModel("Movimiento");
    	$this->loadModel("Modulo");
    	$this->anulo = 1;
    	$puede_anular = 1;
    	
    	$irm = $this->Comprobante->find('first', array(
    			'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
    			'contain' => array('ComprobanteItem')
    	));
    	
    	
    	
    	foreach($irm["ComprobanteItem"] as &$producto){
    	
    		
    		$id_remito_compra_item = $producto["id_comprobante_item_origen"];
    		$facturados =$this->ComprobanteItem->GetItemprocesadosEnImportacion($id_remito_compra_item, $this->Comprobante->getTiposComprobanteController(EnumController::FacturasCompra) );
    		$cantidad_facturados = $this->ComprobanteItem->GetCantidadItemProcesados($facturados);
    		if($cantidad_facturados > 0){
    			
    			$puede_anular = 0;
    		}
    		
    		
    	}
    	
    	
    	if( $irm["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::InformeRecepcionMateriales  && $this->Comprobante->getEstadoGrabado($id_comprobante) != EnumEstadoComprobante::Anulado
    			
    			&& $puede_anular == 1
    			){
    		
    		/*
    		$this->request->data["Comprobante"]["id_estado_comprobante"] = EnumEstadoComprobante::Anulado;
    		$this->request->data["Comprobante"]["fecha_anulacion"] = date("Y-m-d");
    		$this->request->data["Comprobante"]["definitivo"] = 1;
    		
    		
    		
    		/*
    		if( isset($this->request->data["ComprobanteItem"]) && count($this->request->data["ComprobanteItem"])>0 &&  $this->request->data["Comprobante"]["genera_movimiento_stock"] == 1 ){
    			
    			
    			foreach($this->request->data["ComprobanteItem"] as &$valor){
    				
    				$valor["cantidad_cierre"] = "0";
    				
    			}
    		}*/
    				
    				
    		$ds = $this->Comprobante->getdatasource(); 
    		
    		$habilitado  = $this->Modulo->estaHabilitado(EnumModulo::STOCK);
    		
    		try{
    			$ds->begin();
    			
    			$this->Comprobante->updateAll(
    					array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
    					array('Comprobante.id' => $id_comprobante) );  
    			
    			
	    		if($habilitado == 1){ //TODO: Si tiene mov stock anular
	    			
	    			$id_tipo_movimiento_defecto = $this->Comprobante->getTipoMovimientoDefecto($id_comprobante);
	    			
	    			$id_movimiento = $this->Comprobante->HizoMovimientoStock($id_comprobante,$id_tipo_movimiento_defecto);
	    			
	    			
	    			if( $id_movimiento > 0 ){//si hizo movimiento
	    				$this->loadModel("Movimiento");
	    				$this->Movimiento->Anular($id_movimiento);
	    			}
	    		}      
	    		
	    		
	    		$status = EnumError::SUCCESS;
	    		$messagge = "El IRM ha sido anulado";
	    		
	    		$ds->commit();
    		
    		}catch(Exception $e){
    			
    			$ds->rollback();
    			$tipo = EnumError::ERROR;
    			$mensaje = "No es posible anular el Comprobante ERROR:".$e->getMessage();
    			
    			
    		}  
    		
    		/*
    		
    		$this->Comprobante->id = $this->request->data["Comprobante"]["id"];
    		$this->Comprobante->saveField('definitivo', 0);
    		$this->Comprobante->saveField('id_estado_comprobante', EnumEstadoComprobante::Abierto);
    		
    		//$this->edit($this->request->data["Comprobante"]["id"]);
    		//return;
    		
    	*/	
    	}else{
    		
    		$status = EnumError::ERROR;
    		$messagge = "ERROR: El IRM no es posible anularlo. Ya que se han aprobado Facturas de Compra para el Remito relacionado con este IRM";
    	
    		
    	}
    	
    	$output = array(
    			"status" => $status,
    			"message" => $messagge,
    			"content" => ""
    	);
    	echo json_encode($output);
    	die();
    	
    	    	
    }
    
    
    
    /**
     * @secured(CONSULTA_INFORME_RECEPCION_MATERIAL)
     */
    public function existe_comprobante()
    {
    	parent::existe_comprobante();
    }
    
    
    
        /**
        * @secured(CONSULTA_INFORME_RECEPCION_MATERIAL)
        */
        public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
        {
            parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
        }
         
        
        
        public function ChequeoIrmYaGenerado(){
        	
        	
        	$this->loadModel("ComprobanteItem");
        	
        	
        	if(isset($this->request->data["ComprobanteItem"])){
        		
        		$array_id_item_comprobante_origen = array();
        		foreach($this->request->data["ComprobanteItem"] as $valor){
        			
        			array_push($array_id_item_comprobante_origen, $valor["id_comprobante_item_origen"]);
        			
        		}
        		
        		$irm = $this->ComprobanteItem->find('all', array(
        				'conditions' => array('ComprobanteItem.id_comprobante_item_origen' => $array_id_item_comprobante_origen,'Comprobante.id_tipo_comprobante' => EnumTipoComprobante::InformeRecepcionMateriales,
        										'Comprobante.id_estado_Comprobante <>'=>EnumEstadoComprobante::Anulado			
        				),
        				'contain' =>array('Comprobante')
        		));
        		
        		if(count($irm)>0)
        			return 1;
        		else
        			return 0;
        	}else{
        		return 0;
        	}
        }
        
		protected function SaveItemsParaElFuturo(&$data,&$message=''){
		
			
			$this->loadModel(EnumModel::Comprobante);
			$this->loadModel(EnumModel::Lote);
			$this->loadModel(EnumModel::ComprobanteItemLote);
			
			
			if(isset($data['ComprobanteItem'])){
				unset($data['ComprobanteItem']['genera_movimiento_stock']);
				foreach($data['ComprobanteItem'] as &$c_item){
					$c_item['id_comprobante'] = $this->{$this->model}->id;
					$c_item['producto_codigo'] = $c_item['codigo'];
					
					foreach($c_item["ComprobanteItemLote"] as &$item_lote){
						
						if($item_lote["id_tipo_lote"] == EnumTipoLote::Interno){
							
							
							$existe = $this->{EnumModel::Lote}->find('count', array(
									'conditions' => array("Lote.codigo"=>$item_lote["codigo"]),
									'contain' =>false
							));
							
							if($existe >0){
								$message = "El Lote ".$item_lote["codigo"]." ya fue usado para otro IRM";
								return  1;
								
							}else{
								
								
								if(isset($item_lote["id_lote"]) && $item_lote["id_lote"]>0 ){
									
									$message = "El Lote ".$item_lote["codigo"]." ya fue usado para otro IRM";
									return  1;
								}else{//doy de alta el LOTE y lo doy de alta en comprobante_item
									
									$lote["codigo"] = $item_lote["codigo"];
									$lote["id_tipo_lote"] = $item_lote["id_tipo_lote"];
									$lote["fecha_emision"] = date('Y-m-d');
									$lote["d_lote"] = $item_lote["d_lote"];
									
									$this->{EnumModel::Lote}->saveAll($lote);
									$id_lote = $this->{EnumModel::Lote}->id;
									
									$item_lote["id_lote"] = $id_lote;
									
									
									
									
								}
								
								
							}
								
							
							
						}else{
							
							
							
							$lote = $this->{EnumModel::Lote}->find('first', array(
									'conditions' => array("Lote.codigo"=>$item_lote["codigo"]),
									'contain' =>false
							));
							
							if(count($lote)>0 && !isset($item_lote["id_lote"]) && !$item_lote["id_lote"]>0){
								$lote["codigo"] = $item_lote["codigo"];
								$lote["id_tipo_lote"] = $item_lote["id_tipo_lote"];
								$lote["fecha_emision"] = date('Y-m-d');
								$lote["d_lote"] = $item_lote["d_lote"];
								
								$this->{EnumModel::Lote}->saveAll($lote);
								$id_lote = $this->{EnumModel::Lote}->id;
								$item_lote["id_lote"] = $id_lote;
								
							}
							
							
							
							
							
						}
						
						
						
						
						$this->{EnumModel::ComprobanteItemLote}->saveAll($item_lote);
						
					}
					
				}
				$comprobante_item = $this->request->data['ComprobanteItem'];
				$this->Comprobante->ComprobanteItem->saveAll($comprobante_item);
				return  0;
			}
        
		}
		
		
		
		protected function SaveItems(&$data,&$message=''){
			
			
			
			$this->loadModel(EnumModel::Comprobante);
			$this->loadModel(EnumModel::ComprobanteItem);
			$this->loadModel(EnumModel::Lote);
			$this->loadModel(EnumModel::ComprobanteItemLote);
			
			$auxiliar = $this->request->data;
			$error = 0;
			
			if(isset($data['ComprobanteItem'])){
				unset($data['ComprobanteItem']['genera_movimiento_stock']);
				foreach($data['ComprobanteItem'] as &$c_item){
					
					if(!isset($c_item["id"]) || !$c_item["id"]>0){
						$c_item['id_comprobante'] = $this->{$this->model}->id;
						$c_item['producto_codigo'] = $c_item['codigo'];
						
						
						$this->ComprobanteItem->saveAll($c_item);
						$id_item = $this->ComprobanteItem->id;
					}else{
						
						$id_item = $c_item["id"];
						
					}
					$lotes = array();
				
					/*
					if(isset($c_item["Lotes"]))
						$lotes = json_decode($c_item["Lotes"],true);
			*/
					
					if(isset($c_item["Lotes"])){
						foreach($c_item["Lotes"] as &$item_lote){
							
							$item_lote["ComprobanteItemLote"]["id_comprobante_item"] = $id_item;
							$this->ComprobanteItemLote->saveAll($item_lote);
						}
					
					}
					
				}
			}
			$this->request->data =  $auxiliar;
			return $error;  
			
		}
      
		
		
		/**
		 * @secured(BTN_EXCEL_INFORMERECEPCIONMATERIALES)
		 */
		public function excelExport($vista="default",$metodo="index",$titulo=""){
			
			parent::excelExport($vista,$metodo,"Listado de IRM");
			
			
			
		}
		
		
		
protected function StockOnEdit(){
			
			
			$this->loadModel("Producto");
			$this->loadModel($this->model);
			
			// $this->revertirItems($this->request->data);//NO se esta usando
			
			if(($this->AceptaStock()== 1 && isset($this->request->data["ComprobanteItem"]) ) ){
				
				//recorro y actuo sobre el stock
				
				foreach($this->request->data["ComprobanteItem"] as $clave =>$item){
					
					
					$tiene_stock_producto = $this->Producto->AceptaStock($item["id_producto"]);
					
					//actualizo el stock xq nose como varios con respecto al anterior
					if( isset($item["id"]) && $tiene_stock_producto == 1 ){ //pregunto si ese producto acepta stock
						
						
						
						
						//busco el item antes de que sea modificado para ver estado anterior
						$c_item = $this->Comprobante->ComprobanteItem->find('first', array(
								'conditions' => array('ComprobanteItem.id' => $item["id"]),
								'contain' =>false
						));
						
						
						
						/*TODO: Esto hay que cambiarlo, hacer que IRM tenga el model de RecepcionMaterial*/
						$nombre_campo_q_afecta_stock = $this->{$this->model}->NombreComprobanteItemAfectaStock();
						
						if($this->request->data["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::InformeRecepcionMateriales)
							$nombre_campo_q_afecta_stock = "cantidad_cierre";
							
							
							
							$total = $c_item["ComprobanteItem"][$nombre_campo_q_afecta_stock];
							
							
							
							
							
							if(!($total ==  $item[$nombre_campo_q_afecta_stock]) ){   //NO se modifico no hago nada
								
								if($item[$nombre_campo_q_afecta_stock]  < $total ) { //  se restaron items
									// si sacao items del pedido porque son menos  actualizo el stock
									$cantidad_devuelta_items = $total - $item[$nombre_campo_q_afecta_stock] ;
									$this->genera_movimiento_stock($item["id"],EnumTipoMovimiento::AjusteNegativoComprobanteItem,$cantidad_devuelta_items);
									
								}else{     //agregue items me aumenta el comprometido
									
									$cantidad_agregar_items =  $item[$nombre_campo_q_afecta_stock] - $total  ;
									$this->genera_movimiento_stock($item["id"],EnumTipoMovimiento::AjustePositivoComprobanteItem,$cantidad_agregar_items);
									
								}
								
								
								unset($item);
								
								
							}
							
					}else{ //si agrego un item nuevo lo agrego y aumento el stock
						
						$this->Comprobante->ComprobanteItem->saveAll($item);
						$id_item_pi = $this->Comprobante->ComprobanteItem->id; //recupero el id del Item agregado
						unset($this->request->data["ComprobanteItem"][$clave]);
						if($tiene_stock_producto == 1)
							$this->genera_movimiento_stock($id_item_pi,EnumTipoMovimiento::AgregoItemComprobante);
							
					}
					
					
				}
				
				
				
			}elseif(isset($this->request->data["ComprobanteItem"])){
				
				
				foreach($this->request->data["ComprobanteItem"] as $clave =>$item){
					
					if(!isset($item["id"])){
						$this->Comprobante->ComprobanteItem->saveAll($item);
						unset($this->request->data["ComprobanteItem"][$clave]);
					}
				}
				
				
				
			}
			
			return true;
			
		}
		
		
		
		protected function genera_movimiento_stock($id_pi,$action,$cantidad_calculada=0,$data =null,$simbolo_operacion=1){    //esta funcion actualiza el stock a traves de un movimiento
			
			
			
			/*SOLO SE VA A MOVER STOCK SI ESTA CERRADO*/
			if($this->request->data["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::Abierto || $this->request->data["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion){
				
				$id_deposito = 0;
				$id_deposito_destino = 0;
				
				if(isset($this->request->data["Comprobante"]["id_deposito_origen"]))
					$id_deposito = $this->request->data["Comprobante"]["id_deposito_origen"];
					
					if(isset($this->request->data["Comprobante"]["id_deposito_destino"]))
						$id_deposito_destino = $this->request->data["Comprobante"]["id_deposito_destino"];
						
						
						
						
						
						
						switch($action){
							
							
							
							
							
							case EnumTipoMovimiento::Comprobante:
								
								
								
								$data = array();
								$data["Movimiento"]["id_movimiento_tipo"] = $action;
								$data["Movimiento"]["id_comprobante"] = $id_pi;
								$data["Movimiento"]["id_deposito_origen"] = $id_deposito;
								$data["Movimiento"]["id_deposito_destino"] = $id_deposito_destino;
								$data["Movimiento"]["llamada_interna"] = 1;
								$data["Movimiento"]["simbolo_operacion"] = $simbolo_operacion;
								
								
								break;
								
								
								
							case EnumTipoMovimiento::AjustePositivoComprobanteItem:
							case EnumTipoMovimiento::CanceloItemComprobante:
							case EnumTipoMovimiento::AgregoItemComprobante:
								
								
								$data = array();
								$data["Movimiento"]["id_movimiento_tipo"] = $action;
								$data["Movimiento"]["id_comprobante_item"] = $id_pi;
								$data["Movimiento"]["cantidad"] = $cantidad_calculada;
								$data["Movimiento"]["id_deposito_origen"] = $id_deposito;
								$data["Movimiento"]["id_deposito_destino"] = $id_deposito_destino;
								$data["Movimiento"]["llamada_interna"] = 1;
								$data["Movimiento"]["simbolo_operacion"] = $simbolo_operacion;
								
								
								
								break;
							case EnumTipoMovimiento::AjusteNegativoComprobanteItem:
								$data = array();
								$data["Movimiento"]["id_movimiento_tipo"] = $action;
								$data["Movimiento"]["id_comprobante_item"] = $id_pi;
								$data["Movimiento"]["cantidad"] = $cantidad_calculada;
								$data["Movimiento"]["id_deposito_origen"] = $id_deposito;
								$data["Movimiento"]["id_deposito_destino"] = $id_deposito_destino;
								$data["Movimiento"]["llamada_interna"] = 1;
								$data["Movimiento"]["simbolo_operacion"] = $simbolo_operacion;
								
								
								break;
								
								
								
								
						}
						
						if($id_deposito_destino!=0)
							$data["Movimiento"]["id_deposito_destino"] = $id_deposito_destino;
							
							
							// $this->request->data = array();//IMPORTANTE seteo en vacio porque si me manda por JSON el primer request y despues redirecciono el request->data queda cargado
							//realizo la llamada al controller y le envio el movimiento
							$this->requestAction(
									array('controller' => 'Movimientos', 'action' => 'add'),
									array('data' => $data)
									);
							
							
			}
			return true;
			
			
			
		}
    
}
?>