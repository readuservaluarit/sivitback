
$(document).ready(function() {

    /*
     * Accordion Menu
     */
    $('.menu').initMenu();
    
    /*
     * Slide Effect
     */
    $('.menu li a').slideList();
    
    /*
     * Scroll Effect
     */
    $('a[href*=#]').bind("click", function(event) {
        event.preventDefault();
        var target = $(this).attr("href");
        
        if (target == '#top') {
            $.jGrowl("Scrolling to the top.", { theme: 'information' });
        }
        
        $('html,body').animate({
            scrollTop: $(target).offset().top
        }, 1000 , function () {
            //location.hash = target;
            // finished scrolling
        });
    });
    
    /*
     * Toolbox
     */
    $('.toolbox-action').click(function() {
        $('.toolbox-content').fadeOut();
        $(this).next().fadeIn();
        
        return false;
    });
    
    $('.close-toolbox').click(function() {
        $(this).parents('.toolbox-content').fadeOut();
    });
    
    /*
     * Dropdown-menu for left sidebar
     */
    $('.user-button').click(function() {
        $('.dropdown-username-menu').slideToggle();
    });
    
    $(document).click(function(e){
        if (!$(e.target).is('.user-button, .arrow-link-down, .dropdown-username-menu *')) {
            $('.dropdown-username-menu').slideUp();
        }
    });
    
    var ddumTimer;
    
    $('.user-button, ul.dropdown-username-menu').mouseleave(function(e) {
        ddumTimer = setTimeout(function() {
            $('.dropdown-username-menu').slideUp();
        },400);
    });
    
    $('.user-button, ul.dropdown-username-menu').mouseenter(function(e) {
        clearTimeout(ddumTimer);
    });
    
    execThemeScripts();    

});
   

function execThemeScripts(){

    execScriptsOfTheme('');
    
}    

function execThemeScriptsSubForm(divId){
    
    execScriptsOfTheme('#' + divId + ' > .container_12 ');
    
}

function execScriptsOfTheme(div){
    
    /*
     * Form Elements
     */
    //$("select, input:checkbox, input:text, input:password, input:radio, input:file, textarea").uniform();
    $(div + " input:checkbox, input:text, input:password, input:radio, input:file, textarea").not(".multiselect").not("[class|='ui-multiselect']").not("[id|='ui-multiselect']").uniform();
    
    //Solo los combos que no tienen la clase multiselect ni la ui-multiselect-menu
    $(div + " select").not(".multiselect").not("[class|='ui-multiselect']").not(".multiselect").not("[id|='ui-multiselect']").uniform();

    
    /*
     * Closable Alert Boxes
     */
    $(div + ' span.hide').click(function() {
        $(this).parent().slideUp();                       
    });    

    /*
     * Closable Content Boxes
     */
    $(div + ' .block-border .block-header span').click(function() {
        if($(this).hasClass('closed')) {
            $(this).removeClass('closed');
        } else {
            $(this).addClass('closed');
        }
        
        $(this).parent().parent().children('.block-content').slideToggle();
    });

    /*
     * Tooltips
     */
    $(div + ' a[rel=tooltip]').tipsy({fade: true});
    $(div + ' a[rel=tooltip-bottom]').tipsy({fade: true});
    $(div + ' a[rel=tooltip-right]').tipsy({fade: true, gravity: 'w'});
    $(div + ' a[rel=tooltip-top]').tipsy({fade: true, gravity: 's'});
    $(div + ' a[rel=tooltip-left]').tipsy({fade: true, gravity: 'e'});
    
    $(div + ' a[rel=tooltip-html]').tipsy({fade: true, html: true});
    
    $(div + ' div[rel=tooltip]').tipsy({fade: true});
    $(div + ' div[rel=tooltip-top]').tipsy({fade: true, gravity: 's'});
    
    
    //Pickers
    $( div + ' .datepicker' ).datepicker();
    $( div + ' .datetimepicker' ).datetimepicker();  
}





















