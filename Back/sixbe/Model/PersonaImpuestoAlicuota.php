<?php
App::uses('AppModel', 'Model');
App::uses('Persona', 'Model');
App::uses('ComprobateImpuesto', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */

class PersonaImpuestoAlicuota extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'persona_impuesto_alicuota';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id_impuesto' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar un Impuesto.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
           
		),
        
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	/*public $hasMany = array(
		'Region' => array(
			'className' => 'Region',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public $childRecordMessage = "Existe por lo menos una regi&oacute;n, predio, establecimiento o usuario relacionado/a con la jurisdicci&oacute;n.";
    
    */
    
    
    public $belongsTo = array(
        'Persona' => array(
            'className' => 'Persona',
            'foreignKey' => 'id_persona',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Impuesto' => array(
            'className' => 'Impuesto',
            'foreignKey' => 'id_impuesto',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
   );
   
   
     public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     "id_sistema"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_impuesto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_sistema"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "fecha_vigencia_desde"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
     "fecha_vigencia_hasta"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_sin_vencimiento"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "alicuota_impuesto_original"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_activo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null)
     );
     
     }
     
   
   public function tieneImpuesto($id_persona,$id_impuesto,$activo=0){ //el flag activo pone filtros las fecha hasta del impuesto, significa que va a validar que este vijente
     $conditions = array(); 
    
    if($activo == 1){
        
        array_push($conditions,array("PersonaImpuestoAlicuota.fecha_vigencia_desde <="=>date('Y-m-d')));
        array_push($conditions,array("PersonaImpuestoAlicuota.fecha_vigencia_hasta >="=>date('Y-m-d')));
        
    }  
    
    
    array_push($conditions,array("PersonaImpuestoAlicuota.id_persona"=>$id_persona));
    array_push($conditions,array("PersonaImpuestoAlicuota.id_impuesto"=>$id_impuesto));
    
    $impuesto = $this->find('first', array(
                                                'conditions' => $conditions,
                                                'contain' =>false
                                     
                                                )); 
        
    
   if($impuesto)
    return true; 
   else
    return false;
}     


public function getAlicuotaRetencion($id_persona,$id_impuesto){
    
    $impuesto = $this->find('first', array(
                                                'conditions' => array('PersonaImpuestoAlicuota.id_persona' => $id_persona,'PersonaImpuestoAlicuota.id_impuesto' => $id_impuesto),
                                                'contain' =>false
                                                )); 
        
    
   if($impuesto)
    return $impuesto["PersonaImpuestoAlicuota"]["porcentaje_alicuota"]; 
   else
    return false;
}  


public function borrarImpuesto($id_impuesto){
    
    //busco los impuestos presentes en comprobante_impuesto
    
	//$comprobante_impuestos_obj = ClassRegistry::init('ComprobanteImpuesto');
	
	
	
	
    $this->deleteAll(array('PersonaImpuestoAlicuota.id_impuesto'=>$id_impuesto));
    
    
}

public function AgregarImpuesto($impuesto,$id_tipo_persona,$id_persona=0){
    
    /*Esta funcion le agrega a todas las personas de la BD este impuesto*/
    
    if($id_tipo_persona == EnumTipoPersona::Proveedor){
        
         $persona_obj = ClassRegistry::init('Persona');
         
         $personas_impuestos = array();
         
         
         
         
        
             $personas = $persona_obj->find('all',array(
             
                                                            'conditions'=>array("Persona.id_tipo_persona" =>$id_tipo_persona ),
                                                            'contain'=>false
                                                     ));
             
             if($personas){
                 
                 foreach($personas as $persona){
                     
                     $persona_impuesto["id_impuesto"] = $impuesto["id_impuesto"];
                     $persona_impuesto["fecha_vigencia_desde"] = $impuesto["fecha_vigencia_desde"];
                     $persona_impuesto["fecha_vigencia_hasta"] = $impuesto["fecha_vigencia_hasta"];
                     $persona_impuesto["porcentaje_alicuota"] = 0;
                     $persona_impuesto["sin_vencimiento"] = $impuesto["sin_vencimiento"];
                     $persona_impuesto["id_persona"] = $persona["Persona"]["id"];
                     
                     array_push($personas_impuestos,$persona_impuesto);
                 } 
                 if(count($personas_impuestos)>0){
                    
                    $retono = $this->saveAll($personas_impuestos);
                 
                 }   
                 
             }
         
        
      
    }
    
    return; 
    
    
    
    
}



public function AgregarImpuestoPersona($id_persona,$impuestos){
    
    
    if($impuestos && $id_persona>0){
        
        $personas_impuestos = array();
        foreach($impuestos as $impuesto){
            
                     $persona_impuesto["id_impuesto"] = $impuesto["Impuesto"]["id"];
                     $persona_impuesto["fecha_vigencia_desde"] = $impuesto["DatoEmpresaImpuesto"]["fecha_vigencia_desde"];
                     $persona_impuesto["fecha_vigencia_hasta"] = $impuesto["DatoEmpresaImpuesto"]["fecha_vigencia_hasta"];
                     
                     
                     
                     if( !is_null($impuesto["DatoEmpresaImpuesto"]["porcentaje_alicuota"]) && $impuesto["DatoEmpresaImpuesto"]["porcentaje_alicuota"] !=null)
                     	$persona_impuesto["porcentaje_alicuota"] = $impuesto["DatoEmpresaImpuesto"]["porcentaje_alicuota"];
                     else 
                     	$persona_impuesto["porcentaje_alicuota"] = 0;
                     
                     	
                     	
                     
                     
                     
                     $persona_impuesto["id_persona"] = $id_persona;
                     
                     array_push($personas_impuestos,$persona_impuesto);
            
        }
        
        if(count($personas_impuestos)>0){
                
                $this->SaveAll($personas_impuestos);
             
        }   

        
        
        
    }
    
    
    
}

public function filtrarBaseImponible($data){
	
	$impuestos_aceptados = array();
	foreach($data as $impuesto){
		
		if($impuesto[0]["base_imponible"]>=$impuesto["i"]["base_imponible_desde"] && $impuesto[0]["base_imponible"]<=$impuesto["i"]["base_imponible_hasta"])
			array_push($impuestos_aceptados,$impuesto);
		
		
	}
	
	return $impuestos_aceptados;
	
}


public function agregarGlobal($id_impuesto,$id_sistema){
	
	
	
	$impuesto_class = ClassRegistry::init('Impuesto');
	
	
	$this->borrarImpuesto($id_impuesto);//primero les borro a todas las personas esos impuestos y luego les agrego el global
	$id_sistema_impuesto = $impuesto_class->getSistema($id_impuesto);
	
	if($id_sistema_impuesto == EnumSistema::COMPRAS)
		$id_tipo_persona = EnumTipoPersona::Proveedor;
		elseif($id_sistema_impuesto == EnumSistema::VENTAS)
		$id_tipo_persona = EnumTipoPersona::Cliente;
		
		$impuesto["id_impuesto"] = $id_impuesto;
		$impuesto["id_impuesto"] = $id_impuesto;
		$impuesto["sin_vencimiento"] = 1;
		$impuesto["fecha_vigencia_desde"] = null;
		$impuesto["fecha_vigencia_hasta"] = null;
		$this->AgregarImpuesto($impuesto,$id_tipo_persona);
}

 
   
}
