<?php
App::uses('ComprobanteItem', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class ComprobanteItemComprobante extends ComprobanteItem {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	//public $useTable = 'comprobante_item_view';

    public $actsAs = array('Containable');
    
    // public $virtualFields = array(
    // 'nro_comprobante_origen' => 'nro_comprobante'
// );
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

public $belongsTo = array(
        'Comprobante' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ComprobanteOrigen' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante_origen',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
 );
  
 public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     $this->virtual_fields_view = array(
     
     "d_tipo_comprobante_origen"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "pto_nro_comprobante_origen"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "pto_nro_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "pto_nro_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "fecha_generacion_origen"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
     "fecha_vencimiento_origen"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
     "fecha_contable_origen"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
     "fecha_contable"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_estado_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "subtotal_comprobante_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     "total_comprobante_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     "saldo"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
     "nro_comprobante_origen"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     "signo_comercial"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
	 "id_moneda_origen"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
	 "valor_moneda"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
	 "valor_moneda_origen"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
	 "monto_diferencia_cambio"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
	 "diferencia_cambio_porcentaje"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>null),
	 "enlazar_con_moneda"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     "simbolo_signo_comercial"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null)
    );
}
 
public function GetItemprocesadosEnImportacion($id,$array_tipo_comprobante= array(),$array_id_estado = array() , $array_estado_omitir = array() ) { //el id del item y el tipo de comprobante
        
         
        
        if(count($array_tipo_comprobante) !=0){
            
           // $short_list_array = explode(',', $id_tipo_comprobante);
        $procesados = $this->find('all', array(
                                'conditions' => array('ComprobanteItem.id_comprobante_origen' => $id["id"],'Comprobante.id_tipo_comprobante' => $array_tipo_comprobante),
                                'contain' =>false,
                                'joins' => array(
                                                    array(
                                                        'table' => 'comprobante',
                                                        'alias' => 'Comprobante',
                                
                                                        'foreignKey' => 'id_comprobante',
                                                        'conditions'=> array(
                                                            'Comprobante.id = ComprobanteItem.id_comprobante', 
                                                            'ComprobanteItem.activo = 1', 
                                                        )
                                                        )
                                                        )
                             ));
        }else//devuelvo todos
            $procesados = $this->find('all', array(
                                'conditions' => array('ComprobanteItem.id_comprobante_origen' => $id,'ComprobanteItem.activo'=>1),
                                'contain' =>false
                             ));
        
        
        return $procesados;
        
    }
    

    
function getOrderItems($model,$id_comprobante=""){
    
    return array("".$model.".id"=>'ASC');
    //return array("".$model.".n_item"=>'ASC');
    
    
}



}
