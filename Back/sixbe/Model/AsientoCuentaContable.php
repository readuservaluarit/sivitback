<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class AsientoCuentaContable extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'asiento_cuenta_contable';

    public $actsAs = array('Containable');
    
   
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

public $belongsTo = array(
        'Asiento' => array(
            'className' => 'Asiento',
            'foreignKey' => 'id_asiento',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CuentaContable' => array(
            'className' => 'CuentaContable',
            'foreignKey' => 'id_cuenta_contable',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CentroCosto' => array(
            'className' => 'CentroCosto',
            'foreignKey' => 'id_centro_costo',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
 );
 
 
  public $hasMany = array(
 
 
         'AsientoCuentaContableCentroCostoItem' => array(
            'className' => 'AsientoCuentaContableCentroCostoItem',
            'foreignKey' => 'id_asiento_cuenta_contable',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        );
        
        
  public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     "nro_asiento"=>array("type"=>EnumTipoDato::biginteger,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_asiento"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_estado_asiento"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "debe_cuenta"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_cuenta_contable"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "debe_concepto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "debe_monto"=>array("type"=>EnumTipoDato::decimal,"show_grid"=>0,"default"=>null,"length"=>null,"unsigned"=>1),
     "haber_cuenta"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "haber_concepto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "haber_concepto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "haber_monto"=>array("type"=>EnumTipoDato::decimal,"show_grid"=>0,"default"=>null,"length"=>null,"unsigned"=>1),
     "d_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "razon_social"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "monto"=>array("type"=>EnumTipoDato::decimal,"show_grid"=>0,"default"=>null,"length"=>null,"unsigned"=>1),
     "id_estado_asiento"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     "id_ejercicio_contable"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     "id_tipo_asiento"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     "id_clase_asiento"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null),
     "observacion"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "codigo_cuenta_contable"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "fecha"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
     "saldo_acumulado"=>array("type"=>EnumTipoDato::decimal,"show_grid"=>0,"default"=>null,"length"=>null),
     "codigo_asiento"=>array("type"=>EnumTipoDato::biginteger,"show_grid"=>0,"default"=>null,"length"=>null),
     "pto_nro_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "fecha_asiento"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
     "tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "AsientoCuentaContableCentroCostoItem"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
    
     );
     
  }      
               
        
}
