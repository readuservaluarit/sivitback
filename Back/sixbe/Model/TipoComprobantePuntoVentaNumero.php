<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class TipoComprobantePuntoVentaNumero extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'tipo_comprobante_punto_venta_numero';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	

    
     
  public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
      
     );
     
     }
              
}