<?php
App::uses('AppModel', 'Model');
App::uses('Producto', 'Model');


/*App::uses('Componente', 'Model');
App::uses('ArticuloRelacion', 'Model');

*/
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class MateriaPrima extends Producto {
    
    
   
/**
 * Use table
 *
 * @var mixed False or table name
 */
	
    

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'codigo' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar una c&oacute;digo v&aacute;lido',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 

 public $hasMany = array(
      'ArticuloRelacion' => array(
                'className' => 'ArticuloRelacion',
                'foreignKey' => 'id_producto_padre',
                'dependent' => false,
                'conditions' => '',
                'fields' => '',
                'order' => '',
                'limit' => '',
                'offset' => '',
                'exclusive' => '',
                'finderQuery' => '',
                'counterQuery' => ''
            ),
       'StockMateriaPrima' => array(
            'className' => 'StockMateriaPrima',
            'foreignKey' => 'id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'ListaPrecioProducto' => array(
            'className' => 'ListaPrecioProducto',
            'foreignKey' => 'id_producto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
 		'ProductoCodigo' => array(
 				'className' => 'ProductoCodigo',
 				'foreignKey' => 'id_producto',
 				'dependent' => false,
 				'conditions' => '',
 				'fields' => '',
 				'order' => '',
 				'limit' => '',
 				'offset' => '',
 				'exclusive' => '',
 				'finderQuery' => '',
 				'counterQuery' => ''
 		),
        
        
        );

       
 /*
public function afterSave($created, $options = array())  {
   
   
   //solo en el update    
    if(!$created){
        
       
        $mp_componente = new ArticuloRelacion();
        
        
        $mpc = $mp_componente->find('all', array(
        'conditions' => array('ArticuloRelacion.id_materia_prima' => $this->data["MateriaPrima"]["id"]),
        'contain' =>false
    ));
        if($mpc){ //por cada componente encontrado el costo
        
            $componente = new Componente();
            foreach($mpc as $materia_prima_componente){ //por cada item de materia prima actualizo el componente
                
                $id_componente = $materia_prima_componente["ArticuloRelacion"]["id_componente"];
                
                $costo_componente = $this->data["MateriaPrima"]["costo"] * $materia_prima_componente["ArticuloRelacion"]["cantidad"];
            
                $componente->updateAll(
                                  array('Componente.costo' => $costo_componente ),
                                  array('Componente.id' => $id_componente) );
                
            }
        
        }
  }
}

	
*/
}
