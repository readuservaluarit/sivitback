<?php
        include(APP.'View'.DS.'Comprobantes'.DS.'json'.DS.'default.ctp');
        // CONFIG  CON MODE DISPLAYED CELLS
     
        $model["codigo_tipo_comprobante"]["show_grid"] = 1;
        $model["codigo_tipo_comprobante"]["header_display"] = "Tipo";
        $model["codigo_tipo_comprobante"]["width"] = "6";
        $model["codigo_tipo_comprobante"]["order"] = "1";
        $model["codigo_tipo_comprobante"]["text_colour"] = "#000000";
                
        $model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
        $model["pto_nro_comprobante"]["width"] = "9"; //REstriccion mas de esto no se puede achicar
        $model["pto_nro_comprobante"]["order"] = "2";
        $model["pto_nro_comprobante"]["text_colour"] = "#000000";

        $model["fecha_generacion"]["show_grid"] = 1;
        $model["fecha_generacion"]["header_display"] = "Fecha Creaci&oacute;n";
	    $model["fecha_generacion"]["width"] = "8";
        $model["fecha_generacion"]["order"] = "3";
        $model["fecha_generacion"]["text_colour"] = "#0000FF";
        
        $model["codigo_persona"]["show_grid"] = 1;
        $model["codigo_persona"]["header_display"] = "Cod. Cliente";
        $model["codigo_persona"]["width"] = "4";
        $model["codigo_persona"]["order"] = "4";
        $model["codigo_persona"]["text_colour"] = "#000000";
        
        $model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["header_display"] = "Raz&oacute;n social";
        $model["razon_social"]["width"] = "10";
        $model["razon_social"]["order"] = "5";
        $model["razon_social"]["text_colour"] = "#000000";
        
        $model["fecha_aprobacion"]["show_grid"] = 1;
        $model["fecha_aprobacion"]["header_display"] = "Fecha Aprobaci&oacute;n";
        $model["fecha_aprobacion"]["width"] = "8";
        $model["fecha_aprobacion"]["order"] = "6";
        $model["fecha_aprobacion"]["text_colour"] = "#0000FF";
        
        
        $model["fecha_contable"]["show_grid"] = 1;
        $model["fecha_contable"]["header_display"] = "Fecha Contable";
        $model["fecha_contable"]["width"] = "8";
        $model["fecha_contable"]["order"] = "7";
        $model["fecha_contable"]["text_colour"] = "#0000FF";
		
		$model["fecha_vencimiento"]["show_grid"] = 1;
        $model["fecha_vencimiento"]["header_display"] = "Fecha Vencimiento";
        $model["fecha_vencimiento"]["width"] = "8";
        $model["fecha_vencimiento"]["order"] = "8";
        $model["fecha_vencimiento"]["text_colour"] = "#FF0000";//RED
        
        $model["d_moneda_simbolo"]["show_grid"] = 1;
        $model["d_moneda_simbolo"]["header_display"] = "Moneda";
        $model["d_moneda_simbolo"]["width"] = "4";
        $model["d_moneda_simbolo"]["order"] = "9";
        $model["d_moneda_simbolo"]["text_colour"] = "#008800";//DEEP GREEN
        
		$model["total_comprobante"]["show_grid"] = 1;
        $model["total_comprobante"]["header_display"] = "Total Comprobate";
        $model["total_comprobante"]["width"] = "7";
        $model["total_comprobante"]["order"] = "10";
        $model["total_comprobante"]["text_colour"] = "#F0000F";
        
        $model["d_estado_comprobante"]["show_grid"] = 1;
        $model["d_estado_comprobante"]["header_display"] = "Estado";
        $model["d_estado_comprobante"]["width"] = "6";
        $model["d_estado_comprobante"]["order"] = "11";
        $model["d_estado_comprobante"]["text_colour"] = "#004400";
        
        
         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Comprobante",
                    "content" => $model
                );
        
          echo json_encode($output);
?>