<?php


class PersonasController extends AppController {
    public $name = 'Personas';
    public $model = 'Persona';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    protected function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->loadModel("ListaPrecio");
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 
													 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros($this->model);
        
      	$id_lista_precio_default = $this->ListaPrecio->getDefault();
        
    	$conditions_persona_impuesto = array();
    	
    	$array = array("PersonaImpuestoAlicuota.fecha_vigencia_desde <="=>date('Y-m-d'),
						"PersonaImpuestoAlicuota.fecha_vigencia_hasta >= "=>date('Y-m-d'));
   		
    	//array_push($conditions_persona_impuesto, $array);
    	array_push($conditions_persona_impuesto, array("OR"=>array("PersonaImpuestoAlicuota.sin_vencimiento"=>1,$array)));
    
    
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
        		'contain' =>array('PersonaImpuestoAlicuota'=>array("conditions"=>$conditions_persona_impuesto,"Impuesto"=>array("TipoImpuesto"))),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order'=>"Persona.razon_social asc"
        );
        $this->PaginatorModificado->settings = $this->paginate;

    	$this->paginado = 0;
        $data = $this->PaginatorModificado->paginate($this->model);
       	$page_count = $this->params['paging'][$this->model]['pageCount'];
       
        /*
    	$data = Cache::read("Persona".$this->id_tipo_persona);
    	
    	if($data == null){
    		
    		$this->{$this->model}->setCache($this->id_tipo_persona);
    		$data = Cache::read("Persona".$this->id_tipo_persona);
    	}
    	*/
    	
       	$hoy = date('Y-m-d');
       	foreach($data as &$persona){
       					
       		
       		foreach ($persona[EnumModel::PersonaImpuestoAlicuota] as &$impuesto){
       			
       			$impuesto["d_impuesto"] = $impuesto[EnumModel::Impuesto]["d_impuesto"];
       			$impuesto["id_tipo_impuesto"] = $impuesto[EnumModel::Impuesto]["id_tipo_impuesto"];
       			unset($impuesto[EnumModel::Impuesto]);
       		}
       		
       		
       		$persona[$this->model][EnumModel::PersonaImpuestoAlicuota] = $persona[EnumModel::PersonaImpuestoAlicuota];
       		
       		
       		
       		unset($persona[EnumModel::PersonaImpuestoAlicuota]);
       	}
       	
       	
        $this->data = $data;
        
        $this->set('data',$this->data );
        
        
        $seteo_form_builder = array("showActionColumn" =>false,"showNewButton" =>false,"showActionColumn"=>false,"showFooter"=>true,"showHeaderListado"=>true);
        
        $this->title_form = "Listado";
        
        $this->preparaHtmLFormBuilder($this,$seteo_form_builder,"default");
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     
        
        
        
        
    //fin vista json
        
    }
    
    
    protected  function RecuperoFiltros($model){
    
    //Recuperacion de Filtros
        $id = $this->getFromRequestOrSession('Persona.id');
        $razon_social = $this->getFromRequestOrSession('Persona.razon_social');
        $cuit = trim($this->getFromRequestOrSession('Persona.cuit'));
        $id_provincia = $this->getFromRequestOrSession('Persona.id_provincia');
        $id_pais = $this->getFromRequestOrSession('Persona.id_pais');
        $apellido = $this->getFromRequestOrSession('Persona.apellido');        
        $nombre = $this->getFromRequestOrSession('Persona.nombre'); 
        $id_persona_padre = $this->getFromRequestOrSession('Persona.id_persona_padre'); 
        $d_persona = $this->getFromRequestOrSession('Persona.d_persona');
        $id_migracion = $this->getFromRequestOrSession('Persona.id_migracion');
        $codigo = $this->getFromRequestOrSession('Persona.codigo');
        $id_estado_persona = $this->getFromRequestOrSession('Persona.id_estado_persona');
        $codigo_persona_padre = $this->getFromRequestOrSession('Persona.codigo_persona_padre');
        $id_persona_categoria = $this->getFromRequestOrSession('Persona.id_persona_categoria');
		$id_tipo_iva = $this->getFromRequestOrSession('Persona.id_tipo_iva');
       
      //$id = '19786';
	  
        $id_tipo_persona = $this->id_tipo_persona;
        
        $conditions = array(); 
         array_push($conditions, array('Persona.activo =' => 1)); 
        if($id!="")
            array_push($conditions, array('Persona.id' =>  $id)); 
             
        if($razon_social!="" && $id =="")
            array_push($conditions, array('Persona.razon_social LIKE' => '%' . $razon_social  . '%'));
        
            
            
            
         if($cuit!="")
            array_push($conditions, array('Persona.cuit LIKE' => '%' . $cuit  . '%'));    
            
          
        if($id_tipo_persona!="")
            array_push($conditions, array('Persona.id_tipo_persona' => $id_tipo_persona)); 
            
            
        if($id_provincia!="")
            array_push($conditions, array('Persona.id_provincia' => $id_provincia)); 
            
            
         if($id_pais!="")
            array_push($conditions, array('Persona.id_pais' => $id_pais)); 
            
         if($apellido!="")
            array_push($conditions, array('Persona.apellido LIKE' => '%' . $apellido .'%')); 
            
         if($nombre!="")
            array_push($conditions, array('Persona.nombre LIKE' => '%' . $nombre. '%' )); 
            
         if($id_persona_padre!="")
            array_push($conditions, array('Persona.id_persona_padre' => $id_persona_padre)); 
            
         if($d_persona!="")
            array_push($conditions, array('Persona.d_persona LIKE' => '%' . $d_persona  . '%')); 
            
         if($id_migracion!="")
            array_push($conditions, array('Persona.id_migracion' => $id_migracion));             
                   
         if($codigo!="")
            array_push($conditions, array('Persona.codigo' => $codigo ));
         
         if($id_estado_persona!="")
            array_push($conditions, array('Persona.id_estado_persona' => $id_estado_persona));  
            
          if($codigo_persona_padre!="")
            array_push($conditions, array('PersonaPadre.codigo' => $codigo_persona_padre));         
              
         if($id_persona_categoria!="")
         	array_push($conditions, array('Persona.id_persona_categoria' => $id_persona_categoria));  
		
		if($id_tipo_iva!="")
		{
		    $id_tipo_iva_array = explode(',', $id_tipo_iva);
		    array_push($conditions, array('Persona.id_tipo_iva ' => $id_tipo_iva_array));
		}
		
		
		
            
        return $conditions;
}
    
   
    protected function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        $id_provincia = '';
        if ($mode != "A"){
            $this->Persona->id = $id;
            $this->Persona->contain("Provincia");
            $this->request->data = $this->Persona->read();
        } 

        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Persona');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Personas');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Personas');
        else
            $formBuilder->setTituloForm('Consulta de Personas');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
$formBuilder->addFormInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('cuit', 'Cuit', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('calle', 'Calle', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  		
		$formBuilder->addFormInput('numero_calle', 'numero_calle', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  		
        $formBuilder->addFormInput('tel', 'Tel&eacute;fono', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
		$formBuilder->addFormInput('ciudad', 'Ciudad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('piso', 'Piso', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
		$formBuilder->addFormInput('dpto', 'dpto', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        //$formBuilder->addFormInput('posicion', 'posicion', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
		$formBuilder->addFormInput('codigo_postal', 'Codigo Postal', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
		$formBuilder->addFormInput('email', 'email', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
		$formBuilder->addFormInput('ib', 'ib', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        //$formBuilder->addFormInput('id_cuenta', 'id_cuenta', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        //$formBuilder->addFormInput('excento', 'excento', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
		$formBuilder->addFormInput('localidad', 'localidad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
		//$formBuilder->addFormInput('id_pais', 'id_pais', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
		 
         //Jurisdiccion
       
        $id_provincia='';
        $d_provincia='';
        if ($mode == "M") {
            $id_provincia=$this->request->data['Provincia']['id'];
            //$d_provincia=$this->request->data['Jurisdiccion']['nombre'];
            $prov = $this->Persona->Provincia->find('first', array('conditions' => array('Provincia.id =' => $id_provincia)));
            $d_provincia = $prov['Provincia']['d_provincia'];
        }
        $selectionList = array($this->model."IdProvincia" => "id", $this->model."DProvincia" => "d_provincia");
        $formBuilder->addFormPicker('ProvinciaPicker', $selectionList, 'id_provincia', 
                                   array('value' => $id_provincia), 'd_provincia', 'Provincia', 
                                   array('class'=>'control-group span5'), array('class' => '', 'label' => false, 'value' => $d_provincia)
                                   );  
        
        $formBuilder->addFormEndRow();   
        
		//validator.validateNumeric('PersonaDescuento', 'El Descuento ingresado debe ser num&eacute;rico');
		
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'PersonaAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('PersonaRazonSocial', 'Debe ingresar una Raz&oacute;n Social');
                validator.validateRequired('PersonaCuit', 'Debe ingresar un CUIT');
                
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_cuit',
                        'data': 'cuit=' + $('#PersonaCuit').val()+'&id='+$('#PersonaId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('PersonaCuit','El Cuit que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

    
    protected function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }


    protected function add() 
    {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $this->loadModel("Impuesto");
            $this->loadModel("DatoEmpresa");
            $this->loadModel("PersonaImpuestoAlicuota");
            $id_add = '';
			$codigo_add = '';
            $mensaje = '';
            
            
            $this->request->data[$this->model]["fecha_creacion"] = date("Y-m-d H:i:s");
            
            try{
                
                $codigo_return = 1;
                if( strlen($this->request->data[$this->model]["codigo"]) == 0 ){
                    $codigo = $this->{$this->model}->getCodigo($this->id_tipo_persona);
                    $codigo_return = $codigo;
                    
                }else{
                    $codigo =  $this->request->data[$this->model]["codigo"];
                }
                
                if($codigo_return !=0){    
                    
                    $existe_codigo = $this->existe_codigo($codigo,'',1);
                
                    if( $existe_codigo == 0){
                        
                        
                            $this->request->data[$this->model]["codigo"] = $codigo;
                        
                       
                            if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
                                
                                   
                                        $id_add = $this->Persona->id;
								        $codigo_add = $codigo;
								        
                                        if($this->request->data[$this->model]["id_tipo_persona"] == EnumTipoPersona::Cliente){
                                        //si en dato_empresa_impuesto hay un impuesto aplicable a todos los clientes entonces se lo agrego        
                                            $impuestos = $this->DatoEmpresa->getImpuestosGlobalCliente();    
                                        }elseif($this->request->data[$this->model]["id_tipo_persona"] == EnumTipoPersona::Proveedor){
                                         //si en dato_empresa_impuesto hay un impuesto aplicable a todos los proveedores entonces se lo agrego           
                                            $impuestos = $this->DatoEmpresa->getImpuestosGlobalProveedor();      
                                        }
                                        
                                        if(isset($impuestos) && count($impuestos)>0)
                                            $this->PersonaImpuestoAlicuota->AgregarImpuestoPersona($id_add,$impuestos);
                                        
                                        $mensaje = "La Persona ha sido creada exitosamente";
                                        $tipo = EnumError::SUCCESS;
                                       
                                    }else{
                                        
                                        $errores = $this->{$this->model}->validationErrors;
                                        $errores_string = "";
                                        foreach ($errores as $error){
                                            $errores_string.= "&bull; ".$error[0]."\n";
                                            
                                        }
                                        $mensaje = $errores_string;
                                        $tipo = EnumError::ERROR; 
                                        
                                        
                                        
                                    }
                            
                        
                    }else{
                     
                     $mensaje = "Ha ocurrido un error,el c&oacute;digo asignado a la persona ya esta asignado";
                     $tipo = EnumError::ERROR;  
                     
                    } 
             
             }else{
                        
                        $mensaje = "Ha ocurrido un error,se debe definir el contador para la persona";
                        $tipo = EnumError::ERROR;   
             }  
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,la Persona no ha podido ser creada.".$mensaje."</br>".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" => $id_add,
			"codigo_add" => $codigo_add,
             );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add","codigo_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
   
   
 
   protected function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            
            
            
            //TODO: si una persona tiene un comprobante abierto no la podes dar de baja
            
             if( strlen($this->request->data[$this->model]["codigo"]) == 0 )
                    $codigo = $this->{$this->model}->getCodigo($this->id_tipo_persona);
                else
                    $codigo =  $this->request->data[$this->model]["codigo"];
                
                $existe_codigo = $this->existe_codigo($codigo,$id,1);
                
                if( $existe_codigo == 0 ){
            
            try{
            
            
            
             
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "Ha sido modificado exitosamente";
                     $status =   EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
               }
               
               
                    
                    
                    
            }catch(Exception $e){
                
            	$status = EnumError::ERROR;
            	$mensaje = $e->getMessage();
                
            }
            
            }else{
                
                $mensaje = "Ha ocurrido un error,el c&oacute;digo asignado a la persona ya esta asignado";
                $status = EnumError::ERROR;   
            }
            
            
            if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
           }else{
                $this->Session->setFlash($mensaje, $status);
                $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                
           } 
            
            
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            $this->{$this->model}->contain('Provincia','Pais');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
  
    
    protected function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->Persona->id = $id;
       try{
            if ($this->Persona->saveField('activo', "0")) {
                
                $this->Persona->saveField('fecha_borrado',date("Y-m-d H:i:s"));
          
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "Ha sido eliminada exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }

    }

    
    protected function home(){
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
    
  
  
    protected function existe_cuit(){
        
        $cuit = $this->request->data['cuit'];
        $id_Persona = $this->request->data['id'];
        $id_tipo_persona_Persona = $this->id_tipo_persona;
        $this->loadModel("Persona");
        
        switch($id_tipo_persona_Persona)
        {   /*El activo se pone para permitir dar de alta una persona con un cuit que ya existia en la BD, pero el cuit que ya existia no debe estar activo */
            case EnumTipoPersona::Cliente:
                $id_estado_persona = EnumEstadoPersona::ActivoCliente;
            break;
            case EnumTipoPersona::Proveedor:
                $id_estado_persona = EnumEstadoPersona::ActivoProveedor;
            break;
            case EnumTipoPersona::Empleado:
                $id_estado_persona = EnumEstadoPersona::ActivoEmpleado;
            break;
            case EnumTipoPersona::Transportista:
                $id_estado_persona = EnumEstadoPersona::ActivoTransportista;
            break;
            
        }
        
        
        
      
        $data = $this->Persona->find('count',
									array('conditions' => array(
																'Persona.cuit' => $cuit, 
																'Persona.id !=' =>$id_Persona,
                                                                'Persona.id_tipo_persona =' =>$id_tipo_persona_Persona,
																'Persona.id_estado_persona =' => $id_estado_persona,
																)
										));
        
       
        
        if($data>0)
        	$error = EnumError::ERROR;
        else
        	$error = EnumError::SUCCESS;
        		
       $output = array(
        				"status" => $error,
        				"message" => "",
        				"content" => $data
        		);
       $this->set($output);
       $this->set("_serialize", array("status", "message", "content"));     
    }
    
    
    
    protected function existe_codigo($codigo='',$id_Persona ='',$llamada_interna=0){
        
        
        if($codigo ==''){
            
            if(isset($this->request->data['codigo']))
                $codigo = $this->request->data['codigo'];
            else
                $codigo = $this->request->query['codigo'];
        }    
        
        
        if(isset($this->request->query['id']) && $this->request->query['id']>0)    
            $id_Persona = $this->request->query['id'];
        
        
            if(($id_Persona =='' || is_null($id_Persona)) && isset($this->request->data['id']) )
            	$id_Persona = $this->request->data['id'];
        
        $id_tipo_persona_Persona = $this->id_tipo_persona;
        
        $this->loadModel("Persona");
        
        
        
        switch($id_tipo_persona_Persona){
            
            /*El activo se pone para permitir dar de alta una persona con un cuit que ya existia en la BD, pero el cuit que ya existia no debe estar activo
            */
            
            case EnumTipoPersona::Cliente:
                $id_estado_persona = EnumEstadoPersona::ActivoCliente;
            break;
            case EnumTipoPersona::Proveedor:
                $id_estado_persona = EnumEstadoPersona::ActivoProveedor;
            break;
            case EnumTipoPersona::Empleado:
                $id_estado_persona = EnumEstadoPersona::ActivoEmpleado;
            break;
			case EnumTipoPersona::Transportista:
                $id_estado_persona = EnumEstadoPersona::ActivoTransportista;
            break;
            case EnumTipoPersona::Sucursal:
                $id_estado_persona = EnumEstadoPersona::ActivoSucursal;
            break;
            case EnumTipoPersona::Contacto:
                $id_estado_persona = EnumEstadoPersona::ActivoContacto;
            break;
            
            
        }
        
      
        $existe = $this->Persona->find('count',
                                    array('conditions' => array(
                                                                'Persona.codigo' => $codigo, 
                                                                'Persona.id !=' =>$id_Persona,
                                                                'Persona.id_tipo_persona =' =>$id_tipo_persona_Persona,
                                                                'Persona.id_estado_persona =' => $id_estado_persona,
                                                                )
                                        ));
        
        $data = $existe;
        
        
        if($llamada_interna == 0){

        	if($data >0)
        		$error = EnumError::ERROR;
        	else
        		$error = EnumError::SUCCESS;
            
            $output = array(
            		"status" => $error,
            		"message" => "",
            		"content" => $data
            );
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content"));           
            
            
            
     
        }else{
            
            return  $data;
        }
    }
    
   
    
    protected function getDomicilio(){
    	/*Esta funcion puede devolver el domicilio FISCAL, el fijo lugar_entrega*/
    	
    	$id_persona = $this->request->data['id_persona'];
    	$entrega_domicilio = $this->request->data['entrega_domicilio'];
    	
    	$this->loadModel(EnumModel::Persona);
    	
    	$persona = $this->Persona->find('first',
    			array('conditions' => array(
    
    					'Persona.id' =>$id_persona
    					
    					
    			),
    					
    					'contain'=>array("Provincia","Pais")
    					
    			));
    	
    	
    	$provincia = "";
    	$pais = "";
    	$localidad = "";
    	$calle = "";
    	$numero_calle = "";
    	
    	/*
    	this.lugar_entrega.Text = form.dgv.GetSelectedRow_Cell("calle").ToString() + " " +
    	form.dgv.GetSelectedRow_Cell("numero_calle").ToString() + ", " +
    	form.dgv.GetSelectedRow_Cell("localidad").ToString() + " " +
    	form.dgv.GetSelectedRow_Cell("ciudad").ToString() + ", " +
    	form.dgv.GetSelectedRow_Cell("d_provincia").ToString();
    	
    	*/
    	
    	$direccion = "";
    	
    	if($persona){
    		
    		
    		if($entrega_domicilio== EnumEntregaPedido::DomicilioFiscal)
    			$direccion = $persona["Persona"]["calle"]." ".$persona["Persona"]["numero_calle"]." ".$persona["Persona"]["localidad"]." ".$persona["Persona"]["ciudad"]." ".$persona["Provincia"]["d_provincia"];
    		
    		if($entrega_domicilio == EnumEntregaPedido::DomicilioFijoCliente)
    			$direccion = $persona["Persona"]["lugar_entrega"];
    		
    			
    	}
    	
    	
    	$output = array(
    			"status" => EnumError::SUCCESS,
    			"message" => "",
    			"content" => $direccion
    	);
    	$this->set($output);
    	$this->set("_serialize", array("status", "message", "content"));           
    	
    }
    
    
    
    
    
    protected function  getDataCuitFromAfip(){
    	
    	$this->loadModel(EnumModel::Persona);
		
    	
    	
    	$cuit = trim($this->getFromRequestOrSession('Persona.cuit'));
    		

    	// $cuit = "33558510809";
    	
    	$output  = $this->Persona->getDataCuitFromAfip($cuit);
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message","page_count", "content"));
    	
    }

    
   
    
    
    
   
    
}
?>