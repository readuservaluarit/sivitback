<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class TipoImpuesto extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'tipo_impuesto';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Your custom message here',
                'allowEmpty' => false,
                'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),

	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

public $hasMany = array(
        'SubTipoImpuesto' => array(
            'className' => 'SubTipoImpuesto',
            'foreignKey' => 'id_tipo_impuesto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
 
 
 
 
 
 public function getDescripcion($id){
    
    $tipo_impuesto = $this->find('first', array(
                                                'conditions' => array('TipoImpuesto.id' => $id),
                                                'contain' =>false
                                                )); 
        
    
   return $tipo_impuesto["TipoImpuesto"]["d_tipo_impuesto"]; 
}



}
