<?php

/**
* @secured(CONSULTA_ESTADO_CHEQUE)
*/
class EstadosChequeController extends AppController {
    public $name = 'EstadosCheque';
    public $model = 'EstadoCheque';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
        $nombre = strtolower($this->getFromRequestOrSession('EstadoCheque.d_estado_cheque'));
        
        $id_tipo_cheque = strtolower($this->getFromRequestOrSession('EstadoCheque.id_tipo_cheque'));
        $id_tipo_cheque = 1;
        $conditions = array(); 
        if ($nombre != "") {
           array_push($conditions,array('LOWER(EstadoCheque.d_ESTADO_CHEQUE) LIKE' => '%' . $nombre . '%'));
        }

       // array_push($conditions,array('EstadoTipoCheque.id_tipo_cheque' =>  $id_tipo_cheque));
  
      /*'joins' =>array (array(
                'table' => 'estado_tipo_cheque',
                'alias' => 'EstadoTipoCheque',
                'type' => 'LEFT',
                'conditions' => array(
                    'EstadoTipoCheque.id_tipo_cheque ='.$id_tipo_cheque
                )
                
            )),*/  
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            
            //'contain'=>array('EstadoTipoCheque'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de EstadosCheque', 'Datos de los EstadosCheque', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_ESTADO_CHEQUE', 'Nombre de EstadoCheque', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'EstadoCheque.id', "10%");
                $formBuilder->addHeader('Nombre', 'EstadoCheque.d_moneda', "50%");
             

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_ESTADO_CHEQUE');
        
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->EstadoCheque->id = $id;
            $this->request->data = $this->EstadoCheque->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('EstadoCheque');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de EstadoCheque');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de EstadoCheque');
        else
            $formBuilder->setTituloForm('Consulta de EstadoCheque');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_ESTADO_CHEQUE', 'Nombre de EstadoCheque', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
     
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'EstadoChequeAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('EstadoChequeDEstadoCheque', 'Debe ingresar un nombre');
        


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_ESTADO_CHEQUE,READONLY_PROTECTED)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            if ($this->EstadoCheque->save($this->request->data))
                $this->Session->setFlash('El EstadoCheque ha sido creada exitosamente.', 'success'); 
            else
                $this->Session->setFlash('Ha ocurrido un error, el EstadoCheque no ha podido ser creada.', 'error');

            $this->redirect(array('action' => 'index'));
        } 

        $this->redirect(array('action' => 'abm', 'A'));                   
    }

    /**
    * @secured(MODIFICACION_ESTADO_CHEQUE,READONLY_PROTECTED)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->EstadoCheque->id = $id;
            
            try{ 
                if ($this->EstadoCheque->saveAll($this->request->data)){
                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "El EstadoCheque ha sido modificado exitosamente",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('El EstadoCheque ha sido modificado exitosamente.', 'success');
                        $this->redirect(array('controller' => 'Monedas', 'action' => 'index'));
                        
                    } 
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, el EstadoCheque no ha podido modificarse.', 'error');
                
            }
            $this->redirect(array('action' => 'index'));
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }

    
    
    /**
     * @secured(CONSULTA_ESTADO_CHEQUE)
     */
    public function getModel($vista='default'){
    	
    	$model = parent::getModelCamposDefault();
    	$model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
    	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
    	
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    	
    	
    	
    	$this->set('model',$model);
    	Configure::write('debug',0);
    	$this->render($vista);
    	
    	
    	
    	
    	
    	
    }


}
?>