<?php


class RRHHNovedadPersonasController extends AppController {
    public $name = 'RRHHNovedadPersonas';
    public $model = RRHHNovedadPersona::class;
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    
    /**
     * @secured(CONSULTA_RRHH_NOVEDAD_PERSONA)
     */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->loadModel("ListaPrecio");
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 
													 'update' => 'main-content', 'evalScripts' => true);
        
       $conditions = $this->RecuperoFiltros($this->model);
        
    
    
    
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
        	'contain' =>array(RRHHLiquidacion::class,RRHHConcepto::class),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
        	'order'=>$this->model.".indice asc"
        );
        $this->PaginatorModificado->settings = $this->paginate;

    
        $data = $this->PaginatorModificado->paginate($this->model);
       	$page_count = $this->params['paging'][$this->model]['pageCount'];
       
      
    	

       	foreach($data as &$novedad){
       					
       		$persona[$this->model][RRHHNovedadPersona::class]["chk_es_cantidad"] = $persona[RRHHConcepto::class]["chk_es_cantidad"];
       		$persona[$this->model][RRHHNovedadPersona::class]["chk_es_valor_unitario"] = $persona[RRHHConcepto::class]["chk_es_valor_unitario"];
       		$persona[$this->model][RRHHNovedadPersona::class]["chk_es_importe"] = $persona[RRHHConcepto::class]["chk_es_importe"];
       		$persona[$this->model][RRHHNovedadPersona::class]["d_rrhh_concepto"] = $persona[RRHHConcepto::class]["d_rrhh_concepto"];
       		$persona[$this->model][RRHHNovedadPersona::class]["codigo"] = $persona[RRHHConcepto::class]["codigo"];
       		$persona[$this->model][RRHHNovedadPersona::class]["chk_es_cantidad"] = $persona[RRHHConcepto::class]["chk_es_cantidad"];
       		$persona[$this->model][RRHHNovedadPersona::class]["chk_es_valor_unitario"] = $persona[RRHHConcepto::class]["chk_es_valor_unitario"];
       		
       	}
       
       	
        $this->data = $data;
        
        $this->set('data',$this->data );
        
        

        
        
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     
        
        
        
        
    //fin vista json
        
    }
    
    
    protected  function RecuperoFiltros($model){

    	
    	
    	$conditions = array(); 
        $id_persona = $this->getFromRequestOrSession('Persona.id');
        $nombre = $this->getFromRequestOrSession('Persona.nombre');
        $apellido = $this->getFromRequestOrSession('Persona.apellido');
        $id_concepto = $this->getFromRequestOrSession('RRHHConcepto.id');
        $codigo_concepto = $this->getFromRequestOrSession('RRHHConcepto.codigo');

        
        
        
        
        
        
        
        if($id_persona!="")
        	array_push($conditions, array('Persona.id' =>  $id_persona)); 
             
        if($nombre!="" && $id_persona =="")
            	array_push($conditions, array('Persona.nombre LIKE' => '%' . $nombre  . '%'));
        
        if($nombre!="" && $id_persona =="")
        		array_push($conditions, array('Persona.apellido LIKE' => '%' . $apellido  . '%'));
            
        if($id_concepto!="")
        	array_push($conditions, array('RRHHConcepto.id' =>  $id_concepto)); 
      
         if($codigo_concepto!="")
         	array_push($conditions, array('RRHHConcepto.codigo' =>  $codigo_concepto));
        		
		
            
        return $conditions;
}
    

public function add() 
    {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $this->loadModel("Impuesto");
            $this->loadModel("DatoEmpresa");
            $this->loadModel("PersonaImpuestoAlicuota");
            $id_add = '';
			$codigo_add = '';
            $mensaje = '';
            
            
            $this->request->data[$this->model]["fecha_creacion"] = date("Y-m-d H:i:s");
            
            try{
                
                $codigo_return = 1;
                if( strlen($this->request->data[$this->model]["codigo"]) == 0 ){
                    $codigo = $this->{$this->model}->getCodigo($this->id_tipo_persona);
                    $codigo_return = $codigo;
                    
                }else{
                    $codigo =  $this->request->data[$this->model]["codigo"];
                }
                
                if($codigo_return !=0){    
                    
                    $existe_codigo = $this->existe_codigo($codigo,'',1);
                
                    if( $existe_codigo == 0){
                        
                        
                            $this->request->data[$this->model]["codigo"] = $codigo;
                        
                       
                            if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
                                
                                   
                                        $id_add = $this->Persona->id;
								        $codigo_add = $codigo;
								        
                                        if($this->request->data[$this->model]["id_tipo_persona"] == EnumTipoPersona::Cliente){
                                        //si en dato_empresa_impuesto hay un impuesto aplicable a todos los clientes entonces se lo agrego        
                                            $impuestos = $this->DatoEmpresa->getImpuestosGlobalCliente();    
                                        }elseif($this->request->data[$this->model]["id_tipo_persona"] == EnumTipoPersona::Proveedor){
                                         //si en dato_empresa_impuesto hay un impuesto aplicable a todos los proveedores entonces se lo agrego           
                                            $impuestos = $this->DatoEmpresa->getImpuestosGlobalProveedor();      
                                        }
                                        
                                        if(isset($impuestos) && count($impuestos)>0)
                                            $this->PersonaImpuestoAlicuota->AgregarImpuestoPersona($id_add,$impuestos);
                                        
                                        $mensaje = "La Persona ha sido creada exitosamente";
                                        $tipo = EnumError::SUCCESS;
                                       
                                    }else{
                                        
                                        $errores = $this->{$this->model}->validationErrors;
                                        $errores_string = "";
                                        foreach ($errores as $error){
                                            $errores_string.= "&bull; ".$error[0]."\n";
                                            
                                        }
                                        $mensaje = $errores_string;
                                        $tipo = EnumError::ERROR; 
                                        
                                        
                                        
                                    }
                            
                        
                    }else{
                     
                     $mensaje = "Ha ocurrido un error,el c&oacute;digo asignado a la persona ya esta asignado";
                     $tipo = EnumError::ERROR;  
                     
                    } 
             
             }else{
                        
                        $mensaje = "Ha ocurrido un error,se debe definir el contador para la persona";
                        $tipo = EnumError::ERROR;   
             }  
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,la Persona no ha podido ser creada.".$mensaje."</br>".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" => $id_add,
			"codigo_add" => $codigo_add,
             );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add","codigo_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
   
   
 
   public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            
            
            
            //TODO: si una persona tiene un comprobante abierto no la podes dar de baja
            
             if( strlen($this->request->data[$this->model]["codigo"]) == 0 )
                    $codigo = $this->{$this->model}->getCodigo($this->id_tipo_persona);
                else
                    $codigo =  $this->request->data[$this->model]["codigo"];
                
                $existe_codigo = $this->existe_codigo($codigo,$id,1);
                
                if( $existe_codigo == 0 ){
            
            try{
            
            
            
             
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "Ha sido modificado exitosamente";
                     $status =   EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
               }
               
               
                    
                    
                    
            }catch(Exception $e){
                
            	$status = EnumError::ERROR;
            	$mensaje = $e->getMessage();
                
            }
            
            }else{
                
                $mensaje = "Ha ocurrido un error,el c&oacute;digo asignado a la persona ya esta asignado";
                $status = EnumError::ERROR;   
            }
            
            
            if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
           }else{
                $this->Session->setFlash($mensaje, $status);
                $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                
           } 
            
            
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            $this->{$this->model}->contain('Provincia','Pais');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
  
    
    public function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->Persona->id = $id;
       try{
            if ($this->Persona->saveField('activo', "0")) {
                
                $this->Persona->saveField('fecha_borrado',date("Y-m-d H:i:s"));
          
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "Ha sido eliminada exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }

    }
    
    
    
    
   
  
   
    
    
   
    
    
    

    
   
    
    
    
   
    
}
?>