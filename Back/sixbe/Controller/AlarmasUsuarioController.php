<?php
    App::uses('ComprobantesController', 'Controller');

    /**
    * @secured(CONSULTA_FACTURA)
    */
    class AlarmasUsuarioController extends AppController{

        public $name = EnumController::AlarmasUsuario;
        public $model = 'AlarmaUsuario';
        public $helpers = array ('Session', 'Js');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
        public $requiere_impuestos = 1; //si el comprobante acepta impuesto, esto llama a las funciones de IVA E IMPUESTOS
        public $type_message = array();

       
        /**
        * @secured(CONSULTA_FACTURA)
        */
        public function index() 
        {
            if($this->request->is('ajax'))
                $this->layout = 'ajax';/*sin ";" throw exception ERROR 505*/

            $this->loadModel($this->model);
            //$this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

          
			
			

            $array_contain = array('Usuario','Alarma'); //sucursal

        
            //$conditions = array("Alarma.activa"=>1);
	$conditions = array();
            		
            $this->paginate = array(
         //       'fields'=>array('Comprobante.*'),
                'paginado' =>$this->paginado,
            	'contain' =>$array_contain,
                'conditions' => $conditions,
                'limit' => $this->numrecords,
            	'maxLimit'=>$this->maxLimitRows,
                'page' => $this->getPageNum(),
                'order' => $this->model.'.id desc'
            );  
            
            $this->PaginatorModificado->settings = $this->paginate;
             

            $data = $this->PaginatorModificado->paginate($this->model);
            
            
            foreach($data as $key=>&$valor){
            	
            	$count = $this->{$this->model}->query($valor["Alarma"]["sql_vista_resumen"]);//esta query siempre debe devolver una cantidad
            	if($count[0][0]["cant"]>0){
            		
            		$valor["AlarmaUsuario"]["d_alarma"] = $valor["Alarma"]["d_alarma"];
            		$valor["AlarmaUsuario"]["d_mensaje_alarma"] = $valor["Alarma"]["d_mensaje_alarma"];
            		$valor["AlarmaUsuario"]["color"] = $valor["Alarma"]["color"];
            		$valor["AlarmaUsuario"]["accion"] = '<span class="add-on"><i class="icon-search"></i></span>';
            	}else{
            		
            		unset($data[$key]);
            		
            	}
            }
            
            
            
            
                
            
            
            $this->data = $data;

                
            
            
            $seteo_form_builder = array("showActionColumn" =>true,"showNewButton" =>true,"showFooter"=>true,"showHeaderListado"=>true);
            
            $this->form_title = "Alarmas";
            $this->preparaHtmLFormBuilder($this,$seteo_form_builder,"default",true);
            $page_count = 0;
                
                $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "list",
                    "content" => $data,
                    "page_count" =>$page_count
                );
                $this->set($output);
                $this->set("_serialize", array("status", "message","page_count", "content"));

            
            //fin vista json

        }
		
		
		        /**
         * @secured(CONSULTA_FACTURA)
         */
        public function existe_comprobante()
            {
            parent::existe_comprobante();
            }

       
        /**
    * @secured(CONSULTA_FACTURA)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->Comprobante->id = $id;
            $this->Comprobante->contain();
            $this->request->data = $this->Comprobante->read();
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Factura');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Facturas');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Facturas');
        else
            $formBuilder->setTituloForm('Consulta de Facturas');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('cuit', 'Cuit', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('calle', 'Calle', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  
        $formBuilder->addFormInput('tel', 'Tel&eacute;fono', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('ciudad', 'Ciudad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descuento', 'Descuento', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descripcion', 'Descripcion', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('localidad', 'Localidad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
         
        
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'ClienteAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('ClienteRazonSocial', 'Debe ingresar una Raz&oacute;n Social');
                validator.validateRequired('ClienteCuit', 'Debe ingresar un CUIT');
                validator.validateNumeric('ClienteDescuento', 'El Descuento ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_cuit',
                        'data': 'cuit=' + $('#ClienteCuit').val()+'&id='+$('#ClienteId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('ClienteCuit','El Cuit que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

        /**
        * @secured(ADD_FACTURA)
        */
        public function add(){

            parent::add();    

        }


        /**
        * @secured(MODIFICACION_FACTURA)
        */
        public function edit($id){

           
            parent::edit($id);
            return;    

        }


        /**
        * @secured(BAJA_FACTURA)
        */
        public function delete($id){

            parent::delete($id);    

        }

        /**
        * @secured(BAJA_FACTURA)
        */
        public function deleteItem($id,$externo=1){

            parent::deleteItem($id,$externo);    

        }


        /**
        * @secured(CONSULTA_FACTURA)
        */
        public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }


        /**
        * @secured(REPORTE_FACTURA)
        */
        public function pdfExport($id,$vista=''){


            $this->loadModel("Comprobante");
            $this->loadModel("ComprobantePuntoVentaNumero");
            
            
            
           
            
          try{  
            
            $factura = $this->Comprobante->find('first', array(
                    'conditions' => array('Comprobante.id' => $id),
                    'contain' =>array('TipoComprobante')
                ));

            $comprobantes_relacionados = $this->getComprobantesRelacionados($id);


            if($factura && $this->ComprobantePuntoVentaNumero->permiteModificarNroComprobante($factura["Comprobante"]["id_tipo_comprobante"],$factura["Comprobante"]["id_punto_venta"]) == 0 ){



                $remitos_relacionados = array();
                $pedidos_internos_relacionados = array();


                foreach($comprobantes_relacionados as $comprobante){


                    if( isset($comprobante["Comprobante"]) && $comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::Remito)
                        array_push($remitos_relacionados,$this->Comprobante->GetNumberComprobante($comprobante["PuntoVenta"]["numero"],$comprobante["Comprobante"]["nro_comprobante"]));

                    if( isset($comprobante["Comprobante"]) && $comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::PedidoInterno)
                        array_push($pedidos_internos_relacionados,$this->Comprobante->GetNumberComprobante($comprobante["PuntoVenta"]["numero"],$comprobante["Comprobante"]["nro_comprobante"]));


                }

                
                
                $this->set('remitos_relacionados',$remitos_relacionados);  
                $this->set('pedidos_internos_relacionados',$pedidos_internos_relacionados);  
                $letra = $factura["TipoComprobante"]["letra"];
                $vista = "factura".$letra;  


                parent::pdfExport($id,$vista);//envio la vista con la letra que corresponde A o B
            }else
            {    
            	
            	//$this->RequestHandler->ext = 'json';
            
            	$salida= array(
            			"status" =>EnumError::ERROR,
            			"message" => "Error: No puede generar el PDF de la Factura ya que es un comprobante preimpreso.",
            			"content" => ""
            	);
            	
            	
            	
           
            
            	$this->set('salida',$salida);
            
            	$this->layout = 'json';
            	$this->autoRender = false;
            	$this->render(false);
            	
            	
            	
            	
            	

            }
            
          }catch (Exception $e){
          	
          	$salida= array(
          			"status" =>EnumError::ERROR,
          			"message" => "Error: No puede generar el PDF".$e->getMessage(),
          			"content" => ""
          	);
          	
          	
          	
          	
          	
          	$this->set('salida',$salida);
          	
          	$this->layout = 'json';
          	$this->autoRender = false;
          	$this->render(false);
          	
          	
          }


        }


        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER


            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);
        }


       



        /**
        * @secured(CONSULTA_FACTURA)
        */
        public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
        {
            parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
        }
        
        public function CalcularImpuestos($id_comprobante,$id_tipo_impuesto='',$error=0,$message=''){
            
            
           $id_tipo_impuesto = EnumTipoImpuesto::PERCEPCIONES; 
          return parent::CalcularImpuestos($id_comprobante,$id_tipo_impuesto);
          
        }
        
        
        
        /**
        * @secured(MODIFICACION_FACTURA)
        */
        public function Anular($id_comprobante){
            
          $this->loadModel("Comprobante");
          $this->loadModel("Asiento");
          $this->loadModel("Modulo");
          
          $output_asiento_revertir = array("id_asiento"=>0);
          $factura = $this->Comprobante->find('first', array(
                    'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                    'contain' => array('ComprobanteValor','Asiento','ChequeEntrada','ComprobanteItem')
						));
                
		  $ds = $this->Comprobante->getdatasource(); 
		  $output_asiento_revertir = array("id_asiento"=>0);

            if(
                $this->Comprobante->getEstadoGrabado($id_comprobante)!= EnumEstadoComprobante::Anulado &&
                count($this->getRecibosRelacionados($id_comprobante))==0    &&
                $factura &&
                !$factura["Comprobante"]["cae"] >0
            ){ //si la factura cumple esto entonces puedo anularla
              
            //TODO: si tiene stock lo revierto
             try{
                 $ds->begin();
                //le clavo el estado ANULADO
                $this->Comprobante->updateAll(
											  array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,
											  'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
											  array('Comprobante.id' => $id_comprobante) );  
                                                            
             
                if($factura["Comprobante"]["comprobante_genera_asiento"] == 1)
                            $output_asiento_revertir = $this->Asiento->revertir($factura["Asiento"]["id"]); 
                              /*Se debe revertir el asiento del comprobante*/                                
                            
                            
                            
                            
                  $habilitado  = $this->Modulo->estaHabilitado(EnumModulo::STOCK);
                            
                  
                  if($habilitado == 1){ //TODO: Si tiene mov stock anular
                  	
                  	$id_tipo_movimiento_defecto = $this->Comprobante->getTipoMovimientoDefecto($id_comprobante);
                  	
                  	$id_movimiento = $this->Comprobante->HizoMovimientoStock($id_comprobante,$id_tipo_movimiento_defecto);
                  	
                  	
                  	if( $id_movimiento > 0 ){//si hizo movimiento
                  		$this->loadModel("Movimiento");
                  		$this->Movimiento->Anular($id_movimiento);
                  	}
                  }
                  
                          
                            
                    $ds->commit(); 
                    
                    
                    $this->abrirComprobantesAsociados($factura);//abro los Pedidos si es que los cerre
                    
                    $tipo = EnumError::SUCCESS; 
                    $mensaje = "El Comprobante se anulo correctamente ";       
                    
                    $factura = $this->Comprobante->find('first', array(
                    		'conditions' => array('Comprobante.id'=>$id_comprobante,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                    		'contain' => false
                    ));
                    
                    $this->Auditar($id_comprobante,$factura);
                    
             }catch(Exception $e){ 
                
                $ds->rollback();
                $tipo = EnumError::ERROR;
                $mensaje = "No es posible anular el Comprobante ERROR:".$e->getMessage();
                
              }     
            }else{
                $tipo = EnumError::ERROR;
                $mensaje = "No es posible anular el Comprobante ya que o ya ha sido ANULADO o esta presente en algun RECIBO o ya posee CAE"."\n";
                
                $recibos_relacionados = $this->getRecibosRelacionados($id_comprobante);
                if(count($recibos_relacionados)>0){
                   foreach($recibos_relacionados as $recibo){
                    $mensaje.= "&bull; Recibo Nro: ".$recibo["Comprobante"]["nro_comprobante"]."\n";
                   } 
                }
            }
            $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                "id_asiento"=>$output_asiento_revertir["id_asiento"]
                );      
            echo json_encode($output);
            die();
        }
        
        
    public function ProcesarLote($array_lote_comprobantes){
        
        
        
        
        
        
    }   
    
    
    private function setRowStyle(&$row){
            
    
    $row["forecolor"] = EnumRowStyleOk::forecolor;        
    $row["backcolor"] = EnumRowStyleOk::backcolor;        
            
            
            
    }    

	
    
    
    /**
     * @secured(BTN_EXCEL_FACTURAS)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Facturas de Venta");
    	
    	
    	
    }
    
    public function generaAsiento($id_comprobante,&$message){
    	
    	$this->loadModel("ComprobantePuntoVentaNumero");
    	
    	$comprobante_punto_venta_numero = $this->ComprobantePuntoVentaNumero->find('first', array(
    			'conditions' => array(
    					'ComprobantePuntoVentaNumero.id_punto_venta' => $this->request->data["Comprobante"]["id_punto_venta"],
    					'ComprobantePuntoVentaNumero.id_tipo_comprobante' => $this->request->data["Comprobante"]["id_tipo_comprobante"]
    			) ,
    			'contain' => false
    	));
    	
    	
    	if($comprobante_punto_venta_numero && $comprobante_punto_venta_numero["ComprobantePuntoVentaNumero"]["id_tipo_comprobante_punto_venta_numero"] == EnumTipoComprobantePuntoVentaNumero::PropioOfflinePreimpreso ||
    			$comprobante_punto_venta_numero["ComprobantePuntoVentaNumero"]["id_tipo_comprobante_punto_venta_numero"] == EnumTipoComprobantePuntoVentaNumero::PropioElectronicoOfflineExportacion
    			){
    				parent::generaAsiento($id_comprobante, $message);
    				return;
    	}//solo genero asiento en estos casos, sino se genera cuando se obtiene el CAE
    	
    }
    
    
    public function getMovimientoFondosFormaPago(){
    	
    	$this->loadModel("Comprobante");
    	
    	
    	
    	
    	$fecha_inicio = strtolower($this->getFromRequestOrSession('Comprobante.fecha_contable'));
    	$fecha_fin = strtolower($this->getFromRequestOrSession('Comprobante.fecha_contable_hasta'));
    	$conciliado = strtolower($this->getFromRequestOrSession('Cheque.conciliado'));
    	$id_cuenta_bancaria = strtolower($this->getFromRequestOrSession('ComprobanteValor.id_cuenta_bancaria'));
    	
    	
    	
    	$conditions = array();
    	array_push($conditions, array('NOT'=>array('Comprobante.id_estado_comprobante' =>array(EnumEstadoComprobante::Anulado,EnumEstadoComprobante::Cancelado)) ));
    	array_push($conditions, array('Comprobante.definitivo'=> 1 ));
    	array_push($conditions, array('Comprobante.id_tipo_comprobante'=> array(EnumTipoComprobante::FacturaCreditoA,EnumTipoComprobante::FacturaCreditoB,EnumTipoComprobante::FacturaCreditoC,EnumTipoComprobante::FacturaA,EnumTipoComprobante::FacturaB,EnumTipoComprobante::FacturaC) ));
    	
    	
    	
    	if($fecha_inicio!=""){
    		array_push($conditions, array('Comprobante.fecha_contable >='=>$fecha_inicio));
    		array_push($this->array_filter_names,array("Fecha Contable Dsd:"=>$this->Comprobante->formatDate($fecha_inicio)));
    	}
    	
    	
    	if($fecha_fin!=""){
    		array_push($conditions, array('Comprobante.fecha_contable <='=>$fecha_fin));
    		array_push($this->array_filter_names,array("Fecha Contable Hta:"=>$this->Comprobante->formatDate($fecha_fin)));
    	}
    	
    	
    	
    	$comprobante_ventas = $this->Comprobante->find('all', array(
    			
    			
    			'conditions' => array($conditions),
    			'fields' => array('SUM(ROUND(Comprobante.total_comprobante,2)) as total','FormaPago.id','CONCAT(FormaPago.codigo,"-",IFNULL(FormaPago.interes,"0"),"%") as d_forma_pago'),
    			
    			'group' => array('Comprobante.id_forma_pago'),
    			
    			'contain' =>array('FormaPago','TipoComprobante')
    	));
    	
    	
    	
    	
    	
    	$array_final = $comprobante_ventas;
    	
    	
    	$total_haber = 0;
    	$total_haber_conciliado = 0;
    	$total_debe = 0;
    	$total_debe_conciliado = 0;
    	
    	$array_data = array();
    	
    	foreach($array_final as &$valor){
    		
    		
    		
    		
    		$valor["Comprobante"]["total_comprobante"] = $valor[0]["total"];
    		$valor["Comprobante"]["d_forma_pago"] = $valor[0]["d_forma_pago"];
    		$total_debe = $total_debe + $valor["Comprobante"]["total_comprobante"];
    		$valor["Comprobante"]["descripcion"] = "Ingreso";
    		
    		
    		
    		
    		
    		unset($valor["FormaPago"]);
    		unset($valor["TipoComprobante"]);
    		unset($valor[0]);
    		
    		array_push($array_data, $valor);
    		
    	}
    	
    	
    	
    	
    	
    	$resultado["ResultadosComprobante"]["total_debe"] =(string) $total_debe;//total de los comprobantes
    	$resultado["ResultadosComprobante"]["total_haber"] =(string) 0;//total de los comprobantes
    	$resultado["ResultadosComprobante"]["saldo"] =(string) ($total_debe + 0);//total de los comprobantes
    	
    	
    	$status = EnumError::SUCCESS;
    	$this->data = $array_data;
    	
    	$output = array(
    			"status" =>EnumError::SUCCESS,
    			"message" => "list",
    			"content" => $array_data,
    			"resultset_content"=>$resultado,
    			"page_count" =>0
    	);
    	
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message","page_count", "content","resultset_content"));
    	
    	
    	
    	
    	
    	
    	
    	
    	
    }
    
    
    public function getDetalleAlarma($id_alarma){
    	
    	
    	$this->loadModel(EnumModel::Alarma);
    	
    	$alarma = $this->{EnumModel::Alarma}->find('first', array(
    			'conditions' => array("Alarma.id"=>$id_alarma),
    			'contain' =>false
    	));
    	
    	
    	$sql_detalle = $this->{EnumModel::Alarma}->query($alarma[EnumModel::Alarma]["sql_vista_detalle"]);
    	
    	
    	App::import('Lib', 'FormBuilderNew');
    	$formBuilder = new FormBuilderNew();
    	
    	
    	$formBuilder->setDataListado($this, '', '', $this->model, $this->name, $sql_detalle);
    	
    	/*Obtengo los nombres de las columnas con el primer registro*/
    	if(count($sql_detalle)>0){
    		
    		$cantidad_columnas = count($sql_detalle[0]);
    		
    		foreach($sql_detalle[0] as $key=>$columna){
    			
    			
    			
    			foreach($columna as $key2=>$valor){
    				$formBuilder->addHeader(Inflector::humanize($key2), '', "");
    				$formBuilder->addField($key, $key2);
    			}
    			
    			

    			
    		}
    		
    	}
   
    	/*
    	$seteo_form_builder = array("showActionColumn" =>false,"showNewButton" =>false,"showNewButton"=>false,"showActionColumn"=>false,"showFooter"=>false,"showHeaderListado"=>false);
    	
    	
    	
    	App::import('Lib', 'FormBuilder');
    	$formBuilder = new FormBuilder();
    	
    	
    	
    	$formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
    	
    	
    	
    	//Headers
    	$formBuilder->addHeader('Nro', 'Comprobante.nro_comprobante', "10%");
    	*/
    	

    	
    	
    	
    	$seteo_form_builder = array("showActionColumn" =>false,"showNewButton" =>false,"showNewButton"=>false,"showActionColumn"=>false,"showFooter"=>false,"showHeaderListado"=>true);
    	
    	
    	
    	foreach($seteo_form_builder as $key=>$propiedad_form_builder){
    		
    		
    		
    		
    		
    		$formBuilder->{$key}($propiedad_form_builder);
    		
    		
    	}
    	
    	
    	
    
    	
    	//$this->data = $sql_detalle;
    	$this->layout = "desktop_panel";
    

    	$this->set('abm',$formBuilder);
    	//$this->render('/FormBuilder/index');
    	
    	
    	
    	
    }
    
    /*
    public function beforeFilter(){
    	
    	App::uses('ConnectionManager', 'Model');
    	
    	$this->Auth->allow('index');
    	
    	parent::beforeFilter();
    }
    */
    
    
   


    }
?>