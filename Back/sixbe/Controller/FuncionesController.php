<?php

/**
* @secured(NO_VISIBLE)
*/
class FuncionesController extends AppController {
    public $name = 'Funciones';
    public $model = 'Funcion';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
  
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        $c_funcion = "";
        $d_funcion = "";
        $conditions = array(); 
        if ($this->request->is('post')) {

            if($this->request->data['Funcion']['c_funcion']!=""){
                $c_funcion = $this->request->data['Funcion']['c_funcion'];
                array_push($conditions, array('Funcion.c_funcion LIKE' => '%' . $c_funcion  . '%')); 
            }
                 
                
            if($this->request->data['Funcion']['d_funcion']!=""){
                $d_funcion = $this->request->data['Funcion']['d_funcion'];
                array_push($conditions, array('Funcion.d_funcion LIKE' => '%' . $d_funcion  . '%'));
            }
             
            
        }
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => 10,
            'page' => $this->getPageNum()
        );
      
      
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
        $formBuilder->setDataListado($this, 'Listado de Funciones', 'Datos de las Funciones', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));

        //Filters
        $formBuilder->addFilterBeginRow();
        $formBuilder->addFilterInput('c_funcion', 'Codigo', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $c_funcion));
        $formBuilder->addFilterInput('d_funcion', 'Detalle', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $d_funcion));
        $formBuilder->addFilterEndRow();
        
        //Headers
        $formBuilder->addHeader('Id', 'funcion.id', 10);
        $formBuilder->addHeader('Codigo', 'funcion.c_funcion', "10%");
        $formBuilder->addHeader('Detalle', 'funcion.d_funcion', "80%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'c_funcion');
        $formBuilder->addField($this->model, 'd_funcion');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    }
    
    
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->Funcion->id = $id;
            $this->request->data = $this->Funcion->read();
        }
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('Funciones');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de Funci&oacute;n');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de Funci&oacute;n');
        else
            $formBuilder->setTituloForm('Consulta de Funci&oacute;n');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal multicolumn well span6'));

        //Fields
        $formBuilder->addFormInput('c_funcion', 'C&oacute;digo', array('class'=>'control-group span5'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('d_funcion', 'Detalle', array('class'=>'control-group span5'), array('class' => '', 'label' => false)); 
        
        /*Ejemplo con dos columnas - NO BORRAR - Poner al form span9*/
        /*  
        $formBuilder->addFormBeginRow();
        $formBuilder->addFormInput('c_funcion', 'C&oacute;digo', array('class'=>'control-group span5'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('d_funcion', 'Detalle', array('class'=>'control-group span5'), array('class' => '', 'label' => false)); 
        $formBuilder->addFormEndRow();
        $formBuilder->addFormBeginRow();
        $formBuilder->addFormInput('c_funcion', 'C&oacute;digo', array('class'=>'control-group span5'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('d_funcion', 'Detalle', array('class'=>'control-group span5'), array('class' => '', 'label' => false)); 
        $formBuilder->addFormEndRow();
        */
        
        
         $script = "
            function validateForm(){
           
            Validator.clearValidationMsgs('validationMsg_');

            var form = 'FuncionAbmForm';

            var validator = new Validator(form);

            validator.validateRequired('FuncionCFuncion', 'Debe ingresar un c?digo');
            validator.validateRequired('FuncionDFuncion', 'Debe ingresar un detalle');
            
            if(!validator.isAllValid()){
            validator.showValidations('', 'validationMsg_');
            return false;   
            }

            return true;

            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(READONLY_PROTECTED)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            if ($this->Funcion->save($this->request->data)){
                $this->Session->setFlash('La funci&oacute;n ha sido creada exitosamente.', 'success');
                $this->redirect(array('action' => 'index'));
            }
        } 
        
        $this->redirect(array('action' => 'abm', 'A'));                   
    }
  
    function edit($id) {
        if (!$this->request->is('get')){
            $this->loadModel($this->model);
            $this->Funcion->id = $id;
            if ($this->Funcion->save($this->request->data)){
                $this->Session->setFlash('La funci&oacute;n ha sido modificada exitosamente.', 'success');
                $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            }
        } 
        
        
        $this->redirect(array('action' => 'abm', 'M', $id));
    }
  
    /**
    * @secured(READONLY_PROTECTED)
    */
    function delete($id) {
        $this->loadModel($this->model);

        if ($this->Funcion->delete($id)) 
            $this->Session->setFlash('La funci&oacute;n ha sido eliminada exitosamente.', 'success');
        else
            $this->Session->setFlash('Ha ocurrido un error, la funci&oacute;n no ha podido eliminarse.', 'error');

        $this->redirect(array('action' => 'index'));
    }
}
?>