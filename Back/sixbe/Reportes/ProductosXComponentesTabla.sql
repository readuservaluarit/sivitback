/*Este reporte muestra la tabla componente_producto de forma cruda*/
SELECT cm.`codigo` AS componente, pr.`codigo` AS valvula FROM componente_producto cp
LEFT JOIN componente cm ON cm.`id` = cp.`id_componente`
LEFT JOIN producto pr ON pr.`id` = cp.`id_producto`
ORDER BY componente

