<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class RRHHConcepto extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'rrhh_concepto';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	
    
    
    
    public $hasMany = array(
    		'RRHHConceptoArea' => array(
    				'className' => 'RRHHConceptoArea',
    				'foreignKey' => 'id_rrhh_concepto',
    				'dependent' => false,
    				'fields' => '',
    				'order' => '',
    				'limit' => '',
    				'offset' => '',
    				'exclusive' => '',
    				'finderQuery' => '',
    				'counterQuery' => ''
    		),
    		'RRHHConceptoCategoria' => array(
    				'className' => 'RRHHConceptoCategoria',
    				'foreignKey' => 'id_rrhh_concepto',
    				'dependent' => false,
    				'fields' => '',
    				'order' => '',
    				'limit' => '',
    				'offset' => '',
    				'exclusive' => '',
    				'finderQuery' => '',
    				'counterQuery' => ''
    		),
    		'RRHHConceptoObraSocial' => array(
    				'className' => 'RRHHConceptoObraSocial',
    				'foreignKey' => 'id_rrhh_concepto',
    				'dependent' => false,
    				'fields' => '',
    				'order' => '',
    				'limit' => '',
    				'offset' => '',
    				'exclusive' => '',
    				'finderQuery' => '',
    				'counterQuery' => ''
    		),
    		'RRHHConceptoPersonaEstadoCivil' => array(
    				'className' => 'RRHHConceptoPersonaEstadoCivil',
    				'foreignKey' => 'id_rrhh_concepto',
    				'dependent' => false,
    				'fields' => '',
    				'order' => '',
    				'limit' => '',
    				'offset' => '',
    				'exclusive' => '',
    				'finderQuery' => '',
    				'counterQuery' => ''
    		),
    		'RRHHConceptoPersonaEstadoCivil' => array(
    				'className' => 'RRHHConceptoPersonaEstadoCivil',
    				'foreignKey' => 'id_rrhh_concepto',
    				'dependent' => false,
    				'fields' => '',
    				'order' => '',
    				'limit' => '',
    				'offset' => '',
    				'exclusive' => '',
    				'finderQuery' => '',
    				'counterQuery' => ''
    		),
    		'RRHHConceptoPersonaSexo' => array(
    				'className' => 'RRHHConceptoPersonaSexo',
    				'foreignKey' => 'id_rrhh_concepto',
    				'dependent' => false,
    				'fields' => '',
    				'order' => '',
    				'limit' => '',
    				'offset' => '',
    				'exclusive' => '',
    				'finderQuery' => '',
    				'counterQuery' => ''
    		),
    		'RRHHConceptoSindicato' => array(
    				'className' => 'RRHHConceptoSindicato',
    				'foreignKey' => 'id_rrhh_concepto',
    				'dependent' => false,
    				'fields' => '',
    				'order' => '',
    				'limit' => '',
    				'offset' => '',
    				'exclusive' => '',
    				'finderQuery' => '',
    				'counterQuery' => ''
    		),
    		'RRHHConceptoTipoLegajo' => array(
    				'className' => 'RRHHConceptoTipoLegajo',
    				'foreignKey' => 'id_rrhh_concepto',
    				'dependent' => false,
    				'fields' => '',
    				'order' => '',
    				'limit' => '',
    				'offset' => '',
    				'exclusive' => '',
    				'finderQuery' => '',
    				'counterQuery' => ''
    		)
    		
    );
    
    
    
    public  function  getTipoCampoConcepto($enum_rrhh_tipo_campo_concepto){
    	
    	
    	switch($enum_rrhh_tipo_campo_concepto){
    		
    		case EnumRRHHTipoCampoConcepto::Cantidad:
    			
    			return "cantidad";
    			
    			break;
    			
    		case EnumRRHHTipoCampoConcepto::Importe:
    			
    			return "importe";
    			
    			break;
    		case EnumRRHHTipoCampoConcepto::ValorGenerico:
    			
    			return "valor_generico";//esto se saca de la tabla RRHHCONCEPTO
    			
    			break;
    		case EnumRRHHTipoCampoConcepto::ValorUnitario:
    			
    			return "valor_unitario";
    			
    			break;
    		
    	}
    	
    	
    }
	


}
