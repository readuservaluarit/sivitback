<?php

class ComprobanteItemsController extends AppController {
	
    public $name = 'ComprobanteItems';
    public $model = 'ComprobanteItem';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $id_tipo_comprobante = array();
    public $array_filter_names = array();
    
 protected  function RecuperoFiltros($model)
{                
		//Recuperacion de Filtros
        $id_comprobante = $this->getFromRequestOrSession($this->model.'.id_comprobante');
		$id_producto = $this->getFromRequestOrSession($this->model.'.id_producto');

        $fecha_generacion_desde = $this->getFromRequestOrSession('Comprobante.fecha_generacion_desde');
        $fecha_generacion_hasta = $this->getFromRequestOrSession('Comprobante.fecha_generacion_hasta');
        $fecha_entrega_desde = $this->getFromRequestOrSession('Comprobante.fecha_entrega_desde');
        $fecha_entrega_hasta = $this->getFromRequestOrSession('Comprobante.fecha_entrega_hasta');
        
        $fecha_vencimiento_desde = $this->getFromRequestOrSession('Comprobante.fecha_vencimiento_desde');
        $fecha_vencimiento_hasta = $this->getFromRequestOrSession('Comprobante.fecha_vencimiento_hasta');
        
        $id_tipo_comprobante = $this->getFromRequestOrSession('Comprobante.id_tipo_comprobante');
        $nro_comprobante = $this->getFromRequestOrSession('Comprobante.nro_comprobante');
        $id_persona = $this->getFromRequestOrSession('Comprobante.id_persona');
        $orden_compra_externa = $this->getFromRequestOrSession('Comprobante.orden_compra_externa');
        $id_punto_venta = $this->getFromRequestOrSession('Comprobante.id_punto_venta');
        $aprobado = $this->getFromRequestOrSession('Comprobante.aprobado');
        $razon_social = $this->getFromRequestOrSession('Comprobante.persona_razon_social');
		

		
        $d_producto = $this->getFromRequestOrSession('Producto.d_producto');
        $id_persona_categoria = $this->getFromRequestOrSession('Persona.id_persona_categoria');
        $producto_es_remitible = $this->getFromRequestOrSession('Producto.remitible');
        $id_estado_comprobante = $this->getFromRequestOrSession('Comprobante.id_estado_comprobante');
        $inspeccion = $this->getFromRequestOrSession('Comprobante.inspeccion');
        $certificados = $this->getFromRequestOrSession('Comprobante.certificados');
		
		$id_estado_excluir = $this->getFromRequestOrSession('Comprobante.id_estado_comprobante_excluir');
        $id_familia_producto = $this->getFromRequestOrSession('Producto.id_familia_producto');
        $filtro_dias = $this->getFromRequestOrSession($this->model.'.dias');
        $d_punto_venta = $this->getFromRequestOrSession('Comprobante.d_punto_venta');
        $referencia_comprobante_externo= $this->getFromRequestOrSession('Comprobante.referencia_comprobante_externo');
        
        $d_producto_codigo = $this->getFromRequestOrSession('Producto.codigo');
        $id_sistema = $this->getFromRequestOrSession('Comprobante.id_sistema');
        $id_producto_tipo= $this->getFromRequestOrSession('Producto.id_producto_tipo'); //MP: analizar una de estas dos lineas. UNA TIENE Q DESAPARECER!
		//$id_producto_tipo = $this->getFromRequestOrSession($this->model.'.id_producto_tipo'); //MP: Atencion se llama de distintas formas
		$id_usuario = $this->getFromRequestOrSession('Comprobante.id_usuario'); //MP: Atencion se llama de distintas formas
        
        $conditions = array();
        

        if($this->getFromRequestOrSession('Comprobante.id_tipo_comprobante') == '' && !is_null($this->id_tipo_comprobante))//SINO manda el filtro con los tipos de comprobante se setean en el beforeFilter de ComprobanteItem
            array_push($conditions, array('Comprobante.id_tipo_comprobante ' => $this->id_tipo_comprobante));   
        
        array_push($conditions, array($this->model.'.activo' => 1)); //solo trae las visibles

         if($fecha_generacion_desde!=""){
            array_push($conditions, array('Comprobante.fecha_generacion >=' =>  $fecha_generacion_desde  ));
            array_push($this->array_filter_names,array("Fcha Gen. Dsd.:"=>$this->{$this->model}->formatDate($fecha_generacion_desde)));
         }
            
         if($fecha_generacion_hasta!=""){
            array_push($conditions, array('Comprobante.fecha_generacion <=' =>  $fecha_generacion_hasta  ));
            array_push($this->array_filter_names,array("Fcha Gen. Hta.:"=>$this->{$this->model}->formatDate($fecha_generacion_hasta)));
         }
		
		if($producto_es_remitible!=""){
            array_push($conditions, array('ProductoTipo.remitible' => $producto_es_remitible)); 
            array_push($this->array_filter_names,array("Remitible.:"=>$producto_es_remitible));
		}
            
         if($aprobado!=""){
            array_push($conditions, array('Comprobante.aprobado ' =>  $aprobado  ));    
            array_push($this->array_filter_names,array("Aprobado.:"=>$aprobado));
         }
         
         if($inspeccion!=""){
         	array_push($conditions, array('Comprobante.inspeccion ' =>  $inspeccion));
         	array_push($this->array_filter_names,array("Inspecci&oacute;n.:"=>$inspeccion));
         }
         
         if($certificados!=""){
         	array_push($conditions, array('Comprobante.certificados ' =>  $certificados));
         	array_push($this->array_filter_names,array("Certificados.:"=>$certificados));
         }
         
         if($fecha_entrega_desde!=""){
            array_push($conditions, array('ComprobanteItem.fecha_entrega >=' =>  $fecha_entrega_desde  ));
            array_push($this->array_filter_names,array("Fcha Entrega Dsd.:"=>$this->{$this->model}->formatDate($fecha_entrega_desde)));
         }
            
         if($fecha_entrega_hasta!=""){
            array_push($conditions, array('ComprobanteItem.fecha_entrega <=' =>  $fecha_entrega_hasta  ));     
            array_push($this->array_filter_names,array("Fcha Entrega Hta.:"=>$this->{$this->model}->formatDate($fecha_entrega_hasta)));
         }
         
         if($fecha_vencimiento_desde!=""){
         	array_push($conditions, array('ComprobanteItem.fecha_vencimiento >=' =>  $fecha_vencimiento_desde  ));
         	array_push($this->array_filter_names,array("Fcha Entrega Dsd.:"=>$this->{$this->model}->formatDate($fecha_vencimiento_desde)));
         }

         if($fecha_vencimiento_hasta!=""){
         	array_push($conditions, array('ComprobanteItem.fecha_vencimiento <=' =>  $fecha_vencimiento_hasta  ));
         	array_push($this->array_filter_names,array("Fcha Entrega Hta.:"=>$this->{$this->model}->formatDate($fecha_vencimiento_hasta)));
         }
		 
         if($id_tipo_comprobante!=""){
             $tipos_comprobante = explode(',', $id_tipo_comprobante);
            array_push($conditions, array('Comprobante.id_tipo_comprobante' => $tipos_comprobante ));  
         }   
         
         if($id_comprobante!="")
            array_push($conditions, array($model.'.id_comprobante' => $id_comprobante ));
            
            
          if($id_producto!=""){
            array_push($conditions, array($this->model.'.id_producto' => $id_producto ));     
          }
		  
		  if($id_producto_tipo!=""){
		  	$id_tipo_producto_array = explode(',', $id_producto_tipo);
		  	array_push($conditions, array('Producto.id_producto_tipo' => $id_tipo_producto_array));     
          }
		  
            
         if($nro_comprobante!=""){
             $nros_comprobante = explode(',', $nro_comprobante);
            array_push($conditions, array('Comprobante.nro_comprobante ' => $nros_comprobante )); 
            array_push($this->array_filter_names,array("Nro Comprobante/s:"=>$nros_comprobante));
          }  
         if($orden_compra_externa!=""){
            array_push($conditions, array('Comprobante.orden_compra_externa LIKE ' => '%'.$orden_compra_externa.'%' ));    
            array_push($this->array_filter_names,array("OC Externa:"=>$orden_compra_externa));
         }
            
            
         if($id_persona!=""){
         	$this->loadModel("Persona");
            array_push($conditions, array('Comprobante.id_persona ' => $id_persona ));
            array_push($this->array_filter_names,array("Rzon. Social.:"=>$this->Persona->getRazonSocial($id_persona)));
         }
            
         if($razon_social!="" && $id_persona == "" ){
            //array_push($conditions, array('LOWER(Persona.razon_social) LIKE' => '%' . $razon_social  . '%')); 
            array_push($conditions, array('LOWER(Comprobante.razon_social) LIKE' => '%' . $razon_social  . '%')); 
         }
		 
		 
         
         if($d_producto!="" && $id_producto ==""){
            array_push($conditions, array('LOWER(Producto.d_producto) LIKE' => '%' . $d_producto  . '%')); 
            array_push($this->array_filter_names,array("Dec. Artic.:"=>$d_producto));
         }
         
         
          if($d_producto_codigo!=""){
            	array_push($conditions, array('LOWER(Producto.codigo) LIKE' => '%' . $d_producto_codigo  . '%')); 
            	array_push($this->array_filter_names,array("Cod. Artic.:"=>$d_producto_codigo));
            	
          }
          
         if($id_estado_comprobante!="")
		 {
		 	  $this->loadModel("EstadoComprobante");
			  $id_estado_comprobante = explode(',', $id_estado_comprobante);
           	  array_push($conditions, array('Comprobante.id_estado_comprobante' => $id_estado_comprobante)); 
           	  array_push($this->array_filter_names,array("Estado.:"=>$this->EstadoComprobante->getDescripcion($id_estado_comprobante)));
		 }
		 
		 
		 
		 
		 if($id_persona_categoria!="" && $id_persona == "")
		 {
		 	$this->loadModel("PersonaCategoria");

		 	array_push($conditions, array('Persona.id_persona_categoria' => $id_persona_categoria));
		 	array_push($this->array_filter_names,array("Estado.:"=>$this->PersonaCategoria->getDescripcion($id_persona_categoria)));
		 }
         
         
          if( $id_estado_excluir !="" ){            
            
             $estados_excluir = explode(",", $id_estado_excluir);
            array_push($conditions,array( "NOT" => (array( 'Comprobante.id_estado_comprobante' => $estados_excluir  )) )); 
        } 
        
           
         if($id_punto_venta!=""){
            array_push($conditions, array('Comprobante.id_punto_venta' => $id_punto_venta));  
         }
            
         if($id_familia_producto!=""){
            array_push($conditions, array('Producto.id_familia_producto' => $id_familia_producto));   
         }
            
         if($d_punto_venta!=""){
            	array_push($conditions, array('LOWER(Comprobante.d_punto_venta) LIKE' => '%' . $d_punto_venta  . '%'));
            	array_push($this->array_filter_names,array("Pto. Vta.:"=>$d_punto_venta));
         }
          	
        if($referencia_comprobante_externo!=""){
        	array_push($conditions, array('Comprobante.referencia_comprobante_externo' => $referencia_comprobante_externo));  
        	array_push($this->array_filter_names,array("Ref. Comp. Ext.:"=>$referencia_comprobante_externo));
        }
        
        if($id_sistema!="")
        	array_push($conditions, array('TipoComprobante.id_sistema' => $id_sistema));  
        	
       if($id_usuario!=""){
        		$this->loadModel("Usuario");
        		array_push($conditions, array('Comprobante.id_usuario' =>  $id_usuario));
        		array_push($this->array_filter_names,array("Usuario:"=>$this->Usuario->getNombreYApellido($id_usuario)));
       }
       
       
       /*Seguridad Usuario B2B*/
       if($this->Auth->user('id_persona')>0 && $this->Auth->user('id_rol') == EnumRol::SivitB2B){
       	
       	$id_usuario_b2b = $this->Auth->user('id');
       	$id_persona = $this->Auth->user('id_persona');
       	array_push($conditions, array('Comprobante.id_usuario' => $id_usuario_b2b));
       	array_push($conditions, array('Comprobante.id_persona' => $id_persona));
       	
       }
       
       
        /*    
         if($id_familia_producto!="")
            array_push($conditions, array('Producto.id_familia_producto' => $id_familia_producto));      
            */
            
            
        return $conditions;
    
    
}    

   /**
    * @secured(CONSULTA_COTIZACION)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        
        
     //Recuperacion de Filtros
	 $conditions = $this->RecuperoFiltros($this->model); 
	 array_push($conditions, array('ComprobanteItem.id_producto >'=> 0)); //solo trae las visibles
        
     $this->paginate = array(
               'paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
               'joins' => array(
            array(
                'table' => 'producto_tipo',
                'alias' => 'ProductoTipo',
                'type' => 'LEFT',
                'conditions' => array(
                    'ProductoTipo.id = Producto.id_producto_tipo'
                )),
               		
               		array(
               				'table' => 'tipo_comprobante',
               				'alias' => 'TipoComprobante',
               				'type' => 'LEFT',
               				'conditions' => array(
               						'TipoComprobante.id = Comprobante.id_tipo_comprobante'
               				)
               				
               				
               		),
               		
               		array(
               				'table' => 'persona',
               				'alias' => 'Persona',
               				'type' => 'LEFT',
               				'conditions' => array(
               						'Persona.id = Comprobante.id_persona'
               				)
               				
               				
               		)
               
               
               
               
               
               ),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        //Headers
        $formBuilder->addHeader('id', 'cotizacion.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'cotizacion.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$producto ){
            
              
                 
                 $producto["ComprobanteItem"]["d_producto"] = $producto["Producto"]["d_producto"];
                 $producto["ComprobanteItem"]["nro_comprobante"] = $producto["Comprobante"]["nro_comprobante"];
                 $producto["ComprobanteItem"]["codigo"] = $this->ComprobanteItem->getCodigoFromProducto($producto);
                
                 //$producto["ComprobanteItem"]["total"] = (string) $this->{$this->model}->getTotalItem($producto);
                 
                 unset($producto["Producto"]);
                 unset($producto["Comprobante"]);
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    //fin vista json
    }
    
    protected function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            
            try{
                if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "El Item ha sido creado exitosamente";
                    $tipo = EnumError::SUCCESS;
                   
                }else{
                    $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.";
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => ""
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    protected function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                    
                    $mensaje = "El Item Ha sido modificado correctamente.";
                    $tipo = EnumError::ERROR;
                    
                }else{
                    $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.";
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
              
                $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.".$e->getMessage();
                $tipo = EnumError::ERROR; 
                
            }
          
        } 
        if($this->RequestHandler->ext == 'json'){  
            $output = array(
                "status" => $tipo,
                "message" =>$mensaje,
                "content" => ""
            ); 
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content"));
        }else{
            $this->Session->setFlash($mensaje, $tipo);
             $this->redirect(array('controller' => 'CotizacionItems', 'action' => 'index'));
        
        } 
      
    }
    
  
    
    

   
    
    
      /**
    * @secured(CONSULTA_ITEM_COMPROBANTE)
    */
    public function getModel($vista = 'default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        
       
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
       
    }
    
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
        
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $this->set('model',$model);
       // $this->set('this',$this);
        Configure::write('debug',0);
        $this->render($vista);
    
        
    }
    
    
    
    public function FiltrarCamposOnTheFly(&$data,$producto,$key){
                
                $filtro_dias = $this->getFromRequestOrSession('ComprobanteItem.dias');
                  
                 if($filtro_dias!= '' && isset($producto["ComprobanteItem"]["dias_atraso_item"])){ //Si mando el filtro de dias chequeo y si no estoy en la vista agrupada
                 	
                 	
                     
                     if( $filtro_dias > 0){ //si hay pedidos atrasados los devuelvo
                         
                         
                         if($producto["ComprobanteItem"]["dias_atraso_item"] <= 0  || $producto["ComprobanteItem"]["dias_atraso_item"]< $filtro_dias )
                            unset($data[$key]);
                         
                     }else{ //devuelvo los items de los PI que le faltan XX dias por vencer
                         
                         if($producto["ComprobanteItem"]["dias_atraso_item"] >= 0 || $producto["ComprobanteItem"]["dias_atraso_item"]> $filtro_dias )
                            unset($data[$key]);
                     }
                     
                     
                 } 
                 
                 
                 $solo_pendiente_a_facturar = $this->getFromRequestOrSession('ComprobanteItem.solo_pendientes_facturar');
                 
                 if($solo_pendiente_a_facturar!= '' && isset($producto["ComprobanteItem"]["cantidad_pendiente_a_facturar"]) )
                 { //TRAIGO TODOS
                 	
                 
                     if( $solo_pendiente_a_facturar == 1) //si no tiene nada pendiente a facturar no traigo nada
                     { 
                     	
                     	
                     	if( $producto["ComprobanteItem"]["cantidad_pendiente_a_facturar"]<= 0 )
                            unset($data[$key]);   
                     }
                 }
                 
                   $solo_pendientes_remitir = $this->getFromRequestOrSession('ComprobanteItem.solo_pendientes_remitir');                                          
                  
                   if($solo_pendientes_remitir!= '' && isset($producto["ComprobanteItem"]["cantidad_pendiente_a_remitir"]))
                 { //TRAIGO TODOS
                   	
              
                     if( $solo_pendientes_remitir == 1) //si no tiene nada pendiente a facturar no traigo nada
                     { 
                     	if( $producto["ComprobanteItem"]["cantidad_pendiente_a_remitir"] <= 0 )
                            unset($data[$key]);   
                     }
                 }  
                 
                 $solo_pendientes_remitir_origen = $this->getFromRequestOrSession('ComprobanteItem.solo_pendientes_remitir_origen');
                 
                 if($solo_pendientes_remitir_origen!= '' && isset($producto["ComprobanteItem"]["total_pendiente_a_remitir_origen"]))
                 { //TRAIGO TODOS
                 	if( $solo_pendientes_remitir == 1) //si no tiene nada pendiente a facturar no traigo nada
                 	{
                 		if($producto["ComprobanteItem"]["total_pendiente_a_remitir_origen"] <= 0 )
                 			unset($data[$key]);
                 	}
                 }  
                 
                 
                    $solo_pendientes_irm = $this->getFromRequestOrSession('ComprobanteItem.solo_pendientes_irm');                                          
                  
                    if($solo_pendientes_irm!= '' && isset($producto["ComprobanteItem"]["cantidad_pendiente_irm"]))
                 { //TRAIGO TODOS
                    	
             
                     if( $solo_pendientes_irm == 1) //si no tiene nada pendiente a facturar no traigo nada
                     { 
                     	if($producto["ComprobanteItem"]["cantidad_pendiente_irm"] <= 0 )
                            unset($data[$key]);   
                     }
                 }  
                 
                 
                 $solo_pendientes_nota_credito = $this->getFromRequestOrSession('ComprobanteItem.solo_pendientes_nota_credito');                                          
                  
                 if($solo_pendientes_nota_credito!= '' && isset($producto["ComprobanteItem"]["cantidad_pendiente_nota_credito"]))
                 { //TRAIGO TODOS
                 	
                 	
              
                 	
                     if( $solo_pendientes_nota_credito == 1) //si no tiene nada pendiente a facturar no traigo nada
                     { 
                     	if($producto["ComprobanteItem"]["cantidad_pendiente_nota_credito"] <= 0 )
                            unset($data[$key]);   
                     }
                 }
                 
                 
                 $fecha_entrega_desde = $this->getFromRequestOrSession('Comprobante.fecha_entrega_desde');
                
                 
                 
                 if($fecha_entrega_desde!= '' && isset($producto["ComprobanteItem"]["fecha_entrega"]))
                 { //TRAIGO TODOS
                 	
                 	$ts1 = strtotime($fecha_entrega_desde);
                 	$ts2 = strtotime($producto["ComprobanteItem"]["fecha_entrega"]);
                 	
                 	if( $ts2< $ts1)
                 			unset($data[$key]);
                 	
                 }
                 
                 
                 $fecha_entrega_hasta = $this->getFromRequestOrSession('Comprobante.fecha_entrega_hasta');
                 
                 
                 
                 
                 if($fecha_entrega_hasta!= '' && isset($producto["ComprobanteItem"]["fecha_entrega"]))
                 { //TRAIGO TODOS
                 	
                 	
                 	$ts1 = strtotime($fecha_entrega_hasta);
                 	$ts2 = strtotime($producto["ComprobanteItem"]["fecha_entrega"]);
                 	
                 	if( $ts2 > $ts1)
                 		unset($data[$key]);
                 		
                 }
                 
                 
                 
                 $fecha_vencimiento_desde = $this->getFromRequestOrSession('Comprobante.fecha_vencimiento_desde');
                 
                 
                 
                 
                 
                 if($fecha_vencimiento_desde!= '' && isset($producto["ComprobanteItem"]["fecha_vencimiento"]))
                 { //TRAIGO TODOS
                 	
                 	$ts1 = strtotime($fecha_vencimiento_desde);
                 	$ts2 = strtotime($producto["ComprobanteItem"]["fecha_vencimiento"]);
                 	
                 	if( $ts2< $ts1)
                 		unset($data[$key]);
                 		
                 }
                 
                 
                 $fecha_vencimiento_hasta = $this->getFromRequestOrSession('Comprobante.fecha_vencimiento_hasta');
                 
                 if($fecha_vencimiento_hasta!= '' && isset($producto["ComprobanteItem"]["fecha_vencimiento"]))
                 { //TRAIGO TODOS
                 	
                 	
                 	$ts1 = strtotime($fecha_vencimiento_hasta);
                 	$ts2 = strtotime($producto["ComprobanteItem"]["fecha_vencimiento"]);
                 	
                 	if( $ts2 > $ts1)
                 		unset($data[$key]);
                 		
                 }
                 
                 
                 
                 $solo_pendientes_armar = $this->getFromRequestOrSession('ComprobanteItem.solo_pendientes_armar');
                 
                 if($solo_pendientes_armar!= '' && isset($producto["ComprobanteItem"]["cantidad_pendiente_a_armar"]))
                 { //TRAIGO TODOS
                 	
                 	
                 	
                 	
                 	if( $solo_pendientes_armar == 1) //si no tiene nada pendiente a facturar no traigo nada
                 	{
                 		if($producto["ComprobanteItem"]["cantidad_pendiente_a_armar"] <= 0 )
                 			unset($data[$key]);
                 	}
                 }
                 
                
        
    }
    
    
    
   
     public function beforeFilter(){
        
         
     $this->loadModel("Comprobante");
     $this->id_tipo_comprobante = $this->Comprobante->getTiposComprobanteController($this->name);
     parent::beforeFilter();
    }
    
    

    
    
    public function excelExport($vista="default",$metodo = "index",$titulo_repore= "Listado"){
    	
    	//TODO: Chequear que la vista exista
    	$this->paginado =0;
    	$filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');
    	
    	
    	
    	switch($filtro_agrupado){
    		
    		case EnumTipoFiltroComprobanteItem::individual:
    		case "":
    			$view = "noagrupada";
    			break;
    		case EnumTipoFiltroComprobanteItem::agrupadaporComprobante:
    			$view="agrupadaporcomprobante";
    			break;
    		case EnumTipoFiltroComprobanteItem::agrupadaporPersona:
    			$view="agrupadaporpersona";
    			break;
    			
    	}
    	
    	parent::ExportarExcel($view,$metodo,$titulo_repore);
    	
    	
    	
    }
    
    
    
    
   
    
   
    
    

    
    
    
   
    
}
?>