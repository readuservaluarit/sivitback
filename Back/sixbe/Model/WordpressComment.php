<?php

App::uses('WordpressAppModel', 'Wordpress.Model');

class WordpressComment extends WordpressAppModel {
    
    public $useDbConfig = 'wordpress_database';
    public $name = 'WordpressComment';
    public $primaryKey = 'comment_ID';
    public $useTable = 'six_wp_comments';

    //The Associations below have been created with all possible keys, those that are not needed can be removed
    public $belongsTo = array(
        'User' => array(
            'className' => 'WordpressUser',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Post' => array(
            'className' => 'WordpressPost',
            'foreignKey' => 'comment_post_ID',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CommentParent' => array(
            'className' => 'WordpressComment',
            'foreignKey' => 'comment_parent',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public $hasMany = array(
        'Commentmetum' => array(
            'className' => 'WordpressCommentmetum',
            'foreignKey' => 'comment_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
