<?php

/**
* @secured(CONSULTA_ORDEN_PRODUCCION)
*/
class OrdenesProduccionController extends AppController {
    public $name = 'OrdenesProduccion';
    public $model = 'OrdenProduccion';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
    /**
    * @secured(CONSULTA_ORDEN_PRODUCCION)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);

		
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $codigo = $this->getFromRequestOrSession('OrdenProduccion.id');
        $fecha = $this->getFromRequestOrSession('OrdenProduccion.fecha_emision');
        $codigo_componente = $this->getFromRequestOrSession('OrdenProduccion.codigo_componente');
        $descripcion_componente = $this->getFromRequestOrSession('OrdenProduccion.d_componente');
        $estado = $this->getFromRequestOrSession('OrdenProduccion.id_estado');
        $id_origen = $this->getFromRequestOrSession('OrdenProduccion.id_origen');
        
        
        $conditions = array(); 

        if($codigo!="")
            array_push($conditions, array('OrdenProduccion.id =' =>$codigo)); 
            
        if($fecha!="")
            array_push($conditions, array('OrdenProduccion.fecha_emision >=' =>$fecha)); 
            
        if($codigo_componente!="")
            array_push($conditions, array('Articulo.codigo LIKE' => '%'. $codigo_componente .'%')); 
    
        if($descripcion_componente !="")
            array_push($conditions, array('Articulo.d_producto LIKE' => '%'. $descripcion_componente .'%')); 
         
        if($estado !="")
            array_push($conditions, array('OrdenProduccion.id_estado =' => $estado ));
        
        if($id_origen !="")
            array_push($conditions, array('Articulo.id_origen =' =>$id_origen  ));    
          
             
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            //'fields' => array('OrdenArmado.id', 'OrdenArmado.fecha','OrdenArmado.plazo_entrega'), 
            'conditions' => $conditions,
            //'joins' => $joins,
            'contain' => array('Estado','Articulo','Movimiento'),
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
             'order' => 'OrdenProduccion.id desc'
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
        
        ////////////////////
        //Filters
        ///////////////////
        
          //CUE
        $formBuilder->addFilterInput('id', 'C&oacute;digo', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $codigo));
        $formBuilder->addFilterInput('descripcion', 'Descripci&oacute;n', array('class'=>'control-group span5'), array('type' => 'text', 'class' => '', 'label' => false, 'value' => $descripcion));
        
        $formBuilder->setDataListado($this, 'Listado de OrdenArmados', 'Datos de los OrdenArmados', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
		
        //Headers
        $formBuilder->addHeader('id', 'OrdenArmado.id', "10%");
        $formBuilder->addHeader('C&oacute;digo', 'OrdenArmado.codigo', "20%");
        $formBuilder->addHeader('Stock', 'OrdenArmado.stock', "10%");
        $formBuilder->addHeader('Stock M&iacute;nimo', 'OrdenArmado.stock_minimo', "10%");
        $formBuilder->addHeader('Descripci&oacute;n', 'OrdenArmado.descripcion', "50%");
        
        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'codigo');
        $formBuilder->addField($this->model, 'stock');
        $formBuilder->addField($this->model, 'stock_minimo');
        $formBuilder->addField($this->model, 'descripcion');
 
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
         
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
		
		$this->loadModel("ArticuloRelacion");
		
        
        foreach($data as &$valor){
            
			         // $id_componente = $op["OrdenProduccion"]["id_componente"];
			  $mpc = $this->ArticuloRelacion->find('all', array(
										 'conditions' => array('ArticuloRelacion.id_producto_padre' =>  $valor["OrdenProduccion"]["id_componente"]),
										 'contain' =>false ));

			//calcular la cantidad recibida en movimientos devolver campos cantidad_parcial cantidad_pendiente
            
            $cantidad_retirado = 0;
			$cantidad_terminado = 0;
			
            if( count($valor["Movimiento"]) > 0 ){//si tiene movimientos
                foreach ($valor["Movimiento"] as $movimiento){
				if($movimiento["id_movimiento_tipo"] == EnumTipoMovimiento::RetirodeMateriaPrimaPorOrdenProduccionViejo) 
                    $cantidad_retirado +=  $movimiento["cantidad"];
				if($movimiento["id_movimiento_tipo"] == EnumTipoMovimiento::IngresoDeComponenteParcialPorOrdenProduccionViejo) 	
					$cantidad_terminado +=  $movimiento["cantidad"];				
                }
            }
			$cant_insume = 0;
			if($mpc){ //por cada materiaPrima encontrada actualizo el stock y le resto lo que insume de materia prima ese componente
				 foreach($mpc as $materia_prima_componente){
					
					$cant_insume += $materia_prima_componente["ArticuloRelacion"]["cantidad_hijo"];
				 }
			}
			//MP: cantidad que insume de MP cada unidad de Componente
            //$cant_insume = $valor["Componente"]["ArticuloRelacion"]["cantidad"];
			
            $valor["OrdenProduccion"]["c_retirado"] =   $cantidad_retirado;  
            $valor["OrdenProduccion"]["c_terminado"] = 		$cantidad_terminado;  
			
			$valor["OrdenProduccion"]["mp_cantidad"] 	=  (string) 	$valor["OrdenProduccion"]["cantidad"]*$cant_insume;  
			$valor["OrdenProduccion"]["mp_retirado"] =   (string)	$valor["OrdenProduccion"]["c_retirado"] * $cant_insume;  
            $valor["OrdenProduccion"]["mp_terminado"] 	= 	(string)	$valor["OrdenProduccion"]["c_terminado"] * $cant_insume;  
            
            $valor["OrdenProduccion"]["codigo_componente"] =   $valor["Articulo"]["codigo"];  
            $valor["OrdenProduccion"]["descripcion_componente"] =  $valor["Articulo"]["d_producto"];
            $valor["OrdenProduccion"]["d_estado"] =   $valor["Estado"]["d_estado"];
            unset($valor["Componente"]); 
            unset($valor["Estado"]); 
        }
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
 

   

    /**
    * @secured(ADD_ORDEN_PRODUCCION)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_op = "";
         
            try{
                
                   $this->request->data["OrdenProduccion"]["fecha_emision"] = date("Y-m-d H:i:s");
                    
                
                    if ($this->OrdenProduccion->saveAll($this->request->data)){
                        $mensaje = "La Orden de Producci&oacute;n ha sido creada exitosamente";
                        $tipo = EnumError::SUCCESS;
                        $id_op = $this->OrdenProduccion->id;
                        
                       
                    }else{
                        $mensaje = "Ha ocurrido un error,la Orden de Producci&oacute;n no ha podido ser creada.";
                        $tipo = EnumError::ERROR; 
                        
                    }
                    
                
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,La Orden de Producci&oacute;n no ha podido ser creada .".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" => $id_op
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
    
    /**
    * @secured(MODIFICACION_ORDEN_PRODUCCION)
    */
    function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->OrdenProduccion->id = $id;
            
            try{ 
                
                 if($this->request->data["OrdenProduccion"]["id_estado"] == EnumEstado::Cerrada)
                    $this->request->data["OrdenProduccion"]["fecha_cierre"] = date("Y-m-d");
                
                
                if ($this->OrdenProduccion->saveAll($this->request->data,array('deep' => true))){
                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "La Orden de Producci&oacute;n sido modificada exitosamente",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('La Orden de Producci&oacute;n ha sido modificada exitosamente.', 'success');
                        $this->redirect(array('controller' => 'OrdenesProduccion', 'action' => 'index'));
                        
                    } 
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, la Orden de Producci&oacute;n no ha podido modificarse.', 'error');
                
            }
           
        }else{
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->OrdenProduccion->id = $id;
            $this->OrdenProduccion->contain();
            $this->request->data = $this->OrdenProduccion->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
            }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    

    
    
    /**
     * @secured(CONSULTA_ORDEN_PRODUCCION)
     */
    public function getModel($vista='default'){
    	
    	$model = parent::getModelCamposDefault();//esta en APPCONTROLLER
    	$model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
    	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    	
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    	
    	$this->set('model',$model);
    	$this->set('model_name',$this->model);
    	Configure::write('debug',0);
    	$this->render($vista);
    	
    }
    
    /*
   private function editforView($model){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER

        
               
        $propiedades = array("show_grid"=>"1","header_display" => "Nro. Op","minimun_width"=> "10","order" =>"1","text_colour"=>"#000000" );
        $this->setFieldView("id",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Cod.Comp","minimun_width"=> "10","order" =>"2","text_colour"=>"#000000","type" => EnumTipoDato::cadena );
        $this->setFieldView("codigo_componente",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Desc.Comp","minimun_width"=> "10","order" =>"3","text_colour"=>"#000000","type" => EnumTipoDato::cadena );
        $this->setFieldView("descripcion_componente",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Fecha Emisi&oacute;n","minimun_width"=> "10","order" =>"4","text_colour"=>"#000000" );
        $this->setFieldView("fecha_emision",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Fecha Cierre","minimun_width"=> "10","order" =>"5","text_colour"=>"#000000" );
        $this->setFieldView("fecha_cierre",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Cant.Comp.Retirado","minimun_width"=> "10","order" =>"6","text_colour"=>"#000000","type" => EnumTipoDato::decimal_flotante );
        $this->setFieldView("c_retirado",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Cant.Comp.Terminado","minimun_width"=> "10","order" =>"7","text_colour"=>"#000000","type" => EnumTipoDato::decimal_flotante );
        $this->setFieldView("c_terminado",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Cant.Mat.Prima","minimun_width"=> "10","order" =>"8","text_colour"=>"#000000","type" => EnumTipoDato::decimal_flotante );
        $this->setFieldView("mp_cantidad",$propiedades,$model);
        
        
        $propiedades = array("show_grid"=>"1","header_display" => "Cant.Mat.Prima.Retirado","minimun_width"=> "10","order" =>"9","text_colour"=>"#000000","type" => EnumTipoDato::decimal_flotante );
        $this->setFieldView("mp_retirado",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Cant.Mat.Prima.Terminado","minimun_width"=> "10","order" =>"10","text_colour"=>"#000000","type" => EnumTipoDato::decimal_flotante );
        $this->setFieldView("mp_terminado",$propiedades,$model);
        
        $propiedades = array("show_grid"=>"1","header_display" => "Estado","minimun_width"=> "10","order" =>"11","text_colour"=>"#000000","type" => EnumTipoDato::cadena );
        $this->setFieldView("d_estado",$propiedades,$model);
        
        
        
        return $model;
    }
  
    
    */
}
?>