<?php
App::uses('ComprobantesController', 'Controller');


  /*no tiene items ya que no tiene comprobantes adentro.*/


class OrdenesPagoAnticipoController extends ComprobantesController {
    
    public $name = EnumController::OrdenesPagoAnticipo;
    public $model = 'Comprobante';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $requiere_impuestos = 1; //si el comprobante acepta impuesto, esto llama a las funciones de IVA E IMPUESTOS
    public $id_sistema_comprobante = EnumSistema::COMPRAS;
    
   
    /**
    * @secured(CONSULTA_ANTICIPO_COMPRA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        $conditions = $this->RecuperoFiltros($this->model); 
            
        $array_conditions = array('ComprobanteItem','Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante','ComprobanteImpuesto'=>array('Impuesto'=>array('Sistema','ImpuestoGeografia')),'PuntoVenta','Sucursal'=>array('Provincia','Pais'),'Transportista'); //contacto es la sucursal
            
        
        
        
        //Si pasa el ID traigo diferentes models relacionados,sino lo basico
        /*
        if(in_array('Comprobante.id',$conditions))//elijo los models a traer dependiendo si es consulta o index
            $array_conditions = array('Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante','ComprobanteImpuesto'=>array('Impuesto'=>array('Sistema','ImpuestoGeografia')),'PuntoVenta');
        else
             $array_conditions = array('Persona','EstadoComprobante','Moneda','CondicionPago','TipoComprobante','ComprobanteImpuesto','PuntoVenta');
        
        */ 
                       
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>$array_conditions,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => $this->model.'.id desc'
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
    
    
        
        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'cotizacion.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'cotizacion.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
            
             //$valor[$this->model]['ComprobanteItem'] = $valor['ComprobanteItem'];
             //unset($valor['ComprobanteItem']);
             
             if(isset($valor['TipoComprobante'])){
                 $valor[$this->model]['d_tipo_comprobante'] = $valor['TipoComprobante']['d_tipo_comprobante'];
                 $valor[$this->model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
                 unset($valor['TipoComprobante']);
             }
             
            
             
             
                 
                 
             
             /*
             foreach($valor[$this->model]['ComprobanteItem'] as &$producto ){
                 
                 $producto["d_producto"] = $producto["Producto"]["d_producto"];
                 $producto["codigo_producto"] = $this->getProductoCodigo($producto);

                 
                 unset($producto["Producto"]);
             }
             */
           
             if(isset($valor['Moneda'])){
                 $valor[$this->model]['d_moneda'] = $valor['Moneda']['d_moneda'];
                 $valor[$this->model]['d_moneda_simbolo'] = $valor['Moneda']['simbolo'];
                 unset($valor['Moneda']);
             }
             
             if(isset($valor['ComprobanteImpuesto'])){
                 
                  $valor[$this->model]['ComprobanteImpuestoDefault'] =  $this->getImpuestos($valor['ComprobanteImpuesto'],$valor["Comprobante"]["id"],array(EnumSistema::COMPRAS),array(),EnumTipoImpuesto::RETENCIONES);
                  $valor[$this->model]['ComprobanteImpuestoTotal'] = (string) $this->TotalImpuestos($valor[$this->model]['ComprobanteImpuestoDefault']);
             
                 
             }
             
             if(isset($valor['EstadoComprobante'])){
                $valor[$this->model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
                unset($valor['EstadoComprobante']);
             }
             if(isset($valor['CondicionPago'])){
                $valor[$this->model]['d_condicion_pago'] = $valor['CondicionPago']['d_condicion_pago'];
                unset($valor['CondicionPago']);
             }
             
             
             
              if(isset($valor['Transportista'])){
                $valor[$this->model]['t_razon_social'] = $valor['Transportista']['razon_social'];
                $valor[$this->model]['t_direccion'] = $valor['Transportista']['calle'].'-'.$valor['Transportista']['numero_calle'].'-'.$valor['Transportista']['localidad'].$valor['Transportista']['ciudad'];
                unset($valor['Transportista']);
             }
             
             if(isset($valor['Persona'])){
            
            
                 if($valor['Persona']['id']== null)
                 {    
                    $valor[$this->model]['razon_social'] = $valor['Comprobante']['razon_social']; 
                 }
                 else{
                     
                     $valor[$this->model]['razon_social'] = $valor['Persona']['razon_social'];
                     $valor[$this->model]['id_tipo_iva'] = $valor['Persona']['id_tipo_iva'];
                     $valor[$this->model]['codigo_persona'] = $valor['Persona']['codigo'];
                 }
                 
                 unset($valor['Persona']);
             }
             
            
          
               if(isset($valor['TipoComprobante'])){
                  $valor['Comprobante']['d_tipo_comprobante'] =$valor['TipoComprobante']['d_tipo_comprobante'];
              }
              
              
              
              $valor["Comprobante"]["es_importado"] = (string) $this->Comprobante->esImportado($valor['ComprobanteItem']);
              
              
              if(isset($valor['PuntoVenta'])){
                $valor[$this->model]['pto_nro_comprobante'] = (string) $this->Comprobante->GetNumberComprobante($valor['PuntoVenta']['numero'],$valor[$this->model]['nro_comprobante']);
                unset($valor['PuntoVenta']);
             }
              
              unset($valor['ComprobanteItem']);
           /* if(isset())
              $comprobantes_relacionados = $this->getComprobantesRelacionados($id);
           */
            
            unset($valor['Sucursal']);
            unset($valor['ComprobanteImpuesto']);
           $this->{$this->model}->formatearFechas($valor); 
        }
        
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    

    /**
    * @secured(ADD_ANTICIPO_COMPRA)
    */
    public function add(){
        
    $this->loadModel("Cheque");
    $cheques_error = '';
    
     $this->ProcesaDescuento();
     $this->ConvertirPreciosUnitariosAPositivo($this->request->data);
    
 
    
    
   
     
    if($this->Cheque->validarEgresoCheque($this->request->data,$cheques_error) == 1)    
            parent::add();    
     else{
              $mensaje = $cheques_error;
              $tipo = EnumError::ERROR;  
              
              $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                );      
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content"));         
     }
            
              
         
         
     }
    
    
    
    
    /**
    * @secured(MODIFICACION_ANTICIPO_COMPRA)
    */
    public function edit($id){
  
    $this->loadModel("Cheque");
    $this->loadModel("Comprobante");
    $cheques_error = '';
    $hay_error = 0;
    
    
    $this->ConvertirPreciosUnitariosAPositivo($this->request->data);
    if($this->Comprobante->getDefinitivoGrabado($id) != 1 ){
    //si envia decuento entonces calculo el neto
    $this->ProcesaDescuento();
    
     
    if($this->Cheque->validarEgresoCheque($this->request->data,$cheques_error) == 1){    
              //si es definitivo le seteo el id_comprobante a los cheques
              if($this->Comprobante->getDefinitivoGrabado($id) != 1 && $this->Comprobante->getDefinitivo($this->request->data) == 1  && $this->Cheque->tieneCheques($this->request->data)== 1 ){
                  
                  
                   $id_comprobante_destino = $this->request->data["Comprobante"]["id"];
                   $array_cheques = $this->Cheque->getArrayCheques($this->request->data);
                   $ds = $this->Cheque->getdatasource();
                    try{ 
                        $ds->begin();
                        $this->Cheque->actualizaComprobanteCheques($id_comprobante_destino,$array_cheques,"id_comprobante_salida");
                         
                        $array_cheque_terceros = array();
                        $array_cheque_propios = array();
                        
                        $this->Cheque->SeparaCheques($array_cheques,$array_cheque_terceros,$array_cheque_propios);//esta funcion le paso un array con cheques y me devuelve 2 arrays uno con los cheques de terceros y otro con los cheques propios
                        
                        $this->Cheque->actualizaEstadoCheques(EnumEstadoCheque::Emitido,$array_cheque_propios);//actualizo el estado de los cheques  
                        $this->Cheque->actualizaEstadoCheques(EnumEstadoCheque::Aplicado,$array_cheque_terceros);//actualizo el estado de los cheques  
                        
                        
                        $ds->commit();
                   }catch(Exception $e){
                     
                     $tipo = EnumError::ERROR;
                     $mensaje = 'Hubo una falla cuando se actulizaron los comprobantes de Salida en los cheques'.$e->getMessage();
                     $ds->rollback();
                     $hay_error = 1;
                   }
              }
              
        
              if($hay_error == 0){ /*Sino hubo falla al actualizar los cheques entonces si edito*/
              
              
              	
                $diferencia = $this->request->data["Comprobante"]["diferencia_valores"]; //si es (-) debe generar CI si es (+) genera DI
                 
                
                if($diferencia < 0)
                	$id_tipo_comprobante_a_generar = EnumTipoComprobante::AnticipoCompra;
                else if($diferencia >0)
                	$id_tipo_comprobante_a_generar = EnumTipoComprobante::DebitoInternoCompra;
                	
                 //el total del comprobante es la sumatoria de los valores ingresados
                 $this->request->data["Comprobante"]["total_comprobante"] = $this->request->data["Comprobante"]["total_comprobante"] + $diferencia*-1;
                         
              
                
             // $this->request->data["Comprobante"]["genera_ci "] =1;
                if($this->request->data["Comprobante"]["id_estado_comprobante"] == EnumEstadoComprobante::CerradoReimpresion &&
                	 $diferencia!=0){
                     
                     
                     
                     //debo generar credito interno ya que me esta pagando de mas. El balance del recibo entre recibido y imputados va a ser 0 ya que el CI compensa. Lo hago transaccional asi es mas seguro.
                $contador = false;
                 	if($diferencia < 0)
                 		$contador = $this->chequeoContador(EnumTipoComprobante::AnticipoCompra,$this->request->data["Comprobante"]["id_punto_venta"]);
                    elseif($diferencia >0)
                    	$contador = $this->chequeoContador(EnumTipoComprobante::DebitoInternoCompra,$this->request->data["Comprobante"]["id_punto_venta"]);
                    
                     if($contador){
                         
                         
                         $dsc = $this->Comprobante->getdatasource();
                         
                        
                         
                         try{
                         
                             $dsc->begin();
                             $this->Comprobante->CalcularMonedaBaseSecundaria($this->request->data,$this->model);//esta linea la pongo aca asi puedo acceder al valor_moneda, valor_moneda2 antes de llamar al edit
                             
                             $nro_comprobante_interno = $this->request->data["Comprobante"]["nro_comprobante"];//El Anticipo de Compra tiene el mismo numero que la OPA
                             
                             
                             if($diferencia < 0)
                                $observacion = "Anticipo por Orden de Pago de Anticipo Nro:".$this->request->data["Comprobante"]["nro_comprobante"];
                             else if($diferencia > 0)
                             	$observacion = "Debito Interno por Orden de Pago de Anticipo Nro:".$this->request->data["Comprobante"]["nro_comprobante"];
                             
                             $cabecera_comprobante = $this->Comprobante->getCabeceraComprobante(abs($diferencia),abs($diferencia),abs($diferencia),$id_tipo_comprobante_a_generar,date('Y-m-d'),EnumEstadoComprobante::CerradoReimpresion,"",$this->request->data["Comprobante"]["id_moneda"],$this->request->data["Comprobante"]["valor_moneda"],$this->request->data["Comprobante"]["valor_moneda2"],$this->request->data["Comprobante"]["valor_moneda3"],$observacion,$nro_comprobante_interno,$this->request->data["Comprobante"]["id_persona"],$this->Auth->user('id'),1,0,1,$this->request->data["Comprobante"]["id_punto_venta"],0);
                             
                             
                             $items_comprobante = $this->Comprobante->getComprobanteItem(1,abs($diferencia),abs($diferencia),1,1,EnumIva::exento,$observacion,1,$id,0);//el credito interno tiene como origen el recibo
                             $id_comprobante_interno = $this->Comprobante->InsertaComprobante($cabecera_comprobante,$items_comprobante);
                             
                             //Se inserto entonces actualizo los datos del recibo.
                             //$this->request->data["Comprobante"]["diferencia"] = 0;
                             
                             
                             //////////Agrego el item Credito interno de ventas al Recibo
                             
                             /*
                             $item_comprobante_credito_interno = $this->Comprobante->getComprobanteItem("",$diferencia,$diferencia,count($this->request->data["ComprobanteItemComprobante"])+1,count($this->request->data["ComprobanteItemComprobante"])+1,EnumIva::exento,"",1,$id_credito_interno,0);
                             
                             array_push($this->request->data["ComprobanteItemComprobante"],$item_comprobante_credito_interno["ComprobanteItem"]);
                             */
                             
                             parent::edit($id);
                             $dsc->commit();
                             return 0; 
                             }catch(Exception $e){
                             
                             $tipo = EnumError::ERROR;
                             $mensaje = 'Hubo una falla cuando se intento crear el comprobante interno. Intente nuevamente'.$e->getMessage();
                             $dsc->rollback();
                             
                             
                             
                             
                         }    
                     }else{
                          $tipo = EnumError::ERROR;  
                          $mensaje = "Se debe definir el numero inicial del contador para este comprobante (Anticipo o Debito Interno de Compras).La Orden de Pago de Anticipo no pudo editarse. Inicie el contador y luego edite la Orden de Pago"; 
                         
                     }
                    
                }else{//solo edito no necesito mas nada
                    
                     parent::edit($id);
                     return 0; 
                }
              
              
               
              
               }   
    }else{
              $mensaje = $cheques_error;
              $tipo = EnumError::ERROR;  
              
             
     }
      /*Aca solo entra si hubo error si entra por el edit sale por flujo de mensaje*/
     
     
    }else{
    	
    	$mensaje = 'El comprobante en esta instacia no puede ser editado';
    	$tipo = EnumError::ERROR;
    	$this->error = 1;
    }
    
    
    
      $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
                );      
            $this->set($output);
      
            $this->set("_serialize", array("status", "message", "content"));
            return;         
                
            
        
    }
    
    

    
   /**
    * @secured(BAJA_ANTICIPO_COMPRA)
    */
   public function deleteItem($id_item,$externo=1){
        
        $this->loadModel($this->model);
        
        
        
    
        
        
   
        $this->{$this->model}->ComprobanteItem->id = $id_item;
       try{
           
           //aca se debe chequear si el item es borrable. O sea sino se factura etc.
           
            if ($this->{$this->model}->ComprobanteItem->delete() ) {
                
                   
        
                $status = EnumError::SUCCESS;
                $mensaje = "El Item ha sido eliminado exitosamente.";
              
            }else{
              
                
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
                
                
            }
        }
        catch(Exception $ex){ 
              $mensaje = "Ha ocurrido un error, el item no ha podido ser borrado";
               $status = EnumError::ERROR; 
        }
        
        
        
        
        if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
        }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
        }
        
        
        
        
    }
     
    
      /**
    * @secured(CONSULTA_ANTICIPO_COMPRA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

    }
    
  
    /**
    * @secured(BTN_PDF_ANTICIPOSCOMPRA)
    */
    public function pdfExport($id,$vista=''){
        
    
    $this->loadModel("Comprobante");
    $orden_pago = $this->Comprobante->find('first', array(
                                                        'conditions' => array('Comprobante.id' => $id),
                                                        'contain' =>array('TipoComprobante')
                                                        ));
                                                        
     
    $comprobantes_relacionados = $this->getComprobantesRelacionados($id,0,1,0);  
                                       
      if($orden_pago){
        
        
      	$credito_internos_relacionados = array();
      	$debito_internos_relacionados = array();
      	foreach($comprobantes_relacionados as $comprobante){
      		
      		
      		if( isset($comprobante["Comprobante"]) && $comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::CreditoInternoCompra)
      			array_push($credito_internos_relacionados,$comprobante);
      			
      		if( isset($comprobante["Comprobante"]) && $comprobante["Comprobante"]["id_tipo_comprobante"] == EnumTipoComprobante::DebitoInternoCompra)
      			array_push($debito_internos_relacionados,$comprobante);
      			
      			
      	}
      	
      	$this->set('credito_interno_relacionados',$credito_internos_relacionados);
      	$this->set('debito_interno_relacionados',$debito_internos_relacionados);
      	
        
       
      	$this->viewPath = 'OrdenesPago';// Para cambiarle la ruta a la view, esta en otra carpeta, le fuerzo
        $letra = $orden_pago["TipoComprobante"]["letra"];
        $vista = "ordenPago".$letra;  
        parent::pdfExport($id,$vista);    
        
  
      }
     
        
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
        //$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
        $this->viewPath = 'OrdenesPago';// Para cambiarle la ruta a la view, esta en otra carpeta, le fuerzo
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);
    }

    
    
    
    
    
 
    
     private function ProcesaDescuento(){
        
         if(isset($this->request->data[$this->model]["descuento"]) && $this->request->data[$this->model]["descuento"]>0){
        
            $this->request->data[$this->model]["subtotal_bruto"] = $this->request->data[$this->model]["total_comprobante"]*(1/$this->request->data[$this->model]["descuento"]);
            $this->request->data[$this->model]["subtotal_neto"]  = $this->request->data[$this->model]["subtotal_bruto"];
    
        }else{
        
            $this->request->data[$this->model]["subtotal_bruto"] = $this->request->data[$this->model]["total_comprobante"];
            $this->request->data[$this->model]["subtotal_neto"]  = $this->request->data[$this->model]["total_comprobante"];
        }
        return;
    }
    
    
    
    public function Anular($id_ANTICIPO_COMPRA){
        
        $this->loadModel("Comprobante");
        $this->loadModel("Asiento");
        $this->loadModel("Cheque");
        
        
          $recibo = $this->Comprobante->find('first', array(
                    'conditions' => array('Comprobante.id'=>$id_ANTICIPO_COMPRA,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                    'contain' => array('ComprobanteValor','Asiento','ChequeSalida')
                ));
                
                 $ds = $this->Comprobante->getdatasource(); 
                 // $ds->rollback();
          $aborto = 0;
          
          $listado_cheques = array();
          $listado_cheques_propios = array();
          
                   
            if( $recibo && $recibo["Comprobante"]["id_estado_comprobante"]!= EnumEstadoComprobante::Anulado ){
                
                try{
                    $ds->begin();
                    
                  
                            $aborto =0; 
                       
                       
                     if($aborto!=1){
                               foreach($recibo["ChequeSalida"] as $cheque){
                                   
                                if($cheque["id_estado_cheque"]!= EnumEstadoCheque::Rechazado){//quiere decir que NO salio el cheque y lo puedo usar
                                
                                     
                                     if($cheque["id_estado_cheque"] == EnumTipoCheque::Terceros){
                                     $this->Cheque->updateAll(
                                                            array('Cheque.id_comprobante_salida' => NULL,'Cheque.id_estado_cheque'=>EnumEstadoCheque::Cartera ),
                                                            array('Cheque.id' => $cheque["id"]) );   
                                
                                     array_push($listado_cheques,$cheque["nro_cheque"]);
                                    }else{//
                                        
                                           $this->Cheque->updateAll(
                                           		array('Cheque.id_comprobante_salida' => NULL,'Cheque.id_estado_cheque'=>EnumEstadoCheque::Cartera),
                                                            array('Cheque.id' => $cheque["id"]) );   
                                      array_push($listado_cheques_propios,$cheque["nro_cheque"]);   
                                    }
                                }
                            }        
                        
                      
                        
                        
                        $output_asiento_revertir = array("id_asiento"=>0);
                        
                        if($recibo["Comprobante"]["comprobante_genera_asiento"] == 1)
                            $output_asiento_revertir = $this->Asiento->revertir($recibo["Asiento"]["id"]);  /*Se debe revertir el asiento del comprobante*/  
                        
                        /* Si genero RETENCIONES se deben anular */
                        
                        
                        
                        $this->Comprobante->updateAll(
                                                        array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
                                                        array('Comprobante.id' => $id_ANTICIPO_COMPRA) );
                                                        
                       
                        
                        
                        
                        //busco los anticipos hechos
                        $anticipo_compra = $this->Comprobante->ComprobanteItem->find('first',array(
                        		
                        		'conditions'=>array("ComprobanteItem.id_comprobante_origen"=>$id_ANTICIPO_COMPRA,"Comprobante.id_tipo_comprobante"=>EnumTipoComprobante::AnticipoCompra),
                        		'contain'=>array("Comprobante")
                        		
                        		
                        ) );
                        
                        //anulo los CI hechos
                        if($anticipo_compra)
                        	$this->Comprobante->updateAll(
                        			array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
                        			array('Comprobante.id' => $anticipo_compra["Comprobante"]["id"]) );
                        	
                        	
                        	//busco los debitos internos hechos
                        $debito_interno = $this->Comprobante->ComprobanteItem->find('first',array(
                        			
                        			'conditions'=>array("ComprobanteItem.id_comprobante_origen"=>$id_ANTICIPO_COMPRA,"Comprobante.id_tipo_comprobante"=>EnumTipoComprobante::DebitoInternoCompra),
                        			'contain'=>array("Comprobante")
                        			
                        			
                        	) );
                        
                        
                       
                       
                       if($debito_interno)
                        	$this->Comprobante->updateAll(
                        			array('Comprobante.id_estado_comprobante' => EnumEstadoComprobante::Anulado,'Comprobante.fecha_anulacion' => "'".date("Y-m-d")."'",'Comprobante.definitivo' =>1 ),
                        			array('Comprobante.id' => $debito_interno["Comprobante"]["id"]) );
                        	
                                                        
                        $ds->commit(); 
                        $tipo = EnumError::SUCCESS;
                        
                        
                        $op = $this->Comprobante->find('first', array(
                        		'conditions' => array('Comprobante.id'=>$id_ANTICIPO_COMPRA,'Comprobante.id_tipo_comprobante'=>$this->id_tipo_comprobante),
                        		'contain' => false
                        ));
                        
                        $this->CerrarComprobantesAsociados($op);/*leo Nuevamente el Recibo*/
                        
                      
                       $mensaje = '';
                        if(count($listado_cheques)>0)
                            $mensaje .= ". Se liberaron en la cartera los siguientes cheques: Cheque Nro:".implode(" Cheque Nro: ",$listado_cheques);
                            
                        if(count($listado_cheques_propios)>0)
                        	$mensaje .= " Se Anularon los siguientes cheques propios: Cheque Nro:".implode(" Cheque Nro: ",$listado_cheques_propios);    
                        
                        $mensaje = "El Comprobante se anulo correctamente ".$mensaje;       
                        
                                                
                    }else{
                        
                        $tipo = EnumError::ERROR;
                        $mensaje = "El Comprobante NO puede anularse ya existe un cheque que se ingreso fue utilizado para otra operatoria";
                    }
              
                
                
                   
                    
                    
                  
                  
                    
                     
               }catch(Exception $e){ 
                
                $ds->rollback();
                $tipo = EnumError::ERROR;
                $mensaje = "No es posible anular el Comprobante ERROR:".$e->getMessage();
                
              }  
                            
            }else{
                
               $tipo = EnumError::ERROR;
               $mensaje = "No es posible anular el Comprobante ya se encuenta ANULADO";
            } 
            
            
            if(isset( $output_asiento_revertir["id_asiento"]))
            	$id_asiento_revertir =  $output_asiento_revertir["id_asiento"];
            else
            	$id_asiento_revertir = 0;
            
        $output = array(
                "status" => $tipo,
                "message" => $mensaje,
                "content" => "",
        		"id_asiento" => $id_asiento_revertir,
                );      
        echo json_encode($output);
        die();
        
         
        
    }
    
    
    public function CalcularImpuestos($id_comprobante,$id_tipo_impuesto='',$error=0,$message=''){
        
       
       return 0;
        
        
    }
    
    
    public function getQueryImpuestos($id_comprobante,$id_sistema,$id_tipo_impuesto,$array_impuestos_not_in=array() ){                              
    
      return array();
    }
    
    
    protected function borraImpuestos($id_comprobante){
    	
    return;
        
        
    }
    
    private function totalRetencionesManual(&$error=0,&$message=''){
         
        return 0;
    }
    
    
     private function totalRetencionesAutomatico(&$error=0,&$message=''){
         
     	return 0;
         
         
         
     }
    
    
    protected function actualizoMontos($id_c,$nro_c,$total_iva,$total_impuestos){
     return;   
    }
    
    
    /**
    * @secured(CONSULTA_ANTICIPO_COMPRA)
    */
    public function  getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante=0,$generados = 1 )
    {
        parent::getComprobantesRelacionadosExterno($id_comprobante,$id_tipo_comprobante,$generados);
    }
    
    
    /**
     * @secured(BTN_EXCEL_ANTICIPOSCOMPRA)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Ordenes de Pago");
    	
    	
    	
    }
    
    
    public function CerrarComprobantesAsociados($recibo_objeto){
    	
    	return;
    }
    
    
   public function  ConvertirPreciosUnitariosAPositivo(&$items){//sobreescrita, no utiliza precio_unitario
   	return;
   	
   }
    
   
  
}
?>