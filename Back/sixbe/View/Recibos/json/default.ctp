<?php
        // CONFIG  CON MODE DISPLAYED CELLS
        
        include(APP.'View'.DS.'Comprobantes'.DS.'json'.DS.'default.ctp');
        
   
        
   
        
        $model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
        $model["pto_nro_comprobante"]["order"] = "0";
        $model["pto_nro_comprobante"]["width"] = "20";
        $model["pto_nro_comprobante"]["web_width"] = "15";
        $model["pto_nro_comprobante"]["text_colour"] = "#000000";
        
        $model["fecha_generacion"]["show_grid"] = 1;
        $model["fecha_generacion"]["header_display"] = "Fecha Creaci&oacute;n";
        $model["fecha_generacion"]["order"] = "1";
        $model["fecha_generacion"]["width"] = "15";
        $model["fecha_generacion"]["text_colour"] = "#0000FF";
        
        
        $model["fecha_contable"]["show_grid"] = 1;
        $model["fecha_contable"]["header_display"] = "Fecha Contable";
        $model["fecha_contable"]["order"] = "2";
        $model["fecha_contable"]["width"] = "15";
        $model["fecha_contable"]["text_colour"] = "#0000FF";
        
        $model["codigo_persona"]["show_grid"] = 1;
        $model["codigo_persona"]["header_display"] = "Cod. Cliente";
        $model["codigo_persona"]["order"] = "3";
        $model["codigo_persona"]["width"] = "10";
        $model["codigo_persona"]["text_colour"] = "#000000";
        
        $model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["header_display"] = "Raz&oacute;n social";
        $model["razon_social"]["order"] = "4";
        $model["razon_social"]["width"] = "20";
        $model["razon_social"]["text_colour"] = "#000000";
        
        $model["d_moneda_simbolo"]["show_grid"] = 1;
        $model["d_moneda_simbolo"]["type"] = EnumTipoDato::cadena; 
        $model["d_moneda_simbolo"]["header_display"] = "Moneda";
        $model["d_moneda_simbolo"]["order"] = "7";
        $model["d_moneda_simbolo"]["width"] = "10";
        $model["d_moneda_simbolo"]["text_colour"] = "#F0000F";
        
        $model["total_comprobante"]["show_grid"] = 1;
        $model["total_comprobante"]["header_display"] = "Monto";
        $model["total_comprobante"]["order"] = "8";
        $model["total_comprobante"]["width"] = "20";
        $model["total_comprobante"]["text_colour"] = "#F0000F";
        
        $model["d_estado_comprobante"]["show_grid"] = 1;
        $model["d_estado_comprobante"]["type"] = EnumTipoDato::cadena; 
        $model["d_estado_comprobante"]["header_display"] = "Estado";
        $model["d_estado_comprobante"]["order"] = "9";
        $model["d_estado_comprobante"]["width"] = "20";
        $model["d_estado_comprobante"]["text_colour"] = "#004400";
        
        
        $model["d_usuario"]["show_grid"] = 1;
        $model["d_usuario"]["header_display"] = "Usuario";
        $model["d_usuario"]["width"] = "8";
        $model["d_usuario"]["order"] = "10";
        $model["d_usuario"]["text_colour"] = "#004400";
        
        
        
        
         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Comprobante",
                    "content" => $model
                );
        
          echo json_encode($output);
?>