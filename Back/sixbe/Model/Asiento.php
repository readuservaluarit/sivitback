<?php
App::uses('AppModel', 'Model');
App::uses('AsientoCuentaContable', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class Asiento extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'asiento';

    public $actsAs = array('Containable');
	
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Your custom message here',
                'allowEmpty' => false,
                'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),

	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 
 
  public $belongsTo = array(
        'ClaseAsiento' => array(
            'className' => 'ClaseAsiento',
            'foreignKey' => 'id_clase_asiento',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
        ),
     'EstadoAsiento' => array(
            'className' => 'EstadoAsiento',
            'foreignKey' => 'id_estado_asiento',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
        ),
        
      'EjercicioContable' => array(
            'className' => 'EjercicioContable',
            'foreignKey' => 'id_ejercicio_contable',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
        ),
        
     'TipoAsiento' => array(
            'className' => 'TipoAsiento',
            'foreignKey' => 'id_tipo_asiento',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
        ),
		
  'Moneda' => array(
            'className' => 'Moneda',
            'foreignKey' => 'id_moneda',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
        
   'AsientoEfecto' => array(
            'className' => 'AsientoEfecto',
            'foreignKey' => 'id_asiento_efecto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
   'Comprobante' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
 'PeriodoContable' => array(
            'className' => 'PeriodoContable',
            'foreignKey' => 'id_periodo_contable',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),
  'AgrupacionAsiento' => array(
            'className' => 'AgrupacionAsiento',
            'foreignKey' => 'id_agrupacion_asiento',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
  
        ),    
	);
        
        
    public $hasMany = array(
        'AsientoCuentaContable' => array(
            'className' => 'AsientoCuentaContable',
            'foreignKey' => 'id_asiento',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        );
        
        
        
 
        
public function revertir($id,$monto_cheque=0){
    
    /*Si tiene cheques y esos quedan en la cartera la cuenta valores a depositar se debe dejar igual*/

        
        
            $asiento = $this->find('first', array(
                    'conditions' => array('Asiento.id'=>$id),
                    'contain' => array("AsientoCuentaContable","Comprobante","PeriodoContable"=>array("EjercicioContable"))
                ));
            
            
            
            
       /*     $id_asiento_revertido = 0;*/
            
            if($asiento){         
                                           
            
            	/*
            	foreach($asiento["AsientoCuentaContable"] as &$item_asiento){

                    unset($item_asiento["id"]);
                    
                    if($item_asiento["es_debe"] == 1)
                        $item_asiento["es_debe"] = 0;
                    else
                        $item_asiento["es_debe"] = 1;


                }
                
                
                if(isset($asiento["Comprobante"]["id"]) && $asiento["Comprobante"]["id"]>0)//El Asiento revertido de asiento tiene la fecha del comprobante por defecto
                	$fecha = $asiento["Comprobante"]["fecha_contable"];
                else
                	$fecha = date("Y-m-d");
                
                	
                	
                $id_asiento_revertido = $id;
                $codigo_asiento = $this->getCodigo($id_asiento_revertido);
                $asiento["Asiento"]["fecha"] = $fecha;
                $asiento["Asiento"]["id_asiento_revertido"] = $id;  
                $asiento["Asiento"]["id_usuario"] = $uid = CakeSession::read("Auth.User.id");  
                $asiento["Asiento"]["codigo"] = $codigo_asiento;  
                $asiento["Asiento"]["d_asiento"] = "Revertido de Asiento Nro. ".$asiento["Asiento"]["codigo"];  
                unset($asiento["Asiento"]["id"]); 
				*/
                if($asiento["PeriodoContable"]["cerrado"] == 1 || $asiento["PeriodoContable"]["EjercicioContable"]["id_estado"] == EnumEstado::Cerrada)
                {
                	$id_asiento_revertido = 0; 
                	
                	$message = "El Asiento no es posible anularlo. Ya que el Peri&oacute;do contable en el cual se encuentra,est&aacute; cerrado, o el Ejercicio Contable est&aacute; cerrado";
                	$error = EnumError::ERROR;
                	
                }else{
                	
                	$ds = $this->getdatasource();
                	$asiento_cuenta_contable = new AsientoCuentaContable();
                	$asiento_cuenta_contable->deleteAll(array("AsientoCuentaContable.id_asiento"=>$id),false);
                	
                	$codigo_asiento = $this->getCodigo($id);
                	$id_asiento_revertido = $id;
				
	                if($this->deleteAll(array("Asiento.id"=>$id),false)){
	                    
	                    
	                	$ds->commit();
	                    $message = "El asiento fue revertido correctamente. Nro: ".$codigo_asiento.". ID(".$id_asiento_revertido.")";
	                    
	                    $error = EnumError::SUCCESS;
	              
	                }else{
	                	
	                	$ds->rollback();
	                    $id_asiento_revertido = -1;    
	                    $message = "Hubo un error, el asiento NO fue revertido correctamente";
	                    $error = EnumError::ERROR;
	                } 
                
                }
            }else{
                $id_asiento_revertido = 0; 
                $message = "El asiento que indic&oacute; no existe en la base de datos";
                $error = EnumError::ERROR; 
            }

            $output = array(
                "status" => $error,
                "message" => $message,
                "id_asiento" => $id_asiento_revertido
                //"content" => $data
            );

        return $output;
        }
        
        
        
public function  InsertaAsiento($cabecera,$detalle, &$respuesta_error = ''){ /*Cabecera es un array con ASIENTO y $detalle es un array con el tipo AsientoCuentaContable*/
    
    
    
          
  $ds = $this->getdatasource();
  $respuesta_error = '';
  try{
      
      
    $ds->begin();

    
    if($this->saveAll($cabecera)){
        $id_asiento = $this->id;
        
        $asiento_cuenta_contable = new AsientoCuentaContable();      //creo un objeto para poder insertar los hijos ya que necesito el ID del padre
        
        
        if($detalle){
            foreach($detalle as &$item){
                
                $item["AsientoCuentaContable"]["id_asiento"] = $id_asiento;
            }
            
            try{
            	$asiento_cuenta_contable->saveAll($detalle);
            }catch(Exception $e){
            	
            	$respuesta_error = $e->getMessage();
            }
        }
        $ds->commit();
        $this->chequeoSeguridadBalanceoAsiento($id_asiento);/*Chequea que el asiento este balanceado, sino lo esta el asiento queda en revision*/
        
        return $id_asiento;
         
    }else{
        
    	$respuesta_error.="La cabecera del asiento no puede guardarse"; 
        return 0;
    }
  
  
  }catch(Exception $e){
  	
  	$respuesta_error .= $e->getMessage();
    $ds->rollback();
    return 0;
   } 
    
    
}



public function chequeoSeguridadBalanceoAsiento($id_asiento){
	

	
	/*
	if($detalle){
		
		foreach($detalle as &$item){
			
			if($item["AsientoCuentaContable"]["es_debe"] == 1){
				
				$total_debe = $total_debe  + $item["AsientoCuentaContable"]["monto"];
			}else{
				
				$total_haber= $total_haber + $item["AsientoCuentaContable"]["monto"];
			}
		}
		
		if($total_debe != $total_haber)
			$cabecera["Asiento"]["id_estado_asiento"] = EnumEstadoAsiento::EnRevision;
		
	}
	*/
		App::uses('AsientoCuentaContable', 'Model');
		$acc_obj = new AsientoCuentaContable();
		
		
		$resultado = $acc_obj->find('all', array(
				'conditions' => array('AsientoCuentaContable.id_asiento' => $id_asiento),
				'fields'=>array('SUM(IF(AsientoCuentaContable.es_debe=1,AsientoCuentaContable.monto,0)) - SUM(IF(AsientoCuentaContable.es_debe=0,AsientoCuentaContable.monto,0)) as monto'),
				'contain' =>false
		)); 
		
		
		
		
		$asiento = $this->find('first', array(
				'conditions' => array('Asiento.id' => $id_asiento),
				'contain' =>array("Comprobante"=>array("TipoComprobante"))
		));
		
		
		
		
		
		
		if(isset($resultado[0][0]["monto"]) && $resultado[0][0]["monto"] != 0){
			
			
			$estado_revision = EnumEstadoAsiento::EnRevision;
			
			if(isset($asiento["Comprobante"]["id"]) && $asiento["Comprobante"]["id"]>0){
				
				App::uses('Comprobante', 'Model');
				$comprobante_obj = new Comprobante();
				
				/*Para las facturas y notas de ventas este control no VA*/
				$facturas = $comprobante_obj->getTiposComprobanteController(EnumController::Facturas);
				$notas = $comprobante_obj->getTiposComprobanteController(EnumController::Notas);
				
				$comprobantes = array_merge($facturas,$notas);
				
				if(in_array($asiento["Comprobante"]["id_tipo_comprobante"],$comprobantes) && $asiento["Comprobante"]["cae"]>0 ){
					
					/*SUMO el haber y se lo seteo al debe*/
					$resultado_haber = $acc_obj->find('all', array(
							'conditions' => array('AsientoCuentaContable.id_asiento' => $id_asiento),
							'fields'=>array('SUM(IF(AsientoCuentaContable.es_debe=0,AsientoCuentaContable.monto,0)) as monto'),
							'contain' =>false
					));
					
					$monto = $resultado_haber[0][0]["monto"];
					
					
					$es_debe_cuenta_contable_detalle = ($this->esDebe($asiento["Comprobante"]["TipoComprobante"]["signo_contable"]) - 1)*-1;
					
					
					
					/*Como desbalanceo por centavos entonces fuerzo el es_debe*/
					
					if($es_debe_cuenta_contable_detalle == 0){
				
					$acc_obj->updateAll(array("AsientoCuentaContable.monto" => $monto),
							array('AsientoCuentaContable.es_debe' => 1,"AsientoCuentaContable.id_asiento"=>$id_asiento) );
					}else{
						
						$acc_obj->updateAll(array("AsientoCuentaContable.monto" => $monto),
								array('AsientoCuentaContable.es_debe' => 0,"AsientoCuentaContable.id_asiento"=>$id_asiento) );
					}
					
					
				
					$estado_revision = EnumEstadoAsiento::Registrado;
					
					
				}
			}
				
			$this->updateAll(
					array('Asiento.id_estado_asiento' =>$estado_revision),
					array('Asiento.id' => $id_asiento) ); 
			
			
		}
		
		
		
}

public function getDefinitivoGrabado($id){
    
    $comprobante = $this->find('first', array(
                                                'conditions' => array('Asiento.id' => $id),
                                                'contain' =>false
                                                )); 
        
   
   if(in_array($comprobante["Asiento"]["id_estado_asiento"],array(EnumEstadoAsiento::Registrado)))
        return 1;
   else
        return 0; 
        
        
}

public function formatearFechas(&$comprobante){
      
      
        if($comprobante["Asiento"]["fecha"]!=null)
            $comprobante["Asiento"]["fecha"] = date("d-m-Y", strtotime($comprobante["Asiento"]["fecha"]));
            
}


public function getCantidadAsientos($id_periodo_contable,$id_estado_asiento=0){
    
     $cant_asientos = $this->find('count', array(
                                                'conditions' => array('Asiento.id_periodo_contable' => $id_periodo_contable,'Asiento.id_estado_asiento' => $id_estado_asiento),
                                                'contain' =>false
                                                ));
                                                
                                                 
     return $cant_asientos;                                              
    
}

 function afterSave($created, $options = array())  {
       
       /////////////////////////////////////INSERT//////////////////////////////////////////////////////////////////////
        if($created){ //cuando creo el asiento
            
            
            App::uses('EjercicioContable', 'Model');
            App::uses('Contador', 'Model');
            
            $ejercicio_obj = new EjercicioContable();
            $contador_obj = new Contador();
            
            $ejercicio_actual =  $ejercicio_obj->GetEjercicioPorFecha(date("Y-m-d"),0,1);//le pido el actual segun la fecha
            $id_contador = $ejercicio_actual["Contador"]["id"];
            $codigo = $contador_obj->getProximoNumero($id_contador);         
            $empresa = CakeSession::read('Empresa');
            $id_asiento = $this->getLastInsertId();
              //actualizo el costo ya que llegue al final del arbol y no tiene hijos
            $this->updateAll(
                                                          array('Asiento.codigo' => $codigo ),
                                                          array('Asiento.id' => $id_asiento) );   
            
        }
 }
 
 public function getCodigo($id_asiento){
     
     $asiento = $this->find('first', array(
                                                'conditions' => array('Asiento.id' => $id_asiento),
                                                'contain' =>false
                                                )); 
                                                
     if($asiento)
        return $asiento["Asiento"]["codigo"];
     else
        return 0;                                            
     
 }

 
 public function esDebe($signo_contable){
 	
 	if($signo_contable == -1)
 		return 1;
 		else
 			return 0;
 			
 }
        
	

}
