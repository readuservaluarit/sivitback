
INSERT INTO `ebsatestrrhh`.`tipo_persona` (`id`, `d_tipo_persona`) VALUES ('7', 'FAMILIAR'); 
 INSERT INTO `ebsatestrrhh`.`tipo_persona` (`id`, `d_tipo_persona`) VALUES ('8', 'LEGAJO'); 

INSERT INTO ebsatestrrhh.`persona` (
	id_tipo_persona,
  id_persona_sexo,
  id_persona_estado_civil,
  id_rrhh_categoria,
  codigo,
  apellido,
  nombre,
  numero_documento,
  fecha_nacimiento,
  fecha_creacion,
  email,
  id_rrhh_obra_social,
  id_rrhh_clase_legajo,
  id_area,
  id_rrhh_condicion_sicoss,
  fecha_ingreso,
  fecha_antiguedad
) 
SELECT 


8,
  Id_Sexo,
  CASE
    WHEN Id_Estado_Civil = 1 
    THEN 4 
    WHEN Id_Estado_Civil = 2 
    THEN 1 
    WHEN Id_Estado_Civil = 3 
    THEN 3 
    WHEN Id_Estado_Civil = 4 
    THEN 5 
    WHEN Id_Estado_Civil = 5 
    THEN 2 
  END AS Id_Estado_Civil2,
  CASE
    WHEN Id_Categoria = 3 
    THEN 98 
    WHEN Id_Categoria = 4 
    THEN 109 
    WHEN Id_Categoria = 5 
    THEN 101 
    WHEN Id_Categoria = 6 
    THEN 99 
    WHEN Id_Categoria = 7 
    THEN 100 
    WHEN Id_Categoria = 8 
    THEN 110 
    WHEN Id_Categoria = 9 
    THEN 86 
    WHEN Id_Categoria = 10 
    THEN 87 
    WHEN Id_Categoria = 11 
    THEN 111 
    WHEN Id_Categoria = 15 
    THEN 112 
    WHEN Id_Categoria = 21 
    THEN 113 
    WHEN Id_Categoria = 25 
    THEN 106 
    WHEN Id_Categoria = 26 
    THEN 114 
    WHEN Id_Categoria = 27 
    THEN 115 
    WHEN Id_Categoria = 29 
    THEN 116 
    WHEN Id_Categoria = 35 
    THEN 97 
    WHEN Id_Categoria = 36 
    THEN 117 
    WHEN Id_Categoria = 73 
    THEN 103 
    WHEN Id_Categoria = 75 
    THEN 118 
    WHEN Id_Categoria = 76 
    THEN 119 
    WHEN Id_Categoria = 77 
    THEN 120 
    WHEN Id_Categoria = 82 
    THEN 121 
  END AS Id_Categoria2,
  Codigo_Persona,
  Apellido_Persona,
  Nombre_Persona,
  Numero_Documento,
  Fecha_Nacimiento,
  Fecha_Alta,
  Email_Persona,
  vetest2.`rrhh_obra_social`.`id` AS id_obra_social,
  /*aca poner la base definitiva del SIV*/
  CASE
    WHEN Id_Tipo_Liquidacion = 3 
    THEN 1 
    WHEN Id_Tipo_Liquidacion = 1 
    THEN 2 
  END AS id_rrhh_clase_legajo,
  Id_Seccion AS id_area,
  (Id_Condicion_Laboral + 1) AS a,
  /*Condicion SICOSS*/
  Fecha_Ingreso,
  Fecha_Antiguedad 
FROM
  personas 
  JOIN personas_sueldo 
    ON personas_sueldo.`Id_Persona` = personas.`Id_Persona` 
  JOIN obras_sociales 
    ON obras_sociales.`Id_Obra_Social` = personas_sueldo.`Id_Obra_Social` 
  LEFT JOIN vetest2.`rrhh_obra_social` 
    ON rrhh_obra_social.`codigo` = TRIM(LEADING '0' FROM obras_sociales.`Codigo_Obra_Social` )
    /*Aca va la BASE REAL LA FINAL*/
WHERE Id_Tipo_Persona = 1 
  AND personas.Id_Estado = 1 
  
  UPDATE persona SET codigo = TRIM(LEADING '0' FROM codigo) WHERE id_tipo_persona = 8