/**
 * Los validadores que hay son:
 * =============================
 *
 * "Required"                     Cuando el campo debe contener valores obligatoriamente.
 * "RequiredGroupCheck"         Cuando se deben marcar obligatoriamente uno de dos checkbox.
 * "RequiredGroupField"         Cuando se deben completar obligatoriamente alguno de los campos pasados en el array de ids.
 * "Empty"                        Cuando el campo debe dejarse en blanco obligatoriamente.
 * "Equals"                     El valor de referencia debe ser igual al introducido en el campo.
 * "NotEquals"                     El valor de referencia debe ser distinto al introducido en el campo.
 * "Length"                     El valor del campo debe ser de un largo igual que el valor de referencia.
 * "MinLength"                     El valor del campo debe ser de un largo mayor que el valor de referencia.
 * "MaxLength"                     El valor del campo debe ser de un largo menor que el valor de referencia.
 * "IntRange"                     El valor del campo se debe encontrar entre los valores de referencia enteros introducidos.
 * "Numeric"                     El valor del campo debe ser un numero.
 * "NumericOfLength"            El valor del campo debe ser de un un numero con una cantidad de digitos menor al valor de referencia.
 * "Float"                         El valor del campo debe ser un numero, pero puede ser decimal.
 * "LimitedFloat"                 El valor del campo debe ser un float, pero limitado en la cantidad de digitos.
 * "Alphabetic"                 El campo debe contener letra.
 * "AlphaNumeric"                 El campo debe contener letras o numeros.
 * "SmallerOrEqual"             El valor del campo debe ser menor o igual que el valor de referencia.
 * "Smaller"                     El valor del campo debe ser menor que el valor de referencia.
 * "GreaterOrEqual"                El valor del campo debe ser mayor o igual que el valor de referencia.
 * "Greater"                     El valor del campo debe ser mayor que el valor de referencia.
 * "RegExp"                        Comprueba que el valor del campo comforme a la expresion regular introducida como valor de referencia.
 * "Date"                        Comprueba que el valor ingresado corresponda a una fecha con formato dd/mm/aaaa.
 * "DateSmaller"                Comprueba que el valor ingresado corresponda a una fecha menor a la fecha de referencia.
 * "DateSmallerOrEqual"            Comprueba que el valor ingresado corresponda a una fecha menor o igual a la fecha de referencia.
 * "DateGreater"                Comprueba que el valor ingresado corresponda a una fecha mayor a la fecha de referencia.
 * "DateGreaterOrEqual"            Comprueba que el valor ingresado corresponda a una fecha mayor o igual a la fecha de referencia.
 * "DateSmallerToday"            Comprueba que el valor ingresado corresponda a una fecha menor al día de hoy.
 * "DateSmallerOrEqualToday"    Comprueba que el valor ingresado corresponda a una fecha menor o igual al día de hoy.
 * "DateGreaterToday"            Comprueba que el valor ingresado corresponda a una fecha mayor al día de hoy.
 * "DateGreaterOrEqualToday"    Comprueba que el valor ingresado corresponda a una fecha mayor o igual al día de hoy.
 * "DatePeriod"                    Comprueba que las dos fechas ingresadas correspondan a un período válido.
 * "Time"                        Comprueba que el valor ingresado corresponda a un horario con formato hh/mm.
 * "TimePeriod"                    Comprueba que las dos horas ingresadas correspondan a un período válido.
 * "Email"                        Comprueba que el valor ingresado sea una dirección de email correcta.
 * "Day"                        El valor del campo debe ser un número entre 1 y 31.
 * "Month"                        El valor del campo debe ser un número entre 1 y 12.
 * "Year"                        El valor del campo debe ser un número entre 1900 y el año actual.
 * "YearSmallerOrEqual"            Comprueba que el año ingresado sea menor o igual al año de referencia.
 * "CUIL"                        Comprueba que el CUIL/CUIT ingresado sea valido (11 digitos sin guiones).
 * "ComboNotEqual"              El valor seleccionado del combo debe ser distinto al valod de referencia.
 */


/**
 * Constructor del Validador de campos de formularios.
 * Tira mensaje de error si no encuentra el formualrio.
 * @param formName El nombre del formulario a validar.
 */
Validator = function (formName)
{
    this.formObj = document.forms[formName];
    if(!this.formObj)
    {
        alert("ERROR: No se pudo obtener el fomulario "+formName);
        return;
    }
    this.errorsContainer = "";
    this.errorsArray = new Array();

    this.element = "";   // contiene el nombre del elemento que presento el primer error y sobre el que hara foco si focus==true.
    this.focus = false;  // indica si se hara foco sobre el primer elemento erroneo encontrado.
    this.select = false; // indica si se selecionará el primer elemento erroneo si this.focus = true
};


/* ###################### *
 * ##  PUBLIC METHODS  ## *
 * ###################### */


/**
 * Permite agregar manualmente un mensaje de error respecto de un item al
 * array de errores.
 */
Validator.prototype.addErrorMessage = function (itemName, message) {
    this.addValidationResult(this.getItem( itemName ), message);
};

/**
 * Indica si todas las validaciones efectuadas hasta el momento fueron
 * correctas.
 */
Validator.prototype.isAllValid = function () {
    return (this.errorsArray.length == 0);
};

/**
 * Devuelve un Array con los String de error de validacion.
 */
Validator.prototype.getErrorsArray = function () {
    return this.errorsArray;
};

/**
 * Devuelve un String con todos los mensajes de errores.
 */
Validator.prototype.getErrorsMessage = function () {
    var errors = new String();
    for ( var i = 0; i < this.errorsArray.length; i++ ) {
        errors += this.errorsArray[ i ].message;
    }
    return errors;
};

Validator.prototype.showResult = function (itemName, message) {
    if(this.errorsContainer != "") {
        alert(this.errorsContainer);

        if(this.focus) {
            this.formObj.elements[this.element].focus();

            if(this.select) {
                this.formObj.elements[this.element].select();
            }
        }
        return false;
    }
    return true;
};

/**
 * Muestra las validaciones en el formato de jQuery.vinter.validate
 *
 * @param errorMsgs vector de mensajes de error a mostrar. Si esta vacio se toman los errores del validator,
 *                  sino, se muestran éstos.
 * @param validationDiv id del div en el cual se van a mostrar los errores, si validationDiv esta vacio, se toma
 *                  por default el id 'validationMsg'
 * @param validationTitle id del div en el cual se va a mostrar el titulo, si validationTitle esta vacio, se toma
 *                  por default el id 'validationTitle'
 */
Validator.prototype.showValidations = function (errorMsgs, validationDiv, validationTitle) {
    if (validationDiv == undefined){
        validationDiv = 'validationMsg';
    }

    if (validationTitle == undefined){
        validationTitle = 'validationTitle';
    }

    var div = this.getItem(validationDiv);
    div.style.marginTop = "0px";
    div.style.marginBottom = "10px";
    div.style.display = "block";
    window.scrollTo(0,0);
    div.innerHTML = "";

    var divInterno = document.createElement("div");
    div.appendChild(divInterno);
    divInterno.setAttribute("class", "alert alert-danger");
    
    var divTitulo = document.createElement("div");
    divInterno.appendChild(divTitulo);
    divTitulo.id = validationTitle;
    //divTitulo.innerHTML = "<span class='hide'>x</span><strong>Errores en el formulario</strong>";
    divInterno.innerHTML = "<button data-dismiss='alert' class='close' type='button'>×</button>";
    
    
    var ul = document.createElement("ul");
    divInterno.appendChild(ul);

    var errores = (errorMsgs == undefined || errorMsgs == null || errorMsgs.length == 0) ? this.getErrorsArray() : errorMsgs;
    Validator.highlightedErrors = new Array();
    for(i in errores) {
        var li = document.createElement("li");
        ul.appendChild(li);

        li.style.fontStyle = "italic";
        li.id = "validationerror_" + i;
        li.innerHTML = errores[i].message;

        // Remarco los campos con errores
        errores[i].element.style.background = "none repeat scroll 0 0 #F5E7E4";
        errores[i].element.style.border = "1px solid #FF0000";
        Validator.highlightedErrors.push(errores[i].element);
    }
    
    //PAra que se oculte al clickear la x
    $('span.hide').click(function() {
        $(this).parent().slideUp();                       
    });

}

/**
 * Limpia los mensajes visuales de errores de validacion.
 *
 * Al simular un metodo estatico puede ser invocado de la siguiente forma:
 *    Validator.clearValidationMsgs();
 */
Validator.clearValidationMsgs = function (validationDivId) { // Aproposito sin prototype para usarlo como static method.
    if (validationDivId == undefined || validationDivId == null || validationDivId.replace(/^\s*|\s*$/g,"") == "") {
        validationDivId = 'validationMsg';
    }

    var div = document.getElementById(validationDivId);
    div.style.display = "none";
    div.innerHTML = "";

    // Desmarco los campos marcados con errores
    for(i in Validator.highlightedErrors) {
        Validator.highlightedErrors[i].style.background = "";
        Validator.highlightedErrors[i].style.border = "";
    }
}




/* ########################## *
 * ##  VALIDATION METHODS  ## *
 * ########################## */

Validator.prototype.validateRequired = function Validator_validateRequired(itemName, errorMsg, genericMsg)
{
    var item = this.getItem( itemName );
    if(this.isNOE(item))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" es obligatorio.\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateRequiredGroupCheck = function validateRequiredGroupCheck(itemName, errorMsg, idCampo2, genericMsg)
{
    var item = this.getItem( itemName );
    if((item.checked == false) && (this.formObj[idCampo2].checked == false))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+", debe seleccionar uno obligatoriamente.\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateRequiredGroupField = function validateRequiredGroupField(itemNames, errorMsg, genericMsg)
{
    var item = null;
    var atLeastOne = false;
    for(i in itemNames) {
        var item = this.getItem( itemNames[i] );
        if (!this.isNOE(item)) atLeastOne = true;
    }

    if(!atLeastOne)
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" debe completar alguno obligatoriamente.\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateEmpty = function validateEmpty(itemName, errorMsg, genericMsg)
{
    var item = this.getItem( itemName );
    if(!this.isNOE(item))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" no debe completarse.\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateEquals = function validateEquals(itemName, errorMsg, value, genericMsg)
{
    var item = this.getItem( itemName );
    if(item.value != value)
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" debe ser igual a "+value+".\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateNotEquals = function validateNotEquals(itemName, errorMsg, value, genericMsg)
{
    var item = this.getItem( itemName );
    if(item.value == value)
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" no puede ser "+value+".\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateLength = function validateLength(itemName, errorMsg, value, genericMsg)
{
    var item = this.getItem( itemName );
    if(!this.isNumber(value))
    {
        alert("ERROR: El parámetro de validateLength para "+item.name+" no es un número.");
        return false;
    }
    if(item.value.length < parseInt(value))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" debe tener exactamente "+value+" caracteres.\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateMinLength = function validateMinLength(itemName, errorMsg, value, genericMsg)
{
    var item = this.getItem( itemName );
    if(!this.isNumber(value))
    {
        alert("ERROR: El parámetro de validateMinLength para "+item.name+" no es un número.");
        return false;
    }
    if(item.value.length < parseInt(value))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" no puede tener menos de "+value+" caracteres.\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateMaxLength = function validateMaxLength(itemName, errorMsg, value, genericMsg)
{
    var item = this.getItem( itemName );
    if(!this.isNumber(value))
    {
        alert("ERROR: El parámetro de validateMaxLength para "+item.name+" no es un número.");
        return false;
    }
    if(item.value.length > parseInt(value))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" no puede tener más de "+value+" caracteres.\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateIntRange = function validateIntRange(itemName, errorMsg, loValue, hiValue, genericMsg)
{
    var item = this.getItem( itemName );
    if(!this.isNumber(loValue) || !this.isNumber(hiValue))
    {
        alert("ERROR: El parámetro de validateIntRange para "+item.name+" no es un número.");
        return false;
    }
    if(!this.isNumber(item.value))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" no es un valor numérico.\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    if(item.value > parseInt(hiValue) || item.value < parseInt(loValue))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" debe estar en el rango "+loValue+ " - "+hiValue+".\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateIntEnteroRange = function validateIntEnteroRange(itemName, errorMsg, loValue, hiValue, genericMsg)
{
    var item = this.getItem( itemName );
    if(!this.isNumber(loValue) || !this.isNumber(hiValue))
    {
        alert("ERROR: El parámetro de validateIntRange para "+item.name+" no es un número.");
        return false;
    }
    if(item.value != "" && !this.isNumber(item.value))
    {
        if( !this.isNumberFloat(item.value) ) {

            if(genericMsg)
                this.addValidationResult(item, errorMsg+" no es un valor numérico.\n");
            else
                this.addValidationResult(item, errorMsg+".\n");
        } else {

            if(genericMsg)
                this.addValidationResult(item, errorMsg+" no es un valor numérico entero.\n");
            else
                this.addValidationResult(item, errorMsg+".\n");
        }

        return false;
    }
    if(item.value > parseInt(hiValue) || item.value < parseInt(loValue))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" debe estar en el rango "+loValue+ " - "+hiValue+".\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateFloatRangeLimitedFloat = function validateFloatRange(itemName, errorMsg, loValue, hiValue, numDigEnteros, numDigDecimales, genericMsg)
{
    var item = this.getItem( itemName );
    if(!this.isNumber(loValue) || !this.isNumber(hiValue))
    {
        alert("ERROR: El parámetro de validateFloatRange para "+item.name+" no es un número.");
        return false;
    }
    if(item.value != "" && !this.isNumberFloat(item.value))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" no es un valor numérico.\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    if(item.value > parseFloat(hiValue) || item.value < parseFloat(loValue))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" debe estar en el rango "+loValue+ " - "+hiValue+".\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }

    //me fijo que sea un float
    var value = item.value;
    var hayPunto = false;
    for(var it = 0; it < value.length; ++it)
    {
        var c = value.charAt(it);
        if(!(this.isDigit(c)))
        {
            if(!( c == '.' ) || (hayPunto))
            {
                if(genericMsg)
                    this.addValidationResult(item, errorMsg+" debe ser numérico.\n");
                else
                    this.addValidationResult(item, errorMsg+".\n");

                return false;
            }
            else
            {
                  hayPunto = true;
            }
        }
    }

    //ahora la presicion
    var pointPos = value.indexOf(".");
    var digDecimales = 0;
    var digEnteros = 0;

    if(pointPos != -1)
    {
        digDecimales = (value.length - pointPos - 1);
        digEnteros = (value.length - digDecimales -1)
    }
    else
    {
        digEnteros = value.length;
    }

    if(numDigEnteros < digEnteros)
    {
        if(genericMsg)
               this.addValidationResult(item, errorMsg+" debe tener menos de "+numDigEnteros+" Digitos enteros\n");
        else
               this.addValidationResult(item, errorMsg+".\n");

           return false;
    }

    if(numDigDecimales < digDecimales)
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" fue ingresado con una presición erronea. La cantidad de decimales debe ser "+numDigDecimales+"\n");
        else
               this.addValidationResult(item, errorMsg+".\n");

           return false;
    }
    return true;
};

Validator.prototype.validateNumeric = function validateNumeric(itemName, errorMsg, genericMsg)
{
    var item = this.getItem( itemName );
    var value = item.value;
    for(var it = 0; it < value.length; ++it)
    {
        var c = value.charAt(it);
        if(!(this.isDigit(c)))
        {
            if(genericMsg)
                this.addValidationResult(item, errorMsg+" debe ser numérico.\n");
            else
                this.addValidationResult(item, errorMsg+".\n");

            return false;
        }
    }
    return true;
};

Validator.prototype.validateNumericOfLength = function validateNumericOfLength(itemName, errorMsg, valor, genericMsg)
{
    var item = this.getItem( itemName );
    if(!this.isNumber(valor))
    {
        alert("ERROR: El parámetro de validateNumericOfLength para "+item.name+" no es un número.");
        return false;
    }
    var value = item.value;
    for(var it = 0; it < value.length; ++it)
    {
        var c = value.charAt(it);
        if(!(this.isDigit(c)))
        {
            if(genericMsg)
                this.addValidationResult(item, errorMsg+" debe ser numérico.\n");
            else
                this.addValidationResult(item, errorMsg+".\n");

            return false;
        }
    }
    if(item.value.length != parseInt(valor))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" debe ser exactamente de "+valor+" digitos.\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateFloat = function validateFloat(itemName, errorMsg, genericMsg)
{
    var item = this.getItem( itemName );
    var value = item.value;
    var hayPunto = false;

    for(var it = 0; it < value.length; ++it)
    {
        var c = value.charAt(it);
        if(!(this.isDigit(c)))
        {
            if(!( c == '.' ) || (hayPunto) )
            {
                if(genericMsg)
                    this.addValidationResult(item, errorMsg+" debe ser numérico.\n");
                else
                    this.addValidationResult(item, errorMsg+".\n");

                return false;
            }
            else
            {
                  hayPunto = true;
            }
        }
    }
    return true;
};

Validator.prototype.validateLimitedFloat = function validateLimitedFloat(itemName, errorMsg, numDigEnteros, numDigDecimales, genericMsg)
{
    var item = this.getItem( itemName );
    //me fijo que sea un float
    var value = item.value;
    var hayPunto = false;
    for(var it = 0; it < value.length; ++it)
    {
        var c = value.charAt(it);
        if(!(this.isDigit(c)))
        {
            if(!( c == '.' ) || (hayPunto))
            {
                if(genericMsg)
                    this.addValidationResult(item, errorMsg+" debe ser numérico.\n");
                else
                    this.addValidationResult(item, errorMsg+".\n");

                return false;
            }
            else
            {
                  hayPunto = true;
            }
        }
    }

    //ahora la presicion
    var pointPos = value.indexOf(".");
    var digDecimales = 0;
    var digEnteros = 0;

    if(pointPos != -1)
    {
        digDecimales = (value.length - pointPos - 1);
        digEnteros = (value.length - digDecimales -1)
    }
    else
    {
        digEnteros = value.length;
    }

    if(numDigEnteros < digEnteros)
    {
        if(genericMsg)
               this.addValidationResult(item, errorMsg+" debe tener menos de "+numDigEnteros+" Digitos enteros\n");
        else
               this.addValidationResult(item, errorMsg+".\n");

           return false;
    }

    if(numDigDecimales < digDecimales)
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" fue ingresado con una presición erronea. La cantidad de decimales debe ser "+numDigDecimales+"\n");
        else
               this.addValidationResult(item, errorMsg+".\n");

           return false;
    }
    return true;
};

Validator.prototype.validateAlphabetic = function validateAlphabetic(itemName, errorMsg, genericMsg)
{
    var item = this.getItem( itemName );
    var value = item.value;
    for(var it = 0; it < value.length; ++it)
    {
        var c = value.charAt(it);
        if(!(this.isLetter(c)))
        {
            if(genericMsg)
                this.addValidationResult(item, errorMsg+" debe esta compuesto por letras.\n");
            else
                this.addValidationResult(item, errorMsg+".\n");

            return false;
        }
    }
    return true;
};

Validator.prototype.validateAlphaNumeric = function validateAlphaNumeric(itemName, errorMsg, genericMsg)
{
    var item = this.getItem( itemName );
    var value = item.value;
    for(var it = 0; it < value.length; ++it)
    {
        var c = value.charAt(it);
        if(!(this.isLetter(c) || this.isDigit(c)))
        {
            if(genericMsg)
                this.addValidationResult(item, errorMsg+" debe ser alfanumérico.\n");
            else
                this.addValidationResult(item, errorMsg+".\n");

            return false;
        }
    }
    return true;
};

Validator.prototype.validateSmallerOrEqual = function validateSmallerOrEqual(itemName, errorMsg, value, genericMsg)
{
    var item = this.getItem( itemName );
    if(!this.isNumber(value))
    {
        alert("ERROR: El parámetro de validateSmallerOrEqual para "+item.name+" no es un número.");
        return false;
    }
    if(item.value > parseInt(value) && ((item.value != null) && (item.value != "")))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" no puede ser mayor a "+value+".\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateSmaller = function validateSmaller(itemName, errorMsg, value, genericMsg)
{
    var item = this.getItem( itemName );
    if(!this.isNumber(value))
    {
        alert("ERROR: El parámetro de validateSmaller para "+item.name+" no es un número.");
        return false;
    }
    if(item.value >= parseInt(value) && ((item.value != null) && (item.value != "")))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" no puede ser mayor o igual a "+value+".\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateGreaterOrEqual = function validateGreaterOrEqual(itemName, errorMsg, value, genericMsg)
{
    var item = this.getItem( itemName );

    if(!this.validateNumeric(itemName, errorMsg, genericMsg))
    {
        return false;
    }
    if(item.value < parseInt(value) && ((item.value != null) && (item.value != "")))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" no puede ser menor a "+value+".\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateGreater = function validateGreater(itemName, errorMsg, value, genericMsg)
{
    var item = this.getItem( itemName );
    if(!this.isNumber(value))
    {
        alert("ERROR: El parámetro de validateGreater para "+item.name+" no es un número.");
        return false;
    }
    if(item.value <= parseInt(value) && ((item.value != null) && (item.value != "")))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" no puede ser menor o igual a "+value+".\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};

Validator.prototype.validateRegExp = function validateRegExp(itemName, errorMsg, value, genericMsg)
{
    var item = this.getItem( itemName );
    var baseExpresion = new RegExp(value);
    var matches = baseExpresion.exec(item.value);
    if(((matches == null) || (matches[0] != item.value))&& ((item.value != null) && (item.value != "")))
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" esta malformado.\n");
        else
            this.addValidationResult(item, errorMsg+".\n");

        return false;
    }
    return true;
};


Validator.prototype.validateDateTime = function validateDateTime(itemName, errorMsg, genericMsg){
    
    //VALIDATE DATE
     var item = this.getItem( itemName );
    if(this.isNOE(item)){
        return false;
    }

    var fecha = item.value.substr(0, 10);

    var dia, mes, anio, sep1, sep2;
    var sep1 = fecha.indexOf("/");
    var sep2 = fecha.indexOf("/",sep1+1);
    var bisiesto;

    if((sep1==-1)||(sep2==-1))
    {
        this.addValidationResult(item, "La " + errorMsg + " no es válida: el formato correcto es dd/mm/aaaa.\n");
        return false;
    }
    if(sep1==0)
    {
        this.addValidationResult(item, "La " + errorMsg + " no es válida: falta el día.\n");
        return false;
    }
    if(!(sep2>(sep1+1)))
    {
        this.addValidationResult(item, "La " + errorMsg + " no es válida: falta el mes.\n");
        return false;
    }
    if(sep2==(fecha.length-1))
    {
        this.addValidationResult(item, "La " + errorMsg + " no es válida: falta el año.\n");
        return false;
    }

    dia = fecha.substring(0,sep1);
    mes = fecha.substring(sep1+1, sep2);
    anio = fecha.substring(sep2+1, fecha.length);

    if(!this.isNumber(dia))
    {
        this.addValidationResult(item, "El día de la " + errorMsg + " debe ser numérico: " + dia + ".\n");
        return false;
    }
    if((dia>31)||!(dia>=1))
    {
        this.addValidationResult(item, "El día de la " + errorMsg + " debe estar entre 1 y 31: "+ dia + ".\n");
        return false;
    }
    if(!this.isNumber(mes))
    {
        this.addValidationResult(item, "El mes de la " + errorMsg + " debe ser numérico: "+ mes + ".\n");
        return false;
    }
    if((mes>12)||!(mes>=1))
    {
        this.addValidationResult(item, "El mes de la " + errorMsg + " debe estar entre 1 y 12: "+ mes + ".\n");
        return false;
    }
    if(mes == 2)
    {
        //28 o 29 dias, segun anio bisiesto
        if((anio % 4) == 0)
        {
            if ((anio % 100) == 0 && (anio % 400) != 0)
            {
                //No bisiesto
                bisiesto = false;
            }
            else
            {
                //Bisiesto
                bisiesto = true;
            }
        }
        else
        {
            //No bisiesto
            bisiesto = false;
        }
        if (!bisiesto)
        {
            if (dia == 29)
            {
                this.addValidationResult(item, "El año " + anio + " de la " + errorMsg + " no es bisiesto.\n");
                return false;
            }
            else
            {
                if (dia > 29)
                {
                    this.addValidationResult(item, "Febrero en la " + errorMsg + " no tiene " + dia + " dias.\n");
                    return false;
                }
            }
        }
        else
        {
            if (dia > 29)
            {
                this.addValidationResult(item, "Febrero en la " + errorMsg + " no tiene " + dia + " días.\n");
                return false;
            }
        }
    }
    if((mes == 1) || (mes == 3) || (mes == 5) || (mes == 7) || (mes == 8) || (mes == 10) || (mes == 12))
    {
        //31 dias
        if (dia > 31)
        {
            this.addValidationResult(item, "El mes introducido en la " + errorMsg + " no tiene " + dia + " días.\n");
            return false;
        }
    }
    if((mes == 4) || (mes == 6) || (mes == 9) || (mes == 11))
    {
        //30 dias
        if (dia > 30)
        {
            this.addValidationResult(item, "El mes introducido en la " + errorMsg + " no tiene " + dia + " días.\n");
            return false;
        }
    }
    if(!this.isNumber(anio))
    {
        this.addValidationResult(item, "El año de la " + errorMsg + " debe ser numérico: "+ anio + ".\n");
        return false;
    }
    if(anio < 1900)
    {
        this.addValidationResult(item, "El año de la " + errorMsg + " debe ser mayor a 1900.\n");
        return false;
    }
    
    //VALIDATE TIME
    var hora = item.value.substr(11,5);
    
    var ind1;
    var h,m,hh,mm;

    ind = hora.indexOf(":");
    h = hora.substring(0, ind);
    m = hora.substring(ind+1, hora.length);
    hh = Number(h);
    mm = Number(m);

    if((h.length!=2)||(m.length!=2))
    {
        this.addValidationResult(item, "El formato de la " + errorMsg + " es incorrecto.\n");
        return false;
    }
    if((!this.isNumber(hh))||(!this.isNumber(mm)))
    {
        this.addValidationResult(item, "El formato de la " + errorMsg + " es incorrecto.\n");
        return false;
    }
    if((hh < 0)||(hh > 23))
    {
        this.addValidationResult(item, "El formato de la " + errorMsg + " es incorrecto.\n");
        return false;
    }
    if((mm < 0)||(mm > 59))
    {
        this.addValidationResult(item, "El formato de la " + errorMsg + " es incorrecto.\n");
        return false;
    }
    return true;
    
}

Validator.prototype.validateDate = function validateDate(itemName, errorMsg, genericMsg)
{
    var item = this.getItem( itemName );
    if(this.isNOE(item)){
        return false;
    }

    var fecha = item.value;

    var dia, mes, anio, sep1, sep2;
    var sep1 = fecha.indexOf("/");
    var sep2 = fecha.indexOf("/",sep1+1);
    var bisiesto;

    if((sep1==-1)||(sep2==-1))
    {
        this.addValidationResult(item, "La " + errorMsg + " no es válida: el formato correcto es dd/mm/aaaa.\n");
        return false;
    }
    if(sep1==0)
    {
        this.addValidationResult(item, "La " + errorMsg + " no es válida: falta el día.\n");
        return false;
    }
    if(!(sep2>(sep1+1)))
    {
        this.addValidationResult(item, "La " + errorMsg + " no es válida: falta el mes.\n");
        return false;
    }
    if(sep2==(fecha.length-1))
    {
        this.addValidationResult(item, "La " + errorMsg + " no es válida: falta el año.\n");
        return false;
    }

    dia = fecha.substring(0,sep1);
    mes = fecha.substring(sep1+1, sep2);
    anio = fecha.substring(sep2+1, fecha.length);

    if(!this.isNumber(dia))
    {
        this.addValidationResult(item, "El día de la " + errorMsg + " debe ser numérico: " + dia + ".\n");
        return false;
    }
    if((dia>31)||!(dia>=1))
    {
        this.addValidationResult(item, "El día de la " + errorMsg + " debe estar entre 1 y 31: "+ dia + ".\n");
        return false;
    }
    if(!this.isNumber(mes))
    {
        this.addValidationResult(item, "El mes de la " + errorMsg + " debe ser numérico: "+ mes + ".\n");
        return false;
    }
    if((mes>12)||!(mes>=1))
    {
        this.addValidationResult(item, "El mes de la " + errorMsg + " debe estar entre 1 y 12: "+ mes + ".\n");
        return false;
    }
    if(mes == 2)
    {
        //28 o 29 dias, segun anio bisiesto
        if((anio % 4) == 0)
        {
            if ((anio % 100) == 0 && (anio % 400) != 0)
            {
                //No bisiesto
                bisiesto = false;
            }
            else
            {
                //Bisiesto
                bisiesto = true;
            }
        }
        else
        {
            //No bisiesto
            bisiesto = false;
        }
        if (!bisiesto)
        {
            if (dia == 29)
            {
                this.addValidationResult(item, "El año " + anio + " de la " + errorMsg + " no es bisiesto.\n");
                return false;
            }
            else
            {
                if (dia > 29)
                {
                    this.addValidationResult(item, "Febrero en la " + errorMsg + " no tiene " + dia + " dias.\n");
                    return false;
                }
            }
        }
        else
        {
            if (dia > 29)
            {
                this.addValidationResult(item, "Febrero en la " + errorMsg + " no tiene " + dia + " días.\n");
                return false;
            }
        }
    }
    if((mes == 1) || (mes == 3) || (mes == 5) || (mes == 7) || (mes == 8) || (mes == 10) || (mes == 12))
    {
        //31 dias
        if (dia > 31)
        {
            this.addValidationResult(item, "El mes introducido en la " + errorMsg + " no tiene " + dia + " días.\n");
            return false;
        }
    }
    if((mes == 4) || (mes == 6) || (mes == 9) || (mes == 11))
    {
        //30 dias
        if (dia > 30)
        {
            this.addValidationResult(item, "El mes introducido en la " + errorMsg + " no tiene " + dia + " días.\n");
            return false;
        }
    }
    if(!this.isNumber(anio))
    {
        this.addValidationResult(item, "El año de la " + errorMsg + " debe ser numérico: "+ anio + ".\n");
        return false;
    }
    if(anio < 1900)
    {
        this.addValidationResult(item, "El año de la " + errorMsg + " debe ser mayor a 1900.\n");
        return false;
    }
    return true;
};

Validator.prototype.validateDateSmaller = function validateDateSmaller(itemName1, errorMsg1, itemName2, errorMsg2)
{
    var item1 = this.getItem( itemName1 );
    var item2 = this.getItem( itemName2 );
    if(!this.validateDate(item1, errorMsg1, true)){
        return false;
    }
    if(!this.validateDate(item2, errorMsg2, true)){
        return false;
    }

    var fecha1 = item1.value;
    var fecha2 = item2.value;

    if(this.compararFechas(fecha1, fecha2) >= 0){
        this.addValidationResult(item1, "La " + errorMsg1 + " debe ser menor a la " + errorMsg2 + ".\n");
        return false;
    }
    return true;
};

Validator.prototype.validateDateSmallerOrEqual = function validateDateSmallerOrEqual(itemName1, errorMsg1, itemName2, errorMsg2)
{
    var item1 = this.getItem( itemName1 );
    var item2 = this.getItem( itemName2 );
    if(!this.validateDate(item1, errorMsg1, true)){
        return false;
    }
    if(!this.validateDate(item2, errorMsg2, true)){
        return false;
    }

    var fecha1 = item1.value;
	
    var fecha2 = item2.value;

    if(this.compararFechas(fecha1, fecha2) > 0){
        this.addValidationResult(item1, "La " + errorMsg1 + " debe ser menor o igual a la " + errorMsg2 + ".\n");
        return false;
    }
    return true;
};



Validator.prototype.validateDateInterval = function validateDateInterval(itemName1, errorMsg1, itemName2, errorMsg2,dias)
{
    var item1 = this.getItem( itemName1 );
    var item2 = this.getItem( itemName2 );
    if(!this.validateDate(item1, errorMsg1, true)){
        return false;
    }
    if(!this.validateDate(item2, errorMsg2, true)){
        return false;
    }

    var fechaInicial = item1.value;
	
    var fechaFinal = item2.value;

 
		var resultado="";
		
			inicial=fechaInicial.split("/");
			final=fechaFinal.split("/");
			// obtenemos las fechas en milisegundos
			var dateStart=new Date(inicial[2],(inicial[1]-1),inicial[0]);
            var dateEnd=new Date(final[2],(final[1]-1),final[0]);
            if(dateStart<dateEnd)
            {
				// la diferencia entre las dos fechas, la dividimos entre 86400 segundos
				// que tiene un dia, y posteriormente entre 1000 ya que estamos
				// trabajando con milisegundos.
				
				if((((dateEnd-dateStart)/86400)/1000) >dias){
					 this.addValidationResult(item1, "La diferencia de dias entre las 2 fechas no puede ser mayor a " + dias +".\n");
					return false;
				}else
					return true;
				
			}else{
				
				 this.addValidationResult(item1, "La " + errorMsg1 + " es posterior a " + errorMsg2 + ".\n");
			
				return false;
			}
		
	
    
};


Validator.prototype.validateDateGreater = function validateDateGreater(itemName1, errorMsg1, itemName2, errorMsg2)
{
    var item1 = this.getItem( itemName1 );
    var item2 = this.getItem( itemName2 );
    if(!this.validateDate(item1, errorMsg1, true)){
        return false;
    }
    if(!this.validateDate(item2, errorMsg2, true)){
        return false;
    }

    var fecha1 = item1.value;
    var fecha2 = item2.value;

    if(this.compararFechas(fecha1, fecha2) <= 0){
        this.addValidationResult(item1, "La " + errorMsg1 + " debe ser mayor a la " + errorMsg2 + ".\n");
        return false;
    }
    return true;
};

Validator.prototype.validateDateGreaterOrEqual = function validateDateGreaterOrEqual(itemName1, errorMsg1, itemName2, errorMsg2)
{
    var item1 = this.getItem( itemName1 );
    var item2 = this.getItem( itemName2 );
    if(!this.validateDate(item1, errorMsg1, true)){
        return false;
    }
    if(!this.validateDate(item2, errorMsg2, true)){
        return false;
    }

    var fecha1 = item1.value;
    var fecha2 = item2.value;

    if(this.compararFechas(fecha1, fecha2) < 0)
    {
        this.addValidationResult(item1, "La " + errorMsg1 + " debe ser mayor o igual a la " + errorMsg2 + ".\n");
        return false;
    }
    return true;
};

Validator.prototype.validateDateSmallerToday = function validateDateSmallerToday(itemName, errorMsg, genericMsg)
{
    var item = this.getItem( itemName );
    if(!this.validateDate(item, errorMsg, true)){
        return false;
    }
    var hoy = this.hiddenConFechaDeHoy();
    return this.validateDateSmaller(item, errorMsg, hoy, "fecha de hoy");
};

Validator.prototype.validateDateSmallerOrEqualToday = function validateDateSmallerOrEqualToday(itemName, errorMsg, genericMsg)
{
    var item = this.getItem( itemName );
    if(!this.validateDate(item, errorMsg, true)){
        return false;
    }

    var hoy = this.hiddenConFechaDeHoy();
    return this.validateDateSmallerOrEqual(item, errorMsg, hoy, "fecha de hoy");
};

Validator.prototype.validateDateGreaterToday = function validateDateGreaterToday(itemName, errorMsg, genericMsg)
{
    var item = this.getItem( itemName );
    if (!this.validateDate(item, errorMsg, true))
        return false;

    var hoy = this.hiddenConFechaDeHoy();
    return this.validateDateGreater(item, errorMsg, hoy, "fecha de hoy");
};

Validator.prototype.validateDateGreaterOrEqualToday = function validateDateGreaterOrEqualToday(itemName, errorMsg, genericMsg)
{
    var item = this.getItem( itemName );
    if(!this.validateDate(item, errorMsg, true))
        return false;

    var hoy = this.hiddenConFechaDeHoy();
    return this.validateDateGreaterOrEqual(item, errorMsg, hoy, "fecha de hoy");
};

Validator.prototype.validateDatePeriod = function validateDatePeriod(itemName1, errorMsg1, itemName2, errorMsg2)
{
    var item1 = this.getItem( itemName1 );
    var item2 = this.getItem( itemName2 );
    if(!this.validateDate(item1, errorMsg1, true)){
        return false;
    }
    if(!this.validateDate(item2, errorMsg2, true)){
        return false;
    }

    return this.validateDateSmallerOrEqual(item1, errorMsg1, item2, errorMsg2);
};

Validator.prototype.validateTime = function validateTime(itemName, errorMsg, genericMsg)
{
    var item = this.getItem( itemName );
    var hora = item.value;
    var ind1;
    var h,m,hh,mm;

    ind = hora.indexOf(":");
    h = hora.substring(0, ind);
    m = hora.substring(ind+1, hora.length);
    hh = Number(h);
    mm = Number(m);

    if((h.length!=2)||(m.length!=2))
    {
        this.addValidationResult(item, "El formato de la " + errorMsg + " es incorrecto.\n");
        return false;
    }
    if((!this.isNumber(hh))||(!this.isNumber(mm)))
    {
        this.addValidationResult(item, "El formato de la " + errorMsg + " es incorrecto.\n");
        return false;
    }
    if((hh < 0)||(hh > 23))
    {
        this.addValidationResult(item, "El formato de la " + errorMsg + " es incorrecto.\n");
        return false;
    }
    if((mm < 0)||(mm > 59))
    {
        this.addValidationResult(item, "El formato de la " + errorMsg + " es incorrecto.\n");
        return false;
    }
    return true;
};

Validator.prototype.validateTimePeriod = function validateTimePeriod(itemName, errorMsg, itemName2, genericMsg)
{
    var item = this.getItem( itemName );
    var hora1 = item.value;
    var hora2 = this.formObj[itemName2].value;

    if(this.compararHoras(hora1, hora2) == 1)
    {
        this.addValidationResult(item, "La " + errorMsg + " es mayor que la " + genericMsg + ".\n");
        return false;
    }
    return true;
};

Validator.prototype.validateEmail = function validateEmail(itemName, errorMsg, genericMsg)
{
    var item = this.getItem( itemName );
    var email = item.value;
    if(!this.checkEmail(email))
    {
        this.addValidationResult(item, errorMsg/* + " Dirección de correo electrónica inválida*/ + "\n");
        return false;
    }
    return true;
};

Validator.prototype.validateDay = function validateDay(itemName, errorMsg, genericMsg)
{
    if(!this.validateNumeric(itemName, errorMsg, true)) {
        return false;
    }
    if(!this.validateSmallerOrEqual(itemName, errorMsg, 31, "31")){
        return false;
    }
    if(!this.validateGreaterOrEqual(itemName, errorMsg, 1, "1")){
        return false;
    }
    return true;
};

Validator.prototype.validateMonth = function validateMonth(itemName, errorMsg, genericMsg)
{
    if(!this.validateNumeric(itemName, errorMsg, true)) {
        return false;
    }
    if(!this.validateSmallerOrEqual(itemName, errorMsg, 12, "12")){
        return false;
    }
    if(!this.validateGreaterOrEqual(itemName, errorMsg, 1, "1")){
        return false;
    }
    return true;
};

Validator.prototype.validateYear = function validateYear(itemName, errorMsg, genericMsg)
{
    if(!this.validateNumeric(itemName, errorMsg, true)) {
        return false;
    }
    if(!this.validateGreaterOrEqual(itemName, errorMsg, 1900, "1900")){
        return false;
    }
    var fecha = new Date();
    if(!this.validateSmallerOrEqual(itemName, errorMsg, fecha.getFullYear(), "año actual")){
        return false;
    }
    return true;
};

Validator.prototype.validateYearSmallerOrEqual = function validateYearSmallerOrEqual(itemName1, errorMsg1, itemName2, errorMsg2)
{
    if(!this.validateYear(itemName1, errorMsg1, true)){
        return false;
    }
    if(!this.validateYear(itemName2, errorMsg2, true)){
        return false;
    }

    var item1 = this.getItem( itemName1 );
    var item2 = this.getItem( itemName2 );

    var fecha1 = item1.value;
    var fecha2 = item2.value;

    if(fecha1 > fecha2){
        this.addValidationResult(item1, errorMsg1 + " debe ser menor o igual a " + errorMsg2 + ".\n");
        return false;
    }
    return true;
};

Validator.prototype.validateCUIL = function validateCUIL(itemName, errorMsg, genericMsg)
{
    var item = this.getItem( itemName );

    if(this.isNOE(item)) {
        return false;
        //The field is empty or not exists. For requiered use validateRequired.
        //No message error is added.
    }

    var baseExpresion = new RegExp("^[0-9]{2}[0-9]{1,8}?[0-9]$");
    var matches = baseExpresion.test(item.value);

    if(matches) {
        var resultado = this.validarCUILSinGuion(item.value);
    } else {
        resultado = false;
    }

    if(!resultado)
    {
        if(genericMsg)
            this.addValidationResult(item, errorMsg+" no es válido (Deben ser 11 dígitos sin guiones).\n");
        else
            this.addValidationResult(item, errorMsg+".\n");
    }

    return resultado;
};

Validator.prototype.validateCUILConGuion = function validateCUILConGuion(itemName, errorMsg, genericMsg)
{
    var item = this.getItem( itemName );

    if(this.isNOE(item)) {
        return false;
        //The field is empty or not exists. For requiered use validateRequired.
        //No message error is added.
    }

    var baseExpresion = new RegExp("^[0-9]{2}\-[0-9]{1,8}?\-[0-9]$");
    var matches = baseExpresion.test(item.value);

    if(matches) {
        var resultado = this.validarCUILSinGuion(item.value.replace(/-/g,''));
    } else {
        resultado = false;
    }

    if(!resultado)
    {
        if(genericMsg)
            this.addValidationResult(item, "El "+errorMsg+" ingresado posee un formato no válido (Debe ser xx-xxxxxxxx-x).\n");
        else
            this.addValidationResult(item, errorMsg+".\n");
    }

    return resultado;
};

Validator.prototype.validateComboNotEqual = function validateComboNotEqual(itemName, errorMsg, value, genericMsg)
{
    var item = this.getItem( itemName );
    if( item.options == undefined ) {
        return false;
    }

    if(item.options[ item.selectedIndex ].value == value) {
        if(genericMsg) {
            this.addValidationResult(item, errorMsg+" no puede ser "+value+".\n");
        } else {
            this.addValidationResult(item, errorMsg+".\n");
        }
        return false;
    }
    return true;
}



/* ####################### *
 * ##  PRIVATE METHODS  ## *
 * ####################### */

Validator.prototype.addValidationResult = function (itemName, message) {
    this.errorsContainer += message;
    this.errorsArray[ this.errorsArray.length ] = {element: itemName, message: message};
};

Validator.prototype.getItem = function ( itemName ) {
    if( itemName == undefined ) {
        alert( "Validator.prototype.getItem: el nombre del item no puede ser null" );
        return;
    }

    if( itemName.value != undefined ) {
        return itemName;
    }

    var item = undefined;
    item = document.getElementById( itemName );
    if( item == undefined ) {
        item = document.getElementsByName( itemName )[ 0 ];
        if( item == undefined ) {
            alert( "Validator.prototype.getItem: no existe ningun elemento con el id o nombre: " + itemName );
            return;
        }
    }
    return item;
}

Validator.prototype.isLetter = function (c)
{
    var lowercaseLetters = "abcdefghijklmnopqrstuvwxyzáéíóúñü";
    var uppercaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚÑÜ";
    return ((uppercaseLetters.indexOf(c) != -1) || (lowercaseLetters.indexOf(c) != -1))
};

Validator.prototype.isDigit = function (c)
{
    return ((c >= "0") && (c <= "9"))
};

Validator.prototype.isNumber = function (number)
{
    for(var it = 0; it < number.length; it++)
    {
        var c = number.charAt(it);
        if(!(this.isDigit(c)))
            return false;
    }
    return true;
};

Validator.prototype.isNumberFloat = function (number)
{
    return /^\d+(\.\d+)?$/.test( number );
};

Validator.prototype.trim = function (str)
{
    return str.replace(/^\s*|\s*$/g,"");
};

/**
 * Indica si un objeto en null, si su value es "" o si solo son espacion vacios.
 */
Validator.prototype.isNOE = function (elemento)
{
    return (elemento==null) || (this.trim(elemento.value)=="");
};

/**
 * Compara dos fechas e indica la relacion entre ambas
 * @param fecha1, fecha2: fechas a comparar en formato dd/mm/aaaa.
 * @return devuelve -1 si fecha1 es menor fecha2, 1 si fecha1 es mayor que fecha2 y 0 si son iguales
 */
Validator.prototype.compararFechas = function (fecha1,fecha2)
{
    //asumo que los datos se validaron de antes
    var ind11,ind12,ind21,ind22;
    var dd1,mm1,yy1,dd2,mm2,yy2;

    ind11 = fecha1.indexOf("/");
    ind12 = fecha1.indexOf("/", ind11 + 1);

    dd1 = Number(fecha1.substring(0,ind11));
    mm1 = Number(fecha1.substring(ind11 + 1, ind12));
    yy1 = Number(fecha1.substring(ind12+1,fecha1.length));

    ind21 = fecha2.indexOf("/");
    ind22 = fecha2.indexOf("/", ind21 + 1);

    dd2 = Number(fecha2.substring(0,ind21));
    mm2 = Number(fecha2.substring(ind21 + 1, ind22));
    yy2 = Number(fecha2.substring(ind22 + 1, fecha2.length));

    if(yy1 < yy2)
        return -1;
    else if(yy1 > yy2)
        return 1;
    else
    {
        if(mm1 < mm2)
            return -1;
        else if(mm1 > mm2)
            return 1;
        else
        {
            if(dd1 < dd2)
                return -1;
            else if(dd1 > dd2)
                return 1;
            else
                return 0;
        }
    }
};

/**
 * Compara dos horas y determina la relación entre ambas.
 * @param hora1 hora en formato hh:mm
 * @param hora2 hora en formato hh:mm
 * @return devuelve 0 si son iguales, -1 si hora2>hora1 y 1 si hora1>hora2
 */
Validator.prototype.compararHoras = function (hora1,hora2)
{
    var ind1,ind2;
    var h1,m1,h2,m2,hm1,hm2;

    ind1 = hora1.indexOf(":");
    ind2 = hora2.indexOf(":");

    h1 = hora1.substring(0,ind1);
    m1 = hora1.substring(ind1 + 1,hora1.length);
    hm1 = Number(h1 + m1);

    h2 = hora2.substring(0,ind2);
    m2 = hora2.substring(ind2 + 1,hora2.length);
    hm2 = Number(h2 + m2);

    if(hm1 == hm2)
        return 0;
    else if(hm1 < hm2)
        return -1;
    else
        return 1;
};

Validator.prototype.validarCUILSinGuion = function (cuit)
{
    if(cuit == null || cuit == "" || cuit.length <= 3) {
        return false;
    }

    var xyStr;
    var dniStr;
    var digitoStr;
    var digitoTmp;
    var n = cuit.length;

    xyStr = cuit.substring(0,2);
    dniStr = cuit.substring(2,n-1);
    digitoStr = cuit.substring(n-1);

    if (xyStr.length != 2 || dniStr.length > 8 || digitoStr.length != 1) {
        return false;
    }

    var xyStc;
    var dniStc;

    try {
        xyStc = parseInt(xyStr, 10);
        dniStc = parseInt(dniStr, 10);
        digitoTmp = parseInt(digitoStr, 10);
    } catch (e) {
        return false;
    }

    if (xyStc != 20 && xyStc != 23 && xyStc != 24 && xyStc != 27 && xyStc != 30 && xyStc != 33 && xyStc != 34) {
        return false;
    }

    var digitoStc = this.calcularDigitoVerificador(xyStc, dniStc);

    if (digitoStc == digitoTmp && xyStc == parseInt(xyStr, 10)) {
        return true;
    }

    return false;
};

Validator.prototype.calcularDigitoVerificador = function (xyStc, dniStc)
{
    var tmp1;
    var tmp2;
    var acum = 0;
    var n = 2;

    tmp1 = xyStc * 100000000 + dniStc;

    for (var i = 0; i < 10; i++) {
        tmp2 = Math.floor(tmp1 / 10);
        acum += (tmp1 - tmp2 * 10) * n;
        tmp1 = tmp2;
        if (n < 7)
            n++;
        else
            n = 2;
    }

    n = Math.floor(11 - acum % 11);

    if (n == 10) {
        if (xyStc == 20 || xyStc == 27 || xyStc == 24)
            xyStc = 23;
        else
            xyStc = 33;

        /*
         * No es necesario hacer la llamada recursiva a calcular(), se puede
         * poner el digito en 9 si el prefijo original era 23 o 33 o poner
         * el dijito en 4 si el prefijo era 27
         */
        return this.calcularDigitoVerificador(xyStc, dniStc);
    } else {
        if (n == 11)
            return 0;
        else
            return n;
    }
};


Validator.prototype.checkEmail = function (emailStr)
{
    if (emailStr.length == 0)
        return true;

   var emailPat=/^(.+)@(.+)$/;
   var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
   var validChars="\[^\\s" + specialChars + "\]";
   var quotedUser="(\"[^\"]*\")";
   var ipDomainPat=/^(\d{1,3})[.](\d{1,3})[.](\d{1,3})[.](\d{1,3})$/;
   var atom=validChars + '+';
   var word="(" + atom + "|" + quotedUser + ")";
   var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
   var domainPat=new RegExp("^" + atom + "(\\." + atom + ")*$");
   var matchArray=emailStr.match(emailPat);

   if (matchArray == null)
       return false;

   var user=matchArray[1];
   var domain=matchArray[2];

   if (user.match(userPat) == null)
       return false;

   var IPArray = domain.match(ipDomainPat);
   if(IPArray != null)
   {
       for (var i = 1; i <= 4; i++)
       {
          if (IPArray[i] > 255)
          {
             return false;
          }
       }
       return true;
   }

   var domainArray=domain.match(domainPat);

   if(domainArray == null)
       return false;

   var atomPat=new RegExp(atom,"g");
   var domArr=domain.match(atomPat);
   var len=domArr.length;

   if((domArr[domArr.length-1].length < 2) || (domArr[domArr.length-1].length > 3))
       return false;

   if (len < 2)
       return false;

   return true;
};

Validator.prototype.hiddenConFechaDeHoy = function (){
    var input = document.createElement('input');
    var fecha = new Date();
    input.type= 'hidden';
    input.value = fecha.getDate() + '/' + (fecha.getMonth() + 1) + '/' +  fecha.getFullYear();
    return input;
};
