<?php
App::uses('AppModel', 'Model');
/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class TipoComprobantePersona extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'tipo_comprobante_persona';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
 /*
 public $belongsTo = array(
        'TipoElemento' => array(
            'className' => 'TipoElemento',
            'foreignKey' => 'id_tipo_elemento',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'Agente' => array(
            'className' => 'Agente',
            'foreignKey' => 'id_agente',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
 );
 
 */
 
/**
 * hasMany associations
 *
 * @var array
 */
	/*public $hasMany = array(
		'Region' => array(
			'className' => 'Region',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'id_jurisdiccion',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public $childRecordMessage = "Existe por lo menos una regi&oacute;n, predio, establecimiento o usuario relacionado/a con la jurisdicci&oacute;n.";
    
    */
    
      public $belongsTo = array(
        'Persona' => array(
            'className' => 'Persona',
            'foreignKey' => 'id_persona',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'TipoComprobante' => array(
            'className' => 'TipoComprobante',
            'foreignKey' => 'id_tipo_comprobante',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
        );
      
      
      
      public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
      	
      	
      	
      	$this->virtual_fields_view = array(
      			
      			"codigo_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
      			"razon_social"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
      			"codigo_persona"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
      		
      	);
      	
      }
      
      
      
}
