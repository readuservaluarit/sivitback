<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
        <META HTTP-EQUIV="Expires" CONTENT="-1">
        <title><?php echo Configure::read("appTitle"); ?></title>
        <meta name="description" content="">
        <meta name="author" content="">

        <?php

            //Favicon
            echo $this->Html->meta('favicon','/favicon.ico',array('type' => 'icon'));


            
          

          

           


            /*Validator*/
            echo $this->Html->script('validator.js');
            
          
   

        

        ?>
      
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


  <script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>

           
<?php 


 /*JqueryUI*/
            echo $this->Html->script('mylibs/jquery-ui-1.9.2.custom.min.js');
            echo $this->Html->css('jquery-ui-1.9.2.custom.css'); //jQuery UI, optional
            
            /*BlockUI*/
            echo $this->Html->script('mylibs/jquery.blockUI.js');
            
            
            echo $this->Html->script('mylibs/numericInput.min.js');
   
            ?>
    </head>

    <body >

        


       <div class="container-fluid">

                <?php echo $this->Session->flash(); ?>
                <?php echo $this->fetch('content'); ?>

</div>


      


        <div id="zk_proc" style="display: none;">
            <div class="z-loading" id="zk_proc-t">
                <div class="z-loading-indicator">
                    <span class="z-loading-icon"></span> Procesando...
                </div>
            </div>
        </div>
        
        
        <div id="alertSuccess" class="alert alert-success" style="display:none;position: absolute;top:55px;margin-left:10px;" align="center">
        </div>
        
        <div id="alertError" class="alert alert-danger" style="display:none;position: absolute;top:55px;margin-left:10px;" align="center">
        </div>



        <script type="text/javascript">
          

            function showProcessing(show){

                if(show)
                    $("#zk_proc").css("display","block"); 
                else
                    $("#zk_proc").css("display","none");

            }
            
            function showSuccess(msg){
                $('#alertSuccess').html(msg);
                $('#alertSuccess').fadeIn(1000, function() {
                // Animation complete
                     $('#alertSuccess').fadeOut(8000, function() {
                    // Animation complete
                    });
                });
            }
            
            function showError(msg){
                $('#alertError').html(msg);
                $('#alertError').fadeIn(1000, function() {
                // Animation complete
                     $('#alertError').fadeOut(8000, function() {
                    // Animation complete
                    });
                });
            }
            
        </script>

        <?php echo $this->Js->writeBuffer(); ?>

    </body>
</html>