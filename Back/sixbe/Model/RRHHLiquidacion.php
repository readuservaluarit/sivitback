<?php
App::uses('AppModel', 'Model');
App::uses('Legajo', 'Model');
App::uses('RRHHNovedadPersona', 'Model');
App::uses('TablaAuxiliar', 'Model');
App::uses('RRHHConcepto', 'Model');
App::uses('RRHHVariable', 'Model');
App::uses('RRHHLiquidacionPersona', 'Model');
App::uses('RRHHNovedadPersona', 'Model');
App::uses('RRHHClaseLiquidacion', 'Model');

/**
 * Jurisdiccion Model
 *
 * @property Region $Region
 * @property Usuario $Usuario
 */
class RRHHLiquidacion extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'rrhh_liquidacion';
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	
    
    public $belongsTo = array(
    		'TipoLiquidacion' => array(
    				'className' => 'TipoLiquidacion',
    				'foreignKey' => 'id_tipo_liquidacion',
    				'dependent' => false,
    				'conditions' => '',
    				'fields' => '',
    				
    		),
    		
    		'EstadoRRHHLiquidacion' => array(
    				'className' => 'EstadoRRHHLiquidacion',
    				'foreignKey' => 'id_estado_rrhh_liquidacion',
    				'dependent' => false,
    				'conditions' => '',
    				'fields' => '',
    				
    		),
    		'RRHHClaseLiquidacion' => array(
    				'className' => 'RRHHClaseLiquidacion',
    				'foreignKey' => 'id_rrhh_clase_liquidacion',
    				'dependent' => false,
    				'conditions' => '',
    				'fields' => '',
    				
    		)
    		
    		
    		
    );
    
    
    
    public $hasMany = array(
    		'RRHHLiquidacionPersona' => array(
    				'className' => 'RRHHLiquidacionPersona',
    				'foreignKey' => 'id_liquidacion',
    				'dependent' => false,
    				'fields' => '',
    				'order' => '',
    				'limit' => '',
    				'offset' => '',
    				'exclusive' => '',
    				'finderQuery' => '',
    				'counterQuery' => ''
    		));
    
    
    
    
    
    public function convertirVariable($id_rrhh_variable,$id_persona,$id_liquidacion){
    	
    	switch ($id_rrhh_variable){
    		
    		
    		case EnumRRHHVariable::SUELDOJORNAL:
    			
    			/*$variable -> $id_persona
    			 * 
    			
    			 * */
    			
    			
    			$persona = new Legajo();
    			
    			
    			$sueldo_jornal = $persona->find('first', array(
    					'conditions' => array('Legajo.id' => $id_persona),
    			'contain' =>array('RRHHCategoria')
    			));//busco los articulos integrantes de este Articulo
    			
    			
    			if($sueldo_jornal["Legajo"]["rrhh_basico"]>0)
    				return $sueldo_jornal["Legajo"]["rrhh_basico"];
    			else 
    				return $sueldo_jornal["RRHHCategoria"]["importe"];
    			
    			
    			break;
    			
    			
    			
    			
    		case EnumRRHHVariable::BASICOCONVENIO:
    			
    			/*$variable -> $id_persona
    			 *
    			 
    			 * */
    			
    			
    			$persona = new Legajo();
    			
    			
    			$sueldo_jornal = $persona->find('first', array(
    					'conditions' => array('Legajo.id' => $variable),
    					'contain' =>array('RRHHCategoria')
    			));//busco los articulos integrantes de este Articulo
    			
    			
    			
    			return $sueldo_jornal["RRHHCategoria"]["importe"];
    					
    					
    			break;
    		
    		
    		case EnumRRHHVariable::TOTALHABREMUNERATIVOS:
    			
    			/*$variable -> $id_persona
    			 *$id_liquidacion
    			 *$variable1 ->EnumRRHHTotalHaberRemunerativo
    			 *$variable2 ->fecha_desde
    			 *$variable3 ->fecha_hasta
    			 *$variable4->id_clase_liquidacion
    			 *$variable5
    			 
    			 * */
    			
    			
    			if($id_liquidacion >0){//cuando creo la formula tildo "Liquidaciones Anteriores"
    			
	    			$novedad_persona = new RRHHNovedadPersona();
	    			
	    			
	    			$total_concepto = $novedad_persona->find('first', array(
	    					'fields'=>array('SUM(RRHHNovedadPersona.importe_calculado) as valor'),
	    					'conditions' => array('RRHHNovedadPersona.id_persona' => $id_persona,'RRHHNovedadPersona.id_rrhh_liquidacion' => $id_liquidacion,'RRHHConcepto.id_rrhh_tipo_concepto'=>EnumRRHHTipoConcepto::Remunerativo),
	    					'contain' =>array("RRHHConcepto")
	    					
	    					
	    			));//busco los articulos integrantes de este Articulo
	    			
	    			/*
	    			$total_concepto = $novedad_persona->find('first', array(
	    					'fields'=>array('SUM(RRHHNovedadPersona.importe_calculado) as valor'),
	    					'conditions' => array('RRHHNovedadPersona.id_persona' => $id_persona,'RRHHNovedadPersona.id_rrhh_liquidacion' => $id_liquidacion),
	    					'contain' =>false,
	    					'joins' => array(
	    							array(
	    									'table' => 'rrhh_concepto',
	    									'alias' => 'RRHHConcepto',
	    									
	    									'foreignKey' => 'id_rrhh_concepto',
	    									'conditions'=> array(
	    											'RRHHNovedadPersona.id_rrhh_concepto = RRHHConcepto.id',
	    											'RRHHConcepto.id_rrhh_tipo_concepto'=>EnumRRHHTipoConcepto::Remunerativo
	    											
	    									)
	    							))
	    				
	    			));//busco los articulos integrantes de este Articulo
	    			
	    			*/
	    			
	    			return $total_concepto[0]["valor"];
	    			
    			}else{
    				
    				switch($variable1){
    					
    					
    					
    				}
    				
    				
    				
    			}
    			
    			break;
    			
    			
    			
    		case EnumRRHHVariable::TOTALHABNOREMUNERATIVOS:
    			
    			/*$variable -> $id_persona
    			 *  $id_liquidacion
    			 * variable1
    			 * variable2
    			 * variable3
    			 * variable4
    			 * variable5
    			 * 
    			 *
    			 
    			 * */
    			
    			
    			$novedad_persona = new RRHHNovedadPersona();
    			
    			
    			$total_concepto = $novedad_persona->find('first', array(
    					'fields'=>array('SUM(RRHHNovedadPersona.importe_calculado) as valor'),
    					'conditions' => array('RRHHNovedadPersona.id_persona' => $id_persona,'RRHHNovedadPersona.id_rrhh_liquidacion' => $id_liquidacion,'RRHHConcepto.id_rrhh_tipo_concepto'=>EnumRRHHTipoConcepto::NoRemunerativo),
    					'contain' =>array('RRHHConcepto')
    					
    			));//busco los articulos integrantes de este Articulo
    			
    			
    			
    			return $total_concepto[0]["valor"];
    			
    			
    			break;
    			
    			
    			
    			
    			
    			
    		case EnumRRHHVariable::ANTIGUEDAD:
    			
    			/*$variable -> $id_persona
    			 * $variable1 -> $id_liquidacion
    			 * $variable2 -> opcion1 variable formula
    			 *
    			 *
    			 *SELECT TIMESTAMPDIFF(YEAR, persona.fecha_antiguedad, rrhh_liquidacion.fecha_periodo)AS valorae FROM persona 
JOIN rrhh_liquidacion ON rrhh_liquidacion.id = <valor2>
WHERE persona.id = <valor1>
    			 * */
    			
    			if($variable3 == EnumRRHHAntiguedad::Anual){
    				
    				$novedad_persona = new Legajo();
    			
    			
    			$total_concepto = $novedad_persona->find('first', array(
    					'fields'=>array('TIMESTAMPDIFF(YEAR, Legajo.fecha_antiguedad, RRHHLiquidacion.fecha_periodo) as valor'),
    					'conditions' => array('RRHHNovedadPersona.id_persona' => $id_persona),
    					'joins' => array(
    							array(
    									'table' => 'rrhh_liquidacion',
    									'alias' => 'RRHHLiquidacion',
    									'type' => 'LEFT',
    									'conditions' => array(
    											'RRHHLiquidacion.id ='.$id_liquidacion
    									)
    									
    							)),
    					'contain' => array("RRHHLiquidacion")
    					
    			));
    									
    			
    			return $total_concepto[0][0]["valor"];
    			
    			}else{//fraccion 3 meses no se
    				
    				
    			}
    			
    		
    			
    			
    			break;
    			
    			
    			
    			
    		case EnumRRHHVariable::TABLAAUXILIAR:
    			
    			/*$variable= $id_persona
    			 * $variable1 = codigo_tabla_auxiliar
    			 * $variable2 = numero de columna //valor1, valor2,valor3,valor4,valor5
    	
    			 */
    			
    			$tabla_auxiliar_obj = new TablaAuxiliar();
    			$persona_obj = new Legajo();
    			
    			$tabla_auxiliar = $tabla_auxiliar_obj->find('first', array(
    					'conditions' => array('TablaAuxiliar.codigo' => $varable1),
    			'contain' =>array('TablaAuxiliarItem')
    			));
    			
    			
    			$persona = $persona_obj->find('first', array(
    					'conditions' => array('Legajo.id' => $id_persona),
    					'contain' =>false
    			));
    			
    			
    			$valor_campo_tabla_auxiliar = $persona["Legajo"][$tabla_auxiliar["TablaAuxiliar"]["tabla_padre_campo_relacional"]];
    			
    			
    			foreach ($tabla_auxiliar["TablaAuxiliarItem"] as $ta_item){
    				
    				if($ta_item["id_registro_tabla_auxiliar"] == $valor_campo_tabla_auxiliar)
    					return $ta_item[$variable2];//devuelvo el valor de la columna indicada
    				
    			}
    			
    			return 0;//sino encuentra nada devuelve 0
    			
    			
    			
    			
    			
    			
    			break;
    			
    			
    			
    			
    		case EnumRRHHVariable::TOTALRETENCIONES:
    			
    			/*$variable -> $id_persona
    			 * $variable1 -> $id_liquidacion
    			 *
    			 
    			 * */
    			
    			
    			$novedad_persona = new RRHHNovedadPersona();
    			
    			
    			$total_concepto = $novedad_persona->find('first', array(
    					'fields'=>array('SUM(RRHHNovedadPersona.importe_calculado) as valor'),
    					'conditions' => array('RRHHNovedadPersona.id_persona' => $id_persona,'RRHHNovedadPersona.id_rrhh_liquidacion' => $id_liquidacion,'RRHHConcepto.id_rrhh_tipo_concepto'=>EnumRRHHTipoConcepto::Retencion),
    					'contain' =>array('RRHHConcepto')
    					
    			));//busco los articulos integrantes de este Articulo
    			
    			
    			
    			return $total_concepto[0]["valor"];
    			
    			
    			break;
    			
    			
    			
    		case EnumRRHHVariable::TOTALCONTRIBUCIONESPATRONALES:
    			
    			/*$variable -> $id_persona
    			 * $variable1 -> $id_liquidacion
    			 *
    			 
    			 * */
    			
    			
    			$novedad_persona = new RRHHNovedadPersona();
    			
    			
    			$total_concepto = $novedad_persona->find('first', array(
    					'fields'=>array('SUM(RRHHNovedadPersona.importe_calculado) as valor'),
    					'conditions' => array('RRHHNovedadPersona.id_persona' => $id_persona,'RRHHNovedadPersona.id_rrhh_liquidacion' => $id_liquidacion,'RRHHConcepto.id_rrhh_tipo_concepto'=>EnumRRHHTipoConcepto::ContribucionPatronal),
    					'contain' =>array('RRHHConcepto')
    					
    			));//busco los articulos integrantes de este Articulo
    			
    			return $total_concepto[0]["valor"];
    			
    			
    			break;
    			
    			
    			
    			
    		case EnumRRHHVariable::DIASTRABAJADOSENELSEMESTRE:
    			
    			/*$variable -> $id_persona
    			 * $variable1 -> $id_liquidacion
    			 *
    			 
    			 * */
    			
    			
    			$novedad_persona = new RRHHNovedadPersona();
    			
    			
    			$total_concepto = $novedad_persona->find('first', array(
    					'fields'=>array('SUM(RRHHNovedadPersona.importe_calculado) as valor'),
    					'conditions' => array('RRHHNovedadPersona.id_persona' => $id_persona,'RRHHNovedadPersona.id_rrhh_liquidacion' => $id_liquidacion,'RRHHConcepto.id_rrhh_tipo_concepto'=>EnumRRHHTipoConcepto::ContribucionPatronal),
    					'contain' =>array('RRHHConcepto')
    					
    			));//busco los articulos integrantes de este Articulo
    			
    			return $total_concepto[0]["valor"];
    			
    			
    			break;
    			
    			
    		case EnumRRHHVariable::CANTIDADDIASMES:
    			
    			/* $id_persona
    			 * $id_liquidacion
    			 * 
    			 * 
    			 * */
    			
    			$liquidacion = $this->find('first', array(
    			'conditions' => array('RRHHLiquidacion.id' => $id_liquidacion),
    			'contain' =>false
    			));
    			
    			return date('t',strtotime($liquidacion["Liquidacion"]["fecha_periodo"]));
    			
    			break;
    			
    			
    		case EnumRRHHVariable::CANTIDADDIASSEMESTRE:
    			/*
    			 * Primer Semestre: de enero a junio con 181 d�as (182 en a�o bisiesto);
				  Segundo Semestre: de julio a diciembre con 184 d�as;
    			 * 
    			 * 
    			 * 
    			 * */
    			
    			
    			$liquidacion = $this->find('first', array(
    			'conditions' => array('RRHHLiquidacion.id' => $id_liquidacion),
    			'contain' =>false
    			));
    			
    			
    			$fechaComoEntero = strtotime($liquidacion["Liquidacion"]["fecha_periodo"]);
    			$anio = date("Y", $fechaComoEntero);
    			$mes = date("m", $fechaComoEntero);
    			
    			
    			if($mes <= 5){//5 es JUNIO, entonces esta liquidacion esta en el primer semestre
    				if($this->esBisiesto($anio))
    					return 182;
    				else
    					return 181;
    			}else{
    				
    				return 184;
    			}

    			
    			break;
    			
    			
    		default:
    			
    			return -1;//error
    			
    			break;
    		
    		
    	}
    	
    	
    	
    	
    	
    	
    	
    	
    	
    }
    
    
    
    public function divirFomulaConcepto($concepto_formula){
    	/*
    	 * 
    	 * preg_match_all("^\[(.*?)\]^","Es [CCO 0206 C] * [UIC P 30.4] / [UIC P 30] / [UIC P 365] * [CCO 9900 T 1 0 200508 200607 VAR 0]",$salida2);
    	 * 
    	 * array (size=2)
  0 => 
    array (size=5)
      0 => string '[CCO 0206 C]' (length=12)
      1 => string '[UIC P 30.4]' (length=12)
      2 => string '[UIC P 30]' (length=10)
      3 => string '[UIC P 365]' (length=11)
      4 => string '[CCO 9900 T 1 0 200508 200607 VAR 0]' (length=36)
  1 => 
    array (size=5)
      0 => string 'CCO 0206 C' (length=10)
      1 => string 'UIC P 30.4' (length=10)
      2 => string 'UIC P 30' (length=8)
      3 => string 'UIC P 365' (length=9)
      4 => string 'CCO 9900 T 1 0 200508 200607 VAR 0' (length=34)
    	 * 
    	 * 
    	 * 
    	 * 
    	 * 
    	 * */
    	
    	
    	$salida2 = array();
    	
    	
    	
    	
    	
    	preg_match_all("^\[(.*?)\]^",$concepto_formula,$salida2);
    	
    /*	
    	foreach($salida2[1] as $concepto_form){
    		
    		
    		
    	}
    
    	*/
    	
    	
    	return $salida2;
    	
    }
    
    
    
    public function interpretaConcepto($concepto_formula,$id_liquidacion,$id_persona){ 
    	
    	
    	/* lista de varios ejemplos de como puede llegar un concepto
    	 * 
    	 *  
    	 *  0 => string 'CCO 0206 C' (length=10)
      1 => string 'UIC P 30.4' (length=10)
      2 => string 'UIC P 30' (length=8)
      3 => string 'UIC P 365' (length=9)
      4 => string 'CCO 9900 T 1 0 200508 200607 VAR 0' (length=34)
    	 * 
    	 * 
    	 * 
    	 * */
    	
    	
    	$partes_concepto = explode(" ", $concepto_formula);
    	$clase_formula_concepto = $partes_concepto[0];
    	
    	$novedad_persona = new RRHHNovedadPersona();
    	$rrhh_variable_obj = new RRHHVariable();
    	
    	
    	switch($clase_formula_concepto){
    		
    		
    		
    		case EnumRRHHClaseFormulaConcepto::CCO:
    			
    			
    			$codigo_concepto = $partes_concepto[1];
    			$tipo_valor_calculo = $partes_concepto[2];
    			
    			if(!isset($partes_concepto[3])){//quiere decir que no requiere datos de una liquidacion anterior al no estar seteado los parametros entonces es de la liquidacion actual el pedido de datos
    				
    				$novedad_persona->id_liquidacion = $id_liquidacion;
    				$novedad_persona->id_persona = $id_persona;
    				
    				return $novedad_persona->getValorConcepto($codigo_concepto,$partes_concepto[2]);
    				
    			}else{//si tengo que buscar en un historico
    				
    				//buscando historicos  CCO 9900 T 1 0 200508 200607 VAR 0
    				/*
    				 * Variablo 1 EnumRRHHTipoTotal
    				 * Variable 2 indica si es una liquidacion anterior.
    				 * Variable 3fecha_desde  ----->fecha_periodo Tabla: rrhh_liquidacion
    				 * Variable 4 fecha_hasta
    				 * Variable 5 Clase liquidacion
    				 * 
    				 * Variable 6 EnumRRHHTipoTotalCalculo
    				 * 
										    				  	const  Suma 							= 0;
																const  PromedioA 						= 1;
																const  PromedioB 						= 2;
																const  Mejor 		    				= 3;
    				 * */
    				
    				
    				
    				
    				$conditions = array();
    				
    				
    				
    				array_push($conditions, array('RRHHConcepto.codigo' => $codigo_concepto));
    				
    				if(isset($partes_concepto[7])){
    					
    					$rrhh_clase_liq_obj = new RRHHClaseLiquidacion();
    					
    					$clase_liquidacion = $rrhh_clase_liq_obj->find('first', array(
    							'conditions' => array('RRHHClaseLiquidacion.codigo' => $partes_concepto[7]),
    							'contain' =>false
    					));
    					
    					
    					
    					
    					array_push($conditions, array('RRHHLiquidacion.id_rrhh_clase_liquidacion' => $clase_liquidacion["RRHHClaseLiquidacion"]["id"]));
    					
    				}
    				
    				
    				array_push($conditions, array('RRHHNovedadPersona.id_persona' => $id_persona));//siempre buscamos las novedades del legajo que estamos liquidando
    				
    		
    				if(isset($partes_concepto[5]) || isset($partes_concepto[6]) ){

    					$this->obtenerFechaConcepto($codigo_concepto,$partes_concepto,$conditions,$id_liquidacion,$id_persona,$clase_liquidacion["RRHHClaseLiquidacion"]["id"],$cantidad);
    				}
    				
    				
    				
    				if($cantidad == -1 || $cantidad>0){ //solo ejecuto estos tipos de calculos cuando no me importa el rango de liquidaciones caso  $cantidad = -1 o cuando el rango de liquidaciones>0 o sea que tengo donde buscar
	    				switch($partes_concepto[8]){
	    					
	    					case EnumRRHHTipoTotalCalculo::Suma:
	    						
	    						$resultado = $novedad_persona->find('all', array(
	    						'fields'=>array("IFNULL(SUM(RRHHNovedadPersona.importe_calculado),0) as valor"),
	    						'conditions' => array($conditions),
	    						'contain' =>array("RRHHConcepto","RRHHLiquidacion"=>array("RRHHClaseLiquidacion"))
	    						));
	    						
	    						return $resultado[0][0]["valor"];
	    						break;
	    						
	    				case EnumRRHHTipoTotalCalculo::PromedioA: //se divide por el numero de meses del resultado, o sea de la liquidacion
	    					
	    						
	    						
	    						$resultado = $novedad_persona->find('all', array(
	    						'fields'=>array("IFNULL(SUM(RRHHNovedadPersona.importe_calculado),0) as valor"),
	    						'conditions' => array($conditions),
	    						'contain' =>array("RRHHConcepto","RRHHLiquidacion"=>array("RRHHClaseLiquidacion"))
	    						));
	    						
	    						
	    						if($partes_concepto[6]>0)
	    							return $resultado[0][0]["valor"]/ltrim($partes_concepto[6]);
	    						else
	    							return 0;
	    						
	    						
	    						break;
	    						
	    				case EnumRRHHTipoTotalCalculo::PromedioB: //se divide por la cantidad efectiva de liquidaciones que encontre con los filtros
	    					
	    					
	    					
	    					$resultado = $novedad_persona->find('all', array(
	    					'fields'=>array("IFNULL(SUM(RRHHNovedadPersona.importe_calculado),0) as valor"),
	    					'conditions' => array($conditions),
	    					'contain' =>array("RRHHConcepto","RRHHLiquidacion"=>array("RRHHClaseLiquidacion"))
	    					));
	    					
	    					
	    					if($cantidad>0)
	    						return $resultado[0][0]["valor"]/$cantidad;
	    						else
	    							return 0;
	    							
	    							
	    							break;
	    					
	    					
	    				}
	    				
    				}else{
    					
    					return 0;
    				}
    				
    				
    			
    				
    				
    				
    				
    			}
    			
    			
    			break;
    			
    		case EnumRRHHClaseFormulaConcepto::UIC:
    			
    			return $partes_concepto[2];//es un numero
    			
    			break;
    			
    			
    		default: //es una variable
    			
    			$id_clase_formula_concepto = $rrhh_variable_obj->getEquivalencia($clase_formula_concepto);
    			
    			
    			if($id_clase_formula_concepto!=null){//devuelve null sino encuentra el mapeo
    				$valor = $this->convertirVariable($id_clase_formula_concepto,$id_persona,$id_liquidacion);
    				return $valor;
    			}else
    				return 0;
    				
    			break;
    			
    			
    			
    			
    		
    		
    		
    		
    	}
    	
    	
    	
    	
    	
    }
    
    
    
    public function liquidar($id_liquidacion){
    	
    	
    	
    	
    	$novedad_persona_obj = new RRHHNovedadPersona();
    	$liquidacion_persona_obj = new RRHHLiquidacionPersona();
    	
    	$legajos = $liquidacion_persona_obj->find('all', array(
    			'conditions' => array('RRHHLiquidacionPersona.id_rrhh_liquidacion' => $id_liquidacion),
    			'contain' =>array("Legajo")
    	));
    	
    	
    	
    	if($legajos){
	    	foreach($legajos as $legajo){
	    		
	    		//por cada legajo busco en la tabla de novedades los conceptos
	    		
	    		$conceptos = $novedad_persona_obj->find('all', array(
	    				'conditions' => array('RRHHNovedadPersona.id_rrhh_liquidacion' => $id_liquidacion,'RRHHNovedadPersona.id_persona'=>$legajo["Legajo"]["id"]),
	    				'contain' =>array("RRHHConcepto"),
	    				'order'=> "RRHHConcepto.orden_calculo asc"
	    		));
	    		
	    		
	    		
	    		foreach($conceptos as &$concepto){//Por cada Concepto que tenga la persona en esta liquidacion lo calculo
	    			
	    			
	    			$formula_calculada = $this->calcularValor($concepto,$id_liquidacion);//devuelve la formula con los parametros convertidos para calcular
	    			
	    			$obtener_valor_formula = $this->interpretaFormula($formula_calculada,$concepto);
	    			
	    			$novedad_persona_obj->actualizaNovedad($concepto);//guardo el concepto y hago la conversion
	    			
	    		}
	    		
	    	}
	    	
    	}else{
    		$error = EnumError::ERROR;
    		$message = "La liquidacion elegida no tiene Legajos asociados";
    		
    	}
    	
    	
    	
    	
    	
    	
    }
    
    
    public function calcularValor(&$concepto,$id_liquidacion){
    	
    	
    	$formula_desgloce = $this->divirFomulaConcepto($concepto["RRHHConcepto"]["formula_codigo"]);
    	$id_persona = $concepto["RRHHNovedadPersona"]["id_persona"];
    	
    	if(isset($formula_desgloce[1]) && count($formula_desgloce[1])>0 ){//si tiene formula
    	
    		
    		foreach ($formula_desgloce[1] as $key =>&$concepto_item){//por cada [] que encuentro en la formula, la resuelvo
    				
    			$valor = $this->interpretaConcepto($concepto_item,$id_liquidacion,$id_persona);
    			
    			/*if($valor == null){
    				
    				throw new Exception("El concepto:".$concepto["RRHHConcepto"]["d_rrhh_concepto"]." tiene en su formula al concepto:".$concepto_item." el cual no se encuentra en la agenda de Novedades de la liquidaci&oacute;n");
    				
    			}else*/
    			$formula_desgloce[2][$key] = $valor;//guardo el valor asi luego reemplazo el texto por los valores y guardo la formula, el 2 es la estructura que guarda resultados
    			
    			
    		
    		}
    		
    		return $formula_desgloce;
    	
    	}
    	
    	return array();
    	
    	
    	
    }
    
    public function interpretaFormula($formula_calculada,&$concepto){
    	
    	
    	/* Estructura de $formula_codigo
    	 *
    	 * 0 =>
    	 array (size=5)
    	 0 => string '[CCO 0206 C]' (length=12)
    	 1 => string '[UIC P 30.4]' (length=12)
    	 2 => string '[UIC P 30]' (length=10)
    	 3 => string '[UIC P 365]' (length=11)
    	 4 => string '[CCO 9900 T 1 0 200508 200607 VAR 0]' (length=36)
    	 1 =>
    	 array (size=5)
    	 0 => string 'CCO 0206 C' (length=10)
    	 1 => string 'UIC P 30.4' (length=10)
    	 2 => string 'UIC P 30' (length=8)
    	 3 => string 'UIC P 365' (length=9)
    	 4 => string 'CCO 9900 T 1 0 200508 200607 VAR 0' (length=34)
    	 
    	 2 =>
    	 array (size=5)
    	 0 => string '2' (length=10)
    	 1 => string '30.4' (length=10)
    	 2 => string '30' (length=8)
    	 3 => string '365' (length=9)
    	 4 => string '90000' (length=34)
    	 
    	 * Si (condicion) entonces A sino B
    	 *
    	 */
    	App::import('Vendor', 'EvalMath', array('file' => 'Classes/evalmath.class.php'));
    	
    	$formula_original = $concepto["RRHHConcepto"]["formula_codigo"];
    	
    	$healthy = $formula_calculada[0];
    	$yummy   = $formula_calculada[2];
    	
    	$formula_original = ltrim($formula_original);
    	
    	$primeros_caracteres = substr($formula_original, 0, 2);//determino si es ES o es IF
    	
    	$nueva_formula2 = str_replace($healthy, $yummy, $formula_original);
    	
    	if($primeros_caracteres == "Es"){
    		
    		$nueva_formula2 = str_replace("Es", "",$nueva_formula2);
    		
    
    	}else{//tengo un IF lo debo resolver
    		
    		/* Ejemplo de como queda la cadena
    			Si 0.00 > 117682.45 Entonces 117682.45 * 0.11 Sino 0.00 * 0.11
    		
    		*/
    		
    		$condicion_si = array();
    		preg_match_all("^Si(.*?)Entonces^",$nueva_formula2,$condicion_si);
    		
    		$resultado = eval($condicion_si[1][0]);
    		
    		if($resultado){
    			$formula_si_verdadero = array();
    			preg_match_all("^Entonces(.*?)Sino^",$nueva_formula2,$formula_si_verdadero);
    			$nueva_formula2 = $formula_si_verdadero;
    			
    		}else{
    			
    		
    			$formula_si_falso = array();
    			
    		
    			$pos = strpos($nueva_formula2, "Sino");
    			$pos = $pos + 4;
    			$formula_si_falso =  substr ( $nueva_formula2 , $pos , strlen($nueva_formula2) );
    			$nueva_formula2 = $formula_si_falso;
    		
    		}
    		

    		
    	
    	
    		
    		
    	}
    	
    
    	
    	
    	
    	
    	
    	$m = new EvalMath();
    	$m->suppress_errors = true;
    	
    	if ($m->evaluate('y(x) ='.$nueva_formula2)) 		
    		$importe_calculado =  $m->e("y(0);");//le paso 0 como parametro porque no tiene ninguna icognita para resolver
		else
			$importe_calculado = 0;//si falla la conversion de la formula, aplico 0
    		

    	
		$concepto["RRHHNovedadPersona"]["formula"] = $nueva_formula2;
    	$concepto["RRHHNovedadPersona"]["importe_calculado"] = $importe_calculado;
    	
    	
    }
    
    
    
    public function obtenerFechaConcepto($codigo_concepto,$partes_concepto,&$conditions,$id_liquidacion,$id_persona,$id_clase_liquidacion,&$cantidad=-1){
    	/*esta fecha viene de una formula que se usa para conceptos que se necesita saber un acumulado*/
    	
    	$id_tipo_total = $partes_concepto[4];
    	
    	$fecha_liquidacion_desde = $partes_concepto[5];
    	$fecha_liquidacion_hasta = $partes_concepto[6];
    	
    	
    	
    	
    	switch($id_tipo_total){
    		
    		case EnumRRHHTipoTotal::Absoluta://indica mes y a�o en formula. AAAAMM
    			
	    		
	    			
	    			$fecha["anio"] = substr($fecha_liquidacion_desde, 0,4);
	    			$fecha["mes"] = substr($fecha_liquidacion_desde, 3,2);
	    			
	    			array_push($conditions, array('YEAR(RRHHLiquidacion.fecha_periodo) >=' =>$fecha["anio"] ));
	    			array_push($conditions, array('MONTH(RRHHLiquidacion.fecha_periodo) >=' =>$fecha["mes"] ));
	    			
	    			
	    			$fecha["anio"] = substr($fecha_liquidacion_hasta, 0,4);
	    			$fecha["mes"] = substr($fecha_liquidacion_hasta, 3,2);
	    			
	    			array_push($conditions, array('YEAR(RRHHLiquidacion.fecha_periodo) <=' =>$fecha["anio"] ));
	    			array_push($conditions, array('MONTH(RRHHLiquidacion.fecha_periodo) <=' =>$fecha["mes"] ));
    			break;
    		
    		case EnumRRHHTipoTotal::AbsolutaA�oCurso://S�lo es necesario indicar los meses de inicio y finalizaci�n. La f�rmula es v�lida solo en el a�o de la liquidacion
    				
    			
    			
    			$fecha_periodo = $this->getFechaPeriodo($id_liquidacion);
    			
    			$fechaComoEntero = strtotime($fecha_periodo);
    			$anio = date("Y", $fechaComoEntero);
    			$mes = date("m", $fechaComoEntero);
    			
    			array_push($conditions, array('YEAR(RRHHLiquidacion.fecha_periodo)' =>	$anio ));//se realiza el calculo solo en el a�o que indica la liquidacion en curso
    			array_push($conditions, array('MONTH(RRHHLiquidacion.fecha_periodo) >=' =>$fecha_liquidacion_desde ));//en este caso viene solo el mes como MM
    			array_push($conditions, array('MONTH(RRHHLiquidacion.fecha_periodo) <=' =>$fecha_liquidacion_hasta ));
    			
    			
    			break;
    			
    		case EnumRRHHTipoTotal::Relativa: /*En este caso no detallamos meses ni a�os sino Per�odos. El Per�odo 001 es el primero
												inmediatamente anterior al mes que estamos liquidando y el Per�odo �n� ser� el �ltimo
													contado desde la misma fecha*/
    			
    			/*Periodo hace referencia a la liquidacion si es 001 indica que es la primera para atras de la actual*/
    			$periodo_desde = $partes_concepto[5];//es la cota superior
    			$periodo_hasta = $partes_concepto[6];//es la cota inferior porque siempre voy para atras ya que el periodo actual es el 000
    			
    			/*Debo seleccionar los ID de las liquidaciones para usarlas como filtro*/
    			
    			$id_liquidaciones_validas = array();
    			
    				$fecha_periodo = $this->getFechaPeriodo($id_liquidacion);
    			
    			
    				$novedad_persona_obj = new RRHHNovedadPersona() ;
    		
    				$limit = "";
    				$offset = null;
    				
    				$condiciones_liquidaciones = array();
    				array_push($condiciones_liquidaciones, array('RRHHNovedadPersona.id_persona' => $id_persona));
    				array_push($condiciones_liquidaciones, array('RRHHLiquidacion.id_rrhh_clase_liquidacion'=> $id_clase_liquidacion));
    				array_push($condiciones_liquidaciones, array('RRHHConcepto.codigo'=> $codigo_concepto));
    				array_push($condiciones_liquidaciones, array('RRHHLiquidacion.fecha_periodo <='=> $fecha_periodo));//siempre este calculo hace referencia a la liquidacion actual o anteriores no al futuro, es una condicion para evitar problemas
    				
    			
    				if($periodo_desde == 0 && $periodo_hasta == 0){
    					array_push($condiciones_liquidaciones, array('RRHHLiquidacion.id'=> $id_liquidacion));//quiero solo la liquidacion actual periododesde y hasta = 0
    					$limit = "1";
    					
    				}elseif($periodo_desde == 0 && $periodo_hasta>0){
    					
    					
    					++$periodo_hasta;//porque arranca en 0
    					$limit = ltrim($periodo_hasta,"0");
    					
    					
    				}elseif($periodo_desde >0 && $periodo_hasta>0){
    					
    					$limit = ltrim($periodo_desde,"0");
    					
    					$offset = ltrim($periodo_hasta,"0");
    					
    					
    				}
    				
    				
    				
    				if(!is_null($offset)){
    					
    					$limit_var = $offset;
    					$offset = $limit;//esto es asi ya que el ORM de CAKE da vuelta. 
    				}else{
    					$limit_var = $limit;
    				}
    					
    					
    					
    				
    				
    				
    				$liquidaciones = $novedad_persona_obj->find('all', array(
    						'conditions' => $condiciones_liquidaciones,
    						'contain' =>array("RRHHLiquidacion","RRHHConcepto"),
    						'order'=>'RRHHLiquidacion.fecha_periodo DESC',
    						'limit'=>  $limit_var,
    						'offset' => $offset
    				));
    				
    				
    	
    			
    				
    				
    				$cantidad = count($liquidaciones);
    				
    				if(count($id_liquidaciones_validas)>0){/*si encontro liquidaciones entonces las agrego*/
    					
    					foreach ($liquidaciones as $liquidacion){
    						
    						
    						array_push($id_liquidaciones_validas, $liquidacion["RRHHLiquidacion"]["id"]);
    						
    						
    					}
    					
    					
    					
    					
    					
    				}
    				
    				
    	
    				
    				
    				
    				
    			
    			break;
    			
    			
    		default:
    			
    			$cantidad = 0;
    			
    			break;
    		
    		
    	}
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    }
    
    
    
    public function getFechaPeriodo($id_liquidacion){
    	
    	
    	$liquidacion = $this->find('first', array(
    			'conditions' => array('RRHHLiquidacion.id' => $id_liquidacion),
    			'contain' =>false
    	));
    	
    	return $liquidacion["RRHHLiquidacion"]["fecha_periodo"];
    	
    }
    
    
    
    public function copiarLiquidacion($id_liquidacion_vieja, $id_liquidacion_nueva,&$excepcion){//inserta las novedades de una liquidacion en base a una anterior
    	
    	
    	
    	try{
    	
    	$this->query("INSERT INTO rrhh_novedad_persona(id_rrhh_liquidacion,id_persona,id_rrhh_concepto,cantidad,valor_unitario,importe,importe_calculado,formula,indice)

						SELECT ".$id_liquidacion_nueva.",id_persona,id_rrhh_concepto,cantidad,valor_unitario,importe,0,'',indice from rrhh_novedad_persona

						WHERE id_rrhh_liquidacion =".$id_liquidacion_vieja."


					");
    	
    	return true;
    	
    	
    	}catch(Exception $e){
    		
    		$excepcion = $e->getMessage();
    		return false;
    		
    	}
    
    }
    
    
 
  
    
    
    


}
