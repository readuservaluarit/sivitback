<?php
        include(APP.'View'.DS.'ComprobanteItemLotes'.DS.'json'.DS.'comprobante_item_lote.ctp');

		/**/
        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "C&oacute;digo";
        $model["codigo"]["order"] = "0";
        $model["codigo"]["width"] = "20";
        $model["codigo"]["text_colour"] = "#660000";  
		$model["codigo"]["picker"] = EnumMenu::mnuLotePicker;
		
        $model["d_lote"]["show_grid"] = 1;
        $model["d_lote"]["header_display"] = "Desc.";
        $model["d_lote"]["order"] = "1";
        $model["d_lote"]["width"] = "25";
        $model["d_lote"]["text_colour"] = "#660000";
        
        $model["d_tipo_lote"]["show_grid"] = 1;
        $model["d_tipo_lote"]["header_display"] = "Tipo Lote";
        $model["d_tipo_lote"]["order"] = "3";
        $model["d_tipo_lote"]["width"] = "15";
        $model["d_tipo_lote"]["text_colour"] = "#660000";
        
        
        
        $model["fecha_lote"]["show_grid"] = 1;
        $model["fecha_lote"]["header_display"] = "Fecha";
        $model["fecha_lote"]["order"] = "4";
        $model["fecha_lote"]["width"] = "20";
        $model["fecha_lote"]["text_colour"] = "#660000";  

        
        $model["d_tiene_certificado"]["show_grid"] = 1;
        $model["d_tiene_certificado"]["header_display"] = "Cert.";
        $model["d_tiene_certificado"]["order"] = "5";
		$model["d_tiene_certificado"]["width"] = "5";
        $model["d_tiene_certificado"]["read_only"] = "0";
        


        $model["d_validado"]["show_grid"] = 1;
        $model["d_validado"]["header_display"] = "Val.";
        $model["d_validado"]["order"] = "6";
		$model["d_validado"]["width"] = "5";
		$model["d_validado"]["read_only"] = "0";
        
        
        $model["codigo_tipo_comprobante"]["show_grid"] = 1;
        $model["codigo_tipo_comprobante"]["header_display"] = "Tipo Comp.";
        $model["codigo_tipo_comprobante"]["order"] = "7";
        $model["codigo_tipo_comprobante"]["width"] = "20";
        $model["codigo_tipo_comprobante"]["text_colour"] = "#660000";  
        
        
        
        $model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
        $model["pto_nro_comprobante"]["order"] = "9";
        $model["pto_nro_comprobante"]["width"] = "20";
        $model["pto_nro_comprobante"]["text_colour"] = "#660000";
        
        
        $model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["header_display"] = "R.Social";
        $model["razon_social"]["order"] = "10";
        $model["razon_social"]["width"] = "20";
        $model["razon_social"]["text_colour"] = "#660000";
        
        
        
    
        
        
           
		$output = array(
				"status" =>EnumError::SUCCESS,
				"message" => "ComprobanteItemLote",
				"content" => $model
			);
        
          echo json_encode($output);
?>