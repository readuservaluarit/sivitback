<?php   // CONFIG  CON MODE DISPLAYED CELLS
        include(APP.'View'.DS.'Comprobantes'.DS.'json'.DS.'default.ctp');
        
       
        $model["codigo_tipo_comprobante"]["show_grid"] = 1;
        $model["codigo_tipo_comprobante"]["header_display"] = "Tipo";
        $model["codigo_tipo_comprobante"]["order"] = "1";
        $model["codigo_tipo_comprobante"]["width"] = "5";
        $model["codigo_tipo_comprobante"]["text_colour"] = "#000000";
        
        $model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
        $model["pto_nro_comprobante"]["order"] = "2";
        $model["pto_nro_comprobante"]["width"] = "17";
        $model["pto_nro_comprobante"]["text_colour"] = "#000000";
        
        $model["d_detalle_tipo_comprobante"]["show_grid"] = 1;
        $model["d_detalle_tipo_comprobante"]["header_display"] = "Detalle";
        $model["d_detalle_tipo_comprobante"]["order"] = "6";
        $model["d_detalle_tipo_comprobante"]["width"] = "7";
        $model["d_detalle_tipo_comprobante"]["text_colour"] = "#440088";
        
        $model["pto_nro_comprobante_origen"]["show_grid"] = 1;
        $model["pto_nro_comprobante_origen"]["header_display"] = "Nro. Pedido";
        $model["pto_nro_comprobante_origen"]["order"] = "7";
        $model["pto_nro_comprobante_origen"]["width"] = "13";
        $model["pto_nro_comprobante_origen"]["text_colour"] = "#000000";
        
        $model["codigo_producto"]["show_grid"] = 1;
        $model["codigo_producto"]["header_display"] = "C&oacute;digo";
        $model["codigo_producto"]["order"] = "8";
        $model["codigo_producto"]["width"] = "20";
        $model["codigo_producto"]["text_colour"] = "#000000";
		
		$model["cantidad"]["show_grid"] = 1;
        $model["cantidad"]["header_display"] = "Cant.";
        $model["cantidad"]["order"] = "9";
        $model["cantidad"]["width"] = "6";
        $model["cantidad"]["text_colour"] = "#440000";
        
        $model["fecha_generacion"]["show_grid"] = 1;
        $model["fecha_generacion"]["header_display"] = "Fecha Creaci&oacute;n";
        $model["fecha_generacion"]["order"] = "10";
        $model["fecha_generacion"]["width"] = "12";
        $model["fecha_generacion"]["text_colour"] = "#0000FF";
		
		$model["fecha_entrega"]["show_grid"] = 1;
        $model["fecha_entrega"]["header_display"] = "Entrega a Armado";
        $model["fecha_entrega"]["order"] = "11";
        $model["fecha_entrega"]["width"] = "12";
        $model["fecha_entrega"]["text_colour"] = "#CC0011";
		
        // $model["codigo_persona"]["show_grid"] = 1;
        // $model["codigo_persona"]["header_display"] = "Cod. Cliente";
        // $model["codigo_persona"]["order"] = "11";
        // $model["codigo_persona"]["width"] = "6";
        // $model["codigo_persona"]["text_colour"] = "#000000";
        
        $model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["header_display"] = "Raz&oacute;n social";
        $model["razon_social"]["order"] = "12";
        $model["razon_social"]["width"] = "20";
        $model["razon_social"]["text_colour"] = "#000000";
        
        $model["d_estado_comprobante"]["show_grid"] = 1;
        $model["d_estado_comprobante"]["type"] = EnumTipoDato::cadena; 
        $model["d_estado_comprobante"]["header_display"] = "Estado";
        $model["d_estado_comprobante"]["order"] = "13";
        $model["d_estado_comprobante"]["width"] = "8";
        $model["d_estado_comprobante"]["text_colour"] = "#AA0000";
        
        $model["d_usuario"]["show_grid"] = 1;
        $model["d_usuario"]["header_display"] = "Operador";
        $model["d_usuario"]["web_width"] = "5";
		$model["d_usuario"]["width"] = "6";
        $model["d_usuario"]["order"] = "14";
        $model["d_usuario"]["text_colour"] = "#770077";
        
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Comprobante",
                    "content" => $model
                );
        echo json_encode($output);
?>