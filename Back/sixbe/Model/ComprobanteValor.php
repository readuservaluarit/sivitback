<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class ComprobanteValor extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'comprobante_valor';

    public $actsAs = array('Containable');
    
   
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

public $belongsTo = array(
        'Comprobante' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Valor' => array(
            'className' => 'Valor',
            'foreignKey' => 'id_valor',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Moneda' => array(
            'className' => 'Moneda',
            'foreignKey' => 'id_moneda',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'Cheque' => array(
            'className' => 'Cheque',
            'foreignKey' => 'id_cheque',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'CuentaBancaria' => array(
            'className' => 'CuentaBancaria',
            'foreignKey' => 'id_cuenta_bancaria',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
		'TarjetaCredito' => array(
				'className' => 'TarjetaCredito',
				'foreignKey' => 'id_tarjeta_credito',
				'conditions' => '',
				'fields' => '',
				'order' => ''
		),
		
 );
 
  public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     "d_banco"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_banco_sucursal"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_moneda"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "nro_cheque"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "nro_cuenta"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "n_item"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_valor"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "total"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>200),
     "pto_nro_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "fecha_generacion"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>200),
     "razon_social"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_estado_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "codigo_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "fecha_valor"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_sistema"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "descripcion"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "fecha_contable"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>200),
     "nro_tarjeta"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "total_por_comprobante"=>array("type"=>EnumTipoDato::decimal_flotante,"show_grid"=>0,"default"=>null,"length"=>200),
     "codigo_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "codigo_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_ingreso"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "nro_cheque"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_detalle_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "d_comprobante_valor"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>200),
     "nro_comprobante"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>200),
     "pto_venta"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>200),
     "fecha_cheque"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>200),
     "fecha_clearing"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>200),
    
    );
    
    }
 
 
 
   /*
 public $hasMany = array(
        'ArticuloRelacion' => array(
            'className' => 'ArticuloRelacion',
            'foreignKey' => 'id_producto',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        
);
	  */

}
