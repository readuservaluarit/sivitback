<?php

/**
* @secured(CONSULTA_PRODUCTO_TIPO)
*/
class ProductosTipoController extends AppController {
    public $name = 'ProductosTipo';
    public $model = 'ProductoTipo';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

/**
* @secured(CONSULTA_PRODUCTO_TIPO)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
                                                                            //data[ProductoTipo][id_destino_producto_excluir]=2
        $id_destino_producto_excluir = strtolower($this->getFromRequestOrSession('ProductoTipo.id_destino_producto_excluir'));
        $id_producto_tipo_excluir = strtolower($this->getFromRequestOrSession('ProductoTipo.id_producto_tipo_excluir'));
        $remitible = $this->getFromRequestOrSession('ProductoTipo.remitible');
        
                    //$id_destino_producto_excluir = 2;
        $conditions = array();
    
        if ($id_destino_producto_excluir != "") {
            $conditions = array('ProductoTipo.id_destino_producto <>' =>$id_destino_producto_excluir );
        }
        
        if ($remitible != "")
            $conditions = array('ProductoTipo.remitible' =>$remitible );
        

        if($id_producto_tipo_excluir!=""){
            $id_producto_tipo_excluir_array = explode(",", $id_producto_tipo_excluir);
             array_push($conditions,array( "NOT" => (array( 'ProductoTipo.id =' => $id_producto_tipo_excluir_array  )) )); 
        
        }
       
        
        
       
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'contain'=>array("DepositoFijoPrincipal","DepositoFijoComplementario"),
            'limit' => 10,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de TipoPersonaCausas', 'Datos de los Tipo de Documento', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_TipoPersona', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'TipoPersona.id', "10%");
                $formBuilder->addHeader('Nombre', 'TipoPersona.d_TipoPersona_causa', "50%");
                $formBuilder->addHeader('Nombre', 'TipoPersona.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_TipoPersonaCausa');
                $formBuilder->addField($this->model, 'cotizacion');
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        foreach($data as &$valor){
        	
        	if(isset($valor["DepositoFijoPrincipal"]["d_deposito"]) && $valor["DepositoFijoPrincipal"]["id"]>0)
        		$valor["ProductoTipo"]["d_deposito_fijo_principal"] = $valor["DepositoFijoPrincipal"]["d_deposito"];
        	
        	
        	if(isset($valor["DepositoFijoComplementario"]["d_deposito"]) && $valor["DepositoFijoComplementario"]["id"]>0)
        		$valor["ProductoTipo"]["d_deposito_fijo_complementario"] = $valor["DepositoFijoComplementario"]["d_deposito"];
        
        			unset($valor["DepositoFijoPrincipal"]);
        			unset($valor["DepositoFijoComplementario"]);
        }
        
        
        
        $this->data = $data;
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }
    
    
    /**
     * @secured(MODIFICACION_PRODUCTO_TIPO)
     */
    
    public function edit($id) {
    	
    	
    	
    	
    	$this->loadModel($this->model);
    	$this->{$this->model}->id = $id;
    	
    	try{
    		if ($this->{$this->model}->saveAll($this->request->data)){
    			$mensaje =  "El Tipo ha sido modificado exitosamente";
    			$status = EnumError::SUCCESS;
    			
    			
    		}else{   //si hubo error recupero los errores de los models
    			
    			$errores = $this->{$this->model}->validationErrors;
    			$errores_string = "";
    			foreach ($errores as $error){ //recorro los errores y armo el mensaje
    				$errores_string.= "&bull; ".$error[0]."\n";
    				
    			}
    			$mensaje = $errores_string;
    			$status = EnumError::ERROR;
    		}
    		
    		
    	}catch(Exception $e){
    		$status = EnumError::ERROR;
    		$mensaje = $e->getMessage();
    		
    	}
    	
    	$output = array(
    			"status" => $status,
    			"message" => $mensaje,
    			"content" => ""
    	);
    	
    	$this->set($output);
    	$this->set("_serialize", array("status", "message", "content"));
    	
    	
    }
    
       /**
        * @secured(CONSULTA_PRODUCTO_TIPO)
        */
        public function getModel($vista='default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
            $model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }

        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $this->set('model',$model);
            Configure::write('debug',0);
            $this->render($vista);

        }


   



    
    
    
    
   
	

}
?>