<?php   //CONFIG  CON MODE DISPLAYED CELLS
        include(APP.'View'.DS.'ListaPrecioProductos'.DS.'json'.DS.'lista_precio_producto.ctp');

		$model["d_lista_precio"]["show_grid"] = 1;
        $model["d_lista_precio"]["header_display"] = "Lista P.";
        $model["d_lista_precio"]["order"] = "1";
        $model["d_lista_precio"]["width"] = "4";
		$model["d_lista_precio"]["text_colour"] = "#550000";
		$model["d_lista_precio"]["read_only"] = "1";
        

        $model["precio"]["show_grid"] = 1;
        $model["precio"]["header_display"] = "Precio Neto";
        $model["precio"]["order"] = "2";
        $model["precio"]["width"] = "4";
		$model["precio"]["text_colour"] = "#000099";
		$model["precio"]["read_only"] = "1";
		
		$model["moneda_simbolo"]["show_grid"] = 1;
        $model["moneda_simbolo"]["header_display"] = "Moneda";
        $model["moneda_simbolo"]["order"] = "3";
        $model["moneda_simbolo"]["width"] = "4";
		$model["moneda_simbolo"]["text_colour"] = "#008800";
		$model["moneda_simbolo"]["read_only"] = "1";
       
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ListaPrecioProducto",
                    "content" => $model
                );
        
          echo json_encode($output);
?>