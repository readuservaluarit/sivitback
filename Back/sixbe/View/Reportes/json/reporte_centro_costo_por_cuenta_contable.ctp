<?php		

        App::import('Model','CentroCosto');
        $centro_costo_obj = new CentroCosto();
        
        
        $centro_costo =  $centro_costo_obj->getCentroCostoActivo();
        
        $model["es_mayor"]["show_grid"] = 0;
        $model["es_mayor"]["type"] = EnumTipoDato::entero;
        
         
        $model["codigo"]["show_grid"] = 1;
        $model["codigo"]["header_display"] = "C&oacute;digo";
        $model["codigo"]["type"] = EnumTipoDato::cadena;
        $model["codigo"]["minimun_width"] = "5"; //REstriccion mas de esto no se puede achicar
        $model["codigo"]["order"] = "0";
        $model["codigo"]["text_colour"] = "#000000";
            
        $model["d_cuenta_contable"]["show_grid"] = 1;
        $model["d_cuenta_contable"]["type"] = EnumTipoDato::cadena;
        $model["d_cuenta_contable"]["header_display"] = "Cuenta Contable";
        $model["d_cuenta_contable"]["minimun_width"] = "5"; //REstriccion mas de esto no se puede achicar
        $model["d_cuenta_contable"]["order"] = "1";
        $model["d_cuenta_contable"]["text_colour"] = "#000000";
        
        
        $model["d_es_mayor"]["show_grid"] = 1;
        $model["d_es_mayor"]["type"] = EnumTipoDato::cadena;
        $model["d_es_mayor"]["header_display"] = "Mayor?";
        $model["d_es_mayor"]["minimun_width"] = "5"; //REstriccion mas de esto no se puede achicar
        $model["d_es_mayor"]["order"] = "2";
        $model["d_es_mayor"]["text_colour"] = "#000000";

        
        /*Muestro todos los centros de costo en el reporte, estan inicializados con 0*/
		$orden = 3;
		foreach($centro_costo as $centro){
        
        $model[$centro["CentroCosto"]["id"]]["show_grid"] = 1;
        $model[$centro["CentroCosto"]["id"]]["type"] = EnumTipoDato::decimal_flotante;
        $model[$centro["CentroCosto"]["id"]]["header_display"] = $centro["CentroCosto"]["d_centro_costo"];
        $model[$centro["CentroCosto"]["id"]]["minimun_width"] = "5"; //REstriccion mas de esto no se puede achicar
        $model[$centro["CentroCosto"]["id"]]["order"] = $orden;
        $model[$centro["CentroCosto"]["id"]]["text_colour"] = "#000000";   
        ++$orden;    
        }
		
		

         $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "CuentaContable",
                    "content" => $model
                );
        
          echo json_encode($output);
?>