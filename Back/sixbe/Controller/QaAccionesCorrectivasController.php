<?php

  /**
    * @secured(CONSULTA_QAACCIONCORRECTIVA)
  */
class QaAccionesCorrectivasController extends AppController {
    public $name = 'QaAccionesCorrectivas';
    public $model = 'QaAccionCorrectiva';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
  
  
    /**
    * @secured(CONSULTA_QAACCIONCORRECTIVA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $fecha = $this->getFromRequestOrSession('QaAccionCorrectiva.fecha');
        $id_QAACCIONCORRECTIVA = $this->getFromRequestOrSession('QaAccionCorrectiva.id');
        $id_estado = $this->getFromRequestOrSession('QaAccionCorrectiva.id_estado');
         
        
        $conditions = array(); 

        if($fecha!="")
            array_push($conditions, array('QaAccionCorrectiva.fecha LIKE' => '%' . $fecha  . '%')); 
              
        if($id_QAACCIONCORRECTIVA!="")
            array_push($conditions, array('QaAccionCorrectiva.id =' => $id_QAACCIONCORRECTIVA));
            
       
         
         if($id_estado!="")
            array_push($conditions, array('QaAccionCorrectiva.id_estado =' => $id_estado)); 
               
                           
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
             'contain' =>array('Area','Reclamo','Estado','Responsable','ResponsableCumplimiento'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum(),
            'order' => 'QaAccionCorrectiva.id desc'
        );
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
    
    
        
        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'QaAccionCorrectiva.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'QaAccionCorrectiva.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
      
      
      
          
    else{ // vista json
    
        
        
        
        
        
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as &$valor){
            
            $valor['QaAccionCorrectiva']['responsable_cumplimiento'] = $valor['ResponsableCumplimiento']['nombre']." ".$valor['ResponsableCumplimiento']['apellido'];
            $valor['QaAccionCorrectiva']['responsable_efectividad'] = $valor['Responsable']['nombre']." ".$valor['Responsable']['apellido'];
            
            
        }
        
        
        /*
        foreach($data as &$valor){
             $valor['QaAccionCorrectiva']['QaAccionCorrectivaItem'] = $valor['QaAccionCorrectivaItem'];
             unset($valor['QaAccionCorrectivaItem']);
             
             foreach($valor['QaAccionCorrectiva']['QaAccionCorrectivaItem'] as &$producto ){
                 
                 $producto["d_producto"] = $producto["Producto"]["d_producto"];
                 //$producto["d_producto"] = $this->setFieldProperties("d_producto","Producto",$producto["Producto"]["d_producto"],"1","3");
                 //$producto["codigo_producto"] = $this->setFieldProperties("codigo_producto","Codigo",$producto["Producto"]["codigo"],"1","4");
                 
                 $producto["codigo_producto"] = $producto["Producto"]["codigo"];
                 
                 
                 unset($producto["Producto"]);
             }
             
             $valor['QaAccionCorrectiva']['moneda'] = $valor['Moneda'];
             unset($valor['Moneda']);
             $valor['QaAccionCorrectiva']['estado'] = $valor['Estado'];
             unset($valor['Estado']);
             $valor['QaAccionCorrectiva']['Cliente'] = $valor['Cliente'];
             unset($valor['Cliente']);
             
            
        }
        */
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
        
        
        
        
    //fin vista json
        
    }
    
    /**
    * @secured(ABM_USUARIOS)
    */
    public function abm($mode, $id = null) {
        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();
            
        $this->layout = 'ajax';
        $this->loadModel($this->model);
        //$this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        if ($mode != "A"){
            $this->QaAccionCorrectiva->id = $id;
            $this->QaAccionCorrectiva->contain();
            $this->request->data = $this->QaAccionCorrectiva->read();
        
        } 
        
        
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();
         //Configuracion del Formulario
      
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('QaAccionCorrectiva');
        
        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de QaAccionesCorrectivas');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de QaAccionesCorrectivas');
        else
            $formBuilder->setTituloForm('Consulta de QaAccionesCorrectivas');
        
        
        
        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));
        
        $formBuilder->addFormBeginRow();
        //Fields
        $formBuilder->addFormInput('razon_social', 'Raz&oacute;n social', array('class'=>'control-group'), array('class' => '', 'label' => false));
        $formBuilder->addFormInput('cuit', 'Cuit', array('class'=>'control-group'), array('class' => 'required', 'label' => false)); 
        $formBuilder->addFormInput('calle', 'Calle', array('class'=>'control-group'), array('class' => 'required', 'label' => false));  
        $formBuilder->addFormInput('tel', 'Tel&eacute;fono', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('ciudad', 'Ciudad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descuento', 'Descuento', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('fax', 'Fax', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('descripcion', 'Descripcion', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
        $formBuilder->addFormInput('localidad', 'Localidad', array('class'=>'control-group'), array('class' => 'required', 'label' => false));
         
        
        $formBuilder->addFormEndRow();   
        
         $script = "
       
            function validateForm(){
                Validator.clearValidationMsgs('validationMsg_');
    
                var form = 'ClienteAbmForm';
                var validator = new Validator(form);
                
                validator.validateRequired('ClienteRazonSocial', 'Debe ingresar una Raz&oacute;n Social');
                validator.validateRequired('ClienteCuit', 'Debe ingresar un CUIT');
                validator.validateNumeric('ClienteDescuento', 'El Descuento ingresado debe ser num&eacute;rico');
                //Valido si el Cuit ya existe
                       $.ajax({
                        'url': './".$this->name."/existe_cuit',
                        'data': 'cuit=' + $('#ClienteCuit').val()+'&id='+$('#ClienteId').val(),
                        'async': false,
                        'success': function(data) {
                           
                            if(data !=0){
                              
                               validator.addErrorMessage('ClienteCuit','El Cuit que eligi&oacute; ya se encuenta asignado, verif&iacute;quelo.');
                               validator.showValidations('', 'validationMsg_');
                               return false; 
                            }
                           
                            
                            }
                        }); 
                
               
                if(!validator.isAllValid()){
                    validator.showValidations('', 'validationMsg_');
                    return false;   
                }
                return true;
                
            } 


            ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');
        
        
    
        
        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');   
        
    }

    /**
    * @secured(CONSULTA_QAACCIONCORRECTIVA)
    */
    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

     /**
    * @secured(ADD_QAACCIONCORRECTIVA)
    */
   public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_ac = "";
           $this->request->data["QaAccionCorrectiva"]["fecha"] = date("Y-m-d H:i:s");
            try{
                
                    if ($this->QaAccionCorrectiva->saveAll($this->request->data)){
                        
                        $id_ac = $this->QaAccionCorrectiva->id;
                        $mensaje = "La QaAccionCorrectiva ha sido creada exitosamente";
                        $tipo = EnumError::SUCCESS;
                       
                    }else{
                        $mensaje = "Ha ocurrido un error,la QaAccionCorrectiva no ha podido ser creada.";
                        $tipo = EnumError::ERROR; 
                        
                    }
                    
              
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error, la QaAccionCorrectiva no ha podido ser creada.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" => $id_ac
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content","id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }
    
       /**
    * @secured(MODIFICACION_QAACCIONCORRECTIVA)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->QaAccionCorrectiva->id = $id;
            
            try{ 
                if ($this->QaAccionCorrectiva->saveAll($this->request->data,array('deep' => true))){
                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "La QaAccionCorrectiva ha sido modificada exitosamente",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('La QaAccionCorrectiva ha sido modificada exitosamente.', 'success');
                        $this->redirect(array('controller' => 'QaAccionesCorrectivas', 'action' => 'index'));
                        
                    } 
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, la QaAccionCorrectiva no ha podido modificarse.', 'error');
                
            }
           
        }else{
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->QaAccionCorrectiva->id = $id;
            $this->QaAccionCorrectiva->contain();
            $this->request->data = $this->QaAccionCorrectiva->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
            }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }
    
     /**
    * @secured(BAJA_QAACCIONCORRECTIVA)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->QaAccionCorrectiva->id = $id;
       try{
            if ($this->QaAccionCorrectiva->saveField('activo', "0")) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "La QaAccionCorrectiva ha sido eliminada exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }


    
    public function home(){
         if($this->request->is('ajax'))
            $this->layout = 'ajax';
         else
            $this->layout = 'default';
    }
    
   
    
    
    
    /**
    * @secured(REPORTE_QAACCIONCORRECTIVA)
    */
    public function pdfExport($id){
        
        
        $this->loadModel("QaAccionCorrectiva");
        $this->QaAccionCorrectiva->id = $id;
        $this->QaAccionCorrectiva->contain(array('QaAccionCorrectivaItem' => array('Producto'),'Moneda','Cliente'=> array('Pais','Provincia'),'CondicionPago'));
        //$this->QaAccionCorrectiva->order(array('QaAccionCorrectivaItem.orden asc'));
        $this->request->data = $this->QaAccionCorrectiva->read(); 
        Configure::write('debug',0);
        if( $this->request->data["Cliente"]["id"] == null ){
            $this->request->data["Cliente"]["razon_social"] = $this->request->data["QaAccionCorrectiva"]["razon_social"];
            $this->request->data["Cliente"]["cuit"] = $this->request->data["QaAccionCorrectiva"]["cuit"]; 
        
        }
            
        $this->set('datos_pdf',$this->request->data); 
        $this->set('id',$id);
        Configure::write('debug',0);
        $this->layout = 'pdf'; //esto usara el layout pdf.ctp
        $this->render();
        
  
     
        
    }
    
    public function initialize(){ //Seteo los ordenes del Listado
        
        
       
        
        //seteo las propiedades de los Items Agregados en runtime
        $this->addCalculatedFieldItem($this->setFieldProperties("d_producto","Producto","","1","3"));
        $this->addCalculatedFieldItem($this->setFieldProperties("codigo_producto","Codigo","","1","4"));
        
    }
    
   
    
}
?>