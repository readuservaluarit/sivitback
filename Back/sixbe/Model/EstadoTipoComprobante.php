<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class EstadoTipoComprobante extends AppModel {

/**
 * Use table
 * @var mixed False or table name
 */
 
    public $useTable = 'estado_tipo_comprobante';
	public $actsAs = array('Containable');
	
/**
 * Validation rules
 * @var array
 */
    public $validate = array(
        'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        /*
        'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

public $belongsTo = array(
        'EstadoComprobante' => array(
            'className' => 'EstadoComprobante',
            'foreignKey' => 'id_estado_comprobante',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'TipoComprobante' => array(
            'className' => 'TipoComprobante',
            'foreignKey' => 'id_tipo_comprobante',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
 );
 
 public function getEstados(){  
         $estado_tipo_comprobante = $this->find('all', array(
                                    //'conditions' => array('Moneda.id' => $id_moneda),
                                    'contain' =>false
                                    ));
         return $estado_tipo_comprobante;
    }
    
    
    public function getindex($conditions = array(),$model ){
 	
 	$data	= $this->find('all', array(
 			'conditions' => $conditions,
 			'contain' =>array('TipoComprobante','EstadoComprobante')
 	));
 	
 	

 	
 	foreach($data as &$valor){
 		
 		if(isset($valor['TipoComprobante'])){
 			$valor[$model]['d_tipo_comprobante'] 		= $valor['TipoComprobante']['d_tipo_comprobante'];
 			$valor[$model]['codigo_tipo_comprobante'] = $valor['TipoComprobante']['codigo_tipo_comprobante'];
 			unset($valor['TipoComprobante']);
 		}
 		
 		if(isset($valor['EstadoComprobante'])){
 			$valor[$model]['d_estado_comprobante'] = $valor['EstadoComprobante']['d_estado_comprobante'];
 			unset($valor['EstadoComprobante']);
 		}
 	}
 	
 	
 	
 	return $data;
 }
    
}
