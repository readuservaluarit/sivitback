<?php
class EntityPickerController extends AppController {

    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    public $theme = 'Grape';
    public $name = 'EntityPicker';
    
    public function index($pickerConfiguration, $pickerId, $selectFields, $callbacksJS, $filterQueryFields) {
        $pickerClass =  $pickerConfiguration;
        
        App::import('Lib/EntityPickers', $pickerClass);
        
        
        $pickerId = urldecode($pickerId);
        $selectFields = urldecode($selectFields);
        $callbacksJS = urldecode($callbacksJS);
        $filterQueryFields = urldecode($filterQueryFields);
        
        $pickerConfiguration = new $pickerClass;
        
        $model = $pickerConfiguration->model;
        
        $this->loadModel($model);
        $this->layout = 'ajax';
             
        $conditions = $pickerConfiguration->getPickerInitialFilter($this); 
        
        //Agrego los filtros de pickers anidados
        $conditions = array_merge($conditions, $this->getFilterQueryFieldsConditions(json_decode($filterQueryFields)));
        
        //Recuperacion de Filtros
        $bus_unico = $this->getFromRequestOrSession($model.'.bus_unico');
        
        //$busqueda = $this->request->data[$model];                                 
        $busqueda = array();
        $busqueda['bus_unico'] = $bus_unico;
        
        $conditions =  array_merge($conditions, $pickerConfiguration->getPickerConditions($busqueda, $filterQueryFields));
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'fields' => $pickerConfiguration->getPickerFields(), 
            'order' => $pickerConfiguration->getPickerOrder(), 
            'conditions' => $conditions, 
            'contain' =>$pickerConfiguration->getPickerContain() , 
            'group' => $pickerConfiguration->getPickerGroup(), 
            'joins' => $pickerConfiguration->getPickerJoins(), 
            'limit' => 10);
         
        $this->set('rows',  $pickerConfiguration->prepareData($this->PaginatorModificado->paginate($model)));
        $this->set('fields', $pickerConfiguration->pickerFields);
        $this->set('model', $model);
        $this->set('title', $pickerConfiguration->pickerTitle);
        $this->set('width', $pickerConfiguration->width);
        $this->set('pickerConfiguration', $pickerConfiguration);
        $this->set('busqueda', isset($busqueda)?$busqueda:null);  
        $this->set('pickerId', $pickerId); 
        $this->set('callbacksJS', $callbacksJS); 
        $this->set('arrayCallbacksJS',objectToArray(json_decode($callbacksJS))); 
        $this->set('filterQueryFields', $filterQueryFields);
        $this->set('arrayFilterQuertFields',objectToArray(json_decode($filterQueryFields))); 
        $this->set('selectFields', $selectFields);  
        $this->set('arraySelectFields', objectToArray(json_decode($selectFields)));  
        
        //print_r(objectToArray(json_decode($selectFields)));
        //die();
    }
    
    function getFilterQueryFieldsConditions($filterQueryFields){
        
        $cond = array();
        
        if(isset($filterQueryFields) && $filterQueryFields!=null && sizeof($filterQueryFields) > 0){
           
           foreach($filterQueryFields as $clave => $valor){
                        
            array_push($cond, array($clave => $valor));
                           
        } 
            
        }
        
        return $cond;
        
    }
    
    /**
    * Override de AppController para customizarlo para los pickers
    * 
    * @param mixed $index
    * @return mixed
    */
    public function getFromRequestOrSession($index){
        
        $sessionKey = 'Picker' . $this->request->params['controller'].'.'.$this->request->params['action'].'.'.$index;
        
        $aux = explode(".", $index);
        $fieldName = implode($aux, "']['");
        
        $valorDato = '';
        
       
        if(($this->request->is('get') && !isset($this->request->params['named']['page']))){
            
            //Si es un get y no hay seteado parametros de pagina es porque es la primera vezque lo abre 
            $this->Session->delete($sessionKey);
            
        }else{

            
            //Si es un post es porque está haciendo una busqueda
            if($this->request->is('post')){
                //lo saco del request y lo pongo en session    
                eval("\$valorDato = isset(\$this->request->data['".$fieldName."'])?\$this->request->data['".$fieldName."']:'';");
                $this->Session->write($sessionKey, $valorDato);
                
            }else{
                
                //Entra aca si está cambiando de página
                $valorDato = $this->Session->read($sessionKey);
                
            }
            
        }
           
        return $valorDato;
        
        
    }
    
}
?>