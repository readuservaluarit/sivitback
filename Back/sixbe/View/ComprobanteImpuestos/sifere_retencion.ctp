<?php

    if($nombre_archivo  == "")
        $nombre_archivo = "SifereRetenciones";
        
    header("Content-Disposition: attachment; filename=".$nombre_archivo.".txt");
    

    header("Pragma: no-cache"); 
    header("Expires: 0"); 
    
    $comprobante_obj  = new Comprobante();
    $persona_obj  = new Persona();
    
    foreach($datos as $comprobante){
        
        
       $punto_venta_04 = str_pad($comprobante["ComprobanteImpuesto"]["d_punto_venta"], 4, '0', STR_PAD_LEFT);//punto de venta de la retencion
       
       $numero_retencion = $comprobante["ComprobanteImpuesto"]["numero"];
       
       if(strlen($numero_retencion)>15)
       	$numero_retencion = substr($numero_retencion,0,16);
       	
       $nro_comprobante_05 = str_pad($numero_retencion, 16, '0', STR_PAD_LEFT);//numero de la retencion
       $fecha = new DateTime($comprobante["ComprobanteImpuesto"]['fecha']);//va la fecha de la retencion
       $fecha_03 =$fecha->format('d/m/Y'); 
       $codigo_jurisdiccion_01 = str_pad($comprobante['Impuesto']["Provincia"]["sifere_jurisdiccion"], 3, '0', STR_PAD_LEFT);//es la jurisdiccion del impuesto
       $cuit =  $comprobante["Comprobante"]["Persona"]["cuit"];
       $nro_doc_final_02 = $persona_obj->getCuitConGuiones($cuit); 
   
       
       $importe_impuesto = $comprobante["ComprobanteImpuesto"]["importe_impuesto"];//son en pesos 
       
       
       $importe_impuesto = $comprobante_obj->SacarComaDecimalaNumeroYFormatearConCeros($importe_impuesto,8,2,"0");
       $parte_entera = substr($importe_impuesto,0,8);
       $parte_decimal = substr($importe_impuesto,8,2);
       $importe_impuesto_08 = $parte_entera.','.$parte_decimal;
       $nro_comprobante_origen = "00000000000000000000";//una retencion en un recibo suele ser por multiples facturas
       
    	//preguntar cecilia si las retenciones sufridas solo son en recibos de cobro
       
       switch($comprobante["Comprobante"]["TipoComprobante"]["id"]){
           
           case EnumTipoComprobante::ReciboAutomatico:
           case EnumTipoComprobante::ReciboManual:
           $tipo_comprobante_06 = "O";
           $letra_07 = " ";
           break;
          
           default:
            $tipo_comprobante_06 = "O";
            $letra_07 = " ";
                        
       }
       $linea_texto = $codigo_jurisdiccion_01.$nro_doc_final_02.$fecha_03.$punto_venta_04.$nro_comprobante_05.$tipo_comprobante_06.$letra_07.$nro_comprobante_origen.$importe_impuesto_08; 
       echo $linea_texto.  "\r\n";       
    }
        


?>