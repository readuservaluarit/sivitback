<?php 
		$model["id"]["show_grid"] = 1;
        $model["id"]["header_display"] = "Nro.";
        $model["id"]["order"] = "0";
        $model["id"]["width"] = "5";
        
        $model["d_periodo_impositivo"]["show_grid"] = 1;
        $model["d_periodo_impositivo"]["header_display"] = "Desc.";
        $model["d_periodo_impositivo"]["order"] = "1";
        $model["d_periodo_impositivo"]["width"] = "30";
				

        $model["nro_periodo"]["show_grid"] = 1;
        $model["nro_periodo"]["header_display"] = "Nro Period.";
        $model["nro_periodo"]["order"] = "2";
        $model["nro_periodo"]["width"] = "10";
		$model["cerrado"]["text_colour"] = "#770000";
        
        $model["d_ejercicio_contable"]["show_grid"] = 1;
        $model["d_ejercicio_contable"]["header_display"] = "Ejercicio Contable.";
        $model["d_ejercicio_contable"]["order"] = "3";
        $model["d_ejercicio_contable"]["width"] = "10";
        
        $model["actual"]["show_grid"] = 1;
        $model["actual"]["header_display"] = "Actual?.";
        $model["actual"]["order"] = "4";
        $model["actual"]["width"] = "10";
		$model["actual"]["text_colour"] = "#004400";
        
        $model["desc_periodo"]["show_grid"] = 1;
        $model["desc_periodo"]["header_display"] = "Desc. Secund.";
        $model["desc_periodo"]["order"] = "5";
        $model["desc_periodo"]["width"] = "30";
		$model["desc_periodo"]["text_colour"] = "#990000";
        
        $model["fecha_desde"]["show_grid"] = 1;
        $model["fecha_desde"]["header_display"] = "Fecha Dsd.";
        $model["fecha_desde"]["order"] = "6";
        $model["fecha_desde"]["width"] = "30";
		$model["fecha_desde"]["text_colour"] = "#000066";
        
        $model["fecha_hasta"]["show_grid"] = 1;
        $model["fecha_hasta"]["header_display"] = "Fecha Hasta";
        $model["fecha_hasta"]["order"] = "7";
        $model["fecha_hasta"]["width"] = "30";
		$model["fecha_hasta"]["text_colour"] = "#0000AA";
        
        $model["id_ultimo_asiento"]["show_grid"] = 1;
        $model["id_ultimo_asiento"]["header_display"] = "Ult. Asiento";
        $model["id_ultimo_asiento"]["order"] = "8";
        $model["id_ultimo_asiento"]["width"] = "30";
		$model["id_ultimo_asiento"]["text_colour"] = "#004455";
        
        $model["cerrado"]["show_grid"] = 1;
        $model["cerrado"]["header_display"] = "Cerrado";
        $model["cerrado"]["order"] = "9";
        $model["cerrado"]["width"] = "30";
		$model["cerrado"]["text_colour"] = "#770000";
     
        $model["fecha_cierre"]["show_grid"] = 1;
        $model["fecha_cierre"]["header_display"] = "Fcha. Cierre";
        $model["fecha_cierre"]["order"] = "10";
        $model["fecha_cierre"]["width"] = "30";
		$model["fecha_cierre"]["text_colour"] = "#550000";

        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "PeriodoImpositivo",
                    "content" => $model
                );
        echo json_encode($output);
         
        

?>