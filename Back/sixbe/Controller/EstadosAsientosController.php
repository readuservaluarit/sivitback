<?php


class EstadosAsientosController extends AppController {
    public $name = 'EstadosAsientos';
    public $model = 'EstadoAsiento';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    
    /**
     * @secured(CONSULTA_ESTADO_ASIENTO)
     */
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
        $nombre = strtolower($this->getFromRequestOrSession('EstadoAsiento.d_ESTADO_ASIENTO'));
        
        $conditions = array(); 
        if ($nombre != "") {
            $conditions = array('LOWER(EstadoAsiento.d_ESTADO_ASIENTO) LIKE' => '%' . $nombre . '%');
        }

        
       array_push($conditions,array('EstadoAsiento.activo ' => 1));
        
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de EstadoAsientosAsientos', 'Datos de los EstadoAsientosAsientos', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_ESTADO_ASIENTO', 'Nombre de EstadoAsiento', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'EstadoAsiento.id', "10%");
                $formBuilder->addHeader('Nombre', 'EstadoAsiento.d_moneda', "50%");
             

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_ESTADO_ASIENTO');
        
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        $this->data = $data;
        
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->EstadoAsiento->id = $id;
            $this->request->data = $this->EstadoAsiento->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('EstadoAsiento');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de EstadoAsiento');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de EstadoAsiento');
        else
            $formBuilder->setTituloForm('Consulta de EstadoAsiento');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_ESTADO_ASIENTO', 'Nombre de EstadoAsiento', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
     
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'EstadoAsientoAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('EstadoAsientoDEstadoAsiento', 'Debe ingresar un nombre');
        


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_ESTADO_ASIENTO,READONLY_PROTECTED)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            if ($this->EstadoAsiento->save($this->request->data))
                $this->Session->setFlash('El EstadoAsiento ha sido creada exitosamente.', 'success'); 
            else
                $this->Session->setFlash('Ha ocurrido un error, el EstadoAsiento no ha podido ser creada.', 'error');

            $this->redirect(array('action' => 'index'));
        } 

        $this->redirect(array('action' => 'abm', 'A'));                   
    }

    /**
    * @secured(MODIFICACION_ESTADO_ASIENTO,READONLY_PROTECTED)
    */
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->EstadoAsiento->id = $id;
            
            try{ 
                if ($this->EstadoAsiento->saveAll($this->request->data)){
                    if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "El EstadoAsiento ha sido modificado exitosamente",
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash('El EstadoAsiento ha sido modificado exitosamente.', 'success');
                        $this->redirect(array('controller' => 'Monedas', 'action' => 'index'));
                        
                    } 
                }
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, el EstadoAsiento no ha podido modificarse.', 'error');
                
            }
            $this->redirect(array('action' => 'index'));
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }

    
    /**
     * @secured(CONSULTA_ESTADO_ASIENTO)
     */
    public function getModel($vista='default'){
    	
    	$model = parent::getModelCamposDefault();//esta en APPCONTROLLER
    	$model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    	
    }
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    	
    	$model =  parent::setDefaultFieldsForView($model); // esta en APPCONTROLLER
    	//$model = parent::SetFieldForView($model,"nro_comprobante","show_grid",1);
    	
    	
    	$this->set('model',$model);
    	Configure::write('debug',0);
    	$this->render($vista);
    }
   


}
?>