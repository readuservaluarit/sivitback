<?php
    class AsientosCuentaContableController extends AppController {
        public $name = 'AsientosCuentaContable';
        public $model = 'AsientoCuentaContable';
        public $helpers = array ('Session', 'Paginator', 'Js');
        public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
        public $traigo_items = 0;

        private function RecuperaFiltros($considera_fecha=1)
        {
            $conditions = array(); 

            $id_asiento = strtolower($this->getFromRequestOrSession('AsientoCuentaContable.id_asiento'));
            
            $id_cuenta_contable = strtolower($this->getFromRequestOrSession('AsientoCuentaContable.id_cuenta_contable'));
            
            $id_cuenta_contable = strtolower($this->getFromRequestOrSession('AsientoCuentaContable.id_cuenta_contable'));
            
            $fecha_desde = strtolower($this->getFromRequestOrSession('Asiento.fecha_desde'));
            $fecha_hasta = strtolower($this->getFromRequestOrSession('Asiento.fecha_hasta'));
            $id_ejercicio_contable = strtolower($this->getFromRequestOrSession('Asiento.id_ejercicio_contable'));
            $id_asiento_efecto = strtolower($this->getFromRequestOrSession('Asiento.id_asiento_efecto'));
            $codigo_cuenta_contable_desde = strtolower($this->getFromRequestOrSession('Asiento.codigo_cuenta_contable_desde'));
            $codigo_cuenta_contable_hasta = strtolower($this->getFromRequestOrSession('Asiento.codigo_cuenta_contable_hasta'));
            $id_comprobante = strtolower($this->getFromRequestOrSession('Asiento.id_comprobante'));
            $id_periodo_contable = strtolower($this->getFromRequestOrSession('Asiento.id_periodo_contable'));
            $id_tipo_comprobante = strtolower($this->getFromRequestOrSession('Asiento.id_tipo_comprobante'));
            $id_persona = strtolower($this->getFromRequestOrSession('Asiento.id_persona'));
            $razon_social = strtolower($this->getFromRequestOrSession('Asiento.razon_social'));
            $nro_comprobante = strtolower($this->getFromRequestOrSession('Asiento.nro_comprobante'));
            $id_tipo_asiento = strtolower($this->getFromRequestOrSession('Asiento.id_tipo_asiento'));
            $id_estado_asiento = $this->getFromRequestOrSession('Asiento.id_estado_asiento');
            $id_punto_venta = strtolower($this->getFromRequestOrSession('Asiento.id_punto_venta'));
            $codigo = $this->getFromRequestOrSession('Asiento.codigo');
            
            
           // $id_asiento = 1265;
            
            
            if ($id_asiento != "") 
                array_push($conditions, array('AsientoCuentaContable.id_asiento =' => $id_asiento));
                
              
             
           if ($codigo != "") 
            array_push($conditions, array('Asiento.codigo LIKE ' => '%' .$codigo .'%' ));

            if ($id_tipo_asiento != "") 
                array_push($conditions, array('Asiento.id_tipo_asiento ' => $id_tipo_asiento));    
            
            if ($id_cuenta_contable != "") 
                array_push($conditions, array('AsientoCuentaContable.id_cuenta_contable =' => $id_cuenta_contable));
                
            if ($id_cuenta_contable != "") 
                array_push($conditions, array('AsientoCuentaContable.id_cuenta_contable ' => $id_cuenta_contable));

            if ($id_asiento_efecto != "") 
                array_push($conditions, array('Asiento.id_asiento_efecto ' => $id_asiento_efecto));
                
            if ($id_ejercicio_contable != "") 
                array_push($conditions, array('Asiento.id_ejercicio_contable ' => $id_ejercicio_contable));    
                
            
            if ($codigo_cuenta_contable_desde != "" && $id_cuenta_contable=="") 
                array_push($conditions, array('CuentaContable.codigo >=' => $codigo_cuenta_contable_desde));

            if ($codigo_cuenta_contable_hasta != "" && $id_cuenta_contable=="") 
                array_push($conditions, array('CuentaContable.codigo <=' => $codigo_cuenta_contable_hasta));  
                
              
            if ($id_comprobante != "") 
                array_push($conditions, array('Asiento.id_comprobante ' => $id_comprobante ));

            if ($id_periodo_contable != "") 
                array_push($conditions, array('Asiento.id_periodo_contable ' => $id_periodo_contable )); 
                
            if ($id_persona != "") 
                array_push($conditions, array('Comprobante.id_persona ' => $id_persona ));
                
            if ($id_tipo_comprobante != "") 
                array_push($conditions, array('Comprobante.id_tipo_comprobante ' => $id_tipo_comprobante ));
                
            if ($nro_comprobante != "") 
                array_push($conditions, array('Comprobante.nro_comprobante LIKE' => '%' .$nro_comprobante .'%'));         
             
            if ($id_punto_venta != "") 
                array_push($conditions, array('Comprobante.id_punto_venta ' => $id_punto_venta ));
                
                
            if ($id_estado_asiento != "") 
                array_push($conditions, array('Asiento.id_estado_asiento ' => $id_estado_asiento ));    
                
           
            if($considera_fecha == 1){
                	
                	
                	if ($fecha_desde != "")
                		array_push($conditions, array('DATE(Asiento.fecha) >= ' => $fecha_desde));
                		
                	if ($fecha_hasta != "")
                		array_push($conditions, array('DATE(Asiento.fecha) <= ' => $fecha_hasta));    
                	
                		
                }
           

            return $conditions; 
        }

        /**
        * @secured(CONSULTA_ASIENTO)
        */
        public function index() 
        {
            if($this->request->is('ajax'))
                $this->layout = 'ajax';

            $this->loadModel("AsientoCuentaContable");
            //$this->PaginatorModificado->settings = array('limit' => 100, 'update' => 'main-content', 'evalScripts' => true);

            //Recuperacion de Filtros
            $conditions = $this->RecuperaFiltros();

            /*
            $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
            );*/
            if($this->RequestHandler->ext != 'json'){  
                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();

                $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));

                //Headers
                $formBuilder->addHeader('id', 'cotizacion.id', "10%");
                $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
                $formBuilder->addHeader('CUIT', 'cotizacion.cuit', "20%");
                $formBuilder->addHeader('Email', 'cliente.email', "30%");
                $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'razon_social');
                $formBuilder->addField($this->model, 'cuit');
                $formBuilder->addField($this->model, 'email');
                $formBuilder->addField($this->model, 'tel');

                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');

                //vista formBuilder
            }
            else
            { // vista json

                $data = $this->AsientoCuentaContable->find('all', array(
                        'conditions' => $conditions,
                        'contain' => array("Asiento"=>array("Comprobante"=>array("TipoComprobante","PuntoVenta","Persona")),
													"CuentaContable","CentroCosto","AsientoCuentaContableCentroCostoItem"),
                        //'contain' => array("Asiento","CuentaContable","CentroCosto"),
                        'order'=>array("Asiento.codigo asc,AsientoCuentaContable.id asc")
                    ));
                //$this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
                /*$page_count = $this->params['paging'][$this->model]['pageCount'];*/
                /*               $data = $this->AsientoCuentaContable->find('all', array(
                'conditions' => $conditions,
                'contain' => array("Asiento","CuentaContable"),
                'order'=>array("AsientoCuentaContable.id desc")
                ));                                                */
                //$this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
                $page_count = 0; //$this->params['paging'][$this->model]['pageCount'];
                $traigo_items_filtro = $this->getFromRequestOrSession('AsientoCuentaContable.item');
                $traigo_items_filtro = 1;
                $this->loadModel("Comprobante");
                
                $id_cuenta_contable = $this->getFromRequestOrSession('AsientoCuentaContable.id_cuenta_contable');
                $consulta_conciliacion = $this->getFromRequestOrSession('AsientoCuentaContable.consulta_conciliacion');
                //$id_cuenta_contable = 21;
                //$consulta_conciliacion = 1;
                
                $array_conciliados = array();
                
                if($consulta_conciliacion !=""){
                    $array_conciliados = $this->getConciliados($id_cuenta_contable);//devuelve un array con los id_asiento_cuenta_contable
                    $array_id_conciliados_acc = $this->getIdAsientoCuentaContablemportacionConciliados($array_conciliados);//devuelve array con los id de la tabla ACC conciliados con el csv
                 }
                 
                 $id_anterior = -1;
                 $id_actual = 0;
                foreach($data as &$record)
                {       
                	
                	
                	
                    //Toda la logica de mapeo se basan en el model. por eso se subu un nivel a [AsientoCuentaContable ]
                    
                    $id_actual = $record["AsientoCuentaContable"]["id_asiento"];
                    
                    
                    
                    
                  
                  $record["AsientoCuentaContable"]["d_asiento"] =             $record["Asiento"]["d_asiento"];
   
                        
                        
                    $record["AsientoCuentaContable"]["codigo_asiento"] =        $record["Asiento"]["codigo"];
                    
                    
                    
                    if($consulta_conciliacion !="" && in_array($record["AsientoCuentaContable"]["id"],$array_id_conciliados_acc) ){
                
                        $record["AsientoCuentaContable"]["conciliado"] =  1;
                    }
            
                    
                    
                    
                    
                    $record["AsientoCuentaContable"]["observacion"] =            $record["Asiento"]["observacion"];
                    if($record["Asiento"]["id_comprobante"] > 0)
                    $record["AsientoCuentaContable"]["tipo_comprobante"] =      $record["Asiento"]["Comprobante"]["TipoComprobante"]["codigo_tipo_comprobante"];
                    
                    
                    if($record["Asiento"]["id_comprobante"] > 0){
                    	
                    	
                    	
                        if($record["Asiento"]["Comprobante"]["id_punto_venta"]>0)
                            $record["AsientoCuentaContable"]["pto_nro_comprobante"] =      (string) $this->Comprobante->GetNumberComprobante($record["Asiento"]["Comprobante"]['PuntoVenta']['numero'],$record["Asiento"]["Comprobante"]['nro_comprobante']);
                        else
                           $record["AsientoCuentaContable"]["pto_nro_comprobante"] =      (string) $this->Comprobante->GetNumberComprobante($record["Asiento"]["Comprobante"]['d_punto_venta'],$record["Asiento"]["Comprobante"]['nro_comprobante']);
                       
                           
                         
                    }   
                       
                       
                    $record["AsientoCuentaContable"]["codigo_cuenta_contable"] =                $record["CuentaContable"]["codigo"];
                    $record["AsientoCuentaContable"]["d_cuenta_contable"] =     $record["CuentaContable"]["d_cuenta_contable"];
                    
                    $record["AsientoCuentaContable"]["nro_asiento"] =   $record["Asiento"]["codigo"];
                    $record["AsientoCuentaContable"]["fecha_asiento"] = $record["Asiento"]["fecha"];
                                            
                    //Subir un nivel datos propios del Asiento (cabecera)
                    $record["AsientoCuentaContable"]["id_estado_asiento"] =     $record["Asiento"]["id_estado_asiento"];
                    $record["AsientoCuentaContable"]["id_ejercicio_contable"] = $record["Asiento"]["id_ejercicio_contable"];
                    $record["AsientoCuentaContable"]["id_tipo_asiento"] =       $record["Asiento"]["id_tipo_asiento"];
                    $record["AsientoCuentaContable"]["id_clase_asiento"] =       $record["Asiento"]["id_clase_asiento"];

                    $record["AsientoCuentaContable"]["debe_cuenta"] =    "";
                    $record["AsientoCuentaContable"]["debe_concepto"] =  "";
                    $record["AsientoCuentaContable"]["debe_monto"] =     "";  

                    $record["AsientoCuentaContable"]["haber_cuenta"] =    "";
                    $record["AsientoCuentaContable"]["haber_concepto"] =  "";
                    $record["AsientoCuentaContable"]["haber_monto"] =     "";

                    if($record["AsientoCuentaContable"]["es_debe"] || $record["AsientoCuentaContable"]["es_debe"] == 1)
                    {
                        $record["AsientoCuentaContable"]["debe_cuenta"] =     $record["CuentaContable"]["codigo"];
                        $record["AsientoCuentaContable"]["debe_concepto"] =   $record["CuentaContable"]["d_cuenta_contable"];
                        $record["AsientoCuentaContable"]["debe_monto"] =      $record["AsientoCuentaContable"]["monto"];                                                                                       
                    }
                    else
                    {
                        $record["AsientoCuentaContable"]["haber_cuenta"] =    $record["CuentaContable"]["codigo"];
                        $record["AsientoCuentaContable"]["haber_concepto"] =  $record["CuentaContable"]["d_cuenta_contable"];
                        $record["AsientoCuentaContable"]["haber_monto"] =     $record["AsientoCuentaContable"]["monto"];                                                                                          
                    }
                    
                  
                    if($traigo_items_filtro == '' || $traigo_items_filtro == 0){
                        
                     unset($record["AsientoCuentaContableCentroCostoItem"]);
                        
                    }else{
                        
                        
                        $record["AsientoCuentaContable"]["AsientoCuentaContableCentroCostoItem"] =  $record["AsientoCuentaContableCentroCostoItem"]; 
                        unset($record["AsientoCuentaContableCentroCostoItem"]);
                    }
                    
                    

                    $record["AsientoCuentaContable"]["es_debe"] =  (string)((int)($record["AsientoCuentaContable"]["es_debe"]));
                    
                    unset($record["Asiento"]);
                    unset($record["CentroCosto"]);
                    //unset($record["CuentaContable"]);
                    
                    $id_anterior = $id_actual;
                
                    
                    
                    
                    
                }       

                $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "list",
                    "content" => $data,
                    "page_count" =>$page_count
                );
                $this->set($output);
                $this->set("_serialize", array("status", "message","page_count", "content"));
            }
            //fin vista json
        }
        
        
          /**
        * @secured(CONSULTA_ASIENTO)
        */
        public function LibroDiario(){

         

            $this->loadModel("AsientoCuentaContable");
            $this->loadModel("Comprobante");
            $this->loadModel("Asiento");
            $this->loadModel("DatoEmpresa");

            $conditions = $this->RecuperaFiltros();
            
            $id_moneda = $this->getFromRequestOrSession('Asiento.id_moneda');
            $id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda);
            $campo_valor = $this->AsientoCuentaContable->getFieldByTipoMoneda($id_tipo_moneda);//obtengo el campo en la cual expresar el reporte

         


            $data = $this->AsientoCuentaContable->find('all', array(
                      'joins' => array( 
                        array(
                            'table' => 'comprobante',
                            'alias' => 'Comprobante',
                            'type' => 'LEFT',
                            'conditions' => array('Comprobante.id = Asiento.id_comprobante')
                        )),
                    'conditions' => $conditions,
                    //'contain' =>array('Comprobante'=>array('PuntoVenta','EstadoComprobante','Moneda'),'Producto'),
                    'contain' => array("Asiento"=> array("EstadoAsiento","Comprobante"=>array('TipoComprobante','PuntoVenta')),"CuentaContable","CentroCosto"),
                    'order'=>array("Asiento.codigo asc")
                ));
                
                
        
        
            $comprobante = new Comprobante() ;

            $id_actual = 0;
            $id_anterior = -1;
            foreach($data as &$record)
            {   
            
                $this->Asiento->formatearFechas($record);      
                //Toda la logica de mapeo se basan en el model. por eso se subu un nivel a [AsientoCuentaContable ]
                
                $id_actual = $record["Asiento"]["id"];
                
               
            
                if($id_actual!=$id_anterior){
                    $record["AsientoCuentaContable"]["d_asiento"] =     $record["Asiento"]["d_asiento"];
                     $record["AsientoCuentaContable"]["nro_asiento"] =   $record["Asiento"]["codigo"];
                     $record["AsientoCuentaContable"]["fecha"] = $record["Asiento"]["fecha"];
                     $record["AsientoCuentaContable"]["d_estado_asiento"] =      $record["Asiento"]["EstadoAsiento"]["d_estado_asiento"];
                }else{
                    $record["AsientoCuentaContable"]["d_asiento"] = "";
                     $record["AsientoCuentaContable"]["nro_asiento"] =   "";
                     $record["AsientoCuentaContable"]["fecha"] = "";
                     $record["AsientoCuentaContable"]["d_estado_asiento"] = "";
                }
                
        
                
               
                
                

                //Subir un nivel datos propios del Asiento (cabecera)
                $record["AsientoCuentaContable"]["id_estado_asiento"] =     $record["Asiento"]["id_estado_asiento"];
                

                $record["AsientoCuentaContable"]["id_ejercicio_contable"] = $record["Asiento"]["id_ejercicio_contable"];

                $record["AsientoCuentaContable"]["id_tipo_asiento"] =       $record["Asiento"]["id_tipo_asiento"];
                //$record["AsientoCuentaContable"]["d_tipo_asiento"] =       $record["Asiento"]["d_tipo_asiento"];
                $record["AsientoCuentaContable"]["id_clase_asiento"] =       $record["Asiento"]["id_clase_asiento"];
                //$record["AsientoCuentaContable"]["d_clase_asiento"] =       $record["Asiento"]["d_clase_asiento"];
                $record["AsientoCuentaContable"]["observacion"] =       $record["Asiento"]["observacion"];

                $record["AsientoCuentaContable"]["debe_cuenta"] =    "";
                $record["AsientoCuentaContable"]["debe_concepto"] =  "";
                $record["AsientoCuentaContable"]["debe_monto"] =     "";  

                $record["AsientoCuentaContable"]["haber_cuenta"] =    "";
                $record["AsientoCuentaContable"]["haber_concepto"] =  "";
                $record["AsientoCuentaContable"]["haber_monto"] =     "";

                
                
                
                if($record["AsientoCuentaContable"]["es_debe"])
                {
                    $record["AsientoCuentaContable"]["debe_cuenta"] =     $record["CuentaContable"]["codigo"];
                    $record["AsientoCuentaContable"]["debe_concepto"] =   $record["CuentaContable"]["d_cuenta_contable"];
                    $record["AsientoCuentaContable"]["debe_monto"] =   (string)  round($record["AsientoCuentaContable"]["monto"],4);                                                                                        
                }
                else
                {
                    $record["AsientoCuentaContable"]["haber_cuenta"] =    $record["CuentaContable"]["codigo"];
                    $record["AsientoCuentaContable"]["haber_concepto"] =  $record["CuentaContable"]["d_cuenta_contable"];
                    $record["AsientoCuentaContable"]["haber_monto"] =  (string)   round($record["AsientoCuentaContable"]["monto"],4);                                                                                           
                }
                
                
                
                if(isset($record["Asiento"]["Comprobante"])){
                    
                    if(isset($record["Asiento"]["Comprobante"]["PuntoVenta"]["numero"]))
                        $punto_venta = $record["Asiento"]["Comprobante"]["PuntoVenta"]["numero"];
                    else
                        $punto_venta = '';
                        
                        
                    
                    if(isset($record["Asiento"]["Comprobante"]["TipoComprobante"])){
                            $record["AsientoCuentaContable"]["d_comprobante"] = $record["Asiento"]["Comprobante"]["TipoComprobante"]["codigo_tipo_comprobante"].'-'. $comprobante->GetNumberComprobante($punto_venta,$record["Asiento"]["Comprobante"]["nro_comprobante"]);
                            $record["AsientoCuentaContable"]["razon_social"] = $record["Asiento"]["Comprobante"]["razon_social"];
                    }
                
               }else{
                   $record["AsientoCuentaContable"]["razon_social"] = ''; 
                   
                   
               } 
               
               $id_anterior = $id_actual;
                
                //Sobre escribo
                $record["AsientoCuentaContable"]["es_debe"] =  (string)((int)($record["AsientoCuentaContable"]["es_debe"]));
                unset($record["EstadoAsiento"]);
                unset($record["Asiento"]);
                unset($record["CentroCosto"]);
                unset($record["CuentaContable"]);
                unset($record["Asiento"]);
                unset($record["Comprobante"]);
            }

            //$this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
            //$page_count = $this->params['paging'][$this->model]['pageCount'];

            
            $this->data = $data;
            
            $output = array(
                "status" =>EnumError::SUCCESS,
                "message" => "list",
                "content" => $data,
                "page_count" =>"0"
            );

         
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content","page_count"));
        }
        
        
        
        
        /**
         * @secured(CONSULTA_ASIENTO)
         */
        public function LibroDiariolXls($vista="default",$metodo="LibroDiario",$titulo=""){
        	
        	$this->xlsReport =1;
        	$this->model = "AsientoCuentaContable";
        	$this->ExportarExcel("libro_diario","LibroDiario","Libro Diario"); 
        	
        	
        	
        }
        
        
        
        
        public function MayorDeCuentas(){

         

            $this->loadModel("AsientoCuentaContable");
            $this->loadModel("Asiento");
            $this->loadModel("Comprobante");
            $this->loadModel("CuentaContable");
            $this->loadModel("EjercicioContable");
            $this->loadModel("DatoEmpresa");

            $conditions = $this->RecuperaFiltros();
            
            $id_moneda = $this->getFromRequestOrSession('Asiento.id_moneda');
            $id_tipo_moneda = $this->DatoEmpresa->getNombreCampoMonedaPorIdMoneda($id_moneda);
            $campo_valor = $this->AsientoCuentaContable->getFieldByTipoMoneda($id_tipo_moneda);//obtengo el campo en la cual expresar el reporte
            
            $d_cuenta_contable='';
         

           // $conditions = array_merge($conditions,array("AsientoCuentaContable.id_cuenta_contable=5"));
            
        
            $id_cuenta_contable = $this->getFromRequestOrSession('AsientoCuentaContable.id_cuenta_contable');
          
            
            $cuenta_contable = $this->CuentaContable->find('first', array(
                 
                    'conditions' => array('CuentaContable.id ' => $id_cuenta_contable ),
                    )
                    
                    );
                    
            
            if($cuenta_contable){
            	
            	
            	$conditions_saldo_inicial = array();
            	
            	
            	
            	
            	$conditions_saldo_inicial = $this->RecuperaFiltros(0);
            	
            	$fecha_desde = $this->EjercicioContable->getFechaInicio($this->getFromRequestOrSession('Asiento.id_ejercicio_contable'));//fecha de inicio del ejercicio
            	$fecha_hasta = $this->getFromRequestOrSession('Asiento.fecha_desde');//fecha de inicio del filtro
            	
            	array_push($conditions_saldo_inicial, array('DATE(Asiento.fecha) >= ' => $fecha_desde));
            	array_push($conditions_saldo_inicial, array('DATE(Asiento.fecha) < ' => $fecha_hasta));    
            	
            	
            	
            	
            	
            	
            	
            	
            	
                
               $codigo_cuenta_contable = $cuenta_contable["CuentaContable"]["codigo"];      
               $d_cuenta_contable = $cuenta_contable["CuentaContable"]["d_cuenta_contable"];      
                    
               $asientos = $this->AsientoCuentaContable->find('all', array(
                            //'fields'=>array("AsientoCuentaContable.monto"),  
                            'conditions' => $conditions,
                            //'contain' =>array('Comprobante'=>array('PuntoVenta','EstadoComprobante','Moneda'),'Producto'),
                            'contain' => array("CuentaContable","Asiento"=>array("Comprobante"=>array("Persona"))),
                    		'order'=>'Asiento.fecha ASC'
                            
                    
                          
                        ));
               
               
               
               
                $id_cuenta_contable = $this->getFromRequestOrSession('AsientoCuentaContable.id_cuenta_contable');
                
                
                
                
                $saldo_inicial = $this->getFromRequestOrSession('AsientoCuentaContable.saldo_inicial');
                        
                 
               	
               
               	
               	
				
                if($saldo_inicial == 1){
                	
                	$asientos_saldo_inicial_debe = $this->AsientoCuentaContable->find('all', array(
                			'fields'=>array("SUM(IF(AsientoCuentaContable.es_debe=1,AsientoCuentaContable.monto,0)) as monto"),
                			'conditions' => $conditions_saldo_inicial,
                			//'contain' =>array('Comprobante'=>array('PuntoVenta','EstadoComprobante','Moneda'),'Producto'),
                			'contain' => array("CuentaContable","Asiento"=>array("Comprobante"=>array("Persona"))),
                			'order'=>'Asiento.fecha ASC'
                			
                			
                			
                	));
                	
                	
                	
                	$asientos_saldo_inicial_haber = $this->AsientoCuentaContable->find('all', array(
                			'fields'=>array("SUM(IF(AsientoCuentaContable.es_debe=0,AsientoCuentaContable.monto,0)) as monto"),
                			'conditions' => $conditions_saldo_inicial,
                			//'contain' =>array('Comprobante'=>array('PuntoVenta','EstadoComprobante','Moneda'),'Producto'),
                			'contain' => array("CuentaContable","Asiento"=>array("Comprobante"=>array("Persona"))),
                			'order'=>'Asiento.fecha ASC'
                	));
                	
                
	               	$primer_elemento_array["CuentaContable"]["codigo"] = $codigo_cuenta_contable;
	               	$primer_elemento_array["Asiento"]["d_asiento"] = "Saldo Inicial Haber";
	               	$primer_elemento_array["Asiento"]["codigo"] = "";
	               	$primer_elemento_array["Asiento"]["fecha"] = "";
	               	$primer_elemento_array["AsientoCuentaContable"]["monto"] = abs($asientos_saldo_inicial_haber[0][0]["monto"]);
	               	$primer_elemento_array["AsientoCuentaContable"]["es_debe"] = 0;
	               	array_unshift($asientos,$primer_elemento_array);//agrego el saldo inicial como primer elemento
	               	
	               	
	               	$primer_elemento_array["CuentaContable"]["codigo"] = $codigo_cuenta_contable;
	               	$primer_elemento_array["Asiento"]["d_asiento"] = "Saldo Inicial Debe";
	               	$primer_elemento_array["Asiento"]["codigo"] = "";
	               	$primer_elemento_array["Asiento"]["fecha"] = "";
	               	$primer_elemento_array["AsientoCuentaContable"]["monto"] = abs($asientos_saldo_inicial_debe[0][0]["monto"]);
	               	$primer_elemento_array["AsientoCuentaContable"]["es_debe"] = 1;
	               	array_unshift($asientos,$primer_elemento_array);//agrego el saldo inicial como primer elemento
               	
                }
               	
               	
                        
                    $saldo_acumulado =0;   
                 
                    foreach($asientos as &$asiento){
                        
                        $this->Asiento->formatearFechas($asiento); 
                        
                        
                        if(isset($asiento["Asiento"]["Comprobante"]["Persona"]) && isset($asiento["Asiento"]["Comprobante"]["id_persona"])>0){
                        	
                        	$asiento["AsientoCuentaContable"]["razon_social"] = $asiento["Asiento"]["Comprobante"]["Persona"]["razon_social"];
                        }
                        
                         $asiento["AsientoCuentaContable"]["codigo_cuenta_contable"] = (string) $asiento["CuentaContable"]["codigo"];
                         $asiento["AsientoCuentaContable"]["d_asiento"] = (string) $asiento["Asiento"]["d_asiento"];
                         $asiento["AsientoCuentaContable"]["fecha"] = (string) $asiento["Asiento"]["fecha"] ;
                         $asiento["AsientoCuentaContable"]["nro_asiento"] = $asiento["Asiento"]["codigo"];
                         
                         
                        if($asiento["AsientoCuentaContable"]["es_debe"] == 1){
                            
                           $asiento["AsientoCuentaContable"]["debe_monto"] = (string) $asiento["AsientoCuentaContable"]["monto"]; 
                           $asiento["AsientoCuentaContable"]["haber_monto"] = (string) 0.00;
                           
                        }else{
                            
                            $asiento["AsientoCuentaContable"]["haber_monto"] = (string)   $asiento["AsientoCuentaContable"]["monto"]; 
                             $asiento["AsientoCuentaContable"]["debe_monto"] = (string)  0.00; 
                        }
                        
                         $saldo_acumulado = $saldo_acumulado +   $asiento["AsientoCuentaContable"]["debe_monto"] -     $asiento["AsientoCuentaContable"]["haber_monto"];
                         $asiento["AsientoCuentaContable"]["saldo_acumulado"] = (string) $saldo_acumulado;    
                             
                        
                        
                        
                         
                        
                    }
                    
                   
                  
                    
                    
                  
                    
              
                    $error = EnumError::SUCCESS;
                    $message = "list";
                    
            }else{
             $error = EnumError::ERROR;   
             $message = "La cuenta contable elegida no existe"; 
             $asientos = "";  
            }
            
            $this->data = $asientos;
             
             $output = array(
                "status" =>$error ,
                "message" => $message,
                "content" => $asientos,
                "page_count" =>"0"
            );

           
           // ob_start();
           /* echo json_encode($output);
            die(); 
            */
           $this->set($output);
                $this->set("_serialize", array("status", "message", "content","page_count"));
            
            
            }
            
            
            
            public function MayorDeCuentasXls(){
             
                 $this->xlsReport =1;
                 $this->model = "AsientoCuentaContable";
                 $this->ExportarExcel("mayor_de_cuentas","MayorDeCuentas","Mayor de Cuentas");   
            }
        
        /*probar con el save() All de Asientos y el save all de AsientoCuentaContable*/
        protected function add() {
            if ($this->request->is('post')){
                $this->loadModel($this->model);

                try{
                    if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
                        $mensaje = "El Item ha sido creado exitosamente";
                        $tipo = EnumError::SUCCESS;

                    }else{
                        $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.";
                        $tipo = EnumError::ERROR; 

                    }
                }catch(Exception $e){

                    $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.".$e->getMessage();
                    $tipo = EnumError::ERROR;
                }
                $output = array(
                    "status" => $tipo,
                    "message" => $mensaje,
                    "content" => ""
                );
                //si es json muestro esto
                if($this->RequestHandler->ext == 'json'){ 
                    $this->set($output);
                    $this->set("_serialize", array("status", "message", "content"));
                }else{

                    $this->Session->setFlash($mensaje, $tipo);
                    $this->redirect(array('action' => 'index'));
                }     

            }

            //si no es un post y no es json
            if($this->RequestHandler->ext != 'json')
                $this->redirect(array('action' => 'abm', 'A'));   


        }

        protected function edit($id) {


            if (!$this->request->is('get')){

                $this->loadModel($this->model);
                $this->{$this->model}->id = $id;

                try{ 
                    if ($this->{$this->model}->saveAll($this->request->data)){

                        $mensaje = "El Item Ha sido modificado correctamente.";
                        $tipo = EnumError::ERROR;

                    }else{
                        $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.";
                        $tipo = EnumError::ERROR; 

                    }
                }catch(Exception $e){

                    $mensaje = "Ha ocurrido un error,el Item NO ha podido ser creado.".$e->getMessage();
                    $tipo = EnumError::ERROR; 

                }

            } 
            if($this->RequestHandler->ext == 'json'){  
                $output = array(
                    "status" => $tipo,
                    "message" =>$mensaje,
                    "content" => ""
                ); 

                $this->set($output);
                $this->set("_serialize", array("status", "message", "content"));
            }else{
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('controller' => 'CotizacionItems', 'action' => 'index'));

            } 

        }

        /**
        * @secured(CONSULTA_ASIENTO)
        */ 
        public function getModel($vista = 'default'){

            $model = parent::getModelCamposDefault();//esta en APPCONTROLLER

            $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL

        }

        private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla

            $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
            $this->set('model',$model);
            // $this->set('this',$this);
            Configure::write('debug',0);
            $this->render($vista);
        }
    }
?>