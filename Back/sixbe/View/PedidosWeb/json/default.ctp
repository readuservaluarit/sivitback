<?php



        include(APP.'View'.DS.'Comprobantes'.DS.'json'.DS.'default.ctp');
		// CONFIG  CON MODE DISPLAYED CELLS
        $model["pto_nro_comprobante"]["show_grid"] = 1;
        $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
        $model["pto_nro_comprobante"]["minimun_width"] = "90"; //REstriccion mas de esto no se puede achicar
        $model["pto_nro_comprobante"]["web_width"] = "20";
        $model["pto_nro_comprobante"]["order"] = "1";
        $model["pto_nro_comprobante"]["text_colour"] = "#000000";


		

        $model["fecha_generacion"]["show_grid"] = 1;
        $model["fecha_generacion"]["header_display"] = "Fecha Creaci&oacute;n";
        $model["fecha_generacion"]["order"] = "2";
        $model["fecha_generacion"]["web_width"] = "5";
        $model["fecha_generacion"]["text_colour"] = "#0000FF";
        
        
        
        
        $model["codigo_persona"]["show_grid"] = 1;
        $model["codigo_persona"]["header_display"] = "Nro Cliente";
        $model["codigo_persona"]["order"] = "3";
        $model["codigo_persona"]["web_width"] = "5";
        $model["codigo_persona"]["text_colour"] = "#000000";
        
        
        
        
        
        $model["razon_social"]["show_grid"] = 1;
        $model["razon_social"]["header_display"] = "Raz&oacute;n social";
        $model["razon_social"]["web_width"] = "20";
        $model["razon_social"]["order"] = "4";
        $model["razon_social"]["text_colour"] = "#000000";
        
        
        
        
        $model["meli_nickname"]["show_grid"] = 1;
        $model["meli_nickname"]["header_display"] = "Apodo";
        $model["meli_nickname"]["web_width"] = "5";
        $model["meli_nickname"]["order"] = "12";
        $model["meli_nickname"]["text_colour"] = "#770077";
        
        
        
        
        $model["orden_compra_externa"]["show_grid"] = 1;
        $model["orden_compra_externa"]["header_display"] = "Numero de Compra";
        $model["orden_compra_externa"]["web_width"] = "30";
        $model["orden_compra_externa"]["order"] = "5";
        $model["orden_compra_externa"]["text_colour"] = "#004400";
        
        
        
        
        $model["fecha_aprobacion"]["show_grid"] = 1;
        $model["fecha_aprobacion"]["header_display"] = "Fecha Aprobaci&oacute;n";
        $model["fecha_aprobacion"]["web_width"] = "10";
        $model["fecha_aprobacion"]["order"] = "7";
        $model["fecha_aprobacion"]["text_colour"] = "#0000FF";
        
        
        
        
        $model["fecha_entrega"]["show_grid"] = 1;
        $model["fecha_entrega"]["header_display"] = "Fecha Entrega";
        $model["fecha_entrega"]["web_width"] = "10";
        $model["fecha_entrega"]["order"] = "8";
        $model["fecha_entrega"]["text_colour"] = "#0000FF";
        
        
        
        
        $model["d_moneda"]["show_grid"] = 1;
        $model["d_moneda"]["header_display"] = "Moneda";
        $model["d_moneda"]["web_width"] = "5";
        $model["d_moneda"]["order"] = "9";
        $model["d_moneda"]["text_colour"] = "#F0000F";
        
        
        
        
        $model["total_comprobante"]["show_grid"] = 1;
        $model["total_comprobante"]["header_display"] = "Monto";
        $model["total_comprobante"]["web_width"] = "10";
        $model["total_comprobante"]["order"] = "10";
        $model["total_comprobante"]["text_colour"] = "#F0000F";
        
        
        
        
        $model["fecha_cierre"]["show_grid"] = 1;
        $model["fecha_cierre"]["header_display"] = "Fecha Cierre";
        $model["fecha_cierre"]["web_width"] = "10";
        $model["fecha_cierre"]["order"] = "11";
        $model["fecha_cierre"]["text_colour"] = "#0000FF";

        
        
        
        $model["d_estado_comprobante"]["show_grid"] = 1;
        $model["d_estado_comprobante"]["header_display"] = "Estado";
        $model["d_estado_comprobante"]["web_width"] = "5";
		$model["d_estado_comprobante"]["order"] = "12";
        $model["d_estado_comprobante"]["text_colour"] = "#004400";
        
        
        
        
        
        $model["d_usuario"]["show_grid"] = 1;
        $model["d_usuario"]["header_display"] = "Vendedor";
        $model["d_usuario"]["web_width"] = "5";
        $model["d_usuario"]["order"] = "13";
        $model["d_usuario"]["text_colour"] = "#770077";
        
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "Comprobante",
                    "content" => $model
                    );
        
        echo json_encode($output);
        
        
?>