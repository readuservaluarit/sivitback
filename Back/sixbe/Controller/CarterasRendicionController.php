<?php
/**
* @secured(CONSULTA_CARTERA_RENDICION)
*/
class CarterasRendicionController extends AppController {
    public $name = 'CarterasRendicion';
    public $model = 'CarteraRendicion';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
/**
* @secured(CONSULTA_CARTERA_RENDICION)
*/
    public function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);

        
        //Recuperacion de Filtros
        $d_cartera_rendicion = strtolower($this->getFromRequestOrSession('CarteraRendicion.d_cartera_rendicion'));
        $id_tipo_cartera_rendicion = strtolower($this->getFromRequestOrSession('CarteraRendicion.id_tipo_cartera_rendicion'));
        $id_cuenta_contable = strtolower($this->getFromRequestOrSession('CarteraRendicion.id_cuenta_contable'));
        
        $conditions = array(); 
        
        if ($d_cartera_rendicion != "") {
            array_push($conditions, array('LOWER(CarteraRendicion.d_cartera_rendicion) LIKE' => '%' . $d_cartera_rendicion . '%'));
        }
        
        if ($id_tipo_cartera_rendicion != "") 
             array_push($conditions, array('CarteraRendicion.id_tipo_cartera_rendicion =' => $id_tipo_cartera_rendicion));
             
        if ($id_cuenta_contable != "") {
             array_push($conditions, array('CarteraRendicion.id_cuenta_contable' => $id_cuenta_contable));    
        }

        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            'contain' => array('TipoCarteraRendicion','CuentaContable'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de CarteraRendicions', 'Datos de las CarteraRendicions', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_CARTERA_RENDICION', 'Nombre', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'CarteraRendicion.id', "10%");
                $formBuilder->addHeader('Nombre', 'CarteraRendicion.d_CARTERA_RENDICION', "50%");
                $formBuilder->addHeader('Nombre', 'CarteraRendicion.cotizacion', "40%");

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_CARTERA_RENDICION');
                $formBuilder->addField($this->model, 'cotizacion');
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        
        foreach($data as &$cartera_rendicion){
            
            $cartera_rendicion["CarteraRendicion"]["d_cuenta_contable"] = $cartera_rendicion["CuentaContable"]["codigo"].' '.$cartera_rendicion["CuentaContable"]["d_cuenta_contable"];
            $cartera_rendicion["CarteraRendicion"]["d_tipo_cartera_rendicion"] =  $cartera_rendicion["TipoCarteraRendicion"]["d_tipo_cartera_rendicion"];
            
            
            $cartera_rendicion["CarteraRendicion"]["d_cartera_rendicion_extensa"] =  $cartera_rendicion["TipoCarteraRendicion"]["d_tipo_cartera_rendicion"]." - ".$cartera_rendicion["CarteraRendicion"]["d_cartera_rendicion"] ;
            
            
            unset($cartera_rendicion["TipoCarteraRendicion"]);
            unset($cartera_rendicion["CuentaContable"]);
            
        }
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
        
     }
    }


    public function abm($mode, $id = null) {


        if ($mode != "A" && $mode != "M" && $mode != "C")
            throw new MethodNotAllowedException();

        $this->layout = 'ajax';
        $this->loadModel($this->model);
        if ($mode != "A"){
            $this->CarteraRendicion->id = $id;
            $this->request->data = $this->CarteraRendicion->read();
        }


        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        //Configuracion del Formulario
        $formBuilder->setController($this);
        $formBuilder->setModelName($this->model);
        $formBuilder->setControllerName($this->name);
        $formBuilder->setTitulo('CarteraRendicion');

        if ($mode == "A")
            $formBuilder->setTituloForm('Alta de CarteraRendicion');
        elseif ($mode == "M")
            $formBuilder->setTituloForm('Modificaci&oacute;n de CarteraRendicion');
        else
            $formBuilder->setTituloForm('Consulta de CarteraRendicion');

        //Form
        $formBuilder->setForm2($this->model,  array('class' => 'form-horizontal well span6'));

        //Fields

        
        
     
        
        $formBuilder->addFormInput('d_CARTERA_RENDICION', 'Nombre', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));
        $formBuilder->addFormInput('cotizacion', 'Cotizaci&oacute;n', array('class'=>'control-group'), array('class' => 'control-group', 'label' => false));    
        
        
        $script = "
        function validateForm(){

        Validator.clearValidationMsgs('validationMsg_');

        var form = 'CarteraRendicionAbmForm';

        var validator = new Validator(form);

        validator.validateRequired('CarteraRendicionDCarteraRendicion', 'Debe ingresar un nombre');
        validator.validateRequired('CarteraRendicionCotizacion', 'Debe ingresar una cotizaci&oacute;n');


        if(!validator.isAllValid()){
        validator.showValidations('', 'validationMsg_');
        return false;   
        }

        return true;

        } 


        ";

        $formBuilder->addFormCustomScript($script);

        $formBuilder->setFormValidationFunction('validateForm()');

        $this->set('abm',$formBuilder);
        $this->set('mode', $mode);
        $this->set('id', $id);
        $this->render('/FormBuilder/abm');    
    }


    public function view($id) {        
        $this->redirect(array('action' => 'abm', 'C', $id)); 
    }

    /**
    * @secured(ADD_CARTERA_RENDICION)
    */
    public function add() {
        if ($this->request->is('post')){
            $this->loadModel($this->model);
            $id_cat = '';
            
            try{
                if ($this->CarteraRendicion->saveAll($this->request->data, array('deep' => true))){
                    $mensaje = "La Cartera Rendicion ha sido creada exitosamente";
                    $tipo = EnumError::SUCCESS;
                    $id_cat = $this->CarteraRendicion->id;
                   
                }else{
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $tipo = EnumError::ERROR; 
                    
                }
            }catch(Exception $e){
                
                $mensaje = "Ha ocurrido un error,la Cartera Rendicion no ha podido ser creada.".$e->getMessage();
                $tipo = EnumError::ERROR;
            }
             $output = array(
            "status" => $tipo,
            "message" => $mensaje,
            "content" => "",
            "id_add" =>$id_cat
            );
            //si es json muestro esto
            if($this->RequestHandler->ext == 'json'){ 
                $this->set($output);
                $this->set("_serialize", array("status", "message", "content" ,"id_add"));
            }else{
                
                $this->Session->setFlash($mensaje, $tipo);
                $this->redirect(array('action' => 'index'));
            }     
            
        }
        
        //si no es un post y no es json
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'A'));   
        
      
    }

    /**
    * @secured(MODIFICACION_CARTERA_RENDICION)
    */
     
    public function edit($id) {
        
        
        if (!$this->request->is('get')){
            
            $this->loadModel($this->model);
            $this->{$this->model}->id = $id;
            
            try{ 
                if ($this->{$this->model}->saveAll($this->request->data)){
                     $mensaje =  "La Cartera Rendicion ha sido modificada exitosamente";
                     $status = EnumError::SUCCESS;
                    
                    
                }else{   //si hubo error recupero los errores de los models
                    
                    $errores = $this->{$this->model}->validationErrors;
                    $errores_string = "";
                    foreach ($errores as $error){ //recorro los errores y armo el mensaje
                        $errores_string.= "&bull; ".$error[0]."\n";
                        
                    }
                    $mensaje = $errores_string;
                    $status = EnumError::ERROR; 
               }
               
               if($this->RequestHandler->ext == 'json'){  
                        $output = array(
                            "status" => $status,
                            "message" => $mensaje,
                            "content" => ""
                        ); 
                        
                        $this->set($output);
                        $this->set("_serialize", array("status", "message", "content"));
                    }else{
                        $this->Session->setFlash($mensaje, $status);
                        $this->redirect(array('controller' => $this->name, 'action' => 'index'));
                        
                    } 
            }catch(Exception $e){
                $this->Session->setFlash('Ha ocurrido un error, La Cartera Rendicion no ha podido modificarse.', 'error');
                
            }
           
        }else{ //si me pide algun dato me debe mandar el id
           if($this->RequestHandler->ext == 'json'){ 
             
            $this->{$this->model}->id = $id;
            //$this->{$this->model}->contain('Provincia','Pais');
            $this->request->data = $this->{$this->model}->read();          
            
            $output = array(
                            "status" =>EnumError::SUCCESS,
                            "message" => "list",
                            "content" => $this->request->data
                        );   
            
            $this->set($output);
            $this->set("_serialize", array("status", "message", "content")); 
          }else{
                
                 $this->redirect(array('action' => 'abm', 'M', $id));
                 
            }
            
            
        } 
        
        if($this->RequestHandler->ext != 'json')
            $this->redirect(array('action' => 'abm', 'M', $id));
    }

    /**
    * @secured(BAJA_CARTERA_RENDICION,READONLY_PROTECTED)
    */
    function delete($id) {
        $this->loadModel($this->model);
        $mensaje = "";
        $status = "";
        $this->CarteraRendicion->id = $id;
     
       try{
            if ($this->CarteraRendicion->delete() ) {
                
                //$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
                
                $status = EnumError::SUCCESS;
                $mensaje = "La Cartera Rendicion ha sido eliminada exitosamente.";
                $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
                
            }
                
            else
                 throw new Exception();
                 
          }catch(Exception $ex){ 
            //$this->Session->setFlash($ex->getMessage(), 'error');
            
            $status = EnumError::ERROR;
            $mensaje = $ex->getMessage();
            $output = array(
                    "status" => $status,
                    "message" => $mensaje,
                    "content" => ""
                ); 
        }
        
        if($this->RequestHandler->ext == 'json'){
            $this->set($output);
        $this->set("_serialize", array("status", "message", "content"));
            
        }else{
            $this->Session->setFlash($mensaje, $status);
            $this->redirect(array('controller' => $this->name, 'action' => 'index'));
            
        }
        
        
     
        
        
    }
    
    
   
	
	public function getModel($vista='default')
	{    
        $model = parent::getModelCamposDefault();//esta en APPCONTROLLER
        $model =  parent::setDefaultFieldsForView($model);//deja todo en 0 y no mostrar
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista, debe estar LOCAL
    }
    
    
    
    private function editforView($model,$vista)
	{  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);    
        
    }

}
?>