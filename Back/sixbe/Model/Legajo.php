<?php
App::uses('AppModel', 'Model');

class Legajo extends AppModel {
	
	
	/*
	
	public $virtualFields = array(
	'pepe' => '(SELECT GROUP_CONCAT(pia.id) FROM persona_impuesto_alicuota pia WHERE pia.id_persona = Persona.id)'
	);
	*/
	/**
	 * Use table
	 *
	 * @var mixed False or table name
	 */
	public $useTable = 'persona';
	
	public $actsAs = array('Containable');
	
	public $source = "persona";
	public $view = "legajo_view";
	
	
	
	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
			
			/*
			 'razon_social' => array(
			 'notempty' => array(
			 'rule' => array('notempty'),
			 'message' => 'Debe seleccionar una raz&oacute;n social v&aacute;lida',
			 'allowEmpty' => false,
			 'required' => true,
			 //'last' => false, // Stop validation after this rule
			 //'on' => 'create', // Limit validation to 'create' or 'update' operations
			 ),
			 ),*/
			/*
	
			'cuit' => array(
			'notempty' => array(
			'rule' => array('notempty'),
			'message' => 'Debe seleccionar un Cuit v&aacute;lido',
			'allowEmpty' => false,
			'required' => true,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			)*/
			
	);
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	
	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	
	
	public $belongsTo = array(
			'Provincia' => array(
					'className' => 'Provincia',
					'foreignKey' => 'id_provincia',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'Pais' => array(
					'className' => 'Pais',
					'foreignKey' => 'id_pais',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'TipoIva' => array(
					'className' => 'TipoIva',
					'foreignKey' => 'id_tipo_iva',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'PersonaCategoria' => array(
					'className' => 'PersonaCategoria',
					'foreignKey' => 'id_persona_categoria',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			
			'ListaPrecio' => array(
					'className' => 'ListaPrecio',
					'foreignKey' => 'id_lista_precio',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'TipoDocumento' => array(
					'className' => 'TipoDocumento',
					'foreignKey' => 'id_tipo_documento',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'Iva' => array(
					'className' => 'Iva',
					'foreignKey' => 'id_iva',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'Moneda' => array(
					'className' => 'Moneda',
					'foreignKey' => 'id_moneda',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'Transportista' => array(
					'className' => 'Transportista',
					'foreignKey' => 'id_transportista',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'ListaPrecio' => array(
					'className' => 'ListaPrecio',
					'foreignKey' => 'id_lista_precio',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'TipoActividad' => array(
					'className' => 'TipoActividad',
					'foreignKey' => 'id_tipo_actividad',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'TipoPersona' => array(
					'className' => 'TipoPersona',
					'foreignKey' => 'id_tipo_persona',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'CondicionPago' => array(
					'className' => 'CondicionPago',
					'foreignKey' => 'id_condicion_pago',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'PersonaFigura' => array(
					'className' => 'PersonaFigura',
					'foreignKey' => 'id_persona_figura',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'SituacionIb' => array(
					'className' => 'SituacionIb',
					'foreignKey' => 'id_situacion_ib',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'EstadoPersona' => array(
					'className' => 'EstadoPersona',
					'foreignKey' => 'id_estado_persona',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'Area' => array(
					'className' => 'Area',
					'foreignKey' => 'id_area',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'PersonaPadre' => array(
					'className' => 'Persona',
					'foreignKey' => 'id_persona_padre',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'CuentaContable' => array(
					'className' => 'CuentaContable',
					'foreignKey' => 'id_cuenta_contable',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'CuentaContableCuentaCorriente' => array(
					'className' => 'CuentaContable',
					'foreignKey' => 'id_cuenta_contable_cuenta_corriente',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			
			'Usuario' => array(
					'className' => 'Usuario',
					'foreignKey' => 'id_usuario',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					
			),
			'RRHHConvenio' => array(
					'className' => 'RRHHConvenio',
					'foreignKey' => 'id_rrhh_convenio',
					'dependent' => false,
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			),
			'RRHHCategoria' => array(
					'className' => 'RRHHCategoria',
					'foreignKey' => 'id_rrhh_categoria',
					'dependent' => false,
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
			
			
			
			
	);
	
	public $hasMany = array(
			'RRHHPersonaConcepto' => array(
					'className' => 'RRHHPersonaConcepto',
					'foreignKey' => 'id_persona',
					'dependent' => false,
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			),
			'RRHHNovedadPersona' => array(
					'className' => 'RRHHNovedadPersona',
					'foreignKey' => 'id_persona',
					'dependent' => false,
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			),
			
			
	);
	
	
	public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
		
		
		
		$this->virtual_fields_view = array(
				
				"d_estado_persona"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_provincia"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_pais"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_tipo_documento"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_cuenta_contable"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_categoria"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_rrhh_convenio"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_rrhh_situacion_sicoss"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_rrhh_condicion_sicoss"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_rrhh_actividad"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_rrhh_obra_social"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_rrhh_lugar_pago"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_rrhh_clase_legajo"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
				"d_rrhh_categoria"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null)
		);
		
	}
	
	
	
	public function getDireccion($object_persona='',$id_persona=0,$completa=1){ //si le pasas el object de persona no hace el find caso contrario lo hace. Sino encuentra la persona devuelve false
		
		
		//Persona::loadModel();
		
		if($object_persona ==''){
			
			
			$object_persona = $this->find('count',array('conditions' => array(
					
					'Persona.id' =>$id_persona,
					
			)
			));
			
			
			
		}
		
		
		
		if($object_persona){
			
			if($completa == 1)
				return $object_persona["razon_social"].' '.$object_persona["calle"].' '.$object_persona["numero_calle"].'-'.$object_persona["ciudad"].'-'.$object_persona["localidad"].'-'.$object_persona["Provincia"]["d_provincia"];
				else
					return  $object_persona["razon_social"].' '.$object_persona["calle"].' '.$object_persona["numero_calle"].'-'.$object_persona["ciudad"];
		}
		else
			return false;
			
			
	}
	
	
	function getSaldoCuentaCorriente($id_persona,$id_tipo_persona,$tipo_moneda=''){
		
		
		
		/*Concidera todos los comprobantes del sistema de ventas (id=2) EnumSistema::VENTAS los que tienen un 1 en el tipo_comprobante signo_comercial se concideran que
		 suman, caso contrario tienen un -1 y significa que resta.*/
		
		
		if($tipo_moneda == '')
			$tipo_moneda = EnumTipoMoneda::Base;
			
			switch($id_tipo_persona){
				
				case EnumTipoPersona::Cliente:
					
					$query = "SELECT SUM(c.total_comprobante/c.".$tipo_moneda.")/*calculo lo que debe*/
							
							
                    /*Le resto lo que pago o lo que tiene a favor*/
                    - (
							
                        SELECT SUM(c.total_comprobante/c.".$tipo_moneda.") FROM comprobante c
                        WHERE c.id_tipo_comprobante IN (SELECT id FROM tipo_comprobante WHERE id_sistema =".EnumSistema::VENTAS." AND signo_comercial=-1)
                        AND comprobante.id_persona=".$id_persona."
                        AND id_estado_comprobante IN(".EnumEstadoComprobante::CerradoConCae.",".EnumEstadoComprobante::CerradoReimpresion.")
                        		
                        ) AS saldo
                        		
                         FROM comprobante c
                        WHERE c.id_tipo_comprobante IN
                             (SELECT id FROM tipo_comprobante WHERE id_sistema =".EnumSistema::VENTAS." AND signo_comercial=1)
                              AND comprobante.id_persona=".$id_persona."
                              AND id_estado_comprobante IN(".EnumEstadoComprobante::CerradoConCae.",".EnumEstadoComprobante::CerradoReimpresion.")
                        ";
					
					$result = $this->query($query);
					
					$saldo = $result[0]["saldo"];
					
					break;
					
				case EnumTipoPersona::Proveedor:
					
					$saldo = 0;
					break;
					
					
					return $saldo;
					
					
					
			}
	}
	
	
	function getListadoCuentaCorriente($id_persona,$id_tipo_persona,$conditions){
		
		
		
		
		
		$comprobante = ClassRegistry::init('Comprobante');
		
		
		switch($id_tipo_persona){
			
			case EnumTipoPersona::Cliente:
				
				
				
				$condiciones = array_merge(array( 'NOT'=>array('TipoComprobante.signo_comercial'=>array(0)),'Comprobante.id_persona' => $id_persona,'Comprobante.id_estado_comprobante'=>array(EnumEstadoComprobante::CerradoConCae,EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::Cumplida),
				/*'NOT'=>array('Comprobante.id_tipo_comprobante'=>$comprobante->getTiposComprobanteController(EnumController::Recibos) ),*/'TipoComprobante.id_sistema'=>EnumSistema::VENTAS,'Comprobante.definitivo' => 1),$conditions);
				
				
				
				
				
				
				
				break;
				
			case EnumTipoPersona::Proveedor:
				
				$condiciones = array_merge(array('NOT'=>array('TipoComprobante.signo_comercial'=>array(0)),'Comprobante.id_persona' => $id_persona,'Comprobante.id_estado_comprobante'=>array(EnumEstadoComprobante::CerradoConCae,EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::Cumplida),
				/*'NOT'=>array('Comprobante.id_tipo_comprobante'=>array(EnumTipoComprobante::OrdenPagoAutomatica,EnumTipoComprobante::OrdenPagoManual,EnumTipoComprobante::GASTOA,EnumTipoComprobante::GASTOB,EnumTipoComprobante::GASTOC))*/'TipoComprobante.id_sistema'=>array(EnumSistema::COMPRAS,EnumSistema::CAJA),'Comprobante.definitivo' => 1),$conditions);
				
				
				break;
		}
		
		
		
		$comprobantes = $comprobante->find('all', array(
				'conditions' =>$condiciones ,
				'contain' =>array('TipoComprobante','PuntoVenta','Persona','Moneda'),
				'order'=>'Comprobante.fecha_contable ASC'
		));
		return $comprobantes;
		
		
	}
	
	
	function getRazonSocial($id_persona){
		
		
		$persona = $this->find('first', array(
				'conditions' => array('Persona.id' => $id_persona),
				'contain' =>false
		));
		
		return $persona["Persona"]["razon_social"];
	}
	
	
	function getTipoDocumento($id_persona){
		
		
		$persona = $this->find('first', array(
				'conditions' => array('Persona.id' => $id_persona),
				'contain' =>array('TipoDocumento')
		));
		
		
		return trim($persona["TipoDocumento"]["abreviacion_afip"]);
	}
	
	function getNroDocumento($id_persona){
		
		
		$persona = $this->find('first', array(
				'conditions' => array('Persona.id' => $id_persona),
				'contain' =>array('Pais','PersonaFigura')
		));
		
		if( $persona["Persona"]["id_tipo_iva"] == EnumTipoIva::ClienteDelExterrior || $persona["Persona"]["id_tipo_iva"] == EnumTipoIva::ProveedorDelExterior)
		{
			
			if($persona["Persona"]["id_persona_figura"] != 0){//si esta definida veo que cuit extranjero usar
				if($persona["PersonaFigura"]["id"] == EnumPersonaFigura::Fisica)
					return $persona["Pais"]["cuit_persona_fisica"];
					
					if($persona["PersonaFigura"]["id"] == EnumPersonaFigura::Juridica)
						return $persona["Pais"]["cuit_persona_juridica"];
						
			}else{
				
				return 0;//no esta seteado el id_persona_figura es un error
			}
		}else{
			
			return $persona["Persona"]["cuit"];
		}
	}
	
	
	function getCondicionIva($id_persona){
		
		
		$persona = $this->find('first', array(
				'conditions' => array('Persona.id' => $id_persona),
				'contain' =>false
		));
		
		return $persona["Persona"]["id_tipo_iva"];
	}
	
	
	public function getCuitConGuiones($cuit){
		
		
		$primera_parte = substr($cuit,0,2);
		$segunda_parte = substr($cuit,2,8);
		$tercera_parte = substr($cuit,10,1);
		return $primera_parte.'-'.$segunda_parte.'-'.$tercera_parte;
		
	}
	
	
	public function getCodigo($id_tipo_persona,$suma ="+"){
		/*Obtiene el codigo a traves de un contador definido en la tabla TipoPersona*/
		
		try{
			
			$id_contador = $this->getContador($id_tipo_persona);
			
			if($id_contador > 0){
				$this->query("UPDATE contador SET numero_actual = LAST_INSERT_ID(numero_actual ".$suma." 1)  WHERE id=".$id_contador.";");
				
				$result = $this->query("SELECT LAST_INSERT_ID() as codigo_persona;");
				$codigo_persona = $result[0][0]["codigo_persona"];
				
			}else{
				return 0;
				
			}
			
		}catch(Exception $ex){
			// $numero_comprobante = 0;
			return 0;
		}
		
		return $codigo_persona;
		
		
		
	}
	
	
	
	public function getContador($id_tipo_persona){
		
		App::uses('TipoPersona', 'Model');
		
		
		$contador_obj = New TipoPersona();
		
		$tipo_persona = $contador_obj->find('first', array(
				'conditions' => array('TipoPersona.id' => $id_tipo_persona),
				
		));
		
		if($tipo_persona)
			return $tipo_persona["TipoPersona"]["id_contador"];
			else
				return 0;
	}
	
	
	public function afterFind($results, $primary = false) {
		
		
		$this->setSource($this->source);
		
		return $results;
	}
	
	
	function afterSave($created, $options = array())  {
		
		
		$this->setCache();
		
	}
	
	
	
	public function beforeSave($options = array())
	{
		
		
		return true;
	}
	
	
	
	public function beforeDelete($cascade = true) {
		
		return true;
	}
	
	public function afterDelete() {
		$this->setCache();
		return true;
	}
	
	
	public function beforeFind($results, $primary = false) {
		$this->setSource($this->view);
		return true;
	}
	
	public function setCache($id_tipo_persona=0){
		
		$this->setSource($this->view); //uso la view para generar la cache
		if($id_tipo_persona == 0)
			$id_tipo_persona = $this->data["Persona"]["id_tipo_persona"];
			
			
			
			$articulo = $this->find('all', array(
					'conditions' => array($this->name.'.id_tipo_persona'=>$id_tipo_persona),
					'contain' => false
			)); //bus
			
			
			
			try{
				Cache::write("Persona".$id_tipo_persona, $articulo);
			}catch(Exception $e){
				
				
				Cache::config("Persona".$id_tipo_persona, array(
						'engine' => "File",
						'path' => CACHE . 'index' . DS
				));
				Cache::write("Persona".$id_tipo_persona, $articulo);
			}
	}
	
	
	public function savePersonFromMeli($buyer){
		
		
		$data["Persona"]["meli_id_usuario"] = $buyer["id"];
		$data["Persona"]["meli_nickname"] = $buyer["nickname"];
		$data["Persona"]["id_tipo_persona"] = EnumTipoPersona::Cliente;
		$data["Persona"]["tel"] =  $buyer["phone"]["area_code"].$buyer["phone"]["extension"].$buyer["phone"]["number"];
		$data["Persona"]["razon_social"] =  $buyer["first_name"]." ".$buyer["last_name"];
		$data["Persona"]["id_estado_persona"] = EnumEstadoPersona::ActivoCliente;
		$data["Persona"]["fecha_creacion"] = date("Y-m-d H:i:s");
		$data["Persona"]["codigo"] = $this->getCodigo(EnumTipoPersona::Cliente);
		
		
		$this->getMeliBillingData($data,$buyer);
		
		
		$this->saveAll($data);
		
		return $this->id;
		
		
		
	}
	
	
	
	
	
	
	
	
	public function getDataCuitFromAfip($cuit="",&$data=array()){
		
		
		
		App::import('Vendor', 'config', array('file' => 'fesix/config.php'));//importo el archivo de configuracion de la carpeta Vendor/fesix
		App::import('Vendor', 'AfipWsaa', array('file' => 'fesix/afip/AfipWsaa.php'));//importo el archivo de configuracion de la carpeta Vendor/fesix
		App::import('Vendor', 'AfipWsr', array('file' => 'fesix/afip/AfipWsr.php'));//imp
		
		
		$webService   = 'ws_sr_padron_a5';//Creando el objeto WSAA (Web Service de Autenticación y Autorización)
		
		
		
		// 	$cuit = "20335245637";
		
		
		$datoEmpresa = CakeSession::read('Empresa');
		
		$wsaa = new AfipWsaa($webService, $datoEmpresa["DatoEmpresa"]["alias"]);
		
		
		
		
		//$data = array();
		
		
		if($cuit!=""){
			
			try{
				if ($ta = $wsaa->loginCms())
				{
					
					
					
					
					
					App::uses('CakeSession', 'Model/Datasource');
					$datoEmpresa = CakeSession::read('Empresa');
					
					
					
					$empresaCuit = '20351532638';
					
					
					$token      = $ta['token'];
					$sign       = $ta['sign'];
					$expiration = $ta['expiration'];
					$uniqueid   = $ta['uniqueid'];
					
					//Conectando al WebService de Factura electronica (WsFev1)
					$wsr = new AfipWsr($empresaCuit,$token,$sign);
					
					$results = 	$wsr->getPersona($cuit);
					
					
					if($results->error_interno == EnumError::SUCCESS){
						
						
						$error = EnumError::SUCCESS;
						
						$provincia_obj = ClassRegistry::init(EnumModel::Provincia);
						$persona_figura_obj = ClassRegistry::init(EnumModel::PersonaFigura);
						
						
						
						
						$datos_generales = $results->personaReturn->datosGenerales;
						$provincia =  $provincia_obj->getProvinciaPorCodigoAfip($datos_generales->domicilioFiscal->idProvincia);
						
						
						$id_provincia = 0;
						$d_provincia = "";
						if($provincia){
							$id_provincia = $provincia["Provincia"]["id"];
							$d_provincia = $provincia["Provincia"]["d_provincia"];
							
						}
						
						
						$id_persona_figura = $persona_figura_obj->getPersonaFiguraPorCodigoAfip($datos_generales->tipoPersona);	//FISICA, JURIDICA
						$codigo_postal = $datos_generales->domicilioFiscal->codPostal;
						$direccion = $datos_generales->domicilioFiscal->direccion;
						
						$localidad = $datos_generales->domicilioFiscal->localidad;
						$razon_social = $datos_generales->razonSocial;
						//$tipo_clave = $datos_generales->tipoClave;
						
						$data["Persona"]["id_provincia"] = $id_provincia;
						$data["Persona"]["d_provincia"] = $d_provincia;
						$data["Persona"]["cod_postal"] = $codigo_postal;
						$data["Persona"]["id_persona_figura"] = $id_persona_figura;
						$data["Persona"]["calle"] = $direccion;
						$data["Persona"]["localidad"] = $localidad;
						$data["Persona"]["razon_social"] = $razon_social;
						
						
						
						
						$datos_extra = $results->personaReturn->datosRegimenGeneral;//datos copados
						$datos_extra_monotributo = $results->personaReturn->datosMonotributo;//datos copados
						
						
						$data["Persona"]["datos_extra_regimen_general"] = $datos_extra;
						$data["Persona"]["datos_extra_monotributo"] = $datos_extra_monotributo;
						
						
						
						
					}else{
						
						$error = EnumError::ERROR;
						$messagge = $results->message_interno;
					}
					
					
				}else{
					$error = EnumError::ERROR;
					$messagge = $wsaa->getErrLog();
					
				}
			}catch(Exception $a){
				
				$error = EnumError::ERROR;
				$message = $a->getMessage();
				
			}
			
			
		}else{
			
			$error = EnumError::ERROR;
			$messagge = "Debe especificar un documento para poder consultar el servicio";
			
			
			
		}
		
		$output = array(
				"status" => $error,
				"message" => $messagge,
				"content" => $data,
				"page_count" =>0,
				"message_type"=>EnumMessageType::MessageBox
		);
		
		
		
		return $output;
		
		
		
		
		
		
	}
	
	
	
	
	
}
