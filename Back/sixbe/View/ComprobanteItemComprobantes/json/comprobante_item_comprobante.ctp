<?php
        
        
        include(APP.'View'.DS.'ComprobanteItemComprobantes'.DS.'json'.DS.'default.ctp');
        
      
        $model["n_item"]["show_grid"] = 1;
        $model["n_item"]["header_display"] = "Nro. Item";
        $model["n_item"]["order"] = "1";
        $model["n_item"]["width"] = "10";
      
        $model["d_tipo_comprobante_origen"]["show_grid"] = 1;
        $model["d_tipo_comprobante_origen"]["header_display"] = "Tipo";
        $model["d_tipo_comprobante_origen"]["order"] = "2";
        $model["d_tipo_comprobante_origen"]["width"] = "10";
        
        $model["pto_nro_comprobante_origen"]["show_grid"] = 1;
        $model["pto_nro_comprobante_origen"]["header_display"] = "Nro.";
        $model["pto_nro_comprobante_origen"]["order"] = "2";
        $model["pto_nro_comprobante_origen"]["width"] = "10";
        
        $model["fecha_generacion_origen"]["show_grid"] = 1;
        $model["fecha_generacion_origen"]["header_display"] = "Fcha. Gen.";
        $model["fecha_generacion_origen"]["order"] = "3";
        $model["fecha_generacion_origen"]["width"] = "10";
        
        $model["fecha_vencimiento_origen"]["show_grid"] = 1;
        $model["fecha_vencimiento_origen"]["header_display"] = "Fcha. Venc.";
        $model["fecha_vencimiento_origen"]["order"] = "4";
        $model["fecha_vencimiento_origen"]["width"] = "10";
        
        $model["item_observacion"]["show_grid"] = 1;
        $model["item_observacion"]["header_display"] = "Item Observ.";
        $model["item_observacion"]["order"] = "5";
        $model["item_observacion"]["width"] = "20";
        
        $model["precio_unitario"]["show_grid"] = 1;
        $model["precio_unitario"]["header_display"] = "Monto Imputado.";
        $model["precio_unitario"]["order"] = "5";
        $model["precio_unitario"]["width"] = "20";
        $model["precio_unitario"]["read_only"] = "1";
        
        $model["subtotal_comprobante_origen"]["show_grid"] = 1;
        $model["subtotal_comprobante_origen"]["header_display"] = "SubTotal Origen.";
        $model["subtotal_comprobante_origen"]["order"] = "5";
        $model["subtotal_comprobante_origen"]["width"] = "20";
        
        $model["total_comprobante_origen"]["show_grid"] = 1;
        $model["total_comprobante_origen"]["header_display"] = "Total Origen.";
        $model["total_comprobante_origen"]["order"] = "5";
        $model["total_comprobante_origen"]["width"] = "20";
        
        $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItemComprobante",
                    "content" => $model
                );
        echo json_encode($output);
?>