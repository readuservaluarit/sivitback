<?php

/**
 * PDF exportación de Orden Armado
 */

App::import('Vendor','tcpdf/tcpdf'); 
App::import('Model','ComprobanteItem');


$html_composicion = '';

if(isset($datos_pdf['ComprobanteItem'][0]['Producto']['ArticuloRelacion']) && count($datos_pdf['ComprobanteItem'][0]['Producto']['ArticuloRelacion'])>0){
$componentes_producto = $datos_pdf['ComprobanteItem'][0]['Producto']['ArticuloRelacion'];


foreach($componentes_producto as $comp){

    
   $html_composicion .='<tr  style="font-size:9px">
        <td align="center" valign="middle">'.trim($comp['ArticuloHijo']["codigo"]).'</td>
        <td align="center" valign="middle">'.trim($comp['ArticuloHijo']["d_producto"]).'</td>

        <td align="center" valign="middle">'.round(trim($comp['cantidad_hijo'])).'</td>
        <td align="center" valign="middle">&nbsp;</td>
        <td align="center" valign="middle">&nbsp;</td>
      </tr>';




}

}


// create new PDF document
//$medidas=array(215,9,355,6); //215,9 x 355,6 LEGAL http://www.innovation-marketing.at/index_107_107_20_1000000203_1_0__.html
//$pdf = new TCPDF('P', 'mm', PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetHeaderMargin(1);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Valuarit.com');
$pdf->SetTitle('Orden Armado');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
//$pdf->SetFont('helvetica', '', 12, '', true);
$pdf->AddPage('P', 'A4');
//set margins
//$pdf->SetMargins(-300, 50, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(5);


$fecha_emision = new DateTime($datos_pdf['Comprobante']['fecha_generacion']);
$fecha_entrega = new DateTime($datos_pdf['Comprobante']['fecha_entrega']);
$comprobante_item = new ComprobanteItem();
$producto_codigo = $datos_pdf['ComprobanteItem'][0]['Producto']['codigo'];


$pdf->StartTransform();

$html = <<<EX

<style>
table {
  border-collapse: collapse;
}
table td, table td {
  border: 1px solid black;
}
table tr:first-child td {
  border-top: 0;
}
table tr:last-child td {
  border-bottom: 0;
}
table tr td:first-child,
table tr td:first-child {
  border-left: 0;
}
table tr td:last-child,
table tr td:last-child {
  border-right: 0;
}
</style>


<body>


 
    <table width="100%" border="1" cellpadding="1" cellspacing="1" style="font-size:8px; text-align:center" width="100%">
  <tr>
    <td rowspan="2" scope="col">VALMEC</td>
    <td colspan="2" scope="col" style="font-size:11px;">ORDEN ARMADO NRO {$datos_pdf['Comprobante']['nro_comprobante']}</td>
  </tr>
  
  
  <tr>
    <td>Emision: {$fecha_emision->format('d-m-Y')}</td>
    <td>Entrega Comp a Armado: {$fecha_entrega->format('d-m-Y')}</td>
  </tr>
 
 
  <tr>
    <td colspan="3">CANTIDAD: {$datos_pdf['ComprobanteItem'][0]['cantidad_cierre']} - CODIGO: <span style="font-size:11px;"> {$producto_codigo} </span> - {$datos_pdf['Producto']['d_producto']}</td>
  </tr>
 
  
  <tr>
    <td colspan="3"><span style="font-size:8px;">ITEM OBSERVACION PEDIDO INTERNO: {$datos_pdf['ComprobanteItem'][0]['ComprobanteItemOrigen']['item_observacion']} </span></td>
  </tr>
  
  
    <tr>
    <td colspan="3">FECHA LIMITE ARMADO:{$datos_pdf['Comprobante']['fecha_vencimiento']}</td>
  </tr>
  
  
  <tr>
    <td colspan="3">PRESION DE ENSAYO: <span style="font-size:11px;"> {$datos_pdf['ComprobanteItem'][0]['item_observacion']} </span></td>
  </tr>
  <tr>
    <td colspan="3">Cliente:  {$datos_pdf['ComprobanteItem'][0]['ComprobanteItemOrigen']['Comprobante']['Persona']['razon_social']}  - Pedido Interno: {$datos_pdf['ComprobanteItem'][0]['ComprobanteItemOrigen']['Comprobante']['nro_comprobante']} -  OC: {$datos_pdf['ComprobanteItem'][0]['ComprobanteItemOrigen']['Comprobante']['orden_compra_externa']}</td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" align="center" cellpadding="1" cellspacing="1" dir="ltr" style="text-align:center;" widtd="100%">
      <tr>
        <td bgcolor="#999999" scope="col">RF</td>
        <td scope="col">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#999999">FF</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#999999">RU</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#999999">BSPT</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#999999">NPT</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#999999">BSP</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
    <td align="left" valign="top"><table width="100%" border="0" cellpadding="1" cellspacing="1" widtd="100%">
      <tr>
        <td colspan="2" scope="col"><table width="100%" border="0" cellpadding="1" cellspacing="1" style="border:none; text-align:center" widtd="100%">
          <tr>
            <td bgcolor="#999999" scope="col">S.W</td>
            <td scope="col">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#999999">B.W</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
        <td bgcolor="#999999" style="font-size:8px; text-align:center" scope="col" widtd="32%">Extension requerida</td>
        <td widtd="14%" scope="col">&nbsp;</td>
      </tr>
      <tr>
        <td widtd="41%" align="center" valign="middle" bgcolor="#999999">Antifuego</td>
        <td widtd="13%">&nbsp;</td>
        <td align="center" valign="middle" bgcolor="#999999">Antiestatica</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align="center" valign="middle" bgcolor="#999999">Reductor</td>
        <td colspan="2" align="center" valign="middle" bgcolor="#999999">Extensor</td>
      </tr>
      <tr>
        <td colspan="2">&nbsp;</td>
        <td colspan="2">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align="center" valign="middle" bgcolor="#999999">Monograma del API</td>
        <td colspan="2">&nbsp;</td>
      </tr>
    </table></td>
    <td align="left" valign="top"><table width="100%" border="0" cellpadding="1" cellspacing="1" widtd="100%">
      <tr>
        <td widtd="41%" align="center" valign="middle" bgcolor="#999999" scope="col">Actuador Neumatico</td>
        <td widtd="59%" scope="col"><table width="100%" border="0" cellpadding="1" cellspacing="1" widtd="100%">
          <tr>
            <td align="center" valign="middle" bgcolor="#999999" scope="col">SR</td>
            <td scope="col">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#999999">DA</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center" valign="middle" bgcolor="#999999">Solenoide</td>
        <td><table width="100%" border="0" cellpadding="1" cellspacing="1" widtd="100%">
          <tr>
            <td widtd="37%" rowspan="2" scope="col">&nbsp;</td>
            <td bgcolor="#999999" scope="col" widtd="34%">VAC</td>
            <td widtd="29%" scope="col">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#999999">VCC</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center" valign="middle" bgcolor="#999999">Actuador Electrico</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999">Sellos tipo / Lote</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="left" valign="top">Observaciones: {$datos_pdf['Comprobante']['observacion']}</td>
    <td align="left" valign="top"><table widtd="100%" border="0" cellspacing="1" cellpadding="1">
      <tr>
        <td widtd="27%" align="center" valign="middle" bgcolor="#999999" scope="col">Cant.</td>
        <td widtd="73%" align="center" valign="middle" bgcolor="#999999" scope="col">Cliente</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3" align="left" valign="top">
    <table width="100%" border="0" cellpadding="1" cellspacing="1" widtd="100%">
      <tr>
        <td colspan="2" rowspan="2" align="center" valign="middle" bgcolor="#999999" scope="col">Ensayos finales</td>
        <td widtd="14%" rowspan="2" align="center" valign="middle" bgcolor="#999999" scope="col">Presion (kg/cm2)</td>
        <td widtd="8%" rowspan="2" align="center" valign="middle" bgcolor="#999999" scope="col">Cant</td>
        <td widtd="10%" rowspan="2" align="center" valign="middle" bgcolor="#999999" scope="col">Tiempo (m)</td>
        <td widtd="9%" rowspan="2" align="center" valign="middle" bgcolor="#999999" scope="col">Fluido</td>
        <td widtd="13%" rowspan="2" align="center" valign="middle" bgcolor="#999999" scope="col">Instrum./ Banco</td>
        <td align="center" valign="middle" bgcolor="#999999" scope="col">Armado por</td>
        </tr>
      <tr>
        <td scope="col">&nbsp;</td>
        </tr>
      <tr>
        <td colspan="2" align="center" valign="middle" bgcolor="#999999">Cuerpo</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center" valign="middle">AGUA</td>
        <td>&nbsp;</td>
        <td align="center" valign="middle" bgcolor="#999999">Ensayado por </td>
        </tr>
      <tr>
        <td widtd="3%" rowspan="2" bgcolor="#999999">C</td>
        <td widtd="16%" align="center" valign="middle" bgcolor="#999999">Hidrostatico</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center" valign="middle">AGUA</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td widtd="16%" align="center" valign="middle" bgcolor="#999999">Neumatico</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center" valign="middle">AIRE</td>
        <td>&nbsp;</td>
        <td align="center" valign="middle" bgcolor="#999999">Liberado por </td>
        </tr>
      <tr>
        <td colspan="6">Otros:</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
    </td>
  </tr>
</table>


<table width="100%" border="0">



<tr style="font-size:10px;">
<td></td></tr>
<tr style="font-size:10px;"><td></td></tr>
</table>

<table width="100%" border="1">
      <tr style="font-size:10px;">
        <td width="15%" align="center" valign="middle" bgcolor="#999999" scope="col">Codigo</td>
        <td width="35%" align="center" valign="middle" bgcolor="#999999" scope="col">Descripcion</td>
        <td width="5%" align="center" valign="middle" bgcolor="#999999" scope="col">Cant</td>
        <td width="30%" align="center" valign="middle" bgcolor="#999999" scope="col">Lotes/coladas</td>
        <td width="15%" align="center" valign="middle" bgcolor="#999999" scope="col">O/F</td>
      </tr>
     {$html_composicion}
</table>
<table width="100%" border="1">
  <tr>
    <td height="60px;" align="left" valign="top">Observaciones Manuales:</td>
  </tr>
</table>
<table width="100%" border="0">
  <tr>
    <td align="left">F07- Orden de Armado</td>
    <td align="right">Rev. 11- Abril 2016</td>
  </tr>
</table>
EX;



$pdf->writeHTML($html, true, false, true, false, '');









//$pdf->Rotate(90);
//$pdf->writeHTML($html, true, false, true, false, '');
$pdf->StopTransform();


ob_clean();
$pdf->Output($datos_pdf["TipoComprobante"]["abreviado_tipo_comprobante"].'_'.$datos_pdf['Comprobante']['nro_comprobante_completo'].'.pdf', 'D');



//============================================================+
// END OF FILE
//============================================================+