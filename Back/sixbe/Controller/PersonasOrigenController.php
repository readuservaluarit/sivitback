<?php
class PersonasOrigenController extends AppController {
    public $name = 'PersonasOrigen';
    public $model = EnumModel::PersonaOrigen;
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');

    protected function index() {

        if($this->request->is('ajax'))
            $this->layout = 'ajax';

        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => $this->numrecords, 'update' => 'main-content', 'evalScripts' => true);
		//Recuperacion de Filtros
     
        $id = $this->getFromRequestOrSession('PersonaOrigen.id');
     
        $conditions = array(); 
        
        if ($id != "") {
           array_push($conditions,array('PersonaOrigen.id' =>$id ));
        }
		array_push($conditions,array('PersonaOrigen.id_tipo_persona' =>  $this->id_tipo_persona));
  
     
        
        $this->paginate = array('paginado'=>$this->paginado,'maxLimit'=> $this->maxLimitRows,
            
            //'contain'=>array('EstadoTipoCheque'),
            'conditions' => $conditions,
            'limit' => $this->numrecords,
            'page' => $this->getPageNum()
        );
        
        if($this->RequestHandler->ext != 'json'){  

                App::import('Lib', 'FormBuilder');
                $formBuilder = new FormBuilder();
                $formBuilder->setDataListado($this, 'Listado de EstadosPersona', 'Datos de los EstadosPersona', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
                
                //Filters
                $formBuilder->addFilterBeginRow();
                $formBuilder->addFilterInput('d_ESTADO_PERSONA', 'Nombre de PersonaOrigen', array('class'=>'control-group span5'), array('type' => 'text', 'style' => 'width:500px;', 'label' => false, 'value' => $nombre));
                $formBuilder->addFilterEndRow();
                
                //Headers
                $formBuilder->addHeader('Id', 'PersonaOrigen.id', "10%");
                $formBuilder->addHeader('Nombre', 'PersonaOrigen.d_moneda', "50%");
             

                //Fields
                $formBuilder->addField($this->model, 'id');
                $formBuilder->addField($this->model, 'd_ESTADO_PERSONA');
        
                
                $this->set('abm',$formBuilder);
                $this->render('/FormBuilder/index');
        }else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; 
        $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        $this->data = $data;
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
            "content" => $data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
     }
    }


    
  
    
    protected function add() {
    	if ($this->request->is('post')){
    		$this->loadModel($this->model);
    		$id_cat = '';
    		
    		try{
    			
    			$this->request->data[$this->model]["id_tipo_persona"] = $this->id_tipo_persona;
    			
    			if ($this->{$this->model}->saveAll($this->request->data, array('deep' => true))){
    				$mensaje = "El Origen ha sido creado exitosamente";
    				$tipo = EnumError::SUCCESS;
    				$id_cat = $this->Categoria->id;
    				
    			}else{
    				$errores = $this->{$this->model}->validationErrors;
    				$errores_string = "";
    				foreach ($errores as $error){
    					$errores_string.= "&bull; ".$error[0]."\n";
    					
    				}
    				$mensaje = $errores_string;
    				$tipo = EnumError::ERROR;
    				
    			}
    		}catch(Exception $e){
    			
    			$mensaje = "Ha ocurrido un error,el Origen no ha podido ser creado.".$e->getMessage();
    			$tipo = EnumError::ERROR;
    		}
    		$output = array(
    				"status" => $tipo,
    				"message" => $mensaje,
    				"content" => "",
    				"id_add" =>$id_cat
    		);
    		//si es json muestro esto
    		if($this->RequestHandler->ext == 'json'){
    			$this->set($output);
    			$this->set("_serialize", array("status", "message", "content" ,"id_add"));
    		}else{
    			
    			$this->Session->setFlash($mensaje, $tipo);
    			$this->redirect(array('action' => 'index'));
    		}
    		
    	}
    	
    	//si no es un post y no es json
    	if($this->RequestHandler->ext != 'json')
    		$this->redirect(array('action' => 'abm', 'A'));
    		
    		
    }
    

    
    protected function edit($id) {
    	
    	
    	if (!$this->request->is('get')){
    		
    		$this->loadModel($this->model);
    		$this->{$this->model}->id = $id;
    		
    		try{
    			$this->request->data[$this->model]["id_tipo_persona"] = $this->id_tipo_persona;
    			
    			if ($this->{$this->model}->saveAll($this->request->data)){
    				$mensaje =  "El Origen ha sido modificada exitosamente";
    				$status = EnumError::SUCCESS;
    				
    				
    			}else{   //si hubo error recupero los errores de los models
    				
    				$errores = $this->{$this->model}->validationErrors;
    				$errores_string = "";
    				foreach ($errores as $error){ //recorro los errores y armo el mensaje
    					$errores_string.= "&bull; ".$error[0]."\n";
    					
    				}
    				$mensaje = $errores_string;
    				$status = EnumError::ERROR;
    			}
    			
    			if($this->RequestHandler->ext == 'json'){
    				$output = array(
    						"status" => $status,
    						"message" => $mensaje,
    						"content" => ""
    				);
    				
    				$this->set($output);
    				$this->set("_serialize", array("status", "message", "content"));
    			}else{
    				$this->Session->setFlash($mensaje, $status);
    				$this->redirect(array('controller' => $this->name, 'action' => 'index'));
    				
    			}
    		}catch(Exception $e){
    			$this->Session->setFlash('Ha ocurrido un error, el Origen no ha podido modificarse.', 'error');
    			
    		}
    		
    	}
    	if($this->RequestHandler->ext != 'json')
    		$this->redirect(array('action' => 'abm', 'M', $id));
    }
    
    
  
    protected  function delete($id) {
    	
    	
    	$this->loadModel($this->model);
    	$mensaje = "";
    	$status = "";
    	$this->{$this->model}->id = $id;
    	
    	try{
    		if ($this->{$this->model}->delete() ) {
    			
    			//$this->Session->setFlash('El Cliente ha sido eliminado exitosamente.', 'success');
    			
    			$status = EnumError::SUCCESS;
    			$mensaje = "El Origen ha sido eliminado exitosamente.";
    			$output = array(
    					"status" => $status,
    					"message" => $mensaje,
    					"content" => ""
    			);
    			
    		}
    		
    		else
    			throw new Exception();
    			
    	}catch(Exception $ex){
    		//$this->Session->setFlash($ex->getMessage(), 'error');
    		
    		$status = EnumError::ERROR;
    		$mensaje = $ex->getMessage();
    		$output = array(
    				"status" => $status,
    				"message" => $mensaje,
    				"content" => ""
    		);
    	}
    	
    	if($this->RequestHandler->ext == 'json'){
    		$this->set($output);
    		$this->set("_serialize", array("status", "message", "content"));
    		
    	}else{
    		$this->Session->setFlash($mensaje, $status);
    		$this->redirect(array('controller' => $this->name, 'action' => 'index'));
    		
    	}
    	
    	
    	
    	
    	
    }
    

   


}
?>