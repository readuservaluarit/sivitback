<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class ComprobanteImpuesto extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'comprobante_impuesto';

    public $actsAs = array('Containable');
    
   
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        
 
        'base_imponible' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Impuesto: La Base imponible es obligatoria para los impuestos',
                //'allowEmpty' => false,
                'required' => 'create',
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),

        'importe_impuesto' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Impuesto: El importe del impuesto es obligatorio',
                //'allowEmpty' => false,
                'required' => 'create',
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        )
       
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

public $belongsTo = array(
        'Comprobante' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Impuesto' => array(
            'className' => 'Impuesto',
            'foreignKey' => 'id_impuesto',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'ComprobanteReferencia' => array(
            'className' => 'Comprobante',
            'foreignKey' => 'id_comprobante_referencia',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
		'TipoAlicuotaAfip' => array(
				'className' => 'TipoAlicuotaAfip',
				'foreignKey' => 'tipo_alicuota_afip',
				'conditions' => '',
				'fields' => '',
				'order' => ''
		)
        
       
      
 );
 
 
 public function setVirtualFieldsForView(){//configura los virtual fields  para este Model
    
     
     
     $this->virtual_fields_view = array(
     
     "d_impuesto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "pto_nro_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_punto_venta"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "razon_social"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_provincia"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_iva_detalle"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "abreviado_impuesto"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_moneda"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "fecha_contable"=>array("type"=>EnumTipoDato::date,"show_grid"=>0,"default"=>null,"length"=>null),
     "autogenerado"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "d_estado_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "codigo_persona"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "codigo_tipo_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "nro_comprobante"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "pto_nro_comprobante_retencion"=>array("type"=>EnumTipoDato::cadena,"show_grid"=>0,"default"=>null,"length"=>null),
     "id_moneda"=>array("type"=>EnumTipoDato::entero,"show_grid"=>0,"default"=>null,"length"=>null)
   
     
    
    );
    
    }
 
 
 public function getImpuesto($array_comprobante_impuesto,$id_impuesto){
     
     
     
     if(count($array_comprobante_impuesto)>0){
         
         foreach($array_comprobante_impuesto as $impuesto){
             
             
             if(!isset($impuesto["ComprobanteImpuesto"])) {   //esto lo pregunto xq me puede mandar en el add el COmprobanteImpuesto
                 if($impuesto["id_impuesto"]== $id_impuesto){
                     
                     return $impuesto;
                     die();
                     
                 }
             }else{
                    if($impuesto["ComprobanteImpuesto"]["id_impuesto"]== $id_impuesto){
                     
                     return $impuesto;
                     die();
                 
             }
             
         }
     }
     }else{
         
         return 0;
     }
     
 }
 
 
 
 
public function getAcumulado($mes,$id_tipo_impuesto,$id_subtipo_impuestos,$id_persona,$id_tipo_comercializacion){
    
	
	$year = date('Y');//por defecto tomo el a�o del servidor como el actual
	
    $comprobante = $this->find('all', array(
                                    'fields'=>array('IFNULL(SUM(importe_impuesto),0) as total_acumulado'),
    		'conditions' => array('Impuesto.id_tipo_impuesto'=>$id_tipo_impuesto,'Impuesto.id_sub_tipo_impuesto'=>$id_subtipo_impuestos,'MONTH(Comprobante.fecha_contable)'=>$mes,'YEAR(Comprobante.fecha_contable)'=>$year,'Comprobante.id_persona'=>$id_persona,'Comprobante.definitivo'=>1,
										                                    
                                    					'Impuesto.id_tipo_comercializacion'=>$id_tipo_comercializacion,'Comprobante.id_estado_comprobante'=>array(EnumEstadoComprobante::CerradoReimpresion,EnumEstadoComprobante::CerradoConCae)),
                                    'contain' => array("Comprobante","Impuesto")
                                    ));  
    
    
    return $comprobante[0][0]["total_acumulado"];
}



public function getTotalImpuestoFromComprobanteCitiAlicuotas($id_comprobante,$data){
	
	foreach($data as $item){
		
		if($item["Comprobante"]["id"] == $id_comprobante)
			return $item[0]["total_impuesto"];
		
		
	}
	
	return 0;
	
}



  

}
