UPDATE orden_armado
JOIN oa_para_actualizar ON oa_para_actualizar.`id` = orden_armado.`id`

SET orden_armado.`id_producto` 		 = oa_para_actualizar.`id_producto`,
    orden_armado.`id_pedido_interno` = oa_para_actualizar.`id_pedido_interno`

WHERE orden_armado.id = oa_para_actualizar.`id`