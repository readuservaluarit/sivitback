<?php

App::uses('ComprobanteItemsController', 'Controller');

class RemitoCompraItemsController extends ComprobanteItemsController {
    
    public $name = 'RemitoCompraItems';
    public $model = 'ComprobanteItem';
    public $helpers = array ('Session', 'Paginator', 'Js');
    public $components = array('Session', 'PaginatorModificado', 'RequestHandler');
    
  
  
     /**
    * @secured(CONSULTA_REMITO_COMPRA)
    */
    public function index() {
        
        if($this->request->is('ajax'))
            $this->layout = 'ajax';
            
        $this->loadModel($this->model);
        $this->PaginatorModificado->settings = array('limit' => 10, 'update' => 'main-content', 'evalScripts' => true);
        
        //Recuperacion de Filtros
        $conditions = $this->RecuperoFiltros($this->model);
        
        
     
        
        
        $filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');
        
        
        //$filtro_agrupado = 3; //MP antes tenia uno 1 si manda un /index plano me agrupaba los items y no me traie los campos q necesito para el form PIMasterDetail0
        $this->ComprobanteItem->paginateAgrupado($this,$filtro_agrupado,$conditions,
        		$this->numrecords,$this->getPageNum(),
        		array('Comprobante'=>array('EstadoComprobante','Moneda','PuntoVenta','Persona','TipoComprobante'),
        				'Producto'=>array('ProductoTipo','Unidad'),'ComprobanteItemOrigen'=>array('Iva','Comprobante'=>array('Moneda'))),$this->ComprobanteItem->getOrderItems($this->model,$this->getFromRequestOrSession($this->model.'.id_comprobante'))); // AGREGADO PARA EL ORDEN DE ISCO
        		
        
      if($this->RequestHandler->ext != 'json'){  
        App::import('Lib', 'FormBuilder');
        $formBuilder = new FormBuilder();

        
        	
        
    
    
        
        $formBuilder->setDataListado($this, 'Listado de Clientes', 'Datos de los Clientes', $this->model, $this->name, $this->PaginatorModificado->paginate($this->model));
        
        
        

        //Headers
        $formBuilder->addHeader('id', 'Remito.id', "10%");
        $formBuilder->addHeader('Razon Social', 'cliente.razon_social', "20%");
        $formBuilder->addHeader('CUIT', 'Remito.cuit', "20%");
        $formBuilder->addHeader('Email', 'cliente.email', "30%");
        $formBuilder->addHeader('Tel&eacute;fono', 'cliente.tel', "20%");

        //Fields
        $formBuilder->addField($this->model, 'id');
        $formBuilder->addField($this->model, 'razon_social');
        $formBuilder->addField($this->model, 'cuit');
        $formBuilder->addField($this->model, 'email');
        $formBuilder->addField($this->model, 'tel');
     
        $this->set('abm',$formBuilder);
        $this->render('/FormBuilder/index');
    
        //vista formBuilder
    }
      
    else{ // vista json
        $this->PaginatorModificado->settings = $this->paginate; $data = $this->PaginatorModificado->paginate($this->model);
        $page_count = $this->params['paging'][$this->model]['pageCount'];
        
        foreach($data as $key=>&$producto ){
        	
        	
        	
        	switch($filtro_agrupado){
        		
        		
        		
        		case "":
        		case EnumTipoFiltroComprobanteItem::individual:
        		case EnumTipoFiltroComprobanteItem::agrupadaporComprobante:
        	
        	     
		        	if(isset( $producto["ComprobanteItemOrigen"]) && $producto["ComprobanteItemOrigen"]["id_comprobante"]>0){/*Si es importado de ORden de compra*/
		        	     
		        		 $cantidad_orden_compra_origen = $producto["ComprobanteItemOrigen"]["cantidad"];
		        		 
		        		 $total_remitos_origen = $this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItemOrigen"],$this->Comprobante->getTiposComprobanteController(EnumController::RemitosCompra));
		                 
		        		 $producto["ComprobanteItem"]["pto_nro_comprobante"] = $this->ComprobanteItem->GetNumberComprobante($producto["Comprobante"]["d_punto_venta"],$producto["Comprobante"]["nro_comprobante"]);
		        		 
		        		 $facturados =$this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItem"], $this->Comprobante->getTiposComprobanteController(EnumController::FacturasCompra) );
		        		 $producto["ComprobanteItem"]["total_facturado"] = (string)  number_format((float)$this->ComprobanteItem->GetCantidadItemProcesados($facturados), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		        		 $producto["ComprobanteItem"]["cantidad_pendiente_a_facturar"] = (string) number_format( (float)($producto["ComprobanteItem"]["cantidad"] - $producto["ComprobanteItem"]["total_facturado"]), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		        		 
		        		 $producto["ComprobanteItem"]["total_orden_compra_origen"] = (string) $cantidad_orden_compra_origen;
		        		 
		        		 $producto["ComprobanteItem"]["d_estado_comprobante"] = $producto["Comprobante"]["EstadoComprobante"]["d_estado_comprobante"];
		        		 $producto["ComprobanteItem"]["razon_social"] = $producto["Comprobante"]["Persona"]["razon_social"];
		        		 
		        		 
		        		 $irm_conformados =$this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItem"], $this->Comprobante->getTiposComprobanteController(EnumController::InformeRecepcionMateriales),array(EnumEstadoComprobante::CerradoReimpresion));
		        		
		        		 
		        		 $producto["ComprobanteItem"]["cantidad_recibida_irm"] = (string)  number_format((float)$this->ComprobanteItem->GetCantidadItemProcesados($irm_conformados,"cantidad_cierre"), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		        		 $producto["ComprobanteItem"]["cantidad_pendiente_irm"] = (string)  ($producto["ComprobanteItem"]["cantidad"] - $producto["ComprobanteItem"]["cantidad_recibida_irm"] );
		        		 
		        		 
		        		 if($producto["ComprobanteItemOrigen"]["requiere_conformidad"] == 1){
		        		 	
		        		 	$producto["ComprobanteItem"]["cantidad_aprobada_a_facturar"] =  
							(string) number_format( (float)($producto["ComprobanteItem"]["cantidad_recibida_irm"] 
							- $producto["ComprobanteItem"]["total_facturado"]), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		        		 }else{
		        		 	
		        		 	$producto["ComprobanteItem"]["cantidad_aprobada_a_facturar"] =  
							(string) number_format( (float) ($producto["ComprobanteItem"]["cantidad_pendiente_a_facturar"]), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		        		 }
		        		 
		        		 
		        		 $producto["ComprobanteItem"]["total_pendiente_a_remitir_origen"] = 
		        		 (string)  number_format((float) ($cantidad_orden_compra_origen/*10*/
		        		 		- $this->ComprobanteItem->GetCantidadItemProcesados($total_remitos_origen))/*2*/, $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');;
		        		 
		        		 
		        		 		
		        		 $producto["ComprobanteItem"]["precio_unitario"] = $producto["ComprobanteItemOrigen"]["precio_unitario"];
		        		 $producto["ComprobanteItem"]["id_moneda"]  = 			$producto["ComprobanteItemOrigen"]["Comprobante"]["id_moneda"];
		        		 $producto["ComprobanteItem"]["moneda_simbolo"]  = 			$producto["ComprobanteItemOrigen"]["Comprobante"]["Moneda"]["simbolo"];
		        		 
		        		 
		        		 
		        		 $aux["ComprobanteItem"] = $producto["ComprobanteItemOrigen"];//uso esta variable auxziliar asi uso la funcion getTotalItem
		        		 $producto["ComprobanteItem"]["total"]  = 	(string)$this->{$this->model}->getTotalItem($aux);
		        		 
		        		 
		        		 $producto["ComprobanteItem"]["id_iva"]  = 	$producto["ComprobanteItemOrigen"]["id_iva"];
		        		 $producto["ComprobanteItem"]["d_iva"]  = 	$producto["ComprobanteItemOrigen"]["Iva"]["d_iva"];
		        		 $producto["ComprobanteItem"]["id_unidad"]  = 	$producto["ComprobanteItemOrigen"]["id_unidad"];//la unidad viene en la Orden de Compra
		        		 
		        		 $producto["ComprobanteItem"]["requiere_conformidad"]  = 	$producto["ComprobanteItemOrigen"]["requiere_conformidad"];
		        		 $producto["ComprobanteItem"]["valor_moneda"]  = 	$producto["ComprobanteItemOrigen"]["Comprobante"]["valor_moneda"];
		        		 $producto["ComprobanteItem"]["valor_moneda2"]  = 	$producto["ComprobanteItemOrigen"]["Comprobante"]["valor_moneda2"];
		        		 $producto["ComprobanteItem"]["valor_moneda3"]  = 	$producto["ComprobanteItemOrigen"]["Comprobante"]["valor_moneda3"];
		        		 
		        		 
		        		 
		        		 
		        	}else 
		        	{
		        		
		        		/*Si el remito es libre te pide IRM si o si*/
		        		
		        		$facturados =$this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItem"], $this->Comprobante->getTiposComprobanteController(EnumController::FacturasCompra) );
		        		$producto["ComprobanteItem"]["total_facturado"] = (string)  number_format((float)$this->ComprobanteItem->GetCantidadItemProcesados($facturados), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		        		$producto["ComprobanteItem"]["cantidad_pendiente_a_facturar"] = (string) number_format( (float)($producto["ComprobanteItem"]["cantidad"] - $producto["ComprobanteItem"]["total_facturado"]), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		        		
		        		
		        		$irm_conformados =$this->ComprobanteItem->GetItemprocesadosEnImportacion($producto["ComprobanteItem"], $this->Comprobante->getTiposComprobanteController(EnumController::InformeRecepcionMateriales),array(EnumEstadoComprobante::CerradoReimpresion));
		        		
		        		
		        		$producto["ComprobanteItem"]["cantidad_recibida_irm"] = (string)  number_format((float)$this->ComprobanteItem->GetCantidadItemProcesados($irm_conformados,"cantidad_cierre"), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		        		$producto["ComprobanteItem"]["cantidad_pendiente_irm"] = (string)  ($producto["ComprobanteItem"]["cantidad"] - $producto["ComprobanteItem"]["cantidad_recibida_irm"] );
		        		
		        		
		        		$producto["ComprobanteItem"]["cantidad_aprobada_a_facturar"] =
		        		(string) number_format( (float)($producto["ComprobanteItem"]["cantidad_recibida_irm"]
		        				- $producto["ComprobanteItem"]["total_facturado"]), $producto["ComprobanteItem"]["cantidad_ceros_decimal"], '.', '');
		        
		        		
		        		
		        		$producto["ComprobanteItem"]["total_orden_compra_origen"]  = 0;
		        		$producto["ComprobanteItem"]["total_pendiente_a_remitir_origen"]  = 0;
		        		$producto["ComprobanteItem"]["total"]  = 0;
		        		$producto["ComprobanteItem"]["precio_unitario"]  = 0;
		        		$producto["ComprobanteItem"]["requiere_conformidad"] = 0;/*sino viene desde OC no deberia pasar Remito Libre*/
		       
		        	}
		        	
		        	
		        	
		                 $producto["ComprobanteItem"]["d_producto"] = $producto["Producto"]["d_producto"];
		                 $producto["ComprobanteItem"]["id_unidad"] = $producto["Producto"]["id_unidad"];//Si es un Remito Libre la unidad viene del Producto
		                 $producto["ComprobanteItem"]["codigo"] = $this->ComprobanteItem->getCodigoFromProducto($producto);
		                
		                 //$producto["ComprobanteItem"]["total"] = (string) $this->{$this->model}->getTotalItem($producto);;
		                 
		                 unset($producto["Producto"]);
		                 unset($producto["Comprobante"]);
		                 unset($producto["ComprobanteItemOrigen"]);
		                 
		                 $this->FiltrarCamposOnTheFly($data,$producto,$key);
		       break;
        		case EnumTipoFiltroComprobanteItem::agrupadaporPersona:
        			
        			$producto["ComprobanteItem"]["total"] = $producto[0]["total"] ;
        			$producto["ComprobanteItem"]["razon_social"] = $producto["Persona"]["razon_social"];
        			
        			break;
        			
        	}	
                 
                 
                 
                 
        }
        
        $this->data = $this->prepararRespuesta($data,$page_count);
        
        $output = array(
            "status" =>EnumError::SUCCESS,
            "message" => "list",
        		"content" =>   $this->data,
            "page_count" =>$page_count
        );
        $this->set($output);
        $this->set("_serialize", array("status", "message","page_count", "content"));
	}
	//fin vista json
	}
    
    
     /**
    * @secured(CONSULTA_REMITO_COMPRA)
    */
    public function getModel($vista='default'){
        
        $model = parent::getModelCamposDefault();
        $model = $this->editforView($model,$vista);//esta funcion edita y agrega campos para la vista
     
    }
    
    
    private function editforView($model,$vista){  //esta funcion recibe el model y pone los campos que se van a ver en la grilla
    
        $this->set('model',$model);
        Configure::write('debug',0);
        $this->render($vista);  
        
        
      
 
    }
    
    
    public function prepararRespuesta($data,&$page_count){
    	
    	
    	
    	$filtro_agrupado = $this->getFromRequestOrSession('ComprobanteItem.id_agrupado');//dice como mostrar los items
    	$array_id_comprobantes = array();
    	
    	
    	
    	if($filtro_agrupado == EnumTipoFiltroComprobanteItem::agrupadaporComprobante){ //si es la vista agrupada tomo los filtros de los items y agrupo en memoria por id_comprobante
    		
    		
    		
    		foreach($data as $key=>$valor){
    			if(in_array($valor["ComprobanteItem"]["id_comprobante"],$array_id_comprobantes))
    				unset($data[$key]);
    				else
    					array_push($array_id_comprobantes,$valor["ComprobanteItem"]["id_comprobante"]);
    					
    		}
    		
    		
    		
    		
    		
    		
    	}
    	
    	return parent::prepararRespuesta($data,$page_count);
    }

    
    /**
     * @secured(BTN_EXCEL_REMITOSCOMPRA)
     */
    public function excelExport($vista="default",$metodo="index",$titulo=""){
    	
    	parent::excelExport($vista,$metodo,"Listado de Items de Mercaderia de Proveedores");
    	
    	
    	
    }
    
   
    
}
?>