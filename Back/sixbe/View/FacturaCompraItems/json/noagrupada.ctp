<?php

    include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');
        
			$model["razon_social"]["show_grid"] = 1;
            $model["razon_social"]["header_display"] = "Raz&oacute;n Social";
            $model["razon_social"]["order"] = "0";
            $model["razon_social"]["width"] = "11";
            
			$model["pto_nro_comprobante"]["show_grid"] = 1;
            $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
            $model["pto_nro_comprobante"]["order"] = "1";
            $model["pto_nro_comprobante"]["width"] = "9";
			
			
              
            $model["n_item"]["show_grid"] = 1;
            $model["n_item"]["header_display"] = "Nº Item";
            $model["n_item"]["order"] = "3";
            $model["n_item"]["width"] = "2";
            
            $model["codigo"]["show_grid"] = 1;
            $model["codigo"]["header_display"] = "C&oacute;digo";
            $model["codigo"]["order"] = "4";
            $model["codigo"]["width"] = "9";
			$model["codigo"]["text_colour"] = "#000099";//BLUE

            
            $model["fecha_generacion"]["show_grid"] = 1;
            $model["fecha_generacion"]["header_display"] = "Fch Generaci&oacute;n";
            $model["fecha_generacion"]["order"] = "5";
            $model["fecha_generacion"]["width"] = "8";
            
			$model["fecha_entrega"]["show_grid"] = 1;
            $model["fecha_entrega"]["header_display"] = "Fch Entrega";
            $model["fecha_entrega"]["order"] = "6";
            $model["fecha_entrega"]["width"] = "7";
			$model["fecha_entrega"]["text_colour"] = "#AA1100";//RED GAO
            
       
            
            $model["cantidad"]["show_grid"] = 1;
            $model["cantidad"]["header_display"] = "Cantidad";
            $model["cantidad"]["order"] = "10";            
            $model["cantidad"]["width"] = "5";
			$model["cantidad"]["text_colour"] = "#0000FF";//BLUE
			
			
            $model["total_facturado"]["show_grid"] = 1;
            $model["total_facturado"]["header_display"] = "Cant. FC";
            $model["total_facturado"]["order"] = "11";
            $model["total_facturado"]["width"] = "5";
			$model["total_facturado"]["text_colour"] = "#00AA00";//GREEN
            
            $model["cantidad_pendiente_a_facturar"]["show_grid"] = 1;
            $model["cantidad_pendiente_a_facturar"]["header_display"] = "Cant. Pend FC";
            $model["cantidad_pendiente_a_facturar"]["order"] = "12";            
            $model["cantidad_pendiente_a_facturar"]["width"] = "5";
			$model["cantidad_pendiente_a_facturar"]["text_colour"] = "#990099";//RED VIOLET
            
            
            $model["total_remitido"]["show_grid"] = 1;
            $model["total_remitido"]["header_display"] = "Cant. RM";
            $model["total_remitido"]["order"] = "13";
            $model["total_remitido"]["width"] = "5";
            $model["total_remitido"]["text_colour"] = "#660099";//RED VIOLET
            
			
			$model["cantidad_pendiente_a_remitir"]["show_grid"] = 1;
            $model["cantidad_pendiente_a_remitir"]["header_display"] = "Cant. Pend. RM";
            $model["cantidad_pendiente_a_remitir"]["order"] = "14";            
            $model["cantidad_pendiente_a_remitir"]["width"] = "5";
			$model["cantidad_pendiente_a_remitir"]["text_colour"] = "#881188";//RED VIOLET
			
            $model["d_estado_comprobante"]["show_grid"] = 1;
            $model["d_estado_comprobante"]["header_display"] = "Estado";
            $model["d_estado_comprobante"]["order"] = "15";            
            $model["d_estado_comprobante"]["width"] = "9";
            $model["d_estado_comprobante"]["text_colour"] = "#FF0000";//RED

 $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItem",
                    "content" => $model
                );
        
          echo json_encode($output);
?>