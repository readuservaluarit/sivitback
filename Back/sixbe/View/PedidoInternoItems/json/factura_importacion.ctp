<?php

             include(APP.'View'.DS.'ComprobanteItems'.DS.'json'.DS.'default.ctp');
             
             App::import('Model','Comprobante');
			$comprobante = new Comprobante();
			$produccion = $comprobante->getActivoModulo(EnumModulo::PRODUCCION);
        
		
            
			$model["pto_nro_comprobante"]["show_grid"] = 1;
            $model["pto_nro_comprobante"]["header_display"] = "Nro. Comprobante";
            $model["pto_nro_comprobante"]["order"] = "1";
            $model["pto_nro_comprobante"]["width"] = "15";
			
			$model["orden_compra_externa"]["show_grid"] = 1;
            $model["orden_compra_externa"]["header_display"] = "OC Externa";
            $model["orden_compra_externa"]["order"] = "2";            
            $model["orden_compra_externa"]["width"] = "5";
			
              
            $model["n_item"]["show_grid"] = 1;
            $model["n_item"]["header_display"] = "Nro Item";
            $model["n_item"]["order"] = "3";
            $model["n_item"]["width"] = "2";
            
            $model["codigo"]["show_grid"] = 1;
            $model["codigo"]["header_display"] = "C&oacute;digo";
            $model["codigo"]["order"] = "4";
            $model["codigo"]["width"] = "12";
			$model["codigo"]["text_colour"] = "#000099";//BLUE

            
            $model["fecha_generacion"]["show_grid"] = 1;
            $model["fecha_generacion"]["header_display"] = "Fch Generaci&oacute;n";
            $model["fecha_generacion"]["order"] = "5";
            $model["fecha_generacion"]["width"] = "8";
            
			$model["fecha_entrega"]["show_grid"] = 1;
            $model["fecha_entrega"]["header_display"] = "Fch Entrega";
            $model["fecha_entrega"]["order"] = "6";
            $model["fecha_entrega"]["width"] = "7";
			$model["fecha_entrega"]["text_colour"] = "#AA1100";//RED GAO
            
         
            
            
            $model["cantidad"]["show_grid"] = 1;
            $model["cantidad"]["header_display"] = "Cantidad";
            $model["cantidad"]["order"] = "10";            
            $model["cantidad"]["width"] = "5";
			$model["cantidad"]["text_colour"] = "#0000FF";//BLUE
			
			
            $model["total_facturado"]["show_grid"] = 1;
            $model["total_facturado"]["header_display"] = "Cant. FC";
            $model["total_facturado"]["order"] = "11";
            $model["total_facturado"]["width"] = "5";
			$model["total_facturado"]["text_colour"] = "#00AA00";//GREEN
            
            $model["cantidad_pendiente_a_facturar"]["show_grid"] = 1;
            $model["cantidad_pendiente_a_facturar"]["header_display"] = "Cant. Pend FC";
            $model["cantidad_pendiente_a_facturar"]["order"] = "12";            
            $model["cantidad_pendiente_a_facturar"]["width"] = "5";
			$model["cantidad_pendiente_a_facturar"]["text_colour"] = "#990099";//RED VIOLET
            
            
            $model["total_remitido"]["show_grid"] = 1;
            $model["total_remitido"]["header_display"] = "Cant. RM";
            $model["total_remitido"]["order"] = "13";
            $model["total_remitido"]["width"] = "5";
            $model["total_remitido"]["text_colour"] = "#660099";//RED VIOLET
            
            
            

			
			$model["cantidad_pendiente_a_remitir"]["show_grid"] = 1;
            $model["cantidad_pendiente_a_remitir"]["header_display"] = "Cant. Pend. RM";
            $model["cantidad_pendiente_a_remitir"]["order"] = "14";            
            $model["cantidad_pendiente_a_remitir"]["width"] = "5";
			$model["cantidad_pendiente_a_remitir"]["text_colour"] = "#881188";//RED VIOLET
			
			
			
			
			
			$model["d_moneda_simbolo"]["show_grid"] = 1;
            $model["d_moneda_simbolo"]["header_display"] = "Moneda";
            $model["d_moneda_simbolo"]["order"] = "20";            
            $model["d_moneda_simbolo"]["width"] = "5";
			$model["d_moneda_simbolo"]["text_colour"] = "#881188";//RED VIOLET
			
			
			$model["precio_unitario"]["show_grid"] = 1;
            $model["precio_unitario"]["header_display"] = "P.Unit.";
            $model["precio_unitario"]["order"] = "21";            
            $model["precio_unitario"]["width"] = "5";

			
			$model["descuento_unitario"]["show_grid"] = 1;
            $model["descuento_unitario"]["header_display"] = "Desc.Unit.";
            $model["descuento_unitario"]["order"] = "22";            
            $model["descuento_unitario"]["width"] = "5";

			
			$model["total"]["show_grid"] = 1;
            $model["total"]["header_display"] = "Total";
            $model["total"]["order"] = "23";            
            $model["total"]["width"] = "5";
			$model["total"]["text_colour"] = "#FF0000";//RED
			
			
            $model["d_estado_comprobante"]["show_grid"] = 1;
            $model["d_estado_comprobante"]["header_display"] = "Estado";
            $model["d_estado_comprobante"]["order"] = "24";            
            $model["d_estado_comprobante"]["width"] = "9";
            $model["d_estado_comprobante"]["text_colour"] = "#FF0000";//RED

 $output = array(
                    "status" =>EnumError::SUCCESS,
                    "message" => "ComprobanteItem",
                    "content" => $model
                );
        
          echo json_encode($output);
?>