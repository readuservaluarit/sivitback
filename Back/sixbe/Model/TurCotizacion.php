<?php
App::uses('AppModel', 'Model');
App::uses('Comprobante', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class TurCotizacion extends Comprobante {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'comprobante';

    public $actsAs = array('Containable');


}
