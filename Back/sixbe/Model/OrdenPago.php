<?php
App::uses('AppModel', 'Model');
/**
 * AreaExterior Model
 *
 * @property Predio $Predio
 * @property TipoAreaExterior $TipoAreaExterior
 * @property TipoTerminacionPiso $TipoTerminacionPiso
 * @property EstadoConservacion $EstadoConservacion
 * @property Tipo $Tipo
 */
class OrdenPago extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'orden_pago';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
		
        /*
		'cuit' => array(
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Debe ingresar un CUIT v&aacute;lido',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 * 
 */
 
 public $belongsTo = array(
        'Proveedor' => array(
            'className' => 'Proveedor',
            'foreignKey' => 'id_proveedor',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Moneda' => array(
            'className' => 'Moneda',
            'foreignKey' => 'id_moneda',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CondicionPago' => array(
            'className' => 'CondicionPago',
            'foreignKey' => 'id_condicion_pago',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'OrdenCompra' => array(
            'className' => 'OrdenCompra',
            'foreignKey' => 'id_orden_compra',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'PuntoVenta' => array(
            'className' => 'PuntoVenta',
            'foreignKey' => 'id_punto_venta',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
        
       
 );
 
 public $hasMany = array(
        'OrdenPagoRecepcionItem' => array(
            'className' => 'OrdenPagoRecepcionItem',
            'foreignKey' => 'id_orden_pago',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        );
 

	

}
